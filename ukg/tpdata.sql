-- current pay period dates: '12/05/2021'  '12/18/2021'
dimEmployee @ 12:03
factclockhours @ 12:20

FROM:
    UPDATE tpdata
    SET TechClockhoursDay = x.clockhours,
        TechVacationHoursDay = x.Vacation,
        TechPTOHoursDay = x.PTO,
        TechHolidayHoursDay = x.Holiday
    FROM (    
      SELECT b.thedate, c.employeenumber, SUM(clockhours) AS clockhours,
        SUM(coalesce(VacationHours,0)) AS vacation,
        SUM(coalesce(PTOHours,0)) AS pto,
        SUM(coalesce(HolidayHours,0)) AS holiday
      FROM dds.edwClockHoursFact a
      INNER JOIN dds.day b on a.datekey = b.datekey
        AND b.thedate BETWEEN @from_date AND curdate()
      LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
      GROUP BY b.thedate, c.employeenumber) x 
    WHERE tpdata.thedate = x.thedate
      AND tpdata.employeenumber = x.employeenumber;		

TO:			
      SELECT b.thedate, c.employee_number, SUM(clock_hours) AS TechClockhoursDay,
        SUM(coalesce(vac_hours,0)) AS TechVacationHoursDay,
        SUM(coalesce(pto_hours,0)) AS TechPTOHoursDay,
        SUM(coalesce(hol_hours,0)) AS TechHolidayHoursDay
      FROM clock_hours a
      INNER JOIN dds.day b on a.the_date = b.thedate
        AND b.thedate BETWEEN '12/05/2021' AND curdate()
      LEFT JOIN employees c on a.employee_key = c.employee_key
      GROUP BY b.thedate, c.employee_number			
			

SELECT * FROM sco.tpdata WHERE thedate = '12/05/2021'	


SELECT aa.*, bb.thedate, bb.employeenumber, bb.lastname, bb.TechClockhoursDay ,bb.TechVacationHoursDay , bb.TechPTOHoursDay, bb.TechHolidayHoursDay 
FROM (
      SELECT b.thedate, c.employee_number, SUM(clock_hours) AS TechClockhoursDay,
        SUM(coalesce(vac_hours,0)) AS TechVacationHoursDay,
        SUM(coalesce(pto_hours,0)) AS TechPTOHoursDay,
        SUM(coalesce(hol_hours,0)) AS TechHolidayHoursDay
      FROM clock_hours a
			JOIN sco.tpdata aa on a.the_date = aa.thedate AND a.employee_number = aa.employeenumber
      INNER JOIN dds.day b on a.the_date = b.thedate
        AND b.thedate BETWEEN '12/05/2021' AND curdate()
      LEFT JOIN employees c on a.employee_key = c.employee_key
      GROUP BY b.thedate, c.employee_number) aa
full OUTER JOIN #wtf bb on aa.thedate = bb.thedate
  AND aa.employee_number = bb.employeenumber
WHERE aa.employee_number = '1114921'


  select * 
	INTO #wtf
	from sco.tpdata
	where thedate BETWEEN '12/05/2021' AND curdate()