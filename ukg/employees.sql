
/*
CREATE TABLE edwEmployeeDim ( 
      EmployeeKey AutoInc,
      StoreCode CIChar( 3 ),
      Store CIChar( 30 ),
      EmployeeNumber CIChar( 7 ),
      ActiveCode CIChar( 1 ),
      Active CIChar( 8 ),
      FullPartTimeCode CIChar( 1 ),
      FullPartTime CIChar( 8 ),
      PyDeptCode CIChar( 2 ),
      PyDept CIChar( 30 ),
      DistCode CIChar( 4 ),
      Distribution CIChar( 25 ),
      TechNumber CIChar( 3 ),
      Name CIChar( 80 ),
      BirthDate Date,
      BirthDateKey Integer,
      HireDate Date,
      HireDateKey Integer,
      TermDate Date,
      TermDateKey Integer,
      CurrentRow Logical,
      RowChangeDate Date,
      RowChangeDateKey Integer,
      RowFromTS TimeStamp,
      RowThruTS TimeStamp,
      RowChangeReason CIChar( 200 ),
      EmployeeKeyFromDate Date,
      EmployeeKeyFromDateKey Integer,
      EmployeeKeyThruDate Date,
      EmployeeKeyThruDateKey Integer,
      PayrollClassCode CIChar( 1 ),
      PayrollClass CIChar( 12 ),
      Salary Money,
      HourlyRate Money,
      PayPeriodCode CIChar( 1 ),
      PayPeriod CIChar( 15 ),
      ManagerName CIChar( 80 ),
      LastName CIChar( 25 ),
      FirstName CIChar( 25 ),
      MiddleName CIChar( 25 )) IN DATABASE;
*/		
DROP TABLE employees;	
CREATE TABLE employees ( 
      employee_key integer,
      store CIChar( 3 ),
      employee_number CIChar( 7 ),
      active_code CIChar( 1 ),
      active CIChar( 8 ),
      full_part CIChar( 8 ),
      dept_code CIChar( 2 ),
      dept CIChar( 30 ),
      DistCode CIChar( 4 ),
      name CIChar( 80 ),
      birth_date Date,
      hire_date Date,
      term_date Date,
      payroll_class_code CIChar( 1 ),
      pay_period_code CIChar( 1 ),
      last_name CIChar( 25 ),
      first_name CIChar( 25 ),
      middle_name CIChar( 25 )) IN DATABASE;



INSERT INTO employees
select 
     EmployeeKey AutoInc,
      StoreCode ,
      EmployeeNumber,
      ActiveCode,
      Active,
			FullPartTimeCode,
      PyDeptCode,
      PyDept,
      DistCode ,
      Name,
      BirthDate,
      HireDate ,
      TermDate,
      PayrollClassCode,
      PayPeriodCode,
      LastName ,
      FirstName,
      MiddleName
FROM dds.edwEmployeeDim
WHERE currentrow
  AND activecode <> 'T';
	
select * FROM employees	