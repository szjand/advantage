select a.thedate, b.storecode, b.flagDeptCode, d.clockhours
FROM dds.day a
LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
  AND b.Active = true
  AND b.employeenumber <> 'NA'
  AND b.flagDeptCode <> 'NA'
  AND b.techNumber NOT IN ('d01','d36','d06')
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND b.storeCode = c.storecode
  AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
LEFT JOIN dds.edwClockHoursFact d on a.datekey = d.datekey
  AND c.employeeKey = d.employeeKey  
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()  ) n on n.thedate BETWEEN m.fromdate AND m.thruDate  
  GROUP BY fromDate, thruDate, storecode, flagDeptCode) y on x.storecode = y.storecode AND x.flagDeptCode = y.flagDeptCode AND x.fromDate = y.fromDate
WHERE y.clockhours IS NOT NULL;  

SELECT * FROM (
select a.thedate, d.storecode, d.flagDeptCode, b.clockhours, d.technumber, d.name
FROM dds.day a -- range of days
INNER JOIN dds.edwClockHoursFact b on a.dateKey = b.dateKey
INNER JOIN dds.edwEmployeeDim c on b.employeeKey = c.employeeKey
  AND b.employeeKey = c.employeeKey
  AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate -- correct techkey
--  AND a.thedate BETWEEN c.hiredate AND c.termdate
LEFT JOIN dds.dimTech d on c.employeeNumber = d.employeeNumber
  AND d.Active = true -- tech IS active 
  AND d.employeeNumber <> 'NA'
  AND d.flagDeptCode <> 'NA' 
  AND a.theDate BETWEEN d.techKeyFromDate AND d.techKeyThruDate 
WHERE a.thedate BETWEEN '01/01/2014' AND curdate() -- date range
  AND d.storecode IS NOT NULL 
) x where name LIKE 'berry%'

DROP TABLE tmpDeptTechCensus;
CREATE table tmpDeptTechCensus (
  theDate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  flagDeptCode cichar(2) constraint NOT NULL,
  techNumber cichar(3) constraint NOT NULL,
  techKey integer constraint NOT NULL,
  employeeNumber cichar(7) constraint NOT NULL,
  employeeKey integer constraint NOT NULL,
  lastName cichar(25) constraint NOT NULL,
  firstName cichar(25) constraint NOT NULL,
  fullName cichar(51) constraint NOT NULL,
  fullPartTime cichar(8) constraint NOT NULL, 
  constraint pk primary key (theDate,storecode,techKey)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpDeptTechCensus','tmpDeptTechCensus.adi','theDate',
   'theDate','',2,512,'' );   
  
INSERT INTO tmpDeptTechCensus
select a.thedate, d.storecode, d.flagDeptCode, d.technumber, d.techKey,
  c.employeeNumber, c.employeeKey, c.lastName,
  c.firstName, TRIM(c.firstName) + ' ' + TRIM(c.lastName), c.fullPartTime
FROM dds.day a -- range of days
--INNER JOIN dds.edwClockHoursFact b on a.dateKey = b.dateKey
INNER JOIN dds.edwEmployeeDim c on a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate -- correct techkey
  AND a.thedate BETWEEN c.hiredate AND c.termdate
LEFT JOIN dds.dimTech d on c.employeeNumber = d.employeeNumber
  AND d.Active = true -- tech IS active 
  AND d.employeeNumber <> 'NA'
  AND d.flagDeptCode <> 'NA' 
  AND a.theDate BETWEEN d.techKeyFromDate AND d.techKeyThruDate 
WHERE a.thedate BETWEEN '01/01/2014' AND curdate() -- date range
  AND d.storecode IS NOT NULL;
  
select *
FROM tmpDeptTechCensus  
WHERE lastname = 'stafford'

DELETE FROM shopPerformanceDataRolling;
INSERT INTO shopPerformanceDataRolling  
SELECT x.storecode, x.flagDeptCode, x.fromDate, x.thruDate, x.flagHours, y.clockHours, 
  round(1.0 * x.flaghours/y.clockhours, 2) 
FROM (  
  -- pretty good for flag hours
  SELECT storecode, fromDate, thruDate, flagDeptCode, round(SUM(flagHours), 2) AS flaghours
  FROM (
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate BETWEEN  '01/08/2014' AND curdate() -1) a
  LEFT JOIN (
    SELECT a.storecode, thedate, flaghours, c.flagdeptcode
    FROM dds.factrepairorder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimtech c on a.techkey = c.techkey
    WHERE b.thedate BETWEEN '01/08/2014' AND curdate() -1
      AND c.flagdeptcode NOT IN ('QL','NA', 'UN')) b on b.thedate BETWEEN a.fromDate AND a.thruDate 
  GROUP BY fromDate, thruDate, storecode, flagDeptCode) x
LEFT JOIN (  
-- clock hours USING tmpDeptTechCensus
  SELECT fromDate, thruDate, storecode, flagDeptCode, SUM(clockHours) AS clockHours
  FROM (
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate BETWEEN  '01/08/2014' AND curdate() -1) a
  LEFT JOIN (  
    SELECT m.thedate, o.storecode, o.flagDeptCode, sum(n.clockhours) AS clockHours
    FROM dds.day m -- range of days
    INNER JOIN dds.edwClockHoursFact n on m.dateKey = n.dateKey
    INNER JOIN tmpDeptTechCensus o on m.theDate = o.theDate
      AND n.employeeKey = o.employeeKey
    WHERE m.thedate BETWEEN '01/01/2014' AND curdate() -- date range
    GROUP BY m.thedate, o.storecode, o.flagDeptCode) b on b.thedate BETWEEN a.fromDate AND a.thruDate 
  GROUP BY fromDate, thruDate, storecode, flagDeptCode) y on x.storecode = y.storecode AND x.flagDeptCode = y.flagDeptCode AND x.fromDate = y.fromDate 

  
  
  
  

select thedate, fullPartTime, COUNT(*)
FROM tmpDeptTechCensus
WHERE storecode = 'ry1' AND flagdeptcode = 'mr'
GROUP BY thedate, fullPartTime

SELECT *
FROM tmpDeptTechCensus
WHERE storecode = 'ry1' AND flagdeptcode = 'mr'
AND thedate = '01/01/2014'
ORDER BY technumber

select * FROM tmpDeptTechCensus

SELECT thedate, storecode, 
  flagDeptCode, 
  SUM(CASE WHEN fullPartTime = 'Full' THEN 1 ELSE 0 END) AS "Full",
  SUM(CASE WHEN fullPartTime = 'Part' THEN 1 ELSE 0 END) AS "Part"
FROM tmpDeptTechCensus 
GROUP BY thedate, storecode, 
  flagDeptCode 
