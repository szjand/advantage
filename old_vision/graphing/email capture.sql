SELECT thedate
FROM dds.day
WHERE dayname = 'monday'
  AND thedate BETWEEN curdate() - 100 AND curdate()
  
SELECT storecode, transactionType, subDepartment
FROM cstfbEmailData  
GROUP BY storecode, transactionType, subDepartment


date:
  each monday for the preceding 12 weeks
  each date represents data for the preceding 90 days
metric:
  email capture
  net promoter scores
scope:
  market
  store
  sales
  service
    mr
    ql
    bs
score: 
  capture rate
  number of transactions

date   metric   scope  score  
3/24   email     GF     .47
                 RY1    .43
                 RY2    .60
                 ---    ---
3/17   email     ---    ---
3/10   email     ---    ---

-------------------------------------------------------------------------------
SELECT * FROM dds.day
select * FROM cstfbEmailData ORDER BY transactionDate

-- most recent AND previous 11 mondays
SELECT top 12 theDate
FROM dds.day
WHERE dayofWeek = 2
  AND theDate <= curdate() 
ORDER BY theDate DESC   

-- maybe need something LIKE sequence 
-- actually greg IS probably going to want his level shit, which IS derived
-- FROM the departmental hierarchy
-- what ever, this notion of scope that looks LIKE gf - ry1 - servicee - ALL really sucks
SELECT thruDate, 'GF  - All - All' AS Scope, transactions, 
  round(1.0 * validEmail/transactions, 2) AS captureRate
FROM (
  SELECT thruDate, COUNT(*) AS transactions, 
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS validEmail
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT top 12 theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= curdate()
      ORDER BY theDate DESC) a) b    
  LEFT JOIN cstfbEmailData c on c.transactionDate BETWEEN b.fromDate AND b.thruDate
  GROUP BY thruDate) d
UNION 
SELECT thruDate, 'GF  - ' + trim(storeCode) + ' - All', transactions, 
  round(1.0 * validEmail/transactions, 2) AS captureRate
FROM (
  SELECT thruDate, storeCode, COUNT(*) AS transactions, 
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS validEmail
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT top 12 theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= curdate()
      ORDER BY theDate DESC) a) b    
  LEFT JOIN cstfbEmailData c on c.transactionDate BETWEEN b.fromDate AND b.thruDate
  GROUP BY thruDate, storeCode) d