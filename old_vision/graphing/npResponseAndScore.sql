SELECT thruDate, 'GF  - All - All' AS Scope, transactions, 
  round(1.0 * validEmail/transactions, 2) AS captureRate
FROM (
  SELECT thruDate, COUNT(*) AS transactions, 
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS validEmail
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT top 12 theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= curdate()
      ORDER BY theDate DESC) a) b    
  LEFT JOIN cstfbNpData c on c.transactionDate BETWEEN b.fromDate AND b.thruDate
  GROUP BY thruDate) d

--sp.GetMarketScores  

DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = (
  SELECT MAX(thedate) - 90 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());
--INSERT INTO __output
SELECT COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate;


-- market
SELECT 'GF' AS market, 'All' as store, 'All' as department, fromDate, thruDate, 
  COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
FROM (
  SELECT mondays - 90 AS fromDate, mondays AS thruDate
  FROM (  
    SELECT top 12 theDate as mondays
    FROM dds.day
    WHERE dayofWeek = 2
      AND theDate <= curdate()
    ORDER BY theDate DESC) a) b
LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
GROUP BY fromDate, thruDate

select * FROM cstfbNpData
-- store
SELECT 'GF' as market, storecode AS store, 'All' as department, fromDate, thruDate, 
  COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
FROM (
  SELECT mondays - 90 AS fromDate, mondays AS thruDate
  FROM (  
    SELECT top 12 theDate as mondays
    FROM dds.day
    WHERE dayofWeek = 2
      AND theDate <= curdate()
    ORDER BY theDate DESC) a) b
LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
GROUP BY storecode, fromDate, thruDate


  AND storeCode = @store
  AND 
    CASE @department
      WHEN 'SALES' THEN transactionType = 'Sales'
      WHEN 'SERVICE' THEN transactionType = 'Service'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
    END;
    
-- 4/4

CREATE a TABLE to hold the rolling 90 day data    
CREATE market - store - department (transactionType) - subDepartment
THEN, UPDATE it each monday morning AS part of the scheduled data refresh

DROP TABLE cstfbNpDataRolling;
CREATE TABLE cstfbNpDataRolling (
  market cichar(6),
  storeCode cichar(3),
  department cichar(12),
  subDepartment cichar(12),
  fromDate date,
  thruDate date, 
  sent integer,
  returned integer,
  responseRate double(2),
  score double(2),
  constraint PK Primary Key(market,storeCode,department,subDepartment, fromDate,thruDate)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpDataRolling','cstfbNpDataRolling.adi','market','market',
   '',2,512, '' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpDataRolling','cstfbNpDataRolling.adi','storeCode','storeCode',
   '',2,512, '' );       
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpDataRolling','cstfbNpDataRolling.adi','department','department',
   '',2,512, '' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpDataRolling','cstfbNpDataRolling.adi','subDepartment','subDepartment',
   '',2,512, '' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpDataRolling','cstfbNpDataRolling.adi','thruDate','thruDate',
   '',2,512, '' );     

INSERT INTO cstfbNpDataRolling  
-- market
SELECT 'GF' AS market, 'All' as store, 'All' as department, 'All' as subDepartment,
  fromDate, thruDate, 
  COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
FROM (
  SELECT mondays - 90 AS fromDate, mondays AS thruDate
  FROM (  
    SELECT top 20 theDate as mondays
    FROM dds.day
    WHERE dayofWeek = 2
      AND theDate <= curdate()
    ORDER BY theDate DESC) a) b
LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
GROUP BY fromDate, thruDate;   

INSERT INTO cstfbNpDataRolling  
-- store
SELECT 'GF' as market, storecode AS store, 'All' as department, 'All' as subDepartment,
  fromDate, thruDate, 
  COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
FROM (
  SELECT mondays - 90 AS fromDate, mondays AS thruDate
  FROM (  
    SELECT top 20 theDate as mondays
    FROM dds.day
    WHERE dayofWeek = 2
      AND theDate <= curdate()
    ORDER BY theDate DESC) a) b
LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
GROUP BY storecode, fromDate, thruDate;

INSERT INTO cstfbNpDataRolling  
-- department
SELECT 'GF' as market, storecode AS store, left(transactionType, 12) as department, 'All' as subDepartment,
  fromDate, thruDate, 
  COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
    .01/COUNT(*)), 2)  AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
    100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
  -
  SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
    /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
FROM (
  SELECT mondays - 90 AS fromDate, mondays AS thruDate
  FROM (  
    SELECT top 20 theDate as mondays
    FROM dds.day
    WHERE dayofWeek = 2
      AND theDate <= curdate()
    ORDER BY theDate DESC) a) b
LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
GROUP BY storecode, transactionType, fromDate, thruDate;

INSERT INTO cstfbNpDataRolling  
-- subDepartment
SELECT 'GF' as market, storecode AS store, left(transactionType, 12) as department, 
  subDepartment,
  fromDate, thruDate, 
  COUNT(*) AS sent,
  SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
  round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
      .01/COUNT(*)), 2) AS responseRate,
  (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
        100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
      -
      SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
        /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
-- SELECT *    
FROM (
  SELECT mondays - 90 AS fromDate, mondays AS thruDate
  FROM (  
    SELECT top 20 theDate as mondays
    FROM dds.day
    WHERE dayofWeek = 2
      AND theDate <= curdate()
    ORDER BY theDate DESC) a) b
LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
WHERE c.subDepartment IN ('BS','MR','QL')
GROUP BY storecode, transactionType, subDepartment, fromDate, thruDate;