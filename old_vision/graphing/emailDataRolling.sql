DROP TABLE cstfbEmailDataRolling;
CREATE TABLE cstfbEmailDataRolling (
  market cichar(6),
  storeCode cichar(3),
  department cichar(12),
  subDepartment cichar(12),
  fromDate date,
  thruDate date, 
  transactions integer,
  validEmail integer,
  emailCaptureRate double(2),
  constraint PK Primary Key(market,storeCode,department,subDepartment, fromDate,thruDate)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailDataRolling','cstfbEmailDataRolling.adi','market','market',
   '',2,512, '' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailDataRolling','cstfbEmailDataRolling.adi','storeCode','storeCode',
   '',2,512, '' );       
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailDataRolling','cstfbEmailDataRolling.adi','department','department',
   '',2,512, '' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailDataRolling','cstfbEmailDataRolling.adi','subDepartment','subDepartment',
   '',2,512, '' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailDataRolling','cstfbEmailDataRolling.adi','thruDate','thruDate',
   '',2,512, '' );     

DELETE FROM cstfbEmailDataRolling;
-- initial load   
INSERT INTO cstfbEmailDataRolling  
-- market
SELECT d.*, round(1.0 * validEmails/sent, 2)
FROM (
  SELECT 'GF' AS market, 'All' as store, 'All' as department, 'All' as subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS validEmails
  FROM (
    SELECT a.thrudate, a.thrudate - 90 AS fromDate
    FROM (
      SELECT top 120 thedate AS thruDate
      FROM dds.day
      WHERE thedate <= curdate()
      ORDER BY thedate DESC ) a) b
  LEFT JOIN cstfbEmailData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY fromDate, thruDate) d;    

INSERT INTO cstfbEmailDataRolling  
-- stores
SELECT d.*, round(1.0 * validEmails/sent, 2)
FROM (
  SELECT 'GF' AS market, storeCode as store, 'All' as department, 'All' as subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS validEmails
  FROM (
    SELECT a.thrudate, a.thrudate - 90 AS fromDate
    FROM (
      SELECT top 120 thedate AS thruDate
      FROM dds.day
      WHERE thedate <= curdate()
      ORDER BY thedate DESC ) a) b
  LEFT JOIN cstfbEmailData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY storeCode, fromDate, thruDate) d;   
  
INSERT INTO cstfbEmailDataRolling  
-- department
SELECT d.market, d.store, 
  CASE department
    WHEN 'RO' THEN 'Service'
    WHEN 'Sale' THEN 'Sales'
  END AS department, 
  subDepartment, fromDate, thruDate, sent, validEmails,
  round(1.0 * validEmails/sent, 2) AS emailCaptureRate
FROM (
  SELECT 'GF' AS market, storeCode as store, left(transactionType, 12) as department,
    'All' as subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS validEmails
  FROM (
    SELECT a.thrudate, a.thrudate - 90 AS fromDate
    FROM (
      SELECT top 120 thedate AS thruDate
      FROM dds.day
      WHERE thedate <= curdate()
      ORDER BY thedate DESC ) a) b
  LEFT JOIN cstfbEmailData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY storeCode, transactionType, fromDate, thruDate) d;  
  
  
INSERT INTO cstfbEmailDataRolling  
-- subdepartment
SELECT d.market, d.store, 
  CASE department
    WHEN 'RO' THEN 'Service'
    WHEN 'Sale' THEN 'Sales'
  END AS department, 
  subDepartment, fromDate, thruDate, sent, validEmails,
  round(1.0 * validEmails/sent, 2) AS emailCaptureRate
FROM (
  SELECT 'GF' AS market, storeCode as store, left(transactionType, 12) as department,
    subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS validEmails
  FROM (
    SELECT a.thrudate, a.thrudate - 90 AS fromDate
    FROM (
      SELECT top 120 thedate AS thruDate
      FROM dds.day
      WHERE thedate <= curdate()
      ORDER BY thedate DESC ) a) b
  LEFT JOIN cstfbEmailData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY storeCode, transactionType, subDepartment, fromDate, thruDate) d;  
  
  
