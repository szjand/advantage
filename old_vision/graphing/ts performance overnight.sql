


SELECT flagdeptcode, SUM(oldClock) FROM #old GROUP BY flagdeptcode

1. including garret clock hours, emp# 141085
so, the issue IN old, IS i am including the employeenumber for any one
that was ever a tech within the time period (BETWEEN 1/8/14 AND curdate)
fuck me, once again
select * FROM #old WHERE employeenumber = '141085' AND oldclock > 0
SELECT SUM(oldClock) FROM #old WHERE employeenumber = '141085' -- yep, there IS the dif
select * FROM #new WHERE employeenumber = '141085' AND newclock > 0

need some sense of dept census for each day to correctly generate clock hours
ecode = b.storecode and a.flagdeptcode = b.flagdeptcode

-- daily census ?
select a.thedate, a.datekey, b.storecode, b.name, b.flagDeptCode, c.employeeKey,
  c.fullPartTime
FROM dds.day a
LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
  AND b.Active = true
  AND b.employeenumber <> 'NA'
  AND b.flagDeptCode <> 'NA'
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND b.storeCode = c.storecode
  AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()

-- for the purpose of a census, need to consider edwEmployeeDim term stat
-- doesn't matter for clock hours OR flag hours,, because a termed employee will NOT have
-- clockhours, but it does matter for the dept COUNT
-- *a* so, need to include the notion of an empKey being with hiredate/thrudate?

-- daily census 
-- day --
select a.thedate, a.datekey, b.storecode, c.firstname, c.lastname, b.techNumber, 
  b.flagDeptCode, c.employeeKey, c.employeeNumber,
  c.fullPartTime, pyDeptCode, pyDept, distCode, Distribution
FROM dds.day a
LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
  AND b.Active = true
  AND b.employeenumber <> 'NA'
  AND b.flagDeptCode <> 'NA'
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND b.storeCode = c.storecode
  AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
  AND a.thedate BETWEEN c.hireDate AND c.termDate -- *a*
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()


-- daily clock hours day--dimtech--dimEmp--factClockHours
select a.thedate, a.datekey, b.storecode, b.name, b.flagDeptCode, c.employeeKey, d.clockhours
FROM dds.day a
LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
  AND b.Active = true
  AND b.employeenumber <> 'NA'
  AND b.flagDeptCode <> 'NA'
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND b.storeCode = c.storecode
  AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
LEFT JOIN dds.edwClockHoursFact d on a.datekey = d.datekey
  AND c.employeeKey = d.employeeKey  
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()

-- daily clock hours, day--factClockHours--dimEmp--dimTech 
-- *a* this includes tyler espelien IN ry2, should NOT, he has
-- NOT been an active mr tech since 5/22/13
SELECT a.thedate, d.storecode, c.fullPartTime, c.firstname,
  c.lastname, d.flagdeptCode, b.clockHours, b.employeeKey
FROM dds.day a
LEFT JOIN dds.edwClockHoursFact b on a.datekey = b.datekey
LEFT JOIN dds.edwEmployeeDim c on b.employeeKey = c.employeeKey
LEFT JOIN dds.dimTech d on c.storecode = d.storecode
  AND c.employeeNumber = d.employeeNumber
  AND a.thedate BETWEEN d.techKeyFromDate AND d.techKeyThruDate
  AND d.active = true -- *a*
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()  
--  AND b.clockhours <> 0
  AND d.storecode IS NOT NULL 

-- daily
SELECT thedate, storecode, flagDeptCode, fullPartTime, SUM(clockHours)
FROM (
SELECT a.thedate, d.storecode, c.fullPartTime, c.firstname,
  c.lastname, d.flagdeptCode, b.clockHours
FROM dds.day a
LEFT JOIN dds.edwClockHoursFact b on a.datekey = b.datekey
LEFT JOIN dds.edwEmployeeDim c on b.employeeKey = c.employeeKey
LEFT JOIN dds.dimTech d on c.storecode = d.storecode
  AND c.employeeNumber = d.employeeNumber
  AND a.thedate BETWEEN d.techKeyFromDate AND d.techKeyThruDate
  AND d.active = true -- *a*
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()  
  AND b.clockhours <> 0
  AND d.storecode IS NOT NULL 
) x GROUP BY thedate, storecode, flagDeptCode, fullPartTime
-- 2 wks ending yesterday
SELECT storecode, flagdeptcode, SUM(clockHours)
FROM (
SELECT a.thedate, d.storecode, c.fullPartTime, c.firstname,
  c.lastname, d.flagdeptCode, b.clockHours
FROM dds.day a
LEFT JOIN dds.edwClockHoursFact b on a.datekey = b.datekey
LEFT JOIN dds.edwEmployeeDim c on b.employeeKey = c.employeeKey
LEFT JOIN dds.dimTech d on c.storecode = d.storecode
  AND c.employeeNumber = d.employeeNumber
  AND a.thedate BETWEEN d.techKeyFromDate AND d.techKeyThruDate
  AND d.active = true -- *a*
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()  
  AND b.clockhours <> 0
  AND d.storecode IS NOT NULL 
) x
WHERE thedate BETWEEN '05/05/2014' AND '05/18/2014'
GROUP BY  storecode, flagdeptcode 



-- daily dept head count
SELECT thedate, storecode, flagDeptCode, fullPartTime, COUNT(*)
FROM (-- daily census ?
  select a.thedate, a.datekey, b.storecode, b.name, b.flagDeptCode, c.employeeKey,
    c.fullPartTime
  FROM dds.day a
  LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
    AND b.Active = true
    AND b.employeenumber <> 'NA'
    AND b.flagDeptCode <> 'NA'
  INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
    AND b.storeCode = c.storecode
    AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
    AND a.thedate BETWEEN c.hireDate AND c.termDate
  WHERE a.thedate BETWEEN '01/01/2014' AND curdate()
) x GROUP BY thedate, storecode, flagDeptCode, fullPartTime


-- daily clock hours BY dept
SELECT storecode, flagdeptcode, SUM(clockHours) AS clockHours
FROM (
  select a.thedate, a.datekey, b.storecode, b.name, b.flagDeptCode, c.employeeKey, d.clockhours
  FROM dds.day a
  LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
    AND b.Active = true
    AND b.employeenumber <> 'NA'
    AND b.flagDeptCode <> 'NA'
  INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
    AND b.storeCode = c.storecode
    AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
  LEFT JOIN dds.edwClockHoursFact d on a.datekey = d.datekey
    AND c.employeeKey = d.employeeKey  
  WHERE a.thedate BETWEEN '01/01/2014' AND curdate()
  ) x 
  WHERE thedate BETWEEN '05/04/2014' AND '05/17/2014'
  GROUP BY storecode, flagdeptcode
  
  
-- what i am looking for IS the ability to drill INTO any of these numbers
-- AND see who IS actually included  


-- 5/19
-- these 2 return the same data ----------------------------------------------------------
-- daily clock hours day--dimtech--dimEmp--factClockHours
select a.thedate, a.datekey, b.storecode, b.name, b.flagDeptCode, c.employeeKey, d.clockhours
INTO #ddimTech
FROM dds.day a
LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
  AND b.Active = true
  AND b.employeenumber <> 'NA'
  AND b.flagDeptCode <> 'NA'
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND b.storeCode = c.storecode
  AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
LEFT JOIN dds.edwClockHoursFact d on a.datekey = d.datekey
  AND c.employeeKey = d.employeeKey  
WHERE a.thedate BETWEEN '01/01/2014' AND curdate();

-- daily clock hours, day--factClockHours--dimEmp--dimTech 
-- *a* this includes tyler espelien IN ry2, should NOT, he has
-- NOT been an active mr tech since 5/22/13
SELECT a.thedate, d.storecode, c.fullPartTime, c.firstname,
  c.lastname, d.flagdeptCode, b.clockHours, b.employeeKey, a.datekey
-- DROP TABLE #dfactCH  
INTO #dfactCH
FROM dds.day a
LEFT JOIN dds.edwClockHoursFact b on a.datekey = b.datekey
LEFT JOIN dds.edwEmployeeDim c on b.employeeKey = c.employeeKey
LEFT JOIN dds.dimTech d on c.storecode = d.storecode
  AND c.employeeNumber = d.employeeNumber
  AND a.thedate BETWEEN d.techKeyFromDate AND d.techKeyThruDate
  AND d.active = true -- *a*
  AND d.flagDeptCode <> 'NA' -- exclude nokelby
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()  
--  AND b.clockhours <> 0
  AND d.storecode IS NOT NULL; 
  
SELECT * FROM #dfactch
  
SELECT * 
FROM #ddimTech a
full OUTER JOIN #dfactCH b on a.datekey = b.datekey
  AND a.employeekey = b.employeekey
WHERE a.thedate IS NULL OR b.thedate IS NULL
 
SELECT * 
FROM #ddimTech a
full OUTER JOIN #dfactCH b on a.datekey = b.datekey
  AND a.employeekey = b.employeekey
WHERE a.clockhours <> b.clockhours

-- these 2 return the same data ----------------------------------------------------------