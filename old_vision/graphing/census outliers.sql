-- edwEmployeeDim storecode matches dimtech storecode
SELECT * FROM (
select a.thedate, a.datekey, b.storecode AS dt, c.storecode AS ed, c.firstname, c.lastname, b.techNumber, 
  b.flagDeptCode, c.employeeKey, c.employeeNumber,
  c.fullPartTime, pyDeptCode, pyDept, distCode, Distribution
FROM dds.day a
LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
  AND b.Active = true
  AND b.employeenumber <> 'NA'
  AND b.flagDeptCode <> 'NA'
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND b.storeCode = c.storecode
  AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
  AND a.thedate BETWEEN c.hireDate AND c.termDate -- *a*
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()
) x WHERE dt <> ed


SELECT storecode, flagdeptcode, pyDeptCode, pyDept, distCode, Distribution, COUNT(*)
FROM (
select a.thedate, a.datekey, c.storecode, c.firstname, c.lastname, b.techNumber, 
  b.flagDeptCode, c.employeeKey, c.employeeNumber,
  c.fullPartTime, pyDeptCode, pyDept, distCode, Distribution
FROM dds.day a
LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
  AND b.Active = true
  AND b.employeenumber <> 'NA'
  AND b.flagDeptCode <> 'NA'
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND b.storeCode = c.storecode
  AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
  AND a.thedate BETWEEN c.hireDate AND c.termDate -- *a*
WHERE a.thedate = curdate()
) x GROUP BY storecode, flagdeptcode, pyDeptCode, pyDept, distCode, Distribution

-- the outliers, important because, 1. wrong census, obviously, 
-- AND i suspect wrong clock hours, including clock hours that should NOT be included
SELECT storecode, flagdeptcode, pyDeptCode, pyDept, distCode, Distribution, firstname, lastname, technumber
FROM (
select a.thedate, a.datekey, c.storecode, c.firstname, c.lastname, b.techNumber, 
  b.flagDeptCode, c.employeeKey, c.employeeNumber,
  c.fullPartTime, pyDeptCode, pyDept, distCode, Distribution
FROM dds.day a
LEFT JOIN dds.dimTech b on a.theDate BETWEEN b.techKeyFromDate AND b.techKeyThruDate
  AND b.Active = true
  AND b.employeenumber <> 'NA'
  AND b.flagDeptCode <> 'NA'
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND b.storeCode = c.storecode
  AND a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate  
  AND a.thedate BETWEEN c.hireDate AND c.termDate -- *a*
WHERE a.thedate = curdate()
) x 
WHERE technumber IN ('d36','d12','d01','d06','d13', '262')
ORDER BY storecode, flagdeptcode, pyDeptCode, pyDept, distCode, Distribution

-- flag hours for the outliers
SELECT b.name, b.technumber, MIN(c.thedate), MAX(c.theDate), MIN(a.ro), MAX(a.ro)
FROM dds.factRepairOrder a
INNER JOIN dds.dimTech b on a.storecode = b.storecode 
  AND a.techKey = b.techKey
INNER JOIN dds.day c on a.flagdatekey = c.datekey
WHERE b.technumber IN ('d36','d12','d01','d06','d13', '262')
--  AND c.theyear = 2014
GROUP BY b.name, b.technumber

tech   most recent flag
d36    12/14/13          jacob berry '113560'
                                     termed 5/3/14
                                     dist FROM recon to pdq on 12/26/13
d01    11/11/13          gopal bhattarai '114200'
                                     termed 5/3/14
                                     dist FROM recon 11/21/13
d06    8/7/13            noel eichhorst '138380'
                                     dist FROM recon 8/26/13
d12    4/1/14            tyler magnuson '190600'
                                     dist FROM recon 3/26/14                                                     
262    3/4/14            kevin stafford '1130500'
                                     dist FROM re to bs 11/20/13
                                     termed 5/9/14 IN pymast, but AS clockhours thru 5/12, so chg termdate to 5/12 IN edwEmployeeDim       
                                     tech D21 FROM 3/1/13 - 12/1/13
                                     tech 262 FROM 12/2/13                                                           

--5/22 kevin stafford IS no longer employed, 
-- tyler magnuson body shop detailer, clock hours should NOT be included
-- his flag hours FROM 4/1 AND before are FROM WHEN he was a detail tech

select * 
FROM dds.dimtech a WHERE technumber = '526'
WHERE technumber IN ('d36','d01','d06','d12','262', 'd21')
ORDER BY technumber, techkey

-- 5/25
the issue IS IN figuring clock hours for the dept prof page
tech numbers active for active employees that are no longer active techs, 
  inflating the clock hours for the dept
so, what i need to DO IS de-activate the above techs IN arkona
am leary of doing that, but wtf, who ELSE even cares?
THEN DO a type 2 UPDATE IN dimtechs: active to inactive

DECLARE @nowTS timestamp;
DECLARE @nowDate date;
DECLARE @nowDateKey integer;
DECLARE @thruDate date;
DECLARE @thruDateKey integer;
DECLARE @fromDate date;
DECLARE @fromDateKey integer;
DECLARE @techKey integer;

@thruDate = '12/26/2013';
@fromDate = @thruDate + 1;
@techKey = 352;

@nowTs = (SELECT now() FROM system.iota);
@nowDate = (SELECT CAST(now() AS sql_date) FROM system.iota);
@nowDateKey = (SELECT datekey FROM dds.day WHERE thedate = @nowDate);
@thruDateKey = (SELECT datekey FROM dds.day WHERE thedate = @thruDate);
@fromDateKey = (SELECT datekey FROM dds.day WHERE theDate = @fromDate);
BEGIN TRANSACTION;
TRY 
-- UPDATE old record
UPDATE dds.dimTech
SET rowChangeDate = @nowDate,
    rowChangeDateKey = @nowDateKey,
    rowThruTS = @nowTs,
    currentRow = false,
    rowChangeReason = 'active to inactive',
    techKeyThruDate = @thruDate,
    techKeyThruDateKey = @thruDateKey
WHERE techKey = @techKey;
-- INSERT new row      
INSERT INTO dds.dimtech (storecode, employeenumber, name, description, technumber,
  active, laborcost, flagdeptcode, flagdept, currentrow, rowfromts, rowthruts, 
  techkeyfromdate, techkeyfromdatekey, techkeythrudate, techkeythrudatekey) 
SELECT storecode, employeenumber, name, description, technumber,
  false, laborcost, flagdeptcode, 
  CASE 
    WHEN flagdeptcode = 'BS' THEN 'Body Shop'
    WHEN flagdeptcode = 'AM' or flagdeptcode = 'MR' THEN 'Service'
    WHEN flagdeptcode = 'QL' THEN 'Quick Lane'
    WHEN flagdeptcode = 'RE' THEN 'Detail'
  END, true, @NowTS, CAST(NULL AS sql_timestamp),
  @fromDate, @fromDateKey, 
  (SELECT thedate FROM dds.day WHERE datetype = 'NA'),
  (SELECT datekey FROM dds.day WHERE datetype = 'NA') 
FROM dds.dimTech
WHERE techKey = @techKey;

INSERT INTO dds.keymapdimtech
SELECT techkey, storecode, technumber, active, flagdeptcode,
  laborcost, employeenumber
FROM dds.dimtech a
WHERE NOT EXISTS (
  SELECT 1 
  FROM dds.keymapdimtech
  WHERE techkey = a.techkey);
  
COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE xfmDimTech(666,__errtext);
END TRY; 


-- only since 1/1/2014
DROP TABLE tmpDeptTechCensus;
CREATE table tmpDeptTechCensus (
  theDate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  flagDeptCode cichar(2) constraint NOT NULL,
  techNumber cichar(3) constraint NOT NULL,
  techKey integer constraint NOT NULL,
  employeeNumber cichar(7) constraint NOT NULL,
  employeeKey integer constraint NOT NULL,
  lastName cichar(25) constraint NOT NULL,
  firstName cichar(25) constraint NOT NULL,
  fullName cichar(51) constraint NOT NULL,
  fullPartTime cichar(8) constraint NOT NULL,
  constraint pk primary key (theDate,storecode,techKey)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpDeptTechCensus','tmpDeptTechCensus.adi','theDate',
   'theDate','',2,512,'' );   
  
INSERT INTO tmpDeptTechCensus
select a.thedate, d.storecode, d.flagDeptCode, d.technumber, d.techKey,
  c.employeeNumber, c.employeeKey, c.lastName,
  c.firstName, TRIM(c.firstName) + ' ' + TRIM(c.lastName), c.fullparttime
FROM dds.day a -- range of days
--INNER JOIN dds.edwClockHoursFact b on a.dateKey = b.dateKey
INNER JOIN dds.edwEmployeeDim c on a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate -- correct techkey
  AND a.thedate BETWEEN c.hiredate AND c.termdate
LEFT JOIN dds.dimTech d on c.employeeNumber = d.employeeNumber
  AND d.Active = true -- tech IS active 
  AND d.employeeNumber <> 'NA'
  AND d.flagDeptCode <> 'NA' 
  AND a.theDate BETWEEN d.techKeyFromDate AND d.techKeyThruDate 
WHERE a.thedate BETWEEN '01/01/2014' AND curdate() -- date range
  AND d.storecode IS NOT NULL;
  
  
now, clockhours based on census, (ts3tempTechCensus.sql)