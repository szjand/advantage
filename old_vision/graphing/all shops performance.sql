/*
SELECT thedate, flaghours, c.flagdeptcode
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimtech c on a.techkey = c.techkey
WHERE b.thedate BETWEEN curdate() - 90 AND curdate() -1
  AND c.flagdeptcode NOT IN ('QL','NA')
  
  
SELECT thedate, a.storecode, c.flagdeptcode, round(sum(flaghours), 2)
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimtech c on a.techkey = c.techkey
WHERE b.thedate BETWEEN curdate() - 90 AND curdate() -1
  AND c.flagdeptcode NOT IN ('QL','NA')
GROUP BY thedate, a.storecode, c.flagdeptcode  

SELECT thedate - 7 AS fromDate, thedate AS thruDate
FROM dds.day
WHERE thedate BETWEEN  curdate() - 90 AND curdate() -1

SELECT thedate - 6 AS fromDate, thedate AS thruDate
FROM dds.day
WHERE thedate BETWEEN  curdate() - 90 AND curdate() -1

-- pretty good for flag hours
SELECT fromDate, thruDate, storecode, flagDeptCode, round(SUM(flagHours), 2) AS flaghours
FROM (
  SELECT thedate - 7 AS fromDate, thedate AS thruDate
  FROM dds.day
  WHERE thedate BETWEEN  curdate() - 90 AND curdate() -1) a
LEFT JOIN (
  SELECT a.storecode, thedate, flaghours, c.flagdeptcode
  FROM dds.factrepairorder a
  INNER JOIN dds.day b on a.flagdatekey = b.datekey
  INNER JOIN dds.dimtech c on a.techkey = c.techkey
  WHERE b.thedate BETWEEN curdate() - 90 AND curdate() -1
    AND c.flagdeptcode NOT IN ('QL','NA')) b on b.thedate BETWEEN a.fromDate AND a.thruDate 
GROUP BY fromDate, thruDate, storecode, flagDeptCode	

-- need clockhours

select * FROM dds.edwClockHoursFact 

-- employeenumber and flagdept that have flagged time IN the past 90 day
SELECT a.storecode, c.employeenumber, c.flagdeptcode
FROM dds.factrepairorder a
INNER JOIN dds.dimtech c on a.techkey = c.techkey
WHERE c.flagdeptcode NOT IN ('QL','NA')
  AND c.employeenumber <> 'NA' 
  AND a.flagdatekey IN (
    SELECT datekey
	FROM dds.day
	WHERE thedate BETWEEN curdate() - 90 AND curdate() -1) 
GROUP BY a.storecode, c.employeenumber, c.flagdeptcode 


SELECT d.*, f.clockhours, g.thedate
FROM (
  SELECT a.storecode, c.employeenumber, c.flagdeptcode
  FROM dds.factrepairorder a
  INNER JOIN dds.dimtech c on a.techkey = c.techkey
  WHERE c.flagdeptcode NOT IN ('QL','NA')
    AND c.employeenumber <> 'NA' 
    AND a.flagdatekey IN (
      SELECT datekey
  	FROM dds.day
  	WHERE thedate BETWEEN curdate() - 90 AND curdate() -1) 
  GROUP BY a.storecode, c.employeenumber, c.flagdeptcode) d 
LEFT JOIN dds.edwEmployeeDim e on d.storecode = e.storecode
  AND d.employeenumber = e.employeenumber
LEFT JOIN dds.edwClockHoursFact f on e.employeekey = f.employeekey
LEFT JOIN dds.day g on f.datekey = g.datekey
WHERE g.thedate BETWEEN curdate() - 90 AND curdate()

-- NOT bad for clockhours
SELECT fromDate, thruDate, storecode, flagDeptCode, round(SUM(clockhours), 2) AS flaghours
FROM (
  SELECT thedate - 7 AS fromDate, thedate AS thruDate
  FROM dds.day
  WHERE thedate BETWEEN  curdate() - 90 AND curdate() -1) m
LEFT JOIN (
  SELECT d.*, f.clockhours, g.thedate
  FROM (
    SELECT a.storecode, c.employeenumber, c.flagdeptcode
    FROM dds.factrepairorder a
    INNER JOIN dds.dimtech c on a.techkey = c.techkey
    WHERE c.flagdeptcode NOT IN ('QL','NA')
      AND c.employeenumber <> 'NA' 
      AND a.flagdatekey IN (
        SELECT datekey
    	FROM dds.day
    	WHERE thedate BETWEEN curdate() - 90 AND curdate() -1) 
    GROUP BY a.storecode, c.employeenumber, c.flagdeptcode) d 
  LEFT JOIN dds.edwEmployeeDim e on d.storecode = e.storecode
    AND d.employeenumber = e.employeenumber
  LEFT JOIN dds.edwClockHoursFact f on e.employeekey = f.employeekey
  LEFT JOIN dds.day g on f.datekey = g.datekey
  WHERE g.thedate BETWEEN curdate() - 90 AND curdate()) n on n.thedate BETWEEN m.fromdate AND m.thruDate  
GROUP BY fromDate, thruDate, storecode, flagDeptCode;  
*/

--drop TABLE zShopPerformanceDataRolling;
CREATE TABLE zShopPerformanceDataRolling (
  storeCode cichar(3) constraint NOT NULL,
  subDepartment cichar(2) constraint NOT NULL,
  fromDate date constraint NOT NULL,
  thruDate date constraint NOT NULL, 
  flagHours double default '0' constraint NOT NULL,
  clockHours double default '0' constraint NOT NULL,
  proficiency double default '0' constraint NOT NULL,
  constraint PK primary key (storecode,subdepartment,fromDate)) IN database;
 
DELETE FROM zShopPerformanceDataRolling;
INSERT INTO zShopPerformanceDataRolling  
SELECT x.storecode, x.flagDeptCode, x.fromDate, x.thruDate, x.flagHours, y.clockHours, 
  round(1.0 * x.flaghours/y.clockhours, 2) 
FROM (
  -- pretty good for flag hours
  SELECT storecode, fromDate, thruDate, storecode, flagDeptCode, round(SUM(flagHours), 2) AS flaghours
  FROM (
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate BETWEEN  '01/08/2014' AND curdate() -1) a
  LEFT JOIN (
    SELECT a.storecode, thedate, flaghours, c.flagdeptcode
    FROM dds.factrepairorder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimtech c on a.techkey = c.techkey
    WHERE b.thedate BETWEEN '01/08/2014' AND curdate() -1
      AND c.flagdeptcode NOT IN ('QL','NA')) b on b.thedate BETWEEN a.fromDate AND a.thruDate 
  GROUP BY fromDate, thruDate, storecode, flagDeptCode) x	
LEFT JOIN (
  SELECT storecode, fromDate, thruDate, storecode, flagDeptCode, round(SUM(clockhours), 2) AS clockHours
  FROM (
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate BETWEEN  curdate() - 120 AND curdate() -1) m
  LEFT JOIN (
    SELECT d.*, f.clockhours, g.thedate
    FROM (
      SELECT a.storecode, c.employeenumber, c.flagdeptcode
      FROM dds.factrepairorder a
      INNER JOIN dds.dimtech c on a.techkey = c.techkey
      WHERE c.flagdeptcode NOT IN ('QL','NA')
        AND c.employeenumber <> 'NA' 
        AND a.flagdatekey IN (
          SELECT datekey
      	FROM dds.day
      	WHERE thedate BETWEEN '01/08/2014' AND curdate() -1) 
      GROUP BY a.storecode, c.employeenumber, c.flagdeptcode) d 
    LEFT JOIN dds.edwEmployeeDim e on d.storecode = e.storecode
      AND d.employeenumber = e.employeenumber
    LEFT JOIN dds.edwClockHoursFact f on e.employeekey = f.employeekey
    LEFT JOIN dds.day g on f.datekey = g.datekey
    WHERE g.thedate BETWEEN '01/08/2014' AND curdate()) n on n.thedate BETWEEN m.fromdate AND m.thruDate  
  GROUP BY fromDate, thruDate, storecode, flagDeptCode) y on x.storecode = y.storecode AND x.flagDeptCode = y.flagDeptCode AND x.fromDate = y.fromDate
WHERE y.clockhours IS NOT NULL;   
  

create PROCEDURE getShopPerformanceRolling (  
  storeCode cichar(3),
  department cichar(12),
  fromDate date output,
  thruDate date output,
  flagHours double output,
  clockHours double output,
  proficiency double output)
BEGIN

/*
valid storeCodes: ry1, ry2
valid departments: mainshop, bodyshop, detail
EXECUTE PROCEDURE getShopPerformanceRolling('ry1','mainshop');

*/  
  
DECLARE @store string;
DECLARE @department string;

@store = upper((SELECT StoreCode FROM __input));
@department = upper((SELECT Department FROM __input));
INSERT INTO __output
SELECT top 12 fromDate, thruDate, flaghours, clockhours, proficiency
FROM zShopPerformanceDataRolling
WHERE upper(storeCode) = @store
  AND thruDate IN (
    SELECT top 12 thedate
    FROM dds.day
    WHERE thedate between '01/08/2014' AND curdate() - 1
      AND mod(curdate() - thedate - 8, 7) = 0
    ORDER BY theDate DESC)  
  AND 
    CASE @department
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
    END
ORDER BY thruDate; 
END;        


-- SELECT * FROM applications
INSERT INTO applications (appname, appcode) values ('proficiency','prof');

-- SELECT * FROM applicationroles
INSERT INTO applicationRoles (appname, role, appcode)
values ('proficiency','manager','prof');

-- SELECT * FROM applicationMetaData

INSERT INTO applicationMetaData 
values ('proficiency','prof',700,'manager','dropdown','mainlink_prof_html','','leftnavbar','',10);

INSERT INTO applicationMetaData 
values ('proficiency','prof',700,'manager','summary','sublink_prof_summary_html','prof.manager.summary','leftnavbar','',20);

-- SELECT * FROM employeeAppAuthorization
INSERT INTO employeeAppAuthorization
SELECT 'mhuot@rydellchev.com', appname, appseq, appcode, approle, functionality, CAST(NULL AS sql_integer)
FROM applicationmetadata 
WHERE appcode = 'prof';
  
      