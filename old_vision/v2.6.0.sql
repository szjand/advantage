v2.6.0
--< 1. dept proficiencies ------------------------------------------------------<


INSERT INTO applications (appname, appcode) values ('proficiency','prof');

-- SELECT * FROM applicationroles
INSERT INTO applicationRoles (appname, role, appcode)
values ('proficiency','manager','prof');

-- SELECT * FROM applicationMetaData

INSERT INTO applicationMetaData 
values ('proficiency','prof',700,'manager','dropdown','mainlink_prof_html','','leftnavbar','',10);

INSERT INTO applicationMetaData 
values ('proficiency','prof',700,'manager','summary','sublink_prof_summary_html','prof.manager.summary','leftnavbar','',20);


INSERT INTO employeeAppAuthorization
SELECT b.username, appname, appseq, appcode, approle, functionality, CAST(NULL AS sql_integer)
FROM applicationmetadata a, tpEmployees b 
WHERE appcode = 'prof'
  AND b.username IN ('jpenas@rydellchev.com','rsattler@rydellchev.com','msteinke@rydellchev.com',
    'neuliss@rydellcars.com','kfoster@rydellchev.com','aneumann@gfhonda.com',
	'kespelund@rydellchev.com','bcahalan@rydellchev.com','bfoster@rydellcars.com',
	'brian@rydellchev.com','mhuot@rydellchev.com','mlear@gfhonda.com', 'gsorum@cartiva.com');
	
	
CREATE TABLE shopPerformanceDataRolling (
  storeCode cichar(3) constraint NOT NULL,
  subDepartment cichar(2) constraint NOT NULL,
  fromDate date constraint NOT NULL,
  thruDate date constraint NOT NULL, 
  flagHours double default '0' constraint NOT NULL,
  clockHours double default '0' constraint NOT NULL,
  proficiency double default '0' constraint NOT NULL,
  constraint PK primary key (storecode,subdepartment,fromDate)) IN database;
 
DELETE FROM shopPerformanceDataRolling;
INSERT INTO shopPerformanceDataRolling  
SELECT x.storecode, x.flagDeptCode, x.fromDate, x.thruDate, x.flagHours, y.clockHours, 
  round(1.0 * x.flaghours/y.clockhours, 2) 
FROM (
  -- pretty good for flag hours
  SELECT storecode, fromDate, thruDate, storecode, flagDeptCode, round(SUM(flagHours), 2) AS flaghours
  FROM (
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate BETWEEN  '01/08/2014' AND curdate() -1) a
  LEFT JOIN (
    SELECT a.storecode, thedate, flaghours, c.flagdeptcode
    FROM dds.factrepairorder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimtech c on a.techkey = c.techkey
    WHERE b.thedate BETWEEN '01/08/2014' AND curdate() -1
      AND c.flagdeptcode NOT IN ('QL','NA')) b on b.thedate BETWEEN a.fromDate AND a.thruDate 
  GROUP BY fromDate, thruDate, storecode, flagDeptCode) x	
LEFT JOIN (
  SELECT storecode, fromDate, thruDate, storecode, flagDeptCode, round(SUM(clockhours), 2) AS clockHours
  FROM (
    SELECT thedate - 13 AS fromDate, thedate AS thruDate
    FROM dds.day
    WHERE thedate BETWEEN  curdate() - 120 AND curdate() -1) m
  LEFT JOIN (
    SELECT d.*, f.clockhours, g.thedate
    FROM (
      SELECT a.storecode, c.employeenumber, c.flagdeptcode
      FROM dds.factrepairorder a
      INNER JOIN dds.dimtech c on a.techkey = c.techkey
      WHERE c.flagdeptcode NOT IN ('QL','NA')
        AND c.employeenumber <> 'NA' 
        AND a.flagdatekey IN (
          SELECT datekey
      	FROM dds.day
      	WHERE thedate BETWEEN '01/08/2014' AND curdate() -1) 
      GROUP BY a.storecode, c.employeenumber, c.flagdeptcode) d 
    LEFT JOIN dds.edwEmployeeDim e on d.storecode = e.storecode
      AND d.employeenumber = e.employeenumber
    LEFT JOIN dds.edwClockHoursFact f on e.employeekey = f.employeekey
    LEFT JOIN dds.day g on f.datekey = g.datekey
    WHERE g.thedate BETWEEN '01/08/2014' AND curdate()) n on n.thedate BETWEEN m.fromdate AND m.thruDate  
  GROUP BY fromDate, thruDate, storecode, flagDeptCode) y on x.storecode = y.storecode AND x.flagDeptCode = y.flagDeptCode AND x.fromDate = y.fromDate
WHERE y.clockhours IS NOT NULL;   
  

create PROCEDURE getShopPerformanceRolling (  
  storeCode cichar(3),
  department cichar(12),
  fromDate date output,
  thruDate date output,
  flagHours double output,
  clockHours double output,
  proficiency double output)
BEGIN

/*
valid storeCodes: ry1, ry2
valid departments: mainshop, bodyshop, detail
EXECUTE PROCEDURE getShopPerformanceRolling('ry1','mainshop');

*/  
  
DECLARE @store string;
DECLARE @department string;

@store = upper((SELECT StoreCode FROM __input));
@department = upper((SELECT Department FROM __input));
INSERT INTO __output
SELECT top 12 fromDate, thruDate, flaghours, clockhours, proficiency
FROM shopPerformanceDataRolling
WHERE upper(storeCode) = @store
  AND thruDate IN (
    SELECT top 12 thedate
    FROM dds.day
    WHERE thedate between '01/08/2014' AND curdate() - 1
      AND mod(curdate() - thedate - 8, 7) = 0
    ORDER BY theDate DESC)  
  AND 
    CASE @department
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
    END
ORDER BY thruDate; 
END;       	


--/> 1. dept proficiencies -----------------------------------------------------/>

--< roPhoneCalls --------------------------------------------------------------<
-- 1. ro info
--DROP TABLE tmpRoPhoneNumbers1;
CREATE TABLE tmpRoPhoneNumbers1 (
  ro cichar(9) constraint NOT NULL,
  customerKey integer constraint NOT NULL, 
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  constraint pk primary key (ro))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers1','tmpRoPhoneNumbers1.adi','customerKey',
   'customerKey','',2,512,'' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers1','tmpRoPhoneNumbers1.adi','roVin',
   'ro;vin','',2,512,'' );       
INSERT INTO tmpRoPhoneNumbers1
SELECT distinct a.ro, a.customerKey, e.vin, a.rocreatedTS, b.thedate AS openDate, 
  c.thedate AS closeDate, d.theDate AS fcDate
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.day c on a.closedatekey = c.datekey
INNER JOIN dds.day d on a.finalclosedatekey = d.datekey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey
WHERE a.customerKey NOT IN ( -- eliminate ros for inventory vehicles
  SELECT customerKey
  FROM dds.dimCustomer
  WHERE fullname = 'inventory'
   OR lastname IN ('CARWASH','SHOP TIME'))
AND b.theyear = 2014;

-- 2.
-- phone numbers FROM scheduler
--DROP TABLE tmpRoPhoneNumbers2;
CREATE TABLE tmpRoPhoneNumbers2 (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  schedPhone cichar(10) constraint NOT NULL,
  schedPhoneRight7 cichar(7) constraint NOT NULL,
  constraint pk primary key (ro, schedPhone))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','roVin',
   'ro;vin','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','schedPhone',
   'schedPhone','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','schedPhoneRight7',
   'schedPhoneRight7','',2,512,'' );    
INSERT INTO tmpRoPhoneNumbers2
SELECT h.ro, h.vin, h.roCreatedTS, h.openDate, h.closeDate, h.fcDate,
  TRIM(schedPhone), right(TRIM(schedPhone), 7) 
FROM (
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.homephone) AS schedPhone
FROM dds.stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.homephone) <> '' 
-- GROUP BY b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, a.homephone
UNION 
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.bestPhoneToday)
FROM dds.stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.bestPhoneToday) <> '' 
--GROUP BY a.ro, a.bestPhoneToday
UNION 
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.cellPhone)
FROM dds.stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.cellPhone) <> '') h;
--GROUP BY a.ro, a.cellPhone
   
-- 3.
-- phone numbers FROM dimCustomer
--DROP TABLE tmpRoPhoneNumbers3;
CREATE TABLE tmpRoPhoneNumbers3 (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  arkPhone cichar(10) constraint NOT NULL,
  arkPhoneRight7 cichar(7) constraint NOT NULL,
  constraint pk primary key (ro, arkPhone))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','roVin',
   'ro;vin','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','arkPhone',
   'arkPhone','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','arkPhoneRight7',
   'arkPhoneRight7','',2,512,'' );      
INSERT INTO tmpRoPhoneNumbers3
SELECT ro, vin, roCreatedTS, openDate, closeDate, fcDate, trim(arkPhone),
  right(TRIM(arkPhone),7) AS arkPhone7
FROM (
  SELECT a.*, b.homephone AS arkPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.homePhone), 'x')) = 7 or length(coalesce(trim(b.homePhone), 'x')) = 10
  UNION
  SELECT a.*, b.businessPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.businessPhone), 'x')) = 7  OR length(coalesce(trim(b.businessPhone), 'x')) = 10
  UNION
  SELECT a.*, b.cellPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.cellPhone), 'x')) = 7 OR length(coalesce(trim(b.cellPhone), 'x')) = 10) g; 

 
-- 4.
-- all phone numbers for an ro
-- each ro can have 0:6 phone numbers
--DROP TABLE tmpRoPhoneNumbers;
CREATE TABLE tmpRoPhoneNumbers (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  customerPhoneNumber cichar(10) default '0' constraint NOT NULL,
  customerPhoneNumberRight7 cichar(7) default '0' constraint NOT NULL,
  constraint pk primary key (ro, customerPhoneNumber))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','ro',
   'ro','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','customerPhoneNumber',
   'customerPhoneNumber','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','customerPhoneNumberRight7',
   'customerPhoneNumberRight7','',2,512,'' ); 
INSERT INTO tmpRoPhoneNumbers
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  coalesce(b.schedPhone, '0'), coalesce(b.schedPhoneRight7, '0')
FROM tmpRoPhoneNumbers1 a
LEFT JOIN (
  SELECT * 
  FROM tmpRoPhoneNumbers2
  UNION 
  SELECT * 
  FROM tmpRoPhoneNumbers3) b on a.ro = b.ro;
  
  
-- include extCall.id for eventual linking to extConnect AND call copy
--DROP TABLE tmpRoCalls;
CREATE TABLE tmpRoCalls (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  customerPhoneNumber cichar(10) constraint NOT NULL,
  customerPhoneNumberRight7 cichar(7) constraint NOT NULL,
  callStartTS timestamp constraint NOT NULL,
  callDate date constraint NOT NULL,
  callDuration integer default '0' constraint NOT NULL, 
  extCallId integer constraint NOT NULL, 
  constraint pk primary key (ro, customerPhoneNumber, callStartTS))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','ro',
   'ro','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','roCreatedTS',
   'roCreatedTS','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','customerPhoneNumberRight7',
   'customerPhoneNumberRight7','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','callStartTS',
   'callStartTS','',2,512,'' );    
   
--thinking i need to DO an INNER JOIN to extcall, ALL i want IN this TABLE are 
--actual calls, there IS no need to list phone numbers for ros that have NOT been called
-- AND IF i am deriving only actual calls, don't need the coalescing
INSERT INTO tmpRoCalls
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), 
  max(b.durationSeconds) AS durationSeconds, max(id)
-- SELECT COUNT(*)   
FROM tmpRoPhoneNumbers a
INNER JOIN (
  SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
    right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds
  FROM sh.extCall
  WHERE callType = 3
    AND length(TRIM(dialedNumber)) = 12) b 
      on a.customerPhoneNumber = b.dialedNumber
        AND b.startTime > a.roCreatedTS    
GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date);       

--< roPhoneCalls ---------------------------------------------------------------<

-- 1. ro info
--DROP TABLE tmpRoPhoneNumbers1;
CREATE TABLE tmpRoPhoneNumbers1 (
  ro cichar(9) constraint NOT NULL,
  customerKey integer constraint NOT NULL, 
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  constraint pk primary key (ro))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers1','tmpRoPhoneNumbers1.adi','customerKey',
   'customerKey','',2,512,'' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers1','tmpRoPhoneNumbers1.adi','roVin',
   'ro;vin','',2,512,'' );       
INSERT INTO tmpRoPhoneNumbers1
SELECT distinct a.ro, a.customerKey, e.vin, a.rocreatedTS, b.thedate AS openDate, 
  c.thedate AS closeDate, d.theDate AS fcDate
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.day c on a.closedatekey = c.datekey
INNER JOIN dds.day d on a.finalclosedatekey = d.datekey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey
WHERE a.customerKey NOT IN ( -- eliminate ros for inventory vehicles
  SELECT customerKey
  FROM dds.dimCustomer
  WHERE fullname = 'inventory'
   OR lastname IN ('CARWASH','SHOP TIME'))
AND b.theyear = 2014;

-- 2.
-- phone numbers FROM scheduler
--DROP TABLE tmpRoPhoneNumbers2;
CREATE TABLE tmpRoPhoneNumbers2 (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  schedPhone cichar(10) constraint NOT NULL,
  schedPhoneRight7 cichar(7) constraint NOT NULL,
  constraint pk primary key (ro, schedPhone))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','roVin',
   'ro;vin','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','schedPhone',
   'schedPhone','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','schedPhoneRight7',
   'schedPhoneRight7','',2,512,'' );    
INSERT INTO tmpRoPhoneNumbers2
SELECT h.ro, h.vin, h.roCreatedTS, h.openDate, h.closeDate, h.fcDate,
  TRIM(schedPhone), right(TRIM(schedPhone), 7) 
FROM (
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.homephone) AS schedPhone
FROM dds.stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.homephone) <> '' 
-- GROUP BY b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, a.homephone
UNION 
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.bestPhoneToday)
FROM dds.stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.bestPhoneToday) <> '' 
--GROUP BY a.ro, a.bestPhoneToday
UNION 
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.cellPhone)
FROM dds.stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.cellPhone) <> '') h;
--GROUP BY a.ro, a.cellPhone
   
-- 3.
-- phone numbers FROM dimCustomer
--DROP TABLE tmpRoPhoneNumbers3;
CREATE TABLE tmpRoPhoneNumbers3 (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  arkPhone cichar(10) constraint NOT NULL,
  arkPhoneRight7 cichar(7) constraint NOT NULL,
  constraint pk primary key (ro, arkPhone))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','roVin',
   'ro;vin','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','arkPhone',
   'arkPhone','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','arkPhoneRight7',
   'arkPhoneRight7','',2,512,'' );      
INSERT INTO tmpRoPhoneNumbers3
SELECT ro, vin, roCreatedTS, openDate, closeDate, fcDate, trim(arkPhone),
  right(TRIM(arkPhone),7) AS arkPhone7
FROM (
  SELECT a.*, b.homephone AS arkPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.homePhone), 'x')) = 7 or length(coalesce(trim(b.homePhone), 'x')) = 10
  UNION
  SELECT a.*, b.businessPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.businessPhone), 'x')) = 7  OR length(coalesce(trim(b.businessPhone), 'x')) = 10
  UNION
  SELECT a.*, b.cellPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.cellPhone), 'x')) = 7 OR length(coalesce(trim(b.cellPhone), 'x')) = 10) g; 

 
-- 4.
-- all phone numbers for an ro
-- each ro can have 0:6 phone numbers
--DROP TABLE tmpRoPhoneNumbers;
CREATE TABLE tmpRoPhoneNumbers (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  customerPhoneNumber cichar(10) default '0' constraint NOT NULL,
  customerPhoneNumberRight7 cichar(7) default '0' constraint NOT NULL,
  constraint pk primary key (ro, customerPhoneNumber))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','ro',
   'ro','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','customerPhoneNumber',
   'customerPhoneNumber','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','customerPhoneNumberRight7',
   'customerPhoneNumberRight7','',2,512,'' ); 
INSERT INTO tmpRoPhoneNumbers
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  coalesce(b.schedPhone, '0'), coalesce(b.schedPhoneRight7, '0')
FROM tmpRoPhoneNumbers1 a
LEFT JOIN (
  SELECT * 
  FROM tmpRoPhoneNumbers2
  UNION 
  SELECT * 
  FROM tmpRoPhoneNumbers3) b on a.ro = b.ro;
  
  
-- include extCall.id for eventual linking to extConnect AND call copy
--DROP TABLE tmpRoCalls;
CREATE TABLE tmpRoCalls (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  customerPhoneNumber cichar(10) constraint NOT NULL,
  customerPhoneNumberRight7 cichar(7) constraint NOT NULL,
  callStartTS timestamp constraint NOT NULL,
  callDate date constraint NOT NULL,
  callDuration integer default '0' constraint NOT NULL, 
  extCallId integer constraint NOT NULL, 
  constraint pk primary key (ro, customerPhoneNumber, callStartTS))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','ro',
   'ro','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','roCreatedTS',
   'roCreatedTS','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','customerPhoneNumberRight7',
   'customerPhoneNumberRight7','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','callStartTS',
   'callStartTS','',2,512,'' );    
   
--thinking i need to DO an INNER JOIN to extcall, ALL i want IN this TABLE are 
--actual calls, there IS no need to list phone numbers for ros that have NOT been called
-- AND IF i am deriving only actual calls, don't need the coalescing
INSERT INTO tmpRoCalls
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), 
  max(b.durationSeconds) AS durationSeconds, max(id)
-- SELECT COUNT(*)   
FROM tmpRoPhoneNumbers a
INNER JOIN (
  SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
    right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds
  FROM sh.extCall
  WHERE callType = 3
    AND length(TRIM(dialedNumber)) = 12) b 
      on a.customerPhoneNumber = b.dialedNumber
        AND b.startTime > a.roCreatedTS    
GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date);       

ALTER PROCEDURE getCurrentROs( 
      userName CICHAR ( 50 ),
      firstName CICHAR ( 20 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      openDate CICHAR ( 8 ) OUTPUT,
      customer CICHAR ( 50 ) OUTPUT,
      roStatus CICHAR ( 24 ) OUTPUT,
      vin CICHAR ( 17 ) OUTPUT,
      vehicle CICHAR ( 60 ) OUTPUT,
      statusUpdate Memo OUTPUT,
      theDate DATE OUTPUT,
      lastContactMeans CICHAR ( 12 ) OUTPUT,
      lastContactTS TIMESTAMP OUTPUT,
      lastContactDuration Integer OUTPUT,
      howManyCalls integer output 
   ) 
BEGIN 
/*
-- 4/29
  ADD last contact output
  
EXECUTE PROCEDURE getCurrentros('tbursinger@rydellchev.com');
EXECUTE PROCEDURE getCurrentros('dpederson@gfhonda.com');
EXECUTE PROCEDURE getCurrentros('mhuot@rydellchev.com');
EXECUTE PROCEDURE getCurrentros('aneumann@gfhonda.com');
*/
DECLARE @id string;
@id = (SELECT userName FROM __input);
INSERT INTO __output
SELECT b.firstname, a.ro, c.mmddyy AS opendate, a.customer, a.rostatus, a.vin, a.vehicle, 
  d.statusUpdateWithDateName, a.openDate,
  CASE f.howMany WHEN 0 THEN '' ELSE 'phone' END AS lastContactMeans,
  e.callStartTS,
  e.callDuration,
  f.howMany
FROM CurrentRos a
INNER JOIN ServiceWriters b ON a.userName = b.userName
  AND CASE
    WHEN @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com', 'kespelund@rydellchev.com') THEN b.storecode = 'RY1'
    WHEN @id = 'aneumann@gfhonda.com' THEN b.storecode = 'RY2'
    ELSE b.userName = @id 
  END 
LEFT JOIN dds.day c ON a.opendate = c.thedate
LEFT JOIN AgedROUpdates d on a.ro = d.ro 
  and d.updateTS = (
    SELECT MAX(updatets)
    FROM AgedRoUpdates
    WHERE ro = d.ro)
-- LEFT JOIN tmpCurrentRoLastContact e on a.ro = e.ro;	 
LEFT JOIN tmpRoCalls e on a.ro = e.ro  
  and e.callStartTS = (-- most recent call
    SELECT MAX(callStartTS)
    FROM tmpRoCalls
    WHERE ro = a.ro)
LEFT JOIN (-- number of calls
  SELECT a.ro, 
    SUM(
      CASE 
        WHEN customerPhoneNumber IS NULL THEN 0
        ELSE 1
      END) AS howMany
  FROM currentRos a
  LEFT JOIN tmpRoCalls b on a.ro = b.ro   
  GROUP BY a.ro) f on a.ro = f.ro;



END;


CREATE PROCEDURE getRoPhoneCalls (
  ro cichar(9),
  ro cichar(9) output,
  lastContactTS timestamp output,
  lastContactDuration integer output)
BEGIN
/*
EXECUTE PROCEDURE getRoPhoneCalls('16152847');
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output
SELECT top 100 @ro, callStartTS, callDuration
FROM tmpRoCalls
WHERE ro = @ro
ORDER BY callStartTS DESC;
END;  


  
--/> roPhoneCalls --------------------------------------------------------------/>