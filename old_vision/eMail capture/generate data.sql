/*
3/5
  this IS currently deleting AND repopulating with just the most recent 90 days
  that will NOT WORK for pdq writer stats, need history, so, base cstfbEmailData
  needs to go back to a fixed date
  each of the getCstfb stored procs specify previous 90 days, so we should be good there
  also the notion of tieing this data to pdq stats based on the name makes me
  gitchy, thinking of adding employeenumber to this TABLE
  probably should actually be USING tpEmployees.username across the board
  
  also past time to ADD a primary key to cstfbEmailData, 
    initially, the best candidate IS transactionNumber
    
so 
  1. make the scrape back to a fixed date, USING pdq AS the marker, go back to 11/1/13    
*/

-- service data only   
DELETE FROM cstfbEmailData;
INSERT INTO cstfbEmailData
-- service data only   
SELECT thedate, c.storecode, c.censusDept,
  trim(cc.firstname) + ' ' + TRIM(cc.lastname), ro, 'RO', 
  d.fullName, 
--  CASE d.hasValidEmail WHEN true THEN lower(d.email) ELSE 'None' END , 
/**/
  CASE hasValidEmail
    WHEN True THEN
      CASE emailValid
        WHEN true THEN email
        ELSE email2
      END
    ELSE
      CASE 
        WHEN length(TRIM(email)) <> 0 THEN email
        WHEN length(trim(email2)) <> 0 THEN email2
        ELSE 'None'
      END
  END AS emailAddress,
 /**/   
  CASE 
    WHEN d.homephone  = '0' OR d.homephone  = '' OR d.homephone IS NULL THEN 'No Phone'
    ELSE left(d.homephone, 12)   
  END AS HomePhone,
  CASE 
    WHEN d.businessphone  = '0' OR d.businessphone  = '' OR d.businessphone IS NULL THEN 'No Phone'
    ELSE left(d.businessphone, 12)   
  END AS WorkPhone,  
  CASE 
    WHEN d.cellphone  = '0' OR d.cellphone  = '' OR d.cellphone IS NULL THEN 'No Phone'
    ELSE left(d.cellphone, 12)   
    END AS CellPhone,    
  d.HasValidEmail
FROM (
  SELECT ro, opendatekey, servicewriterkey, customerkey
  FROM dds.factRepairOrder
  GROUP BY ro, opendatekey, servicewriterkey, customerkey) a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
INNER JOIN dds.edwEmployeeDim cc on c.employeenumber = cc.employeenumber
  AND cc.currentrow = true
INNER JOIN dds.dimCustomer d on a.customerKey = d.CustomerKey
WHERE c.censusdept IN ('BS','MR','QL')
--  AND b.thedate BETWEEN curdate() - 90 AND curdate()
  AND b.thedate BETWEEN '11/01/2013' AND curdate()
  AND EXISTS (
    SELECT 1
    FROM dds.factRepairORder 
    WHERE ro = a.ro
      AND paymentTypeKey IN ( -- eliminate internal only work
        SELECT paymentTypeKey
        FROM dds.dimPAymentType
        WHERE paymentTypeCode IN ('C','S','W'))); 
        
--DELETE FROM cstfbEmailData WHERE transactiontype = 'sale';     
INSERT INTO cstfbEmailData
-- sales
SELECT b.thedate, a.storeCode, 'None', c.fullname, a.stockNumber, 'Sale', 
  d.fullName, 
--  CASE d.hasValidEmail WHEN true THEN lower(d.email) ELSE 'None' END , 
/**/
  CASE hasValidEmail
    WHEN True THEN
      CASE emailValid
        WHEN true THEN email
        ELSE email2
      END
    ELSE
      CASE 
        WHEN length(TRIM(email)) <> 0 THEN email
        WHEN length(trim(email2)) <> 0 THEN email2
        ELSE 'None'
      END
  END AS emailAddress,
/**/  
  CASE 
    WHEN d.homephone  = '0' OR d.homephone  = '' OR d.homephone IS NULL THEN 'No Phone'
    ELSE left(d.homephone, 12)   
  END AS HomePhone,
  CASE 
    WHEN d.businessphone  = '0' OR d.businessphone  = '' OR d.businessphone IS NULL THEN 'No Phone'
    ELSE left(d.businessphone, 12)   
  END AS WorkPhone,  
  CASE 
    WHEN d.cellphone  = '0' OR d.cellphone  = '' OR d.cellphone IS NULL THEN 'No Phone'
    ELSE left(d.cellphone, 12)   
    END AS CellPhone,    
  d.HasValidEmail
FROM dds.factVehicleSale a
INNER JOIN dds.day b on a.cappedDateKey = b.datekey
INNER JOIN dds.dimSalesPerson c on a.consultantKey = c.salesPersonKey
INNER JOIN dds.dimCustomer d on a.buyerKey = d.customerKey
INNER JOIN dds.dimCarDealInfo e on a.carDealInfoKey = e.carDealInfoKey
--WHERE b.thedate BETWEEN curdate() - 90 AND curdate()
WHERE b.thedate BETWEEN '11/01/2013' AND curdate()
  AND e.saleTypeCode <> 'W'
  AND d.fullName NOT IN ('RYDELL AUTO CENTER', 'RYDELL NISSAN OF GRAND FORKS');           