/*

getMarketEmailScores();
getStoreEmailScores( storeCode );
getDepartmentEmailScores( storeCode, department );
getDepartmentPeopleEmailScores( storeCode, department );

The above should return:
  Transactions - integer,
  EmailAddresses - integer,
  EmailCaptureRate - double

getMarketEmailTransactions();
getStoreEmailTransactions( storeCode );
getDepartmentEmailTransactions( storeCode, department );
getEmployeeTransactions( employee );
The above should return:
  TransactionDate - date
  TransactionNumber - (deal# or ro#)
  TransactionType - RO or Sale
  CustomerName - char
  EmailAddress - char
  HomePhone - char
  WorkPhone - char
  CellPhone - char
*/

thinking a large flat TABLE that IS updated nightly
that will feed these procs

shit, will need stocknumber to identify the sales transactions

-- DO NOT yet know what i will DO about a primary key 
/*
2/11: ADD store code
      ADD subDepartment, for service breakdown, depts: Sales, Service, MainShop, PDQ, BodyShop
       
*/
DROP table cstfbEmailData;
CREATE table cstfbEmailData (
  transactionDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  subDepartment cichar(12) constraint NOT NULL,
  employeeName cichar(52) constraint NOT NULL,
  transactionNumber cichar(9) constraint NOT NULL, -- ro/stocknumber
  transactionType cichar(6) constraint NOT NULL, -- ro/sale
  customerName cichar(50)  constraint NOT NULL,
  emailAddress cichar(60) constraint NOT NULL,
  homePhone cichar(12) constraint NOT NULL,
  workPhone cichar(20) constraint NOT NULL,
  cellPhone cichar(12) constraint NOT NULL,
  hasValidEmail logical constraint NOT NULL) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailData','cstfbEmailData.adi','transactionDate','transactionDate',
   '',2,512, '' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailData','cstfbEmailData.adi','employeeName','employeeName',
   '',2,512, '' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailData','cstfbEmailData.adi','transactionNumber','transactionNumber',
   '',2,512, '' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailData','cstfbEmailData.adi','hasValidEmail','hasValidEmail',
   '',2,512, '' );          
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailData','cstfbEmailData.adi','storeCode','storeCode',
   '',2,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailData','cstfbEmailData.adi','transactionType','transactionType',
   '',2,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbEmailData','cstfbEmailData.adi','subDepartment','subDepartment',
   '',2,512, '' );     
      
--<----------------------------------------------------------------------------< Scores     
-- service data only   
DELETE FROM cstfbEmailData;
INSERT INTO cstfbEmailData
SELECT thedate, c.storecode, c.censusDept,
  trim(cc.firstname) + ' ' + TRIM(cc.lastname), ro, 'RO', 
  d.fullName, 
  CASE d.hasValidEmail WHEN true THEN lower(d.email) ELSE 'None' END , 
  CASE 
    WHEN d.homephone  = '0' OR d.homephone  = '' OR d.homephone IS NULL THEN 'No Phone'
    ELSE left(d.homephone, 12)   
  END AS HomePhone,
  CASE 
    WHEN d.businessphone  = '0' OR d.businessphone  = '' OR d.businessphone IS NULL THEN 'No Phone'
    ELSE left(d.businessphone, 12)   
  END AS WorkPhone,  
  CASE 
    WHEN d.cellphone  = '0' OR d.cellphone  = '' OR d.cellphone IS NULL THEN 'No Phone'
    ELSE left(d.cellphone, 12)   
    END AS CellPhone,    
  d.HasValidEmail
FROM (
  SELECT ro, opendatekey, servicewriterkey, customerkey
  FROM dds.factRepairOrder
  GROUP BY ro, opendatekey, servicewriterkey, customerkey) a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
INNER JOIN dds.edwEmployeeDim cc on c.employeenumber = cc.employeenumber
  AND cc.currentrow = true
INNER JOIN dds.dimCustomer d on a.customerKey = d.CustomerKey
WHERE c.censusdept IN ('BS','MR','QL')
  AND b.thedate BETWEEN curdate() - 90 AND curdate()
  AND EXISTS (
    SELECT 1
    FROM dds.factRepairORder 
    WHERE ro = a.ro
      AND paymentTypeKey IN ( -- eliminate internal only work
        SELECT paymentTypeKey
        FROM dds.dimPAymentType
        WHERE paymentTypeCode IN ('C','S','W'))); 
        
-- sales data
-- DELETE FROM cstfbEmailData WHERE transactionType = 'Sale';
-- retail only
INSERT INTO cstfbEmailData
SELECT b.thedate, a.storeCode, 'None', c.fullname, a.stockNumber, 'Sale', 
  d.fullName, 
  CASE d.hasValidEmail WHEN true THEN lower(d.email) ELSE 'None' END , 
  CASE 
    WHEN d.homephone  = '0' OR d.homephone  = '' OR d.homephone IS NULL THEN 'No Phone'
    ELSE left(d.homephone, 12)   
  END AS HomePhone,
  CASE 
    WHEN d.businessphone  = '0' OR d.businessphone  = '' OR d.businessphone IS NULL THEN 'No Phone'
    ELSE left(d.businessphone, 12)   
  END AS WorkPhone,  
  CASE 
    WHEN d.cellphone  = '0' OR d.cellphone  = '' OR d.cellphone IS NULL THEN 'No Phone'
    ELSE left(d.cellphone, 12)   
    END AS CellPhone,    
  d.HasValidEmail
FROM dds.factVehicleSale a
INNER JOIN dds.day b on a.cappedDateKey = b.datekey
INNER JOIN dds.dimSalesPerson c on a.consultantKey = c.salesPersonKey
INNER JOIN dds.dimCustomer d on a.buyerKey = d.customerKey
INNER JOIN dds.dimCarDealInfo e on a.carDealInfoKey = e.carDealInfoKey
WHERE b.thedate BETWEEN curdate() - 90 AND curdate()
  AND e.saleTypeCode <> 'W'
  AND d.fullName NOT IN ('RYDELL AUTO CENTER', 'RYDELL NISSAN OF GRAND FORKS');        

-- SELECT * FROM cstfbEmailData
ALTER PROCEDURE getCstfbMarketEmailScores(
  transactions integer output,
  emailAddresses integer output,
  emailCaptureRate double(2) output)
BEGIN 
/*
EXECUTE PROCEDURE getCstfbMarketEmailScores();
*/
INSERT INTO __output
SELECT theCount, emails, round(100.0 * emails/theCount, 1)/100
FROM (
  select COUNT(*) AS theCount, 
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS emails
  FROM cstfbEmailData
  WHERE transactionDate BETWEEN curdate()-90 AND curdate()) a;
END;

alter PROCEDURE getCstfbStoreEmailScores(
  storeCode cichar(3),
  transactions integer output,
  emailAddresses integer output,
  emailCaptureRate double(2) output)
BEGIN 
/*
EXECUTE PROCEDURE getCstfbStoreEmailScores('ry2');
*/
DECLARE @storeCode string;
@storeCode = (SELECT storeCode FROM __input);
INSERT INTO __output
SELECT theCount, emails, round(100.0 * emails/theCount, 1)/100
FROM (
  select storeCode, COUNT(*) AS theCount, 
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS emails
  FROM cstfbEmailData
  WHERE storeCode = @storeCode
    AND transactionDate BETWEEN curdate()-90 AND curdate()
  GROUP BY storeCode) a;
END;

ALTER PROCEDURE getCstfbDepartmentEmailScores(
  storeCode cichar(3),
  department cichar(12),         
  transactions integer output,
  emailAddresses integer output,
  emailCaptureRate double(2) output)
BEGIN 
/*
depts: Sales, Service, MainShop, PDQ, BodyShop
EXECUTE PROCEDURE getCstfbDepartmentEmailScores('ry2', 'pdq');
*/
DECLARE @storeCode string;
DECLARE @department string;
@storeCode = (SELECT storeCode FROM __input);
@department = (SELECT UPPER(department) FROM __input);
IF @department = 'SALES' THEN 
  INSERT INTO __output
  SELECT theCount, emails, round(100.0 * emails/theCount, 1)/100 
  FROM (
    select storeCode, coalesce(COUNT(*), 0) AS theCount, 
      coalesce(SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END), 0) AS emails
    FROM cstfbEmailData
    WHERE storeCode = @storeCode
      AND transactionType = 'Sale'
      AND transactionDate BETWEEN curdate()-90 AND curdate()
    GROUP BY storeCode) a;
END IF;    
IF @department = 'SERVICE' THEN
  INSERT INTO __output
  SELECT theCount, emails, round(100.0 * emails/theCount, 1)/100
  FROM (
    select storeCode, COUNT(*) AS theCount, 
      SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS emails
    FROM cstfbEmailData
    WHERE storeCode = @storeCode
      AND transactionType = 'RO'
      AND transactionDate BETWEEN curdate()-90 AND curdate()
    GROUP BY storeCode, transactionType) a;
END IF;  
IF UPPER(@department) IN ('MAINSHOP', 'PDQ', 'BODYSHOP') THEN 
INSERT INTO __output
SELECT theCount, emails, round(100.0 * emails/theCount, 1)/100
FROM (
  select storeCode, COUNT(*) AS theCount, 
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS emails
  FROM cstfbEmailData
  WHERE storeCode = @storeCode
    AND transactionType = 'RO'
    AND transactionDate BETWEEN curdate()-90 AND curdate()
    AND subDepartment = 
      CASE UPPER(@department)
        WHEN 'MAINSHOP' THEN 'MR'
        WHEN 'PDQ' THEN 'QL'
        WHEN 'BODYSHOP' THEN 'BS'
    END    
  GROUP BY storeCode, transactionType, subDepartment) a;
END IF;  
END;


alter PROCEDURE getCstfbDepartmentPeopleEmailScores(
  storeCode cichar(3),
  department cichar(12), 
  employee cichar(52) output,        
  transactions integer output,
  emailAddresses integer output,
  emailCaptureRate double(2) output)
BEGIN 
/*
depts: Sales, Service, MainShop, PDQ, BodyShop
this IS never called against service, just the sub depts
EXECUTE PROCEDURE getCstfbDepartmentPeopleEmailScores('ry1', 'sales');
*/
DECLARE @storeCode string;
DECLARE @department string;
@storeCode = (SELECT storeCode FROM __input);
@department = (SELECT UPPER(department) FROM __input);

IF @department = 'SALES' THEN 
  INSERT INTO __output
  SELECT employeeName, theCount, emails, round(100.0 * emails/theCount, 1)/100 
  FROM (
    select employeeName, storeCode, coalesce(COUNT(*), 0) AS theCount, 
      coalesce(SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END), 0) AS emails
    FROM cstfbEmailData
    WHERE storeCode = @storeCode
      AND transactionType = 'Sale'
      AND transactionDate BETWEEN curdate()-90 AND curdate()
    GROUP BY storeCode, transactionType, employeeName) a;
END IF;    

IF @department = 'SERVICE' THEN
  INSERT INTO __output
  SELECT employeeName, theCount, emails, round(100.0 * emails/theCount, 1)/100
  FROM (
    select employeeName, storeCode, COUNT(*) AS theCount, 
      SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS emails
    FROM cstfbEmailData
    WHERE storeCode = @storeCode
      AND transactionType = 'RO'
      AND transactionDate BETWEEN curdate()-90 AND curdate()
    GROUP BY storeCode, transactionType, employeeName) a;
END IF;  
IF UPPER(@department) IN ('MAINSHOP', 'PDQ', 'BODYSHOP') THEN 
INSERT INTO __output
SELECT employeeName, theCount, emails, round(100.0 * emails/theCount, 1)/100
FROM (
  select employeeName, storeCode, COUNT(*) AS theCount, 
    SUM(CASE WHEN hasValidEmail = true THEN 1 ELSE 0 END) AS emails
  FROM cstfbEmailData
  WHERE storeCode = @storeCode
    AND transactionType = 'RO'
    AND transactionDate BETWEEN curdate()-90 AND curdate()
    AND subDepartment = 
      CASE UPPER(@department)
        WHEN 'MAINSHOP' THEN 'MR'
        WHEN 'PDQ' THEN 'QL'
        WHEN 'BODYSHOP' THEN 'BS'
    END    
  GROUP BY storeCode, transactionType, subDepartment, employeeName) a;
END IF;  
END;
--/>---------------------------------------------------------------------------/> Scores 

--< ---------------------------------------------------------------------------< Transactions

ALTER PROCEDURE getCstfbMarketEmailTransactions(
  employee cichar(52) output, 
  transactionDate date output,
  transactionNumber cichar(9) output,
  transactionType cichar(6) output,
  customerName cichar(50) output, 
  emailAddress cichar(60) output, 
  homePhone cichar(12) output,
  workPhone cichar(20) output, 
  cellPhone cichar(12) output)
BEGIN 
/*
EXECUTE PROCEDURE getCstfbMarketEmailTransactions();
*/
INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, transactionType,
  customerName, emailAddress, homePhone, workPhone, cellPhone
FROM cstfbEmailData
WHERE transactionDate BETWEEN curdate()-90 AND curdate();
END;

CREATE PROCEDURE getCstfbStoreEmailTransactions(
  storeCode cichar(3),
  employee cichar(52) output, 
  transactionDate date output,
  transactionNumber cichar(9) output,
  transactionType cichar(6) output,
  customerName cichar(50) output, 
  emailAddress cichar(60) output, 
  homePhone cichar(12) output,
  workPhone cichar(20) output, 
  cellPhone cichar(12) output)
BEGIN 
/*
EXECUTE PROCEDURE getCstfbStoreEmailTransactions('RY2');
*/
DECLARE @storeCode string;
@storeCode = (SELECT storeCode FROM __input);
INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, transactionType,
  customerName, emailAddress, homePhone, workPhone, cellPhone
FROM cstfbEmailData
WHERE transactionDate BETWEEN curdate()-90 AND curdate()
  AND storeCode = @storeCode;
END;


CREATE PROCEDURE getCstfbDepartmentEmailTransactions(
  storeCode cichar(3),
  department cichar(12),
  employee cichar(52) output, 
  transactionDate date output,
  transactionNumber cichar(9) output,
  transactionType cichar(6) output,
  customerName cichar(50) output, 
  emailAddress cichar(60) output, 
  homePhone cichar(12) output,
  workPhone cichar(20) output, 
  cellPhone cichar(12) output)
BEGIN 
/*
select * FROM cstfbEmailData

EXECUTE PROCEDURE getCstfbDepartmentEmailTransactions('RY1', 'mainshop');
*/
DECLARE @storeCode string;
DECLARE @department string;
@storeCode = (SELECT storeCode FROM __input);
@department = (SELECT UPPER(department) FROM __input);
INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, transactionType,
  customerName, emailAddress, homePhone, workPhone, cellPhone
FROM cstfbEmailData
WHERE transactionDate BETWEEN curdate()-90 AND curdate()
  AND storeCode = @storeCode
  AND 
    CASE 
      WHEN @department = 'SALES' THEN transactionType = 'Sale'
      WHEN @department = 'SERVICE' THEN transactionType = 'RO'
      WHEN @department = 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN @department = 'PDQ' THEN subDepartment = 'QL'
      WHEN @department = 'BODYSHOP' THEN subdepartment = 'BS'
    END; 
END;

alter PROCEDURE getCstfbEmployeeEmailTransactions(
  employeeName cichar(52),
  employee cichar(52) output, 
  transactionDate date output,
  transactionNumber cichar(9) output,
  transactionType cichar(6) output,
  customerName cichar(50) output, 
  emailAddress cichar(60) output, 
  homePhone cichar(12) output,
  workPhone cichar(20) output, 
  cellPhone cichar(12) output)
BEGIN 
/*
EXECUTE PROCEDURE getCstfbEmployeeEmailTransactions('Travis Bursinger');
*/
DECLARE @employeeName string;
@employeeName = (SELECT employeeName FROM __input);
INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, transactionType,
  customerName, emailAddress, homePhone, workPhone, cellPhone
FROM cstfbEmailData
WHERE transactionDate BETWEEN curdate()-90 AND curdate()
  AND employeeName = @employeeName;
END;

--/> --------------------------------------------------------------------------/< Transactions