ALTER PROCEDURE getCstfbDepartmentEmailTransactions
   ( 
      storeCode CICHAR ( 3 ),
      department CICHAR ( 12 ),
      employee CICHAR ( 52 ) OUTPUT,
      transactionDate DATE OUTPUT,
      transactionNumber CICHAR ( 9 ) OUTPUT,
      transactionType CICHAR ( 6 ) OUTPUT,
      customerName CICHAR ( 50 ) OUTPUT,
      emailAddress CICHAR ( 60 ) OUTPUT,
      homePhone CICHAR ( 12 ) OUTPUT,
      workPhone CICHAR ( 20 ) OUTPUT,
      cellPhone CICHAR ( 12 ) OUTPUT,
      hasValidEmail LOGICAL OUTPUT
   ) 
BEGIN 
/*
break out new,used,detail

EXECUTE PROCEDURE getCstfbDepartmentEmailTransactions('RY1', 'detail');
*/
DECLARE @storeCode string;
DECLARE @department string;
@storeCode = (SELECT storeCode FROM __input);
@department = (SELECT UPPER(department) FROM __input);
INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, transactionType,
  customerName, emailAddress, homePhone, workPhone, cellPhone, hasValidEmail
FROM cstfbEmailData
WHERE transactionDate BETWEEN curdate()-90 AND curdate()
  AND storeCode = @storeCode
  AND 
    CASE 
      WHEN @department = 'SALES' THEN transactionType = 'Sale'
      WHEN @department = 'NEW' THEN subDepartment = 'NEW'
      WHEN @department = 'USED' THEN subDepartment = 'USED'
      WHEN @department = 'SERVICE' THEN transactionType = 'RO'
      WHEN @department = 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN @department = 'PDQ' THEN subDepartment = 'QL'
      WHEN @department = 'BODYSHOP' THEN subdepartment = 'BS'
      WHEN @department = 'DETAIL' THEN subDepartment = 'RE'
    END; 


END;

