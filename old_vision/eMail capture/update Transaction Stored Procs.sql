alter PROCEDURE getCstfbEmployeeEmailTransactions
   ( 
      employeeName CICHAR ( 52 ),
      employee CICHAR ( 52 ) OUTPUT,
      transactionDate DATE OUTPUT,
      transactionNumber CICHAR ( 9 ) OUTPUT,
      transactionType CICHAR ( 6 ) OUTPUT,
      customerName CICHAR ( 50 ) OUTPUT,
      emailAddress CICHAR ( 60 ) OUTPUT,
      homePhone CICHAR ( 12 ) OUTPUT,
      workPhone CICHAR ( 20 ) OUTPUT,
      cellPhone CICHAR ( 12 ) OUTPUT,
	  hasValidEmail logical output
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getCstfbEmployeeEmailTransactions('Cory Stinar');
*/
DECLARE @employeeName string;
@employeeName = (SELECT employeeName FROM __input);
INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, transactionType,
  customerName, emailAddress, homePhone, workPhone, cellPhone, hasValidEmail
FROM cstfbEmailData
WHERE transactionDate BETWEEN curdate()-90 AND curdate()
  AND employeeName = @employeeName;

END;

alter PROCEDURE getCstfbDepartmentEmailTransactions
   ( 
      storeCode CICHAR ( 3 ),
      department CICHAR ( 12 ),
      employee CICHAR ( 52 ) OUTPUT,
      transactionDate DATE OUTPUT,
      transactionNumber CICHAR ( 9 ) OUTPUT,
      transactionType CICHAR ( 6 ) OUTPUT,
      customerName CICHAR ( 50 ) OUTPUT,
      emailAddress CICHAR ( 60 ) OUTPUT,
      homePhone CICHAR ( 12 ) OUTPUT,
      workPhone CICHAR ( 20 ) OUTPUT,
      cellPhone CICHAR ( 12 ) OUTPUT,
	  hasValidEmail logical output
   ) 
BEGIN 
/*
select * FROM cstfbEmailData

EXECUTE PROCEDURE getCstfbDepartmentEmailTransactions('RY1', 'sales');
*/
DECLARE @storeCode string;
DECLARE @department string;
@storeCode = (SELECT storeCode FROM __input);
@department = (SELECT UPPER(department) FROM __input);
INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, transactionType,
  customerName, emailAddress, homePhone, workPhone, cellPhone, hasValidEmail
FROM cstfbEmailData
WHERE transactionDate BETWEEN curdate()-90 AND curdate()
  AND storeCode = @storeCode
  AND 
    CASE 
      WHEN @department = 'SALES' THEN transactionType = 'Sale'
      WHEN @department = 'SERVICE' THEN transactionType = 'RO'
      WHEN @department = 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN @department = 'PDQ' THEN subDepartment = 'QL'
      WHEN @department = 'BODYSHOP' THEN subdepartment = 'BS'
    END; 

END;


alter PROCEDURE getCstfbMarketEmailTransactions
   ( 
      employee CICHAR ( 52 ) OUTPUT,
      transactionDate DATE OUTPUT,
      transactionNumber CICHAR ( 9 ) OUTPUT,
      transactionType CICHAR ( 6 ) OUTPUT,
      customerName CICHAR ( 50 ) OUTPUT,
      emailAddress CICHAR ( 60 ) OUTPUT,
      homePhone CICHAR ( 12 ) OUTPUT,
      workPhone CICHAR ( 20 ) OUTPUT,
      cellPhone CICHAR ( 12 ) OUTPUT,
	  hasValidEmail logical output
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getCstfbMarketEmailTransactions();
*/
INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, transactionType,
  customerName, emailAddress, homePhone, workPhone, cellPhone, hasValidEmail
FROM cstfbEmailData
WHERE transactionDate BETWEEN curdate()-90 AND curdate();

END;


ALTER PROCEDURE getCstfbStoreEmailTransactions
   ( 
      storeCode CICHAR ( 3 ),
      employee CICHAR ( 52 ) OUTPUT,
      transactionDate DATE OUTPUT,
      transactionNumber CICHAR ( 9 ) OUTPUT,
      transactionType CICHAR ( 6 ) OUTPUT,
      customerName CICHAR ( 50 ) OUTPUT,
      emailAddress CICHAR ( 60 ) OUTPUT,
      homePhone CICHAR ( 12 ) OUTPUT,
      workPhone CICHAR ( 20 ) OUTPUT,
      cellPhone CICHAR ( 12 ) OUTPUT,
	  hasValidEmail logical output
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getCstfbStoreEmailTransactions('RY1');
*/
DECLARE @storeCode string;
@storeCode = (SELECT storeCode FROM __input);
INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, transactionType,
  customerName, emailAddress, homePhone, workPhone, cellPhone, hasValidEmail
FROM cstfbEmailData
WHERE transactionDate BETWEEN curdate()-90 AND curdate()
  AND storeCode = @storeCode;

END;



