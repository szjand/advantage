/*
December 2015 ACL DETAIL
The compensation component
(Use Ron Adler 11687 AS example_
Aubrey: 4612.90 based on flag hours AND rate, includes pto
Jeri: 6117.20 based acct 124724 does NOT include pto(?)
Jon: 5115.50 based on detail accrual query
turns out it was an accrual problem
overaccrued
*/

-- #wtf FROM scratchpad_2.sql - used to generated acl_v1
SELECT thedate,  gtacct, gtctl#, gtjrnl, SUM(gttamt)
-- SELECT SUM(gttamt)
from #wtf
WHERE gtacct = '124724'
  AND gtctl# = '11687'
GROUP BY thedate, gtacct, gtctl#, gtjrnl
order BY gtctl#

select a.gtdate, a.gtacct, a.gtjrnl, a.gtdesc , b.gmdesc, a.gttamt
-- SELECT SUM(gttamt)
from dds.stgarkonaglptrns a
left join dds.stgarkonaglpmast b on trim(a.gtacct) = trim(b.gmacct)
  and b.gmyear = 2015
where gtctl# = '11687'
  and gtdate between '11/01/2015' and '12/31/2015'
  AND a.gtacct = '124724'
ORDER BY gtdate
/*
DECLARE @fromdate date;
DECLARE @thrudate date;
@fromdate = '12/01/2015';
@thrudate = '12/31/2015';
SELECT TRIM(firstname) + ' ' + lastname AS Technician, clockhours AS "Hours Worked", 
  flaghours AS "Hours Flagged", prof AS Proficiency, 12.00 AS "Flag Rate Hourly Rate", 
  12 * flaghours AS "Base Pay", 
  rate - 12 AS "Flat Rate Prof Hourly Increase", 
  (rate - 12) * flaghours AS "Flat Rate Bonus", ptohours AS "PTO Hours", 
  12 as "Hourly Rate", 12 * ptohours AS Total, 
  (12 * flaghours) + ((rate - 12) * flaghours) + (12 * ptohours) AS "Total Pay"
FROM (
  SELECT m.*, n.flaghours, round(flaghours/clockhours, 2) AS prof,
  CASE
    WHEN lastname IN ('Telken','Kingbird','Roehrich','Gonzalez') THEN 12.0
    WHEN lastname = 'Nelson' AND firstname = 'Hunter' THEN 12.0
    WHEN round(flaghours/clockhours, 2) <= 1.3 THEN 12.0   
    WHEN round(flaghours/clockhours, 2) >= 1.3 AND round(flaghours/clockhours, 2) < 1.4 THEN 12.5
    WHEN round(flaghours/clockhours, 2) >= 1.4 AND round(flaghours/clockhours, 2) < 1.5 THEN 13.5
    WHEN round(flaghours/clockhours, 2) >= 1.5 AND round(flaghours/clockhours, 2) < 1.6 THEN 15
    WHEN round(flaghours/clockhours, 2) >= 1.6 AND round(flaghours/clockhours, 2) < 1.7 THEN 16
    WHEN round(flaghours/clockhours, 2) >= 1.7 AND round(flaghours/clockhours, 2) < 1.8 THEN 17
    WHEN round(flaghours/clockhours, 2) >= 1.8 AND round(flaghours/clockhours, 2) < 1.9 THEN 18
    WHEN round(flaghours/clockhours, 2) >= 1.9 AND round(flaghours/clockhours, 2) < 2 THEN 19
    WHEN round(flaghours/clockhours, 2) > 2 THEN 20.0  
  END AS rate
  FROM (
    SELECT a.firstname, a.lastname, a.employeenumber, a.employeekey, 
      SUM(b.clockhours) AS clockhours, SUM(holidayhours+ptohours+vacationhours) AS ptohours,
      c.technumber, c.techkey
    FROM dds.edwEmployeeDim a 
    LEFT JOIN dds.edwClockHoursFact b on a.employeekey = b.employeekey
      AND b.datekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN @fromdate AND @thrudate)
    LEFT JOIN dds.dimtech c on a.employeenumber = c.employeenumber
      AND c.currentrow = true    
    WHERE a.distcode = 'wtec'
    --    AND a.currentrow = true
      AND a.active = 'active'
      AND a.employeekeyfromdate <= @thrudate
      AND a.employeekeythrudate > @fromdate
      AND a.payrollclass = 'commission'
    GROUP BY a.firstname, a.lastname, a.employeenumber, a.employeekey, c.technumber, c.techkey) m   
  LEFT JOIN (
    SELECT d.technumber, d.description, d.employeenumber, SUM(flaghours) AS flaghours
    FROM dds.factrepairorder a
    INNER JOIN dds.day b on a.closedatekey = b.datekey
    INNER JOIN dds.dimtech d on a.techkey = d.techkey
    WHERE b.thedate between @fromdate AND @thrudate
      AND d.flagdeptcode = 're'
      AND d.employeenumber <> 'NA'
    GROUP BY d.technumber, d.description, d.employeenumber) n on m.employeenumber = n.employeenumber) x  
ORDER BY lastname, firstname  
*/