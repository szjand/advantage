SELECT *
FROM (
  select a.yhdemp, trim(b.firstname) + ' ' + b.lastname,a.yhdtgp AS totalGross,
  --  c.*,
    cast (
      case
        when yhdemm between 1 and 9 then '0' + trim(cast(yhdemm AS sql_char))
        else trim(cast(yhdemm AS sql_char))
      end 
      + '/' +
      case
        when yhdedd between 1 and 9 then '0' + trim(cast(yhdedd AS sql_char))
        else trim(cast(yhdedd AS sql_char))
      end 
      + '/' +
      case
        when yhdeyy between 1 and 9 then '200' + trim(cast(yhdeyy AS sql_char))
        else '20' + trim(cast(yhdeyy AS sql_char))
      END AS sql_date) as PayrollEndDate,
    cast (
      case
        when yhdcmm between 1 and 9 then '0' + trim(cast(yhdcmm AS sql_char))
        else trim(cast(yhdcmm AS sql_char))
      end 
      + '/' +
      case
        when yhdcdd between 1 and 9 then '0' + trim(cast(yhdcdd AS sql_char))
        else trim(cast(yhdcdd AS sql_char))
      end 
      + '/' +
      case
        when yhdcyy between 1 and 9 then '200' + trim(cast(yhdcyy AS sql_char))
        else '20' + trim(cast(yhdcyy AS sql_char))
      END AS sql_date) as CheckDate
  FROM dds.stgArkonaPYHSHDTA a
  INNER JOIN dds.edwEmployeeDim b on a.yhdemp = b.employeenumber
    AND b.currentrow = true
  INNER JOIN dds.dimTech c on b.employeenumber = c.employeenumber
    AND c.currentrow = true
    AND c.flagDeptCode = 'MR') e
LEFT JOIN (
  select biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM dds.day c 
  group by biweeklypayperiodstartdate, biweeklypayperiodenddate) f on e.payrollEndDate = f.BiWeeklyPayPeriodEndDate    
WHERE e.payrollEndDate > curdate() - 45    
  




LEFT JOIN (
  select biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM dds.day c 
  group by biweeklypayperiodstartdate, biweeklypayperiodenddate) f on e.payrollEndDate = f.BiWeeklyPayPeriodEndDate