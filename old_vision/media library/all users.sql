anyone with any access to vision gets access to media library

SELECT *
FROM employeeAppAuthorization


INSERT INTO employeeappauthorization
SELECT *
FROM (
  SELECT DISTINCT username
  FROM employeeappauthorization a
  WHERE NOT EXISTS (
    SELECT 1
    FROM employeeappauthorization
    WHERE username = a.username
      AND appcode = 'lib')) b
LEFT JOIN  (     
  SELECT DISTINCT appname, appseq, appcode, approle, functionality, CAST(NULL AS sql_integer)
  FROM applicationMetaData
  WHERE appname = 'library') c on  1 = 1
  