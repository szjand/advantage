7/16/14
1.
need to include adjustments
2. 
  based on conversation with cahalan regarding the main shop production page
    Production at a glance (weekly/monthly)
	  exclude shop time AS part of the flag hours
	Detail
	  break out shop time AS a separate COLUMN
	  prof will CONTINUE to be based on total flag hours (including shop time)
	  expose whether a tech IS currently (the day on which the information IS
	    being views) employed AS a tech IN the shop that IS being displayed
3. 
also need to generalize to cover ALL shops, NOT just main shop

flag hours will always be based on techKey IN factRepairOrder regardless of anything ELSE

but census will depend on dimTech (active/flagDept) AND dimEmpl (termdate) AND eventually
  possible dimEmp.distCode
  
so it ispossible to have flag hours for a period, attributed to a tech that IS NOT
  part of the census for that dept/team during that period - NOT AS often USING
  flag date rather than CLOSE date, but due to the nature of editing rows, back
  dating adjustments, etc still possible. but IN that case the "no longer employed"
  flag would show for that tech, 
  which team would show those flag hours?
    eg, 
	7/2/14 tech 123, a member of team1 IS termed
	7/3 tech 123 has flag hours
	
	
SELECT * FROM tmpben WHERE thedate = '07/15/2014' AND flagdeptcode = 'mr' AND storecode = 'ry1' 
  and termdate = (
    select max(termdate)
	from tmpben
	WHERE termdate < curdatE())

SELECT technumber, MAX(thedate) FROM tmpben WHERE technumber IN ('623','629') AND flaghours <> 0 GROUP BY technumber

perfect, 629 has flaghours on 6/26, was termed on 6/20

SELECT * FROM tmpBen WHERE technumber = '629' AND thedate BETWEEN '06/18/2014' AND curdate()

so, part of the problem here IS IN how we chose to NOT UPDATE tpteams for yannish, swift & hamman until this pay period
they show up AS part of the team, although IN fact, they were NOT actually part of the census

hmmm JOIN tmpBen to the most recent row for that person IN tmpBen
FROM the most recent row, determine FROM flagdept/termdate whether currently employed

-- way too slow  32 sec
SELECT a.thedate, a.employeenumber, a.technumber, a.flaghours, a.clockhours, 
  b.employeenumber, b.technumber, b.flagdeptcode, b.termdate 
FROM tmpBen a
LEFT JOIN tmpBen b on a.employeenumber = b.employeenumber
  AND b.thedate = (
    SELECT MAX(thedate)
	FROM tmpBen
	WHERE employeenumber = a.employeenumber)
WHERE a.technumber = '629' 
  AND a.thedate BETWEEN '06/18/2014' AND curdate()

-- way too slow  32 sec  
-- TRY it AS subselect
-- still way too slow

SELECT a.thedate, a.employeenumber, a.technumber, a.flaghours, a.clockhours, 
  (SELECT flagdeptcode FROM tmpBen WHERE employeenumber = a.employeenumber AND thedate = (SELECT MAX(thedate) FROM tmpben WHERE employeenumber = a.employeenumber)),
  (SELECT termdate FROM tmpBen WHERE employeenumber = a.employeenumber AND thedate = (SELECT MAX(thedate) FROM tmpben WHERE employeenumber = a.employeenumber))
FROM tmpBen a
WHERE a.technumber = '629' 
  AND a.thedate BETWEEN '06/18/2014' AND curdate()
  
maybe ADD additional columns to tmpDeptTechCensus: team, termdate
since it IS already generated nightly
AND will start to generate some continuity AND consistency BETWEEN production pages AND proficiency pages  
-- 2 sec
SELECT *
FROM tmpDeptTechCensus a
LEFT JOIN tmpDeptTechCensus b on a.storecode = b.storecode
  AND a.technumber = b.technumber
  AND b.thedate = (
    SELECT MAX(thedate)
	FROM tmpDeptTechCensus
	WHERE technumber = a.technumber)
WHERE a.technumber = '629'
-- still too slow to generate on the fly, ADD columns currentDept, currentTermDate to etl ?
SELECT a.*, 
  (SELECT flagdeptcode FROM tmpDeptTechCensus WHERE employeenumber = a.employeenumber AND thedate = (SELECT MAX(thedate) FROM tmpDeptTechCensus WHERE employeenumber = a.employeenumber))
FROM tmpDeptTechCensus a
WHERE a.technumber = '629'  
/*************************************************************************************************/
-- rework tmpDeptTechCensus (sp.xfmDeptTechCensus) to be used IN place of #census
-- orig sp
--DELETE 
--FROM tmpDeptTechCensus
--WHERE theDate BETWEEN curdate() - 31 AND curdate();
--INSERT INTO tmpDeptTechCensus
select a.thedate, d.storecode, d.flagDeptCode, d.technumber, d.techKey,
  c.employeeNumber, c.employeeKey, c.lastName,
  c.firstName, TRIM(c.firstName) + ' ' + TRIM(c.lastName), c.fullPartTime
FROM dds.day a -- range of days
--INNER JOIN dds.edwClockHoursFact b on a.dateKey = b.dateKey
INNER JOIN dds.edwEmployeeDim c on a.thedate BETWEEN c.employeeKeyFromDate AND c.employeeKeyThruDate -- correct techkey
  AND a.thedate BETWEEN c.hiredate AND c.termdate
LEFT JOIN dds.dimTech d on c.employeeNumber = d.employeeNumber
  AND d.Active = true -- tech IS active 
  AND d.employeeNumber <> 'NA'
  AND d.flagDeptCode <> 'NA' 
  AND a.theDate BETWEEN d.techKeyFromDate AND d.techKeyThruDate 
WHERE a.thedate BETWEEN curdate()
-- changes reqd:
1. 
  instead of day -o< dimEmp -o< dimTech
  needs to be day -o< dimTech -o< dimEmp to include dept techs
2. use the #census WHERE clause  

-- replacement for tmpDeptTechCensus AND #census
-- think this looks pretty good
-- on each day, this technumber IS assigned to this employee who flags hours
--   for this store AND dept AND IS assigned to this team

-- 1 row per date/store/technumber
-- SELECT thedate, storecode, technumber FROM (


DROP TABLE tmpDeptTechCensus;
CREATE table tmpDeptTechCensus (
  theDate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  employeeKey integer,
  employeeNumber cichar(7) constraint NOT NULL,
  techKey integer constraint NOT NULL,
  techNumber cichar(3) constraint NOT NULL,
  description cichar(25) constraint NOT NULL, 
  firstName cichar(25),
  lastName cichar(25),
  fullName cichar(51),
  flagDeptCode cichar(2) constraint NOT NULL,
  team cichar(25) constraint NOT NULL,
  laborCost double constraint NOT NULL,
  fullPartTime cichar(8), 
  constraint pk primary key (theDate,storecode,techNumber)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpDeptTechCensus','tmpDeptTechCensus.adi','theDate',
   'theDate','',2,512,'' );   
INSERT INTO tmpDeptTechCensus
SELECT thedate, b.storeCode, c.employeeKey, b.employeeNumber, 
  b.techKey, b.techNumber, coalesce(b.name, b.description) AS description,
  c.firstname, c.lastname, TRIM(c.firstName) + ' ' + c.lastName AS fullName,
  b.flagDeptCode, 
  CASE 
    WHEN b.employeeNumber = 'NA' THEN 'Department'
    ELSE
      CASE
        WHEN d.teamName IS NULL THEN 'Hourly'
        ELSE d.teamName
      END
  END AS Team,
  b.laborCost, fullPartTime 
FROM dds.day a
LEFT JOIN dds.dimtech b on a.thedate BETWEEN b.techKeyFromDate AND b.techKeythruDate
  AND b.active = true 
  AND b.flagdeptcode not in ('NA','UN') -- excludes nokelby, dummy tech number for etl
  AND b.techNumber <> '999' -- excludes arkona
LEFT JOIN dds.edwEmployeeDim c on b.employeeNumber = c.employeenumber
  AND b.storecode = c.storecode
  AND a.thedate BETWEEN employeeKeyFromDate AND employeeKeyThruDate-- correct techkey for clock hours  
LEFT JOIN (
  SELECT w.teamkey, w.teamname, w.departmentkey, x.fromDate, x.thruDate, y.employeenumber
  FROM tpTeams w
  LEFT JOIN tpTeamTechs x on w.teamkey = x.teamkey
  LEFT JOIN tpTechs y on x.techKey = y.techKey) d 
    on b.employeenumber = d.employeenumber 
      AND a.thedate BETWEEN d.fromdate AND d.thrudate    
WHERE a.thedate BETWEEN '01/01/2013' AND curdate();
 
/*************************************************************************************************/	  
 
--< active techs vs termdate ---------------------------------------------------<

select * 
FROM dds.dimTech  a
ORDER BY employeenumber, description
WHERE a.active = 'true

!!!!!!!!!!!!!!!!!
based on an employeenumber
190600, tyler magnuson has 2 current dimTechRows nope

decoding dimTech
variables:
  active (logical)
  currentRow (logical)
  techKeyFrom/Thru (date)
  
on any given date, an employeenumber can have only 1 row IN dim tech
WHERE thedate IS BETWEEN techkey from/thrudates AND Active = true
OR, IN english
on any given date, an employee can only have 1 active technumber

SELECT employeenumber FROM (
SELECT employeenumber, technumber
FROM dds.dimTech
GROUP BY employeenumber, technumber
) x GROUP BY employeenumber HAVING COUNT(*) > 1

select * 
FROM dds.dimTech
WHERE employeenumber IN (
  SELECT employeenumber FROM (
  SELECT employeenumber, technumber
  FROM dds.dimTech
  GROUP BY employeenumber, technumber
  ) x where employeenumber <> 'NA' GROUP BY employeenumber HAVING COUNT(*) > 1)
--AND active = true  
ORDER BY employeenumber, techKeyFromDate

-- these are currently active technumbers for employees that have been termed,
-- ideally, these technumbers need to be deactivated IN arkona
select *
FROM dds.dimTech a 
LEFT JOIN (
  SELECT employeenumber, termDate
  FROM dds.edwEmployeeDim 
  WHERE currentrow = true) b on a.employeenumber = b.employeenumber
WHERE a.active = true AND a.currentrow = true
  AND b.employeenumber IS NOT NULL
  AND b.termdate < curdate()
  
  
SELECT *
FROM dds.dimTech a
WHERE NOT EXISTS (
  SELECT 1 
  FROM dds.dimTech
  WHERE storecode = a.storecode
    AND technumber = a.technumber
	AND active = true)  
--/> active techs vs termdate --------------------------------------------------/>

-- adjustments
    UPDATE tpData
    SET TechFlagHourAdjustmentsDay = x.Adj
    FROM (
      SELECT a.technumber, a.lastname, a.thedate, coalesce(Adj, 0) AS Adj
      FROM tpData a
      LEFT JOIN (  
        select pttech, ptdate, round(SUM(ptlhrs), 2) AS adj
        FROM dds.stgArkonaSDPXTIM a
        INNER JOIN tpTechs b on a.pttech = b.technumber
        GROUP BY pttech, ptdate) b on a.technumber = b.pttech AND a.thedate = b.ptdate) x    
    WHERE tpData.technumber = x.technumber
      AND tpData.thedate = x.thedate;
	  
	  
-- need to ADD adjustments, but it has to be on the day, NOT on the flag date,
-- this does NOT give me a row for every day IN a range, just every day with flag hours
-- a separate day/tech based sub table?
-- that IS probably the best solution
SELECT b.thedate, c.techkey, round(sum(a.flaghours), 2) AS totalFlaghours,
  SUM(CASE WHEN d.opcodekey IN (22106, 22107, 22108) THEN flaghours else 0 END) AS shopTime,
  round(SUM(CASE WHEN d.opcodekey NOT IN (22106, 22107, 22108) THEN flaghours else 0 END), 2) AS flagTime
-- INTO #flag
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagDateKey = b.datekey
INNER JOIN dds.dimTech c on a.techKey = c.techkey 
LEFT JOIN dds.dimOpcode d on a.opcodekey = d.opcodekey
LEFT JOIN (
  select pttech, ptdate, round(SUM(ptlhrs), 2) AS adj
  FROM dds.stgArkonaSDPXTIM a
  INNER JOIN tpTechs b on a.pttech = b.technumber
  GROUP BY pttech, ptdate) d on c.technumber = d.technumber AND 
WHERE theDate BETWEEN '01/01/2014' AND curdate()
GROUP BY b.thedate, c.techkey;


select * FROM tpdata
WHERE departmentkey = 18
AND thedate BETWEEN 

SELECT * FROM #census

-- flag hours: break out shop time, include adjustments

-- make it date based
-- DROP TABLE #flag;
SELECT thedate, techkey, 
  coalesce(flaghours, 0) + coalesce(ptlhrs, 0) AS flagHours, shopTime
FROM (
  SELECT b.storecode, a.thedate, c.techkey, c.technumber, 
    round(SUM(CASE WHEN b.opcodekey NOT IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS flagHours,
    round(SUM(CASE WHEN b.opcodekey IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS shopTime 
  -- INTO #flag  
  FROM dds.day a
  LEFT JOIN dds.factRepairOrder b on a.datekey = b.flagdatekey
  LEFT JOIN dds.dimTech c on b.techKey = c.techkey
  WHERE thedate BETWEEN '01/01/2013' AND curdate()
    AND c.techkey IS NOT NULL 
  GROUP BY  a.thedate, b.storecode, c.technumber, c.techkey, c.technumber) m
LEFT JOIN (
  SELECT ptco#, ptdate, pttech, SUM(ptlhrs) AS ptlhrs
  FROM dds.stgArkonaSDPXTIM
  GROUP BY ptco#, ptdate, pttech) n on m.thedate = n.ptdate
    AND m.storecode = n.ptco#
    AND m.technumber = n.pttech

/*** currently employed *******************************************************/

SELECT storecode, employeenumber FROM (
SELECT storecode, employeenumber, technumber, flagdeptcode 
FROM tmpBen
WHERE employeenumber <> 'na'
  AND employeekey IS NOT NULL 
GROUP BY storecode, employeenumber, technumber, flagdeptcode 
) x GROUP BY storecode, employeenumber HAVING COUNT(*) > 1
  
SELECT employeenumber FROM (
SELECT a.*, b.termdate, c.flagDeptCode
FROM (
  SELECT employeekey, employeenumber, techkey, technumber, flagdeptcode 
  FROM tmpBen
  WHERE employeenumber <> 'na'
    AND employeekey IS NOT NULL 
  GROUP BY employeekey, employeenumber, techkey, technumber, flagdeptcode) a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true   
LEFT JOIN dds.dimTech c on a.techNumber = c.techNumber
  AND c.currentrow = true  
WHERE (
  termDate < curdate()  
  OR a.flagDeptCode <> c.flagDeptCode)
) x GROUP BY employeenumber HAVING COUNT(*) > 1

-- SELECT employeenumber FROM (
SELECT a.*, b.termdate
FROM (
  SELECT storecode, employeenumber
  FROM tmpBen
  WHERE employeenumber <> 'na'
  GROUP BY storecode, employeenumber) a
LEFT JOIN dds.edwEmployeeDim b on a.storecode = b.storecode 
  AND a.employeenumber = b.employeenumber  
  AND b.currentrow = true
-- ) x GROUP BY employeenumber HAVING COUNT(*) > 1   


SELECT storecode, technumber
FROM tmpBen
WHERE employeenumber <> 'na'
GROUP BY storecode, technumber

-- 7/19
DECLARE @date date;
DECLARE @sundayToSaturdayWeek integer;
DECLARE @endDate date;

@date = '04/23/2014';

@sundayToSaturdayWeek = (
  SELECT DISTINCT sundayToSaturdayWeek
  from tmpBen
  WHERE theDate = @date);
--  where theDate = (
--    select theDate
--    from __input));
@endDate = (
  SELECT MAX(thedate)
  FROM dds.day
  WHERE sundayToSaturdayWeek = @sundayToSaturdayWeek);    
-- INSERT INTO __output
SELECT top 500 x.*, y.SundayToSaturdayWeekSelectFormat
FROM (

  SELECT 'Team' as typeInfo, team, 
    left(cast(null as sql_char), 51) as tech, SUM(flagHours) AS flagHours,
    SUM(shopTime) AS shopTime, 
    SUM(clockHours) AS clockHours, 
    CASE SUM(clockHours)
      WHEN 0 THEN 0
      ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
    END AS prof, '' AS currentlyEmployed
  FROM tmpBen
  WHERE storecode = 'ry1'
    AND flagdeptcode = 'mr' 
    AND sundaytosaturdayweek = @sundayToSaturdayWeek
  GROUP BY weekStartDate, weekEndDate, team
  -- tech details
  UNION  
  SELECT 'Tech' AS typeInfo, team, tech, flagHours, shopTime, clockHours, prof,
    coalesce(currentlyEmployed, 'Yes')
  FROM (  
    SELECT employeenumber, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName) AS tech, 
      SUM(flagHours) AS flagHours,
      SUM(shopTime) AS shopTime, 
      SUM(clockHours) AS clockHours, 
      CASE SUM(clockHours)
        WHEN 0 THEN 0
        ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
      END AS prof
    FROM tmpBen
    WHERE storecode = 'ry1'
      AND flagdeptcode = 'mr' 
      AND sundaytosaturdayweek = @sundayToSaturdayWeek
    GROUP BY employeenumber, weekStartDate, weekEndDate, team, 
      coalesce(TRIM(firstname) + ' ' + lastname, descName)) m 
  LEFT JOIN (
    SELECT a.storecode, a.employeenumber, b.termdate, c.flagDeptCode,
      CASE 
        WHEN b.termdate < curdate() THEN 'No'
        ELSE
          CASE 
            WHEN a.flagDeptCode <> c.flagDeptCode THEN 'No' 
            ELSE 'Yes'
          END
      END AS currentlyEmployed      
    FROM tmpBen a
    LEFT JOIN dds.edwEmployeeDim b on a.storecode = b.storecode
      and a.employeeNumber = b.employeenumber
      AND b.currentrow = true
    LEFT JOIN dds.dimTech c on a.storecode = c.storecode
      AND a.employeenumber = c.employeenumber
      AND c.currentrow = true  
    WHERE a.thedate = @endDate
      AND a.storecode = 'ry1'
      AND a.employeenumber <> 'na'
      AND a.flagDeptCode = 'mr') n on m.employeenumber = n.employeenumber ) x
  LEFT JOIN (
    SELECT DISTINCT SundayToSaturdayWeekSelectFormat
    from tmpBen
    WHERE sundayToSaturdayWeek = @sundayToSaturdayWeek) y on 1 = 1   
  ORDER BY team, tech;

-- take the last day of the period, get the info FROM tmpBen/dimEmp/dimTech
--  compare AND derive employed

SELECT a.storecode, a.employeenumber, b.termdate, c.flagDeptCode,
  CASE 
    WHEN b.termdate < curdate() THEN 'No'
    ELSE
      CASE 
        WHEN a.flagDeptCode <> c.flagDeptCode THEN 'No' 
        ELSE 'Yes'
      END
  END AS currentlyEmployed      
FROM tmpBen a
LEFT JOIN dds.edwEmployeeDim b on a.storecode = b.storecode
  and a.employeeNumber = b.employeenumber
  AND b.currentrow = true
LEFT JOIN dds.dimTech c on a.storecode = c.storecode
  AND a.employeenumber = c.employeenumber
  AND c.currentrow = true  
WHERE a.thedate = '05/24/2014'
  AND a.storecode = 'ry1'
  AND a.employeenumber <> 'na'
  AND a.flagDeptCode = 'mr'

SELECT 'Tech' AS typeInfo, team, tech, flagHours, shopTime, clockHours, prof,
  currentlyEmployed
FROM (  
  SELECT employeenumber, team, 
    coalesce(TRIM(firstname) + ' ' + lastname, descName) AS tech, 
    SUM(flagHours) AS flagHours,
    SUM(shopTime) AS shopTime, 
    SUM(clockHours) AS clockHours, 
    CASE SUM(clockHours)
      WHEN 0 THEN 0
      ELSE round(SUM(flagHours + shopTime)/SUM(clockHours), 2) 
    END AS prof
  FROM tmpBen
  WHERE storecode = 'ry1'
    AND flagdeptcode = 'mr' 
    AND sundaytosaturdayweek = (SELECT sundaytosaturdayweek FROM dds.day WHERE thedate = '05/24/2014')
  GROUP BY employeenumber, weekStartDate, weekEndDate, team, 
    coalesce(TRIM(firstname) + ' ' + lastname, descName)) x  
LEFT JOIN (
  SELECT a.storecode, a.employeenumber, b.termdate, c.flagDeptCode,
    CASE 
      WHEN b.termdate < curdate() THEN 'No'
      ELSE
        CASE 
          WHEN a.flagDeptCode <> c.flagDeptCode THEN 'No' 
          ELSE 'Yes'
        END
    END AS currentlyEmployed      
  FROM tmpBen a
  LEFT JOIN dds.edwEmployeeDim b on a.storecode = b.storecode
    and a.employeeNumber = b.employeenumber
    AND b.currentrow = true
  LEFT JOIN dds.dimTech c on a.storecode = c.storecode
    AND a.employeenumber = c.employeenumber
    AND c.currentrow = true  
  WHERE a.thedate = '05/24/2014'
    AND a.storecode = 'ry1'
    AND a.employeenumber <> 'na'
    AND a.flagDeptCode = 'mr') y on x.employeenumber = y.employeenumber    