-- body shop techs with totals (FROM sp.xfmTmpBen)
-- *a* the issue with clock hours IS that arkona includes hol-vac-pto hours
-- vision does not
SELECT description, employeenumber, technumber, SUM(flaghours) AS flaghours, 
  SUM(shoptime) AS shoptime, SUM(flaghours) + SUM(shoptime) AS totalflag,
  SUM(clockhours) AS clock, SUM(totalclockhours) AS totalclock
FROM (
    SELECT a.thedate, a.dayofweek, a.monthofyear, a.monthname, a.holiday, 
      a.sundaytosaturdayweek, a.yearmonth, a.weekstartdate, a.weekenddate,
      b.storecode, b.techkey, b.employeekey, b.employeenumber, 
      b.description, 
      b.techNumber, b.flagDeptCode, b.firstname, b.lastname, 
      b.team, coalesce(c.flaghours, 0) AS flagHours, 
      coalesce(c.shopTime, 0) AS shopTime,
      coalesce(d.clockhours, 0) AS clockhours, 
      SundayToSaturdayWeekSelectFormat, coalesce(d.totalclockhours, 0) AS totalclockhours
    -- INTO #ben  
    FROM (
      SELECT thedate, dayOfWeek, monthOfYear, monthName, weekDay, weekend, holiday,
        sundayToSaturdayWeek, yearmonth,
        (SELECT MIN(thedate) FROM dds.day WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek) AS weekStartDate,
        (SELECT MAX(thedate) FROM dds.day WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek) AS weekEndDate,
        (SELECT MIN(thedate) FROM dds.day WHERE yearmonth = a.yearmonth) AS monthStart,
        (SELECT MAX(thedate) FROM dds.day WHERE yearmonth = a.yearmonth) AS monthEnd,
        SundayToSaturdayWeekSelectFormat
    --  INTO #day  
      FROM dds.day a
      WHERE datetype = 'date'
        AND yearmonth = 201505) a
    LEFT JOIN tmpDeptTechCensus b on a.thedate = b.thedate
    LEFT JOIN (
      SELECT thedate, techkey, coalesce(flaghours, 0) + coalesce(ptlhrs, 0) AS flagHours, shopTime
      -- INTO #flag 
      FROM (
        SELECT b.storecode, a.thedate, c.techkey, c.technumber, 
          round(SUM(CASE WHEN b.opcodekey NOT IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS flagHours,
          round(SUM(CASE WHEN b.opcodekey IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS shopTime  
        FROM dds.day a
        LEFT JOIN dds.factRepairOrder b on a.datekey = b.flagdatekey
        LEFT JOIN dds.dimTech c on b.techKey = c.techkey
        WHERE yearmonth = 201505
          AND c.techkey IS NOT NULL 
        GROUP BY  a.thedate, b.storecode, c.technumber, c.techkey, c.technumber) m
      LEFT JOIN (
        SELECT ptco#, ptdate, pttech, SUM(ptlhrs) AS ptlhrs
        FROM dds.stgArkonaSDPXTIM
        GROUP BY ptco#, ptdate, pttech) n on m.thedate = n.ptdate
          AND m.storecode = n.ptco#
          AND m.technumber = n.pttech) c on a.thedate = c.thedate
        AND b.techkey = c.techkey
      LEFT JOIN (
        SELECT b.thedate, employeekey, SUM(clockHours) AS clockHours,
-- *a*        
        SUM(clockHours + vacationhours + holidayhours + ptohours) AS totalclockHours
        -- INTO #clock
        FROM dds.edwClockHoursFact a
        INNER JOIN dds.day b on a.datekey = b.datekey
        WHERE yearmonth = 201505
          AND a.employeekey IN (
            SELECT distinct employeekey
            FROM tmpDeptTechCensus)
        GROUP BY thedate, employeekey) d on a.thedate = d.thedate
          AND b.employeekey = d.employeekey
) z
WHERE flagdeptcode = 'bs'  
GROUP BY description, employeenumber, technumber  
HAVING SUM(clockhours) <> 0
ORDER BY technumber    



-- flag hours

SELECT x.*, flaghours + shoptime
FROM (
SELECT b.storecode, a.thedate, c.techkey, c.technumber, b.ro,
  round(SUM(CASE WHEN b.opcodekey NOT IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS flagHours,
  round(SUM(CASE WHEN b.opcodekey IN (22106, 22107, 22108) THEN flagHours ELSE 0 END), 2) AS shopTime  
FROM dds.day a
LEFT JOIN dds.factRepairOrder b on a.datekey = b.flagdatekey
INNER JOIN dds.dimTech c on b.techKey = c.techkey
  AND c.technumber = '234'
WHERE yearmonth = 201505
  AND c.techkey IS NOT NULL 
GROUP BY  a.thedate, b.storecode, c.technumber, c.techkey, c.technumber, b.ro
) x
ORDER BY ro

-- holy fuck me IN the neck shit
-- line 1 of ro 18036179, on 6/1 gayla zeroed out ALL the flaghour entries FROM may
-- AND made one flag hour entry for the total of 31.7 hours on 6/1
-- therefor it does NOT show up on vision with a flagdate limit of 5/31
select b.thedate, a.ro, a.line, a.flaghours, a.*
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimtech c on a.techkey = c.techkey
WHERE ro = '18036179'

SELECT *
FROM dds.stgArkonaSDPRDET
WHERE ptro# = '18036179' AND ptline = 1 ORDER BY ptseq#
