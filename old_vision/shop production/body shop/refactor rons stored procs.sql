/*
2 stored procs

1. GetEstimatorGoals: calls GetEstimatorGoalsByDate 6 times to generate
     6 weeks of data

2. GetEstimatorGoalsByDate

*/

/*
-- GetEstimatorGoalsByDate, @hours calc
-- goal/capacity hours
SELECT a.date,
  SUM(
    CASE 
      WHEN b.date IS NULL THEN a.hours
      ELSE
        CASE 
          WHEN b.partial = false THEN 0
          ELSE a.hours/2
        END
    END)/2 
FROM WorkerSchedules a
LEFT JOIN WorkersPTO b on a.departmentID = b.departmentID
  AND a.partyID = b.partyID
  AND a.date = b.date
  AND b.thruTS IS NULL 
WHERE a.thruTS IS NULL
  AND a.role = 'TechRole_Metal'
  AND b.thruTS IS NULL 
  AND a.date BETWEEN curdate() - 35 AND curdate() + 7  
group BY a.date  
  
  
--

-- loaded metal hours 
SELECT a.date, 
  SUM(CASE c.team when 1 then a.hours ELSE 0 END) AS team1MetalHours,
  SUM(CASE c.team when 2 then a.hours ELSE 0 END) AS team2MetalHours,
  SUM(CASE WHEN (b.fileHandler = 'Unassigned' or b.fileHandler IS NULL OR b.fileHandler = '') THEN a.hours ELSE 0 END) AS team3MetalHours
FROM jobSchedules a
INNER JOIN bsJobs b on a.jobID = b.jobID
LEFT JOIN bsFileHandlerTeams c on b.fileHandler = c.name
WHERE a.thruTS IS NULL 
  AND a.role IN ('BodyShop_Blueprint','BodyShop_Repair')
   AND a.date BETWEEN curdate() - 35 AND curdate() + 7  
GROUP BY a.date 








-- 6 weeks of dates

SELECT min(thedate) AS mon, max(thedate) as fri, sundayToSaturdayWeek
FROM dds.day
WHERE dayOfWeek BETWEEN 2 AND 6
  AND sundaytosaturdayweek BETWEEN (
    SELECT sundayToSaturdayWeek
    FROM dds.day 
    WHERE thedate = curdate()) - 5 AND (
    SELECT sundayToSaturdayWeek
    FROM dds.day
    WHERE thedate = curdate()) 
GROUP BY sundayToSaturdayWeek    


SELECT *
-- INTO #wtf
FROM (
  SELECT min(thedate) AS mon, max(thedate) as fri, dayname, sundayToSaturdayWeek
  FROM dds.day
  WHERE dayOfWeek BETWEEN 2 AND 6
    AND sundaytosaturdayweek BETWEEN (
      SELECT sundayToSaturdayWeek
      FROM dds.day 
      WHERE thedate = curdate()) - 5 AND (
      SELECT sundayToSaturdayWeek
      FROM dds.day
      WHERE thedate = curdate()) 
  GROUP BY sundayToSaturdayWeek, dayname) s
LEFT JOIN (
  SELECT a.date, 
    SUM(CASE c.team when 1 then a.hours ELSE 0 END) AS team1MetalHours,
    SUM(CASE c.team when 2 then a.hours ELSE 0 END) AS team2MetalHours,
    SUM(CASE WHEN (b.fileHandler = 'Unassigned' or b.fileHandler IS NULL OR b.fileHandler = '') THEN a.hours ELSE 0 END) AS team3MetalHours
  FROM jobSchedules a
  INNER JOIN bsJobs b on a.jobID = b.jobID
  LEFT JOIN bsFileHandlerTeams c on b.fileHandler = c.name
  WHERE a.thruTS IS NULL 
    AND a.role IN ('BodyShop_Blueprint','BodyShop_Repair')
     AND a.date BETWEEN curdate() - 45 AND curdate() + 7  
  GROUP BY a.date) t on t.date BETWEEN s.mon AND s.fri
ORDER BY mon  
  

-- 7/24
-- DROP TABLE #wtf;
DECLARE @fromDate date; 
DECLARE @thruDate date;

@fromDate = (
  SELECT min(thedate) 
  FROM dds.day
  WHERE dayOfWeek BETWEEN 2 AND 6
    AND sundaytosaturdayweek BETWEEN (
      SELECT sundayToSaturdayWeek
      FROM dds.day 
      WHERE thedate = curdate()) - 5 AND (
      SELECT sundayToSaturdayWeek
      FROM dds.day
      WHERE thedate = curdate()));
@thruDate = (
  SELECT max(thedate) 
  FROM dds.day
  WHERE dayOfWeek BETWEEN 2 AND 6
    AND sundaytosaturdayweek BETWEEN (
      SELECT sundayToSaturdayWeek
      FROM dds.day 
      WHERE thedate = curdate()) - 5 AND (
      SELECT sundayToSaturdayWeek
      FROM dds.day
      WHERE thedate = curdate()));     
      
SELECT s.thedate, s.dayOfWeek, s.dayName, s.sundayToSaturdayWeek, 
  t.team1MetalHours, t.team2MetalHours, t.team3MetalHours,
  u.t1Goal, v.t2Goal,
  t.team1MetalHours + t.team2MetalHours + t.team3MetalHours AS load,
  u.t1Goal + v.t2Goal capacity
INTO #wtf  
FROM dds.day s
LEFT JOIN (-- metal hours loaded BY day BY team
  SELECT a.date, 
    SUM(CASE c.team when 1 then a.hours ELSE 0 END) AS team1MetalHours,
    SUM(CASE c.team when 2 then a.hours ELSE 0 END) AS team2MetalHours,
    SUM(CASE WHEN (b.fileHandler = 'Unassigned' or b.fileHandler IS NULL OR b.fileHandler = '') THEN a.hours ELSE 0 END) AS team3MetalHours
  FROM jobSchedules a
  INNER JOIN bsJobs b on a.jobID = b.jobID
  LEFT JOIN bsFileHandlerTeams c on b.fileHandler = c.name
  WHERE a.thruTS IS NULL 
    AND a.role IN ('BodyShop_Blueprint','BodyShop_Repair')
    AND a.date BETWEEN @fromDate AND @thruDate
  GROUP BY a.date) t on s.thedate = t.date
LEFT JOIN ( -- 1/2 capacity 
  SELECT a.date,
    SUM(
      CASE 
        WHEN b.date IS NULL THEN a.hours
        ELSE
          CASE 
            WHEN b.partial = false THEN 0
            ELSE a.hours/2
          END
      END)/2 AS t1Goal
  FROM WorkerSchedules a
  LEFT JOIN WorkersPTO b on a.departmentID = b.departmentID
    AND a.partyID = b.partyID
    AND a.date = b.date
    AND b.thruTS IS NULL 
  WHERE a.thruTS IS NULL
    AND a.role = 'TechRole_Metal'
    AND b.thruTS IS NULL 
    AND a.date BETWEEN @fromDate AND @thruDate
  group BY a.date) u on s.thedate = u.date 
LEFT JOIN ( -- 1/2 capacity
  SELECT a.date,
    SUM(
      CASE 
        WHEN b.date IS NULL THEN a.hours
        ELSE
          CASE 
            WHEN b.partial = false THEN 0
            ELSE a.hours/2
          END
      END)/2 AS t2Goal
  FROM WorkerSchedules a
  LEFT JOIN WorkersPTO b on a.departmentID = b.departmentID
    AND a.partyID = b.partyID
    AND a.date = b.date
    AND b.thruTS IS NULL 
  WHERE a.thruTS IS NULL
    AND a.role = 'TechRole_Metal'
    AND b.thruTS IS NULL 
    AND a.date BETWEEN @fromDate AND @thruDate
  group BY a.date) v on s.thedate = v.date   
WHERE s.theDate BETWEEN @fromDate AND @thruDate
  AND s.dayOfWeek BETWEEN 2 AND 6;      
  
-- select * FROM #wtf  

SELECT a.*,
  (SELECT team1MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT1Load,
  (SELECT t1Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT1Goal, 
  (SELECT team2MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT2Load,
  (SELECT t2Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT2Goal,    
  (SELECT team3MetalHours
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT3Load, 
  (SELECT team1MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT1Load,
  (SELECT t1Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT1Goal, 
  (SELECT team2MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT2Load,
  (SELECT t2Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT2Goal,    
  (SELECT team3MetalHours
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT3Load,
  (SELECT team1MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT1Load,
  (SELECT t1Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT1Goal, 
  (SELECT team2MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT2Load,
  (SELECT t2Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT2Goal,    
  (SELECT team3MetalHours
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT3Load,
  (SELECT team1MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT1Load,
  (SELECT t1Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT1Goal, 
  (SELECT team2MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT2Load,
  (SELECT t2Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT2Goal,    
  (SELECT team3MetalHours
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT3Load,   
  (SELECT team1MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT1Load,
  (SELECT t1Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT1Goal, 
  (SELECT team2MetalHours 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT2Load,
  (SELECT t2Goal 
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT2Goal,    
  (SELECT team3MetalHours
    FROM #wtf WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT3Load,
   (SELECT SUM(team1MetalHours)
     FROM #wtf
     WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
       GROUP BY sundayToSaturdayWeek) AS T1Loaded,
    (SELECT SUM(t1Goal)
      FROM #wtf
      WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
        GROUP BY sundayToSaturdayWeek) AS t1Goal, 
   (SELECT SUM(team2MetalHours)
     FROM #wtf
     WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
       GROUP BY sundayToSaturdayWeek) AS T2Loaded,
    (SELECT SUM(t2Goal)
      FROM #wtf
      WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
        GROUP BY sundayToSaturdayWeek) AS t2Goal,
   (SELECT SUM(team3MetalHours)
     FROM #wtf
     WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
       GROUP BY sundayToSaturdayWeek) AS T3Loaded                                           
FROM (
SELECT MIN(thedate) AS MonDate, sundayToSaturdayWeek
FROM #wtf 
GROUP BY sundayToSaturdayWeek) a;
*/


/*
CREATE TABLE zjonEstimatorProduction (
  theDate date constraint NOT NULL,
  dayOfWeek integer constraint NOT NULL,
  sundayToSaturdayWeek integer constraint NOT NULL, 
  team1MetalHours double default '0' constraint NOT NULL,
  team2MetalHours double default '0' constraint NOT NULL, 
  team3MetalHours double default '0' constraint NOT NULL, 
  t1Goal double default '0' constraint NOT NULL,  
  t2Goal double default '0' constraint NOT NULL,
  constraint PK primary key (theDate)) IN database;  
  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'zjonEstimatorProduction','zjonEstimatorProduction.adi','dayOfWeek', 'dayOfWeek','',2,512,'');    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'zjonEstimatorProduction','zjonEstimatorProduction.adi','sundayToSaturdayWeek', 'sundayToSaturdayWeek','',2,512,'');       
*/ 

-- drop procedure GetEstimatorGoals;
ALTER PROCEDURE zjonGetEstimatorGoals( 
      MonDate DATE OUTPUT,
      MonT1Loaded DOUBLE ( 2 ) OUTPUT,
      MonT1Goal DOUBLE ( 2 ) OUTPUT,
      MonT2Loaded DOUBLE ( 2 ) OUTPUT,
      MonT2Goal DOUBLE ( 2 ) OUTPUT,
      MonT3Loaded DOUBLE ( 2 ) OUTPUT,
      TuesT1Loaded DOUBLE ( 2 ) OUTPUT,
      TuesT1Goal DOUBLE ( 2 ) OUTPUT,
      TuesT2Loaded DOUBLE ( 2 ) OUTPUT,
      TuesT2Goal DOUBLE ( 2 ) OUTPUT,
      TuesT3Loaded DOUBLE ( 2 ) OUTPUT,
      WedT1Loaded DOUBLE ( 2 ) OUTPUT,
      WedT1Goal DOUBLE ( 2 ) OUTPUT,
      WedT2Loaded DOUBLE ( 2 ) OUTPUT,
      WedT2Goal DOUBLE ( 2 ) OUTPUT,
      WedT3Loaded DOUBLE ( 2 ) OUTPUT,
      ThursT1Loaded DOUBLE ( 2 ) OUTPUT,
      ThursT1Goal DOUBLE ( 2 ) OUTPUT,
      ThursT2Loaded DOUBLE ( 2 ) OUTPUT,
      ThursT2Goal DOUBLE ( 2 ) OUTPUT,
      ThursT3Loaded DOUBLE ( 2 ) OUTPUT,
      FriT1Loaded DOUBLE ( 2 ) OUTPUT,
      FriT1Goal DOUBLE ( 2 ) OUTPUT,
      FriT2Loaded DOUBLE ( 2 ) OUTPUT,
      FriT2Goal DOUBLE ( 2 ) OUTPUT,
      FriT3Loaded DOUBLE ( 2 ) OUTPUT,
      T1Loaded DOUBLE ( 2 ) OUTPUT,
      T1Goal DOUBLE ( 2 ) OUTPUT,
      T2Loaded DOUBLE ( 2 ) OUTPUT,
      T2Goal DOUBLE ( 2 ) OUTPUT,
      T3Loaded DOUBLE ( 2 ) OUTPUT)
BEGIN
/*      
EXECUTE PROCEDURE zjonGetEstimatorGoals();

EXECUTE PROCEDURE GetEstimatorGoals();
*/
DECLARE @fromDate date; 
DECLARE @thruDate date;
DECLARE @departmentID string;
DECLARE @coef double;
@DepartmentID = '38db5855-0879-484c-b2d6-d1fe7183b1c5';
@Coef = 1;
@fromDate = (
  SELECT min(thedate) 
  FROM dds.day
  WHERE dayOfWeek BETWEEN 2 AND 6
    AND sundaytosaturdayweek 
      BETWEEN (
        SELECT sundayToSaturdayWeek
        FROM dds.day 
        WHERE thedate = curdate()) - 5 
      AND (
        SELECT sundayToSaturdayWeek
        FROM dds.day
        WHERE thedate = curdate()));
@thruDate = (
  SELECT max(thedate) 
  FROM dds.day
  WHERE dayOfWeek BETWEEN 2 AND 6
    AND sundaytosaturdayweek 
      BETWEEN (
        SELECT sundayToSaturdayWeek
        FROM dds.day 
        WHERE thedate = curdate()) - 5 
      AND (
        SELECT sundayToSaturdayWeek
        FROM dds.day
        WHERE thedate = curdate()));     

DELETE FROM zjonEstimatorProduction;    

INSERT INTO zjonEstimatorProduction   
SELECT s.thedate, s.dayOfWeek, s.sundayToSaturdayWeek, 
  coalesce(t.team1MetalHours, 0), coalesce(t.team2MetalHours, 0), 
  coalesce(t.team3MetalHours, 0),
  coalesce(u.t1Goal, 0), coalesce(u.t2Goal, 0)
FROM dds.day s
LEFT JOIN (-- metal hours loaded BY day BY team
  SELECT a.date, 
    SUM(CASE c.team when 1 then a.hours ELSE 0 END) AS team1MetalHours,
    SUM(CASE c.team when 2 then a.hours ELSE 0 END) AS team2MetalHours,
    SUM(CASE WHEN (b.fileHandler = 'Unassigned' or b.fileHandler IS NULL OR 
      b.fileHandler = '') THEN a.hours ELSE 0 END) AS team3MetalHours
  FROM jobSchedules a
  INNER JOIN bsJobs b on a.jobID = b.jobID
  LEFT JOIN bsFileHandlerTeams c on b.fileHandler = c.name
  WHERE a.thruTS IS NULL 
    AND a.role IN ('BodyShop_Blueprint','BodyShop_Repair')
    AND a.date BETWEEN @fromDate AND @thruDate
  GROUP BY a.date) t on s.thedate = t.date
LEFT JOIN ( -- 1/2 capacity for each team: Goal (capacity)
  SELECT a.date,
    SUM(
      CASE 
        WHEN b.date IS NULL THEN a.hours
        ELSE
          CASE 
            WHEN b.partial = false THEN 0
            ELSE a.hours/2
          END
      END)/2 AS t1Goal,
    SUM(
      CASE 
        WHEN b.date IS NULL THEN a.hours
        ELSE
          CASE 
            WHEN b.partial = false THEN 0
            ELSE a.hours/2/@Coef
          END
      END)/2 AS t2Goal 
  FROM WorkerSchedules a
  INNER JOIN partyRelationships aa on aa.partyId1 = @departmentID
    AND aa.partyId2 = a.partyID
    AND aa.typ = 'PartyRelationship_DepartmentEmployee'
    AND aa.thruTS IS NULL 
  LEFT JOIN WorkersPTO b on a.departmentID = b.departmentID
    AND a.partyID = b.partyID
    AND a.date = b.date
    AND b.thruTS IS NULL 
  WHERE a.thruTS IS NULL
    AND a.role = 'TechRole_Metal'
    AND b.thruTS IS NULL 
    AND a.date BETWEEN @fromDate AND @thruDate
  group BY a.date) u on s.thedate = u.date 
WHERE s.theDate BETWEEN @fromDate AND @thruDate
  AND s.dayOfWeek BETWEEN 2 AND 6;       
-- select * FROM zjonEstimatorProduction  
INSERT INTO __output
SELECT top 100 a.MonDate,
  (SELECT team1MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT1Load,
  (SELECT t1Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT1Goal, 
  (SELECT team2MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT2Load,
  (SELECT t2Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT2Goal,    
  (SELECT team3MetalHours
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 2) AS MonT3Load, 
  (SELECT team1MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT1Load,
  (SELECT t1Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT1Goal, 
  (SELECT team2MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT2Load,
  (SELECT t2Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT2Goal,    
  (SELECT team3MetalHours
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 3) AS TuesT3Load,
  (SELECT team1MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT1Load,
  (SELECT t1Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT1Goal, 
  (SELECT team2MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT2Load,
  (SELECT t2Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT2Goal,    
  (SELECT team3MetalHours
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 4) AS WedT3Load,
  (SELECT team1MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT1Load,
  (SELECT t1Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT1Goal, 
  (SELECT team2MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT2Load,
  (SELECT t2Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT2Goal,    
  (SELECT team3MetalHours
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 5) AS ThursT3Load,   
  (SELECT team1MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT1Load,
  (SELECT t1Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT1Goal, 
  (SELECT team2MetalHours 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT2Load,
  (SELECT t2Goal 
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT2Goal,    
  (SELECT team3MetalHours
    FROM zjonEstimatorProduction WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      AND dayOfWeek = 6) AS FriT3Load,
  (SELECT SUM(team1MetalHours)
    FROM zjonEstimatorProduction
    WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      GROUP BY sundayToSaturdayWeek) AS T1Loaded,
  (SELECT SUM(t1Goal)
    FROM zjonEstimatorProduction
    WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      GROUP BY sundayToSaturdayWeek) AS t1Goal, 
  (SELECT SUM(team2MetalHours)
    FROM zjonEstimatorProduction
    WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      GROUP BY sundayToSaturdayWeek) AS T2Loaded,
  (SELECT SUM(t2Goal)
    FROM zjonEstimatorProduction
    WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
      GROUP BY sundayToSaturdayWeek) AS t2Goal,
  (SELECT SUM(team3MetalHours)
   FROM zjonEstimatorProduction
   WHERE sundayToSaturdayWeek = a.sundayToSaturdayWeek
     GROUP BY sundayToSaturdayWeek) AS T3Loaded                                           
FROM (
  SELECT MIN(thedate) AS MonDate, sundayToSaturdayWeek
  FROM zjonEstimatorProduction 
  GROUP BY sundayToSaturdayWeek) a
ORDER BY MonDate DESC;
END;

