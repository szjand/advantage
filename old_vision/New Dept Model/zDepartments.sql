DROP TABLE zDepartments;
CREATE TABLE zDepartments (
  DepartmentKey integer CONSTRAINT NOT NULL,
  dl1 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
  dl2 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
  dl3 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
  dl4 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
  dl5 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
CONSTRAINT PK PRIMARY KEY (DepartmentKey)) IN database;  
-- CONSTRAINT PK PRIMARY KEY (dl1,dl2,dl3,dl4,dl5)) IN database;
  
INSERT INTO zDepartments(DepartmentKey, dl1) values(1, 'Grand Forks');  
INSERT INTO zDepartments(DepartmentKey, dl1, dl2) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Variable');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Fixed');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Variable');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Main Shop');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Parts');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Car Wash');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Detail');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'PDQ');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Body Shop');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Fixed', 'Main Shop');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Fixed', 'Parts');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Fixed', 'PDQ');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4, dl5) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Main Shop', 'Drive');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4, dl5) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Main Shop', 'Shop');

EXECUTE PROCEDURE sp_CreateIndex90( 
   'zDepartments','zDepartments.adi','NK','dl1;dl2;dl3;dl4;dl5',
   '',2051,512, '' ); 
   
DROP TABLE zPositions;
CREATE TABLE zPositions (
  DepartmentKey cichar(24) CONSTRAINT NOT NULL,
  Position cichar(24) CONSTRAINT NOT NULL,
  ThruDate date CONSTRAINT NOT NULL,
  FromDate cichar(24) CONSTRAINT NOT NULL,
CONSTRAINT PK PRIMARY KEY (DepartmentKey, Position, ThruDate)) IN database;