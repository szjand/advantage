/*
9/16/14
would really love to define the whole relationship chain between:
dimEmployee -> dimTech -> tpEmployees -> tpTechs (tmpDeptTechCensus), 
right now it is a fucking nightmarish mish mash of ad hoc
poorly thought out one offs.
Do not believe one can do RI across links, anyway, quit whining, take a small stab at incremental imiprovement

sp.getTpMainShopTechSummary
what should the person identifier be?
what it IS currently doing (AND i question whether this IS good OR good enuf) IS:
input parameter IS username, so
tpEmployees.userName -> tpEmployees.employeeNumber -> 
  tpTechs.employeeNumber -> tpEmployees.techKey ALL of which IS one loosey-goosey
  relationship chain
ahh fuck, good enuf (barely)
  because no enforcement of questions LIKE:
  tpEmployees: PK IS username, can a username emp# change?
  tpTechs: PK: techKey
           NK: (unique)techNumber, departmentKey 
		     can the employeeNumber change for a NK?

ok, this could make it a little better
tpEmployees: unique index on employeeNumber
tpTechs: unique index on employeeNumber
can NOT DO ri, because advantage insists on referenced TABLE index being PK
so, a before insert trigger on tpTechs checking for existence of emp# IN tpEmployees	
*/
-- ok
SELECT employeenumber
FROM tptechs
GROUP BY employeenumber HAVING COUNT(*) > 1
-- ok
SELECT employeenumber
FROM tpEmployees
GROUP BY employeenumber
HAVING COUNT(*) > 1
-- uh oh
SELECT *
FROM tpTechs a
WHERE NOT EXISTS (
  SELECT 1
  FROM tpEmployees
  WHERE employeeNumber = a.employeeNumber)
-- ok, go ahead AND whack then  
SELECT *
FROM tpdata
WHERE techkey IN (28,29);
--< done 10/5/14 --------------------------------------------------------------<
DELETE
FROM tpTechs
WHERE techkey IN (28,29);
-- ADD the unique indexes
EXECUTE PROCEDURE sp_CreateIndex90( 'tpEmployees', 'tpEmployees.adi', 'employeeNumber', 'employeeNumber', '', 2051, 512, NULL );
DROP INDEX tpTechs.employeeNumber;
EXECUTE PROCEDURE sp_CreateIndex90( 'tpTechs', 'tpTechs.adi', 'employeeNumber', 'employeeNumber', '', 2051, 512, NULL );
--/> done 10/5/14 ------------------------------------------------------------/>