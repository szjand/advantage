UPDATE pdqWriterPAyrollData 
SET extraCreditDay = 0,
    extraCreditPPTD = 0,
	extraCreditPointsPPTD = 0;
	
UPDATE pdqStoreData 
SET extraCreditDay = 0,
    extraCreditPPTD = 0,
	extraCreditPointsPPTD = 0;	
	
DELETE FROM pdqWriterReviews;	