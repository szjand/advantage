/*
3/18/14
  hourly rate IS to be FROM Arkona
  need to ADD hourlyRate AND otRate to the table
*/
DROP TABLE pdqWriterPayrollData;
CREATE TABLE pdqWriterPayrollData ( 
      DateKey Integer constraint NOT NULL,
      TheDate Date constraint NOT NULL,
      PayPeriodStart Date constraint NOT NULL,
      PayPeriodEnd Date constraint NOT NULL,
      PayPeriodSeq Integer constraint NOT NULL,
      DayOfPayPeriod Integer constraint NOT NULL,
      DayName CIChar( 12 ) constraint NOT NULL,
      payPeriodSelectFormat CIChar( 100 ) constraint NOT NULL,
      storeCode cichar(3) constraint NOT NULL,
      employeeNumber CIChar( 9 ) constraint NOT NULL,
      firstName CIChar( 25 ) constraint NOT NULL,
      lastName CIChar( 25 ) constraint NOT NULL,
      fullName cichar(52) constraint NOT NULL,
      username cichar(50) constraint NOT NULL,
      hourlyRate numeric(8,2) constraint NOT NULL default '0',
      otRate numeric(8,2) constraint NOT NULL default '0',
      regularHoursDay numeric(8,2) constraint NOT NULL default '0',
      regularHoursPPTD numeric(8,2) constraint NOT NULL default '0',
      otHoursDay numeric(8,2) constraint NOT NULL default '0',
      otHoursPPTD numeric(8,2) constraint NOT NULL default '0',
      vacationHoursDay numeric(8,2) constraint NOT NULL default '0',
      vacationHoursPPTD numeric(8,2) constraint NOT NULL default '0',
      ptoHoursDay numeric(8,2) constraint NOT NULL default '0',
      ptoHoursPPTD numeric(8,2) constraint NOT NULL default '0',
      holidayHoursDay numeric(8,2) constraint NOT NULL default '0',
      holidayHoursPPTD numeric(8,2) constraint NOT NULL default '0',  
      lofDay integer constraint NOT NULL default '0',
      lofPPTD integer constraint NOT NULL default '0',
      rotateDay integer constraint NOT NULL default '0',
      rotatePPTD integer constraint NOT NULL default '0',
      rotatePenetrationPPTD numeric(8,2) constraint NOT NULL default '0',
      rotatePointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0',    
      airFilterDay integer constraint NOT NULL default '0',
      airFilterPPTD integer constraint NOT NULL default '0',
      airFilterPenetrationPPTD numeric(8,2) constraint NOT NULL default '0',
      airFilterPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0',       
      nitrogenDay integer constraint NOT NULL default '0',
      nitrogenPPTD integer constraint NOT NULL default '0',
      nitrogenPenetrationPPTD numeric(8,2) constraint NOT NULL default '0',
      nitrogenPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0', 
      alignmentDay integer constraint NOT NULL default '0',
      alignmentPPTD integer constraint NOT NULL default '0',
      alignmentPenetrationPPTD numeric(8,2) constraint NOT NULL default '0',
      alignmentPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0',      
      extraCreditDay integer constraint NOT NULL default '0',
      extraCreditPPTD integer constraint NOT NULL default '0',
      extraCreditPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '2' default '0',
      emailCaptureRate numeric(8,2) constraint NOT NULL default '0',
      emailCapturePointsPPTD integer constraint NOT NULL  constraint minimum '0' constraint maximum '3'default '0',
      laborSalesDay numeric(8,2) constraint NOT NULL default '0',
      laborSalesPPTD numeric (8,2) constraint NOT NULL default '0',
      totalPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '17' default '0',
      levelPPTD cichar(3) constraint NOT NULL default '1',    
      commissionRatePPTD numeric(8,3) constraint NOT NULL default '0',
      constraint PK primary key (dateKey, storeCode, employeeNumber)) IN database;
          
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPayrollData','pdqWriterPayrollData.adi','DateUserName','dateKey;userName',
   '',2051,512, '' );                  
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPayrollData','pdqWriterPayrollData.adi','storeCode', 'storeCode',
   '',2,512, '' );     
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPayrollData','pdqWriterPayrollData.adi','dateKey', 'dateKey',
   '',2,512, '' );          
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPayrollData','pdqWriterPayrollData.adi','employeeNumber', 'employeeNumber',
   '',2,512, '' );       
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPayrollData','pdqWriterPayrollData.adi','username', 'username',
   '',2,512, '' );       
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPayrollData','pdqWriterPayrollData.adi','payperiodseq', 'payperiodseq',
   '',2,512, '' );       

   
--DROP TABLE pdqManagerPointsMatrix;
CREATE TABLE pdqManagerPointsMatrix (
  metric cichar(40) constraint NOT NULL,
  points integer constraint NOT NULL,
  minimum double constraint NOT NULL,
  maximum double constraint NOT NULL,
  constraint PK primary key (metric, points)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqManagerPointsMatrix','pdqManagerPointsMatrix.adi','metric','metric',
   '',2,512, '' );   
INSERT INTO pdqManagerPointsMatrix values('rotate',0,0,.39); 
INSERT INTO pdqManagerPointsMatrix values('rotate',1,.4,.49);
INSERT INTO pdqManagerPointsMatrix values('rotate',2,.5,.59);
INSERT INTO pdqManagerPointsMatrix values('rotate',3,.6,1.0);
INSERT INTO pdqManagerPointsMatrix values('airfilter',0,0,.12);   
INSERT INTO pdqManagerPointsMatrix values('airfilter',1,.13,.15);
INSERT INTO pdqManagerPointsMatrix values('airfilter',2,.16,.19);
INSERT INTO pdqManagerPointsMatrix values('airfilter',3,.2,1.0);
INSERT INTO pdqManagerPointsMatrix values('nitrogen',0,0,.04);
INSERT INTO pdqManagerPointsMatrix values('nitrogen',1,.05,.06);
INSERT INTO pdqManagerPointsMatrix values('nitrogen',2,.07,.08);
INSERT INTO pdqManagerPointsMatrix values('nitrogen',3,.09,1.0);
INSERT INTO pdqManagerPointsMatrix values('email',0,0,.49);
INSERT INTO pdqManagerPointsMatrix values('email',1,.5,.59);
INSERT INTO pdqManagerPointsMatrix values('email',2,.6,.69);
INSERT INTO pdqManagerPointsMatrix values('email',3,.7,1.0);
INSERT INTO pdqManagerPointsMatrix values('alignment',0,0,.04);
INSERT INTO pdqManagerPointsMatrix values('alignment',1,.05,.06);
INSERT INTO pdqManagerPointsMatrix values('alignment',2,.07,.08);
INSERT INTO pdqManagerPointsMatrix values('alignment',3,.09,1.0);
INSERT INTO pdqManagerPointsMatrix values('extraCredit',0,0,7);
INSERT INTO pdqManagerPointsMatrix values('extraCredit',1,8,11);
INSERT INTO pdqManagerPointsMatrix values('extraCredit',2,12,12);


DROP TABLE pdqManagerCommissionMatrix;
CREATE TABLE pdqManagerCommissionMatrix (
    level cichar(3) constraint NOT NULL,
    storeCode cichar(3) constraint NOT NULL,
    minLaborSales numeric(8,2) constraint NOT NULL,
    minPoints integer constraint NOT NULL,
    maxPoints integer constraint NOT NULL,
    commissionRate double constraint NOT NULL,
    constraint PK Primary Key (storeCode, level)) IN database;
INSERT INTO pdqManagerCommissionMatrix values('1','RY1',5000,0,8,.0075);  
INSERT INTO pdqManagerCommissionMatrix values('2','RY1',5000,9,12,.0125);
INSERT INTO pdqManagerCommissionMatrix values('3','RY1',5000,13,17,.020);  
INSERT INTO pdqManagerCommissionMatrix values('1','RY2',1500,0,8,.0075);  
INSERT INTO pdqManagerCommissionMatrix values('2','RY2',1500,9,12,.0125);
INSERT INTO pdqManagerCommissionMatrix values('3','RY2',1500,13,17,.020);   

-- DROP TABLE pdqWriterReviews;
CREATE TABLE pdqWriterReviews (
  username cichar(50) constraint NOT NULL,
  theDate date constraint NOT NULL,
  url cichar(500) constraint NOT NULL,
  constraint PK primary key (username, thedate, url)) IN database;

EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterReviews','pdqWriterReviews.adi','username', 'username',
   '',2,512, '' );    
    
--EXECUTE PROCEDURE sp_dropreferentialintegrity( 'tpEmployees-pdqWriterReviews'); 
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'tpEmployees-pdqWriterReviews',
     'tpEmployees', 
     'pdqWriterReviews', 
     'username', 
     2, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 
  
  
-- 3/25 pdqStoreData
DROP TABLE pdqStoreData;
CREATE TABLE pdqStoreData ( 
      DateKey Integer constraint NOT NULL,
      TheDate Date constraint NOT NULL,
      PayPeriodStart Date constraint NOT NULL,
      PayPeriodEnd Date constraint NOT NULL,
      PayPeriodSeq Integer constraint NOT NULL,
      DayOfPayPeriod Integer constraint NOT NULL,
      DayName CIChar( 12 ) constraint NOT NULL,
      payPeriodSelectFormat CIChar( 100 ) constraint NOT NULL,
      storeCode cichar(3) constraint NOT NULL,
      lofDay integer constraint NOT NULL default '0',
      lofPPTD integer constraint NOT NULL default '0',
      rotateDay integer constraint NOT NULL default '0',
      rotatePPTD integer constraint NOT NULL default '0',
      rotatePenetrationPPTD double constraint NOT NULL default '0',
      rotatePointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0',    
      airFilterDay integer constraint NOT NULL default '0',
      airFilterPPTD integer constraint NOT NULL default '0',
      airFilterPenetrationPPTD double constraint NOT NULL default '0',
      airFilterPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0',       
      nitrogenDay integer constraint NOT NULL default '0',
      nitrogenPPTD integer constraint NOT NULL default '0',
      nitrogenPenetrationPPTD double constraint NOT NULL default '0',
      nitrogenPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0', 
      alignmentDay integer constraint NOT NULL default '0',
      alignmentPPTD integer constraint NOT NULL default '0',
      alignmentPenetrationPPTD double constraint NOT NULL default '0',
      alignmentPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0',      
      extraCreditDay integer constraint NOT NULL default '0',
      extraCreditPPTD integer constraint NOT NULL default '0',
      emailCaptureRate double constraint NOT NULL default '0',
	  extraCreditPointsPPTD Integer constraint NOT NULL default '0',
      laborSalesDay double constraint NOT NULL default '0',
      laborSalesPPTD double constraint NOT NULL default '0',
      constraint PK primary key (dateKey, storeCode)) IN database;
                
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPayrollData','pdqStoreData.adi','storeCode', 'storeCode',
   '',2,512, '' );     
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPayrollData','pdqStoreData.adi','dateKey', 'dateKey',
   '',2,512, '' );          

EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPayrollData','pdqStoreData.adi','payperiodseq', 'payperiodseq',
   '',2,512, '' );       
   
CREATE TABLE pdqTmpManagerSummary (
  storecode cichar(3),
  lofPPTD integer,
  rotatePenetrationPPTD double, 
  airFilterPenetrationPPTD double,
  nitrogenPenetrationPPTD double,
  alignmentPenetrationPPTD double, 
  emailCaptureRate double,
  extraCreditPPTD integer,
  laborSalesPPTD double,
  salary double,
  totalPoints integer,
  commissionRate double, 
  commissionPay double) IN database;   
  