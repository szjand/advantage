--< PDQ Writer Pay Summary ----------------------------------------------------<

PROCEDURE getPdqWpPayPeriods(  
      payPeriodDisplay CICHAR ( 100 ) OUTPUT,
      payPeriodIndicator Integer OUTPUT) 
      
returns payperiods for DROP down list
          
execute procedure getPdqWpPayPeriods();  
     
---------------------------------------------------    
PROCEDURE getPdqWpWriterSummary(
  username cichar(50),
  payPeriodIndicator integer,
  fullName cichar(52) output,
  totalLofs integer output,
  totalPoints integer output,
  totalLaborSales double output,
  hourlyRate double output,
  overtimeRate double output,
  otherPayRate double output,
  commissionRate numeric(8,3) output,
  totalCommissionPay double output,
  totalHourlyPay double output,
  totalGrossPay double output)

returns base values AND totals for a writer AND a payperiod

EXECUTE PROCEDURE getPdqWpWriterSummary('sberg@rydellchev.com', 0);

---------------------------------------------------    
PROCEDURE getPdqWpPointDetails(
  username cichar(50),
  payPeriodIndicator integer,
  metric cichar(24) output,
  howMany integer output,
  penetration double output,
  points integer output)
  
returns 1 row per metric with COUNT, penetration, points for a writer AND a payperiod
(commission point totals TABLE on the page)  

EXECUTE PROCEDURE getPdqWpPointDetails('sberg@rydellchev.com', 0);


---------------------------------------------------    
CREATE PROCEDURE getPdqWpHourlyDetails(
  username cichar(50),
  payPeriodIndicator integer,
  hourType cichar(24) output,
  hours double output,
  rate double output,
  totalPay double output)

returns 1 row per hourly pay type with hours, rate, pay for a writer AND a payperiod
  
EXECUTE PROCEDURE getPdqWpHourlyDetails('svacura@rydellchev.com', 0);

--/> PDQ Writer Pay Summary ---------------------------------------------------/> 

--< PDQ Writer ROs page -------------------------------------------------------<

PROCEDURE getPdqWpWriterRos ( 
      username CICHAR ( 50 ),
      payPeriodIndicator Integer,
      dayName CICHAR (12) OUTPUT,
      theDate date OUTPUT,
      ro cichar(9) OUTPUT,
      lof cichar(3) OUTPUT,
      rotate cichar(3) OUTPUT,
      airFilter cichar(3) OUTPUT,
      nitrogen cichar(3) OUTPUT,
      alignment cichar(3) OUTPUT,
      emailCapture cichar(3) OUTPUT,
      laborSales double output)

returns 1 row per ro for a writer AND a payperiod

EXECUTE PROCEDURE getPdqWpWriterRos('sberg@rydellchev.com', 0);

--/> PDQ Writer ROs page ------------------------------------------------------/>

--< PDQ Writer Totals page ----------------------------------------------------<

PROCEDURE getPdqWpWriterTotals ( 
      payPeriodIndicator Integer,
      storeCode cichar(3) output,
      fullName CICHAR (52) OUTPUT,
      lof integer OUTPUT,
      rotate double OUTPUT,
      airFilter double OUTPUT,
      nitrogen double OUTPUT,
      alignment double OUTPUT,
      emailCapture double OUTPUT,
      extraCredit integer output,
      laborSales double output,
      totalPoints integer output)
      
returns 1 row per writer for the most recent date IN a pay period  

EXECUTE PROCEDURE getPdqWpWriterTotals(0);
    
--/> PDQ Writer Totals page ---------------------------------------------------/>
  