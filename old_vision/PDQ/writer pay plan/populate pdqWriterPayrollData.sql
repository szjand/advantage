-- initial load, only data since 3/9/14
-- leave off points for now, generate IN query for page
-- base row
/*
-- initial load, only data since 3/9/14
3/28
  remove extraCredit, that IS handled IN the stored procs WHERE extra credit 
  data IS added AND removed
  
4/3/14
  removed NITROINV FROM metric opcodes
  CATCH divide BY 0 IN email
*/
DELETE FROM pdqWriterPayrollData;
TRY 
  INSERT INTO pdqWriterPayrollData (dateKey, theDate, payPeriodStart, payPeriodEnd, 
    payPeriodSeq, dayOfPayPeriod, dayName, payPeriodSelectFormat, 
    storecode, employeeNumber, firstName, lastName, fullName, username)

SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  a.biweeklypayperiodsequence, a.dayinbiweeklypayperiod, a.dayname, x.ppsel, 
  b.storecode, b.employeenumber, b.firstName, b.lastName, 
  trim(b.firstname) + ' ' + b.lastname AS fullName, b.username
FROM dds.day a
LEFT JOIN (
  SELECT aa.storecode, aa.employeenumber, aa.firstname, aa.lastname, bb.username
  FROM dds.edwEmployeeDim aa
  LEFT JOIN tpEmployees bb on aa.employeenumber = bb.employeenumber
  WHERE aa.currentrow = true
    AND aa.employeenumber <> '1148720'
    AND aa.active = 'active'
    AND EXISTS (
      SELECT 1
      FROM dds.dimServiceWriter
      WHERE censusdept = 'ql'
        AND currentrow = true
        AND active = true
        AND employeenumber = aa.employeenumber)) b on 1 = 1        
LEFT JOIN (
  SELECT biweeklypayperiodsequence, TRIM(pt1) + ' ' + TRIM(pt2) AS ppsel
    FROM (  
    SELECT distinct biweeklypayperiodstartdate, biweeklypayperiodenddate, biweeklypayperiodsequence, 
      (SELECT trim(monthname) + ' '
        + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
        FROM dds.day
        WHERE thedate = w.biweeklypayperiodstartdate) AS pt1,
      (SELECT trim(monthname) + ' ' 
        + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
        + TRIM(CAST(theyear AS sql_char))
        FROM dds.day
        WHERE thedate = w.biweeklypayperiodenddate) AS pt2
    FROM dds.day w
    WHERE w.thedate BETWEEN '03/09/2014' AND curdate()) y) x on a.biweeklypayperiodsequence = x.biweeklypayperiodsequence             
WHERE a.thedate BETWEEN '03/09/2014' AND curdate();          
CATCH ALL 
  RAISE pdqWriterPayrollData(1, 'Base Row ' + __errtext);
END TRY;     

TRY   
-- clock hours day     
  UPDATE pdqWriterPayrollData
  SET regularHoursDay = x.reghours,
      otHoursDay = x.ot,
      vacationHoursDay = x.vac,
      ptoHoursDay = x.pto,
      holidayHoursDay = x.hol 
  FROM (       
    SELECT a.thedate, a.storecode, a.employeenumber,
      SUM(c.regularhours) AS regHours, SUM(c.overtimehours) AS OT, 
      SUM(c.vacationHours) AS VAC, SUM(c.ptohours) AS PTO, 
      SUM(c.holidayhours) AS HOL 
    FROM pdqWriterPayrollData a    
    LEFT JOIN dds.edwEmployeeDim b on a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeeNumber
    LEFT JOIN dds.edwClockHoursFact c on a.datekey = c.datekey
      AND b.employeekey = c.employeekey
    GROUP BY a.thedate, a.storecode, a.employeenumber) x 
  WHERE pdqWriterPayrollData.thedate = x.thedate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;      
-- clock hours PPTD        
  UPDATE pdqWriterPayrollData
  SET regularHoursPPTD = x.reg
  FROM (
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, 
      a.regularHoursDay, SUM(b.regularHoursDay) AS reg
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeeNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, 
      a.regularHoursDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber; 
    
  UPDATE pdqWriterPayrollData
  SET otHoursPPTD = x.ot
  FROM (
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, 
    a.otHoursDay, SUM(b.otHoursDay) AS ot
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeeNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, 
      a.otHoursDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;    
    
  UPDATE pdqWriterPayrollData
  SET vacationHoursPPTD = x.vac
  FROM (
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, 
      a.vacationHoursDay, SUM(b.vacationHoursDay) AS vac
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeeNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, 
      a.vacationHoursDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;   
    
  UPDATE pdqWriterPayrollData
  SET ptoHoursPPTD = x.pto
  FROM (
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.ptoHoursDay, 
      SUM(b.ptoHoursDay) AS pto
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeeNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.ptoHoursDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;       
    
  UPDATE pdqWriterPayrollData
  SET holidayHoursPPTD = x.hol
  FROM (
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.holidayHoursDay, 
      SUM(b.holidayHoursDay) AS hol
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeeNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.holidayHoursDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;        
CATCH ALL 
  RAISE pdqWriterPayrollData(2, 'ClockHours ' + __errtext);
END TRY;     

TRY   
-- lof count per day
  UPDATE pdqWriterPayrollData
  SET lofDay = x.lof
  FROM (
    SELECT a.thedate, a.storecode, a.employeenumber, coalesce(sum(lofs), 0) AS lof
    FROM pdqWriterPayrollData a
    INNER JOIN dds.dimServiceWriter b on a.employeenumber = b.employeenumber  
    LEFT JOIN (
      SELECT bb.thedate, cc.serviceWriterKey, COUNT(*) AS LOFs
      FROM dds.factRepairOrder aa
      INNER JOIN dds.day bb on aa.closedatekey = bb.datekey
      INNER JOIN dds.dimServiceWriter cc on aa.serviceWriterKey = cc.ServiceWriterKey
      INNER JOIN dds.dimOpcode dd on aa.opcodeKey = dd.opcodeKey
      WHERE bb.theyear > 2013
        AND cc.censusDept = 'QL'
        AND dd.pdqcat1 = 'lof'    
      GROUP BY bb.thedate, cc.serviceWriterKey) c 
        on a.theDate = c.thedate 
        AND b.serviceWriterKey = c.serviceWriterKey
    GROUP BY a.thedate, a.storecode, a.employeenumber) x  
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;         
    
  -- lof pptd
  UPDATE pdqWriterPayrollData
  SET lofPPTD = x.lof
  FROM (
    SELECT a.thedate, a.storecode, a.employeenumber, a.payperiodseq, a.lofday, 
      SUM(b.lofday) AS lof
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeeNumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storecode, a.employeenumber, a.payperiodseq, a.lofday) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;  
CATCH ALL 
  RAISE pdqWriterPayrollData(3, 'LOF ' + __errtext);
END TRY;       
                   
TRY
-- metrics 
  UPDATE pdqWriterPayrollData
  SET rotateDay = x.rotates,
      airFilterDay = x.airfilters,
      nitrogenDay = x.nitrogen,
      alignmentDay = x.alignments
  FROM (    
    SELECT a.thedate, a.storecode, a.employeenumber, 
      coalesce(sum(c.airfilter), 0) AS airfilters, 
      coalesce(sum(c.alignment), 0) AS alignments, 
      coalesce(sum(c.nitrogen), 0) AS nitrogen, 
      coalesce(sum(c.rotate), 0) AS rotates
    FROM pdqWriterPayrollData a
    INNER JOIN dds.dimServiceWriter b on a.employeenumber = b.employeenumber 
    LEFT JOIN (
      SELECT bb.thedate, aa.serviceWriterKey,
        SUM(CASE WHEN pdqcat2 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilter,
        SUM(CASE WHEN pdqcat2 = 'Alignment' THEN 1 ELSE 0 END) AS Alignment,
        SUM(CASE WHEN pdqcat2 = 'Nitrogen' THEN 1 ELSE 0 END) AS Nitrogen,
        SUM(CASE WHEN pdqcat2 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
      FROM dds.factRepairOrder aa
      INNER JOIN dds.day bb on aa.closedatekey = bb.datekey
      INNER JOIN dds.dimopcode cc on aa.opcodekey = cc.opcodekey
      INNER JOIN dds.dimServiceWriter dd on aa.serviceWriterKey = dd.serviceWriterKey
      WHERE bb.theYear > 2013
        AND cc.pdqcat1 = 'Other'
        AND cc.opcode <> 'NITROINV' 
      GROUP BY bb.thedate, aa.serviceWriterKey) c 
        on a.thedate = c.thedate
         AND b.serviceWriterKey = c.serviceWriterKey
    GROUP BY a.thedate, a.storecode, a.employeenumber) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;  
/*	
  UPDATE pdqWriterPayrollData 
  SET extraCreditDay = x.extraCredit
  FROM (
    SELECT thedate, username, COUNT(*) extraCredit
	FROM pdqWriterReviews
	group BY theDate, username) x     
  WHERE pdqWriterPayrollData.theDate = x.thedate
    AND pdqWriterPayrollData.username = x.username;	
*/  
CATCH ALL 
  RAISE pdqWriterPayrollData(4, 'Metrics Day ' + __errtext);
END TRY;                                                                                      

TRY
-- metrics pptd
  UPDATE pdqWriterPayrollData
  SET rotatePPTD = x.rotates
  FROM (  
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.rotateDay, 
      SUM(b.rotateDay) AS rotates
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeenumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.rotateDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;   
    
  UPDATE pdqWriterPayrollData
  SET airFilterPPTD = x.airfilters
  FROM (  
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.airFilterDay, 
      SUM(b.airFilterDay) AS airfilters
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeenumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.airFilterDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;      
    
  UPDATE pdqWriterPayrollData
  SET nitrogenPPTD = x.nitrogen
  FROM (  
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.nitrogenDay, 
      SUM(b.nitrogenDay) AS nitrogen
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeenumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.nitrogenDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;     
    
  UPDATE pdqWriterPayrollData
  SET alignmentPPTD = x.alignments
  FROM (  
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.alignmentDay, 
      SUM(b.alignmentDay) AS alignments
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeenumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.alignmentDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;      
/*    
  UPDATE pdqWriterPayrollData
  SET extraCreditPPTD = x.extraCredit
  FROM (  
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.extraCreditDay, 
      SUM(b.extraCreditDay) AS extraCredit
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeenumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.extraCreditDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;       
*/     
CATCH ALL 
  RAISE pdqWriterPayrollData(5, 'Metrics PPTD ' + __errtext);
END TRY; 

-- penetration PPTD
TRY
  UPDATE pdqWriterPayrollData
  SET rotatePenetrationPPTD = x.rotate,
      airFilterPenetrationPPTD = x.airfilter,
      nitrogenPenetrationPPTD = x.nitrogen,
      alignmentPenetrationPPTD = x.alignment
  FROM (    
  SELECT thedate, storeCode, employeeNumber, 
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * rotatePPTD/lofPPTD, 2) 
    END AS rotate,
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * airFilterPPTD/lofPPTD, 2) 
    END AS airfilter,  
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * nitrogenPPTD/lofPPTD, 2) 
    END AS nitrogen,  
    CASE lofPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * alignmentPPTD/lofPPTD, 2) 
    END AS alignment    
  FROM pdqWriterPayrollData) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber; 
CATCH ALL 
  RAISE pdqWriterPayrollData(6, 'Penetration PPTD ' + __errtext);
END TRY; 

-- email capture
TRY
-- this IS pretty good, NOT EXACTLY the same AS the email capture page, 
-- pretty fucking close though
UPDATE pdqWriterPayrollData
SET emailCaptureRate = x.email
FROM ( 
  SELECT thedate, storecode, employeenumber, 
  case theCount 
    when 0 then 0
    else round(1.0 * valid/theCount, 2) 
  END AS email
  FROM (  
    SELECT a.thedate, a.storeCode, a.employeeNumber, 
--      count(coalesce(
--        CASE hasValidEmail
--          WHEN true THEN 1
--          ELSE 0
--        END, 0)) AS theCount,
      count(hasValidEmail) AS theCount,  
      sum(coalesce(
        CASE hasValidEmail
          WHEN true THEN 1
          ELSE 0
        END, 0)) AS valid     
    FROM pdqWriterPayrollData a    
    LEFT JOIN cstfbEmailData b on a.fullName = b.employeeName
      AND b.transactionDate BETWEEN a.theDate - 90 AND a.thedate
    GROUP BY thedate, a.storeCode, a.employeeNumber) y) x  
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;        
CATCH ALL 
  RAISE pdqWriterPayrollData(7, 'email Capture ' + __errtext);
END TRY; 

-- labor sales
TRY
  UPDATE pdqWriterPayrollData
  SET laborSalesDay = x.laborSales
  FROM (
    SELECT a.theDate, a.storeCode, a.employeeNumber, coalesce(SUM(c.laborSales), 0) AS laborSales
    FROM pdqWriterPayrollData a
    INNER JOIN dds.dimServiceWriter b on a.employeenumber = b.employeenumber
    LEFT JOIN (
      SELECT bb.theDate, aa.serviceWriterKey, SUM(aa.laborSales) AS laborSales
      FROM dds.factRepairOrder aa
      INNER JOIN dds.day bb on aa.closeDateKey = bb.dateKey
      WHERE bb.theYear > 2013
        AND aa.serviceTypeKey = (
          SELECT serviceTypeKey
          FROM dds.dimServiceType
          WHERE serviceTypeCode = 'MR')  
        AND aa.paymentTypeKey NOT IN (
          SELECT paymentTypeKey 
          FROM dds.dimPaymentType
          WHERE paymentTypeCode IN ('I','W'))
      GROUP BY bb.thedate, aa.serviceWriterKey) c 
        on a.theDate = c.theDate
          AND b.serviceWriterKey = c.serviceWriterKey   
    GROUP BY a.theDate, a.storeCode, a.employeeNumber) x  
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;   
      
  UPDATE pdqWriterPayrollData
  SET laborSalesPPTD = x.laborSales
  FROM (  
    SELECT a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.laborSalesDay, 
      SUM(b.laborSalesDay) AS laborSales
    FROM pdqWriterPayrollData a, pdqWriterPayrollData b
    WHERE a.payperiodseq = b.payperiodseq
      AND a.storeCode = b.storeCode
      AND a.employeeNumber = b.employeenumber
      AND b.thedate <= a.thedate
    GROUP BY a.thedate, a.storeCode, a.employeeNumber, a.payperiodseq, a.laborSalesDay) x
  WHERE pdqWriterPayrollData.theDate = x.theDate
    AND pdqWriterPayrollData.storeCode = x.storeCode
    AND pdqWriterPayrollData.employeeNumber = x.employeeNumber; 
CATCH ALL 
  RAISE pdqWriterPayrollData(8, 'Labor Sales ' + __errtext);
END TRY; 

-- points / level
TRY 
UPDATE pdqWriterPayrollData
SET 
  rotatePointsPPTD = (
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'rotate' 
      AND rotatePenetrationPPTD BETWEEN minimum AND maximum),
  airFilterPointsPPTD = (    
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'airfilter' 
      AND airFilterPenetrationPPTD BETWEEN minimum AND maximum), 
  nitrogenPointsPPTD = (   
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'nitrogen' 
      AND nitrogenPenetrationPPTD BETWEEN minimum AND maximum),  
  alignmentPointsPPTD = (
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'alignment' 
      AND alignmentPenetrationPPTD BETWEEN minimum AND maximum),   
  extraCreditPointsPPTD = (
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'extraCredit' 
      AND extraCreditPPTD BETWEEN minimum AND maximum), 
  emailCapturePointsPPTD = (    
    SELECT points 
    FROM pdqWriterPointsMatrix 
    WHERE metric = 'email' 
      AND emailCaptureRate BETWEEN minimum AND maximum);  
      
UPDATE pdqWriterPayrollData
SET totalPointsPPTD = rotatePointsPPTD + airFilterPointsPPTD +  nitrogenPointsPPTD +
  alignmentPointsPPTD + extraCreditPointsPPTD + emailCapturePointsPPTD;         

CATCH ALL 
  RAISE pdqWriterPayrollData(9, 'Points ' + __errtext);
END TRY; 

-- level/commissionRate pptd
TRY
UPDATE pdqWriterPayrollData
SET levelPPTD = (
  SELECT level
  FROM pdqWriterCommissionMatrix
  WHERE totalPointsPPTD BETWEEN minPoints AND maxPoints);

UPDATE pdqWriterPayrollData
SET commissionRatePPTD = x.commissionRate
FROM (  
  SELECT thedate, storecode, employeenumber, 
    CASE 
      WHEN laborSalesPPTD < 800 THEN 0
      ELSE (
        SELECT commissionRate
        FROM pdqWriterCommissionMatrix
        WHERE a.totalPointsPPTD BETWEEN minPoints AND maxPoints)
    END AS commissionRate  
  FROM pdqWriterPayrollData a) x
WHERE pdqWriterPayrollData.theDate = x.theDate
  AND pdqWriterPayrollData.storeCode = x.storeCode
  AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;  
CATCH ALL 
  RAISE pdqWriterPayrollData(10, 'Commission ' + __errtext);
END TRY;

TRY 
UPDATE pdqWriterPayrollData
SET hourlyRate = x.hourlyRate
FROM (
  SELECT a.storeCode, a.employeenumber, a.thedate, b.hourlyRate
  FROM pdqWriterPayrollData a
  LEFT JOIN dds.edwEmployeeDim b on a.storeCode = b.storeCode
    AND a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.employeeKeyFromDate AND b.employeeKeyThruDate) x
WHERE pdqWriterPayrollData.theDate = x.theDate
  AND pdqWriterPayrollData.storeCode = x.storeCode
  AND pdqWriterPayrollData.employeeNumber = x.employeeNumber;  

UPDATE pdqWriterPayrollData
SET otRate = 1.5 * hourlyRate;  
CATCH ALL 
  RAISE pdqWriterPayrollData(11, 'Hourly Rate ' + __errtext);
END TRY;