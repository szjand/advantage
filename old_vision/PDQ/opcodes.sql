2/28
  missing alignment opcodes

SELECT storecode, opcode, pdqcat1, pdqcat2, pdqcat3
FROM dimopcode
WHERE pdqcat1 = 'LOF'
  OR (pdqcat1 = 'Other' and pdqcat2 IN ('Rotate','Air Filter','Nitrogen'))
ORDER BY storecode, pdqcat1, pdqcat2, pdqcat3

SELECT storecode, pdqcat1, pdqcat2, pdqcat3
FROM dimopcode
WHERE pdqcat1 <> 'n/a'
GROUP BY storecode, pdqcat1, pdqcat2, pdqcat3


SELECT DISTINCT a.ro, c.opcode, d.name
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey
WHERE b.yearmonth > 201309
  AND a.serviceTypeKey = (
    SELECT serviceTypeKEy
    FROM dimServiceType
    WHERE serviceTypeCode = 'QL')
  AND d.CensusDept = 'QL'
    
SELECT *
FROM dimopcode
WHERE opcode LIKE '%lign%'

SELECT a.ro, b.name, b.censusdept, c.servicetypecode
FROM factRepairOrder a
INNER JOIN dimservicewriter b on a.servicewriterkey = b.servicewriterkey
INNER JOIN dimServiceType c on a.serviceTypeKey = c.serviceTypeKey
WHERE a.opcodekey in (
  SELECT opcodekey
  FROM dimopcode
  WHERE opcode = 'ALIGN')
AND closedatekey IN (
  SELECT datekey 
  FROM day
  WHERE yearmonth > 201310)  
AND b.censusdept = 'ql'  

what are the servicetypes being written BY pdq writers

SELECT a.storecode, b.name, b.censusdept, c.serviceTypeCode, COUNT(*), MIN(ro), MAX(ro)
FROM factRepairOrder a
INNER JOIN dimservicewriter b on a.servicewriterkey = b.servicewriterkey
INNER JOIN dimServiceType c on a.serviceTypeKey = c.serviceTypeKey
WHERE b.censusdept = 'ql'
AND closedatekey IN (
  SELECT datekey 
  FROM day
  WHERE yearmonth > 201310) 
GROUP BY a.storecode, b.name, b.censusdept, c.serviceTypeCode  

-- what ros are being written BY pdq techs without an lof on them
-- gives me some currently uncategorized LOF opcodes
-- RY1
-- PDQD520
-- PDQD2530
-- PDQDF
-- LOFC ??? customer supplied oil

-- RY2
-- YW10AA
-- 3K
-- 110009
SELECT d.storecode, d.opcode, left(d.description, 75), COUNT(*), MIN(ro), MAX(ro)
FROM factRepairOrder a
INNER JOIN dimservicewriter b on a.servicewriterkey = b.servicewriterkey
INNER JOIN dimServiceType c on a.serviceTypeKey = c.serviceTypeKey
INNER JOIN dimOpcode d on a.opcodekey = d.opcodekey
WHERE b.censusdept = 'ql'
  AND closedatekey IN (
    SELECT datekey 
    FROM day
    WHERE yearmonth > 201305) 
  AND NOT EXISTS (
    SELECT ro
    FROM factRepairOrder
    WHERE ro = a.ro
      AND opcodeKey IN (
        SELECT opcodeKey
        FROM dimopcode
        WHERE pdqcat1 = 'LOF'))
GROUP BY d.storecode, d.opcode, left(d.description, 75)
--ORDER BY opcode
ORDER BY COUNT(*) DESC

-- the question for ben IS, for penetration
        
"exhaustive" list

SELECT storecode, 'LOF', opcode
FROM dimopcode 
WHERE pdqcat1 = 'LOF'
  AND storecode = 'RY1'
UNION 
SELECT storecode, 'Rotate', opcode
FROM dimopcode 
WHERE pdqcat2 = 'Rotate'
  AND storecode = 'RY1'
UNION 
SELECT storecode, 'Air Filter', opcode
FROM dimopcode 
WHERE pdqcat2 = 'Air Filters'
  AND storecode = 'RY1' 
UNION 
SELECT storecode, 'Nitrogen', opcode
FROM dimopcode 
WHERE pdqcat2 = 'Nitrogen'
  AND storecode = 'RY1'   
UNION 
select 'RY1', 'Align', 'ALIGN'
FROM system.iota
union 
SELECT storecode, 'LOF', opcode
FROM dimopcode 
WHERE pdqcat1 = 'LOF'
  AND storecode = 'RY2'
UNION 
SELECT storecode, 'Rotate', opcode
FROM dimopcode 
WHERE pdqcat2 = 'Rotate'
  AND storecode = 'RY2'
UNION 
SELECT storecode, 'Air Filter', opcode
FROM dimopcode 
WHERE pdqcat2 = 'Air Filters'
  AND storecode = 'RY2' 
UNION 
SELECT storecode, 'Nitrogen', opcode
FROM dimopcode 
WHERE pdqcat2 = 'Nitrogen'
  AND storecode = 'RY2'   
UNION 
select 'RY2', 'Align', 'ALIGN'
FROM system.iota;
  
  
a few stragglers FROM ben
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Air Filters',
    pdqcat3 = 'N/A'
WHERE storecode = 'ry1'
  AND opcode = '13c';    
  
UPDATE dimOpcode
SET pdqcat1 = 'LOF',
    pdqcat2 = 'Diesel',
    pdqcat3 = 'BOC'
WHERE storecode = 'ry1'
  AND opcode = 'pdqdsf';            
  
UPDATE dimOpcode
SET pdqcat1 = 'LOF',
    pdqcat2 = 'Best',
    pdqcat3 = 'BOC'
WHERE storecode = 'ry1'
  AND opcode = 'pdqm1f';   
  
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Rotate',
    pdqcat3 = 'Rotate'
WHERE storecode = 'ry1'
  AND opcode = 'rotf';     
  
UPDATE dimOpcode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Air Filters',
    pdqcat3 = 'Air Filters'
WHERE storecode = 'ry2'
  AND opcode = 'icf';     
    
-- ADD categorization to alignment  
UPDATE dimOpCode
SET pdqcat1 = 'Other',
    pdqcat2 = 'Alignment',
    pdqcat3 = 'N/A'
WHERE opcode = 'ALIGN';
    
SELECT * FROM dimopcode WHERE opcode = 'icf'  

SELECT b.thedate, a.* 
FROM factrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
WHERE opcodekey = 15472
ORDER BY thedate DESC 