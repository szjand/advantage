DROP TABLE pdqWriterData;
CREATE TABLE pdqWriterData ( 
      DateKey Integer constraint NOT NULL,
      TheDate Date constraint NOT NULL,
      PayPeriodStart Date constraint NOT NULL,
      PayPeriodEnd Date constraint NOT NULL,
      PayPeriodSeq Integer constraint NOT NULL,
      DayOfPayPeriod Integer constraint NOT NULL,
      DayName CIChar( 12 ) constraint NOT NULL,
      payPeriodSelectFormat CIChar( 100 ) constraint NOT NULL,
      DepartmentKey Integer constraint NOT NULL,
      serviceWriterKey Integer constraint NOT NULL,
      serviceWriterNumber CIChar( 3 ) constraint NOT NULL,
      EmployeeNumber CIChar( 9 ) constraint NOT NULL,
      FirstName CIChar( 25 ) constraint NOT NULL,
      LastName CIChar( 25 ) constraint NOT NULL,
      fullName cichar(52) constraint NOT NULL,
      username cichar(50) constraint NOT NULL,
      regularHoursDay numeric(8,2) constraint NOT NULL default '0',
      regularHoursPPTD numeric(8,2) constraint NOT NULL default '0',
      otHoursDay numeric(8,2) constraint NOT NULL default '0',
      otHoursPPTD numeric(8,2) constraint NOT NULL default '0',
      vacationHoursDay numeric(8,2) constraint NOT NULL default '0',
      vacationHoursPPTD numeric(8,2) constraint NOT NULL default '0',
      ptoHoursDay numeric(8,2) constraint NOT NULL default '0',
      ptoHoursPPTD numeric(8,2) constraint NOT NULL default '0',
      holidayHoursDay numeric(8,2) constraint NOT NULL default '0',
      holidayHoursPPTD numeric(8,2) constraint NOT NULL default '0',  
      lofDay integer constraint NOT NULL default '0',
      lofPPTD integer constraint NOT NULL default '0',
      rotateDay integer constraint NOT NULL default '0',
      rotatePPTD integer constraint NOT NULL default '0',
      rotatePenetrationPPTD numeric(8,2) constraint NOT NULL default '0',
      rotatePointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0',    
      airFilterDay integer constraint NOT NULL default '0',
      airFilterPPTD integer constraint NOT NULL default '0',
      airFilterPenetrationPPTD numeric(8,2) constraint NOT NULL default '0',
      airFilterPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0',       
      nitrogenDay integer constraint NOT NULL default '0',
      nitrogenPPTD integer constraint NOT NULL default '0',
      nitrogenPenetrationPPTD numeric(8,2) constraint NOT NULL default '0',
      nitrogenPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0', 
      alignmentDay integer constraint NOT NULL default '0',
      alignmentPPTD integer constraint NOT NULL default '0',
      alignmentPenetrationPPTD numeric(8,2) constraint NOT NULL default '0',
      alignmentPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '3' default '0',      
      extraCreditDay integer constraint NOT NULL default '0',
      extraCreditPPTD integer constraint NOT NULL default '0',
      extraCreditPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '2' default '0',
      emailCaptureRate numeric(8,2) constraint NOT NULL default '0',
      emailCapturePointsPPTD integer constraint NOT NULL  constraint minimum '0' constraint maximum '3'default '0',
      laborSalesDay numeric(8,2) constraint NOT NULL default '0',
      laborSalesPPTD numeric (8,2) constraint NOT NULL default '0',
      totalPointsPPTD integer constraint NOT NULL constraint minimum '0' constraint maximum '17' default '0',
      levelPPTD cichar(3) constraint NOT NULL default '1',    
      commissionRatePPTD numeric(8,3) constraint NOT NULL default '0',
      constraint PK primary key (TheDate, DepartmentKey, serviceWriterNumber)) IN database;
          
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterData','pdqWriterData.adi','dateKeywriterKey','dateKey;serviceWriterKey',
   '',2051,512, '' );               
-- this one will NOT WORK
-- ned could have a row for ry1 & ry2 on the same day                  
--EXECUTE PROCEDURE sp_CreateIndex90( 
--   'pdqWriterData','pdqWriterData.adi','dateusername','theDate;username',
--   '',2051,512, '' );         
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterData','pdqWriterData.adi','dateusernamedeptKey','theDate;username;departmentKey',
   '',2051,512, '' ); 
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterData','pdqWriterData.adi','deptKey','departmentKey',
   '',2,512, '' );           

DROP TABLE pdqWriterPointsMatrix;
CREATE TABLE pdqWriterPointsMatrix (
  metric cichar(40) constraint NOT NULL,
  points integer constraint NOT NULL,
  minimum numeric(8,2) constraint NOT NULL,
  maximum numeric(8,2) constraint NOT NULL,
  constraint PK primary key (metric, points)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqWriterPointsMatrix','pdqWriterPointsMatrix.adi','metric','metric',
   '',2,512, '' );   
INSERT INTO pdqWriterPointsMatrix values('rotate',0,0,.39); 
INSERT INTO pdqWriterPointsMatrix values('rotate',1,.4,.49);
INSERT INTO pdqWriterPointsMatrix values('rotate',2,.5,.59);
INSERT INTO pdqWriterPointsMatrix values('rotate',3,.6,1.0);
INSERT INTO pdqWriterPointsMatrix values('airfilter',0,0,.12);   
INSERT INTO pdqWriterPointsMatrix values('airfilter',1,.13,.15);
INSERT INTO pdqWriterPointsMatrix values('airfilter',2,.16,.19);
INSERT INTO pdqWriterPointsMatrix values('airfilter',3,.2,1.0);
INSERT INTO pdqWriterPointsMatrix values('nitrogen',0,0,.04);
INSERT INTO pdqWriterPointsMatrix values('nitrogen',1,.05,.06);
INSERT INTO pdqWriterPointsMatrix values('nitrogen',2,.07,.08);
INSERT INTO pdqWriterPointsMatrix values('nitrogen',3,.09,1.0);
INSERT INTO pdqWriterPointsMatrix values('email',0,0,.49);
INSERT INTO pdqWriterPointsMatrix values('email',1,.5,.59);
INSERT INTO pdqWriterPointsMatrix values('email',2,.6,.69);
INSERT INTO pdqWriterPointsMatrix values('email',3,.7,1.0);
INSERT INTO pdqWriterPointsMatrix values('alignment',0,0,.04);
INSERT INTO pdqWriterPointsMatrix values('alignment',1,.05,.06);
INSERT INTO pdqWriterPointsMatrix values('alignment',2,.07,.08);
INSERT INTO pdqWriterPointsMatrix values('alignment',3,.09,1.0);
INSERT INTO pdqWriterPointsMatrix values('extraCredit',0,0,0);
INSERT INTO pdqWriterPointsMatrix values('extraCredit',1,1,1);
INSERT INTO pdqWriterPointsMatrix values('extraCredit',2,2,2);

DROP TABLE pdqWriterCommissionMatrix;
CREATE TABLE pdqWriterCommissionMatrix (
    level cichar(3) constraint NOT NULL,
    minLaborSales numeric(8,2) constraint NOT NULL,
    minPoints integer constraint NOT NULL,
    maxPoints integer constraint NOT NULL,
    commissionRate numeric(8,3) constraint NOT NULL,
    constraint PK Primary Key (level)) IN database;
INSERT INTO pdqWriterCommissionMatrix values('1',1750,0,8,.05);  
INSERT INTO pdqWriterCommissionMatrix values('2',1750,9,12,.065);
INSERT INTO pdqWriterCommissionMatrix values('3',1750,13,17,.085);  



--DROP TABLE pdqManagerPointsMatrix;
CREATE TABLE pdqManagerPointsMatrix (
  metric cichar(40) constraint NOT NULL,
  points integer constraint NOT NULL,
  minimum double constraint NOT NULL,
  maximum double constraint NOT NULL,
  constraint PK primary key (metric, points)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'pdqManagerPointsMatrix','pdqManagerPointsMatrix.adi','metric','metric',
   '',2,512, '' );   
INSERT INTO pdqManagerPointsMatrix values('rotate',0,0,.39); 
INSERT INTO pdqManagerPointsMatrix values('rotate',1,.4,.49);
INSERT INTO pdqManagerPointsMatrix values('rotate',2,.5,.59);
INSERT INTO pdqManagerPointsMatrix values('rotate',3,.6,1.0);
INSERT INTO pdqManagerPointsMatrix values('airfilter',0,0,.12);   
INSERT INTO pdqManagerPointsMatrix values('airfilter',1,.13,.15);
INSERT INTO pdqManagerPointsMatrix values('airfilter',2,.16,.19);
INSERT INTO pdqManagerPointsMatrix values('airfilter',3,.2,1.0);
INSERT INTO pdqManagerPointsMatrix values('nitrogen',0,0,.04);
INSERT INTO pdqManagerPointsMatrix values('nitrogen',1,.05,.06);
INSERT INTO pdqManagerPointsMatrix values('nitrogen',2,.07,.08);
INSERT INTO pdqManagerPointsMatrix values('nitrogen',3,.09,1.0);
INSERT INTO pdqManagerPointsMatrix values('email',0,0,.49);
INSERT INTO pdqManagerPointsMatrix values('email',1,.5,.59);
INSERT INTO pdqManagerPointsMatrix values('email',2,.6,.69);
INSERT INTO pdqManagerPointsMatrix values('email',3,.7,1.0);
INSERT INTO pdqManagerPointsMatrix values('alignment',0,0,.04);
INSERT INTO pdqManagerPointsMatrix values('alignment',1,.05,.06);
INSERT INTO pdqManagerPointsMatrix values('alignment',2,.07,.08);
INSERT INTO pdqManagerPointsMatrix values('alignment',3,.09,1.0);
INSERT INTO pdqManagerPointsMatrix values('extraCredit',0,0,7);
INSERT INTO pdqManagerPointsMatrix values('extraCredit',1,8,11);
INSERT INTO pdqManagerPointsMatrix values('extraCredit',2,12,12);


DROP TABLE pdqManagerCommissionMatrix;
CREATE TABLE pdqManagerCommissionMatrix (
    level cichar(3) constraint NOT NULL,
    storeCode cichar(3) constraint NOT NULL,
    minLaborSales numeric(8,2) constraint NOT NULL,
    minPoints integer constraint NOT NULL,
    maxPoints integer constraint NOT NULL,
    commissionRate double constraint NOT NULL,
    constraint PK Primary Key (storeCode, level)) IN database;
INSERT INTO pdqManagerCommissionMatrix values('1','RY1',5000,0,8,.75);  
INSERT INTO pdqManagerCommissionMatrix values('2','RY1',5000,9,12,1.25);
INSERT INTO pdqManagerCommissionMatrix values('3','RY1',5000,13,17,2.0);  
INSERT INTO pdqManagerCommissionMatrix values('1','RY2',1500,0,8,.75);  
INSERT INTO pdqManagerCommissionMatrix values('2','RY2',1500,9,12,1.25);
INSERT INTO pdqManagerCommissionMatrix values('3','RY2',1500,13,17,2.0);