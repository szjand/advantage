-- this IS golden, so far, need to figure out what to DO about the other weekly totals (seq 2,3,8,9)
DELETE FROM WriterLandingPage;
INSERT INTO WriterLandingPage
SELECT d.SundayToSaturdayWeek, 0, d.fullname, d.writernumber, d.metric, 
  trim(d.sun) AS Sun, trim(d.mon), trim(d.tue), trim(d.wed), trim(d.thu), trim(d.fri),
  trim(d.sat), e.thisweekdisplay, d.eeusername, d.seq
FROM ( -- daily
  SELECT a.sundaytosaturdayweek, 0, a.fullname, a.writernumber, a.metric, 
    max(CASE WHEN a.dayname = 'Sunday' THEN todayDisplay END) AS Sun,
    max(CASE WHEN a.dayname = 'Monday' THEN todayDisplay END) AS Mon,
    max(CASE WHEN a.dayname = 'Tuesday' THEN todayDisplay END) AS Tue,
    max(CASE WHEN a.dayname = 'Wednesday' THEN todayDisplay END) AS Wed,
    max(CASE WHEN a.dayname = 'Thursday' THEN todayDisplay END) AS Thu,
    max(CASE WHEN a.dayname = 'Friday' THEN todayDisplay END) AS Fri,
    max(CASE WHEN a.dayname = 'Saturday' THEN todayDisplay END) AS Sat,
    eeusername, seq
  FROM (
    SELECT a.*,
--      case a.metric
--        when 'CSI' then cast(c.csi AS sql_char) collate ads_default_ci
--        when 'Walk Arounds' then cast(c.walkarounds AS sql_char) collate ads_default_ci
--        else b.todaydisplay 
--      end as todaydisplay, 
      b.todaydisplay,
      b.thisweekdisplay, b.*
    FROM ( -- 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL with seq)
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.eeusername, 
        b.fullname, b.writernumber, c.metric, c.seq
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.thedate BETWEEN '03/03/2013' AND curdate()
        AND c.seq IS NOT NULL) a
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric) a 
--    LEFT JOIN servicewritercheckouts c ON a.eeusername = c.eeusername
--      AND a.thedate = c.thedate) a  
  GROUP BY a.sundaytosaturdayweek, a.fullname, a.writernumber, a.metric, eeusername, seq) d
LEFT JOIN ( -- this appears to be working for seq 1,4-7 to get weekly
  SELECT a.eeusername, a.metric, a.SundayToSaturdayWeek, a.thisweekdisplay
  FROM servicewritermetricdata a
  INNER JOIN servicewritermetrics c ON a.metric = c.metric
    AND c.seq IS NOT NULL 
  INNER JOIN (
      SELECT MAX(thedate) AS thedate, eeusername, metric, SundayToSaturdayWeek
      FROM servicewritermetricdata
      WHERE thisweekdisplay IS NOT NULL 
      GROUP BY eeusername, metric, SundayToSaturdayWeek) b ON a.thedate = b.thedate
        and a.eeusername = b.eeusername
        and a.metric = b.metric
        and a.SundayToSaturdayWeek = b.SundayToSaturdayWeek) e ON d.eeusername = e.eeusername
    AND d.metric = e.metric
    AND d.SundayToSaturdayWeek = e.SundayToSaturdayWeek 
-- WHERE d.eeusername = 'rodt@rydellchev.com' AND d.SundayToSaturdayWeek = 484 ORDER BY seq
  