-- 3/28, per Huot, final closed ros only
SELECT f.eeUserName, a.ro, b.thedate AS CloseDate, a.customername AS Customer, 
  CASE 
    WHEN c.bnphon  = '0' THEN 'No Phone'
    WHEN c.bnphon  = '' THEN 'No Phone'
    WHEN c.bnphon IS NULL THEN 'No Phone'
    ELSE c.bnphon
  END AS HomePhone, 
  CASE 
    WHEN c.bnbphn  = '0' THEN 'No Phone'
    WHEN c.bnbphn  = '' THEN 'No Phone'
    WHEN c.bnbphn IS NULL THEN 'No Phone'
    ELSE c.bnbphn
  END  AS WorkPhone, 
  CASE 
    WHEN c.bncphon  = '0' THEN 'No Phone'
    WHEN c.bncphon  = '' THEN 'No Phone'
    WHEN c.bncphon IS NULL THEN 'No Phone'
    ELSE c.bncphon
  END  AS CellPhone,  trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle,
  false as FollowUpComplete
FROM dds.factro a
INNER JOIN -- cashiered ros only
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN curdate() - 90 AND curdate()    
LEFT JOIN dds.stgArkonaBOPNAME c ON a.customerkey = c.bnkey   
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
LEFT JOIN ServiceWriters f ON a.writerid = f.writernumber
WHERE writerid IN ('714','720','705')
  AND EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype = 'w')
  AND a.void = false;      

-- 4/3 60 days back, include ALL writers
-- added storecode to bopname JOIN
--inpmast IS ok
--SELECT imvin FROM dds.stgArkonaINPMAST GROUP BY imvin HAVING COUNT(*) > 1
DROP TABLE WarrantyROs;  
CREATE TABLE WarrantyROs (
  eeUserName cichar(50),
  ro cichar(9),
  CloseDate date,
  Customer cichar(30), 
  HomePhone cichar(12),
  WorkPhone cichar(12),
  Vin cichar(17),
  Vehicle cichar(60),
  FollowUpComplete logical) IN database;
INSERT INTO WarrantyROs
SELECT f.eeUserName, a.ro, b.thedate AS CloseDate, a.customername AS Customer, 
  CASE 
    WHEN c.bnphon  = '0' THEN 'No Phone'
    WHEN c.bnphon  = '' THEN 'No Phone'
    WHEN c.bnphon IS NULL THEN 'No Phone'
    ELSE trim(c.bnphon)
  END AS HomePhone, 
  CASE 
    WHEN c.bnbphn  = '0' THEN 'No Phone'
    WHEN c.bnbphn  = '' THEN 'No Phone'
    WHEN c.bnbphn IS NULL THEN 'No Phone'
    ELSE trim(c.bnbphn)
  END  AS WorkPhone, a.vin, trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle,
  false as FollowUpComplete
FROM dds.factro a
INNER JOIN -- cashiered ros only
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN curdate() - 60 AND curdate()    
LEFT JOIN dds.stgArkonaBOPNAME c ON a.storecode = c.bnco#
  AND a.customerkey = c.bnkey    
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
INNER JOIN ServiceWriters f ON a.storecode = f.storecode 
  AND a.writerid = f.writernumber
--WHERE writerid IN ('714','720','705')
  AND EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype = 'w')
  AND a.void = false;
  
CREATE TABLE WarrantyCalls (
  ro cichar(9),
  CallTS timestamp,
  eeUserName cichar(50),
  description memo);    

DROP PROCEDURE GetWarrantyROs;
CREATE PROCEDURE GetWarrantyROs
   ( 
  eeUserName cichar(50),
  ro cichar (9) output,
  CloseDate date output,
  Customer cichar(30) output, 
  HomePhone cichar(12) output,
  WorkPhone cichar(12) output,
  Vehicle cichar(60) output,
  LastCall memo output)

BEGIN 
/*
EXECUTE PROCEDURE GetWarrantyROs('rodt@rydellchev.com');
*/
DECLARE @id string;
@id = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT a.ro, a.CloseDate AS [Closed Date], a.customer, a.HomePhone, a.WorkPhone, 
  a.vehicle, b.description
FROM WarrantyROs a
LEFT JOIN (
  SELECT *
  FROM WarrantyCalls x
  WHERE CallTS = (
    SELECT MAX(CallTS)
    FROM WarrantyCalls
    WHERE ro = x.ro)) b ON a.ro = b.ro 
WHERE a.eeUsername = @id
  AND FollowUpComplete = false;

END;  
  
DROP PROCEDURE InsertWarrantyCall;
CREATE PROCEDURE InsertWarrantyCall (
  ro cichar(9),
  eeUserName cichar(50),
  description memo)
BEGIN
/*
EXECUTE PROCEDURE InsertWarrantyCall('16106213','rodt@rydellchev.com','you are killing me here')
*/
INSERT INTO WarrantyCalls values(
  (SELECT ro FROM __input),
  now(),
  (SELECT eeUserName FROM __input), 
  (SELECT description FROM __input));
END;    
    
DROP PROCEDURE GetWarrantyROToUpdate;  
CREATE PROCEDURE GetWarrantyROToUpdate
   ( 
      ro CICHAR ( 9 ),
      CloseDate DATE OUTPUT,
      customer CICHAR ( 30 ) OUTPUT,
      HomePhone cichar(12) output,
      WorkPhone cichar(12) output,
      Vehicle CICHAR ( 60 ) OUTPUT,
      Updated TIMESTAMP OUTPUT,
      UpdatedBy CICHAR ( 50 ) OUTPUT,
      Description Memo OUTPUT
   ) 
/*
EXECUTE PROCEDURE GetWarrantyROToUpdate('16106213');
*/   
BEGIN 
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output    
SELECT a.closedate, a.customer, a.homephone, a.workphone, a.vehicle, 
  b.CallTS, b.eeusername, b.description
FROM WarrantyROs a
LEFT JOIN WarrantyCalls  b ON a.ro = b.ro 
WHERE a.ro = @ro; 

END;  
  
INSERT INTO WarrantyCalls values ('16114706',timestampadd(sql_tsi_day,-3,now()),'rodt@rydellchev.com','first warranty follow up call'); 
INSERT INTO WarrantyCalls values ('16114706',timestampadd(sql_tsi_day,-2,now()),'rodt@rydellchev.com','second warranty follow up call');
INSERT INTO WarrantyCalls values ('16114706',timestampadd(sql_tsi_day,-1,now()),'rodt@rydellchev.com','third warranty follow up call'); 
   
   
-- 4/3


SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.writerid, b.fullname, 
  b.eeUserName, cast(COUNT(*) AS sql_char) AS howmany
FROM (  
  SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
  FROM dds.day a
  WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a  
LEFT JOIN (  
  SELECT b.thedate AS finalclosedate, a.storecode, a.writerid, a.ro, 
    d.fullname, d.eeusername
  FROM dds.factro a
  INNER JOIN -- cashiered ros only
    dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate BETWEEN curdate() - 60 AND curdate()    
  INNER JOIN ServiceWriters d ON a.storecode = d.storecode 
    AND a.writerid = d.writernumber
    AND EXISTS (
      SELECT 1 
      FROM dds.factroline
      WHERE ro = a.ro
        AND paytype = 'w')
    AND a.void = false) b ON b.finalclosedate < a.thedate
GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, b.writerid, b.fullname, b.eeUserName      

-- should probably be using the WarrantyROS TABLE instead so that i can accomodate
-- those that have been marked complete
-- uh, ri issues
SELECT ro
FROM warrantyros a
GROUP BY ro HAVING COUNT(*) > 1

SELECT * FROM warrantyros WHERE ro = '16107140'
SELECT * FROM dds.factro WHERE ro = '16107140'
SELECT * FROM dds.stgArkonaBOPNAME WHERE bnkey = 1001136
problem was IN JOIN to bopname, needed to include storecode
inpmast IS ok
SELECT imvin FROM dds.stgArkonaINPMAST GROUP BY imvin HAVING COUNT(*) > 1

INSERT INTO writerlandingpage
SELECT sundaytosaturdayweek, 0, fullname, writernumber,'Open Warranty Calls',
  sun,mon,tue,wed,thu,fri,sat,'-',eeusername, 3
FROM (  
  SELECT sundaytosaturdayweek, 0, fullname, writernumber,
    trim(cast(SUM(CASE WHEN dayname = 'sunday' THEN howmany END) AS sql_char)) AS sun,
    trim(cast(SUM(CASE WHEN dayname = 'monday' THEN howmany END) AS sql_char)) AS mon,
    trim(cast(SUM(CASE WHEN dayname = 'tuesday' THEN howmany END) AS sql_char)) AS tue,
    trim(cast(SUM(CASE WHEN dayname = 'wednesday' THEN howmany END) AS sql_char)) AS wed,
    trim(cast(SUM(CASE WHEN dayname = 'thursday' THEN howmany END) AS sql_char)) AS thu,
    trim(cast(SUM(CASE WHEN dayname = 'friday' THEN howmany END) AS sql_char)) AS fri,
    trim(cast(SUM(CASE WHEN dayname = 'saturday' THEN howmany END) AS sql_char)) AS sat,
    eeusername, 3
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.writernumber, 
      b.fullname, b.eeUserName, COUNT(*) AS howmany
    FROM (  
      SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
      FROM dds.day a
      WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
    LEFT JOIN (  
      SELECT b.writernumber, b.fullname, b.eeusername, a.ro, a.closedate
      FROM warrantyros a
      LEFT JOIN servicewriters b ON a.eeusername = b.eeusername
      WHERE a.followupcomplete = false) b ON b.closedate < a.thedate
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, b.writernumber, 
      b.fullname, b.eeUserName) c  
  GROUP BY sundaytosaturdayweek, writernumber, fullname, eeusername) a
  
  
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
4/16
initial loading of WarrantyROs TABLE
7 days worth
DELETE FROM warrantyROs;
INSERT INTO warrantyROs
SELECT f.eeUserName, a.ro, b.thedate AS CloseDate, a.customername AS Customer, 
  CASE 
    WHEN c.bnphon  = '0' THEN 'No Phone'
    WHEN c.bnphon  = '' THEN 'No Phone'
    WHEN c.bnphon IS NULL THEN 'No Phone'
    ELSE trim(c.bnphon)
  END AS HomePhone, 
  CASE 
    WHEN c.bnbphn  = '0' THEN 'No Phone'
    WHEN c.bnbphn  = '' THEN 'No Phone'
    WHEN c.bnbphn IS NULL THEN 'No Phone'
    ELSE trim(c.bnbphn)
  END  AS WorkPhone, 
  CASE 
    WHEN c.bncphon  = '0' THEN 'No Phone'
    WHEN c.bncphon  = '' THEN 'No Phone'
    WHEN c.bncphon IS NULL THEN 'No Phone'
    ELSE trim(c.bncphon)
  END  AS CellPhone,    a.vin, trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle,
  false as FollowUpComplete
FROM dds.factro a
INNER JOIN -- ros cashiered BETWEEN today AND 7 days ago
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN curdate() - 7 AND curdate()    
LEFT JOIN dds.stgArkonaBOPNAME c ON a.storecode = c.bnco#
  AND a.customerkey = c.bnkey    
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
INNER JOIN ServiceWriters f ON a.storecode = f.storecode 
  AND a.writerid = f.writernumber
  AND EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype = 'w')
  AND a.void = false;

subsequently, INSERT FROM factro any warranty ro that has closed IN the past 7 days
that IS NOT already IN warrantyROs 
put this IN sp.swMetricDataToday  
INSERT INTO WarrantyROS 
SELECT m.*
FROM (
  SELECT f.eeUserName, a.ro, b.thedate AS CloseDate, a.customername AS Customer, 
    CASE 
      WHEN c.bnphon  = '0' THEN 'No Phone'
      WHEN c.bnphon  = '' THEN 'No Phone'
      WHEN c.bnphon IS NULL THEN 'No Phone'
      ELSE trim(c.bnphon)
    END AS HomePhone, 
    CASE 
      WHEN c.bnbphn  = '0' THEN 'No Phone'
      WHEN c.bnbphn  = '' THEN 'No Phone'
      WHEN c.bnbphn IS NULL THEN 'No Phone'
      ELSE trim(c.bnbphn)
    END  AS WorkPhone, a.vin, trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle,
    false as FollowUpComplete
  FROM dds.factro a
  INNER JOIN -- cashiered ros only
    dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate BETWEEN curdate() - 7 AND curdate()    
  LEFT JOIN dds.stgArkonaBOPNAME c ON a.storecode = c.bnco#
    AND a.customerkey = c.bnkey    
  LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
  INNER JOIN ServiceWriters f ON a.storecode = f.storecode 
    AND a.writerid = f.writernumber
    AND EXISTS (
      SELECT 1 
      FROM dds.factroline
      WHERE ro = a.ro
        AND paytype = 'w')
    AND a.void = false) m
LEFT JOIN warrantyROS n ON m.ro = n.ro
  AND m.eeusername = n.eeusername    
WHERE n.eeusername IS NULL;     