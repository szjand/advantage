-- Table Type of HTMLSnippets is ADT
Create Table HTMLSnippets(
   StoreCode CIChar( 3 ),
   MemberGroup CIChar( 25 ),
   App CIChar( 25 ),
   AppCode CIChar( 10 ),
   Page CIChar( 25 ),
   Description CIChar( 100 ),
   Snippet Memo,
   Seq Integer );


INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceManager', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/managerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/agedros">View Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantycalls">View Warranty Calls</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceManager', 'NetPromoter', 'np', 'LeftNavBar', NULL, '<!-- Net Promoter -->
<nav class="menu-item">
  <a class="mainlink" href="#np/managersummary"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>
  <ul class="submenu">
<li><span class="sublink"><a href="#np/aboutnp">About Net Promoter</a></span></li>   
 <li><span class="sublink"><a href="#np/managersurveys">View Surveys</a></span></li>
  </ul>
</nav><!-- .menu-item -->
', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceManager', 'Employees', 'emps', 'LeftNavBar', NULL, '<!-- Employees -->
<nav class="menu-item">
  <a class="mainlink employees" href="#emps/employeelist"><span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', 3 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/writerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/agedros">View Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantycalls">View Warranty Calls</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'NetPromoter', 'np', 'LeftNavBar', NULL, '<!-- Net Promoter -->
<nav class="menu-item">
  <a class="mainlink" href="#np/writersummary"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>
<ul class="submenu">
<li><span class="sublink"><a href="#np/aboutnp">About Net Promoter</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->
', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceManager', 'Employees', 'emps', 'LeftNavBar', NULL, '<!-- Employees -->
<nav class="menu-item">
  <a class="mainlink employees" href="#emps/employeelist"><span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceManager', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">  <a class="mainlink" href="#sco/managerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/agedros">View Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/ry2servicecalls">View Nissan Follow Up Calls</a></span></li>
  </ul></nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/writerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/agedros">View Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/ry2servicecalls">View Nissan Follow Up Calls</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceMisc', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/ry2servicecalls"><span class="glyphicon glyphicon-pencil"></span>View Nissan Follow Up Calls<span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'MainAction', NULL, '<div class="btn-group" id="servicestats">
  <a class="button" href="#sco/writerstats/<%username%>">Service Stats</a>
</div>
', NULL );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'NetPromoter', 'np', 'MainAction', NULL, '<div class="btn-group" id="netpromoter">
  <a class="button" href="#np/writer/<%username%>">Net Promoter</a>
</div>
', NULL );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Aged ROs', '<a class="button" href="#sco/agedros/<%username%>">View Aged ROs</a>', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Warranty Calls', '<a class="button" href="#sco/warrantycalls/<%username%>">View Warranty Calls</a>', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'MainAction', NULL, '<div class="btn-group" id="servicestats">
  <a class="button" href="#sco/writerstats/<%username%>">Service Stats</a>
</div>
', NULL );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Aged ROs', '<a class="button" href="#sco/agedros/<%username%>">View Aged ROs</a>', NULL );

CREATE PROCEDURE GetMainActions
   ( 
      userName CICHAR ( 50 ),
      snippet Memo OUTPUT
   ) 
BEGIN 
  DECLARE @user cichar(50);
  @user = (SELECT userName FROM __input);
  INSERT INTO __output
    SELECT top 100 replace(a.snippet, '<%username%>', TRIM(@user)) 
    FROM HTMLSnippets a
    INNER JOIN People b on b.eeUserName = @user and a.StoreCode = b.StoreCode and a.MemberGroup = b.MemberGroup
    INNER JOIN HTMLSnippets c on c.page = 'LeftNavBar' and a.StoreCode = c.StoreCode and a.MemberGroup = c.MemberGroup AND a.AppCode = c.AppCode  
    WHERE a.Page = 'MainAction'
    ORDER BY c.seq;

END;

CREATE PROCEDURE GetSubActions
   ( 
      userName CICHAR ( 50 ),
      appCode CICHAR ( 10 ),
      snippet Memo OUTPUT
   ) 
BEGIN 
  DECLARE @user cichar(50);
  DECLARE @appCode cichar(10);
  @user = (SELECT userName FROM __input);
  @appCode = (SELECT appCode FROM __input);
  INSERT INTO __output
    SELECT top 100 replace(a.snippet, '<%username%>', TRIM(@user)) 
    FROM HTMLSnippets a
    INNER JOIN People b on b.eeUserName = @user and a.StoreCode = b.StoreCode and a.MemberGroup = b.MemberGroup
    WHERE a.Page = 'SubAction' AND a.AppCode = @appCode
    ORDER BY a.seq;


END;

CREATE PROCEDURE GetUserData
   ( 
      eeUsername CICHAR ( 50 ),
      StoreCode CICHAR ( 3 ) OUTPUT,
      MemberGroup CICHAR ( 24 ) OUTPUT,
      FirstName CICHAR ( 40 ) OUTPUT,
      LastName CICHAR ( 40 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetUserData('aneumann@gfhonda.com');
*/
DECLARE @user string;
@user = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT StoreCode, MemberGroup, FirstName, LastName
FROM People
WHERE eeUsername = @user;

END;

alter PROCEDURE GetLeftNavBar
   ( 
      eeusername CICHAR ( 50 ),
      snippet Memo OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getleftnavbar('mhuot@rydellchev.com');
EXECUTE PROCEDURE getleftnavbar('rodt@rydellchev.com');
EXECUTE PROCEDURE getleftnavbar('aneumann@gfhonda.com');
EXECUTE PROCEDURE getleftnavbar('dpederson@gfhonda.com');

*/
DECLARE @user cichar(50);
@user = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT top 100 snippet
FROM HTMLSnippets a
INNER JOIN people b on a.membergroup = b.membergroup
  AND a.storecode = b.storecode 
WHERE b.eeUsername = @user
  AND Page = 'leftnavbar'
ORDER BY a.seq;




END;

alter PROCEDURE ValidateLogin
   ( 
      UserName CICHAR ( 50 ),
      Password CICHAR ( 50 ),
      MemberGroup CICHAR ( 24 ) OUTPUT,
      FirstName CICHAR ( 40 ) OUTPUT,
      LastName CICHAR ( 40 ) OUTPUT,
      StoreCode CICHAR ( 3 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE validatelogin('mhuot@rydellchev.com','password');
*/
DECLARE @username cichar(50);
DECLARE @password cichar(50);
@username = (SELECT username FROM __input);
@password = (SELECT password FROM __input);  
IF EXISTS (
    SELECT 1
    FROM people
    WHERE eeusername = @username
      AND password = @password) THEN
  INSERT INTO __output   
  SELECT membergroup, firstname, lastname, StoreCode
  FROM people
  WHERE eeUserName = @username
    AND Password = @password;
ELSE
  INSERT INTO __output
  SELECT 'Denied', '','', '' FROM system.iota;
END IF;     



END;