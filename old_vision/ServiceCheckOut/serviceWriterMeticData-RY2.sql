/*
DROP TABLE ServiceWriterMetricData;
CREATE TABLE ServiceWriterMetricData (
  eeUserName cichar(50),
  storeCode cichar(3),
  fullName cichar(40),
  lastName cichar(20),
  firstName cichar(20),
  writerNumber cichar(3),
  dateKey integer,
  theDate date,
  dayOfWeek integer, 
  dayName cichar(12),
  dayOfMonth integer,
  sundayToSaturdayWeek integer,
  metric cichar(45),
  seq integer,
  today numeric(12,2),
  thisWeek numeric(12,2),
  todayDisplay cichar(12),
  thisWeekDisplay cichar(12)) IN database;
  
one row per writer/metric/day
SELECT metric, eeusername, thedate
FROM ServiceWriterMetricData 
GROUP BY metric, eeusername, thedate
HAVING COUNT(*) > 1  
*/

/* first DO the non display/partial metrics
  rosClosed: final CLOSE, have been cashiered
  rosClosedWithInspectionLine
  ros with customer pay lines only: 
    rosCashiered  
    ros CashieredByWriter
    
4/15/13
  rosClosedWithInspectionLine: changed to 23I only    
  OPEN warranty calls: changed to 7 days back
** no backfilling of OPEN warranty calls **

5/13/13: generate the data for RY2
*/  
  

-- ROs Closed -----------------------------------------------------------------
-- backfill ------------------------
-- DELETE FROM ServiceWriterMetricData WHERE metric = 'ROs Closed';
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Closed', 0, COUNT(*) AS today,
  0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
-- SELECT *  
FROM dds.factro a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey -- cashiered ros only
  AND b.datetype = 'date'
  AND b.thedate BETWEEN '03/03/2013' AND curdate() -1
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber  
  AND c.storecode = 'RY2'
WHERE a.void = false     
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;    
-- ROs Closed -----------------------------------------------------------------  
-- nightly -----------------------------
DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() -1 AND metric = 'ROs Closed';
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Closed', 0, COUNT(*) AS today,
  0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
FROM dds.factro a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey -- cashiered ros only
  AND b.datetype = 'date'
  AND b.thedate = curdate() -1
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber  
  AND c.storecode = 'ry2'
WHERE a.void = false     
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;   
-- ROs Closed -----------------------------------------------------------------  
-- today -------------------
DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() AND metric = 'ROs Closed';
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Closed', 0, COUNT(*) AS today,
  0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
FROM factROToday a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey -- cashiered ros only
  AND b.datetype = 'date'
  AND b.thedate = curdate() 
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber  
  AND c.storecode = 'ry2'
WHERE a.void = false     
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;  
-- ROs Closed -----------------------------------------------------------------  
-- weekly -------------------
UPDATE ServiceWriterMetricData
  SET thisweek = x.thisweek             
FROM (   
  SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
    today + coalesce((
      SELECT SUM(today)
      FROM ServiceWriterMetricData b
      WHERE b.eeusername = a.eeusername
        AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
        AND b.metric = a.metric
        AND b.thedate < a.thedate),0) AS thisweek
  FROM ServiceWriterMetricData a
  WHERE metric = 'ROs Closed') x
WHERE ServiceWriterMetricData.eeusername = x.eeusername
  AND ServiceWriterMetricData.thedate = x.thedate
  AND ServiceWriterMetricData.metric = x.metric;  

UPDATE ServiceWriterMetricData
  SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
WHERE metric = 'ROs Closed';   

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- rosClosedWithInspectionLine ------------------------------------------------
-- backfill --------------------
--DELETE FROM ServiceWriterMetricData WHERE metric = 'rosClosedWithInspectionLine';
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithInspectionLine', 0, COUNT(*) AS today,
  0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
FROM dds.factro a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN '03/03/2013' AND curdate()
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber 
  AND c.storecode = 'ry2'
WHERE a.void = false 
  AND EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
    AND opcode = '24Z')  
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;

-- rosClosedWithInspectionLine ------------------------------------------------  
-- nightly ------------------------------
DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() -1 AND metric = 'rosClosedWithInspectionLine';  
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithInspectionLine', 0, COUNT(*) AS today,
  0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
FROM dds.factro a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate = curdate() - 1
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber 
  AND c.storecode = 'ry2'
WHERE a.void = false 
  AND EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
    AND opcode = '24Z')  
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;
-- rosClosedWithInspectionLine ------------------------------------------------ 
-- today --------------------------------
DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() AND metric = 'rosClosedWithInspectionLine';  
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithInspectionLine', 0, COUNT(*) AS today,
  0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
FROM factROToday a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate = curdate() 
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber 
  AND c.storecode = 'ry2'
WHERE a.void = false 
  AND EXISTS (
    SELECT 1
    FROM factROLineToday
    WHERE ro = a.ro
    AND opcode = '24Z')  
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;
 
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- rosCashiered ---------------------------------------------------------------
/* NOT reqd for ry2
-- backfill --------------------  
--DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashiered';
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'rosCashiered', 0 AS seq,
  COUNT(*) AS today,
  0 AS thisweek,
  TRIM(CAST(COUNT(*) AS sql_char)),
  CAST(NULL AS sql_char)
FROM dds.factro a
INNER JOIN 
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN '03/03/2013' AND curdate()
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber
  AND c.storecode = 'ry2'
WHERE EXISTS (
    SELECT 1
    FROM dds.stgArkonaGLPTRNS
    WHERE gtdoc# = a.ro)
  AND NOT EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype IN ('w','i'))
  AND NOT EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
      AND servicetype <> 'mr')      
  AND a.void = false 
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek; 
 
-- rosCashiered --------------------------------------------------------------- 
-- nightly -------------------- 
DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashiered' AND thedate = curdate() - 1;
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'rosCashiered', 0 AS seq,
  COUNT(*) AS today,
  0 AS thisweek,
  TRIM(CAST(COUNT(*) AS sql_char)),
  CAST(NULL AS sql_char)
FROM dds.factro a
INNER JOIN 
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate = curdate() - 1
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber
  AND c.storecode = 'ry2'
WHERE EXISTS (
    SELECT 1
    FROM dds.stgArkonaGLPTRNS
    WHERE gtdoc# = a.ro)
  AND NOT EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype IN ('w','i'))
  AND NOT EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
      AND servicetype <> 'mr')      
  AND a.void = false 
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek; 
   
-- rosCashiered ---------------------------------------------------------------
-- today -------------------- 
DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashiered' AND thedate = curdate();
INSERT INTO ServiceWriterMetricData         
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'rosCashiered', 0 AS seq,
  COUNT(*) AS today,
  0 AS thisweek,
  TRIM(CAST(COUNT(*) AS sql_char)),
  CAST(NULL AS sql_char)
FROM factROToday a
INNER JOIN 
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate = curdate() 
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber
  AND c.storecode = 'ry2'
WHERE EXISTS (
    SELECT 1
    FROM tmpGLPTRNS
    WHERE gtdoc# = a.ro)
  AND NOT EXISTS (
    SELECT 1 
    FROM factROLineToday
    WHERE ro = a.ro
      AND paytype IN ('w','i'))
  AND NOT EXISTS (
    SELECT 1
    FROM factROLineToday
    WHERE ro = a.ro
      AND servicetype <> 'mr')      
  AND a.void = false 
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek; 
 
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- rosCashieredByWriter -------------------------------------------------------
-- backfill --------------------  
--DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashieredByWriter';
INSERT INTO ServiceWriterMetricData        
SELECT a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
  a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek, 'rosCashieredByWriter', 0,
  SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS today,
  0,
  TRIM(CAST(SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS sql_char)),
  CAST(NULL AS sql_char)
FROM (  
  SELECT b.datekey, b.thedate, b.dayOfWeek, b.dayName,
    b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
    g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
    g.writerNumber, f.gquser
  FROM dds.factro a
  INNER JOIN 
    dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate BETWEEN '03/03/2013' AND curdate()
  INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# 
  INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
  INNER JOIN ServiceWriters g ON a.storecode = g.storecode
    AND a.writerid = g.writernumber
    AND g.storecode = 'ry2'
    AND NOT EXISTS (
      SELECT 1 
      FROM dds.factroline
      WHERE ro = a.ro
        AND paytype IN ('w','i'))
    AND NOT EXISTS (
      SELECT 1
      FROM dds.factroline
      WHERE ro = a.ro
        AND servicetype <> 'mr')      
    AND a.void = false 
  GROUP BY b.datekey, b.thedate, b.dayOfWeek, b.dayName,
    b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
    g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
    g.writerNumber, f.gquser) a
LEFT JOIN ServiceWriters b ON a.gquser = b.dtUserName   
GROUP BY a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
  a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek; 
  
-- rosCashieredByWriter -------------------------------------------------------  
-- nightly -------------------- 
DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashieredByWriter' AND thedate = curdate() - 1;
INSERT INTO ServiceWriterMetricData        
SELECT a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
  a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek, 'rosCashieredByWriter', 0,
  SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS today,
  0,
  TRIM(CAST(SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS sql_char)),
  CAST(NULL AS sql_char)
FROM (  
  SELECT b.datekey, b.thedate, b.dayOfWeek, b.dayName,
    b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
    g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
    g.writerNumber, f.gquser
  FROM dds.factro a
  INNER JOIN 
    dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate = curdate() - 1
  INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# 
  INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
  INNER JOIN ServiceWriters g ON a.storecode = g.storecode
    AND g.storecode = 'ry2'
    AND a.writerid = g.writernumber
    AND NOT EXISTS (
      SELECT 1 
      FROM dds.factroline
      WHERE ro = a.ro
        AND paytype IN ('w','i'))
    AND NOT EXISTS (
      SELECT 1
      FROM dds.factroline
      WHERE ro = a.ro
        AND servicetype <> 'mr')      
    AND a.void = false 
  GROUP BY b.datekey, b.thedate, b.dayOfWeek, b.dayName,
    b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
    g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
    g.writerNumber, f.gquser) a
LEFT JOIN ServiceWriters b ON a.gquser = b.dtUserName   
GROUP BY a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
  a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek; 
  
-- rosCashieredByWriter -------------------------------------------------------
-- today -------------------- 
DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashieredByWriter' AND thedate = curdate();
INSERT INTO ServiceWriterMetricData        
SELECT a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
  a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek, 'rosCashieredByWriter', 0,
  SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS today,
  0,
  TRIM(CAST(SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS sql_char)),
  CAST(NULL AS sql_char)
FROM (  
  SELECT b.datekey, b.thedate, b.dayOfWeek, b.dayName,
    b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
    g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
    g.writerNumber, f.gquser
  FROM factROToday a
  INNER JOIN 
    dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate = curdate()
  INNER JOIN tmpGLPTRNS e ON a.ro = e.gtdoc# 
  INNER JOIN tmpGLPDTIM f ON e.gttrn# = f.gqtrn#  
  INNER JOIN ServiceWriters g ON a.storecode = g.storecode
    AND a.writerid = g.writernumber
    AND g.storecode = 'ry2'
    AND NOT EXISTS (
      SELECT 1 
      FROM factROLineToday
      WHERE ro = a.ro
        AND paytype IN ('w','i'))
    AND NOT EXISTS (
      SELECT 1
      FROM factROLineToday 
      WHERE ro = a.ro
        AND servicetype <> 'mr')      
    AND a.void = false 
  GROUP BY b.datekey, b.thedate, b.dayOfWeek, b.dayName,
    b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
    g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
    g.writerNumber, f.gquser) a
LEFT JOIN ServiceWriters b ON a.gquser = b.dtUserName   
GROUP BY a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
  a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek; 
*/  
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- rosClosedWithFlagHours -----------------------------------------------------
-- backfill ------------------------
--DELETE FROM ServiceWriterMetricData WHERE metric = 'rosClosedWithFlagHours';
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithFlagHours', 0 AS seq, 
  howmany AS today, 0 AS thisweek, 
  trim(cast(howmany AS sql_char)), CAST(NULL AS sql_char) 
FROM (
  SELECT b.thedate, a.writerid, COUNT(*) AS howmany
  FROM dds.factro a
  INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate BETWEEN '03/03/2013' AND curdate()
  INNER JOIN ServiceWriters c ON a.storecode = c.storecode
    AND a.writerid = c.writernumber 
    AND c.storecode = 'ry2' 
  WHERE EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
    AND lineflaghours > 0)   
  GROUP BY b.thedate, a.writerid) a 
LEFT JOIN dds.day b ON a.thedate = b.thedate   
LEFT JOIN servicewriters c ON a.writerid = c.writernumber;
 
-- rosClosedWithFlagHours -----------------------------------------------------
-- nightly ------------------------
DELETE FROM ServiceWriterMetricData WHERE metric = 'rosClosedWithFlagHours' AND thedate = curdate()-1;
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithFlagHours', 0 AS seq, 
  howmany AS today, 0 AS thisweek, 
  trim(cast(howmany AS sql_char)), CAST(NULL AS sql_char) 
FROM (
  SELECT b.thedate, a.writerid, COUNT(*) AS howmany
  FROM dds.factro a
  INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate = curdate() -1
  INNER JOIN ServiceWriters c ON a.storecode = c.storecode
    AND a.writerid = c.writernumber  
    AND c.storecode = 'ry2'
  WHERE EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
    AND lineflaghours > 0)   
  GROUP BY b.thedate, a.writerid) a 
LEFT JOIN dds.day b ON a.thedate = b.thedate   
LEFT JOIN servicewriters c ON a.writerid = c.writernumber;

-- rosClosedWithFlagHours -----------------------------------------------------
-- today ------------------------
DELETE FROM ServiceWriterMetricData WHERE metric = 'rosClosedWithFlagHours' AND thedate = curdate();
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithFlagHours', 0 AS seq, 
  howmany AS today, 0 AS thisweek, 
  trim(cast(howmany AS sql_char)), CAST(NULL AS sql_char) 
FROM (
  SELECT b.thedate, a.writerid, COUNT(*) AS howmany
  FROM factROToday a
  INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate = curdate() 
  INNER JOIN ServiceWriters c ON a.storecode = c.storecode
    AND a.writerid = c.writernumber 
    AND c.storecode = 'ry2' 
  WHERE EXISTS (
    SELECT 1
    FROM factROLineToday
    WHERE ro = a.ro
    AND lineflaghours > 0)   
  GROUP BY b.thedate, a.writerid) a 
LEFT JOIN dds.day b ON a.thedate = b.thedate   
LEFT JOIN servicewriters c ON a.writerid = c.writernumber;

-- CREATE weekly running total
UPDATE ServiceWriterMetricData
  SET thisweek = x.thisweek             
FROM (   
  SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
    today + coalesce((
      SELECT SUM(today)
      FROM ServiceWriterMetricData b
      WHERE b.eeusername = a.eeusername
        AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
        AND b.metric = a.metric
        AND b.thedate < a.thedate),0) AS thisweek
  FROM ServiceWriterMetricData a
  WHERE a.metric = 'rosClosedWithFlagHours') x
WHERE ServiceWriterMetricData.eeusername = x.eeusername
  AND ServiceWriterMetricData.thedate = x.thedate
  AND ServiceWriterMetricData.metric = x.metric; 
  
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- flaghours ------------------------------------------------------------------
-- backfill ------------------------
--DELETE FROM ServiceWriterMetricData WHERE metric = 'flagHours';
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'flagHours', 0 AS seq, 
  flaghours AS today, 0 AS thisweek, 
  trim(cast(flaghours AS sql_char)), CAST(NULL AS sql_char) 
FROM (
  SELECT b.writerid, b.thedate, SUM(a.lineflaghours) AS flaghours
  FROM dds.factroline a
  INNER JOIN (
    SELECT *
    FROM dds.factro a
    INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate BETWEEN '03/03/2013' AND curdate()
    INNER JOIN servicewriters c ON a.storecode = c.storecode
      AND a.writerid = c.writernumber
      AND c.storecode = 'ry2'
    WHERE a.void = false) b ON a.ro = b.ro 
  GROUP BY b.writerid, b.thedate) a
LEFT JOIN dds.day b ON a.thedate = b.thedate   
LEFT JOIN servicewriters c ON a.writerid = c.writernumber;
 
-- flaghours ------------------------------------------------------------------
-- nightly ------------------------
DELETE FROM ServiceWriterMetricData WHERE metric = 'flagHours' AND thedate = curdate()-1;
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'flagHours', 0 AS seq, 
  flaghours AS today, 0 AS thisweek, 
  trim(cast(flaghours AS sql_char)), CAST(NULL AS sql_char) 
FROM (
  SELECT b.writerid, b.thedate, SUM(a.lineflaghours) AS flaghours
  FROM dds.factroline a
  INNER JOIN (
    SELECT *
    FROM dds.factro a
    INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate = curdate() -1
    INNER JOIN servicewriters c ON a.storecode = c.storecode
      AND a.writerid = c.writernumber
      AND c.storecode = 'ry2'
    WHERE a.void = false) b ON a.ro = b.ro 
  GROUP BY b.writerid, b.thedate) a
LEFT JOIN dds.day b ON a.thedate = b.thedate   
LEFT JOIN servicewriters c ON a.writerid = c.writernumber;

-- flaghours ------------------------------------------------------------------
-- today ------------------------
DELETE FROM ServiceWriterMetricData WHERE metric = 'flagHours' AND thedate = curdate();
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'flagHours', 0 AS seq, 
  flaghours AS today, 0 AS thisweek, 
  trim(cast(flaghours AS sql_char)), CAST(NULL AS sql_char) 
FROM (
  SELECT b.writerid, b.thedate, SUM(a.lineflaghours) AS flaghours
  FROM factROLineToday a
  INNER JOIN (
    SELECT *
    FROM factROToday a
    INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate = curdate() 
    INNER JOIN servicewriters c ON a.storecode = c.storecode
      AND a.writerid = c.writernumber
      AND c.storecode = 'ry2'
    WHERE a.void = false) b ON a.ro = b.ro 
  GROUP BY b.writerid, b.thedate) a
LEFT JOIN dds.day b ON a.thedate = b.thedate   
LEFT JOIN servicewriters c ON a.writerid = c.writernumber;

-- CREATE weekly running total
UPDATE ServiceWriterMetricData
  SET thisweek = x.thisweek             
FROM (   
  SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
    today + coalesce((
      SELECT SUM(today)
      FROM ServiceWriterMetricData b
      WHERE b.eeusername = a.eeusername
        AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
        AND b.metric = a.metric
        AND b.thedate < a.thedate),0) AS thisweek
  FROM ServiceWriterMetricData a
  WHERE a.metric = 'flagHours') x
WHERE ServiceWriterMetricData.eeusername = x.eeusername
  AND ServiceWriterMetricData.thedate = x.thedate
  AND ServiceWriterMetricData.metric = x.metric; 
 
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
/* 
  now the displayed metrics
*/  
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- Cashiering -----------------------------------------------------------------
/*
  requires ServiceWriterMetricData.rosCashieredByWriter & 
  ServiceWriterMetricData.rosCashiered to be complete
  generate weekly totals before generating display metric (cashiering)
*/
/* NOT reqd for ry2
UPDATE ServiceWriterMetricData
  SET thisweek = x.thisweek             
FROM (   
  SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
    today + coalesce((
      SELECT SUM(today)
      FROM ServiceWriterMetricData b
      WHERE b.eeusername = a.eeusername
        AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
        AND b.metric = a.metric
        AND b.thedate < a.thedate),0) AS thisweek
  FROM ServiceWriterMetricData a
  WHERE metric in ('rosCashiered','rosCashieredByWriter')) x
WHERE ServiceWriterMetricData.eeusername = x.eeusername
  AND ServiceWriterMetricData.thedate = x.thedate
  AND ServiceWriterMetricData.metric = x.metric;  

UPDATE ServiceWriterMetricData
  SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
WHERE metric IN ('rosCashiered','rosCashieredByWriter') ; 
   
DELETE FROM ServiceWriterMetricData WHERE metric = 'Cashiering';
INSERT INTO ServiceWriterMetricData
SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
  a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
  a.sundaytosaturdayweek, 'Cashiering', 1 AS seq, a.today, a.thisweek, 
  left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
  left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
FROM ServiceWriterMetricData a
INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
  AND a.thedate = b.thedate
WHERE a.metric = 'rosCashieredByWriter'
  AND b.metric = 'rosCashiered';
*/
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------  
-- Open Aged ROs --------------------------------------------------------------------
/*
UPDATE throughout the day IN terms of removing ros that get closed
DELETE FROM agedros daily based ON an ro closing
overnight a full scrap
AgedROs TABLE IS necessary for the nightly/today to run

no need to UPDATE with newly aged ros thru out the day

need to UPDATE the weekly numbers here
ALL i need are this week, which will be the same AS the daily total each day
*/

-- backfill ------------------------
--DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Aged ROs';
INSERT INTO ServiceWriterMetricData   
SELECT b.eeUserName, b.storecode, b.fullname, b.lastname, b.firstname,
  b.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Aged ROs', 2 AS seq, COUNT(*) AS today,
--  0 AS thisWeek, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
  COUNT(*) AS thisWeek, trim(CAST(COUNT(*) AS sql_char)) AS todayDisplay, 
  trim(CAST(COUNT(*) AS sql_char)) AS thisWeekDisplay
FROM (  
  SELECT *
  FROM dds.day a
  WHERE a.thedate BETWEEN '03/03/2013' AND curdate() - 1) a 
LEFT JOIN (
  SELECT b.thedate AS opendate, c.thedate AS closedate, a.*, d.*
  FROM dds.factro a
  LEFT JOIN dds.day b ON a.opendatekey = b.datekey
  LEFT JOIN dds.day c ON a.finalclosedatekey = c.datekey
  INNER JOIN ServiceWriters d ON a.storecode = d.storecode
    AND a.writerid = d.writernumber
    AND d.storecode = 'ry2'
  WHERE void = false
    AND b.thedate > '01/31/2012'
    AND a.storecode = 'RY2') b ON b.opendate < a.thedate - 7 AND b.closedate > a.thedate    
GROUP BY b.eeUserName, b.storecode, b.fullname, b.lastname, b.firstname,
  b.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek;
  
-- Open Aged ROs --------------------------------------------------------------
-- currently no st2 ros IN aged ros, so can't UPDATE via the TABLE agedros, so populate it first
INSERT INTO AgedROs 
SELECT f.eeUsername, a.ro, b.thedate, a.customername, 
  CASE d.ptrsts
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Process'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier/D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Error'
    WHEN 'P' THEN 'PI Rq'
    ELSE 'WTF'
  END AS status, 
  a.vin, 
  trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
INNER JOIN dds.day c ON a.finalclosedatekey = c.datekey
  AND c.datetype <> 'date'
INNER JOIN dds.stgArkonaPDPPHDR d ON a.ro = d.ptdoc# -- ? eliminate those NOT IN pdpphdr
LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
INNER JOIN ServiceWriters f ON a.storecode = f.storecode
  AND a.writerid = f.writernumber
  AND f.storecode = 'ry2'
  AND void = false
  AND curdate() - b.thedate > 7; 

-- nightly ------------------------  
DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Aged ROs' AND thedate = curdate() -1 ;
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Aged ROs', 2 AS seq, b.howmany AS today,
--  0 AS thisWeek, trim(CAST(b.howmany AS sql_char)), CAST(NULL AS sql_char)
  howmany thisWeek, trim(CAST(howmany AS sql_char)) AS todayDisplay, 
  trim(CAST(howmany AS sql_char)) AS thisWeekDisplay
FROM dds.day a, ( 
  SELECT eeusername, COUNT(*) AS howmany
  FROM agedros  
  GROUP BY eeusername) b, 
  servicewriters c
WHERE a.thedate = curdate() - 1  
  AND b.eeusername = c.eeusername
  AND c.storecode = 'ry2';
  
-- Open Aged ROs --------------------------------------------------------------
-- today ------------------------
DELETE 
FROM agedros 
WHERE EXISTS (
  SELECT 1
  FROM factROToday
  WHERE ro = agedros.ro
  AND finalclosedatekey <> 7306);
  
DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Aged ROs' AND thedate = curdate();
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Aged ROs', 2 AS seq, b.howmany AS today,
--  0 AS thisWeek, trim(CAST(b.howmany AS sql_char)), CAST(NULL AS sql_char)
  howmany thisWeek, trim(CAST(howmany AS sql_char)) AS todayDisplay, 
  trim(CAST(howmany AS sql_char)) AS thisWeekDisplay
FROM dds.day a, ( 
  SELECT eeusername, COUNT(*) AS howmany
  FROM agedros  
  GROUP BY eeusername) b, 
  servicewriters c
WHERE a.thedate = curdate()
  AND b.eeusername = c.eeusername;  
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- Open Warranty Calls --------------------------------------------------------
/*
  the challenges here, like AgedROs, the data depends ON dds.factro, 
    sco.factrotoday & sco.WarrantyROs
    backfill FROM dds.factro
4/13 changed the date range to curdate()-7  

oh shit, forget backfilling, history does NOT matter
what matters IS what IS ON the plate today  
because we have no history of whether the call was complete OR NOT !!
  
*/
-- backfill ------------------------
-- so this becomes only for today onward
-- no backfilling
-- the backfill consists of the first day the data IS loaded
-- so, no backfilling here, the original backfill code IS IN warranty ros.sql
/*
DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Warranty Calls';
INSERT INTO ServiceWriterMetricData        
SELECT b.eeUserName, b.storecode, b.fullname, b.lastname, b.firstname,
  b.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Warranty Calls', 3 AS seq, 
  COUNT(*) AS today, COUNT(*) AS thisWeek,
  trim(CAST(COUNT(*) AS sql_char)) AS todayDisplay, 
  trim(CAST(COUNT(*) AS sql_char)) AS thisWeekDisplay
FROM (  
  SELECT *
  FROM dds.day a
  WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
LEFT JOIN (    
  SELECT *  
  FROM dds.factro a
  INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate < curdate() - 7 --BETWEEN curdate() - 7 AND curdate()    
  INNER JOIN ServiceWriters f ON a.storecode = f.storecode 
    AND a.writerid = f.writernumber
    AND EXISTS (
      SELECT 1 
      FROM dds.factroline
      WHERE ro = a.ro
        AND paytype = 'w')
    AND a.void = false) b ON b.thedate <= a.thedate
WHERE b.eeusername IS NOT null    
GROUP BY b.eeUserName, b.storecode, b.fullname, b.lastname, b.firstname,
  b.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
  a.dayOfMonth, a.sundayToSaturdayWeek;  
*/

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- Avg Hours/RO ---------------------------------------------------------------
/*
  requires ServiceWriterMetricData.rosClosedWithFlagHours & ServiceWriterMetricData.flagHours to be complete
  different than cashiering AND insp requested, those weekly numbers are based ON discreet
  values that get updated at the END, a simple concantenation of the discreet values
  this IS a ratio
*/
-- backfill ------------------------
DELETE FROM ServiceWriterMetricData WHERE metric = 'Avg Hours/RO';
INSERT INTO ServiceWriterMetricData
SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
  a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
  a.sundaytosaturdayweek, 'Avg Hours/RO', 4 AS seq, 
  round(a.today/b.today, 1) AS today,
  round(a.thisweek/b.thisweek, 1) AS thisweek,  
  left(CAST(cast(round(a.today/b.today, 1) as sql_double) AS sql_char), 12)  AS todaydisplay,
  left(cast(CAST(round(a.thisweek/b.thisweek, 1) AS sql_double) AS sql_char), 12) AS thisweekdisplay
FROM ServiceWriterMetricData a
INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
  AND a.thedate = b.thedate
WHERE a.metric = 'flagHours'
  AND b.metric = 'rosClosedWithFlagHours';
  

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- Labor Sales -----------------------------------------------------------------
-- backfill ------------------------
--DELETE FROM ServiceWriterMetricData WHERE metric = 'Labor Sales';
INSERT INTO ServiceWriterMetricData        
SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
  d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
  round(cast(abs(sum(a.laborsales)) AS sql_double), 0) AS today,
  0, 
  trim(cast(coalesce(abs(round(CAST(sum(a.laborsales) AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
  CAST(NULL AS sql_char)
FROM dds.factro a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN '03/03/2013' AND curdate()
INNER JOIN ServiceWriters d ON a.storecode = d.storecode
  AND a.writerid = d.writernumber  
  AND d.storecode = 'ry2' 
WHERE a.void = false  
GROUP BY d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
  d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek; 

-- LaborSales -----------------------------------------------------------------
-- nightly ------------------------
DELETE FROM ServiceWriterMetricData WHERE metric = 'Labor Sales' AND thedate = curdate()-1;
INSERT INTO ServiceWriterMetricData        
SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
  d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
  round(cast(abs(sum(a.laborsales)) AS sql_double), 0) AS today,
  0, 
  trim(cast(coalesce(abs(round(CAST(sum(a.laborsales) AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
  CAST(NULL AS sql_char)
FROM dds.factro a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate = curdate() - 1
INNER JOIN ServiceWriters d ON a.storecode = d.storecode
  AND a.writerid = d.writernumber 
  AND d.storecode = 'ry2'   
WHERE a.void = false  
GROUP BY d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
  d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek; 

-- LaborSales -----------------------------------------------------------------
-- today ------------------------
DELETE FROM ServiceWriterMetricData WHERE metric = 'Labor Sales' AND thedate = curdate();
INSERT INTO ServiceWriterMetricData        
SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
  d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
  round(cast(abs(sum(a.laborsales)) AS sql_double), 0) AS today,
  0, 
  trim(cast(coalesce(abs(round(CAST(sum(a.laborsales) AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
  CAST(NULL AS sql_char)
FROM factROToday a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate = curdate()
INNER JOIN ServiceWriters d ON a.storecode = d.storecode
  AND a.writerid = d.writernumber  
  AND d.storecode = 'ry2'  
WHERE a.void = false  
GROUP BY d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
  d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- ros Opened ------------------------------------------------------------------
-- backfill ------------------------
--DELETE FROM ServiceWriterMetricData WHERE metric = 'ROs Opened';
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Opened', 6 AS seq, COUNT(*) AS today,
  0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
  AND b.datetype = 'date'
  AND b.thedate BETWEEN '03/03/2013' AND curdate()
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber 
  AND c.storecode = 'ry2'
WHERE a.void = false
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;
-- ros Opened ------------------------------------------------------------------
-- nightly ------------------------  

DELETE FROM ServiceWriterMetricData WHERE metric = 'ROs Opened' AND thedate = curdate() -1 ;
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Opened', 6 AS seq, COUNT(*) AS today,
  0 AS thisWeek, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
FROM dds.factro a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
  AND b.datetype = 'date'
  AND b.thedate = curdate() -1
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber 
  AND c.storecode = 'ry2'
WHERE a.void = false
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;
-- ros Opened ------------------------------------------------------------------  
-- today ------------------------

DELETE FROM ServiceWriterMetricData WHERE metric = 'ROs Opened' AND thedate = curdate();
INSERT INTO ServiceWriterMetricData        
SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Opened', 6 AS seq, COUNT(*) AS today,
  0 AS thisWeek, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
FROM factROToday a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
  AND b.datetype = 'date'
  AND b.thedate = curdate()
INNER JOIN ServiceWriters c ON a.storecode = c.storecode
  AND a.writerid = c.writernumber 
  AND c.storecode = 'ry2'
WHERE a.void = false
GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
  c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek;
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
  
-- Inspections Requested ------------------------------------------------------
/*
  requires ServiceWriterMetricData.ROs Closed & 
  ServiceWriterMetricData.rosClosedWithInspectionLine to be complete
  generate weekly totals before generating display metric (cashiering)
*/

UPDATE ServiceWriterMetricData
  SET thisweek = x.thisweek             
FROM (   
  SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
    today + coalesce((
      SELECT SUM(today)
      FROM ServiceWriterMetricData b
      WHERE b.eeusername = a.eeusername
        AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
        AND b.metric = a.metric
        AND b.thedate < a.thedate),0) AS thisweek
  FROM ServiceWriterMetricData a
  WHERE metric = 'rosClosedWithInspectionLine') x
WHERE ServiceWriterMetricData.eeusername = x.eeusername
  AND ServiceWriterMetricData.thedate = x.thedate
  AND ServiceWriterMetricData.metric = x.metric;  

UPDATE ServiceWriterMetricData
  SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
WHERE metric = 'rosClosedWithInspectionLine'; 


DELETE FROM ServiceWriterMetricData WHERE metric = 'Inspections Requested';
INSERT INTO ServiceWriterMetricData
SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
  a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
  a.sundaytosaturdayweek, 'Inspections Requested', 7 AS seq, a.today, a.thisweek, 
  left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
  left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
FROM ServiceWriterMetricData a
INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
  AND a.thedate = b.thedate
WHERE a.metric = 'rosClosedWithInspectionLine'
  AND b.metric = 'ROs Closed';
    
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- CSI ------------------------------------------------------------------------
-- backfill ------------------------
/*
DELETE FROM servicewritermetricdata WHERE metric = 'CSI';
INSERT INTO servicewritermetricdata
SELECT a.eeusername, b.storecode, b.fullname, b.lastname, b.firstname, 
  b.writernumber, c.datekey, c.thedate, c.dayofweek, c.dayname, 
  c.dayofmonth, c.sundaytosaturdayweek, 'CSI', 8 AS seq, 
  a.csi AS today, a.csi AS thisWeek,
  left(CAST(a.csi AS sql_char), 12) AS todayDisplay,
  left(CAST(a.csi AS sql_char), 12) AS thisWeekDisplay 
FROM servicewritercheckouts a
LEFT JOIN servicewriters b ON a.eeusername = b.eeusername
LEFT JOIN dds.day c ON a.thedate = c.thedate;
*/
-- CSI ------------------------------------------------------------------------
-- nightly ------------------------

-- CSI ------------------------------------------------------------------------
-- today ------------------------

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

-- Walk Arounds ---------------------------------------------------------------
-- backfill ------------------------
/*
DELETE FROM servicewritermetricdata WHERE metric = 'Walk Arounds';
INSERT INTO servicewritermetricdata
SELECT a.eeusername, b.storecode, b.fullname, b.lastname, b.firstname, 
  b.writernumber, c.datekey, c.thedate, c.dayofweek, c.dayname, 
  c.dayofmonth, c.sundaytosaturdayweek, 'Walk Arounds', 9 AS seq, 
  a.walkarounds AS today, a.walkarounds AS thisWeeek,
  left(CAST(a.walkarounds AS sql_char), 12) AS todayDisplay,
  left(CAST(a.walkarounds AS sql_char), 12) AS thisWeekDisplay 
FROM servicewritercheckouts a
LEFT JOIN servicewriters b ON a.eeusername = b.eeusername
LEFT JOIN dds.day c ON a.thedate = c.thedate;
*/
-- Walk Arounds ---------------------------------------------------------------
-- nightly ------------------------

-- Walk Arounds ---------------------------------------------------------------
-- today ------------------------

------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------
-- THEN UPDATE weekly (running totals)
-- ok to DO the whole fucking tables for now  

-- UPDATE cumulative metrics only with weekly totals
UPDATE ServiceWriterMetricData
  SET thisweek = x.thisweek             
FROM (   
  SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
    today + coalesce((
      SELECT SUM(today)
      FROM ServiceWriterMetricData b
      WHERE b.eeusername = a.eeusername
        AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
        AND b.metric = a.metric
        AND b.thedate < a.thedate),0) AS thisweek
  FROM ServiceWriterMetricData a
  WHERE EXISTS (
    SELECT 1
    FROM ServiceWriterMetrics
    WHERE metric = a.metric
      AND cumulative = true)) x
WHERE ServiceWriterMetricData.eeusername = x.eeusername
  AND ServiceWriterMetricData.thedate = x.thedate
  AND ServiceWriterMetricData.metric = x.metric;  

UPDATE ServiceWriterMetricData
  SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
WHERE metric IN (
  SELECT metric
  FROM servicewritermetrics
  WHERE cumulative = true); 
  

    