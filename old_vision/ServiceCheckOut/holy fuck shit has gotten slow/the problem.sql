on 11/20/15 @ 10:30 am, proc went FROM 7 minutes to 30 minutes

2/1/16 it IS taking so long that it does NOT finish real time updating
AND the writers are starting to complain
IN addition, the whole sco app IS mind numbingly slow

i know that much of the sql for sco IS "NOT optimized", but DO NOT 
really want to refactor it now, ALL of this shit IS going to be 
moved to ember/postgres AND that IS WHEN it will be rethought AND refactored

SELECT a.*, timestampdiff(sql_tsi_second, startts, endts)/60.0
FROM zproclog a
WHERE executable = 'scoToday'
  AND proc = 'swMetricDataToday'
  AND CAST(startts AS sql_date) BETWEEN curdate() - 3 AND curdate()
  BETWEEN '11/10/2015' AND '11/30/2015'

Tried this out on zztest, seemed to WORK ok,
reduce the size of ServiceWriterMetricData dramatically = dramatic permformance improvement

CREATE TABLE ServiceWriterMetricData_archived ( 
      userName CIChar( 50 ),
      storeCode CIChar( 3 ),
      fullName CIChar( 40 ),
      lastName CIChar( 20 ),
      firstName CIChar( 20 ),
      writerNumber CIChar( 3 ),
      dateKey Integer,
      theDate Date,
      dayOfWeek Integer,
      dayName CIChar( 12 ),
      dayOfMonth Integer,
      sundayToSaturdayWeek Integer,
      metric CIChar( 45 ),
      seq Integer,
      today Numeric( 12 ,2 ),
      thisWeek Numeric( 12 ,2 ),
      todayDisplay CIChar( 12 ),
      thisWeekDisplay CIChar( 12 )) IN DATABASE;

SELECT yearmonth, COUNT(*)
FROM servicewritermetricdata a
INNER JOIN dds.day b on a.thedate = b.thedate
GROUP BY yearmonth


INSERT INTO ServiceWriterMetricData_archived
SELECT * 
FROM servicewritermetricdata
WHERE thedate < '12/01/2015'

DELETE 
FROM servicewritermetricdata
WHERE thedate < '12/01/2015'


SELECT *
FROM servicewritermetricdata
ORDER BY thedate DESC 

EXECUTE PROCEDURE swmetricdatatoday()  -- now takes 6 seconds


select MIN(a.thedate)
FROM servicewritermetricdata a

INSERT INTO ServiceWriterMetricData_archived
SELECT * 
FROM servicewritermetricdata
WHERE thedate < '10/01/2016';

DELETE 
FROM servicewritermetricdata
WHERE thedate < '10/01/2016';