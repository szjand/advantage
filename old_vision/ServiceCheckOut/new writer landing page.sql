DROP TABLE WriterLandingPage;
CREATE TABLE WriterLandingPage ( 
      WeekID Integer,
      WriterEEMemberID Integer,
      Name CIChar( 40 ),
      WriterNumber CIChar( 3 ),
      Metric CIChar( 40 ),
      Sun cichar(12),
      Mon cichar(12),
      Tue cichar(12),
      Wed cichar(12),
      Thu cichar(12),
      Fri cichar(12),
      Sat cichar(12),
      Total cichar(12),
      eeUserName CIChar( 50 ),
      seq Integer) IN DATABASE;

DROP procedure GetWriterLandingPage;    
CREATE PROCEDURE GetWriterLandingPage
   ( 
      eeUserName CICHAR ( 50 ),
      Metric CICHAR ( 40 ) OUTPUT,
      Sun cichar(12) OUTPUT,
      Mon cichar(12) OUTPUT,
      Tue cichar(12) OUTPUT,
      Wed cichar(12) OUTPUT,
      Thu cichar(12) OUTPUT,
      Fri cichar(12) OUTPUT,
      Sat cichar(12) OUTPUT,
      Total cichar(12) OUTPUT,
      seq Integer OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetWriterLandingPage('rodt@rydellchev.com');

need to pivot the data to match the landing page FROM the functional spec:
              sun  mon  tue  thr  fri   sat
Cashiering     3    4    6    4    8     8
Aged OPEN Ros  5    7    0    7    5     7
...
        

*/
  DECLARE @id string;
  @id = (SELECT eeUserName FROM __input);
  INSERT INTO __output  
SELECT metric, Sun,Mon,Tue,Wed,Thu,Fri,Sat,Total, seq
FROM writerlandingpage a
WHERE  weekid = (
    SELECT sundaytosaturdayweek
    FROM dds.day
    WHERE thedate = curdate())
  AND eeUserName = @id;





END;
