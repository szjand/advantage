CREATE TABLE WriterLandingPage ( 
      WeekID Integer,
      WriterEEMemberID Integer,
      Name CIChar( 40 ),
      WriterNumber CIChar( 3 ),
      Metric CIChar( 40 ),
      Sun Double( 15 ),
      Mon Double( 15 ),
      Tue Double( 15 ),
      Wed Double( 15 ),
      Thu Double( 15 ),
      Fri Double( 15 ),
      Sat Double( 15 ),
      Total Integer,
      eeUserName CIChar( 50 ),
      seq Integer) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'WriterLandingPage', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'WriterLandingPagefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'WriterLandingPage', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'WriterLandingPagefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'WriterLandingPage', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'WriterLandingPagefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'WriterLandingPage', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'WriterLandingPagefail');
   
INSERT INTO "WriterLandingPage" VALUES( 100, 9, 'Rodney Troftgruben', '714', 'Cashiering', 0, 12, 13, 14, 15, 15, 0, 14, 'rodt@rydellchev.com', 1 );
INSERT INTO "WriterLandingPage" VALUES( 100, 9, 'Rodney Troftgruben', '714', 'Open Aged ROs', 12, 12, 13, 14, 15, 15, 0, 14, 'rodt@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 100, 9, 'Rodney Troftgruben', '714', 'Open Warranty Calls', 12, 12, 13, 14, 15, 15, 0, 14, 'rodt@rydellchev.com', 3 );
INSERT INTO "WriterLandingPage" VALUES( 100, 9, 'Rodney Troftgruben', '714', 'Avg Hours/RO', 0, 12, 13, 14, 15, 15, 0, 14, 'rodt@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 100, 9, 'Rodney Troftgruben', '714', 'Labor Sales', 0, 12, 13, 14, 15, 15, 0, 14, 'rodt@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 100, 9, 'Rodney Troftgruben', '714', 'RO''s Opened', 0, 12, 13, 14, 15, 15, 0, 14, 'rodt@rydellchev.com', 6 );
INSERT INTO "WriterLandingPage" VALUES( 100, 9, 'Rodney Troftgruben', '714', 'Inspections Requested', 0, 12, 13, 14, 15, 15, 0, 14, 'rodt@rydellchev.com', 7 );
INSERT INTO "WriterLandingPage" VALUES( 100, 9, 'Rodney Troftgruben', '714', 'CSI', 0, 12, 13, 14, 15, 0, 0, 14, 'rodt@rydellchev.com', 8 );
INSERT INTO "WriterLandingPage" VALUES( 100, 9, 'Rodney Troftgruben', '714', 'Walk Arounds', 0, 12, 13, 14, 15, 0, 0, 14, 'rodt@rydellchev.com', 9 );
INSERT INTO "WriterLandingPage" VALUES( 100, 10, 'Travis Bursinger', '720', 'Cashiering', 0, 12, 13, 14, 15, 15, 0, 14, 'tbursinger@rydellchev.com', 1 );
INSERT INTO "WriterLandingPage" VALUES( 100, 10, 'Travis Bursinger', '720', 'Open Aged ROs', 12, 12, 13, 14, 15, 15, 0, 14, 'tbursinger@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 100, 10, 'Travis Bursinger', '720', 'Open Warranty Calls', 12, 12, 13, 14, 15, 15, 0, 14, 'tbursinger@rydellchev.com', 3 );
INSERT INTO "WriterLandingPage" VALUES( 100, 10, 'Travis Bursinger', '720', 'Avg Hours/RO', 0, 12, 13, 14, 15, 15, 0, 14, 'tbursinger@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 100, 10, 'Travis Bursinger', '720', 'Labor Sales', 0, 12, 13, 14, 15, 15, 0, 14, 'tbursinger@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 100, 10, 'Travis Bursinger', '720', 'RO''s Opened', 0, 12, 13, 14, 15, 15, 0, 14, 'tbursinger@rydellchev.com', 6 );
INSERT INTO "WriterLandingPage" VALUES( 100, 10, 'Travis Bursinger', '720', 'Inspections Requested', 0, 12, 13, 14, 15, 15, 0, 14, 'tbursinger@rydellchev.com', 7 );
INSERT INTO "WriterLandingPage" VALUES( 100, 10, 'Travis Bursinger', '720', 'CSI', 0, 12, 13, 14, 15, 0, 0, 14, 'tbursinger@rydellchev.com', 8 );
INSERT INTO "WriterLandingPage" VALUES( 100, 10, 'Travis Bursinger', '720', 'Walk Arounds', 0, 12, 13, 14, 15, 0, 0, 14, 'tbursinger@rydellchev.com', 9 );
INSERT INTO "WriterLandingPage" VALUES( 100, 11, 'John Tyrrell', '715', 'Cashiering', 0, 12, 13, 14, 15, 15, 0, 14, 'jtyrrell@rydellchev.com', 1 );
INSERT INTO "WriterLandingPage" VALUES( 100, 11, 'John Tyrrell', '715', 'Open Aged ROs', 12, 12, 13, 14, 15, 15, 0, 14, 'jtyrrell@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 100, 11, 'John Tyrrell', '715', 'Open Warranty Calls', 12, 12, 13, 14, 15, 15, 0, 14, 'jtyrrell@rydellchev.com', 3 );
INSERT INTO "WriterLandingPage" VALUES( 100, 11, 'John Tyrrell', '715', 'Avg Hours/RO', 0, 12, 13, 14, 15, 15, 0, 14, 'jtyrrell@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 100, 11, 'John Tyrrell', '715', 'Labor Sales', 0, 12, 13, 14, 15, 15, 0, 14, 'jtyrrell@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 100, 11, 'John Tyrrell', '715', 'RO''s Opened', 0, 12, 13, 14, 15, 15, 0, 14, 'jtyrrell@rydellchev.com', 6 );
INSERT INTO "WriterLandingPage" VALUES( 100, 11, 'John Tyrrell', '715', 'Inspections Requested', 0, 12, 13, 14, 15, 15, 0, 14, 'jtyrrell@rydellchev.com', 7 );
INSERT INTO "WriterLandingPage" VALUES( 100, 11, 'John Tyrrell', '715', 'CSI', 0, 12, 13, 14, 15, 0, 0, 14, 'jtyrrell@rydellchev.com', 8 );
INSERT INTO "WriterLandingPage" VALUES( 100, 11, 'John Tyrrell', '715', 'Walk Arounds', 0, 12, 13, 14, 15, 0, 0, 14, 'jtyrrell@rydellchev.com', 9 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Travis Bursinger', '720', 'Labor Sales', 0, 798, 798, 0, 0, 0, 0, NULL, 'tbursinger@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Rodney Troftgruben', '714', 'Labor Sales', 1423, 3597, 3597, 0, 0, 0, 0, NULL, 'rodt@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'John Tyrrell', '705', 'Labor Sales', 0, 412, 412, 0, 0, 0, 0, NULL, 'jtyrrell@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Ken Carlson', '645', 'Labor Sales', 0, 79, 79, 0, 0, 0, 0, NULL, 'jkl', 5 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Daniel Wiebusch', '428', 'Labor Sales', 185, 185, 185, 0, 0, 0, 0, NULL, 'def', 5 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Kenneth Espelund', '402', 'Labor Sales', 509, 1084, 1084, 0, 0, 0, 0, NULL, 'abc', 5 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Travis Bursinger', '720', 'Labor Sales', 0, 3530, 6188, 7504, 12356, 28185, 28927, NULL, 'tbursinger@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Rodney Troftgruben', '714', 'Labor Sales', 0, 2700, 5469, 6978, 10671, 17892, 21437, NULL, 'rodt@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'John Tyrrell', '705', 'Labor Sales', 0, 563, 1357, 1866, 2827, 3513, 3513, NULL, 'jtyrrell@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Ken Carlson', '645', 'Labor Sales', 0, 169, 823, 1406, 1600, 1739, 1739, NULL, 'jkl', 5 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Matt Flikka', '429', 'Labor Sales', 0, 1189, 2376, 3167, 3236, 4022, 4074, NULL, 'ghi', 5 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Daniel Wiebusch', '428', 'Labor Sales', 0, 183, 1737, 1797, 2556, 4537, 4537, NULL, 'def', 5 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Kenneth Espelund', '402', 'Labor Sales', 0, 1482, 3889, 5496, 11518, 21219, 21273, NULL, 'abc', 5 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Travis Bursinger', '720', 'Labor Sales', 21, 1550, 3935, 5339, 7220, 12187, 12830, NULL, 'tbursinger@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Rodney Troftgruben', '714', 'Labor Sales', 0, 261, 1797, 5738, 8152, 11227, 13411, NULL, 'rodt@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'John Tyrrell', '705', 'Labor Sales', 0, 39, 683, 1643, 2583, 3870, 3900, NULL, 'jtyrrell@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Ken Carlson', '645', 'Labor Sales', 0, 0, 107, 235, 341, 404, 404, NULL, 'jkl', 5 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Matt Flikka', '429', 'Labor Sales', 0, 610, 1275, 2237, 3476, 3724, 3724, NULL, 'ghi', 5 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Daniel Wiebusch', '428', 'Labor Sales', 0, 0, 486, 647, 1653, 2927, 2927, NULL, 'def', 5 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Kenneth Espelund', '402', 'Labor Sales', 0, 572, 4136, 4675, 6894, 8953, 8953, NULL, 'abc', 5 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Travis Bursinger', '720', 'Labor Sales', 0, 1092, 2370, 8005, 9593, 11101, 13528, NULL, 'tbursinger@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Rodney Troftgruben', '714', 'Labor Sales', 107, 956, 2763, 5492, 8888, 10302, 11832, NULL, 'rodt@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'John Tyrrell', '705', 'Labor Sales', 0, 337, 838, 1248, 2265, 2280, 2334, NULL, 'jtyrrell@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Ken Carlson', '645', 'Labor Sales', 0, 21, 21, 57, 107, 107, 279, NULL, 'jkl', 5 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Matt Flikka', '429', 'Labor Sales', 0, 0, 46, 418, 542, 1112, 1789, NULL, 'ghi', 5 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Daniel Wiebusch', '428', 'Labor Sales', 0, 59, 228, 228, 228, 228, 228, NULL, 'def', 5 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Kenneth Espelund', '402', 'Labor Sales', 500, 2173, 9592, 16185, 20086, 23097, 25172, NULL, 'abc', 5 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Travis Bursinger', '720', 'Labor Sales', 0, 416, 1964, 3096, 4157, 5493, 5493, NULL, 'tbursinger@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Rodney Troftgruben', '714', 'Labor Sales', 3162, 4107, 6070, 7963, 12216, 14433, 16182, NULL, 'rodt@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'John Tyrrell', '705', 'Labor Sales', 0, 333, 730, 1822, 2612, 2753, 2753, NULL, 'jtyrrell@rydellchev.com', 5 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Ken Carlson', '645', 'Labor Sales', 0, 74, 160, 268, 276, 276, 276, NULL, 'jkl', 5 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Matt Flikka', '429', 'Labor Sales', 0, 350, 404, 849, 1870, 3724, 4475, NULL, 'ghi', 5 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Daniel Wiebusch', '428', 'Labor Sales', 0, 32, 354, 461, 807, 848, 848, NULL, 'def', 5 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Kenneth Espelund', '402', 'Labor Sales', 0, 2407, 5382, 8619, 11861, 13933, 14790, NULL, 'abc', 5 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Travis Bursinger', '720', 'Avg Hours/RO', 0, 2, 2, 0, 0, 0, 0, NULL, 'tbursinger@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Rodney Troftgruben', '714', 'Avg Hours/RO', 1.9, 2.6, 2.6, 0, 0, 0, 0, NULL, 'rodt@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'John Tyrrell', '705', 'Avg Hours/RO', 0, 1, 1, 0, 0, 0, 0, NULL, 'jtyrrell@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Ken Carlson', '645', 'Avg Hours/RO', 0, 0.6, 0.6, 0, 0, 0, 0, NULL, 'jkl', 4 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Daniel Wiebusch', '428', 'Avg Hours/RO', 1.8, 1.8, 1.8, 0, 0, 0, 0, NULL, 'def', 4 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Kenneth Espelund', '402', 'Avg Hours/RO', 5.3, 3.6, 3.6, 0, 0, 0, 0, NULL, 'abc', 4 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Travis Bursinger', '720', 'Avg Hours/RO', 0, 2.9, 2.3, 1.9, 2.4, 3.1, 3, NULL, 'tbursinger@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Rodney Troftgruben', '714', 'Avg Hours/RO', 0, 2.7, 2.6, 2.5, 2.3, 2.5, 2.3, NULL, 'rodt@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'John Tyrrell', '705', 'Avg Hours/RO', 0, 1.7, 1.6, 1.5, 1.6, 1.5, 1.5, NULL, 'jtyrrell@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Ken Carlson', '645', 'Avg Hours/RO', 0, 0.8, 1, 1.4, 1.3, 1.3, 1.3, NULL, 'jkl', 4 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Matt Flikka', '429', 'Avg Hours/RO', 0, 1.9, 2.5, 2.9, 2.5, 2.3, 2.2, NULL, 'ghi', 4 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Daniel Wiebusch', '428', 'Avg Hours/RO', 0, 1.1, 2.2, 2.1, 1.9, 2.1, 2.1, NULL, 'def', 4 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Kenneth Espelund', '402', 'Avg Hours/RO', 0, 3.2, 2.2, 2.6, 2.6, 2.9, 2.9, NULL, 'abc', 4 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Travis Bursinger', '720', 'Avg Hours/RO', 0.2, 2.1, 2, 1.8, 1.8, 2.1, 2.2, NULL, 'tbursinger@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Rodney Troftgruben', '714', 'Avg Hours/RO', 0, 0.9, 2.6, 3.5, 2.8, 3, 2.8, NULL, 'rodt@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'John Tyrrell', '705', 'Avg Hours/RO', 0, 0.3, 1, 1.3, 1.4, 1.5, 1.5, NULL, 'jtyrrell@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Ken Carlson', '645', 'Avg Hours/RO', 0, 0, 1, 1.7, 1, 1, 1, NULL, 'jkl', 4 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Matt Flikka', '429', 'Avg Hours/RO', 0, 5.7, 3.2, 2.7, 2.5, 2.1, 2.1, NULL, 'ghi', 4 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Daniel Wiebusch', '428', 'Avg Hours/RO', 0, 0, 1.3, 1.3, 1.8, 2.3, 2.3, NULL, 'def', 4 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Kenneth Espelund', '402', 'Avg Hours/RO', 0, 1.1, 2.6, 2.6, 2.4, 2.1, 2.1, NULL, 'abc', 4 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Travis Bursinger', '720', 'Avg Hours/RO', 0, 2.2, 2.2, 3.8, 2.9, 2.6, 2.6, NULL, 'tbursinger@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Rodney Troftgruben', '714', 'Avg Hours/RO', 1, 2.4, 2.4, 2.4, 3, 2.8, 2.9, NULL, 'rodt@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'John Tyrrell', '705', 'Avg Hours/RO', 0, 1.3, 1.6, 1.4, 1.6, 1.6, 1.6, NULL, 'jtyrrell@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Ken Carlson', '645', 'Avg Hours/RO', 0, 0.2, 0.2, 0.2, 0.4, 0.4, 0.9, NULL, 'jkl', 4 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Matt Flikka', '429', 'Avg Hours/RO', 0, 0, 0.8, 1, 1, 1.7, 2.1, NULL, 'ghi', 4 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Daniel Wiebusch', '428', 'Avg Hours/RO', 0, 1.5, 1.3, 1.3, 1.3, 1.3, 1.3, NULL, 'def', 4 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Kenneth Espelund', '402', 'Avg Hours/RO', 1.7, 2.8, 5.2, 4.6, 4.3, 4, 4.1, NULL, 'abc', 4 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Travis Bursinger', '720', 'Avg Hours/RO', 0, 0.9, 1.3, 1.5, 1.8, 1.9, 1.9, NULL, 'tbursinger@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Rodney Troftgruben', '714', 'Avg Hours/RO', 15.4, 5.1, 3.4, 3, 3.2, 3.1, 3.2, NULL, 'rodt@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'John Tyrrell', '705', 'Avg Hours/RO', 0, 1, 1, 1.5, 1.3, 1.2, 1.2, NULL, 'jtyrrell@rydellchev.com', 4 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Ken Carlson', '645', 'Avg Hours/RO', 0, 0.4, 0.5, 0.7, 0.7, 0.7, 0.7, NULL, 'jkl', 4 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Matt Flikka', '429', 'Avg Hours/RO', 0, 1.7, 1.3, 1.3, 2, 2.6, 2.8, NULL, 'ghi', 4 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Daniel Wiebusch', '428', 'Avg Hours/RO', 0, 0.3, 0.8, 0.8, 1.2, 1.2, 1.2, NULL, 'def', 4 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Kenneth Espelund', '402', 'Avg Hours/RO', 0, 1.8, 2.2, 2.4, 2.6, 2.5, 2.3, NULL, 'abc', 4 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Travis Bursinger', '720', 'Open Aged ROs', 8, 7, 8, 0, 0, 0, 0, NULL, 'tbursinger@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Rodney Troftgruben', '714', 'Open Aged ROs', 21, 20, 21, 0, 0, 0, 0, NULL, 'rodt@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Matt Flikka', '429', 'Open Aged ROs', 2, 3, 4, 0, 0, 0, 0, NULL, 'ghi', 2 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Kenneth Espelund', '402', 'Open Aged ROs', 20, 20, 20, 0, 0, 0, 0, NULL, 'abc', 2 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Ken Carlson', '645', 'Open Aged ROs', 1, 1, 1, 0, 0, 0, 0, NULL, 'jkl', 2 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'John Tyrrell', '705', 'Open Aged ROs', 7, 5, 6, 0, 0, 0, 0, NULL, 'jtyrrell@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 483, 0, 'Daniel Wiebusch', '428', 'Open Aged ROs', 9, 9, 9, 0, 0, 0, 0, NULL, 'def', 2 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Travis Bursinger', '720', 'Open Aged ROs', 28, 27, 24, 28, 27, 8, 8, NULL, 'tbursinger@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Rodney Troftgruben', '714', 'Open Aged ROs', 31, 32, 29, 28, 31, 26, 23, NULL, 'rodt@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Matt Flikka', '429', 'Open Aged ROs', 3, 3, 3, 3, 4, 2, 2, NULL, 'ghi', 2 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Kenneth Espelund', '402', 'Open Aged ROs', 23, 23, 24, 29, 27, 19, 20, NULL, 'abc', 2 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Ken Carlson', '645', 'Open Aged ROs', 3, 3, NULL, NULL, NULL, 1, 1, NULL, 'jkl', 2 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'John Tyrrell', '705', 'Open Aged ROs', 5, 5, 5, 6, 6, 6, 7, NULL, 'jtyrrell@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 482, 0, 'Daniel Wiebusch', '428', 'Open Aged ROs', 6, 6, 8, 12, 14, 10, 10, NULL, 'def', 2 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Travis Bursinger', '720', 'Open Aged ROs', 20, 21, 22, 23, 26, 25, 26, NULL, 'tbursinger@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Rodney Troftgruben', '714', 'Open Aged ROs', 33, 33, 38, 36, 32, 31, 31, NULL, 'rodt@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Matt Flikka', '429', 'Open Aged ROs', 2, 2, 2, 2, 2, 2, 3, NULL, 'ghi', 2 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Kenneth Espelund', '402', 'Open Aged ROs', 24, 24, 22, 23, 24, 22, 22, NULL, 'abc', 2 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Ken Carlson', '645', 'Open Aged ROs', 2, 2, 3, 2, 2, 3, 3, NULL, 'jkl', 2 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'John Tyrrell', '705', 'Open Aged ROs', 5, 5, 5, 5, 5, 5, 5, NULL, 'jtyrrell@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 481, 0, 'Daniel Wiebusch', '428', 'Open Aged ROs', 5, 5, 6, 6, 5, 5, 6, NULL, 'def', 2 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Travis Bursinger', '720', 'Open Aged ROs', 15, 15, 18, 21, 18, 18, 20, NULL, 'tbursinger@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Rodney Troftgruben', '714', 'Open Aged ROs', 30, 29, 30, 27, 28, 31, 33, NULL, 'rodt@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Matt Flikka', '429', 'Open Aged ROs', NULL, NULL, 1, 1, 2, 2, 2, NULL, 'ghi', 2 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Kenneth Espelund', '402', 'Open Aged ROs', 17, 17, 22, 20, 20, 19, 20, NULL, 'abc', 2 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Ken Carlson', '645', 'Open Aged ROs', 2, 2, 2, 2, 2, 2, 2, NULL, 'jkl', 2 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'John Tyrrell', '705', 'Open Aged ROs', 5, 5, 5, 5, 5, 5, 5, NULL, 'jtyrrell@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 480, 0, 'Daniel Wiebusch', '428', 'Open Aged ROs', 4, 3, 3, 4, 5, 5, 5, NULL, 'def', 2 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Travis Bursinger', '720', 'Open Aged ROs', 10, 10, 11, 11, 12, 13, 15, NULL, 'tbursinger@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Rodney Troftgruben', '714', 'Open Aged ROs', 22, 22, 24, 27, 26, 28, 30, NULL, 'rodt@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Matt Flikka', '429', 'Open Aged ROs', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, 'ghi', 2 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Kenneth Espelund', '402', 'Open Aged ROs', 9, 9, 9, 10, 11, 13, 12, NULL, 'abc', 2 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Ken Carlson', '645', 'Open Aged ROs', 2, 2, 2, 2, 2, 2, 2, NULL, 'jkl', 2 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'John Tyrrell', '705', 'Open Aged ROs', 6, 6, 6, 5, 5, 5, 5, NULL, 'jtyrrell@rydellchev.com', 2 );
INSERT INTO "WriterLandingPage" VALUES( 479, 0, 'Daniel Wiebusch', '428', 'Open Aged ROs', 2, 2, 3, 3, 3, 3, 4, NULL, 'def', 2 );
   
   
CREATE PROCEDURE GetWriterLandingPage
   ( 
      eeUserName CICHAR ( 50 ),
      Metric CICHAR ( 40 ) OUTPUT,
      Sun DOUBLE ( 15 ) OUTPUT,
      Mon DOUBLE ( 15 ) OUTPUT,
      Tue DOUBLE ( 15 ) OUTPUT,
      Wed DOUBLE ( 15 ) OUTPUT,
      Thu DOUBLE ( 15 ) OUTPUT,
      Fri DOUBLE ( 15 ) OUTPUT,
      Sat DOUBLE ( 15 ) OUTPUT,
      Total DOUBLE ( 15 ) OUTPUT,
      seq Integer OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetWriterLandingPage('rodt@rydellchev.com');

need to pivot the data to match the landing page FROM the functional spec:
              sun  mon  tue  thr  fri   sat
Cashiering     3    4    6    4    8     8
Aged OPEN Ros  5    7    0    7    5     7
...
        

*/
  DECLARE @id string;
  @id = (SELECT eeUserName FROM __input);
  INSERT INTO __output  
SELECT metric, Sun,Mon,Tue,Wed,Thu,Fri,Sat,Total, seq
FROM writerlandingpage a
WHERE  weekid = (
    SELECT sundaytosaturdayweek
    FROM dds.day
    WHERE thedate = curdate())
  AND eeUserName = @id;





END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'GetWriterLandingPage', 
   'COMMENT', 
   '');

   

