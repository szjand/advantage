SELECT b.eeusername, a.sundaytosaturdayweek, a.thedate, a.dayname, c.metric, c.seq 
FROM dds.day a,
  servicewriters b,
  servicewritermetrics c
WHERE thedate BETWEEN curdate() - 14 AND curdate () 
  AND b.eeusername = 'rodt@rydellchev.com'
  
SELECT b.eeusername, a.sundaytosaturdayweek, c.metric,
  '' as sunday, '' AS monday, '' AS tuesday, '' AS wednesday, '' AS thursday, 
  '' AS friday, '' AS saturday
FROM dds.day a,
  servicewriters b,
  servicewritermetrics c
WHERE thedate BETWEEN curdate() - 14 AND curdate () 
  AND b.eeusername = 'rodt@rydellchev.com'  
GROUP BY b.eeusername, a.sundaytosaturdayweek, c.metric, c.seq  
ORDER BY b.eeusername, a.sundaytosaturdayweek, c.seq 
  

  
SELECT b.eeusername, a.sundaytosaturdayweek, a.thedate, a.dayname, c.metric, c.seq 
FROM dds.day a,
  servicewriters b,
  servicewritermetrics c
WHERE thedate BETWEEN curdate() - 14 AND curdate () 

  AND b.eeusername = 'rodt@rydellchev.com'
  
    
  
INSERT INTO ServiceWriterMetrics values ('Cashiering', 1);
INSERT INTO ServiceWriterMetrics values ('Open Aged ROs', 2); 
INSERT INTO ServiceWriterMetrics values ('Open Warranty Calls', 3); 
INSERT INTO ServiceWriterMetrics values ('Avg Hours/RO', 4); 
INSERT INTO ServiceWriterMetrics values ('Labor Sales', 5); 
INSERT INTO ServiceWriterMetrics values ('RO''s Opened', 6); 
INSERT INTO ServiceWriterMetrics values ('Inspections Requested', 7); 



SELECT eeusername, a.thedate,
  CASE a.dayname
    WHEN 'sunday' THEN 
      CASE metric
        WHEN 'Cashiering' THEN 51
        WHEN 'Open Aged ROs' THEN 51
        WHEN 'Open Warranty Calls' THEN 51
        WHEN 'Avg Hours/RO' THEN 51
        WHEN 'Labor Sales' THEN 51
        WHEN 'RO''s Opened' THEN 51
        WHEN 'Inspections Requested' THEN 12
      END 
    end
FROM dds.day a,
  servicewriters b,
  servicewritermetrics c
WHERE thedate BETWEEN curdate() - 14 AND curdate () 
  AND b.eeusername = 'rodt@rydellchev.com'

  
DROP TABLE ServiceWriterStats;  
CREATE TABLE ServiceWriterStats (
  eeusername cichar(50),
  thedate date,
  Cashiering double, 
  AgedROs integer, 
  WarrantyCalls integer,
  AvgHours double,
  LaborSales integer,
  ROsOpened integer,
  InspectionsRequested integer) IN database;  
  
SELECT *
FROM ServiceWriterStats;
DELETE FROM ServiceWriterStats;
INSERT INTO ServiceWriterStats values (
  'rodt@rydellchev.com', curdate(), 45, 13,15,1.8, 15123, 14, 10);
INSERT INTO ServiceWriterStats values (
  'tbursinger@rydellchev.com', curdate(), 13, 28,12,2.1, 14190, 17, 13); 
INSERT INTO ServiceWriterStats values (
  'jtyrrell@rydellchev.com', curdate(), 62, 10,1,1.1, 10014, 16, 12);   

DROP PROCEDURE GetCheckOutStats;  
CREATE PROCEDURE GetCheckOutStats (
  eeUserName cichar(50),
  Cashiering double output,
  AgedROs integer output,
  WarrantyCalls integer output,
  AvgHoursPerRO double output,
  LaborSales double output,
  ROsOpened integer output,
  InspectionsRequested integer output)
  
BEGIN
/*
EXECUTE procedure GetCheckOutStats ('jtyrrell@rydellchev.com');
*/
DECLARE @id string;
@id = (SELECT eeUserName FROM __input);
INSERT INTO __output
SELECT Cashiering, AgedROs, WarrantyCalls, AvgHours, LAborSales, ROsOpened, InspectionsRequested 
FROM ServiceWriterStats
WHERE eeUserName = @id
  AND thedate = curdate();

END;