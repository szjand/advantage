SELECT *
FROM ( -- daily
  SELECT a.sundaytosaturdayweek, 0, a.fullname, a.writernumber, a.metric, 
    max(CASE WHEN a.dayname = 'Sunday' THEN todayDisplay END) AS Sun,
    max(CASE WHEN a.dayname = 'Monday' THEN todayDisplay END) AS Mon,
    max(CASE WHEN a.dayname = 'Tuesday' THEN todayDisplay END) AS Tue,
    max(CASE WHEN a.dayname = 'Wednesday' THEN todayDisplay END) AS Wed,
    max(CASE WHEN a.dayname = 'Thursday' THEN todayDisplay END) AS Thu,
    max(CASE WHEN a.dayname = 'Friday' THEN todayDisplay END) AS Fri,
    max(CASE WHEN a.dayname = 'Saturday' THEN todayDisplay END) AS Sat,
    eeusername, seq
  FROM (
    SELECT a.*,
      b.todaydisplay,
      b.thisweekdisplay, b.*
    FROM ( -- 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL with seq)
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.eeusername, 
        b.fullname, b.writernumber, c.metric, c.seq
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = curdate())
        AND c.seq IS NOT NULL) a
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric) a 
  GROUP BY a.sundaytosaturdayweek, a.fullname, a.writernumber, a.metric, eeusername, seq) d
LEFT JOIN ( -- this appears to be working for seq 1,4-7 to get weekly
  SELECT a.eeusername, a.metric, a.SundayToSaturdayWeek, a.thisweekdisplay
  FROM servicewritermetricdata a
  INNER JOIN servicewritermetrics c ON a.metric = c.metric
    AND c.seq IS NOT NULL 
  INNER JOIN (
      SELECT MAX(thedate) AS thedate, eeusername, metric, SundayToSaturdayWeek
      FROM servicewritermetricdata
      WHERE thisweekdisplay IS NOT NULL 
      GROUP BY eeusername, metric, SundayToSaturdayWeek) b ON a.thedate = b.thedate
        and a.eeusername = b.eeusername
        and a.metric = b.metric
        and a.SundayToSaturdayWeek = b.SundayToSaturdayWeek) e ON d.eeusername = e.eeusername
    AND d.metric = e.metric
    AND d.SundayToSaturdayWeek = e.SundayToSaturdayWeek 
    
    
    
    
  SELECT a.eeusername, a.metric, a.SundayToSaturdayWeek, a.thisweekdisplay
  FROM servicewritermetricdata a
  INNER JOIN servicewritermetrics c ON a.metric = c.metric
    AND c.seq IS NOT NULL 
  INNER JOIN (
      SELECT MAX(thedate) AS thedate, eeusername, metric, SundayToSaturdayWeek
      FROM servicewritermetricdata
      WHERE thisweekdisplay IS NOT NULL 
      GROUP BY eeusername, metric, SundayToSaturdayWeek) b ON a.thedate = b.thedate
        and a.eeusername = b.eeusername
        and a.metric = b.metric
        and a.SundayToSaturdayWeek = b.SundayToSaturdayWeek    
        
        
SELECT metric, dayname, SUM(today) AS today, SUM(thisWeek) AS thisWeek
FROM servicewritermetricdata
WHERE SundayToSaturdayWeek = 484
GROUP BY metric, dayname, dayofweek
ORDER BY metric, dayofweek


-- base list of day/metric
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, c.metric, c.seq
      FROM dds.day a, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = curdate())
        AND c.seq IS NOT NULL
        
cashiering = rosCashieredByWriter '/' rosCashiered
Avg Hours/RO = flagHours/rosClosedWithFlagHours
Inspections Requested = rosClosedWithInspection '/' rosClosed


ALL the others are additive 


-- base list of day/metric
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, c.metric, c.seq
      FROM dds.day a, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = curdate())
        AND c.metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
          'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
          'rosClosedWithInspection','rosClosed','Walk Arounds')
          
SELECT * 
FROM (  
  SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, c.metric, c.seq
  FROM dds.day a, servicewritermetrics c
  WHERE a.SundayToSaturdayWeek  = (
    SELECT SundayToSaturdayWeek 
    FROM dds.day
    WHERE thedate = curdate())
    AND c.metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
      'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
      'rosClosedWithInspection','rosClosed','Walk Arounds')) a            
left JOIN ( 
  SELECT SundayToSaturdayWeek, thedate, metric, SUM(today) AS today, SUM(thisWeek) AS thisweek
  FROM servicewritermetricdata 
  WHERE metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
      'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
      'rosClosedWithInspection','rosClosed','Walk Arounds')      
  GROUP BY SundayToSaturdayWeek, thedate, metric) b ON a.thedate = b.thedate   
AND a.metric = b.metric 
ORDER BY a.thedate, a.metric   
  
 
SELECT *
FROM servicewritermetricdata

DROP TABLE deptMetricData;
CREATE TABLE deptMetricData (
  SundayToSaturdayWeek integer,
  thedate date, 
  dayname cichar(12),
  metric cichar(45),
  today numeric(12,2),
  thisWeek numeric(12,2)) IN database;
INSERT INTO deptMetricData
SELECT SundayToSaturdayWeek, thedate, dayname, metric, 
  SUM(coalesce(today, 0)) AS today,  SUM(coalesce(thisWeek,0)) AS thisweek
FROM servicewritermetricdata 
WHERE metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
    'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
    'rosClosedWithInspection','rosClosed','Walk Arounds')      
GROUP BY SundayToSaturdayWeek, thedate, dayname, metric;  

select * from deptMetricData
SELECT fullname, thedate, today, thisweek FROM serviceWriterMetricData WHERE metric = 'ros Opened' AND SundayToSaturdayWeek = 484 ORDER BY fullname, thedate

-- so why IS this week showing wrong ON the current day IN this query 
SELECT * FROM deptMetricData WHERE metric = 'ros Opened' AND SundayToSaturdayWeek = 484
the only one with OPEN ros ON wednesday IS ken (1), today shows 1 thisWeek shows 28, should be 114
showing ken espelunds values, NOT ALL shops

so the problem seems to be the wrong thisweek value, ros opened for today only

because there are no thisweek entries for ALL but one
the problem IS with adding thisweek
AS opposed to simply summing today
which IS exactly why this IS NOT fucked up, but trying to look at serviceWriterMetricData with out
the OUTER JOIN FROM day/metric IS

one JOIN for the daily
another for the total
-- daily
SELECT d.*, e.thisweek
FROM (
  SELECT a.sundaytosaturdayweek, a.metric, 
    max(CASE WHEN a.dayname = 'Sunday' THEN today END) AS Sun,
    max(CASE WHEN a.dayname = 'Monday' THEN today END) AS Mon,
    max(CASE WHEN a.dayname = 'Tuesday' THEN today END) AS Tue,
    max(CASE WHEN a.dayname = 'Wednesday' THEN today END) AS Wed,
    max(CASE WHEN a.dayname = 'Thursday' THEN today END) AS Thu,
    max(CASE WHEN a.dayname = 'Friday' THEN today END) AS Fri,
    max(CASE WHEN a.dayname = 'Saturday' THEN today END) AS Sat,
    SUM(coalesce(today,0)) AS total,
    seq 
  FROM ( 
    SELECT a.*, b.today
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, c.metric, c.seq
      FROM dds.day a, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = curdate())
        AND c.metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
          'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
          'rosClosedWithInspection','rosClosed','Walk Arounds')) a  
    LEFT JOIN ( -- dept level 
      SELECT SundayToSaturdayWeek, thedate, dayname, metric, 
        SUM(coalesce(today, 0)) AS today
      FROM servicewritermetricdata 
      WHERE metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
          'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
          'rosClosedWithInspection','rosClosed','Walk Arounds')      
      GROUP BY SundayToSaturdayWeek, thedate, dayname, metric) b ON a.thedate = b.thedate
      AND a.metric = b.metric) a 
  GROUP BY a.sundaytosaturdayweek, a.metric, seq) d  
LEFT JOIN ( --wtd
  SELECT a.metric, a.SundayToSaturdayWeek, sum(a.thisweek) AS thisweek
  FROM servicewritermetricdata a
  INNER JOIN servicewritermetrics c ON a.metric = c.metric
    AND c.seq IS NOT NULL 
  INNER JOIN (
      SELECT MAX(thedate) AS thedate, eeusername, metric, SundayToSaturdayWeek
      FROM servicewritermetricdata
      WHERE thisweekdisplay IS NOT NULL 
      GROUP BY eeusername, metric, SundayToSaturdayWeek) b ON a.thedate = b.thedate
        and a.eeusername = b.eeusername
        and a.metric = b.metric
        and a.SundayToSaturdayWeek = b.SundayToSaturdayWeek
  GROUP BY a.metric, a.SundayToSaturdayWeek) e ON d.metric = e.metric
    AND d.SundayToSaturdayWeek = e.SundayToSaturdayWeek 


cashiering = rosCashieredByWriter '/' rosCashiered
Avg Hours/RO = flagHours/rosClosedWithFlagHours
Inspections Requested = rosClosedWithInspection '/' rosClosed    

SELECT x.*, y.*
FROM (    
  SELECT a.SundayToSaturdayWeek, b.metric, b.seq
  FROM dds.day a, serviceWriterMetrics b
  WHERE b.seq IS NOT NULL 
    AND a.SundayToSaturdayWeek = (
    SELECT SundayToSaturdayWeek
    FROM dds.day
    WHERE thedate = curdate())  
  GROUP BY a.SundayToSaturdayWeek, b.metric, b.seq) x -- just need 1 row/week  
LEFT JOIN ( -- y
  SELECT d.*, e.thisweek
  FROM ( -- d one row/raw metric
    SELECT a.sundaytosaturdayweek, a.metric, 
      max(CASE WHEN a.dayname = 'Sunday' THEN today END) AS Sun,
      max(CASE WHEN a.dayname = 'Monday' THEN today END) AS Mon,
      max(CASE WHEN a.dayname = 'Tuesday' THEN today END) AS Tue,
      max(CASE WHEN a.dayname = 'Wednesday' THEN today END) AS Wed,
      max(CASE WHEN a.dayname = 'Thursday' THEN today END) AS Thu,
      max(CASE WHEN a.dayname = 'Friday' THEN today END) AS Fri,
      max(CASE WHEN a.dayname = 'Saturday' THEN today END) AS Sat,
--      SUM(coalesce(today,0)) AS total,
      seq 
    FROM ( 
      SELECT a.*, b.today
      FROM ( -- a one row per date/raw metric for a SundayToSaturdayWeek  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, c.metric, c.seq
        FROM dds.day a, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = curdate())) a  
          AND c.metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
            'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
            'rosClosedWithInspection','rosClosed','Walk Arounds', 'Open Warranty Calls')
      LEFT JOIN ( -- b - dept level - 1 row per day/raw metric(actual data) 
        SELECT SundayToSaturdayWeek, thedate, dayname, metric, 
          SUM(coalesce(today, 0)) AS today
        FROM servicewritermetricdata 
        WHERE metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
            'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
            'rosClosedWithInspection','rosClosed','Walk Arounds', 'Open Warranty Calls')      
        GROUP BY SundayToSaturdayWeek, thedate, dayname, metric) b ON a.thedate = b.thedate
        AND a.metric = b.metric) a 
    GROUP BY a.sundaytosaturdayweek, a.metric, seq) d  
  LEFT JOIN ( --wtd, the deal here IS the most recent non NULL entry
    SELECT a.metric, a.SundayToSaturdayWeek, sum(a.thisweek) AS thisweek
    FROM servicewritermetricdata a
    INNER JOIN servicewritermetrics c ON a.metric = c.metric
--      AND c.seq IS NOT NULL 
      AND a.metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
            'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
            'rosClosedWithInspection','rosClosed','Walk Arounds', 'Open Warranty Calls')
    inner JOIN (
        SELECT MAX(thedate) AS thedate, eeusername, metric, SundayToSaturdayWeek
        FROM servicewritermetricdata
        WHERE thisweekdisplay IS NOT NULL 
        GROUP BY eeusername, metric, SundayToSaturdayWeek) b ON a.thedate = b.thedate
--          and a.eeusername = b.eeusername
          and a.metric = b.metric
          and a.SundayToSaturdayWeek = b.SundayToSaturdayWeek
    GROUP BY a.metric, a.SundayToSaturdayWeek) e ON d.metric = e.metric
      AND d.SundayToSaturdayWeek = e.SundayToSaturdayWeek) y ON x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  AND x.metric = y.metric  
ORDER BY x.seq  



--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------
-- d one row/raw metric with data for each day
SELECT a.metric
FROM serviceWriterMetrics a
WHERE seq IS NOT NULL 

SELECT x.*,
  CASE x.metric
    WHEN 'Open Aged ROS' THEN y.thisweek
    WHEN 'Open Warranty Calls' THEN y.thisweek
    ELSE coalesce(sun,0)+coalesce(mon,0)+coalesce(tue,0)+coalesce(wed,0)+coalesce(thu,0)+coalesce(fri,0)+coalesce(sat,0)
  END AS thisweek
FROM ( -- x 
  SELECT a.sundaytosaturdayweek, a.metric, 
    max(CASE WHEN a.dayname = 'Sunday' THEN today END) AS Sun,
    max(CASE WHEN a.dayname = 'Monday' THEN today END) AS Mon,
    max(CASE WHEN a.dayname = 'Tuesday' THEN today END) AS Tue,
    max(CASE WHEN a.dayname = 'Wednesday' THEN today END) AS Wed,
    max(CASE WHEN a.dayname = 'Thursday' THEN today END) AS Thu,
    max(CASE WHEN a.dayname = 'Friday' THEN today END) AS Fri,
    max(CASE WHEN a.dayname = 'Saturday' THEN today END) AS Sat,
    seq
  FROM ( -- a
    SELECT a.*, b.today
    FROM ( -- a one row per date/raw metric for a SundayToSaturdayWeek  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, c.metric, c.seq
      FROM dds.day a, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = curdate())) a  
--        AND c.metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
--          'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
--          'rosClosedWithInspection','rosClosed','Walk Arounds', 'Open Warranty Calls')) a  
    LEFT JOIN ( -- b - dept level - 1 row per day/raw metric(actual data) 
      SELECT SundayToSaturdayWeek, thedate, dayname, metric, 
        SUM(coalesce(today, 0)) AS today
      FROM servicewritermetricdata 
      WHERE metric IN ('rosCashieredByWriter','rosCashiered','Open Aged ROs',
          'flagHours','rosClosedWithFlagHours','Labor Sales','ROs Opened',
          'rosClosedWithInspection','rosClosed','Walk Arounds', 'Open Warranty Calls')      
      GROUP BY SundayToSaturdayWeek, thedate, dayname, metric) b ON a.thedate = b.thedate
      AND a.metric = b.metric) a 
  GROUP BY a.sundaytosaturdayweek, a.metric, seq) x
LEFT JOIN (
  SELECT a.SundayToSaturdayWeek, a.metric, a.thisWeek
  FROM (    
    SELECT SundayToSaturdayWeek, metric, thedate, sum(today) AS thisWeek
    FROM serviceWriterMetricData 
    WHERE metric in ('Open Aged ROS', 'Open Warranty Calls') 
    GROUP BY SundayToSaturdayWeek,  metric, thedate) a
  INNER JOIN (
    SELECT SundayToSaturdayWeek, metric, max(thedate) AS theDate
    FROM serviceWriterMetricData 
    WHERE metric in ('Open Aged ROS', 'Open Warranty Calls') 
    GROUP BY SundayToSaturdayWeek,  metric) b ON a.SundayToSaturdayWeek = b.SundayToSaturdayWeek
      AND a.metric = b.metric
      AND a.thedate = b.thedate ) y ON x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 
  AND x.metric = y.metric 