IF huot IS logged IN
LEFT nav bar consists of
     Service Stats
             Aged ROs
             Warranty ROs
      Net Promoter
              Scores
              Surveys
      Apps BY Employees
      
rod IS logged in                                                
LEFT nav bar consists of
     Service Stats
             Aged ROs
             Warranty ROs
      Net Promoter

so the widget consists of Headers AND Details AS determined BY the role ALL 
mushed INTO a memo field

userid:  mhuot@rydellchev.com
widget:  sidebar-widgets
header:  ServiceStats
detail:  Aged ROs

TABLE widgets
widget: sidebar-widget
label:  Service Stats
link:   ../servicestats/index.php
seq:    1

CREATE TABLE widgets (
  widget cichar(50),
  menu-item  cichar(50),
  link cichar(100),
  seq integer) IN database;
  
INSERT INTO widgets values('sidebar-widget','Service Stats','../servicestats/index.php',1);  
      
DROP TABLE widgets2;      
CREATE TABLE widgets2 (
  eeUsername cichar(50),
  snippet memo, 
  seq integer) IN database;
  
CREATE TABLE snippets (
  storecode cichar(3),
  membergroup cichar(24),
  snippet memo,
  seq integer) IN database;  
INSERT INTO snippets (storecode, membergroup, snippet, seq)
values ('RY1','ServiceManager',
  '<!-- Service Stats --><nav class="menu-item"><a class="mainlink" href="../servicestats/index.php"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>' +
  '<ul class="submenu"><li><span class="sublink">View Aged ROs</span></li>' +
  '<li><span class="sublink">View Warranty Calls</span></li></ul>' +
  '</nav><!-- .menu-item -->', 1); 
INSERT INTO snippets (storecode, membergroup, snippet, seq)
values ('RY1','ServiceManager',
  '<!-- Net Promoter --><nav class="menu-item">' +
  '<a class="mainlink" href="../netpromoter/index.php"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>' +
  '<ul class="submenu">'+
  '<li><span class="sublink">View Score</span></li>' +
  '<li><span class="sublink">View Surveys</span></li>' +
  '</ul>' +
  '</nav><!-- .menu-item -->', 2); 
INSERT INTO snippets (storecode, membergroup, snippet, seq)
values ('RY1','ServiceManager',
  '<!-- Employees --><nav class="menu-item">' +
  '<a class="mainlink employees" href="../main/employees.php">' +
  '<span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span>' +
  '</a>]</nav><!-- .menu-item -->', 3);    
INSERT INTO snippets (storecode, membergroup, snippet, seq)
values ('RY1','ServiceWriter',
  '<!-- Service Stats --><nav class="menu-item"><a class="mainlink" href="../servicestats/index.php"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>' +
  '<ul class="submenu"><li><span class="sublink">View Aged ROs</span></li>' +
  '<li><span class="sublink">View Warranty Calls</span></li></ul>' +
  '</nav><!-- .menu-item -->', 1);    
INSERT INTO snippets (storecode, membergroup, snippet, seq)
values ('RY1','ServiceWriter',
  '<!-- Net Promoter --><nav class="menu-item">' +
  '<a class="mainlink" href="../netpromoter/index.php"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>' +
  '</nav><!-- .menu-item -->', 2);   
  
INSERT INTO snippets (storecode, membergroup, snippet, seq)
values ('RY2','ServiceManager',
  '<!-- Service Stats --><nav class="menu-item"><a class="mainlink" href="../servicestats/index.php"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>' +
  '<ul class="submenu"><li><span class="sublink">View Aged ROs</span></li>' +
  '<li><span class="sublink">View Nissan Follow Up Calls</span></li></ul>' +
  '</nav><!-- .menu-item -->', 1);   
INSERT INTO snippets (storecode, membergroup, snippet, seq)
values ('RY2','ServiceManager',
  '<!-- Employees --><nav class="menu-item">' +
  '<a class="mainlink employees" href="../main/employees.php">' +
  '<span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span>' +
  '</a>]</nav><!-- .menu-item -->', 2);    
  
INSERT INTO snippets (storecode, membergroup, snippet, seq)
values ('RY2','ServiceWriter',
  '<!-- Service Stats --><nav class="menu-item"><a class="mainlink" href="../servicestats/index.php"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>' +
  '<ul class="submenu"><li><span class="sublink">View Aged ROs</span></li>' +
  '</ul>' +
  '</nav><!-- .menu-item -->', 1);      
   
 
DELETE FROM snippets WHERE storecode = 'RY2'

ALTER TABLE people 
ADD COLUMN StoreCode cichar(3);
UPDATE people 
SET storecode = 'RY1'
WHERE eeusername IN (
  SELECT eeusername
  FROM people
  WHERE eeusername LIKE '%@rydell%');
UPDATE people 
SET storecode = 'RY2'
WHERE eeusername IN (
  SELECT eeusername
  FROM people
  WHERE eeusername LIKE '%@gfhonda%');  
  
DROP PROCEDURE GetLeftNavBar; 
alter PROCEDURE GetLeftNavBar (
  eeusername cichar(50),
  snippet memo output)
begin  
/*
EXECUTE PROCEDURE getleftnavbar('mhuot@rydellchev.com');
EXECUTE PROCEDURE getleftnavbar('rodt@rydellchev.com');
EXECUTE PROCEDURE getleftnavbar('aneumann@gfhonda.com');
EXECUTE PROCEDURE getleftnavbar('dpederson@gfhonda.com');

*/
DECLARE @user cichar(50);
@user = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT top 100 snippet
FROM snippets a
INNER JOIN people b on a.membergroup = b.membergroup
  AND a.storecode = b.storecode 
WHERE b.eeUsername = @user
ORDER BY a.seq;
END;
