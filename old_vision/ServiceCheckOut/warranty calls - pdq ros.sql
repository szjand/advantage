Warranty Oil Changes DO NOT generate surveys
2 bad surveys, both supposedly regarding the alignment
19147678 line 5, warranty align
19147437 line 2 (recall) was warranty, line 1 was align - customer pay

per ben, exclude 13A (air filters), ROT (ALL rotates), nitro, 23& (inspections)

SELECT *
FROM dds.factrepairorder a
INNER JOIN dds.dimservicewriter b on a.servicewriterkey = b.servicewriterkey
  AND b.censusdept = 'QL' -- written BY a pdq writer
INNER JOIN dds.dimPaymentType c on a.paymenttypekey = c.paymenttypekey
  AND c.paymenttypecode = 'W' -- warranty pay type
INNER JOIN dds.dimopcode d on a.opcodekey = d.opcodekey
  AND pdqcat1 <> 'lof' -- exclude oil changes  
WHERE a.opendatekey IN (SELECT datekey FROM dds.day WHERE thedate > '08/31/2013')
  AND a.storecode = 'ry1'
  AND a.finalclosedatekey <> (SELECT datekey FROM dds.day WHERE datetype = 'NA')
ORDER BY a.ro



SELECT d.opcode, left(d.description, 50), left(cast(e.complaint AS sql_char),50), 
  COUNT(*), MIN(a.ro), MAX(a.ro)
FROM dds.factrepairorder a
INNER JOIN dds.dimservicewriter b on a.servicewriterkey = b.servicewriterkey
  AND b.censusdept = 'QL' -- written BY a pdq writer
INNER JOIN dds.dimPaymentType c on a.paymenttypekey = c.paymenttypekey
  AND c.paymenttypecode = 'W' -- warranty pay type
INNER JOIN dds.dimopcode d on a.opcodekey = d.opcodekey
  AND pdqcat1 <> 'lof' -- exclude oil changes  
LEFT JOIN dds.dimCCC e on a.ccckey = e.ccckey  
WHERE a.opendatekey IN (SELECT datekey FROM dds.day WHERE thedate > '08/31/2013')
  AND a.storecode = 'ry1'
  AND a.finalclosedatekey <> (SELECT datekey FROM dds.day WHERE datetype = 'NA')
GROUP BY d.opcode, left(d.description, 50), left(cast(e.complaint AS sql_char),50)


SELECT a.ro, a.line, b.name, d.opcode, left(d.description,25), e.complaint
FROM dds.factrepairorder a
INNER JOIN dds.dimservicewriter b on a.servicewriterkey = b.servicewriterkey
  AND b.censusdept = 'QL' -- written BY a pdq writer
INNER JOIN dds.dimPaymentType c on a.paymenttypekey = c.paymenttypekey
  AND c.paymenttypecode = 'W' -- warranty pay type
INNER JOIN dds.dimopcode d on a.opcodekey = d.opcodekey
  AND pdqcat1 <> 'lof' -- exclude oil changes  
LEFT JOIN dds.dimCCC e on a.ccckey = e.ccckey  
WHERE a.opendatekey IN (SELECT datekey FROM dds.day WHERE thedate > '08/31/2013')
  AND a.storecode = 'ry1'
  AND a.finalclosedatekey <> (SELECT datekey FROM dds.day WHERE datetype = 'NA')
ORDER BY a.ro

SELECT * FROM dds.dimopcode WHERE pdqcat1 <> 'N/A'
select * FROM dds.dimccc

SELECT *
FROM dds.dimccc
WHERE ro = '19147437'