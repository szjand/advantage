alter PROCEDURE xfmCustDim
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE xfmCustDim();

xfmDimCustomer IS populated FROM BOPNAME WHERE bnupdts > curdate() - 7
dimCustomer IS structured with type2 attributes, but IS currently being handled
  AS exclusively type1
10/9
  ADD HasValidEmail  
  
5/17 tyrrell bitching about email address XXHOLENONEXX@YAHOO.COM decoding
     AS no valid email  

fb CASE 340 DO NOT call  
  
*/
  
BEGIN TRANSACTION; 
TRY 
  TRY 
    DELETE FROM xfmDimCustomer;
    INSERT INTO xfmDimCustomer
    SELECT bnco#,bnkey,bntype,
      CASE bntype
        WHEN 'C' THEN 'Company'
        WHEN 'I' THEN 'Person'
      END,    
      bnsnam,bnlnam, bnfnam,bnmidi,bnphon,bnbphn,bncphon,
      bnemail,
      CASE 
        WHEN length(TRIM(coalesce(bnemail, ''))) = 0 THEN False
    	  WHEN position('@' IN bnemail) = 0 THEN False
        WHEN position ('.' IN bnemail) = 0 THEN False
        WHEN position('wng' IN bnemail) > 0 THEN False
      	WHEN position('dnh' IN bnemail) > 0 THEN False
      	WHEN position('noemail' IN bnemail) > 0 THEN False
      	WHEN position('none@' IN bnemail) > 0 THEN False
      	WHEN position('dng' IN bnemail) > 0 THEN False
      	ELSE True
      END AS emailValid,      
      bneml2, 
      CASE 
        WHEN length(TRIM(coalesce(bneml2, ''))) = 0 THEN False
    	  WHEN position('@' IN bneml2) = 0 THEN False
        WHEN position ('.' IN bneml2) = 0 THEN False
        WHEN position('wng' IN bneml2) > 0 THEN False
      	WHEN position('dnh' IN bneml2) > 0 THEN False
      	WHEN position('noemail' IN bneml2) > 0 THEN False
      	WHEN position('none@' IN bneml2) > 0 THEN False
      	WHEN position('dng' IN bneml2) > 0 THEN False
      	ELSE True
      END AS email2Valid,     
      bncity,bncnty,bnstcd,bnzip,
      CASE 
        WHEN (
          CASE 
            WHEN length(TRIM(coalesce(bnemail, ''))) = 0 THEN False
        	  WHEN position('@' IN bnemail) = 0 THEN False
            WHEN position ('.' IN bnemail) = 0 THEN False
            WHEN position('wng' IN bnemail) > 0 THEN False
          	WHEN position('dnh' IN bnemail) > 0 THEN False
          	WHEN position('noemail' IN bnemail) > 0 THEN False
          	WHEN position('none@' IN bnemail) > 0 THEN False
          	WHEN position('dng' IN bnemail) > 0 THEN False
          	ELSE True
          END )  
        OR (
          CASE 
            WHEN length(TRIM(coalesce(bneml2, ''))) = 0 THEN False
        	  WHEN position('@' IN bneml2) = 0 THEN False
            WHEN position ('.' IN bneml2) = 0 THEN False
            WHEN position('wng' IN bneml2) > 0 THEN False
          	WHEN position('dnh' IN bneml2) > 0 THEN False
          	WHEN position('noemail' IN bneml2) > 0 THEN False
          	WHEN position('none@' IN bneml2) > 0 THEN False
          	WHEN position('dng' IN bneml2) > 0 THEN False
          	ELSE True
          END )  = true THEN true
        ELSE false  
      END AS hasValidEmail,
      CASE 
        WHEN bnacbp = 'N' THEN true
        ELSE false
      END AS do_not_call 
    FROM tmpBOPNAME a
    WHERE bnco# IN ('RY1','RY2','RY3') 
      AND bntype <> ''
      AND bnsnam <> ''
      AND bnupdts = (
        SELECT MAX(bnupdts)
        FROM tmpBOPNAME
        WHERE bnkey = a.bnkey);
  CATCH ALL 
    RAISE dimCustomer(1, 'xfmDimCustomer ' + __errtext);
  END TRY;
  TRY 
    MERGE dimCustomer a
    USING xfmDimCustomer b ON a.bnkey = b.bnkey
    WHEN matched THEN 
    UPDATE 
    SET a.storecode = b.storecode,
        a.customertypecode = b.customertypecode,
        a.customertype = b.customertype,
        a.fullname = b.fullname,
        a.lastname = b.lastname,
        a.firstname = b.firstname,
        a.middlename = b.middlename,
        a.homephone = b.homephone,
        a.businessphone = b.businessphone,
        a.cellphone = b.cellphone,
        a.email = b.email,
        a.emailValid = b.emailValid,
        a.email2 = b.email2,
        a.email2Valid = b.email2Valid,
        a.city = b.city,
        a.county = b.county,
        a.state = b.state,
        a.zip = b.zip,
        a.HasValidEmail = b.HasValidEmail,
        a.do_not_call = b.do_not_call
    WHEN NOT matched THEN 
    INSERT (storecode,bnkey,customertypecode,customertype,fullname,
      lastname,firstname,middlename,homephone,businessphone,
      cellphone,email,emailValid,email2,email2Valid,city,county,state,zip,
      currentrow,customerkeyfromdate,customerkeyfromdatekey,customerkeythrudate,
      customerkeythrudatekey, HasValidEmail, do_not_call)
    values(b.storecode,b.bnkey,b.customertypecode,b.customertype,b.fullname,
      b.lastname,b.firstname,b.middlename,b.homephone,b.businessphone,
      b.cellphone,b.email,b.emailValid,b.email2,b.email2Valid,b.city,b.county,
      b.state,b.zip,true,curdate()-1,
      (SELECT datekey FROM day WHERE thedate = curdate() - 1),
      (SELECT thedate FROM day WHERE datetype = 'NA'),
      (SELECT datekey FROM day WHERE datetype = 'NA'),
      b.HasValidEmail, b.do_not_call);      
    CATCH ALL 
      RAISE dimCustomer(2, 'MERGE ' + __errtext);
    END TRY;
  TRY 
  INSERT INTO keyMapDimCustomer (CustomerKey,StoreCode,BNKEY)
  SELECT CustomerKey,StoreCode,BNKEY
  FROM dimCustomer a
  WHERE NOT EXISTS (
    SELECT 1
    FROM keyMapDimCustomer
    WHERE CustomerKey = a.CustomerKey);  
  CATCH ALL
    RAISE dimCustomer(3, 'KEYMAP ' + __errtext);
  END TRY;         
COMMIT WORK;  	  
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; // transaction









END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'xfmCustDim', 
   'COMMENT', 
   '');

