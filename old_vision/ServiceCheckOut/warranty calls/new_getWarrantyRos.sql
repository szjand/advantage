alter PROCEDURE GetWarrantyRos
   ( 
      userName CICHAR ( 50 ),
--      FirstName CICHAR ( 20 ) OUTPUT,
	  FirstName CICHAR ( 25 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      CloseDate CICHAR ( 12 ) OUTPUT,
      Customer CICHAR ( 50 ) OUTPUT,
      HomePhone CICHAR ( 12 ) OUTPUT,
      WorkPhone CICHAR ( 20 ) OUTPUT,
      CellPhone CICHAR ( 12 ) OUTPUT,
      Vehicle CICHAR ( 60 ) OUTPUT,
      LastCall Memo OUTPUT,
      theDate DATE OUTPUT,
      aged LOGICAL OUTPUT
   ) 
BEGIN 
/*
1/27/14
  updated to return warrantyCalls.statusUpdateWithDateName instead of just
  the description, consistency, updates IN list format show a short date AND
  who made the call
  
11/4/14
  clean up replacing huot with andrew 
  
3/20/15
  bdc IS taking over making warranty calls, so they need to be able to see the
  warrantyCalls page AND UPDATE the calls FOR ALL WRITERS, currently only 
  managers see outstanding calls for ALL writers AND that IS fucking hard coded
  with manager usernames IN the WHERE clause
  time for that shit to change
  change the conditional WHERE clause to identify users with 
  empAppAuth of (appcode=sco,approle=manager,functionality=warranty calls)
   
5/10/16
  adding honda ros
  noticed that this query IS way too fucking slow
  refactored JOIN to WarrantyCalls (huge difference)     
  
1/10/17 
  change JOIN FROM servicewriters to tpemployees, to include pdq writers
  IN the new GM spec  
  change firstname to cichar(25)        
    
EXECUTE PROCEDURE GetWarrantyROs('rodt@rydellchev.com');
EXECUTE PROCEDURE GetWarrantyROs('tgagnon@rydellcars.com');
*/
DECLARE @id string;
@id = (SELECT userName FROM __input);
INSERT INTO __output
SELECT d.firstname, a.ro, c.mmddyy AS [Closed Date], a.customer, a.HomePhone, a.WorkPhone, 
  a.CellPhone, a.vehicle, b.statusUpdateWithDateName, closeDate,
  CASE
    WHEN curdate() - closeDate > 1 THEN true
    ELSE false
  END 
FROM WarrantyROs a
LEFT JOIN dds.day c ON a.closedate = c.thedate
-- LEFT JOIN ServiceWriters d ON a.userName = d.userName
LEFT JOIN tpemployees d ON a.userName = d.userName
LEFT JOIN warrantycalls b on a.ro = b.ro
  AND b.callts = (
    SELECT  MAX(callts)
    FROM warrantycalls WHERE ro = b.ro)
--LEFT JOIN (
--  SELECT *
--  FROM WarrantyCalls x
--  WHERE CallTS = (
--    SELECT MAX(CallTS)
--    FROM WarrantyCalls
--    WHERE ro = x.ro)) b ON a.ro = b.ro 
WHERE 
  CASE
--    WHEN @id NOT IN ('mhuot@rydellchev.com','aberry@rydellchev.com', 'kespelund@rydellchev.com','aneumann@rydellcars.com') THEN a.userName = @id
    WHEN EXISTS (
      SELECT 1
      FROM employeeappauthorization
      WHERE username = @id
        AND appcode = 'sco'
        AND approle = 'manager'
        AND functionality = 'warranty calls') THEN 1 = 1
    ELSE a.username = @id   
  END
  AND FollowUpComplete = false;






END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'GetWarrantyRos', 
   'COMMENT', 
   '');

