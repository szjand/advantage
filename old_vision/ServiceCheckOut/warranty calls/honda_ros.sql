-- SELECT COUNT(*) FROM (
INSERT INTO warrantyros
    SELECT distinct d.userName, a.ro, b.thedate, e.FullName, e.HomePhone, e.BusinessPhone,
      e.CellPhone, f.Vin, TRIM(f.Make) + ' ' + TRIM(f.model), False,
      (SELECT now() FROM system.iota)
-- SELECT *      
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
      AND b.datetype = 'Date'
      AND b.thedate between curdate() - 7 AND curdate()
    INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
      AND c.CensusDept = 'MR'  
    INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber 
      AND a.storecode = d.storecode
    INNER JOIN dds.dimCustomer e on a.CustomerKey = e.CustomerKey 
      AND e.fullname <> 'INVENTORY'
    INNER JOIN dds.dimVehicle f on a.VehicleKey = f.VehicleKey
      AND CAST(f.modelyear AS sql_integer) BETWEEN year(curdate()) - 3 AND year(curdate()) + 1
      AND f.make IN ('HONDA','NISSAN')
    WHERE a.StoreCode = 'RY2'
      
 


        
SELECT year(curdate()) FROM system.iota        
        
      AND NOT EXISTS (
        SELECT 1
        FROM WarrantyROs 
        WHERE ro = a.RO)
        
SELECT * FROM dds.dimservicewriter WHERE name LIKE 'lind%' OR name LIKE 'wieb%'    

SELECT * FROM dds.dimvehicle


-- DELETE FROM warrantyros WHERE ro IN ('2735358','2735399')
-- much faster
-- instead of SELECT DISTINCT
-- ADD this to sp.swMetricDataYesterday
INSERT INTO WarrantyROS (username, ro, closedate, customer, homePhone,
  workPhone, cellPhone, vin, vehicle, followUpComplete, insertedTS)
SELECT d.userName, a.ro, a.thedate, e.FullName, e.HomePhone, e.BusinessPhone,
  e.CellPhone, f.Vin, TRIM(f.Make) + ' ' + TRIM(f.model), False,
  (SELECT now() FROM system.iota)
FROM (      
  SELECT thedate, ro, storecode, finalclosedatekey, servicewriterkey, customerkey, vehiclekey
  FROM dds.factrepairorder a
  INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
    AND b.datetype = 'Date'
    AND b.thedate between curdate() - 7 AND curdate()  
  WHERE a.storecode = 'RY2' 
  AND NOT EXISTS (
    SELECT 1
    FROM WarrantyROs 
    WHERE ro = a.RO)         
  GROUP BY thedate, ro, storecode, finalclosedatekey, servicewriterkey, customerkey, vehiclekey) a
INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
  AND c.CensusDept = 'MR'  
INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber 
  AND a.storecode = d.storecode   
INNER JOIN dds.dimCustomer e on a.CustomerKey = e.CustomerKey 
  AND e.fullname <> 'INVENTORY'  
INNER JOIN dds.dimVehicle f on a.VehicleKey = f.VehicleKey
  AND CAST(f.modelyear AS sql_integer) BETWEEN year(curdate()) - 7 AND year(curdate()) + 1
  AND f.make IN ('HONDA','NISSAN')  
  
-- ADD this to sp.swMetricDataToday
-- *c*
INSERT INTO WarrantyROS (username, ro, closedate, customer, homePhone,
  workPhone, cellPhone, vin, vehicle, followUpComplete, insertedTS)
SELECT d.userName, a.ro, a.thedate, e.FullName, e.HomePhone, e.BusinessPhone,
  e.CellPhone, f.Vin, TRIM(f.Make) + ' ' + TRIM(f.model), False,
  (SELECT now() FROM system.iota)
FROM (      
  SELECT thedate, ro, storecode, finalclosedatekey, servicewriterkey, customerkey, vehiclekey
  FROM dds.todayFactRepairOrder a
  INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
    AND b.thedate = curdate()  
  WHERE a.storecode = 'RY2' 
  AND NOT EXISTS (
    SELECT 1
    FROM WarrantyROs 
    WHERE ro = a.RO)         
  GROUP BY thedate, ro, storecode, finalclosedatekey, servicewriterkey, customerkey, vehiclekey) a
INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
  AND c.CensusDept = 'MR'  
INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber 
  AND a.storecode = d.storecode   
INNER JOIN dds.dimCustomer e on a.CustomerKey = e.CustomerKey 
  AND e.fullname <> 'INVENTORY'  
INNER JOIN dds.dimVehicle f on a.VehicleKey = f.VehicleKey
  AND CAST(f.modelyear AS sql_integer) BETWEEN year(curdate()) - 7 AND year(curdate()) + 1
  AND f.make IN ('HONDA','NISSAN');  
  
