alter PROCEDURE swMetricDataToday
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE swMetricDataToday();
4/13 rosClosedWithInspectionLine: changed to 23I only
     populate TABLE WarrantyROs prior to UPDATE serviceWriterMetricData.OPEN Warranty Calls
5/4
  return a 0 instead of NULL for OPEN warranty calls     
6/8
  modified for ry2  
    include opcode 24z for ry2 inspections
    exclude cashiering, warranty calls for ry2
    ADD HondaNissanCalls for ry2  
9/14
  replace calls to factROToday/LineToday to todayFactRepairOrder  
10/2
  v1.5, added CurrentROs   
  
12/26/13
  new factRepairOrder/todayFactRepairOrder: no more line level labor sales, RoLaborSales only     
1/31: replace eeUserName with userName

2/14: 
-- *a*
  DO NOT SUM roLaborSales, SUM laborSales   
5/13/14:
-- *b*  
  returning double COUNT results for some honda ros for phone calls
  issue IS currentros being populated with writer 428 twice, wiebusch AND lindquest
  include storecode on JOIN to dimServiceWriter
9/4/14
  fb 324: added insertedTS to TABLE warrantyRos
5/10/16
-- *c*  
  fb 338: ADD ry2 ros to warrantyros
*/
TRY 
  BEGIN TRANSACTION;
  TRY
    TRY  
    -- ROs Closed -------------------------------------------------------------   
      DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() AND metric = 'ROs Closed';
      INSERT INTO ServiceWriterMetricData  
      SELECT f.userName, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'ROs Closed', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM ( -- 1 row per writer w/# of ros
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
          GROUP BY a.ro, a.servicewriterkey) c
        GROUP BY servicewriterkey) d  
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();        
      -- ROs Closed -----------------------------------------------------------------  
      -- weekly -------------------
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT userName, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.userName = a.userName
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'ROs Closed') x
      WHERE ServiceWriterMetricData.userName = x.userName
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'ROs Closed';   
    CATCH ALL
      RAISE swMetricDataToday(100, __errtext);
    END TRY;
    TRY     
    -- rosClosedWithInspectionLine ------------------------------------------------ 
      DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() AND metric = 'rosClosedWithInspectionLine';  
      INSERT INTO ServiceWriterMetricData     
      SELECT f.userName, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosClosedWithInspectionLine', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM ( -- 1 row per writer w/# of ros
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          INNER JOIN dds.dimOpcode b on a.opcodekey = b.opcodekey
            AND b.opcode IN ('23I', '24Z')
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
          GROUP BY a.ro, a.servicewriterkey) c
        GROUP BY servicewriterkey) d  
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();        
    CATCH ALL
      RAISE swMetricDataToday(101, __errtext);  
    END TRY;  
    TRY
    -- rosCashiered ---------------------------------------------------------------
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashiered' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData         
      SELECT f.userName, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosCashiered', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM (        
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
            AND a.storecode = 'RY1'
            AND NOT EXISTS (
              SELECT 1
              FROM dds.todayFactRepairOrder m
              INNER JOIN dds.dimPaymentType n on m.paymenttypekey = n.paymenttypekey
              WHERE ro = a.ro
                AND n.paymenttypecode IN ('w','i'))
            AND NOT EXISTS (
              SELECT 1
              FROM dds.todayFactRepairOrder o
              INNER JOIN dds.dimservicetype p on o.servicetypekey = p.servicetypekey
              WHERE ro = a.ro
                AND p.servicetypecode <> 'mr')          
            AND EXISTS (
              SELECT 1
              FROM tmpGLPTRNS -- generated BY scoToday.tmpGLPTRNS 
              WHERE gtdoc# = a.ro)
          GROUP BY a.ro, a.servicewriterkey) c   
        GROUP BY servicewriterkey) d
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();
    CATCH ALL
      RAISE swMetricDataToday(102, __errtext);  
    END TRY;      
    TRY 
    -- rosCashieredByWriter -------------------------------------------------------
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashieredByWriter' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData      
      SELECT f.userName, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosCashieredByWriter', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      --SELECT f.lastname, howmany
      FROM (
        SELECT servicewriterkey, coalesce(COUNT(*), 0) AS howmany
        FROM ( -- 1 row per ro/writer 
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          INNER JOIN tmpGLPTRNS b on a.ro = b.gtdoc#
          INNER JOIN tmpGLPDTIM c on b.gttrn# = c.gqtrn#
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
            AND a.storecode = 'RY1'
            AND NOT EXISTS (
              SELECT 1
              FROM dds.todayFactRepairOrder m
              INNER JOIN dds.dimPaymentType n on m.paymenttypekey = n.paymenttypekey
              WHERE ro = a.ro
                AND n.paymenttypecode IN ('w','i'))
            AND NOT EXISTS (
              SELECT 1
              FROM dds.todayFactRepairOrder o
              INNER JOIN dds.dimservicetype p on o.servicetypekey = p.servicetypekey
              WHERE ro = a.ro
                AND p.servicetypecode <> 'mr')
            AND c.gquser IN (SELECT dtUsername FROM ServiceWriters)
          GROUP BY a.ro, a.servicewriterkey) c 
        GROUP BY servicewriterkey) d 
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();        
    CATCH ALL
      RAISE swMetricDataToday(103, __errtext);  
    END TRY;    
    TRY  
    -- rosClosedWithFlagHours -----------------------------------------------------
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosClosedWithFlagHours' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData    
      SELECT f.userName, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosClosedWithFlagHours', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM ( -- 1 row per writer w/# of ros
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
            AND roFlagHours > 0
          GROUP BY a.ro, a.servicewriterkey) c
        GROUP BY servicewriterkey) d  
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.censusdept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();   
    -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT userName, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.userName = a.userName
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'rosClosedWithFlagHours') x
      WHERE ServiceWriterMetricData.userName = x.userName
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric; 
    CATCH ALL
      RAISE swMetricDataToday(104, __errtext);  
    END TRY;        
    -- flaghours ------------------------------------------------------------------
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'flagHours' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData    
      SELECT d.userName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'flagHours', 0 AS seq, 
        a.flaghours AS today, 0 AS thisweek, 
        trim(cast(a.flaghours AS sql_char)), CAST(NULL AS sql_char)     
      FROM (
        SELECT a.servicewriterkey, SUM(a.FlagHours) AS FlagHours
        FROM dds.todayFactRepairOrder a
        INNER JOIN dds.day b on a.FinalCloseDateKey = b.DateKey
          AND b.theDate = curdate()
        GROUP BY a.ServiceWriterKey) a
      INNER JOIN dds.day b on b.thedate = curdate()
      INNER JOIN dds.dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
        AND c.CensusDept = 'MR'
      INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber;        
      -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT userName, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.userName = a.userName
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'flagHours') x
      WHERE ServiceWriterMetricData.userName = x.userName
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric; 
    CATCH ALL
      RAISE swMetricDataToday(105, __errtext);  
    END TRY;    
    TRY    
    -- Cashiering -----------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosCashieredByWriter & 
      ServiceWriterMetricData.rosCashiered to be complete
      generate weekly totals before generating display metric (cashiering)
    */
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT userName, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.userName = a.userName
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric in ('rosCashiered','rosCashieredByWriter')) x
      WHERE ServiceWriterMetricData.userName = x.userName
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN ('rosCashiered','rosCashieredByWriter') ; 
        
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Cashiering';
      INSERT INTO ServiceWriterMetricData
      SELECT a.userName, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Cashiering', 1 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.userName = b.userName
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosCashieredByWriter'
        AND b.metric = 'rosCashiered';
    CATCH ALL
      RAISE swMetricDataToday(106, __errtext);  
    END TRY;     
    -- Open Aged ROs --------------------------------------------------------------------
    /*
    UPDATE throughout the day IN terms of removing ros that get closed
    DELETE FROM agedros daily based ON an ro closing
    overnight a full scrap
    AgedROs TABLE IS necessary for the nightly/today to run
    
    no need to UPDATE with newly aged ros thru out the day
    
    need to UPDATE the weekly numbers here
    ALL i need are this week, which will be the same AS the daily total each day
    */
    TRY 
      DELETE
      FROM agedros 
      WHERE EXISTS (
        SELECT 1
        -- FROM factROToday
        FROM dds.todayFactRepairOrder
        WHERE ro = agedros.ro
        AND finalclosedatekey <> 7306);
/*       
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Aged ROs' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData        
      SELECT c.userName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
        a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Aged ROs', 2 AS seq, b.howmany AS today,
      --  0 AS thisWeek, trim(CAST(b.howmany AS sql_char)), CAST(NULL AS sql_char)
        howmany thisWeek, trim(CAST(howmany AS sql_char)) AS todayDisplay, 
        trim(CAST(howmany AS sql_char)) AS thisWeekDisplay
      FROM dds.day a, ( 
        SELECT userName, COUNT(*) AS howmany
        FROM agedros  
        GROUP BY userName) b, 
        servicewriters c
      WHERE a.thedate = curdate()
        AND b.userName = c.userName; 
*/	
  DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Aged ROs' AND thedate = curdate();
  INSERT INTO ServiceWriterMetricData 	
  SELECT c.userName, c.storecode, c.fullname, c.lastname, c.firstname,
    c.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
    a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Aged ROs', 2 AS seq, coalesce(b.howmany, 0) AS today,
    coalesce(howmany, 0) as thisWeek, trim(CAST(coalesce(howmany, 0) AS sql_char)) AS todayDisplay, 
    trim(CAST(coalesce(howmany, 0) AS sql_char)) AS thisWeekDisplay	 
  FROM dds.day a
  LEFT JOIN serviceWriters c on 1 = 1
  LEFT JOIN (
    SELECT userName, COUNT(*) AS howmany
    FROM agedros  
    GROUP BY userName) b on c.username = b.username
  WHERE a.thedate = curdate()
    AND c.active = true;  		 
    CATCH ALL
      RAISE swMetricDataToday(107, __errtext);  
    END TRY;        
    -- Open Warranty Calls --------------------------------------------------------
    /*
      the challenges here, like AgedROs, the data depends ON dds.factro, 
        sco.factrotoday & sco.WarrantyROs
        backfill FROM dds.factro
    */
    TRY  
      INSERT INTO WarrantyROS (username, ro, closedate, customer, homePhone,
        workPhone, cellPhone, vin, vehicle, followUpComplete, insertedTS)  
      SELECT distinct d.userName, a.ro, b.thedate, e.FullName, e.HomePhone, e.BusinessPhone,
        e.CellPhone, f.Vin, TRIM(f.Make) + ' ' + TRIM(f.model), False,
        (SELECT now() FROM system.iota)
      FROM dds.todayFactRepairOrder a
      INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
        AND b.thedate = curdate()
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'  
      INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber 
      INNER JOIN dds.dimCustomer e on a.CustomerKey = e.CustomerKey 
      INNER JOIN dds.dimVehicle f on a.VehicleKey = f.VehicleKey
	-- i think the JOIN IS faster than the NOT EXISTS
      LEFT JOIN WarrantyRos g on a.ro = g.ro
      WHERE a.StoreCode = 'RY1'
    	AND a.paymenttypekey = 5 -- ads IS slow AND stupid with multiple joins
    	AND g.ro IS NULL;
--      AND EXISTS (
--        SELECT 1
--        FROM dds.todayFactRepairOrder r
--        INNER JOIN dds.dimPaymentType s on r.PaymentTypeKey = s.PaymentTypeKey
--          AND s.PaymentTypeCode = 'W'
--        WHERE ro = a.ro) 
--      AND NOT EXISTS (
--        SELECT 1
--        FROM WarrantyROs 
--        WHERE ro = a.RO); 
 
-- SELECT COUNT(*) FROM warrantyros;
      INSERT INTO WarrantyROS (username, ro, closedate, customer, homePhone,
        workPhone, cellPhone, vin, vehicle, followUpComplete, insertedTS, Warranty)
      SELECT distinct d.userName, a.ro, b.thedate, e.FullName, e.HomePhone, e.BusinessPhone,
        e.CellPhone, f.Vin, TRIM(f.Make) + ' ' + TRIM(f.model), False,
        (SELECT now() FROM system.iota), False 
      FROM dds.todayFactRepairOrder a
      INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
        AND b.thedate between curdate() - 7 AND curdate()
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
      INNER JOIN tpemployees d on c.employeenumber = d.employeenumber
      INNER JOIN dds.dimCustomer e on a.CustomerKey = e.CustomerKey 
      INNER JOIN dds.dimVehicle f on a.VehicleKey = f.VehicleKey
        AND f.make IN ('CHEVROLET','GMC','BUICK','CADILLAC','PONTIAC')
        AND cast(f.modelyear AS sql_integer) BETWEEN year(curdate()) - 7 
          AND year(curdate())
      LEFT JOIN WarrantyRos g on a.ro = g.ro    
      WHERE a.StoreCode = 'RY1'
        AND a.paymenttypekey = 2 -- Customer Pay
        AND LEFT(a.ro, 2) <> '18' -- Body Shop
        AND g.ro IS NULL; -- NOT already IN WarrantyRos 
	               
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Warranty Calls' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData      
      SELECT a.userName, a.storecode, a.fullname, a.lastname, a.firstname,
        a.writerNumber, c.datekey, c.thedate, c.dayOfWeek, c.dayName,
        c.dayOfMonth, c.sundayToSaturdayWeek, 'Open Warranty Calls', 3 AS seq, coalesce(b.howmany, 0) AS today,
        coalesce(b.howmany, 0) AS thisWeek, trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS todayDisplay, 
        trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS thisWeekDisplay
      FROM servicewriters a  
      LEFT JOIN (
        SELECT userName, COUNT(*) AS howmany
        FROM warrantyROs 
        WHERE followUpComplete = false
        GROUP BY userName) b ON a.userName = b.userName
      LEFT JOIN dds.day c ON 1 = 1
        AND c.thedate = curdate()  
      WHERE a.active = true
        AND a.storecode = 'ry1'; -- ry2 mod
    CATCH ALL
      RAISE swMetricDataToday(108, __errtext);  
    END TRY;       
    -- Avg Hours/RO ---------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosClosedWithFlagHours & ServiceWriterMetricData.flagHours to be complete
      different than cashiering AND insp requested, those weekly numbers are based ON discreet
      values that get updated at the END, a simple concantenation of the discreet values
      this IS a ratio
    */
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Avg Hours/RO';
      INSERT INTO ServiceWriterMetricData
      SELECT a.userName, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Avg Hours/RO', 4 AS seq, 
        round(a.today/b.today, 1) AS today,
        round(a.thisweek/b.thisweek, 1) AS thisweek,  
        left(CAST(cast(round(a.today/b.today, 1) as sql_double) AS sql_char), 12)  AS todaydisplay,
        left(cast(CAST(round(a.thisweek/b.thisweek, 1) AS sql_double) AS sql_char), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.userName = b.userName
        AND a.thedate = b.thedate
      WHERE a.metric = 'flagHours'
        AND b.metric = 'rosClosedWithFlagHours';
    CATCH ALL
      RAISE swMetricDataToday(109, __errtext);      
    END TRY;     
    -- LaborSales -----------------------------------------------------------------
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Labor Sales' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData     
      SELECT d.userName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
        round(cast(abs(a.laborsales) AS sql_double), 0) AS today,
        0, 
        trim(cast(coalesce(abs(round(CAST(a.laborsales AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
        CAST(NULL AS sql_char)
      FROM (        
-- *a*	     
--      SELECT a.ServiceWriterKey, SUM(RoLaborSales) AS LaborSales
	  SELECT a.ServiceWriterKey, SUM(laborSales) AS LaborSales
        FROM dds.todayFactRepairOrder a
        INNER JOIN dds.day b on a.FinalCloseDateKey = b.datekey
          AND b.thedate = curdate()
        GROUP BY a.ServiceWriterKey) a 
      INNER JOIN dds.day b on b.thedate = curdate()
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'
      INNER JOIN ServiceWriters d on c.StoreCode = d.StoreCode
        and c.WriterNumber = d.WriterNumber;         
    CATCH ALL
      RAISE swMetricDataToday(110, __errtext);      
    END TRY;     
    -- rosOpened ------------------------------------------------------------------  
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'ROs Opened' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData   
      SELECT d.userName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Opened', 6 AS seq, a.HowMany AS today,
        0 AS thisWeek, trim(CAST(a.HowMany AS sql_char)), CAST(NULL AS sql_char)  
      FROM (      
        SELECT ServiceWriterKey, COUNT(*) AS HowMany
        FROM (  
          SELECT a.ServiceWriterKey, a.ro
          FROM dds.todayFactRepairOrder a
          INNER JOIN dds.day b on a.OpenDateKey = b.datekey
            AND b.thedate = curdate()
          GROUP BY a.ServiceWriterKey, a.ro) x
        GROUP BY ServiceWriterKey) a 
      INNER JOIN dds.day b on b.thedate = curdatE()
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'
      INNER JOIN ServiceWriters d on c.StoreCode = d.StoreCode
        and c.WriterNumber = d.WriterNumber;        
    CATCH ALL
      RAISE swMetricDataToday(111, __errtext);      
    END TRY;         
    -- Inspections Requested ------------------------------------------------------
    /*
      requires ServiceWriterMetricData.ROs Closed & 
      ServiceWriterMetricData.rosClosedWithInspectionLine to be complete
      generate weekly totals before generating display metric (cashiering)
    */
    TRY 
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT userName, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.userName = a.userName
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'rosClosedWithInspectionLine') x
      WHERE ServiceWriterMetricData.userName = x.userName
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'rosClosedWithInspectionLine'; 
      
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Inspections Requested';
      INSERT INTO ServiceWriterMetricData
      SELECT a.userName, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Inspections Requested', 7 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.userName = b.userName
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosClosedWithInspectionLine'
        AND b.metric = 'ROs Closed';
    CATCH ALL
      RAISE swMetricDataToday(112, __errtext);      
    END TRY;         

    -- UPDATE cumulative metrics only with weekly totals
    TRY 
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT userName, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.userName = a.userName
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE EXISTS (
          SELECT 1
          FROM ServiceWriterMetrics
          WHERE metric = a.metric
            AND cumulative = true)) x
      WHERE ServiceWriterMetricData.userName = x.userName
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN (
        SELECT metric
        FROM servicewritermetrics
        WHERE cumulative = true)  ; 
    CATCH ALL
      RAISE swMetricDataToday(113, __errtext);      
    END TRY;
	
	TRY 
      DELETE FROM CurrentROS;
      INSERT INTO CurrentROs  
      SELECT distinct e.userName, a.ro, a.thedate, b.fullname AS customername, 
        a.rostatus AS status, c.vin, TRIM(c.make) + ' ' + trim(c.model) AS vehicle        
      FROM (
        SELECT a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey
        FROM dds.factRepairOrder a
        INNER JOIN dds.day b on a.opendatekey = b.datekey
          AND b.thedate >= curdate() - 7
        INNER JOIN dds.dimRoStatus c on a.rostatuskey = c.rostatuskey
          AND c.rostatus <> 'Closed'
        GROUP BY a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey) a  
      INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
      INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
      INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
        AND d.CensusDept = 'MR'
      INNER JOIN ServiceWriters e on d.writernumber = e.writernumber 
-- *b*	  
	    AND d.storecode = e.storecode
      UNION     
      SELECT distinct e.userName, a.ro, a.thedate, b.fullname AS customername, 
        a.rostatus AS status, c.vin, TRIM(c.make) + ' ' + trim(c.model) AS vehicle        
      FROM (
        SELECT a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey
        FROM dds.TodayfactRepairOrder a
        INNER JOIN dds.day b on a.opendatekey = b.datekey
          AND b.thedate = curdate()
        INNER JOIN dds.dimRoStatus c on a.rostatuskey = c.rostatuskey
          AND c.rostatus <> 'Closed'
        GROUP BY a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey) a  
      INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
      INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
      INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
        AND d.CensusDept = 'MR'
      INNER JOIN ServiceWriters e on d.writernumber = e.writernumber
-- *b*	  
	    AND d.storecode = e.storecode;  	
    CATCH ALL
      RAISE swMetricDataToday(114, __errtext);      
    END TRY;
-- *c*
    TRY 
      INSERT INTO WarrantyROS (username, ro, closedate, customer, homePhone,
        workPhone, cellPhone, vin, vehicle, followUpComplete, insertedTS, Warranty )
      SELECT d.userName, a.ro, a.thedate, e.FullName, e.HomePhone, e.BusinessPhone,
        e.CellPhone, f.Vin, TRIM(f.Make) + ' ' + TRIM(f.model), False,
        (SELECT now() FROM system.iota), true
      FROM (      
        SELECT thedate, ro, storecode, finalclosedatekey, servicewriterkey, customerkey, vehiclekey
        FROM dds.todayFactRepairOrder a
        INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
          AND b.thedate = curdate()  
        WHERE a.storecode = 'RY2' 
        AND NOT EXISTS (
          SELECT 1
          FROM WarrantyROs 
          WHERE ro = a.RO)         
        GROUP BY thedate, ro, storecode, finalclosedatekey, servicewriterkey, customerkey, vehiclekey) a
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'  
      INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber 
        AND a.storecode = d.storecode   
      INNER JOIN dds.dimCustomer e on a.CustomerKey = e.CustomerKey 
        AND e.fullname <> 'INVENTORY'  
      INNER JOIN dds.dimVehicle f on a.VehicleKey = f.VehicleKey
        AND CAST(f.modelyear AS sql_integer) BETWEEN year(curdate()) - 7 AND year(curdate()) + 1
        AND f.make IN ('HONDA','NISSAN');   
    CATCH ALL
      RAISE swMetricDataToday(115, __errtext);      
    END TRY;             
  COMMIT WORK;
  CATCH ALL
    ROLLBACK WORK;
    RAISE;
  END TRY;  
CATCH ALL
  RAISE;
END TRY;    

















END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'swMetricDataToday', 
   'COMMENT', 
   '');

