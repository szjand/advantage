

DROP TABLE ManagerLandingPage;
CREATE TABLE ManagerLandingPage (
  WeekID integer,
  Metric cichar(40), 
  Sun cichar(12),
  Mon cichar(12),
  Tue cichar(12),
  Wed cichar(12),
  Thu cichar(12),
  Fri cichar(12),
  Sat cichar(12),
  Total cichar(12),
  seq integer
);


INSERT INTO ManagerLandingPage values (
  483, 'Cashiering','0 / 0','0 / 0','0 / 0','0 / 0','0 / 0','0 / 0','0 / 0','0 / 0',1);
INSERT INTO ManagerLandingPage values (
  483, 'Open Aged ROs','10','10','10','10','10','10','10','10',2);
INSERT INTO ManagerLandingPage values (
  483, 'Open Warranty Calls','20','20','20','20','20','20','20','20',3);
INSERT INTO ManagerLandingPage values (
  483, 'Avg Hours/RO','3.1','3.1','3.1','3.1','3.1','3.1','3.1','3.1',4);
INSERT INTO ManagerLandingPage values (
  483, 'Labor Sales','20','20','20','20','20','20','20','20',5);
INSERT INTO ManagerLandingPage values (
  483, 'ROs Opened','20','20','20','20','20','20','20','20',6);
INSERT INTO ManagerLandingPage values (
  483, 'Inspections Requested','0 / 0','0 / 0','0 / 0','0 / 0','0 / 0','0 / 0','0 / 0','0 / 0',7);
INSERT INTO ManagerLandingPage values (
  483, 'CSI','20','20','20','20','20','20','20','20',8);
INSERT INTO ManagerLandingPage values (
  483, 'Walk Arounds','20','20','20','20','20','20','20','20',9);     
  
INSERT INTO ManagerLandingPage values (
  482, 'Cashiering','5 / 5','5 / 5','5 / 5','5 / 5','5 / 5','5 / 5','5 / 5','5 / 5',1);
INSERT INTO ManagerLandingPage values (
  482, 'Open Aged ROs','20','20','20','20','20','20','20','20',2);
INSERT INTO ManagerLandingPage values (
  482, 'Open Warranty Calls','50','50','50','50','50','50','50','50',3);
INSERT INTO ManagerLandingPage values (
  482, 'Avg Hours/RO','2.5','2.5','2.5','2.5','2.5','2.5','2.5','2.5',4);
INSERT INTO ManagerLandingPage values (
  482, 'Labor Sales','50','50','50','50','50','50','50','50',5);
INSERT INTO ManagerLandingPage values (
  482, 'ROs Opened','50','50','50','50','50','50','50','50',6);
INSERT INTO ManagerLandingPage values (
  482, 'Inspections Requested','5 / 5','5 / 5','5 / 5','5 / 5','5 / 5','5 / 5','5 / 5','5 / 5',7);
INSERT INTO ManagerLandingPage values (
  482, 'CSI','50','50','50','50','50','50','50','50',8);
INSERT INTO ManagerLandingPage values (
  482, 'Walk Arounds','50','50','50','50','50','50','50','50',9);         

DROP PROCEDURE GetManagerLandingPageCurrentWeek;    
CREATE PROCEDURE GetManagerLandingPageCurrentWeek (    
  Metric cichar(45) output, 
  Sun cichar(12) output,
  Mon cichar(12) output,
  Tue cichar(12) output,
  Wed cichar(12) output,
  Thu cichar(12) output,
  Fri cichar(12) output,
  Sat cichar(12) output,
  Total cichar(12) output,
  seq integer output)
  
BEGIN
/*
EXECUTE PROCEDURE GetManagerLandingPageCurrentWeek();
*/
  INSERT INTO __output
  SELECT metric,sun,mon,tue,wed,thu,fri,sat,total,seq
  FROM ManagerLandingPage
  WHERE weekid = (
    SELECT sundaytosaturdayweek
    FROM dds.day
    WHERE thedate = curdate());
END;      
DROP PROCEDURE GetManagerLandingPagePreviousWeek;
CREATE PROCEDURE GetManagerLandingPagePreviousWeek (    
  Metric cichar(45) output, 
  Sun cichar(12) output,
  Mon cichar(12) output,
  Tue cichar(12) output,
  Wed cichar(12) output,
  Thu cichar(12) output,
  Fri cichar(12) output,
  Sat cichar(12) output,
  Total cichar(12) output,
  seq integer output)
  
BEGIN
/*
EXECUTE PROCEDURE GetManagerLandingPagePreviousWeek();
*/
  INSERT INTO __output
  SELECT metric,sun,mon,tue,wed,thu,fri,sat,total,seq
  FROM ManagerLandingPage
  WHERE weekid = (
    SELECT sundaytosaturdayweek
    FROM dds.day
    WHERE thedate = curdate() - 7);
END;  