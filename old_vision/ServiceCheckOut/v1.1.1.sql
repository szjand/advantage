ALTER TABLE people
ADD COLUMN Password cichar(24)
ADD COLUMN MemberGroup cichar(24);

--SELECT *
--FROM people

UPDATE people
SET password = 'password';

UPDATE people
SET MemberGroup = 'ServiceMisc'
WHERE fullname = 'Devon Brekke';

UPDATE people
SET MemberGroup = 'ServiceManager'
WHERE fullname IN ('Andrew Neumann','Mike Huot','Al Berry','Ben Cahalan');

UPDATE people
SET MemberGroup = 'ServiceWriter'
WHERE MemberGroup IS NULL;

create PROCEDURE ValidateLogin (
  UserName cichar(50),
  Password cichar(50),
  MemberGroup cichar(24) output,
  FirstName cichar(40) output,
  LastName cichar(40) output)
BEGIN
/*
EXECUTE PROCEDURE validatelogin('mhuot@rydellchev.com','password');
*/
DECLARE @username cichar(50);
DECLARE @password cichar(50);
@username = (SELECT username FROM __input);
@password = (SELECT password FROM __input);  
IF EXISTS (
    SELECT 1
    FROM people
    WHERE eeusername = @username
      AND password = @password) THEN
  INSERT INTO __output   
  SELECT membergroup, firstname, lastname 
  FROM people
  WHERE eeUserName = @username
    AND Password = @password;
ELSE
  INSERT INTO __output
  SELECT 'Denied', '','' FROM system.iota;
END IF;     
END;  