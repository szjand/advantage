/*
SELECT name FROM system.storedprocedures
WHERE name LIKE '%pdq%'
ORDER BY name


SELECT name FROM system.tables
WHERE name LIKE '%pdq%'
ORDER BY name
*/

BEGIN TRANSACTION;
TRY
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqWriterPayrollData', 'zUnused_pdqWriterPayrollData', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqWriterCommissionMatrix', 'zUnused_pdqWriterCommissionMatrix', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqWriterPointsMatrix', 'zUnused_pdqWriterPointsMatrix', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqManagerPointsMatrix', 'zUnused_pdqManagerPointsMatrix', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqManagerCommissionMatrix', 'zUnused_pdqManagerCommissionMatrix', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqStoreData', 'zUnused_pdqStoreData', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqWriterReviews', 'zUnused_pdqWriterReviews', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqTmpManagerSummary', 'zUnused_pdqTmpManagerSummary', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'positionReportsTo', 'zUnused_positionReportsTo', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'positionNS', 'zUnused_positionNS', 1, 0);

EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqManagerPaySummary', 'zUnused_getPdqManagerPaySummary', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqReviewForAllWriters', 'zUnused_getPdqReviewForAllWriters', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqReviewForAWriter', 'zUnused_getPdqReviewForAWriter', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqWpHourlyDetails', 'zUnused_getPdqWpHourlyDetails', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqWpOpCodes', 'zUnused_getPdqWpOpCodes', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqWpPayPeriods', 'zUnused_getPdqWpPayPeriods', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqWpPointDetails', 'zUnused_getPdqWpPointDetails', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqWpStoreTotals', 'zUnused_getPdqWpStoreTotals', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqWpWriterRos', 'zUnused_getPdqWpWriterRos', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqWpWriterSummary', 'zUnused_getPdqWpWriterSummary', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqWpWriterTotals', 'zUnused_getPdqWpWriterTotals', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'getPdqWriters', 'zUnused_getPdqWriters', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqWriterReviewCreate', 'zUnused_pdqWriterReviewCreate', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqWriterReviewDelete', 'zUnused_pdqWriterReviewDelete', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'pdqWriterReviewEdit', 'zUnused_pdqWriterReviewEdit', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayXfmPdqStoreData', 'zUnused_todayXfmPdqStoreData', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'todayXfmPdqWriterPayrollData', 'zUnused_todayXfmPdqWriterPayrollData', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'xfmPdqStoreData', 'zUnused_xfmPdqStoreData', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'xfmPdqWriterPayrollData', 'zUnused_xfmPdqWriterPayrollData', 10, 0);

EXECUTE PROCEDURE sp_DropReferentialIntegrity('tpEmployees-pdqWriterReviews');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('Position-Tasks1');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('Position-Tasks2');
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   


SELECT *
FROM system.storedprocedures
WHERE lower(CAST(sql_script AS sql_longvarchar)) LIKE '%mhuo%'
  AND name NOT LIKE 'z%'
  
  
SELECT *
FROM system.storedprocedures
WHERE lower(CAST(sql_script AS sql_longvarchar)) LIKE '%functionality = ''warranty calls''%'  

  
SELECT *
FROM system.storedprocedures
WHERE lower(CAST(sql_script AS sql_longvarchar)) LIKE '%task%' 
  AND name NOT LIKE 'z%'
  
  
SELECT *
FROM system.storedprocedures
WHERE lower(CAST(sql_script AS sql_longvarchar)) LIKE '%currentros%' 
  AND name NOT LIKE 'z%'
  
  
SELECT *
FROM system.storedprocedures
WHERE lower(CAST(sql_script AS sql_longvarchar)) LIKE '%positionful%' 
  AND name NOT LIKE 'z%'  
  
    
SELECT *
FROM system.relations  
WHERE ri_primary_table LIKE 'zu%'
  OR ri_foreign_table LIKE 'zu%'