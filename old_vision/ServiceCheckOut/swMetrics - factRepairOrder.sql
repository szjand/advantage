the goal IS to replace ALL calls to factro AND factroline with calls to factRepairOrder
edit the sco stored procs swMetricDataYesterday AND swMetricDataToday
populate the tables AgedROs, WarrantyROs & RY2ROsForCalls, ServiceWriterMetricData

9/12
    ADD censusdept to INNER JOIN on dimServiceWriters
    the wiebusch deal, what i am envisioning IS that wiebusch will NOT show up 
    under the list of writers for huot, but any drive ros still OPEN for him 
    will show up IN huots list  
    16127357 gets LEFT out because wiebusch changed to PDQ on 8/6, this ro was opened on 8/12
      so the writer key for that ro points to wiebusch AS pdq  
      too fucking bad!
--< agedROS -------------------------------------------------------------------
-- swMetricDataYesterday
SELECT distinct e.eeusername, a.ro, a.thedate, b.fullname AS customername, 
  a.rostatus AS status, c.vin, TRIM(c.make) + ' ' + trim(c.model) AS vehicle
FROM (
  SELECT a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.opendatekey = b.datekey
    AND b.thedate < curdate() - 7
  INNER JOIN dds.dimRoStatus c on a.rostatuskey = c.rostatuskey
    AND c.rostatus <> 'Closed'
  GROUP BY a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey) a  
INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
  AND d.CensusDept = 'MR'
INNER JOIN ServiceWriters e on d.writernumber = e.writernumber;

-- thought ALL ros got a finalclosedate, greg says no, only those that were  IN delayed CLOSE
-- at some point IN time
-- he was wrong, per jeremy, ALL ros should get a final CLOSE date, it's just that those 
-- with warranty, service contract OR internal lines may have a final CLOSE date that IS diff than the CLOSE date
--/> agedROS -------------------------------------------------------------------

--< ROs Closed -------------------------------------------------------------------
-- yesterday OR today
DECLARE @date date;
@date = curdate() - 1; 
SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
  f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
  g.dayofmonth, g.sundaytosaturdayweek, 'ROs Closed', 0, howmany AS today,
  0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
FROM ( -- 1 row per writer w/# of ros
  SELECT servicewriterkey, COUNT(*) AS howmany
  FROM ( -- 1 row per ro/writer    
    SELECT a.ro, a.servicewriterkey
    FROM dds.factRepairOrder a
    WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
    GROUP BY a.ro, a.servicewriterkey) c
  GROUP BY servicewriterkey) d  
INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
  AND e.CensusDept = 'MR'
INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
INNER JOIN dds.day g on g.thedate = @date;
-- weekly :: ALL done against ServiceWriterMetricData
--/> ROs Closed -------------------------------------------------------------------

--< rosClosedWithInspectionLine ------------------------------------------------------------------      
DECLARE @date date;
@date = curdate() - 1;
SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
  f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
  g.dayofmonth, g.sundaytosaturdayweek, 'rosClosedWithInspectionLine', 0, howmany AS today,
  0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
FROM ( -- 1 row per writer w/# of ros
  SELECT servicewriterkey, COUNT(*) AS howmany
  FROM ( -- 1 row per ro/writer    
    SELECT a.ro, a.servicewriterkey
    FROM dds.factRepairOrder a
    INNER JOIN dds.dimOpcode b on a.opcodekey = b.opcodekey
      AND b.opcode IN ('23I', '24Z')
    WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
    GROUP BY a.ro, a.servicewriterkey) c
  GROUP BY servicewriterkey) d  
INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
  AND e.CensusDept = 'MR'
INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
INNER JOIN dds.day g on g.thedate = @date;  
--/> rosClosedWithInspectionLine -------------------------------------------------------------------


--< rosCashiered -------------------------------------------------------------------
DECLARE @date date;
@date = curdate() - 1;  
SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
  f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
  g.dayofmonth, g.sundaytosaturdayweek, 'rosCashiered', 0, howmany AS today,
  0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
FROM (        
  SELECT servicewriterkey, COUNT(*) AS howmany
  FROM ( -- 1 row per ro/writer    
    SELECT a.ro, a.servicewriterkey
    FROM dds.factRepairOrder a
    WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
      AND a.storecode = 'RY1'
      AND NOT EXISTS (
        SELECT 1
        FROM dds.factRepairOrder m
        INNER JOIN dds.dimPaymentType n on m.paymenttypekey = n.paymenttypekey
        WHERE ro = a.ro
          AND n.paymenttypecode IN ('w','i'))
      AND NOT EXISTS (
        SELECT 1
        FROM dds.factRepairOrder o
        INNER JOIN dds.dimservicetype p on o.servicetypekey = p.servicetypekey
        WHERE ro = a.ro
          AND p.servicetypecode <> 'mr')          
      AND EXISTS (
        SELECT 1
        FROM dds.stgArkonaGLPTRNS
        WHERE gtdoc# = a.ro)
    GROUP BY a.ro, a.servicewriterkey) c   
  GROUP BY servicewriterkey) d
INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
  AND e.CensusDept = 'MR'
INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
INNER JOIN dds.day g on g.thedate = @date;
--/> rosCashiered -------------------------------------------------------------------

--< rosCashieredByWriter -------------------------------------------------------------------
-- possible issue: returns no row for a writers ro that was closed but NOT cashiered BY a writer
-- old version returns a 0
DECLARE @date date;
@date = curdate() - 1;  
SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
  f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
  g.dayofmonth, g.sundaytosaturdayweek, 'rosCashieredByWriter', 0, howmany AS today,
  0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
--SELECT f.lastname, howmany
FROM (
  SELECT servicewriterkey, coalesce(COUNT(*), 0) AS howmany
  FROM ( -- 1 row per ro/writer 
    SELECT a.ro, a.servicewriterkey
    FROM dds.factRepairOrder a
    INNER JOIN dds.stgArkonaGLPTRNS b on a.ro = b.gtdoc#
    INNER JOIN dds.stgArkonaGLPDTIM c on b.gttrn# = c.gqtrn#
    WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
      AND a.storecode = 'RY1'
      AND NOT EXISTS (
        SELECT 1
        FROM dds.factRepairOrder m
        INNER JOIN dds.dimPaymentType n on m.paymenttypekey = n.paymenttypekey
        WHERE ro = a.ro
          AND n.paymenttypecode IN ('w','i'))
      AND NOT EXISTS (
        SELECT 1
        FROM dds.factRepairOrder o
        INNER JOIN dds.dimservicetype p on o.servicetypekey = p.servicetypekey
        WHERE ro = a.ro
          AND p.servicetypecode <> 'mr')
      AND c.gquser IN (SELECT dtUsername FROM ServiceWriters)
    GROUP BY a.ro, a.servicewriterkey) c 
  GROUP BY servicewriterkey) d 
INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
  AND e.CensusDept = 'MR'
INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
INNER JOIN dds.day g on g.thedate = @date;

--/> rosCashieredByWriter -------------------------------------------------------------------

--< rosClosedWithFlagHours -------------------------------------------------------------------  
DECLARE @date date;
@date = curdate() - 1;
--SELECT fullname, howmany
SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
  f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
  g.dayofmonth, g.sundaytosaturdayweek, 'rosClosedWithFlagHours', 0, howmany AS today,
  0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
FROM ( -- 1 row per writer w/# of ros
  SELECT servicewriterkey, COUNT(*) AS howmany
  FROM ( -- 1 row per ro/writer    
    SELECT a.ro, a.servicewriterkey
    FROM dds.factRepairOrder a
    WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
      AND roFlagHours > 0
    GROUP BY a.ro, a.servicewriterkey) c
  GROUP BY servicewriterkey) d  
INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
  AND e.censusdept = 'MR'
INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
INNER JOIN dds.day g on g.thedate = @date;
--/> rosClosedWithFlagHours -------------------------------------------------------------------

--< flagHours -------------------------------------------------------------------
DECLARE @date date;
@date = curdate() - 1;      
SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
  d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'flagHours', 0 AS seq, 
  a.flaghours AS today, 0 AS thisweek, 
  trim(cast(a.flaghours AS sql_char)), CAST(NULL AS sql_char)     
FROM (
  SELECT a.servicewriterkey, SUM(a.FlagHours) AS FlagHours
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.FinalCloseDateKey = b.DateKey
    AND b.theDate = @date
  GROUP BY a.ServiceWriterKey) a
INNER JOIN dds.day b on b.thedate = @date
INNER JOIN dds.dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
  AND c.CensusDept = 'MR'
INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber;     
--/> flagHours -------------------------------------------------------------------

--< warranty ros -------------------------------------------------------------------
-- any final closed ro with a warranty line that does NOT already exist IN WarrantyROs
SELECT distinct d.eeUserName, a.ro, b.thedate, e.FullName, e.HomePhone, e.BusinessPhone,
  e.CellPhone, f.Vin, TRIM(f.Make) + ' ' + TRIM(f.model), False
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
  AND b.datetype = 'Date'
  AND b.thedate between curdate() - 7 AND curdate()
INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
  AND c.CensusDept = 'MR'  
INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber 
INNER JOIN dds.dimCustomer e on a.CustomerKey = e.CustomerKey 
INNER JOIN dds.dimVehicle f on a.VehicleKey = f.VehicleKey
WHERE a.StoreCode = 'RY1'
  AND EXISTS (
    SELECT 1
    FROM dds.factRepairOrder r
    INNER JOIN dds.dimPaymentType s on r.PaymentTypeKey = s.PaymentTypeKey
      AND s.PaymentTypeCode = 'W'
    WHERE ro = a.ro) 
  AND NOT EXISTS (
    SELECT 1
    FROM WarrantyROs 
    WHERE ro = a.RO);   
--/> warranty ros -------------------------------------------------------------------

--< labor sales -------------------------------------------------------------------
-- labor sales BY writer BY day
DECLARE @date date;
@date = curdate() - 1;  
SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
  d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
  round(cast(abs(a.laborsales) AS sql_double), 0) AS today,
  0, 
  trim(cast(coalesce(abs(round(CAST(a.laborsales AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
  CAST(NULL AS sql_char)
FROM (        
  SELECT a.ServiceWriterKey, SUM(LaborSales) AS LaborSales
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.FinalCloseDateKey = b.datekey
    AND b.thedate = @date
  GROUP BY a.ServiceWriterKey) a 
INNER JOIN dds.day b on b.thedate = @date
INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
  AND c.CensusDept = 'MR'
INNER JOIN ServiceWriters d on c.StoreCode = d.StoreCode
  and c.WriterNumber = d.WriterNumber;     
--/> labor sales -------------------------------------------------------------------

--< ROs Opened -------------------------------------------------------------------
-- ros opened BY writer BY day
DECLARE @date date;
@date = curdate() - 1;      
SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
  d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
  b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Opened', 6 AS seq, a.HowMany AS today,
  0 AS thisWeek, trim(CAST(a.HowMany AS sql_char)), CAST(NULL AS sql_char)  
FROM (      
  SELECT ServiceWriterKey, COUNT(*) AS HowMany
  FROM (  
    SELECT a.ServiceWriterKey, a.ro
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.OpenDateKey = b.datekey
      AND b.thedate = @date
    GROUP BY a.ServiceWriterKey, a.ro) x
  GROUP BY ServiceWriterKey) a 
INNER JOIN dds.day b on b.thedate = @date
INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
  AND c.CensusDept = 'MR'
INNER JOIN ServiceWriters d on c.StoreCode = d.StoreCode
  and c.WriterNumber = d.WriterNumber;         
--/> ROs Opened -------------------------------------------------------------------

--< RY2ROsForCalls -------------------------------------------------------------------
SELECT LEFT(d.description, position(' ' IN description) - 1),  a.ro, a.thedate AS CloseDate, 
  b.fullname AS Customer, 
  iif(b.HomePhone = '0', 'No Phone', b.HomePhone), 
  iif(b.BusinessPhone = '0', 'No Phone', b.BusinessPhone),  
  iif(b.CellPhone = '0', 'No Phone', b.CellPhone),
  c.vin, trim(TRIM(c.make) + ' ' + TRIM(c.model)) AS vehicle,
  false as FollowUpComplete
FROM (
  SELECT a.ro, b.thedate, a.customerkey, a.vehiclekey, a.servicewriterkey
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.FinalCloseDateKey = b.datekey
    AND b.thedate BETWEEN curdate() - 2 AND curdate()  
  WHERE a.StoreCode = 'ry2'
  GROUP BY a.ro, b.thedate, a.customerkey, a.vehiclekey, a.servicewriterkey) a
INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
  AND b.customerkey <> 2
INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
  AND c.make = 'Nissan'
INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
LEFT JOIN ServiceWriters e on d.writernumber = e.writernumber
WHERE NOT EXISTS (
  SELECT 1
  FROM RY2ROsForCalls
  WHERE ro = a.ro);
--/> RY2ROsForCalls -------------------------------------------------------------------


--< agedROS -------------------------------------------------------------------
--/> agedROS -------------------------------------------------------------------
--< agedROS -------------------------------------------------------------------
--/> agedROS -------------------------------------------------------------------
--< agedROS -------------------------------------------------------------------
--/> agedROS -------------------------------------------------------------------