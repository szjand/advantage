/*
7/29/2013
the last 2 weeks, saturday show up AS zeros ON the mgr page
this IS the stored PROCEDURE swMetricDataYesterday, altered to accept a specific
date rather than curdate() -1
9/17/13
  modified to use factRepairOrder (ala stored proce swMetricDataYesterday
12/26/13
  modified to use remodeld factrepairorder  
12/31/13
  fucked up on writers labor sales, double counting because of only storing
    laborsales at ro level
  avgHours/RO (flag hours) IS fucked up due to the changes AS well  
  
2/14/14
  redone to synchronize with sp.swMetricDataYesterday  
********************************************************************************  
  i am NOT enthusiastic about this working correctly, 
  too much shit changes compared to what should
  the main concern IS labor sales, just DO that section
********************************************************************************  
*/
DECLARE @date date;
@date = '02/05/2014';
TRY 
  BEGIN TRANSACTION;
  TRY
    -- AgedROs table ----------------------------------------------------------
    TRY
      DELETE FROM AgedROs;
      INSERT INTO AgedROs
      SELECT distinct e.username, a.ro, a.thedate, b.fullname AS customername, 
        a.rostatus AS status, c.vin, TRIM(c.make) + ' ' + trim(c.model) AS vehicle        
      FROM (
        SELECT a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey
        FROM dds.factRepairOrder a
        INNER JOIN dds.day b on a.opendatekey = b.datekey
          AND b.thedate < curdate() - 7
        INNER JOIN dds.dimRoStatus c on a.rostatuskey = c.rostatuskey
          AND c.rostatus <> 'Closed'
        GROUP BY a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey) a  
      INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
      INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
      INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
        AND d.CensusDept = 'MR'
      INNER JOIN ServiceWriters e on d.writernumber = e.writernumber;     
    CATCH ALL
      RAISE swMetricDataYesterday(99,__errtext);
    END TRY;
    -- ROs Closed ------------------------------------------------------------- 
    TRY  
      DELETE FROM ServiceWriterMetricData WHERE thedate = @date AND metric = 'ROs Closed';
      INSERT INTO ServiceWriterMetricData        
      SELECT f.username, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'ROs Closed', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM ( -- 1 row per writer w/# of ros
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.factRepairOrder a
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
          GROUP BY a.ro, a.servicewriterkey) c
        GROUP BY servicewriterkey) d  
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = @date;       
      -- weekly -------------------
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT username, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.username = a.username
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'ROs Closed') x
      WHERE ServiceWriterMetricData.username = x.username
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'ROs Closed';   
    CATCH ALL
      RAISE swMetricDataYesterday(100, __errtext);
    END TRY;
    -- rosClosedWithInspectionLine ------------------------------------------------    
    TRY     
      DELETE FROM ServiceWriterMetricData WHERE thedate = @date AND metric = 'rosClosedWithInspectionLine';  
      INSERT INTO ServiceWriterMetricData        
      SELECT f.username, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosClosedWithInspectionLine', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM ( -- 1 row per writer w/# of ros
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.factRepairOrder a
          INNER JOIN dds.dimOpcode b on a.opcodekey = b.opcodekey
            AND b.opcode IN ('23I', '24Z')
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
          GROUP BY a.ro, a.servicewriterkey) c
        GROUP BY servicewriterkey) d  
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = @date;
    CATCH ALL
      RAISE swMetricDataYesterday(101, __errtext);  
    END TRY;  
    -- rosCashiered ---------------------------------------------------------------
    TRY
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashiered' AND thedate = @date;
      INSERT INTO ServiceWriterMetricData        
      SELECT f.username, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosCashiered', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM (        
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.factRepairOrder a
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
            AND a.storecode = 'RY1'
            AND NOT EXISTS (
              SELECT 1
              FROM dds.factRepairOrder m
              INNER JOIN dds.dimPaymentType n on m.paymenttypekey = n.paymenttypekey
              WHERE ro = a.ro
                AND n.paymenttypecode IN ('w','i'))
            AND NOT EXISTS (
              SELECT 1
              FROM dds.factRepairOrder o
              INNER JOIN dds.dimservicetype p on o.servicetypekey = p.servicetypekey
              WHERE ro = a.ro
                AND p.servicetypecode <> 'mr')          
            AND EXISTS (
              SELECT 1
              FROM dds.stgArkonaGLPTRNS
              WHERE gtdoc# = a.ro)
          GROUP BY a.ro, a.servicewriterkey) c   
        GROUP BY servicewriterkey) d
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = @date;  
    CATCH ALL
      RAISE swMetricDataYesterday(102, __errtext);  
    END TRY;  
    -- rosCashieredByWriter -------------------------------------------------------
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashieredByWriter' AND thedate = @date;
      INSERT INTO ServiceWriterMetricData        
      SELECT f.username, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosCashieredByWriter', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      --SELECT f.lastname, howmany
      FROM (
        SELECT servicewriterkey, coalesce(COUNT(*), 0) AS howmany
        FROM ( -- 1 row per ro/writer 
          SELECT a.ro, a.servicewriterkey
          FROM dds.factRepairOrder a
          INNER JOIN dds.stgArkonaGLPTRNS b on a.ro = b.gtdoc#
          INNER JOIN dds.stgArkonaGLPDTIM c on b.gttrn# = c.gqtrn#
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
            AND a.storecode = 'RY1'
            AND NOT EXISTS (
              SELECT 1
              FROM dds.factRepairOrder m
              INNER JOIN dds.dimPaymentType n on m.paymenttypekey = n.paymenttypekey
              WHERE ro = a.ro
                AND n.paymenttypecode IN ('w','i'))
            AND NOT EXISTS (
              SELECT 1
              FROM dds.factRepairOrder o
              INNER JOIN dds.dimservicetype p on o.servicetypekey = p.servicetypekey
              WHERE ro = a.ro
                AND p.servicetypecode <> 'mr')
            AND c.gquser IN (SELECT dtUsername FROM ServiceWriters)
          GROUP BY a.ro, a.servicewriterkey) c 
        GROUP BY servicewriterkey) d 
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = @date;   
    CATCH ALL
      RAISE swMetricDataYesterday(103, __errtext);  
    END TRY;  
    -- rosClosedWithFlagHours -----------------------------------------------------     
    TRY  
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosClosedWithFlagHours' AND thedate = @date;
      INSERT INTO ServiceWriterMetricData        
      SELECT f.username, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosClosedWithFlagHours', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM ( -- 1 row per writer w/# of ros
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.factRepairOrder a
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = @date)
            AND roFlagHours > 0
          GROUP BY a.ro, a.servicewriterkey) c
        GROUP BY servicewriterkey) d  
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.censusdept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = @date;       
      -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT username, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.username = a.username
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'rosClosedWithFlagHours') x
      WHERE ServiceWriterMetricData.username = x.username
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;       
    CATCH ALL
      RAISE swMetricDataYesterday(104, __errtext);  
    END TRY; 
    -- flaghours ------------------------------------------------------------------  
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'flagHours' AND thedate = @date;
      INSERT INTO ServiceWriterMetricData        
      SELECT d.username, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'flagHours', 0 AS seq, 
        a.flaghours AS today, 0 AS thisweek, 
        trim(cast(a.flaghours AS sql_char)), CAST(NULL AS sql_char)     
      FROM (
        SELECT a.servicewriterkey, SUM(a.FlagHours) AS FlagHours
        FROM dds.factRepairOrder a
        INNER JOIN dds.day b on a.FinalCloseDateKey = b.DateKey
          AND b.theDate = @date
        GROUP BY a.ServiceWriterKey) a
      INNER JOIN dds.day b on b.thedate = @date
      INNER JOIN dds.dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
        AND c.CensusDept = 'MR'
      INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber; 
      -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT username, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.username = a.username
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'flagHours') x
      WHERE ServiceWriterMetricData.username = x.username
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric; 
    CATCH ALL
      RAISE swMetricDataYesterday(105, __errtext);  
    END TRY; 
    -- Cashiering -----------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosCashieredByWriter & 
      ServiceWriterMetricData.rosCashiered to be complete
      generate weekly totals before generating display metric (cashiering)
    */
    TRY     
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT username, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.username = a.username
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric in ('rosCashiered','rosCashieredByWriter')) x
      WHERE ServiceWriterMetricData.username = x.username
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN ('rosCashiered','rosCashieredByWriter') ; 
        
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Cashiering';
      INSERT INTO ServiceWriterMetricData
      SELECT a.username, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Cashiering', 1 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.username = b.username
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosCashieredByWriter'
        AND b.metric = 'rosCashiered';
    CATCH ALL
      RAISE swMetricDataYesterday(106, __errtext);  
    END TRY;    
    -- Open Aged ROs --------------------------------------------------------------------
    /*
    UPDATE throughout the day IN terms of removing ros that get closed
    DELETE FROM agedros daily based ON an ro closing
    overnight a full scrap
    AgedROs TABLE IS necessary for the nightly/today to run
    
    no need to UPDATE with newly aged ros thru out the day
    
    need to UPDATE the weekly numbers here
    ALL i need are this week, which will be the same AS the daily total each day
    */
    TRY    
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Aged ROs' AND thedate = @date ;
      INSERT INTO ServiceWriterMetricData        
      SELECT c.username, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
        a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Aged ROs', 2 AS seq, b.howmany AS today,
      --  0 AS thisWeek, trim(CAST(b.howmany AS sql_char)), CAST(NULL AS sql_char)
        howmany thisWeek, trim(CAST(howmany AS sql_char)) AS todayDisplay, 
        trim(CAST(howmany AS sql_char)) AS thisWeekDisplay
      FROM dds.day a, ( 
        SELECT username, COUNT(*) AS howmany
        FROM agedros  
        GROUP BY username) b, 
        servicewriters c
      WHERE a.thedate = @date  
        AND b.username = c.username;
    CATCH ALL
      RAISE swMetricDataYesterday(107, __errtext);  
    END TRY;  
    -- Open Warranty Calls --------------------------------------------------------
    /*
      the challenges here, like AgedROs, the data depends ON dds.factro, 
        sco.factrotoday & sco.WarrantyROs
        backfill FROM dds.factro
    */
    TRY
      INSERT INTO WarrantyROS 
      SELECT distinct d.username, a.ro, b.thedate, e.FullName, e.HomePhone, e.BusinessPhone,
        e.CellPhone, f.Vin, TRIM(f.Make) + ' ' + TRIM(f.model), False
      FROM dds.factRepairOrder a
      INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
        AND b.datetype = 'Date'
        AND b.thedate between curdate() - 7 AND curdate()
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'  
      INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber 
      INNER JOIN dds.dimCustomer e on a.CustomerKey = e.CustomerKey 
      INNER JOIN dds.dimVehicle f on a.VehicleKey = f.VehicleKey
      WHERE a.StoreCode = 'RY1'
        AND EXISTS (
          SELECT 1
          FROM dds.factRepairOrder r
          INNER JOIN dds.dimPaymentType s on r.PaymentTypeKey = s.PaymentTypeKey
            AND s.PaymentTypeCode = 'W'
          WHERE ro = a.ro) 
        AND NOT EXISTS (
          SELECT 1
          FROM WarrantyROs 
          WHERE ro = a.RO);          
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Warranty Calls' AND thedate = @date;
      INSERT INTO ServiceWriterMetricData      
      SELECT a.username, a.storecode, a.fullname, a.lastname, a.firstname,
        a.writerNumber, c.datekey, c.thedate, c.dayOfWeek, c.dayName,
        c.dayOfMonth, c.sundayToSaturdayWeek, 'Open Warranty Calls', 3 AS seq, coalesce(b.howmany, 0) AS today,
        coalesce(b.howmany, 0) AS thisWeek, trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS todayDisplay, 
        trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS thisWeekDisplay
      FROM servicewriters a  
      LEFT JOIN (
        SELECT username, COUNT(*) AS howmany
        FROM warrantyROs 
        WHERE followUpComplete = false
        GROUP BY username) b ON a.username = b.username
      LEFT JOIN dds.day c ON 1 = 1
        AND c.thedate = @date  
      WHERE a.active = true
      AND a.storecode = 'ry1'; -- ry2 mod  
    CATCH ALL
      RAISE swMetricDataYesterday(108, __errtext);  
    END TRY;   
    -- Avg Hours/RO ---------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosClosedWithFlagHours & ServiceWriterMetricData.flagHours to be complete
      different than cashiering AND insp requested, those weekly numbers are based ON discreet
      values that get updated at the END, a simple concantenation of the discreet values
      this IS a ratio
    */
    TRY    
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Avg Hours/RO';
      INSERT INTO ServiceWriterMetricData
      SELECT a.username, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Avg Hours/RO', 4 AS seq, 
        round(a.today/b.today, 1) AS today,
        round(a.thisweek/b.thisweek, 1) AS thisweek,  
        left(CAST(cast(round(a.today/b.today, 1) as sql_double) AS sql_char), 12)  AS todaydisplay,
        left(cast(CAST(round(a.thisweek/b.thisweek, 1) AS sql_double) AS sql_char), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.username = b.username
        AND a.thedate = b.thedate
      WHERE a.metric = 'flagHours'
        AND b.metric = 'rosClosedWithFlagHours';
    CATCH ALL
      RAISE swMetricDataYesterday(109, __errtext);      
    END TRY; 
    -- LaborSales -----------------------------------------------------------------     
    TRY       
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Labor Sales' AND thedate = @date;
      INSERT INTO ServiceWriterMetricData        
      SELECT d.username, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
        round(cast(abs(a.laborsales) AS sql_double), 0) AS today,
        0, 
        trim(cast(coalesce(abs(round(CAST(a.laborsales AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
        CAST(NULL AS sql_char)
      FROM (        
-- *a*	     
--      SELECT a.ServiceWriterKey, SUM(RoLaborSales) AS LaborSales
  	    SELECT a.ServiceWriterKey, SUM(laborSales) AS LaborSales
        FROM  dds.factRepairOrder a
        INNER JOIN dds.day b on a.FinalCloseDateKey = b.datekey
           AND b.thedate = @date
        GROUP BY a.ServiceWriterKey) a 
      INNER JOIN dds.day b on b.thedate = @date
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'
      INNER JOIN ServiceWriters d on c.StoreCode = d.StoreCode
        and c.WriterNumber = d.WriterNumber; 
    CATCH ALL
      RAISE swMetricDataYesterday(110, __errtext);      
    END TRY;   
    -- rosOpened ------------------------------------------------------------------ 
    TRY      
      DELETE FROM ServiceWriterMetricData WHERE metric = 'ROs Opened' AND thedate = @date ;
      INSERT INTO ServiceWriterMetricData        
      SELECT d.username, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Opened', 6 AS seq, a.HowMany AS today,
        0 AS thisWeek, trim(CAST(a.HowMany AS sql_char)), CAST(NULL AS sql_char)  
      FROM (      
        SELECT ServiceWriterKey, COUNT(*) AS HowMany
        FROM (  
          SELECT a.ServiceWriterKey, a.ro
          FROM dds.factRepairOrder a
          INNER JOIN dds.day b on a.OpenDateKey = b.datekey
            AND b.thedate = @date
          GROUP BY a.ServiceWriterKey, a.ro) x
        GROUP BY ServiceWriterKey) a 
      INNER JOIN dds.day b on b.thedate = @date
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'
      INNER JOIN ServiceWriters d on c.StoreCode = d.StoreCode
        and c.WriterNumber = d.WriterNumber;     
    CATCH ALL
      RAISE swMetricDataYesterday(111, __errtext);      
    END TRY;     
    -- Inspections Requested ------------------------------------------------------
    /*
      requires ServiceWriterMetricData.ROs Closed & 
      ServiceWriterMetricData.rosClosedWithInspectionLine to be complete
      generate weekly totals before generating display metric (cashiering)
    */
    TRY    
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT username, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.username = a.username
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'rosClosedWithInspectionLine') x
      WHERE ServiceWriterMetricData.username = x.username
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'rosClosedWithInspectionLine'; 
      
      
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Inspections Requested';
      INSERT INTO ServiceWriterMetricData
      SELECT a.username, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Inspections Requested', 7 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.username = b.username
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosClosedWithInspectionLine'
        AND b.metric = 'ROs Closed';
    CATCH ALL
      RAISE swMetricDataYesterday(112, __errtext);      
    END TRY;         
    TRY
    -- UPDATE cumulative metrics only with weekly totals
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT username, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.username = a.username
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE EXISTS (
          SELECT 1
          FROM ServiceWriterMetrics
          WHERE metric = a.metric
            AND cumulative = true)) x
      WHERE ServiceWriterMetricData.username = x.username
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN (
        SELECT metric
        FROM servicewritermetrics
        WHERE cumulative = true); 
    CATCH ALL
      RAISE swMetricDataYesterday(113, __errtext);      
    END TRY;                         
  COMMIT WORK;
  CATCH ALL
    ROLLBACK WORK;
    RAISE;
  END TRY;  
CATCH ALL
  RAISE;
END TRY;    





