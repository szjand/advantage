SELECT *
FROM tmpRoCalls

select * FROM warrantyros ORDER BY closedate

select * FROM tmprophonenumbers

DROP TABLE #wtf;
select a.ro, coalesce(trim(b.lastname), a.username) AS writer, a.closeDate,
  c.updateDate, c.description, d.lastname AS updatedBy,
  e.callStartTS, e.callDuration, e.recordingFileName
-- INTO #wtf  
FROM warrantyros a
LEFT JOIN tpEmployees b on a.username = b.username -- writer
LEFT JOIN ( -- last UPDATE to warranty ro
  SELECT k.username, k.ro, cast(k.callts AS sql_date) AS updateDate, k.description 
  FROM warrantycalls k
  WHERE callts = (
    SELECT MAX(callts)
    FROM warrantycalls
    WHERE ro = k.ro
    GROUP BY ro)) c on a.ro = c.ro
LEFT JOIN tpEmployees d on c.username = d.username -- updated by
LEFT JOIN tmpRoCalls e on a.ro = e.ro
  AND calldate BETWEEN a.closedate AND c.updateDate + 3 --------
WHERE a.closedate > '04/23/2014'  
  AND followUpComplete = true
ORDER BY a.closedate DESC, a.ro
  

SELECT a.*, b.phoneNos
FROM #wtf a
LEFT JOIN (
  select ro, count(*) phoneNos
  from tmpRoPhoneNumbers
  GROUP BY ro) b on a.ro = b.ro
WHERE recordingFileName IS NOT null  
ORDER BY a.closedate 

-- initiall greg IS saying 90 days worth
select * FROM warrantyros WHERE followupcomplete = true
SELECT COUNT(*) FROM warrantyros WHERE followupcomplete = true-- 4994
SELECT COUNT(*) FROM warrantyros WHERE followupcomplete = true AND closedate > curdate() - 90 -- 1054
SELECT COUNT(*) FROM warrantyros WHERE followupcomplete = true AND closedate > curdate() - 60 -- 692

SELECT curdatE() - 60 FROM system.iota -- 4/18/14

select MIN(calldate) FROM tmpRoCalls -- earliest call data: 4/10/14
SELECT * FROM tmpRoCalls ORDER BY calldate

-- include concatenated updates
-- ros with multiple updates
SELECT a.* 
FROM warrantycalls a
INNER JOIN (
   SELECT ro
   FROM warrantycalls
   GROUP BY ro
   HAVING COUNT(*) > 1) b on a.ro = b.ro
ORDER BY a.ro   

-- looks LIKE i am getting double rows IN warranty calls
select * FROM warrantycalls WHERE ro = '16148139'
SELECT * FROM warrantyros WHERE ro = '16148139'

barring a bug, guessing, writer makes  UPDATE, forgot to check complete, goes back
AND checks complete?

test that with ro 16155496

select * FROM warrantycalls WHERE ro = '16155496'

nope, different messages, WHEN complete IS checked, Complete IS IN the description
AND the dups i am seeing are dups IN ALL fields, none of which contain complete

DELETE FROM warrantycalls WHERE ro = '16155496';
UPDATE warrantyros SET followupcomplete = false WHERE ro = '16155496';

TRY updating with LEFT message, THEN IN the second one, check complete, 
  but also SELECT LEFT message, 
yep, that did it, LEFT message IN the second UPDATE, description IN second
  call IS just LEFT message
  
i think that IS done IN the code, stored proc just inserts whatever description 
  passed to it
  fucking code IS tail chasing annoying beyond my patience for now
  
so for now, GROUP BY ro,statusUpdateWithDateName

SELECT ro, statusUpdateWithDateName
FROM warrantyCalls
GROUP BY ro, statusUpdateWithDateName

SELECT *
FROM warrantyros a
  LEFT JOIN (
  SELECT ro, statusUpdateWithDateName
  FROM warrantyCalls
  GROUP BY ro, statusUpdateWithDateName) b on a.ro = b.ro
LEFT JOIN   
 
  
-- main page, ro info AND number of calls made BETWEEN CLOSE date AND CLOSE date + 10
SELECT a.ro, trim(b.firstname) + ' ' + b.lastname AS writer, a.closedate, a.customer, a.vehicle, 
  coalesce(c.calls, 0) calls
FROM warrantyros a
LEFT JOIN tpEmployees b on a.username = b.username
LEFT JOIN (
  SELECT ro, COUNT(*) AS calls 
  FROM tmpRoCalls k
  WHERE ro IN (
    SELECT ro
    FROM warrantyros
    WHERE closedate BETWEEN curdate() - 60 AND curdate()) 
  AND calldate BETWEEN 
    (SELECT closedate FROM warrantyros WHERE ro = k.ro)
    AND
    (SELECT closedate + 10 FROM warrantyros WHERE ro = k.ro)
  GROUP BY ro) c on a.ro = c.ro
WHERE a.closedate BETWEEN curdate() - 60 AND curdate() 
  AND a.followUpComplete = true  
ORDER BY a.ro DESC   
  
-- detail page, modal with the calls
DECLARE @ro string;
@ro = '16151988'; -- (SELECT ro FROM __input);
-- INSERT INTO __output
SELECT top 100 @ro, callStartTS, callDuration, recordingFileName
FROM tmpRoCalls
WHERE ro = @ro
  AND callDate BETWEEN fcDate AND fcDate + 10
-- *a*
  AND callDuration > 1
ORDER BY callStartTS DESC;

-- 6/19/14
/*
we have a spec, one page shows ro, writer, ro closedate, customer, completed BY, 
  completed on AND a player for each call, OR no phone calls made
2 stored proc calls
1: everything except the calls
2: the calls
*/  
-- basic stuff
SELECT a.ro, trim(b.firstname) + ' ' + b.lastname AS writer, a.closedate, 
  a.customer, coalesce(c.calls, 0) calls
FROM warrantyros a
LEFT JOIN tpEmployees b on a.username = b.username
LEFT JOIN (
  SELECT ro, COUNT(*) AS calls 
  FROM tmpRoCalls k
  WHERE ro IN (
    SELECT ro
    FROM warrantyros
    WHERE closedate BETWEEN curdate() - 60 AND curdate()) 
  AND calldate BETWEEN 
    (SELECT closedate FROM warrantyros WHERE ro = k.ro)
    AND
    (SELECT closedate + 10 FROM warrantyros WHERE ro = k.ro)
  GROUP BY ro) c on a.ro = c.ro
WHERE a.closedate BETWEEN curdate() - 60 AND curdate() 
  AND a.followUpComplete = true  

-- ADD the completed stuff
SELECT TRIM(b.firstname) + ' ' + b.lastname, CAST(callTS AS sql_date) 
FROM warrantycalls a
LEFT JOIN tpEmployees b on a.username = b.username
WHERE ro IN (
  SELECT ro
  FROM warrantyRos
  WHERE closedate BETWEEN curdate() - 60 AND curdate())
AND callts = (
  SELECT MAX(callts)
  FROM warrantyCalls
  WHERE ro = a.ro)  


-- this IS the fastest BY far (subquery for COUNT IN SELECT)    
SELECT a.ro, trim(b.firstname) + ' ' + b.lastname AS writer, a.closedate, 
  a.customer, TRIM(f.firstname) + ' ' + f.lastname AS completedBy, 
  CAST(e.callTs AS sql_date) AS completedOn,
  (SELECT COUNT(*) 
     FROM tmpRoCalls 
     WHERE ro = a.ro 
       AND callDate BETWEEN CAST(a.closeDate AS sql_date) 
         AND CAST(a.closeDate AS sql_date) + 10) AS callCount
FROM warrantyros a
LEFT JOIN tpEmployees b on a.username = b.username
LEFT JOIN warrantycalls e on a.ro = e.ro
  AND e.callts = (
    SELECT MAX(callts)
    FROM warrantyCalls
    WHERE ro = a.ro) 
LEFT JOIN tpEmployees f on e.username = f.username    
WHERE a.closedate BETWEEN curdate() - 60 AND curdate() 
  AND a.followUpComplete = true    
