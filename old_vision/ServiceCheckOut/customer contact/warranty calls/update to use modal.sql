v2.8.0

/*
DO some clean up
-- NOT needed anymore
EXECUTE PROCEDURE sp_RenameDDObject( 'getWarrantyRoPhoneCallsAllAtOnce', 'zUnused_getWarrantyRoPhoneCallsAllAtOnce', 10 , 0);
-- replace with getCurrentRoPhoneCalls
EXECUTE PROCEDURE sp_RenameDDObject( 'getRoPhoneCalls', 'zUnused_getRoPhoneCalls', 10 , 0);
*/

-- 1. ALL Completed Warranty Calls page
alter PROCEDURE getWarrantyRosCompleted
   ( 
      ro CICHAR ( 9 ) OUTPUT,
      writer CICHAR ( 52 ) OUTPUT,
      closeDate DATE OUTPUT,
      customer CICHAR ( 50 ) OUTPUT,
      completedBy CICHAR ( 52 ) OUTPUT,
      completedOn DATE OUTPUT,
      callCount Integer OUTPUT,
      status Memo OUTPUT, 
	  lastContactTs timestamp output,
	  lastContactDuration integer output 
   ) 
BEGIN 
/*
6/26/14
  reduce to 5 days worth
6/29/14
  AS part of refactoring warranty calls completed to use the modal, this now
  returns the most recent status UPDATE AS part of the output 
  AND return last call ts & duration
  AND limit calls (AND COUNT of calls) to those for which there IS a recording 
  upped the list to 10 days worth of completed warranty calls
  
SELECT * FROM (EXECUTE PROCEDURE getWarrantyRosCompleted()) a ORDER BY callCount

SELECT COUNT(*) FROM (EXECUTE PROCEDURE getWarrantyRosCompleted()) a 

SELECT * FROM (EXECUTE PROCEDURE getWarrantyRosCompleted()) a WHERE ro = '16156506'
  
EXECUTE PROCEDURE getWarrantyRosCompleted() ;


*/
INSERT INTO __output
SELECT top 1000 a.ro, trim(b.firstname) + ' ' + b.lastname AS writer, a.closedate, 
  a.customer, TRIM(f.firstname) + ' ' + f.lastname AS completedBy, 
  CAST(e.callTs AS sql_date) AS completedOn,
  (SELECT COUNT(*) 
     FROM tmpRoCalls 
     WHERE ro = a.ro 
	   AND recordingFileName IS NOT NULL 
	   AND recordingFileName NOT LIKE '//GF-H%'
       AND callDate BETWEEN CAST(a.closeDate AS sql_date) 
         AND CAST(a.closeDate AS sql_date) + 5) AS callCount,
  e.description,
  g.callStartTs,
  g.callDuration  		 
FROM warrantyros a
LEFT JOIN tpEmployees b on a.username = b.username
LEFT JOIN warrantycalls e on a.ro = e.ro
  AND e.callts = (
    SELECT MAX(callts)
    FROM warrantyCalls
    WHERE ro = a.ro) 
LEFT JOIN tmpRoCalls g on a.ro = g.ro  
  and g.callStartTS = (-- most recent call
    SELECT MAX(callStartTS)
    FROM tmpRoCalls
    WHERE ro = a.ro
	  AND recordingFileName IS NOT NULL 
	  AND recordingFileName NOT LIKE '//GF-H%'
      AND callDate BETWEEN CAST(a.closeDate AS sql_date) AND CAST(a.closeDate AS sql_date) + 10)	
LEFT JOIN tpEmployees f on e.username = f.username    
WHERE a.closedate BETWEEN curdate() - 10 AND curdate()
  AND a.followUpComplete = true
ORDER BY e.callTs DESC;    

END;

-- 2. Modal

ALTER PROCEDURE getWarrantyRoPhoneCalls( 
      ro CICHAR ( 9 ),
	  infoType cichar(6) output,
      ro CICHAR ( 9 ) OUTPUT,	  	   
	  customer cichar(50) output,
	  vehicle cichar(60) output,	  
	  updateTs timestamp output,
	  updateBy cichar(52)  output,
	  status cichar(250) output,	  
      callStartTS TIMESTAMP OUTPUT,
      callDuration Integer OUTPUT,
      recordingFileName CICHAR ( 100 ) OUTPUT) 
BEGIN 
/*
6/26/14
  reduce to calls made within 5 days of final CLOSE date 

6/29/14
  refactor to use modal
  limit to calls for which there IS a recording  
  include ro info & info FROM status udpates
    
EXECUTE PROCEDURE getWarrantyRoPhoneCalls('16156653');
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output
SELECT top 100 *
FROM (
  SELECT 'RO' AS infoType, ro, customer, vehicle, 
    CAST(NULL AS sql_timestamp) AS updateTs, 
    left(cast(NULL as sql_char), 52) AS updateBy,
    left(cast(NULL as sql_char), 250) AS status,
    CAST(NULL AS sql_timestamp) AS callStartTs, 
    CAST(NULL AS sql_integer) AS callDuration,
    left(CAST(NULL AS sql_char), 100) AS recordingFileName
  FROM warrantyRos
  WHERE ro = @ro 
  UNION 
  SELECT 'STATUS', left(cast(null as sql_char), 9) as ro, 
    left(cast(NULL as sql_char), 50) AS customer, 
    left(cast(NULL as sql_char), 60) AS vehicle,  
    callTs AS updateTs, n.fullname AS updateBy, left(trim(description), 250) AS status,
    CAST(NULL AS sql_timestamp) AS callStartTs, 
    CAST(NULL AS sql_integer) AS callDuration,
    left(CAST(NULL AS sql_char), 100) AS recordingFileName  
  FROM warrantyCalls m
  LEFT JOIN tpEmployees n on m.username = n.username 
  WHERE ro = @ro
  UNION 
  SELECT 'CALL', left(cast(null as sql_char), 9) as ro, 
    left(cast(NULL as sql_char), 50) AS customer,
    left(cast(NULL as sql_char), 60) AS vehicle, 
    CAST(NULL AS sql_timestamp) AS updateTS, 
    left(cast(NULL as sql_char), 52) AS updateBy,
    left(cast(NULL as sql_char), 250) AS status,
    a.callStartTS, a.callDuration, a.recordingFileName
  FROM tmpRoCalls a
  WHERE ro = @ro
    AND recordingFileName IS NOT NULL 
	AND recordingFileName NOT LIKE '//GF-H%'
    AND callDuration > 1 -- exclude 1 second calls
    AND callDate BETWEEN fcDate AND fcDate + 5) x
ORDER BY updatets DESC, callStartTS DESC;	
END;


-- change the modal for current ros AS well
CREATE PROCEDURE getCurrentRoPhoneCalls( 
      ro CICHAR ( 9 ),
	  infoType cichar(6) output,
      ro CICHAR ( 9 ) OUTPUT,	  	   
	  customer cichar(50) output,
	  vehicle cichar(60) output,	  
	  updateTs timestamp output,
	  updateBy cichar(52)  output,
	  status cichar(250) output,	  
      callStartTS TIMESTAMP OUTPUT,
      callDuration Integer OUTPUT,
      recordingFileName CICHAR ( 100 ) OUTPUT) 
BEGIN 
/*
6/29/14
  replaces sp getRoPhoneCalls, fashioned so that the modal matches that IN 
  warranty calls
  
EXECUTE PROCEDURE getCurrentRoPhoneCalls('16157414'); 
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output
SELECT top 100 *
FROM (
  SELECT 'RO' AS infoType, ro, customer, vehicle, 
    CAST(NULL AS sql_timestamp) AS updateTs, 
    left(cast(NULL as sql_char), 52) AS updateBy,
    left(cast(NULL as sql_char), 250) AS status,
    CAST(NULL AS sql_timestamp) AS callStartTs, 
    CAST(NULL AS sql_integer) AS callDuration,
    left(CAST(NULL AS sql_char), 100) AS recordingFileName
  FROM currentRos
  WHERE ro = @ro 
  UNION 
  SELECT 'STATUS', left(cast(null as sql_char), 9) as ro, 
    left(cast(NULL as sql_char), 50) AS customer, 
    left(cast(NULL as sql_char), 60) AS vehicle,  
    updateTs AS updateTs, n.fullname AS updateBy, left(trim(statusUpdate), 250) AS status,
    CAST(NULL AS sql_timestamp) AS callStartTs, 
    CAST(NULL AS sql_integer) AS callDuration,
    left(CAST(NULL AS sql_char), 100) AS recordingFileName  
  FROM agedRoUpdates m
  LEFT JOIN tpEmployees n on m.username = n.username 
  WHERE m.ro = @ro
  UNION 
  SELECT 'CALL', left(cast(null as sql_char), 9) as ro, 
    left(cast(NULL as sql_char), 50) AS customer,
    left(cast(NULL as sql_char), 60) AS vehicle, 
    CAST(NULL AS sql_timestamp) AS updateTS, 
    left(cast(NULL as sql_char), 52) AS updateBy,
    left(cast(NULL as sql_char), 250) AS status,
    a.callStartTS, a.callDuration, a.recordingFileName
  FROM tmpRoCalls a
  WHERE ro = @ro
    AND recordingFileName IS NOT NULL 
	AND recordingFileName NOT LIKE '//GF-H%'
    AND callDuration > 1) x -- exclude 1 second calls
ORDER BY updatets DESC, callStartTS DESC;	
END;

