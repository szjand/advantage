select a.ro, a.customerphonenumber, a.fcdate, b.starttime, b.endtime, b.durationSeconds,
  c.connecttime, c.disconnecttime, c.talktimeSeconds,c.holdTimeSeconds,
  c.ringTimeSeconds, c.durationSeconds
FROM tmprophonenumbers a
LEFT JOIN sh.extcall b on a.customerPhoneNumber = right(TRIM(dialedNumber), 10)
LEFT JOIN sh.extconnect c on b.id = c.calltableid
WHERE a.ro = '16155694'