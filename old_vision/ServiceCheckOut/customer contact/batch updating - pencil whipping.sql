/*
6/23/14
now that we know that the warranty calls are NOT actually being made, but
the list IS being pencil whipped, ie, checked complete even though a phone 
call has NOT been made

for cahalan
  he IS interested IN seeing IF these are being completed IN batches, 
	ie, 
*/
-- the last, AND therefor presumably the completing entry
select *
FROM warrantyCalls a
WHERE callTS = (
  SELECT MAX(callTS)
	FROM warrantyCalls
	WHERE ro = a.ro)
	
select CAST(callTS AS sql_date), userName, COUNT(*) updates
FROM warrantyCalls a
WHERE CAST(callTS AS sql_date) > '01/01/2014' -- 2014 only
  AND callTS = (
    SELECT MAX(callTS)
  	FROM warrantyCalls
  	WHERE ro = a.ro)	
GROUP BY CAST(callTS AS sql_date), userName	

-- DROP TABLE #wtf;
SELECT *
INTO #wtf -- days AND updaters WHERE more than 15 ros were checked complete
FROM (
  select CAST(callTS AS sql_date) theDate, userName, COUNT(*) updates
  FROM warrantyCalls a
  WHERE CAST(callTS AS sql_date) > '01/01/2014' -- 2014 only
    AND callTS = (
      SELECT MAX(callTS)
    	FROM warrantyCalls
    	WHERE ro = a.ro)	
  GROUP BY CAST(callTS AS sql_date), userName) x WHERE updates > 15


select * FROM #wtf ORDER BY updates DESC 	

SELECT a.*
FROM warrantyCalls a 
INNER JOIN #wtf b ON CAST(a.callTS AS sql_date) = b.theDate
  AND a.username = b.username
WHERE b.thedate = '06/16/2014'
  AND a.username = 'rodt@rydellchev.com'	
ORDER BY a.callTS	

-- hmmm, GROUP BY day AND hour

select CAST(callTS AS sql_date), hour(callTS) AS theHour, userName, COUNT(*) as updates
FROM warrantyCalls a
WHERE CAST(callTS AS sql_date) > '01/01/2014' -- 2014 only
  AND callTS = (
    SELECT MAX(callTS)
  	FROM warrantyCalls
  	WHERE ro = a.ro)	
GROUP BY CAST(callTS AS sql_date), hour(callTS), userName	
ORDER BY COUNT(*) DESC 

-- DROP TABLE #wtf;
SELECT *
INTO #wtf -- days AND updaters WHERE more than 15 ros were checked complete
FROM (
  select CAST(callTS AS sql_date) AS theDate, hour(callTS) AS theHour,
	  userName, COUNT(*) as updates
  FROM warrantyCalls a
  WHERE CAST(callTS AS sql_date) > '01/01/2014' -- 2014 only
    AND callTS = (
      SELECT MAX(callTS)
    	FROM warrantyCalls
    	WHERE ro = a.ro)	
  GROUP BY CAST(callTS AS sql_date), hour(callTS), userName) x WHERE updates > 10
	
SELECT *
FROM #wtf	
ORDER BY updates DESC 	

SELECT a.*
FROM warrantyCalls a 
INNER JOIN #wtf b ON CAST(a.callTS AS sql_date) = b.theDate
  AND hour(a.callts) = b.theHour
  AND a.username = b.username
WHERE b.thedate = '03/03/2014'
  AND a.username = 'rodt@rydellchev.com'	
	AND b.theHour = 15
ORDER BY a.callTS	
