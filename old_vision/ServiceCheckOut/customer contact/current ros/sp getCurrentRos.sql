ALTER PROCEDURE getCurrentROs( 
      userName CICHAR ( 50 ),
      firstName CICHAR ( 20 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      openDate CICHAR ( 8 ) OUTPUT,
      customer CICHAR ( 50 ) OUTPUT,
      roStatus CICHAR ( 24 ) OUTPUT,
      vin CICHAR ( 17 ) OUTPUT,
      vehicle CICHAR ( 60 ) OUTPUT,
      statusUpdate Memo OUTPUT,
      theDate DATE OUTPUT,
      lastContactMeans CICHAR ( 12 ) OUTPUT,
      lastContactTS TIMESTAMP OUTPUT,
      lastContactDuration Integer OUTPUT,
      howManyCalls integer output 
   ) 
BEGIN 
/*
-- 4/29
  ADD last contact output
  
EXECUTE PROCEDURE getCurrentros('tbursinger@rydellchev.com');
EXECUTE PROCEDURE getCurrentros('dpederson@gfhonda.com');
EXECUTE PROCEDURE getCurrentros('mhuot@rydellchev.com');
EXECUTE PROCEDURE getCurrentros('aneumann@gfhonda.com');
*/
DECLARE @id string;
@id = (SELECT userName FROM __input);
INSERT INTO __output
SELECT b.firstname, a.ro, c.mmddyy AS opendate, a.customer, a.rostatus, a.vin, a.vehicle, 
  d.statusUpdateWithDateName, a.openDate,
  CASE f.howMany WHEN 0 THEN '' ELSE 'phone' END AS lastContactMeans,
  e.callStartTS,
  e.callDuration,
  f.howMany
FROM CurrentRos a
INNER JOIN ServiceWriters b ON a.userName = b.userName
  AND CASE
    WHEN @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com', 'kespelund@rydellchev.com') THEN b.storecode = 'RY1'
    WHEN @id = 'aneumann@gfhonda.com' THEN b.storecode = 'RY2'
    ELSE b.userName = @id 
  END 
LEFT JOIN dds.day c ON a.opendate = c.thedate
LEFT JOIN AgedROUpdates d on a.ro = d.ro 
  and d.updateTS = (
    SELECT MAX(updatets)
    FROM AgedRoUpdates
    WHERE ro = d.ro)
-- LEFT JOIN tmpCurrentRoLastContact e on a.ro = e.ro;	 
LEFT JOIN tmpRoCalls e on a.ro = e.ro  
  and e.callStartTS = (-- most recent call
    SELECT MAX(callStartTS)
    FROM tmpRoCalls
    WHERE ro = a.ro)
LEFT JOIN (-- number of calls
  SELECT a.ro, 
    SUM(
      CASE 
        WHEN customerPhoneNumber IS NULL THEN 0
        ELSE 1
      END) AS howMany
  FROM currentRos a
  LEFT JOIN tmpRoCalls b on a.ro = b.ro   
  GROUP BY a.ro) f on a.ro = f.ro;



END;

