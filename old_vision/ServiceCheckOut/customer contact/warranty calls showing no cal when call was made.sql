/*
1/19/15 calls made are showing AS no call made
ex: 16180131
rod t ext: 5907

*/
-- this IS the base extract FROM todayXfmTmpRoCalls
-- shows calls made to this customer on 1/19 are FROM Honda
SELECT * FROM (
    SELECT *
    FROM (
      SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
        a.customerPhoneNumber, a.customerPhoneNumberRight7,
        b.startTime, CAST(b.startTime AS sql_date), 
        max(b.durationSeconds) AS durationSeconds, max(id), 
        b.filename 
      FROM todayTmpRoPhoneNumbers a
      INNER JOIN (
        SELECT id, dialedNumber10, 
          right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds,
  		    left(trim(replace(replace(replace(replace(fileName, 'E:\Recordings\','https://mediaserver.cartiva.com:3000/'), '\','/'),'.wav',''), '.cca','')), 100) AS fileName        
        FROM sh.extCall a
        LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
          AND a.extension = b.extension
          AND LEFT(trim(a.sipCallId), 15) = b.guid    
        WHERE callType = 3
          AND length(TRIM(dialedNumber)) = 12) b 
            on a.customerPhoneNumber = b.dialedNumber10
              AND b.startTime > a.roCreatedTS    
      GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
        a.customerPhoneNumber, a.customerPhoneNumberRight7,
        b.startTime, CAST(b.startTime AS sql_date), b.fileName) e
)  x WHERE ro = '16180131'       


