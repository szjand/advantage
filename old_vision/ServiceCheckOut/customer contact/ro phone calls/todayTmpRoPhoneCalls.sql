
DELETE FROM todayTmpRoPhoneNumbers1;    
INSERT INTO todayTmpRoPhoneNumbers1
SELECT distinct a.ro, a.customerKey, e.vin, a.rocreatedTS, b.thedate AS openDate, 
  c.thedate AS closeDate, d.theDate AS fcDate
-- SELECT COUNT(*)
FROM dds.todayfactRepairOrder a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.day c on a.closedatekey = c.datekey
INNER JOIN dds.day d on a.finalclosedatekey = d.datekey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey
WHERE a.customerKey NOT IN ( -- eliminate ros for inventory vehicles
  SELECT customerKey
  FROM dds.dimCustomer
  WHERE fullname = 'inventory'
   OR lastname IN ('CARWASH','SHOP TIME'));

-- 2.
-- phone numbers FROM scheduler
 
DELETE FROM todayTmpRoPhoneNumbers2; 
INSERT INTO todayTmpRoPhoneNumbers2
SELECT h.ro, h.vin, h.roCreatedTS, h.openDate, h.closeDate, h.fcDate,
  TRIM(schedPhone), right(TRIM(schedPhone), 7) 
FROM (
  SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
    trim(a.homephone) AS schedPhone
  FROM dds.todaySchedulerAppointments a
  INNER JOIN todayTmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
  WHERE trim(a.homephone) <> '' 
  UNION 
  SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
    trim(a.bestPhoneToday)
  FROM dds.todaySchedulerAppointments a
  INNER JOIN todayTmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
  WHERE trim(a.bestPhoneToday) <> '' 
  UNION 
  SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
    trim(a.cellPhone)
  FROM dds.todaySchedulerAppointments a
  INNER JOIN todayTmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
  WHERE trim(a.cellPhone) <> '') h;

-- 3.
-- phone numbers FROM dimCustomer  
DELETE FROM todayTmpRoPhoneNumbers3;       
INSERT INTO todayTmpRoPhoneNumbers3
SELECT ro, vin, roCreatedTS, openDate, closeDate, fcDate, trim(arkPhone),
  right(TRIM(arkPhone),7) AS arkPhone7
FROM (
  SELECT a.*, b.homephone AS arkPhone
  FROM todayTmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.homePhone), 'x')) = 7 or length(coalesce(trim(b.homePhone), 'x')) = 10
  UNION
  SELECT a.*, b.businessPhone
  FROM todayTmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.businessPhone), 'x')) = 7  OR length(coalesce(trim(b.businessPhone), 'x')) = 10
  UNION
  SELECT a.*, b.cellPhone
  FROM todayTmpRoPhoneNumbers1 a
  INNER JOIN dds.dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.cellPhone), 'x')) = 7 OR length(coalesce(trim(b.cellPhone), 'x')) = 10) g; 

-- 4.
DELETE FROM todayTmpRoPhoneNumbers;
INSERT INTO todayTmpRoPhoneNumbers
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  coalesce(b.schedPhone, '0'), coalesce(b.schedPhoneRight7, '0')
FROM todayTmpRoPhoneNumbers1 a
LEFT JOIN (
  SELECT * 
  FROM todayTmpRoPhoneNumbers2
  UNION 
  SELECT * 
  FROM todayTmpRoPhoneNumbers3) b on a.ro = b.ro;  

  
/* ADD tmpRoCalls */  
  
INSERT INTO tmpRoCalls
SELECT *
FROM (
  SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
    a.customerPhoneNumber, a.customerPhoneNumberRight7,
    b.startTime, CAST(b.startTime AS sql_date), 
    max(b.durationSeconds) AS durationSeconds, max(id), 
    b.filename 
  FROM todayTmpRoPhoneNumbers a
  INNER JOIN (
    SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
      right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds,
--      replace(replace(b.filename, 'E:\Recordings','.'),'\\GF-H-CALLCOPY\Recordings','.') AS fileName
      left(trim(replace(replace(replace(replace(fileName, 'E:\Recordings\','http://172.17.196.70:3030/'), '\','/'),'.wav',''), '.cca','')), 100) AS fileName
    FROM sh.extCall a
    LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
      AND a.extension = b.extension
      AND LEFT(trim(a.sipCallId), 15) = b.guid    
    WHERE callType = 3
      AND length(TRIM(dialedNumber)) = 12) b 
        on a.customerPhoneNumber = b.dialedNumber
          AND b.startTime > a.roCreatedTS    
  GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
    a.customerPhoneNumber, a.customerPhoneNumberRight7,
    b.startTime, CAST(b.startTime AS sql_date), b.fileName) e
WHERE NOT EXISTS (
  SELECT 1 
  FROM tmpRoCalls
  WHERE ro = e.ro
    AND customerPhoneNumber = e.customerPhoneNumber
    AND startTime = e.startTime);          