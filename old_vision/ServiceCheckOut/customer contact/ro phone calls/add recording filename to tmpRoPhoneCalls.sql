ALTER TABLE tmpRoCalls
ADD COLUMN recordingFileName cichar(100);

ALTER PROCEDURE getRoPhoneCalls
   ( 
      ro CICHAR ( 9 ),
      ro CICHAR ( 9 ) OUTPUT,
      lastContactTS TIMESTAMP OUTPUT,
      lastContactDuration Integer OUTPUT,
      recordingFileName CICHAR ( 100 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getRoPhoneCalls('16152847');
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output
SELECT top 100 @ro, callStartTS, callDuration, recordingFileName
FROM tmpRoCalls
WHERE ro = @ro
ORDER BY callStartTS DESC;


END;




SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), 
  max(b.durationSeconds) AS durationSeconds, max(id)
-- SELECT COUNT(*)   
FROM tmpRoPhoneNumbers a
INNER JOIN (
  SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
    right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds
  FROM sh.extCall
  WHERE callType = 3
    AND length(TRIM(dialedNumber)) = 12) b 
      on a.customerPhoneNumber = b.dialedNumber
        AND b.startTime > a.roCreatedTS    
GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date);  
  
 
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), 
  max(b.durationSeconds) AS durationSeconds, max(id),
  b.fileName
-- SELECT COUNT(*)   
FROM tmpRoPhoneNumbers a
INNER JOIN (
  SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
    right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds, 
    replace(replace(b.filename, 'E:\Recordings','.'),'\\GF-H-CALLCOPY\Recordings','.') AS fileName
  FROM sh.extCall a
  LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
    AND a.extension = b.extension
    AND LEFT(trim(a.sipCallId), 15) = b.guid
  WHERE callType = 3
    AND length(TRIM(dialedNumber)) = 12 ) b 
      on a.customerPhoneNumber = b.dialedNumber
        AND b.startTime > a.roCreatedTS    
GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), b.fileName;  
  
  
  
SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
  right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds, 
  replace(replace(b.filename, 'E:\Recordings','.'),'\\GF-H-CALLCOPY\Recordings','.') AS fileName
FROM sh.extCall a
LEFT JOIN sh.callCopyRecordings b on CAST(a.startTime AS sql_date) = b.theDate
  AND a.extension = b.extension
  AND LEFT(trim(a.sipCallId), 15) = b.guid
WHERE callType = 3
  AND length(TRIM(dialedNumber)) = 12  
