SELECT *
FROM stgRydellServiceAppointments
ORDER BY createdts DESC

SELECT *
FROM stgRydellServiceAppointments
WHERE ro = '16151239'
ORDER BY createdts


SELECT ro, customer, homephone, bestphonetoday, cellphone
FROM stgRydellServiceAppointments
WHERE ro IN (
SELECT ro FROM (
SELECT ro, trim(homephone), trim(bestphonetoday), trim(cellphone)
FROM stgRydellServiceAppointments
WHERE ro <> ''
GROUP BY ro, homephone, bestphonetoday, cellphone
) x GROUP BY ro HAVING COUNT(*) > 1)
ORDER BY ro

-- ros with more than one non blank homephone
SELECT ro FROM (
SELECT ro, homePhone FROM (
SELECT ro, homephone
FROM stgRydellServiceAppointments
WHERE ro <> ''
  AND homephone <> ''
) x GROUP BY ro, homePhone
) y GROUP BY ro HAVING COUNT(*) > 1

SELECT ro, createdts, customer, homephone
FROM stgRydellServiceAppointments
WHERE ro IN (
SELECT ro FROM (
SELECT ro, homePhone FROM (
SELECT ro, homephone
FROM stgRydellServiceAppointments
WHERE ro <> ''
  AND homephone <> ''
) x GROUP BY ro, homePhone
) y GROUP BY ro HAVING COUNT(*) > 1)
ORDER BY ro
  
  
-- of these 2 rows, one of the ro nubers IS wrong (christie's, -131715)
select *
FROM stgRydellServiceAppointments
WHERE ro = '16151384'  
  
-- so, how many rows are for multiple vehicles?
SELECT ro FROM (
SELECT ro, vin 
FROM stgRydellServiceAppointments
WHERE ro <> ''
  AND vin <> ''
GROUP BY ro, vin 
) x GROUP BY ro HAVING COUNT(*) > 1

select *
FROM stgRydellServiceAppointments
WHERE ro IN (
SELECT ro FROM (
SELECT ro, vin 
FROM stgRydellServiceAppointments
WHERE ro <> ''
  AND vin <> ''
GROUP BY ro, vin 
) x GROUP BY ro HAVING COUNT(*) > 1)
ORDER BY ro

-- ok, validate row with factReapairOrder, the correct row for the correct vehicle?
-- OR dimCustomer???

-- oh, this IS fucking great, multiple dimcustomer rows for the same person with 
-- different phone numbers for the different rows
SELECT * FROM dimcustomer WHERE fullname = 'PAPE, ROSS MICHAEL'

SELECT a.ro, a.customer, b.homephone, a.bestphonetoday, a.cellphone,
  b.fullname, b.homephone, b.businessphone, b.cellphone
from stgRydellServiceAppointments a
INNER JOIN dimcustomer b on a.customer = b.fullname
ORDER BY a.createdts DESC 

-- ALL the ros are for the came customer key
SELECT * FROM factRepairORder WHERE customerkey IN (SELECT customerkey FROM dimcustomer WHERE fullname = 'PAPE, ROSS MICHAEL')
-- but each car deal IS for a different customerkey
select * FROM factVehicleSale WHERE buyerkey IN (SELECT customerkey FROM dimcustomer WHERE fullname = 'PAPE, ROSS MICHAEL')

-- 5/5/14  
SELECT a.ro, a.customer, a.vin, b.customer, b.vin, b.homephone, b.bestPhoneToday, b.cellphone 
FROM scotest.currentros a
LEFT JOIN stgRydellServiceAppointments b on a.ro = b.ro

a tmpTable of currentros, customer, vehicle, phonenumbers
FROM factRepairOrder/dimCustomer/scotest.currentros

SELECT * FROM scotest.currentros

SELECT a.ro, c.homePhone AS arkonaPhone
INTO #arkona
FROM scotest.currentros a
INNER JOIN (
  SELECT DISTINCT ro, customerkey
  FROM factRepairOrder
  WHERE ro IN (
    SELECT ro
    FROM scotest.currentros)
  UNION 
  SELECT DISTINCT ro, customerkey
  FROM todayFactRepairOrder
  WHERE ro IN (
    SELECT ro
    FROM scotest.currentros)) b on a.ro = b.ro 
INNER JOIN dimCustomer c on b.customerKey = c.customerKey  
WHERE c.homePhone <> '0'  
UNION 
SELECT a.ro, c.businessPhone AS arkonaPhone
FROM scotest.currentros a
INNER JOIN (
  SELECT DISTINCT ro, customerkey
  FROM factRepairOrder
  WHERE ro IN (
    SELECT ro
    FROM scotest.currentros)
  UNION 
  SELECT DISTINCT ro, customerkey
  FROM todayFactRepairOrder
  WHERE ro IN (
    SELECT ro
    FROM scotest.currentros)) b on a.ro = b.ro 
INNER JOIN dimCustomer c on b.customerKey = c.customerKey  
WHERE c.businessPhone <> '0'  
UNION 
SELECT a.ro, c.cellPhone AS arkonaPhone
FROM scotest.currentros a
INNER JOIN (
  SELECT DISTINCT ro, customerkey
  FROM factRepairOrder
  WHERE ro IN (
    SELECT ro
    FROM scotest.currentros)
  UNION 
  SELECT DISTINCT ro, customerkey
  FROM todayFactRepairOrder
  WHERE ro IN (
    SELECT ro
    FROM scotest.currentros)) b on a.ro = b.ro 
INNER JOIN dimCustomer c on b.customerKey = c.customerKey  
WHERE c.cellPhone <> '0'
ORDER BY a.ro

SELECT * FROM stgRydellServiceAppointments

SELECT ro, homephone AS schedPhone
INTO #sched
FROM stgRydellServiceAppointments
WHERE homePhone <> ''
  AND ro IN (
    SELECT ro
    FROM scotest.currentros)
UNION 
SELECT ro, bestPhoneToday AS schedPhone
FROM stgRydellServiceAppointments
WHERE bestPhoneToday <> ''
  AND ro IN (
    SELECT ro
    FROM scotest.currentros)  
UNION 
SELECT ro, cellPhone AS schedPhone
FROM stgRydellServiceAppointments
WHERE cellPhone <> ''
  AND ro IN (
    SELECT ro
    FROM scotest.currentros)       
    
SELECT *
FROM #sched a
WHERE NOT EXISTS (
  SELECT 1
  FROM #arkona
  WHERE ro = a.ro
    AND arkonaPhone = schedPhone)

-- gives me scheduler phone numbers that DO NOT exist IN arkona    
SELECT *
FROM #arkona a
LEFT JOIN (  
SELECT *
FROM #sched a
WHERE NOT EXISTS (
  SELECT 1
  FROM #arkona
  WHERE ro = a.ro
    AND arkonaPhone = schedPhone)) b on a.ro = b.ro  
    
-- ok, this IS ALL cool for current ros AND stuff
-- but
-- need a more generalized solution
-- limit to service customers validate scheduler data ro/vehicle, customer
that IS filter scheduler data based on a clean match of ro/vin/customer
get rid of the shit IN scheduler, LIKE multiple vehicles/customers on the same ro number

first question i have, what IS the MAX # of phone numbers for a scheduler customer
duh, scheduler has provisions for 3 phone numbers, so, the MAX IS 3 additional numbers
careful, this IS NOT pure customer stuff, this IS customer IN the context of an RO

DROP TABLE #wtf;
SELECT ro, vin, fullname, customerKey, homephone, businessphone, cellphone
INTO #wtf
FROM (
  SELECT a.ro, c.vin, b.fullName, b.customerKey, b.homePhone, b.businessPhone, b.cellPhone 
  FROM factRepairOrder a
  INNER JOIN dimCustomer b on a.customerKey = b.customerKey
  INNER JOIN dimVehicle c on a.vehicleKey = c.vehicleKey
  INNER JOIN day d on a.openDateKey = d.datekey
  WHERE d.theYear = 2014) x
-- eliminates inventory ros, a normal customer lack of phone IS '0'  
WHERE (homephone IS NOT NULL)
GROUP BY ro, vin, fullname, customerKey, homephone, businessphone, cellphone 


DROP TABLE #wtf1;
SELECT ro, vin, homephone, bestphonetoday, cellphone
INTO #wtf1
FROM stgRydellServiceAppointments
WHERE ro <> ''
  AND (homephone <> '' OR bestphonetoday <> '' OR cellphone <> '')
  AND vin <> ''
GROUP BY ro, vin, homephone, bestphonetoday, cellphone 

select * 
FROM #wtf1
WHERE ro IN (
  SELECT ro FROM (
    SELECT ro, vin, customer, homephone, bestphonetoday, cellphone
    FROM stgRydellServiceAppointments
    WHERE ro <> ''
      AND (homephone <> '' OR bestphonetoday <> '' OR cellphone <> '')
      AND vin <> ''
    GROUP BY ro, vin, customer, homephone, bestphonetoday, cellphone  
  ) x GROUP BY ro HAVING COUNT(*) > 1)
  
can NOT fucking rely on customer IN scheduler
so DO ro AND vin, but only those that match ro AND vin IN factRepairOrder

-- 2014 ros FROM factRepairOrder
select * 
FROM #wtf 

SELECT * 
FROM #wtf a
LEFT JOIN #wtf1 b on a.ro = b.ro AND a.vin = b.vin
WHERE a.homephone = '0' AND a.businessphone = '0' AND a.cellphone = '0'

select *
FROM #wtf
WHERE cellphone = '0'

this IS so fucked up, a different (than the one used on the ro) customer record
shows 2 phone numbers, neither of which match what the scheduler has for phone numbers
-- ALL the ros are for the came customer key
SELECT * FROM factRepairORder WHERE customerkey IN (SELECT customerkey FROM dimcustomer WHERE fullname = 'ZETTLER, MIKKEL')
-- but each car deal IS for a different customerkey
select * FROM factVehicleSale WHERE buyerkey IN (SELECT customerkey FROM dimcustomer WHERE fullname = 'ZETTLER, MIKKEL')

SELECT * FROM stgArkonaBOPNAME WHERE bnsnam = 'ZETTLER, MIKKEL'

select * FROM dimcustomer WHERE customerkey = 150954

-- this might be the base TABLE for extracting relevant lists of phone numbers
-- FROM each source
SELECT * 
FROM #wtf a
LEFT JOIN #wtf1 b on a.ro = b.ro AND a.vin = b.vin
WHERE b.ro IS NOT NULL 
ORDER BY a.ro

-- AND FROM this "base" TABLE, multiple lines per ro
SELECT ro FROM (
SELECT * 
FROM #wtf a
LEFT JOIN #wtf1 b on a.ro = b.ro AND a.vin = b.vin
WHERE b.ro IS NOT NULL 
) x GROUP BY ro HAVING COUNT(*) > 1

-- which are accounted for BY the fact that the scheduler often contains
-- multiple lines for a single ro, AND NOT infrequently, those lines
-- have a different array of phone numbers
SELECT *
FROM (
  SELECT * 
  FROM #wtf a
  LEFT JOIN #wtf1 b on a.ro = b.ro AND a.vin = b.vin
  WHERE b.ro IS NOT NULL) c
WHERE ro IN (
  SELECT ro FROM (
  SELECT * 
  FROM #wtf a
  LEFT JOIN #wtf1 b on a.ro = b.ro AND a.vin = b.vin
  WHERE b.ro IS NOT NULL 
  ) x GROUP BY ro HAVING COUNT(*) > 1)  

-- again, remember the goal, to extract probable relevant phone numbers FROM
-- scheduler that DO NOT exist IN arkona  
-- ok, build the list of ro/vin with each phone number
-- first arkona
DROP TABLE #ark;
-- need to include ro/vin whether OR NOT there IS a phone number
-- UNION should limit the output to a single row for ros with no phone number at all
SELECT DISTINCT ro, vin, arkPhone
INTO #ark
FROM (
  SELECT a.ro, a.vin, a.homephone AS arkPhone
  FROM #wtf a
--  WHERE a.homephone <> '0'
  UNION
  SELECT a.ro, a.vin, a.businessphone
  FROM #wtf a
--  WHERE a.businessphone <> '0'
  UNION
  SELECT a.ro, a.vin, a.cellphone
  FROM #wtf a) b
--  WHERE a.cellphone <> '0') b
  
-- AND scheduler  
SELECT DISTINCT ro, vin, schedPhone
INTO #sched
FROM (
  SELECT b.ro, b.vin, b.homephone AS schedPhone
  FROM #wtf a
  LEFT JOIN #wtf1 b on a.ro = b.ro AND a.vin = b.vin
  WHERE b.ro IS NOT NULL  
    AND b.homephone <> '' 
  UNION
  SELECT b.ro, b.vin, b.bestPhoneToday AS schedPhone
  FROM #wtf a
  LEFT JOIN #wtf1 b on a.ro = b.ro AND a.vin = b.vin
  WHERE b.ro IS NOT NULL  
    AND b.bestPhoneToday <> '' 
  UNION
  SELECT b.ro, b.vin, b.cellPhone AS schedPhone
  FROM #wtf a
  LEFT JOIN #wtf1 b on a.ro = b.ro AND a.vin = b.vin
  WHERE b.ro IS NOT NULL  
    AND b.cellPhone <> '') c   

-- this should be the list of phone numbers NOT IN arkona    
SELECT *
FROM #sched a
WHERE NOT EXISTS (
  SELECT 1
  FROM #ark
  WHERE ro = a.ro
    AND vin = a.vin
    AND arkPhone = a.schedPhone)

-- AND of course, there IS one with 3 phone numbers 16143472 
SELECT ro, COUNT(*)
FROM (   
SELECT *
FROM #sched a
WHERE NOT EXISTS (
  SELECT 1
  FROM #ark
  WHERE ro = a.ro
    AND vin = a.vin
    AND arkPhone = a.schedPhone)) x
GROUP BY ro HAVING COUNT(*) > 1
ORDER BY COUNT(*) desc     

SELECT * FROM #ark WHERE ro = '16143472'
UNION
SELECT * FROM #sched WHERE ro = '16143472'

-- shit, so how DO i store this stuff
initial thought was IN dimCustomer otherPhone1, 2, 3
but what IF arkona has no homephone, but scheduler does, does the scheduler
homephone become the default homephone for the customer?
i am leary of going down that path 
  i have limited confidence IN scheduler data being scalable across the enterprise (sales, parts, etc) 
  ALL i am really addressing here IS service events (ros)
i am NOT trying to generate (at this point IN time) the canonical list of a customers
  phone numbers, but identifying the list of possible phone numbers BY which
  a writer may have contacted a customer  
  this IS NOT a list of phone numbers that a writer would consult prior to contacting
  a customer, but the list of possible phone numbers BY which the writer may have
  contacted the customer
  AND even more limited to those contacts IN the context of the current OPEN ro

 
  
  
SELECT *
FROM #wtf a
LEFT JOIN #wtf1 b on a.ro = b.ro AND a.vin = b.vin
WHERE a.homephone <> b.homephone

-- 5/6
this IS NOT about dimCustomer, this IS about ros, at first current ros, but probably
eventually (IF this notion takes off) aged AND warranty calls

2 stored proc calls need:
  most recent calls (including the number of calls) (AS part of sp.getCurrentRos)
  a full list of calls
so need to store IN a TABLE, 
ro AND ALL calls to that ro
for reference, need to be able to associate an ro with a SET of phone numbers
ro
called number 
call timestamp
call duration
total number of calls
  -- 5/8 (0 - n), ie, there will be a row for each ro, whether called OR not?
call sequence
-- what about calls after ro closed (warranty follow up?)
most recent call (flag)

so for an ro, need a list of ALL possible phone numbers, THEN determine IF
one of those numbers have been called IN the relevant time frame

so step it thru
1. ro info
2. ro phone number array info

Need an initial load, AS well AS realtime updating

lets start out limiting to 2014 

-- 5/10 IN the context of the shit i ran INTO with multiple right7 phone numbers
i guess what i am going for IS that ALL phone numbers are either 7 OR 10 IN length

fuck me, right
have to clean up some data before being able to rely on joins
1. limit phoneNumber attribute to cichar(10)
2. TRIM scheduler phone numbers, tmpRoPhoneNumbers2
3. limit dimCustomer phone numbers to those of length 7 OR 10, tmpRoPhoneNumbers3 
4. limit shoretel numbers to those that are length 12, DROP first 2 characters
     which are +1 

SELECT length(dialedNumber), MIN(dialedNumber), MAX(dialedNumber), COUNT(*) 
FROM sh.extcall 
WHERE callType = 3
GROUP BY length(dialedNumber)

SELECT length(customerPhoneNumber), COUNT(*) 
FROM tmpRoPhoneNumbers GROUP BY length(customerPhoneNumber)

SELECT *
FROM tmpRoPhoneNumbers
WHERE length(customerPhoneNumber) IN (8,9)
-- scheduler IS ok AS long AS it IS trimmed
SELECT length(trim(schedPhone)), MIN(schedPhone), MAX(schedPhone), COUNT(*)
FROM tmpRoPhoneNumbers2
GROUP BY length(trim(schedPhone))
-- dimCustomer
SELECT length(arkPhone), MIN(arkPhone), MAX(arkPhone), COUNT(*)
FROM tmpRoPhoneNumbers3
GROUP BY length(arkPhone)

-- ok, the results are IN, ALL customer  phone numbers are 
-- length 1 (none, '0'), 7, OR 10
SELECT length(customerPhoneNumber), MIN(customerPhoneNumber), MAX(customerPhoneNumber), COUNT(*)
FROM tmpRoPhoneNumbers
GROUP BY length(customerPhoneNumber)

-- sh.extCall
SELECT length(dialedNumber), MIN(dialedNumber), MAX(dialedNumber), COUNT(*) 
FROM sh.extcall 
WHERE callType = 3
GROUP BY length(dialedNumber)

-- 1. ro info
DROP TABLE tmpRoPhoneNumbers1;
CREATE TABLE tmpRoPhoneNumbers1 (
  ro cichar(9) constraint NOT NULL,
  customerKey integer constraint NOT NULL, 
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  constraint pk primary key (ro))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers1','tmpRoPhoneNumbers1.adi','customerKey',
   'customerKey','',2,512,'' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers1','tmpRoPhoneNumbers1.adi','roVin',
   'ro;vin','',2,512,'' );       
INSERT INTO tmpRoPhoneNumbers1
SELECT distinct a.ro, a.customerKey, e.vin, a.rocreatedTS, b.thedate AS openDate, 
  c.thedate AS closeDate, d.theDate AS fcDate
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN day c on a.closedatekey = c.datekey
INNER JOIN day d on a.finalclosedatekey = d.datekey
INNER JOIN dimVehicle e on a.vehicleKey = e.vehicleKey
WHERE a.customerKey NOT IN ( -- eliminate ros for inventory vehicles
  SELECT customerKey
  FROM dimCustomer
  WHERE fullname = 'inventory'
   OR lastname IN ('CARWASH','SHOP TIME'))
AND b.theyear = 2014;

-- 2.
-- phone numbers FROM scheduler
DROP TABLE tmpRoPhoneNumbers2;
CREATE TABLE tmpRoPhoneNumbers2 (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  schedPhone cichar(10) constraint NOT NULL,
  schedPhoneRight7 cichar(7) constraint NOT NULL,
  constraint pk primary key (ro, schedPhone))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','roVin',
   'ro;vin','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','schedPhone',
   'schedPhone','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers2','tmpRoPhoneNumbers2.adi','schedPhoneRight7',
   'schedPhoneRight7','',2,512,'' );    
INSERT INTO tmpRoPhoneNumbers2
SELECT h.ro, h.vin, h.roCreatedTS, h.openDate, h.closeDate, h.fcDate,
  TRIM(schedPhone), right(TRIM(schedPhone), 7) 
FROM (
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.homephone) AS schedPhone
FROM stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.homephone) <> '' 
-- GROUP BY b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, a.homephone
UNION 
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.bestPhoneToday)
FROM stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.bestPhoneToday) <> '' 
--GROUP BY a.ro, a.bestPhoneToday
UNION 
SELECT b.ro, b.vin, b.roCreatedTS, b.OpenDate, b.closeDate, b.fcDate, 
  trim(a.cellPhone)
FROM stgRydellServiceAppointments a
INNER JOIN tmpRoPhoneNumbers1 b on a.ro = b.ro AND a.vin = b.vin
WHERE trim(a.cellPhone) <> '') h;
--GROUP BY a.ro, a.cellPhone
   
-- 3.
-- phone numbers FROM dimCustomer
DROP TABLE tmpRoPhoneNumbers3;
CREATE TABLE tmpRoPhoneNumbers3 (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  arkPhone cichar(10) constraint NOT NULL,
  arkPhoneRight7 cichar(7) constraint NOT NULL,
  constraint pk primary key (ro, arkPhone))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','roVin',
   'ro;vin','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','arkPhone',
   'arkPhone','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers3','tmpRoPhoneNumbers3.adi','arkPhoneRight7',
   'arkPhoneRight7','',2,512,'' );      
INSERT INTO tmpRoPhoneNumbers3
SELECT ro, vin, roCreatedTS, openDate, closeDate, fcDate, trim(arkPhone),
  right(TRIM(arkPhone),7) AS arkPhone7
FROM (
  SELECT a.*, b.homephone AS arkPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.homePhone), 'x')) = 7 or length(coalesce(trim(b.homePhone), 'x')) = 10
  UNION
  SELECT a.*, b.businessPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.businessPhone), 'x')) = 7  OR length(coalesce(trim(b.businessPhone), 'x')) = 10
  UNION
  SELECT a.*, b.cellPhone
  FROM tmpRoPhoneNumbers1 a
  INNER JOIN dimCustomer b
  on a.customerKey = b.customerKey
  WHERE length(coalesce(trim(b.cellPhone), 'x')) = 7 OR length(coalesce(trim(b.cellPhone), 'x')) = 10) g; 

 
-- 4.
-- all phone numbers for an ro
-- each ro can have 0:6 phone numbers
DROP TABLE tmpRoPhoneNumbers;
CREATE TABLE tmpRoPhoneNumbers (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  customerPhoneNumber cichar(10) default '0' constraint NOT NULL,
  customerPhoneNumberRight7 cichar(7) default '0' constraint NOT NULL,
  constraint pk primary key (ro, customerPhoneNumber))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','ro',
   'ro','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','customerPhoneNumber',
   'customerPhoneNumber','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoPhoneNumbers','tmpRoPhoneNumbers.adi','customerPhoneNumberRight7',
   'customerPhoneNumberRight7','',2,512,'' ); 
INSERT INTO tmpRoPhoneNumbers
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  coalesce(b.schedPhone, '0'), coalesce(b.schedPhoneRight7, '0')
FROM tmpRoPhoneNumbers1 a
LEFT JOIN (
  SELECT * 
  FROM tmpRoPhoneNumbers2
  UNION 
  SELECT * 
  FROM tmpRoPhoneNumbers3) b on a.ro = b.ro;

/*
-- ros with no phone numbers  
SELECT *
FROM tmpRoPhoneNumbers
WHERE customerPhoneNumber = '0'
-- ros with 4 phone numbers
SELECT * FROM (
SELECT a.ro, coalesce(howMany, 0) AS howMany
FROM (
  SELECT ro
  FROM tmpRoPhoneNumbers
  GROUP BY ro) a
LEFT JOIN (
  SELECT ro, COUNT(ro) AS howMany
  FROM tmpRoPhoneNumbers
  WHERE customerPhoneNumber <> '0'
  GROUP BY ro) b on a.ro = b.ro
LEFT JOIN (
  SELECT DISTINCT ro, customerKey
  FROM factRepairOrder) c on a.ro = c.ro
) x WHERE howMany = 4
*/
-- ok, DO i store ALL possible 6 phone numbers IN this TABLE, whether OR NOT
-- there have been phone calls?   
ro
called number 
call timestamp
call duration
total number of calls
  -- 5/8 (0 - n), ie, there will be a row for each ro, whether called OR not?
call sequence
-- what about calls after ro closed (warranty follow up?)
most recent call (flag)
-- ALL this TABLE tells me natively are the phone calls to an ro customer 
-- that took place after the ro opened
-- current ros IS pretty clear AND easy, likewise aged ros
-- gets a bit more dicy on warranty follow ups, something LIKE calls
-- BETWEEN finalclosedate AND finalclosedate + ~ 2 weeks

-- 5/10
a row for every ro, default phoneNumer to '0', ie, some ros have no phone numbers
an ro may have 0:m phone numbers
each of those phone numbers may have been called 0:m times

so what this IS giving me IS shit LIKE ro 16140952, customer IS rudy robles,
  ro IS FROM 1/2/14, AND this lists calls to 7727211 FROM april 2014
  which may be ok, again, the range of possible phone calls will be limited BY the
  date range IN the request
  
uh oh, the number of calls depends on the date range!!!
so, it may have to be generated dynamically IN the call, same thing with the sequence

-- include extCall.id for eventual linking to extConnect AND call copy
DROP TABLE tmpRoCalls;
CREATE TABLE tmpRoCalls (
  ro cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  roCreatedTS timestamp constraint NOT NULL,
  openDate date constraint NOT NULL,
  closeDate date constraint NOT NULL,
  fcDate date constraint NOT NULL,
  customerPhoneNumber cichar(10) constraint NOT NULL,
  customerPhoneNumberRight7 cichar(7) constraint NOT NULL,
  callStartTS timestamp constraint NOT NULL,
  callDate date constraint NOT NULL,
  callDuration integer default '0' constraint NOT NULL, 
  extCallId integer constraint NOT NULL, 
  constraint pk primary key (ro, customerPhoneNumber, callStartTS))IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','ro',
   'ro','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','roCreatedTS',
   'roCreatedTS','',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','customerPhoneNumberRight7',
   'customerPhoneNumberRight7','',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRoCalls','tmpRoCalls.adi','callStartTS',
   'callStartTS','',2,512,'' );    
   
--thinking i need to DO an INNER JOIN to extcall, ALL i want IN this TABLE are 
--actual calls, there IS no need to list phone numbers for ros that have NOT been called
-- AND IF i am deriving only actual calls, don't need the coalescing
INSERT INTO tmpRoCalls
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), 
  max(b.durationSeconds) AS durationSeconds, max(id)
-- SELECT COUNT(*)   
FROM tmpRoPhoneNumbers a
INNER JOIN (
  SELECT id, right(TRIM(dialedNumber), 10) AS dialedNumber, 
    right(TRIM(dialedNumber), 7) AS dialedNumberRight7, startTime, durationSeconds
  FROM sh.extCall
  WHERE callType = 3
    AND length(TRIM(dialedNumber)) = 12) b 
      on a.customerPhoneNumber = b.dialedNumber
        AND b.startTime > a.roCreatedTS    
GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date);  
/*  
SELECT a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date), max(b.durationSeconds) AS durationSeconds
-- SELECT COUNT(*)   
FROM tmpRoPhoneNumbers a
INNER JOIN sh.extcall b on a.customerPhoneNumberRight7 = b.dialedNumberRight7 
  AND b.startTime > a.roCreatedTS
  AND b.callType = 3
GROUP BY a.ro, a.vin, a.roCreatedTS, a.openDate, a.closeDate, a.fcDate, 
  a.customerPhoneNumber, a.customerPhoneNumberRight7,
  b.startTime, CAST(b.startTime AS sql_date); 
*/ 
ok, this IS now the base query for currentRos
need 2 different calls:
  1. most recent call, including the total number of calls
  2. list of calls for an ro, ordered BY callStartTS desc  
SELECT a.*, b.customerPhoneNumber, callStartTS, callDuration
FROM scoTest.currentRos a
LEFT JOIN tmpRoCalls b on a.ro = b.ro

      
1. 

this IS returning multiple rows for ro 16152712, whydat?
this IS turning INTO a major mind fuck, 
ahhh, it IS a typo IN customerPhoneNumber, for 6811131 there IS one with areacode 213, one with areacode 218
fuck me sideways
so the solution IS just use Right7
SELECT * FROM tmpRoCalls WHERE ro = '16152712' ORDER BY customerPhoneNumber, callStartTS
    
thought i had checked for multiple phoneNumber for the same right7, maybe NOT 
against customerPhone, NOT well enough apparently

SELECT customerPhoneNumberRight7 FROM (
SELECT customerPhoneNumber, customerPhoneNumberRight7
FROM tmpRoPhoneNumbers 
WHERE ro = '16152712'
GROUP BY customerPhoneNumber, customerPhoneNumberRight7
) x GROUP BY customerPhoneNumberRight7 HAVING COUNT(*) > 1

-- YIKES
SELECT DISTINCT customerPhoneNumber, customerPhoneNumberRight7 FROM tmpRoPhoneNumbers WHERE customerPhoneNumberRight7 IN (
SELECT customerPhoneNumberRight7 FROM (
SELECT customerPhoneNumber, customerPhoneNumberRight7
FROM tmpRoPhoneNumbers 
--WHERE ro = '16152712'
GROUP BY customerPhoneNumber, customerPhoneNumberRight7
) x GROUP BY customerPhoneNumberRight7 HAVING COUNT(*) > 1)
ORDER BY customerPhoneNumberRight7

many with phone being 7 AND 10: 7017728111/7728111
multiple valid area codes: 2187730917/7017730917, 4057882466/7057882466 (oklahoma vs ontario canada)
multiple including typo areacode: 2136811131/2186811131
IN the CASE of 2182816971 vs 7012816971, both decode (on google) AS valid, one IN crookston, one IN fargo

SELECT * FROM tmpRoPhoneNumbers WHERE CustomerPhoneNumberRight7 = '6811131'
SELECT * FROM sh.extcall WHERE dialedNumberRight7 = '6811131'
select * FROM tmpRoCalls WHERE CustomerPhoneNumberRight7 = '6811131'

the problem IS that a call was made to both the valid number AND the invalid number



SELECT * FROM tmpRoPhoneNumbers WHERE customerPhoneNumberRight7 = '5203523'
   
SELECT a.*, b.customerPhoneNumber, callStartTS, callDuration, c.howMany
FROM scoTest.currentRos a
LEFT JOIN tmpRoCalls b on a.ro = b.ro  
  and b.callStartTS = (-- most recent call
    SELECT MAX(callStartTS)
    FROM tmpRoCalls
    WHERE ro = a.ro)
LEFT JOIN (-- number of calls
  SELECT a.ro, 
    SUM(
      CASE 
        WHEN customerPhoneNumber IS NULL THEN 0
        ELSE 1
      END) AS howMany
  FROM scoTest.currentRos a
  LEFT JOIN tmpRoCalls b on a.ro = b.ro   
  GROUP BY a.ro) c on a.ro = c.ro



 
  SELECT a.ro, 
    SUM(
      CASE 
        WHEN customerPhoneNumber IS NULL THEN 0
        ELSE 1
      END) AS howMany
  FROM scoTest.currentRos a
  LEFT JOIN tmpRoCalls b on a.ro = b.ro   
  GROUP BY a.ro
  ORDER BY a.ro    
  
SELECT * FROM scotest.currentRos WHERE ro = '16152712'  

/*
-- uh oh, multiple outbound calls to the same number at the same time
SELECT ro, customerPhoneNumber, callStartTS FROM (
SELECT a.*, 
  coalesce(startTime, cast('9999-12-31 00:00:01' AS sql_timestamp)) AS callStartTS, 
  cast(coalesce(startTime, cast('9999-12-31 00:00:01' AS sql_timestamp)) AS sql_date) AS callDate,
  coalesce(durationSeconds, 0) AS durationSeconds
-- SELECT COUNT(*)  
FROM tmpRoPhoneNumbers a
INNER JOIN (  
  SELECT dialednumberRight7, starttime, durationSeconds
  FROM sh.extcall a
  WHERE calltype = 3) b 
    on a.customerPhoneNumberRight7 = b.dialedNumberRight7
      AND b.starttime > a.roCreatedTS
) x GROUP BY ro, customerPhoneNumber, callStartTS HAVING COUNT(*) > 1      
 
-- yep, it happens, NOT a lot, but it does happen, probably need to GROUP incl starttime
select * FROM sh.extcall
WHERE CAST(starttime AS sql_date) = '04/18/2014'
  AND right(TRIM(dialednumber),7) = '7397669' 
SELECT * FROM tmproCalls      
*/
/*
-- 6 calls on one ro
SELECT *
FROM (
  SELECT *
  FROM tmpRoPhoneNumbers2
  UNION 
  SELECT * 
  FROM tmpRoPhoneNumbers3) a
INNER JOIN (  
  SELECT dialednumberRight7, starttime, durationSeconds
  FROM sh.extcall a
  WHERE calltype = 3) b 
    on a.schedPhoneRight7 = b.dialedNumberRight7
      AND b.starttime > a.roCreatedTS 
WHERE ro = '16152431'  

-- current ros with no phone calls
SELECT *
FROM scotest.currentros m    
LEFT JOIN ( 
  SELECT *
  FROM (
    SELECT *
    FROM tmpRoPhoneNumbers2
    UNION 
    SELECT * 
    FROM tmpRoPhoneNumbers3) a
  INNER JOIN (  
    SELECT dialednumberRight7, starttime, durationSeconds
    FROM sh.extcall a
    WHERE calltype = 3) b 
      on a.schedPhoneRight7 = b.dialedNumberRight7
        AND b.starttime > a.roCreatedTS ) n on m.ro = n.ro
WHERE n.ro IS NULL    

-- current ros with no phone numbers
SELECT *
FROM scotest.currentros m    
LEFT JOIN ( 
  SELECT *
  FROM (
    SELECT *
    FROM tmpRoPhoneNumbers2
    UNION 
    SELECT * 
    FROM tmpRoPhoneNumbers3) a) b on m.ro = b.ro
WHERE b.ro IS NULL     
ORDER BY m.opendate
*/
      
/*

          factRepairOrder   Day  dimVehcle
              \              /    /
               \            /____/
                \          /
                 \        /
  scheduler    tmpRoPhoneNumbers1  dimCustomer    need ro/vin for match to scheduler
        \            /         \            /
         \          /           \          /
         tmpRoPhoneNumbers2   tmpRoPhoneNumbers3
              \              /
               \            /
              tmpRoPhoneNumbers       shoretel
                    \                 /
                     \               /
                      \             / 
                         tmpRoCalls
*/
                         


