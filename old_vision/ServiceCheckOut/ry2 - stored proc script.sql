CREATE PROCEDURE GetEmployeeTasksForUser
   ( 
      eeUserNameMgr CICHAR ( 50 ),
      eeUserName CICHAR ( 50 ) OUTPUT,
      task CICHAR ( 45 ) OUTPUT,
      caption CICHAR ( 60 ) OUTPUT,
      displayclass CICHAR ( 45 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetEmployeeTasksForUser ('aberry@rydellchev.com');
EXECUTE PROCEDURE GetEmployeeTasksForUser ('aneumann@gfhonda.com');
get the tasks for an the mgrs employee WHEN the manager IS logged IN
used for display IN LEFT nav
purpose: ON the mgr landing page, the individual writers show up IN the LEFT sidebar
  WHERE the mgr can click ON a writer AND see that writer's landing page
  the mgr should also be able to UPDATE that writers aged ros AND ry1 warr calls
input: manager eeusername
output: writer eeusername, writer tasks,
        caption IS a display value for the task
        displayclass IS a value used for css classing
  exceptions: DO NOT show checkout for writers
              show no tasks for andrew neumann
  
*/
DECLARE @user string;
@user = (SELECT eeUserNameMgr FROM __input);
/**/
INSERT INTO __output
  SELECT a.eeusername, 
    CASE WHEN eeusername = 'aneumann@gfhonda.com' THEN '' ELSE c.task END, 
    CASE WHEN eeusername = 'aneumann@gfhonda.com' THEN '' ELSE c.caption END, 
    CASE WHEN eeusername = 'aneumann@gfhonda.com' THEN '' ELSE c.displayclass END
  FROM (  
    SELECT b.eeusername, b.storecode, b.department, b.position
    FROM  positionfulfillment b 
    WHERE b.thruts > now()
      AND position IN ('Service Writer','Service Follow Up')
      AND b.storecode = (
        SELECT distinct storecode
        FROM positionfulfillment
        WHERE eeusername = @user)) a  
  INNER JOIN (    
      SELECT a.task, a.storecode, a.department, a.position, b.caption, b.displayclass
      FROM positiontasks a
      INNER JOIN tasks b ON a.task = b.task
      WHERE a.position IN ('Service Writer','Service Follow Up')
      AND b.task <> 'Checkout') c ON a.storecode = c.storecode
    AND a.department = c.department
    AND a.position = c.position;   
/*
SELECT DISTINCT g.writerEEUserName, a.task, a.caption, -- aneumann returns 2 of each
  CASE 
    WHEN a.task = 'Update Aged ROs' THEN 'aged'
    WHEN a.task = 'Update Warranty Calls' THEN 'warranty'
    WHEN a.task = 'Update RY2 Calls' THEN 'ry2'
  end 
FROM tasks a
INNER JOIN positiontasks b ON a.task = b.task
  AND b.thruts > now()
INNER JOIN positions c ON b.storecode = c.storecode
  AND b.department = c.department
  AND b.position = c.position
INNER JOIN positionfulfillment d ON c.storecode = d.storecode
  AND c.department = d.department
  AND c.position = d.position
  AND d.thruts > now()
INNER JOIN people e ON d.eeusername = e.eeusername
  AND e.eeusername = @id
LEFT JOIN (
  SELECT * FROM (EXECUTE PROCEDURE GetWritersForManager(@id)) f) g  ON 1 = 1
WHERE a.task <> 'Checkout';  
*/

END;

CREATE PROCEDURE GetRY2ROCalls
   ( 
      ro CICHAR ( 9 ),
      callTS CICHAR ( 20 ) OUTPUT,
      eeUserName CICHAR ( 50 ) OUTPUT,
      description Memo OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetRY2ROCalls('2674058');
*/
DECLARE @ro cichar(9);
@ro = (SELECT ro FROM __input);
INSERT INTO __output
  SELECT 
    TRIM(c.mmdd) 
    + ' ' + 
    trim(
      CASE 
        WHEN hour(callts) > 12 THEN 
          trim(cast(hour(callts) - 12 AS sql_char))
        ELSE trim(cast(hour(callts) AS sql_char))
      END 
      + ':' +
      CASE
        WHEN minute(callts) < 10 THEN
          '0' + cast(minute(callts) AS sql_char)
        else cast(minute(callts) AS sql_char)
      END) 
    + ' ' +  
    CASE WHEN hour(callts) > 12 THEN 'PM' ELSE 'AM' END, 
  b.fullname as eeusername, description
  FROM RY2ROCalls a
  LEFT JOIN people b ON a.eeusername = b.eeusername
  LEFT JOIN dds.day c ON CAST(a.callts AS sql_date) = c.thedate
  WHERE ro = @ro;

END;



CREATE PROCEDURE GetRY2ROsForCalls
   ( 
      Writer CICHAR ( 25 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      CloseDate CICHAR ( 12 ) OUTPUT,
      Customer CICHAR ( 30 ) OUTPUT,
      HomePhone CICHAR ( 12 ) OUTPUT,
      WorkPhone CICHAR ( 12 ) OUTPUT,
      CellPhone CICHAR ( 12 ) OUTPUT,
      Vehicle CICHAR ( 60 ) OUTPUT,
      LastCall Memo OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetRY2ROsForCalls();
*/

INSERT INTO __output
SELECT a.writer, a.ro, c.mmddyy AS [Closed Date], a.customer, a.homephone, a.workphone, 
  a.cellphone, a.vehicle, b.description
FROM RY2ROsForCalls a 
LEFT JOIN (
  SELECT *
  FROM RY2ROCalls x
  WHERE callts = (
    SELECT MAX(callts)
    FROM RY2ROCalls
    WHERE ro = x.ro)) b ON a.ro = b.ro
LEFT JOIN dds.day c ON a.closedate = c.thedate    
WHERE a.FollowUpComplete = false;    


END;

CREATE PROCEDURE GetRY2ROToUpdate
   ( 
      ro CICHAR ( 9 ),
      CloseDate DATE OUTPUT,
      customer CICHAR ( 30 ) OUTPUT,
      HomePhone CICHAR ( 12 ) OUTPUT,
      WorkPhone CICHAR ( 12 ) OUTPUT,
      Vehicle CICHAR ( 60 ) OUTPUT,
      Updated TIMESTAMP OUTPUT,
      UpdatedBy CICHAR ( 50 ) OUTPUT,
      Description Memo OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetRY2ROToUpdate('2674058');
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output    
SELECT a.closedate, a.customer, a.homephone, a.workphone, a.vehicle, 
  b.CallTS, b.eeusername, b.description
FROM RY2ROsForCalls a
LEFT JOIN RY2ROCalls  b ON a.ro = b.ro 
WHERE a.ro = @ro; 



END;

CREATE PROCEDURE GetTasksForUser
   ( 
      eeUserName CICHAR ( 50 ),
      task CICHAR ( 45 ) OUTPUT,
      caption CICHAR ( 60 ) OUTPUT,
      displayclass CICHAR ( 45 ) OUTPUT,
      eeUserName CICHAR ( 50 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetTasksForUser ('rodt@rydellchev.com');
EXECUTE PROCEDURE GetTasksForUser ('mhuot@rydellchev.com');
EXECUTE PROCEDURE GetTasksForUser ('aneumann@gfhonda.com');
EXECUTE PROCEDURE GetTasksForUser ('dpederson@gfhonda.com');
EXECUTE PROCEDURE GetTasksForUser ('dbrekke@gfhonda.com');

input: eeusername
output: eeusername AND the tasks for that eeusername
exceptions: andrew neumann IS both manager AND writer
  return only his manager tasks :: test for existence of manager position first
used for the Your Dashboard portion of the LEFT sidebar nav  
*/
DECLARE @user cichar(50);
@user = (SELECT eeUserName FROM __input);

IF EXISTS (
    SELECT 1
    FROM  positionfulfillment 
    WHERE thruts > now()
      AND position = 'Manager'
      AND eeusername = @user) THEN
  INSERT INTO __output
  SELECT c.task, c.caption, c.displayclass, @user
  FROM (  
    SELECT b.eeusername, b.storecode, b.department, b.position
    FROM  positionfulfillment b 
    WHERE b.thruts > now()
      AND b.position = 'Manager'
      AND b.eeusername = @user) a  
  INNER JOIN (    
      SELECT a.task, a.storecode, a.department, a.position, b.caption, b.displayclass
      FROM positiontasks a
      INNER JOIN tasks b ON a.task = b.task
      WHERE a.position = 'Manager') c ON a.storecode = c.storecode
    AND a.department = c.department
    AND a.position = c.position;
ELSEIF EXISTS (
    SELECT 1
    FROM PositionFulfillment 
    WHERE thruts > now()
      AND position IN ('Service Writer','Service Follow Up')
      AND eeusername = @user) THEN
  INSERT INTO __output
  SELECT c.task, c.caption, c.displayclass, @user
  FROM (  
    SELECT b.eeusername, b.storecode, b.department, b.position
    FROM  positionfulfillment b 
    WHERE b.thruts > now()
      AND position IN ('Service Writer','Service Follow Up')
      AND b.eeusername = @user) a  
  INNER JOIN (    
      SELECT a.task, a.storecode, a.department, a.position, b.caption, b.displayclass
      FROM positiontasks a
      INNER JOIN tasks b ON a.task = b.task
      WHERE a.position IN ('Service Writer','Service Follow Up')) c ON a.storecode = c.storecode
    AND a.department = c.department
    AND a.position = c.position;    
END IF;     

END;

CREATE PROCEDURE GetWritersForManager
   ( 
      ManagerEEUserName CICHAR ( 50 ),
      writerEEUserName CICHAR ( 50 ) OUTPUT,
      writerName CICHAR ( 60 ) OUTPUT
   ) 
BEGIN 
/*

execute procedure GetWritersForManager('dwiebusch@rydellchev.com');
execute procedure GetWritersForManager('aberry@rydellchev.com');
execute procedure GetWritersForManager('aneumann@gfhonda.com');
*/
DECLARE @eeusername string;
@eeusername = (SELECT ManagereeUserName FROM __input);

--SELECT top 20 trim(FirstName) + ' ' + TRIM(lastName), eeusername
--FROM Servicewriters
--ORDER BY firstname;

IF @eeusername IN ('mhuot@rydellchev.com','aberry@rydellchev.com','bcahalan@rydellchev.com') THEN 
  INSERT INTO __output 
  SELECT a.eeusername, a.fullname
  FROM people a
  INNER JOIN Positionfulfillment b ON a.eeusername = b.eeusername
    AND b.storecode = 'ry1'
    AND b.position = 'Service Writer';
ELSEIF @eeusername = 'aneumann@gfhonda.com' THEN 
  INSERT INTO __output
  SELECT a.eeusername, a.fullname
  FROM people a
  INNER JOIN Positionfulfillment b ON a.eeusername = b.eeusername
    AND b.storecode = 'ry2'
    AND b.position = 'Service Writer';
ELSE 
  INSERT INTO __output
  SELECT '','' FROM system.iota;
END IF; 




END;

CREATE PROCEDURE InsertRY2ServiceCall
   ( 
      ro CICHAR ( 9 ),
      eeUserName CICHAR ( 50 ),
      description Memo,
      followUpComplete CICHAR ( 12 )
   ) 
BEGIN 
/*
EXECUTE PROCEDURE InsertRY2ServiceCall('16106213','rodt@rydellchev.com','you are killing me here', 'true')
*/
INSERT INTO RY2ROCalls values(
  (SELECT ro FROM __input),
  now(),
  (SELECT eeUserName FROM __input), 
  (SELECT description FROM __input));
  
UPDATE RY2ROsForCalls
  SET followUpComplete = 
    CASE (SELECT followUpComplete FROM __input)
      WHEN 'false' THEN false
      ELSE true
    END 
WHERE ro = (SELECT ro FROM __input); 



END;

alter PROCEDURE GetAgedROs
   ( 
      eeUsername CICHAR ( 50 ),
      firstName CICHAR ( 20 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      openDate CICHAR ( 8 ) OUTPUT,
      customer CICHAR ( 30 ) OUTPUT,
      roStatus CICHAR ( 12 ) OUTPUT,
      vin CICHAR ( 17 ) OUTPUT,
      vehicle CICHAR ( 60 ) OUTPUT,
      statusUpdate Memo OUTPUT,
      theDate DATE OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getagedros('rodt@rydellchev.com');
EXECUTE PROCEDURE getagedros('dpederson@gfhonda.com');
EXECUTE PROCEDURE getagedros('mhuot@rydellchev.com');
EXECUTE PROCEDURE getagedros('aneumann@gfhonda.com');
*/
DECLARE @id string;
@id = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT b.firstname, a.ro, c.mmddyy AS opendate, a.customer, a.rostatus, a.vin, a.vehicle, 
  d.statusUpdate, openDate
FROM AgedRos a
INNER JOIN ServiceWriters b ON a.eeusername = b.eeusername
  AND CASE
    WHEN @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com') THEN b.storecode = 'RY1'
    WHEN @id = 'aneumann@gfhonda.com' THEN b.storecode = 'RY2'
    ELSE b.eeusername = @id 
  END 
LEFT JOIN dds.day c ON a.opendate = c.thedate
LEFT JOIN (
  SELECT *
  FROM AgedROUpdates x
  WHERE updateTS = (
    SELECT MAX(updatets)
    FROM AgedRoUpdates
    WHERE ro = x.ro)) d ON a.ro = d.ro; 





END;
ALTER PROCEDURE GetAgedROUpdates
   ( 
      ro CICHAR ( 9 ),
      updateTS CICHAR ( 20 ) OUTPUT,
      eeUserName CICHAR ( 50 ) OUTPUT,
      statusUpdate Memo OUTPUT
   ) 
BEGIN 
/*
6/12: JOIN to service writers returns NULL IF the call was updated BY a manager
      change it to people
EXECUTE PROCEDURE GetAgedROUpdates('16114855');
*/
DECLARE @ro cichar(9);
@ro = (SELECT ro FROM __input);
  INSERT INTO __output
  SELECT top 100
    TRIM(c.mmdd) 
    + ' ' + 
    trim(
      CASE 
        WHEN hour(updateTS) > 12 THEN 
          trim(cast(hour(updateTS) - 12 AS sql_char))
        ELSE trim(cast(hour(updateTS) AS sql_char))
      END 
      + ':' +
      CASE
        WHEN minute(updateTS) < 10 THEN
          '0' + cast(minute(updateTS) AS sql_char)
        else cast(minute(updateTS) AS sql_char)
      END) 
    + ' ' +  
    CASE WHEN hour(updateTS) > 12 THEN 'PM' ELSE 'AM' END,   
    TRIM(b.firstname) + ' ' +  LEFT(b.lastname, 1) AS eeusername, 
    statusUpdate
  FROM AgedROUpdates a
  LEFT JOIN people b ON a.eeusername = b.eeusername
  LEFT JOIN dds.day c ON CAST(a.updateTS AS sql_date) = c.thedate
  WHERE ro = @ro
  ORDER BY updateTS desc;

END;

alter PROCEDURE GetManagerLandingPageCurrentWeek
   ( 
      eeUsername CICHAR ( 50 ),
      weekid Integer OUTPUT,
      Metric CICHAR ( 45 ) OUTPUT,
      Sun CICHAR ( 22 ) OUTPUT,
      Mon CICHAR ( 22 ) OUTPUT,
      Tue CICHAR ( 22 ) OUTPUT,
      Wed CICHAR ( 22 ) OUTPUT,
      Thu CICHAR ( 22 ) OUTPUT,
      Fri CICHAR ( 22 ) OUTPUT,
      Sat CICHAR ( 22 ) OUTPUT,
      Total CICHAR ( 22 ) OUTPUT,
      seq Integer OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetManagerLandingPageCurrentWeek('mhuot@rydellchev.com');
EXECUTE PROCEDURE GetManagerLandingPageCurrentWeek('aneumann@gfhonda.com');
4/23
  pull the data live, NOT FROM a TABLE, remove TABLE ManagerLandingPage
  ADD checkouts
6/10 
  ADD input parameter, which store matters
  AND the output for each store needs to be just those writers 
*/
DECLARE @id string;
DECLARE @date date;
@id = (SELECT eeUsername FROM __input);
@date = curdate();
IF @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com') THEN 
  INSERT INTO __output
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
    cast(cast(SUM(e.today) AS sql_double) AS sql_char) AS thisweek,
    e.seq
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
        AND c.metric IN ('Labor Sales','ROs Opened','ROs Closed','Walk Arounds')
        AND b.storecode = c.storecode) a /**************************/
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND b.storecode = 'RY1' /**************************/
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq  
  UNION 
  -- the problem IS that this number IS NOT additive accross days
  -- it should be the most recent value
  SELECT f.SundayToSaturdayWeek, f.metric, f.sun,f.mon,f.tue,f.wed,f.thu,f.fri,
    f.sat, cast(cast(g.today AS sql_double) AS sql_char), f.seq
  FROM ( -- dept daily values pivoted
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      e.seq
    FROM ( -- dept daily values (ADD store/dept to grouping to generalize)
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY1' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) e   
    GROUP BY e.SundayToSaturdayWeek, e.metric, e.seq) f  
  LEFT JOIN ( -- most recent dept value for a week 
    SELECT metric, today
    FROM ( -- daily
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY1' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) y
    WHERE thedate = (
      SELECT MAX(thedate)
      FROM (
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY1' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) x)) g ON f.metric = g.metric
  UNION 
  -- the problem IS that this number IS NOT additive accross days
  -- it should be the most recent value
  SELECT f.SundayToSaturdayWeek, f.metric, f.sun,f.mon,f.tue,f.wed,f.thu,f.fri,
    f.sat, cast(cast(g.today AS sql_double) AS sql_char), f.seq
  FROM ( -- dept daily values pivoted
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      e.seq
    FROM ( -- dept daily values (ADD store/dept to grouping to generalize)
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Warranty Calls'
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) e   
    GROUP BY e.SundayToSaturdayWeek, e.metric, e.seq) f  
  LEFT JOIN ( -- most recent dept value for a week 
    SELECT metric, today
    FROM ( -- daily
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Warranty Calls'
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) y
    WHERE thedate = (
      SELECT MAX(thedate)
      FROM (
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Warranty Calls'
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) x)) g ON f.metric = g.metric    
  UNION 
  SELECT x.SundayToSaturdayWeek, 'Cashiering', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
    trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
    trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,1 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric = 'rosCashieredByWriter') a
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
    LEFT JOIN (
      SELECT e.sundaytosaturdayweek, e.metric, 
        max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
        max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
        max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
        max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
        max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
        max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
        max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
        cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
        e.seq
      FROM (  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
          SUM(coalesce(today, 0)) AS today
        FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
          SELECT *
          FROM dds.day a, servicewriters b, servicewritermetrics c
          WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
              AND c.metric  = 'rosCashiered') a
        LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
          AND a.eeusername = b.eeusername 
          AND a.metric = b.metric    
        GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 
  UNION
  SELECT x.SundayToSaturdayWeek, 'Avg Hours/RO', 
    CASE y.sun WHEN 0 then '0' ELSE cast(round(x.sun/y.sun, 1) AS sql_char) END, 
    CASE y.mon WHEN 0 then '0' ELSE cast(round(x.mon/y.mon, 1) AS sql_char) END, 
    CASE y.tue WHEN 0 then '0' ELSE cast(round(x.tue/y.tue, 1) AS sql_char) END, 
    CASE y.wed WHEN 0 then '0' ELSE cast(round(x.wed/y.wed, 1) AS sql_char) END, 
    CASE y.thu WHEN 0 then '0' ELSE cast(round(x.thu/y.thu, 1) AS sql_char) END, 
    CASE y.fri WHEN 0 then '0' ELSE cast(round(x.fri/y.fri, 1) AS sql_char) END, 
    CASE y.sat WHEN 0 then '0' ELSE cast(round(x.sat/y.sat, 1) AS sql_char) END, 
    CASE y.thisweek WHEN 0 then '0' ELSE cast(round(x.thisweek/y.thisweek, 1) AS sql_char) END, 
    4 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'flagHours'
          AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY1' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x  
  LEFT JOIN (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric = 'rosClosedWithFlagHours'
            AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY1' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq)  y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 
  UNION   
  SELECT x.SundayToSaturdayWeek, 'Inspections Requested', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
    trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
    trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,
    8 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'rosClosedWithInspectionLine'
          AND b.storecode = c.storecode) a /**********************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY1' /**********************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
    LEFT JOIN (
      SELECT e.sundaytosaturdayweek, e.metric, 
        max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
        max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
        max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
        max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
        max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
        max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
        max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
        cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
        e.seq
      FROM (  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
          SUM(coalesce(today, 0)) AS today
        FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
          SELECT *
          FROM dds.day a, servicewriters b, servicewritermetrics c
          WHERE a.SundayToSaturdayWeek  = (
              SELECT SundayToSaturdayWeek 
              FROM dds.day
              WHERE thedate = @date)
            AND c.metric  = 'ROs Closed'
            AND b.storecode = c.storecode) a /**********************************/
        LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
          AND b.storecode = 'RY1' /**********************************/
          AND a.eeusername = b.eeusername 
          AND a.metric = b.metric    
        GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  UNION 
  -- checkouts
  SELECT f.SundayToSaturdayWeek, 'CheckOuts',f.sun,f.mon,f.tue,f.wed,f.thu,
    f.fri,f.sat, cast(cast(g.total AS sql_double) AS sql_char), f.seq
  FROM (
    SELECT e.sundaytosaturdayweek, 'Check Outs', 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      999 AS seq
    FROM (    
      SELECT SundayToSaturdayWeek, thedate, dayname, COUNT(*) AS today
      FROM serviceWriterMetricData 
      WHERE metric = 'walk arounds' 
        AND storecode = 'RY1' /****************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, thedate, dayname) e
    GROUP BY SundayToSaturdayWeek ) f   
  LEFT JOIN (
    SELECT SundayToSaturdayWeek, COUNT(*) AS total
    FROM serviceWriterMetricData 
    WHERE metric = 'walk arounds' 
      AND storecode = 'RY1' /****************************/
      AND SundayToSaturdayWeek = (
        SELECT SundayToSaturdayWeek
        FROM dds.day
        WHERE thedate = @date)
    GROUP BY SundayToSaturdayWeek) g ON f.SundayToSaturdayWeek = g.SundayToSaturdayWeek;   
ELSEIF @id = 'aneumann@gfhonda.com' THEN   
  INSERT INTO __output
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
    cast(cast(SUM(e.today) AS sql_double) AS sql_char) AS thisweek,
    e.seq
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
        AND c.metric IN ('Labor Sales','ROs Opened','ROs Closed','Walk Arounds')
        AND b.storecode = c.storecode) a /**************************/
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND b.storecode = 'RY2' /**************************/
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq
  UNION 
  -- the problem IS that this number IS NOT additive accross days
  -- it should be the most recent value
  SELECT f.SundayToSaturdayWeek, f.metric, f.sun,f.mon,f.tue,f.wed,f.thu,f.fri,
    f.sat, cast(cast(g.today AS sql_double) AS sql_char), f.seq
  FROM ( -- dept daily values pivoted
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      e.seq
    FROM ( -- dept daily values (ADD store/dept to grouping to generalize)
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY2' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) e   
    GROUP BY e.SundayToSaturdayWeek, e.metric, e.seq) f  
  LEFT JOIN ( -- most recent dept value for a week 
    SELECT metric, today
    FROM ( -- daily
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY2' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) y
    WHERE thedate = (
      SELECT MAX(thedate)
      FROM (
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY2' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) x)) g ON f.metric = g.metric
  UNION
  SELECT x.SundayToSaturdayWeek, 'Avg Hours/RO', 
    CASE y.sun WHEN 0 then '0' ELSE cast(round(x.sun/y.sun, 1) AS sql_char) END, 
    CASE y.mon WHEN 0 then '0' ELSE cast(round(x.mon/y.mon, 1) AS sql_char) END, 
    CASE y.tue WHEN 0 then '0' ELSE cast(round(x.tue/y.tue, 1) AS sql_char) END, 
    CASE y.wed WHEN 0 then '0' ELSE cast(round(x.wed/y.wed, 1) AS sql_char) END, 
    CASE y.thu WHEN 0 then '0' ELSE cast(round(x.thu/y.thu, 1) AS sql_char) END, 
    CASE y.fri WHEN 0 then '0' ELSE cast(round(x.fri/y.fri, 1) AS sql_char) END, 
    CASE y.sat WHEN 0 then '0' ELSE cast(round(x.sat/y.sat, 1) AS sql_char) END, 
    CASE y.thisweek WHEN 0 then '0' ELSE cast(round(x.thisweek/y.thisweek, 1) AS sql_char) END, 
    4 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'flagHours'
          AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY2' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x  
  LEFT JOIN (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric = 'rosClosedWithFlagHours'
            AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY2' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq)  y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  UNION   
  SELECT x.SundayToSaturdayWeek, 'Inspections Requested', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
    trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
    trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,
    8 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'rosClosedWithInspectionLine'
          AND b.storecode = c.storecode) a /**********************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY2' /**********************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
    LEFT JOIN (
      SELECT e.sundaytosaturdayweek, e.metric, 
        max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
        max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
        max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
        max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
        max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
        max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
        max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
        cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
        e.seq
      FROM (  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
          SUM(coalesce(today, 0)) AS today
        FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
          SELECT *
          FROM dds.day a, servicewriters b, servicewritermetrics c
          WHERE a.SundayToSaturdayWeek  = (
              SELECT SundayToSaturdayWeek 
              FROM dds.day
              WHERE thedate = @date)
            AND c.metric  = 'ROs Closed'
            AND b.storecode = c.storecode) a /**********************************/
        LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
          AND b.storecode = 'RY2' /**********************************/
          AND a.eeusername = b.eeusername 
          AND a.metric = b.metric    
        GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  UNION
  -- checkouts
  SELECT f.SundayToSaturdayWeek, 'CheckOuts',f.sun,f.mon,f.tue,f.wed,f.thu,
    f.fri,f.sat, cast(cast(g.total AS sql_double) AS sql_char), f.seq
  FROM (
    SELECT e.sundaytosaturdayweek, 'Check Outs', 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      999 AS seq
    FROM (    
      SELECT SundayToSaturdayWeek, thedate, dayname, COUNT(*) AS today
      FROM serviceWriterMetricData 
      WHERE metric = 'walk arounds' 
        AND storecode = 'RY2' /****************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, thedate, dayname) e
    GROUP BY SundayToSaturdayWeek ) f   
  LEFT JOIN (
    SELECT SundayToSaturdayWeek, COUNT(*) AS total
    FROM serviceWriterMetricData 
    WHERE metric = 'walk arounds' 
      AND storecode = 'RY2' /****************************/
      AND SundayToSaturdayWeek = (
        SELECT SundayToSaturdayWeek
        FROM dds.day
        WHERE thedate = @date)
    GROUP BY SundayToSaturdayWeek) g ON f.SundayToSaturdayWeek = g.SundayToSaturdayWeek;               

ENDIF;




END;
ALTER PROCEDURE GetManagerLandingPagePreviousWeek
   ( 
      eeUsername CICHAR ( 50 ),
      weekid Integer OUTPUT,
      Metric CICHAR ( 45 ) OUTPUT,
      Sun CICHAR ( 22 ) OUTPUT,
      Mon CICHAR ( 22 ) OUTPUT,
      Tue CICHAR ( 22 ) OUTPUT,
      Wed CICHAR ( 22 ) OUTPUT,
      Thu CICHAR ( 22 ) OUTPUT,
      Fri CICHAR ( 22 ) OUTPUT,
      Sat CICHAR ( 22 ) OUTPUT,
      Total CICHAR ( 22 ) OUTPUT,
      seq Integer OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetManagerLandingPageCurrentWeek('mhuot@rydellchev.com');
EXECUTE PROCEDURE GetManagerLandingPageCurrentWeek('aneumann@gfhonda.com');
4/23
  pull the data live, NOT FROM a TABLE, remove TABLE ManagerLandingPage
  ADD checkouts
6/10 
  ADD input parameter, which store matters
  AND the output for each store needs to be just those writers   
*/

DECLARE @id string;
DECLARE @date date;
@id = (SELECT eeUsername FROM __input);
@date = curdate() - 7;
IF @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com') THEN 
  INSERT INTO __output
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
    cast(cast(SUM(e.today) AS sql_double) AS sql_char) AS thisweek,
    e.seq
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
        AND c.metric IN ('Labor Sales','ROs Opened','ROs Closed','Walk Arounds')
        AND b.storecode = c.storecode) a /**************************/
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND b.storecode = 'RY1' /**************************/
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq  
  UNION 
  -- the problem IS that this number IS NOT additive accross days
  -- it should be the most recent value
  SELECT f.SundayToSaturdayWeek, f.metric, f.sun,f.mon,f.tue,f.wed,f.thu,f.fri,
    f.sat, cast(cast(g.today AS sql_double) AS sql_char), f.seq
  FROM ( -- dept daily values pivoted
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      e.seq
    FROM ( -- dept daily values (ADD store/dept to grouping to generalize)
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY1' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) e   
    GROUP BY e.SundayToSaturdayWeek, e.metric, e.seq) f  
  LEFT JOIN ( -- most recent dept value for a week 
    SELECT metric, today
    FROM ( -- daily
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY1' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) y
    WHERE thedate = (
      SELECT MAX(thedate)
      FROM (
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY1' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) x)) g ON f.metric = g.metric
  UNION 
  -- the problem IS that this number IS NOT additive accross days
  -- it should be the most recent value
  SELECT f.SundayToSaturdayWeek, f.metric, f.sun,f.mon,f.tue,f.wed,f.thu,f.fri,
    f.sat, cast(cast(g.today AS sql_double) AS sql_char), f.seq
  FROM ( -- dept daily values pivoted
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      e.seq
    FROM ( -- dept daily values (ADD store/dept to grouping to generalize)
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Warranty Calls'
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) e   
    GROUP BY e.SundayToSaturdayWeek, e.metric, e.seq) f  
  LEFT JOIN ( -- most recent dept value for a week 
    SELECT metric, today
    FROM ( -- daily
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Warranty Calls'
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) y
    WHERE thedate = (
      SELECT MAX(thedate)
      FROM (
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Warranty Calls'
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) x)) g ON f.metric = g.metric    
  UNION 
  SELECT x.SundayToSaturdayWeek, 'Cashiering', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
    trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
    trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,1 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric = 'rosCashieredByWriter') a
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
    LEFT JOIN (
      SELECT e.sundaytosaturdayweek, e.metric, 
        max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
        max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
        max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
        max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
        max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
        max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
        max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
        cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
        e.seq
      FROM (  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
          SUM(coalesce(today, 0)) AS today
        FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
          SELECT *
          FROM dds.day a, servicewriters b, servicewritermetrics c
          WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
              AND c.metric  = 'rosCashiered') a
        LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
          AND a.eeusername = b.eeusername 
          AND a.metric = b.metric    
        GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 
  UNION
  SELECT x.SundayToSaturdayWeek, 'Avg Hours/RO', 
    CASE y.sun WHEN 0 then '0' ELSE cast(round(x.sun/y.sun, 1) AS sql_char) END, 
    CASE y.mon WHEN 0 then '0' ELSE cast(round(x.mon/y.mon, 1) AS sql_char) END, 
    CASE y.tue WHEN 0 then '0' ELSE cast(round(x.tue/y.tue, 1) AS sql_char) END, 
    CASE y.wed WHEN 0 then '0' ELSE cast(round(x.wed/y.wed, 1) AS sql_char) END, 
    CASE y.thu WHEN 0 then '0' ELSE cast(round(x.thu/y.thu, 1) AS sql_char) END, 
    CASE y.fri WHEN 0 then '0' ELSE cast(round(x.fri/y.fri, 1) AS sql_char) END, 
    CASE y.sat WHEN 0 then '0' ELSE cast(round(x.sat/y.sat, 1) AS sql_char) END, 
    CASE y.thisweek WHEN 0 then '0' ELSE cast(round(x.thisweek/y.thisweek, 1) AS sql_char) END, 
    4 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'flagHours'
          AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY1' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x  
  LEFT JOIN (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric = 'rosClosedWithFlagHours'
            AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY1' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq)  y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 
  UNION   
  SELECT x.SundayToSaturdayWeek, 'Inspections Requested', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
    trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
    trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,
    8 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'rosClosedWithInspectionLine'
          AND b.storecode = c.storecode) a /**********************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY1' /**********************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
    LEFT JOIN (
      SELECT e.sundaytosaturdayweek, e.metric, 
        max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
        max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
        max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
        max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
        max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
        max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
        max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
        cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
        e.seq
      FROM (  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
          SUM(coalesce(today, 0)) AS today
        FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
          SELECT *
          FROM dds.day a, servicewriters b, servicewritermetrics c
          WHERE a.SundayToSaturdayWeek  = (
              SELECT SundayToSaturdayWeek 
              FROM dds.day
              WHERE thedate = @date)
            AND c.metric  = 'ROs Closed'
            AND b.storecode = c.storecode) a /**********************************/
        LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
          AND b.storecode = 'RY1' /**********************************/
          AND a.eeusername = b.eeusername 
          AND a.metric = b.metric    
        GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  UNION 
  -- checkouts
  SELECT f.SundayToSaturdayWeek, 'CheckOuts',f.sun,f.mon,f.tue,f.wed,f.thu,
    f.fri,f.sat, cast(cast(g.total AS sql_double) AS sql_char), f.seq
  FROM (
    SELECT e.sundaytosaturdayweek, 'Check Outs', 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      999 AS seq
    FROM (    
      SELECT SundayToSaturdayWeek, thedate, dayname, COUNT(*) AS today
      FROM serviceWriterMetricData 
      WHERE metric = 'walk arounds' 
        AND storecode = 'RY1' /****************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, thedate, dayname) e
    GROUP BY SundayToSaturdayWeek ) f   
  LEFT JOIN (
    SELECT SundayToSaturdayWeek, COUNT(*) AS total
    FROM serviceWriterMetricData 
    WHERE metric = 'walk arounds' 
      AND storecode = 'RY1' /****************************/
      AND SundayToSaturdayWeek = (
        SELECT SundayToSaturdayWeek
        FROM dds.day
        WHERE thedate = @date)
    GROUP BY SundayToSaturdayWeek) g ON f.SundayToSaturdayWeek = g.SundayToSaturdayWeek;      
ELSEIF @id = 'aneumann@gfhonda.com' THEN   
  INSERT INTO __output
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
    cast(cast(SUM(e.today) AS sql_double) AS sql_char) AS thisweek,
    e.seq
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
        AND c.metric IN ('Labor Sales','ROs Opened','ROs Closed','Walk Arounds')
        AND b.storecode = c.storecode) a /**************************/
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND b.storecode = 'RY2' /**************************/
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq
  UNION 
  -- the problem IS that this number IS NOT additive accross days
  -- it should be the most recent value
  SELECT f.SundayToSaturdayWeek, f.metric, f.sun,f.mon,f.tue,f.wed,f.thu,f.fri,
    f.sat, cast(cast(g.today AS sql_double) AS sql_char), f.seq
  FROM ( -- dept daily values pivoted
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      e.seq
    FROM ( -- dept daily values (ADD store/dept to grouping to generalize)
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY2' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) e   
    GROUP BY e.SundayToSaturdayWeek, e.metric, e.seq) f  
  LEFT JOIN ( -- most recent dept value for a week 
    SELECT metric, today
    FROM ( -- daily
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY2' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) y
    WHERE thedate = (
      SELECT MAX(thedate)
      FROM (
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY2' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) x)) g ON f.metric = g.metric
  UNION
  SELECT x.SundayToSaturdayWeek, 'Avg Hours/RO', 
    CASE y.sun WHEN 0 then '0' ELSE cast(round(x.sun/y.sun, 1) AS sql_char) END, 
    CASE y.mon WHEN 0 then '0' ELSE cast(round(x.mon/y.mon, 1) AS sql_char) END, 
    CASE y.tue WHEN 0 then '0' ELSE cast(round(x.tue/y.tue, 1) AS sql_char) END, 
    CASE y.wed WHEN 0 then '0' ELSE cast(round(x.wed/y.wed, 1) AS sql_char) END, 
    CASE y.thu WHEN 0 then '0' ELSE cast(round(x.thu/y.thu, 1) AS sql_char) END, 
    CASE y.fri WHEN 0 then '0' ELSE cast(round(x.fri/y.fri, 1) AS sql_char) END, 
    CASE y.sat WHEN 0 then '0' ELSE cast(round(x.sat/y.sat, 1) AS sql_char) END, 
    CASE y.thisweek WHEN 0 then '0' ELSE cast(round(x.thisweek/y.thisweek, 1) AS sql_char) END, 
    4 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'flagHours'
          AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY2' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x  
  LEFT JOIN (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric = 'rosClosedWithFlagHours'
            AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY2' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq)  y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  UNION   
  SELECT x.SundayToSaturdayWeek, 'Inspections Requested', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
    trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
    trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,
    8 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'rosClosedWithInspectionLine'
          AND b.storecode = c.storecode) a /**********************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY2' /**********************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
    LEFT JOIN (
      SELECT e.sundaytosaturdayweek, e.metric, 
        max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
        max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
        max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
        max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
        max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
        max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
        max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
        cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
        e.seq
      FROM (  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
          SUM(coalesce(today, 0)) AS today
        FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
          SELECT *
          FROM dds.day a, servicewriters b, servicewritermetrics c
          WHERE a.SundayToSaturdayWeek  = (
              SELECT SundayToSaturdayWeek 
              FROM dds.day
              WHERE thedate = @date)
            AND c.metric  = 'ROs Closed'
            AND b.storecode = c.storecode) a /**********************************/
        LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
          AND b.storecode = 'RY2' /**********************************/
          AND a.eeusername = b.eeusername 
          AND a.metric = b.metric    
        GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  UNION
  -- checkouts
  SELECT f.SundayToSaturdayWeek, 'CheckOuts',f.sun,f.mon,f.tue,f.wed,f.thu,
    f.fri,f.sat, cast(cast(g.total AS sql_double) AS sql_char), f.seq
  FROM (
    SELECT e.sundaytosaturdayweek, 'Check Outs', 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      999 AS seq
    FROM (    
      SELECT SundayToSaturdayWeek, thedate, dayname, COUNT(*) AS today
      FROM serviceWriterMetricData 
      WHERE metric = 'walk arounds' 
        AND storecode = 'RY2' /****************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, thedate, dayname) e
    GROUP BY SundayToSaturdayWeek ) f   
  LEFT JOIN (
    SELECT SundayToSaturdayWeek, COUNT(*) AS total
    FROM serviceWriterMetricData 
    WHERE metric = 'walk arounds' 
      AND storecode = 'RY2' /****************************/
      AND SundayToSaturdayWeek = (
        SELECT SundayToSaturdayWeek
        FROM dds.day
        WHERE thedate = @date)
    GROUP BY SundayToSaturdayWeek) g ON f.SundayToSaturdayWeek = g.SundayToSaturdayWeek;               

ENDIF;





END;
ALTER PROCEDURE GetWarrantyCalls
   ( 
      ro CICHAR ( 9 ),
      callTS CICHAR ( 20 ) OUTPUT,
      eeUserName CICHAR ( 50 ) OUTPUT,
      description Memo OUTPUT
   ) 
BEGIN 
/*
6/12: JOIN to service writers returns NULL IF the call was updated BY a manager
      change it to people
EXECUTE PROCEDURE GetWarrantyCalls('16121810');
select * FROM warrantycalls WHERE ro = '16110108'
*/
DECLARE @ro cichar(9);
@ro = (SELECT ro FROM __input);
INSERT INTO __output
  SELECT top 100
    TRIM(c.mmdd) 
    + ' ' + 
    trim(
      CASE 
        WHEN hour(callts) > 12 THEN 
          trim(cast(hour(callts) - 12 AS sql_char))
        ELSE trim(cast(hour(callts) AS sql_char))
      END 
      + ':' +
      CASE
        WHEN minute(callts) < 10 THEN
          '0' + cast(minute(callts) AS sql_char)
        else cast(minute(callts) AS sql_char)
      END) 
    + ' ' +  
    CASE WHEN hour(callts) > 12 THEN 'PM' ELSE 'AM' END, 
  TRIM(b.firstname) + ' ' +  LEFT(b.lastname, 1) as eeusername, description
  FROM WarrantyCalls a
--  LEFT JOIN servicewriters b ON a.eeusername = b.eeusername
  LEFT JOIN people b ON a.eeusername = b.eeusername
  LEFT JOIN dds.day c ON CAST(a.callts AS sql_date) = c.thedate
  WHERE ro = @ro
  ORDER BY callTS desc;

END;

ALTER PROCEDURE GetWarrantyROs
   ( 
      eeUserName CICHAR ( 50 ),
      FirstName CICHAR ( 20 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      CloseDate CICHAR ( 12 ) OUTPUT,
      Customer CICHAR ( 30 ) OUTPUT,
      HomePhone CICHAR ( 12 ) OUTPUT,
      WorkPhone CICHAR ( 12 ) OUTPUT,
      CellPhone CICHAR ( 12 ) OUTPUT,
      Vehicle CICHAR ( 60 ) OUTPUT,
      LastCall Memo OUTPUT,
      theDate DATE OUTPUT,
      aged LOGICAL OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetWarrantyROs('rodt@rydellchev.com');
EXECUTE PROCEDURE GetWarrantyROs('mhuot@rydellchev.com');
*/
DECLARE @id string;
@id = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT d.firstname, a.ro, c.mmddyy AS [Closed Date], a.customer, a.HomePhone, a.WorkPhone, 
  a.CellPhone, a.vehicle, b.description, closeDate,
  CASE
    WHEN curdate() - closeDate > 1 THEN true
    ELSE false
  END 
FROM WarrantyROs a
LEFT JOIN dds.day c ON a.closedate = c.thedate
LEFT JOIN ServiceWriters d ON a.eeUserName = d.eeUserName
LEFT JOIN (
  SELECT *
  FROM WarrantyCalls x
  WHERE CallTS = (
    SELECT MAX(CallTS)
    FROM WarrantyCalls
    WHERE ro = x.ro)) b ON a.ro = b.ro 
WHERE 
  CASE
    WHEN @id NOT IN ('mhuot@rydellchev.com','aberry@rydellchev.com') THEN a.eeUsername = @id
    ELSE 1 = 1
  END
  AND FollowUpComplete = false;






END;
ALTER PROCEDURE GetWriterLandingPage
   ( 
      eeUserName CICHAR ( 50 ),
      Metric CICHAR ( 45 ) OUTPUT,
      Sun CICHAR ( 12 ) OUTPUT,
      Mon CICHAR ( 12 ) OUTPUT,
      Tue CICHAR ( 12 ) OUTPUT,
      Wed CICHAR ( 12 ) OUTPUT,
      Thu CICHAR ( 12 ) OUTPUT,
      Fri CICHAR ( 12 ) OUTPUT,
      Sat CICHAR ( 12 ) OUTPUT,
      Total CICHAR ( 12 ) OUTPUT,
      seq Integer OUTPUT,
      def CICHAR ( 250 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetWriterLandingPage('rodt@rydellchev.com');
EXECUTE PROCEDURE GetWriterLandingPage('dpederson@gfhonda.com');

need to pivot the data to match the landing page FROM the functional spec:
              sun  mon  tue  thr  fri   sat
Cashiering     3    4    6    4    8     8
Aged OPEN Ros  5    7    0    7    5     7
...
        

  DECLARE @id string;
  @id = (SELECT eeUserName FROM __input);
  INSERT INTO __output  
SELECT metric, Sun,Mon,Tue,Wed,Thu,Fri,Sat,Total
FROM writerlandingpage a
WHERE  weekid = (
    SELECT sundaytosaturdayweek
    FROM dds.day
    WHERE thedate = curdate())
  AND eeUserName = @id;

*/


DECLARE @eeusername string;
@eeusername = (SELECT eeusername FROM __input);
INSERT INTO __output 
SELECT d.metric, trim(d.sun) AS Sun, trim(d.mon) AS Mon, trim(d.tue) AS Tue, 
  trim(d.wed) AS Wed, trim(d.thu) AS Thu, trim(d.fri) AS Fri,
  trim(d.sat) AS Sat, e.thisweekdisplay AS Total, d.seq, d.def
FROM ( -- daily
  SELECT a.sundaytosaturdayweek, 0, a.fullname, a.writernumber, a.metric, 
    max(CASE WHEN a.dayname = 'Sunday' THEN todayDisplay END) AS Sun,
    max(CASE WHEN a.dayname = 'Monday' THEN todayDisplay END) AS Mon,
    max(CASE WHEN a.dayname = 'Tuesday' THEN todayDisplay END) AS Tue,
    max(CASE WHEN a.dayname = 'Wednesday' THEN todayDisplay END) AS Wed,
    max(CASE WHEN a.dayname = 'Thursday' THEN todayDisplay END) AS Thu,
    max(CASE WHEN a.dayname = 'Friday' THEN todayDisplay END) AS Fri,
    max(CASE WHEN a.dayname = 'Saturday' THEN todayDisplay END) AS Sat,
    eeusername, seq, MAX(def) AS def
  FROM (
    SELECT a.*,
      b.todaydisplay,
      b.thisweekdisplay, b.*
    FROM ( -- 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL with seq)
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, b.eeusername, 
        b.fullname, b.writernumber, c.metric, c.seq, c.def
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
        SELECT SundayToSaturdayWeek 
        FROM dds.day
        WHERE thedate = curdate()) -- current week 
--        WHERE thedate = curdate() - 7) -- previous week
        AND c.seq IS NOT NULL
        AND b.storecode = c.storecode) a ----------------------------------------------
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric) a 
  GROUP BY a.sundaytosaturdayweek, a.fullname, a.writernumber, a.metric, eeusername, seq) d
LEFT JOIN ( -- this appears to be working for seq 1,4-7 to get weekly
  SELECT a.eeusername, a.metric, a.SundayToSaturdayWeek, a.thisweekdisplay
  FROM servicewritermetricdata a
  INNER JOIN servicewritermetrics c ON a.metric = c.metric
    AND a.storecode = c.storecode -- this fixes the dups
    AND c.seq IS NOT NULL 
  INNER JOIN (
      SELECT MAX(thedate) AS thedate, eeusername, metric, SundayToSaturdayWeek
      FROM servicewritermetricdata
      WHERE thisweekdisplay IS NOT NULL 
      GROUP BY eeusername, metric, SundayToSaturdayWeek) b ON a.thedate = b.thedate
        and a.eeusername = b.eeusername
        and a.metric = b.metric
        and a.SundayToSaturdayWeek = b.SundayToSaturdayWeek) e ON d.eeusername = e.eeusername
    AND d.metric = e.metric
    AND d.SundayToSaturdayWeek = e.SundayToSaturdayWeek 
WHERE d.eeUserName = @eeUserName;


END;


ALTER PROCEDURE InsertServiceWriterCheckout
   ( 
      eeUserName CICHAR ( 50 ),
      theDate DATE,
      csi DOUBLE ( 15 ),
      walkarounds Integer
   ) 
BEGIN 
/*
EXECUTE PROCEDURE InsertServiceWriterCheckout(
  'rodt@rydellchev.com',curdate(), 48.9, 15);
  
DELETE FROM ServiceWriterMetricData
WHERE eeusername = 'rodt@rydellchev.com'
  AND thedate = curdatE()
  AND metric in ('csi', 'walk arounds')
*/
/*
  INSERT INTO ServiceWriterCheckouts values(
    (SELECT eeusername FROM __input),
    (SELECT thedate FROM __input),
    (SELECT csi FROM __input),
    (SELECT walkarounds FROM __input));

END;
*/
DECLARE @eeusername string;
DECLARE @date date;
DECLARE @csi double;
DECLARE @walkarounds integer;
@eeusername = (SELECT eeusername FROM __input);
@date = (SELECT thedate FROM __input);
@csi = (SELECT csi FROM __input);
@walkarounds = (SELECT walkarounds FROM __input);

INSERT INTO ServiceWriterMetricData
SELECT @eeusername, c.storecode, c.fullname, c.lastname, c.firstname, c.writernumber,
  a.datekey, @date, a.dayofweek, a.dayname, a.dayofmonth, a.SundayToSaturdayWeek,
  'CSI', b.seq, @csi, @csi, trim(CAST(@csi AS sql_char)), trim(CAST(@csi AS sql_char))
FROM dds.day a, servicewritermetrics b, servicewriters c
WHERE a.thedate = @date
  AND c.eeusername = @eeusername
  AND b.metric = 'CSI'
  AND b.storecode = c.storecode;

INSERT INTO ServiceWriterMetricData
SELECT @eeusername, c.storecode, c.fullname, c.lastname, c.firstname, c.writernumber,
  a.datekey, @date, a.dayofweek, a.dayname, a.dayofmonth, a.SundayToSaturdayWeek,
  'Walk Arounds', b.seq, @walkarounds, @walkarounds, trim(CAST(@walkarounds AS sql_char)), trim(CAST(@walkarounds AS sql_char))
FROM dds.day a, servicewritermetrics b, servicewriters c
WHERE a.thedate = @date
  AND c.eeusername = @eeusername
  AND b.metric = 'Walk Arounds'
  AND b.storecode = c.storecode;


END;

ALTER PROCEDURE swMetricDataToday
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE swMetricDataToday();
4/13 rosClosedWithInspectionLine: changed to 23I only
     populate TABLE WarrantyROs prior to UPDATE serviceWriterMetricData.OPEN Warranty Calls
5/4
  return a 0 instead of NULL for OPEN warranty calls     
6/8
  modified for ry2  
    include opcode 24z for ry2 inspections
    exclude cashiering, warranty calls for ry2
    ADD HondaNissanCalls for ry2  
*/
TRY 
  BEGIN TRANSACTION;
  TRY
    TRY  
    -- ROs Closed -------------------------------------------------------------   
      DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() AND metric = 'ROs Closed';
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Closed', 0, COUNT(*) AS today,
        0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
      FROM factROToday a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey -- cashiered ros only
        AND b.datetype = 'date'
        AND b.thedate = curdate() 
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber  
      WHERE a.void = false     
      GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek;   
      -- ROs Closed -----------------------------------------------------------------  
      -- weekly -------------------
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'ROs Closed') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'ROs Closed';   
    CATCH ALL
      RAISE swMetricDataToday(100, __errtext);
    END TRY;
    TRY     
    -- rosClosedWithInspectionLine ------------------------------------------------ 
      DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() AND metric = 'rosClosedWithInspectionLine';  
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithInspectionLine', 0, COUNT(*) AS today,
        0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
      FROM factROToday a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate = curdate() 
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false 
        AND EXISTS (
          SELECT 1
          FROM factROLineToday
          WHERE ro = a.ro
          AND opcode IN ('23I', '24Z')) -- ry2 mod  
      GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek;
    CATCH ALL
      RAISE swMetricDataToday(101, __errtext);  
    END TRY;  
    TRY
    -- rosCashiered ---------------------------------------------------------------
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashiered' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData         
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'rosCashiered', 0 AS seq,
        COUNT(*) AS today,
        0 AS thisweek,
        TRIM(CAST(COUNT(*) AS sql_char)),
        CAST(NULL AS sql_char)
      FROM factROToday a
      INNER JOIN 
        dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate = curdate() 
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber
        AND c.storecode = 'ry1' -- ry2 mod
      WHERE EXISTS (
          SELECT 1
          FROM tmpGLPTRNS
          WHERE gtdoc# = a.ro)
        AND NOT EXISTS (
          SELECT 1 
          FROM factROLineToday
          WHERE ro = a.ro
            AND paytype IN ('w','i'))
        AND NOT EXISTS (
          SELECT 1
          FROM factROLineToday
          WHERE ro = a.ro
            AND servicetype <> 'mr')      
        AND a.void = false 
      GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek; 
    CATCH ALL
      RAISE swMetricDataToday(102, __errtext);  
    END TRY;      
    TRY 
    -- rosCashieredByWriter -------------------------------------------------------
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashieredByWriter' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData        
      SELECT a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
        a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
        a.dayOfMonth, a.sundayToSaturdayWeek, 'rosCashieredByWriter', 0,
        SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS today,
        0,
        TRIM(CAST(SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS sql_char)),
        CAST(NULL AS sql_char)
      FROM (  
        SELECT b.datekey, b.thedate, b.dayOfWeek, b.dayName,
          b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
          g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
          g.writerNumber, f.gquser
        FROM factROToday a
        INNER JOIN 
          dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate = curdate()
        INNER JOIN tmpGLPTRNS e ON a.ro = e.gtdoc# 
        INNER JOIN tmpGLPDTIM f ON e.gttrn# = f.gqtrn#  
        INNER JOIN ServiceWriters g ON a.storecode = g.storecode
          AND a.writerid = g.writernumber
          AND g.storecode = 'ry1' -- ry2 mod
        WHERE NOT EXISTS (
            SELECT 1 
            FROM factROLineToday
            WHERE ro = a.ro
              AND paytype IN ('w','i'))
          AND NOT EXISTS (
            SELECT 1
            FROM factROLineToday
            WHERE ro = a.ro
              AND servicetype <> 'mr')      
          AND a.void = false 
        GROUP BY b.datekey, b.thedate, b.dayOfWeek, b.dayName,
          b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
          g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
          g.writerNumber, f.gquser) a
      LEFT JOIN ServiceWriters b ON a.gquser = b.dtUserName   
      GROUP BY a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
        a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
        a.dayOfMonth, a.sundayToSaturdayWeek; 
    CATCH ALL
      RAISE swMetricDataToday(103, __errtext);  
    END TRY;    
    TRY  
    -- rosClosedWithFlagHours -----------------------------------------------------
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosClosedWithFlagHours' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithFlagHours', 0 AS seq, 
        howmany AS today, 0 AS thisweek, 
        trim(cast(howmany AS sql_char)), CAST(NULL AS sql_char) 
      FROM (
        SELECT b.thedate, a.writerid, COUNT(*) AS howmany
        FROM factROToday a
        INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate = curdate() 
        INNER JOIN ServiceWriters c ON a.storecode = c.storecode
          AND a.writerid = c.writernumber  
        WHERE EXISTS (
          SELECT 1
          FROM factROLineToday
          WHERE ro = a.ro
          AND lineflaghours > 0)   
        GROUP BY b.thedate, a.writerid) a 
      LEFT JOIN dds.day b ON a.thedate = b.thedate   
      LEFT JOIN servicewriters c ON a.writerid = c.writernumber;  
    -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'rosClosedWithFlagHours') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric; 
    CATCH ALL
      RAISE swMetricDataToday(104, __errtext);  
    END TRY;        
    -- flaghours ------------------------------------------------------------------
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'flagHours' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'flagHours', 0 AS seq, 
        flaghours AS today, 0 AS thisweek, 
        trim(cast(flaghours AS sql_char)), CAST(NULL AS sql_char) 
      FROM (
        SELECT b.writerid, b.thedate, SUM(a.lineflaghours) AS flaghours
        FROM factROLineToday a
        INNER JOIN (
          SELECT *
          FROM factROToday a
          INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate = curdate() 
          INNER JOIN servicewriters c ON a.storecode = c.storecode
            AND a.writerid = c.writernumber
          WHERE a.void = false) b ON a.ro = b.ro 
        GROUP BY b.writerid, b.thedate) a
      LEFT JOIN dds.day b ON a.thedate = b.thedate   
      LEFT JOIN servicewriters c ON a.writerid = c.writernumber;
      -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'flagHours') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric; 
    CATCH ALL
      RAISE swMetricDataToday(105, __errtext);  
    END TRY;    
    TRY    
    -- Cashiering -----------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosCashieredByWriter & 
      ServiceWriterMetricData.rosCashiered to be complete
      generate weekly totals before generating display metric (cashiering)
    */
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric in ('rosCashiered','rosCashieredByWriter')) x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN ('rosCashiered','rosCashieredByWriter') ; 
        
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Cashiering';
      INSERT INTO ServiceWriterMetricData
      SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Cashiering', 1 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosCashieredByWriter'
        AND b.metric = 'rosCashiered';
    CATCH ALL
      RAISE swMetricDataToday(106, __errtext);  
    END TRY;     
    -- Open Aged ROs --------------------------------------------------------------------
    /*
    UPDATE throughout the day IN terms of removing ros that get closed
    DELETE FROM agedros daily based ON an ro closing
    overnight a full scrap
    AgedROs TABLE IS necessary for the nightly/today to run
    
    no need to UPDATE with newly aged ros thru out the day
    
    need to UPDATE the weekly numbers here
    ALL i need are this week, which will be the same AS the daily total each day
    */
    TRY 
      DELETE 
      FROM agedros 
      WHERE EXISTS (
        SELECT 1
        FROM factROToday
        WHERE ro = agedros.ro
        AND finalclosedatekey <> 7306);
        
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Aged ROs' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
        a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Aged ROs', 2 AS seq, b.howmany AS today,
      --  0 AS thisWeek, trim(CAST(b.howmany AS sql_char)), CAST(NULL AS sql_char)
        howmany thisWeek, trim(CAST(howmany AS sql_char)) AS todayDisplay, 
        trim(CAST(howmany AS sql_char)) AS thisWeekDisplay
      FROM dds.day a, ( 
        SELECT eeusername, COUNT(*) AS howmany
        FROM agedros  
        GROUP BY eeusername) b, 
        servicewriters c
      WHERE a.thedate = curdate()
        AND b.eeusername = c.eeusername;  
    CATCH ALL
      RAISE swMetricDataToday(107, __errtext);  
    END TRY;        
    -- Open Warranty Calls --------------------------------------------------------
    /*
      the challenges here, like AgedROs, the data depends ON dds.factro, 
        sco.factrotoday & sco.WarrantyROs
        backfill FROM dds.factro
    */
    TRY  
      INSERT INTO WarrantyROS 
      SELECT m.*
      FROM (
        SELECT f.eeUserName, a.ro, b.thedate AS CloseDate, a.customername AS Customer, 
          CASE 
            WHEN c.bnphon  = '0' THEN 'No Phone'
            WHEN c.bnphon  = '' THEN 'No Phone'
            WHEN c.bnphon IS NULL THEN 'No Phone'
            ELSE left(c.bnphon, 12)
          END AS HomePhone, 
          CASE 
            WHEN c.bnbphn  = '0' THEN 'No Phone'
            WHEN c.bnbphn  = '' THEN 'No Phone'
            WHEN c.bnbphn IS NULL THEN 'No Phone'
            ELSE left(c.bnbphn, 12)
          END  AS WorkPhone, 
          CASE 
            WHEN c.bncphon  = '0' THEN 'No Phone'
            WHEN c.bncphon  = '' THEN 'No Phone'
            WHEN c.bncphon IS NULL THEN 'No Phone'
            ELSE left(c.bncphon, 12)
          END  AS CellPhone,          
          a.vin, trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle,
          false as FollowUpComplete
        FROM dds.factro a
        INNER JOIN -- cashiered ros only
          dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate BETWEEN curdate() - 7 AND curdate()    
        LEFT JOIN dds.stgArkonaBOPNAME c ON a.storecode = c.bnco#
          AND a.customerkey = c.bnkey    
        LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
        INNER JOIN ServiceWriters f ON a.storecode = f.storecode 
          AND a.writerid = f.writernumber
          AND f.storecode = 'ry1' -- ry2 mod
          AND EXISTS (
            SELECT 1 
            FROM dds.factroline
            WHERE ro = a.ro
              AND paytype = 'w')
          AND a.void = false) m
      LEFT JOIN warrantyROS n ON m.ro = n.ro
        AND m.eeusername = n.eeusername    
      WHERE n.eeusername IS NULL;    
             
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Warranty Calls' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData      
      SELECT a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
        a.writerNumber, c.datekey, c.thedate, c.dayOfWeek, c.dayName,
        c.dayOfMonth, c.sundayToSaturdayWeek, 'Open Warranty Calls', 3 AS seq, coalesce(b.howmany, 0) AS today,
        coalesce(b.howmany, 0) AS thisWeek, trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS todayDisplay, 
        trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS thisWeekDisplay
      FROM servicewriters a  
      LEFT JOIN (
        SELECT eeusername, COUNT(*) AS howmany
        FROM warrantyROs 
        WHERE followUpComplete = false
        GROUP BY eeusername) b ON a.eeusername = b.eeusername
      LEFT JOIN dds.day c ON 1 = 1
        AND c.thedate = curdate()  
      WHERE a.active = true
        AND a.storecode = 'ry1'; -- ry2 mod
    CATCH ALL
      RAISE swMetricDataToday(108, __errtext);  
    END TRY;       
    -- Avg Hours/RO ---------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosClosedWithFlagHours & ServiceWriterMetricData.flagHours to be complete
      different than cashiering AND insp requested, those weekly numbers are based ON discreet
      values that get updated at the END, a simple concantenation of the discreet values
      this IS a ratio
    */
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Avg Hours/RO';
      INSERT INTO ServiceWriterMetricData
      SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Avg Hours/RO', 4 AS seq, 
        round(a.today/b.today, 1) AS today,
        round(a.thisweek/b.thisweek, 1) AS thisweek,  
        left(CAST(cast(round(a.today/b.today, 1) as sql_double) AS sql_char), 12)  AS todaydisplay,
        left(cast(CAST(round(a.thisweek/b.thisweek, 1) AS sql_double) AS sql_char), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
        AND a.thedate = b.thedate
      WHERE a.metric = 'flagHours'
        AND b.metric = 'rosClosedWithFlagHours';
    CATCH ALL
      RAISE swMetricDataToday(109, __errtext);      
    END TRY;     
    -- LaborSales -----------------------------------------------------------------
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Labor Sales' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData        
      SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
        round(cast(abs(sum(a.laborsales)) AS sql_double), 0) AS today,
        0, 
        trim(cast(coalesce(abs(round(CAST(sum(a.laborsales) AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
        CAST(NULL AS sql_char)
      FROM factROToday a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate = curdate()
      INNER JOIN ServiceWriters d ON a.storecode = d.storecode
        AND a.writerid = d.writernumber    
      WHERE a.void = false  
      GROUP BY d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek;
    CATCH ALL
      RAISE swMetricDataToday(110, __errtext);      
    END TRY;     
    -- rosOpened ------------------------------------------------------------------  
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'ROs Opened' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Opened', 6 AS seq, COUNT(*) AS today,
        0 AS thisWeek, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
      FROM factROToday a
      INNER JOIN dds.day b ON a.opendatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate = curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false
      GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek;
    CATCH ALL
      RAISE swMetricDataToday(111, __errtext);      
    END TRY;         
    -- Inspections Requested ------------------------------------------------------
    /*
      requires ServiceWriterMetricData.ROs Closed & 
      ServiceWriterMetricData.rosClosedWithInspectionLine to be complete
      generate weekly totals before generating display metric (cashiering)
    */
    TRY 
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'rosClosedWithInspectionLine') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'rosClosedWithInspectionLine'; 
      
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Inspections Requested';
      INSERT INTO ServiceWriterMetricData
      SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Inspections Requested', 7 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosClosedWithInspectionLine'
        AND b.metric = 'ROs Closed';
    CATCH ALL
      RAISE swMetricDataToday(112, __errtext);      
    END TRY;         

    -- UPDATE cumulative metrics only with weekly totals
    TRY 
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE EXISTS (
          SELECT 1
          FROM ServiceWriterMetrics
          WHERE metric = a.metric
            AND cumulative = true)) x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN (
        SELECT metric
        FROM servicewritermetrics
        WHERE cumulative = true)  ; 
    CATCH ALL
      RAISE swMetricDataToday(113, __errtext);      
    END TRY;
    
  COMMIT WORK;
  CATCH ALL
    ROLLBACK WORK;
    RAISE;
  END TRY;  
CATCH ALL
  RAISE;
END TRY;    


END;

alter PROCEDURE swMetricDataYesterday
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE swMetricDataYesterday();
4/22
  added flushing AND refilling TABLE AgedROs
5/4
  return a 0 instead of NULL for OPEN warranty calls  
6/8
  modified for ry2  
    include opcode 24z for ry2 inspections
    exclude cashiering, warranty calls for ry2
    ADD HondaNissanCalls for ry2   
*/
TRY 
  BEGIN TRANSACTION;
  TRY
    -- AgedROs table ----------------------------------------------------------
    TRY
      DELETE FROM AgedROs;
      INSERT INTO AgedROs
      SELECT f.eeUsername, a.ro, b.thedate, a.customername, 
        CASE d.ptrsts
          WHEN '1' THEN 'Open'
          WHEN '2' THEN 'In-Process'
          WHEN '3' THEN 'Appr-PD'
          WHEN '4' THEN 'Cashier'
          WHEN '5' THEN 'Cashier/D'
          WHEN '6' THEN 'Pre-Inv'
          WHEN '7' THEN 'Odom Rq'
          WHEN '9' THEN 'Parts-A'
          WHEN 'L' THEN 'G/L Error'
          WHEN 'P' THEN 'PI Rq'
          ELSE 'WTF'
        END AS status, 
        a.vin, 
        trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle
      FROM dds.factro a
      INNER JOIN dds.day b ON a.opendatekey = b.datekey
      INNER JOIN dds.day c ON a.finalclosedatekey = c.datekey
        AND c.datetype <> 'date'
      INNER JOIN dds.stgArkonaPDPPHDR d ON a.ro = d.ptdoc# -- ? eliminate those NOT IN pdpphdr
      LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
      INNER JOIN ServiceWriters f ON a.storecode = f.storecode
        AND a.writerid = f.writernumber
        AND void = false
        AND curdate() - b.thedate > 7;    
    CATCH ALL
      RAISE swMetricDataYesterday(99,__errtext);
    END TRY;
    -- ROs Closed ------------------------------------------------------------- 
    TRY  
      DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() -1 AND metric = 'ROs Closed';
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Closed', 0, COUNT(*) AS today,
        0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey -- cashiered ros only
        AND b.datetype = 'date'
        AND b.thedate = curdate() -1
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber  
      WHERE a.void = false     
      GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek;      
      -- weekly -------------------
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'ROs Closed') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'ROs Closed';   
    CATCH ALL
      RAISE swMetricDataYesterday(100, __errtext);
    END TRY;
    -- rosClosedWithInspectionLine ------------------------------------------------    
    TRY     
      DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() -1 AND metric = 'rosClosedWithInspectionLine';  
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithInspectionLine', 0, COUNT(*) AS today,
        0, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate = curdate() - 1
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false 
        AND EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
          AND opcode in ('23I', '24Z'))  
      GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek;
    CATCH ALL
      RAISE swMetricDataYesterday(101, __errtext);  
    END TRY;  
    -- rosCashiered ---------------------------------------------------------------
    TRY
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashiered' AND thedate = curdate() - 1;
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'rosCashiered', 0 AS seq,
        COUNT(*) AS today,
        0 AS thisweek,
        TRIM(CAST(COUNT(*) AS sql_char)),
        CAST(NULL AS sql_char)
      FROM dds.factro a
      INNER JOIN 
        dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate = curdate() - 1
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber
        AND c.storecode = 'ry1' -- ry2 mod
      WHERE EXISTS (
          SELECT 1
          FROM dds.stgArkonaGLPTRNS
          WHERE gtdoc# = a.ro)
        AND NOT EXISTS (
          SELECT 1 
          FROM dds.factroline
          WHERE ro = a.ro
            AND paytype IN ('w','i'))
        AND NOT EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
            AND servicetype <> 'mr')      
        AND a.void = false 
      GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek; 
    CATCH ALL
      RAISE swMetricDataYesterday(102, __errtext);  
    END TRY;  
    -- rosCashieredByWriter -------------------------------------------------------
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashieredByWriter' AND thedate = curdate() - 1;
      INSERT INTO ServiceWriterMetricData        
      SELECT a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
        a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
        a.dayOfMonth, a.sundayToSaturdayWeek, 'rosCashieredByWriter', 0,
        SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS today,
        0,
        TRIM(CAST(SUM(CASE WHEN b.storecode IS NOT NULL THEN 1 ELSE 0 END) AS sql_char)),
        CAST(NULL AS sql_char)
      FROM (  
        SELECT b.datekey, b.thedate, b.dayOfWeek, b.dayName,
          b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
          g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
          g.writerNumber, f.gquser
        FROM dds.factro a
        INNER JOIN 
          dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate = curdate() - 1
        INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# 
        INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
        INNER JOIN ServiceWriters g ON a.storecode = g.storecode
          AND a.writerid = g.writernumber
          AND g.storecode = 'ry1' -- ry2 mod
        WHERE NOT EXISTS (
            SELECT 1 
            FROM dds.factroline
            WHERE ro = a.ro
              AND paytype IN ('w','i'))
          AND NOT EXISTS (
            SELECT 1
            FROM dds.factroline
            WHERE ro = a.ro
              AND servicetype <> 'mr')      
          AND a.void = false 
        GROUP BY b.datekey, b.thedate, b.dayOfWeek, b.dayName,
          b.dayOfMonth, b.sundayToSaturdayWeek, a.ro, a.writerid, 
          g.eeUserName, g.storecode, g.fullname, g.lastname, g.firstname,
          g.writerNumber, f.gquser) a
      LEFT JOIN ServiceWriters b ON a.gquser = b.dtUserName   
      GROUP BY a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
        a.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
        a.dayOfMonth, a.sundayToSaturdayWeek; 
    CATCH ALL
      RAISE swMetricDataYesterday(103, __errtext);  
    END TRY;  
    -- rosClosedWithFlagHours -----------------------------------------------------     
    TRY  
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosClosedWithFlagHours' AND thedate = curdate()-1;
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'rosClosedWithFlagHours', 0 AS seq, 
        howmany AS today, 0 AS thisweek, 
        trim(cast(howmany AS sql_char)), CAST(NULL AS sql_char) 
      FROM (
        SELECT b.thedate, a.writerid, COUNT(*) AS howmany
        FROM dds.factro a
        INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate = curdate() -1
        INNER JOIN ServiceWriters c ON a.storecode = c.storecode
          AND a.writerid = c.writernumber  
        WHERE EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
          AND lineflaghours > 0)   
        GROUP BY b.thedate, a.writerid) a 
      LEFT JOIN dds.day b ON a.thedate = b.thedate   
      LEFT JOIN servicewriters c ON a.writerid = c.writernumber;
      
      -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'rosClosedWithFlagHours') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;       
    CATCH ALL
      RAISE swMetricDataYesterday(104, __errtext);  
    END TRY; 
    -- flaghours ------------------------------------------------------------------  
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'flagHours' AND thedate = curdate()-1;
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'flagHours', 0 AS seq, 
        flaghours AS today, 0 AS thisweek, 
        trim(cast(flaghours AS sql_char)), CAST(NULL AS sql_char) 
      FROM (
        SELECT b.writerid, b.thedate, SUM(a.lineflaghours) AS flaghours
        FROM dds.factroline a
        INNER JOIN (
          SELECT *
          FROM dds.factro a
          INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate = curdate() -1
          INNER JOIN servicewriters c ON a.storecode = c.storecode
            AND a.writerid = c.writernumber
          WHERE a.void = false) b ON a.ro = b.ro 
        GROUP BY b.writerid, b.thedate) a
      LEFT JOIN dds.day b ON a.thedate = b.thedate   
      LEFT JOIN servicewriters c ON a.writerid = c.writernumber;
      -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'flagHours') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric; 
    CATCH ALL
      RAISE swMetricDataYesterday(105, __errtext);  
    END TRY; 
    -- Cashiering -----------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosCashieredByWriter & 
      ServiceWriterMetricData.rosCashiered to be complete
      generate weekly totals before generating display metric (cashiering)
    */
    TRY     
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric in ('rosCashiered','rosCashieredByWriter')) x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN ('rosCashiered','rosCashieredByWriter') ; 
        
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Cashiering';
      INSERT INTO ServiceWriterMetricData
      SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Cashiering', 1 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosCashieredByWriter'
        AND b.metric = 'rosCashiered';
    CATCH ALL
      RAISE swMetricDataYesterday(106, __errtext);  
    END TRY;    
    -- Open Aged ROs --------------------------------------------------------------------
    /*
    UPDATE throughout the day IN terms of removing ros that get closed
    DELETE FROM agedros daily based ON an ro closing
    overnight a full scrap
    AgedROs TABLE IS necessary for the nightly/today to run
    
    no need to UPDATE with newly aged ros thru out the day
    
    need to UPDATE the weekly numbers here
    ALL i need are this week, which will be the same AS the daily total each day
    */
    TRY    
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Aged ROs' AND thedate = curdate() -1 ;
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
        a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Aged ROs', 2 AS seq, b.howmany AS today,
      --  0 AS thisWeek, trim(CAST(b.howmany AS sql_char)), CAST(NULL AS sql_char)
        howmany thisWeek, trim(CAST(howmany AS sql_char)) AS todayDisplay, 
        trim(CAST(howmany AS sql_char)) AS thisWeekDisplay
      FROM dds.day a, ( 
        SELECT eeusername, COUNT(*) AS howmany
        FROM agedros  
        GROUP BY eeusername) b, 
        servicewriters c
      WHERE a.thedate = curdate() - 1  
        AND b.eeusername = c.eeusername;
    CATCH ALL
      RAISE swMetricDataYesterday(107, __errtext);  
    END TRY;  
    -- Open Warranty Calls --------------------------------------------------------
    /*
      the challenges here, like AgedROs, the data depends ON dds.factro, 
        sco.factrotoday & sco.WarrantyROs
        backfill FROM dds.factro
    */
    TRY
      INSERT INTO WarrantyROS 
      SELECT m.*
      FROM (
        SELECT f.eeUserName, a.ro, b.thedate AS CloseDate, a.customername AS Customer, 
          CASE 
            WHEN c.bnphon  = '0' THEN 'No Phone'
            WHEN c.bnphon  = '' THEN 'No Phone'
            WHEN c.bnphon IS NULL THEN 'No Phone'
            ELSE left(c.bnphon, 12)
          END AS HomePhone, 
          CASE 
            WHEN c.bnbphn  = '0' THEN 'No Phone'
            WHEN c.bnbphn  = '' THEN 'No Phone'
            WHEN c.bnbphn IS NULL THEN 'No Phone'
            ELSE left(c.bnbphn, 12)
          END  AS WorkPhone, 
          CASE 
            WHEN c.bncphon  = '0' THEN 'No Phone'
            WHEN c.bncphon  = '' THEN 'No Phone'
            WHEN c.bncphon IS NULL THEN 'No Phone'
            ELSE left(c.bncphon, 12)
          END  AS CellPhone,          
          a.vin, trim(TRIM(e.immake) + ' ' + TRIM(e.immodl)) AS vehicle,
          false as FollowUpComplete
        FROM dds.factro a
        INNER JOIN -- cashiered ros only
          dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate BETWEEN curdate() - 7 AND curdate()    
        LEFT JOIN dds.stgArkonaBOPNAME c ON a.storecode = c.bnco#
          AND a.customerkey = c.bnkey    
        LEFT JOIN dds.stgArkonaINPMAST e ON a.vin = e.imvin
        INNER JOIN ServiceWriters f ON a.storecode = f.storecode 
          AND a.writerid = f.writernumber
          AND f.storecode = 'ry1' -- ry2 mod
          AND EXISTS (
            SELECT 1 
            FROM dds.factroline
            WHERE ro = a.ro
              AND paytype = 'w')
          AND a.void = false) m
      LEFT JOIN warrantyROS n ON m.ro = n.ro
        AND m.eeusername = n.eeusername    
      WHERE n.eeusername IS NULL;  
          
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Warranty Calls' AND thedate = curdate() -1 ;
      INSERT INTO ServiceWriterMetricData      
      SELECT a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
        a.writerNumber, c.datekey, c.thedate, c.dayOfWeek, c.dayName,
        c.dayOfMonth, c.sundayToSaturdayWeek, 'Open Warranty Calls', 3 AS seq, coalesce(b.howmany, 0) AS today,
        coalesce(b.howmany, 0) AS thisWeek, trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS todayDisplay, 
        trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS thisWeekDisplay
      FROM servicewriters a  
      LEFT JOIN (
        SELECT eeusername, COUNT(*) AS howmany
        FROM warrantyROs 
        WHERE followUpComplete = false
        GROUP BY eeusername) b ON a.eeusername = b.eeusername
      LEFT JOIN dds.day c ON 1 = 1
        AND c.thedate = curdate() - 1  
      WHERE a.active = true
      AND a.storecode = 'ry1'; -- ry2 mod  
    CATCH ALL
      RAISE swMetricDataYesterday(108, __errtext);  
    END TRY;   
    -- Avg Hours/RO ---------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosClosedWithFlagHours & ServiceWriterMetricData.flagHours to be complete
      different than cashiering AND insp requested, those weekly numbers are based ON discreet
      values that get updated at the END, a simple concantenation of the discreet values
      this IS a ratio
    */
    TRY    
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Avg Hours/RO';
      INSERT INTO ServiceWriterMetricData
      SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Avg Hours/RO', 4 AS seq, 
        round(a.today/b.today, 1) AS today,
        round(a.thisweek/b.thisweek, 1) AS thisweek,  
        left(CAST(cast(round(a.today/b.today, 1) as sql_double) AS sql_char), 12)  AS todaydisplay,
        left(cast(CAST(round(a.thisweek/b.thisweek, 1) AS sql_double) AS sql_char), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
        AND a.thedate = b.thedate
      WHERE a.metric = 'flagHours'
        AND b.metric = 'rosClosedWithFlagHours';
    CATCH ALL
      RAISE swMetricDataYesterday(109, __errtext);      
    END TRY; 
    -- LaborSales -----------------------------------------------------------------     
    TRY       
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Labor Sales' AND thedate = curdate()-1;
      INSERT INTO ServiceWriterMetricData        
      SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
        round(cast(abs(sum(a.laborsales)) AS sql_double), 0) AS today,
        0, 
        trim(cast(coalesce(abs(round(CAST(sum(a.laborsales) AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
        CAST(NULL AS sql_char)
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate = curdate() - 1
      INNER JOIN ServiceWriters d ON a.storecode = d.storecode
        AND a.writerid = d.writernumber    
      WHERE a.void = false  
      GROUP BY d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek;
    CATCH ALL
      RAISE swMetricDataYesterday(110, __errtext);      
    END TRY;   
    -- rosOpened ------------------------------------------------------------------ 
    TRY      
      DELETE FROM ServiceWriterMetricData WHERE metric = 'ROs Opened' AND thedate = curdate() -1 ;
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Opened', 6 AS seq, COUNT(*) AS today,
        0 AS thisWeek, trim(CAST(COUNT(*) AS sql_char)), CAST(NULL AS sql_char)
      FROM dds.factro a
      INNER JOIN dds.day b ON a.opendatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate = curdate() -1
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false
      GROUP BY c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek;
    CATCH ALL
      RAISE swMetricDataYesterday(111, __errtext);      
    END TRY;     
    -- Inspections Requested ------------------------------------------------------
    /*
      requires ServiceWriterMetricData.ROs Closed & 
      ServiceWriterMetricData.rosClosedWithInspectionLine to be complete
      generate weekly totals before generating display metric (cashiering)
    */
    TRY    
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'rosClosedWithInspectionLine') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'rosClosedWithInspectionLine'; 
      
      
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Inspections Requested';
      INSERT INTO ServiceWriterMetricData
      SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Inspections Requested', 7 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosClosedWithInspectionLine'
        AND b.metric = 'ROs Closed';
    CATCH ALL
      RAISE swMetricDataYesterday(112, __errtext);      
    END TRY;         
    TRY
    -- UPDATE cumulative metrics only with weekly totals
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE EXISTS (
          SELECT 1
          FROM ServiceWriterMetrics
          WHERE metric = a.metric
            AND cumulative = true)) x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN (
        SELECT metric
        FROM servicewritermetrics
        WHERE cumulative = true); 
    CATCH ALL
      RAISE swMetricDataYesterday(113, __errtext);      
    END TRY;                         
  COMMIT WORK;
  CATCH ALL
    ROLLBACK WORK;
    RAISE;
  END TRY;  
CATCH ALL
  RAISE;
END TRY;    

END;
