INSERT INTO writerlandingpage
SELECT a.sundaytosaturdayweek, 0, fullname, a.writerid, 'RO''s Opened',
  sun, mon, tue, wed, thu, fri, sat, total, eeusername, 6 
FROM ( -- sunday to saturday
  SELECT sundaytosaturdayweek, writerid, fullname, eeusername,
    MAX(CASE WHEN dayname = 'sunday' THEN opened END) AS sun,
    MAX(CASE WHEN dayname = 'monday' THEN opened END) AS mon,
    MAX(CASE WHEN dayname = 'tuesday' THEN opened END) AS tue,
    MAX(CASE WHEN dayname = 'wednesday' THEN opened END) AS wed,
    MAX(CASE WHEN dayname = 'thursday' THEN opened END) AS thu,
    MAX(CASE WHEN dayname = 'friday' THEN opened END) AS fri,
    MAX(CASE WHEN dayname = 'saturday' THEN opened END) AS sat
  FROM (  
    SELECT sundaytosaturdayweek, dayname, fullname, eeusername, a.writerid,  
      trim(cast(coalesce(a.opened, 0) AS sql_char)) AS opened
    FROM (
      SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
      FROM dds.day a
      WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) z  
    LEFT JOIN ( -- openedros
      SELECT b.thedate, a.writerid, COUNT(*) AS opened
      FROM dds.factro a
      INNER JOIN dds.day b ON a.opendatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN ServiceWriters c ON a.storecode = c.storecode
        AND a.writerid = c.writernumber 
      WHERE a.void = false
      GROUP BY b.thedate, a.writerid) a ON a.thedate = z.thedate 
    LEFT JOIN servicewriters c ON a.writerid = c.writernumber) x 
  GROUP BY sundaytosaturdayweek, fullname, eeusername, writerid) a
LEFT JOIN (  
  SELECT sundaytosaturdayweek, a.writerid,  
    trim(cast(SUM(opened) AS sql_char)) AS total
  FROM (
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) z  
  LEFT JOIN ( -- openedros
    SELECT b.thedate, a.writerid, COUNT(*) AS opened
    FROM dds.factro a
    INNER JOIN dds.day b ON a.opendatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate BETWEEN '03/03/2013' AND curdate()
    INNER JOIN ServiceWriters c ON a.storecode = c.storecode
      AND a.writerid = c.writernumber 
    WHERE a.void = false
    GROUP BY b.thedate, a.writerid) a ON a.thedate = z.thedate 
  GROUP BY sundaytosaturdayweek, a.writerid) b ON a.sundaytosaturdayweek = b.sundaytosaturdayweek
  AND a.writerid = b.writerid
  
  
  
  
  