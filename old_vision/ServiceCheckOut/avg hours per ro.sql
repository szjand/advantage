/*

SELECT b.thedate AS finalclosedate, a.writerid, a.ro, a.laborsales, 
  SUM(c.lineflaghours) AS flaghours
FROM dds.factro a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN '03/03/2013' AND curdate()
INNER JOIN dds.factroline c ON a.ro = c.ro
  AND c.lineflaghours > 0 -- only ros with flag hours 
INNER JOIN ServiceWriters d ON a.storecode = d.storecode
  AND a.writerid = d.writernumber    
WHERE a.void = false   
GROUP BY b.thedate, a.writerid, a.ro, a.laborsales


--want a row per writer per day
SELECT finalclosedate, writerid, COUNT(*) AS howmany, SUM(flaghours) AS flaghours
FROM (
SELECT b.thedate AS finalclosedate, a.writerid, a.ro, a.laborsales, 
  SUM(c.lineflaghours) AS flaghours
FROM dds.factro a
INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN '03/03/2013' AND curdate()
INNER JOIN dds.factroline c ON a.ro = c.ro
  AND c.lineflaghours > 0 -- only ros with flag hours 
INNER JOIN ServiceWriters d ON a.storecode = d.storecode
  AND a.writerid = d.writernumber    
WHERE a.void = false   
GROUP BY b.thedate, a.writerid, a.ro, a.laborsales) x
GROUP by finalclosedate, writerid


-- END result IS one row per writer/sundaytosaturdayweek
SELECT sundaytosaturdayweek, writerid,
  MAX(CASE WHEN dayname = 'sunday' THEN howmany END) AS sunCount,
  MAX(CASE WHEN dayname = 'sunday' THEN flaghours END) AS sunFH,
  MAX(CASE WHEN dayname = 'Monday' THEN howmany END) AS monCount,
  MAX(CASE WHEN dayname = 'Monday' THEN flaghours END) AS monFH,
  MAX(CASE WHEN dayname = 'Tuesday' THEN howmany END) AS tueCount,
  MAX(CASE WHEN dayname = 'Tuesday' THEN flaghours END) AS tueFH,
  MAX(CASE WHEN dayname = 'Wednesday' THEN howmany END) AS wedCount,
  MAX(CASE WHEN dayname = 'Wednesday' THEN flaghours END) AS wedFH,
  MAX(CASE WHEN dayname = 'Thursday' THEN howmany END) AS thuCount,
  MAX(CASE WHEN dayname = 'Thursday' THEN flaghours END) AS thuFH,
  MAX(CASE WHEN dayname = 'Friday' THEN howmany END) AS friCount,
  MAX(CASE WHEN dayname = 'Friday' THEN flaghours END) AS friH,
  MAX(CASE WHEN dayname = 'Saturday' THEN howmany END) AS satCount,
  MAX(CASE WHEN dayname = 'Saturday' THEN flaghours END) AS satFH            
--SELECT * 
FROM (  
  SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
  FROM dds.day a
  WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
LEFT JOIN (
  SELECT finalclosedate, writerid, COUNT(*) AS howmany, SUM(flaghours) AS flaghours
  FROM (
  SELECT b.thedate AS finalclosedate, a.writerid, a.ro, a.laborsales, 
    SUM(c.lineflaghours) AS flaghours
  FROM dds.factro a
  INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
      AND b.datetype = 'date'
      AND b.thedate BETWEEN '03/03/2013' AND curdate()
  INNER JOIN dds.factroline c ON a.ro = c.ro
    AND c.lineflaghours > 0 -- only ros with flag hours 
  INNER JOIN ServiceWriters d ON a.storecode = d.storecode
    AND a.writerid = d.writernumber    
  WHERE a.void = false   
  GROUP BY b.thedate, a.writerid, a.ro, a.laborsales) x
  GROUP by finalclosedate, writerid) b ON a.thedate = b.finalclosedate
WHERE writerid IS NOT NULL   
GROUP BY sundaytosaturdayweek, writerid

-- this IS it
SELECT sundaytosaturdayweek, writerid,
  CASE sunCount
    WHEN 0 THEN 0
    ELSE round(sunFH/sunCount, 1)
  END AS sunday,
  CASE suncount+moncount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh)/(suncount+moncount), 1)
  END AS monday,
  CASE suncount+moncount+tuecount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh)/(suncount+moncount+tuecount), 1)
  END AS tuesday,  
  CASE suncount+moncount+tuecount+wedcount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh+wedfh)/(suncount+moncount+tuecount+wedcount), 1)
  END AS wednesday, 
  CASE suncount+moncount+tuecount+wedcount+thucount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh+wedfh+thufh)/(suncount+moncount+tuecount+wedcount+thucount), 1)
  END AS thursday, 
  CASE suncount+moncount+tuecount+wedcount+thucount+fricount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh+wedfh+thufh+frifh)/(suncount+moncount+tuecount+wedcount+thucount+fricount), 1)
  END AS friday, 
  CASE suncount+moncount+tuecount+wedcount+thucount+fricount+satcount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh+wedfh+thufh+frifh+satfh)/(suncount+moncount+tuecount+wedcount+thucount+fricount+satcount), 1)
  END AS saturday        
FROM(
  SELECT sundaytosaturdayweek, writerid,
    coalesce(MAX(CASE WHEN dayname = 'sunday' THEN howmany END), 0) AS sunCount,
    coalesce(MAX(CASE WHEN dayname = 'sunday' THEN flaghours END), 0) AS sunFH,
    coalesce(MAX(CASE WHEN dayname = 'Monday' THEN howmany END), 0) AS monCount,
    coalesce(MAX(CASE WHEN dayname = 'Monday' THEN flaghours END), 0) AS monFH,
    coalesce(MAX(CASE WHEN dayname = 'Tuesday' THEN howmany END), 0) AS tueCount,
    coalesce(MAX(CASE WHEN dayname = 'Tuesday' THEN flaghours END), 0) AS tueFH,
    coalesce(MAX(CASE WHEN dayname = 'Wednesday' THEN howmany END), 0) AS wedCount,
    coalesce(MAX(CASE WHEN dayname = 'Wednesday' THEN flaghours END), 0) AS wedFH,
    coalesce(MAX(CASE WHEN dayname = 'Thursday' THEN howmany END), 0) AS thuCount,
    coalesce(MAX(CASE WHEN dayname = 'Thursday' THEN flaghours END), 0) AS thuFH,
    coalesce(MAX(CASE WHEN dayname = 'Friday' THEN howmany END), 0) AS friCount,
    coalesce(MAX(CASE WHEN dayname = 'Friday' THEN flaghours END), 0) AS friFH,
    coalesce(MAX(CASE WHEN dayname = 'Saturday' THEN howmany END), 0) AS satCount,
    coalesce(MAX(CASE WHEN dayname = 'Saturday' THEN flaghours END), 0) AS satFH            
  --SELECT * 
  FROM (  
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (
    SELECT finalclosedate, writerid, COUNT(*) AS howmany, SUM(flaghours) AS flaghours
    FROM (
    SELECT b.thedate AS finalclosedate, a.writerid, a.ro, a.laborsales, 
      SUM(c.lineflaghours) AS flaghours
    FROM dds.factro a
    INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate BETWEEN '03/03/2013' AND curdate()
    INNER JOIN dds.factroline c ON a.ro = c.ro
      AND c.lineflaghours > 0 -- only ros with flag hours 
    INNER JOIN ServiceWriters d ON a.storecode = d.storecode
      AND a.writerid = d.writernumber    
    WHERE a.void = false   
    GROUP BY b.thedate, a.writerid, a.ro, a.laborsales) x
    GROUP by finalclosedate, writerid) b ON a.thedate = b.finalclosedate
  WHERE writerid IS NOT NULL   
  GROUP BY sundaytosaturdayweek, writerid) r

  
INSERT INTO writerlandingpage
SELECT sundaytosaturdayweek, 0, fullname, writerid, 'Avg Hours/RO',
  CASE sunCount
    WHEN 0 THEN 0
    ELSE round(sunFH/sunCount, 1)
  END AS sunday,
  CASE suncount+moncount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh)/(suncount+moncount), 1)
  END AS monday,
  CASE suncount+moncount+tuecount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh)/(suncount+moncount+tuecount), 1)
  END AS tuesday,  
  CASE suncount+moncount+tuecount+wedcount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh+wedfh)/(suncount+moncount+tuecount+wedcount), 1)
  END AS wednesday, 
  CASE suncount+moncount+tuecount+wedcount+thucount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh+wedfh+thufh)/(suncount+moncount+tuecount+wedcount+thucount), 1)
  END AS thursday, 
  CASE suncount+moncount+tuecount+wedcount+thucount+fricount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh+wedfh+thufh+frifh)/(suncount+moncount+tuecount+wedcount+thucount+fricount), 1)
  END AS friday, 
  CASE suncount+moncount+tuecount+wedcount+thucount+fricount+satcount
    WHEN 0 THEN 0
    ELSE round((sunfh+monfh+tuefh+wedfh+thufh+frifh+satfh)/(suncount+moncount+tuecount+wedcount+thucount+fricount+satcount), 1)
  END AS saturday,
  CAST(NULL AS sql_double), eeusername, 4       
FROM(
  SELECT sundaytosaturdayweek, writerid, fullname, eeusername, 
    coalesce(MAX(CASE WHEN dayname = 'sunday' THEN howmany END), 0) AS sunCount,
    coalesce(MAX(CASE WHEN dayname = 'sunday' THEN flaghours END), 0) AS sunFH,
    coalesce(MAX(CASE WHEN dayname = 'Monday' THEN howmany END), 0) AS monCount,
    coalesce(MAX(CASE WHEN dayname = 'Monday' THEN flaghours END), 0) AS monFH,
    coalesce(MAX(CASE WHEN dayname = 'Tuesday' THEN howmany END), 0) AS tueCount,
    coalesce(MAX(CASE WHEN dayname = 'Tuesday' THEN flaghours END), 0) AS tueFH,
    coalesce(MAX(CASE WHEN dayname = 'Wednesday' THEN howmany END), 0) AS wedCount,
    coalesce(MAX(CASE WHEN dayname = 'Wednesday' THEN flaghours END), 0) AS wedFH,
    coalesce(MAX(CASE WHEN dayname = 'Thursday' THEN howmany END), 0) AS thuCount,
    coalesce(MAX(CASE WHEN dayname = 'Thursday' THEN flaghours END), 0) AS thuFH,
    coalesce(MAX(CASE WHEN dayname = 'Friday' THEN howmany END), 0) AS friCount,
    coalesce(MAX(CASE WHEN dayname = 'Friday' THEN flaghours END), 0) AS friFH,
    coalesce(MAX(CASE WHEN dayname = 'Saturday' THEN howmany END), 0) AS satCount,
    coalesce(MAX(CASE WHEN dayname = 'Saturday' THEN flaghours END), 0) AS satFH            
  --SELECT * 
  FROM (  
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (
    SELECT finalclosedate, writerid, fullname, eeusername, 
      COUNT(*) AS howmany, SUM(flaghours) AS flaghours
    FROM (
      SELECT b.thedate AS finalclosedate, a.writerid, a.ro, a.laborsales, d.fullname, d.eeusername,
        SUM(c.lineflaghours) AS flaghours
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN dds.factroline c ON a.ro = c.ro
        AND c.lineflaghours > 0 -- only ros with flag hours 
      INNER JOIN ServiceWriters d ON a.storecode = d.storecode
        AND a.writerid = d.writernumber    
      WHERE a.void = false   
      GROUP BY b.thedate, a.writerid, a.ro, a.laborsales, d.fullname, d.eeusername) x
    GROUP by finalclosedate, writerid, fullname, eeusername) b ON a.thedate = b.finalclosedate
  WHERE writerid IS NOT NULL   
  GROUP BY sundaytosaturdayweek, writerid, fullname, eeusername) r
*/  
  
/*  
4/2 change outputs to strings instead of numbers
    AND the landing page IS DAILY SNAPSHOTS, NOT running total
    the checkout page IS the total week to date
    
*/  

-- this looks ok for the landing page
INSERT INTO writerlandingpage  
SELECT a.sundaytosaturdayweek, 0, fullname, a.writerid, 'Avg Hours/RO',
  trim(cast(a.sunday AS sql_char)),trim(cast(a.monday AS sql_char)),trim(cast(a.tuesday AS sql_char)),
  trim(cast(a.wednesday AS sql_char)),trim(cast(a.thursday AS sql_char)),trim(cast(a.friday AS sql_char)), 
  trim(cast(a.saturday AS sql_char)),trim(cast(b.total AS sql_char)),
  eeusername, 4  
--SELECT a.*, b.total 
FROM(
  SELECT sundaytosaturdayweek, writerid, fullname, eeusername, 
    max(CASE WHEN dayname = 'Sunday' THEN AvgPerRO END) AS Sunday,
    max(CASE WHEN dayname = 'Monday' THEN AvgPerRO END) AS Monday,
    max(CASE WHEN dayname = 'Tuesday' THEN AvgPerRO END) AS Tuesday,
    max(CASE WHEN dayname = 'Wednesday' THEN AvgPerRO END) AS Wednesday,
    max(CASE WHEN dayname = 'Thursday' THEN AvgPerRO END) AS Thursday,
    max(CASE WHEN dayname = 'Friday' THEN AvgPerRO END) AS Friday,
    max(CASE WHEN dayname = 'Saturday' THEN AvgPerRO END) AS Saturday
  FROM ( -- sunday - saturday
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (
    SELECT finalclosedate, writerid, fullname, eeusername,
      CASE howmany
        WHEN 0 THEN '0'
        ELSE cast(round(flaghours/howmany, 1) AS sql_char)
      END AS AvgPerRO
    FROM (
      SELECT finalclosedate, writerid, fullname, eeusername, COUNT(*) AS howmany, SUM(flaghours) AS flaghours
      FROM (
        SELECT b.thedate AS finalclosedate, a.writerid, a.ro, d.fullname, eeusername,
          SUM(c.lineflaghours) AS flaghours
        FROM dds.factro a
        INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
            AND b.datetype = 'date'
            AND b.thedate BETWEEN '03/03/2013' AND curdate()
        INNER JOIN dds.factroline c ON a.ro = c.ro
          AND c.lineflaghours > 0 -- only ros with flag hours 
        INNER JOIN ServiceWriters d ON a.storecode = d.storecode
          AND a.writerid = d.writernumber    
        WHERE a.void = false   
        GROUP BY b.thedate, a.writerid, a.ro, d.fullname, eeusername) x
      GROUP by finalclosedate, writerid,fullname, eeusername) y) b ON a.thedate = b.finalclosedate
  GROUP BY sundaytosaturdayweek, writerid, fullname, eeusername) a    
LEFT JOIN ( -- total
  SELECT sundaytosaturdayweek, writerid, round(SUM(flaghours)/SUM(howmany), 1) AS total
  FROM (
    SELECT a.thedate, a.dayname, a.sundaytosaturdayweek
    FROM dds.day a
    WHERE a.thedate BETWEEN '03/03/2013' AND curdate()) a 
  LEFT JOIN (  
    SELECT finalclosedate, writerid, sum(flaghours) as flaghours, COUNT(*) AS howmany
    FROM (
      SELECT b.thedate AS finalclosedate, a.writerid, a.ro,
        SUM(c.lineflaghours) AS flaghours
      FROM dds.factro a
      INNER JOIN dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN dds.factroline c ON a.ro = c.ro
        AND c.lineflaghours > 0 -- only ros with flag hours 
      INNER JOIN ServiceWriters d ON a.storecode = d.storecode
        AND a.writerid = d.writernumber    
      WHERE a.void = false   
      GROUP BY b.thedate, a.writerid, a.ro) a
    GROUP BY finalclosedate, writerid) b ON a.thedate = b.finalclosedate
  GROUP BY sundaytosaturdayweek, writerid)b ON a.sundaytosaturdayweek = b.sundaytosaturdayweek
    AND a.writerid = b.writerid;  