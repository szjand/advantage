
DROP TABLE ServiceWriterMetrics;
CREATE TABLE ServiceWriterMetrics (
  Metric cichar(40),
  Seq integer) IN database;
INSERT INTO ServiceWriterMetrics values ('Cashiering', 1);
INSERT INTO ServiceWriterMetrics values ('Open Aged ROs', 2); 
INSERT INTO ServiceWriterMetrics values ('Open Warranty Calls', 3); 
INSERT INTO ServiceWriterMetrics values ('Avg Hours/RO', 4); 
INSERT INTO ServiceWriterMetrics values ('Labor Sales', 5); 
INSERT INTO ServiceWriterMetrics values ('RO''s Opened', 6); 
INSERT INTO ServiceWriterMetrics values ('Inspections Requested', 7); 
 