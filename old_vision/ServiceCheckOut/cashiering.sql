SELECT distinct b.thedate, a.ro, a.writerid, 
  f.gquser
INTO #wtf  
FROM dds.factro a
INNER JOIN -- cashiered ros only
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN curdate() - 30 AND curdate()
LEFT JOIN dds.factroline c ON a.ro = c.ro    
LEFT JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc#  
LEFT JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
WHERE writerid IN ('714','720','705')
  AND NOT EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype IN ('w','i'))
  AND NOT EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
      AND servicetype <> 'mr')      
  AND a.void = false      
  AND f.gquser IS NOT NULL

SELECT thedate, writerid, COUNT(*),
  SUM(CASE WHEN gquser IN (SELECT dtusername FROM servicewriters) THEN 1 ELSE 0 END) AS Writer,
  round(SUM(CASE WHEN gquser IN (SELECT dtusername FROM servicewriters) THEN 1 ELSE 0 END)*100.0/COUNT(*), 0)
FROM #wtf -- no charge RO
GROUP BY thedate, writerid    

SELECT * FROM #wtf WHERE writerid = '720' AND thedate = '02/26/2013'



-- 3/31
SELECT b.thedate, a.ro, a.writerid, f.gquser
FROM dds.factro a
INNER JOIN 
  dds.day b ON a.finalclosedatekey = b.datekey
    AND b.datetype = 'date'
    AND b.thedate BETWEEN curdate() - 30 AND curdate()
INNER JOIN dds.factroline c ON a.ro = c.ro    
INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# -- cashiered ros only
INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
INNER JOIN ServiceWriters g ON a.storecode = g.storecode
  AND a.writerid = g.writernumber
  AND NOT EXISTS (
    SELECT 1 
    FROM dds.factroline
    WHERE ro = a.ro
      AND paytype IN ('w','i'))
  AND NOT EXISTS (
    SELECT 1
    FROM dds.factroline
    WHERE ro = a.ro
      AND servicetype <> 'mr')      
  AND a.void = false      
GROUP BY b.thedate, a.ro, a.writerid, f.gquser  

/*
of the ros with only Customer Pay lines opened BY THEWRITER cashiered ON THEDAY, how may were cashiered BY any writer
*/
SELECT sundaytosaturdayweek, thedate, dayname, writerid,
  round(writercashiered*100.0/Total*1.0, 0)
FROM (   
  SELECT sundaytosaturdayweek, thedate, dayname, writerid, COUNT(*) AS Total,  
    SUM(CASE WHEN y.storecode IS NULL THEN 0 ELSE 1 END) AS WriterCashiered
  FROM (
    SELECT b.sundaytosaturdayweek, b.thedate, dayname, a.ro, a.writerid, f.gquser
    FROM dds.factro a
    INNER JOIN 
      dds.day b ON a.finalclosedatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate BETWEEN '03/03/2013' AND curdate()
    INNER JOIN dds.factroline c ON a.ro = c.ro    
    INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# -- cashiered ros only
    INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
    INNER JOIN ServiceWriters g ON a.storecode = g.storecode
      AND a.writerid = g.writernumber
      AND NOT EXISTS (
        SELECT 1 
        FROM dds.factroline
        WHERE ro = a.ro
          AND paytype IN ('w','i'))
      AND NOT EXISTS (
        SELECT 1
        FROM dds.factroline
        WHERE ro = a.ro
          AND servicetype <> 'mr')      
      AND a.void = false      
    GROUP BY b.sundaytosaturdayweek, b.thedate, b.dayname, a.ro, a.writerid, f.gquser) x
  LEFT JOIN servicewriters y ON x.gquser = y.dtUserName 
  GROUP BY sundaytosaturdayweek, thedate, dayname, writerid) z 


-- feeling LIKE percentage isd to abstract
-- change it to xx of yy

SELECT sundaytosaturdayweek, dayname, writerid,
  trim(CAST(WriterCashiered AS sql_char)) + ' of ' + CAST(total AS sql_char)
FROM (
  SELECT sundaytosaturdayweek, dayname, writerid, COUNT(*) AS Total,  
    SUM(CASE WHEN y.storecode IS NULL THEN 0 ELSE 1 END) AS WriterCashiered
  FROM (
    SELECT b.sundaytosaturdayweek, b.thedate, dayname, a.ro, a.writerid, f.gquser
    FROM dds.factro a
    INNER JOIN 
      dds.day b ON a.finalclosedatekey = b.datekey
        AND b.datetype = 'date'
        AND b.thedate BETWEEN '03/03/2013' AND curdate()
    INNER JOIN dds.factroline c ON a.ro = c.ro    
    INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# -- cashiered ros only
    INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
    INNER JOIN ServiceWriters g ON a.storecode = g.storecode
      AND a.writerid = g.writernumber
      AND NOT EXISTS (
        SELECT 1 
        FROM dds.factroline
        WHERE ro = a.ro
          AND paytype IN ('w','i'))
      AND NOT EXISTS (
        SELECT 1
        FROM dds.factroline
        WHERE ro = a.ro
          AND servicetype <> 'mr')      
      AND a.void = false      
    GROUP BY b.sundaytosaturdayweek, b.thedate, b.dayname, a.ro, a.writerid, f.gquser) x
  LEFT JOIN servicewriters y ON x.gquser = y.dtUserName 
  GROUP BY sundaytosaturdayweek, dayname, writerid) z 
  

INSERT INTO writerlandingpage
SELECT m.sundaytosaturdayweek, 0, fullname, m.writerid, 'Cashiering',
  sun, mon, tue, wed, thu, fri, sat, total, eeusername, 1  
--SELECT *
FROM ( -- sunday to saturday
  SELECT sundaytosaturdayweek, z.writerid, w.fullname, w.eeusername, 
    max(CASE WHEN dayname = 'sunday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS sun,
    max(CASE WHEN dayname = 'monday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS mon,
    max(CASE WHEN dayname = 'tuesday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS tue,
    max(CASE WHEN dayname = 'wednesday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS wed,
    max(CASE WHEN dayname = 'thursday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS thu,
    max(CASE WHEN dayname = 'friday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS fri,
    max(CASE WHEN dayname = 'saturday' THEN trim(CAST(WriterCashiered AS sql_char)) + ' / ' + trim(CAST(total AS sql_char)) END) AS sat
  FROM (
    SELECT sundaytosaturdayweek, dayname, writerid, COUNT(*) AS Total,  
      SUM(CASE WHEN y.storecode IS NULL THEN 0 ELSE 1 END) AS WriterCashiered
    FROM (
      SELECT b.sundaytosaturdayweek, b.thedate, dayname, a.ro, a.writerid, f.gquser
      FROM dds.factro a
      INNER JOIN 
        dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN dds.factroline c ON a.ro = c.ro    
      INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# -- cashiered ros only
      INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
      INNER JOIN ServiceWriters g ON a.storecode = g.storecode
        AND a.writerid = g.writernumber
        AND NOT EXISTS (
          SELECT 1 
          FROM dds.factroline
          WHERE ro = a.ro
            AND paytype IN ('w','i'))
        AND NOT EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
            AND servicetype <> 'mr')      
        AND a.void = false      
      GROUP BY b.sundaytosaturdayweek, b.thedate, b.dayname, a.ro, a.writerid, f.gquser) x
    LEFT JOIN servicewriters y ON x.gquser = y.dtUserName 
    GROUP BY sundaytosaturdayweek, dayname, writerid) z 
  LEFT JOIN servicewriters w ON z.writerid = w.writernumber 
  GROUP BY sundaytosaturdayweek, z.writerid, w.fullname, w.eeusername) m
LEFT JOIN ( -- total
  SELECT sundaytosaturdayweek, writerid,
    trim(TRIM(CAST(sum(writercashiered) AS sql_char)) + ' / ' + TRIM(CAST(sum(total) AS sql_char))) AS total
  FROM (
    SELECT sundaytosaturdayweek, dayname, writerid, COUNT(*) AS Total,  
      SUM(CASE WHEN y.storecode IS NULL THEN 0 ELSE 1 END) AS WriterCashiered
    FROM (
      SELECT b.sundaytosaturdayweek, b.thedate, dayname, a.ro, a.writerid, f.gquser
      FROM dds.factro a
      INNER JOIN 
        dds.day b ON a.finalclosedatekey = b.datekey
          AND b.datetype = 'date'
          AND b.thedate BETWEEN '03/03/2013' AND curdate()
      INNER JOIN dds.factroline c ON a.ro = c.ro    
      INNER JOIN dds.stgArkonaGLPTRNS e ON a.ro = e.gtdoc# -- cashiered ros only
      INNER JOIN dds.stgArkonaGLPDTIM f ON e.gttrn# = f.gqtrn#  
      INNER JOIN ServiceWriters g ON a.storecode = g.storecode
        AND a.writerid = g.writernumber
        AND NOT EXISTS (
          SELECT 1 
          FROM dds.factroline
          WHERE ro = a.ro
            AND paytype IN ('w','i'))
        AND NOT EXISTS (
          SELECT 1
          FROM dds.factroline
          WHERE ro = a.ro
            AND servicetype <> 'mr')      
        AND a.void = false      
      GROUP BY b.sundaytosaturdayweek, b.thedate, b.dayname, a.ro, a.writerid, f.gquser) x
    LEFT JOIN servicewriters y ON x.gquser = y.dtUserName 
    GROUP BY sundaytosaturdayweek, dayname, writerid) e
  GROUP BY sundaytosaturdayweek, writerid ) n ON m.writerid = n.writerid AND m.sundaytosaturdayweek = n.sundaytosaturdayweek
  

 

