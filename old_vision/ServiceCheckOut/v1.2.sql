DROP PROCEDURE GetRO;
CREATE PROCEDURE GetWriterLaborSalesMonthToDate
   ( 
      eeUserName CICHAR ( 50 ),
      LaborSalesMonthToDate cichar(12) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetWriterLaborSalesMonthToDate('rodt@rydellchev.com');
*/ 
/*
INSERT INTO __output 
SELECT SUM(today)
FROM serviceWriterMetricData
WHERE metric = 'labor sales'
  AND month(thedate) = month(curdate())
  AND eeusername = (SELECT eeusername FROM __input)
GROUP BY eeusername; 
*/
DECLARE @string string;
DECLARE @string2 string;
@string = (
  SELECT substring(CAST(SUM(today) AS sql_char),1,position ('.' IN CAST(SUM(today) AS sql_char))-1)
  FROM serviceWriterMetricData
  WHERE metric = 'labor sales'
    AND month(thedate) = month(curdate())
    AND eeusername = (SELECT eeusername FROM __input)
  GROUP BY eeusername);
@string2 = (SELECT coalesce(@string, '0') FROM system.iota); 
INSERT INTO __output
SELECT 
  CASE
    WHEN length(@string2) < 4 THEN '$' + @string2
    WHEN length(@string2) = 4 THEN '$' + LEFT(@string2, 1) + ',' + right(@string,3)
    WHEN length(@string2) = 5 THEN '$' + LEFT(@string2, 2) + ',' + right(@string,3)
    WHEN length(@string2) = 6 THEN '$' + LEFT(@string, 3) + ',' + right(@string,3)
  END 
FROM system.iota;
END;


CREATE PROCEDURE GetROHeader(
  RO cichar(9),
  RO cichar(9) OUTPUT, 
  ROStatus cichar(12) OUTPUT,
  OpenDate cichar(12) OUTPUT,
  CloseDate cichar(12) OUTPUT,
  Customer cichar(30) OUTPUT,
  HomePhone cichar(12) OUTPUT,
  WorkPhone cichar(12) OUTPUT,
  CellPhone cichar(12) OUTPUT,
  CustomerEMail cichar(60) OUTPUT,
  Vehicle cichar(60) OUTPUT,
  WriterFirstName cichar(40) OUTPUT,
  PleaseNote memo OUTPUT)
BEGIN
/*
EXECUTE PROCEDURE GetROHeader('16119081');
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output  
/*
SELECT @ro,'Closed' as ROStatus, '12/31/9999' as OpenDate, a.CloseDate, a.Customer, a.HomePhone, a.WorkPhone, a.CellPhone, 
  'seriously@fuckmylife.com' AS CustomerEMail, a.Vehicle,
  h.FirstName AS WriterFirstName, i.note as PleaseNote
FROM warrantyros a  
INNER JOIN servicewriters h ON a.eeusername = h.eeusername
LEFT JOIN dds.zRONotes i ON a.ro = i.ro  
WHERE a.ro = @ro;
*/
SELECT @ro, a.rostatus, left(d.DayName,3) + ' ' + d.mmdd AS OpenDate, 
  left(e.DayName,3) + ' ' + e.mmdd AS CloseDate, 
  a.customername AS Customer,  
  CASE 
    WHEN f.bnphon  = '0' THEN 'No Phone'
    WHEN f.bnphon  = '' THEN 'No Phone'
    WHEN f.bnphon IS NULL THEN 'No Phone'
    ELSE left(f.bnphon, 12)
  END AS HomePhone, 
  CASE 
    WHEN f.bnbphn  = '0' THEN 'No Phone'
    WHEN f.bnbphn  = '' THEN 'No Phone'
    WHEN f.bnbphn IS NULL THEN 'No Phone'
    ELSE left(f.bnbphn, 12)
  END  AS WorkPhone, 
  CASE 
    WHEN f.bncphon  = '0' THEN 'No Phone'
    WHEN f.bncphon  = '' THEN 'No Phone'
    WHEN f.bncphon IS NULL THEN 'No Phone'
    ELSE left(f.bncphon, 12)
  END  AS CellPhone,    
  case when f.bnemail = '' THEN 'None' ELSE f.bnemail END as CustomerEMail, 
  trim(TRIM(g.immake) + ' ' + TRIM(g.immodl)) AS vehicle, h.fullname AS WriterFirstName,
  b.note AS PleaseNote
FROM (
  SELECT storecode, ro, writerid, vin, customername, customerkey, opendatekey, 
    closedatekey, finalclosedatekey, status AS ROStatus
  FROM factrotoday 
  WHERE ro = @ro
  UNION 
  SELECT storecode, ro, writerid, vin, customername, customerkey, opendatekey, 
    closedatekey, finalclosedatekey, 'Closed' AS ROStatus 
  FROM dds.factro
  WHERE ro = @ro
    AND NOT EXISTS(
      SELECT 1
      FROM factrotoday
      WHERE ro = @ro)) a
LEFT JOIN (
  SELECT *
  FROM dds.zRONotes
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenRONotes
  WHERE ro = @ro) b ON b.ro = a.ro   
LEFT JOIN dds.day d ON a.opendatekey = d.datekey 
LEFT JOIN dds.day e ON a.finalclosedatekey = e.datekey    
LEFT JOIN dds.stgArkonaBOPNAME f ON a.storecode = f.bnco#
  AND a.customerkey = f.bnkey  
LEFT JOIN dds.stgArkonaINPMAST g ON a.vin = g.imvin 
LEFT JOIN servicewriters h ON a.storecode = h.storecode
  AND a.writerid = h.writernumber;
END;  

CREATE PROCEDURE GetRODetail(
  RO cichar(9),
  RO cichar(9) OUTPUT,
  Line integer OUTPUT,
  LineStatus cichar(9) OUTPUT,
  PayType cichar(1) OUTPUT,
  OpCode cichar(10) OUTPUT,
  OpCodeDesc memo OUTPUT,
  Complaint memo OUTPUT,
  Cause memo OUTPUT,
  CorCode cichar(10) OUTPUT,
  CorCodeDesc memo OUTPUT,
  Correction memo OUTPUT)
BEGIN
/*
EXECUTE PROCEDURE GetRODetail('16119081');
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output  
/*
SELECT @ro,  b.Line, 
  'Closed' as LineStatus,b.paytype AS PayType, b.opcode AS OpCode,
  c.sodes1 AS OpCodeDesc, d.complaint AS Complaint, e.cause AS Cause, 
  b.corcode AS CorCode, f.sodes1 AS CorCodeDesc,
  g.Correction 
FROM warrantyros a
LEFT JOIN dds.factroline b ON a.ro = b.ro   
LEFT JOIN dds.stgArkonaSDPLOPC c ON b.storecode = c.soco#
  AND b.opcode = c.solopc
LEFT JOIN dds.zROComplaint d ON b.storecode = d.storecode
  AND b.ro = d.ro AND b.line = d.line 
LEFT JOIN dds.zROCause e ON b.storecode = e.storecode
  AND b.ro = e.ro AND b.line = e.line  
LEFT JOIN dds.stgArkonaSDPLOPC f ON b.storecode = f.soco#
  AND b.corcode = f.solopc  
LEFT JOIN dds.zROCorrection g ON b.storecode = g.storecode 
  AND b.ro = g.ro AND b.line = g.line 
INNER JOIN servicewriters h ON a.eeusername = h.eeusername
LEFT JOIN dds.zRONotes i ON a.ro = i.ro  
WHERE a.ro = @ro;
*/
SELECT @ro,  b.Line, coalesce(h.status, 'Closed'),
  b.paytype AS PayType, b.opcode AS OpCode,
  trim(c.sodes1) + ' ' + TRIM(c.sodes2) AS OpCodeDesc, d.complaint AS Complaint, 
  e.cause AS Cause, 
  b.corcode AS CorCode, trim(f.sodes1) + ' ' + TRIM(f.sodes2) AS CorCodeDesc,
  g.Correction 
FROM (
  SELECT *
  FROM dds.factroline
  WHERE ro = @ro
  UNION
  SELECT *
  FROM factROLineToday
  WHERE ro = @ro) b --ON a.storecode = b.storecode AND a.ro = b.ro  
LEFT JOIN dds.stgArkonaSDPLOPC c ON b.storecode = c.soco#
  AND b.opcode = c.solopc
LEFT JOIN (
  SELECT *
  FROM dds.zROComplaint
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenROComplaint
  WHERE ro = @ro) d ON b.storecode = d.storecode AND b.ro = d.ro AND b.line = d.line 
LEFT JOIN (
  SELECT *
  FROM dds.zROCause
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenROCause
  WHERE ro = @ro) e ON b.storecode = e.storecode AND b.ro = e.ro AND b.line = e.line  
LEFT JOIN dds.stgArkonaSDPLOPC f ON b.storecode = f.soco#
  AND b.corcode = f.solopc  
LEFT JOIN (
  SELECT * 
  FROM dds.zROCorrection
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenROCorrection
  WHERE ro = @ro) g ON b.storecode = g.storecode AND b.ro = g.ro AND b.line = g.line 
LEFT JOIN dds.zOpenROLineStatus h ON b.ro = h.ro AND b.line = h.line;
END;

  
ALTER PROCEDURE GetManagerLandingPageCurrentWeek
   ( 
      eeUsername CICHAR ( 50 ),
      weekid Integer OUTPUT,
      Metric CICHAR ( 45 ) OUTPUT,
      Sun CICHAR ( 22 ) OUTPUT,
      Mon CICHAR ( 22 ) OUTPUT,
      Tue CICHAR ( 22 ) OUTPUT,
      Wed CICHAR ( 22 ) OUTPUT,
      Thu CICHAR ( 22 ) OUTPUT,
      Fri CICHAR ( 22 ) OUTPUT,
      Sat CICHAR ( 22 ) OUTPUT,
      Total CICHAR ( 22 ) OUTPUT,
      seq Integer OUTPUT,
      def CICHAR ( 250 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetManagerLandingPageCurrentWeek('mhuot@rydellchev.com');
EXECUTE PROCEDURE GetManagerLandingPageCurrentWeek('aneumann@gfhonda.com');
4/23
  pull the data live, NOT FROM a TABLE, remove TABLE ManagerLandingPage
  ADD checkouts
6/10 
  ADD input parameter, which store matters
  AND the output for each store needs to be just those writers 
*/
DECLARE @id string;
DECLARE @date date;
@id = (SELECT eeUsername FROM __input);
@date = curdate();
IF @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com') THEN 
  INSERT INTO __output
SELECT x.*, z.def
FROM (  
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
    cast(cast(SUM(e.today) AS sql_double) AS sql_char) AS thisweek,
    e.seq
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
        AND c.metric IN ('Labor Sales','ROs Opened','ROs Closed','Walk Arounds')
        AND b.storecode = c.storecode) a /**************************/
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND b.storecode = 'RY1' /**************************/
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq  
  UNION 
  -- the problem IS that this number IS NOT additive accross days
  -- it should be the most recent value
  SELECT f.SundayToSaturdayWeek, f.metric, f.sun,f.mon,f.tue,f.wed,f.thu,f.fri,
    f.sat, cast(cast(g.today AS sql_double) AS sql_char), f.seq
  FROM ( -- dept daily values pivoted
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      e.seq
    FROM ( -- dept daily values (ADD store/dept to grouping to generalize)
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY1' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) e   
    GROUP BY e.SundayToSaturdayWeek, e.metric, e.seq) f  
  LEFT JOIN ( -- most recent dept value for a week 
    SELECT metric, today
    FROM ( -- daily
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY1' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) y
    WHERE thedate = (
      SELECT MAX(thedate)
      FROM (
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY1' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) x)) g ON f.metric = g.metric
  UNION 
  -- the problem IS that this number IS NOT additive accross days
  -- it should be the most recent value
  SELECT f.SundayToSaturdayWeek, f.metric, f.sun,f.mon,f.tue,f.wed,f.thu,f.fri,
    f.sat, cast(cast(g.today AS sql_double) AS sql_char), f.seq
  FROM ( -- dept daily values pivoted
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      e.seq
    FROM ( -- dept daily values (ADD store/dept to grouping to generalize)
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Warranty Calls'
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) e   
    GROUP BY e.SundayToSaturdayWeek, e.metric, e.seq) f  
  LEFT JOIN ( -- most recent dept value for a week 
    SELECT metric, today
    FROM ( -- daily
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Warranty Calls'
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) y
    WHERE thedate = (
      SELECT MAX(thedate)
      FROM (
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Warranty Calls'
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) x)) g ON f.metric = g.metric    
  UNION 
  SELECT x.SundayToSaturdayWeek, 'Cashiering', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
    trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
    trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,1 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric = 'rosCashieredByWriter') a
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
    LEFT JOIN (
      SELECT e.sundaytosaturdayweek, e.metric, 
        max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
        max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
        max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
        max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
        max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
        max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
        max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
        cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
        e.seq
      FROM (  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
          SUM(coalesce(today, 0)) AS today
        FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
          SELECT *
          FROM dds.day a, servicewriters b, servicewritermetrics c
          WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
              AND c.metric  = 'rosCashiered') a
        LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
          AND a.eeusername = b.eeusername 
          AND a.metric = b.metric    
        GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 
  UNION
  SELECT x.SundayToSaturdayWeek, 'Avg Hours/RO', 
    CASE y.sun WHEN 0 then '0' ELSE cast(round(x.sun/y.sun, 1) AS sql_char) END, 
    CASE y.mon WHEN 0 then '0' ELSE cast(round(x.mon/y.mon, 1) AS sql_char) END, 
    CASE y.tue WHEN 0 then '0' ELSE cast(round(x.tue/y.tue, 1) AS sql_char) END, 
    CASE y.wed WHEN 0 then '0' ELSE cast(round(x.wed/y.wed, 1) AS sql_char) END, 
    CASE y.thu WHEN 0 then '0' ELSE cast(round(x.thu/y.thu, 1) AS sql_char) END, 
    CASE y.fri WHEN 0 then '0' ELSE cast(round(x.fri/y.fri, 1) AS sql_char) END, 
    CASE y.sat WHEN 0 then '0' ELSE cast(round(x.sat/y.sat, 1) AS sql_char) END, 
    CASE y.thisweek WHEN 0 then '0' ELSE cast(round(x.thisweek/y.thisweek, 1) AS sql_char) END, 
    4 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'flagHours'
          AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY1' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x  
  LEFT JOIN (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric = 'rosClosedWithFlagHours'
            AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY1' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq)  y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek 
  UNION   
  SELECT x.SundayToSaturdayWeek, 'Inspections Requested', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
    trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
    trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,
    8 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'rosClosedWithInspectionLine'
          AND b.storecode = c.storecode) a /**********************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY1' /**********************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
    LEFT JOIN (
      SELECT e.sundaytosaturdayweek, e.metric, 
        max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
        max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
        max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
        max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
        max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
        max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
        max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
        cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
        e.seq
      FROM (  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
          SUM(coalesce(today, 0)) AS today
        FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
          SELECT *
          FROM dds.day a, servicewriters b, servicewritermetrics c
          WHERE a.SundayToSaturdayWeek  = (
              SELECT SundayToSaturdayWeek 
              FROM dds.day
              WHERE thedate = @date)
            AND c.metric  = 'ROs Closed'
            AND b.storecode = c.storecode) a /**********************************/
        LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
          AND b.storecode = 'RY1' /**********************************/
          AND a.eeusername = b.eeusername 
          AND a.metric = b.metric    
        GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  UNION 
  -- checkouts
  SELECT f.SundayToSaturdayWeek, 'CheckOuts',f.sun,f.mon,f.tue,f.wed,f.thu,
    f.fri,f.sat, cast(cast(g.total AS sql_double) AS sql_char), f.seq
  FROM (
    SELECT e.sundaytosaturdayweek, 'Check Outs', 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      999 AS seq
    FROM (    
      SELECT SundayToSaturdayWeek, thedate, dayname, COUNT(*) AS today
      FROM serviceWriterMetricData 
      WHERE metric = 'walk arounds' 
        AND storecode = 'RY1' /****************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, thedate, dayname) e
    GROUP BY SundayToSaturdayWeek ) f   
  LEFT JOIN (
    SELECT SundayToSaturdayWeek, COUNT(*) AS total
    FROM serviceWriterMetricData 
    WHERE metric = 'walk arounds' 
      AND storecode = 'RY1' /****************************/
      AND SundayToSaturdayWeek = (
        SELECT SundayToSaturdayWeek
        FROM dds.day
        WHERE thedate = @date)
    GROUP BY SundayToSaturdayWeek) g ON f.SundayToSaturdayWeek = g.SundayToSaturdayWeek) x
LEFT JOIN serviceWriterMetrics z ON x.metric = z.metric
  AND z.storecode = 'ry1';              
ELSEIF @id = 'aneumann@gfhonda.com' THEN   
  INSERT INTO __output
SELECT x.*, z.def
FROM (    
  SELECT e.sundaytosaturdayweek, e.metric, 
    max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
    max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
    max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
    max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
    max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
    max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
    max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
    cast(cast(SUM(e.today) AS sql_double) AS sql_char) AS thisweek,
    e.seq
  FROM (  
    SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
      SUM(coalesce(today, 0)) AS today
    FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
      SELECT *
      FROM dds.day a, servicewriters b, servicewritermetrics c
      WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
        AND c.metric IN ('Labor Sales','ROs Opened','ROs Closed','Walk Arounds')
        AND b.storecode = c.storecode) a /**************************/
    LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
      AND b.storecode = 'RY2' /**************************/
      AND a.eeusername = b.eeusername 
      AND a.metric = b.metric    
    GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
  GROUP BY e.sundaytosaturdayweek, e.metric, e.seq
  UNION 
  -- the problem IS that this number IS NOT additive accross days
  -- it should be the most recent value
  SELECT f.SundayToSaturdayWeek, f.metric, f.sun,f.mon,f.tue,f.wed,f.thu,f.fri,
    f.sat, cast(cast(g.today AS sql_double) AS sql_char), f.seq
  FROM ( -- dept daily values pivoted
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      e.seq
    FROM ( -- dept daily values (ADD store/dept to grouping to generalize)
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY2' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) e   
    GROUP BY e.SundayToSaturdayWeek, e.metric, e.seq) f  
  LEFT JOIN ( -- most recent dept value for a week 
    SELECT metric, today
    FROM ( -- daily
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY2' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) y
    WHERE thedate = (
      SELECT MAX(thedate)
      FROM (
      SELECT SundayToSaturdayWeek, dayname, thedate, metric, SUM(today) AS today, seq
      FROM serviceWriterMetricData a
      WHERE a.metric = 'Open Aged ROs'
        AND a.storecode = 'RY2' /**********************************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, dayname, thedate, metric, seq) x)) g ON f.metric = g.metric
  UNION
  SELECT x.SundayToSaturdayWeek, 'Avg Hours/RO', 
    CASE y.sun WHEN 0 then '0' ELSE cast(round(x.sun/y.sun, 1) AS sql_char) END, 
    CASE y.mon WHEN 0 then '0' ELSE cast(round(x.mon/y.mon, 1) AS sql_char) END, 
    CASE y.tue WHEN 0 then '0' ELSE cast(round(x.tue/y.tue, 1) AS sql_char) END, 
    CASE y.wed WHEN 0 then '0' ELSE cast(round(x.wed/y.wed, 1) AS sql_char) END, 
    CASE y.thu WHEN 0 then '0' ELSE cast(round(x.thu/y.thu, 1) AS sql_char) END, 
    CASE y.fri WHEN 0 then '0' ELSE cast(round(x.fri/y.fri, 1) AS sql_char) END, 
    CASE y.sat WHEN 0 then '0' ELSE cast(round(x.sat/y.sat, 1) AS sql_char) END, 
    CASE y.thisweek WHEN 0 then '0' ELSE cast(round(x.thisweek/y.thisweek, 1) AS sql_char) END, 
    4 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'flagHours'
          AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY2' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x  
  LEFT JOIN (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(e.today AS sql_double) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(e.today AS sql_double) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(e.today AS sql_double) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(e.today AS sql_double) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(e.today AS sql_double) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(e.today AS sql_double) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(e.today AS sql_double) END) AS Sat,
      SUM(cast(e.today AS sql_double)) AS thisweek,
      e.seq
    FROM ( 
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
          SELECT SundayToSaturdayWeek 
          FROM dds.day
          WHERE thedate = @date)
            AND c.metric = 'rosClosedWithFlagHours'
            AND b.storecode = c.storecode) a /**************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY2' /**************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, a.additive) e
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq)  y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  UNION   
  SELECT x.SundayToSaturdayWeek, 'Inspections Requested', trim(x.sun) + ' / ' + y.sun, trim(x.mon) + ' / ' + y.mon,
    trim(x.tue) + ' / ' + y.tue, trim(x.wed) + ' / ' + y.wed, trim(x.thu) + ' / ' + y.thu,
    trim(x.fri) + ' / ' + y.fri, trim(x.sat) + ' / ' + y.sat, trim(x.thisweek) + ' / ' + y.thisweek,
    8 AS seq
  FROM (
    SELECT e.sundaytosaturdayweek, e.metric, 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      cast(SUM(cast(e.today AS sql_double)) AS sql_char) AS thisweek,
      e.seq
    FROM (  
      SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
        SUM(coalesce(today, 0)) AS today
      FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
        SELECT *
        FROM dds.day a, servicewriters b, servicewritermetrics c
        WHERE a.SundayToSaturdayWeek  = (
            SELECT SundayToSaturdayWeek 
            FROM dds.day
            WHERE thedate = @date)
          AND c.metric = 'rosClosedWithInspectionLine'
          AND b.storecode = c.storecode) a /**********************************/
      LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
        AND b.storecode = 'RY2' /**********************************/
        AND a.eeusername = b.eeusername 
        AND a.metric = b.metric    
      GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) x 
    LEFT JOIN (
      SELECT e.sundaytosaturdayweek, e.metric, 
        max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
        max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
        max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
        max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
        max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
        max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
        max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
        cast(SUM(cast(e.today AS sql_double)) as sql_char)AS thisweek,
        e.seq
      FROM (  
        SELECT a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq, 
          SUM(coalesce(today, 0)) AS today
        FROM ( -- a :: 1 row per day (for each day IN the range)/writer (ALL current writers)/metric (ALL)
          SELECT *
          FROM dds.day a, servicewriters b, servicewritermetrics c
          WHERE a.SundayToSaturdayWeek  = (
              SELECT SundayToSaturdayWeek 
              FROM dds.day
              WHERE thedate = @date)
            AND c.metric  = 'ROs Closed'
            AND b.storecode = c.storecode) a /**********************************/
        LEFT JOIN servicewritermetricdata b ON a.thedate = b.thedate
          AND b.storecode = 'RY2' /**********************************/
          AND a.eeusername = b.eeusername 
          AND a.metric = b.metric    
        GROUP BY a.sundaytosaturdayweek, a.thedate, a.dayname, a.metric, a.seq) e  
    GROUP BY e.sundaytosaturdayweek, e.metric, e.seq) y ON  x.SundayToSaturdayWeek = y.SundayToSaturdayWeek
  UNION
  -- checkouts
  SELECT f.SundayToSaturdayWeek, 'CheckOuts',f.sun,f.mon,f.tue,f.wed,f.thu,
    f.fri,f.sat, cast(cast(g.total AS sql_double) AS sql_char), f.seq
  FROM (
    SELECT e.sundaytosaturdayweek, 'Check Outs', 
      max(CASE WHEN e.dayname = 'Sunday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sun,
      max(CASE WHEN e.dayname = 'Monday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Mon,
      max(CASE WHEN e.dayname = 'Tuesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Tue,
      max(CASE WHEN e.dayname = 'Wednesday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Wed,
      max(CASE WHEN e.dayname = 'Thursday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Thu,
      max(CASE WHEN e.dayname = 'Friday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Fri,
      max(CASE WHEN e.dayname = 'Saturday' THEN cast(cast(e.today AS sql_double) AS sql_char) END) AS Sat,
      999 AS seq
    FROM (    
      SELECT SundayToSaturdayWeek, thedate, dayname, COUNT(*) AS today
      FROM serviceWriterMetricData 
      WHERE metric = 'walk arounds' 
        AND storecode = 'RY2' /****************************/
        AND SundayToSaturdayWeek = (
          SELECT SundayToSaturdayWeek
          FROM dds.day
          WHERE thedate = @date)
      GROUP BY SundayToSaturdayWeek, thedate, dayname) e
    GROUP BY SundayToSaturdayWeek ) f   
  LEFT JOIN (
    SELECT SundayToSaturdayWeek, COUNT(*) AS total
    FROM serviceWriterMetricData 
    WHERE metric = 'walk arounds' 
      AND storecode = 'RY2' /****************************/
      AND SundayToSaturdayWeek = (
        SELECT SundayToSaturdayWeek
        FROM dds.day
        WHERE thedate = @date)
    GROUP BY SundayToSaturdayWeek) g ON f.SundayToSaturdayWeek = g.SundayToSaturdayWeek) x
LEFT JOIN serviceWriterMetrics z ON x.metric = z.metric
  AND z.storecode = 'ry2';                     

ENDIF;





END;
  
ALTER PROCEDURE GetROHeader
   ( 
      RO CICHAR ( 9 ),
      RO CICHAR ( 9 ) OUTPUT,
      ROStatus CICHAR ( 12 ) OUTPUT,
      OpenDate CICHAR ( 12 ) OUTPUT,
      CloseDate CICHAR ( 12 ) OUTPUT,
      Customer CICHAR ( 30 ) OUTPUT,
      HomePhone CICHAR ( 12 ) OUTPUT,
      WorkPhone CICHAR ( 12 ) OUTPUT,
      CellPhone CICHAR ( 12 ) OUTPUT,
      CustomerEMail CICHAR ( 60 ) OUTPUT,
      Vehicle CICHAR ( 60 ) OUTPUT,
      WriterFirstName CICHAR ( 40 ) OUTPUT,
      PleaseNote Memo OUTPUT
   ) 
BEGIN 
/*

EXECUTE PROCEDURE GetROHeader('16120890');
EXECUTE PROCEDURE getrodetail('16121172')

6/27 forgot about the fucking honda deal 
- trying to use the same basic script AS used for getting warranty ros, returns 
a shitload of no phone #s
the problem turns out to be IN the JOIN to bopname
warranty uses custkey & store to JOIN factro AND bopname, with honda this results
IN a lot of NULL bopname rows
- but IF i take out the store, get a shitload of multiple rows
- conclusion: use customerkey & name to JOIN factro to bopname, good enough
further testing showed a couple being LEFT out, but, again, good enough
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output  
/* version 1
SELECT @ro,'Closed' as ROStatus, '12/31/9999' as OpenDate, a.CloseDate, a.Customer, a.HomePhone, a.WorkPhone, a.CellPhone, 
  'seriously@fuckmylife.com' AS CustomerEMail, a.Vehicle,
  h.FirstName AS WriterFirstName, i.note as PleaseNote
FROM warrantyros a  
INNER JOIN servicewriters h ON a.eeusername = h.eeusername
LEFT JOIN dds.zRONotes i ON a.ro = i.ro  
WHERE a.ro = @ro;
*/
/* version 3 */
SELECT ro, rostatus, opendatedaynamemmdd, finalclosedatedaynamemmdd, customer,
  homephone, workphone, cellphone, customeremail, vehicle, b.fullname, pleasenote
FROM dds.zroheader a
LEFT JOIN servicewriters b ON a.writernumber = b.writernumber
WHERE a.ro = @ro;
--WHERE a.ro = '16120890'

/* version 2
SELECT @ro, a.rostatus, left(d.DayName,3) + ' ' + d.mmdd AS OpenDate, 
  left(e.DayName,3) + ' ' + e.mmdd AS CloseDate, 
  a.customername AS Customer,  
  CASE 
    WHEN f.bnphon  = '0' THEN 'No Phone'
    WHEN f.bnphon  = '' THEN 'No Phone'
    WHEN f.bnphon IS NULL THEN 'No Phone'
    ELSE left(f.bnphon, 12)
  END AS HomePhone, 
  CASE 
    WHEN f.bnbphn  = '0' THEN 'No Phone'
    WHEN f.bnbphn  = '' THEN 'No Phone'
    WHEN f.bnbphn IS NULL THEN 'No Phone'
    ELSE left(f.bnbphn, 12)
  END  AS WorkPhone, 
  CASE 
    WHEN f.bncphon  = '0' THEN 'No Phone'
    WHEN f.bncphon  = '' THEN 'No Phone'
    WHEN f.bncphon IS NULL THEN 'No Phone'
    ELSE left(f.bncphon, 12)
  END  AS CellPhone,    
  case when f.bnemail = '' THEN 'None' ELSE f.bnemail END as CustomerEMail, 
  trim(TRIM(g.immake) + ' ' + TRIM(g.immodl)) AS vehicle, h.fullname AS WriterFirstName,
  b.note AS PleaseNote
FROM (
  SELECT storecode, ro, writerid, vin, customername, customerkey, opendatekey, 
    closedatekey, finalclosedatekey, status AS ROStatus
  FROM factrotoday 
  WHERE ro = @ro
  UNION 
  SELECT storecode, ro, writerid, vin, customername, customerkey, opendatekey, 
    closedatekey, finalclosedatekey, 'Closed' AS ROStatus 
  FROM dds.factro
  WHERE ro = @ro
    AND NOT EXISTS(
      SELECT 1
      FROM factrotoday
      WHERE ro = @ro)) a
LEFT JOIN (
  SELECT *
  FROM dds.zRONotes
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenRONotes
  WHERE ro = @ro) b ON b.ro = a.ro   
LEFT JOIN dds.day d ON a.opendatekey = d.datekey 
LEFT JOIN dds.day e ON a.finalclosedatekey = e.datekey    
--LEFT JOIN dds.stgArkonaBOPNAME f ON a.storecode = f.bnco#
--  AND a.customerkey = f.bnkey  
LEFT JOIN dds.stgArkonaBOPNAME f ON a.customername = f.bnsnam
  AND a.customerkey = f.bnkey    
LEFT JOIN dds.stgArkonaINPMAST g ON a.vin = g.imvin 
LEFT JOIN servicewriters h ON a.storecode = h.storecode
  AND a.writerid = h.writernumber;
*/



END;


