/*
CREATE PROCEDURE
	GetWarrantyAdminROs
	GetWarrantyAdminROUpdates
ALTER TABLE 
  AgedRoUpdates ADD COLUMN statusUpdateWithDateName cichar(250);	
ALTER PROCEDURE 
  GetAgedROs: return statusUpdateWithDateName
  GetCurrentROs: return statusUpdateWithDateName
  InsertROUpdate: INSERT statusUpdateWithDateName value
  InsertServiceWriterCheckout: remove walk arounds
DATA to import
  People: 2 new users
  HTMLSnippets  
DATA changes   
  DELETE FROM servicewritermetrics WHERE metric = 'walk arounds';
  DELETE FROM servicewritermetricdata WHERE metric = 'walk arounds';   
*/ 

INSERT INTO "People" VALUES( 'jgustafson@rydellchev.com', 'June Gustafson', 'June', 'Gustafson', 'password', 'WarrantyAdmin', 'ALL' );
INSERT INTO "People" VALUES( 'cmoulds@rydellcars.com', 'Crystal Moulds', 'Crystal', 'Moulds', 'password', 'WarrantyAdmin', 'ALL' );

DELETE FROM HTMLSnippets;

INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceManager', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/managerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/currentros">Current ROs</a></span></li>    
    <li><span class="sublink"><a href="#sco/agedros">Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantyadminros">Warranty ROs in Cashier Status</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantycalls">Warranty Calls</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceManager', 'NetPromoter', 'np', 'LeftNavBar', NULL, '<!-- Net Promoter -->
<nav class="menu-item">
  <a class="mainlink" href="#np/managersummary"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>
  <ul class="submenu">
<li><span class="sublink"><a href="#np/aboutnp">About Net Promoter</a></span></li>   
 <li><span class="sublink"><a href="#np/managersurveys">Surveys</a></span></li>
  </ul>
</nav><!-- .menu-item -->
', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceManager', 'Employees', 'emps', 'LeftNavBar', NULL, '<!-- Employees -->
<nav class="menu-item">
  <a class="mainlink employees" href="#emps/employeelist"><span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', 3 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/writerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/currentros">Current ROs</a></span></li>    
    <li><span class="sublink"><a href="#sco/agedros">Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantycalls">Warranty Calls</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'NetPromoter', 'np', 'LeftNavBar', NULL, '<!-- Net Promoter -->
<nav class="menu-item">
  <a class="mainlink" href="#np/writersummary"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>
<ul class="submenu">
<li><span class="sublink"><a href="#np/aboutnp">About Net Promoter</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->
', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceManager', 'Employees', 'emps', 'LeftNavBar', NULL, '<!-- Employees -->
<nav class="menu-item">
  <a class="mainlink employees" href="#emps/employeelist"><span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceManager', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">  <a class="mainlink" href="#sco/managerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/currentros">Current ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/agedros">Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantyadminros">Warranty ROs in Cashier Status</a></span></li>
    <li><span class="sublink"><a href="#sco/ry2servicecalls">Nissan Follow Up Calls</a></span></li>
  </ul></nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/writerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/currentros">Current ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/agedros">Aged ROs</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceMisc', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/ry2servicecalls"><span class="glyphicon glyphicon-pencil"></span>Nissan Follow Up Calls<span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'MainAction', NULL, '<div class="btn-group" id="servicestats">
  <a class="button" href="#sco/writerstats/<%username%>">Service Stats</a>
</div>
', NULL );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'NetPromoter', 'np', 'MainAction', NULL, '<div class="btn-group" id="netpromoter">
  <a class="button" href="#np/writer/<%username%>">Net Promoter</a>
</div>
', NULL );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Aged ROs', '<a class="button" href="#sco/agedros/<%username%>">View Aged ROs</a>', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Warranty Calls', '<a class="button" href="#sco/warrantycalls/<%username%>">View Warranty Calls</a>', 3 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'MainAction', NULL, '<div class="btn-group" id="servicestats">
  <a class="button" href="#sco/writerstats/<%username%>">Service Stats</a>
</div>
', NULL );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Aged ROs', '<a class="button" href="#sco/agedros/<%username%>">View Aged ROs</a>', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Current ROs', '<a class="button" href="#sco/currentros/<%username%>">View Current ROs</a>', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'CurrentROs', '<a class="button" href="#sco/currentros/<%username%>">View Current ROs</a>', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'ALL', 'WarrantyAdmin', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/warrantyadminros"><span class="glyphicon glyphicon-pencil"></span>Warranty ROs<span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', NULL );



CREATE PROCEDURE GetWarrantyAdminROs (  
      eeUsername CICHAR ( 50 ),
      firstName CICHAR ( 20 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      openDate CICHAR ( 8 ) OUTPUT,
      customer CICHAR ( 50 ) OUTPUT,
      roStatus CICHAR ( 24 ) OUTPUT,
      vin CICHAR ( 17 ) OUTPUT,
      vehicle CICHAR ( 60 ) OUTPUT,
      statusUpdate Memo OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetWarrantyAdminROs('jgustafson@rydellchev.com');
EXECUTE PROCEDURE GetWarrantyAdminROs('cmoulds@rydellcars.com');

Shit IS so fast, don't need a WarrantyAdmin TABLE, pulls the data directly
FROM factRepairOrder/TodayfactRepairOrder
*/
INSERT INTO __output
SELECT e.firstname, a.ro, a.OpenDate, b.fullname AS Customer, a.roStatus, 
  c.vin, TRIM(c.make) + ' ' + trim(c.model) AS vehicle, f.statusUpdateWithDateName   
FROM (  
  SELECT a.ro, d.thedate, d.mmddyy as OpenDate, a.customerkey, b.rostatus, a.vehiclekey, a.servicewriterkey
  FROM dds.factRepairOrder a
  INNER JOIN dds.dimRoStatus b ON a.RoStatusKey = b.RoStatusKey
  INNER JOIN dds.dimPaymentType c ON a.PaymentTypeKey = c.PaymentTypeKey
  LEFT JOIN dds.day d ON a.opendatekey = d.datekey
  WHERE a.finalclosedatekey IN (SELECT datekey FROM dds.day WHERE thedate = '12/31/9999') -- this adds a shitload of speed
    AND b.RoStatusCode IN ('5','4')
    AND c.PaymentTypeCode = 'W'  
  UNION -- don't need to GROUP OR DO DISTINCT, fucking UNION does it for me
  SELECT a.ro, d.thedate, d.mmddyy as OpenDate, a.customerkey, b.rostatus, a.vehiclekey, a.servicewriterkey
  FROM dds.TodayfactRepairOrder a
  INNER JOIN dds.dimRoStatus b ON a.RoStatusKey = b.RoStatusKey
  INNER JOIN dds.dimPaymentType c ON a.PaymentTypeKey = c.PaymentTypeKey
  LEFT JOIN dds.day d ON a.opendatekey = d.datekey
  WHERE a.finalclosedatekey IN (SELECT datekey FROM dds.day WHERE thedate = '12/31/9999') -- this adds a shitload of speed
    AND b.RoStatusCode IN ('5','4')
    AND c.PaymentTypeCode = 'W') a	
INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
  AND d.CensusDept = 'MR'
INNER JOIN ServiceWriters e on d.writernumber = e.writernumber
LEFT JOIN (
  SELECT *
  FROM AgedROUpdates x
  WHERE updateTS = (
    SELECT MAX(updatets)
    FROM AgedRoUpdates
    WHERE ro = x.ro)) f ON a.ro = f.ro;
END;	

CREATE PROCEDURE GetWarrantyAdminROUpdates
   ( 
      ro CICHAR ( 9 ),
      updateTS CICHAR ( 20 ) OUTPUT,
      eeUserName CICHAR ( 50 ) OUTPUT,
      statusUpdate Memo OUTPUT
   ) 
BEGIN 
/*

*/
DECLARE @ro cichar(9);
@ro = (SELECT ro FROM __input);
  INSERT INTO __output
  SELECT top 100
    TRIM(c.mmdd) 
    + ' ' + 
    trim(
      CASE 
        WHEN hour(updateTS) > 12 THEN 
          trim(cast(hour(updateTS) - 12 AS sql_char))
        ELSE trim(cast(hour(updateTS) AS sql_char))
      END 
      + ':' +
      CASE
        WHEN minute(updateTS) < 10 THEN
          '0' + cast(minute(updateTS) AS sql_char)
        else cast(minute(updateTS) AS sql_char)
      END) 
    + ' ' +  
    CASE WHEN hour(updateTS) > 12 THEN 'PM' ELSE 'AM' END,   
    TRIM(b.firstname) + ' ' +  LEFT(b.lastname, 1) AS eeusername, 
    statusUpdate
  FROM AgedROUpdates a
  LEFT JOIN people b ON a.eeusername = b.eeusername
  LEFT JOIN dds.day c ON CAST(a.updateTS AS sql_date) = c.thedate
  WHERE ro = @ro
  ORDER BY updateTS desc;

END;

ALTER TABLE AgedRoUpdates 
ADD COLUMN statusUpdateWithDateName cichar(250);

UPDATE AgedRoUpdates
SET statusUpdateWithDateName = xx.statusUpdateWithDateName
FROM (
  SELECT x.ro, x.updateTS, x.eeusername, LEFT(x.thedate + ' ' + trim(x.thename) + ': ' + x.theupdate, 250) AS statusUpdateWithDateName
  FROM (
    SELECT a.*,
      (SELECT mmdd FROM dds.day WHERE thedate = CAST(a.updateTS AS sql_date)) AS thedate,
      (SELECT firstname FROM people WHERE eeusername = a.eeusername) AS thename,
      cast(statusupdate AS sql_char) collate ads_default_ci AS theUpdate
    FROM agedroupdates a) x) xx
WHERE AgedRoUpdates.ro = xx.ro
  AND AgedRoUpdates.eeusername = xx.eeusername
  AND AgedRoUpdates.updatets = xx.updatets;
  
ALTER PROCEDURE GetAgedROs
   ( 
      eeUsername CICHAR ( 50 ),
      firstName CICHAR ( 20 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      openDate CICHAR ( 8 ) OUTPUT,
      customer CICHAR ( 50 ) OUTPUT,
      roStatus CICHAR ( 24 ) OUTPUT,
      vin CICHAR ( 17 ) OUTPUT,
      vehicle CICHAR ( 60 ) OUTPUT,
      statusUpdate Memo OUTPUT,
      theDate DATE OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getagedros('rodt@rydellchev.com');
EXECUTE PROCEDURE getagedros('tbursinger@rydellchev.com');
EXECUTE PROCEDURE getagedros('mhuot@rydellchev.com');
EXECUTE PROCEDURE getagedros('aneumann@gfhonda.com');
*/
DECLARE @id string;
@id = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT b.firstname, a.ro, c.mmddyy AS opendate, a.customer, a.rostatus, a.vin, a.vehicle, 
  d.statusUpdateWithDateName, openDate
FROM AgedRos a
INNER JOIN ServiceWriters b ON a.eeusername = b.eeusername
  AND CASE
    WHEN @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com') THEN b.storecode = 'RY1'
    WHEN @id = 'aneumann@gfhonda.com' THEN b.storecode = 'RY2'
    ELSE b.eeusername = @id 
  END 
LEFT JOIN dds.day c ON a.opendate = c.thedate
LEFT JOIN (
  SELECT *
  FROM AgedROUpdates x
  WHERE updateTS = (
    SELECT MAX(updatets)
    FROM AgedRoUpdates
    WHERE ro = x.ro)) d ON a.ro = d.ro; 
END;

ALTER PROCEDURE GetCurrentROs
   ( 
      eeUsername CICHAR ( 50 ),
      firstName CICHAR ( 20 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      openDate CICHAR ( 8 ) OUTPUT,
      customer CICHAR ( 50 ) OUTPUT,
      roStatus CICHAR ( 24 ) OUTPUT,
      vin CICHAR ( 17 ) OUTPUT,
      vehicle CICHAR ( 60 ) OUTPUT,
      statusUpdate Memo OUTPUT,
      theDate DATE OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getCurrentros('rodt@rydellchev.com');
EXECUTE PROCEDURE getCurrentros('dpederson@gfhonda.com');
EXECUTE PROCEDURE getCurrentros('mhuot@rydellchev.com');
EXECUTE PROCEDURE getCurrentros('aneumann@gfhonda.com');
*/
DECLARE @id string;
@id = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT b.firstname, a.ro, c.mmddyy AS opendate, a.customer, a.rostatus, a.vin, a.vehicle, 
  d.statusUpdateWithDateName, openDate
FROM CurrentRos a
INNER JOIN ServiceWriters b ON a.eeusername = b.eeusername
  AND CASE
    WHEN @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com') THEN b.storecode = 'RY1'
    WHEN @id = 'aneumann@gfhonda.com' THEN b.storecode = 'RY2'
    ELSE b.eeusername = @id 
  END 
LEFT JOIN dds.day c ON a.opendate = c.thedate
LEFT JOIN (
  SELECT *
  FROM AgedROUpdates x
  WHERE updateTS = (
    SELECT MAX(updatets)
    FROM AgedRoUpdates
    WHERE ro = x.ro)) d ON a.ro = d.ro; 
END;

ALTER PROCEDURE InsertROUpdate
   ( 
      ro CICHAR ( 9 ),
      eeUserName CICHAR ( 50 ),
      statusUpdate Memo
   ) 
BEGIN 
/*
EXECUTE PROCEDURE InsertROUpdate()
*/
DECLARE @ro string;
DECLARE @eeUserName string;
DECLARE @statusUpdate string;
@ro = (SELECT ro FROM __input);
@eeusername = (SELECT eeUserName FROM __input);
@statusUpdate = (SELECT statusUpdate FROM __input);

INSERT INTO AgedRoUpdates values(@ro, now(), @eeusername, @statusUpdate,
  left(CAST((SELECT mmdd FROM dds.day WHERE thedate = curdate()) AS sql_char) + ' ' +
    TRIM((SELECT firstname FROM people WHERE eeusername = @eeUserName)) + ': ' + 
    @statusupdate, 250));
END;

--< Remove Walk Arounds ----------------------------------------------------------------
ALTER PROCEDURE InsertServiceWriterCheckout
   ( 
      eeUserName CICHAR ( 50 ),
      theDate DATE,
      csi DOUBLE ( 15 )
//      walkarounds Integer
   ) 
BEGIN 
/*
EXECUTE PROCEDURE InsertServiceWriterCheckout(
  'rodt@rydellchev.com',curdate(), 48.9, 15);
  
DELETE FROM ServiceWriterMetricData
WHERE eeusername = 'rodt@rydellchev.com'
  AND thedate = curdatE()
  AND metric in ('csi', 'walk arounds')

10/11
  remove walkarounds
*/
DECLARE @eeusername string;
DECLARE @date date;
DECLARE @csi double;
DECLARE @walkarounds integer;
@eeusername = (SELECT eeusername FROM __input);
@date = (SELECT thedate FROM __input);
@csi = (SELECT csi FROM __input);
INSERT INTO ServiceWriterMetricData
SELECT @eeusername, c.storecode, c.fullname, c.lastname, c.firstname, c.writernumber,
  a.datekey, @date, a.dayofweek, a.dayname, a.dayofmonth, a.SundayToSaturdayWeek,
  'CSI', b.seq, @csi, @csi, trim(CAST(@csi AS sql_char)), trim(CAST(@csi AS sql_char))
FROM dds.day a, servicewritermetrics b, servicewriters c
WHERE a.thedate = @date
  AND c.eeusername = @eeusername
  AND b.metric = 'CSI'
  AND b.storecode = c.storecode;
END;

EXECUTE PROCEDURE sp_DropReferentialIntegrity( 'swMetrics-serviceWriterMetricData' );

DELETE FROM servicewritermetrics WHERE metric = 'walk arounds';
DELETE FROM servicewritermetricdata WHERE metric = 'walk arounds';

EXECUTE PROCEDURE sp_CreateReferentialIntegrity (
  'swMetrics-serviceWriterMetricData','ServiceWriterMetrics','ServiceWriterMetricData', 
  'STORECODE-METRIC', 1, 2, NULL ,'', ''); 

--/> Remove Walk Arounds ----------------------------------------------------------------

--< gregs session shit AND jons new dept/position shit ----------------------------

Create Table Sessions(
   SessionID CIChar( 50 ),
   State Memo );
INSERT INTO "Sessions" VALUES( '5VU4XaZMQj-PueEfxkxcfg', '{"globalUser":{"loggedIn":"true","userName":"mhuot@rydellchev.com","firstName":"Mike","lastName":"Huot","fullName":"Mike Huot","memberGroup":"ServiceManager","storeCode":"RY1"},"pageHistory":[],"oldURL":"http://localhost/intranet/main/dashboard.php","newURL":"http://localhost/intranet/main/dashboard.php#sco/managerstats"}' );
INSERT INTO "Sessions" VALUES( 'pMqB7jyuR9CeTce8YG4tNw', '{"name":"Greg Sorum"}' );

CREATE PROCEDURE phpSetSessionData
   ( 
      SessionID CICHAR ( 50 ),
      State Memo,
      NewSessionID CICHAR ( 50 ) OUTPUT
   ) 
BEGIN 
  DECLARE @SessionExists Logical;
  DECLARE @SessionID cichar(50);
  DECLARE @State memo;
  @State = (SELECT State FROM __input);
  @SessionID = (select SessionID from __input);
  @SessionExists = (SELECT coalesce(SessionID, 'No Session') FROM Sessions WHERE SessionID = @SessionID) = @SessionID;
  IF @SessionExists THEN
    UPDATE Sessions
	  SET State = @State
	WHERE SessionID = @SessionID;
  ELSE
    @SessionID = (select NewIDString(FILE) from system.iota);
    INSERT INTO Sessions values(@SessionID, @State);
  ENDIF;  
  insert into __output
	select @SessionID from system.iota;

END;

CREATE PROCEDURE phpGetSessionData
   ( 
      SessionID CICHAR ( 50 ),
      State Memo OUTPUT
   ) 
BEGIN 
  INSERT INTO __output
    SELECT State
	FROM Sessions
	WHERE SessionID = (SELECT SessionID FROM __input);

END;


CREATE TABLE zDepartments (
  DepartmentKey integer,
  dl1 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
  dl2 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
  dl3 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
  dl4 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
  dl5 cichar(24) CONSTRAINT NOT NULL DEFAULT 'N/A',
CONSTRAINT PK PRIMARY KEY (DepartmentKey)) IN database;  
-- CONSTRAINT PK PRIMARY KEY (dl1,dl2,dl3,dl4,dl5)) IN database;
  
INSERT INTO zDepartments(DepartmentKey, dl1) values(1, 'Grand Forks');  
INSERT INTO zDepartments(DepartmentKey, dl1, dl2) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Variable');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Fixed');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Variable');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Main Shop');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Parts');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Car Wash');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Detail');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'PDQ');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Body Shop');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Fixed', 'Main Shop');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Fixed', 'Parts');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY2', 'Fixed', 'PDQ');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4, dl5) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Main Shop', 'Drive');
INSERT INTO zDepartments(DepartmentKey, dl1, dl2, dl3, dl4, dl5) values((select max(DepartmentKey) + 1 from zDepartments), 'Grand Forks', 'RY1', 'Fixed', 'Main Shop', 'Shop');

EXECUTE PROCEDURE sp_CreateIndex90( 
   'zDepartments','zDepartments.adi','NK','dl1;dl2;dl3;dl4;dl5',
   '',2051,512, '' ); 
   

CREATE TABLE zPositions (
  DepartmentKey cichar(24) CONSTRAINT NOT NULL,
  Position cichar(24) CONSTRAINT NOT NULL,
  ThruDate date CONSTRAINT NOT NULL,
  FromDate cichar(24) CONSTRAINT NOT NULL,
CONSTRAINT PK PRIMARY KEY (DepartmentKey, Position, ThruDate)) IN database;
--/> gregs session shit AND jons new dept/position shit ----------------------------