DO RI FROM service writers to ALL tables that include eeusername
IN each of those tables, eeusername must be not-nullable AND indexed

ServiceWriterMetrics to serviceWriterMetricData (any TABLE that has metric)
SELECT parent FROM system.columns WHERE name = 'metric'
DROP TABLE deptMetricData

-- tables with eeusername
ServiceWriters                                                                                                                                                                                          
-- WarrantyROs                                                                                                                                                                                             
---- ServiceWriterCheckouts no longer used                                                                                                                                                                                
-- WriterLandingPage                                                                                                                                                                                       
---- ServiceWriterStats no longer used                                                                                                                                                                                     
-- AgedROs                                                                                                                                                                                                 
-- AgedROUpdates                                                                                                                                                                                           
-- WarrantyCalls                                                                                                                                                                                           
ServiceWriterMetricData   

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'SW-WarrantyROs',
     'ServiceWriters', 
     'WarrantyROs', 
     'EEUSERNAME', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     '');   
                                                                                                                                                                           
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'SW-WriterLandingPage',
     'ServiceWriters', 
     'WriterLandingPage', 
     'EEUSERNAME', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 
     
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'SW-AgedROs',
     'ServiceWriters', 
     'AgedROs', 
     'EEUSERNAME', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     '');     
     
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'SW-WarrantyCalls',
     'ServiceWriters', 
     'WarrantyCalls', 
     'EEUSERNAME', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     '');     

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'SW-ServiceWriterMetricData',
     'ServiceWriters', 
     'ServiceWriterMetricData', 
     'EEUSERNAME', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     '');       
     
     
UPDATE ServiceWriters
SET eeusername = 'kespelund@rydellchev.com'
WHERE eeusername = 'abc';

UPDATE ServiceWriters
SET eeusername = 'dwiebusch@rydellchev.com'
WHERE eeusername = 'def';

UPDATE ServiceWriters
SET eeusername = 'mflikka@rydellchev.com'
WHERE eeusername = 'ghi';

UPDATE ServiceWriters
SET eeusername = 'kcarlson@rydellchev.com'
WHERE eeusername = 'jkl';     


-- metrics --------------------------------------------------------------------
ServiceWriterMetrics to serviceWriterMetricData (any TABLE that has metric)
SELECT parent FROM system.columns WHERE name = 'metric'
DROP TABLE deptMetricData

UPDATE serviceWriterMetrics SET storecode = 'RY1';

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'swMetrics-serviceWriterMetricData',
     'ServiceWriterMetrics', 
     'ServiceWriterMetricData', 
     'STORECODE-METRIC', 
     1, 
     2, 
     NULL/* Enter Fail table path here. */,
     '', 
     ''); 
     
UPDATE serviceWriterMetrics
SET metric = 'ROs Closed'
WHERE metric = 'rosClosed'     
