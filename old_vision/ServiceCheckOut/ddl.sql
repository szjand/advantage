CREATE TABLE ServiceWriters (
  StoreCode cichar(3),
  FullName cichar(40),
  LastName cichar(20),
  FirstName cichar(20),
  WriterNumber cichar(3),
  dtUserName cichar(10),
  EmployeeNumber cichar(7),
  Active logical,
  eeMemberID integer) IN database;
  
INSERT INTO ServiceWriters values(
  'RY1','Rodney Troftgruben','Troftgruben','Rodney','714','RYDERODNEY','1140870',true,0) 
  
INSERT INTO ServiceWriters values(
  'RY1','Travis Bursinger','Bursinger','Travis','720','RYDETRAVIS','121362',true,0)

INSERT INTO ServiceWriters values(
  'RY1','John Tyrrell','Tyrrell','John','705','RYDEJOHNTY','1142827',true,0)  
    
    
SELECT fullname FROM servicewriters    
      
    
CREATE TABLE LandingPage (
  StoreCode cichar(3),
  FullName cichar(40),
  WriterNumber cichar(30),
  eeMemberID integer,
  TheDate date,
  Cashiering double,
  AgedOpenROs integer,
  OpenWarrantyCalls integer,
  LaborSales money,
  ROsOpened integer,
  InspectionsRequested integer) IN database;
DELETE FROM landingpage;
INSERT INTO landingpage (storecode, fullname, writernumber, thedate)  
SELECT b.*, a.thedate
FROM dds.day a, (
  SELECT storecode, fullname, writernumber
  FROM servicewriters) b
WHERE thedate BETWEEN '03/17/2013' AND curdate() 
  
SELECT b.dayname, a.* FROM landingpage a 
inner join dds.day b on a.thedate = b.thedate
WHERE writernumber = '714'
      
  