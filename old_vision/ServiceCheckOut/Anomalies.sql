SELECT LEFT(executable, 25) AS exec, LEFT(proc,25) AS proc, 
  startts, endts, error
--  CASE WHEN proc = 'none' THEN timestampdiff(sql_tsi_second, startts, endts)/60.0 END 
FROM zproclog
WHERE executable <> 'hourly'
--  AND proc = 'none'
ORDER BY startts DESC


SELECT LEFT(executable, 25) AS exec, LEFT(proc,25) AS proc, 
  startts, endts, error
FROM zproclog
WHERE executable = 'serviceline'
--AND CAST(startts AS sql_date) IN ('03/27/2013','03/28/2013')
ORDER BY startts desc

SELECT LEFT(executable, 25), LEFT(proc,25) AS proc,
  min(timestampdiff(sql_tsi_second, startts, endts)/60.0),
  max(timestampdiff(sql_tsi_second, startts, endts)/60.0),
  avg(timestampdiff(sql_tsi_second, startts, endts)/60.0),
  COUNT(*)
FROM zproclog
WHERE executable <> 'hourly'
  AND CAST(endts AS sql_date) <> '12/31/9999'
  AND error IS NULL 
GROUP BY LEFT(executable, 25), LEFT(proc,25)
