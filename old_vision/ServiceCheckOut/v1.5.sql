--< Current WORK ------------------------------------------------------------------
/*
-- ADD to swMetricDataToday/Yesterday

Writers will be expected to UPDATE their current WORK, WHEN an OPEN ro IS older than
7 days, it becomes the managers repsonsibility to dog it
ON the 8th day, the row should move from CurrentROs to AgedROs ALONG WITH ANY UPDATE COMMENTS
made WHILE it was IN CurrentROs,
Currently, AgedRO updaes are stored IN a TABLE called AgedROUpdates
IS it worthwhile to change the name to something LIKE ROUpdates OR just CONTINUE to 
use the same TABLE though it IS semantically awkard
DO i change the TABLE AgedROUpdates

-- new data
      HTMLSnippets

-- new tables
      CurrentROs

-- new stored procs
      GetCurrentROs(eeUsername)
	  GetCurrentROUpdates(ro)

-- modified stored proccs	  
      GetRoToUpdate calls specifically to AgedROs TABLE
         UNIONED AgedROs with CurrentROs   		
	  ?? GetTasksForUser - IS this actually being used, we'll see, renamed to zUnused_xxx 	
	  InsertAgedROUpdate 
	     DROP PROCEDURE InsertAgedROUpdate;
		 CREATE PROCEDURE InsertROUpdate(ro,eeUserName)	  		 
	  swMetricDataToday: ADD delete/INSERT CurrentROs

*/

EXECUTE PROCEDURE sp_RenameDDObject('GetTasksForUser','zUnused_GetTasksForUser', 10, 0);

DELETE FROM HTMLSnippets;

INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Aged ROs', '<a class="button" href="#sco/agedros/<%username%>">View Aged ROs</a>', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'MainAction', NULL, '<div class="btn-group" id="servicestats">
  <a class="button" href="#sco/writerstats/<%username%>">Service Stats</a>
</div>
', NULL );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Warranty Calls', '<a class="button" href="#sco/warrantycalls/<%username%>">View Warranty Calls</a>', 3 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Aged ROs', '<a class="button" href="#sco/agedros/<%username%>">View Aged ROs</a>', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'NetPromoter', 'np', 'MainAction', NULL, '<div class="btn-group" id="netpromoter">
  <a class="button" href="#np/writer/<%username%>">Net Promoter</a>
</div>
', NULL );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'MainAction', NULL, '<div class="btn-group" id="servicestats">
  <a class="button" href="#sco/writerstats/<%username%>">Service Stats</a>
</div>
', NULL );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceMisc', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/ry2servicecalls"><span class="glyphicon glyphicon-pencil"></span>View Nissan Follow Up Calls<span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/writerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/currentros">View Current ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/agedros">View Aged ROs</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceManager', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">  <a class="mainlink" href="#sco/managerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/currentros">View Current ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/agedros">View Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/ry2servicecalls">View Nissan Follow Up Calls</a></span></li>
  </ul></nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceManager', 'Employees', 'emps', 'LeftNavBar', NULL, '<!-- Employees -->
<nav class="menu-item">
  <a class="mainlink employees" href="#emps/employeelist"><span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'NetPromoter', 'np', 'LeftNavBar', NULL, '<!-- Net Promoter -->
<nav class="menu-item">
  <a class="mainlink" href="#np/writersummary"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>
<ul class="submenu">
<li><span class="sublink"><a href="#np/aboutnp">About Net Promoter</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->
', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/writerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/currentros">View Current ROs</a></span></li>    
    <li><span class="sublink"><a href="#sco/agedros">View Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantycalls">View Warranty Calls</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceManager', 'Employees', 'emps', 'LeftNavBar', NULL, '<!-- Employees -->
<nav class="menu-item">
  <a class="mainlink employees" href="#emps/employeelist"><span class="glyphicon glyphicon-user"></span><em>Apps by Employee</em><span class="arrow-right"></span></a>
</nav>
<!-- .menu-item -->', 3 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceManager', 'NetPromoter', 'np', 'LeftNavBar', NULL, '<!-- Net Promoter -->
<nav class="menu-item">
  <a class="mainlink" href="#np/managersummary"><span class="glyphicon glyphicon-signal"></span>Net Promoter<span class="arrow-right"></span></a>
  <ul class="submenu">
<li><span class="sublink"><a href="#np/aboutnp">About Net Promoter</a></span></li>   
 <li><span class="sublink"><a href="#np/managersurveys">View Surveys</a></span></li>
  </ul>
</nav><!-- .menu-item -->
', 2 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceManager', 'ServiceCheckOut', 'sco', 'LeftNavBar', NULL, '<!-- Service Stats -->
<nav class="menu-item">
  <a class="mainlink" href="#sco/managerstats"><span class="glyphicon glyphicon-pencil"></span>Service Stats<span class="arrow-right"></span></a>
  <ul class="submenu">
    <li><span class="sublink"><a href="#sco/currentros">View Current ROs</a></span></li>    
    <li><span class="sublink"><a href="#sco/agedros">View Aged ROs</a></span></li>
    <li><span class="sublink"><a href="#sco/warrantycalls">View Warranty Calls</a></span></li>
  </ul>
</nav>
<!-- .menu-item -->', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY1', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'Current ROs', '<a class="button" href="#sco/currentros/<%username%>">View Current ROs</a>', 1 );
INSERT INTO "HTMLSnippets" VALUES( 'RY2', 'ServiceWriter', 'ServiceCheckOut', 'sco', 'SubAction', 'CurrentROs', '<a class="button" href="#sco/currentros/<%username%>">View Current ROs</a>', 1 );



CREATE TABLE CurrentROs ( 
      eeUserName CIChar( 50 ),
      ro CIChar( 9 ),
      openDate Date,
      customer CIChar( 50 ),
      RoStatus CIChar( 24 ),
      vin CIChar( 17 ),
      vehicle CIChar( 60 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CurrentROs',
   'CurrentROs.adi',
   'EEUSERNAME',
   'eeUserName',
   '',
   2,
   512,
   '' ); 
DELETE FROM CurrentROS;
INSERT INTO CurrentROs  
SELECT distinct e.eeusername, a.ro, a.thedate, b.fullname AS customername, 
  a.rostatus AS status, c.vin, TRIM(c.make) + ' ' + trim(c.model) AS vehicle        
FROM (
  SELECT a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey
  FROM dds.factRepairOrder a
  INNER JOIN dds.day b on a.opendatekey = b.datekey
    AND b.thedate > curdate() - 7
  INNER JOIN dds.dimRoStatus c on a.rostatuskey = c.rostatuskey
    AND c.rostatus <> 'Closed'
  GROUP BY a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey) a  
INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
  AND d.CensusDept = 'MR'
INNER JOIN ServiceWriters e on d.writernumber = e.writernumber
UNION     
SELECT distinct e.eeusername, a.ro, a.thedate, b.fullname AS customername, 
  a.rostatus AS status, c.vin, TRIM(c.make) + ' ' + trim(c.model) AS vehicle        
FROM (
  SELECT a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey
  FROM dds.TodayfactRepairOrder a
  INNER JOIN dds.day b on a.opendatekey = b.datekey
    AND b.thedate = curdate()
  INNER JOIN dds.dimRoStatus c on a.rostatuskey = c.rostatuskey
    AND c.rostatus <> 'Closed'
  GROUP BY a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey) a  
INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
  AND d.CensusDept = 'MR'
INNER JOIN ServiceWriters e on d.writernumber = e.writernumber;  


CREATE PROCEDURE GetCurrentROs
   ( 
      eeUsername CICHAR ( 50 ),
      firstName CICHAR ( 20 ) OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      openDate CICHAR ( 8 ) OUTPUT,
      customer CICHAR ( 50 ) OUTPUT,
      roStatus CICHAR ( 24 ) OUTPUT,
      vin CICHAR ( 17 ) OUTPUT,
      vehicle CICHAR ( 60 ) OUTPUT,
      statusUpdate Memo OUTPUT,
      theDate DATE OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getCurrentros('rodt@rydellchev.com');
EXECUTE PROCEDURE getCurrentros('dpederson@gfhonda.com');
EXECUTE PROCEDURE getCurrentros('mhuot@rydellchev.com');
EXECUTE PROCEDURE getCurrentros('aneumann@gfhonda.com');
*/
DECLARE @id string;
@id = (SELECT eeUsername FROM __input);
INSERT INTO __output
SELECT b.firstname, a.ro, c.mmddyy AS opendate, a.customer, a.rostatus, a.vin, a.vehicle, 
  d.statusUpdate, openDate
FROM CurrentRos a
INNER JOIN ServiceWriters b ON a.eeusername = b.eeusername
  AND CASE
    WHEN @id IN ('mhuot@rydellchev.com','aberry@rydellchev.com') THEN b.storecode = 'RY1'
    WHEN @id = 'aneumann@gfhonda.com' THEN b.storecode = 'RY2'
    ELSE b.eeusername = @id 
  END 
LEFT JOIN dds.day c ON a.opendate = c.thedate
LEFT JOIN (
  SELECT *
  FROM AgedROUpdates x
  WHERE updateTS = (
    SELECT MAX(updatets)
    FROM AgedRoUpdates
    WHERE ro = x.ro)) d ON a.ro = d.ro; 
END;

ALTER PROCEDURE GetROToUpdate
   ( 
      ro CICHAR ( 9 ),
      openDate DATE OUTPUT,
      customer CICHAR ( 50 ) OUTPUT,
      roStatus CICHAR ( 24 ) OUTPUT,
      vin CICHAR ( 17 ) OUTPUT,
      vehicle CICHAR ( 60 ) OUTPUT,
      updatets TIMESTAMP OUTPUT,
      updateBy CICHAR ( 50 ) OUTPUT,
      statusUpdate Memo OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getrotoupdate('2687217');
*/
DECLARE @ro string;
@ro = (SELECT ro FROM __input);
INSERT INTO __output    
SELECT a.opendate, a.customer, a.rostatus, a.vin, a.vehicle, 
   b.updatets, b.eeusername, b.statusUpdate
FROM (
 SELECT *
 FROM AgedRos 
 UNION 
 SELECT *
 FROM CurrentROs) a
LEFT JOIN AgedROUpdates  b ON a.ro = b.ro 
WHERE a.ro = @ro;

END;

DROP PROCEDURE InsertAgedROUpdate;
CREATE PROCEDURE InsertROUpdate
   ( 
      ro CICHAR ( 9 ),
      eeUserName CICHAR ( 50 ),
      statusUpdate Memo
   ) 
BEGIN 
/*
EXECUTE PROCEDURE InsertROUpdate()
*/
INSERT INTO AgedRoUpdates values(
  (SELECT ro FROM __input),
  now(),
  (SELECT eeUserName FROM __input), 
  (SELECT statusUpdate FROM __input));

END;


CREATE PROCEDURE GetCurrentROUpdates
   ( 
      ro CICHAR ( 9 ),
      updateTS CICHAR ( 20 ) OUTPUT,
      eeUserName CICHAR ( 50 ) OUTPUT,
      statusUpdate Memo OUTPUT
   ) 
BEGIN 
/*

*/
DECLARE @ro cichar(9);
@ro = (SELECT ro FROM __input);
  INSERT INTO __output
  SELECT top 100
    TRIM(c.mmdd) 
    + ' ' + 
    trim(
      CASE 
        WHEN hour(updateTS) > 12 THEN 
          trim(cast(hour(updateTS) - 12 AS sql_char))
        ELSE trim(cast(hour(updateTS) AS sql_char))
      END 
      + ':' +
      CASE
        WHEN minute(updateTS) < 10 THEN
          '0' + cast(minute(updateTS) AS sql_char)
        else cast(minute(updateTS) AS sql_char)
      END) 
    + ' ' +  
    CASE WHEN hour(updateTS) > 12 THEN 'PM' ELSE 'AM' END,   
    TRIM(b.firstname) + ' ' +  LEFT(b.lastname, 1) AS eeusername, 
    statusUpdate
  FROM AgedROUpdates a
  LEFT JOIN people b ON a.eeusername = b.eeusername
  LEFT JOIN dds.day c ON CAST(a.updateTS AS sql_date) = c.thedate
  WHERE ro = @ro
  ORDER BY updateTS desc;


END;

alter PROCEDURE swMetricDataToday
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE swMetricDataToday();
4/13 rosClosedWithInspectionLine: changed to 23I only
     populate TABLE WarrantyROs prior to UPDATE serviceWriterMetricData.OPEN Warranty Calls
5/4
  return a 0 instead of NULL for OPEN warranty calls     
6/8
  modified for ry2  
    include opcode 24z for ry2 inspections
    exclude cashiering, warranty calls for ry2
    ADD HondaNissanCalls for ry2  
9/14
  replace calls to factROToday/LineToday to todayFactRepairOrder  
10/2
  v1.5, added CurrentROs       
*/
TRY 
  BEGIN TRANSACTION;
  TRY
    TRY  
    -- ROs Closed -------------------------------------------------------------   
      DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() AND metric = 'ROs Closed';
      INSERT INTO ServiceWriterMetricData  
      SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'ROs Closed', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM ( -- 1 row per writer w/# of ros
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
          GROUP BY a.ro, a.servicewriterkey) c
        GROUP BY servicewriterkey) d  
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();        
      -- ROs Closed -----------------------------------------------------------------  
      -- weekly -------------------
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'ROs Closed') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'ROs Closed';   
    CATCH ALL
      RAISE swMetricDataToday(100, __errtext);
    END TRY;
    TRY     
    -- rosClosedWithInspectionLine ------------------------------------------------ 
      DELETE FROM ServiceWriterMetricData WHERE thedate = curdate() AND metric = 'rosClosedWithInspectionLine';  
      INSERT INTO ServiceWriterMetricData     
      SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosClosedWithInspectionLine', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM ( -- 1 row per writer w/# of ros
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          INNER JOIN dds.dimOpcode b on a.opcodekey = b.opcodekey
            AND b.opcode IN ('23I', '24Z')
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
          GROUP BY a.ro, a.servicewriterkey) c
        GROUP BY servicewriterkey) d  
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();        
    CATCH ALL
      RAISE swMetricDataToday(101, __errtext);  
    END TRY;  
    TRY
    -- rosCashiered ---------------------------------------------------------------
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashiered' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData         
      SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosCashiered', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM (        
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
            AND a.storecode = 'RY1'
            AND NOT EXISTS (
              SELECT 1
              FROM dds.todayFactRepairOrder m
              INNER JOIN dds.dimPaymentType n on m.paymenttypekey = n.paymenttypekey
              WHERE ro = a.ro
                AND n.paymenttypecode IN ('w','i'))
            AND NOT EXISTS (
              SELECT 1
              FROM dds.todayFactRepairOrder o
              INNER JOIN dds.dimservicetype p on o.servicetypekey = p.servicetypekey
              WHERE ro = a.ro
                AND p.servicetypecode <> 'mr')          
            AND EXISTS (
              SELECT 1
              FROM tmpGLPTRNS -- generated BY scoToday.tmpGLPTRNS 
              WHERE gtdoc# = a.ro)
          GROUP BY a.ro, a.servicewriterkey) c   
        GROUP BY servicewriterkey) d
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();
    CATCH ALL
      RAISE swMetricDataToday(102, __errtext);  
    END TRY;      
    TRY 
    -- rosCashieredByWriter -------------------------------------------------------
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosCashieredByWriter' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData      
      SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosCashieredByWriter', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      --SELECT f.lastname, howmany
      FROM (
        SELECT servicewriterkey, coalesce(COUNT(*), 0) AS howmany
        FROM ( -- 1 row per ro/writer 
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          INNER JOIN tmpGLPTRNS b on a.ro = b.gtdoc#
          INNER JOIN tmpGLPDTIM c on b.gttrn# = c.gqtrn#
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
            AND a.storecode = 'RY1'
            AND NOT EXISTS (
              SELECT 1
              FROM dds.todayFactRepairOrder m
              INNER JOIN dds.dimPaymentType n on m.paymenttypekey = n.paymenttypekey
              WHERE ro = a.ro
                AND n.paymenttypecode IN ('w','i'))
            AND NOT EXISTS (
              SELECT 1
              FROM dds.todayFactRepairOrder o
              INNER JOIN dds.dimservicetype p on o.servicetypekey = p.servicetypekey
              WHERE ro = a.ro
                AND p.servicetypecode <> 'mr')
            AND c.gquser IN (SELECT dtUsername FROM ServiceWriters)
          GROUP BY a.ro, a.servicewriterkey) c 
        GROUP BY servicewriterkey) d 
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.CensusDept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();        
    CATCH ALL
      RAISE swMetricDataToday(103, __errtext);  
    END TRY;    
    TRY  
    -- rosClosedWithFlagHours -----------------------------------------------------
      DELETE FROM ServiceWriterMetricData WHERE metric = 'rosClosedWithFlagHours' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData    
      SELECT f.eeusername, f.storecode, f.fullname, f.lastname, f.firstname,
        f.writernumber, g.datekey, g.thedate, g.dayofweek, g.dayname,
        g.dayofmonth, g.sundaytosaturdayweek, 'rosClosedWithFlagHours', 0, howmany AS today,
        0, trim(CAST(howmany AS sql_char)), CAST(NULL AS sql_char)
      FROM ( -- 1 row per writer w/# of ros
        SELECT servicewriterkey, COUNT(*) AS howmany
        FROM ( -- 1 row per ro/writer    
          SELECT a.ro, a.servicewriterkey
          FROM dds.todayFactRepairOrder a
          WHERE finalclosedatekey = (SELECT datekey FROM dds.day WHERE thedate = curdate())
            AND roFlagHours > 0
          GROUP BY a.ro, a.servicewriterkey) c
        GROUP BY servicewriterkey) d  
      INNER JOIN dds.dimServiceWriter e on d.servicewriterkey = e.servicewriterkey 
        AND e.censusdept = 'MR'
      INNER JOIN ServiceWriters f on e.storecode = f.storecode AND e.writernumber = f.writernumber 
      INNER JOIN dds.day g on g.thedate = curdate();   
    -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'rosClosedWithFlagHours') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric; 
    CATCH ALL
      RAISE swMetricDataToday(104, __errtext);  
    END TRY;        
    -- flaghours ------------------------------------------------------------------
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'flagHours' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData    
      SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'flagHours', 0 AS seq, 
        a.flaghours AS today, 0 AS thisweek, 
        trim(cast(a.flaghours AS sql_char)), CAST(NULL AS sql_char)     
      FROM (
        SELECT a.servicewriterkey, SUM(a.FlagHours) AS FlagHours
        FROM dds.todayFactRepairOrder a
        INNER JOIN dds.day b on a.FinalCloseDateKey = b.DateKey
          AND b.theDate = curdate()
        GROUP BY a.ServiceWriterKey) a
      INNER JOIN dds.day b on b.thedate = curdate()
      INNER JOIN dds.dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
        AND c.CensusDept = 'MR'
      INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber;        
      -- CREATE weekly running total
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE a.metric = 'flagHours') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric; 
    CATCH ALL
      RAISE swMetricDataToday(105, __errtext);  
    END TRY;    
    TRY    
    -- Cashiering -----------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosCashieredByWriter & 
      ServiceWriterMetricData.rosCashiered to be complete
      generate weekly totals before generating display metric (cashiering)
    */
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric in ('rosCashiered','rosCashieredByWriter')) x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN ('rosCashiered','rosCashieredByWriter') ; 
        
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Cashiering';
      INSERT INTO ServiceWriterMetricData
      SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Cashiering', 1 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosCashieredByWriter'
        AND b.metric = 'rosCashiered';
    CATCH ALL
      RAISE swMetricDataToday(106, __errtext);  
    END TRY;     
    -- Open Aged ROs --------------------------------------------------------------------
    /*
    UPDATE throughout the day IN terms of removing ros that get closed
    DELETE FROM agedros daily based ON an ro closing
    overnight a full scrap
    AgedROs TABLE IS necessary for the nightly/today to run
    
    no need to UPDATE with newly aged ros thru out the day
    
    need to UPDATE the weekly numbers here
    ALL i need are this week, which will be the same AS the daily total each day
    */
    TRY 
      DELETE
      FROM agedros 
      WHERE EXISTS (
        SELECT 1
        -- FROM factROToday
        FROM dds.todayFactRepairOrder
        WHERE ro = agedros.ro
        AND finalclosedatekey <> 7306);
        
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Aged ROs' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData        
      SELECT c.eeUserName, c.storecode, c.fullname, c.lastname, c.firstname,
        c.writerNumber, a.datekey, a.thedate, a.dayOfWeek, a.dayName,
        a.dayOfMonth, a.sundayToSaturdayWeek, 'Open Aged ROs', 2 AS seq, b.howmany AS today,
      --  0 AS thisWeek, trim(CAST(b.howmany AS sql_char)), CAST(NULL AS sql_char)
        howmany thisWeek, trim(CAST(howmany AS sql_char)) AS todayDisplay, 
        trim(CAST(howmany AS sql_char)) AS thisWeekDisplay
      FROM dds.day a, ( 
        SELECT eeusername, COUNT(*) AS howmany
        FROM agedros  
        GROUP BY eeusername) b, 
        servicewriters c
      WHERE a.thedate = curdate()
        AND b.eeusername = c.eeusername;  
    CATCH ALL
      RAISE swMetricDataToday(107, __errtext);  
    END TRY;        
    -- Open Warranty Calls --------------------------------------------------------
    /*
      the challenges here, like AgedROs, the data depends ON dds.factro, 
        sco.factrotoday & sco.WarrantyROs
        backfill FROM dds.factro
    */
    TRY  
      INSERT INTO WarrantyROS 
      SELECT distinct d.eeUserName, a.ro, b.thedate, e.FullName, e.HomePhone, e.BusinessPhone,
        e.CellPhone, f.Vin, TRIM(f.Make) + ' ' + TRIM(f.model), False
      FROM dds.todayFactRepairOrder a
      INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
        AND b.thedate = curdate()
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'  
      INNER JOIN ServiceWriters d on c.WriterNumber = d.WriterNumber 
      INNER JOIN dds.dimCustomer e on a.CustomerKey = e.CustomerKey 
      INNER JOIN dds.dimVehicle f on a.VehicleKey = f.VehicleKey
      WHERE a.StoreCode = 'RY1'
        AND EXISTS (
          SELECT 1
          FROM dds.todayFactRepairOrder r
          INNER JOIN dds.dimPaymentType s on r.PaymentTypeKey = s.PaymentTypeKey
            AND s.PaymentTypeCode = 'W'
          WHERE ro = a.ro) 
        AND NOT EXISTS (
          SELECT 1
          FROM WarrantyROs 
          WHERE ro = a.RO);           
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Open Warranty Calls' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData      
      SELECT a.eeUserName, a.storecode, a.fullname, a.lastname, a.firstname,
        a.writerNumber, c.datekey, c.thedate, c.dayOfWeek, c.dayName,
        c.dayOfMonth, c.sundayToSaturdayWeek, 'Open Warranty Calls', 3 AS seq, coalesce(b.howmany, 0) AS today,
        coalesce(b.howmany, 0) AS thisWeek, trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS todayDisplay, 
        trim(CAST(coalesce(b.howmany, 0) AS sql_char)) AS thisWeekDisplay
      FROM servicewriters a  
      LEFT JOIN (
        SELECT eeusername, COUNT(*) AS howmany
        FROM warrantyROs 
        WHERE followUpComplete = false
        GROUP BY eeusername) b ON a.eeusername = b.eeusername
      LEFT JOIN dds.day c ON 1 = 1
        AND c.thedate = curdate()  
      WHERE a.active = true
        AND a.storecode = 'ry1'; -- ry2 mod
    CATCH ALL
      RAISE swMetricDataToday(108, __errtext);  
    END TRY;       
    -- Avg Hours/RO ---------------------------------------------------------------
    /*
      requires ServiceWriterMetricData.rosClosedWithFlagHours & ServiceWriterMetricData.flagHours to be complete
      different than cashiering AND insp requested, those weekly numbers are based ON discreet
      values that get updated at the END, a simple concantenation of the discreet values
      this IS a ratio
    */
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Avg Hours/RO';
      INSERT INTO ServiceWriterMetricData
      SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Avg Hours/RO', 4 AS seq, 
        round(a.today/b.today, 1) AS today,
        round(a.thisweek/b.thisweek, 1) AS thisweek,  
        left(CAST(cast(round(a.today/b.today, 1) as sql_double) AS sql_char), 12)  AS todaydisplay,
        left(cast(CAST(round(a.thisweek/b.thisweek, 1) AS sql_double) AS sql_char), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
        AND a.thedate = b.thedate
      WHERE a.metric = 'flagHours'
        AND b.metric = 'rosClosedWithFlagHours';
    CATCH ALL
      RAISE swMetricDataToday(109, __errtext);      
    END TRY;     
    -- LaborSales -----------------------------------------------------------------
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Labor Sales' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData     
      SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'Labor Sales', 5 AS seq, 
        round(cast(abs(a.laborsales) AS sql_double), 0) AS today,
        0, 
        trim(cast(coalesce(abs(round(CAST(a.laborsales AS sql_double), 0)), 0) AS sql_char)) AS todayDisplay, 
        CAST(NULL AS sql_char)
      FROM (        
        SELECT a.ServiceWriterKey, SUM(LaborSales) AS LaborSales
        FROM dds.todayFactRepairOrder a
        INNER JOIN dds.day b on a.FinalCloseDateKey = b.datekey
          AND b.thedate = curdate()
        GROUP BY a.ServiceWriterKey) a 
      INNER JOIN dds.day b on b.thedate = curdate()
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'
      INNER JOIN ServiceWriters d on c.StoreCode = d.StoreCode
        and c.WriterNumber = d.WriterNumber;         
    CATCH ALL
      RAISE swMetricDataToday(110, __errtext);      
    END TRY;     
    -- rosOpened ------------------------------------------------------------------  
    TRY 
      DELETE FROM ServiceWriterMetricData WHERE metric = 'ROs Opened' AND thedate = curdate();
      INSERT INTO ServiceWriterMetricData   
      SELECT d.eeUserName, d.storecode, d.fullname, d.lastname, d.firstname,
        d.writerNumber, b.datekey, b.thedate, b.dayOfWeek, b.dayName,
        b.dayOfMonth, b.sundayToSaturdayWeek, 'ROs Opened', 6 AS seq, a.HowMany AS today,
        0 AS thisWeek, trim(CAST(a.HowMany AS sql_char)), CAST(NULL AS sql_char)  
      FROM (      
        SELECT ServiceWriterKey, COUNT(*) AS HowMany
        FROM (  
          SELECT a.ServiceWriterKey, a.ro
          FROM dds.todayFactRepairOrder a
          INNER JOIN dds.day b on a.OpenDateKey = b.datekey
            AND b.thedate = curdate()
          GROUP BY a.ServiceWriterKey, a.ro) x
        GROUP BY ServiceWriterKey) a 
      INNER JOIN dds.day b on b.thedate = curdatE()
      INNER JOIN dds.dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
        AND c.CensusDept = 'MR'
      INNER JOIN ServiceWriters d on c.StoreCode = d.StoreCode
        and c.WriterNumber = d.WriterNumber;        
    CATCH ALL
      RAISE swMetricDataToday(111, __errtext);      
    END TRY;         
    -- Inspections Requested ------------------------------------------------------
    /*
      requires ServiceWriterMetricData.ROs Closed & 
      ServiceWriterMetricData.rosClosedWithInspectionLine to be complete
      generate weekly totals before generating display metric (cashiering)
    */
    TRY 
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE metric = 'rosClosedWithInspectionLine') x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric = 'rosClosedWithInspectionLine'; 
      
      DELETE FROM ServiceWriterMetricData WHERE metric = 'Inspections Requested';
      INSERT INTO ServiceWriterMetricData
      SELECT a.eeusername, a.storecode, a.fullname, a.lastname, a.firstname, 
        a.writernumber, a.datekey, a.thedate, a.dayofweek, a.dayname, a.dayofmonth,
        a.sundaytosaturdayweek, 'Inspections Requested', 7 AS seq, a.today, a.thisweek, 
        left(trim(a.todaydisplay) + ' / ' + trim(b.todaydisplay), 12) AS todaydisplay, 
        left(trim(a.thisweekdisplay) + ' / ' + trim(b.thisweekdisplay), 12) AS thisweekdisplay
      FROM ServiceWriterMetricData a
      INNER JOIN ServiceWriterMetricData b ON a.eeusername = b.eeusername
        AND a.thedate = b.thedate
      WHERE a.metric = 'rosClosedWithInspectionLine'
        AND b.metric = 'ROs Closed';
    CATCH ALL
      RAISE swMetricDataToday(112, __errtext);      
    END TRY;         

    -- UPDATE cumulative metrics only with weekly totals
    TRY 
      UPDATE ServiceWriterMetricData
        SET thisweek = x.thisweek             
      FROM (   
        SELECT eeusername, thedate, sundaytosaturdayweek, today, metric,
          today + coalesce((
            SELECT SUM(today)
            FROM ServiceWriterMetricData b
            WHERE b.eeusername = a.eeusername
              AND b.sundaytosaturdayweek = a.sundaytosaturdayweek
              AND b.metric = a.metric
              AND b.thedate < a.thedate),0) AS thisweek
        FROM ServiceWriterMetricData a
        WHERE EXISTS (
          SELECT 1
          FROM ServiceWriterMetrics
          WHERE metric = a.metric
            AND cumulative = true)) x
      WHERE ServiceWriterMetricData.eeusername = x.eeusername
        AND ServiceWriterMetricData.thedate = x.thedate
        AND ServiceWriterMetricData.metric = x.metric;  
      
      UPDATE ServiceWriterMetricData
        SET thisWeekDisplay =  TRIM(CAST(cast(thisWeek AS sql_integer) AS sql_char))
      WHERE metric IN (
        SELECT metric
        FROM servicewritermetrics
        WHERE cumulative = true)  ; 
    CATCH ALL
      RAISE swMetricDataToday(113, __errtext);      
    END TRY;
	
	TRY 
      DELETE FROM CurrentROS;
      INSERT INTO CurrentROs  
      SELECT distinct e.eeusername, a.ro, a.thedate, b.fullname AS customername, 
        a.rostatus AS status, c.vin, TRIM(c.make) + ' ' + trim(c.model) AS vehicle        
      FROM (
        SELECT a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey
        FROM dds.factRepairOrder a
        INNER JOIN dds.day b on a.opendatekey = b.datekey
          AND b.thedate > curdate() - 7
        INNER JOIN dds.dimRoStatus c on a.rostatuskey = c.rostatuskey
          AND c.rostatus <> 'Closed'
        GROUP BY a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey) a  
      INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
      INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
      INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
        AND d.CensusDept = 'MR'
      INNER JOIN ServiceWriters e on d.writernumber = e.writernumber
      UNION     
      SELECT distinct e.eeusername, a.ro, a.thedate, b.fullname AS customername, 
        a.rostatus AS status, c.vin, TRIM(c.make) + ' ' + trim(c.model) AS vehicle        
      FROM (
        SELECT a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey
        FROM dds.TodayfactRepairOrder a
        INNER JOIN dds.day b on a.opendatekey = b.datekey
          AND b.thedate = curdate()
        INNER JOIN dds.dimRoStatus c on a.rostatuskey = c.rostatuskey
          AND c.rostatus <> 'Closed'
        GROUP BY a.ro, b.thedate, a.customerkey, c.rostatus, a.vehiclekey, a.servicewriterkey) a  
      INNER JOIN dds.dimCustomer b on a.customerkey = b.customerkey
      INNER JOIN dds.dimVehicle c on a.vehiclekey = c.vehiclekey  
      INNER JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
        AND d.CensusDept = 'MR'
      INNER JOIN ServiceWriters e on d.writernumber = e.writernumber;  	
    CATCH ALL
      RAISE swMetricDataToday(114, __errtext);      
    END TRY;
	    
  COMMIT WORK;
  CATCH ALL
    ROLLBACK WORK;
    RAISE;
  END TRY;  
CATCH ALL
  RAISE;
END TRY;    




END;



--/> Current WORK -------------------------------------------------------------------	