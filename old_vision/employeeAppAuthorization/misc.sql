/*
Could you give Josh H and Wyatt Thompson access to the team numbers like Jeff and Shawn.

07/24/20
*/
SELECT * 
FROM employeeappauthorization
WHERE username LIKE 'sanderson%'
  AND appname = 'teampay'
  
SELECT * 
FROM employeeappauthorization
WHERE username LIKE 'wthompso%'
  AND appname = 'teampay'  
 
SELECT * FROM tpemployees WHERE username = 'wthompson@rydellchev.com'

  
DELETE 
FROM employeeappauthorization
WHERE username = 'jheffernan@rydellchev.com'
  AND appname = 'teampay';
    
INSERT INTO employeeAppAuthorization
select 'jheffernan@rydellchev.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeAppAuthorization
WHERE username LIKE 'sanderson%'
  AND appname = 'teampay';

DELETE 
FROM employeeappauthorization
WHERE username = 'wthompson@rydellchev.com'
  AND appname = 'teampay';
    
INSERT INTO employeeAppAuthorization
select 'wthompson@rydellchev.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeAppAuthorization
WHERE username LIKE 'sanderson%'
  AND appname = 'teampay';


  
-- usernames with no access IN employeeAppAuthorization
SELECT *
FROM tpemployees a
WHERE NOT EXISTS (
  SELECT 1
  FROM employeeAppAuthorization
  WHERE username = a.username)
    
DELETE FROM tpemployees
WHERE username IN ('akempert@rydellcars.com','aiverson@rydellchev.com')  

SELECT appName, appSeq, appCode, appRole, functionality, 1

-- give everyone that doesn't already have it access to customer feedback
INSERT INTO employeeAppAuthorization  
SELECT a.username, b.*
FROM (  
  SELECT distinct username
  FROM employeeAppAuthorization a
  WHERE NOT EXISTS (
    SELECT 1
    FROM employeeAppAuthorization
    WHERE username = a.username
      AND appcode = 'cstfb')) a,   
(
  SELECT appName, appSeq, appCode, appRole, functionality, 1
  FROM applicationMetaData
  WHERE appcode = 'cstfb') b	 

-- 11/15 bobby pearson needs net promoter, email collection, dfm  

INSERT INTO employeeAppAuthorization  
SELECT 'rpearson@rydellcars.com', appName, appSeq, appCode, appRole, functionality, 1
  FROM applicationMetaData
  WHERE appcode in ('cstfb', 'dig')
  
--11/23
-- per kathy lind, no one at honda sales has access to cstfb
SELECT a.employeenumber, a.username, b.position, c.active, d.*
FROM ptoemployees a
INNER JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
LEFT JOIN employeeAppAuthorization d on a.username = d.username  
LEFT JOIN tpemployees e on a.username = e.username
WHERE b.department = 'RY2 Sales'  
  AND b.position <> 'Reception'

INSERT INTO employeeAppAuthorization    
SELECT r.username, s.*
FROM (
  SELECT a.employeenumber, a.username, b.position, c.active
  FROM ptoemployees a
  INNER JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
  LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
    AND c.currentrow = true
  WHERE b.department = 'RY2 Sales'  
    AND b.position <> 'Reception'
    AND NOT EXISTS (
      SELECT 1
      FROM employeeAppAuthorization
      WHERE username = a.username
        AND appCode = 'cstfb')) r
LEFT JOIN (
  SELECT appName, appSeq, appCode, appRole, functionality, 1
  FROM applicationMetaData
  WHERE appcode = 'cstfb') s on 1 = 1       
    
    
SELECT e.firstname,e.lastname, e.username, e.password
FROM ptoemployees a
INNER JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
LEFT JOIN employeeAppAuthorization d on a.username = d.username  
LEFT JOIN tpemployees e on a.username = e.username
WHERE b.department = 'RY2 Sales'  
  AND b.position <> 'Reception'    
GROUP BY e.firstname,e.lastname, e.username, e.password  

-- 2/19/17 remove access to body shop team pay for loren shereck

SELECT *
FROM employeeAppAuthorization
WHERE username LIKE 'lsher%'

DELETE 
FROM employeeAppAuthorization
WHERE username = 'lshereck@rydellcars.com'
  AND appname = 'teampay'
  AND approle = 'teamlead'
  
  
SELECT *
FROM employeeAppAuthorization
WHERE username LIKE 'aneuma%'  

-- give nick neumann access to email capture

SELECT * FROM tpemployees WHERE lastname = 'neumann'

nneumann@rydellcars.com / Skoal311

INSERT INTO employeeAppAuthorization
select 'nneumann@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeAppAuthorization
WHERE username = 'aneumann@rydellcars.com'
  AND appcode = 'cstfb'
  AND functionality IN ('dropdown','email capture splash','about email capture')