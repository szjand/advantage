DELETE 
-- select *
FROM employeeappauthorization
WHERE username IN ( 'devavold@rydellcars.com','aostlund@rydellcars.com',
      'dlueker@rydellcars.com','msteinke@rydellcars.com',
      'dmarek@rydellcars.com','bhill@rydellcars.com')
  AND appname = 'teampay'
  AND appseq = 101
  AND approle = 'supervisor'
  AND functionality = 'summary';
  
delete
FROM employeeAppAuthorization
WHERE username IN ('kblumhagen@rydellcars.com')
  AND appname = 'teampay'
  AND appseq = 101
  AND approle = 'parts'
  AND functionality = 'summary';  

DELETE 
-- select *
FROM employeeAppAuthorization
WHERE username IN ('lshereck@rydellcars.com')
  AND appname = 'teampay'
  AND appseq = 101
  AND approle = 'foreman'
  AND functionality = 'summary';
  