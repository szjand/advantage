/*
Hey guys,

Can we get Lauren Nelson access to view CSI calls on the �old� Vision page.  They are listed under �Writer Stats / Warranty Calls�.  Sorry I couldn�t remember who takes care of this so I figured I would email you both just in case.

Thanks,

Justin Kilmer
Rydell Customer Care
5/11/20

select *
FROM employeeappauthorization
WHERE username = 'jgoulet@rydellcars.com'

select *
FROM employeeappauthorization
WHERE username = 'lnelson@rydellcars.com'

INSERT INTO employeeappauthorization
SELECT 'lnelson@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'jgoulet@rydellcars.com'
  AND appcode = 'sco'; 
  
SELECT * FROM tpemployees WHERE username = 'lnelson@rydellcars.com'  
*/



/*
02/05/20
Can we get Cole Thompson set up on the old Vision Page.  If you need someones access to mirror go with Jennifer Goulet.  I basically need him to be able to access �Writer Stats�  --? Warranty calls.
 
Thank you,
 
Justin Kilmer
/*
select *
FROM employeeappauthorization
WHERE username = 'jgoulet@rydellcars.com'

select *
FROM employeeappauthorization
WHERE username = 'cthompson@rydellcars.com'

INSERT INTO employeeappauthorization
SELECT 'cthompson@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'jgoulet@rydellcars.com'
  AND appcode = 'sco'; 


/*
Hey Jon,
I need to get my new employee Eric Palacios hooked up with the Old Vision page, 
and he needs to ability to view the �Writer Stats� and view the warranty calls (CSI Calls).  
If you need the name of a specific employee to mirror, have it mirror Amanda Sundby.  
She has the ability to view this on her Vision page.
*/

select *
FROM employeeappauthorization
WHERE username = 'asundby@rydellcars.com'

select *
FROM employeeappauthorization
WHERE username = 'epalacios@rydellcars.com'

INSERT INTO employeeappauthorization
SELECT 'epalacios@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'asundby@rydellcars.com'
  AND appcode = 'sco'; 

/*
Hey Jon,

Can you give Ana Gagnon access to view the CSI warranty calls on the vision page.  
She will be making those calls for us.

Thanks,

Justin Kilmer
*/
-- 1/30/19
select *
FROM employeeappauthorization
WHERE username = 'agagnon@rydellcars.com'

INSERT INTO employeeappauthorization
SELECT 'agagnon@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'emilee@rydellcars.com'
  AND appcode = 'sco'; 

/*
Hey Jon,

Can you please set up Mayhayla Knowles and Lori Payne with the ability to 
see Warranty Calls under the Write Stats on the Vision page

Thanks

Justin Kilmer
*/
select *
FROM tpemployees
WHERE lastname LIKE 'woi%'

select *
FROM employeeappauthorization
WHERE username = 'emilee@rydellcars.com'


select *
FROM tpemployees
WHERE lastname LIKE 'know%'

select *
FROM employeeappauthorization
WHERE username = 'mknowles@rydellcars.com'


select *
FROM tpemployees
WHERE lastname LIKE 'pay%'

select *
FROM employeeappauthorization
WHERE username = 'lpayne@rydellcars.com'

INSERT INTO employeeappauthorization
SELECT 'mknowles@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'emilee@rydellcars.com'
  AND appcode = 'sco';
  
INSERT INTO employeeappauthorization
SELECT 'lpayne@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'emilee@rydellcars.com'
  AND appcode = 'sco';  
  
  
SELECT *
FROM tpemployees  
WHERE username = 'lpayne@rydellcars.com'