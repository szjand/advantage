INSERT INTO applicationRoles (appName, role, appCode)
values ('teampay','estimator','tp');
/*
SELECT *
FROM applicationMetaData
WHERE appCode = 'tp'
*/
INSERT INTO applicationMetaData values (
  'teampay', -- appName
  'tp', -- appCode,
  101,-- appSeq
  'estimator', -- appRole
  'dropdown', -- functionality
  'mainlink_bodyshop_est_estimator_html',-- configMapMarkupKey
  '',-- pubSub
  'leftnavbar',-- navType
  'estimator (app header)',-- navText
  10-- navSeq
  );
  
INSERT INTO applicationMetaData values (
  'teampay', -- appName
  'tp', -- appCode,
  101,-- appSeq
  'estimator', -- appRole
  'estimator summary', -- functionality
  'sublink_bodyshop_est_estimator_summary_html',-- configMapMarkupKey
  'btp.estimator.summary',-- pubSub
  'leftnavbar',-- navType
  'estimator summary',-- navText
  20-- navSeq
  );  
  
INSERT INTO applicationMetaData values (
  'teampay', -- appName
  'tp', -- appCode,
  101,-- appSeq
  'manager', -- appRole
  'estimator summary', -- functionality
  'sublink_bodyshop_est_mgr_summary_html',-- configMapMarkupKey
  'btp.manager.summary',-- pubSub
  'leftnavbar',-- navType
  'estimator summary',-- navText
  85-- navSeq
  );    
/* 
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 13
FROM tpEmployees a, applicationMetaData b
WHERE a.username = 'rsattler@rydellchev.com'
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'estimator summary'
  AND b.navSeq = 85;
  
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 13
FROM tpEmployees a, applicationMetaData b
WHERE a.username = 'mhuot@rydellchev.com'
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'estimator summary'
  AND b.navSeq = 85;
*/  
  
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 13
FROM tpEmployees a, applicationMetaData b
WHERE (
  a.username = 'rsattler@rydellchev.com' OR
  a.username = 'mhuot@rydellchev.com' or
  firstname = 'trent' OR
  firstname = 'jeri' OR
  (firstname = 'benjamin' AND lastname <> 'dalen') OR
  username = 'brian@rydellchev.com' OR
  firstname = 'andrew' or
  lastname = 'steinke' OR
  lastname = 'espelund' OR
  lastname = 'sorum')
  AND b.appcode = 'tp'
  AND b.approle = 'manager' 
  AND b.functionality = 'estimator summary'
  AND b.navSeq = 85;     