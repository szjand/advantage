SELECT * FROM Applications

ALTER TABLE applications
ADD COLUMN appCode cichar(6);
ALTER TABLE applications
ADD COLUMN displayName cichar(24);

EXECUTE PROCEDURE sp_CreateIndex90( 
   'applications','applications.adi','appName','appName',
   '',2051,512, '' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'applications','applications.adi','appCode','appCode',
   '',2051,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'applications','applications.adi','PK','appname;appCode',
   '',2051,512, '' );     
EXECUTE PROCEDURE sp_ModifyTableProperty ('applications','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR','')
  

UPDATE applications SET appcode = 'stp' WHERE appName = 'teampay';
UPDATE applications SET appcode = 'np' WHERE appName = 'netpromoter';
UPDATE applications SET appcode = 'sco' WHERE appName = 'servicecheckout';
UPDATE applications SET appcode = 'warad' WHERE appName = 'warrantyadmin';
UPDATE applications SET appcode = 'auth' WHERE appName = 'authentication';


SELECT * FROM applicationRoles

ALTER TABLE applicationRoles
ADD COLUMN appCode cichar(6);
ALTER TABLE applicationRoles
ADD COLUMN displayName cichar(24);

UPDATE ApplicationRoles
SET appCode = (
  SELECT appCode 
  FROM applications
  WHERE appName = ApplicationRoles.appName);

EXECUTE PROCEDURE sp_CreateIndex90( 
   'applicationRoles','applicationRoles.adi','PK','appName;appCode;role',
   '',2051,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'applicationRoles','applicationRoles.adi','FK','appName;appCode',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_ModifyTableProperty ('applications','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR','');  
  
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('Applications-ApplicationRoles',
     'applications', 'applicationroles', 'FK', 
     1, 2, NULL, '', '');   -- cascade UPDATE
	 
-- applicationMetaData
EXECUTE PROCEDURE sp_CreateIndex90( 
   'applicationMetaData','applicationMetaData.adi','PK','appName;appCode;appRole;functionality',
   '',2051,512, '' );   
EXECUTE PROCEDURE sp_ModifyTableProperty ('applicationMetaData','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR','');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'applicationMetaData','applicationMetaData.adi','FK','appName;appCode;appRole',
   '',2,512,'' );  
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('appRoles-appMetaData',
     'applicationRoles', 'applicationMetaData', 'FK', 
     1, 2, NULL, '', '');   -- cascade UPDATE

CREATE TABLE employeeAppAuthorization(
  userName cichar(50) constraint NOT NULL,	
  appName cichar(50) constraint NOT NULL,
  appCode cichar(6) constraint NOT NULL,
  appRole cichar(25) constraint NOT NULL,
  functionality cichar(50) constraint NOT NULL,
  constraint PK primary key (userName, appName, appRole, functionality)) IN database;
  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'employeeAppAuthorization','employeeAppAuthorization','FK1','appName;appCode;appRole;functionality',
   '',2,512,'' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'employeeAppAuthorization','employeeAppAuthorization','FK2','userName',
   '',2,512,'' );     
   
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('appMetaData-empAppAuthorization',
     'applicationMetaData', 'employeeAppAuthorization', 'FK1', 
     1, 2, NULL, '', '');   -- cascade UPDATE     
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('tpEmployees-empAppAuthorization',
     'tpEmployees', 'employeeAppAuthorization', 'FK2', 
     1, 2, NULL, '', '');   -- cascade UPDATE       


  
-- ALL the data FROM employeeAppRoles INTO employeeAppAuthorization
DELETE FROM employeeAppAuthorization;
INSERT INTO employeeAppAuthorization
SELECT a.username, a.appname, b.appcode, a.role, c.functionality
FROM employeeAppRoles a
LEFT JOIN applications b on a.appname = b.appname
LEFT JOIN applicationmetadata c on a.appname = c.appname AND a.role = c.approle
-- WHERE a.appname = 'servicecheckout' IN('warrantyadmin','netpromoter','teampay','authentication')




