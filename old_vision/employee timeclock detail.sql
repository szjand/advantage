
SELECT c.lastname, b.thedate, a.vacationhours AS hours, 'vacation' 
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
WHERE b.thedate BETWEEN '01/01/2014' AND curdate()
  AND c.employeekey IN (
    SELECT employeekey
    FROM edwEmployeeDim
    WHERE employeenumber in ('1149500','194675'))
  AND vacationhours <> 0    
  
UNION

SELECT c.lastname, b.thedate, a.ptohours AS hours, 'pto'
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
WHERE b.thedate BETWEEN '01/01/2014' AND curdate()
  AND c.employeekey IN (
    SELECT employeekey
    FROM edwEmployeeDim
    WHERE employeenumber in ('1149500','194675'))
  AND ptohours <> 0      
  
UNION

SELECT c.lastname, CAST(NULL AS sql_date), sum(a.ptohours + vacationhours) AS hours, 'Total'
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
WHERE b.thedate BETWEEN '01/01/2014' AND curdate()
  AND c.employeekey IN (
    SELECT employeekey
    FROM edwEmployeeDim
    WHERE employeenumber in ('1149500','194675'))
  AND ptohours + vacationhours <> 0    
GROUP BY lastname  
ORDER BY lastname, thedate DESC 