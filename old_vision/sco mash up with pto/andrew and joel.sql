-- procs that explicitly reference aneumann@gfhonda.com
SELECT LEFT(name, 50) AS proc, 'andrew'
FROM system.storedprocedures
WHERE lower(CAST(sql_script AS sql_longvarchar)) LIKE '%aneumann@gfhonda.com%'
  AND name NOT LIKE 'zUn%'
UNION   
SELECT LEFT(name, 50), 'huot'
FROM system.storedprocedures
WHERE lower(CAST(sql_script AS sql_longvarchar)) LIKE '%mhuot@rydellchev.com%'
  AND name NOT LIKE 'zUn%'  
ORDER BY proc  

modified the stored procs that rely on hardcoded manager usernames
PROCEDURES: GetAgedROs, GetCurrentROs, GetScoManagerLandingPage, GetWarrantyRos,
  GetWritersForManager

UPDATE tpEmployees
SET username = 'aneumann@rydellcars.com',
    employeenumber = '1102195'
-- SELECT * FROM tpEmployees
WHERE username LIKE 'aneu%';

-- andrews missing app authorization
INSERT INTO employeeAppAuthorization 
SELECT 'aneumann@rydellcars.com', a.appname, a.appseq, a.appcode, a.approle, a.functionality, a.appdepartmentkey
FROM (
  SELECT *
  FROM employeeappauthorization
  WHERE username = 'mhuot@rydellchev.com') a
LEFT JOIN (
  SELECT *
  FROM employeeappauthorization
  WHERE username = 'aneumann@rydellcars.com') b on a.appname = b.appname
    AND a.approle = b.approle
    AND a.functionality = b.functionality
WHERE b.username IS NULL;

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'aneumann@rydellcars.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';     
