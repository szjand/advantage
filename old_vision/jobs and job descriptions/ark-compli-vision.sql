SELECT a.storeCode, a.employeenumber, a.lastname, a.firstname, 
  a.fullPartTime AS status, 
  a.payrollClass,
  a.pyDept AS PRDept, a.distCode, a.distribution as distCodeDescription,
  yrtext AS ark_job, TRIM(e.firstname) + ' ' + e.lastname AS ark_sup_name, 
  f.title AS compli_job, f.supervisorname AS compli_sup
--SELECT COUNT(*)  
-- SELECT employeenumber FROM (SELECT *
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn -- job code & mgr emp#
  AND b.yrjobd <> ''
  AND a.storecode = b.yrco#
LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd -- job description
  AND b.yrco# = c.yrco#
LEFT JOIN stgArkonaPYMAST d on a.employeeNumber = d.ymempn  
LEFT JOIN edwEmployeeDim e on e.employeenumber = b.yrmgrn -- mgr name
  AND e.currentrow = true
LEFT JOIN scotest.pto_tmp_compli_users f on a.employeenumber = f.userid
  AND f.status = '1'
WHERE a.currentrow = true
  AND a.active = 'Active'
ORDER BY a.lastname, a.firstname  
--) x GROUP BY employeenumber HAVING COUNT(*) > 1  

-- FROM scotest
SELECT a.storeCode, a.employeenumber, a.lastname, a.firstname, 
  a.fullPartTime AS status, 
  a.payrollClass,
  a.pyDept AS PRDept, a.distCode, a.distribution as distCodeDescription,
  yrtext AS ark_job, 
  f.title AS compli_job, trim(g.department) + ':' + g.position AS vision_job
--SELECT COUNT(*)  
-- SELECT employeenumber FROM (SELECT *
FROM dds.edwEmployeeDim a
LEFT JOIN dds.stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn -- job code & mgr emp#
  AND b.yrjobd <> ''
  AND a.storecode = b.yrco#
LEFT JOIN dds.stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd -- job description
  AND b.yrco# = c.yrco#
LEFT JOIN dds.stgArkonaPYMAST d on a.employeeNumber = d.ymempn  
LEFT JOIN dds.edwEmployeeDim e on e.employeenumber = b.yrmgrn -- mgr name
  AND e.currentrow = true
LEFT JOIN pto_tmp_compli_users f on a.employeenumber = f.userid
  AND f.status = '1'
LEFT JOIN ptoPositionFulfillment g on a.employeenumber = g.employeenumber  
WHERE a.currentrow = true
  AND a.active = 'Active'
ORDER BY a.lastname, a.firstname  


-- some notion of deriving accounting dept based on dist codes

payroll departments: PYPCLKCTL

SELECT yicco#, yicdept, yicdesc 
FROM stgArkonaPYPCLKCTL
WHERE currentrow = true
  AND yicco# IN ('ry1','ry2')
ORDER BY yicco#, yicdept  

accounting departments: GLPDEPT

SELECT * 
FROM stgArkonaGLPDEPT
WHERE company_number IN ('ry1','ry2')

-- columns to include: gross_Expense_act_
SELECT * 
FROM zfixpyactgr
WHERE NOT (gross_Expense_act_ = overtime_act_)

SELECT * 
FROM zfixpyactgr
WHERE NOT (holiday_Expense_act_ = sick_leave_expense_act_)
-- columns to include: vacation_expense_act_
SELECT * 
FROM zfixpyactgr
WHERE NOT (holiday_Expense_act_ = vacation_expense_act_)

SELECT * 
FROM zfixpyactgr
WHERE holiday_Expense_act_ <> vacation_expense_act_

SELECT * 
FROM zfixpyactgr
WHERE retire_expense_act_ <> emplr_contributions


SELECT company_number, dist_code, MAX(seq_number)
FROM zfixpyactgr
GROUP BY company_number, dist_code
ORDER BY MAX(seq_number) DESC  

SELECT a.*, b.gross_expense_act_, c.gmdept--, d.gmdept
FROM (
  SELECT company_number, dist_code, seq_number
  FROM zfixpyactgr
  GROUP by company_number, dist_code, seq_number) a
LEFT JOIN zfixpyactgr b on a.company_number = b.company_number
  AND a.dist_code = b.dist_code
  and a.seq_number = b.seq_number
LEFT JOIN stgArkonaGLPMAST c on b.company_number = c.gmco#
  and b.gross_expense_act_ = c.gmacct 
  AND c.gmyear = 2015
  
  
LEFT JOIN stgArkonaGLPMAST d on b.company_number = c.gmco#
  and b.vacation_expense_act_ = d.gmacct 
  AND c.gmyear = 2015  
 
select * FROM stgarkonaglpmast WHERE gmyear = 2015


-- what are ALL the distinct accounts assoc with a dist code

SELECT a.*, b.gmdept AS acct_dept
FROM (
  SELECT company_number, dist_code, gross_expense_act_ AS acct--, 'gross'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, vacation_expense_act_ AS acct--, 'vac'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, retire_expense_act_ AS acct--, 'ret'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, emplr_fica_expense AS acct--, 'ret'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, emplr_med_expense AS acct--, 'ret'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, federal_un_emp_act_ AS acct--, 'ret'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, emplr_contributions AS acct--, 'ret'
  FROM zfixpyactgr) a
LEFT JOIN stgArkonaGLPMAST b on  a.acct = b.gmacct 
  AND b.gmyear = 2015  
  
SELECT a.company_number, a.dist_code, a.description, b.gmdept
FROM (
  SELECT company_number, dist_code, description, gross_expense_act_ AS acct--, 'gross'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, description, vacation_expense_act_ AS acct--, 'vac'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, description, retire_expense_act_ AS acct--, 'ret'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, description, emplr_fica_expense AS acct--, 'ret'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, description, emplr_med_expense AS acct--, 'ret'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code, description, federal_un_emp_act_ AS acct--, 'ret'
  FROM zfixpyactgr
  UNION
  SELECT company_number, dist_code description, description, emplr_contributions AS acct--, 'ret'
  FROM zfixpyactgr) a
LEFT JOIN stgArkonaGLPMAST b on a.acct = b.gmacct 
  AND b.gmyear = 2015    
WHERE gmdept <> '' 
GROUP BY a.company_number, a.dist_code, a.description, b.gmdept  
