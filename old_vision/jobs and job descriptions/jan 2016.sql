SELECT a.storecode, a.employeenumber, a.lastname, a.firstname, a.fullparttime,
  a.payrollclass, a.pydept, a.distcode, a.distribution, c.yrtext AS ark_job,
  d.title AS compli_job,
  TRIM(e.department) + ' : ' + e.position AS vision_job
-- SELECT a.lastname, a.firstname, LEFT(c.yrtext, 35), LEFT(d.title,35), left(TRIM(e.department) + ' : ' + e.position, 45) AS vision_job 
FROM dds.edwEmployeeDim a
LEFT JOIN dds.stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
  AND a.storecode = b.yrco#
  AND b.yrjobd <> ''
LEFT JOIN dds.stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
  AND c.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
  AND a.storecode = c.yrco#
LEFT JOIN pto_compli_users d on a.employeenumber = d.userid
LEFT JOIN ptoPositionFulfillment e on a.employeenumber = e.employeenumber
WHERE a.currentrow = true
  AND a.active = 'Active'
ORDER BY a.lastname, a.firstname  