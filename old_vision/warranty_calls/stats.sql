/*
execute procedure InsertROUpdate( :ro, :userName, :status )

execute procedure InsertWarrantyCall( 
        :ro, 
        :userName, 
        :description, 
        :followUpComplete )

execute procedure getWaradROs( :type 

execute procedure getWarrantyAdminROUpdates( :ro )

execute procedure getWarrantyROsCompleted()

execute procedure GetWarrantyROPhoneCalls( :ro )

execute procedure getWarrantyROs( :userName )

execute procedure getWarrantyCalls( :ro )
*/

execute procedure swMetricDataYesterday() updates warrantyros nightly

bdc took over the calls IN march 2015
January 2017, new GM policy, surveys sent on customer pay, so those ros are now included

SELECT * FROM warrantycalls where cast(callts as sql_date) > '12/31/2021' ORDER BY left(description, 20), callts DESC

SELECT DISTINCT LEFT(description, 60) from warrantycalls where cast(callts as sql_date) > '12/31/2021'

SELECT b.yearmonth, left(cast(a.description AS sql_char), 105), COUNT(*)
FROM warrantycalls a
JOIN dds.day b on CAST(callts AS sql_date) = b.thedate
WHERE b.theyear = 2022
GROUP BY b.yearmonth, cast(a.description AS sql_char)

16513899 no reason
SELECT max(b.theyear), MAX(b.monthname), 
  SUM(CASE WHEN a.description LIKE 'Complete%' THEN 1 else 0 END) AS complete,
	SUM(CASE WHEN a.description LIKE 'No warranty call%' THEN 1 else 0 END) AS no_call_reqd,
	SUM(CASE WHEN a.description LIKE 'Unable to reach%' THEN 1 else 0 END) AS unable_to_reach,
	SUM(CASE WHEN a.description LIKE 'Left message%' THEN 1 else 0 END) AS left_message,
	COUNT(*) AS TOTAL
FROM warrantycalls a
JOIN dds.day b on CAST(callts AS sql_date) = b.thedate
WHERE b.theyear BETWEEN 2021 AND 2022
GROUP BY b.yearmonth
ORDER BY yearmonth

SELECT MIN(b.thedate), MAX(b.thedate)  -- '1/3 - 4/11
FROM warrantycalls a
JOIN dds.day b on CAST(callts AS sql_date) = b.thedate
WHERE b.theyear = 2022

SELECT * FROM warrantyros WHERE closedate > '01/03/2022' --AND '04/11/2022'

-- days FROM CLOSE to update
SELECT a.ro, a.closedate, CAST(b.callts AS sql_date), left(cast(b.description AS sql_char), 65), 
  timestampdiff(sql_tsi_day, a.closedate, CAST(b.callts AS sql_date)) 
FROM warrantyros a
LEFT JOIN warrantycalls b on a.ro = b.ro
WHERE closedate BETWEEN '01/03/2022' AND '04/11/2022'


-- avg days FROM CLOSE to update
SELECT COUNT(*),
  sum(timestampdiff(sql_tsi_day, a.closedate, CAST(b.callts AS sql_date))),
	1.0 * sum(timestampdiff(sql_tsi_day, a.closedate, CAST(b.callts AS sql_date))) / COUNT(*)
FROM warrantyros a
LEFT JOIN warrantycalls b on a.ro = b.ro
WHERE closedate BETWEEN '01/03/2022' AND '04/11/2022'