-- digital funnel metrics
DROP TABLE dfm_stores;
CREATE TABLE dfm_stores (
  name cichar(60),
constraint PK primary key (name))  IN DATABASE;  

DROP TABLE dfm_metrics;
CREATE TABLE dfm_metrics (
  metric cichar(60),
constraint pk primary key (metric)) IN database;  

DROP TABLE dfm_data;
CREATE TABLE dfm_data (
  name cichar(60) constraint NOT NULL,
  metric cichar(60) constraint NOT NULL,
  the_month cichar(3) constraint NOT NULL,
  the_year cichar(4) constraint NOT NULL,
  percentage double default '0' constraint NOT NULL,
  yearmonth integer constraint NOT NULL,
constraint pk primary key (name,metric,the_month,the_year)) IN database; 
/*
-- 2/9/15 i know there will be sorting issues, so
ALTER TABLE dfm_data
ADD COLUMN yearmonth integer constraint NOT NULL;

UPDATE dfm_data
SET yearmonth = 201412
WHERE the_month = 'dec'
  AND the_year = '2014';

UPDATE dfm_data
SET yearmonth = 201501
WHERE the_month = 'jan'
  AND the_year = '2015'; 
*/  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dfm_data','dfm_data.adi','FK1','name','',2,512,'');

EXECUTE PROCEDURE sp_CreateIndex90( 
   'dfm_data','dfm_data.adi','FK2','metric','',2,512,'');  

-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dfm_store_data');   
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dfm_store_data',
     'dfm_stores', 'dfm_data', 'FK1', 
     1, 2, NULL, '', '');   -- cascade UPDATE       

-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dfm_metric_data');       
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dfm_metric_data',
     'dfm_metrics', 'dfm_data', 'FK2', 
     1, 2, NULL, '', '');   -- cascade UPDATE      