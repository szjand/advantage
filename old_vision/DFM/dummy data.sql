
INSERT INTO dfm_stores values('gf-chev');
INSERT INTO dfm_stores values('gf-honda');
INSERT INTO dfm_stores values('sf-ford');

DELETE FROM dfm_metrics;
INSERT INTO dfm_metrics values('eLead Contact');
INSERT INTO dfm_metrics values('Phone Appointments to Leads');
INSERT INTO dfm_metrics values('Internet Appointments to Leads');

-- DELETE FROM dfm_data;
INSERT INTO dfm_data values ('gf-chev','eLead Contact','Dec','2014',(SELECT round(rand(), 2) FROM system.iota), 201412);
INSERT INTO dfm_data values ('gf-chev','Phone Appointments to Leads','Dec','2014',(SELECT round(rand(), 2) FROM system.iota), 201412);
INSERT INTO dfm_data values ('gf-chev','Internet Appointments to Leads','Dec','2014',(SELECT round(rand(), 2) FROM system.iota), 201412);
INSERT INTO dfm_data values ('gf-chev','eLead Contact','Jan','2015',(SELECT round(rand(), 2) FROM system.iota), 201501);
INSERT INTO dfm_data values ('gf-chev','Phone Appointments to Leads','Jan','2015',(SELECT round(rand(), 2) FROM system.iota), 201501);
INSERT INTO dfm_data values ('gf-chev','Internet Appointments to Leads','Jan','2015',(SELECT round(rand(), 2) FROM system.iota), 201501);

INSERT INTO dfm_data values ('gf-honda','eLead Contact','Dec','2014',(SELECT round(rand(), 2) FROM system.iota), 201412);
INSERT INTO dfm_data values ('gf-honda','Phone Appointments to Leads','Dec','2014',(SELECT round(rand(), 2) FROM system.iota), 201412);
INSERT INTO dfm_data values ('gf-honda','Internet Appointments to Leads','Dec','2014',(SELECT round(rand(), 2) FROM system.iota), 201412);
INSERT INTO dfm_data values ('gf-honda','eLead Contact','Jan','2015',(SELECT round(rand(), 2) FROM system.iota), 201501);
INSERT INTO dfm_data values ('gf-honda','Phone Appointments to Leads','Jan','2015',(SELECT round(rand(), 2) FROM system.iota), 201501);
INSERT INTO dfm_data values ('gf-honda','Internet Appointments to Leads','Jan','2015',(SELECT round(rand(), 2) FROM system.iota), 201501);

INSERT INTO dfm_data values ('sf-ford','eLead Contact','Dec','2014',(SELECT round(rand(), 2) FROM system.iota), 201412);
INSERT INTO dfm_data values ('sf-ford','Phone Appointments to Leads','Dec','2014',(SELECT round(rand(), 2) FROM system.iota), 201412);
INSERT INTO dfm_data values ('sf-ford','Internet Appointments to Leads','Dec','2014',(SELECT round(rand(), 2) FROM system.iota), 201412);
INSERT INTO dfm_data values ('sf-ford','eLead Contact','Jan','2015',(SELECT round(rand(), 2) FROM system.iota), 201501);
INSERT INTO dfm_data values ('sf-ford','Phone Appointments to Leads','Jan','2015',(SELECT round(rand(), 2) FROM system.iota), 201501);
INSERT INTO dfm_data values ('sf-ford','Internet Appointments to Leads','Jan','2015',(SELECT round(rand(), 2) FROM system.iota), 201501);

