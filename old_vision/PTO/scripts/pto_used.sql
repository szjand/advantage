/*
execute procedure sp_DropReferentialIntegrity('ptoEmployees-pto_used');
DROP TABLE pto_used;
UPDATE frequency?
needs to be generalized for regular UPDATE
UPDATE pattern? 
  delete/reload entire table
  delete/reload for a specified period (eg 30 days)
  
depends on :
  dds.edwClockHoursFact  
  ptoEmployees

10/26 
within the notion of an employee is only IN the ptoStructure with a single instance, 
AND the notion of WHEN someone IS termed, simply deleting that 
employeenumber based instance, pto_Used could (AND will) IN the CASE of a transfer
contain hours for an employeenumber that no longer EXISTS IN the system
(ry2 emp uses 8 hrs pto, transfers to ry1, gets a new employeenumber, that
new employeenumber has 0 hours pto used, but the person has 8)
ADD a source attribute: time clock/transfer/other
   
SELECT COUNT(*) FROM pto_used 

11/4/14
  fucked up the JOIN BETWEEN edwClockHoursFact AND edwEmployeeDim, need ALL
  employeekeys
  
*/
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity ('ptoEmployees-pto_used');
-- DROP TABLE pto_used;
CREATE TABLE pto_used (
  employeeNumber cichar(7) constraint NOT NULL,
  theDate date constraint NOT NULL,
  source cichar(24) constraint NOT NULL,
  hours numeric(6,2) constraint NOT NULL,
  constraint pk primary key (employeeNumber, theDate, source)) IN database;

EXECUTE PROCEDURE sp_CreateIndex90( 'pto_used', 'pto_used.adi', 'the_date', 'theDate', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'pto_used', 'pto_used.adi', 'FK', 'employeeNumber', '', 2, 1024, '' );
-- UPDATE & DELETE: cascade
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoEmployees-pto_used', 'ptoEmployees', 'pto_used', 'FK', 1, 1, NULL, '', '');

DELETE FROM pto_used WHERE source = 'Time Clock';
INSERT INTO pto_used (employeeNumber, theDate, hours, source)

SELECT m.employeenumber, theDate, ptoHours, 'Time Clock'
FROM ptoEmployees m
LEFT JOIN dds.edwEmployeeDim p on m.employeenumber = p.employeenumber
  AND p.currentrow = true
  AND p.activecode = 'a'
INNER JOIN (
  SELECT c.employeenumber, b.thedate, 
    a.vacationhours + a.ptohours AS ptoHours
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.day b on a.datekey = b.datekey
    AND b.theyear = 2014
  INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey  
--    AND c.currentrow = true
--    AND c.activecode = 'a'
  WHERE a.vacationhours > 0 OR  a.ptohours > 0) n on m.employeenumber = n.employeenumber
WHERE p.lastname IS NOT NULL; 



