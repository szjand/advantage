SELECT *
FROM dds.edwEmployeeDim a
WHERE a.distcode = 'wtec'
  AND a.currentrow = true
  AND a.active = 'active'
  
SELECT DISTINCT pydept FROM dds.edwEmployeeDim WHERE currentrow = true

SELECT *
FROM dds.edwEmployeeDim 
WHERE currentrow = true
  AND pydept = 'car wash'
  
select *
FROM ptoPositionFulfillment
WHERE employeenumber IN (
SELECT employeenumber
FROM dds.edwEmployeeDim a
WHERE a.distcode = 'wtec'
  AND a.currentrow = true
  AND a.active = 'active')
   

1. ptoEmployees
-- choice needs to be made here, anniversary could be current hiredate OR
--   original hiredate
INSERT INTO ptoEmployees (employeeNumber, pto_anniversary
SELECT a.employeenumber, a.hiredate--, b.ymhdto
FROM dds.edwEmployeeDim a
LEFT JOIN dds.stgArkonaPYMAST b on a.employeenumber = b.ymempn
WHERE a.employeenumber = '1117950'
  AND a.currentrow = true;
  
2. ptoPositionFulfillment (department, position, employeeNumber
INSERT INTO ptoPositionFulfillment (department, position, employeeNumber)
values ('Detail', 'Detail Technician', '1117950');

3. pto_employee_pto_pto_category_dates
INSERT INTO pto_employee_pto_category_dates (ptoCategory, employeeNumber, 
  fromDate, thruDate)
SELECT b.ptoCategory, a.employeeNumber, 
  cast(timestampadd(sql_tsi_year, b.fromYearsTenure, a.ptoAnniversary) AS sql_date) AS fromDate,
  cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, b.thruYearsTenure + 1, a.ptoAnniversary)) AS sql_date) AS thruDate
FROM ptoEmployees a, pto_categories b
WHERE a.employeenumber = '1117950';


-- 10/27 CATCH up on pto new employees
SELECT firstname,lastname,username,password FROM tpemployees ORDER BY firstname


SELECT firstname,lastname,username,password 
FROM tpemployees
WHERE TRIM(firstname) + ' ' + lastname IN (
'Jennifer Vetter','Joey Barta','Anna Aguilar','Cory Baron',
'Shaun Rigby','Jace Marion','Zachary Palm','Darrick Richardson',
'Steven Emmons','Andrew Darling','Eric Horton','Jason Schwan',
'Zachary Vodden','Zachary Woodbridge','Brandon Lamont')
ORDER BY password


SELECT 'Jennifer Vetter','Joey Barta','Anna Aguilar','Cory Baron',
'Shaun Rigby','Jace Marion','Zachary Palm','Darrick Richardson',
'Steven Emmons','Andrew Darling','Eric Horton','Jason Schwan',
'Zachary Vodden','Zachary Woodbridge','Brandon Lamont'
FROM system.iota




