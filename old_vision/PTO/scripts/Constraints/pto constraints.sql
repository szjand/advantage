
-- any position that functions AS a ptoAdmin can only be filled BY one person,
-- eg, at any given time there can only be one Office:Office Manager 
-- but multiple Office:Title Clerk

SELECT authByDepartment, authByPosition
FROM (
  SELECT *
  FROM (-- ALL pto admin positions
    select authByDepartment, authByPosition
    FROM ptoAuthorization
    WHERE authByLevel <> 1 -- exclude top level (board of directors
    GROUP BY authByDepartment, authByPosition) a
  LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
    AND a.authByPosition = b.position) c
GROUP BY authByDepartment, authByPosition 
HAVING COUNT(*) > 1      

can this be phrased IN terms of the heirarchy
any non-leaf position


-- 10/28/14
-- yikes, a position for which there IS no authFor
-- any time ptoAuthorization IS changed
-- upon further reflection ***
SELECT *
FROM ptoDepartmentPositions a
LEFT JOIN ptoAuthorization b on a.department = b.authForDepartment
  AND a.position = b.authForPosition
WHERE b.authForDepartment IS NULL  
  AND level <> 1
  AND EXISTS ( -- *** ? only matters IF there IS someone IN the position
    SELECT 1
    FROM ptoPositionFulfillment
    WHERE department = a.department
      AND position = a.position)
      
      
-- the garrett anomaly: tpEmployees.employeenumber <> ptoEmployees.employeenumber  
-- thinking that they both need to synch to dds.edwEmployeeDim 
SELECT a.firstname, a.lastname, a.employeenumber, b.employeenumber
FROM tpEmployees a
LEFT JOIN dds.edwEmployeeDim b on a.firstname = b.firstname
  AND a.lastname = b.lastname
  AND b.currentrow = true
  AND b.active = 'active'
WHERE a.employeenumber <> b.employeenumber        

-- 11/6 mandatory relationships
-- eg ptoEmployee with no row IN pto_empoloyee_pto_category_dates

ptoEmployees -- pto_employee_pto_allocation
             -- pto_employee_pto_category_dates
             
shit, now i am thinking pto_employee_pto_category_dates was just used to generate
initial values which now exist IN pto_employee_pto_allocation    
AND the only mandatory relationship IS to pto_employee_pto_allocation
select *
FROM ptoEmployees a
LEFT JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
LEFT JOIN pto_employee_pto_allocation c on a.employeenumber = c.employeenumber
WHERE b.employeenumber IS NULL 

select *
FROM ptoEmployees a
LEFT JOIN pto_employee_pto_allocation c on a.employeenumber = c.employeenumber
WHERE c.employeenumber IS NULL 

-- 7/23/15
ptoAuthorization authByLevel, authForLevel, need to match the relevant
ptoDepartmentPositions level

SELECT *
FROM ptoAuthorization a
INNER JOIN ptoDepartmentPositions b ON a.authByDepartment = b.department
  AND a.authByPosition = b.position
INNER JOIN ptoDepartmentPositions c on a.authForDepartment = c.department
  AND a.authForPosition = c.position
WHERE a.authByLevel <> b.level  
  OR a.authForLevel <> c.level  