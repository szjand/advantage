I submitted an IT ticket for Nick�s transfer from the New Car Sales Manager 
at the GM store to the General Sales Manager at the Honda store, effective 
today. Will you please add the position of General Sales Manager back to 
the PTO org structure for Honda? All Sales positions will report to this 
GSM position, and the GSM position itself will report to Mike Lear. The GSM 
is subject to the Allowed Time Off policy. 

-- new position
INSERT INTO ptoDepartmentPositions (department,position,level,ptopolicyhtml)
values('RY2 Sales', 'General Sales Manager', 3, 'no_policy_html');
-- fulfilled BY nick
UPDATE ptoPositionFulfillment
SET department = 'RY2 Sales',
    position = 'General Sales Manager'
WHERE employeenumber = '2126300';

UPDATE ptoAuthorization
SET authByDepartment = 'RY2 Sales',
    authByPosition = 'General Sales Manager'  
-- SELECT * FROM ptoAuthorization
WHERE authByDepartment = 'RY2'
  AND authByPosition = 'General Manager'
  AND authForDepartment = 'RY2 Sales';
  
