/*
v3.3  12/14/14
1.
New Positions: 
    RY1 Sales:New Car Sales Consultant auth  by RY1 Sales:NewCar Sales Manager, 
    RY Sales:Used Car Sales Consultant auth by RY1 Sales:Used Car Buyer
2.    
Move RY1 Sales:Used Car Buyer to be auth by RY1 Sales:General Sales Manager
3.
Move RY1 Sales: Finance Manager to be auth by RY1 Sales:New Car Sales Manager
4.
Move RY1 Sales:New Car Technology Specialist  to be auth by RY1 Sales:New Car Sales Manager
5.
Move RY1 Sales:New Car inventory Manager  to be auth by RY1 Sales:New Car Sales Manager
6.
Move RY1 Sales:Digital Coordinator  to be auth by RY1 Sales:New Car Sales Manager
*/
BEGIN TRANSACTION;
TRY 
-- 1 New Positions
INSERT INTO ptoPositions (position)
values('New Car Sales Consultant');
INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
values('RY1 Sales','New Car Sales Consultant',5,'no_policy_html');
INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('RY1 Sales','New Car Sales Manager','RY1 Sales','New Car Sales Consultant',4,5);

INSERT INTO ptoPositions (position)
values('Used Car Sales Consultant');
INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
values('RY1 Sales','Used Car Sales Consultant',5,'no_policy_html');
INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('RY1 Sales','Used Car Buyer','RY1 Sales','Used Car Sales Consultant',4,5);

-- 2 Move RY1 Sales:Used Car Buyer to be auth by RY1 Sales:General Sales Manager
UPDATE ptoAuthorization
SET authByPosition = 'General Sales Manager'
WHERE authForDepartment = 'RY1 Sales'
  AND authForPosition = 'Used Car Buyer';   
-- 3 Move RY1 Sales: Finance Manager to be auth by RY1 Sales:New Car Sales Manager
UPDATE ptoDepartmentPositions
SET level = 5
WHERE department = 'RY1 Sales'
  AND position = 'Finance Manager';
    
UPDATE ptoAuthorization
SET authByPosition = 'New Car Sales Manager',
    authByLevel = 4,
    authForLevel = 5
WHERE authForDepartment = 'RY1 Sales'
  AND authForPosition = 'Finance Manager';
-- 4 Move RY1 Sales:New Car Technology Specialist  to be auth by RY1 Sales:New Car Sales Manager
UPDATE ptoDepartmentPositions
SET level = 5
WHERE department = 'RY1 Sales'
  AND position = 'New Car Technology Specialist';
  
UPDATE ptoAuthorization
SET authByPosition = 'New Car Sales Manager',
    authByLevel = 4,
    authForLevel = 5
WHERE authForDepartment = 'RY1 Sales'
  AND authForPosition = 'New Car Technology Specialist';
-- 5 Move RY1 Sales:New Car inventory Manager  to be auth by RY1 Sales:New Car Sales Manager
UPDATE ptoDepartmentPositions
SET level = 5
WHERE department = 'RY1 Sales'
  AND position = 'New Car Inventory Manager';

UPDATE ptoAuthorization
SET authByPosition = 'New Car Sales Manager',
    authByLevel = 4,
    authForLevel = 5
WHERE authForDepartment = 'RY1 Sales'
  AND authForPosition = 'New Car Inventory Manager';      
-- 6 Move RY1 Sales:Digital Coordinator  to be auth by RY1 Sales:New Car Sales Manager
UPDATE ptoDepartmentPositions
SET level = 6
WHERE department = 'RY1 Sales'
  AND position = 'Sales Support';
UPDATE ptoDepartmentPositions
SET level = 5
WHERE department = 'RY1 Sales'
  AND position = 'Digital Coordinator';
UPDATE ptoAuthorization
  SET authByLevel = 5,
      authForLevel = 6
WHERE authForDepartment = 'RY1 Sales'
  AND authForPosition = 'Sales Support';        
  
UPDATE ptoAuthorization
SET authByPosition = 'New Car Sales Manager',
    authByLevel = 4,
    authForLEvel = 5
WHERE authForDepartment = 'RY1 Sales'
  AND authForPosition = 'Digital Coordinator';     

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    