10/2/14

Brock & Jake (Detail Porters) will now reside under Aubrey Crane instead of Dave Willkie.

All detail personnel including detail technicians (formerly assigned to John Gardner) will now report to Aubrey Crane for PTO requests.


-- positions for which wilkie IS the admin: RY1 Sales:Porter, RY1 Sales:Used Car Buyer
SELECT *
FROM ptoAuthorization a
INNER JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.Position
INNER JOIN ptoEmployees c on b.employeeNumber = c.employeeNumber
INNER JOIN dds.edwEmployeeDim d on c.employeeNumber = d.employeenumber
  AND d.currentrow = true
  AND d.lastname = 'wilkie'  
  
-- positions for which joh gardner IS the admin: Detail:Detail Technician
SELECT *
FROM ptoAuthorization a
INNER JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.Position
INNER JOIN ptoEmployees c on b.employeeNumber = c.employeeNumber
INNER JOIN dds.edwEmployeeDim d on c.employeeNumber = d.employeenumber
  AND d.currentrow = true
  AND d.lastname = 'gardner'   
  AND d.firstname = 'john'
  
-- aubrey's position: Detail:Assistant Manager
SELECT *
FROM ptoPositionFulfillment a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.lastname = 'crane'  
  
-- john gardner's position:  Detail:Team Leader 
SELECT *
FROM ptoPositionFulfillment a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.lastname = 'gardner'
  AND b.firstname = 'john'  
  
SELECT *
FROM ptoAuthorization
WHERE authForDepartment = 'RY1 Sales'
  AND authForPosition = 'Porter'  
  
-- yikes, first change ptoDeparmentPositions.level
-- does this need some RI, thinking yep, but i don't really want level to be
-- part of the ptoDepartmentPosistions PK, which would be required IN advantage
-- fuck me, yet more poorly thought out modelling
-- oh well
-- IN the mean time, this query can be used AS an assertion
SELECT *
FROM ptoDepartmentPositions a
INNER JOIN ptoAuthorization b on a.department = b.authByDepartment
  AND a.position = b.authByPosition
WHERE a.level <> b.authByLevel  
UNION 
SELECT *
FROM ptoDepartmentPositions a
INNER JOIN ptoAuthorization b on a.department = b.authForDepartment
  AND a.position = b.authForPosition
WHERE a.level <> b.authForLevel; 

/*
thinking of doing this AS an admin, no way to know what the new level would be
outside the context of ptoAuthorization, so the new level would have to be 
determined AS a result of configuring (visually) IN the admin page AND derived
FROM the subsequent reorgainzation (who admins the position IN the new structure)
*/
SELECT * -- current level 4
FROM ptoDepartmentPositions
WHERE department = 'RY1 Sales' AND position = 'porter'

-- 10/6, fuuuuuuck, just hard code the necesary changes, go through this  few
-- times AND maybe i will figure out what admin functionality looks LIKE


1. change ptoDepartmentPositions.level AS requd
  RY1 Sales:Porter level 4 -> level 5
  Detail:Detail Technician stays at level 5
  
  SELECT * 
  FROM ptoDepartmentPositions
  WHERE department = 'ry1 sales'
    AND position = 'porter'
    
  UPDATE ptoDepartmentPositions
  SET level = 5 
  WHERE department = 'ry1 sales'
    AND position = 'porter';

2. UPDATE ptoAuthorization AS requd

  SELECT *
  FROM ptoAuthorization
  WHERE authForDepartment = 'ry1 sales'
    AND authForPosition = 'porter'    
  
  UPDATE ptoAuthorization
  SET authByDepartment = 'Detail',
      authByPosition = 'Assistant Manager',
      authByLevel = (
        SELECT level 
        FROM ptoDepartmentPositions
        WHERE department = 'detail'
          AND position = 'assistant manager'),
      authForLevel = (
        SELECT level
        FROM ptoDepartmentPositions
        WHERE department = 'ry1 sales'
          AND position = 'porter')      
  WHERE authForDepartment = 'ry1 sales'
    AND authForPosition = 'porter';
    
  SELECT *
  FROM ptoAuthorization
  WHERE authForDepartment = 'detail'
    AND authForPosition = 'detail technician'       
    
  UPDATE ptoAuthorization
  SET authByDepartment = 'Detail',
      authByPosition = 'Assistant Manager'
  WHERE authForDepartment = 'detail'
    AND authForPosition = 'detail technician';       
      
3. repopulate ptoClosure
  -- fuck updating AND editing, just regenerate anytime ptoAuthorization changes,
  -- only takes ~200 msec
  DECLARE @i integer;
  DECLARE @cur CURSOR AS
    SELECT distinct uc.ancestorDepartment, uc.ancestorPosition,
      u.authForDepartment, u.authForPosition
    FROM ptoClosure uc, ptoAuthorization u
    WHERE uc.descendantDepartment = u.authByDepartment 
      AND uc.descendantPosition = u.authByPosition   
    AND uc.depth = @i - 1;
  @i = 1;    
  DELETE FROM ptoClosure;
  -- self referencing row (depth = 0) for each position that IS NOT a leaf
  INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
    descendantPosition, depth)
  SELECT DISTINCT authByDepartment, authByPosition, authByDepartment, authByPosition, 0 
  FROM ptoAuthorization; 
  -- root   
  UPDATE ptoClosure
  SET topmost = true
  WHERE descendantPosition = 'board of directors';
  -- AND one self referencing (depth = 0) row for each leaf node
  INSERT INTO ptoClosure (ancestorDepartment,ancestorPosition,descendantDepartment,
    descendantPosition, depth, lowest)
  SELECT authForDepartment, authForPosition, authForDepartment, authForPosition, 0, true 
  FROM ptoAuthorization a
  WHERE NOT EXISTS (
    SELECT 1
    FROM  ptoAuthorization
    WHERE authByDepartment = a.authForDepartment
      AND authByPosition = a.authForPosition);
      
  WHILE @i < (SELECT MAX(authForLevel) + 1 FROM ptoAuthorization) DO 
  OPEN @cur;
  TRY
    WHILE FETCH @cur DO
      INSERT INTO ptoClosure values(@cur.ancestorDepartment, @cur.ancestorPosition,
        @cur.authForDepartment, @cur.authForPosition, @i, false, false);
    END WHILE;
  FINALLY
    CLOSE @cur;
  END TRY;  
  @i = @i + 1;
  END WHILE;      
      