7/20/15
v3.8

structural changes

personnel changes

new position: Video Editor (Tyler LeBlanc) auth BY Steve Symons (IT Manager)

	Steve Berg: from pdq -> building maint
	Chris Bjerk from sc -> team leader

SELECT * FROM ptopositionfulfillment WHERE employeenumber = (SELECT employeenumber FROM tpemployees WHERE firstname = 'steven' AND lastname = 'symons')

SELECT * 
FROM ptopositionfulfillment 
WHERE employeenumber = (
  SELECT employeenumber 
  FROM tpemployees 
  WHERE firstname = 'robert' 
    AND lastname = 'foote')
    
SELECT * 
FROM ptopositionfulfillment 
WHERE employeenumber = (
  SELECT employeenumber 
  FROM tpemployees 
  WHERE lastname = 'bjerk')    

SELECT a.firstname, a.lastname, a.employeenumber, b.position
FROM tpemployees a
INNER JOIN ptoPositionFulFillment b on a.employeenumber = b.employeenumber
  AND b.department = 'Building Maintenance'
  AND b.position = 'manager'
  
SELECT a.firstname, a.lastname, a.employeenumber, b.position
FROM tpemployees a
INNER JOIN ptoPositionFulFillment b on a.employeenumber = b.employeenumber
  AND b.department = 'Building Maintenance'
  
SELECT a.firstname, a.lastname, a.employeenumber, b.position
FROM tpemployees a
INNER JOIN ptoPositionFulFillment b on a.employeenumber = b.employeenumber
  AND b.department in ('Building Maintenance', 'car wash')  
 
SELECT * FROM ptodepartmentpositions WHERE position = 'support' 

SELECT * FROM ptodepartmentpositions WHERE position = 'technician' 

SELECT * FROM ptodepartmentpositions WHERE department = 'building maintenance' 

FROM taylor:
Jon, both Tyler Davis and Rogelio Sanchez are Car Wash Maintenance Technicians 
at an equal level and should both be reporting directly to Steve Bergh. There 
should be no Car Wash Maintenance Manager. Can we change all of the 
Building Maintenance and Car Wash Maintenance personnel to having 
“Technician” on the end of their titles?

-- who ALL are auth BY steve bergh (Appearance:Director)
SELECT b.department, b.position, c.*
FROM ptoAuthorization a
LEFT JOIN ptoPositionFulFillment b on a.authfordepartment = b.department
  AND a.authforposition = b.position
LEFT JOIN tpemployees c on b.employeenumber = c.employeenumber  
WHERE a.authByDepartment = 'Appearance'
  AND a.authByPosition = 'Director'
  
-- who ALL are auth BY ben cahalan (RY1:General Manager)
SELECT b.department, b.position, c.*
FROM ptoAuthorization a
LEFT JOIN ptoPositionFulFillment b on a.authfordepartment = b.department
  AND a.authforposition = b.position
LEFT JOIN tpemployees c on b.employeenumber = c.employeenumber  
WHERE a.authByDepartment = 'RY1'
  AND a.authByPosition = 'General Manager'  
  
SELECT *
from PtoAuthorization
WHERE authfordepartment = 'building maintenance'
  
BEGIN TRANSACTION;
TRY
/**/
-- insertions: positions, departmentPositions
  INSERT INTO ptoPositions values('Video Editor');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
    values('IT','Video Editor', 4, 'no_policy_html');
-- insertions: ptoAuthorization    
  INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
    values('IT','Manager','IT','Video Editor',3,4);        
    
  UPDATE ptoPositions
  SET position = 'Maintenance Technician'
  WHERE position = 'Maintenance Manager';

  INSERT into ptoPositions values('Technician');
  
  UPDATE ptoDepartmentPositions
  SET position = 'Technician',
      level = 4
  WHERE department = 'Building Maintenance'
    AND position = 'Support';
      
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  

BEGIN TRANSACTION;
TRY
  UPDATE ptoAuthorization 
  SET authByDepartment = 'Building Maintenance',
      authByPosition = 'Manager',
      authByLevel = 3,
      authForLevel = 4
  WHERE authForDepartment = 'Building Maintenance'
    AND authForPosition = 'Technician';
    
  UPDATE ptoPositionFulfillment
  SET department = 'Building Maintenance',
      position = 'Technician'
  -- SELECT * FROM ptoPositionFulfillment
  WHERE employeenumber = (
    SELECT employeenumber
    FROM tpemployees
    WHERE firstname = 'stephen'
      AND lastname = 'berg');      
      
  UPDATE ptoPositionFulfillment
  SET position = 'Team 2 Sales Consultant'
  WHERE employeenumber = (
    SELECT employeenumber
    FROM tpemployees
    WHERE firstname = 'Christopher'
      AND lastname = 'Bjerk');   
      
  UPDATE ptoPositionFulfillment
  SET position = 'Technician'
  -- SELECT * FROM ptoPositionFulfillment
  WHERE employeenumber = (
    SELECT employeenumber
    FROM tpemployees
    WHERE firstname = 'raymond'
      AND lastname = 'franks');   
      
  UPDATE ptoPositionFulfillment
  SET position = 'Manager'
  -- SELECT * FROM ptoPositionFulfillment
  WHERE employeenumber = (
    SELECT employeenumber
    FROM tpemployees
    WHERE firstname = 'robert'
      AND lastname = 'foote');   
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;      