
v3.5
2/10/15

1.
Nick Shirek IS no longer "in" ry2, but IS categorized AS a Store Manager Trainee,
  i guess, reporting to the Board, AS such, should he even fucking be IN PTO?
  
SELECT * 
FROM ptoPositionFulfillment a
INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
where b.lastname = 'shirek'

his previous position of RY2 Sales:Team Leader no longer EXISTS
in compli sales consultants now report to either John Olderback OR Dan Lizakowski
oops, thought ben f was going to change (IN compli) sales manager AND team leader
to team 1 team leader AND team 2 team leader, he has NOT
so, no change for john, dan changes FROM sales consultant to team leader


SELECT * 
FROM ptoPositionFulfillment a
INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
where b.lastname IN('Lizakowski','Olderbak','shirek')

current thinking IS to structure ry2 sales with 2 teams
team 1 (Olderbak - sales manager) AND team 2 (lizakowski - team leader)

SELECT trim(b.firstname) + ' ' +  b.lastname, a.employeenumber
FROM ptoPositionFulfillment a
LEFT JOIN tpemployees b on a.employeenumber = b.employeenumber
WHERE a.position = 'sales consultant'
  AND a.department = 'ry2 sales'
  AND b.employeenumber IS NOT NULL 
  
1. new positions
   RY2:Store Manager Trainee -> auth BY Market:BOD
   RY2 Sales:Team 1 Sales Consultant -> auth BY RY2 Sales:Sales Manager (olderbak)
   RY2 Sales:Team 2 Sales Consultant -> auth BY RY2 Sales: Team Leader
2. positionFulFillment
hmmmm, IS the rest done exclusively with position fulfillment
no changes to ptoAuthorization beyond the above new positions?
  nick shirek -> RY2:Store Manager Trainee
  dan lizakowski -> RY2 Sales: Team Leader
  Chris Nielson            2102196 Team 1
  Devlin Reasy             2118100 Team 1
  Fred Spencer             2130678 Team 1
  Corey Wilde              2150210 Team 1
  Jade Tandeski            2141225 Team 1
  Melissa Lawson           283521  Team 2
  Christopher Bjerk        212120  Team 2
  Lekeith Strother         2132420 Team 2  
  Joshua Ames              22910   Team 2
  Samuel Price             2112451 Team 2  
  
  Casey Sullivan           2133490 Termed  
THEN, clean up: no more deptPos RY2 Sales:Sales Consultant    



BEGIN TRANSACTION;
TRY 
-- 1 New Positions
INSERT INTO ptoPositions (position)
values('Store Manager Trainee');
INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
values('RY2','Store Manager Trainee',2,'no_policy_html');
INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('Market','Board of Directors','RY2','Store Manager Trainee',1,2);   

INSERT INTO ptoPositions (position)
values('Team 1 Sales Consultant');
INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
values('RY2 Sales','Team 1 Sales Consultant',4,'no_policy_html');
INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('RY2 Sales','Sales Manager','RY2 Sales','Team 1 Sales Consultant',3,4);  

INSERT INTO ptoPositions (position)
values('Team 2 Sales Consultant');
INSERT INTO ptoDepartmentPositions(department,position,level,ptoPolicyHtml)
values('RY2 Sales','Team 2 Sales Consultant',4,'no_policy_html');
INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('RY2 Sales','Team Leader','RY2 Sales','Team 2 Sales Consultant',3,4);

-- 2 positionFulfillment
-- nick
UPDATE ptoPositionFulfillment
SET department = 'RY2',
    position = 'Store Manager Trainee'
WHERE employeenumber = '2126300';  
-- dan
UPDATE ptoPositionFulfillment
SET position = 'Team Leader'
WHERE employeenumber = '285820';   
-- team 1
UPDATE ptoPositionFulfillment
SET position = 'Team 1 Sales Consultant'
WHERE employeenumber in ('2102196','2118100','2130678','2150210','2141225');  
-- team 2
UPDATE ptoPositionFulfillment
SET position = 'Team 2 Sales Consultant'
WHERE employeenumber in ('283521','212120','2132420','22910','2112451');   

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    

-- still have residual ri to zUnused tables, no need for that
EXECUTE PROCEDURE sp_DropReferentialIntegrity('ptoEmployees-pto_cat_dates');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('ptoEmployees-pto_at_rollout');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('pto_categories-pto_cat_dates');

-- clean up, no more RY2 Sales:Sales Consultant
BEGIN TRANSACTION;
TRY

-- ok, since the above updates, these residual ry2 sales:sales consultants are
-- actually termed folks, so how to deal with them
-- fuck it, DELETE them FROM ptoEmployees AND ALL associated pto tables, 
-- tp just remove authorization for now
-- 1. ptoEmployees

/*
-- UPDATE AND DELETE are both SET to cascade on ALL tables referencing ptoEmployees
SELECT left(name, 30), LEFT(ri_primary_table, 30), LEFT(ri_foreign_table,30),
  ri_updaterule, ri_deleterule 
FROM system.relations WHERE ri_primary_table = 'ptoEmployees'

*/
-- active part timers that exist IN ptoEmployees
DELETE 
-- SELECT *
FROM ptoEmployees
WHERE employeenumber IN (
  SELECT employeenumber 
  FROM dds.edwEmployeeDim 
  WHERE currentrow = true 
    AND active = 'active' 
    AND fullparttime = 'part');

-- terms that exist IN ptoEmployees   
DELETE  
-- SELECT *
FROM ptoEmployees
WHERE employeenumber IN (
  SELECT employeenumber 
  FROM dds.edwEmployeeDim 
  WHERE currentrow = true 
    AND active = 'term');    

-- 2 positions
/*
-- DELETE SET to restrict on the others
SELECT left(name, 40), LEFT(ri_primary_table, 30), LEFT(ri_foreign_table,30),
  ri_updaterule, ri_deleterule 
FROM system.relations 
WHERE left(trim(ri_primary_table), 3) = 'pto' AND TRIM(ri_primary_table) <> 'ptoEmployees'
select * FROM ptoPositionFulfillment WHERE department = 'ry2 sales' AND position = 'sales consultant'
-- so will need to start at leaf AND WORK up
*/

DELETE
FROM ptoClosure
WHERE descendantDepartment = 'ry2 sales' AND descendantPosition = 'sales consultant'; 
DELETE 
FROM ptoAuthorization
WHERE authForDepartment = 'ry2 sales' AND authForPosition = 'sales consultant';
DELETE 
FROM ptoDepartmentPositions
WHERE Department = 'ry2 sales' AND Position = 'sales consultant';

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    
  
  
-- clean up employeeAppAuthorization
BEGIN TRANSACTION;
TRY

-- employee does NOT exist IN ptoEmployees  
DELETE 
FROM employeeAppAuthorization  
WHERE appcode = 'pto'
  AND username IN (
    select distinct a.username
    FROM tpEmployees a
    INNER JOIN employeeAppAuthorization b on a.username = b.username
      AND appcode = 'pto'
    WHERE NOT EXISTS (
      SELECT 1
      FROM ptoEmployees
      WHERE employeenumber = a.employeenumber));
      
-- termed employees      
DELETE FROM employeeAppAuthorization
WHERE username IN (
SELECT username
FROM tpEmployees a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.termdate < curdate());           
      
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    
  