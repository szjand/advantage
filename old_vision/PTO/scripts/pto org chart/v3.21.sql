changes to Taylors org chart:

V1.6	6/24/2016
	Detail Positional Changes:
1.		Detail Assistant Manager position eliminated
2.		Detail Prep Technician position eliminated
	Other:
3.		Digital Marketing Manager position title changed to Digital Coordinator		
4.		Market Marketing Manager position added; reports to Market GM; 		
      oversees Project Coordinator and Digital Coordinator
5.		RY1 Service Runner position title changed to Service Stager 
6.		Recruiter/Benefits Coordinator position eliminated

6.  No changes , position was a part time position only, NOT IN pto hierarchy
5.  change already done IN 3.16
3/4 no effect on pto 

2. -----------------------------------
SELECT *
FROM ptodepartmentpositions a
LEFT JOIN ptopositionfulfillment b on a.department = b.department
  AND a.position = b.position
LEFT JOIN ptoemployees c on b.employeenumber = c.employeenumber  
WHERE a.department = 'detail'
  AND a.position = 'prep technician'
  
position was held BY michael mckay, he was termed on 6/22

-- position also EXISTS for body shop
SELECT * FROM ptodepartmentpositions WHERE position = 'prep technician'
-- DELETE FROM ptoDepartmentPositions, ri takes care of ptopositionfulfillment
DELETE FROM ptoDepartmentPositions
WHERE department = 'detail'
  AND position = 'prep technician'
  
1. ---------------------------------------
   aubrey (Detail assistant manager) has been termed
   her pto responsibilities shift to john gardner (detail floor supervisor)
   a position created IN jan 2016 AS an ATO position with no pto authorization responsibilites
   except for detail cashier which IS now authorized BY appearance director
    
update ptoauthorization
SET authbydepartment = 'Appearance',
    authbyposition = 'Director'
WHERE authforposition = 'cashier'
  AND authfordepartment = 'detail';  

-- unused position  
DELETE FROM ptopositions WHERE position = 'support'

-- move auth for detail: detail tech, porter, support specialist to detail floor supervisor
update ptoauthorization
SET authbyposition = 'Floor Supervisor'
WHERE authbydepartment = 'detail'
  AND authbyposition = 'assistant manager'

-- remove detail:assistant manager  
DELETE FROM ptoDepartmentPositions WHERE department = 'detail' and position = 'assistant manager'
  