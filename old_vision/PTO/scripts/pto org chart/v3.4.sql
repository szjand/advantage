/*
v3.4  1/26/15

1. ALL PDQ now Authorized BY RY2 Main Shop:Manager 
     Advisor, Lead Technician, Lowerbay Technician, Upperbay Technician use to
     be auth BY Assistant Manager
     per Joel, Garrett has too much on his plate
2. side effect of this structural change, garrett should no longer have pto access
     AS manager, but rather AS employee     
*/
BEGIN TRANSACTION;
TRY 

UPDATE ptoDepartmentPositions
SET level = 4
WHERE department = 'ry2 pdq'
  AND level = 5;
  
UPDATE ptoAuthorization
SET authByDepartment = 'RY2 Main Shop',
    authByPosition = 'Manager',
    authByLevel = 3,
    authForLevel = 4
WHERE authForDepartment = 'RY2 PDQ'
  AND authForPosition IN ('Advisor','Lead Technician','Lowerbay Technician','Upperbay Technician');
  
DELETE 
FROM employeeappauthorization
WHERE username = 'gevenson@rydellcars.com'
  AND appcode = 'pto';
  
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'gevenson@rydellcars.com', appName, 800, appCode, appRole, functionality
FROM applicationMetadata
WHERE appcode = 'pto'
  AND approle = 'ptoemployee';

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    