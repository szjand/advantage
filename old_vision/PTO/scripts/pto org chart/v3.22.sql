-- ADD postion Market:Marketing Manager

INSERT INTO ptoPositions values('Marketing Manager');
INSERT INTO ptoDepartmentPositions values('Market','Marketing Manager',3,'no_policy_html');

/*
SELECT * FROM ptoAuthorization WHERE authForPosition like '%Coordinator%'
SELECT * 
FROM ptopositionfulfillment 
WHERE employeenumber = (
  SELECT employeenumber
  FROM tpemployees
  WHERE lastname = 'delohery')
  
SELECT * FROM ptoAuthorization WHERE authForPosition = 'Digital Marketing Manager'
  
  
*/
-- authorizes project coordinator
update ptoAuthorization
SET authByPosition = 'Marketing Manager'
WHERE authForPosition like 'Project Coordinator';

-- authorizes delohery whose position changes FROM Digital Marketing Manager to Digital Coordinator
-- SELECT * FROM ptoAuthorization WHERE authForPosition = 'Project Coordinator'
-- change delohery's position
UPDATE ptoPositions
SET position = 'Digital Coordinator'
WHERE position = 'Digital Marketing Manager';
-- which IS now a market position
UPDATE ptoDepartmentPositions
SET department = 'Market'
WHERE position = 'Digital Coordinator';

-- now authorized BY marketing manager
-- SELECT * FROM ptoAuthorization WHERE authForPosition = 'digital coordinator'
update ptoAuthorization
SET authByPosition = 'Marketing Manager',
    authByDepartment = 'Market'
WHERE authForPosition like 'Digital Coordinator';

-- new positision RY1 Sales : Sales Product Specialist
INSERT INTO ptoPositions values ('Sales Product Specialist');
INSERT INTO ptoDepartmentPositions values('RY1 Sales', 'Sales Product Specialist', 4, 'no_policy_html');
INSERT INTO ptoAuthorization values ('HR','Director','RY1 Sales','Sales Product Specialist', 4, 5);
-- updated the 3 3 employees thru vision UI


select * 
FROM ptoPositionFulfillment a
INNER JOIN tpemployees b on a.employeenumber = b.employeenumber
WHERE a.position = 'finance manager'

IN ui, UPDATE anthony to finance manager AND make kenneth langenstein the GSM
