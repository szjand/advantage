2/24/15
v3.6
1.
ALL car wash position authorized BY Appearance:Director

SELECT *
FROM ptoDepartmentPositions 
WHERE department = 'car wash'

the positions affected are Cashier, Maintenance Manager, Team Member
for each of those positions, the level changes FROM 5 to 4

IN ptoAuthorization, the authBy changes AS well AS both levels
select *
FROM ptoAuthorization a
WHERE authForDepartment = 'car wash'
  AND authForPosition IN ('Cashier','Maintenance Manager','Team Member') 

2. no Bulding Maintenance positions auth BY Building Maintenace:Manager (Ray)
   auth BY RY1:Fixed Director
     
SELECT *
FROM ptoDepartmentPositions 
WHERE department = 'building maintenance' 

select *
FROM ptoAuthorization a
WHERE authForDepartment = 'building maintenance'


    
BEGIN TRANSACTION;
TRY
-- 1.
UPDATE ptoDepartmentPositions
SET level = 4
WHERE department = 'car wash'
  AND position IN ('Cashier','Maintenance Manager','Team Member');

UPDATE ptoAuthorization
SET authByDepartment = 'Appearance',
    authByPosition = 'Director',
    authByLevel = 3,
    authForLevel = 4
WHERE authForDepartment = 'car wash' 
  AND authForPosition IN ('Cashier','Maintenance Manager','Team Member');   
  
-- 2.
UPDATE ptoDepartmentPositions
SET level = 3
WHERE department = 'Building Maintenance'
  AND position = 'Support';   
  
UPDATE ptoAuthorization
SET authByDepartment = 'RY1',
    authByPosition = 'Fixed Director',
    authByLevel = 2,
    authForLevel = 3
WHERE authForDepartment = 'Building Maintenance'
  AND authForPosition = 'Support';  
  
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;   