-- ATO vs PTO


-- remove positions
-- Also on the org chart, the positions of PDQ Lead Tech, 
--  Assistant Finance Director (can just be changed to Sales & Leasing Professional) 
--  and PT Technician (C) can all be eliminated.  

-- PDQ Lead Tech
/* 
there IS currently no one IN the position of Lead Technician, IN any dept

SELECT *
FROM ptoPositionFulfillment
WHERE department LIKE '%PDQ%'
  AND position = 'Lead Technician'
  
SELECT *
FROM ptoPositionFulfillment
WHERE position = 'Lead Technician'  

select DISTINCT position
FROM ptoPositionFulfillment
ORDER BY position

select department, position, COUNT(*)
FROM ptoPositionFulfillment
GROUP BY department, position
ORDER BY position

SELECT a.*
FROM ptoPositionFulFillment a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN tpemployees c on b.employeenumber = c.employeenumber
WHERE c.lastname = 'rogne'

-- PT Technician (C)
SELECT *
FROM ptoPositionFulfillment
WHERE position LIKE '%PT Technician%'

select * FROM tpemployees WHERE employeenumber = '1108248'

SELECT *
FROM ptoPositions
WHERE position LIKE '%PT Technician%'

change reed (1108248_) to Technician (C)

-- Asst Fin Mgr
change position to Sales & Leasing Professional
SELECT * FROM ptoPositionFulfillment WHERE position = 'Assistant Finance Director'

SELECT * FROM ptoPositionFulfillment WHERE employeenumber = (
select employeenumber FROM tpemployees WHERE lastname = 'garceau' AND firstname = 'chris')

SELECT *
FROM ptoAuthorization
WHERE authforposition = 'Assistant Finance Director'

-- taylor 1/19
Will you please add the position of Parts Runner to Honda Parts? 
The PTO authorizer for this position is the Parts Manager.b
*/

-- therefore

ALTER TABLE ptoPositions
ALTER COLUMN position position cichar(40);
ALTER TABLE ptoDepartmentPositions
ALTER COLUMN position position cichar(40);
ALTER TABLE ptoPositionFulfillment
ALTER COLUMN position position cichar(40);
ALTER TABLE ptoAuthorization
ALTER COLUMN authByPosition authByPosition cichar(40);
ALTER TABLE ptoAuthorization 
ALTER COLUMN authForPosition authForPosition cichar(40);
ALTER TABLE ptoClosure
ALTER COLUMN ancestorPosition ancestorPosition cichar(40);
ALTER TABLE ptoClosure
ALTER COLUMN descendantPosition descendantPosition cichar(40);
  
BEGIN TRANSACTION;
TRY 

  DELETE FROM ptoPositions
  WHERE position = 'Lead Technician'; 
  
  UPDATE ptoPositionFulfillment
  SET position = 'Technician (C)'
  WHERE employeenumber = '1108248';
  
  DELETE FROM ptoPositions
  WHERE position = 'PT Technician (C)';
  
  UPDATE ptoPositions
  SET position = 'Sales & Leasing Professional'
  WHERE position = 'Assistant Finance Director';
  
  
  INSERT INTO ptoPositions (position) values('Parts Runner');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
  values ('RY2 Parts', 'Parts Runner', 3, 'parts_policy_html');
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
  values ('RY2 Parts', 'Manager', 'RY2 Parts', 'Parts Runner',3,4);
  -- no one currently occupies the position  
  
  UPDATE ptoPositions
  SET position = 'Parts Specialist'
  -- SELECT * FROM ptoPositions
  WHERE position = 'Parts';
  
  INSERT INTO ptoPositions (position)
  values ('Support Specialist');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
  values ('Detail', 'Support Specialist', 5, 'detail_policy_html');
  UPDATE ptoPositionFulfillment
  SET position = 'Support Specialist'
  WHERE employeenumber = (
    SELECT employeenumber 
    FROM tpemployees
    WHERE lastname = 'salveson');
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
  values ('Detail','Assistant Manager','Detail','Support Specialist',4,5);  
  
  INSERT INTO ptoPositions (position)
  values ('Floor Supervisor');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
  values ('Detail', 'Floor Supervisor', 4, 'detail_policy_html');
  UPDATE ptoPositionFulfillment
  SET position = 'Floor Supervisor'
  WHERE employeenumber = (
    SELECT employeenumber 
    -- SELECT *
    FROM tpemployees
    WHERE lastname = 'gardner'
      AND firstname = 'john');
    
  UPDATE ptoDepartmentpositions
  SET position = 'Custodian'
  -- SELECT * FROM ptoDepartmentpositions
  WHERE position = 'Custodians/Maintenance';
  
  UPDATE positions
  SET position = 'Receptionist'
  WHERE position = 'Reception';
      
    
  INSERT INTO ptoPositions (position)
  values ('Sales Admin');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
  values ('Office', 'Sales Admin', 5, 'office_policy_html');
  UPDATE ptoPositionFulfillment
  SET position = 'Sales Admin'
  WHERE employeenumber = (
    SELECT employeenumber 
    -- SELECT *
    FROM tpemployees
    WHERE lastname = 'tandeski'
      AND firstname = 'heather');
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
  values('Office','Office Manager','Office','Sales Admin',4,5);   
      
  UPDATE ptopositions
  SET position = 'Accounts Receivable/Payroll Clerk'
--  SELECT * FROM ptopositions
  WHERE position like 'Accounts Receivable/Payroll%';
  
  
  INSERT INTO ptoPositions (position)
  values('Digital Sales Consultant');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
  values ('RY1 Sales', 'Digital Sales Consultant', 4, 'no_policy_html');

  INSERT INTO ptoAuthorization (authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
  values('HR','Director','RY1 Sales','Digital Sales Consultant',4,5);   
  
  UPDATE ptoPositionFulfillment
  SET position = 'Digital Sales Consultant'
  WHERE employeenumber = (
    SELECT employeenumber 
    -- SELECT *
    FROM tpemployees
    WHERE lastname = 'foster'
      AND firstname = 'samuel');
  UPDATE ptoPositionFulfillment
  SET position = 'Digital Sales Consultant'
  WHERE employeenumber = (
    SELECT employeenumber 
    -- SELECT *
    FROM tpemployees
    WHERE lastname = 'homstad'
      AND firstname = 'jared');  
  UPDATE ptoPositionFulfillment
  SET position = 'Digital Sales Consultant'
  WHERE employeenumber = (
    SELECT employeenumber 
    -- SELECT *
    FROM tpemployees
    WHERE lastname = 'seay'
      AND firstname = 'bryn');   
  UPDATE ptoPositionFulfillment
  SET position = 'Digital Sales Consultant'
  WHERE employeenumber = (
    SELECT employeenumber 
    -- SELECT *
    FROM tpemployees
    WHERE lastname = 'trosen'
      AND firstname = 'tanner');     
  UPDATE ptoPositionFulfillment
  SET position = 'Digital Sales Consultant'
  WHERE employeenumber = (
    SELECT employeenumber 
    -- SELECT *
    FROM tpemployees
    WHERE lastname = 'chavez'
      AND firstname = 'jesse');              
  UPDATE ptoPositionFulfillment
  SET position = 'Digital Sales Consultant',
      department = 'RY1 Sales'
  WHERE employeenumber = (
    SELECT employeenumber 
    -- SELECT *
    FROM tpemployees
    WHERE lastname = 'wheeler'
      AND firstname = 'wendi'); 
        
  UPDATE ptopositions
  SET position = 'Warranty Admin'
  WHERE position = 'Warranty Admin - RAC'; 

  INSERT INTO ptoPositions (position)
  values('Service Runner');
  INSERT INTO ptoDepartmentPositions (department,position,level,ptoPolicyHtml)
  values ('RY1 Main Shop', 'Service Runner', 4, 'no_policy_html');
  UPDATE ptoPositionFulfillment
  SET position = 'Service Runner'
  WHERE employeenumber = (
    SELECT employeenumber 
    -- SELECT *
    FROM tpemployees
    WHERE lastname = 'Schwan'
      AND firstname = 'Jason');
  INSERT INTO ptoAuthorization (authByDepartment,authByPosition,authForDepartment,
    authForPosition,authByLevel,authForLevel)
  values('RY1 Service','Director','RY1 Main Shop','Service Runner',3,4);   
  
  UPDATE ptoPositions
  SET position = 'Shipping Specialist'
  WHERE position = 'Shipping & Receiving';
     
  UPDATE ptopositions
  SET position = 'Receptionist'
  WHERE position = 'Reception';
  
-- NOT going to take these on yet 
--  ALTER TABLE ptoDepartmentPositions
--  ADD COLUMN time_off_type cichar(3);
--  ALTER TABLE ptoDepartmentPositions
--  ADD COLUMN eligible_for_payout logical;
  
-- PTO PAYOUT Employees in the following commissioned positions are eligible to 
-- have any remaining PTO at the end of their anniversary year of allocation 
-- paid out to them in a lump sum payment to be added on to their next paycheck, 
-- following their anniversary date: 
-- � Service Advisors � Sales Consultants � Body Shop Estimators � Main Shop Technicians 
-- � Detail Technicians � Body Shop Technicians � Aftermarket Sales Consultants
-- pto position can NOT be the sole indicator of pto payout eligibility, 
-- position does NOT indicate whether a tech IS hourly OR commissioneds
  
  

   
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;
  
BEGIN TRANSACTION;
TRY
  UPDATE ptoDepartmentPositions
  SET level = 5
  WHERE position = 'Sales & Leasing Professional';
  
  UPDATE ptoAuthorization
  SET authByPosition = 'New Car Sales Manager',
      authByLevel = 4,
      authForLevel = 5
  WHERE authForPosition = 'Sales & Leasing Professional';
  
 
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;    