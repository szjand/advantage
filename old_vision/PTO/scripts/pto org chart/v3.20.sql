/*

Jon,

Will you please add the position of Sales Admin to Honda? Jade Tandeski 
should have this title instead of Receptionist. Also, will you please 
have the RY2 Sales Admin, Receptionist and Custodian reporting to the RY2 GSM?

Changes are reflected in the attachment. 

also unassign bryn AND jarod, they are no longer sales consultants,
they have new positions which are ATO
fuck 

*/

BEGIN TRANSACTION;
TRY
-- new position 
INSERT INTO ptoDepartmentPositions 
values('RY2 Sales','Admin', 4, 'no_policy_html');
-- authorized BY RY2 Sales General Sales Manager
INSERT INTO ptoAuthorization
values('RY2 Sales','General Sales Manager','RY2 Sales','Admin', 3, 4);
-- assign jade to new postion
UPDATE ptoPositionFulfillment
SET position = 'Admin'
--select * 
--FROM ptoPositionFulfillment
WHERE employeenumber = (
  SELECT employeenumber
  FROM tpemployees
  WHERE firstname = 'jade'
    AND lastname = 'tandeski');
    
-- move authorization for receptionist AND custodian FROM ben to nick    
UPDATE ptoAuthorization    
--SELECT *
--FROM ptoAuthorization
SET authByDepartment = 'RY2 Sales',
    authByPosition = 'General Sales Manager'
WHERE authForDepartment = 'RY2'
  AND authForPosition = 'Custodian';    
  
UPDATE ptoAuthorization    
--SELECT *
--FROM ptoAuthorization
SET authByDepartment = 'RY2 Sales',
    authByPosition = 'General Sales Manager'
WHERE authForDepartment = 'RY2 Sales'
  AND authForPosition = 'Receptionist';    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    