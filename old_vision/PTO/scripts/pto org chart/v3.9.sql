7/23/15
v3.9

1.  RY1 Main Shop:Porter
        FROM level 5 to level 4
        auth BY RY1 Service Director
        
SELECT a.*, b.firstname, b.lastname
FROM ptoPositionFulfillment a
LEFT JOIN tpEmployees b on a.employeenumber = b.employeenumber
WHERE department = 'RY1 Main Shop'
  AND position = 'Porter'
  
2. remove positions Body Shop: Paint Team Leader, Metal Team Leader, Estimator(B) & Estimator(C)
   rename Body Shop:Estimator(A) to Estimator
   
3.  Body Shop:Parts & Detailer
        FROM level 5 to level 4
        auth BY Body Shop:Manager

-- none of those positions are currently fulfilled
SELECT a.position
FROM ptoDepartmentPositions a
LEFT JOIN ptoPositionFulFillment b on a.department = b.department
  AND a.position = b.position
WHERE a.Department = 'Body Shop'
  AND b.department IS NULL   

SELECT *
FROM ptoPositionFulfillment
WHERE department = 'Body Shop'
  AND position = 'Estimator (A)'

SELECT * 
FROM ptoPositions a
LEFT JOIN ptoDepartmentPositions b on a.position = b.position
WHERE b.position IS NULL 

SELECT a.*, b.firstname, b.lastname
FROM ptoPositionFulfillment a
LEFT JOIN tpEmployees b on a.employeenumber = b.employeenumber
WHERE department = 'Body Shop'
  AND position = 'Detailer'

BEGIN TRANSACTION;
TRY
-- 1.
  UPDATE ptoDepartmentPositions
  SET level = 4
  WHERE department = 'RY1 Main Shop'
    AND position = 'Porter';
  UPDATE ptoAuthorization
  SET authByDepartment = 'RY1 Service',
      authByPosition = 'Director',
      authByLevel = 3,
      authForLevel = 4
  WHERE authForDepartment = 'RY1 Main Shop'
    AND authForPosition = 'Porter';  
--2.
  DELETE 
  FROM ptoPositions
  WHERE position IN (
    SELECT a.position
    FROM ptoDepartmentPositions a
    LEFT JOIN ptoPositionFulFillment b on a.department = b.department
      AND a.position = b.position
    WHERE a.Department = 'Body Shop'
      AND b.department IS NULL);  
  UPDATE ptoPositions
  SET position = 'Estimator'
  WHERE position = 'Estimator (A)';
-- 3.
  UPDATE ptoDepartmentPositions
  SET level = 4
  WHERE department = 'Body Shop'
    AND position = 'Detailer';
  UPDATE ptoAuthorization
  SET authByDepartment = 'Body Shop',
      authByPosition = 'Manager',
      authByLevel = 3,
      authForLevel = 4
  WHERE authForDepartment = 'Body Shop'
    AND authForPosition = 'Detailer';    
  UPDATE ptoDepartmentPositions
  SET level = 4
  WHERE department = 'Body Shop'
    AND position = 'Parts';
  UPDATE ptoAuthorization
  SET authByDepartment = 'Body Shop',
      authByPosition = 'Manager',
      authByLevel = 3,
      authForLevel = 4
  WHERE authForDepartment = 'Body Shop'
    AND authForPosition = 'Parts';    
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  
