/*h
  RY1: General Manager -> Market:General Manager
  RY2:General Manager: delete
  Market:General Manager: authorizes RY2 Sales:Receptionist & RY2:Custodian
*/
BEGIN TRANSACTION;
TRY 
-- remove the rows authfor ry2 main shop:manager, ry2 parts:manager
DELETE 
-- SELECT *
FROM ptoAuthorization
WHERE authForDepartment IN ('RY2 Main Shop','RY2 Parts')
  AND authForPosition = 'Manager'; 

UPDATE ptoDepartmentPositions
SET department = 'Market'
-- SELECT * FROM ptoDepartmentPositions
WHERE department = 'RY1'
  AND position = 'General Manager';

UPDATE ptoAuthorization
SET authBydepartment = 'Market',
    authByposition = 'General Manager'    
-- SELECT * FROM ptoauthorization    
WHERE authfordepartment = 'ry2 sales'
  AND authforposition = 'receptionist';
  
UPDATE ptoAuthorization
SET authBydepartment = 'Market',
    authByposition = 'General Manager'    
-- SELECT * FROM ptoauthorization    
WHERE authfordepartment = 'ry2'
  AND authforposition = 'custodian'; 
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
   
  
  
  
  