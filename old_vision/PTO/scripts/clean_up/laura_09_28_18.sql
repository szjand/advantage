/*
Hi Jon/Afton-

Could you please help me with a few updates in Vision?

-	Cody Larson – Moved from Honda PDQ to Maintenance, effective 9/24 but it won’t let me add him to that job in Vision.

We had a few promotions/moves take place among the leadership team that will need to be updated in Vision.
-	Wyatt Olson – PDQ Assistant Manager replacing Nick Neumann
-	Nick Neumann – Service Drive Manager replacing Ken E
-	Ken Espelund – Main Shop Manager alongside Craig Rogne


For the Building Maintenance Department – Can you please update Shawn Squires to the PTO admin for the below jobs?
-	Assistant Manager
-	Technician

For the RY1 Drive – Can you please update Nick Neumann to the PTO admin for the below jobs?
-	Advisor
-	Courtesy Driver

For the RY1 PDQ – Can you please update Wyatt Olson to the PTO admin for the below jobs?
-	Advisor
-	Cashier
-	Lowerbay/Upperbay Techs

Please let me know if you have any questions.

Thanks!

Laura Roth | Human Resources Manager | Rydell Auto Center | 701-757-5888 (o) |  701-772-8975 (f)
 

*/

SELECT *
FROM ptoemployees
WHERE employeenumber IN ('182713','282713')

SELECT * 
FROM tpemployees
WHERE lastname = 'larson';

UPDATE tpemployees
SET employeenumber = '182713'
WHERE employeenumber = '282713'

UPDATE ptoemployees
SET employeenumber = '182713'
WHERE employeenumber = '282713'

pto_get_new_employees()

SELECT *
FROM ptodepartmentpositions
WHERE department = 'building maintenance'

SELECT *
FROM ptoauthorization
WHERE authbydepartment = 'building maintenance'

SELECT * 
FROM tpemployees

SELECT *
FROM ptopositionfulfillment a
LEFT JOIN ptoemployees b on a.employeenumber = b.employeenumber
WHERE department = 'building maintenance'

-- temp make ben the authorizer
UPDATE ptoauthorization
SET authbydepartment = 'market',
    authbyposition = 'general manager'
WHERE authfordepartment = 'building maintenance'

-- now can remove foote
-- AND now make squires the manager

-- temp make ben the authorizer
UPDATE ptoauthorization
SET authbydepartment = 'building maintenance',
    authbyposition = 'manager'
WHERE authfordepartment = 'building maintenance'

select * FROM ptopositionfulfillment a
LEFT JOIN ptoemployees b on a.employeenumber = b.employeenumber
WHERE department = 'building maintenance'

DELETE 
FROM ptopositionfulfillment
WHERE employeenumber = '147000'

SELECT *
FROM ptoauthorization
WHERE authbydepartment = 'ry1 drive'

SELECT *
FROM ptoauthorization
WHERE authbydepartment = 'ry1 main shop'

-- get rid of inactive position fulfillments
DELETE 
FROM ptopositionfulfillment 
where employeenumber in (
  select employeenumber
  from ptoemployees
  WHERE active = false)



-- FROM script pto_authorizer_termed
  
-- ok, this IS good, shows complete hierarchy AS well AS termed employees
-- DROP TABLE #wtf;
SELECT a.*, b.authfordepartment, b.authforposition,
  c.employeenumber, d.name, d.active
INTO #wtf  
FROM (
  select b.department, b.position, c.lastname, c.firstname, c.employeenumber
  FROM ptoauthorization a
  LEFT JOIN ptoPositionFulFillment b on a.authbydepartment = b.department
    AND a.authbyposition = b.position
  LEFT JOIN tpemployees c on b.employeenumber = c.employeenumber  
  WHERE b.department IS NOT null
  GROUP BY b.department, b.position, c.lastname, c.firstname, c.employeenumber) a
LEFT JOIN ptoauthorization b on a.department = b.authbydepartment
  AND a.position = b.authbyposition
LEFT JOIN ptopositionfulfillment c on b.authfordepartment = c.department
  AND b.authforposition = c.position
LEFT JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber 
  AND d.currentrow = true 
ORDER BY a.department, a.position, b.authforposition  



change emp# IN posful FROM espelund to nick neuman for ry1 dr / manager
change emp# IN posful FROM nick neumann to wyatt olson for ry1 pdq / assistant manager
change dep/pos IN posful for espelund to ry1 main shop / manager
that should be it

SELECT * FROM tpemployees WHERE lastname = 'neumann' AND firstname = 'nickolas';  1102198
SELECT * FROM tpemployees WHERE lastname = 'espelund';  140790
SELECT * FROM tpemployees WHERE lastname = 'olson' AND firstname = 'wyatt';  144788


-- 1 this works becuaseethere are no constraints on a a position being filled
-- by multiple employees
INSERT INTO ptodepartmentpositions values('RY1 Main Shop','Manager',4,'no_policy_html');

UPDATE ptopositionfulfillment
SET position = 'Assistant Manager'
WHERE employeenumber = '144788';

UPDATE ptopositionfulfillment
SET department = 'RY1 Drive', position = 'Manager'
WHERE employeenumber = '1102198';

UPDATE ptopositionfulfillment
SET department = 'RY1 Main Shop', position = 'Manager'
WHERE employeenumber = '140790';

INSERT INTO ptoauthorization values('RY1 Service','Director','RY1 Main Shop','Manager',2,3)