/*
1/30/20
Morning Jon!

Could you please update a couple titles for me in Vision for PTO? 

PDQ Advisors are now called Express Service Representatives at both locations.

Morgan�s BDC Department also has all different names and titles so maybe we just create a separate category rather than having it under �Market�.
Customer Care Center
Customer Care Representatives � Those are the current General Operators � PTO Admin Justin Kilmer
Customer Care Service Specialist - Those are the current Service Operators � PTO Admin Justin Kilmer
Customer Care Sales Specialist � Those are the current Business Development Representatives � PTO Admin Brooke Sutherland
Thanks!
*/

-- PDQ
INSERT INTO ptopositions values('Express Service Representative');

department	position	level	ptoPolicyHtml
RY1 PDQ		Advisor		5		rydell_pdq_advisor_policy_html

department	position	level	ptoPolicyHtml
RY2 PDQ		Advisor		4		honda_pdq_advisors_policy_html

INSERT INTO ptodepartmentpositions values('RY1 PDQ','Express Service Representative',5,'rydell_pdq_advisor_policy_html');
INSERT INTO ptodepartmentpositions values('RY2 PDQ','Express Service Representative',4,'honda_pdq_advisor_policy_html');

INSERT INTO ptoAuthorization
SELECT authByDepartment, authByPosition,authForDepartment,'Express Service Representative',authByLevel,authForLEvel
FROM ptoauthorization a
WHERE authfordepartment in ('RY1 PDQ', 'RY2 PDQ')
  AND authforposition = 'Advisor';
  
SELECT a.*, b.lastname, b.fullname
FROM ptopositionfulfillment a
JOIN tpemployees b on a.employeenumber = b.employeenumber
WHERE a.department IN ('ry1 pdq','ry2 pdq')
  AND a.position = 'advisor'
ORDER BY department, lastname  

  
UPDATE ptoPositionFulfillment
SET position = 'Express Service Representative'
WHERE employeenumber IN(
SELECT a.employeenumber
FROM ptopositionfulfillment a
JOIN tpemployees b on a.employeenumber = b.employeenumber
WHERE a.department IN ('ry1 pdq','ry2 pdq')
  AND a.position = 'advisor'); 
  
-- BDC (Morgan)
Morgan�s BDC Department also has all different names and titles so maybe we just create a separate category rather than having it under �Market�.
Customer Care Center
Customer Care Representative � Those are the current General Operators � PTO Admin Justin Kilmer
Customer Care Service Specialist - Those are the current Service Operators � PTO Admin Justin Kilmer
Customer Care Sales Specialist � Those are the current Business Development Representatives � PTO Admin Brooke Sutherland
Justin will be a Customer Care Lead and Brooke will be a Customer Care Sales Lead.

SELECT * FROM ptoauthorization
WHERE authfordepartment = 'call center'

SELECT * FROM ptoauthorization
WHERE authbyposition = 'marketing manager'

SELECT * FROM ptoauthorization
WHERE authforposition = 'marketing manager'

SELECT * 
FROM ptoPositionFulfillment a
LEFT JOIN tpemployees b on a.employeenumber = b.employeenumber
WHERE a.department = 'market'

SELECT a.lastname, b.*
FROM tpemployees a
JOIN ptopositionfulfillment b on a.employeenumber = b.employeenumber
WHERE lastname IN( 'sutherland','kilmer')
  
INSERT INTO ptodepartments values ('Customer Care Center');
INSERT INTO ptopositions values ('Customer Care Representatives);
INSERT INTO ptopositions values ('Customer Care Service Specialist');
INSERT INTO ptopositions values ('Customer Care Sales Specialist');
INSERT INTO ptopositions values ('Customer Care Sales Lead');
INSERT INTO ptopositions values ('Customer Care Lead');
INSERT INTO ptodepartmentpositions values('Customer Care Center','Customer Care Representative',6,'bdc_policy_html');
INSERT INTO ptodepartmentpositions values('Customer Care Center','Customer Care Service Specialist',6,'bdc_policy_html');
INSERT INTO ptodepartmentpositions values('Customer Care Center','Customer Care Sales Specialist',6,'bdc_policy_html');
INSERT INTO ptodepartmentpositions values('Customer Care Center','Customer Care Sales Lead',5,'bdc_policy_html');
INSERT INTO ptodepartmentpositions values('Customer Care Center','Customer Care Lead',5,'bdc_policy_html');

INSERT INTO ptoAuthorization values ('Market','Marketing Manager','Customer Care Center','Customer Care Sales Lead',4,5);
INSERT INTO ptoAuthorization values ('Market','Marketing Manager','Customer Care Center','Customer Care Lead',4,5);
INSERT INTO ptoAuthorization values ('Customer Care Center','Customer Care Sales Lead','Customer Care Center','Customer Care Sales Specialist',5,6);
INSERT INTO ptoAuthorization values ('Customer Care Center','Customer Care Lead','Customer Care Center','Customer Care Service Specialist',5,6);
INSERT INTO ptoAuthorization values ('Customer Care Center','Customer Care Lead','Customer Care Center','Customer Care Representative',5,6);

-- ok, looks LIKE everything IS IN place
SELECT *
FROM ptodepartmentpositions
WHERE department = 'Customer Care Center'

SELECT *
FROM ptoAuthorization
WHERE authForDepartment = 'Customer Care Center'


SELECT a.authByDepartment, a.authByPosition, b.employeenumber, bb.fullname,
  a.authForDepartment, a.authForPosition, c.employeenumber, cc.fullname
--INTO #wtf  
FROM ptoauthorization a
JOIN ptopositionfulfillment b on a.authByDepartment = b.Department
  AND a.authByPosition = b.position
JOIN tpemployees bb on b.employeenumber = bb.employeenumber  
JOIN ptopositionfulfillment c on a.authForDepartment = c.Department
  AND a.authForPosition = c.position  
JOIN tpemployees cc on c.employeenumber = cc.employeenumber    
WHERE a.authfordepartment = 'call center' OR a.authforposition = 'Business Development Representative';

 
SELECT employeenumber_1, fullname_1, b.*
FROM #wtf a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber_1 = b.employeenumber

UPDATE ptoPositionFulfillment SET department = 'Customer Care Center', position = 'Customer Care Sales Lead' WHERE employeenumber = '256845';
UPDATE ptoPositionFulfillment SET department = 'Customer Care Center', position = 'Customer Care Sales Specialist' WHERE employeenumber IN ('157385','175873');
UPDATE ptoPositionFulfillment SET department = 'Customer Care Center', position = 'Customer Care Lead' WHERE employeenumber = '178079';
UPDATE ptoPositionFulfillment SET department = 'Customer Care Center', position = 'Customer Care Representative' WHERE employeenumber IN ('137593','137854','175342','197643');
UPDATE ptoPositionFulfillment SET department = 'Customer Care Center', position = 'Customer Care Service Specialist' WHERE employeenumber IN ('1149710','132987','137837','149878','16250');

SELECT a.authByDepartment, a.authByPosition, b.employeenumber, bb.fullname,
  a.authForDepartment, a.authForPosition, c.employeenumber, cc.fullname
--INTO #wtf  
FROM ptoauthorization a
JOIN ptopositionfulfillment b on a.authByDepartment = b.Department
  AND a.authByPosition = b.position
JOIN tpemployees bb on b.employeenumber = bb.employeenumber  
JOIN ptopositionfulfillment c on a.authForDepartment = c.Department
  AND a.authForPosition = c.position  
JOIN tpemployees cc on c.employeenumber = cc.employeenumber    
WHERE a.authfordepartment = 'Customer Care Center'









---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

Will you please add the role of Systems Engineer to the PTO org structure? 
For the time being, he will report directly to Ben Cahalan. 
If he needs to be associated with a department, he should go to IT for now.

INSERT INTO ptoPositions(position)
values('Systems Engineer');
INSERT INTO ptoDepartmentPositions(department,position,level,ptopolicyhtml)
values('IT','Systems Engineer',4, 'no_policy_html');
INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('RY1','General Manager','IT','Systems Engineer',3,4);  

-- Hobby Shop
-- assume doug peterson to be manager
INSERT INTO ptodepartments (department) values ('Hobby Shop');

INSERT INTO ptoDepartmentPositions(department,position,level,ptopolicyhtml)
values('Hobby Shop','Manager',3, 'no_policy_html');

INSERT INTO ptoDepartmentPositions(department,position,level,ptopolicyhtml)
values('Hobby Shop','Technician',4, 'no_policy_html');

-- don't know who manages doug ...
INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('Hobby Shop','Manager','Hobby Shop','Technician',3,4); 

-- 9/4/18
Name:	Nicholas Ladwig
Phone #:	
Title:	Business Development Representative
What is a �Business Development Representative�?
Good question!  It�s a new position that will be reporting to Morgan but will be very 
similar to our old Internet Sales position.  So access wise he will need whatever sales people have.

INSERT INTO ptoPositions(position)
values('Business Development Representative');
INSERT INTO ptoDepartmentPositions(department,position,level,ptopolicyhtml)
values('Market','Business Development Representative',4, 'no_policy_html');
INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('Market','Marketing Manager','Market','Business Development Representative',3,4);  


-- 9/5/18
Hi Jon-

We hired Romao Maresca into a Parts Assistant Manager, under Dan Stinar.  Could you add that position into Vision for me?

INSERT INTO ptoDepartmentPositions(department,position,level,ptopolicyhtml)
values('RY1 Parts','Assistant Manager',4, 'parts_policy_html');

INSERT INTO ptoAuthorization(authByDepartment,authByPosition,authForDepartment,
  authForPosition,authByLevel,authForLevel)
values('RY1 Parts','Manager','RY1 Parts','Assistant Manager',3,4); 