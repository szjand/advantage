-- this should be ALL employees eligible for pto payout
select x.* 
into #eligible 
FROM (
  SELECT a.userid, a.firstname, a.lastname, a.supervisorname, a.dateofhire, a.title
  FROM pto_compli_users a
  WHERE status = '1'
    AND title in (
      'team 1 sales consultant', -- ry2 sc
      'team 2 sales consultant', -- ry2 sc
      'estimator', -- bs estimator
      'used car sales consultant', -- ry1 sc
      'new car sales consultant', -- ry1 sc
      'main shop advisor', -- ry1/2 writer
      'detail technician' -- r1 detail flat rate
      )
  UNION -- team pay techs  
  SELECT a.employeenumber, a.firstname, a.lastname, c.supervisorname, c.dateofhire, c.title
  FROM tpTechs a
  INNER JOIN tpTeamTechs b on a.techkey = b.techkey
    AND b.thrudate > curdate()
  LEFT JOIN pto_compli_users c on a.employeenumber = c.userid) x 
LEFT JOIN dds.edwEmployeeDim d on x.userid = d.employeenumber
  AND d.currentrow = true
  AND d.fullparttime = 'part'
WHERE d.employeenumber IS NULL ORDER BY supervisorname, title

-- pto period expired (thru this coming sunday)
SELECT a.userid, a.firstname, a.lastname, a.supervisorname, b.thrudate, b.hours, 
  coalesce(SUM(c.hours), 0) AS used, b.hours - coalesce(SUM(c.hours), 0) AS payout_due
-- SELECT
FROM #eligible a
inner JOIN pto_employee_pto_allocation b on a.userid = b.employeenumber
  AND b.thrudate <= '02/15/2015'
  AND hours > 0
LEFT JOIN pto_used c on a.userid = c.employeenumber
  AND c.theDate BETWEEN b.fromdate AND b.thrudate  
GROUP BY a.userid, a.firstname, a.lastname, a.supervisorname, b.thrudate, b.hours  

-- nobody coming up next week
SELECT a.userid, a.firstname, a.lastname, a.supervisorname, b.thrudate, b.hours, coalesce(SUM(c.hours), 0) AS used
-- SELECT *
FROM #eligible a
inner JOIN pto_employee_pto_allocation b on a.userid = b.employeenumber
  AND b.thrudate BETWEEN '02/15/2015' AND '02/21/2015'
  AND hours > 0
LEFT JOIN pto_used c on a.userid = c.employeenumber
  AND c.theDate BETWEEN b.fromdate AND b.thrudate  
GROUP BY a.userid, a.firstname, a.lastname, a.supervisorname, b.thrudate, b.hours 

-- basic eligibility
SELECT a.userid, a.firstname, a.lastname, a.dateofhire AS compli_hiredate, 
  b.ptoAnniversary AS vision_anniv, a.title AS compli_position, 
  trim(c.department) + ':' + c.position AS vision_position
FROM #eligible a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
LEFT JOIN ptoPositionFulfillment c on a.userid = c.employeenumber
ORDER BY lastname, firstname

select * FROM #eligible

-- the whole magilla for foster
-- basic eligibility
SELECT a.userid, a.firstname, a.lastname, a.dateofhire AS compli_hiredate, 
  b.ptoAnniversary AS vision_anniv, a.title AS compli_position, 
  trim(c.department) + ':' + c.position AS vision_position,
  a.supervisorname AS compli_authorizer,
  TRIM(g.firstname) + ' ' + g.lastname AS vision_authorizer,
  d.cur_per_from, d.cur_per_thru, d.hours, d.used
FROM #eligible a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
LEFT JOIN ptoPositionFulfillment c on a.userid = c.employeenumber
--LEFT JOIN pto_employee_pto_allocation d on a.userid = d.employeenumber
--  AND curdate() BETWEEN d.fromdate AND d.thrudate
LEFT JOIN ( -- hours AND used
  SELECT a.employeenumber, a.fromdate cur_per_from, a.thrudate AS cur_per_thru, a.hours, coalesce(SUM(b.hours), 0) AS used 
  FROM pto_employee_pto_allocation a
  LEFT JOIN pto_used b on a.employeenumber = b.employeenumber
    AND b.thedate BETWEEN a.fromdate AND a.thrudate  
  WHERE curdate() BETWEEN a.fromdate AND a.thrudate
  GROUP BY a.employeenumber, a.fromdate, a.thrudate, a.hours) d on a.userid = d.employeenumber 
LEFT JOIN ptoAuthorization e on c.department = e.authForDepartment
  AND c.position = e.authForPosition
LEFT JOIN ptoPositionFulfillment f on e.authByDepartment = f.department
  AND e.authByPosition = f.position
LEFT JOIN tpEmployees g on f.employeenumber = g.employeenumber      
ORDER BY cur_per_thru  




-- uh oh, kevin hanson does NOT have access to pto
SELECT *
FROM employeeappauthorization
WHERE username = (
  SELECT username
  FROM tpemployees
  WHERE fullname = 'kevin hanson')
  
-- ptoEmployees without access to pto  
SELECT *
FROM ptoEmployees a
LEFT JOIN tpemployees b on a.employeenumber = b.employeenumber
WHERE NOT EXISTS (
  SELECT 1
  FROM employeeAppAuthorization
  WHERE username = b.username
    AND appcode = 'pto')
    
-- ptoEmployees with no vision access at all        
select *
FROM ptoEmployees a
LEFT JOIN tpEmployees b on a.employeenumber = b.employeenumber
WHERE b.employeenumber IS NULL   

