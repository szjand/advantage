-- those employees that get unused pto paid out 

-- FROM compli
SELECT a.userid, a.firstname, a.lastname, a.supervisorname, a.dateofhire, a.title
FROM pto_compli_users a
WHERE status = '1'
  AND title in (
    'sales consultant', -- ry2 sc
    'estimator', -- bs estimator
    'used car sales consultant', -- ry1 sc
    'new car sales consultant', -- ry1 sc
    'main shop advisor', -- ry1/2 writer
    'detail technician' -- r1 detail flat rate
    )

-- FROM team pay    
SELECT a.employeenumber, a.firstname, a.lastname, c.supervisorname, c.dateofhire, c.title
FROM tpTechs a
INNER JOIN tpTeamTechs b on a.techkey = b.techkey
  AND b.thrudate > curdate()
LEFT JOIN pto_compli_users c on a.employeenumber = c.userid  


-- this should be ALL employees eligible for pto payout
select x.* FROM (
SELECT a.userid, a.firstname, a.lastname, a.supervisorname, a.dateofhire, a.title
FROM pto_compli_users a
WHERE status = '1'
  AND title in (
    'team 1 sales consultant', -- ry2 sc
    'team 2 sales consultant', -- ry2 sc
    'estimator', -- bs estimator
    'used car sales consultant', -- ry1 sc
    'new car sales consultant', -- ry1 sc
    'main shop advisor', -- ry1/2 writer
    'detail technician' -- r1 detail flat rate
    )
UNION     
SELECT a.employeenumber, a.firstname, a.lastname, c.supervisorname, c.dateofhire, c.title
FROM tpTechs a
INNER JOIN tpTeamTechs b on a.techkey = b.techkey
  AND b.thrudate > curdate()
LEFT JOIN pto_compli_users c on a.employeenumber = c.userid) x 
LEFT JOIN dds.edwEmployeeDim d on x.userid = d.employeenumber
  AND d.currentrow = true
  AND d.fullparttime = 'part'
WHERE d.employeenumber IS NULL ORDER BY supervisorname, title

  
-- name match with arkona ok, no terms
SELECT * FROM (
SELECT d.*, trim(j.firstname) + ' ' + j.lastname AS ark_name, j.termdate,
  j.payrollclass, j.fullparttime
FROM (
  SELECT a.userid, trim(a.firstname) + ' ' + a.lastname AS compli_name, 
  a.dateofhire AS compli_anniv, a.title AS compli_job,
  a.supervisorname AS compli_super     
  FROM pto_compli_users a
  WHERE status = '1'
    AND title in (
      'team 1 sales consultant', -- ry2 sc
      'team 2 sales consultant', -- ry2 sc
      'estimator', -- bs estimator
      'used car sales consultant', -- ry1 sc
      'new car sales consultant', -- ry1 sc
      'main shop advisor', -- ry1/2 writer
      'detail technician' -- r1 detail flat rate
      )
  UNION     
  SELECT a.employeenumber, trim(a.firstname) + ' ' + a.lastname, 
    c.dateofhire, c.title, c.supervisorname
  FROM tpTechs a
  INNER JOIN tpTeamTechs b on a.techkey = b.techkey
    AND b.thrudate > curdate()
  LEFT JOIN pto_compli_users c on a.employeenumber = c.userid) d
LEFT JOIN ptoPositionFulfillment e on d.userid = e.employeenumber  
LEFT JOIN ptoAuthorization f on e.department = f.authForDepartment
  AND e.position = f.authForPosition
LEFT JOIN ptoPositionFulfillment g on f.authByDepartment = g.department
  AND f.authByPosition = g.position
LEFT JOIN ptoEmployees h on g.employeenumber = h.employeenumber  
LEFT JOIN tpEmployees i on h.employeenumber = i.employeenumber
LEFT JOIN dds.edwEmployeeDim j on d.userid = j.employeenumber
  AND j.currentrow = true
-- ORDER BY userid
) x WHERE compli_name <> ark_name

-- anniv discrepancies
SELECT * FROM (
SELECT d.*, TRIM(e.department) + ':' + e.position AS vision_job, j.ptoAnniversary AS vision_anniv, k.ymhdte, k.ymhdto
-- SELECT *
FROM (
  SELECT a.userid, trim(a.firstname) + ' ' + a.lastname AS compli_name, 
  a.dateofhire AS compli_anniv, a.title AS compli_job,
  a.supervisorname AS compli_super     
  FROM pto_compli_users a
  WHERE status = '1'
    AND title in (
      'team 1 sales consultant', -- ry2 sc
      'team 2 sales consultant', -- ry2 sc
      'estimator', -- bs estimator
      'used car sales consultant', -- ry1 sc
      'new car sales consultant', -- ry1 sc
      'main shop advisor', -- ry1/2 writer
      'detail technician' -- r1 detail flat rate
      )
  UNION     
  SELECT a.employeenumber, trim(a.firstname) + ' ' + a.lastname, 
    c.dateofhire, c.title, c.supervisorname
  FROM tpTechs a
  INNER JOIN tpTeamTechs b on a.techkey = b.techkey
    AND b.thrudate > curdate()
  LEFT JOIN pto_compli_users c on a.employeenumber = c.userid) d
LEFT JOIN ptoPositionFulfillment e on d.userid = e.employeenumber  
LEFT JOIN ptoAuthorization f on e.department = f.authForDepartment
  AND e.position = f.authForPosition
LEFT JOIN ptoPositionFulfillment g on f.authByDepartment = g.department
  AND f.authByPosition = g.position
LEFT JOIN ptoEmployees h on g.employeenumber = h.employeenumber  
LEFT JOIN tpEmployees i on h.employeenumber = i.employeenumber
LEFT JOIN ptoEmployees j on d.userid = j.employeenumber
LEFT JOIN dds.stgArkonaPYMAST k on j.employeenumber = k.ymempn
) x WHERE compli_anniv <> vision_anniv

select * from (-- super discrepancy
SELECT d.*, TRIM(e.department) + ':' + e.position AS vision_job, 
  j.ptoAnniversary AS vision_anniv, TRIM(i.firstname) + ' ' + i.lastname AS vision_super
-- SELECT *
FROM (
  SELECT a.userid, trim(a.firstname) + ' ' + a.lastname AS compli_name, 
  a.dateofhire AS compli_anniv, a.title AS compli_job,
  a.supervisorname AS compli_super     
  FROM pto_compli_users a
  WHERE status = '1'
    AND title in (
      'team 1 sales consultant', -- ry2 sc
      'team 2 sales consultant', -- ry2 sc
      'estimator', -- bs estimator
      'used car sales consultant', -- ry1 sc
      'new car sales consultant', -- ry1 sc
      'main shop advisor', -- ry1/2 writer
      'detail technician' -- r1 detail flat rate
      )
  UNION     
  SELECT a.employeenumber, trim(a.firstname) + ' ' + a.lastname, 
    c.dateofhire, c.title, c.supervisorname
  FROM tpTechs a
  INNER JOIN tpTeamTechs b on a.techkey = b.techkey
    AND b.thrudate > curdate()
  LEFT JOIN pto_compli_users c on a.employeenumber = c.userid) d
LEFT JOIN ptoPositionFulfillment e on d.userid = e.employeenumber -- emp position 
LEFT JOIN ptoAuthorization f on e.department = f.authForDepartment
  AND e.position = f.authForPosition
LEFT JOIN ptoPositionFulfillment g on f.authByDepartment = g.department -- auth position
  AND f.authByPosition = g.position
LEFT JOIN ptoEmployees h on g.employeenumber = h.employeenumber -- super email 
LEFT JOIN tpEmployees i on h.employeenumber = i.employeenumber -- super name
LEFT JOIN ptoEmployees j on d.userid = j.employeenumber -- emp email
) x where compli_super <> vision_super



-- payout eligible folks that are currently part timers
SELECT * FROM (
SELECT d.*, trim(j.firstname) + ' ' + j.lastname AS ark_name, j.termdate,
  j.payrollclass, j.fullparttime
FROM (
  SELECT a.userid, trim(a.firstname) + ' ' + a.lastname AS compli_name, 
  a.dateofhire AS compli_anniv, a.title AS compli_job,
  a.supervisorname AS compli_super     
  FROM pto_compli_users a
  WHERE status = '1'
    AND title in (
      'team 1 sales consultant', -- ry2 sc
      'team 2 sales consultant', -- ry2 sc
      'estimator', -- bs estimator
      'used car sales consultant', -- ry1 sc
      'new car sales consultant', -- ry1 sc
      'main shop advisor', -- ry1/2 writer
      'detail technician' -- r1 detail flat rate
      )
  UNION     
  SELECT a.employeenumber, trim(a.firstname) + ' ' + a.lastname, 
    c.dateofhire, c.title, c.supervisorname
  FROM tpTechs a
  INNER JOIN tpTeamTechs b on a.techkey = b.techkey
    AND b.thrudate > curdate()
  LEFT JOIN pto_compli_users c on a.employeenumber = c.userid) d
LEFT JOIN ptoPositionFulfillment e on d.userid = e.employeenumber  
LEFT JOIN ptoAuthorization f on e.department = f.authForDepartment
  AND e.position = f.authForPosition
LEFT JOIN ptoPositionFulfillment g on f.authByDepartment = g.department
  AND f.authByPosition = g.position
LEFT JOIN ptoEmployees h on g.employeenumber = h.employeenumber  
LEFT JOIN tpEmployees i on h.employeenumber = i.employeenumber
LEFT JOIN dds.edwEmployeeDim j on d.userid = j.employeenumber
  AND j.currentrow = true
  AND j.active = 'active'
-- ORDER BY userid
) x WHERE fullparttime = 'part'


-- holy fuck buckets, nick shirek IS no long directly responsible for sales 
-- consultants at ry2, he IS a manager IN training
-- sales consultants report to either john o OR dan l
