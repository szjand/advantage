/*
2/12/15

i am freaking fucking out
this will NOT be maintainable AS long AS generating a list of payout eligible
employees depends on this fucking hardcoded shit
there IS no consistency BETWEEN compli AND arkona AND vision IN terms of positions
so IF the fucking pto org chart changes, i need to "remember" to UPDATE this 
fucking code

first thought: ADD payout eligible AS an attribute of ptoEmployee
  why IS that any more maintainable, it really isn't
maybe AS an attribute of ptoDepartmentPositions
  seems to make a little more sense
  except it won't WORK for techs, there IS nothing IN pto hierarchy that indicates
  flat rate OR hourly
  
fuck me, i don't know what to DO, wa wa wa wa boo hoo  
*/



-- DROP TABLE pto_payout_eligible;
CREATE TABLE pto_payout_eligible (
  employee_number cichar(9) constraint NOT NULL,
  employee_name cichar(52) constraint NOT NULL,
  employee_email cichar(50) constraint NOT NULL,
  anniversary_date date constraint NOT NULL, 
  current_pto_period_from date constraint NOT NULL,
  current_pto_period_thru date constraint NOT NULL,
  pto_hours double constraint NOT NULL,
  pto_used double constraint NOT NULL,
  pto_unused double constraint NOT NULL, 
  authorizer cichar(52) constraint NOT NULL,
  authorizer_email cichar(50) constraint NOT NULL,
  authorizer_authorizer cichar(52) constraint NOT NULL,
  authorizer_authorizer_email cichar(50) constraint NOT NULL,
  constraint pk primary key (employee_number)) IN database;
  
  
-- fuck it  
INSERT INTO pto_payout_eligible
SELECT d.*, e.username, e.ptoAnniversary, f.cur_per_from, f.cur_per_thru, 
  f.hours, f.used, f.hours - f.used, 
  TRIM(k.firstname) + ' ' + k.lastname, j.username,
  TRIM(p.firstname) + ' ' + p.lastname, o.username
FROM ( -- emp#, name
  SELECT a.userid, trim(a.firstname) + ' ' + a.lastname
  FROM pto_compli_users a
  WHERE status = '1'
    AND title in (
      'team 1 sales consultant', -- ry2 sc
      'team 2 sales consultant', -- ry2 sc
      'estimator', -- bs estimator
      'used car sales consultant', -- ry1 sc
      'new car sales consultant', -- ry1 sc
      'main shop advisor', -- ry1/2 writer
      'detail technician') -- r1 detail flat rate
  UNION -- + team pay techs    
  SELECT a.employeenumber, trim(a.firstname) + ' ' + a.lastname
  FROM tpTechs a
  INNER JOIN tpTeamTechs b on a.techkey = b.techkey
    AND b.thrudate > curdate()
  LEFT JOIN pto_compli_users c on a.employeenumber = c.userid) d
INNER JOIN ptoEmployees e on d.userid = e.employeenumber  -- username, anniv
LEFT JOIN ( -- cur_period, hours AND used
  SELECT a.employeenumber, a.fromdate cur_per_from, a.thrudate AS cur_per_thru, a.hours, coalesce(SUM(b.hours), 0) AS used 
  FROM pto_employee_pto_allocation a
  LEFT JOIN pto_used b on a.employeenumber = b.employeenumber
    AND b.thedate BETWEEN a.fromdate AND a.thrudate  
  WHERE curdate() BETWEEN a.fromdate AND a.thrudate
  GROUP BY a.employeenumber, a.fromdate, a.thrudate, a.hours) f on d.userid = f.employeenumber 
LEFT JOIN ptoPositionFulfillment g on d.userid = g.employeenumber -- emp pos
LEFT JOIN ptoAuthorization h on g.department = h.authForDepartment -- emp pos -> auth pos
  AND g.position = h.authForPosition
LEFT JOIN ptoPositionFulfillment i on h.authByDepartment = i.Department -- auth pos
  AND h.authByPosition = i.Position
LEFT JOIN ptoEmployees j on i.employeenumber = j.employeenumber -- auth emp# & email
LEFT JOIN dds.edwEmployeeDim k on j.employeenumber = k.employeenumber -- auth name
  AND k.currentrow = true  
LEFT JOIN ptoPositionFulfillment l on j.employeenumber = l.employeenumber -- auth emp#
LEFT JOIN ptoAuthorization m on l.department = m.authForDepartment -- auth pos -> auth_auth pos
  AND l.position = m.authForPosition 
LEFT JOIN ptoPositionFulfillment n on m.authByDepartment = n.department -- auth_auth position
  AND m.authByPosition = n.position
LEFT JOIN ptoEmployees o on n.employeenumber = o.employeenumber -- auth_auth emp# & email  
LEFT JOIN dds.edwEmployeeDim p on o.employeenumber = p.employeenumber
  AND p.currentrow = true;
  
SELECT curdate(), curdate() + 6 FROM system.iota;
 
select *
FROM pto_payout_eligible
ORDER BY current_pto_period_thru

SELECT *
FROM pto_payout_eligible
WHERE current_pto_period_thru BETWEEN curdate() AND '03/31/2015'
  AND pto_hours > 0

SELECT employee_email, 'You may be eligible for PTO payout.'
  + char(13) + char(10) + 'You''re current PTO expires on '
  + cast(current_pto_period_thru AS sql_char)+ '.  ' + char(13) + char(10)
  + 'Records show that you have ' + trim(CAST(pto_unused AS sql_char))
  + ' unused hours.'
   + char(13) + char(10) + 'Please see ' + TRIM(authorizer) + '.' AS msg
FROM pto_payout_eligible
WHERE current_pto_period_thru BETWEEN '01/01/2015' AND '03/31/2015'
  AND pto_hours > 0
  
  
SELECT COUNT(*)   