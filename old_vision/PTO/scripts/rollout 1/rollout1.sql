alter PROCEDURE pto_insert_new_employee
   ( 
      employeeNumber CICHAR ( 9 ),
      department CICHAR ( 30 ),
      position CICHAR ( 30 ),
      ptoHoursEarned DOUBLE ( 15 ),
      ptoAnniversaryDate DATE,
      status CICHAR (4) OUTPUT,
      text cichar(500) output,
      username cichar(50) output,
      password cichar(8) output,
      mailError logical output
   ) 
BEGIN
/*
this needs a fuck of a lot of WORK:
  1. mandatory relationships
  2. WHERE the fuck am i going to get username
  
10/20/14
  tables to be populated:
    ptoEmployees
    ptoPositionFulfillment
    pto_employee_pto_allocation
oh shit, also need the employeeAppAuthorization for pto apps  
which means they need to be IN tpEmployees which ideally means
tpEmployees.username AND ptoEmployees.username are the same  
but ain't yet the truth


oh fuck me, ptoEmployees knows squat about passwords
enuf for tonight

SELECT *
FROM tpEmployees a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
WHERE a.username <> b.username

EXECUTE PROCEDURE pto_insert_new_employee (
'163099',
'ry1 main shop',
'technician (c)',
0,
'09/30/2014');

-- jay olson test of conflicting usernames - that worked
DELETE FROM ptoEmployees WHERE employeenumber = '1106399';

EXECUTE PROCEDURE pto_insert_new_employee (
'163099',
'ry1 main shop',
'technician (c)',
0,
'09/30/2014');


errors:
  position IS a ptoAdmin that IS already populated
  
DELETE FROM ptoEmployees WHERE employeeNumber = '163099'

need to detect possible transfer
need to expose unhandled error IN text

ahh fuck CASE:
  emp EXISTS IN tpEmployees BY employeenumber, but does NOT yet exist IN pto
  username IN tpEmployees IS xxx@rydellchev.
  fuck it: the basic issue IS synchronizing tpEmployees.username with compli.userid
  
DELETE FROM ptoEmployees WHERE employeenumber IN ('264235','163099','161010','196525','1147325','111210','1135340');
DELETE FROM employeeAppAuthorization WHERE username IN (
  SELECT username 
  FROM tpEmployees 
  WHERE employeenumber IN (
    '264235','163099','161010','196525','1147325','111210','1135340'));
DELETE FROM tpEmployees WHERE employeenumber IN ('264235','163099','161010','196525','1147325','111210','1135340');   
DELETE FROM pto_employee_pto_allocation WHERE employeenumber IN ('264235','163099','161010','196525','1147325','111210','1135340');   

10/24/14 OPEN items
  what about the transfers
  setting anniv date to future - done IN stored proc, could possible be client side
  
*/  
 
DECLARE @employeeNumber string;
DECLARE @department string;
DECLARE @position string;
DECLARE @ptoHoursEarned double;
DECLARE @ptoAnniversaryDate date;
DECLARE @anniversaryType string;
DECLARE @userName string;
DECLARE @appRole string;

DECLARE @i integer;
DECLARE @pos1 integer;
DECLARE @pos2 integer;
DECLARE @password string;
DECLARE @str string;


@employeenumber = (SELECT trim(employeeNumber) FROM __input);
@department = (SELECT department FROM __input);
@position = (SELECT position FROM __input);
@ptoHoursEarned = (SELECT ptoHoursEarned FROM __input);
@ptoAnniversaryDate = (SELECT ptoAnniversaryDate FROM __input);

@anniversaryType = (
  SELECT 
    CASE 
      WHEN @ptoAnniversaryDate = a.ymhdte THEN 'Latest'
      WHEN @ptoAnniversaryDate = a.ymhdto THEN 'Original'
      ELSE 'Other'
    END 
  FROM dds.stgArkonaPYMAST a
  WHERE ymempn = @employeeNumber);
  
@userName = (
  SELECT email
  FROM pto_compli_users
  WHERE userid = @employeeNumber);   
  
@str = 'LoremXesumdoLorspnametconsecteturadXeYacWngeLpnDonecetorcWanuncmattYa'+
  'auctorVWvamusWacuLYanonLacusegetmattYaCrasetLacWnWadoLorPeLLentesquefacWLY'+
  'aYaveLpnduWacvarWussemconsecteturquYaAeneanWnterdumquamatLWberopharetraWnt'+
  'erdumMaecenasspnametuLtrWcWesnuncVWvamusmaLesuadatortorposuereLuctusdapWbu'+
  'sVestWbuLumaWnterdumquamSuspendYaseactemporLectusCrasmattYaanteveLsuscXepn'+
  'semperodWoveLpnLaoreetteLLusveLsoLLWcpnudWnquamerosaLeoALWquamdapWbusnuncL'+
  'WberononconguejustovuLputatevpnaePhaseLLusnuLLaarcumaLesuadaWnterdummoLest'+
  'WeutconsequatettortorSeddWctumnequeegetnYaWLaoreetcongueProWnrhoncusrutrum'+
  'congueMaecenasvBLoremsagpntYavWverrafeLYadWgnYasWmbWbendumLectuQuYaquetWnc'+
  'vBuntauctormagnaconvaLLYavestWbuLumMaurYapuLvWnaretmetusnonhendrerpnNuncbW'+
  'bendumsemdWctumpLaceratposuerenuncveLpnaccumsanrYausacvuLputateXesumtortor'+
  'spnametmassaFusceuLtrWcWeseratnoneLeWfendcommodoNuLLanonerosvBjustotWncvBu'+
  'ntportaanonnuLLaPraesentmattYafeugWattWncvBuntALWquamsedsuscXepnodWoQuYaqu'+
  'evenenatYautarcuegetvenenatYaVestWbuLumuLtrWcWesadXeYacWngvuLputateNuLLase'+
  'dLacusLoremVWvamusLacWnWasceLerYaquerYausacsodaLesmaurYaconsequatnonSedtWn'+
  'cvBuntatmaurYavBdapWbusDuYautteLLusenWmCurabpnurfermentumerosacLWguLahendr'+
  'erpnfeugWatcondWmentumturpYatWncvBuntPhaseLLusvestWbuLumsapWenegetornarevo'+
  'LutpatUtpeLLentesqueturpYanWbhnecvuLputateteLLusLobortYavpnaePraesentcursu'+
  'snuLLauteratpretWumpretWumVestWbuLumfeugWattortorvBporttpnorvWverraodWonYa'+
  'WmoLestWeorcWvpnaeWnterdumdoLorLWberovBsapWenQuYaquevpnaeconguemassaeugrav'+
  'vBasemUtcongueWnsemetmoLLYaNuLLamnonmagnanuncSedvBcommodoLWguLaEtWamquYava'+
  'rWussapWenNuncvBsemenWmSuspendYaseLacWnWanecodWoatsodaLesVWvamussedduWvuLp'+
  'utatevenenatYapurusdapWbusmoLestWeteLLusMaurYaveLveLpnquYaLWguLacongueeuYa'+
  'modegetetnequeSuspendYasenonLacWnWaLoremvBmattYaarcuPraesentseddoLornWbhCu'+
  'rabpnurconsecteturveLmWquYaWacuLYaLoremXesumdoLorspnametconsecteturadXeYac'+
  'WngeLpnInachendrerpnsapWenWndWctumLoremMorbWsedbLandpnteLLusPeLLentesquegr'+
  'avvBatemporpuruscommodoWacuLYaQuYaqueLobortYatempusLoremspnametLuctusSuspe'+
  'ndYasesemtortorornareWnodWospnametconsequatornarequam';
  
  @i = (SELECT frac_second(now()) FROM system.iota);
  @pos1 = (SELECT cast(right(trim(cast(rand(@i) AS sql_char)), 3) AS sql_integer) FROM system.iota) ;
  @pos2 = 2 * (SELECT cast(right(trim(cast(rand(@pos1) AS sql_char)), 3) AS sql_integer) FROM system.iota) ;
  @password = (
    SELECT substring(@str, @pos1, 3) + left(CAST(@i AS sql_char), 2) + 
      upper(substring(@str, @pos2, 1)) + substring(@str, @pos1 +@i, 2)
--  FROM t1);
    FROM system.iota);
--  SELECT @password FROM system.iota;
  
BEGIN TRANSACTION;
TRY 
--< show stoppers
  IF NOT EXISTS (
    SELECT 1
    FROM dds.edwEmployeeDim 
    WHERE employeenumber = @employeenumber
      AND currentrow = true) THEN 
    RAISE show_stoppers(1, 'Employee Number ' + @employeenumber + ' is invalid');
  ELSEIF NOT EXISTS (
    SELECT 1
    FROM ptoDepartments
    WHERE department = @department) THEN 
    RAISE show_stoppers(2, 'Department ' + TRIM(@department) + ' is invalid');
  ELSEIF NOT EXISTS (
    SELECT 1
    FROM ptoPositions
    WHERE position = @position) THEN 
    RAISE show_stoppers(3, 'Position ' + TRIM(@position) + ' is invalid');
  ELSEIF @ptoHoursEarned < 0 THEN
    RAISE show_stoppers(4, 'PTO Earned can not be less than 0'); 
  ELSEIF @ptoHoursEarned > 200 THEN
    RAISE show_stoppers(5, 'PTO Earned can not be greater than 200');  
  ELSEIF EXISTS (
    SELECT 1 
    FROM ptoPositionFulfillment a
    INNER JOIN ptoAuthorization b on a.department = b.authByDepartment
      AND a.position = b.authByPosition
    WHERE a.department = @department
      AND a.position = @position) THEN 
    RAISE show_stoppers(6, TRIM(@department) + ':' + TRIM(@position) + ' is already filled' collate ads_default_ci);
  ELSEIF @ptoAnniversaryDate > curdate() THEN
    RAISE show_stoppers(7, 'Sorry, we can not allow you to enter a future date');          
  ENDIF;    
--/> show stoppers  
-- new row IN ptoEmployees 
  TRY 
    INSERT INTO ptoEmployees (employeeNumber, ptoAnniversary, username, anniversaryType)
    values(@employeenumber, @ptoAnniversaryDate, @username, @anniversaryType);
  CATCH ALL
    RAISE ptoEmployees(101, 'Unable to insert ' + @employeenumber + ' into ptoEmployees');
  END TRY;  
-- new row IN ptoPosistionFulfillment
  TRY 
    INSERT INTO ptoPositionFulfillment (department, position, employeeNumber)
    values (@department, @position, @employeeNumber);
  CATCH ALL
    RAISE ptoPositionFulfillment(102, 'Unable to insert ' + @employeenumber + ' into ptoPositionFulfillment');
  END TRY;     
-- new rows IN pto_employee_pto_allocation
  TRY   
    INSERT INTO pto_employee_pto_allocation (employeeNumber, fromdate, thrudate, hours)
    SELECT @employeeNumber, @ptoAnniversaryDate, 
      cast(timestampadd(sql_tsi_year, 1, @ptoAnniversaryDate) AS sql_date) - 1,      
      CASE @ptoHoursEarned
        WHEN 0 THEN (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 0)
        ELSE @ptoHoursEarned
      END 
    FROM system.iota
    UNION ALL 
    SELECT @employeeNumber, cast(timestampadd(sql_tsi_year, 1, 
      @ptoAnniversaryDate) AS sql_date),
      cast(timestampadd(sql_tsi_year, 2, @ptoAnniversaryDate) AS sql_date) - 1,
      (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 1)
    FROM system.iota
    UNION ALL  
    SELECT @employeeNumber, cast(timestampadd(sql_tsi_year, 2, 
      @ptoAnniversaryDate) AS sql_date),
      cast(timestampadd(sql_tsi_year, 10, @ptoAnniversaryDate) AS sql_date) - 1,
      (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 2)
    FROM system.iota
    UNION ALL  
    SELECT @employeeNumber, cast(timestampadd(sql_tsi_year, 10, 
      @ptoAnniversaryDate) AS sql_date),
      cast(timestampadd(sql_tsi_year, 20, @ptoAnniversaryDate) AS sql_date) - 1,
      (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 3)
    FROM system.iota
    UNION ALL  
    SELECT @employeeNumber, cast(timestampadd(sql_tsi_year, 20, @ptoAnniversaryDate) AS sql_date),
      cast(timestampadd(sql_tsi_year, 100, @ptoAnniversaryDate) AS sql_date) - 1,
      (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 4)
    FROM system.iota;  
  CATCH ALL
    RAISE pto_employee_pto_allocation(103, 'Uanble to insert ' + @employeenumber + ' into pto_employee_pto_allocation');
  END TRY; 

-- compli.email <> existing tpEmployees.username
    IF EXISTS (
      SELECT 1
      FROM tpEmployees
      WHERE employeeNumber = @employeeNumber) THEN 
        IF (
          SELECT username
          FROM tpEmployees
          WHERE employeenumber = @employeenumber) <> @username THEN
            RAISE ohmygod(1,'danger will robinson, username mismatch');
        ENDIF;
    ENDIF;
    
-- new row IN tpEmployees IF one does NOT already exist for the employee number    
  TRY
    IF NOT EXISTS (
      SELECT 1
      FROM tpEmployees
      WHERE employeeNumber = @employeeNumber) THEN 
    INSERT INTO tpEmployees
    SELECT @userName, firstname, lastname, employeenumber, @password, 'Fuck You', storecode, TRIM(firstname) + ' ' + lastname
    FROM dds.edwEmployeeDim
    WHERE employeenumber = @employeenumber
      AND currentrow = true;
    ENDIF; 
  CATCH ALL
    RAISE tpEmployees(105, 'Uanble to insert ' + @employeenumber + ' into tpEmployees');
  END TRY;
  
-- new row(s) IN employeeAppAuthorization   
  TRY 
    INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
    SELECT @userName, appName, 800, appCode, appRole, functionality
    FROM applicationMetadata
    WHERE appcode = 'pto'
      AND approle = 'ptoemployee';
  CATCH ALL
    RAISE employeeAppAuthorization(105, 'Uanble to insert ' + @employeenumber + ' into employeeAppAuthorization');
  END TRY;  
  
COMMIT WORK;
INSERT INTO __output
values ('Pass','Hoo-fucking-Ray', @username,@password, true);
CATCH ALL
  ROLLBACK;
  IF __errclass IN ('show_stoppers','ptoEmployees','ptoPositionFulfillment',
    'pto_employee_pto_allocation','tpEmployees','employeeAppAuthorization') THEN 
    INSERT INTO __output values('Fail', __errclass + ': ' + __errtext, '', '', true); 
  ELSE
    INSERT INTO __output values('Fail', 'This is an unhandled exception :: ' +
      __errclass + ' :: ' + __errtext, '', '', true);
  ENDIF;
END TRY;
  





END;

ALTER PROCEDURE pto_update_employee_pto(
  employeeNumber cichar(9),
  department cichar(30),
  position cichar(30),
  ptoAnniversaryDate date,
  ptoHoursEarned integer, 
  status CICHAR (4) OUTPUT,
  text cichar(500) output,
  mailError logical output)
BEGIN
/*
1.
IF the hours are being updated for a period that extends past the current year
this really means to ADD a row to pto_emp_pto_alloc
eg: 
FROM           thru          hours
10/10/12      10/9/14       72
10/10/14      10/9/22       112    
10/9/23       10/9/33       152
IF today IS 10/21 AND i am adjusting pto to 200 for the current year, the TABLE becomes
FROM           thru          hours
10/10/12      10/9/14       72
10/10/14      10/9/15       200
10/10/15      10/9/22       112    
10/9/23       10/9/33       152

IF the current period of more than the current year, the entire period does NOT
get adjusted, just the current year, requing the insertion of the row

one alternative IS to turn the pto_emp_pto_alloc TABLE to one row per employee
per year (based on anniversary date)

2. 
IF position IS a pto admin, disallow the change

EXECUTE PROCEDURE pto_update_employee_pto('2130690','RY2 Sales', 'Sales Consultant', '08/20/2007', 100);
*/
DECLARE @employeeNumber string;
DECLARE @newDepartment string;
DECLARE @newPosition string;
DECLARE @newPtoAnniversary date;
DECLARE @newHours integer;
DECLARE @curDepartment string;
DECLARE @curPosition string;
DECLARE @curPtoAnniversary date;
DECLARE @curHours integer;

@employeeNumber = (SELECT employeenumber FROM __input);
@newDepartment = (SELECT department FROM __input);
@newPosition = (SELECT position FROM __input);
@newPtoAnniversary = (SELECT ptoAnniversaryDate FROM __input);
@newHours = (SELECT ptoHoursEarned FROM __input);

@curDepartment = (
  SELECT department 
  FROM ptoPositionFulfillment 
  WHERE employeenumber = @employeeNumber);
@curPosition = (
  SELECT position
  FROM ptoPositionFulfillment
  WHERE employeeNumber = @employeeNumber);  
@curPtoAnniversary = (
  SELECT ptoAnniversary
  FROM ptoEmployees
  WHERE employeeNumber = @employeeNumber);
@curHours = (
  SELECT hours
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber
    AND curdate() BETWEEN fromDate AND thruDate);  
BEGIN TRANSACTION;
TRY 
IF @newDepartment <> @curDepartment THEN
  IF EXISTS (
    SELECT 1 
    FROM ptoPositionFulfillment a
    INNER JOIN ptoAuthorization b on a.department = b.authByDepartment
      AND a.position = b.authByPosition
    WHERE a.department = @newDepartment
      AND a.position = @newPosition) THEN 
    RAISE show_stoppers(6, TRIM(@newDepartment) + ':' + TRIM(@newPosition) + ' is already filled' collate ads_default_ci);
  ENDIF;
ENDIF;
IF @newPosition <> @curPosition THEN
  IF EXISTS (
    SELECT 1 
    FROM ptoPositionFulfillment a
    INNER JOIN ptoAuthorization b on a.department = b.authByDepartment
      AND a.position = b.authByPosition
    WHERE a.department = @newDepartment
      AND a.position = @newPosition) THEN 
    RAISE show_stoppers(6, TRIM(@newDepartment) + ':' + TRIM(@nenwPosition) + ' is already filled' collate ads_default_ci);
  ENDIF;
ENDIF;
IF @newPtoAnniversary <> @curPtoAnniversary THEN
  IF @newPtoAnniversary > curdate() THEN
    RAISE show_stoppers(7, 'Sorry, we can not allow you to enter a future date');          
  ENDIF;  
ENDIF;
IF @newHours <> @curHours THEN
ENDIF;

TRY 
  UPDATE ptoPositionFulfillment
  SET department = @newDepartment,
      position = @newPosition
  WHERE employeeNumber = @employeeNumber;
CATCH ALL 
  RAISE updateTable(1, 'Unable to update ptoPositionFulfillment');
END TRY;

TRY 
  UPDATE ptoEmployees
  SET ptoAnniversary = @newPtoAnniversary
  WHERE employeeNumber = @employeeNumber;
CATCH ALL
  RAISE updateTable(2, 'Unable to update ptoEmployees');
END TRY;

TRY   
  UPDATE pto_employee_pto_allocation
  SET hours = @newHours
  WHERE employeeNumber = @employeeNumber
    AND curdate() BETWEEN fromDate AND thruDate; 
CATCH ALL
  RAISE updateTable(3, 'Unable to update pto_employee_pto_allocation');
END TRY;
     
COMMIT WORK;
INSERT INTO __output
values ('Pass','Hoo-fucking-Ray', false);

CATCH ALL
  ROLLBACK;
  IF __errclass IN ('show_stoppers','updateTable') THEN 
    INSERT INTO __output values('Fail', __errclass + ': ' + __errtext, false); 
  ELSE
    INSERT INTO __output values('Fail', 'This is an unhandled exception :: ' +
      __errclass + ' :: ' + __errtext, false);
  ENDIF;
END TRY;
END;

alter PROCEDURE pto_get_employee_pto
   ( 
      employeeNumber CICHAR ( 9 ),
      employeeNumber CICHAR ( 9 ) OUTPUT,
      name CICHAR ( 52 ) OUTPUT,
      store CICHAR ( 12 ) OUTPUT,
      originalHireDate DATE OUTPUT,
      latestHireDate DATE OUTPUT,
      department CICHAR ( 30 ) OUTPUT,
      position CICHAR ( 30 ) OUTPUT,
      ptoAdministrator CICHAR ( 52 ) OUTPUT,
      ptoAnniversary DATE OUTPUT,
      ptoHoursEarned DOUBLE ( 15 ) OUTPUT,
      ptoHoursUsed DOUBLE ( 15 ) OUTPUT,
      ptoHoursRemaining DOUBLE ( 15 ) OUTPUT,
      title CICHAR ( 63 ) OUTPUT,
      ptoFromDate DATE OUTPUT,
      ptoThruDate DATE OUTPUT,
      ptoPolicyHtml cichar(60) output,
      ptoHoursApproved integer output,
      ptoHoursRequested integer output,
      totalDaysInPeriod integer output,
      remainingDaysInPeriod integer output
   ) 
BEGIN 
/*
doug bohm '116185'
jay olson '1106399'
crystal moulds  
loren shereck '1126040'
rudy robles '1117600'
travis b '121362'

EXECUTE PROCEDURE sp_RenameDDObject('pto_get_employee_info','zUnused_pto_get_employee_info', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject('pto_get_employee_to_edit','zUnused_pto_get_employee_to_edit', 10, 0);

10/28 ADD 
  requests AND approved

for this first rollout, the period END date IS easy FROM pto_employee_pto_allocation
but next year, the period END date may NOT be the thrudate FROM pto_employee_pto_allocation

EXECUTE PROCEDURE pto_get_employee_pto ('121362');

*/           
DECLARE @employeeNumber string;
@employeenumber = (SELECT employeenumber from __input);
INSERT INTO __output
select h.employeenumber, h.name, h.store, h.originalHireDate, h.latestHireDate,
  h.department, h.position, 
  h.ptoAdmin, h.ptoAnniversary, 
  h.hours, coalesce(i.ptoUsed, 0) AS ptoUsed, 
  h.hours - coalesce(i.ptoUsed, 0)-coalesce(k.ptoHoursApproved,0)-coalesce(m.ptoHoursRequested,0) AS ptoHoursRemaining,
  TRIM(h.department) + ':' + h.position,
  fromDate, thruDate, 
  j.ptoPolicyHtml,
  coalesce(k.ptoHoursApproved,0) AS ptoHoursApproved, 
  coalesce(m.ptoHoursRequested,0) AS ptoHoursRequested,
  timestampdiff(sql_tsi_day, fromDate, thruDate) AS totalDaysInPeriod,
  timestampdiff(sql_tsi_day, curdate(), thrudate) AS remainingDaysInPeriod 
FROM (
  SELECT a.employeenumber, a.ptoAnniversary, 
    TRIM(b.firstname) + ' ' + b.lastname as name, b.storecode,
    b.fullPartTime, 
    c.department, c.position,
    trim(f.firstname) + ' ' + TRIM(f.lastname) AS ptoAdmin,
    g.hours, g.fromDate, g.thrudate,
    CASE b.storecode
      WHEN 'RY1' THEN 'RY1 - GM'
      WHEN 'RY2' THEN 'RY2 - Honda'
    END AS store,
    gg.ymhdto AS originalHireDate, gg.ymhdte AS latestHireDate,
    g.fromDate, g.thruDate  
  FROM ptoEmployees a
  INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  INNER JOIN ptoPositionFulfillment c on a.employeenumber = c.employeenumber  
  INNER JOIN ptoAuthorization d on c.department = d.authForDepartment
    AND c.position = d.authForPosition
  INNER JOIN ptoPositionFulfillment e on d.authByDepartment = e.department 
    AND d.authByPosition = e.position 
  INNER JOIN dds.edwEmployeeDim f on e.employeenumber = f.employeenumber 
    AND f.currentrow = true 
  INNER JOIN pto_employee_pto_allocation g on a.employeenumber = g.employeenumber
    AND curdate() BETWEEN g.fromdate AND g.thrudate
  LEFT JOIN dds.stgArkonaPYMAST gg on a.employeenumber = gg.ymempn    
  WHERE a.employeenumber = @employeeNumber) h
LEFT JOIN ( --ptoUsed: LEFT JOIN, may NOT be any used
  SELECT a.employeenumber, SUM(a.hours) AS ptoUsed
  FROM pto_used a
  INNER JOIN pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
  WHERE a.employeenumber = @employeeNumber
    AND a.theDate BETWEEN b.fromDate AND b.thruDate
  GROUP BY a.employeenumber) i on h.employeenumber = i.employeenumber
LEFT JOIN ptoDepartmentPositions j on h.department = j.department
  AND h.position = j.position
LEFT JOIN (
  SELECT employeeNumber, SUM(hours) AS ptoHoursApproved
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Approved'
    AND thedate >= curdate()
  GROUP BY employeeNumber) k on h.employeenumber = k.employeeNumber  
LEFT JOIN ( 
  SELECT employeeNumber, SUM(hours) AS ptoHoursRequested
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Pending'
    AND thedate >= curdate() 
  GROUP BY employeeNumber) m on h.employeeNumber = m.employeeNumber;
END;

ALTER PROCEDURE pto_get_manager_employees (
      employeeNumber CICHAR ( 9 ),
      employeeNumber cichar(9) output,
      name CICHAR ( 52 ) OUTPUT,
      department CICHAR ( 30 ) OUTPUT,
      title CICHAR ( 30 ) OUTPUT,
      ptoAnniversary DATE OUTPUT,
      ptoExpiringWeeks integer output,
      ptoHoursEarned DOUBLE ( 15 ) OUTPUT,
      ptoHoursRequested integer output,
      ptoHoursApproved integer output,
      ptoHoursUsed DOUBLE ( 15 ) OUTPUT,
      ptoHoursRemaining DOUBLE ( 15 ) OUTPUT)
      
BEGIN
/*      

SELECT *
FROM employeeAppAuthorization
WHERE username = 'kespelund@rydellchev.com'


INSERT INTO employeeAppAuthorization (username, appName, appSeq, appCode,
  appRole, functionality)  
SELECT 'kespelund@rydellchev.com', a.appName, a.appSeq, a.appCode, a.appRole, a.functionality 
FROM applicationMetaData a
WHERE appcode = 'pto'
  AND approle = 'ptomanager';

-- ALL people for which ken IS ultimately responsible  
-- USING ptoClosure - this IS NOT what i need right now
SELECT descendantDepartment, descendantPosition, d.firstname, d.lastname,
  d.fullparttime
FROM ptoClosure a
INNER JOIN ptoPositionFulfillment b on a.descendantDepartment = b.department
  AND a.descendantPosition = b.Position
INNER JOIN ptoEmployees c on b.employeeNumber = c.employeenumber
INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
  AND d.currentrow = true 
WHERE ancestorDepartment = 'RY1 drive'
  AND ancestorPosition = 'manager'
  AND depth <> 0
  
11/02/14 need to exclude part timers  
  
EXECUTE PROCEDURE pto_get_manager_employees ('140790');
*/  

DECLARE @employeeNumber string;
@employeeNumber = (SELECT employeeNumber FROM __input);
INSERT INTO __output
select top 1000 d.employeeNumber, TRIM(d.firstname) + ' ' + d.lastName AS name, c.department, 
  c.position AS title, e.ptoAnniversary,
  timestampdiff(sql_tsi_week, curdate(), f.thruDate) AS ptoExpiringWeeks,
  f.hours AS ptoHoursEarned,
  coalesce(g.ptoHoursRequested,0) AS ptoHoursRequested,
  coalesce(h.ptoHoursApproved,0) AS ptoHoursApproved,
  coalesce(i.ptoHoursUsed,0) AS ptoHoursUsed,
  f.hours-coalesce(g.ptoHoursRequested,0)-coalesce(h.ptoHoursApproved,0)-coalesce(i.ptoHoursUsed,0) AS ptorHoursRemaining
FROM ptoAuthorization a
INNER JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
INNER JOIN ptoPositionFulfillment c on a.authForDepartment = c.department
  AND a.authForPosition = c.position  
INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
  AND d.currentrow = true 
  AND d.active = 'active' 
  AND d.fullparttime = 'full'
INNER JOIN ptoEmployees e on c.employeeNumber = e.employeeNumber  
INNER JOIN pto_employee_pto_allocation f on e.employeenumber = f.employeenumber
  AND curdate() BETWEEN f.fromdate AND f.thrudate
LEFT JOIN ( 
  SELECT employeeNumber, SUM(hours) AS ptoHoursRequested
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Pending'
    AND thedate >= curdate() 
  GROUP BY employeeNumber) g on c.employeeNumber = g.employeeNumber  
LEFT JOIN (
  SELECT employeeNumber, SUM(hours) AS ptoHoursApproved
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Approved'
    AND thedate >= curdate()
  GROUP BY employeeNumber) h on c.employeenumber = h.employeenumber  
LEFT JOIN ( -- ptoUsed: LEFT JOIN, may NOT be any used
  SELECT a.employeenumber, SUM(a.hours) AS ptoHoursUsed
  FROM pto_used a
  INNER JOIN pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
  WHERE a.theDate BETWEEN b.fromDate AND b.thruDate
  GROUP BY a.employeeNumber) i on c.employeenumber = i.employeenumber  
WHERE b.employeenumber = @employeenumber
ORDER BY timestampdiff(sql_tsi_week, curdate(), f.thruDate);
END;

  
  
  