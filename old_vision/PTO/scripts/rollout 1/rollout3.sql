-- give auth to pto/ptoemployee for ALL currently employed tpEmployees that 
-- are full time AND hourly AND already exist IN ptoEmployees
-- ALL they need are employeeAppAuthorization
INSERT INTO employeeAppAuthorization (username, appName, appSeq, appCode,
  appRole, functionality)  
SELECT b.username, a.appName, a.appSeq, a.appCode, a.appRole, a.functionality 
FROM applicationMetaData a
LEFT JOIN (
  SELECT a.username, d.firstname, d.lastname, a.password, b.*, c.*,
    (SELECT COUNT(*)
       FROM employeeAppAuthorization u
       INNER JOIN tpEmployees v on u.userName = v.userName
       WHERE v.employeenumber = a.employeenumber),
    iif(a.username = b.username, true, false),
    d.payrollclass, d.fullparttime
  FROM tpEmployees a
  LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
  LEFT JOIN pto_employee_pto_allocation c on b.employeenumber = c.employeenumber
    AND curdate() BETWEEN c.fromdate AND c.thrudate  
  LEFT JOIN dds.edwEmployeeDim d on a.employeenumber = d.employeenumber
    AND d.currentrow = true
    AND d.active = 'active'  
  WHERE b.employeenumber IS NOT NULL -- only those that are currently IN ptoEmployees  
    AND NOT EXISTS (
      SELECT 1
      FROM pto_exclude
      WHERE employeenumber = a.employeenumber)
    AND d.employeenumber IS NOT NULL) b on 1 = 1
WHERE a.appcode = 'pto'
  AND a.approle = 'ptoemployee'  
  AND b.payrollclass = 'hourly'
  AND b.fullparttime = 'full';
  
/******************************************************************************/  

-- full time hourly employees IN ptoEmployees
-- CREATE tpEmployees row AND give ptoemployee access IN employeeAppAuthorization  
-- DROP TABLE #passwords;
CREATE TABLE #passwords (
  username cichar(50),
  password cichar(8));
INSERT INTO #passwords(username)

SELECT d.email
FROM dds.edwEmployeeDim a
INNER JOIN pto_compli_users d on a.employeenumber = d.userid
WHERE a.currentrow = true
  AND a.active = 'active'
--  AND d.userid IS NOT NULL -- IN compli
  AND a.payrollclass = 'hourly'
  AND a.fullparttime = 'full'
  AND NOT EXISTS (
    SELECT 1
    FROM pto_exclude
    WHERE employeenumber = a.employeeNumber)
  AND NOT EXISTS (
    SELECT 1
    FROM tpEmployees
    WHERE username = d.email collate ads_default_ci)
  AND NOT EXISTS (
    SELECT 1
    FROM tpEmployees
    WHERE employeenumber = a.employeenumber)
-- fix for adding tpEmp & empAppAuth WHERE there IS no ptoEmployees row    
  AND EXISTS (
    SELECT 1 
    FROM ptoEmployees
    WHERE employeenumber = a.employeenumber);    
    
/******************************************************************************/    

DECLARE @cur CURSOR AS
  SELECT *
  FROM #passwords;
DECLARE @str string, @password string;
DECLARE @i integer, @pos1 integer, @pos2 integer, @j integer;  
@str = 'LoremXesumdoLorspnametconsecteturadXeYacWngeLpnDonecetorcWanuncmattYa'+
  'auctorVWvamusWacuLYanonLacusegetmattYaCrasetLacWnWadoLorPeLLentesquefacWLY'+
  'aYaveLpnduWacvarWussemconsecteturquYaAeneanWnterdumquamatLWberopharetraWnt'+
  'erdumMaecenasspnametuLtrWcWesnuncVWvamusmaLesuadatortorposuereLuctusdapWbu'+
  'sVestWbuLumaWnterdumquamSuspendYaseactemporLectusCrasmattYaanteveLsuscXepn'+
  'semperodWoveLpnLaoreetteLLusveLsoLLWcpnudWnquamerosaLeoALWquamdapWbusnuncL'+
  'WberononconguejustovuLputatevpnaePhaseLLusnuLLaarcumaLesuadaWnterdummoLest'+
  'WeutconsequatettortorSeddWctumnequeegetnYaWLaoreetcongueProWnrhoncusrutrum'+
  'congueMaecenasvBLoremsagpntYavWverrafeLYadWgnYasWmbWbendumLectuQuYaquetWnc'+
  'vBuntauctormagnaconvaLLYavestWbuLumMaurYapuLvWnaretmetusnonhendrerpnNuncbW'+
  'bendumsemdWctumpLaceratposuerenuncveLpnaccumsanrYausacvuLputateXesumtortor'+
  'spnametmassaFusceuLtrWcWeseratnoneLeWfendcommodoNuLLanonerosvBjustotWncvBu'+
  'ntportaanonnuLLaPraesentmattYafeugWattWncvBuntALWquamsedsuscXepnodWoQuYaqu'+
  'evenenatYautarcuegetvenenatYaVestWbuLumuLtrWcWesadXeYacWngvuLputateNuLLase'+
  'dLacusLoremVWvamusLacWnWasceLerYaquerYausacsodaLesmaurYaconsequatnonSedtWn'+
  'cvBuntatmaurYavBdapWbusDuYautteLLusenWmCurabpnurfermentumerosacLWguLahendr'+
  'erpnfeugWatcondWmentumturpYatWncvBuntPhaseLLusvestWbuLumsapWenegetornarevo'+
  'LutpatUtpeLLentesqueturpYanWbhnecvuLputateteLLusLobortYavpnaePraesentcursu'+
  'snuLLauteratpretWumpretWumVestWbuLumfeugWattortorvBporttpnorvWverraodWonYa'+
  'WmoLestWeorcWvpnaeWnterdumdoLorLWberovBsapWenQuYaquevpnaeconguemassaeugrav'+
  'vBasemUtcongueWnsemetmoLLYaNuLLamnonmagnanuncSedvBcommodoLWguLaEtWamquYava'+
  'rWussapWenNuncvBsemenWmSuspendYaseLacWnWanecodWoatsodaLesVWvamussedduWvuLp'+
  'utatevenenatYapurusdapWbusmoLestWeteLLusMaurYaveLveLpnquYaLWguLacongueeuYa'+
  'modegetetnequeSuspendYasenonLacWnWaLoremvBmattYaarcuPraesentseddoLornWbhCu'+
  'rabpnurconsecteturveLmWquYaWacuLYaLoremXesumdoLorspnametconsecteturadXeYac'+
  'WngeLpnInachendrerpnsapWenWndWctumLoremMorbWsedbLandpnteLLusPeLLentesquegr'+
  'avvBatemporpuruscommodoWacuLYaQuYaqueLobortYatempusLoremspnametLuctusSuspe'+
  'ndYasesemtortorornareWnodWospnametconsequatornarequam';  
@j = 1;  
OPEN @cur;
TRY
  WHILE FETCH @cur DO
    @i = (SELECT frac_second(timestampadd(SQL_TSI_FRAC_SECOND, cast(right(trim(cast(rand(@j) AS sql_char)), 3) AS sql_integer),now())) FROM system.iota);    
    @pos1 = (SELECT cast(right(trim(cast(rand(@i) AS sql_char)), 3) AS sql_integer) FROM system.iota) ;
    @pos2 = 2 * (SELECT cast(right(trim(cast(rand(@pos1) AS sql_char)), 3) AS sql_integer) FROM system.iota);  
    @password = (
      SELECT substring(@str, @pos1, 3) + left(CAST(@i AS sql_char), 2) + 
        upper(substring(@str, @pos2, 1)) + substring(@str, @pos1 +@i, 2)
      FROM system.iota);   
    UPDATE #passwords
    SET password = @password
    WHERE username = @cur.username; 
    @j = @j + 1;
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY;     

BEGIN TRANSACTION;
TRY 
-- tpEmployees
INSERT INTO tpEmployees
SELECT d.email, a.firstname, a.lastname, a.employeenumber, e.password, 'Fuck You', a.storecode, TRIM(a.firstname) + ' ' + a.lastname
FROM dds.edwEmployeeDim a
INNER JOIN pto_compli_users d on a.employeenumber = d.userid
INNER  JOIN #passwords e on d.email = e.username collate ads_default_ci
WHERE a.currentrow = true
  AND a.active = 'active'
  AND d.userid IS NOT NULL -- IN compli
  AND a.payrollclass = 'hourly'
  AND a.fullparttime = 'full'
  AND NOT EXISTS (
    SELECT 1
    FROM pto_exclude
    WHERE employeenumber = a.employeeNumber)
  AND NOT EXISTS (
    SELECT 1
    FROM tpEmployees
    WHERE username = d.email collate ads_default_ci)
  AND NOT EXISTS (
    SELECT 1
    FROM tpEmployees
    WHERE employeenumber = a.employeenumber);    

-- AND employeeAppAuthorization
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT b.userName, a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
LEFT JOIN #passwords b on 1 = 1
WHERE a.appcode = 'pto'
  AND a.approle = 'ptoemployee';
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;

-- at this point ALL hourly employees have
/******************************************************************************/
-- ken espelund
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'kespelund@rydellchev.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
-- garret evenson
UPDATE tpEmployees
SET employeenumber = '241085'
WHERE employeenumber = '141085';

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'gevenson@rydellcars.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
UPDATE ptoPositionFulFillment
SET position = 'Assistant Manager'
WHERE employeenumber = '241085';  

-- joel dangerfield
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'jdangerfield@gfhonda.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
UPDATE ptoPositionFulFillment
SET position = 'Assistant Manager'
WHERE employeenumber = '241085';
/******************************************************************************/

-- pto admins with no vision access

-- DROP TABLE #base;
SELECT *
INTO #base
FROM (
  SELECT c.storecode, c.employeenumber, c.firstname, c.lastname, b.department, b.position, left(d.username,30) as visionUserName,
    e.email AS compliUserName, 
    iif(d.username = e.email collate ads_default_ci, true,false) AS userNamesAgree,
    f.hasPtoAccess, e.dateofhire AS anniversaryDate
  FROM (
    SELECT authByDepartment, authByPosition
    FROM ptoAuthorization
    WHERE authByLevel <> 1
    GROUP BY authByDepartment, authByPosition) a
  LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
    AND a.authByPosition = b.position
  LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
    AND c.currentrow = true
    AND c.active = 'active'  
  LEFT JOIN tpEmployees d on c.employeenumber = d.employeenumber  
  LEFT JOIN pto_compli_users e on c.employeenumber = e.userid
  LEFT JOIN (
    SELECT username, COUNT(*) hasPtoAccess
    from employeeAppAuthorization 
    WHERE appname = 'pto'
    GROUP BY username) f on d.username = f.username) x   
WHERE visionUsername IS NULL AND lastname <> 'sorum';  

-- DROP TABLE #passwords;
CREATE TABLE #passwords (
  username cichar(50),
  password cichar(8));
INSERT INTO #passwords(username)
SELECT compliUserName 
FROM #base;

DECLARE @cur CURSOR AS
  SELECT *
  FROM #passwords;
DECLARE @str string, @password string;
DECLARE @i integer, @pos1 integer, @pos2 integer, @j integer;  
@str = 'LoremXesumdoLorspnametconsecteturadXeYacWngeLpnDonecetorcWanuncmattYa'+
  'auctorVWvamusWacuLYanonLacusegetmattYaCrasetLacWnWadoLorPeLLentesquefacWLY'+
  'aYaveLpnduWacvarWussemconsecteturquYaAeneanWnterdumquamatLWberopharetraWnt'+
  'erdumMaecenasspnametuLtrWcWesnuncVWvamusmaLesuadatortorposuereLuctusdapWbu'+
  'sVestWbuLumaWnterdumquamSuspendYaseactemporLectusCrasmattYaanteveLsuscXepn'+
  'semperodWoveLpnLaoreetteLLusveLsoLLWcpnudWnquamerosaLeoALWquamdapWbusnuncL'+
  'WberononconguejustovuLputatevpnaePhaseLLusnuLLaarcumaLesuadaWnterdummoLest'+
  'WeutconsequatettortorSeddWctumnequeegetnYaWLaoreetcongueProWnrhoncusrutrum'+
  'congueMaecenasvBLoremsagpntYavWverrafeLYadWgnYasWmbWbendumLectuQuYaquetWnc'+
  'vBuntauctormagnaconvaLLYavestWbuLumMaurYapuLvWnaretmetusnonhendrerpnNuncbW'+
  'bendumsemdWctumpLaceratposuerenuncveLpnaccumsanrYausacvuLputateXesumtortor'+
  'spnametmassaFusceuLtrWcWeseratnoneLeWfendcommodoNuLLanonerosvBjustotWncvBu'+
  'ntportaanonnuLLaPraesentmattYafeugWattWncvBuntALWquamsedsuscXepnodWoQuYaqu'+
  'evenenatYautarcuegetvenenatYaVestWbuLumuLtrWcWesadXeYacWngvuLputateNuLLase'+
  'dLacusLoremVWvamusLacWnWasceLerYaquerYausacsodaLesmaurYaconsequatnonSedtWn'+
  'cvBuntatmaurYavBdapWbusDuYautteLLusenWmCurabpnurfermentumerosacLWguLahendr'+
  'erpnfeugWatcondWmentumturpYatWncvBuntPhaseLLusvestWbuLumsapWenegetornarevo'+
  'LutpatUtpeLLentesqueturpYanWbhnecvuLputateteLLusLobortYavpnaePraesentcursu'+
  'snuLLauteratpretWumpretWumVestWbuLumfeugWattortorvBporttpnorvWverraodWonYa'+
  'WmoLestWeorcWvpnaeWnterdumdoLorLWberovBsapWenQuYaquevpnaeconguemassaeugrav'+
  'vBasemUtcongueWnsemetmoLLYaNuLLamnonmagnanuncSedvBcommodoLWguLaEtWamquYava'+
  'rWussapWenNuncvBsemenWmSuspendYaseLacWnWanecodWoatsodaLesVWvamussedduWvuLp'+
  'utatevenenatYapurusdapWbusmoLestWeteLLusMaurYaveLveLpnquYaLWguLacongueeuYa'+
  'modegetetnequeSuspendYasenonLacWnWaLoremvBmattYaarcuPraesentseddoLornWbhCu'+
  'rabpnurconsecteturveLmWquYaWacuLYaLoremXesumdoLorspnametconsecteturadXeYac'+
  'WngeLpnInachendrerpnsapWenWndWctumLoremMorbWsedbLandpnteLLusPeLLentesquegr'+
  'avvBatemporpuruscommodoWacuLYaQuYaqueLobortYatempusLoremspnametLuctusSuspe'+
  'ndYasesemtortorornareWnodWospnametconsequatornarequam';  
@j = 1;  
OPEN @cur;
TRY
  WHILE FETCH @cur DO
    @i = (SELECT frac_second(timestampadd(SQL_TSI_FRAC_SECOND, cast(right(trim(cast(rand(@j) AS sql_char)), 3) AS sql_integer),now())) FROM system.iota);    
    @pos1 = (SELECT cast(right(trim(cast(rand(@i) AS sql_char)), 3) AS sql_integer) FROM system.iota) ;
    @pos2 = 2 * (SELECT cast(right(trim(cast(rand(@pos1) AS sql_char)), 3) AS sql_integer) FROM system.iota);  
    @password = (
      SELECT substring(@str, @pos1, 3) + left(CAST(@i AS sql_char), 2) + 
        upper(substring(@str, @pos2, 1)) + substring(@str, @pos1 +@i, 2)
      FROM system.iota);   
    UPDATE #passwords
    SET password = @password
    WHERE username = @cur.username; 
    @j = @j + 1;
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY; 

BEGIN TRANSACTION;
TRY

INSERT INTO tpEmployees(username,firstname,lastname,employeenumber,password,membergroup,storecode, fullname)
SELECT compliUserName, firstname, lastname, employeenumber, b.password, 'Fuck You', storecode, TRIM(firstname) + ' ' + lastname
FROM #base a
LEFT JOIN #passwords b on a.compliUserName = b.username collate ads_Default_ci
WHERE lastname <> 'neumann';

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT compliUserName, appName, 800, appCode, appRole, functionality
FROM applicationMetadata a, #base b
WHERE appcode = 'pto'
  AND approle = 'ptomanager'
  AND lastname <> 'neumann';
  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

-- pto admins with no pto access
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT compliUserName, appName, 800, appCode, appRole, functionality
FROM #base a
LEFT JOIN applicationMetaData b
  on b.appcode = 'pto'
  AND b.approle = 'ptomanager'
WHERE NOT EXISTS (
  SELECT 1
  FROM #base
  WHERE firstname = a.firstname
    AND lastname = a.lastname
    AND userNamesAgree = true
    AND hasptoAccess = 3)
  AND lastname NOT IN ('sorum','neumann', 'foster', 'cahalan')
  AND hasptoaccess IS NULL 
  AND usernamesagree = true;
  
-- pto admins with incorrect pto access
BEGIN TRANSACTION;
TRY   
DELETE 
FROM employeeAppAuthorization
WHERE appcode = 'pto'
  AND approle = 'ptoemployee'  
AND username IN (
  SELECT visionUserNAme
  FROM #base a
  WHERE lastname NOT IN ('sorum','neumann', 'foster', 'cahalan')
    AND hasptoaccess = 2
    AND usernamesagree);
    
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT compliUserName, appName, 800, appCode, appRole, functionality
FROM #base a 
LEFT JOIN applicationMetaData b
  on b.appcode = 'pto'
  AND b.approle = 'ptomanager' 
WHERE lastname NOT IN ('sorum','neumann', 'foster', 'cahalan')
  AND hasptoaccess = 2
  AND usernamesagree;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;       

/******************************************************************************/

-- trent, june & jeri
-- change tpEmployees username to rydellcars.com, should cascase UPDATE to employeeAppAuthorization
UPDATE tpEmployees
SET username = 'jschmiess@rydellcars.com'
WHERE username = 'jpenas@rydellchev.com';
UPDATE tpEmployees
SET username = 'jgustafson@rydellcars.com'
WHERE username = 'jgustafson@rydellchev.com';
UPDATE tpEmployees
SET username = 'tolson@rydellcars.com'
WHERE username = 'tolson@gfhonda.com';

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT a.username, appName, 800, appCode, appRole, functionality 
FROM (
  SELECT *
  FROM tpemployees
  WHERE username IN ('jschmiess@rydellcars.com','jgustafson@rydellcars.com','tolson@rydellcars.com')) a
LEFT JOIN applicationMetaData b
  on b.appcode = 'pto'
  AND b.approle = 'ptomanager';   
  
/******************************************************************************/ 
-- ddl2 dept policy
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'bdc_policy_html'
WHERE department LIKE '%bdc%';

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'guest_experience_policy_html'
WHERE department LIKE '%guest%';

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'honda_pdq_advisors_policy_html'
WHERE department = 'ry2 pdq'
  AND position = 'advisor';
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'honda_pdq_techs_policy_html'
WHERE department = 'ry2 pdq'
  AND position LIKE '%tech%';
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'parts_policy_html'
WHERE department = 'ry1 parts';  

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'parts_policy_html'
WHERE department = 'ry2 parts';  

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'collision_center_policy_html'
WHERE department = 'body shop'; 

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_pdq_advisor_policy_html'
WHERE department = 'ry1 pdq'
  AND position = 'advisor';
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_pdq_tech_policy_html'
WHERE department = 'ry1 pdq'
  AND position LIKE '%tech%';  
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'car_wash_policy_html'
WHERE department = 'car wash';  

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_service_advisor_policy_html'
WHERE department = 'ry1 drive'
  AND position = 'advisor'; 
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_service_rental_policy_html'
WHERE department = 'rental';  

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_service_tech_policy_html'
WHERE department = 'ry1 main shop'
  AND position LIKE '%tech%'; 
  
-- bev
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_service_tech_policy_html'
WHERE department = 'ry1 main shop'
  AND position = 'dispatcher';   
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'detail_policy_html'
WHERE department = 'detail';   

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'office_policy_html'
FROM ptoDepartmentPositions
WHERE department = 'office';

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'fi_admin_policy_html'
FROM ptoDepartmentPositions
WHERE department = 'office'
  AND position = 'F&I Admin';


UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'no_policy_html'
WHERE ptoPolicyHtml IS NULL; 

/******************************************************************************/
-- last minute cleanup
DELETE FROM employeeAppAuthorization
WHERE username = 'alindquist@gfhonda.com'
  AND approle = 'ptoemployee';
  
-- bev does NOT have pto 
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'blongoria@rydellchev.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
-- justin kilmer 
DELETE FROM employeeAppAuthorization
WHERE username = 'jkilmer@rydellcars.com'
  AND approle = 'ptoemployee';
  
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'jkilmer@rydellcars.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
-- k lind
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'klind@rydellcars.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
-- mark steinke  
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'msteinke@rydellchev.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
DELETE FROM employeeAppAuthorization
WHERE username = 'msteinke@rydellchev.com'
  AND appcode = 'tp'
  AND functionality = 'production summary'
  AND appDepartmentKey = 18;  
  
-- ned
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'neuliss@rydellcars.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';  
  
-- lear
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'mlear@gfhonda.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';  
  
-- randy
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'rsattler@rydellchev.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';        
  
-- zack sieracki
UPDATE ptoPositionFulfillment
SET position = 'Advisor'
-- SELECT * FROM ptoPositionFulfillment WHERE employeenumber = '1126110'
WHERE employeenumber = '1126110';  

-- ben dalen
UPDATE ptoPositionFulfillment
SET department = 'RY1 Sales',
    position = 'Sales Consultant'
-- SELECT * FROM ptoPositionFulfillment WHERE employeenumber = '131100'
WHERE employeenumber = '131100';  

-- jesse chavez
UPDATE ptoPositionFulfillment
SET department = 'RY1 Sales',
    position = 'Sales Consultant'
-- SELECT * FROM ptoPositionFulfillment WHERE employeenumber = '124120'
WHERE employeenumber = '124120';  