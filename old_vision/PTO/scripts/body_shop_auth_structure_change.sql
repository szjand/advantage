/*
I am now authorizing PTO for all body shop employees,  
Randy is the body shop director and I am the body shop Manager.  
As for Andrew he is considered a Metal Technician (C). If you could 
change the PTO hierarchy to be under me I would appreciate it.

Thank you
John

*/
select * FROM tpemployees WHERE lastname = 'gardner'

SELECT *
FROM ptopositionfulfillment
WHERE employeenumber = '150120'

Body Shop  Assistant Manager

SELECT * 
FROM ptoauthorization
WHERE authbydepartment = 'Body Shop'
  AND authbyposition = 'Assistant Manager'
  
SELECT * FROM tpemployees WHERE lastname = 'dibi'  

SELECT * FROM ptopositionfulfillment WHERE employeenumber = '15648'

SELECT * 
FROM ptoauthorization
WHERE authbydepartment = 'Body Shop'
  AND authbyposition = 'Manager'
  
INSERT INTO ptopositionfulfillment values('Body Shop', 'Metal Technician (C)', '15648') 

select * FROM ptopositions ORDER BY position

SELECT * FROM tpemployees WHERE lastname = 'sattler'

SELECT * FROM ptodepartmentpositions WHERE position = 'director'
INSERT INTO ptodepartmentpositions values ('Body Shop','Director',3,'no_policy_html')

UPDATE ptopositionfulfillment
SET position = 'Director'
-- select * FROM ptopositionfulfillment
WHERE employeenumber = '1122342'

select * FROM tpemployees WHERE lastname = 'gardner'

SELECT *
FROM ptopositionfulfillment
WHERE employeenumber = '150120'

UPDATE ptopositionfulfillment
SET position = 'Manager'
-- select * FROM ptopositionfulfillment
WHERE employeenumber = '150120'
