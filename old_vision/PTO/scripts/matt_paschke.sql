SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '275342' ORDER BY fromdate

SELECT * FROM pto_adjustments WHERE employeenumber = '275342' ORDER BY fromdate

INSERT INTO pto_adjustments values('275342','05/10/2020',40,'Adjustment','PTO hours added by HR for first year of employment');
UPDATE pto_employee_pto_allocation
SET hours = 0
WHERE employeenumber = '275342' 
  AND fromdate = '05/10/2020';
  
  
SELECT * FROM ptoemployees where employeenumber = '1109700'
  
SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '155214' ORDER BY fromdate  


SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '1109700' ORDER BY fromdate  

DELETE FROM pto_employee_pto_allocation WHERE employeenumber = '1109700';
INSERT INTO pto_employee_pto_allocation (employeeNumber, fromdate, thrudate, hours)
SELECT c.employeenumber, 
  cast(timestampadd(sql_tsi_year, c.n, c.ptoAnniversary) as sql_date) AS fromDate,
  CASE c.n
    WHEN 20 THEN cast('12/31/9999' AS sql_date)
    ELSE cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, c.n + 1, c.ptoAnniversary)) AS sql_date) 
  END AS thruDate,
  ptoHours  
FROM (
  SELECT a.employeenumber, a.ptoAnniversary, b.*
  FROM ptoEmployees a,
    (
      SELECT n
      FROM dds.tally
      WHERE n BETWEEN 0 AND 20) b  
  WHERE a.employeenumber = '1109700') c   
LEFT JOIN pto_categories d on c.n BETWEEN d.fromYearsTenure AND d.thruYearsTenure;
	
INSERT INTO pto_adjustments values('275342','05/10/2020',40,'Adjustment','PTO hours added by HR for first year of employment');	