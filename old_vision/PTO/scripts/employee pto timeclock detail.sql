SELECT *
FROM edwEmployeeDim
WHERE lastname = 'syverson'


SELECT trim(c.firstname) + ' ' +  c.lastname AS name, b.thedate as "Date", a.vacationhours AS hours, 'Vacation'
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
WHERE a.vacationhours <> 0
  AND year(b.thedate) = 2014
  AND c.employeenumber = '1135410'
UNION
SELECT trim(c.firstname) + ' ' +  c.lastname, b.thedate as "Date", a.ptohours, 'PTO'
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
WHERE a.ptohours <> 0
  AND year(b.thedate) = 2014
  AND c.employeenumber = '1135410'
UNION   
SELECT 'zTotal', cast(null as sql_date),SUM(a.vacationhours + a.ptohours),''
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
WHERE a.vacationhours + a.ptohours <> 0
  AND year(b.thedate) = 2014
  AND c.employeenumber = '1135410'  
ORDER BY name  
  
  
SELECT *
FROM pto_used
WHERE employeenumber = '196341'  

SELECT *
FROM pto_requests
WHERE employeenumber = '196341'  

SELECT employeenumber, COUNT(*)
FROM pto_requests
WHERE requestType = 'paid time off'
GROUP BY employeenumber
ORDER BY COUNT(*) DESC 

SELECT a.*, b.fromdate, b.thrudate, b.hours + coalesce(c.hours, 0) AS hours 
FROM ptoEmployees a
INNER JOIN pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
LEFT JOIN pto_adjustments c on b.employeenumber = c.employeenumber
  AND b.fromdate = c.fromdate
WHERE a.employeenumber = '196341'
  AND curdate() BETWEEN b.fromdate AND b.thrudate
  
select a.theDate, a.hours, 'Used'
FROM pto_used a 
INNER JOIN pto_employee_pto_allocation b on a.employeeNumber = b.employeeNumber
WHERE a.employeenumber = '196341'
  AND curdate() BETWEEN b.fromDate AND b.thruDate
UNION 
SELECT a.theDate, a.hours, 'Approved'
FROM pto_requests a
INNER JOIN pto_employee_pto_allocation b on a.employeeNumber = b.employeeNumber
WHERE a.employeenumber = '196341'
  AND curdate() BETWEEN b.fromDate AND b.thruDate
  AND a.requestType = 'Paid Time Off'
  AND a.requestStatus = 'Approved'
UNION 
SELECT a.theDate, a.hours, 'Requested'
FROM pto_requests a
INNER JOIN pto_employee_pto_allocation b on a.employeeNumber = b.employeeNumber
WHERE a.employeenumber = '196341'
  AND curdate() BETWEEN b.fromDate AND b.thruDate
  AND a.requestType = 'Paid Time Off'
  AND a.requestStatus = 'Pending'

  
  
  SELECT *
  FROM pto_requests a
  INNER JOIN pto_employee_pto_allocation b on a.employeeNumber = b.employeeNumber
    AND curdate() BETWEEN b.fromDate AND b.thruDate
  WHERE a.employeenumber = '196341'
    AND a.thedate BETWEEN b.fromDate AND b.thruDate
    AND a.requestType = 'Paid Time Off'
    AND a.requestStatus = 'Approved'  
    
    
SELECT theType, SUM(hours)
FROM (   
EXECUTE PROCEDURE pto_get_employee_pto_used ('196341')) a
GROUP BY theType   