Duane Radke
For 2014
  From the time clock, Duane shows 128 hours PTO taken
  From paychecks, Duane shows having been paid for 184 PTO hours
  
For the period of 1/1/2014 thru 4/30/2015 Duane is owed 255 hours of PTO

Vision does not show PTO hours for which Duane was paid, but were not entered in the time clock

So
If you all agree, I will update Vision with the additional 56 hours for which Duane was paid in 2014

If I do that, Duane�s Vision page will show his balance for the current pto period as -1 hour:

PTO Hours Earned	                255
PTO Hours Clocked                 128
PTO Hours Paid but not Clocked	   56
PTO Hours Requested	               72
Balance	                           -1

Agreed?

Thanks

jon




-- paychecks
-- yhdcyy: check date year
-- yhdcmm: check date month
-- yhdvac: vacation taken
-- yhdsck: sick leave taken
-- yhdemm: payroll ending month
Select yhdcmm, yhdvac, yhdsck
from dds.stgArkonaPYHSHDTA
WHERE yhdemp = '1113940'
AND yhdcyy = 14
-- AND yhdcmm IN (8,9,10)
AND yhdvac + yhdsck <> 0

Select yhdcmm, sum(yhdvac + yhdsck)
-- SELECT *
from dds.stgArkonaPYHSHDTA
WHERE yhdemp = '1113940'
AND yhdcyy = 14
-- AND yhdcmm IN (8,9,10)
AND yhdvac + yhdsck <> 0
GROUP BY yhdcmm


Select sum(yhdvac + yhdsck)
from dds.stgArkonaPYHSHDTA
WHERE yhdemp = '1113940'
AND yhdcyy = 14
-- AND yhdcmm IN (8,9,10)
AND yhdvac + yhdsck <> 0

SELECT SUM(hours)
FROM pto_used
WHERE employeenumber = '1113940'

Select yhdcmm, yhdemm, yhdvac, yhdsck
from dds.stgArkonaPYHSHDTA
WHERE yhdemp = '1113940'
AND yhdcyy = 14
-- AND yhdcmm IN (8,9,10)
AND yhdvac + yhdsck <> 0

INSERT INTO pto_used(employeenumber,thedate,source,hours)
values('1113940','06/30/2014','Misc Paid',32);
INSERT INTO pto_used(employeenumber,thedate,source,hours)
values('1113940','07/31/2014','Misc Paid',24);


Select yhdcmm, yhdemm, yhdvac, yhdsck
from dds.stgArkonaPYHSHDTA
WHERE yhdemp = '1132590'
AND yhdcyy = 14
-- AND yhdcmm IN (8,9,10)
AND yhdvac + yhdsck <> 0