SELECT c.storecode, c.lastname, c.firstname, c.employeenumber, a.ptoAnniversary, 
  trim(g.firstname) + ' ' + g.lastname AS [auth by]
FROM ptoEmployees a
INNER JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active'
INNER JOIN ptoDepartmentPositions d on b.department = d.department
  AND b.position = d.position
INNER JOIN ptoAuthorization e on d.department = e.authForDepartment
  AND d.position = e.authForPosition
INNER JOIN ptoPositionFulFillment f on e.authByDepartment = f.department
  AND e.authByPosition = f.position  
INNER JOIN dds.edwEmployeeDim g on f.employeenumber = g.employeenumber
  AND g.currentrow = true  
WHERE b.position IN ('used car sales consultant','new car sales consultant')
  OR (b.department = 'ry2 sales' AND b.position = 'sales consultant')
ORDER BY c.storecode, c.lastname  