SELECT left(trim(d.firstname) + ' ' + trim(d.lastname), 25), a.employeenumber, 
  b.ptoAnniversary, a.fromdate, a.thrudate, a.hours AS curAllocHours, 
  timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thruDate) -1 AS curTenure,
  c.ptoHours AS catHours, e.*
FROM pto_employee_pto_allocation a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN pto_categories c on 
  timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thruDate) -1 
  BETWEEN c.fromyearstenure AND c.thruyearstenure
LEFT JOIN dds.edwEmployeeDim d on a.employeenumber = d.employeenumber
  AND d.currentrow = true  
LEFT JOIN pto_adjustments e on a.employeenumber = e.employeenumber
  AND a.fromdate = e.fromdate  
WHERE curdate() BETWEEN a.fromdate AND a.thrudate 
  AND a.hours <> c.ptohours
  AND e.employeenumber IS NOT NULL 
ORDER BY d.firstname  

-- per ben's irritable conversation with joel
INSERT INTO pto_adjustments(employeenumber, fromdate, hours,reason,notes)
SELECT a.employeenumber, c.fromdate, 40, 'Rollover', 'per joel dangerfield'
FROM ptoemployees a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
INNER JOIN pto_employee_pto_allocation c on a.employeenumber = c.employeenumber
  AND curdate() BETWEEN c.fromdate AND c.thrudate  
WHERE b.lastname = 'knudson'  
  AND b.firstname = 'kenneth';
  
INSERT INTO pto_adjustments(employeenumber, fromdate, hours,reason,notes)
SELECT a.employeenumber, c.fromdate, 32, 'Rollover', 'per joel dangerfield'
FROM ptoemployees a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
INNER JOIN pto_employee_pto_allocation c on a.employeenumber = c.employeenumber
  AND curdate() BETWEEN c.fromdate AND c.thrudate  
WHERE b.lastname = 'steffens';  

INSERT INTO pto_adjustments(employeenumber, fromdate, hours,reason,notes)
SELECT a.employeenumber, c.fromdate, 8, 'Rollover', 'per joel dangerfield'
FROM ptoemployees a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
INNER JOIN pto_employee_pto_allocation c on a.employeenumber = c.employeenumber
  AND curdate() BETWEEN c.fromdate AND c.thrudate  
WHERE b.lastname = 'johnson'
  AND b.firstname = 'brandon'; 
