Good Morning Jon can you change Richard Stallmo�s Hire  date it should be 08/04/09 
he started over in crookston before we moved him here 9/16/09
Kim Miller

SELECT * FROM tpemployees WHERE employeenumber = '1130700';

SELECT * FROM ptoemployees WHERE employeenumber = '1130700';

SELECT a.* 
FROM pto_employee_pto_allocation a
WHERE employeenumber = '1130700';

SELECT fromdate, fromdate - 43 AS new_from_date
FROM pto_employee_pto_allocation a
WHERE employeenumber = '1130700'
  AND month(fromdate) = 9 
  
SELECT thrudate, thrudate - 43 AS new_thru_date
FROM pto_employee_pto_allocation a
WHERE employeenumber = '1130700'
  AND month(thrudate) = 9   


BEGIN TRANSACTION;
TRY

UPDATE ptoEmployees
SET ptoAnniversary = '08/04/2009'
WHERE employeenumber = '1130700';

UPDATE pto_employee_pto_allocation
SET fromdate = fromdate - 43
WHERE employeenumber = '1130700'
  AND month(fromdate) = 9;

UPDATE pto_employee_pto_allocation
SET thrudate = thrudate - 43
WHERE employeenumber = '1130700'
  AND month(thrudate) = 9;
    
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;
