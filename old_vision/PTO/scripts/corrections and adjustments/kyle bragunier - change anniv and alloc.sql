/*
I talked to Ned and Kyle and his date should be 09/21/14  thanks Jon

From: Jon Andrews [mailto:jandrews@cartiva.com] 
Sent: Tuesday, March 24, 2015 9:09 AM
To: neuliss@rydellchev.com; Kim Miller; tbroyles@rydellcars.com
Cc: bfoster@rydellcars.com; Greg Sorum
Subject: Kyle Bragunier PTO

I received a voice mail from Ned yesterday, requesting that I change the Anniversary Date in Vision for Kyle from 2/4/13 to 9/21/14.
Sorry to go all pedantic on y�all, but, in my mind this is an issue of understanding what various terms mean.

In Arkona, there are 2 possible values relating to an employee�s hire date: Original Hire, Last Hire. (Kyle: Original: 9/21/14 & Last: 2/4/13)

In Compli, there is a single value: Hire Date (Kyle: 2/4/13)

In Vision, there is likewise a single value: Anniversary Date (Kyle: 2/4/13).  This is the one with which I am most familiar.  Specifically, in Vision, the anniversary date means the date from which PTO accrual is derived. That�s it, that is all it means.  Sometimes that will match the Arkona original hire date, sometimes it will match the Arkona last hire date, sometimes it will match neither one!

So, back to Kyle:
  If his Vision Anniversary Date is 2/4/13, then his current pto period is from 2/4/15 thru 2/3/16 for which he has earned 112 pto hours
  If his Vision Anniversary Date is 9/21/14, then his current pto period is from 9/21/14 thru 9/20/15 for which he has earned 0 pto hours.

We in the development team need to believe that HR,  Management and the affected employees are all on the same page and understand what is being displayed and how PTO is calculated and tracked.

So, given all that, I am not sure what, if any, changes I need to make in Vision.
*/


-- change his ptoAnniversary    
UPDATE ptoEmployees 
SET ptoAnniversary= '09/21/2014'
WHERE employeenumber = '118310';

-- remove allocation based on old anniversary
DELETE FROM pto_employee_pto_allocation
WHERE employeenumber = '118310';

-- allocation based on new anniversary

INSERT INTO pto_employee_pto_allocation (employeeNumber, fromdate, thrudate, hours)
SELECT c.employeenumber, 
  cast(timestampadd(sql_tsi_year, c.n, c.ptoAnniversary) as sql_date) AS fromDate,
  CASE c.n
    WHEN 20 THEN cast('12/31/9999' AS sql_date)
    ELSE cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, c.n + 1, c.ptoAnniversary)) AS sql_date) 
  END AS thruDate,
  ptoHours  
FROM (
  SELECT a.employeenumber, a.ptoAnniversary, b.*
  FROM ptoEmployees a,
    (
      SELECT n
      FROM dds.tally
      WHERE n BETWEEN 0 AND 20) b  
  WHERE a.employeenumber = '118310') c   
LEFT JOIN pto_categories d on c.n BETWEEN d.fromYearsTenure AND d.thruYearsTenure; 