-- ptoAnniversary: change FROM 11/02/99 to 12/21/94

SELECT a.*
FROM ptoEmployees a
INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
WHERE b.lastname = 'ramberg'

SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '1114105';

-- 12/21/2014 he goes FROM cat3 to cat4

SELECT a.*, 
  timestampadd(sql_tsi_year, n, fromdate) AS fromDate,
  timestampadd(sql_tsi_year, n, thrudate) AS thruDate
FROM (
  SELECT *
  FROM dds.tally a
  LEFT JOIN pto_categories b on a.n BETWEEN fromYearsTenure AND thruYearsTenure
  WHERE n BETWEEN 0 AND 20) a
LEFT JOIN (
  SELECT thedate AS fromDate, 
    timestampadd(sql_tsi_day, -1, timestampadd(sql_tsi_year, 1, thedate)) AS thruDate 
  FROM dds.day
  WHERE theDate =  '12/21/1994') b on 1 = 1

 
 so for 1/1/13 to 12/31/14 he earned ? cat3 OR cat 4
 1/13/13 -> 11/1/14 152 hours
 11/2/14 -> 12/20/14 + 25 hours (portion of the anniv year @ cat4
   SELECT 192*(timestampdiff(sql_tsi_day, '11/02/2014', '12/20/2014')/365.0)
   FROM system.iota;
12/21/2014 -> 12/20/2015 192 hours   

BEGIN TRANSACTION;
TRY 
UPDATE ptoEmployees
SET ptoAnniversary = '12/21/1994',
    anniversaryType = 'Other'
WHERE employeenumber = '1114105';

DELETE FROM pto_employee_pto_allocation
WHERE employeenumber = '1114105';

INSERT INTO pto_employee_pto_allocation(employeenumber, fromdate,thrudate,hours)
values('1114105', '01/01/2014', '12/20/2015', 369);

INSERT INTO pto_employee_pto_allocation(employeenumber, fromdate,thrudate,hours)
values('1114105', '12/21/2015', '12/31/9999', 192);

CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 


bob kvasager
181665

SELECT *
FROM pto_employee_pto_allocation
WHERE employeenumber = '181665'

SELECT *
FROM pto_employee_pto_allocation
WHERE employeenumber = '1147062'

SELECT *
FROM pto_employee_pto_allocation
WHERE employeenumber = (
  SELECT employeenumber
  FROM ptoemployees
  WHERE username like 'swal%')