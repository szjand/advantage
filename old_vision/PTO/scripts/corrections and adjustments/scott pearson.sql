-- change start date FROM 05/01/2002 to 03/17/1994

SELECT *
FROM ptoemployees a
LEFT JOIN pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
WHERE username = 'spearson@rydellcars.com'

SELECT *
FROM ptoemployees a
LEFT JOIN pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
WHERE year(ptoAnniversary) = 1994

UPDATE ptoEmployees
  SET ptoAnniversary = '03/17/1994'
WHERE employeenumber = '1109815'

scotts tenure at rollout
SELECT timestampdiff(sql_tsi_year,
cast('03/17/1994 00:00:00' AS sql_timestamp), 
  cast('11/01/2014 00:00:00' AS sql_timestamp))
FROM system.iota

BEGIN TRANSACTION;
TRY 

DELETE FROM pto_employee_pto_allocation
WHERE employeenumber = '1109815';

INSERT INTO pto_employee_pto_allocation    
SELECT '1109815', '01/01/2014', '03/16/2015', round(192 * timestampdiff(sql_tsi_day, cast('01/01/2014 00:00:00' AS sql_timestamp), 
  cast('03/17/2015 00:00:00' AS sql_timestamp))/365.0, 0)
FROM system.iota;

INSERT INTO pto_employee_pto_allocation    
SELECT '1109815', '03/17/2015', '12/31/9999', 192
FROM system.iota;

COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  


