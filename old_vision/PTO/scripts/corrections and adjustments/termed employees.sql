-- ptoEmployees that are currently termed IN arkona
SELECT a.employeenumber, a.username
INTO #term
FROM ptoemployees a
inner JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active <> 'active'
-- LEFT JOIN tpEmployees c on a.employeenumber = c.employeenumber  
-- LEFT JOIN pto_compli_users d on a.employeenumber = d.userid
-- LEFT JOIN employeeAppAuthorization e on c.username = e.username


termed employee:
1. remove authorization
2. remove tpEmployees
3. remove ptoEmployees

DELETE 
FROM employeeAppAuthorization
WHERE username IN (
  SELECT username
  FROM #term);
  
DELETE 
FROM tpEmployees
WHERE username IN (
  SELECT username
  FROM #term);
  
DELETE 
FROM ptoEmployees
WHERE username IN (
  SELECT username
  FROM #term);  
  