-- all people for whom andrew IS ultimately reaponsible
SELECT descendantDepartment, descendantPosition, d.firstname, d.lastname,
  d.fullparttime
FROM ptoClosure a
INNER JOIN ptoPositionFulfillment b on a.descendantDepartment = b.department
  AND a.descendantPosition = b.Position
INNER JOIN ptoEmployees c on b.employeeNumber = c.employeenumber
INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
  AND d.currentrow = true 
WHERE ancestorDepartment = 'RY1 service'
  AND ancestorPosition = 'Director'
  AND depth <> 0

-- andrew people, with unscheduled pto remaining due to be taken before 3/1/15 AND NOT scheduled
SELECT trim(f.descendantDepartment) + ' : ' + f.descendantPosition, 
  f.employeenumber, f.firstname, f.lastname, f.fullparttime, f.ptoAtRollout,
  f.ptoThru,
  coalesce(g.ptoUsed, 0) AS ptoUsed, coalesce(h.ptoRequested, 0) AS ptoRequested,
  f.ptoAtRollout - coalesce(g.ptoUsed, 0) - coalesce(h.ptoRequested, 0)
FROM (
  SELECT descendantDepartment, descendantPosition, c.employeenumber, d.firstname, d.lastname,
    d.fullparttime, e.ptoAtRollout, e.ptoFrom, e.ptoThru
  FROM ptoClosure a
  INNER JOIN ptoPositionFulfillment b on a.descendantDepartment = b.department
    AND a.descendantPosition = b.Position
  INNER JOIN ptoEmployees c on b.employeeNumber = c.employeenumber
  INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
    AND d.currentrow = true 
  LEFT JOIN pto_at_rollout e on d.employeenumber = e.employeenumber  
  WHERE ancestorDepartment = 'RY1 service'
    AND ancestorPosition = 'Director'
    AND depth <> 0
    AND ptoThru < '03/01/2015') f
LEFT JOIN ( -- this IS one that IS driving me buggy, what IF it's 2015 AND i am NOT USING pto_at_rollout
  SELECT a.employeeNumber, SUM(a.hours) AS ptoUsed
  FROM pto_used a
  INNER JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
    AND a.theDate BETWEEN b.ptoFrom AND curdate()
  GROUP BY a.employeenumber) g on f.employeenumber = g.employeenumber
LEFT JOIN (
  SELECT employeenumber, SUM(hours) AS ptoRequested
  FROM pto_requests
  WHERE theDate BETWEEN curdate() AND '03/15/2015'
  GROUP BY employeenumber) h on f.employeenumber = h.employeenumber 
-- ORDER BY descendantDepartment, depth, descendantPosition

-- same thing for randy
/* 
hmmm, currently, username EXISTS only IN tpEmployees
so, how to unify it ALL
edwEmployeeDim -> tpEmployees currently PK IS username
               -> ptoEmployees currently PK IS employeenumber
the short fix, ADD username to ptoEmployees, let employeenumber remain AS 
  the primary key, ADD username AS a unique index
see ptoPositionFulfillment.sql  
  
SELECT *
FROM ptoEmployees a
LEFT JOIN tpEmployees b on a.employeenumber = b.employeenumber                              
*/

DECLARE @username string;
DECLARE @department string;
DECLARE @employeeNumber string;
DECLARE @position string;
@username = 'bcahalan@rydellchev.com';
@employeenumber = (
  SELECT employeeNumber
  FROM ptoEmployees
  WHERE username = @username);
@department = (
  SELECT department 
  FROM ptoPositionFulFillment
  WHERE employeeNumber = @employeeNumber);
@position = (
  SELECT position
  FROM ptoPositionFulfillment
  WHERE employeenumber = @employeeNumber);  
SELECT f.employeenumber, f.firstname, f.lastname, 
  trim(f.descendantDepartment) + ' : ' + f.descendantPosition AS position,
  f.fullparttime, payrollClass, f.ptoAtRollout, ptoAnniversary,
  f.ptoThru,
  coalesce(g.ptoUsed, 0) AS ptoUsed, coalesce(h.ptoRequested, 0) AS ptoRequested,
  f.ptoAtRollout - coalesce(g.ptoUsed, 0) - coalesce(h.ptoRequested, 0) AS ptoRemaining   
FROM (
  SELECT descendantDepartment, descendantPosition, c.employeenumber, d.firstname, d.lastname,
    d.fullparttime, d.payrollclass, e.ptoAtRollout, e.ptoFrom, e.ptoThru, c.ptoAnniversary
  FROM ptoClosure a
  INNER JOIN ptoPositionFulfillment b on a.descendantDepartment = b.department
    AND a.descendantPosition = b.Position
  INNER JOIN ptoEmployees c on b.employeeNumber = c.employeenumber
  INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
    AND d.currentrow = true 
  LEFT JOIN pto_at_rollout e on d.employeenumber = e.employeenumber  
  WHERE ancestorDepartment = @department
    AND ancestorPosition = @position
    AND depth <> 0
    AND ptoThru < '03/01/2015') f
LEFT JOIN ( -- this IS one that IS driving me buggy, what IF it's 2015 AND i am NOT USING pto_at_rollout
  SELECT a.employeeNumber, SUM(a.hours) AS ptoUsed
  FROM pto_used a
  INNER JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
    AND a.theDate BETWEEN b.ptoFrom AND curdate()
  GROUP BY a.employeenumber) g on f.employeenumber = g.employeenumber
LEFT JOIN (
  SELECT employeenumber, SUM(hours) AS ptoRequested
  FROM pto_requests
  WHERE theDate BETWEEN curdate() AND '03/15/2015'
  GROUP BY employeenumber) h on f.employeenumber = h.employeenumber 
WHERE f.ptoAtRollout - coalesce(g.ptoUsed, 0) - coalesce(h.ptoRequested, 0) > 0  


-- ADD ptoAdmin
DECLARE @username string;
DECLARE @department string;
DECLARE @employeeNumber string;
DECLARE @position string;
@username = 'bcahalan@rydellchev.com';
@employeenumber = (
  SELECT employeeNumber
  FROM ptoEmployees
  WHERE username = @username);
@department = (
  SELECT department 
  FROM ptoPositionFulFillment
  WHERE employeeNumber = @employeeNumber);
@position = (
  SELECT position
  FROM ptoPositionFulfillment
  WHERE employeenumber = @employeeNumber);  
SELECT f.employeenumber, f.firstname, f.lastname, 
  trim(f.descendantDepartment) + ' : ' + f.descendantPosition AS position,
  f.fullparttime, f.payrollClass, f.ptoAtRollout, ptoAnniversary,
  f.ptoThru,
  coalesce(g.ptoUsed, 0) AS ptoUsed, coalesce(h.ptoRequested, 0) AS ptoRequested,
  f.ptoAtRollout - coalesce(g.ptoUsed, 0) - coalesce(h.ptoRequested, 0) AS ptoRemaining ,
  TRIM(k.firstname) + ' ' + k.lastname AS ptoAdmin  
FROM (
  SELECT descendantDepartment, descendantPosition, c.employeenumber, d.firstname, d.lastname,
    d.fullparttime, d.payrollclass, e.ptoAtRollout, e.ptoFrom, e.ptoThru, c.ptoAnniversary
  FROM ptoClosure a
  INNER JOIN ptoPositionFulfillment b on a.descendantDepartment = b.department
    AND a.descendantPosition = b.Position
  INNER JOIN ptoEmployees c on b.employeeNumber = c.employeenumber
  INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
    AND d.currentrow = true 
  LEFT JOIN pto_at_rollout e on d.employeenumber = e.employeenumber  
  WHERE ancestorDepartment = @department
    AND ancestorPosition = @position
    AND depth <> 0
    AND ptoThru < '03/01/2015') f
LEFT JOIN ( -- this IS one that IS driving me buggy, what IF it's 2015 AND i am NOT USING pto_at_rollout
  SELECT a.employeeNumber, SUM(a.hours) AS ptoUsed
  FROM pto_used a
  INNER JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
    AND a.theDate BETWEEN b.ptoFrom AND curdate()
  GROUP BY a.employeenumber) g on f.employeenumber = g.employeenumber
LEFT JOIN (
  SELECT employeenumber, SUM(hours) AS ptoRequested
  FROM pto_requests
  WHERE theDate BETWEEN curdate() AND '03/01/2015'
  GROUP BY employeenumber) h on f.employeenumber = h.employeenumber 
LEFT JOIN ptoAuthorization i on f.descendantDepartment = i.authForDepartment  
  AND f.descendantPosition = i.authForPosition
LEFT JOIN ptoPositionFulfillment j on i.authByDepartment = j.department
  AND i.authByPosition = j.position  
LEFT JOIN dds.edwEmployeeDim k on j.employeenumber = k.employeenumber
  AND k.currentrow = true  
WHERE f.ptoAtRollout - coalesce(g.ptoUsed, 0) - coalesce(h.ptoRequested, 0) > 0  


-- 10/2/14 here ALL ALL the ptoAdmins
SELECT a.authByDepartment, a.authByPosition, c.employeenumber, 
  trim(d.firstname) + ' ' + d.lastname
FROM (
  SELECT DISTINCT authByDepartment, authByPosition
  FROM ptoAuthorization) a
LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
LEFT JOIN ptoEmployees c on b.employeenumber = c.employeenumber
LEFT JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
  AND d.currentrow = true   