  
-- james kuefler exemption
SELECT *
FROM (  
  SELECT employeenumber, firstname, lastname, distcode
  FROM dds.edwEmployeeDim
  WHERE storecode = 'ry1'
    AND currentrow = true
    AND active = 'active') a  
INNER JOIN (
  SELECT employeenumber, firstname, lastname, distcode
  FROM dds.edwEmployeeDim
  WHERE storecode = 'ry2'
    AND currentrow = true
    AND active = 'active') b on a.firstname = b.firstname AND a.lastname = b.lastname    
          
-- no row IN ptoPositionFulfillment
SELECT *
FROM ptoEmployees a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
WHERE b.employeenumber IS NULL 

-- with ark dist/dept
SELECT a.*, c.pyDeptCode, c.pyDept, c.distCode, c.distribution
FROM ptoEmployees a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
  AND c.active = 'active'
WHERE b.employeenumber IS NULL 

-- only 2 are IN non goofy dept/dist: 241085, 213520: ry2 pdq/tpdq
-- fix these
INSERT INTO ptoPositionFulfillment values('RY2 PDQ','Advisor','241085');
INSERT INTO ptoPositionFulfillment values('RY2 PDQ','Upperbay Technician','213520');

-- of the goofies, exclude based on distcode?
-- see who ALL IS IN those distcodes      
-- nope distcode IS NOT enough, 
SELECT name, employeenumber, pydept, distcode
FROM dds.edwEmployeeDim
WHERE currentrow = true
  AND active = 'active'
  AND distcode IN (
    SELECT distinct c.distCode
    FROM ptoEmployees a
    LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
    LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
      AND c.currentrow = true 
      AND c.active = 'active'
    WHERE b.employeenumber IS NULL)  
ORDER BY name    


-- with ark dist/dept
shit, but need greg AS an admin

/*
exclude depts:
  94: Project
  84: Judy
  22: Hobby Shop
  26: Saturn Development
  25: Rydell Company
AND individuals:
  17166: Tony Arrington
  190915: Ben Marotte
  1160100: Steve Zoellick
  1137210: Kyle Theige  
  1120800: Wes
  16425: Jon
  1103050: Ron
  1130430: Mike
  1135265: Libby
  181310, 281325: James Kuefler
*/ 
-- exclude FROM ptoEmployees 
SELECT a.*, c.pyDeptCode, c.pyDept, c.distCode, c.distribution
FROM ptoEmployees a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
  AND c.active = 'active'
WHERE b.employeenumber IS NULL 
  AND c.pydeptcode NOT IN ('25','94','84','22','26')  
  AND c.employeenumber NOT IN ('17166','190915','1160100','1137210','1120800',
    '16425','1103050','1130430','1135265', '181310','281325')
    
    
SELECT a.*
FROM ptoEmployees a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
WHERE (b.pydeptcode IN ('25','94','84','22','26')  
  OR b.employeenumber IN ('17166','190915','1160100','1137210','1120800',
    '16425','1103050','1130430','1135265', '181310','281325'))  
    
SELECT employeenumber
FROM dds.edwEmployeeDim
WHERE currentrow = true
  AND active = 'active'
  AND (pydeptcode IN ('25','94','84','22','26')  
  OR employeenumber IN ('17166','190915','1160100','1137210','1120800',
    '16425','1103050','1130430','1135265', '181310','281325'))    
      
      
-- 9/28
CREATE TABLE pto_exclude (
  employeeNumber cichar(7) constraint NOT NULL,
constraint pk primary key (employeeNumber)) IN database;        
INSERT INTO pto_exclude
SELECT employeenumber
FROM dds.edwEmployeeDim
WHERE currentrow = true
  AND active = 'active'
  AND (pydeptcode IN ('25','94','84','22','26')  
  OR employeenumber IN ('17166','190915','1160100','1137210','1120800',
    '16425','1103050','1130430','1135265', '181310','281325'));
    
SELECT *
FROM ptoEmployees a
WHERE employeenumber IN (
  SELECT employeenumber
  FROM pto_exclude)
  

DELETE FROM ptoEmployees
WHERE employeenumber IN (
  SELECT employeenumber
  FROM pto_exclude);
  
-- 10/26/14
-- ADD sales:drivers to pto_exclude AND DELETE position FROM hierarchy
-- done on production server
select a.*, b.name, b.pydeptcode, b.pydept, b.distcode, b.fullparttime
FROM ptoPositionFulfillment a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
WHERE a.department LIKE '%sales%'
  AND a.position = 'driver'  
    
INSERT INTO pto_exclude (employeenumber)
SELECT employeenumber
FROM ptoPositionFulfillment
WHERE department LIKE '%sales%'
  AND position = 'driver'; 
  
DELETE FROM ptoEmployees
WHERE employeenumber IN (
  SELECT employeenumber
  FROM pto_exclude);  
-- RI takes care of ptoPositionFulfillment  
SELECT * FROM ptoPositionFulfillment
WHERE department LIKE '%sales%'
  AND position = 'driver';

DELETE 
FROM ptoAuthorization
WHERE authForDepartment LIKE '%sales%'
  AND authForPosition = 'driver';
  
DELETE 
FROM ptoClosure
WHERE descendantDepartment LIKE '%sales%'
  AND descendantPosition = 'driver';  
    
DELETE 
FROM ptoDepartmentPositions
WHERE department LIKE '%sales%'
  AND position = 'driver';
     
select *
FROM ptoClosure
WHERE descendantPosition = 'driver'    

-- 6/23/15
-- ADD kent brock, 118026, to pto_exclude, randy's lake maint guy
INSERT INTO pto_exclude values('118026');