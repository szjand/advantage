ALTER PROCEDURE pto_update_employee_pto(
  employeeNumber cichar(9),
  department cichar(30),
  position cichar(30),
  ptoAnniversaryDate date,
  ptoHoursEarned integer, 
  status CICHAR (4) OUTPUT,
  text cichar(500) output,
  mailError logical output)
BEGIN
/*
1.
IF the hours are being updated for a period that extends past the current year
this really means to ADD a row to pto_emp_pto_alloc
eg: 
FROM           thru          hours
10/10/12      10/9/14       72
10/10/14      10/9/22       112    
10/9/23       10/9/33       152
IF today IS 10/21 AND i am adjusting pto to 200 for the current year, the TABLE becomes
FROM           thru          hours
10/10/12      10/9/14       72
10/10/14      10/9/15       200
10/10/15      10/9/22       112    
10/9/23       10/9/33       152

IF the current period of more than the current year, the entire period does NOT
get adjusted, just the current year, requing the insertion of the row

one alternative IS to turn the pto_emp_pto_alloc TABLE to one row per employee
per year (based on anniversary date)

2. 
IF position IS a pto admin, disallow the change

EXECUTE PROCEDURE pto_update_employee_pto('2130690','RY2 Sales', 'Sales Consultant', '08/20/2007', 100);
*/
DECLARE @employeeNumber string;
DECLARE @newDepartment string;
DECLARE @newPosition string;
DECLARE @newPtoAnniversary date;
DECLARE @newHours integer;
DECLARE @curDepartment string;
DECLARE @curPosition string;
DECLARE @curPtoAnniversary date;
DECLARE @curHours integer;

@employeeNumber = (SELECT employeenumber FROM __input);
@newDepartment = (SELECT department FROM __input);
@newPosition = (SELECT position FROM __input);
@newPtoAnniversary = (SELECT ptoAnniversaryDate FROM __input);
@newHours = (SELECT ptoHoursEarned FROM __input);

@curDepartment = (
  SELECT department 
  FROM ptoPositionFulfillment 
  WHERE employeenumber = @employeeNumber);
@curPosition = (
  SELECT position
  FROM ptoPositionFulfillment
  WHERE employeeNumber = @employeeNumber);  
@curPtoAnniversary = (
  SELECT ptoAnniversary
  FROM ptoEmployees
  WHERE employeeNumber = @employeeNumber);
@curHours = (
  SELECT hours
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber
    AND curdate() BETWEEN fromDate AND thruDate);  
BEGIN TRANSACTION;
TRY 
IF @newDepartment <> @curDepartment THEN
  IF EXISTS (
    SELECT 1 
    FROM ptoPositionFulfillment a
    INNER JOIN ptoAuthorization b on a.department = b.authByDepartment
      AND a.position = b.authByPosition
    WHERE a.department = @newDepartment
      AND a.position = @newPosition) THEN 
    RAISE show_stoppers(6, TRIM(@newDepartment) + ':' + TRIM(@newPosition) + ' is already filled' collate ads_default_ci);
  ENDIF;
ENDIF;
IF @newPosition <> @curPosition THEN
  IF EXISTS (
    SELECT 1 
    FROM ptoPositionFulfillment a
    INNER JOIN ptoAuthorization b on a.department = b.authByDepartment
      AND a.position = b.authByPosition
    WHERE a.department = @newDepartment
      AND a.position = @newPosition) THEN 
    RAISE show_stoppers(6, TRIM(@newDepartment) + ':' + TRIM(@nenwPosition) + ' is already filled' collate ads_default_ci);
  ENDIF;
ENDIF;
IF @newPtoAnniversary <> @curPtoAnniversary THEN
  IF @newPtoAnniversary > curdate() THEN
    RAISE show_stoppers(7, 'Sorry, we can not allow you to enter a future date');          
  ENDIF;  
ENDIF;
IF @newHours <> @curHours THEN
ENDIF;

TRY 
  UPDATE ptoPositionFulfillment
  SET department = @newDepartment,
      position = @newPosition
  WHERE employeeNumber = @employeeNumber;
CATCH ALL 
  RAISE updateTable(1, 'Unable to update ptoPositionFulfillment');
END TRY;

TRY 
  UPDATE ptoEmployees
  SET ptoAnniversary = @newPtoAnniversary
  WHERE employeeNumber = @employeeNumber;
CATCH ALL
  RAISE updateTable(2, 'Unable to update ptoEmployees');
END TRY;

TRY   
  UPDATE pto_employee_pto_allocation
  SET hours = @newHours
  WHERE employeeNumber = @employeeNumber
    AND curdate() BETWEEN fromDate AND thruDate; 
CATCH ALL
  RAISE updateTable(3, 'Unable to update pto_employee_pto_allocation');
END TRY;
     
COMMIT WORK;
INSERT INTO __output
values ('Pass','Hoo-fucking-Ray', false);

CATCH ALL
  ROLLBACK;
  IF __errclass IN ('show_stoppers','updateTable') THEN 
    INSERT INTO __output values('Fail', __errclass + ': ' + __errtext, false); 
  ELSE
    INSERT INTO __output values('Fail', 'This is an unhandled exception :: ' +
      __errclass + ' :: ' + __errtext, false);
  ENDIF;
END TRY;
END;