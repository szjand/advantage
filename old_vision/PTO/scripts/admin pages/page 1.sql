CREATE PROCEDURE pto_get_new_employees (
  employeeNumber cichar(9) output,
  name cichar(52) output,
  latestHireDate date output,
  originalHireDate date output)
BEGIN
/*
exposing only full time hourly employees NOT IN pto_exclude
EXECUTE PROCEDURE pto_get_new_employees();
*/
INSERT INTO __output 
SELECT a.employeenumber, TRIM(a.firstname) + ' ' + a.lastname AS name, a.hiredate, 
  d.ymhdto
FROM dds.edwEmployeeDim a
LEFT JOIN pto_exclude b on a.employeenumber = b.employeenumber
LEFT JOIN ptoEmployees c on a.employeenumber = c.employeenumber
LEFT JOIN dds.stgArkonaPYMAST d on a.employeenumber = d.ymempn
WHERE a.currentRow = true
  AND a.active = 'active'
  AND a.payrollclass = 'hourly'
  AND a.fullparttime = 'full'
  AND b.employeenumber IS NULL
  AND c.employeenumber IS NULL; 
END;  

CREATE PROCEDURE pto_search_for_employee(
  search cichar(52),
  employeeNumber cichar(9) output,
  name cichar(52) output,
  latestHireDate date output,
  originalHireDate date output)
BEGIN
/*
should this be based on only who IS currently IN ptoEmployees
yes
EXECUTE PROCEDURE pto_search_for_employee('sorum')
*/
DECLARE @search string;
@search = (SELECT search FROM __input);
INSERT INTO __output 
SELECT a.employeenumber, TRIM(a.firstname) + ' ' + a.lastname AS name, a.hiredate, 
  d.ymhdto
FROM dds.edwEmployeeDim a
INNER JOIN ptoEmployees c on a.employeenumber = c.employeenumber
LEFT JOIN pto_exclude b on a.employeenumber = b.employeenumber
LEFT JOIN dds.stgArkonaPYMAST d on a.employeenumber = d.ymempn
WHERE a.currentRow = true
  AND a.active = 'active'
--  AND a.payrollclass = 'hourly'
--  AND a.fullparttime = 'full'
  AND b.employeenumber IS NULL
  AND (
    username LIKE '%' + TRIM(@search) + '%' 
    OR firstname LIKE '%' + TRIM(@search) + '%' 
    OR lastname LIKE '%' + TRIM(@search) + '%');  
END; 
    
  
  
CREATE PROCEDURE pto_get_departments (
  department cichar(30) output)
BEGIN
/*
execute procedure pto_get_departments();
*/
INSERT INTO __output
SELECT top 200 department
FROM ptoDepartments
ORDER BY department;
END;  

CREATE PROCEDURE pto_get_positions_for_department (
  department cichar(30),
  position cichar(30) output)
BEGIN
/*
EXECUTE procedure pto_get_positions_for_department('ry2 pdq');
*/
DECLARE @department string;
@department = (SELECT lower(department) FROM __input);
INSERT INTO __output
SELECT top 100 position
FROM ptoDepartmentPositions
WHERE department = @department
ORDER BY position;
END;

CREATE PROCEDURE pto_get_employee_to_edit (
  employeeNumber cichar(9),
  employeeNumber cichar(9) output,
  name cichar(52) output,
  department cichar(30) output,
  position cichar(30) output,
  ptoAdministrator cichar(52) output,
  ptoAnniversary date output,
  ptoHoursEarned double output,
  ptoHoursUsed double output,
  ptoHoursRemaining double output)
BEGIN 
/*
fucking hosana IS this it
IF anniv date IS IN 2014, THEN, ew, NOT quite almost
the issue of determining pto status on the fly based
on current date
SELECT *
FROM ptoemployees a
LEFT JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
WHERE year(ptoanniversary) = 2013

EXECUTE PROCEDURE pto_get_employee_to_edit('264235');
*/
--DECLARE @username string;
DECLARE @employeeNumber string;
--@username = (select username from __input);--'jolson@rydellcars.com';
--@employeenumber = (
--  SELECT employeenumber
--  FROM ptoEmployees
--  WHERE username = @username);
@employeeNumber = (SELECT employeeNumber from __input);
INSERT INTO __output  
select h.employeenumber, h.name, h.department, h.position, 
  h.ptoAdmin, h.ptoAnniversary, 
  h.ptoAtRollout, coalesce(i.ptoUsed, 0) AS ptoUsed, 
  h.ptoAtRollout - coalesce(i.ptoUsed, 0) AS ptoRemaining
FROM (
  SELECT a.employeenumber, a.ptoAnniversary, 
    TRIM(b.firstname) + ' ' + b.lastname as name, b.storecode,
    b.fullPartTime, 
    c.department, c.position,
    trim(f.firstname) + ' ' + TRIM(f.lastname) AS ptoAdmin,
    g.ptoAtRollout, g.pto2015Start - 1 AS ptoUntil
  FROM ptoEmployees a
  INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  INNER JOIN ptoPositionFulfillment c on a.employeenumber = c.employeenumber  
  INNER JOIN ptoAuthorization d on c.department = d.authForDepartment
    AND c.position = d.authForPosition
  INNER JOIN ptoPositionFulfillment e on d.authByDepartment = e.department 
    AND d.authByPosition = e.position 
  INNER JOIN dds.edwEmployeeDim f on e.employeenumber = f.employeenumber 
    AND f.currentrow = true 
  INNER JOIN pto_at_rollout g on a.employeenumber = g.employeenumber
  WHERE a.employeenumber = @employeeNumber) h
LEFT JOIN ( --ptoUsed: LEFT JOIN, may NOT be any used
  SELECT a.employeenumber, SUM(a.hours) AS ptoUsed
  FROM pto_used a
  INNER JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
  WHERE a.employeenumber = @employeeNumber
    AND a.theDate BETWEEN b.ptoFrom AND b.ptoThru
  GROUP BY a.employeenumber) i on h.employeenumber = i.employeenumber; 
END;     

CREATE PROCEDURE pto_get_administrator_for_position (
  department cichar(30),
  position cichar(30),
  administrator cichar(52) output)
BEGIN
/*
EXECUTE PROCEDURE pto_get_administrator_for_position('ry1 drive','advisor');
*/
DECLARE @department string;
DECLARE @position string;
@department = (SELECT department FROM __input);
@position = (SELECT position FROM __input);
INSERT INTO __output
SELECT trim(c.firstname) + ' ' + c.lastname
FROM ptoAuthorization a
INNER JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
WHERE a.authForDepartment = @department
  AND a.authForPosition = @position;
END;  

CREATE PROCEDURE pto_get_employee_info (
  employeeNumber cichar(9),
  employeeNumber cichar(9) output,
  name cichar(52) output,
  store cichar(12) output,
  originalHireDate date output,
  latestHireDate date output)
BEGIN
/*
EXECUTE PROCEDURE pto_get_employee_info('186100');
*/  
DECLARE @employeeNumber string;
@employeeNumber = (SELECT employeeNumber FROM __input);
INSERT INTO __output 
SELECT @employeeNumber, TRIM(a.firstName) + ' ' + a.lastName, 
  CASE a.storecode
    WHEN 'RY1' THEN 'RY1 - GM'
    WHEN 'RY2' THEN 'RY2 - Honda'
  END AS store, 
  a.hiredate, b.ymhdto
FROM dds.edwEmployeeDim a
LEFT JOIN dds.stgArkonaPYMAST b on a.employeenumber = b.ymempn
WHERE employeenumber = @employeenumber
  AND currentrow = true;  
END;

CREATE PROCEDURE pto_insert_new_employee (
  employeeNumber cichar(9),
  department cichar(30),
  position cichar(30),
  ptoHoursEarned double,
  ptoAnniversaryDate date)
BEGIN
/*
this needs a fuck of a lot of WORK:
  1. mandatory relationships
  2. WHERE the fuck am i going to get username
EXECUTE PROCEDURE pto_insert_new_employee (
'666',
'ry1 pdq',
'advisor',
0,
curdate());

DELETE FROM ptoEmployees WHERE employeenumber = '666';
*/  
DECLARE @employeeNumber string;
DECLARE @department string;
DECLARE @position string;
DECLARE @ptoHoursEarned double;
DECLARE @ptoAnniversaryDate date;
@employeenumber = (SELECT trim(employeeNumber) FROM __input);
@department = (SELECT department FROM __input);
@position = (SELECT position FROM __input);
@ptoHoursEarned = (SELECT ptoHoursEarned FROM __input);
@ptoAnniversaryDate = (SELECT ptoAnniversaryDate FROM __input);
INSERT INTO ptoEmployees
values (@employeeNumber, ptoAnniversary, TRIM(@employeenumber) + '@wtf.com',
  'made up');
END;  
 
/*  
-- employees BY department
SELECT a.employeenumber, trim(c.firstname) + ' ' + c.lastname AS name, 
  TRIM(b.department) + ':' + b.position AS title, 
  a.ptoAnniversary,
  d.ptoAtRollout, coalesce(e.hours, 0) AS ptoUsed,
  d.ptoAtRollout - coalesce(e.hours, 0) AS hoursRemaining
FROM ptoEmployees a
INNER JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true
LEFT JOIN pto_at_rollout d on a.employeenumber = d.employeenumber  
LEFT JOIN (
  select employeeNumber, sum(hours) AS hours
  from pto_used
  GROUP BY employeenumber) e on a.employeenumber = e.employeenumber
WHERE b.department = 'ry1 pdq'  
   
-- employees BY position   
SELECT a.employeenumber, trim(c.firstname) + ' ' + c.lastname AS name, 
  TRIM(b.department) + ':' + b.position AS title, 
  a.ptoAnniversary,
  d.ptoAtRollout, coalesce(e.hours, 0) AS ptoUsed,
  d.ptoAtRollout - coalesce(e.hours, 0) AS hoursRemaining
FROM ptoEmployees a
INNER JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true
LEFT JOIN pto_at_rollout d on a.employeenumber = d.employeenumber  
LEFT JOIN (
  select employeeNumber, sum(hours) AS hours
  from pto_used
  GROUP BY employeenumber) e on a.employeenumber = e.employeenumber
WHERE b.position = 'Upperbay Technician'

SELECT *
FROM pto_at_rollout
*/