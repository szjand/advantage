alter PROCEDURE pto_insert_new_employee
   ( 
      employeeNumber CICHAR ( 9 ),
      department CICHAR ( 30 ),
      position CICHAR ( 30 ),
      ptoHoursEarned DOUBLE ( 15 ),
      ptoAnniversaryDate DATE,
      status CICHAR (4) OUTPUT,
      text cichar(500) output,
      username cichar(50) output,
      password cichar(8) output,
      mailError logical output
   ) 
BEGIN
/*
this needs a fuck of a lot of WORK:
  1. mandatory relationships
  2. WHERE the fuck am i going to get username
  
10/20/14
  tables to be populated:
    ptoEmployees
    ptoPositionFulfillment
    pto_employee_pto_allocation
oh shit, also need the employeeAppAuthorization for pto apps  
which means they need to be IN tpEmployees which ideally means
tpEmployees.username AND ptoEmployees.username are the same  
but ain't yet the truth


oh fuck me, ptoEmployees knows squat about passwords
enuf for tonight

SELECT *
FROM tpEmployees a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
WHERE a.username <> b.username

EXECUTE PROCEDURE pto_insert_new_employee (
'163099',
'ry1 main shop',
'technician (c)',
0,
'09/30/2014');

-- jay olson test of conflicting usernames - that worked
DELETE FROM ptoEmployees WHERE employeenumber = '1106399';

EXECUTE PROCEDURE pto_insert_new_employee (
'163099',
'ry1 main shop',
'technician (c)',
0,
'09/30/2014');


errors:
  position IS a ptoAdmin that IS already populated
  
DELETE FROM ptoEmployees WHERE employeeNumber = '163099'

need to detect possible transfer
need to expose unhandled error IN text

ahh fuck CASE:
  emp EXISTS IN tpEmployees BY employeenumber, but does NOT yet exist IN pto
  username IN tpEmployees IS xxx@rydellchev.
  fuck it: the basic issue IS synchronizing tpEmployees.username with compli.userid
  
DELETE FROM ptoEmployees WHERE employeenumber IN ('264235','163099','161010','196525','1147325','111210','1135340');
DELETE FROM employeeAppAuthorization WHERE username IN (
  SELECT username 
  FROM tpEmployees 
  WHERE employeenumber IN (
    '264235','163099','161010','196525','1147325','111210','1135340'));
DELETE FROM tpEmployees WHERE employeenumber IN ('264235','163099','161010','196525','1147325','111210','1135340');   
DELETE FROM pto_employee_pto_allocation WHERE employeenumber IN ('264235','163099','161010','196525','1147325','111210','1135340');   

10/24/14 OPEN items
  what about the transfers
  setting anniv date to future - done IN stored proc, could possible be client side
  
*/  
 
DECLARE @employeeNumber string;
DECLARE @department string;
DECLARE @position string;
DECLARE @ptoHoursEarned double;
DECLARE @ptoAnniversaryDate date;
DECLARE @anniversaryType string;
DECLARE @userName string;
DECLARE @appRole string;

DECLARE @i integer;
DECLARE @pos1 integer;
DECLARE @pos2 integer;
DECLARE @password string;
DECLARE @str string;


@employeenumber = (SELECT trim(employeeNumber) FROM __input);
@department = (SELECT department FROM __input);
@position = (SELECT position FROM __input);
@ptoHoursEarned = (SELECT ptoHoursEarned FROM __input);
@ptoAnniversaryDate = (SELECT ptoAnniversaryDate FROM __input);

@anniversaryType = (
  SELECT 
    CASE 
      WHEN @ptoAnniversaryDate = a.ymhdte THEN 'Latest'
      WHEN @ptoAnniversaryDate = a.ymhdto THEN 'Original'
      ELSE 'Other'
    END 
  FROM dds.stgArkonaPYMAST a
  WHERE ymempn = @employeeNumber);
  
@userName = (
  SELECT email
  FROM pto_compli_users
  WHERE userid = @employeeNumber);   
  
@str = 'LoremXesumdoLorspnametconsecteturadXeYacWngeLpnDonecetorcWanuncmattYa'+
  'auctorVWvamusWacuLYanonLacusegetmattYaCrasetLacWnWadoLorPeLLentesquefacWLY'+
  'aYaveLpnduWacvarWussemconsecteturquYaAeneanWnterdumquamatLWberopharetraWnt'+
  'erdumMaecenasspnametuLtrWcWesnuncVWvamusmaLesuadatortorposuereLuctusdapWbu'+
  'sVestWbuLumaWnterdumquamSuspendYaseactemporLectusCrasmattYaanteveLsuscXepn'+
  'semperodWoveLpnLaoreetteLLusveLsoLLWcpnudWnquamerosaLeoALWquamdapWbusnuncL'+
  'WberononconguejustovuLputatevpnaePhaseLLusnuLLaarcumaLesuadaWnterdummoLest'+
  'WeutconsequatettortorSeddWctumnequeegetnYaWLaoreetcongueProWnrhoncusrutrum'+
  'congueMaecenasvBLoremsagpntYavWverrafeLYadWgnYasWmbWbendumLectuQuYaquetWnc'+
  'vBuntauctormagnaconvaLLYavestWbuLumMaurYapuLvWnaretmetusnonhendrerpnNuncbW'+
  'bendumsemdWctumpLaceratposuerenuncveLpnaccumsanrYausacvuLputateXesumtortor'+
  'spnametmassaFusceuLtrWcWeseratnoneLeWfendcommodoNuLLanonerosvBjustotWncvBu'+
  'ntportaanonnuLLaPraesentmattYafeugWattWncvBuntALWquamsedsuscXepnodWoQuYaqu'+
  'evenenatYautarcuegetvenenatYaVestWbuLumuLtrWcWesadXeYacWngvuLputateNuLLase'+
  'dLacusLoremVWvamusLacWnWasceLerYaquerYausacsodaLesmaurYaconsequatnonSedtWn'+
  'cvBuntatmaurYavBdapWbusDuYautteLLusenWmCurabpnurfermentumerosacLWguLahendr'+
  'erpnfeugWatcondWmentumturpYatWncvBuntPhaseLLusvestWbuLumsapWenegetornarevo'+
  'LutpatUtpeLLentesqueturpYanWbhnecvuLputateteLLusLobortYavpnaePraesentcursu'+
  'snuLLauteratpretWumpretWumVestWbuLumfeugWattortorvBporttpnorvWverraodWonYa'+
  'WmoLestWeorcWvpnaeWnterdumdoLorLWberovBsapWenQuYaquevpnaeconguemassaeugrav'+
  'vBasemUtcongueWnsemetmoLLYaNuLLamnonmagnanuncSedvBcommodoLWguLaEtWamquYava'+
  'rWussapWenNuncvBsemenWmSuspendYaseLacWnWanecodWoatsodaLesVWvamussedduWvuLp'+
  'utatevenenatYapurusdapWbusmoLestWeteLLusMaurYaveLveLpnquYaLWguLacongueeuYa'+
  'modegetetnequeSuspendYasenonLacWnWaLoremvBmattYaarcuPraesentseddoLornWbhCu'+
  'rabpnurconsecteturveLmWquYaWacuLYaLoremXesumdoLorspnametconsecteturadXeYac'+
  'WngeLpnInachendrerpnsapWenWndWctumLoremMorbWsedbLandpnteLLusPeLLentesquegr'+
  'avvBatemporpuruscommodoWacuLYaQuYaqueLobortYatempusLoremspnametLuctusSuspe'+
  'ndYasesemtortorornareWnodWospnametconsequatornarequam';
  
  @i = (SELECT frac_second(now()) FROM system.iota);
  @pos1 = (SELECT cast(right(trim(cast(rand(@i) AS sql_char)), 3) AS sql_integer) FROM system.iota) ;
  @pos2 = 2 * (SELECT cast(right(trim(cast(rand(@pos1) AS sql_char)), 3) AS sql_integer) FROM system.iota) ;
  @password = (
    SELECT substring(@str, @pos1, 3) + left(CAST(@i AS sql_char), 2) + 
      upper(substring(@str, @pos2, 1)) + substring(@str, @pos1 +@i, 2)
--  FROM t1);
    FROM system.iota);
--  SELECT @password FROM system.iota;
  
BEGIN TRANSACTION;
TRY 
--< show stoppers
  IF NOT EXISTS (
    SELECT 1
    FROM dds.edwEmployeeDim 
    WHERE employeenumber = @employeenumber
      AND currentrow = true) THEN 
    RAISE show_stoppers(1, 'Employee Number ' + @employeenumber + ' is invalid');
  ELSEIF NOT EXISTS (
    SELECT 1
    FROM ptoDepartments
    WHERE department = @department) THEN 
    RAISE show_stoppers(2, 'Department ' + TRIM(@department) + ' is invalid');
  ELSEIF NOT EXISTS (
    SELECT 1
    FROM ptoPositions
    WHERE position = @position) THEN 
    RAISE show_stoppers(3, 'Position ' + TRIM(@position) + ' is invalid');
  ELSEIF @ptoHoursEarned < 0 THEN
    RAISE show_stoppers(4, 'PTO Earned can not be less than 0'); 
  ELSEIF @ptoHoursEarned > 200 THEN
    RAISE show_stoppers(5, 'PTO Earned can not be greater than 200');  
  ELSEIF EXISTS (
    SELECT 1 
    FROM ptoPositionFulfillment a
    INNER JOIN ptoAuthorization b on a.department = b.authByDepartment
      AND a.position = b.authByPosition
    WHERE a.department = @department
      AND a.position = @position) THEN 
    RAISE show_stoppers(6, TRIM(@department) + ':' + TRIM(@position) + ' is already filled' collate ads_default_ci);
  ELSEIF @ptoAnniversaryDate > curdate() THEN
    RAISE show_stoppers(7, 'Sorry, we can not allow you to enter a future date');          
  ENDIF;    
--/> show stoppers  
-- new row IN ptoEmployees 
  TRY 
    INSERT INTO ptoEmployees (employeeNumber, ptoAnniversary, username, anniversaryType)
    values(@employeenumber, @ptoAnniversaryDate, @username, @anniversaryType);
  CATCH ALL
    RAISE ptoEmployees(101, 'Unable to insert ' + @employeenumber + ' into ptoEmployees');
  END TRY;  
-- new row IN ptoPosistionFulfillment
  TRY 
    INSERT INTO ptoPositionFulfillment (department, position, employeeNumber)
    values (@department, @position, @employeeNumber);
  CATCH ALL
    RAISE ptoPositionFulfillment(102, 'Unable to insert ' + @employeenumber + ' into ptoPositionFulfillment');
  END TRY;     
-- new rows IN pto_employee_pto_allocation
  TRY   
    INSERT INTO pto_employee_pto_allocation (employeeNumber, fromdate, thrudate, hours)
    SELECT @employeeNumber, @ptoAnniversaryDate, 
      cast(timestampadd(sql_tsi_year, 1, @ptoAnniversaryDate) AS sql_date) - 1,      
      CASE @ptoHoursEarned
        WHEN 0 THEN (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 0)
        ELSE @ptoHoursEarned
      END 
    FROM system.iota
    UNION ALL 
    SELECT @employeeNumber, cast(timestampadd(sql_tsi_year, 1, 
      @ptoAnniversaryDate) AS sql_date),
      cast(timestampadd(sql_tsi_year, 2, @ptoAnniversaryDate) AS sql_date) - 1,
      (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 1)
    FROM system.iota
    UNION ALL  
    SELECT @employeeNumber, cast(timestampadd(sql_tsi_year, 2, 
      @ptoAnniversaryDate) AS sql_date),
      cast(timestampadd(sql_tsi_year, 10, @ptoAnniversaryDate) AS sql_date) - 1,
      (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 2)
    FROM system.iota
    UNION ALL  
    SELECT @employeeNumber, cast(timestampadd(sql_tsi_year, 10, 
      @ptoAnniversaryDate) AS sql_date),
      cast(timestampadd(sql_tsi_year, 20, @ptoAnniversaryDate) AS sql_date) - 1,
      (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 3)
    FROM system.iota
    UNION ALL  
    SELECT @employeeNumber, cast(timestampadd(sql_tsi_year, 20, @ptoAnniversaryDate) AS sql_date),
      cast(timestampadd(sql_tsi_year, 100, @ptoAnniversaryDate) AS sql_date) - 1,
      (SELECT ptoHours FROM pto_categories WHERE ptoCategory = 4)
    FROM system.iota;  
  CATCH ALL
    RAISE pto_employee_pto_allocation(103, 'Uanble to insert ' + @employeenumber + ' into pto_employee_pto_allocation');
  END TRY; 

-- compli.email <> existing tpEmployees.username
    IF EXISTS (
      SELECT 1
      FROM tpEmployees
      WHERE employeeNumber = @employeeNumber) THEN 
        IF (
          SELECT username
          FROM tpEmployees
          WHERE employeenumber = @employeenumber) <> @username THEN
            RAISE ohmygod(1,'danger will robinson, username mismatch');
        ENDIF;
    ENDIF;
    
-- new row IN tpEmployees IF one does NOT already exist for the employee number    
  TRY
    IF NOT EXISTS (
      SELECT 1
      FROM tpEmployees
      WHERE employeeNumber = @employeeNumber) THEN 
    INSERT INTO tpEmployees
    SELECT @userName, firstname, lastname, employeenumber, @password, 'Fuck You', storecode, TRIM(firstname) + ' ' + lastname
    FROM dds.edwEmployeeDim
    WHERE employeenumber = @employeenumber
      AND currentrow = true;
    ENDIF; 
  CATCH ALL
    RAISE tpEmployees(105, 'Uanble to insert ' + @employeenumber + ' into tpEmployees');
  END TRY;
  
-- new row(s) IN employeeAppAuthorization   
  TRY 
    INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
    SELECT @userName, appName, 800, appCode, appRole, functionality
    FROM applicationMetadata
    WHERE appcode = 'pto'
      AND approle = 'ptoemployee';
  CATCH ALL
    RAISE employeeAppAuthorization(105, 'Uanble to insert ' + @employeenumber + ' into employeeAppAuthorization');
  END TRY;  
  
COMMIT WORK;
INSERT INTO __output
values ('Pass','Hoo-fucking-Ray', @username,@password, true);
CATCH ALL
  ROLLBACK;
  IF __errclass IN ('show_stoppers','ptoEmployees','ptoPositionFulfillment',
    'pto_employee_pto_allocation','tpEmployees','employeeAppAuthorization') THEN 
    INSERT INTO __output values('Fail', __errclass + ': ' + __errtext, '', '', true); 
  ELSE
    INSERT INTO __output values('Fail', 'This is an unhandled exception :: ' +
      __errclass + ' :: ' + __errtext, '', '', true);
  ENDIF;
END TRY;
  





END;
