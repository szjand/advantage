-- garrett evenson, 241085, rehired
/*
Garrett Evenson will be coming back as a Flat Rate Tech starting tomorrow, 8/1
Could you please add him back into vision with his PTO Anniversary date being 5/14/2007
He won�t have any vacation this year until he cycles again in May of 2019
*/
update ptoemployees
SET active = true
WHERE employeenumber = '241085';

select *
FROM pto_employee_pto_allocation
WHERE employeenumber = '241085'

select *
FROM pto_used
WHERE employeenumber = '241085'
  AND thedate > '05/14/2018'
  
SELECT *
FROM pto_adjustments  

INSERT into pto_adjustments values 
('241085', '05/14/2018', -152, 'adjustment','per Laura, no pto IN first year of rehire')


SELECT *
FROM ptopositionfulfillment
WHERE employeenumber = '241085'

SELECT *
FROM ptoemployees
WHERE employeenumber = '241085'


