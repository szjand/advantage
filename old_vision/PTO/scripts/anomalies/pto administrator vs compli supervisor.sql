SELECT a.*, c.firstname, c.lastname, c.payrollclass, c.fullparttime, d.username, 
  d.password,
  iif(e.username IS NOT NULL, true,false) AS hasPtoAccess
FROM (
  SELECT authByDepartment, authByPosition
  FROM ptoAuthorization
  WHERE authByLevel <> 1
    AND authByDepartment NOT IN ('ry1 sales','ry2 sales')
  GROUP BY authByDepartment, authByPosition) a
LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active'  
LEFT JOIN tpEmployees d on b.employeenumber = d.employeenumber    
LEFT JOIN (
  SELECT username
  FROM employeeAppAuthorization
  WHERE appcode = 'pto'
    AND approle = 'ptomanager'
  GROUP BY username) e on d.username = e.username
  
SELECT *
FROM ( 
SELECT a.userid, a.firstname, a.lastname, a.title, b.* 
FROM pto_compli_users a
full OUTER JOIN tpEmployees b on a.userid = b.employeenumber  
WHERE a.status = '1'
) x WHERE userid IS NULL 


SELECT *
FROM tpemployees a
LEFT JOIN pto_compli_users b on a.employeenumber = b.userid
WHERE b.userid IS NULL 

-- auth BY vs compli supervisor
SELECT a.userid, a.title, a.firstname, a.lastname, a.supervisorname AS compliSupervisor, 
  trim(e.firstname) + ' ' + e.lastname AS visionPtoAdministrator
FROM pto_compli_users a
LEFT JOIN ptoPositionFulfillment b on a.userid = b.employeenumber
LEFT JOIN ptoAuthorization c on b.department = c.authForDepartment
  AND b.position = c.authForPosition
LEFT JOIN ptoPositionFulfillment d on c.authByDepartment = d.department
  AND c.authByPosition = d.position
LEFT JOIN dds.edwEmployeeDim e on d.employeenumber = e.employeenumber
  AND e.currentrow = true  
   
WHERE a.status = '1'
  AND TRIM(e.firstname) + ' ' + e.lastname <> a.supervisorname collate ads_default_ci
  AND a.title NOT like 'Sales%'
  AND c.authByLevel <> 1
ORDER BY title  
