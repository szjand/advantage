employees with an anniv IN 2014, but current period.fromdate = 1/1/14
should actuall be anniversary date
11/11/14
ben foster agrees

-- the problem children
SELECT *
-- SELECT a.employeenumber INTO #badFromDates
FROM ptoEmployees a
INNER JOIN pto_employee_pto_allocation b on a.employeeNumber = b.employeeNumber
WHERE year(a.ptoAnniversary) = 2014
  AND b.fromDate = (
    SELECT MIN(fromDate)
    FROM pto_employee_pto_allocation
    WHERE employeenumber = b.employeeNumber)
  AND a.ptoAnniversary <> b.fromDate    
ORDER BY a.employeenumber  

SELECT a.*, b.ptoAnniversary, c.fromDate 
FROM #badFromDates a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN pto_employee_pto_allocation c on b.employeenumber = c.employeenumber
  AND curdate() BETWEEN c.fromdate AND c.thrudate
ORDER BY b.ptoAnniversary;


UPDATE pto_employee_pto_allocation
SET fromDate = x.ptoAnniversary
FROM (
  SELECT a.*, b.ptoAnniversary, c.fromDate 
  FROM #badFromDates a
  LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
  LEFT JOIN pto_employee_pto_allocation c on b.employeenumber = c.employeenumber
    AND curdate() BETWEEN c.fromdate AND c.thrudate) x  
WHERE pto_employee_pto_allocation.employeenumber = x.employeenumber
  AND pto_employee_pto_allocation.fromDate = x.fromDate;

-- looks good enuf to DO on production  
SELECT d.firstname, d.lastname, d.payrollclass, d.fullparttime, d.distribution, a.*, b.ptoAnniversary, c.fromDate, c.thrudate, c.thrudate - c.fromdate 
FROM #badFromDates a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN pto_employee_pto_allocation c on b.employeenumber = c.employeenumber
  AND curdate() BETWEEN c.fromdate AND c.thrudate
LEFT JOIN dds.edwEmployeeDim d on a.employeenumber = d.employeenumber
  AND d.currentrow = true  
WHERE d.payrollclass = 'hourly'
  AND d.fullparttime = 'full'  
ORDER BY ptoAnniversary