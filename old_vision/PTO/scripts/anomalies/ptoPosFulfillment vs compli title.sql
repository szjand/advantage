
SELECT a.*, b.name 
FROM ptoPositionFulfillment a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE department = 'ry1 pdq'
  AND position = 'lead technician'
  
SELECT a.employeenumber, left(a.username,20), b.title, b.title,c.position, c.department
FROM ptoEmployees a
LEFT JOIN pto_compli_users b on a.employeenumber = b.userid
LEFT JOIN ptoPositionFulfillment c on a.employeenumber = c.employeenumber
ORDER BY b.title