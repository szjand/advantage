SELECT * FROM dds.stgArkonaPYMAST

FROM bens spreadsheet, pto_formula_clean.xlsx, get the anniversary dates


SELECT a.employeenumber, a.firstname, a.lastname, a.hiredate, b.ymhdte, 
  b.ymhdto, c.anniversaryDate
FROM dds.edwEmployeeDim a
LEFT JOIN dds.stgArkonaPYMAST b on a.employeenumber = b.ymempn
LEFT JOIN pto_formula_clean_annDate c on a.employeenumber = c.employeenumber
WHERE a.currentRow = true
  AND a.activeCode = 'A'
-- a couple of anomalies  
  AND b.ymhdto <> c.anniversaryDate
  AND a.hiredate <> c.anniversaryDate
  
UPDATE pto_formula_clean_annDate
SET anniversaryDate = '11/21/2012'
WHERE employeenumber = '1120120';

UPDATE pto_formula_clean_annDate
SET anniversaryDate = '07/20/2011'
WHERE employeenumber = '1135710';