SELECT *
FROM #nov_roll_out
WHERE employeenumber = '193180'

SELECT a.*, trim(b.firstname) + ' ' + b.lastname AS name, b.fullPartTime, b.store,
  TRIM(c.department) + ' : ' + c.position AS title,
  trim(f.firstname) + ' ' + TRIM(f.lastname) AS ptoAdmin,
  pto_earned_at_rollout_hours AS ptoEarned,
  ptoConsumed AS ptoUsed,
  pto_earned_at_rollout_hours - ptoConsumed AS ptoRemaining,
  pto2015start - 1 AS ptoUntil
-- SELECT *  
FROM ptoEmployees a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
LEFT JOIN ptoPositionFulfillment c on a.employeenumber = c.employeenumber  
LEFT JOIN ptoAuthorization d on c.department = d.authForDepartment
  AND c.position = d.authForPosition
LEFT JOIN ptoPositionFulfillment e on d.authByDepartment = e.department 
  AND d.authByPosition = e.position 
LEFT JOIN dds.edwEmployeeDim f on e.employeenumber = f.employeenumber 
  AND f.currentrow = true 
LEFT JOIN #nov_roll_out g on a.employeenumber = g.employeenumber  
WHERE a.employeenumber = '193180'

-- consumed 
select *
FROM pto_used
WHERE employeenumber = '193180';

-- requested/scheduled    
SELECT *
FROM pto_requests
WHERE employeenumber = '193180'

-- calendar view ALL requested/consumed IN one

-- some notion of being within x time period of ptoExpiration AND OPEN pto NOT 
--   scheduled within that time frame
ivan anfilofieff 24210: exp date 1/2/15, of 112 hours due, only 14 have been taken

-- interesting use cases
blake mccoy 193180: has taken more pto than earned AND has an approved
      future request
      

-- this IS ALL well AND good, but hard coding the use of pto_at_rollout IS NOT
-- what i want to do
-- generalize it for any user
DECLARE @username string;
DECLARE @employeeNumber string;
@username = 'SWalior@rydellcars.com';
@employeenumber = (
  SELECT employeenumber
  FROM ptoEmployees
  WHERE username = @username);
select h.*, coalesce(i.ptoUsed, 0) AS ptoUsed, h.ptoAtRollout - coalesce(i.ptoUsed, 0) AS ptoRemaining
FROM (
  SELECT a.employeenumber, a.ptoAnniversary, 
    TRIM(b.firstname) + ' ' + b.lastname as name, b.storecode,
    b.fullPartTime, 
    TRIM(c.department) + ' : ' + c.position AS title,
    trim(f.firstname) + ' ' + TRIM(f.lastname) AS ptoAdmin,
    g.ptoAtRollout, g.pto2015Start - 1 AS ptoUntil
  FROM ptoEmployees a
  INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  INNER JOIN ptoPositionFulfillment c on a.employeenumber = c.employeenumber  
  INNER JOIN ptoAuthorization d on c.department = d.authForDepartment
    AND c.position = d.authForPosition
  INNER JOIN ptoPositionFulfillment e on d.authByDepartment = e.department 
    AND d.authByPosition = e.position 
  INNER JOIN dds.edwEmployeeDim f on e.employeenumber = f.employeenumber 
    AND f.currentrow = true 
  INNER JOIN pto_at_rollout g on a.employeenumber = g.employeenumber
  WHERE a.employeenumber = @employeeNumber) h
LEFT JOIN ( --ptoUsed: LEFT JOIN, may NOT be any used
  SELECT a.employeenumber, SUM(a.hours) AS ptoUsed
  FROM pto_used a
  INNER JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
  WHERE a.employeenumber = @employeeNumber
    AND a.theDate BETWEEN b.ptoFrom AND b.ptoThru
  GROUP BY a.employeenumber) i on h.employeenumber = i.employeenumber; 
  
-- pto used BY date  
DECLARE @username string;
DECLARE @employeeNumber string;
@username = 'SWalior@rydellcars.com';
@employeenumber = (
  SELECT employeenumber
  FROM ptoEmployees
  WHERE username = @username);
SELECT theDate, hours
FROM pto_used a
WHERE employeenumber = @employeeNumber  
  AND thedate BETWEEN (
    SELECT pto2014Start
    FROM pto_at_rollout
    WHERE employeenumber = @employeenumber) AND curdate();
    
-- OPEN requests
-- pto used BY date  
DECLARE @username string;
DECLARE @employeeNumber string;
@username = 'SWalior@rydellcars.com';
@employeenumber = (
  SELECT employeenumber
  FROM ptoEmployees
  WHERE username = @username);
SELECT * 
FROM pto_requests
WHERE employeenumber = @employeenumber
  AND thedate > curdate();
