SELECT trim(c.firstname) + ' ' + c.lastname, a.employeenumber, MAX(theDate), SUM(hours)
FROM pto_used a
INNER JOIN ptoemployees b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND a.source = 'sales dept'
GROUP BY trim(c.firstname) + ' ' + c.lastname, a.employeenumber 

-- populate with script FROM db2: HR-sales consultant vacation paid.sql
CREATE TABLE pto_tmp_sc_pto_paid (
  storecode cichar(3),
  employee_number cichar(7),
  name cichar(25),
  amount double,
  checkDate date);
UPDATE pto_tmp_sc_pto_paid
SET employee_number = TRIM(employee_number)
  
SELECT trim(c.firstname) + ' ' + c.lastname, a.employeenumber, theDate, hours
FROM pto_used a
INNER JOIN ptoemployees b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND a.source = 'sales dept'  

-- ALL pto paid FROM payroll, with pto already IN vision, generated pto hourly rate  
SELECT a.*, b.*, 
  CASE 
    WHEN b.hours IS NULL THEN 0
    ELSE round(a.amount/b.hours, 2)
  END AS "PTO Hourly Rate"
FROM pto_tmp_sc_pto_paid a 
LEFT JOIN pto_used b on a.employee_number = b.employeenumber
  AND a.checkDate = b.thedate
INNER JOIN dds.edwEmployeeDim c on a.employee_number = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active'  
ORDER BY a.storecode, a.name  
 
SELECT DISTINCT storecode, employee_number, name, "PTO Hourly Rate"
FROM (  
SELECT a.*, b.*, 
  CASE 
    WHEN b.hours IS NULL THEN 0
    ELSE round(a.amount/b.hours, 2)
  END AS "PTO Hourly Rate"
FROM pto_tmp_sc_pto_paid a 
LEFT JOIN pto_used b on a.employee_number = b.employeenumber
  AND a.checkDate = b.thedate
INNER JOIN dds.edwEmployeeDim c on a.employee_number = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active') c    
ORDER BY storecode, name  

-- 2/9/15  
-- sc pto rate AS determined BY data currently IN vision
SELECT storecode, employee_number, name, MAX([PTO Hourly Rate])
FROM (  
  SELECT DISTINCT storecode, employee_number, name, "PTO Hourly Rate"
  FROM (  
  SELECT a.*, b.*, 
    CASE 
      WHEN b.hours IS NULL THEN 0
      ELSE round(a.amount/b.hours, 2)
    END AS "PTO Hourly Rate"
  FROM pto_tmp_sc_pto_paid a 
  LEFT JOIN pto_used b on a.employee_number = b.employeenumber
    AND a.checkDate = b.thedate
  INNER JOIN dds.edwEmployeeDim c on a.employee_number = c.employeenumber
    AND c.currentrow = true
    AND c.active = 'active') c) d    
GROUP BY storecode, employee_number, name    
ORDER BY storecode, name    