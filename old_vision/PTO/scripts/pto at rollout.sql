
/*
-- remove those that have termed since orig spreadsheet
DELETE FROM ptoEmployees
-- select * FROM ptoEmployees
WHERE employeenumber IN (
  SELECT employeenumber
  FROM #nov_roll_out
  WHERE firstname IS NULL); 
*/  

-- get base values needed , THEN generate calcs
-- assume an 11/2/2014 rollout
-- DROP TABLE #nov_roll_out; 
/*
DECLARE @rollout date;
@rollout = curdate();--'09/01/2014';
SELECT e.*,
  timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 AS multiplier,
  CASE 
    WHEN pto_category = 0 THEN 0
    ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
      SELECT pto_hours
      FROM pto_categories
      WHERE pto_category = e.pto_category), 0)
  END AS pto_earned_at_rollout_hours,
  CASE 
    WHEN pto_category = 0 THEN 0
    ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
      SELECT pto_hours/8
      FROM pto_categories
      WHERE pto_category = e.pto_category), 0)
  END AS pto_earned_at_rollout_days  
INTO #nov_roll_out  
FROM ( -- emp
  SELECT a.*, b.pto_category, aa.firstname, aa.lastname, 
    b.from_date AS cat_from_date, b.thru_date as cat_thru_date, d.ptoConsumed,
    CASE year(pto_anniversary)
      -- IF hired IN 2014 THEN 1/1/2014
--      WHEN year(pto_anniversary) = 2014 THEN cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
      -- IF hired IN 2013, THEN anniv date IN 2014
      WHEN 2013 THEN cast(createTimestamp(2014, month(pto_anniversary), dayofmonth(pto_anniversary), 0,0,0,0) AS sql_date)
      -- ELSE 1/1/2014
      ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
    END AS pto2014start,
    cast(createTimestamp(2015, month(pto_anniversary), dayofmonth(pto_anniversary), 0,0,0,0) AS sql_date) AS pto2015start
  FROM ptoEmployees a
  LEFT JOIN dds.edwEmployeeDim aa on a.employeenumber = aa.employeenumber
    AND aa.currentrow = true
    AND aa.active = 'active'
  LEFT JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
  LEFT JOIN ( -- consumed betw 1/1/14 AND rollout
    SELECT a.employeenumber, coalesce(SUM(c.vacationhours + c.ptohours), 0) AS ptoConsumed
    FROM ptoEmployees a
    LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
      AND b.currentrow = true
      AND b.active = 'active'
    LEFT JOIN dds.edwClockHoursFact c on b.employeekey = c.employeeKey  
      AND c.vacationhours + c.ptohours > 0
      AND c.datekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN '01/01/2014' AND @rollout)
    GROUP BY a.employeenumber) d on a.employeenumber = d.employeenumber
  WHERE @rollout BETWEEN b.from_date AND b.thru_date) e
ORDER BY pto_anniversary DESC;
*/
/*
EXECUTE PROCEDURE sp_DropReferentialIntegrity('ptoEmployees-pto_at_rollout');
DROP TABLE pto_at_rollout;
*/
CREATE TABLE pto_at_rollout (
  employeeNumber cichar(7) constraint NOT NULL,
  ptoFrom date constraint NOT NULL,
  ptoThru date constraint NOT NULL,
  ptoCategory integer constraint NOT NULL,
  multiplier numeric(6,3) constraint NOT NULL,
  pto2014Start date constraint NOT NULL,
  pto2015Start date constraint NOT NULL,
  ptoAtRollout numeric(8,2) constraint NOT NULL,
  constraint pk primary key (employeeNumber)) IN database;
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoEmployees-pto_at_rollout', 'ptoEmployees', 'pto_at_rollout', 'PK', 1, 1, NULL, '', '');
 
/*
10/15/14
-- *a* uh oh
-- every emp should have a row WHERE fromdate < today
-- these are ALL 2013 ptoAnniversary AND change to ptocat 1 BETWEEN now AND 12/31/14
-- *a1*
now 11020, 
*/  
DECLARE @rollout date;
@rollout = curdate();   
DELETE FROM pto_at_rollout;
INSERT INTO pto_at_rollout 
SELECT c.employeenumber, pto2014start AS ptofrom,
--< *a1*
--  pto2015start - 1  
  CASE 
     WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) > curdate()
       THEN cast(createTimestamp(2014, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) -1
     ELSE pto2015start - 1 
--/> *a1*     
  END AS ptothru,
  ptocategory,
  timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 AS multiplier,
  pto2014start, pto2015start,
  CASE 
    WHEN ptocategory = 0 THEN 0
    ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
      SELECT ptohours
      FROM pto_categories
      WHERE ptocategory = c.ptocategory), 0)
  END AS ptoearnedatrollout
FROM (                                                                        
  SELECT a.*, b.ptocategory,
--< *a*  
--    CASE year(ptoanniversary)
--      WHEN 2013 THEN cast(createTimestamp(2014, month(ptoanniversary), 
--        dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date)
--      ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
--    END AS pto2014start, 
    CASE 
      WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) < curdate()
        THEN cast(createTimestamp(2014, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date)
--      WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) < curdate()
      ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
    END AS pto2014start,  
--/> *a*      
    cast(createTimestamp(2015, month(ptoanniversary), 
      dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) AS pto2015start 
  FROM ptoEmployees a
  INNER JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
  WHERE @rollout BETWEEN b.fromdate AND b.thrudate) c;
