ALTER TABLE pto_employee_pto_category_dates
ADD COLUMN ptoHours double

-- this looks LIKE it will tke the place at pto_at_rollout
-- answers the question, 
-- for any employee, 
-- on any given day, 
-- what IS their pto earned
SELECT e.*,
    CASE 
      WHEN ptocategory = 0 THEN 0
      ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
        SELECT ptohours
        FROM pto_categories
        WHERE ptocategory = e.ptocategory), 0)
    END AS ptoearnedatrollout
INTO #wtf    
FROM (  
  select a.ptocategory, a.employeenumber, a.fromdate AS catFromDate, 
    a.thruDate AS catThruDate, b.ptoAnniversary, b.username, b.anniversaryType,
      CASE year(ptoanniversary)
        WHEN 2013 THEN cast(createTimestamp(2014, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date)
        ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
      END AS pto2014start, 
      cast(createTimestamp(2015, month(ptoanniversary), 
        dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) AS pto2015start   
  -- SELECT *   
  FROM pto_employee_pto_category_dates a
  INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
    AND curdate() BETWEEN a.fromdate AND a.thrudate - 1) e
ORDER BY  ptoAnniversary    
/* 
  still NOT 100% on does this WORK for any date IN the future
  what's up with the reliance on WHEN ptoanniv year = 2013
  that has to DO with establishing the FROM thru range IN which the current
  date EXISTS, once this IS rolled out
  this needs to be more generic, needs to WORK for any date
  
  one's ptoearned will always be a function of current ptocategory
  
so, on 2/12/15, what IS the period, what IS the ptoearned  
 */
 
 
UPDATE pto_employee_pto_category_dates
SET ptoHours = x.ptoearnedatrollout
FROM (
  SELECT e.*,
    CASE 
      WHEN ptocategory = 0 THEN 0
      ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
        SELECT ptohours
        FROM pto_categories
        WHERE ptocategory = e.ptocategory), 0)
    END AS ptoearnedatrollout
  FROM (  
    select a.ptocategory, a.employeenumber, a.fromdate AS catFromDate, 
      a.thruDate AS catThruDate, b.ptoAnniversary, b.username, b.anniversaryType,
        CASE year(ptoanniversary)
          WHEN 2013 THEN cast(createTimestamp(2014, month(ptoanniversary), 
            dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date)
          ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
        END AS pto2014start, 
        cast(createTimestamp(2015, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) AS pto2015start   
    -- SELECT *   
    FROM pto_employee_pto_category_dates a
    INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
      AND curdate() BETWEEN a.fromdate AND a.thrudate - 1) e) x
WHERE pto_employee_pto_category_dates.employeenumber = x.employeenumber
  AND pto_employee_pto_category_dates.ptocategory = x.ptocategory    
  
-- bingo,   
SELECT *
FROM pto_employee_pto_category_dates a
LEFT JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
LEFT JOIN ptoemployees c on a.employeenumber = c.employeenumber
WHERE a.ptoHours IS NOT NULL  
AND a.ptoHours <> b.ptoatrollout

SELECT *
FROM pto_at_rollout a
LEFT JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
  AND b.ptoHours IS NOT NULL 
WHERE a.ptoatrollout <> b.ptohours  

now, need to populate the rest of the pto_employee_pto_category_dates.hours

UPDATE pto_employee_pto_category_dates
  SET ptoHours = x.ptoHours
FROM (  
  SELECT a.employeenumber, b.*
  FROM pto_employee_pto_category_dates a
  LEFT JOIN pto_categories b on a.ptoCategory = b.ptoCategory) x
WHERE pto_employee_pto_category_dates.ptoHours IS NULL
  AND pto_employee_pto_category_dates.employeenumber = x.employeenumber
  AND pto_employee_pto_category_dates.ptoCategory = x.ptoCategory  
  
SELECT *
FROM pto_employee_pto_category_dates a
LEFT JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
  AND a.ptoCategory = b.ptoCategory
LEFT JOIN ptoemployees c on a.employeenumber = c.employeenumber
WHERE a.employeenumber = '164015'

-- fuck me, this implies that for each year IN his category 3 period, he has
-- 291 hours of pto, which IS NOT true
-- only during the period of 1/1/2014 -> 12/2/15
select *
FROM pto_employee_pto_category_dates
WHERE employeenumber = '164015'

SELECT *
FROM #wtf
WHERE employeenumber = '164015'

so what the fuck am i going to DO
1. ADD a ptoCategory IN ptoCategories that represents @rollout
-- heffernan 164015
2. multiple rows IN pto_employee_pto_category_dates per category 
for each year IN the period from/thru this emp IS entitled to this many ptohours



ptoCat      emp#    FROM      thru     hours
0            123                             

select *
FROM pto_at_rollout
WHERE employeenumber = '164015'


what about a situation IN which the pto_at_rollout period crosses over multiple
pto_categories
seems to destroy the notion of ptoCategory AS THE determining factor

!!!!!!!!! goddammit, on any given date i should be able to query the structure
!!!!!!!!! AND get an individuals pto allocation

seem to be at the place WHERE tenure AND pto allocated can be orthogonal

select a.* 
FROM pto_at_rollout a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.payrollclass = 'hourly'
WHERE EXISTS (
  SELECT 1
  FROM pto_employee_pto_category_dates
  WHERE employeenumber = a.employeenumber
    AND ptoCategory <> a.ptoCategory
    AND a.pto2015start BETWEEN fromDate AND thruDate)

select a.*, b.*, c.ptoAnniversary
FROM pto_at_rollout a
LEFT JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
LEFT JOIN ptoEmployees c on a.employeenumber = c.employeenumber
WHERE a.employeenumber = '150120'  
ORDER BY b.ptoCategory

IS there some notion of current pto period, sometimes pto_at_rollout from/thru,
sometimes ptoCategory

ken rygg 1120120 IS a good test CASE

select a.*, b.*, c.ptoAnniversary
FROM pto_at_rollout a
LEFT JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
LEFT JOIN ptoEmployees c on a.employeenumber = c.employeenumber
WHERE a.employeenumber = '1120120'  
ORDER BY b.ptoCategory

emp         anniv        FROM         thru       hours
1120120     11/21/12     11/21/12     11/20/13   0
                         11/21/13     11/20/15   136
                         11/21/15     11/20/22   112
                         11/21/22     11/20/32   152
                         11/21/32                192
                            
                            

default ptoAlloc IS based on pto_categories, but that IS NOT the final word

so maybe a different TABLE
pto_employee_pto_allocation
without any funny stuff, this IS populated FROM  pto_categories based on ptoEmp.Anniv
with funny stuff ... something LIKE this
emp         anniv        FROM          thru       hours
1120120     11/21/12     pto2014Start* 11/20/15   136
                         11/21/15      11/20/22   112
                         11/21/22      11/20/32   152
                         11/21/32      12/31/99   192
* for rollout  populating only, otherwise, ptoAnniversary (maybe - thinking jeff bear
  initial FROM date will be, 9/13/2010 thru whenever his ptocategor changes, based
  on that anniversary date), whatever that may be
for the CASE of a jeff bear (USING orig hire date) ...                        

AS simple AS:                         
employeeNumber, fromDate, thruDate, hours  
keep it simple, no need for historical pto info
 
the challenge IS to populate it, both initially AND ongoing
anniv IS important but NOT necessarily the first date, 
hmmm, pto_employee_pto_category_dates may be useful IN the populating
of this, IN the sense of the above overlap TABLE

DROP TABLE pto_employee_pto_allocation;
CREATE TABLE pto_employee_pto_allocation (
  employeeNumber cichar(7) constraint NOT NULL, 
  fromDate date constraint NOT NULL,
  thruDate date constraint NOT NULL,
  hours double  constraint NOT NULL,
  constraint pk primary key (employeeNumber,fromDate)) IN database;
  
EXECUTE PROCEDURE sp_CreateIndex90( 'pto_employee_pto_allocation', 'pto_employee_pto_allocation.adi', 'FK', 'employeeNumber', '', 2, 1024, '' );
-- UPDATE: cascase, DELETE: cascasde
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoEmployees-pto_emp_pto_alloc', 'ptoEmployees', 'pto_employee_pto_allocation', 'FK', 1, 1, NULL, '', '');
  
/*
no overlap of from/thru BETWEEN rows for an employee
*/
IF i can get this to WORK, pto status for an employee on any given date becomes
a very simple query, the from/thru FROM this TABLE becomes the foundation
for extracting ptoUsed AND requests AS well

the challenge IS IN getting the fucker populated

the initial row will be straight pto_at_rollout
this could be the basis additional rows 
select *
FROM ptoemployees a
LEFT JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
LEFT JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
  AND c.fromdate > pto2014start
LEFT JOIN pto_categories d on c.ptocategory = d.ptocategory  
WHERE a.employeenumber = '1120120'  
ORDER BY c.ptoCategory
                      
OR the second row FROM a query LIKE above useing pto2015start    

OR

SELECT a.employeenumber, b.ptoAnniversary, a.pto2014Start, a.pto2015Start - 1, 
  a.ptoAtRollout
FROM pto_at_rollout a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber    
WHERE a.employeenumber = '1120120'         
UNION
SELECT *
FROM (
  SELECT top 1 a.employeenumber, a.ptoAnniversary, b.pto2015start, c.thrudate, d.ptoHours
  FROM ptoemployees a
  LEFT JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
  LEFT JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    AND c.fromdate > pto2014start
  LEFT JOIN pto_categories d on c.ptocategory = d.ptocategory  
  WHERE a.employeenumber = '1120120'  
  ORDER BY c.ptoCategory) f  
  
  
UNION
SELECT *
FROM (
  SELECT top 1 a.employeenumber, a.ptoAnniversary, fromdate, c.thrudate, d.ptoHours
  FROM ptoemployees a
  LEFT JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
  LEFT JOIN pto_employee_pto_category_dates c on a.employeenumber = c.employeenumber
    AND c.fromdate > pto2015start
  LEFT JOIN pto_categories d on c.ptocategory = d.ptocategory  
  WHERE a.employeenumber = '1120120'  
  ORDER BY c.ptoCategory) g  

  