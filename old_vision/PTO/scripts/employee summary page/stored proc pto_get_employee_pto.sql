alter PROCEDURE pto_get_employee_pto
   ( 
      employeeNumber CICHAR ( 9 ),
      employeeNumber CICHAR ( 9 ) OUTPUT,
      name CICHAR ( 52 ) OUTPUT,
      store CICHAR ( 12 ) OUTPUT,
      originalHireDate DATE OUTPUT,
      latestHireDate DATE OUTPUT,
      department CICHAR ( 30 ) OUTPUT,
      position CICHAR ( 30 ) OUTPUT,
      ptoAdministrator CICHAR ( 52 ) OUTPUT,
      ptoAnniversary DATE OUTPUT,
      ptoHoursEarned DOUBLE ( 15 ) OUTPUT,
      ptoHoursUsed DOUBLE ( 15 ) OUTPUT,
      ptoHoursRemaining DOUBLE ( 15 ) OUTPUT,
      title CICHAR ( 63 ) OUTPUT,
      ptoFromDate DATE OUTPUT,
      ptoThruDate DATE OUTPUT,
      ptoPolicyHtml cichar(60) output,
      ptoHoursApproved integer output,
      ptoHoursRequested integer output,
      totalDaysInPeriod integer output,
      remainingDaysInPeriod integer output
   ) 
BEGIN 
/*
doug bohm '116185'
jay olson '1106399'
crystal moulds  
loren shereck '1126040'
rudy robles '1117600'
travis b '121362'

10/28 ADD 
  requests AND approved

11/8/14 
*a*
  modified return correct period thru date WHEN pto_alloc thrudate = 12/31/9999
  shit, from/thru date also used IN requested, approved, used, adjustments
    need to get a fix on from/thru immediately AND use them throughout
    define the period up top,  use it everywhere

ptoUsed: some managers enter pto time IN advance, time clock data FROM the
  future should NOT be included IN used pto to date
    
  fixed total days IN period
*b*
  include pto_adjustments INTO ptoHoursEarned
*c*
  limit requested & approved to current period  
  
EXECUTE PROCEDURE pto_get_employee_pto ('17410');

*/           
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
@employeenumber = (SELECT employeenumber from __input);
-- *a*
IF (
  SELECT year(thruDate)
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber
    AND curdate() BETWEEN fromDate AND thruDate) = 9999 THEN 
    @fromDate = (
      SELECT cast(createtimestamp(year(curdate()), 
        (SELECT month(ptoAnniversary) FROM ptoEmployees WHERE employeenumber = @employeenumber),
        (SELECT dayOfMonth(ptoAnniversary) FROM ptoEmployees WHERE employeenumber = @employeenumber),
        0,0,0,0) AS sql_date)
      FROM system.iota);
ELSE @fromDate = (
  SELECT fromDate
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeenumber
    AND curdate() BETWEEN fromDate AND thruDate);  
ENDIF;    
IF (
  SELECT year(thruDate)
  FROM pto_employee_pto_allocation
  WHERE employeeNumber = @employeeNumber
    AND curdate() BETWEEN fromDate AND thruDate) = 9999 THEN 
    @thruDate = cast(timestampAdd(sql_tsi_day, -1, timestampadd(sql_tsi_year, 1, @fromDate)) AS sql_date);
ELSE @thruDate = (
  SELECT thruDate
  FROM pto_employee_pto_allocation
  WHERE employeenumber = @employeenumber
    AND curdate() BETWEEN fromDate AND thruDate);   
ENDIF;    
    
INSERT INTO __output
select h.employeenumber, h.name, h.store, h.originalHireDate, h.latestHireDate,
  h.department, h.position, 
  h.ptoAdmin, h.ptoAnniversary, 
-- *b*  
  h.hours + coalesce(n.ptoAdjustment,0) AS ptoHoursEarned,
  coalesce(i.ptoUsed, 0) AS ptoUsed, 
  h.hours  + coalesce(n.ptoAdjustment,0) - coalesce(i.ptoUsed, 0) - 
    coalesce(k.ptoHoursApproved,0) - coalesce(m.ptoHoursRequested,0) AS ptoHoursRemaining,
  TRIM(h.department) + ':' + h.position,
-- *a*
  @fromDate AS ptoFromDate, @thruDate AS ptoThruDate,
  j.ptoPolicyHtml,
  coalesce(k.ptoHoursApproved,0) AS ptoHoursApproved, 
  coalesce(m.ptoHoursRequested,0) AS ptoHoursRequested,
  timestampdiff(sql_tsi_day, @fromDate, @thruDate) + 1 AS totalDaysInPeriod,
  timestampdiff(sql_tsi_day, curdate(), @thrudate) AS remainingDaysInPeriod 
FROM (
  SELECT a.employeenumber, a.ptoAnniversary, 
    TRIM(b.firstname) + ' ' + b.lastname as name, b.storecode,
    b.fullPartTime, 
    c.department, c.position,
    trim(f.firstname) + ' ' + TRIM(f.lastname) AS ptoAdmin,
    g.hours, 
-- *a*     
    @fromDate AS fromDate,
    @thruDate AS thruDate,
    CASE b.storecode
      WHEN 'RY1' THEN 'RY1 - GM'
      WHEN 'RY2' THEN 'RY2 - Honda'
    END AS store,
    gg.ymhdto AS originalHireDate, gg.ymhdte AS latestHireDate
  FROM ptoEmployees a
  INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  INNER JOIN ptoPositionFulfillment c on a.employeenumber = c.employeenumber  
  INNER JOIN ptoAuthorization d on c.department = d.authForDepartment
    AND c.position = d.authForPosition
  INNER JOIN ptoPositionFulfillment e on d.authByDepartment = e.department 
    AND d.authByPosition = e.position 
  INNER JOIN dds.edwEmployeeDim f on e.employeenumber = f.employeenumber 
    AND f.currentrow = true 
  INNER JOIN pto_employee_pto_allocation g on a.employeenumber = g.employeenumber
    -- return hours only
    AND curdate() BETWEEN fromDate AND thruDate
  LEFT JOIN dds.stgArkonaPYMAST gg on a.employeenumber = gg.ymempn    
  WHERE a.employeenumber = @employeeNumber) h
LEFT JOIN ( --ptoUsed: LEFT JOIN, may NOT be any used
  SELECT a.employeenumber, SUM(a.hours) AS ptoUsed
  FROM pto_used a  
  WHERE a.employeenumber = @employeeNumber
-- *a*
-- limit to curdate(), some managers enter pto time IN advance
     AND a.theDate BETWEEN @fromDate AND curdate()
  GROUP BY a.employeenumber) i on h.employeenumber = i.employeenumber
LEFT JOIN ptoDepartmentPositions j on h.department = j.department
  AND h.position = j.position
-- *c*  
LEFT JOIN (
  SELECT employeeNumber, SUM(hours) AS ptoHoursApproved
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Approved'
-- *a*    
    AND thedate BETWEEN curdate() AND @thruDate        
  GROUP BY employeeNumber) k on h.employeenumber = k.employeeNumber  
-- *c*  
LEFT JOIN ( 
  SELECT employeeNumber, SUM(hours) AS ptoHoursRequested
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Pending'
-- *a* 
    AND thedate BETWEEN curdate() AND @thruDate    
  GROUP BY employeeNumber) m on h.employeeNumber = m.employeeNumber  
-- *b*
LEFT JOIN (
  SELECT employeenumber, fromDate, SUM(hours) AS ptoAdjustment
  FROM pto_adjustments
  GROUP BY employeeNumber, fromDate) n on h.employeeNumber = n.employeenumber
    AND h.fromDate = n.fromDate;   
END;

