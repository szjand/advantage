


-- edwEmployeeDim NOT IN ptoEmployees, but IN pto_compli_users
SELECT a.employeenumber, a.firstname, a.lastname, a.pydept, a.distribution, 
  a.payrollclass, a.hiredate, c.*
FROM dds.edwEmployeeDim a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN pto_compli_users c on a.employeenumber = c.userid
  AND c.status = '1'
WHERE a.currentrow = true
  AND a.active = 'active'
  AND a.employeenumber NOT IN (
    SELECT employeenumber
    FROM pto_exclude)
  AND b.employeenumber IS NULL 
ORDER BY a.pydept, a.lastname  

-- pto_compli_census_092314, but NOT IN ptoEmployees
-- this IS f/u because pto_compli includes inactive users
-- so to include the compli export AS part of synchronization, need to include
-- the status AS well
SELECT *
FROM pto_compli_users a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
WHERE a.status = '1'
  AND b.employeeNumber IS NULL 

-- compli date of hire does NOT agree with ptoEmployees.ptoAnniversary  
SELECT a.userid, a.firstname, a.lastname, a.email, a.dateofhire, b.ptoanniversary
FROM pto_compli_census_092414 a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
WHERE a.status = 1
  AND b.employeeNumber IS NOT NULL   
  AND a.dateofhire <> ptoAnniversary
  
/*
the nightmare IS what ALL has to be IN agreement
so, what IS the one source of truth for ptoAnniversary
candidates: 
  ark last hire date
  ark orig hiredate
  other 
AND IF it turns out to be other (OR whatever it IS), WHERE IS it stored, 
  AND how IS it marked AS verified AND correct?

-- compli date of hire does NOT agree with ptoEmployees.ptoAnniversary  
-- ADD pymast
*/
SELECT a.userid, a.firstname, a.lastname, a.email, a.dateofhire AS CompliHireDate, 
  b.ptoanniversary AS "ptoEmp.ptoAnniv", 
  c.ymhdte AS ArkLastHireDate,
  c.ymhdto AS ArkOrigHireDate
FROM pto_compli_census_092414 a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
LEFT JOIN dds.stgArkonaPYMAST c on a.userid = c.ymempn
WHERE a.status = 1
  AND b.employeeNumber IS NOT NULL   
  AND (a.dateofhire <> ymhdte OR a.dateofhire <> ymhdto)

  
!!!!!! per ben foster, exclude part time, commission, salary !!!!!!!!!!!  
-- based on that, these folks can be deleted FROM ptoEmpoloyees
-- but, IF i DELETE them FROM ptoEmployees, due to RI, they will be also 
-- be deleted FROM the ptoAuthorization structure !!! Yikes, that IS NOT
-- what i want
/*
options
  1. include fullPartTime, payrollClass IN ptoEmployees
        which implies yet more synchro requirements BETWEEN ptoEmployees 
        AND edwEmployeeDim AND arkona
no, those are NOT key values, they DO NOT belong anywhere except IN edwEmployeeDim 
may NOT need to change anything outside of specific queries for different
types of information, AND that can be differentiated BY a JOIN to edwEmployeeDim 
  
therefor, the only time a row FROM ptoEmployees (AND ALL referencing tables) IS
deleted IS ONLY WHEN employment has been terminated.
    
*/

-- part time OR non-hourly employees
SELECT a.*, b.name, b.fullparttime, b.payrollclass
FROM ptoEmployees a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
WHERE fullparttime <> 'full'
  OR payrollclass <> 'hourly'

-- ptoEmployees: mandatory relationships --------------------------------------
-- ptoPositionFulfillment
-- existence
SELECT *
FROM ptoEmployees a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
WHERE b.employeenumber IS NULL 
-- 1:1
SELECT a.employeenumber
FROM ptoEmployees a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
GROUP BY a.employeenumber
HAVING COUNT(*) > 1

-- pto_at_rollout
-- existence
SELECT *
FROM ptoEmployees a
LEFT JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
WHERE b.employeenumber IS NULL 
-- 1:1
SELECT a.employeenumber
FROM ptoEmployees a
LEFT JOIN pto_at_rollout b on a.employeenumber = b.employeenumber
GROUP BY a.employeenumber
HAVING COUNT(*) > 1

-- pto_employee_pto_category_dates
-- existence
SELECT *
FROM ptoEmployees a
LEFT JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
WHERE b.employeenumber IS NULL 
-- 1:5
SELECT a.employeenumber
FROM ptoEmployees a
LEFT JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
GROUP BY a.employeenumber
HAVING COUNT(*) <> 5
-- ptoEmployees: mandatory relationships --------------------------------------

ptoEmployees - dimEmp - compli_exportusers

dimEmp (arkona) IS base source
    
    
SELECT a.name, a.hiredate, a.employeenumber AS dimEmp, b.employeenumber AS pto, c.userid
FROM dds.edwEmployeeDim a
full OUTER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
full OUTER JOIN compli_exportusers c on a.employeenumber = c.userid
  AND c.status = 1
  AND c.userid NOT IN (
    SELECT employeenumber
    FROM pto_exclude)
WHERE a.currentrow = true
  AND a.active = 'active'
  AND a.employeenumber NOT IN (
    SELECT employeenumber
    FROM pto_exclude)    
ORDER BY a.employeenumber      
      

-- sync ptoEmployees to dimEmp ------------------------------------------------
-- before starting sync, verify that ALL mandatory relationships to tables referencing
-- ptoEmployees are intact
-- 10/24 the tables have changed
SELECT a.employeenumber
FROM ptoEmployees a
LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
WHERE b.employeenumber IS NULL 
  AND a.employeenumber NOT IN (
    SELECT employeenumber
    FROM pto_exclude)
UNION ALL
SELECT a.employeenumber
FROM ptoEmployees a
LEFT JOIN pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
WHERE b.employeenumber IS NULL 
  AND a.employeenumber NOT IN (
    SELECT employeenumber
    FROM pto_exclude)
--UNION ALL
--SELECT a.employeenumber
--FROM ptoEmployees a
--LEFT JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
--WHERE b.employeenumber IS NULL 
--  AND a.employeenumber NOT IN (
--    SELECT employeenumber
--    FROM pto_exclude);
    
-- termed employee IN ptoEmployees
SELECT a.storecode, a.employeenumber, a.name, a.hiredate, a.termdate, c.status
FROM dds.edwEmployeeDim a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN compli_exportusers c on a.employeenumber = c.userid
WHERE a.currentrow = true
  AND a.termdate <> '12/31/9999' 
  AND b.employeenumber IS NOT NULL;
  
DELETE FROM ptoEmployees
WHERE employeenumber IN (
  SELECT a.employeenumber
  FROM dds.edwEmployeeDim a
  LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
  LEFT JOIN compli_exportusers c on a.employeenumber = c.userid
  WHERE a.currentrow = true
    AND a.termdate <> '12/31/9999' 
    AND b.employeenumber IS NOT NULL);
  
  
  
-- missing FROM ptoEmployees
SELECT a.employeenumber, a.firstname, a.lastname, a.pydept, a.distribution, a.payrollclass, a.hiredate
FROM dds.edwEmployeeDim a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
WHERE a.currentrow = true
  AND a.active = 'active'
  AND a.employeenumber NOT IN (
    SELECT employeenumber
    FROM pto_exclude)
  AND b.employeenumber IS NULL 
ORDER BY pydept, lastname; 
 
-- first a subset WHERE ymhdte = ymhdto, unambiguous anniv date,
-- exclude those that are NOT yet IN compli
DROP TABLE #missing1;
SELECT storecode, employeenumber, ymhdto, email, 'Original', title
-- INTO #missing1
FROM (
  SELECT a.storecode, a.employeenumber, a.name, a.hiredate, c.ymhdte, c.ymhdto, 
    d.dateofhire, d.title, d.email
  FROM dds.edwEmployeeDim a
  LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
  LEFT JOIN dds.stgArkonaPYMAST c on a.employeenumber = c.ymempn
  LEFT JOIN compli_exportusers d on a.employeenumber = d.userid
  WHERE a.currentrow = true
    AND a.active = 'active'
    AND a.employeenumber NOT IN (
      SELECT employeenumber
      FROM pto_exclude)
    AND b.employeenumber IS NULL) x
WHERE ymhdte = ymhdto
  AND email IS NOT NULL;

-- ambiguous anniv date, but IS IN compli  
SELECT *
FROM (
  SELECT a.storecode, a.employeenumber, a.name, a.hiredate, c.ymhdte, c.ymhdto, 
    d.dateofhire, d.title, d.email
  FROM dds.edwEmployeeDim a
  LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
  LEFT JOIN dds.stgArkonaPYMAST c on a.employeenumber = c.ymempn
  LEFT JOIN compli_exportusers d on a.employeenumber = d.userid
  WHERE a.currentrow = true
    AND a.active = 'active'
    AND a.employeenumber NOT IN (
      SELECT employeenumber
      FROM pto_exclude)
    AND b.employeenumber IS NULL) x
WHERE ymhdte <> ymhdto
  AND email IS NOT NULL;  

-- 4 steps: insert into ptoEmployees, ptoPositionFulfillment, 
--     pto_employee_pto_category_dates   
--     regenerate pto_at_rollout 
this will be a manual process, through vision site pto admin page
WHERE these people will be exposed

for these new adds, the notion of pto_at_rollout seems redundant, which takes me 
back to the struggle i was HAVING envisioning what i was struggling with 
before, eg, generating the whole pto picture for an individual without the
preconfigured notion of pto_at_rollout

for ALL these new employees (hired after 1/1/14), the notion of pto_at_rollout
IS irrelevant, BETWEEN info in ptoEmployees AND pto_employee_pto_category_dates
ALL necessary info IS easy to generate for any given date

so what i have been wondering ...
any way, get back to adding new ptoEmployee AND referencing rows

SELECT * FROM #missing1

DECLARE @employeeNumber string;
DECLARE @department string;
DECLARE @position string;
@employeeNumber = '112090';
@department = 'Car Wash';
@position = 'Team Member';
BEGIN TRANSACTION;
  TRY
    INSERT INTO ptoEmployees (employeeNumber, ptoAnniversary, username, anniversaryType)
    SELECT employeenumber, ymhdto, email, expr
    FROM #missing1
    WHERE employeenumber = @employeeNumber;
    INSERT INTO ptoPositionFulfillment (department, position,employeenumber)
    values (@department, @position, @employeeNumber);
    INSERT INTO pto_employee_pto_category_dates (ptoCategory, employeeNumber, 
      fromDate, thruDate)
    SELECT b.ptoCategory, a.employeeNumber, 
      cast(timestampadd(sql_tsi_year, b.fromYearsTenure, a.ptoAnniversary) AS sql_date) AS fromDate,
      cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, b.thruYearsTenure + 1, a.ptoAnniversary)) AS sql_date) AS thruDate
    FROM ptoEmployees a, pto_categories b
    WHERE a.employeenumber = @employeeNumber;    
  COMMIT WORK;
  CATCH ALL
    ROLLBACK;
    RAISE;
END TRY;  

-- DO the rest manually, one at a time
DECLARE @employeeNumber string;
DECLARE @department string;
DECLARE @position string;
@employeeNumber = '161092';
@department = 'Detail';
@position = 'Detail Technician';
BEGIN TRANSACTION;
  TRY
    INSERT INTO ptoEmployees (employeeNumber, ptoAnniversary, username, anniversaryType)
    values(@employeeNumber, '9/3/2013', 'bhalver@rydellcars.com', 'Original');
    INSERT INTO ptoPositionFulfillment (department, position,employeenumber)
    values (@department, @position, @employeeNumber);
    INSERT INTO pto_employee_pto_category_dates (ptoCategory, employeeNumber, 
      fromDate, thruDate)
    SELECT b.ptoCategory, a.employeeNumber, 
      cast(timestampadd(sql_tsi_year, b.fromYearsTenure, a.ptoAnniversary) AS sql_date) AS fromDate,
      cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, b.thruYearsTenure + 1, a.ptoAnniversary)) AS sql_date) AS thruDate
    FROM ptoEmployees a, pto_categories b
    WHERE a.employeenumber = @employeeNumber;    
  COMMIT WORK;
  CATCH ALL
    ROLLBACK;
    RAISE;
END TRY; 

  
-- sync ptoEmployees to dimEmp ------------------------------------------------  

-- IN compli, NOT IN ptoEmployees
SELECT * 
FROM pto_compli_users a
LEFT JOIN ptoemployees b on a.userid = b.employeenumber
WHERE b.employeenumber IS NULL 
  AND a.status = '1'
  AND a.userid NOT IN (
    SELECT employeenumber
    FROM pto_exclude)
    
-- IN arkona, NOT IN compli   
SELECT a.employeenumber, a.firstname, a.lastname, a.pydept, a.distribution, 
  a.payrollclass, a.fullparttime, c.ymhdte, c.ymhdto
FROM dds.edwEmployeeDim a
LEFT JOIN pto_compli_users b on a.employeenumber = b.userid
LEFT JOIN dds.stgArkonaPYMAST c on a.employeenumber = c.ymempn
WHERE a.currentrow = true
  AND a.active = 'active'
  AND a.employeenumber NOT IN (
    SELECT employeenumber
    FROM pto_exclude)
  AND b.userid IS NULL 
ORDER BY a.pydept, a.lastname;  


-- 11/8/14 TRY to get the uber comparator
-- edwEmployeeDim
-- DROP TABLE #arkona;
SELECT a.employeenumber, a.firstname, a.lastname, a.storecode, a.pyDept, 
  a.Distribution, a.payrollclass, left(c.yrtext, 30) AS Position,
  left(TRIM(e.firstname) + ' ' + e.lastname,35) AS supervisor
INTO #arkona
--SELECT *
FROM dds.edwEmployeeDim a
LEFT JOIN dds.stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
  AND b.yrjobd <> ''
  AND a.storecode = b.yrco#
LEFT JOIN (
  SELECT yrco#, yrjobd, MAX(yrtext) AS yrtext
  FROM dds.stgArkonaPYPRJOBD
  GROUP BY yrco#, yrjobd) c on b.yrjobd = c.yrjobd
    AND b.yrco# = c.yrco#  
LEFT JOIN dds.edwEmployeeDim e on b.yrmgrn = e.employeenumber
  AND e.currentrow = true  
WHERE a.currentrow = true
  AND a.active = 'active'
  AND a.fullparttime = 'full';
-- compli
-- DROP TABLE #compli;
SELECT userid, firstname, lastname, locationid, title, supervisorname AS supervisor
INTO #compli 
FROM pto_compli_users  
WHERE status = '1';
-- pto
SELECT a.employeenumber, b.department, b.position, e.firstname, e.lastname
--INTO #pto
FROM ptoEmployees a
INNER JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
INNER JOIN ptoAuthorization c on b.department = c.authForDepartment 
  AND b.position = c.authForDepartment
INNER JOIN ptoPositionFulfillment d on c.authByDepartment = d.department
  AND c.authByPosition = d.position
INNER JOIN dds.edwEmployeeDim e on d.employeenumber = e.employeenumber
  AND e.currentrow = true    
WHERE a.active = true;


SELECT *
FROM #arkona a
LEFT JOIN #compli b on a.employeenumber = b.userid
WHERE a.supervisor <> b.supervisor collate ads_default_ci

SELECT * FROM pto_compli_users


-- 7/20/15 compare positions compli to vision
SELECT a.storecode, a.employeenumber, a.lastname, a.firstname, 
  coalesce(b.title, '*** NOT YET IN COMPLI ***') AS [Compli Position], 
  coalesce(TRIM(c.department) + ':' + c.position, '*** NOT YET IN VISION ***') AS [Vision Position]
FROM dds.edwEmployeeDim a
LEFT JOIN pto_compli_users b on a.employeenumber = b.userid
  AND b.status = '1'
LEFT JOIN ptoPositionFulFillment c on a.employeenumber = c.employeenumber  
WHERE a.currentrow = true
  AND a.active = 'Active'
  AND a.FullPartTime = 'Full'
  AND NOT EXISTS (
    SELECT 1
    FROM pto_exclude
    WHERE employeenumber = a.employeenumber)
ORDER BY a.storecode, a.lastname, a.firstname  