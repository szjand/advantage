/*
password EXISTS only IN tpEmployees

ok, part of the rub, employeeAppAuthorization does NOT contain employeeNumber
tpEmployees PK = username, but does have a unique index on employeenumber
ptoEmployees PK = employeeNumber


SELECT *
FROM ptoEmployees a
LEFT JOIN tpEmployees b on a.employeenumber = b.employeenumber

-- tpEmployees that are currently employed AND EXIST IN ptoEmployees
-- number of rows IN empAppAuth
-- whether tpEmployees.username = ptoEmployees.username
SELECT a.username, a.firstname, a.lastname, a.password, b.*, c.*,
  (SELECT COUNT(*)
     FROM employeeAppAuthorization u
     INNER JOIN tpEmployees v on u.userName = v.userName
     WHERE v.employeenumber = a.employeenumber),
  iif(a.username = b.username, true, false)
FROM tpEmployees a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN pto_employee_pto_allocation c on b.employeenumber = c.employeenumber
  AND curdate() BETWEEN c.fromdate AND c.thrudate  
WHERE b.employeenumber IS NOT NULL   
  AND NOT EXISTS (
    SELECT 1
    FROM pto_exclude
    WHERE employeenumber = a.employeenumber)
  AND EXISTS (
    SELECT 1
    FROM dds.edwEmployeeDim
    WHERE employeenumber = a.employeenumber
      AND currentrow = true
      AND active = 'active')    
--AND a.employeenumber = '185800'   
ORDER BY thrudate




SELECT *
FROM pto_employee_pto_allocation
WHERE year(thruDate) = 2014

SELECT *
FROM pto_employee_pto_allocation
WHERE thruDate <= curdate()




select *
FROM tpEmployees a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
WHERE b.employeenumber IS NULL 

-- tpEmployees that are currently employed AND EXIST IN ptoEmployees
-- number of rows IN empAppAuth
-- whether tpEmployees.username = ptoEmployees.username
-- for those employees that already have vision access thru the existence
-- IN tpEmployees, only those that are currently employed, hourly AND full time
SELECT a.username, d.firstname, d.lastname, a.password, b.*, c.*,
  (SELECT COUNT(*)
     FROM employeeAppAuthorization u
     INNER JOIN tpEmployees v on u.userName = v.userName
     WHERE v.employeenumber = a.employeenumber),
  iif(a.username = b.username, true, false),
  d.payrollclass, d.fullparttime
FROM tpEmployees a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN pto_employee_pto_allocation c on b.employeenumber = c.employeenumber
  AND curdate() BETWEEN c.fromdate AND c.thrudate  
LEFT JOIN dds.edwEmployeeDim d on a.employeenumber = d.employeenumber
  AND d.currentrow = true
  AND d.active = 'active'  
WHERE b.employeenumber IS NOT NULL   
  AND NOT EXISTS (
    SELECT 1
    FROM pto_exclude
    WHERE employeenumber = a.employeenumber)
  AND d.employeenumber IS NOT NULL -- currently employed    


-- employeeAppAuthorization

SELECT *
FROM applicationmetadata
WHERE appcode = 'pto'
  AND approle = 'ptoemployee'

SELECT *
FROM employeeAppAuthorization
WHERE appname = 'pto'

*/

-- give auth to pto/ptoemployee for ALL currently employed tpEmployees that are full time AND hourly
-- AND already exist IN ptoEmployees
-- ALL they need are employeeAppAuthorization
INSERT INTO employeeAppAuthorization (username, appName, appSeq, appCode,
  appRole, functionality)  
SELECT b.username, a.appName, a.appSeq, a.appCode, a.appRole, a.functionality 
FROM applicationMetaData a
LEFT JOIN (
  SELECT a.username, d.firstname, d.lastname, a.password, b.*, c.*,
    (SELECT COUNT(*)
       FROM employeeAppAuthorization u
       INNER JOIN tpEmployees v on u.userName = v.userName
       WHERE v.employeenumber = a.employeenumber),
    iif(a.username = b.username, true, false),
    d.payrollclass, d.fullparttime
  FROM tpEmployees a
  LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
  LEFT JOIN pto_employee_pto_allocation c on b.employeenumber = c.employeenumber
    AND curdate() BETWEEN c.fromdate AND c.thrudate  
  LEFT JOIN dds.edwEmployeeDim d on a.employeenumber = d.employeenumber
    AND d.currentrow = true
    AND d.active = 'active'  
  WHERE b.employeenumber IS NOT NULL -- only those that are currently IN ptoEmployees  
    AND NOT EXISTS (
      SELECT 1
      FROM pto_exclude
      WHERE employeenumber = a.employeenumber)
    AND d.employeenumber IS NOT NULL) b on 1 = 1
WHERE a.appcode = 'pto'
  AND a.approle = 'ptoemployee'  
  AND b.payrollclass = 'hourly'
  AND b.fullparttime = 'full';

  
-- currently employed full time hourly employees NOT currently IN tpemployes 
-- they need tpEmployees, employeeAppAuthorization

DROP TABLE #passwords;
CREATE TABLE #passwords (
  username cichar(50),
  password cichar(8));
INSERT INTO #passwords(username)

SELECT d.email
FROM dds.edwEmployeeDim a
INNER JOIN pto_compli_users d on a.employeenumber = d.userid
WHERE a.currentrow = true
  AND a.active = 'active'
  AND d.userid IS NOT NULL -- IN compli
  AND a.payrollclass = 'hourly'
  AND a.fullparttime = 'full'
  AND NOT EXISTS (
    SELECT 1
    FROM pto_exclude
    WHERE employeenumber = a.employeeNumber)
  AND NOT EXISTS (
    SELECT 1
    FROM tpEmployees
    WHERE username = d.email collate ads_default_ci)
  AND NOT EXISTS (
    SELECT 1
    FROM tpEmployees
    WHERE employeenumber = a.employeenumber)
-- fix for adding tpEmp & empAppAuth WHERE there IS no ptoEmployees row    
  AND EXISTS (
    SELECT 1 
    FROM ptoEmployees
    WHERE employeenumber = a.employeenumber);   
    
DECLARE @cur CURSOR AS
  SELECT *
  FROM #passwords;
DECLARE @str string, @password string;
DECLARE @i integer, @pos1 integer, @pos2 integer, @j integer;  
@str = 'LoremXesumdoLorspnametconsecteturadXeYacWngeLpnDonecetorcWanuncmattYa'+
  'auctorVWvamusWacuLYanonLacusegetmattYaCrasetLacWnWadoLorPeLLentesquefacWLY'+
  'aYaveLpnduWacvarWussemconsecteturquYaAeneanWnterdumquamatLWberopharetraWnt'+
  'erdumMaecenasspnametuLtrWcWesnuncVWvamusmaLesuadatortorposuereLuctusdapWbu'+
  'sVestWbuLumaWnterdumquamSuspendYaseactemporLectusCrasmattYaanteveLsuscXepn'+
  'semperodWoveLpnLaoreetteLLusveLsoLLWcpnudWnquamerosaLeoALWquamdapWbusnuncL'+
  'WberononconguejustovuLputatevpnaePhaseLLusnuLLaarcumaLesuadaWnterdummoLest'+
  'WeutconsequatettortorSeddWctumnequeegetnYaWLaoreetcongueProWnrhoncusrutrum'+
  'congueMaecenasvBLoremsagpntYavWverrafeLYadWgnYasWmbWbendumLectuQuYaquetWnc'+
  'vBuntauctormagnaconvaLLYavestWbuLumMaurYapuLvWnaretmetusnonhendrerpnNuncbW'+
  'bendumsemdWctumpLaceratposuerenuncveLpnaccumsanrYausacvuLputateXesumtortor'+
  'spnametmassaFusceuLtrWcWeseratnoneLeWfendcommodoNuLLanonerosvBjustotWncvBu'+
  'ntportaanonnuLLaPraesentmattYafeugWattWncvBuntALWquamsedsuscXepnodWoQuYaqu'+
  'evenenatYautarcuegetvenenatYaVestWbuLumuLtrWcWesadXeYacWngvuLputateNuLLase'+
  'dLacusLoremVWvamusLacWnWasceLerYaquerYausacsodaLesmaurYaconsequatnonSedtWn'+
  'cvBuntatmaurYavBdapWbusDuYautteLLusenWmCurabpnurfermentumerosacLWguLahendr'+
  'erpnfeugWatcondWmentumturpYatWncvBuntPhaseLLusvestWbuLumsapWenegetornarevo'+
  'LutpatUtpeLLentesqueturpYanWbhnecvuLputateteLLusLobortYavpnaePraesentcursu'+
  'snuLLauteratpretWumpretWumVestWbuLumfeugWattortorvBporttpnorvWverraodWonYa'+
  'WmoLestWeorcWvpnaeWnterdumdoLorLWberovBsapWenQuYaquevpnaeconguemassaeugrav'+
  'vBasemUtcongueWnsemetmoLLYaNuLLamnonmagnanuncSedvBcommodoLWguLaEtWamquYava'+
  'rWussapWenNuncvBsemenWmSuspendYaseLacWnWanecodWoatsodaLesVWvamussedduWvuLp'+
  'utatevenenatYapurusdapWbusmoLestWeteLLusMaurYaveLveLpnquYaLWguLacongueeuYa'+
  'modegetetnequeSuspendYasenonLacWnWaLoremvBmattYaarcuPraesentseddoLornWbhCu'+
  'rabpnurconsecteturveLmWquYaWacuLYaLoremXesumdoLorspnametconsecteturadXeYac'+
  'WngeLpnInachendrerpnsapWenWndWctumLoremMorbWsedbLandpnteLLusPeLLentesquegr'+
  'avvBatemporpuruscommodoWacuLYaQuYaqueLobortYatempusLoremspnametLuctusSuspe'+
  'ndYasesemtortorornareWnodWospnametconsequatornarequam';  
@j = 1;  
OPEN @cur;
TRY
  WHILE FETCH @cur DO
    @i = (SELECT frac_second(timestampadd(SQL_TSI_FRAC_SECOND, cast(right(trim(cast(rand(@j) AS sql_char)), 3) AS sql_integer),now())) FROM system.iota);    
    @pos1 = (SELECT cast(right(trim(cast(rand(@i) AS sql_char)), 3) AS sql_integer) FROM system.iota) ;
    @pos2 = 2 * (SELECT cast(right(trim(cast(rand(@pos1) AS sql_char)), 3) AS sql_integer) FROM system.iota);  
    @password = (
      SELECT substring(@str, @pos1, 3) + left(CAST(@i AS sql_char), 2) + 
        upper(substring(@str, @pos2, 1)) + substring(@str, @pos1 +@i, 2)
      FROM system.iota);   
    UPDATE #passwords
    SET password = @password
    WHERE username = @cur.username; 
    @j = @j + 1;
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY;     

BEGIN TRANSACTION;
TRY 
-- tpEmployees
INSERT INTO tpEmployees
SELECT d.email, a.firstname, a.lastname, a.employeenumber, e.password, 'Fuck You', a.storecode, TRIM(a.firstname) + ' ' + a.lastname
FROM dds.edwEmployeeDim a
INNER JOIN pto_compli_users d on a.employeenumber = d.userid
INNER  JOIN #passwords e on d.email = e.username collate ads_default_ci
WHERE a.currentrow = true
  AND a.active = 'active'
  AND d.userid IS NOT NULL -- NOT IN compli
  AND a.payrollclass = 'hourly'
  AND a.fullparttime = 'full'
  AND NOT EXISTS (
    SELECT 1
    FROM pto_exclude
    WHERE employeenumber = a.employeeNumber)
  AND NOT EXISTS (
    SELECT 1
    FROM tpEmployees
    WHERE username = d.email collate ads_default_ci)
  AND NOT EXISTS (
    SELECT 1
    FROM tpEmployees
    WHERE employeenumber = a.employeenumber);    

-- AND employeeAppAuthorization
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT b.userName, a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
LEFT JOIN #passwords b on 1 = 1
WHERE a.appcode = 'pto'
  AND a.approle = 'ptoemployee';
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;

-- 10/31 fuck me those with no vision access that have been given tpEmp & empAppAuth WHEN there IS no pto Employee

give Ken Espelund ptomanager
SELECT * FROM tpEmployees WHERE lastname = 'espelund'

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'kespelund@rydellchev.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
SELECT *
FROM employeeAppAuthorization  
WHERE username = 'kespelund@rydellchev.com'
AND appcode = 'pto'

SELECT *
FROM applicationMetaData
WHERE appcode = 'pto'
  AND approle = 'ptomanager' 
  

-- garret
-- this was fucked up, 
SELECT * FROM tpEmployees WHERE username LIKE '%even%';
UPDATE tpEmployees
SET employeenumber = '241085'
WHERE employeenumber = '141085';

SELECT * FROM employeeAppAuthorization WHERE username LIKE '%even%';

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'gevenson@rydellcars.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
UPDATE ptoPositionFulFillment
SET position = 'Assistant Manager'
--SELECT * FROM ptoEmployees a LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
WHERE employeenumber = '241085';
 
  



SELECT *
FROM tpemployees
    
  
     


-- joel
SELECT * FROM employeeAppAuthorization WHERE username LIKE '%dang%';

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT 'jdangerfield@gfhonda.com', a.appName, 800, a.appCode, a.appRole, a.functionality
FROM applicationMetadata a
WHERE a.appcode = 'pto'
  AND a.approle = 'ptomanager';
  
UPDATE ptoPositionFulFillment
SET position = 'Assistant Manager'
--SELECT * FROM ptoEmployees a LEFT JOIN ptoPositionFulfillment b on a.employeenumber = b.employeenumber
WHERE employeenumber = '241085';



-- ALL admins
SELECT c.firstname, c.lastname, b.department, b.position
FROM (
  SELECT authByDepartment, authByPosition
  FROM ptoAuthorization
  WHERE authByLevel <> 1
  GROUP BY authByDepartment, authByPosition) a
LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active'  

 
-- ALL admins AND reportees   

SELECT c.firstname, c.lastname, /**/b.department, b.position, /**/
  /**/d.authForDepartment, d.authForPosition,/**/ f.firstname, f.lastname
FROM (
  SELECT authByDepartment, authByPosition
  FROM ptoAuthorization
  WHERE authByLevel <> 1
  GROUP BY authByDepartment, authByPosition) a
LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active'  
LEFT JOIN ptoAuthorization d on a.authByDepartment = d.authByDepartment
  AND a.authByPosition = d.authByPosition 
LEFT JOIN ptoPositionFulfillment e on d.authForDepartment = e.department
  AND d.authForPosition = e.position
LEFT JOIN dds.edwEmployeeDim f on e.employeeNumber = f.employeenumber
  AND f.currentrow = true
  AND f.active = 'active'  
WHERE f.employeenumber IS NOT NULL    

-- ALL admins AND reportees 
-- DO this list for ben, leave out dept/pos, inc authorizer email, employee username/password

-- ALL admins FROM ptoAuthorization structure
/*
(vision username = tpEmployees.username)
ok, this gives categories of admins :
1. those with no vision access
2. those with vision access AND
   a. vision username = compli username
      i. correct pto access
      ii. incorrect pto access
      iii. no pto access
   b. vision username <> compli username
       this might be furher subdividable BY 
         folks that currently, regularly use vision (ken e,  bev, randy, mark ...) leave tpEmployees.username AS it is
            AND folks who don't (june, jerry, trent ...) change tpEmployees.username to compli
      i. correct pto access
      ii. incorrect pto access
      iii. no pto access                             
*/   
DROP TABLE #base;
SELECT b.employeenumber, c.firstname, c.lastname, b.department, b.position, left(d.username,30) as visionUserName,
  e.email AS compliUserName, 
  iif(d.username = e.email collate ads_default_ci, true,false) AS userNamesAgree,
  f.hasPtoAccess,
  (SELECT COUNT(*)
    FROM employeeAppAuthorization
    WHERE appcode = 'sco'
      AND username = d.username) AS scoAccess  
INTO #base  
--SELECT *
FROM (
  SELECT authByDepartment, authByPosition
  FROM ptoAuthorization
  WHERE authByLevel <> 1
  GROUP BY authByDepartment, authByPosition) a
LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active'  
LEFT JOIN tpEmployees d on c.employeenumber = d.employeenumber  
LEFT JOIN pto_compli_users e on c.employeenumber = e.userid
LEFT JOIN (
  SELECT username, COUNT(*) hasPtoAccess
  from employeeAppAuthorization 
  WHERE appname = 'pto'
  GROUP BY username) f on d.username = f.username

  
SELECT * from employeeAppAuthorization WHERE username LIKE 'kesp%'

  
1. those with no vision access

-- DROP TABLE #base;
SELECT *
INTO #base
FROM (
  SELECT c.storecode, c.employeenumber, c.firstname, c.lastname, b.department, b.position, left(d.username,30) as visionUserName,
    e.email AS compliUserName, 
    iif(d.username = e.email collate ads_default_ci, true,false) AS userNamesAgree,
    f.hasPtoAccess, e.dateofhire AS anniversaryDate
  FROM (
    SELECT authByDepartment, authByPosition
    FROM ptoAuthorization
    WHERE authByLevel <> 1
    GROUP BY authByDepartment, authByPosition) a
  LEFT JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
    AND a.authByPosition = b.position
  LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
    AND c.currentrow = true
    AND c.active = 'active'  
  LEFT JOIN tpEmployees d on c.employeenumber = d.employeenumber  
  LEFT JOIN pto_compli_users e on c.employeenumber = e.userid
  LEFT JOIN (
    SELECT username, COUNT(*) hasPtoAccess
    from employeeAppAuthorization 
    WHERE appname = 'pto'
    GROUP BY username) f on d.username = f.username) x   
WHERE visionUsername IS NULL AND lastname <> 'sorum';  

-- DROP TABLE #passwords;
CREATE TABLE #passwords (
  username cichar(50),
  password cichar(8));
INSERT INTO #passwords(username)
SELECT compliUserName 
FROM #base;

DECLARE @cur CURSOR AS
  SELECT *
  FROM #passwords;
DECLARE @str string, @password string;
DECLARE @i integer, @pos1 integer, @pos2 integer, @j integer;  
@str = 'LoremXesumdoLorspnametconsecteturadXeYacWngeLpnDonecetorcWanuncmattYa'+
  'auctorVWvamusWacuLYanonLacusegetmattYaCrasetLacWnWadoLorPeLLentesquefacWLY'+
  'aYaveLpnduWacvarWussemconsecteturquYaAeneanWnterdumquamatLWberopharetraWnt'+
  'erdumMaecenasspnametuLtrWcWesnuncVWvamusmaLesuadatortorposuereLuctusdapWbu'+
  'sVestWbuLumaWnterdumquamSuspendYaseactemporLectusCrasmattYaanteveLsuscXepn'+
  'semperodWoveLpnLaoreetteLLusveLsoLLWcpnudWnquamerosaLeoALWquamdapWbusnuncL'+
  'WberononconguejustovuLputatevpnaePhaseLLusnuLLaarcumaLesuadaWnterdummoLest'+
  'WeutconsequatettortorSeddWctumnequeegetnYaWLaoreetcongueProWnrhoncusrutrum'+
  'congueMaecenasvBLoremsagpntYavWverrafeLYadWgnYasWmbWbendumLectuQuYaquetWnc'+
  'vBuntauctormagnaconvaLLYavestWbuLumMaurYapuLvWnaretmetusnonhendrerpnNuncbW'+
  'bendumsemdWctumpLaceratposuerenuncveLpnaccumsanrYausacvuLputateXesumtortor'+
  'spnametmassaFusceuLtrWcWeseratnoneLeWfendcommodoNuLLanonerosvBjustotWncvBu'+
  'ntportaanonnuLLaPraesentmattYafeugWattWncvBuntALWquamsedsuscXepnodWoQuYaqu'+
  'evenenatYautarcuegetvenenatYaVestWbuLumuLtrWcWesadXeYacWngvuLputateNuLLase'+
  'dLacusLoremVWvamusLacWnWasceLerYaquerYausacsodaLesmaurYaconsequatnonSedtWn'+
  'cvBuntatmaurYavBdapWbusDuYautteLLusenWmCurabpnurfermentumerosacLWguLahendr'+
  'erpnfeugWatcondWmentumturpYatWncvBuntPhaseLLusvestWbuLumsapWenegetornarevo'+
  'LutpatUtpeLLentesqueturpYanWbhnecvuLputateteLLusLobortYavpnaePraesentcursu'+
  'snuLLauteratpretWumpretWumVestWbuLumfeugWattortorvBporttpnorvWverraodWonYa'+
  'WmoLestWeorcWvpnaeWnterdumdoLorLWberovBsapWenQuYaquevpnaeconguemassaeugrav'+
  'vBasemUtcongueWnsemetmoLLYaNuLLamnonmagnanuncSedvBcommodoLWguLaEtWamquYava'+
  'rWussapWenNuncvBsemenWmSuspendYaseLacWnWanecodWoatsodaLesVWvamussedduWvuLp'+
  'utatevenenatYapurusdapWbusmoLestWeteLLusMaurYaveLveLpnquYaLWguLacongueeuYa'+
  'modegetetnequeSuspendYasenonLacWnWaLoremvBmattYaarcuPraesentseddoLornWbhCu'+
  'rabpnurconsecteturveLmWquYaWacuLYaLoremXesumdoLorspnametconsecteturadXeYac'+
  'WngeLpnInachendrerpnsapWenWndWctumLoremMorbWsedbLandpnteLLusPeLLentesquegr'+
  'avvBatemporpuruscommodoWacuLYaQuYaqueLobortYatempusLoremspnametLuctusSuspe'+
  'ndYasesemtortorornareWnodWospnametconsequatornarequam';  
@j = 1;  
OPEN @cur;
TRY
  WHILE FETCH @cur DO
    @i = (SELECT frac_second(timestampadd(SQL_TSI_FRAC_SECOND, cast(right(trim(cast(rand(@j) AS sql_char)), 3) AS sql_integer),now())) FROM system.iota);    
    @pos1 = (SELECT cast(right(trim(cast(rand(@i) AS sql_char)), 3) AS sql_integer) FROM system.iota) ;
    @pos2 = 2 * (SELECT cast(right(trim(cast(rand(@pos1) AS sql_char)), 3) AS sql_integer) FROM system.iota);  
    @password = (
      SELECT substring(@str, @pos1, 3) + left(CAST(@i AS sql_char), 2) + 
        upper(substring(@str, @pos2, 1)) + substring(@str, @pos1 +@i, 2)
      FROM system.iota);   
    UPDATE #passwords
    SET password = @password
    WHERE username = @cur.username; 
    @j = @j + 1;
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY; 

BEGIN TRANSACTION;
TRY

INSERT INTO tpEmployees(username,firstname,lastname,employeenumber,password,membergroup,storecode, fullname)
SELECT compliUserName, firstname, lastname, employeenumber, b.password, 'Fuck You', storecode, TRIM(firstname) + ' ' + lastname
FROM #base a
LEFT JOIN #passwords b on a.compliUserName = b.username collate ads_Default_ci
WHERE lastname <> 'neumann';

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT compliUserName, appName, 800, appCode, appRole, functionality
FROM applicationMetadata a, #base b
WHERE appcode = 'pto'
  AND approle = 'ptomanager'
  AND lastname <> 'neumann';
  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  


/*
1. those with no vision access
2. those with vision access AND
   a. vision username = compli username
      i. correct pto access
      ii. incorrect pto access
      iii. no pto access
   b. vision username <> compli username
       this might be furher subdividable BY 
         folks that currently, regularly use vision (ken e,  bev, randy, mark ...) leave tpEmployees.username AS it is
            AND folks who don't (june, jerry, trent ...) change tpEmployees.username to compli
      i. correct pto access
      ii. incorrect pto access
      iii. no pto access
                               
*/   
-- 2.a.iii
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT compliUserName, appName, 800, appCode, appRole, functionality
FROM #base a
LEFT JOIN applicationMetaData b
  on b.appcode = 'pto'
  AND b.approle = 'ptomanager'
WHERE NOT EXISTS (
  SELECT 1
  FROM #base
  WHERE firstname = a.firstname
    AND lastname = a.lastname
    AND userNamesAgree = true
    AND hasptoAccess = 3)
  AND lastname NOT IN ('sorum','neumann', 'foster', 'cahalan')
  AND hasptoaccess IS NULL 
  AND usernamesagree = true;

-- 2.a.ii  
SELECT *
FROM #base a
WHERE lastname NOT IN ('sorum','neumann', 'foster', 'cahalan')
  AND hasptoaccess = 2
  AND usernamesagree

BEGIN TRANSACTION;
TRY   
DELETE 
FROM employeeAppAuthorization
WHERE appcode = 'pto'
  AND approle = 'ptoemployee'  
AND username IN (
  SELECT visionUserNAme
  FROM #base a
  WHERE lastname NOT IN ('sorum','neumann', 'foster', 'cahalan')
    AND hasptoaccess = 2
    AND usernamesagree);
    
INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT compliUserName, appName, 800, appCode, appRole, functionality
FROM #base a 
LEFT JOIN applicationMetaData b
  on b.appcode = 'pto'
  AND b.approle = 'ptomanager' 
WHERE lastname NOT IN ('sorum','neumann', 'foster', 'cahalan')
  AND hasptoaccess = 2
  AND usernamesagree;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;       

-- trent, june & jeri
-- change tpEmployees username to rydellcars.com, should cascase UPDATE to employeeAppAuthorization
SELECT * FROM #base 
WHERE visionusername IN ('jpenas@rydellchev.com','jgustafson@rydellchev.com','tolson@gfhonda.com')

SELECT *
FROM tpemployees
WHERE username IN ('jpenas@rydellchev.com','jgustafson@rydellchev.com','tolson@gfhonda.com')

UPDATE tpEmployees
SET username = 'jschmiess@rydellcars.com'
WHERE username = 'jpenas@rydellchev.com';
UPDATE tpEmployees
SET username = 'jgustafson@rydellcars.com'
WHERE username = 'jgustafson@rydellchev.com';
UPDATE tpEmployees
SET username = 'tolson@rydellcars.com'
WHERE username = 'tolson@gfhonda.com';

INSERT INTO employeeAppAuthorization (username,appname,appSeq,appcode,approle,functionality)
SELECT a.username, appName, 800, appCode, appRole, functionality 
FROM (
  SELECT *
  FROM tpemployees
  WHERE username IN ('jschmiess@rydellcars.com','jgustafson@rydellcars.com','tolson@rydellcars.com')) a
LEFT JOIN applicationMetaData b
  on b.appcode = 'pto'
  AND b.approle = 'ptomanager';   
