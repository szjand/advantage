/*
6/11/20
Sue�s position was responsible for authorizing PTO for Rental Agents.
Who now assumes that responsibility?

You can put that under Nick Neumann now.  Thanks!

Laura Roth
*/
SELECT * FROM ptoemployees WHERE username LIKE 'swali%'  1147062
SELECT * FROM ptoemployees WHERE username LIKE 'nneuma%' 1102198

SELECT * FROM ptopositionfulfillment WHERE employeenumber = '1102198'

RY1 Drive Manager

SELECT * FROM ptoauthorization WHERE authfordepartment = 'rental'

UPDATE ptoauthorization
SET authByDepartment = 'RY1 Drive',
    authByPosition = 'Manager'
WHERE authforDepartment = 'Rental'
  AND authForPosition = 'Agent'


/*
05/21/20
Hi Jon-

Could you please switch the Honda PTO Admin for PDQ to Ryan Shroyer?  He�s the PDQ Manager there.

Thanks!

Laura Roth 
*/

UPDATE ptoauthorization
SET authforposition = 'Express Service Representative'
-- SELECT * FROM ptoauthorization
WHERE authfordepartment = 'ry2 pdq'
  AND authForPosition = 'Advisor'
  
UPDATE ptoauthorization  
SET authbydepartment = 'RY2 PDQ'
-- SELECT * FROM ptoauthorization
WHERE authfordepartment = 'ry2 pdq'
  AND authforposition NOT IN ('manager','assistant manager')
  
SELECT * FROM tpemployees WHERE lastname = 'shroyer'  

SELECT * FROM employeeappauthorization
where username = 'jdangerfield@gfhonda.com'
AND appcode = 'pto'
  AND approle <> 'ptoadminemployee'

SELECT * FROM employeeappauthorization
where username = 'rshroyer@rydellcars.com'
AND appcode = 'pto'


delete
FROM employeeappauthorization
WHERE username = 'rshroyer@rydellcars.com'
  AND appname = 'pto';


INSERT INTO employeeappauthorization
SELECT 'rshroyer@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
where username = 'jdangerfield@gfhonda.com'
AND appcode = 'pto'
  AND approle <> 'ptoadminemployee'



/*
Hi Jon-

Paul should have 112 hours that updated in Vision on 4/23.  Could you please add those in?  His PTO Anniversary should be 4/23/2018.

Thanks!

Laura Roth 
05/14/20


jon:
Who authorizes PTO for RY1 PDQ Express Service Representatives? (is that position what use to be called an Advisor?)

laura:
It was Wyatt Olson but his last day is tomorrow.  So you can move all of them over to Dayton Marek.  
Dayton will be the new PDQ Manager instead of Detail Manager and Tyler Hagen will be the new Detail Manager.

And yes, it used to be the PDQ Advisor role.

*/
-- wyatt olson
SELECT * FROM ptoemployees WHERE username LIKE 'wolson%'
-- wolson@rydellcars.com
-- 144788 
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '144788'
-- RY1 PDQ Assistant Manager
SELECT * 
FROM ptoauthorization
WHERE authbydepartment = 'ry1 pdq'
  AND authbyposition = 'assistant manager'
  
-- dayton  Detail Manager
SELECT * FROM ptoemployees WHERE username LIKE 'dmare%'
-- dmarek@rydellcars.com
-- 190910
select * from ptopositionfulfillment where employeenumber = '190910'

-- currently kevin vancamp (175642) IS pdq manager	     
SELECT * FROM ptodepartmentpositions WHERE department = 'ry1 pdq'  
SELECT * FROM ptopositionfulfillment WHERE department = 'ry1 pdq' AND position = 'manager'
SELECT * FROM dds.edwEmployeeDim WHERE employeenumber = '175642'
select * FROM tpemployees WHERE employeenumber = '175642'

-- tyler hagen  Detail Technician
SELECT * FROM ptoemployees WHERE username LIKE 'thage%'
-- thagen@rydellcars.com
-- 195975
select * from ptopositionfulfillment where employeenumber = '195975'  


-- pbehm@rydellcars.com
SELECT * FROM ptoemployees WHERE employeenumber = '187564'
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '187564'

SELECT *
FROM ptoauthorization
WHERE authfordepartment = 'RY1 PDQ'

1. CONVERT tyler to detail manager
2. CONVERT dayton to pdq manager
3. change pto authoriztion FROM pdq assistant manger to pdq manager
4. ADD express service representative to authorization
5. remove kevin vancamp
6. give tyler access to pto management

UPDATE ptopositionfulfillment
SET position = 'Manager'
-- SELECT * FROM ptopositionfulfillment
WHERE employeenumber = '195975';

UPDATE ptopositionfulfillment
SET department = 'RY1 PDQ', position = 'Manager'
-- SELECT * FROM ptopositionfulfillment
WHERE employeenumber = '190910';

UPDATE ptoauthorization
SET authbyposition = 'Manager'
-- SELECT * FROM ptoauthorization 
WHERE authbydepartment = 'RY1 PDQ'
  AND authbyposition = 'Assistant Manager';

INSERT INTO ptoauthorization values ('RY1 PDQ','Manager','RY1 PDQ','Express Service Representative',5,6);

DELETE FROM ptopositionfulfillment WHERE employeenumber = '175642';

delete
FROM employeeappauthorization
WHERE username = 'thagen@rydellcars.com'
  AND appname = 'pto';


INSERT INTO employeeappauthorization
SELECT 'thagen@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'dmarek@rydellcars.com'
AND appcode = 'pto';

/*
Hey Jon-

Can you please do me a favor and switch the PTO admin in Vision to Dylan Haley 
for the Customer Care Sales Specialists only?  Dylan instead of Morgan.

Thanks!

Laura Roth 
03/31/2020
*/

SELECT *
FROM tpemployees
WHERE lastname = 'haley'

SELECT *
FROM ptopositionfulfillment
WHERE employeenumber = '163700'

SELECT *
FROM ptodepartmentpositions
ORDER BY department, position

SELECT *
FROM ptopositionfulfillment
WHERE position = 'Customer Care Sales Lead'

Customer Care Center
		 Customer Care Lead
		 Customer Care Representative
		 Customer Care Sales Lead
		 Customer Care Sales Specialist
		 Customer Care Service Specialist
		 
SELECT *
FROM ptoauthorization
WHERE authforposition = 'Customer Care Sales Specialist'		 

-- change dylan to Customer Care Sales Lead

UPDATE ptopositionfulfillment
SET department = 'Customer Care Center',
    position = 'Customer Care Sales Lead'
-- SELECT * FROM ptopositionfulfillment
WHERE employeenumber = '163700';

-- AND change authorization
UPDATE ptoauthorization
SET authByDepartment = 'Customer Care Center',
    authByPosition = 'Customer Care Sales Lead'
-- SELECT * FROM ptoauthorization
WHERE authforposition = 'Customer Care Sales Specialist';

SELECT *
FROM ptopositionfulfillment a
JOIN tpemployees b on a.employeenumber = b.employeenumber
WHERE position = 'Customer Care Sales Specialist'



/*
Hey Jon-

Could you please switch the PTO admin for the Customer Care Sales Specialist position back to Morgan?

Thanks!
3/06/20
*/

UPDATE ptoAuthorization
SET authByDepartment = 'Market', authByPosition = 'Marketing Manager'
-- SELECT * FROM ptoAuthorization
WHERE authForPosition = 'Customer Care Sales Specialist'






/*
Hi Jon-

Could you please do me a favor and change the PTO admin for Custodians and Maintenance people to Arik Solheim?

Thanks!

Laura Roth 
*/

SELECT *
FROM ptodepartmentpositions
WHERE department = 'building maintenance' OR position = 'custodian'

SELECT *
FROM ptodepartmentpositions
WHERE department = 'guest experience'


  
  
SELECT a.firstname, a.lastname, b.*
FROM tpemployees a
INNER JOIN ptopositionfulfillment b on a.employeenumber = b.employeenumber
WHERE position = 'custodian'
  OR department = 'building maintenance'

select * 
FROM pto_compli_users
WHERE status = '1'
AND title LIKE '%ustod%'


-- chandra pokhrel missing FROM pos fulfillment
INSERT INTO ptopositionfulfillment values('Guest Experience','Custodian','187526')

SELECT * 
FROM ptodepartmentpositions
WHERE position = 'custodian'

select * 
FROM ptodepartmentpositions
WHERE department = 'building maintenance'

-- ADD custodian to building maintenance
INSERT INTO ptodepartmentpositions values ('Building Maintenance','Custodian',4,'no_policy_html');

SELECT *
FROM ptoauthorization
WHERE authfordepartment = 'Building Maintenance'

-- make arik the authorizer for BM man
UPDATE ptoauthorization
SET authbydepartment = 'Building Maintenance',
    authbyposition = 'Manager'
WHERE authfordepartment = 'Building Maintenance'
  AND authforposition IN ('Assistant Manager','Technician')  
-- I fucked up the manager authorization
UPDATE ptoauthorization
SET authbydepartment = 'Market',
    authbyposition = 'General Manager'
WHERE authfordepartment = 'Building Maintenance'
  AND authforposition IN ('Manager')  
  
ADD authorization for BM Custodians
INSERT INTO ptoauthorization values('Building Maintenance','Manager','Building Maintenance','Custodian',3,4)

-- change the department for custodians to Building Maintenance  
SELECT * FROM ptopositionfulfillment WHERE position = 'custodian'

UPDATE ptopositionfulfillment
SET department = 'Building Maintenance'
WHERE position = 'Custodian'