
/*
DAYS USED SINCE JANUARY 1, 2014	
			Hours
KATHY BLUMHAGEN		20	  160
BRIAN HILL		    14.5	116
DAN EVAVOLD		    15	  120
DAVID LUEKER		  17.5	140
ANNE OSTLUND		  9	     72
NICK SATTLER		  13	  104
RANDY SATTLER		  9	     72
MARK STEINKE		  9	     72
MIKE YEM		      4.5	   36

*/
-- DROP TABLE #body;
SELECT a.userid, a.firstname, a.lastname, a.title, a.email, 
  LEFT(a.supervisorName,15) AS compliSuper,
  c.username AS tpUser, 
  c.password as tpPassword, b.username AS ptoUser,
  CASE WHEN b.employeenumber IS NULL THEN NULL ELSE 'Yes' END AS inPTO,
--  CASE WHEN /*a.email*/c.username = b.username THEN 'Yes' ELSE b.username END AS userAgree,
  CASE WHEN c.employeenumber IS NULL THEN NULL ELSE 'Yes' END AS tpEmp,
  CASE WHEN d.username IS NULL THEN NULL ELSE 'Yes' END AS hasPto,
  left(TRIM(e.department) + ':' + e.position,35) AS ptoPos,
  TRIM(h.firstname) + ' ' + h.lastname AS ptoAdmin
INTO #body
FROM pto_compli_users a
LEFT JOIN ptoEmployees b on a.userid = b.employeenumber
LEFT JOIN tpEmployees c on a.userid = c.employeenumber
LEFT JOIN (
  SELECT DISTINCT username
  FROM employeeAppAuthorization
  WHERE appcode = 'pto') d on c.username = d.username
LEFT JOIN ptoPositionFulfillment e on b.employeenumber = e.employeenumber
LEFT JOIN ptoAuthorization f on e.department = f.authForDepartment
  AND e.position = f.authForPosition
LEFT JOIN ptoPositionFulfillment g on f.authByDepartment = g.department
  AND f.authByPosition = g.position
LEFT JOIN dds.edwEmployeeDim h on g.employeenumber = h.employeenumber
  AND h.currentrow = true    
WHERE a.userid IN (
 SELECT employeenumber
 FROM dds.edwEmployeeDim h
 WHERE lastname IN('BLUMHAGEN','HILL','EVAVOLD','LUEKER','OSTLUND','SATTLER',
  'SATTLER','STEINKE','YEM')
    AND currentrow = true)
  AND a.status = '1'
ORDER BY a.firstname;

BEGIN TRANSACTION;
TRY 
-- 1. mark & randy, change tpUser to rydellcars
UPDATE tpEmployees
SET username = 'rsattler@rydellcars.com'
WHERE username = 'rsattler@rydellchev.com';

UPDATE tpEmployees
SET username = 'msteinke@rydellcars.com'
WHERE username = 'msteinke@rydellchev.com';

--SELECT * FROM #body;
-- tpEmployees for those that are NOT currently IN it
INSERT INTO tpEmployees(username,firstname,lastname,employeenumber,password,
  membergroup,storecode,fullname)
SELECT a.ptoUser, b.firstname, b.lastname, a.userid, 
  (SELECT generate_password(length(trim(ptoUser) + TRIM(b.firstname)), 
        cast(trim(a.userid) AS sql_integer)) FROM system.iota) AS password, 
  'Fuck You', b.storecode,
  TRIM(b.firstname) + ' ' + b.lastname
FROM #body a
LEFT JOIN dds.edwEmployeeDim b on a.userid = b.employeenumber
  AND b.currentrow = true 
WHERE a.tpUser IS NULL;

INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,approle,functionality)
SELECT b.ptoUser, a.appname,a.appseq,a.appcode,a.approle,a.functionality
FROM ApplicationMetaData a
INNER JOIN #body b on 1 = 1
WHERE a.appcode = 'pto' 
  AND a.approle = 'ptoemployee'
  AND b.tpuser IS NULL;

/* 
SELECT a.ptouser, b.*
FROM #body a
LEFT JOIN pto_employee_pto_allocation b on a.userid = b.employeenumber  
  AND curdate() BETWEEN b.fromdate AND b.thrudate
*/
-- INSERT used pto FROM ben's spreadsheet USING FROM date FROM current pto period  
--kathy  
insert into pto_used values('160930','01/01/2014','Body Shop non hourly',160);
--brian
insert into pto_used values('165357','01/01/2014','Body Shop non hourly',116);
--dan
insert into pto_used values('141069','01/01/2014','Body Shop non hourly',120);
--dave
insert into pto_used values('188338','01/01/2014','Body Shop non hourly',140);
--anne
insert into pto_used values('1108200','01/01/2014','Body Shop non hourly',72);
--nick
insert into pto_used values('1122320','01/01/2014','Body Shop non hourly',104);
--randy
insert into pto_used values('1122342','01/01/2014','Body Shop non hourly',72);
--mark
insert into pto_used values('1132218','01/01/2014','Body Shop non hourly',72);
--rithy  
insert into pto_used values('1150510','04/29/2014','Body Shop non hourly',36);

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

/*
user list for ben
SELECT userid, b.fullname, b.username, b.password, a.ptoadmin
FROM #body a
LEFT JOIN tpemployees b on a.userid = b.employeenumber
ORDER BY ptoadmin, fullname
*/

  
  
