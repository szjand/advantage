/*
1/13/16
Rethinking Terms

the pto structure IS the only source of information regarding an individuals
pto earnings AND usage
This came painfully to mind WHEN after terminating an employee BY
deleting ALL ptoEmployee , pto_used, pto_allocation etc rows, HR needed to 
know what the PTO status was at te time of termination

So, the new policy, effective immediately:
Vision:
    tpEmployees & employeeAppAuthorization: DELETE, no need for history

    pto: only UPDATE tpoEmployees.active to false
         NO MORE DELETING
         Still need to flesh out process of termed employee IS a pto authorizer
         
Tool: UPDATE users.active to False
      UPDATE ThruTS IN ContactMechanisms, ApplicationUsers, PartyRelationships, PartyPrivileges         
january 2018
  laura roth, new hr, concerned that on the employee search page, a termed person still shows up
  so, i modified sp pto_search_for_employee limiting result to ptoEmployees rows WHERE active = true
  
2-10-18
  kc langenstein IS gone
  nick shirek has been appointed ry1 gsm
  no one named to ry2 gsm
  turns out it will be jared langenstein, but it has NOT yet been announced
  
  so nick shows up for laura AS a new ry1 employee, she can't ADD him AS ry1 gsm
  because that slot IS still filled BY kc langenstein
  
  tangentially, here IS another one, bob foote IS gone, shawn squires IS manager
  but can NOT be put INTO that roll because it IS still filled BY foote
  
  this ALL IS because i have NOT figured out what to DO WHEN a pto authorizer 
  leaves OR IS replaced
  
02-22-18
Hi Jon-
Could you please delete Dale Reed from the Vision page?  His is switching to Part Time.
Thanks!
  
SELECT *
FROM employeeappauthorization
WHERE username LIKE 'rchav%'

  
same old thing, she equates pto with vision
he doesn't need to be deleted, he IS still an employee, but he IS no longer entitled
to pto, so, actually, seems LIKE the correct solution IS to remove his access to
pto, which takes care of him, but her panties are bunched BY him showing up IN her (AND dan stinar's)
admin search page (i am guessing)  
updated the proc: pto_search_for_employee: limited to full time
SELECT * FROM tpemployees WHERE employeenumber = '1114831'
select * FROM tpemployees WHERE lastname = 'berg'

SELECT * 
-- delete
FROM employeeappauthorization 
WHERE username = 'dreed@rydellcars.com'
  AND appname = 'pto'

-- 8/29/18
Adam Kohls just moved to part time � could you please take him out of Vision for me?
same old thing, his status has been changed IN dealertrack, therefore IN edwEmployeeDim
therefore he does NOT show up IN her pto admin page search  
for him, remove access to pto

SELECT * FROM tpemployees WHERE lastname = 'kohls'

SELECT * 
-- delete
FROM employeeappauthorization 
WHERE username = 'akohls@rydellcars.com'
  AND appname = 'pto'


select a.*, b.name, b.fullparttime
FROM ptoemployees a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'Active'
WHERE a.active = true
ORDER BY b.name
  
    
SELECT * FROM tpemployees WHERE lastname = 'stafford'
select * FROM dds.edwEmployeeDim WHERE lastname = 'klohn'

SELECT * FROM 
*/

select * FROM tpemployees WHERE lastname LIKE 'espi%'
SELECT * FROM dds.edwEmployeeDim WHERE lastname = 'ness' AND firstname = 'hannah'

select * FROM ptopositionfulfillment ORDER BY department, employeenumber

SELECT * FROM tpemployees WHERE lastname = 'ludwig'

/*
03/04/20
Dan Stinar termed, replace his ptoAuthorization AuthBy roles with ben cahalan

UPDATE ptoAuthorization
SET authByDepartment = 'Market', authByPosition = 'General Manager'
-- select * FROM ptoAuthorization
WHERE authbydepartment = 'ry1 parts'
  AND authbyposition = 'manager'  
*/  
-- 9/28 think i need to DELETE position fulfillment too
--    the bob foote shaw squires fiasco
-- vision 
-- verify that this IS the correct person
select * FROM dds.edwEmployeeDim WHERE lastname = 'winzer'
select * FROM dds.edwEmployeeDim WHERE employeenumber = '175323'
select * FROM tpemployees WHERE lastname like '%olson%'

DECLARE @username string;
DECLARE @isAdmin integer;
@username = (
  SELECT username
  FROM tpEmployees
  WHERE lastname like '%olson%'
    AND firstname = 'rachel');
-- @username = 'jbooker@gfhonda.com';    
-- does he admin anyone's pto
@isAdmin = (
  SELECT COUNT(*)
  FROM ptoAuthorization a
  INNER JOIN (
    SELECT *
    FROM ptoPositionFulfillment
    WHERE employeenumber = (
      SELECT employeenumber
      FROM tpemployees
      WHERE username = @username)) b on a.authByDepartment = b.department
    AND a.authByPosition =  b.position);     
-- SELECT @isAdmin FROM system.iota;      
IF @isAdmin = 0 THEN 
-- no, go ahead AND whack it all 
  BEGIN TRANSACTION;
  TRY 
    DELETE 
    FROM employeeappauthorization
    WHERE username = @username;   
    DELETE 
    FROM tpEmployees
    WHERE username = @username;
    UPDATE ptoEmployees
    SET active = False
    WHERE username = @username;
  COMMIT WORK;
  CATCH ALL
    ROLLBACK;
    RAISE;
  END TRY;
ELSE 
  SELECT 'STOP THE FUCKING TRAIN' FROM system.iota;
ENDIF;  

/*
08/14/20
IN the process of transferring to postgresql, it seems that WHEN a person IS
termed, the relevant rows IN pto_Employee_pto_allocation should be deleted
DO a script to snapshot pto @ term to persist, emp#, date, pto_remaining

oh shit, this IS NOT so clear, 
*/
select * FROM dds.edwEmployeeDim WHERE lastname = 'budeau'

SELECT * 
-- delete
FROM pto_employee_pto_allocation WHERE employeenumber = '165873' ORDER BY fromdate

-- tool
-- leave people, he may no longer be an employee,but he IS still a person
-- partyid remains
-- SELECT * FROM dps.people WHERE partyid = 'a9cae609-bc67-ca45-9fa5-746012615935'
-- SELECT * FROM dps.users WHERE username LIKE '%ramon%' AND active
DECLARE @PartyID string;
DECLARE @NowTS timestamp;

@PartyID = (SELECT PartyID FROM dps.users WHERE username = 'mramon');
@NowTS = (SELECT now() FROM system.iota);

BEGIN TRANSACTION;
TRY 
  UPDATE dps.contactmechanisms
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE dps.users
  SET Active = False
  WHERE partyid = @PartyID;
  
  UPDATE dps.ApplicationUsers
  SET ThruTS = @NowTS
  WHERE partyid = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE dps.PartyRelationships
  SET ThruTS = @NowTS
  WHERE partyid2 = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE dps.PartyPrivileges
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END;   

