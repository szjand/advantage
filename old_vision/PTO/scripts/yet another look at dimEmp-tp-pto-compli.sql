SELECT a.employeenumber, a.lastname, a.firstname, b.username, b.password,
  c.username, d.title
FROM dds.edwEmployeeDim a
LEFT JOIN tpEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN ptoEmployees c on a.employeenumber = c.employeenumber
LEFT JOIN pto_compli_users d on a.employeenumber = d.userid
WHERE a.employeenumber IN ('148080','184621','218360','152520',
    '195412','1130072','241120','150126','1126052','1150920','184920')
  AND currentrow = true
ORDER BY a.lastname, a.firstname





SELECT a.username, b.appcode AS pto, c.appcode AS tp
FROM (
  SELECT *
  FROM employeeappauthorization) a
LEFT JOIN (  
  SELECT *
  FROM employeeappauthorization
  WHERE appcode = 'pto') b on a.username = b.username
LEFT JOIN (  
  SELECT *
  FROM employeeappauthorization
  WHERE appcode = 'tp') c on a.username = c.username 
group by a.username, b.appcode, c.appcode



SELECT a.employeenumber, left(a.lastname, 20), left(a.firstname, 20), 
  left(b.username, 30),LEFT( b.password,12),
  d.title  AS compli_title, e.pto, e.tp
FROM dds.edwEmployeeDim a
LEFT JOIN tpEmployees b on a.employeenumber = b.employeenumber
LEFT JOIN ptoEmployees c on a.employeenumber = c.employeenumber
LEFT JOIN pto_compli_users d on a.employeenumber = d.userid
LEFT JOIN (
  SELECT a.username, b.appcode AS pto, c.appcode AS tp
  FROM (
    SELECT *
    FROM employeeappauthorization) a
  LEFT JOIN (  
    SELECT *
    FROM employeeappauthorization
    WHERE appcode = 'pto') b on a.username = b.username
  LEFT JOIN (  
    SELECT *
    FROM employeeappauthorization
    WHERE appcode = 'tp') c on a.username = c.username 
  group by a.username, b.appcode, c.appcode) e on b.username = e.username
WHERE a.employeenumber IN ('148080','184621','218360','152520',
    '195412','1130072','241120','150126','1126052','1150920','184920')
  AND currentrow = true
ORDER BY a.lastname, a.firstname