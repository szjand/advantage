ALTER PROCEDURE pto_get_manager_employees (
      employeeNumber CICHAR ( 9 ),
      employeeNumber cichar(9) output,
      name CICHAR ( 52 ) OUTPUT,
      department CICHAR ( 30 ) OUTPUT,
      title CICHAR ( 30 ) OUTPUT,
      ptoAnniversary DATE OUTPUT,
      ptoExpiringWeeks integer output,
      ptoHoursEarned DOUBLE ( 15 ) OUTPUT,
      ptoHoursRequested integer output,
      ptoHoursApproved integer output,
      ptoHoursUsed DOUBLE ( 15 ) OUTPUT,
      ptoHoursRemaining DOUBLE ( 15 ) OUTPUT)
      
BEGIN
/*       
11/02/14 need to exclude part timers  
11/9/14
  need to include pto_adjustments INTO ptoEarned
  AND limit request/approved to the current period for each employee
  think i am going to have to DO subqueries IN the SELECT statement
  OR a CURSOR
  the problem, of course, IS that the from/thru period IS different
  for every employee
  
EXECUTE PROCEDURE pto_get_manager_employees ('157995');
*/  

DECLARE @employeeNumber string;

DECLARE @employeesCursor CURSOR AS 
  SELECT d.employeeNumber
  FROM ptoAuthorization a
  INNER JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
    AND a.authByPosition = b.position
  INNER JOIN ptoPositionFulfillment c on a.authForDepartment = c.department
    AND a.authForPosition = c.position  
  INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
    AND d.currentrow = true 
    AND d.active = 'active' 
    AND d.fullparttime = 'full'
  WHERE b.employeenumber = @employeenumber;
@employeeNumber = (select employeenumber from __input); --'157995';
DELETE FROM pto_tmp_manager_employees;
OPEN @employeesCursor;
TRY
  WHILE FETCH @employeesCursor DO
    INSERT INTO pto_tmp_manager_employees
    SELECT employeenumber, name, department, position, ptoAnniversary, 
      timestampdiff(sql_tsi_week, curdate(), ptoThruDate) AS ptoExpiringWeeks,
      ptoHoursEarned, ptoHoursRequested, ptoHoursApproved, ptoHoursUsed,
      ptoHoursRemaining
    FROM (EXECUTE PROCEDURE pto_get_employee_pto(@employeesCursor.employeeNumber)) a;
  END WHILE;
FINALLY
  CLOSE @employeesCursor;
END TRY;    
INSERT INTO __output
SELECT top 200 *
FROM pto_tmp_manager_employees
ORDER BY ptoExpiringWeeks;
END;
/*
DECLARE @employeeNumber string;
@employeeNumber = (SELECT employeeNumber FROM __input);
INSERT INTO __output
select top 1000 d.employeeNumber, TRIM(d.firstname) + ' ' + d.lastName AS name, c.department, 
  c.position AS title, e.ptoAnniversary,
  timestampdiff(sql_tsi_week, curdate(), f.thruDate) AS ptoExpiringWeeks,
  f.hours AS ptoHoursEarned,
  coalesce(g.ptoHoursRequested,0) AS ptoHoursRequested,
  coalesce(h.ptoHoursApproved,0) AS ptoHoursApproved,
  coalesce(i.ptoHoursUsed,0) AS ptoHoursUsed,
  f.hours-coalesce(g.ptoHoursRequested,0)-coalesce(h.ptoHoursApproved,0)-coalesce(i.ptoHoursUsed,0) AS ptorHoursRemaining
FROM ptoAuthorization a
INNER JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
  AND a.authByPosition = b.position
INNER JOIN ptoPositionFulfillment c on a.authForDepartment = c.department
  AND a.authForPosition = c.position  
INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
  AND d.currentrow = true 
  AND d.active = 'active' 
  AND d.fullparttime = 'full'
INNER JOIN ptoEmployees e on c.employeeNumber = e.employeeNumber  
INNER JOIN pto_employee_pto_allocation f on e.employeenumber = f.employeenumber
  AND curdate() BETWEEN f.fromdate AND f.thrudate
LEFT JOIN ( 
  SELECT employeeNumber, SUM(hours) AS ptoHoursRequested
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Pending'
    AND thedate >= curdate() 
  GROUP BY employeeNumber) g on c.employeeNumber = g.employeeNumber  
LEFT JOIN (
  SELECT employeeNumber, SUM(hours) AS ptoHoursApproved
  FROM pto_requests  
  WHERE requestType = 'paid time off'
    AND requestStatus = 'Approved'
    AND thedate >= curdate()
  GROUP BY employeeNumber) h on c.employeenumber = h.employeenumber  
LEFT JOIN ( -- ptoUsed: LEFT JOIN, may NOT be any used
  SELECT a.employeenumber, SUM(a.hours) AS ptoHoursUsed
  FROM pto_used a
  INNER JOIN pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
  WHERE a.theDate BETWEEN b.fromDate AND b.thruDate
  GROUP BY a.employeeNumber) i on c.employeenumber = i.employeenumber  
WHERE b.employeenumber = @employeenumber
ORDER BY timestampdiff(sql_tsi_week, curdate(), f.thruDate);
END;
*/
  
  
  