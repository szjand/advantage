/*
10/22/14
  ADD ptoPolicyHtml to ptoDepartmentPositions
*/
DROP TABLE ptoDepartments;
CREATE TABLE ptoDepartments (
  departmentKey autoinc, 
  store cichar(3) constraint NOT NULL,
  area cichar(24) constraint NOT NULL default 'N/A',
  dL1 cichar(24) constraint NOT NULL default 'N/A',
  dL2 cichar(24)constraint NOT NULL default 'N/A',
  dL3 cichar(24)constraint NOT NULL default 'N/A',
  description cichar(60)constraint NOT NULL,
  fromDate date constraint NOT NULL,
  thruDate date constraint NOT NULL default '12/31/9999',
  constraint PK primary key (departmentKey)) IN database;
  
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoDepartments', 'ptoDepartments.adi', 'NK', 'store;area;dL1;dL2;dL3;thruDate', '', 2051, 1024, '');

/*
sequenced primary key
there will be no deleting of rows, deleting will be an UPDATE of thruDate
so, trigger needs to exist for begore INSERT AND before UPDATE 
*/

/*
CREATE TRIGGER deptSeqPk
   ON ptoDepartments
   BEFORE 
   INSERT 
BEGIN 

--verify that any data entry into stgTechTrainingOther is for a currently valid/active tech

declare @count integer;
DECLARE @tech cichar(3);
DECLARE @store cichar(3);
DECLARE @date date;
@date = (SELECT thedate FROM __new);
@store = (SELECT storecode FROM __new);
@tech = (SELECT technumber FROM __new);
@count = (
SELECT count(*)
FROM dimtech
  WHERE technumber = @tech
    AND storecode = @store
    AND active = true
    AND @date BETWEEN techkeyfromdate AND techkeythrudate);
if @COUNT = 0 then
  raise ERROR(100, 'OMFG');
end if;
*/
/*  
INSERT INTO ptoDepartments (store,description,fromDate)
values('All', 'Market', '08/01/2014');  
INSERT INTO ptoDepartments (store,area,description,fromDate)
values('All', 'Fixed', 'Market Fixed','08/01/2014');  
INSERT INTO ptoDepartments (store,area,dl1,description,fromDate)
values('All', 'Fixed', 'Parts','Parts','08/01/2014');
INSERT INTO ptoDepartments (store,area,dl1,description,fromDate)
values('All', 'Fixed', 'PDQ','Market PDQ','08/01/2014');
INSERT INTO ptoDepartments (store,area,dl1,dl2,description,fromDate)
values('RY1', 'Fixed', 'Service','PDQ','RY1 PDQ','08/01/2014');
INSERT INTO ptoDepartments (store,area,dl1,dl2,description,fromDate)
values('RY2', 'Fixed', 'Service','PDQ','RY2 PDQ','08/01/2014');
INSERT INTO ptoDepartments (store,area,dl1,description,fromDate)
values('RY1', 'Fixed', 'Service','RY1 Service','08/01/2014');
INSERT INTO ptoDepartments (store,area,dl1,dl2,description,fromDate)
values('RY1', 'Fixed', 'Service','Main Shop','RY1 Main Shop','08/01/2014');
*/
insert into ptoDepartments (store,description, fromDate) values('All','Market','08/01/2014');
insert into ptoDepartments (store,area,description, fromDate) values('All','Fixed','Market Fixed','08/01/2014');
insert into ptoDepartments (store,area,description, fromDate) values('RY1','Variable','RY1 Sales','08/01/2014');
insert into ptoDepartments (store, area,description,fromDate) values('RY2','Variable','RY2 Sales','08/01/2014');
insert into ptoDepartments (store, area,dl1,description,fromDate) values('All','Admin','Office','Office','08/01/2014');
insert into ptoDepartments (store, area,dl1,description,fromDate) values('All','Admin','IT','IT','08/01/2014');
insert into ptoDepartments (store, area,dl1,description,fromDate) values('All','Admin','Guest Relations','Guest Relations','08/01/2014');
insert into ptoDepartments (store, area,dl1,description,fromDate) values('All','Fixed','PDQ','Market PDQ','08/01/2014');
insert into ptoDepartments (store, area,dl1,description,fromDate) values('All','Fixed','Parts','Parts','08/01/2014');
insert into ptoDepartments (store, area,dl1,description,fromDate) values('RY1','Fixed','Service','RY1 Service','08/01/2014');
insert into ptoDepartments (store, area,dl1,description,fromDate) values('RY1','Fixed','Body Shop','Body Shop','08/01/2014');
insert into ptoDepartments (store, area,dl1,dl2,description,fromDate) values('RY1','Fixed','Service','Service Drive','RY1 Drive','08/01/2014');
insert into ptoDepartments (store, area,dl1,dl2,description,fromDate) values('RY1','Fixed','Service','PDQ','RY1 PDQ','08/01/2014');
insert into ptoDepartments (store, area,dl1,dl2,description,fromDate) values('RY1','Fixed','Service','Main Shop','RY1 Main Shop','08/01/2014');
insert into ptoDepartments (store, area,dl1,description,fromDate) values('RY1','Fixed','Appearance','Appearance','08/01/2014');
insert into ptoDepartments (store, area,dl1,dl2,description,fromDate) values('RY1','Fixed','Appearance','Detail','Detail','08/01/2014');
insert into ptoDepartments (store, area,dl1,dl2,description,fromDate) values('RY1','Fixed','Appearance','Car Wash','Car Wash','08/01/2014');
insert into ptoDepartments (store, area,dl1,dl2,description,fromDate) values('RY2','Fixed','Service','PDQ','RY2 PDQ','08/01/2014');
insert into ptoDepartments (store, area,dl1,dl2,description,fromDate) values('RY2','Fixed','Service','Main Shop','RY2 Main Shop','08/01/2014');
insert into ptoDepartments (store, area,dl1,dl2,dl3,description,fromDate) values('RY1','Fixed','Service','Service Drive','Rental','Rental','08/01/2014');
insert into ptoDepartments (store, area,dl1,dl2,dl3,description,fromDate) values('RY1','Fixed','Service','Service Drive','BDC','BDC','08/01/2014');

DROP TABLE ptoPositions;
CREATE TABLE ptoPositions (
  positionKey autoinc,
  position cichar(30) constraint NOT NULL,
  payType cichar(1) constraint NOT NULL,
  fromDate date constraint NOT NULL,
  thruDate date constraint NOT NULL default '12/31/9999',
  constraint PK primary key (positionKey)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoPositions', 'ptoPositions.adi', 'NK', 'position;payType;thruDate', '', 2051, 1024, '');
/*  
INSERT INTO ptoPositions (position, payType, fromDate)
values('Director', 'S', curdate());  
INSERT INTO ptoPositions (position, payType, fromDate)
values('Manager', 'S', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Inventory Manager', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Sales Consultant', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Outside Sales Consultant', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Driver', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Shipping & Receiving', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Aftermarket Sales Consultant', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Shipping Coordinator', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Lead Technician', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Upperbay Technician', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Lowerbay Technician', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Advisor', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Assistant Manager', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Dispatcher', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Lead Technician (A)', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Technician (A)', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Technician (B)', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Technician (C)', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Technician (D)', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('PT Technician (C)', 'H', curdate());
INSERT INTO ptoPositions (position, payType, fromDate)
values('Accessory Technician (A)', 'H', curdate());
*/
insert into ptoPositions (position, paytype,fromdate) values('Director','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Manager','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Production Manager','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Estimator (A)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Estimator (B)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Estimator (C)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Porter','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Admin','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Paint Team Leader','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Paint Technician (A)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Paint Technician (B)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Paint Technician (C)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Metal Team Leader (A)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Metal Technician (A)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Metal Technician (B)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('PDR Technician','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Parts','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Detailer','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Staging Technician','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Detail Technician','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Cashier','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Support','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Team Leader','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Assistant Manager','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('General Manager','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Custodians/Maintenance','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Reception','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Dispatcher','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Building Maintenance','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Advisor','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Lead Technician','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Upperbay Technician','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Lowerbay Technician','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Technician (A)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Technician (B)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Technician (C)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Technician (D)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('PT Technician (C)','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Accessory Technician','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Operator','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Agent','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Courtesy Driver','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Warranty Admin','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Accounts Receivable/Payroll','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Accounts Payable','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Title Clerk','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Warranty Admin - HGF','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('File Clerk','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('F&I Admin','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Project Coordinator','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Accounting Admin','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Office Manager','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Controller','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Inventory Manager','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Sales Consultant','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Outside Sales Consultant','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Driver','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Shipping & Receiving','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Aftermarket Sales Consultant','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Shipping Coordinator','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('IT Specialist','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('General Sales Manager','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Digital Coordinator','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Sales Support','C','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('New Car Sales Manager','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Sales Consultant','C','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Finance Manager','C','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('New Car Technology Specialist','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Used Car Buyer','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Sales Consultant (PT)','C','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Maintenance Manager','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Team Member','H','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Executive Manager','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Used Car Inventory Manager','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('New Car Inventory Manager','S','08/01/2014');
insert into ptoPositions (position, paytype,fromdate) values('Sales Manager','S','08/01/2014');

/*
EXECUTE PROCEDURE sp_DropReferentialIntegrity('ptoDepartments-ptoDepartmentPositions');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('ptoPositions-ptoDepartmentPositions');
*/
DROP TABLE ptoDepartmentPositions;
CREATE TABLE ptoDepartmentPositions (
  departmentKey integer constraint NOT NULL,
  positionKey integer constraint NOT NULL,
  fromDate date constraint NOT NULL,
  thruDate date constraint NOT NULL default '12/31/9999',
  constraint PK  primary key (departmentKey, positionKey, thruDate)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoDepartmentPositions', 'ptoDepartmentPositions.adi', 'FK1', 'departmentKey', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoDepartmentPositions', 'ptoDepartmentPositions.adi', 'FK2', 'positionKey', '', 2, 1024, '' );
       
-- UPDATE: Cascade  DELETE: Restrict  
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoDepartments-ptoDepartmentPositions', 'ptoDepartments', 'ptoDepartmentPositions', 'FK1', 1, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoPositions-ptoDepartmentPositions', 'ptoPositions', 'ptoDepartmentPositions', 'FK1', 1, 2, NULL, '', '');

INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Market Fixed'
  AND b.position = 'Director';   
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Body Shop'
  AND b.position in ('Estimator (A)','Estimator (B)','Estimator (C)','Porter',
    'Admin','Paint Team Leader','Paint Technician (A)','Paint Technician (B)',
    'Paint Technician (C)','Metal Team Leader (A)','Metal Technician (A)',
    'Metal Technician (B)','PDR Technician','Parts','Detailer',
    'Staging Technician (C)','Production Manager','Manager'); 
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Appearance'
  AND b.position in ('Director');  
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)      
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Detail'
  AND b.position in ('Detail Technician');   
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Detail'
  AND b.position in ('Assistant Manager');  
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)  
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Detail'
  AND b.position in ('Cashier', 'Support');  
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)  
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Car Wash'
  AND b.position in ('Cashier', 'Maintenance Manager', 'Team Member');  
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Guest Relations'
  AND b.position in ('Manager','Custodians/Maintenance','Reception');    
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Market'
  AND b.position in ('Building Maintenance');
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY1 Service'
  AND b.position in ('Director'); 
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)     
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Market PDQ'
  AND b.position in ('Manager'); 
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY1 PDQ'
  AND b.position in ('Lead Technician','Upperbay Technician',
  'Lowerbay Technician','Advisor'); 
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY1 Main Shop'
  AND b.position in ('Dispatcher','Lead Technician','Technician (A)',
  'Technician (B)','Technician (C)','Technician (D)','PT Technician (C)',
  'Accessory Technician');  
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY1 Drive'
  AND b.position in ('Manager', 'Advisor'); 
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'BDC'
  AND b.position in ('Operator', 'Team Leader');  
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Rental'
  AND b.position in ('Team Leader', 'Agent', 'Courtesy Driver');  
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Office'
  AND b.position in ('Controller', 'Office Manager', 'Warranty Admin',
  'Accounts Receivable/Payroll','Accounts Payable','Title Clerk',
  'Warranty Admin - HGF','File Clerk','F&I Admin','Project Coordinator',
  'Accounting Admin');     
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Parts'
  AND b.position in ('Manager','Inventory Manager',
  'Outside Sales Consultant','Driver','Shipping & Receiving',
  'Aftermarket Sales Consultant','Shipping Coordinator');     
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Parts'
  AND b.position in ('Sales Consultant') AND b.payType = 'H'; 
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'IT'
  AND b.position in ('Manager','IT Specialist');    
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'Market'
  AND b.position in ('Executive Manager');       
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)   
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY1 Sales'
  AND b.position in ('General Sales Manager','Digital Coordinator',
  'New Car Sales Manager','Team Leader','New Car Sales Manager','Finance Manager',
  'New Car Technology Specialist','Used Car Inventory Manager',
  'Used Car Buyer','Porter','New Car Inventory Manager','Driver','Sales Support');      
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY1 Sales'
  AND b.position in ('Sales Consultant', 'Sales Consultant (PT)') 
  AND b.payType = 'C'; 
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY2 Sales'
  AND b.position in ('General Manager', 'Sales Manager', 'Driver','Team Leader',
  'Sales Consultant (PT)','Reception');  
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY2 Sales'
  AND b.position in ('Sales Consultant')
  AND b.payType = 'C';    
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY2 PDQ'
  AND b.position in ('Assistant Manager','Advisor','Lead Technician',
  'Upperbay Technician','Lowerbay Technician');
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY2 Main Shop'
  AND b.position in ('Manager','Advisor','Technician (A)','Technician (B)',
  'Technician (C)','Technician (D)','Courtesy Driver','Detailer');  
INSERT INTO ptoDepartmentPositions (departmentKey, positionKey, fromDate)    
SELECT a.departmentKey, b.positionKey, '08/01/2014'
FROM ptoDepartments a, ptoPositions b
WHERE a.Description = 'RY2 Main Shop'
  AND b.position in ('Manager','Advisor','Technician (A)','Technician (B)',
  'Technician (C)','Technician (D)','Courtesy Driver','Detailer');    
/*
CREATE TRIGGER TechTrainingOther
   ON stgTechTrainingOther
   BEFORE 
   INSERT 
BEGIN 

--verify that any data entry into stgTechTrainingOther is for a currently valid/active tech

declare @count integer;
DECLARE @tech cichar(3);
DECLARE @store cichar(3);
DECLARE @date date;
@date = (SELECT thedate FROM __new);
@store = (SELECT storecode FROM __new);
@tech = (SELECT technumber FROM __new);
@count = (
SELECT count(*)
FROM dimtech
  WHERE technumber = @tech
    AND storecode = @store
    AND active = true
    AND @date BETWEEN techkeyfromdate AND techkeythrudate);
if @COUNT = 0 then
  raise ERROR(100, 'OMFG');
end if;
*/

-- 8/21 simplified version
DROP TABLE newPtoDepartments;
CREATE TABLE newPtoDepartments (
  department cichar(30) constraint NOT NULL,
  constraint pk primary key (department)) IN database;
INSERT INTO newPtoDepartments
SELECT trim(description) FROM ptoDepartments;  

CREATE TABLE newPtoPositions (
  position cichar(30) constraint NOT NULL,
  constraint pk primary key (position)) IN database;
INSERT INTO newPtoPositions
SELECT DISTINCT trim(position) FROM ptoPositions;  

DROP TABLE newPtoDepartmentPositions;
CREATE TABLE newPtoDepartmentPositions (
  department cichar(30) constraint NOT NULL,
  position cichar(30) constraint NOT NULL,
  constraint pk primary key (department,position)) IN database;
INSERT INTO newPtoDepartmentPositions
SELECT trim(b.description), trim(c.position)
FROM ptoDepartmentPositions a
INNER JOIN ptoDepartments b on a.departmentKey = b.departmentKey
INNER JOIN ptoPositions c on a.positionKey = c.positionKey;   

DROP TABLE ptoDepartments;
DROP TABLE ptoPositions;
DROP TABLE ptoDepartmentPositions;
EXECUTE PROCEDURE sp_RenameDDObject('newptoDepartments','ptoDepartments', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject('newptoPositions','ptoPositions', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject('newptoDepartmentPositions','ptoDepartmentPositions', 1, 0);

EXECUTE PROCEDURE sp_CreateIndex90( 'ptoDepartmentPositions', 'ptoDepartmentPositions.adi', 'FK1', 'department', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoDepartmentPositions', 'ptoDepartmentPositions.adi', 'FK2', 'position', '', 2, 1024, '' );
  
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoDepartments', 'ptoDepartments.adi', 'PK', 'department', '', 2051, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoPositions', 'ptoPositions.adi', 'PK', 'position', '', 2051, 1024, '' );

   
-- UPDATE: Cascade  DELETE: Restrict  
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoDepartments-ptoDepartmentPositions', 'ptoDepartments', 'ptoDepartmentPositions', 'FK1', 1, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoPositions-ptoDepartmentPositions', 'ptoPositions', 'ptoDepartmentPositions', 'FK2', 1, 2, NULL, '', '');

CREATE TABLE ptoAuthorization (
  authByDepartment cichar(30) constraint NOT NULL,
  authByPosition cichar(30) constraint NOT NULL,
  authForDepartment cichar(30) constraint NOT NULL,
  authForPosition cichar(30) constraint NOT NULL,
  constraint pk primary key(authByDepartment,authByPosition,authForDepartment,authForPosition)) IN database;

EXECUTE PROCEDURE sp_CreateIndex90( 'ptoAuthorization', 'ptoAuthorization.adi', 'FK1', 'authByDepartment;authByPosition', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoAuthorization', 'ptoAuthorization.adi', 'FK2', 'authForDepartment;authForPosition', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoAuthorization', 'ptoAuthorization.adi', 'authByLevel', 'authByLevel', '', 2, 1024, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 'ptoAuthorization', 'ptoAuthorization.adi', 'authForLevel', 'authForLevel', '', 2, 1024, '' );
-- UPDATE: Cascade  DELETE: Restrict  
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoDeptPos-ptoAuth1', 'ptoDepartmentPositions', 'ptoAuthorization', 'FK1', 1, 2, NULL, '', '');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ('ptoDeptPos-ptoAuth2', 'ptoDepartmentPositions', 'ptoAuthorization', 'FK2', 1, 2, NULL, '', '');
  
    
-- missing trent
INSERT INTO ptoDepartments values('RY1');
INSERT INTO ptoDepartmentPositions values ('RY1','General Manager'); 
-- missing ben foster
INSERT INTO ptoDepartments values('HR');
INSERT INTO ptoDepartmentPositions values ('HR','Director');
-- missing bs staging tech
INSERT INTO ptoPositions values('Staging Technician');
INSERT INTO ptoDepartmentPositions values ('Body Shop','Staging Technician');
-- missing metal team leader (b)
INSERT INTO ptoPositions values('Metal Team Leader (B)');
INSERT INTO ptoDepartmentPositions values ('Body Shop','Metal Team Leader (B)');
-- missing detail team leader
INSERT INTO ptoDepartmentPositions values ('Detail','Team Leader');
-- missing car wash manager
INSERT INTO ptoDepartmentPositions values ('Car Wash','Manager');

-- trent
INSERT INTO ptoAuthorization values('RY1','General Manager','RY1 Sales','General Sales Manager');
INSERT INTO ptoAuthorization values('RY1','General Manager','HR','Director');
INSERT INTO ptoAuthorization values('RY1','General Manager','Office','Controller');
INSERT INTO ptoAuthorization values('RY1','General Manager','Guest Relations','Manager');
INSERT INTO ptoAuthorization values('RY1','General Manager','RY1 Sales','Used Car Inventory Manager');

-- ben
INSERT INTO ptoAuthorization values('Market Fixed','Director','Body Shop','Manager');
INSERT INTO ptoAuthorization values('Market Fixed','Director','RY1 Service','Director');
INSERT INTO ptoAuthorization values('Market Fixed','Director','Market','Building Maintenance');
INSERT INTO ptoAuthorization values('Market Fixed','Director','Parts','Manager');
INSERT INTO ptoAuthorization values('Market Fixed','Director','Market PDQ','Manager');
INSERT INTO ptoAuthorization values('Market Fixed','Director','RY2 Main Shop','Manager');
INSERT INTO ptoAuthorization values('Market Fixed','Director','Appearance','Director');
-- andrew
INSERT INTO ptoAuthorization values('RY1 Service','Director','RY1 Main Shop','Dispatcher');
INSERT INTO ptoAuthorization values('RY1 Service','Director','RY1 Main Shop','Lead Technician');
INSERT INTO ptoAuthorization values('RY1 Service','Director','RY1 Drive','Manager');
-- randy, mark
INSERT INTO ptoAuthorization values('Body Shop','Manager','Body Shop','Production Manager');
INSERT INTO ptoAuthorization values('Body Shop','Manager','Body Shop','Estimator (A)');
INSERT INTO ptoAuthorization values('Body Shop','Manager','Body Shop','Estimator (B)');
INSERT INTO ptoAuthorization values('Body Shop','Manager','Body Shop','Estimator (C)');
INSERT INTO ptoAuthorization values('Body Shop','Manager','Body Shop','Porter');
INSERT INTO ptoAuthorization values('Body Shop','Manager','Body Shop','Admin');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Paint Team Leader');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Paint Technician (A)');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Paint Technician (B)');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Paint Technician (C)');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Metal Team Leader (A)');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Metal Technician (A)');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Metal Technician (B)');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','PDR Technician');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Parts');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Detailer');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Staging Technician');
INSERT INTO ptoAuthorization values('Body Shop','Production Manager','Body Shop','Metal Team Leader (B)');

INSERT INTO ptoAuthorization values('Appearance','Director','Detail','Team Leader');
INSERT INTO ptoAuthorization values('Appearance','Director','Detail','Assistant Manager');
INSERT INTO ptoAuthorization values('Appearance','Director','Car Wash','Manager');
INSERT INTO ptoAuthorization values('Detail','Team Leader','Detail','Detail Technician');
INSERT INTO ptoAuthorization values('Detail','Assistant Manager','Detail','Cashier');
INSERT INTO ptoAuthorization values('Detail','Assistant Manager','Detail','Support');
INSERT INTO ptoAuthorization values('Car Wash','Manager','Car Wash','Maintenance Manager');
INSERT INTO ptoAuthorization values('Car Wash','Manager','Car Wash','Cashier');
INSERT INTO ptoAuthorization values('Car Wash','Manager','Car Wash','Team Member');
INSERT INTO ptoAuthorization values('Guest Relations','Manager','Guest Relations','Custodians/Maintenance');
INSERT INTO ptoAuthorization values('Guest Relations','Manager','Guest Relations','Reception');

INSERT INTO ptoAuthorization values('Market PDQ','Manager','RY1 PDQ','Advisor');
INSERT INTO ptoAuthorization values('Market PDQ','Manager','RY1 PDQ','Lead Technician');
INSERT INTO ptoAuthorization values('Market PDQ','Manager','RY1 PDQ','Upperbay Technician');
INSERT INTO ptoAuthorization values('Market PDQ','Manager','RY1 PDQ','Lowerbay Technician');
INSERT INTO ptoAuthorization values('RY1 Main Shop','Dispatcher','RY1 Main Shop','Technician (A)');
INSERT INTO ptoAuthorization values('RY1 Main Shop','Dispatcher','RY1 Main Shop','Technician (B)');
INSERT INTO ptoAuthorization values('RY1 Main Shop','Dispatcher','RY1 Main Shop','Technician (C)');
INSERT INTO ptoAuthorization values('RY1 Main Shop','Dispatcher','RY1 Main Shop','Technician (D)');
INSERT INTO ptoAuthorization values('RY1 Main Shop','Dispatcher','RY1 Main Shop','PT Technician (C)');
INSERT INTO ptoAuthorization values('RY1 Main Shop','Dispatcher','RY1 Main Shop','Accessory Technician');

INSERT INTO ptoAuthorization values('RY1 Drive','Manager','RY1 Drive','Advisor');
INSERT INTO ptoAuthorization values('RY1 Drive','Manager','BDC','Team Leader');
INSERT INTO ptoAuthorization values('RY1 Drive','Manager','Rental','Team Leader');
INSERT INTO ptoAuthorization values('Rental','Team Leader','Rental','Agent');
INSERT INTO ptoAuthorization values('Rental','Team Leader','Rental','Courtesy Driver');
INSERT INTO ptoAuthorization values('BDC','Team Leader','BDC','Operator');

INSERT INTO ptoAuthorization values('Office','Controller','Office','Office Manager');
INSERT INTO ptoAuthorization values('Office','Office Manager','Office','Warranty Admin');
INSERT INTO ptoAuthorization values('Office','Office Manager','Office','Accounts Receivable/Payroll');
INSERT INTO ptoAuthorization values('Office','Office Manager','Office','Accounts Payable');
INSERT INTO ptoAuthorization values('Office','Office Manager','Office','Title Clerk');
INSERT INTO ptoAuthorization values('Office','Office Manager','Office','Warranty Admin - HGF');
INSERT INTO ptoAuthorization values('Office','Office Manager','Office','File Clerk');
INSERT INTO ptoAuthorization values('Office','Office Manager','Office','F&I Admin');
INSERT INTO ptoAuthorization values('Office','Office Manager','Office','Project Coordinator');
INSERT INTO ptoAuthorization values('Office','Office Manager','Office','Accounting Admin');
INSERT INTO ptoAuthorization values('Parts','Manager','Parts','Inventory Manager');
INSERT INTO ptoAuthorization values('Parts','Manager','Parts','Sales Consultant');
INSERT INTO ptoAuthorization values('Parts','Manager','Parts','Outside Sales Consultant');
INSERT INTO ptoAuthorization values('Parts','Manager','Parts','Driver');
INSERT INTO ptoAuthorization values('Parts','Manager','Parts','Shipping & Receiving');
INSERT INTO ptoAuthorization values('Parts','Manager','Parts','Aftermarket Sales Consultant');
INSERT INTO ptoAuthorization values('Parts','Manager','Parts','Shipping Coordinator');
INSERT INTO ptoAuthorization values('Market','Executive Manager','IT','Manager');
INSERT INTO ptoAuthorization values('IT','Manager','IT','IT Specialist');

INSERT INTO ptoAuthorization values('RY1 Sales','General Sales Manager','RY1 Sales','Digital Coordinator');
INSERT INTO ptoAuthorization values('RY1 Sales','General Sales Manager','RY1 Sales','New Car Sales Manager');
INSERT INTO ptoAuthorization values('RY1 Sales','General Sales Manager','RY1 Sales','Team Leader');
INSERT INTO ptoAuthorization values('RY1 Sales','General Sales Manager','RY1 Sales','Sales Consultant');
INSERT INTO ptoAuthorization values('RY1 Sales','General Sales Manager','RY1 Sales','Finance Manager');
INSERT INTO ptoAuthorization values('RY1 Sales','General Sales Manager','RY1 Sales','New Car Technology Specialist');
INSERT INTO ptoAuthorization values('RY1 Sales','General Sales Manager','RY1 Sales','New Car Inventory Manager');
INSERT INTO ptoAuthorization values('RY1 Sales','Digital Coordinator','RY1 Sales','Sales Support');
INSERT INTO ptoAuthorization values('RY1 Sales','New Car Inventory Manager','RY1 Sales','Driver');
INSERT INTO ptoAuthorization values('RY1 Sales','Used Car Inventory Manager','RY1 Sales','Used Car Buyer');
INSERT INTO ptoAuthorization values('RY1 Sales','Used Car Inventory Manager','RY1 Sales','Porter');

INSERT INTO ptoAuthorization values('RY2 Sales','General Manager','RY2 Sales','Sales Manager');
INSERT INTO ptoAuthorization values('RY2 Sales','General Manager','RY2 Sales','Team Leader');
INSERT INTO ptoAuthorization values('RY2 Sales','General Manager','RY2 Sales','Reception');
INSERT INTO ptoAuthorization values('RY2 Sales','Sales Manager','RY2 Sales','Driver');
INSERT INTO ptoAuthorization values('RY2 Sales','Team Leader','RY2 Sales','Sales Consultant');
INSERT INTO ptoAuthorization values('RY2 Sales','Team Leader','RY2 Sales','Sales Consultant (PT)');

INSERT INTO ptoAuthorization values('Market PDQ','Manager','RY2 PDQ','Assistant Manager');
INSERT INTO ptoAuthorization values('RY2 PDQ','Assistant Manager','RY2 PDQ','Advisor');
INSERT INTO ptoAuthorization values('RY2 PDQ','Assistant Manager','RY2 PDQ','Lead Technician');
INSERT INTO ptoAuthorization values('RY2 PDQ','Assistant Manager','RY2 PDQ','Lowerbay Technician');
INSERT INTO ptoAuthorization values('RY2 PDQ','Assistant Manager','RY2 PDQ','Upperbay Technician');

INSERT INTO ptoAuthorization values('RY2 Main Shop','Manager','RY2 Main Shop','Technician (A)');
INSERT INTO ptoAuthorization values('RY2 Main Shop','Manager','RY2 Main Shop','Technician (B)');
INSERT INTO ptoAuthorization values('RY2 Main Shop','Manager','RY2 Main Shop','Technician (C)');
INSERT INTO ptoAuthorization values('RY2 Main Shop','Manager','RY2 Main Shop','Technician (D)');
INSERT INTO ptoAuthorization values('RY2 Main Shop','Manager','RY2 Main Shop','Advisor');
INSERT INTO ptoAuthorization values('RY2 Main Shop','Manager','RY2 Main Shop','Courtesy Driver');
INSERT INTO ptoAuthorization values('RY2 Main Shop','Manager','RY2 Main Shop','Detailer');


-- 8/22 
changes resulting FROM first meeting with ben foster
-- kathy lind
UPDATE ptoDepartments
SET department = 'Guest Experience'
WHERE department = 'Guest Relations'

SELECT * FROM ptoAuthorization WHERE authForDepartment = 'rental'
-- rental team leader auth BY ry1 service director, NOT ry1 serv driv mgr
update ptoAuthorization
SET authByDepartment = 'RY1 Service',
    authByPosition = 'Director'
WHERE authForDepartment = 'rental'
  AND authForPosition = 'team leader';  
-- courtesy driver auth BY ry1 drive mgr NOT rental team leader  
 UPDATE ptoAuthorization
 SET authByDepartment = 'RY1 Drive',
     authByPosition = 'Manager'
 WHERE authForDepartment = 'rental'
  AND authForPosition = 'courtesy driver';
-- courtesy driver pos of drive NOT rental
UPDATE ptoDepartmentPositions
SET department = 'RY1 Drive'
WHERE department = 'rental'
  AND position = 'courtesy driver'; 

-- no one IS auth BY Bev, techs ALL auth BY craig
need to ADD a position of Lead Technician (A)
INSERT INTO ptoPositions values ('Lead Technician (A)');
need to UPDATE deptPos RY1 Main Shopt LEad Technician to lead tech A
UPDATE ptoDepartmentPositions
SET position = 'Lead Technician (A)'
WHERE department = 'RY1 Main Shop'
  AND position = 'Lead Technician';
SELECT * FROM ptoAuthorization WHERE authByPosition = 'dispatcher'
UPDATE ptoAuthorization
SET authByPosition = 'Lead Technician (A)'
WHERE authByPosition = 'dispatcher';

-- make building maint a dept: 2 positions: manager(ray) auth BY ben, support(scott, jay)
DELETE FROM ptoAuthorization WHERE authForPosition = 'building maintenance';
DELETE FROM ptoDepartmentPositions WHERE position = 'building maintenance';
DELETE FROM ptoPositions WHERE position = 'building maintenance';

INSERT INTO ptoDepartments values('Building Maintenance');
INSERT INTO ptoDepartmentPositions values('Building Maintenance','Manager');
INSERT INTO ptoDepartmentPositions values('Building Maintenance','Support');
INSERT INTO ptoAuthorization values('Market Fixed','Director','Building Maintenance','Manager');
INSERT INTO ptoAuthorization values('Building Maintenance','Manager','Building Maintenance','Support');

-- pdq
-- remove market pdq, have ned AS ry1 pdq, report to andrew
-- adam remains ass mgr but reports to joel
INSERT INTO ptoDepartmentPositions values('RY1 PDQ', 'Manager');
UPDATE ptoAuthorization
SET authByDepartment = 'RY1 PDQ'
WHERE authByDepartment = 'Market PDQ'
  AND authForDepartment = 'RY1 PDQ'; 
UPDATE ptoAuthorization
SET authByDepartment = 'RY2 Main Shop'
WHERE authByDepartment = 'Market PDQ'
  AND authForDepartment = 'RY2 PDQ'; 
DELETE FROM ptoAuthorization WHERE authForDepartment = 'Market PDQ';   
DELETE FROM ptoDepartmentPositions WHERE department = 'Market PDQ';
DELETE FROM ptoDepartments WHERE department = 'Market PDQ';
INSERT INTO ptoAuthorization values('Market Fixed','Director','RY1 PDQ','Manager');

SELECT distinct authByDepartment, authForDepartment
FROM ptoAuthorization

SELECT DISTINCT authByDepartment
FROM ptoAuthorization
-- depts that don't DO any authorizing
SELECT *
FROM ptoDepartments a
WHERE NOT EXISTS (
  SELECT 1
  FROM ptoAuthorization
  WHERE authBydepartment = a.department)


-- excel to visio drawing2
SELECT TRIM(authForDepartment) + ' : ' + TRIM(authForPosition) AS Name,
  TRIM(authByDepartment) + ' : ' + TRIM(authByPosition) AS "Reports To" 
FROM ptoAuthorization
ORDER BY authbydepartment, authbyposition


-- 8/24 ADD board of directors position, with no authBy rows for Board of Directors IN ptoAuthorizations
INSERT INTO ptoPositions values ('Board of Directors');
INSERT INTO ptoDepartmentPositions values('Market','Board of Directors');
INSERT INTO ptoAuthorization values ('Market','Board of Directors','Market','Executive Manager');
INSERT INTO ptoAuthorization values ('Market','Board of Directors','RY2 Sales','General Manager');
INSERT INTO ptoAuthorization values ('Market','Board of Directors','RY1','General Manager');
INSERT INTO ptoAuthorization values ('Market','Board of Directors','Market Fixed','Director');

-- excel to visio hierarchy only
SELECT TRIM(authForDepartment) + ' : ' + TRIM(authForPosition) AS Name,
  TRIM(authByDepartment) + ' : ' + TRIM(authByPosition) AS "Reports To" 
FROM ptoAuthorization
UNION 
SELECT 'Market : Board of Directors', cast(null as sql_char) FROM system.iota



-- non leaf nodes
SELECT TRIM(authForDepartment) + ' : ' + TRIM(authForPosition) AS Name,
  TRIM(authByDepartment) + ' : ' + TRIM(authByPosition) AS "Reports To" 
FROM ptoAuthorization a
WHERE NOT EXISTS (
  SELECT 1
  FROM ptoAuthorization
  WHERE authForDepartment = a.authByDepartment
    AND authForPosition = a.authByPosition) 
ORDER BY authbydepartment, authbyposition


-- 8/25
changes FROM Ben F:
1. Office Project Coordinator (sarah) auth by to ben, NOT june
dept should be market NOT office
this IS a leaf node
DELETE 
FROM ptoAuthorization
WHERE authForPosition = 'project coordinator';
DELETE 
FROM ptoDepartmentPositions
WHERE position = 'project coordinator';
INSERT INTO ptoDepartmentPositions 
values('Market','Project Coordinator');
INSERT INTO ptoAuthorization
values ('Market Fixed','Director','Market','Project Coordinator');

2.new positions 
bs: Metal Technician (C)
INSERT INTO ptoPositions values ('Metal Technician (C)');
INSERT INTO ptoDepartmentPositions 
values ('Body Shop','Metal Technician (C)');
INSERT INTO ptoAuthorization 
values('Body Shop','Production Manager','Body Shop','Metal Technician (C)');

HR: intern
INSERT INTO ptoPositions values ('Intern');
INSERT INTO ptoDepartmentPositions values ('HR','Intern');
INSERT INTO ptoAuthorization
values('HR','Director','HR','Intern');

-- 10/23/2014
/*
ALTER TABLE ptoDepartmentPositions
ADD COLUMN ptoPolicyHtml cichar(60);
*/
select *
FROM ptoDepartmentPositions
WHERE department LIKE '%det%'

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'bdc_policy_html'
WHERE department LIKE '%bdc%';

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'guest_experience_policy_html'
WHERE department LIKE '%guest%';

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'honda_pdq_advisors_policy_html'
WHERE department = 'ry2 pdq'
  AND position = 'advisor';
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'honda_pdq_techs_policy_html'
WHERE department = 'ry2 pdq'
  AND position LIKE '%tech%';
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'parts_policy_html'
WHERE department = 'parts';  

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'collision_center_policy_html'
WHERE department = 'body shop'; 

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_pdq_advisor_policy_html'
WHERE department = 'ry1 pdq'
  AND position = 'advisor';
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_pdq_tech_policy_html'
WHERE department = 'ry1 pdq'
  AND position LIKE '%tech%';  
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'car_wash_policy_html'
WHERE department = 'car wash';  

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_service_advisor_policy_html'
WHERE department = 'ry1 drive'
  AND position = 'advisor'; 
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_service_rental_policy_html'
WHERE department = 'rental';  

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'rydell_service_tech_policy_html'
WHERE department = 'ry1 main shop'
  AND position LIKE '%tech%'; 
  
UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'detail_policy_html'
WHERE department = 'detail';   

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'office_policy_html'
FROM ptoDepartmentPositions
WHERE department = 'office';

UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'fi_admin_policy_html'
FROM ptoDepartmentPositions
WHERE department = 'office'
  AND position = 'F&I Admin';


UPDATE ptoDepartmentPositions
SET ptoPolicyHtml = 'no_policy_html'
WHERE ptoPolicyHtml IS NULL; 
