/*
Hi Jon-

Could you please add the below people into Vision?

Dylan Deziel
Parts Driver
2/21/2019 anniversary
46 Hours

Joshua Northagen
Main Shop Tech
3/12/2019 anniversary 
56 Hours

Thanks!


Laura Roth 
08/03/20
*/
-- dylan IS a CASE of rehire, never reinstated INTO pto
-- his pto anniv changes AND his allocation TABLE has to be reset
-- per laura, his allocation TABLE starts at 72 hours IN his first year
SELECT employeekey,employeenumber, active,fullparttime,distcode,name,
  hiredate,termdate, employeekeyfromdate, employeekeythrudate, currentrow
FROM dds.edwEmployeeDim WHERE lastname = 'deziel'
SELECT * FROM ptoemployees WHERE employeenumber = '154225'
select * FROM pto_employee_pto_allocation WHERE employeenumber = '154225'
SELECT * FROM pto_used WHERE employeenumber = '154225'
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '154225'

select * FROM ptopositionfulfillment WHERE department = 'ry1 parts'

INSERT INTO ptopositionfulfillment values ('RY1 Parts','Driver','154225')

UPDATE ptoemployees
SET ptoanniversary = '02/21/2019',
    active = true
WHERE employeenumber = '154225';

DELETE 
FROM pto_employee_pto_allocation
WHERE employeenumber = '154225';

    INSERT INTO pto_employee_pto_allocation (employeeNumber, fromdate, thrudate, hours)
    SELECT c.employeenumber, 
      cast(timestampadd(sql_tsi_year, c.n, c.ptoAnniversary) as sql_date) AS fromDate,
      CASE c.n
        WHEN 20 THEN cast('12/31/9999' AS sql_date)
        ELSE cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, c.n + 1, c.ptoAnniversary)) AS sql_date) 
      END AS thruDate,
      ptoHours  
    FROM (
      SELECT a.employeenumber, a.ptoAnniversary, b.*
      FROM ptoEmployees a,
        (
          SELECT n
          FROM dds.tally
          WHERE n BETWEEN 1 AND 20) b  
      WHERE a.employeenumber = '154225') c   
    LEFT JOIN pto_categories d on c.n BETWEEN d.fromYearsTenure AND d.thruYearsTenure
	

-- looks LIKE joshua IS just another CASE of disappearing position_fulfillment	
SELECT employeekey,employeenumber, active,fullparttime,distcode,name,
  hiredate,termdate, employeekeyfromdate, employeekeythrudate, currentrow
FROM dds.edwEmployeeDim WHERE lastname = 'northagen'
SELECT * FROM ptoemployees WHERE employeenumber = '168753'
select * FROM pto_employee_pto_allocation WHERE employeenumber = '168753'	
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '168753'
SELECT * FROM pto_used WHERE employeenumber = '168753'
SELECT * FROM ptopositionfulfillment ORDER BY department, position

INSERT INTO ptopositionfulfillment values ('RY1 Main Shop','Technician (B)','168753')














/*
Could you please add Ana into Vision with the PTO anniversary of 8/1/2019?

why 8/1/2019?

Sorry for the confusion

Ana first quit in March 2018 and then 5/27/18 was when she came back and went to work full time in the BDC.  At some point 
while she was in the BDC, she went back to part time and was in school but it looks like they never sent in a payroll form to
change her back to part time.  So then on 6/23/2019 we moved her back to the office but still as only part time because she was
nannying for Jeri until August 2019.  She went to full time in the office in August 2019.
*/

SELECT * FROM dds.edwEmployeeDim WHERE lastname = 'gagnon' AND firstname = 'anastasia'
ORDER BY employeekey  -- 155214

SELECT * FROM ptoemployees WHERE employeenumber = '155214'
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '155214'
SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '155214'

SELECT * FROM ptodepartmentpositions ORDER BY department,position

INSERT INTO ptopositionfulfillment values('Office','Accounting Admin','155214');

SELECT employeenumber,
  timestampadd(sql_tsi_day, 66, fromdate) AS fromdate,
  timestampadd(sql_tsi_day, 66, thrudate) AS thrudate,
  hours
-- INTO #wtf  
FROM (  
SELECT employeenumber,
  timestampadd(sql_tsi_year, 1, fromdate) AS fromdate,
  timestampadd(sql_tsi_year, 1, thrudate) AS thrudate,
  hours
FROM pto_employee_pto_allocation WHERE employeenumber = '155214') x
 
DELETE FROM pto_employee_pto_allocation WHERE employeenumber = '155214';

INSERT INTO pto_employee_pto_allocation
SELECT employeenumber, CAST(fromdate AS sql_date), CAST(thrudate AS sql_date), hours FROM #wtf;

08/7/20 this was mostly wrong, did NOT synchronize the new pto anniversary

UPDATE ptoemployees
SET ptoanniversary = '08/01/2019'
-- select * FROM ptoemployees 
WHERE employeenumber = '155214' 

SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '155214' ORDER BY fromdate

/*
Hey Jon-

Could you also please add Jamie Claseman into Vision under RY1 Receptionist?  Her PTO date is 12/3/2018 and she should have 56 hours left at this point.

Thanks!

Laura Roth 
05/14/20
*/
SELECT * FROM dds.edwEmployeeDim WHERE lastname = 'claseman'
-- 165980
SELECT * FROM ptoemployees WHERE employeenumber = '165980'
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '165980'
SELECT * FROM ptodepartmentpositions where position like '%rec%' ORDER BY department,position
SELECT * FROM ptopositionfulfillment WHERE department = 'guest experience'
INSERT INTO ptopositionfulfillment values('Guest Experience','Receptionist','165980');



Hi Jon,
Dean Braaten was just in my office and wanted to discuss his PTO. That�s when I noticed He is not on my pto screen. Can we get him switched so I am his admin? 
Thanks,

Romao Maresca 
03/25/90
*/

SELECT * FROM ptoemployees WHERE employeenumber = '118010'
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '118010'
SELECT * FROM ptoDepartmentPositions WHERE department = 'ry1 parts'
INSERT INTO ptopositionfulfillment values ('RY1 Parts','Driver','118010');


--02/09/20
Hi Jon-

For some reason Rolando didn�t pop into Vision with PTO after he hit a year.  Could you add him in there please with 64 hours?  PTO anniversary is 12/17/2018.

Thanks!

Laura Roth 
SELECT *
FROM ptoemployees
WHERE employeenumber = '195123'

SELECT * FROM ptopositionfulfillment WHERE employeenumber = '195123'

SELECT * FROM ptoDepartmentPositions WHERE department = 'ry1 parts'

INSERT INTO ptopositionfulfillment values ('RY1 Parts','Sales Consultant','195123');



1. DO they exist IN ptemployees
SELECT *  
FROM ptoemployees
WHERE username LIKE '%schnei%'

2. does the employeenumber exist IN ptopositionfulfillment

SELECT * FROM tpemployees WHERE lastname = 'roth'


SELECT * FROM ptopositionfulfillment WHERE employeenumber = '1566882'

SELECT * FROM ptodepartmentpositions WHERE position LIKE '%rece%'

SELECT * FROM ptodepartmentpositions WHERE department = 'detail'

SELECT * FROM ptopositionfulfillment WHERE position = 'Receptionist'

INSERT INTO ptopositionfulfillment values ('Detail','Detail Technician','1566882')

-- 12/23/19
-- Tara Mcmillian does not pop up in my vision page for PTO.  Can you please add her.

-- Thank you
-- John Gardner

-- 9/16/19
-- Hi Jon-
SELECT * FROM tpemployees WHERE lastname = 'gardner'

SELECT * FROM ptopositionfulfillment WHERE employeenumber = '198542'

INSERT INTO ptopositionfulfillment values('Body Shop','Estimator','198542')

-- Could you please add Taner Smith into Vision with the PTO anniversary date of 9/2/2018?  He should have 72 hours.

-- Thanks!
SELECT *  
FROM ptoemployees
WHERE username LIKE '%smith%'

SELECT * FROM tpemployees WHERE employeenumber = '176430'

SELECT * FROM ptopositionfulfillment WHERE employeenumber = '176430'

SELECT * FROM ptodepartmentpositions WHERE department = 'detail'

INSERT INTO ptopositionfulfillment values('Detail','Support Specialist','176430')



--8/16/19
--Hi Jon-
--For some reason the PTO for the Call Center dropped out of Vision.  
--Could you please update the PTO admin for the General and 
--Service Operators to Justin Kilmer?
--Thanks!
--Laura Roth 

select * FROM tpemployees WHERE lastname = 'kilmer'
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '178079'

select *
FROM ptoauthorization
WHERE authbydepartment = 'Market'  
  AND authbyposition = 'Business Development Representative'
  
select * FROM ptodepartmentpositions WHERE department = 'market'  
SELECT * FROM ptodepartments ORDER BY department

select *
FROM ptoauthorization
WHERE authfordepartment = 'call center'  

select * FROM ptopositionfulfillment WHERE department = 'ry1 drive' AND position = 'manager'

select * FROM tpemployees WHERE employeenumber = '1102198' -- nick neumann,(ry1 drive manager)
-- AND his admin page shows no operators

-- IN compli: service/general operators
SELECT a.userid, a.firstname, a.lastname, a.supervisorname, a.title
FROM pto_compli_users a
WHERE a.title LIKE '%operator%'
  AND a.status = '1'

SELECT * FROM pto_compli_users WHERE lastname = 'kilmer'
-- aha, justin IS Call Center / Call Center Team Leader

SELECT * FROM ptopositions ORDER BY position  Team Leader EXISTS AS a position
SELECT * FROM ptodepartmentpositions WHERE department = 'call center'

-- so change him IN posful
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '178079'  Market / Business Development Representative

this fixes justins pto admin page
UPDATE ptopositionfulfillment
SET department = 'Call Center',
    position = 'Team Leader'
WHERE employeenumber = '178079'

select * FROM ptopositionfulfillment WHERE employeenumber IN ('111254','149878','175369')

select * FROM ptoauthorization WHERE authbydepartment = 'call center'


dont forget justins manager, currently nick neumann, should be morgan
SELECT * 
FROM ptoauthorization
WHERE authfordepartment = 'call center'
  AND authforposition = 'team leader'
  
select * FROM tpemployees WHERE firstname = 'morgan'
select * FROM ptopositionfulfillment WHERE employeenumber = '185600'

UPDATE ptoauthorization
SET authbydepartment = 'Market',
    authbyposition = 'Marketing Manager'
WHERE authfordepartment = 'call center'
  AND authforposition = 'team leader'

--8/6/19
--Hi Jon-
--I noticed Caden Aker wasn�t in Vision when I went to change his department to PDQ.  
--Is that because he has used all of his vacation so far this year?  
--If not, could you please add him in there under PDQ Tech with the start date of 11/06/17?
--Thanks!

SELECT *  
FROM ptoemployees
WHERE username LIKE '%caker%'

SELECT * FROM ptopositionfulfillment WHERE employeenumber = '10568'

SELECT * FROM ptodepartmentpositions WHERE department = 'ry1 pdq'

SELECT * FROM ptopositionfulfillment WHERE employeenumber = '10568'

INSERT INTO ptopositionfulfillment values('RY1 PDQ','Upperbay Technician','10568')
/*
7/1/19
Hi Jon-

Could you please do me a favor and add Valerie Rauner into Vision?  Her PTO anniversary is 6/19/17 so she should have 112 hours in there.

Thanks!
*/

SELECT *  -- 1588879
FROM ptoemployees
WHERE username LIKE '%rauner%'

SELECT * FROM ptopositionfulfillment WHERE employeenumber = '1588879'
arkona: OFF/SA Sales Admin
dist_code: 14

SELECT employeenumber, name FROM dds.edwEmployeeDim 
WHERE distcode = '14'
  AND storecode = 'RY1'
  AND active = 'active'
  AND currentrow
 
-- none of these are probably a match  
select * FROM ptopositionfulfillment a
INNER JOIN (
SELECT  employeenumber, name
FROM dds.edwEmployeeDim 
WHERE distcode = '14'
  AND storecode = 'RY1'
  AND active = 'active'
  AND currentrow) b on a.employeenumber = b.employeenumber
  
 select * FROM pto_compli_users WHERE userid = '1588879' -- F&I Admin
 
 SELECT * FROM ptodepartmentpositions ORDER BY department, position
 
INSERT INTO ptopositionfulfillment values ('Office','F&I Admin', '1588879')

/*
7/8/19
Good Morning Jon  on the Rydell vision Page I can look employees up for 
their pto but now   I cannot find Tom Marcotte or Al  Vien on the site 
any more.  I think they were on the vision page?
*/

SELECT *  -- 1145660
FROM ptoemployees
WHERE username LIKE '%vien%'

select * FROM pto_compli_users WHERE userid = '1145660' -- Hobby Shop Technician

INSERT INTO ptopositionfulfillment values('Hobby Shop','Technician','1145660')

ok, he shows up IN the search, but his detail page IS blank

SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '1145660'

select *
FROM ptoauthorization
WHERE authfordepartment = 'hobby shop'  

select * FROM ptopositionfulfillment WHERE department = 'hobby shop'

looks LIKE doug peterson IS mission FROM ptopositionfulfillment

SELECT * FROM ptoemployees WHERE username LIKE '%peterson%'  -- 1110542

INSERT INTO ptopositionfulfillment values('Hobby Shop', 'Manager', '1110542')

yep, that fixed it

SELECT *  -- 190918
FROM ptoemployees
WHERE username LIKE '%marco%'

INSERT INTO ptopositionfulfillment values('Hobby Shop','Technician','190918')

/*
Al has a question on his anniversary date in vision it is 6/24/12  but in payroll it is 07/27/07?  

I think it should be 7-27-07 
*/

SELECT *  -- 1145660
FROM ptoemployees
WHERE username LIKE '%vien%'

select * FROM tpemployees WHERE employeenumber = '1145660'


SELECT DISTINCT anniversarytype FROM ptoemployees

UPDATE ptoemployees
SET ptoanniversary = '07/27/2007', anniversarytype = 'Original'
WHERE employeenumber = '1145660'

SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '1145660'


7/27/2018 7/26/2019 152

07/27/2027 12/31/9999 192

delete FROM pto_employee_pto_allocation WHERE employeenumber = '1145660'


INSERT INTO pto_employee_pto_allocation (employeeNumber, fromdate, thrudate, hours)
    SELECT c.employeenumber, 
      cast(timestampadd(sql_tsi_year, c.n, c.ptoAnniversary) as sql_date) AS fromDate,
      CASE c.n
        WHEN 20 THEN cast('12/31/9999' AS sql_date)
        ELSE cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, c.n + 1, c.ptoAnniversary)) AS sql_date) 
      END AS thruDate,
      ptoHours  
    FROM (
      SELECT a.employeenumber, a.ptoAnniversary, b.*
      FROM ptoEmployees a,
        (
          SELECT n
          FROM dds.tally
          WHERE n BETWEEN 0 AND 20) b  
      WHERE a.employeenumber = '1145660') c   
    LEFT JOIN pto_categories d on c.n BETWEEN d.fromYearsTenure AND d.thruYearsTenure; 

/*  
8/27/19 
HI Jon-

Could you add Dan Shoults back into Vision?  Looks like he fell out for some reason.  
His PTO anniversary is 5/8/17.  He should only have 64 hours left.

Thanks!

Laura Roth 
*/

select * FROM tpemployees WHERE lastname = 'shoults'
SELECT * FROM ptopositionfulfillment WHERE employeenumber = '115896'
IN dealertrack he IS a courtesy driver

select * FROM pto_compli_users WHERE lastname = 'shoults'
Courtesy Driver : Nick Neumann

SELECT * FROM ptodepartmentpositions ORDER BY position

select *
FROM ptoauthorization
WHERE authfordepartment = 'ry1 drive'
  AND authforposition = 'courtesy driver'
 
-- AND yes, nick neumann IS the authorizer  
select * FROM ptopositionfulfillment WHERE department = 'ry1 drive' AND position = 'manager'  
select * FROM tpemployees WHERE employeenumber = '1102198'
  
INSERT INTO ptopositionfulfillment values('RY1 Drive','Courtesy Driver','115896')


/*
Hi Jon-

Could you also please add Dave back into Vision?  He should have 4 hours of vacation left.

Thanks!

Laura Roth 
*/

156844
select * FROM ptodepartments
SELECT * FROM ptodepartmentpositions WHERE department = 'building maintenance'

SELECT * FROM ptopositionfulfillment WHERE department = 'building maintenance'

INSERT INTO ptopositionfulfillment values ('Building Maintenance','Technician','156844')