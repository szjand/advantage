
-- get base values needed , THEN generate calcs
DECLARE @rollout date;
@rollout = '11/01/2014';
SELECT e.*,
  timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 AS multiplier,
  CASE 
    WHEN pto_category = 0 THEN 0
    ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
      SELECT pto_hours
      FROM pto_categories
      WHERE pto_category = e.pto_category), 0)
  END AS pto_earned_at_rollout_hours,
  CASE 
    WHEN pto_category = 0 THEN 0
    ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
      SELECT pto_hours/8
      FROM pto_categories
      WHERE pto_category = e.pto_category), 0)
  END AS pto_earned_at_rollout_days  
FROM (
  SELECT a.*, b.pto_category, aa.firstname, aa.lastname, 
    b.from_date AS cat_from_date, b.thru_date as cat_thru_date, d.ptoConsumed,
    CASE 
      WHEN year(pto_anniversary) = 2014 THEN cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
      WHEN year(pto_anniversary) = 2013 THEN cast(createTimestamp(2014, month(pto_anniversary), dayofmonth(pto_anniversary), 0,0,0,0) AS sql_date)
      ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
    END AS pto2014start,
    cast(createTimestamp(2015, month(pto_anniversary), dayofmonth(pto_anniversary), 0,0,0,0) AS sql_date) AS pto2015start
  FROM ptoEmployees a
  LEFT JOIN dds.edwEmployeeDim aa on a.employeenumber = aa.employeenumber
    AND aa.currentrow = true
    AND aa.active = 'active'
  LEFT JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
  LEFT JOIN ( -- consumed betw 1/1/14 AND rollout
    SELECT a.employeenumber, coalesce(SUM(c.vacationhours + c.ptohours), 0) AS ptoConsumed
    FROM ptoEmployees a
    LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
      AND b.currentrow = true
      AND b.active = 'active'
    LEFT JOIN dds.edwClockHoursFact c on b.employeekey = c.employeeKey  
      AND c.vacationhours + c.ptohours > 0
      AND c.datekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN '01/01/2014' AND @rollout)
    GROUP BY a.employeenumber) d on a.employeenumber = d.employeenumber
  WHERE @rollout BETWEEN b.from_date AND b.thru_date) e
ORDER BY pto_anniversary desc