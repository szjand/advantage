/*
6/19/17
Please deduct 8 hours of PTO from both Larry Laughlin and Rudy Robles as they have donated 
their time to the Cares bank.
*/

SELECT * FROM tpemployees WHERE lastname = 'laughlin'  -- '184610'
SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '184610'
1/21/17

INSERT INTO pto_used (employeenumber, thedate, source, hours)
values('184610','01/21/2017','Donate',8);


SELECT * FROM tpemployees WHERE lastname = 'robles'  -- '1117600'
SELECT * FROM pto_employee_pto_allocation WHERE employeenumber = '1117600'
7/12/2016

INSERT INTO pto_used (employeenumber, thedate, source, hours)
values('1117600','07/12/2016','Donate',8);

/*
6/22/18
Hi Jon-

Larry donated 8 hours of his PTO to the Rydell Cares fund.  
Kim already took care of it on her end from a payroll standpoint, 
but could you please subtract 8 hours from his current balance in Vision for us?

Thanks!
*/

SELECT a.lastname, a.firstname, a.employeenumber, 
  b.fromdate, b.thrudate, b.hours, SUM(c.hours) AS hours_used
FROM tpemployees a
LEFT JOIN pto_employee_pto_allocation b on a.employeenumber = b.employeenumber
  AND curdate() BETWEEN b.fromdate AND b.thrudate
LEFT JOIN pto_used c on thedate BETWEEN b.fromdate AND b.thrudate
  AND a.employeenumber = c.employeenumber
WHERE a.lastname = 'stadstad'
GROUP BY a.lastname, a.firstname, a.employeenumber, 
  b.fromdate, b.thrudate, b.hours;
  
INSERT INTO pto_used (employeenumber, thedate, source, hours)
values('1130690',curdate() - 1,'Donate',8);  


Hi Jon-

Rudy is donating a vacation day to the Rydell cares program.  Kim already took care of it on our end, but would you be able to subtract 8 hours from his balance on the vision page?

Thanks!


Laura Roth 


SELECT * FROM dds.edwEmployeeDim WHERE lastname = 'robles'

INSERT INTO pto_used (employeenumber, thedate, source, hours)
values('1117600',curdate() - 1,'Donate',8);  

SELECT *
FROM pto_used
WHERE employeenumber = '1117600'
AND thedate > curdate() - 180