SELECT *
FROM dds.edwEmployeeDim 
WHERE employeenumber IN (
  SELECT employeenumber
  FROM dds.edwEmployeeDim 
  WHERE employeekeyfromdate > '10/31/2014')
OR termdate BETWEEN '11/01/2014' AND curdate()  
ORDER BY employeenumber, employeekey

SELECT *
FROM pto_requests
WHERE employeenumber = '181520'

SELECT *
FROM pto_used
WHERE employeenumber = '181520'

SELECT *
FROM pto_employee_pto_allocation
WHERE employeenumber = '181520'



SELECT *
FROM pto_employee_pto_allocation a
LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
WHERE a.employeenumber = '181520'

-- first row IN allocation
SELECT a.*, b.name, b.distcode, fullparttime, d.ptoAnniversary, 
  timestampdiff(sql_tsi_year, d.ptoAnniversary, a.thruDate) -1 AS tenureForFromThru, 
  e.*
FROM pto_employee_pto_allocation a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
LEFT JOIN pto_exclude c on a.employeenumber = c.employeenumber
LEFT JOIN ptoEmployees d on a.employeenumber = d.employeenumber
LEFT JOIN pto_categories e on timestampdiff(sql_tsi_year, d.ptoAnniversary, a.thruDate) -1
  BETWEEN e.fromYearsTenure AND e.thruYearsTenure
WHERE curdate() BETWEEN a.fromdate AND a.thrudate 
  AND c.employeenumber IS NULL 
  AND a.employeenumber = '2118050'
ORDER BY ptoAnniversary DESC   



SELECT *
FROM (
  SELECT a.*, b.name, b.distcode, fullparttime, d.ptoAnniversary, 
    timestampdiff(sql_tsi_year, d.ptoAnniversary, a.thruDate) -1 AS tenureForFromThru, 
    e.*
  FROM pto_employee_pto_allocation a
  INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
    AND b.active = 'active'
  LEFT JOIN pto_exclude c on a.employeenumber = c.employeenumber
  LEFT JOIN ptoEmployees d on a.employeenumber = d.employeenumber
  LEFT JOIN pto_categories e on timestampdiff(sql_tsi_year, d.ptoAnniversary, a.thruDate) -1
    BETWEEN e.fromYearsTenure AND e.thruYearsTenure
  WHERE curdate() BETWEEN a.fromdate AND a.thrudate 
    AND c.employeenumber IS NULL 
    AND a.employeenumber = '2118050') m
LEFT JOIN (    
  select *
  FROM dds.tally t
  WHERE t.n < 21) n on 1 = 1
  
  
-- this IS close-ish but IS USING period fromDate instead of ptoAnniv date  
SELECT a.employeenumber, 
  cast(timestampadd(sql_tsi_year, b.n, a.fromDate) AS sql_date) AS fromDate,
  CASE 
    WHEN b.n = 20 THEN cast('12/31/9999' AS sql_date)
    ELSE cast(timestampadd(sql_tsi_year, b.n, a.thruDate) AS sql_date) 
  END AS thruDate, 
  b.n, c.*
FROM pto_employee_pto_allocation a
INNER JOIN dds.tally b on 1 = 1
  AND b.n BETWEEN 1 AND 20
LEFT JOIN pto_categories c on b.n BETWEEN c.fromYearsTenure AND c.thruYearsTenure
WHERE a.employeenumber = '2118050'
  AND curdate() BETWEEN a.fromdate AND a.thruDate
  
  
  
SELECT employeenumber, COUNT(*)
FROM pto_employee_pto_allocation
GROUP BY employeenumber
ORDER BY COUNT(*)
  
-- this IS close-ish but IS USING period fromDate instead of ptoAnniv date 
-- so modify to use anniv date 
-- IF i get this to WORK, THEN UNION the first period FROM pto_allocation with this
-- shit doesn't WORK for oldtimes, need a sense of tenure the primary table
SELECT aa.ptoAnniversary, a.employeenumber, 
  cast(createtimestamp(year(aa.ptoAnniversary) + n, month(aa.ptoanniversary), 
    dayofmonth(aa.ptoAnniversary),0,0,0,0) AS sql_date) AS fromDate,
  CASE 
    WHEN b.n = 20 THEN cast('12/31/9999' AS sql_date)
    ELSE cast(timestampadd(sql_tsi_year, b.n, a.thruDate) AS sql_date) 
  END AS thruDate, 
  b.n AS tenure, c.*
FROM pto_employee_pto_allocation a
INNER JOIN ptoEmployees aa on a.employeenumber = aa.employeenumber
INNER JOIN dds.tally b on 1 = 1
  AND b.n BETWEEN 1 AND 20
LEFT JOIN pto_categories c on b.n BETWEEN c.fromYearsTenure AND c.thruYearsTenure
WHERE a.employeenumber = '170150'
  AND curdate() BETWEEN a.fromdate AND a.thruDate  
  
-- this IS close-ish but IS USING period fromDate instead of ptoAnniv date 
-- so modify to use anniv date 
-- IF i get this to WORK, THEN UNION the first period FROM pto_allocation with this
-- shit doesn't WORK for oldtimes, need a sense of the current tenure in the primary TABLE

-- base row of current period with tenure
-- this seems better, at least the correct number of rows based on tenure
SELECT a.*, b.ptoAnniversary, c.name,
  timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1 AS currentTenure,
  d.*
FROM pto_employee_pto_allocation a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
LEFT JOIN dds.tally d on d.n between timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1 AND 20
WHERE curdate() BETWEEN a.fromDate AND a.thruDate 
ORDER BY b.ptoAnniversary

-- current period AND tenure
SELECT a.*, b.ptoAnniversary, c.name,
  timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1 AS currentTenure
INTO #current  
FROM pto_employee_pto_allocation a
INNER JOIN ptoEmployees b on a.employeenumber = b.employeenumber
INNER JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  AND c.currentrow = true
WHERE curdate() BETWEEN a.fromDate AND a.thruDate 
ORDER BY b.ptoAnniversary
-- getting CLOSE
-- but this will NOT DO it ALL,
-- eg old timers, need 2 rows, one for the current period with the adjusted hours
-- AND a second row for the normal 192 hours
-- 11/8, hmm, that second row EXISTS IN pto_employee_pto_allocation
SELECT *
FROM #current a
LEFT JOIN dds.tally b on b.n BETWEEN a.currentTenure AND 20

-- ok, let's just get the current period
SELECT *
FROM #current
ORDER BY thrudate

SELECT *
FROM #current a
LEFT JOIN (
  SELECT employeenumber, COUNT(*)
  FROM pto_employee_pto_allocation
  GROUP BY employeenumber) b on a.employeenumber = b.employeenumber
ORDER BY b.expr  

-- exclude currentrow
-- this looks pretty fucking good for newbie, 20 rows '148275'
-- looks good for an old timer AS well, 1 row '1109822'
-- it IS fucked up for Bear, though '111210'
-- hmm, the problem IS what i use for the basis of periodFromDate
SELECT x.*,
  CASE x.n
    WHEN 20 THEN cast('12/31/9999' AS sql_date)
    ELSE CAST(timestampadd(sql_tsi_day, -1, 
      (timestampadd(sql_tsi_year, 1, periodFromDate))) AS sql_date) 
  END AS periodThruDate
FROM (
  SELECT a.*, b.ptoAnniversary, 
    timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1 AS tenure,
    c.*, d.*, 
    CASE n
      WHEN 1 THEN a.fromDate
      WHEN 20 THEN a.fromDate
      ELSE cast(timestampadd(sql_tsi_year, n, b.ptoAnniversary) AS sql_date)
    END AS periodFromDate  
  -- SELECT *      
  FROM pto_employee_pto_allocation a
  LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
  LEFT JOIN pto_categories c on timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1
    BETWEEN c.fromYearsTenure AND c.thruYearsTenure
  LEFT JOIN dds.tally d on d.n BETWEEN c.fromYearsTenure AND c.thruYearsTenure 
    AND d.n BETWEEN 1 AND 20 
  LEFT JOIN #current z on a.employeenumber = z.employeenumber
    AND a.fromDate = z.fromDate  
  WHERE z.employeenumber IS NULL  
    AND a.employeenumber = '111210') x 
ORDER BY x.ptoAnniversary, x.employeenumber


-- exclude currentrow
-- this looks pretty fucking good for newbie, 20 rows '1109822'
-- looks good for an old timer AS well, 1 row '148275'
-- it IS fucked up for Bear, though '111210'
-- hmm, the problem IS what i use for the basis of periodFromDate
-- actually filtering out irrelevant records seems to work
SELECT *
INTO #omg
FROM (
  SELECT x.*,
    CASE x.n
      WHEN 20 THEN cast('12/31/9999' AS sql_date)
      ELSE CAST(timestampadd(sql_tsi_day, -1, 
        (timestampadd(sql_tsi_year, 1, periodFromDate))) AS sql_date) 
    END AS periodThruDate
  FROM (
    SELECT a.*, b.ptoAnniversary, 
      timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1 AS tenure,
      c.*, d.*, 
      CASE n
        WHEN 1 THEN a.fromDate
        WHEN 20 THEN a.fromDate
        ELSE cast(timestampadd(sql_tsi_year, n, b.ptoAnniversary) AS sql_date)
      END AS periodFromDate  
    -- SELECT *      
    FROM pto_employee_pto_allocation a
    LEFT JOIN ptoEmployees b on a.employeenumber = b.employeenumber
    LEFT JOIN pto_categories c on timestampdiff(sql_tsi_year, b.ptoAnniversary, a.thrudate) - 1
      BETWEEN c.fromYearsTenure AND c.thruYearsTenure
    LEFT JOIN dds.tally d on d.n BETWEEN c.fromYearsTenure AND c.thruYearsTenure 
      AND d.n BETWEEN 1 AND 20 
    LEFT JOIN #current z on a.employeenumber = z.employeenumber
      AND a.fromDate = z.fromDate  
    WHERE z.employeenumber IS NULL) x) y  
--      AND a.employeenumber = '111210') x) y
WHERE periodFromDate >= fromDate         

SELECT *
FROM (
  SELECT employeenumber, fromdate, thrudate, hours
  FROM #current  
  union
  select employeenumber, periodFromDate, periodThruDate, hours
  FROM #omg) x
WHERE employeenumber = '111210'  

this IS feeling really fucking good
to generate the dates for current period, 
DO curdate BETWEEN from/thru, IF thru = 9999 THEN generate appropriate values


SELECT *
FROM pto_employee_pto_allocation a
LEFT JOIN (
  SELECT employeenumber, fromdate, thrudate, hours
  FROM #current  
  union
  select employeenumber, periodFromDate, periodThruDate, hours
  FROM #omg)  b on a.employeenumber = b.employeenumber
  AND a.fromdate = b.fromdate
WHERE a.fromDate = (
  SELECT MIN(fromDate)
  FROM pto_employee_pto_allocation
  WHERE employeenumber = a.employeenumber)
AND a.thrudate <> b.thrudate  
  
  
DELETE FROM pto_employee_pto_allocation;
INSERT INTO pto_employee_pto_allocation
SELECT employeenumber, fromdate, thrudate, hours
FROM #current  
union
select employeenumber, periodFromDate, periodThruDate, hours
FROM #omg;

----------------------------------------------------------------------------------
-- pto_adjustments
-- lindsey
INSERT INTO pto_adjustments (employeeNumber,fromDate,hours,reason)
SELECT employeenumber, fromdate, 50, 'Hiring Stip'
FROM pto_employee_pto_allocation
WHERE curdate() BETWEEN fromDate AND thruDate
  AND employeenumber = '17410';  
-- kim & dawn
INSERT INTO pto_adjustments (employeeNumber,fromDate,hours,reason)
SELECT employeenumber, fromdate, 40, 'Rollover'
FROM pto_employee_pto_allocation
WHERE curdate() BETWEEN fromDate AND thruDate
  AND employeenumber IN ('196341','163096');  

---------------------------------------------------------------------------------
-- change ptoAnniversary
-- jared duckstad, '136170' 9/30/02 -> '09/29/1999', 'Other'
-- jay olson, '1106399', 2/11/02 -> '08/19/1994', 'Original'
-- michael schwann, '1124436' , 2/27/12 -> '05/16/2005' 'Original'
  
  /*
DROP TABLE #current;
DROP TABLE #omg;
*/
DECLARE @rollout date;
DECLARE @employeeNumber string;
DECLARE @newAnniversary date;
DECLARE @anniversaryType string;
@rollout = '11/02/2014';   
@employeenumber = '1124436';
@newAnniversary = '05/16/2005';
@anniversaryType = 'Original';
/**/
BEGIN TRANSACTION;
TRY 
UPDATE ptoEmployees
SET ptoAnniversary = @newAnniversary,
    anniversaryType = @anniversaryType
WHERE employeeNumber = @employeeNumber;
DELETE FROM pto_employee_pto_allocation
WHERE employeeNumber = @employeeNumber;

SELECT c.employeenumber, pto2014start AS ptofrom,
  CASE 
     WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) > curdate()
       THEN cast(createTimestamp(2014, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) -1
     ELSE pto2015start - 1   
  END AS ptothru,
  CASE 
    WHEN ptocategory = 0 THEN 0
    ELSE round(timestampdiff(sql_tsi_day, pto2014start, pto2015start)/365.0 * (
      SELECT ptohours
      FROM pto_categories
      WHERE ptocategory = c.ptocategory), 0)
  END AS hours
INTO #current  
FROM (                                                                        
  SELECT a.*, b.ptocategory,
    CASE 
      WHEN year(ptoanniversary) = 2013 and cast(timestampadd(sql_tsi_year, 1, ptoanniversary) AS sql_date) < curdate()
        THEN cast(createTimestamp(2014, month(ptoanniversary), 
          dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date)
      ELSE cast(createTimestamp(2014, 1, 1, 0,0,0,0) AS sql_date) 
    END AS pto2014start,      
    cast(createTimestamp(2015, month(ptoanniversary), 
      dayofmonth(ptoanniversary), 0,0,0,0) AS sql_date) AS pto2015start 
  FROM ptoEmployees a
  INNER JOIN pto_employee_pto_category_dates b on a.employeenumber = b.employeenumber
  WHERE @rollout BETWEEN b.fromdate AND b.thrudate AND a.employeenumber = @employeenumber) c;
 
/**/
SELECT c.employeenumber, fromDate, thruDate, 
  case year(thruDate)
    when 9999 then 192
    else ptoHours
   END AS ptoHours
INTO #omg   
FROM (
  SELECT a.*, b.n,
    cast(timestampadd(sql_tsi_year, n, ptoAnniversary) AS sql_date) AS fromDate,
    CASE b.n
      WHEN 20 THEN CAST('12/31/9999' AS sql_date)
      ELSE cast(timestampadd(sql_tsi_day, - 1, timestampadd(sql_tsi_year, n + 1, 
        ptoAnniversary)) AS sql_date) 
    END AS thruDate
  FROM ptoEmployees a
  LEFT JOIN dds.tally b on 1 = 1
    AND b.n BETWEEN 0 AND 20
  WHERE a.employeenumber = @employeeNumber) c
LEFT JOIN pto_categories d on timestampdiff(sql_tsi_year, c.ptoAnniversary, c.thrudate) - 1
  BETWEEN d.fromYearsTenure AND d.thruYearsTenure 
LEFT JOIN #current z on z.ptoThru = c.thruDate 
WHERE thruDate > curdate()
  AND z.employeeNumber IS NULL;

IF (
  SELECT COUNT(*)
  FROM #omg) = 1 THEN
    INSERT INTO pto_employee_pto_allocation  
    SELECT *
    FROM #current;
    INSERT INTO pto_employee_pto_allocation
    SELECT employeeNumber, ptoThru + 1, '12/31/9999', 192
    FROM #current;
ELSE      
  INSERT INTO pto_employee_pto_allocation  
  SELECT *
  FROM #current
  UNION 
  SELECT * 
  FROM #omg;  
ENDIF;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  