--DROP PROCEDURE zManagerTest;
ALTER PROCEDURE zManagerTest (
      employeeNumber CICHAR ( 9 ),
      employeeNumber cichar(9) output,
      name CICHAR ( 52 ) OUTPUT,
      department CICHAR ( 30 ) OUTPUT,
      title CICHAR ( 30 ) OUTPUT,
      ptoAnniversary DATE OUTPUT,
      ptoExpiringWeeks integer output,
      ptoHoursEarned DOUBLE ( 15 ) OUTPUT,
      ptoHoursRequested integer output,
      ptoHoursApproved integer output,
      ptoHoursUsed DOUBLE ( 15 ) OUTPUT,
      ptoHoursRemaining DOUBLE ( 15 ) OUTPUT)
      
BEGIN  
/*
CREATE TABLE pto_tmp_manager_employees (
      employeeNumber cichar(9),
      name CICHAR ( 52 ),
      department CICHAR ( 30 ),
      title CICHAR ( 30 ),
      ptoAnniversary DATE,
      ptoExpiringWeeks integer,
      ptoHoursEarned DOUBLE ( 15 ),
      ptoHoursRequested integer,
      ptoHoursApproved integer,
      ptoHoursUsed DOUBLE ( 15 ),
      ptoHoursRemaining DOUBLE) IN database;
EXECUTE PROCEDURE zManagerTest('157995');
*/
DECLARE @employeeNumber string;

DECLARE @employeesCursor CURSOR AS 
  SELECT d.employeeNumber
  FROM ptoAuthorization a
  INNER JOIN ptoPositionFulfillment b on a.authByDepartment = b.department
    AND a.authByPosition = b.position
  INNER JOIN ptoPositionFulfillment c on a.authForDepartment = c.department
    AND a.authForPosition = c.position  
  INNER JOIN dds.edwEmployeeDim d on c.employeenumber = d.employeenumber
    AND d.currentrow = true 
    AND d.active = 'active' 
    AND d.fullparttime = 'full'
  WHERE b.employeenumber = @employeenumber;
@employeeNumber = (select employeenumber from __input); --'157995';
DELETE FROM pto_tmp_manager_employees;
OPEN @employeesCursor;
TRY
  WHILE FETCH @employeesCursor DO
    INSERT INTO pto_tmp_manager_employees
    SELECT employeenumber, name, department, position, ptoAnniversary, 
      timestampdiff(sql_tsi_week, curdate(), ptoThruDate) AS ptoExpiringWeeks,
      ptoHoursEarned, ptoHoursRequested, ptoHoursApproved, ptoHoursUsed,
      ptoHoursRemaining
    FROM (EXECUTE PROCEDURE pto_get_employee_pto(@employeesCursor.employeeNumber)) a;
  END WHILE;
FINALLY
  CLOSE @employeesCursor;
END TRY;    
INSERT INTO __output
SELECT top 200 *
FROM pto_tmp_manager_employees
ORDER BY ptoExpiringWeeks;
END;