
/*
Hi Jon-

Could you look and see what Leslie had for PTO before we termed her?

Thanks!

Laura Roth 
08/13/20
*/
SELECT * FROM ptoemployees where username LIKE '%champa%'
124101
select * FROM pto_employee_pto_allocation WHERE employeenumber = '124101' ORDER BY fromdate
06/27/2020 -> 06/26/2020 112 hrs
SELECT SUM(hours) FROM pto_used WHERE employeenumber = '124101' 
  AND thedate BETWEEN '06/27/2020' AND '06/26/2021'
/*
Can you please let me know how much unused PTO JT Pfaff had please, he is a technician and will be paid out his unused PTO.

Thanks
John Gardner
04/13/20
*/

SELECT * FROM ptoemployees where username LIKE '%pfaf%'
167934
select * FROM pto_employee_pto_allocation WHERE employeenumber = '167934' ORDER BY fromdate
4/11/19 -> 4/10/20 72 hrs
SELECT SUM(hours) FROM pto_used WHERE employeenumber = '167934' 
  AND thedate BETWEEN '04/11/2019' AND '04/10/2020'
20 hours used  

/*
Hi Jon-

Could you please check and see what Brad had for vacation before his anniversary date of 4/11?  Could you take whatever he had and add it to his current balance?

Thanks!

Laura Roth 
04/13/20
*/

SELECT * FROM ptoemployees where username LIKE 'bsee%'
1124875

select * FROM pto_employee_pto_allocation WHERE employeenumber = '1124875' ORDER BY fromdate
192 hours 4/11/19 - 4/10/20

SELECT SUM(hours) FROM pto_used WHERE employeenumber = '1124875' -- 80 hours used balance 32 hours
  AND thedate BETWEEN '04/11/2019' AND '04/10/2020'
162 of 192 hours used

UPDATE pto_employee_pto_allocation SET thrudate = '04/10/2016' WHERE employeenumber = '1124875' AND thrudate > curdate();
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2017','04/10/2018',192);
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2018','04/10/2019',192);
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2019','04/10/2020',192);
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2020','04/10/2021',192);
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2021','04/10/2022',192);
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2022','04/10/2023',192);
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2023','04/10/2024',192);
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2024','04/10/2025',192);
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2025','04/10/2026',192);
INSERT INTO pto_employee_pto_allocation values('1124875','04/11/2066','04/10/2027',192);
INSERT INTO pto_adjustments values('1124875','04/11/2020',30,'rollover','per Laura''s request')

/*
Hi Jon-

Are you able to see what Lundean had for PTO when he was termed on 3/6/2020?  
The term form did not include PTO hours but Lundean is saying he was told he had some coming.

Thanks,

Laura Roth 

04/10/20
Lundean Tomlin
*/
SELECT * FROM ptoemployees where username LIKE 'ltom%'

select * FROM pto_employee_pto_allocation WHERE employeenumber = '165287' ORDER BY fromdate -- 12/17/19 - 12/16/20: 72 hours

termed 03/06/20

SELECT SUM(hours)
FROM pto_used WHERE employeenumber = '165287' -- 104 hours used balance 48 hours
  AND thedate BETWEEN '12/17/2019' AND '12/16/2020'  

SELECT * FROM dds.edwEmployeeDim WHERE employeenumber = '165287'  

/*
Good Morning Jon, 

My anniversary Date has come and went for another year.  If you could please forward my unused vacation time to Nick Shirek so I can get paid out for it, I would very appreciative.  Thanks again!!  

John Olderbak 
02/10/20
*/
SELECT * FROM ptoemployees where username LIKE 'jold%'

select * FROM pto_employee_pto_allocation WHERE employeenumber = '1106225' ORDER BY fromdate -- 152

SELECT SUM(hours)
FROM pto_used WHERE employeenumber = '1106225' -- 104 hours used balance 48 hours
  AND thedate BETWEEN '02/07/2019' AND '02/06/2020'  


/*
Hey Jon-

Could you check and see what AJ Preston had left for PTO prior to it resetting today?  His last day was on Friday, 1/7.

Thanks!

Laura Roth 

02/10/20
*/

SELECT * FROM ptoemployees where username LIKE 'apres%'

select * FROM pto_employee_pto_allocation WHERE employeenumber = '1112410' ORDER BY fromdate

112 hours

SELECT *
FROM pto_used WHERE employeenumber = '1112410' -- 80 hours used balance 32 hours
  AND thedate BETWEEN '02/10/2019' AND '02/09/2020'  




Jon

Can you tell us how many hours of PTO we have to payout Steve Flaat as he retired on 12/30/2020?

Thanks so much

Nick 
1/30/20

*/

SELECT * FROM ptoemployees where username LIKE 'sflaa%'
select * FROM pto_employee_pto_allocation WHERE employeenumber = '145840' ORDER BY fromdate
02/01/2019 -> 01/31/2020 152 hours
SELECT SUM(hours) FROM pto_used WHERE employeenumber = '145840' -- 80 hours used balance 32 hours
  AND thedate BETWEEN '02/01/2019' AND '01/31/2020'

SELECT *
FROM pto_used WHERE employeenumber = '145840' -- 80 hours used balance 32 hours
  AND thedate BETWEEN '02/01/2019' AND '01/31/2020'  
  
  

select * FROM ptoemployees WHERE username LIKE 'jbark%'
select * FROM pto_employee_pto_allocation WHERE employeenumber = '110052'
select SUM(hours) FROM pto_used WHERE employeenumber = '110052'
and thedate BETWEEN '11/01/2018' AND '10/31/2019'

-- also, payouts for which an email was NOT sent
-- ry2 brandon johnson
select * FROM ptoemployees WHERE username LIKE 'bjohn%'  -- 273315
select * FROM pto_employee_pto_allocation WHERE employeenumber = '273315' -- 152 hrs
select SUM(hours) FROM pto_used WHERE employeenumber = '273315'  -- 104 hours
and thedate BETWEEN '07/20/2018' AND '07/19/2019'

select * FROM pto_used WHERE employeenumber = '273315'  -- 104 hours
and thedate BETWEEN '07/01/2019' AND '07/31/2019'


-- sam foster pto reset, how much was unused
SELECT * FROM ptoemployees where username LIKE 'sfost%'
select * FROM pto_employee_pto_allocation WHERE employeenumber = '148080' ORDER BY fromdate
11/17/18 - 11/17/19 112 hours
SELECT SUM(hours) FROM pto_used WHERE employeenumber = '148080' -- 80 hours used balance 32 hours
  AND thedate BETWEEN '11/17/2018' AND '11/16/2019'

  -- 3/21/20 brian peterson
select * FROM pto_employee_pto_allocation WHERE employeenumber = '1110425' ORDER BY fromdate
  
SELECT SUM(hours) FROM pto_used WHERE employeenumber = '1110425' -- 80 hours used balance 32 hours
AND thedate BETWEEN '03/15/2019' AND '03/14/2020'

192 owed, 172 used, balance 20