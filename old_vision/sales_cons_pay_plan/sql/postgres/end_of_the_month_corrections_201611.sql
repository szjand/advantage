﻿
29607XXA
sold to Mack by strother (KEI, 1132420), capped on 11/09
unwound on 11/09
sold to Koplin by preston (PRE, 1112410) and capped on 11/10

process did not pick it up, currently in deals as the mack deal

detected with the sc_changes_on_deals query

need to: 
  unwind the mack deal: new row in deals with -1 count and unwind note

  so, is it deleted or just un-capped ?
  the only row in bopmast is for koplin, so deleted
  and why did the code not pick it up

i believe this one is unusual because, perhaps, it was capped and deleted on the same day 
anyway

-- insert deleted row in deals
insert into scpp.deals
select year_month, employee_number, stock_number, -1.0, deal_date, customer_name,
  model_year, make, model, deal_source, 2, 'Deleted','Deleted','11/09/2016'
from scpp.deals
where stock_number = '29607XXA';  

select * from scpp.xfm_deals
where run_date = '11/30/2016'
  and primary_sc = 'KVA'
  


-- new row in deals from xfm_deals seq 3
insert into scpp.deals
select  201611, '1112410', stock_number, 1.0, run_date, customer_name, model_year,
  make, model, 'Dealertrack', 3, null::citext, 'Capped', run_date
from scpp.xfm_deals
where stock_number = '29607XXA'
  and seq = 3;



-- what do deals with notes look like
select *
from scpp.deals a
where exists (
  select 1
  from scpp.deals
  where stock_number = a.stock_number
    and notes = 'deleted')
order by stock_number, seq    

-- 28873A: Logan Carter
-- Deal from August 2016, on 11/22/16 shows up as deleted
-- it was not
-- fucking bopmast now has a blank stocknumber for the deal

select *
from scpp.deals
where stock_number = '28873a'

select *
from scpp.xfm_deals
where stock_number = '28873a'

delete from scpp.xfm_deals where stock_number = '28873A' and seq = 3;
delete from scpp.deals where stock_number = '28873A' and seq = 2;

-- 29076: Austen Janzen
Deal capped in October
Reset (11/09) and recapped (11/16) in November
showing as -1 in november

select *
from scpp.xfm_deals
where stock_number = '29076'

select *
from scpp.deals
where stock_number = '29076' order by seq

bad year_month on the 11/16 recapping
was 201610, guessing that is because the deal_date is still 10/26

update scpp.deals
set year_month = 201611
where stock_number = '29076'
  and seq = 3;

update scpp.deals
set deal_date = '10/25/2016'
where stock_number = '29076'
  and seq = 2;


-- 29409
shows as deleted, but it is not
this is one of those fucking goofy stock number erased from bopmast





---------------------------
select *
from scpp.deals a
where year_month = 201611
-- where extract(year from deal_date) * 100 + extract(month from deal_date) <> year_month
  and exists (
    select 1
    from scpp.deals
    where stock_number = a.stock_number
      and deal_status = 'Deleted')
order by stock_number, seq


select *
from scpp.deals a
where year_month = 201611
-- where extract(year from deal_date) * 100 + extract(month from deal_date) <> year_month
  and exists (
    select 1
    from scpp.deals
    where stock_number = a.stock_number
      and unit_count = -1)
order by stock_number, seq


-- deals where most recent status is Deleted
select *
from scpp.deals a
where deal_status = 'Deleted'
  and seq = (
    select max(seq)
    from scpp.deals
    where stock_number = a.stock_number)  

select *
from scpp.deals
where year_month = 201611
  and deal_source <> 'dealertrack'

select *
from scpp.admin_notes
where year_month = 201611

select *
from scpp.sales_consultant_data
where year_month = 201611
  and csi_qualified = true
  and logged_qualified = true
  and auto_alert_qualified = true
  




  


  