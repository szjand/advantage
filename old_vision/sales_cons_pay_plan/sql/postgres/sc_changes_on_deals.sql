﻿-- the most recent row for a deal that has had a sc change
select a.stock_number, a.primary_sc, a.secondary_sc, sc_change
from scpp.xfm_deals a
inner join (
  select stock_number, max(seq) as seq
  from scpp.xfm_deals
  group by stock_number) b on a.stock_number = b.stock_number 
    and a.seq = b.seq
inner join (
  select stock_number
  from scpp.xfm_deals
  where sc_change <> 'no change'
  group by stock_number) c on a.stock_number = c.stock_number  
/*
this looks to be ok
what i want is to detect sc changes
currently aware of 2 scenarios that are not being handled
1. after the deal was capped, sales consultant(s) is changed to another sc(s)
2. after the deal was capped, sales consultant changed to house

try to clean up the output below
*/
select x.stock_number, x.unit_count, x.deal_date, z.full_name, z.ry1_id, z.ry2_id, y.primary_sc, 
  y.secondary_sc, y.sc_change
from scpp.deals x
inner join (  -- the most recent row for a deal that has had a sc change
  select a.stock_number, a.primary_sc, a.secondary_sc, a.sc_change, a.run_date
  from scpp.xfm_deals a
  inner join (
    select stock_number, max(seq) as seq
    from scpp.xfm_deals
    group by stock_number) b on a.stock_number = b.stock_number 
      and a.seq = b.seq
  inner join (
    select stock_number
    from scpp.xfm_deals
    where sc_change <> 'no change'
    group by stock_number) c on a.stock_number = c.stock_number) y on x.stock_number = y.stock_number
left join scpp.sales_consultants z on x.employee_number = z.employee_number   
where x.year_month = 201611
  -- and primary_sc not in (ry1_id, ry2_id)
order by x.stock_number


10/24/16
29410A 
select * from scpp.xfm_deals where stock_number = '29410a'
deal capped on 10/9
on 10/15 pr_sc changed to house

delete from scpp.deals where stock_number = '29410a'

10/26, shit it did not stick, scott still shows 29410a in his list

select * from scpp.xfm_deals where stock_number = '29410a'

select * from scpp.deals where stock_number = '29410a'

select * from scpp.xfm_deals_for_update

it is re-inserted into deals because of the load script, xfm_deals has 3 rows for the deal, 
only seq 3 is correct (primary sc = house), the load script exposes seq 2 as a "new" row:
  1. primary sc is pearson
  2. stock number does not exist in deals
boom inserts seq 2

the fundamental problem, i currently believe, is the premature filtering of input into deals
only deals with a sc configured for the month goes into scpp.deals
as opposed to all capped deals going into scpp.deals and basing subsequent counts, etc on 
the relevant sales consultants
this will also be an issue when we include ry2, and garceau/olderbak

so, how to fix it now
1. limit the load script to only the most recent (largest) seq in xfm_deals  

yuck

select *
from scpp.deals
where stock_number in (select stock_number from scpp.deals where seq <> 1)
order by stock_number, seq

a better solution may be to add a row to scpp.deals, with a -1 unit_count, and a note explaining
sc changed to house

i am liking that better than fucking with the load script which might have unforseen side effects
also it is more in keeping with the general ledger model of nothing gets deleted, just modified

although it will be challenging to automate this particular case:
1. post cap, sales consultant changed to house

for now, will hard code this update:
insert into scpp.deals
select a.year_month, a.employee_number, a.stock_number, -1, /* unit count */
  a.deal_date, a.customer_name, a.model_year, a.make, a.model, a.deal_source,
  2, /* seq */
  b.sc_change, /* notes */
  a.deal_status, b.run_date
from scpp.deals a
left join scpp.xfm_deals b on a.stock_number = b.stock_number
  and b.seq = 3
where a.stock_number = '29410a';


/*
this looks to be ok
what i want is to detect sc changes
currently aware of 2 scenarios that are not being handled
1. after the deal was capped, sales consultant(s) is changed to another sc(s)
2. after the deal was capped, sales consultant changed to house
want better output, show deal:sc1, deal:sc2, xfm_deal:sc1, xfm_deal:sc2

hmm, no way to tell just from deals which is the primary sc and which is the secondary
so, in this case it doesn't matter
*/

select * from scpp.xfm_deals where stock_number = '24874XX'

select * 
from scpp.deals a
where unit_count = .5
order by year_month, stock_number
  

select x.stock_number, x.unit_count, x.deal_date, z.full_name, z.ry1_id, z.ry2_id, y.primary_sc, 
  y.secondary_sc, y.sc_change
from scpp.deals x
inner join (  -- the most recent row for a deal that has had a sc change
  select a.stock_number, a.primary_sc, a.secondary_sc, a.sc_change, a.run_date
  from scpp.xfm_deals a
  inner join (
    select stock_number, max(seq) as seq
    from scpp.xfm_deals
    group by stock_number) b on a.stock_number = b.stock_number 
      and a.seq = b.seq
  inner join (
    select stock_number
    from scpp.xfm_deals
    where sc_change <> 'no change'
    group by stock_number) c on a.stock_number = c.stock_number) y on x.stock_number = y.stock_number
left join scpp.sales_consultants z on x.employee_number = z.employee_number   
where x.year_month = 201611
  -- and primary_sc not in (ry1_id, ry2_id)
order by x.stock_number

select *
from (
  select r.*, 
    case s.secondary_sc
      when 'None' then t.full_name || ':' || 1.0::citext
      else t.full_name || ':' || 0.5::citext || ',' || u.full_name || ':' || 0.5::citext
    end as xfm_deals
  from (
    select a.stock_number, string_agg(b.full_name || ':' || unit_count::citext, ',') as deals
    from scpp.deals a
    inner join scpp.sales_consultants b on a.employee_number = b.employee_number
    where year_month = 201612
    group by a.stock_number) r
  left join scpp.xfm_deals s on r.stock_number = s.stock_number
    and s.seq = (
      select max(seq)
      from scpp.xfm_deals
      where stock_number = s.stock_number)  
  left join scpp.sales_consultants t on 
    case 
      when s.store_code = 'RY1' then s.primary_sc = t.ry1_id
      when s.store_code = 'RY2' then s.primary_sc = t.ry2_id 
    end 
  left join scpp.sales_consultants u on 
    case 
      when s.store_code = 'RY1' then s.secondary_sc = u.ry1_id
      when s.store_code = 'RY2' then s.secondary_sc = u.ry2_id 
    end) z
where deals <> xfm_deals      









