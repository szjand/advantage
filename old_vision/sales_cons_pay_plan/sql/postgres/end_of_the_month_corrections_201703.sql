﻿-- checked and ok (sc_changes_on_deals.sql)
-- 2/21/17 missed 29340
--   unwound 1/13/17
--   capped to new customer 2/3/17
--   why does this not pick it up
--   i believe xfm_deals_for_update miss it because, if that is not run every day
--   which, it really is not, the selection is limited :
--     where a.run_date = (select max(xd.run_date) from scpp.xfm_deals xd) 
--   the run date for 29340 is 2/3/17, so if not run until 2/4, it missed it
--   ok, that is fucked up, but should stop being an issue once i fucking get the pipeline going
why did this script not pick it up?
because the query on deals is limited to current month, and most recent 29340 row in deals is 201701
/*
2/26 and this is even worse, 29805B, already sold and unw in feb by foster, then resold in feb by mulhern
literally stumbled across this one, the unw was already in the Already Verified list.
i would never have caught the mulhern sale
so why the fuck did it not pick up 29805b
it is in xfm_deals
but not in scpp.xfm_deals_for_update
wy dat?

2 possible approaches: make the population of xfm_deals_for_update better
  or make the test better
  
Theory 1: in xfm deals:
  sc_change: PSC from FOS to MUL
  status_change: No Change 
  gl_date: 2/21 which is different than deals.run_date
  gl_date_change = t
  date_capped = 2/21 which is different than deals.run_date


xfm_deals_for_updates filter: 
  x: scpp.xfm_deals where:
    run_date = max(run_date) from xfm_deals, ie most recent run date only
    row_type = update
    seq = max(seq) for the stock_number
    consultant exists in scpp.sales_consultants and is not olderbak or garceau
  y: scpp.deals inner join where: 
    deal_status <> x.record_status or x.status_change = deleted
    seq = max(seq) for the stock_number
WHERE:
  1. NOT((x.record_status = 'Accepted' or x.record_status = 'None') and y.deal_status = 'Deleted')
  2. AND NOT(x.status_change = 'Deleted' and y.deal_status = 'None')
  3. AND x.status_change not in ('No Change', 'None to Accepted', 'Accepted to None')
  4. AND NOT(x.status_change = 'Accepted to Capped' and y.deal_status = 'Capped' and y.deal_date = x.gl_date)
  5. AND NOT(x.status_change = 'Capped to Accepted' and y.deal_status = 'Deleted')

                      X               Y
record/deal status  Capped          Deleted
gl/deal date        2/21/17         2/10/17            
status_change       No Change

#3 is the one that eliminates 29085B: x.status_change = No Change
ok, good enuf for now, just fix it manually

insert into scpp.deals
select 201702, (select employee_number from scpp.sales_consultants where last_name = 'mulhern'),
  stock_number, 1.0, run_date, customer_name, model_year, make, model, 'Dealertrack',
  3, null::citext, 'Capped', run_date
from scpp.xfm_deals
where stock_number = '29805b'
  and seq = 5
  
this situation will have to be addressed in the refactoring for the new pay plan/fact_vehicle_sale

shit, same thing with 29897B, sold and unw by Seay, sold by eden, eden sale does not show up,
oops, not yet, deal not capped yet

and still another one, this one only came up because the consultant brought it up
29889 sold and unw in jan, sold in feb by brett
select * from scpp.xfm_deals where stock_number = '29889'


3/8/17: another unhandled situation: load deals bombs on a split deal unwind: 29910xxa: handled manually

29579A: 
*/   
 
select *
from (
  select r.*, 
    case s.secondary_sc
      when 'None' then t.full_name || ':' || 1.0::citext
      else t.full_name || ':' || 0.5::citext || ',' || u.full_name || ':' || 0.5::citext
    end as xfm_deals
  from ( -- r
    select a.stock_number, string_agg(b.full_name || ':' || unit_count::citext, ',') as deals
    from scpp.deals a
    inner join scpp.sales_consultants b on a.employee_number = b.employee_number
    where year_month = 201703
    group by a.stock_number) r
  left join scpp.xfm_deals s on r.stock_number = s.stock_number
    and s.seq = (
      select max(seq)
      from scpp.xfm_deals
      where stock_number = s.stock_number)  
  left join scpp.sales_consultants t on 
    case 
      when s.store_code = 'RY1' then s.primary_sc = t.ry1_id
      when s.store_code = 'RY2' then s.primary_sc = t.ry2_id 
    end 
  left join scpp.sales_consultants u on 
    case 
      when s.store_code = 'RY1' then s.secondary_sc = u.ry1_id
      when s.store_code = 'RY2' then s.secondary_sc = u.ry2_id 
    end) z
where deals <> xfm_deals   
order by stock_number

Already Verified:
28445: a disappearing stock number
28721B: sold/unw mar Mavity
29501A: sold/unw/sold mar Mulhern
29523R: sold/unw/sold mar Bellmore
29579A: sold/unw jan DOC, sold mar CGA/PEA: shit, have to add chris garceau
29910xxa: sold feb warmcack/croaker unw mar warmack/croaker sold mar carlson **** does not show up for carlson !!!!!!!!!!!!!
30011: sold/unw mar croaker
30014: warmack/pearson ok
30016A: sold/unw mar chavez
30061: missing secondary sc
30154: sold/unw/sold mar stadstad
30157A: sold feb unw mar mulhern sold mar preston not capped yet
30175a: sold/unw mar chavez
30282XX: dockendorf/preston ok
30342XX: sold feb unw mar bellmore 
30349XXA: sold/unw/sold mar bellmore
30471A: sold/unw mar loven sold mar yunker
30688XX: warmack/croaker ok
30745X: sold/unw/sold mar loven
30816: sold/unw mar seay

missing deals (not in scpp.deals)
  29579A sold/unw jan, sold mar to heintz
  29889 sold/unw jan, sold feb to sandy
  29910xxa sold feb/unw mar, sold mar to doyea
  30471A sold/unw mar loven sold mar yunker

-- this one is the persistent stocknumber disappeared from bopmast pain in the ass
-- that until i fix that issue has to be fixed every fucking day

28873A, 28108, 28511, 28445: all have disappeared their stocknumber
as of 3/27/17 these are filtered out in scpp\xfm_deals.py
no longer any need to do the cleanup
select *
from scpp.deals
where stock_number = '30157a'
-- -- 
select *
from scpp.xfm_deals
where stock_number = '30157a'
-- 
-- select *
-- from scpp.deals
-- where stock_number = '28108'
-- 
-- select *
-- from scpp.xfm_deals
-- where stock_number = '28108'
-- 
-- delete from scpp.xfm_deals where stock_number = '28445' and seq = 3;
-- delete from scpp.deals where stock_number = '28445' and seq = 2;
-- delete from scpp.xfm_deals where stock_number = '28873A' and seq = 3;
-- delete from scpp.deals where stock_number = '28873A' and seq = 2;

/*
  order matters
select 'ordered', md5(a::text)
from(
select 'a','b','c','d') a
union
select 'not ordered', md5(a::text)
from(
select 'c','d','a','b') a
*/

select * from scpp.sales_Consultants
select * from scpp.deals where stock_number = '29889'
select * from scpp.xfm_deals where stock_number = '29889'

drop table if exists diff;
create temp table diff as 
select *
from (
  select b.stock_number as deal_stk, md5(b::text) as deal_hash
  from (
    select stock_number, customer_name, model_year, make, model, 
      case
        when unit_count in (1, -1) then employee_number || ',' ||'None'
        else (select string_agg(employee_number, ',') from scpp.deals where stock_number = a.stock_number and seq = a.seq group by stock_number)
      end
    -- select *
    from scpp.deals a
    where run_date > '12/31/2016'
      and seq = (
        select max(seq)
        from scpp.deals
        where stock_number = a.stock_number)) b) e
  inner join (    
  select d.stock_number as xfm_stk, md5(d::text) as xfm_hash
  from (
    select stock_number, customer_name, model_year, make, model, 
      case 
        when b.employee_number < coalesce(c.employee_number, 'None')
          then b.employee_number || ',' || coalesce(c.employee_number, 'None')
        else  coalesce(c.employee_number, 'None') || ',' || b.employee_number
      end
    from scpp.xfm_deals a
    left join scpp.sales_consultants b on a.primary_sc =
      case
        when a.store_code = 'RY1' then b.ry1_id
        else b.ry2_id
      end
    left join scpp.sales_consultants c on a.secondary_sc = 
      case
        when a.store_code = 'RY1' then c.ry1_id
        else c.ry2_id
      end
    where run_date > '12/31/2016'
      and a.seq = (
        select max(seq)
        from scpp.xfm_deals
        where stock_number = a.stock_number)) d) f on e.deal_stk = f.xfm_stk and e.deal_hash <> f.xfm_hash

select * from diff;        

-- side by side

select 'deal' as source, stock_number, run_date, customer_name, model_year, make, model, 
  case
    when unit_count = 1 then employee_number || ',' ||'None'
    else (select string_agg(employee_number, ',' order by employee_number) from scpp.deals where stock_number = a.stock_number and seq = a.seq group by stock_number)
  end, 
  unit_count, notes, deal_status, '' as record_status, '' as sc_change, '' as status_change
-- select *
from scpp.deals a
inner join diff aa on a.stock_number = aa.deal_stk
where seq = (
  select max(seq)
  from scpp.deals
  where stock_number = a.stock_number)
union
select 'xfm', stock_number, run_date, customer_name, model_year, make, model, 
  b.employee_number || ',' || coalesce(c.employee_number, 'None'),
  null::integer, '','', record_status, sc_change, status_change
from scpp.xfm_deals a
inner join diff aa on a.stock_number = aa.deal_stk
left join scpp.sales_consultants b on a.primary_sc =
  case
    when a.store_code = 'RY1' then b.ry1_id
    else b.ry2_id
  end
left join scpp.sales_consultants c on a.secondary_sc = 
  case
    when a.store_code = 'RY1' then c.ry1_id
    else c.ry2_id
  end
where a.seq = (
  select max(seq)
  from scpp.xfm_deals
  where stock_number = a.stock_number)
order by stock_number, source desc


-- need to document the meaning of each of these states
select deal_status, count(*)  from scpp.deals a where seq = (select max(seq) from scpp.deals where stock_number = a.stock_number) group by deal_status

select record_status, status_change, count(*)  from scpp.xfm_deals a where seq = (select max(seq) from scpp.xfm_deals where stock_number = a.stock_number) group by record_status, status_change


