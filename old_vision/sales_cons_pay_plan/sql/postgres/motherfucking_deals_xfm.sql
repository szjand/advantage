﻿
-- 4/5
-- deals

-- 4/7 as i understand bopmast better, looks like this needs to be redone
-- initial scrape _0131
------------------------------------------------------------------------------------------
--------------- Jan synched take 2--------------------------------------------------------
------------------------------------------------------------------------------------------
drop table if exists scpp.xfm_deals;
CREATE TABLE if not exists scpp.xfm_deals
(
  row_type citext not null,
  run_date date NOT NULL,
  store_code citext NOT NULL,
  stock_number citext NOT NULL,
  vin citext NOT NULL,
  customer_name citext NOT NULL,
  primary_sc citext NOT NULL,
  secondary_sc citext not null,
  record_status citext not null,
  date_approved date,
  date_capped date,
  origination_date bigint NOT NULL,
  model_year citext not null,
  make citext not null,
  model citext not null,
  seq integer not null default 1,
  seq_note citext,
  constraint xfm_deals_pk primary key (stock_number,seq));

-- initial seeding of xfm_: the entire initial scrape from 0131
-- select * from scpp.xfm_Deals
-- select * from scpp.ext_deals where record_status <> 'U'
insert into scpp.xfm_deals
select 'New', a.*, 1, 'No Change'
from scpp.ext_deals a;

------------------------------------------------------------------------------------------
--------------- Jan synched take 2--------------------------------------------------------
------------------------------------------------------------------------------------------
select stock_number from scpp.xfm_deals group by stock_number having count(*) > 1
select * from scpp.xfm_deals where run_date <> '01/31/2016'
delete from scpp.xfm_Deals; --where run_date <> '01/31/2016'
delete from scpp.deals;
select * from scpp.deals where deal_date > '01/31/2016'
select * from scpp.xfm_deals where stock_number = '27308XX'
select * from scpp.ext_deals where date_capped > '01/29/2016'

select * from scpp.xfm_deals where stock_number = '27106'
-- ok, xfm is seeded
-- run ext_ for 0201 : new rows
-- 0202 -> new rows, date_capped changed on 2 deals
-- new rows:

insert into scpp.xfm_deals
select 'New', a.*, 1, 'New Row'
from scpp.ext_deals a
WHERE not exists (
    select 1
    from scpp.xfm_deals
    where stock_number = a.stock_number);


-- -- -- date_capped change
-- -- -- unit_count, year_month, sc don't change: type 1 update with a note
-- -- -- rethinking, no type 1 updates, do a new new row, inc seq, add seq_note
-- -- -- update scpp.xfm_deals x
-- -- -- set date_capped = y.date_capped
-- -- insert into scpp.xfm_deals (row_type,run_date,store_code,stock_number,
-- --   vin,customer_name,primary_sc,secondary_sc,record_status,date_approved,
-- --   date_capped,origination_date,model_year,make,model,seq,seq_note)
-- -- select 'Update', a.run_date, a.store_code, a.stock_number, a.vin,
-- --   a.customer_name, a.primary_sc, a.secondary_sc, a.record_status,
-- --   a.date_approved, a.date_capped, a.origination_date, a.model_year,
-- --   a.make, a.model,
-- --   (select max(seq) + 1 from scpp.xfm_deals where stock_number = a.stock_number),
-- --   'Changed date capped'
-- -- from scpp.ext_deals a
-- -- inner join (
-- --   select store_Code, stock_number, vin, customer_name, primary_sc, secondary_sc,
-- --   record_Status, date_approved, date_capped, seq
-- --   from scpp.xfm_deals x
-- --   where seq = (
-- --     select max(seq)
-- --     from scpp.xfm_deals
-- --     where stock_number = x.stock_number)) b on a.stock_number = b.stock_number
-- -- where a.date_capped <> b.date_capped
-- --   and a.primary_sc = b.primary_sc
-- --   and a.secondary_sc = b.secondary_sc
-- --   and extract(year from a.date_capped) = extract(year from b.date_capped)
-- --   and extract(month from a.date_capped) = extract(month from b.date_capped);

-- new deals: any stock# not already in xfm_
select 'New', a.*, 1, 'New Row'
from scpp.ext_deals a
WHERE not exists (
    select 1
    from scpp.xfm_deals
    where stock_number = a.stock_number)
    
-- any change

  
select a.stock_number,
  case when a.primary_sc <> b.primary_sc then 'X' else '-' end as psc,
  case when a.secondary_sc <> b.secondary_sc then 'X' else '-' end as pssc,
  case when a.record_status <> b.record_status then 'X' else '-' end as stats,
  case when a.date_approved <> b.date_approved then 'X' else '-' end as appr,
  case when a.date_capped <> b.date_capped then 'X' else '-' end as capped
from scpp.ext_deals a
inner join (
  select store_Code, stock_number, vin, customer_name, primary_sc, secondary_sc,
  record_Status, date_approved, date_capped, seq
  from scpp.xfm_deals x
  where seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = x.stock_number)) b on a.stock_number = b.stock_number
where (a.primary_sc <> b.primary_sc or a.secondary_sc <> b.secondary_sc or a.vin <> b.vin 
  or a.record_status <> b.record_status
  or a.date_approved <> b.date_approved 
  or a.date_capped <> b.date_capped)  

  -- 4/10 hmmm, a status matrix
possible statuses:
  None
  Accepted
  Capped
  Deleted
possible status changes:  
  none -> Accepted
  none -> capped
  none -> capped
  none -> deleted
  accepted -> none
  accepted -> capped
  accepted -> deleted
  capped -> none
  capped -> accepted
  capped -> none

-- hmmm keep it simple, 2 separate queries one for ext_ not null (inner), one for null (outer) this one 
-- specifically to pick up deleted deals
-- 2 attribues: s_c change, status change
-- do i care if dates change if there is no change in status
-- so, for a capped deal, will the cap date ever change without the status changing, betting on no 
-- my head hurts when i peak ahead to how this will all decode in load
alter table scpp.xfm_deals
rename column seq_note to sc_change
alter table scpp.xfm_deals
add column status_change citext default 'No Change';

-- sc or status change
select 'Update', a.run_date, a.store_code, a.stock_number, a.vin, 
  a.customer_name, a.primary_sc, a.secondary_sc, a.record_status,
  a.date_approved, a.date_Capped, a.origination_date, a.model_year,
  a.make, a.model,
  (select max(seq) + 1 from scpp.xfm_deals where stock_number = a.stock_number),
  case
    when a.primary_sc <> b.primary_sc and a.secondary_sc <> b.secondary_sc then
      'PSC from ' || b.primary_sc || ' to ' || a.primary_sc || ' and SSC from ' || b.secondary_sc || ' to ' || a.secondary_sc
    when a.primary_sc <> b.primary_sc then 
      'PSC from ' || b.primary_sc || ' to ' || a.primary_sc
    when a.secondary_sc <> b.secondary_sc then 
      'SSC from ' || b.secondary_sc || ' to ' || a.secondary_sc
    else 'No Change'
  end as sc_change,
  case
    when b.record_status = 'None' then
      case 
        when a.record_status = 'None' then 'No Change'
        when a.record_status = 'A' then 'None to Accepted'
        when a.record_status = 'U' then 'None to Capped'
      end
    when b.record_status = 'A' then 
      case 
        when a.record_status = 'None' then 'Accepted to None'
        when a.record_status = 'A' then 'No Change'
        when a.record_status = 'U' then 'Accepted to Capped'
      end    
    when b.record_status = 'U' then
      case 
        when a.record_status = 'None' then 'Capped to None'
        when a.record_status = 'A' then 'Capped to Accepted'
        when a.record_status = 'U' then 'No Change'
      end  
    end as status_change        
from scpp.ext_deals a
inner join (
  select store_Code, stock_number, vin, customer_name, primary_sc, secondary_sc,
  record_Status, date_approved, date_capped, seq
  from scpp.xfm_deals x
  where seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = x.stock_number)) b on a.stock_number = b.stock_number
where (a.primary_sc <> b.primary_sc or a.secondary_sc <> b.secondary_sc  
  or a.record_status <> b.record_status)  
  
-- deleted from bopmast
-- run date is an issue
-- -- when this shit is real time current_date will work
-- -- but as i populate history current_date makes no sense
-- -- and there is no ext_ record from which to get the date
-- -- and the date from xfm_ is from when the row was last updated, which necessarily
-- -- is not the current run date
-- -- max(run_date) from ext_ should work, and will work even in production!
-- now, on the second day, this query wants to expose the same deals as deleted again!
-- -- this always makes me leary having a quick fix, anyway, here we go:
-- -- if the most recent row for that stock# is status_ch = deleted, skip it
-- -- so the base query of xfm_deals becomes of the most recent seq rows in xfm, only those
-- -- where status_ch <> deleted
select 'Update', /*current_date,*/ 
  (select max(run_date) from scpp.ext_deals) as run_date,
  a.store_code, a.stock_number, a.vin, 
  a.customer_name, a.primary_sc, a.secondary_sc, a.record_status,
  a.date_approved, a.date_Capped, a.origination_date, a.model_year,
  a.make, a.model,
  (select max(seq) + 1 from scpp.xfm_deals where stock_number = a.stock_number),
  'No Change' as sc_change, 
  'Deleted' as status_change
from (
  select *
  from (
    select *
    from scpp.xfm_deals x
    where seq = (
      select max(seq)
      from scpp.xfm_deals
      where stock_number = x.stock_number)) y
  where status_change <> 'Deleted') a
left join scpp.ext_deals b on a.stock_number = b.stock_number    
where b.stock_number is null 

  
-- group non critical changes: psc, ssc, appr
-- or do i just go through and do them one by one, i kind of like that
-- i don't care if there are 100 rows for a vehicle in xfm_
-- except doing one by one, how do i ensure that each previous update
-- is included, as it's now laid out it won't, because the base row is based
-- on ext_ which does not get updated
-- which leads me to think that all changes are done in a single insert
-- initially, the only down side is how generate a meaningful seq_note

-- -- -- psc
-- -- insert into scpp.xfm_deals
-- -- select 'Update', a.*,
-- --   (select max(seq) + 1 from scpp.xfm_deals where stock_number = a.stock_number),
-- --   'Primary SC Changed'
-- -- from scpp.ext_deals a
-- -- inner join (
-- --   select store_Code, stock_number, vin, customer_name, primary_sc, secondary_sc,
-- --   record_Status, date_approved, date_capped, seq
-- --   from scpp.xfm_deals x
-- --   where seq = (
-- --     select max(seq)
-- --     from scpp.xfm_deals
-- --     where stock_number = x.stock_number)) b on a.stock_number = b.stock_number
-- -- where a.primary_sc <> b.primary_sc;

-- any change, blow off the note for now
-- 4/10 not a good idea
-- insert into scpp.xfm_deals (row_type,run_date,store_code,stock_number,
--   vin,customer_name,primary_sc,secondary_sc,record_status,date_approved,
--   date_capped,origination_date,model_year,make,model,seq)
-- select 'Update', a.*,
--   (select max(seq) + 1 from scpp.xfm_deals where stock_number = a.stock_number)
-- from scpp.ext_deals a
-- inner join (
--   select store_Code, stock_number, vin, customer_name, primary_sc, secondary_sc,
--   record_Status, date_approved, date_capped, seq
--   from scpp.xfm_deals x
--   where seq = (
--     select max(seq)
--     from scpp.xfm_deals
--     where stock_number = x.stock_number)) b on a.stock_number = b.stock_number
-- where (a.primary_sc <> b.primary_sc or a.secondary_sc <> b.secondary_sc or a.vin <> b.vin 
--   or a.record_status <> b.record_status
--   or a.date_approved <> b.date_approved 
--   or a.date_capped <> b.date_capped)  

-- 4-10
-- it appears that i am not handling those deals that get removed from
-- bopmast, seem to remember that they seemed like an anomaly,
-- now, i don't think so, sometimes unwinds are removed, sometimes they are not

from ubuntu:
select * -- Houle started
from test.ext_bopmast_0118
where bopmast_stock_number = '27273'

select * -- Pankratz accepted
from test.ext_bopmast_0212
where bopmast_stock_number = '27273'

select * -- Pankratz capped
from test.ext_bopmast_0213
where bopmast_stock_number = '27273'

select * -- disappeared from bopmast
from test.ext_bopmast_0216
where bopmast_stock_number = '27273'

select * -- Carpenter accepted
from test.ext_bopmast_0220
where bopmast_stock_number = '27273'

select * -- Carpenter capped
from test.ext_bopmast_0223
where bopmast_stock_number = '27273'

this should happen in xfm

select * from scpp.xfm_deals where stock_number = '27273'

select *
from scpp.xfm_deals a
left join scpp.ext_deals b on a.stock_number = b.stock_number
where b.stock_number is null 

-- 4/14 the introduction of the gl_date changes things a bit
-- first
alter table scpp.xfm_deals
add column gl_date date;

-- new rows -- done
simple just add the new column

-- changes
-- deleted 
actuall, changes and deleted just need to add the gl_date
this extract is select from ext_ compared to xfm_
that could change as i refactor xfm_deals_for_update