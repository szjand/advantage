﻿drop table if exists scpp.xfm_deals_for_update;
create table if not exists scpp.xfm_deals_for_update (
  x_year_month_date_capped integer,
  x_year_month_run_date integer,
  x_employee_number citext,
  x_stock_number citext,
  x_unit_count numeric(3,1),
  x_date_capped date,
  x_gl_date date,
  x_customer_name citext,
  x_model_year citext,
  x_make citext,
  x_model citext, 
  x_seq integer,
  x_deal_status citext,
  x_run_date date,
  x_status_change citext,
  y_year_month integer,
  y_employee_number citext,
  y_stock_number citext,
  y_unit_count numeric(3,1),
  y_deal_date date,
  y_customer_name citext,
  y_model_year citext,
  y_make citext,
  y_model citext,
  y_seq integer,
  y_notes citext, 
  y_deal_status citext,
  y_run_date date);

-- with candidate_rows as (-- these are the candidate rows for insertion into scpp.deals
--                     -- the most recent (max(seq) row from xfm_ and deals
--                     -- exclude sc_change
--                     -- exclude and deal_status changing from deleted to accepted or none

insert into scpp.xfm_deals_for_update
select  x.year_month_date_capped, x.year_month_run_date, x.employee_number, x.stock_number,
  x.unit_count, x.date_capped, x.customer_name, x.model_year, x.make, x.model,
  x.seq, x.deal_status, x.run_date, x.status_change, 
  y.year_month, y.employee_number, 
  y.stock_number, y.unit_count, 
  y.deal_date,
  y.customer_name, y.model_year, 
  y.make, y.model, y.seq,
  y.notes, y.deal_status, y.run_date
from (  
--   select * from (
  select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer
        as year_month_date_capped,
    (100* extract(year from a.run_date) + extract(month from a.run_date)):: integer
        as year_month_run_date,
    coalesce(b.employee_number, 'House') as employee_number, a.stock_number as stock_number,
    case
      when secondary_sc = 'None' then 1
      else .5
    end as unit_count,
    a.date_capped , a.customer_name as customer_name,
    a.model_year as model_year, a.make as make, a.model as model, 'Dealertrack' as deal_source,
    (select max(d.seq) + 1 from scpp.deals d where d.stock_number = a.stock_number) as seq,
    case a.record_status
      when 'U' then 'Capped'
      when 'A' then 'Accepted'
      when 'None' then 'None'
    end as deal_Status, a.run_date as run_date, status_change as status_change
  from scpp.xfm_deals a
  -- inner join, RY1 only: FK constraint ref s_c_config
  inner join scpp.sales_consultants b on
    case
      when a.store_code = 'RY1' then a.primary_sc = b.ry1_id
      when a.store_code = 'RY2' then a.primary_sc = b.ry2_id
    end
    and b.employee_number not in ('150040','1106225')
  where a.run_date = (select max(xd.run_date) from scpp.xfm_deals xd)
    and a.row_type = 'update'
    and a.seq = (
      select max(e.seq)
      from scpp.xfm_deals e
      where e.stock_number = a.stock_number)
  union
  select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer
        as year_month_date_capped,
    (100* extract(year from a.run_date) + extract(month from a.run_date)):: integer
        as year_month_run_date,    
    coalesce(b.employee_number, 'House'), a.stock_number,
    0.5 as unit_count,
    a.date_capped, a.customer_name,
    a.model_year, a.make, a.model, 'Dealertrack',
    (select max(h.seq) + 1 from scpp.deals h where h.stock_number = a.stock_number) as seq,
    case a.record_status
      when 'U' then 'Capped'
      when 'A' then 'Accepted'
      when 'None' then 'None'
    end, a.run_date, a.status_change
  from scpp.xfm_deals a
  inner join scpp.sales_consultants b on
    case
      when a.store_code = 'RY1' then a.secondary_sc = b.ry1_id
      when a.store_code = 'RY2' then a.secondary_sc = b.ry2_id
    end
    and b.employee_number not in ('150040','1106225')
  where a.secondary_sc <> 'None'
    and a.run_date = (select max(i.run_date) from scpp.xfm_deals i)
    and a.row_type = 'update'
    and a.seq = (
      select max(f.seq)
      from scpp.xfm_deals f
      where f.stock_number = a.stock_number)) x
  inner join scpp.deals y on x.stock_number = y.stock_number
    -- modified to include deleted deals
    -- modified to be only status_changes and deleted deals
--     and (x.employee_number <> y.employee_number or x.deal_date <> y.deal_date or
--       x.deal_status <> y.deal_status or x.status_change = 'deleted')
    and (x.deal_status <> y.deal_status or x.status_change = 'deleted')      
    and y.seq = (
      select max(g.seq)
      from scpp.deals g
      where g.stock_number = y.stock_number)
--   where not ((x.status_change = 'Capped to Accepted' or x.status_change = 'Capped to None')and y.deal_status = 'Deleted'))
  where not ((x.deal_status = 'Accepted' or x.deal_status = 'None') and y.deal_status = 'Deleted') 
    and x.status_change not in ('No Change', 'None to Accepted', 'Accepted to None');

     
