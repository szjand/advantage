﻿-- drop function scpp.get_consultant_profile(_id integer);
-- create or replace function scpp.get_consultant_profile(_id integer) 
-- returns setof json as
-- $$
/*
the sales_consultant_configuration.id attribute identifies a sales consultant in a month
this returns a sales consultant profile for the currently open month
select * from scpp.get_consultant_profile(154);

2/17 this is all well and good, but the fucking sales_consultant_data should already
      populated and be the source for all this stuff.
      so, maybe what i need first is the nightly update for s_c_data
*/
with open_month 
  as (
    select year_month
    from scpp.months
    where open_closed = 'open')
select *, unit_count * per_unit -- row_to_json(z)
from (
  select b.id, aa.year_month,
    a.store_code, a.full_name, a.hire_date, b.pay_plan_name, 
    b.draw, b.full_months_employment, b.years_service, 
    b.pto_rate, coalesce(h.pto_hours, 0) as pto_hours, 
    0 as additional_comp, 0 as unpaid_time_off, 
    case
      when b.full_months_employment < 4 then i.guarantee_new_hire
      when e.metrics_qualified = true then i.guarantee_with_qual
      else i.guarantee
    end as guarantee,    
    coalesce(g.unit_count, 0) as unit_count, 
    case
      when e.metrics_qualified then (
        select tar
        from scpp.per_unit_matrix a
        inner join open_month aa on a.year_month = aa.year_month
        where units @> floor(coalesce(g.unit_count,0))::integer
          and pay_plan_name = b.pay_plan_name) 
      else (
        select map
        from scpp.per_unit_matrix a
        inner join open_month aa on a.year_month = aa.year_month
        where units @> floor(coalesce(g.unit_count,0))::integer
          and pay_plan_name = b.pay_plan_name) 
      end as per_unit,     
      e.* -- metric scores & qualification      
  from scpp.sales_consultants a
  inner join open_month aa on 1 = 1
  inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
--    and b.id = _id
--    and b.id = 154
  inner join scpp.pay_plans c on b.pay_plan_name = c.pay_plan_name
    and b.year_month = c.year_month
  inner join scpp.months d on c.year_month = d.year_month
    and d.open_closed = 'open'
  inner join (
    select a.employee_number, 
      case 
        when a.metric_qualified = true and b.metric_qualified = true 
          and c.metric_qualified = true and d.metric_qualified = true then true
        else false
      end as metrics_qualified,
      a.metric_value as auto_alert_score,
      a.metric_qualified as auto_alert_qualified,
      b.metric_value as csi_score,
      b.metric_qualified as csi_qualified,  
      c.metric_value as email_score,
      c.metric_qualified as email_qualified,
      d.metric_value as logged_score,
      d.metric_qualified as logged_qualified
    from scpp.sales_consultant_metric_data a
    inner join open_month aa on 1 = 1
    left join scpp.sales_consultant_metric_data b on a.employee_number = b.employee_number
      and b.year_month = aa.year_month
      and b.metric = 'csi'
    left join scpp.sales_consultant_metric_data c on a.employee_number = c.employee_number
      and c.year_month = aa.year_month
      and c.metric = 'Email Capture'  
    left join scpp.sales_consultant_metric_data d on a.employee_number = d.employee_number
      and d.year_month = aa.year_month
      and d.metric = 'Logged Opportunity Minimum'    
    where a.year_month = aa.year_month
      and a.metric = 'Auto Alert Calls per Month') e on a.employee_number = e.employee_number
--   left join dds.ext_pyhshdta f on a.employee_number = f.employee_    
--     AND 100 * (f.payroll_ending_year + 2000) + f.payroll_ending_month = c.year_month
--     and f.payroll_ending_day < 20
  left join (
    select employee_number, sum(unit_count) as unit_count
    from scpp.deals a
    inner join open_month aa on a.year_month = aa.year_month
    group by employee_number) g on a.employee_number = g.employee_number
  left join (
    select employee_number, sum(pto_hours) as pto_hours
    from scpp.pto_hours a
    inner join open_month aa on a.year_month = aa.year_month
    group by employee_number) h on a.employee_number = h.employee_number    
  left join scpp.guarantee_matrix i on aa.year_month = i.year_month  
    and b.pay_plan_name = i.pay_plan_name  
 ) z
 where store_code = 'RY1'
 order by full_name
-- $$
-- language sql;
-- 
--     select employee_number, sum(unit_count) as unit_count, floor(sum(unit_count))::integer,
--       sum(unit_count)::integer
--     from scpp.deals a
--     where year_month = 201601
--     group by employee_number

/*
from populate_and_backfill.sql
actual payroll data
select *
from (
  select b.store_code, b.employee_number, b.full_name, 
    100 * (payroll_ending_year + 2000) + payroll_ending_month as year_month,
    sum(total_gross_pay) as month_total_gross_pay,
    sum(case when payroll_ending_day < 20 then total_gross_pay end) as draw
  from dds.ext_pyhshdta a
  inner join scpp.sales_consultants b on a.employee_ = b.employee_number
  group by b.store_code, b.employee_number, b.full_name, 
    100 * (payroll_ending_year + 2000) + payroll_ending_month
) X where year_month = 201601
*/    

      
/* 
installed tablefunc extension
and while this works it sux, have to hard code column names
and don't know how to do multiple value columns (metric_value & metric_qualified)
SELECT *
FROM   crosstab(
      'SELECT employee_number, metric, metric_value
       FROM   scpp.sales_consultant_metric_data
       ORDER  BY 1,2')  -- needs to be "ORDER BY 1,2" here
AS ct (employee_number citext, "Auto Alert Calls per Month" numeric(6,2), 
  "CSI" numeric(6,2), "Email Capture" numeric(6,2), 
  "Logged Opportunity Minimum" numeric(6,2)); 
*/