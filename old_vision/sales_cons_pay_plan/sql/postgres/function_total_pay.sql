﻿-- Function: scpp.get_total_pay(integer, citext)

-- DROP FUNCTION scpp.get_total_pay(integer, citext, citext);

CREATE OR REPLACE FUNCTION scpp.get_total_pay(
    IN _year_month integer DEFAULT 666,
    IN _employee_number citext DEFAULT '1'::citext,
    in _table citext default ''::citext)
  RETURNS TABLE(year_month integer, employee_number citext, total_pay numeric) AS
$BODY$
/* 

the primary purpose of this function was to consolidate the messy total_pay calculation
into one spot
this is as close as i have come, 2 places in one function
good enuf for now

-- current month, all consultants
select * from scpp.get_total_pay()
-- June 2016 all consultants
select * from scpp.get_total_pay(201606, '1')
-- July 2016, Logan Carter
select * from scpp.get_total_pay(201607, '123600')
-- 8/8/16 added input parameter of table, if third parameter is not an empty string
-- returns whatever year month/consultant is in tmp_s_c_data
-- used to update total pay when editing via the admin page
select * from scpp.get_total_pay(1, '1', 'tmp')
*/
begin
if _table = '' then 
  return query 
  select x.year_month, x.employee_number, 
    case -- guarantees that total_pay is never less than earned
      when x.unit_count_x_per_unit > x.total_pay then x.unit_count_x_per_unit
      else x.total_pay
    end as actual_pay
  from (  
    select a.year_month, a.employee_number, a.full_name, a.unit_count_x_per_unit,
      case
        when unit_count_x_per_unit > guarantee then
          unit_count_x_per_unit + pto_pay + additional_comp
        else
          case
            when unit_count_x_per_unit + pto_pay > guarantee then
              unit_count_x_per_unit + pto_pay + additional_comp
            else -- guarantee
              case
                when pto_pay = 0 and pto_hours = 0 then
                -- this is the new hire pro-rate branch
                  case
                    when full_months_employment <> 0 then
                      -- not first month of employment :: straight guarantee
                      guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                    else
                      case
                        when hire_date <= b.sc_first_working_day_of_month then
                          -- started on or before 1st working day of month :: straight guarantee
                          guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                      else
                        -- pro-rated guarantee
                        (guarantee * (
                          select count(*)
                          from dds.day
                          where thedate between a.hire_date and b.last_of_month
                            and dayofweek between 2 and 6
                            and holiday = false)/b.sc_working_days)
                        - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                      end
                  end
                -- no pto due but has pto hours
                when pto_pay = 0 and pto_hours <> 0 then
                  (round((sc_working_days - pto_hours/8) * 1.0/sc_working_days, 4) * guarantee) -
                    (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) +
                    additional_comp
                else -- pto_pay <> 0
                  round(
                    (round((sc_working_days - pto_hours/8) * 1.0/sc_working_days, 4) * guarantee) +
                      pto_hours * pto_rate -
                      (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) +
                      additional_comp, 2)
              end
            end
        end as total_pay
    from scpp.sales_consultant_data a
    inner join scpp.months b on a.year_month = b.year_month
    where 
      case _employee_number
        when '1' then 1 = 1
        else a.employee_number = _employee_number
      end
      and 
        case _year_month
          when 666 then b.year_month = (select c.year_month from scpp.months c where open_closed = 'open')
          else b.year_month = _year_month
        end) x;
else
  return query 
  select x.year_month, x.employee_number, 
    case -- guarantees that total_pay is never less than earned
      when x.unit_count_x_per_unit > x.total_pay then x.unit_count_x_per_unit
      else x.total_pay
    end as actual_pay
  from (  
    select a.year_month, a.employee_number, a.full_name, a.unit_count_x_per_unit,
      case
        when unit_count_x_per_unit > guarantee then
          unit_count_x_per_unit + pto_pay + additional_comp
        else
          case
            when unit_count_x_per_unit + pto_pay > guarantee then
              unit_count_x_per_unit + pto_pay + additional_comp
            else -- guarantee
              case
                when pto_pay = 0 and pto_hours = 0 then
                -- this is the new hire pro-rate branch
                  case
                    when full_months_employment <> 0 then
                      -- not first month of employment :: straight guarantee
                      guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                    else
                      case
                        when hire_date <= b.sc_first_working_day_of_month then
                          -- started on or before 1st working day of month :: straight guarantee
                          guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                      else
                        -- pro-rated guarantee
                        (guarantee * (
                          select count(*)
                          from dds.day
                          where thedate between a.hire_date and b.last_of_month
                            and dayofweek between 2 and 6
                            and holiday = false)/b.sc_working_days)
                        - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
                      end
                  end
                -- no pto due but has pto hours
                when pto_pay = 0 and pto_hours <> 0 then
                  (round((sc_working_days - pto_hours/8) * 1.0/sc_working_days, 4) * guarantee) -
                    (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) +
                    additional_comp
                else -- pto_pay <> 0
                  round(
                    (round((sc_working_days - pto_hours/8) * 1.0/sc_working_days, 4) * guarantee) +
                      pto_hours * pto_rate -
                      (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) +
                      additional_comp, 2)
              end
            end
        end as total_pay
    from scpp.tmp_s_c_data a
    inner join scpp.months b on a.year_month = b.year_month) x;
end if;      
end;  
$BODY$
  LANGUAGE plpgsql;
ALTER FUNCTION scpp.open_new_month()
  OWNER TO rydell;  
