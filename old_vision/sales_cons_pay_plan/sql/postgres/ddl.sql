﻿/*
/*
2/17/16 more mods
sales_consultant_configuration: 
  remove: guarantee - it's a calculated value
          base_salary - this will become an attribute of finance pay plan
          
deals:
  add: unit_count to primary key
  rename: deal_type to deal_source
  
sales_consultant_data
  add: metrics_qualified
       unit_count
  remove: guarantee_paid
          draw_sys
          pto_hours_sys
  rename: guarantee_sys to guarantee
          total_pay_sys to total_pay
          total_pay_ovr to total_pay_paid

2/18
  holy shit i've gone nuts, sales_consultant_data has become the whole
  full blown every-fucking-thing, and tmp_s_c_data is an exact copy
  we'll see how it works out

2/24 notes refactor
  sales_consultant_data:
    remove notes attribute
    remove id
    change FK to sales_consultant_configuration
  tmp_s_c_data:
    remove notes attribute
    remove id    
  sales_consultant_configuration:
    add unique index on year_month, employee_number

2/29/16 
  months: add sc_working_days

3/2/16
  deals: add seq attribute, make it part of the PK 
    by default, when adding a deal from arkona (sys) the seq will be 1
    subsequent additions from the admin page (tmp_s_c_deals) will reference 
  tmp_s_c_deals: remove pk  
  sales_consultants: add username (email address from tpemployees)
        remove consultant_id, add ry1_id, ry2_id
  s_c_spreadsheet_data: new table for sc spreadsheet data

3/7/16
  ext_sales_consultants: same structure as sales_consultants, populated nightly
    used for sales_cons processing
    restructure to accomodate ry1_id, ry2_id

3/9/16
  drop table sales_consultant_metric_data
  sales_consultant_configuration: drop attribute pto_rate

3/13/16
  sales_consultant_data.unit_count_x_per_unit from integer to numeric(12,1)

3/17/16 
  add ext_pto_hours
3/18/16
    add ext_email_capture
3/27/16
  sales_consultants: add not null constraint for ry1_id, ry2_id, user_name
4/1/16
    add xfm_pto

4/19
    add guarantee_individual
*/


/*
create schema public;

create schema scpp;
comment on schema scpp is 'for sales consultant pay plan specific tables';
*/

/*
drop domain public.first_of_month CASCADE;
create domain public.first_of_month
as date
constraint check_first_of_month check (extract(day from VALUE) = 1);
comment on domain public.first_of_month is 'Allow only first of month';
*/
/*
drop function public.last_day_of_month(date) cascade;
create or replace function public.last_day_of_month(the_date DATE)
returns boolean as
$$
select lastdayofmonth
from dds.day
where thedate = the_date;
$$
language sql;
*/
/*
select last_day_of_month('12/31/9999');
select last_day_of_month(current_date);
*/
/*
drop domain public.last_of_month cascade;
create domain public.last_of_month
as date
constraint last_first_of_month check (last_day_of_month(VALUE) = true);
comment on domain public.last_of_month is 'Allow only last day of month';
*/

/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
RI on update and on delete
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

create extension tablefunc schema public;

drop table if exists scpp.months cascade;
-- delete from scpp.months;
create table if not exists scpp.months (
  year_month integer primary key,
  open_closed citext default 'closed',
  seq serial,
  first_of_month date not null,
  last_of_month date not null,
  month_yyyy citext,
  mmm_yyyy citext, 
  yyyy integer,
  previous_year_month integer);

insert into scpp.months (year_month,first_of_month,last_of_month,month_yyyy,mmm_yyyy,previous_year_month, yyyy)
select distinct a.yearmonth, b.thedate, c.thedate,
  a.monthname || ' ' || a.theyear::text,
  left(a.monthname, 3) || ' ' || a.theyear::text,
  a.lastyearmonth, b.theyear
from dds.day a
left join dds.day b on a.yearmonth = b.yearmonth
  and b.firstdayofmonth = true
left join dds.day c on a.yearmonth = c.yearmonth
  and c.lastdayofmonth = true  
where a.yearmonth between 195501 and 202311
  and a.yearmonth is not null
order by a.yearmonth;

update scpp.months
set open_closed = 'open'
where year_month = 201601;  
-- add sc_working_days
alter table scpp.months
add column sc_working_days integer;
update scpp.months a
set sc_working_days = x.sc_working_days
from (
  select yearmonth, count(*) as sc_working_days
  from dds.day a
  inner join scpp.months b on a.yearmonth = b.year_month
  where dayofweek between 2 and 6
    and holiday = false
  group by yearmonth) x  
where a.year_month = x.yearmonth;  
-- 3/7/16 add next_month
alter table scpp.months
add column next_year_month integer;

update scpp.months
set next_year_month = x.next_year_month
from (
  select a.year_month,
    (
      select yearmonth 
      from dds.day
      where thedate = a.last_of_month + 1) as next_year_month
  from scpp.months a) x
where scpp.months.year_month = x.year_month;  

drop table if exists scpp.metrics cascade;
create table if not exists scpp.metrics (
  metric citext,
  metric_value numeric(6,2) not null,
  metric_units citext not null, 
  year_month integer not null references scpp.months,
  constraint metrics_pk primary key (metric,  year_month));

-- delete from scpp.metrics;
insert into scpp.metrics (metric, metric_value, metric_units, year_month)
values
('CSI', 93.5, 'Percent', 201601),
('Email Capture', 75, 'Percent', 201601),
('Logged Opportunity Minimum', 30, 'Opportunities', 201601),
('Auto Alert Calls per Month', 5, 'Calls', 201601);

drop table if exists scpp.per_unit_matrix;
create table if not exists scpp.per_unit_matrix (
  year_month integer not null,
  pay_plan_name citext not null,
  units int4range not null,
  map integer not null,
  tar integer not null,
--  exclude using gist (units with &&),
  foreign key (year_month, pay_plan_name) references scpp.pay_plans(year_month, pay_plan_name),
  constraint per_unit_matrix_pk primary key (units, year_month, pay_plan_name));
-- delete from scpp.per_unit;
insert into scpp.per_unit_matrix (year_month, pay_plan_name, units, map, tar)
values
(201601,'Standard','[0,10]',200,230),
(201601,'Standard','[11,13]',230,260),
(201601,'Standard','[14,16]',280,310),
(201601,'Standard','[17,19]',315,345),
(201601,'Standard','[20,25]',335,365),
(201601,'Standard','[26,30]',350,380),
(201601,'Standard','[31,99]',365,395);

drop table if exists scpp.guarantee_matrix;
create table if not exists scpp.guarantee_matrix (
  year_month integer not null,
  pay_plan_name citext not null,
  guarantee integer not null default 0,
  guarantee_new_hire integer not null default 0,
  guarantee_with_qual integer not null default 0,
  foreign key (year_month, pay_plan_name) references scpp.pay_plans(year_month, pay_plan_name),
  constraint guarantee_matrix_pk primary key (year_month, pay_plan_name));
insert into scpp.guarantee_matrix (year_month, pay_plan_name, guarantee, 
  guarantee_new_hire, guarantee_with_qual)
values
(201601, 'Standard', 2300, 2600, 2600);

-- TODO -- year_month is irrelevant, this bonus is applicable to an entire
--         year and should probably be a year_month interval
--         yikes - this could get messy
--         paid as a lump sum once a year on 12/15 
drop table if exists scpp.retention_bonus;
create table if not exists scpp.retention_bonus (
  years_of_service int4range not null,
  bonus integer not null,
  year_month integer not null references scpp.months,
  exclude using gist (years_of_service with &&),
  constraint retention_bonus_pk Primary key (years_of_service,year_month));
insert into scpp.retention_bonus (years_of_service,bonus,year_month)
values
('[0,1]',0,201512),
('[2,3]',5,201512),
('[4,5]',6,201512),
('[6,7]',7,201512),
('[8,9]',8,201512),
('[10,99]',10,201512);

drop table if exists scpp.sales_consultants cascade;
create table if not exists scpp.sales_consultants (
  store_code citext not null,
  employee_number citext primary key,
  last_name citext not null,
  first_name citext not null,
  full_name citext not null,
  hire_date date not null,
  term_date date not null,
  sales_consultant_id citext,
  user_name citext);

alter table scpp.sales_consultants
  add column user_name citext;
-- 3/9 replace consultant_id with ry1_id and ry2_id
alter table scpp.sales_consultants
  drop column sales_consultant_id;
alter table scpp.sales_consultants
  add column ry1_id citext, 
  add column ry2_id citext;

ALTER TABLE scpp.sales_consultants ALTER COLUMN ry1_id SET NOT NULL;  
ALTER TABLE scpp.sales_consultants ALTER COLUMN ry2_id SET NOT NULL; 
ALTER TABLE scpp.sales_consultants ALTER COLUMN user_name SET NOT NULL; 

-- populate with python scrape of dds
-- hiredate anomalies
update scpp.sales_consultants
set hire_date = '10/12/2015'
where employee_number = '138411';

update scpp.sales_consultants
set hire_date = '08/01/2015'
where employee_number = '2132420';

update scpp.sales_consultants
set hire_date = '10/23/2014'
where employee_number = '124120';

-- 3/7 nightly work (scrape) table
drop table if exists scpp.ext_sales_consultants;
CREATE TABLE if not exists scpp.ext_sales_consultants(
  store_code citext NOT NULL,
  employee_number citext NOT NULL,
  last_name citext NOT NULL,
  first_name citext NOT NULL,
  full_name citext NOT NULL,
  hire_date date NOT NULL,
  term_date date NOT NULL,
  user_name citext,
  ry1_id citext,
  ry2_id citext,
  CONSTRAINT ext_sales_consultants_pkey PRIMARY KEY (employee_number));

  
drop table if exists scpp.pay_plans cascade;
-- add pay_plan_id for ember
create table if not exists scpp.pay_plans (
  pay_plan_id serial,
  pay_plan_name citext not null,
  year_month integer not null references scpp.months,
  base_salary integer default 0 not null,
  per_unit integer default 0 not null,
  f_i_percentage numeric(2,2) default 0 check(f_i_percentage between 0 and 1),
  constraint pay_plans_pk primary key (pay_plan_name, year_month));
insert into scpp.pay_plans (pay_plan_name, year_month, base_salary, per_unit, f_i_percentage)
values
('Standard', 201511, 0, 0, 0),
('Finance', 201511, 2000, 300, .07),
('Standard', 201512, 0, 0, 0),
('Finance', 201512, 2000, 300, .07),
('Standard', 201601, 0, 0, 0),
('Finance', 201601, 2000, 300, .07);

drop table if exists scpp.sales_consultant_configuration;  
create table if not exists scpp.sales_consultant_configuration(
  id serial not null unique,
  employee_number citext not null references scpp.sales_consultants,
  year_month integer not null,
  pay_plan_name citext not null,
  draw integer default 0 not null, 
  full_months_employment integer not null,
  years_service integer not null,
  guarantee integer default 0 not null,
  base_salary integer default 0 not null,
  pto_rate numeric(12,2) default 0 not null,
  constraint guarantee_base_check check (guarantee + base_salary > 0),
  foreign key (year_month,pay_plan_name) references scpp.pay_plans(year_month,pay_plan_name),
  constraint sales_consultant_configuration_pk primary key (employee_number,year_month,pay_plan_name));
-- 2/17
alter table scpp.sales_consultant_configuration 
drop guarantee, drop base_salary;
-- 2/24
alter table scpp.sales_consultant_configuration 
add constraint year_month_employee_number unique (year_month, employee_number);
-- 3/4 add sales_consultants.employee_number foreign key
delete from scpp.pto_intervals where employee_number = '2102196';
delete from scpp.sales_Consultant_configuration where employee_number = '2102196'
alter table scpp.sales_consultant_configuration
  add constraint sales_consultant_fkey
  foreign key(employee_number)
  references scpp.sales_consultants(employee_number);
-- 3/9 remove pto_rate
alter table scpp.sales_consultant_configuration
drop column pto_rate; 



drop table if exists scpp.deals;
create table if not exists scpp.deals (
  year_month integer not null references scpp.months,
  employee_number citext not null references scpp.sales_consultants,
  stock_number citext not null,
  unit_count numeric(3,1) not null check (unit_count in (-1, -.5, 0., .5, 1)),
  deal_date date not null,
  customer_name citext not null,
  model_year citext not null,
  make citext not null,
  model citext not null, 
  deal_type citext not null, -- normal, unwind, added
  constraint deals_pk primary key (year_month, employee_number, stock_number));
create index on scpp.deals(year_month); 
create index on scpp.deals(employee_number); 
-- 2/17
alter table scpp.deals drop constraint deals_pk;
alter table scpp.deals add constraint deals_pk primary key (year_month, employee_number, stock_number, unit_count);
alter table scpp.deals rename column deal_type to deal_source;
-- 3/3
alter table scpp.deals drop constraint deals_pk;
alter table scpp.deals
  add column seq integer default 1 not null;
alter table scpp.deals add constraint deals_pk primary key (year_month, employee_number, stock_number, seq);  
--3/4 add FK from sales_consultant_configuration, remove FK to months
alter table scpp.deals
  drop constraint deals_year_month_fkey;
delete from scpp.deals where year_month <> 201601;
alter table scpp.deals
  add constraint deals_fkey
  foreign key(year_month,employee_number)
  references scpp.sales_consultant_configuration(year_month,employee_number);
-- 4/7 add notes field to populate with info about system changes
alter table scpp.deals
add column notes citext;  
-- 4/8 add deal_stats field
alter table scpp.deals
add column deal_status citext;
update scpp.deals
set deal_status = 'Capped';
ALTER TABLE scpp.deals ALTER COLUMN deal_status SET NOT NULL;

drop table if exists scpp.sales_consultant_metric_data cascade;  
create table if not exists scpp.sales_consultant_metric_data(
  year_month integer not null,
  metric citext not null,
  employee_number citext not null references scpp.sales_consultants,
  metric_value numeric(6,2) default 0 not null, 
  metric_qualified boolean not null,
  foreign key (year_month,metric) references scpp.metrics(year_month,metric),
  constraint sales_consultant_metric_data_pk primary key (employee_number,year_month,metric));
-- 3/4s/b FK to s_c_config
alter table scpp.sales_consultant_metric_data
  add constraint sales_consultant_config_fkey
  foreign key(year_month,employee_number)
  references scpp.sales_consultant_configuration(year_month,employee_number);


/*
2/24/16 
remove notes to separate table
change ri, reference sales_consultant_configuration, not sales_consulants
*/
drop table if exists scpp.sales_consultant_data;
CREATE TABLE if not exists scpp.sales_consultant_data
(
  id bigint NOT NULL,
  year_month integer NOT NULL,
  store_code citext NOT NULL,
  full_name citext NOT NULL,
  employee_number citext not null,
  hire_date date NOT NULL,
  pay_plan_name citext NOT NULL,
  draw integer NOT NULL DEFAULT 0,
  draw_paid integer not null default 0,
  full_months_employment integer NOT NULL DEFAULT 0,
  years_service integer NOT NULL DEFAULT 0,
  pto_rate numeric(12,2) NOT NULL DEFAULT 0,
  pto_hours numeric(5,2) NOT NULL DEFAULT 0,
  pto_pay numeric(12,2) NOT NULL DEFAULT 0,
  additional_comp numeric(12,2) NOT NULL DEFAULT 0,
  unpaid_time_off integer NOT NULL DEFAULT 0,
  guarantee integer NOT NULL DEFAULT 0,
  unit_count_sys numeric(3,1) NOT NULL DEFAULT 0,
  unit_count_ovr numeric(3,1) NOT NULL DEFAULT 0,
  unit_count numeric(3,1) NOT NULL DEFAULT 0,
  per_unit integer NOT NULL DEFAULT 0,
  metrics_qualified boolean NOT NULL DEFAULT false,
  csi_score numeric(6,2) NOT NULL DEFAULT 0,
  csi_qualified boolean NOT NULL DEFAULT false,
  email_score numeric(6,2) NOT NULL DEFAULT 0,
  email_qualified boolean NOT NULL DEFAULT false,
  logged_score numeric(6,2) NOT NULL DEFAULT 0,
  logged_qualified boolean NOT NULL DEFAULT false,
  auto_alert_score numeric(6,2) NOT NULL DEFAULT 0,
  auto_alert_qualified boolean NOT NULL DEFAULT false,
  unit_count_x_per_unit integer NOT NULL DEFAULT 0,
  total_pay numeric(12,2) NOT NULL DEFAULT 0,
  total_pay_paid numeric(12,2) not null default 0,
  notes citext,
  complete boolean not null default false,
  CONSTRAINT sales_consultant_data_pk PRIMARY KEY (year_month, employee_number),
  CONSTRAINT sales_consultant_data_employee_number_fkey FOREIGN KEY (employee_number)
      REFERENCES scpp.sales_consultants (employee_number) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT sales_consultant_data_year_month_fkey FOREIGN KEY (year_month)
      REFERENCES scpp.months (year_month) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- 2/18 per greg's request
alter table scpp.sales_consultant_data
add column store_name citext;
update scpp.sales_consultant_data
set store_name = 'GM'
where store_code = 'RY1';

-- 2/24
alter table scpp.sales_Consultant_Data
drop column notes;

alter table scpp.sales_Consultant_Data
drop column id;

alter table scpp.sales_Consultant_Data
drop constraint sales_consultant_data_employee_number_fkey cascade;

alter table scpp.sales_Consultant_Data
drop constraint sales_consultant_data_year_month_fkey cascade;

-- need to add a unique index on sales_consultant_configuration (year_month, employee_number)
alter table scpp.sales_Consultant_Data
  add constraint sales_consultant_data_fkey 
  foreign key(year_month, employee_number)
  references scpp.sales_consultant_configuration(year_month, employee_number);
-- 3/13
alter table scpp.sales_consultant_data
  alter column unit_count_x_per_unit type numeric(12,1);
  
-- 2/11/16 interim tables for constructing deals from multiple sources
-- 2/11/16  deals
-- 3/17 rename to ext_deals;
-- 4/7 add record_Status, date_approved, change deal_date to date_capped
-- delete from scpp.tmp_deals;
drop table scpp.tmp_deals;
create table if not exists scpp.tmp_deals(
  store_code citext not null,
  year_month integer NOT NULL,
  primary_sc citext not null,
  secondary_sc citext,
  stock_number citext NOT NULL,
  deal_date date NOT NULL,
  customer_name citext NOT NULL,
  model_year citext NOT NULL, 
  make citext NOT NULL,
  model citext NOT NULL);

alter table scpp.tmp_deals rename to ext_deals;

drop table if exists scpp.ext_deals;
create table if not exists scpp.ext_deals(
  run_date date NOT NULL,
  store_code citext NOT NULL,
  stock_number citext NOT NULL,
  vin citext NOT NULL,
  customer_name citext NOT NULL,
  primary_sc citext NOT NULL,
  secondary_sc citext not null,
  record_status citext not null,
  date_approved date,
  date_capped date,
  origination_date bigint NOT NULL,
  model_year citext not null,
  make citext not null,
  model citext not null,
  constraint ext_deals_pk primary key (stock_number));

  
drop table scpp.tmp_sales_consultants;
create table if not exists scpp.tmp_sales_consultants(
  store_code citext not null,
  sales_consultant_id citext NOT NULL,
  employee_number citext not null,
  constraint tmp_sales_consultants_pkey primary key (store_code,sales_consultant_id));

drop table if exists rv.cstfb_email_data;
CREATE TABLE if not exists rv.cstfb_email_data(
  transaction_date date NOT NULL,
  store_code citext NOT NULL,
  sub_department citext NOT NULL,
  employee_name citext NOT NULL,
  employee_number citext not null,
  transaction_number citext NOT NULL,
  transaction_type citext NOT NULL,
  customer_name citext NOT NULL,
  email_address citext NOT NULL,
  home_phone citext NOT NULL,
  work_phone citext NOT NULL,
  cell_phone citext NOT NULL,
  has_valid_email boolean default false,
  constraint cstfb_email_data_pk primary key (transaction_number, customer_name)); 
create index on rv.cstfb_email_data(transaction_date);  
create index on rv.cstfb_email_data(employee_number);
create index on rv.cstfb_email_data(has_valid_email);
create index on rv.cstfb_email_data(transaction_type);

-- 2/17
-- state table
drop table if exists scpp.tmp_s_c_data;
create table if not exists scpp.tmp_s_c_data (
  id bigint NOT NULL,
  year_month integer NOT NULL,
  store_code citext NOT NULL,
  full_name citext NOT NULL,
  employee_number citext not null,
  hire_date date NOT NULL,
  pay_plan_name citext NOT NULL,
  draw integer NOT NULL DEFAULT 0,
  draw_paid integer not null default 0,
  full_months_employment integer NOT NULL DEFAULT 0,
  years_service integer NOT NULL DEFAULT 0,
  pto_rate numeric(12,2) NOT NULL DEFAULT 0,
  pto_hours numeric(5,2) NOT NULL DEFAULT 0,
  pto_pay numeric(12,2) NOT NULL DEFAULT 0,
  additional_comp numeric(12,2) NOT NULL DEFAULT 0,
  unpaid_time_off integer NOT NULL DEFAULT 0,
  guarantee integer NOT NULL DEFAULT 0,
  unit_count_sys numeric(3,1) NOT NULL DEFAULT 0,
  unit_count_ovr numeric(3,1) NOT NULL DEFAULT 0,
  unit_count numeric(3,1) NOT NULL DEFAULT 0,
  per_unit integer NOT NULL DEFAULT 0,
  metrics_qualified boolean NOT NULL DEFAULT false,
  csi_score numeric(6,2) NOT NULL DEFAULT 0,
  csi_qualified boolean NOT NULL DEFAULT false,
  email_score numeric(6,2) NOT NULL DEFAULT 0,
  email_qualified boolean NOT NULL DEFAULT false,
  logged_score numeric(6,2) NOT NULL DEFAULT 0,
  logged_qualified boolean NOT NULL DEFAULT false,
  auto_alert_score numeric(6,2) NOT NULL DEFAULT 0,
  auto_alert_qualified boolean NOT NULL DEFAULT false,
  unit_count_x_per_unit integer NOT NULL DEFAULT 0,
  total_pay numeric(12,2) NOT NULL DEFAULT 0,
  total_pay_paid numeric(12,2) not null default 0,
  notes citext,
  complete boolean not null default false);
-- 2/18 per greg's request
alter table scpp.tmp_s_c_data
add column store_name citext;
update scpp.tmp_s_c_data
set store_name = 'GM'
where store_code = 'RY1';
-- 2/24
alter table scpp.tmp_s_c_data
drop column notes;

alter table scpp.tmp_s_c_data
drop column id;

CREATE TABLE scpp.tmp_s_c_deals
(
  year_month integer NOT NULL,
  employee_number citext NOT NULL,
  stock_number citext NOT NULL,
  unit_count numeric(3,1) NOT NULL,
  deal_date date NOT NULL,
  customer_name citext NOT NULL,
  model_year citext NOT NULL,
  make citext NOT NULL,
  model citext NOT NULL,
  deal_source citext NOT NULL,
  CONSTRAINT tmp_s_c_deals_pk PRIMARY KEY (year_month, employee_number, stock_number, unit_count),
  CONSTRAINT tmp_s_c_deals_employee_number_fkey FOREIGN KEY (employee_number)
      REFERENCES scpp.sales_consultants (employee_number) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tmp_s_c_deals_year_month_fkey FOREIGN KEY (year_month)
      REFERENCES scpp.months (year_month) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT tmp_s_c_deals_unit_count_check CHECK (unit_count = ANY (ARRAY[(-1)::numeric, (-0.5), 0::numeric, 0.5, 1::numeric])))  
-- 3/3
delete from scpp.tmp_s_c_deals;
alter table scpp.tmp_s_c_deals drop constraint tmp_s_c_deals_pk;
alter table scpp.tmp_s_c_deals
  add column seq integer not null check (seq > 0);
alter table scpp.tmp_s_c_deals add constraint tmp_s_c_deals_pk primary key (year_month, employee_number, stock_number, seq);   


-- 2/24
drop table if exists scpp.admin_notes cascade;
create table if not exists scpp.admin_notes (
  year_month integer not null,
  employee_number citext not null,
  the_date date not null,
  note citext not null,
  constraint admin_notes_pk primary key(year_month,employee_number, the_date, note),
  constraint admin_notes_fkey
    foreign key (year_month,employee_number)
    references scpp.sales_consultant_configuration(year_month,employee_number));

drop table if exists scpp.tmp_admin_notes cascade;
create table if not exists scpp.tmp_admin_notes (
  year_month integer not null,
  employee_number citext not null,
  the_date date not null,
  note citext not null,
  constraint tmp_admin_notes_pk primary key(year_month,employee_number, the_date, note),
  constraint tmp_admin_notes_fkey
    foreign key (year_month,employee_number)
    references scpp.sales_consultant_configuration(year_month,employee_number));    

drop table if exists scpp.s_c_spreadsheet_data cascade;
create table if not exists scpp.s_c_spreadsheet_data (
  year_month integer not null,
  employee_number citext not null,
  stock_number citext not null,
  customer_name citext,
  split citext,
  other_sc citext,
  constraint s_c_spreadsheet_data_pk primary key(year_month, employee_number, stock_number));   


drop table if exists scpp.pto_hours cascade;
create table scpp.pto_hours (
  year_month integer not null,
  the_date date not null,
  employee_number citext not null,
  pto_hours numeric(5,2) default 0,
constraint pto_hours_pk primary key (the_date,employee_number)); 
-- 3/4/ add FK
delete 
-- select *
from scpp.pto_hours a
where not exists (
  select 1
  from scpp.sales_consultant_configuration
  where employee_number = a.employee_number
    and year_month = a.year_month);
alter table scpp.pto_hours
  add constraint pto_hours_fkey
  foreign key(year_month,employee_number)
  references scpp.sales_consultant_configuration(year_month,employee_number);

CREATE TABLE scpp.ext_pto_hours(
  year_month integer NOT NULL,
  the_date date NOT NULL,
  employee_number citext NOT NULL,
  pto_hours numeric(5,2) DEFAULT 0,
  CONSTRAINT ext_pto_hours_pk PRIMARY KEY (the_date, employee_number));

create table scpp.ext_email_capture(
  the_date date,
  employee_number citext,
  stock_number citext,
  has_valid_email boolean,
  constraint ext_email_capture_pk primary key(the_date, stock_number)); 

-- this one handles dates, which i didn't do in february
=CONCATENATE("insert into scpp.s_c_spreadsheet_data_201603 values(201603",",'1148583',","'",TEXT(A2,"MM/DD/YYYY"),"',","'",B2,"','",C2,"','",D2,"','",E2,"');")  

CREATE TABLE scpp.s_c_spreadsheet_data_201603
(
  year_month integer NOT NULL,
  employee_number citext NOT NULL,
  deal_Date date,
  stock_number citext NOT NULL,
  customer_name citext,
  split citext,
  other_sc citext,
  CONSTRAINT s_c_spreadsheet_data_201603_k PRIMARY KEY (year_month, employee_number, stock_number)
)


create table scpp.guarantee_individual (
  year_month integer not null,
  employee_number citext not null,
  guarantee integer not null,
  constraint guarantee_individual_pk primary key (year_month, employee_number),
  constraint guarantee_individual_fk foreign key (year_month, employee_number)
    references scpp.sales_consultant_configuration(year_month, employee_number));

insert into scpp.guarantee_individual (year_month,employee_number,guarantee)
values
  (201603, '161325', 4000),    
  (201603, '167600', 4000), 
  (201603, '1130690', 5000);

update scpp.sales_consultant_data w
set guarantee = x.guarantee
from (
  select a.year_month, a.employee_number, a.guarantee
  from scpp.guarantee_individual a
  inner join scpp.months b on a.year_month = b.year_month
    and b.open_closed = 'open') x
where w.year_month = x.year_month
  and w.employee_number = x.employee_number;    
  