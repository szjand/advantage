﻿  
update scpp.sales_consultant_data a
set total_pay = x.total_pay
from (
  select s.year_month, s.employee_number,  
    case
      when unit_count_x_per_unit > guarantee then
        unit_count_x_per_unit + pto_pay + additional_comp
      else
        case
          when unit_count_x_per_unit + pto_pay > guarantee then
            unit_count_x_per_unit + pto_pay + additional_comp
          else 
            guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
          end
      end as total_pay    
  from scpp.sales_consultant_data s
  inner join scpp.months t on s.year_month = t.year_month) x
 where a.employee_number = x.employee_number
   and a.year_month = x.year_month