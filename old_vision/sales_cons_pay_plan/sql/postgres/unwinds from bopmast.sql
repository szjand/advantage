﻿select bopmast_company_number, record_key, record_Status, record_type, bopmast_stock_number, date_capped
from test.ext_bopmast_1230 a
where date_capped > '12/01/2015'
  and not exists (
    select 1
    from test.ext_bopmast_0106
--    where record_key = a.record_key)
    where bopmast_stock_number = a.bopmast_stock_number)
order by date_capped desc 