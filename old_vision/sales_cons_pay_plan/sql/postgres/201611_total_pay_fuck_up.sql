﻿/*
things were all fucked up
email capture was not updated
kc improvised on pay using additional comp

everything for november has been updated except total pay
need to see what kim actually pays the guys

!!!!!!!!!!check accounting
*/

-- from arkona, commissions and draw only (based on codes in pyhscdta)
-- does not include spiffs (Spiff pay outs)
drop table if exists _arkona;
create temp table _arkona as
select a.employee_name, a.employee_, a.total_gross_pay as check_total, b.description as check_description, 
    check_month,check_day,check_year, 
    c.amount, c.description as code_description
--select *
from dds.ext_pyhshdta a
inner join  dds.ext_PYPTBDTA b on a.payroll_run_number = b.payroll_run_number
  and a.company_number = b.company_number
inner join dds.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number
  and a.company_number = c.company_number
  and a.employee_ = c.employee_number
  and trim(c.code_id) in ('79','78') --commissions, draws
-- where trim(a.employee_) = '128530'
where trim(a.distrib_code) = 'SALE'
  and a.payroll_cen_year = 116
  and a.payroll_ending_month between 9 and 11
  and a.company_number = 'RY1'
order by check_month, employee_  

-- ok, generate a monthly total by employee
-- select * from (
select employee_name, employee_, check_month,
  sum(case when code_description = 'DRAWS' then amount else 0 end) as total_draw,
  sum(case when code_description = 'COMMISSIONS' then amount else 0 end) as total_commission,
  sum(amount) as total
from _arkona
group by employee_name, employee_, check_month
-- ) x where total_draw + total_commission <> total

select *
from scpp.ext_email_capture
order by the_date desc limit 500


select employee_number, full_name, unit_count, additional_comp, per_unit, unit_count_x_per_unit, total_pay, employee_number,
  additional_comp + unit_count_x_per_unit ,
  (select total_pay from scpp.get_total_pay(201609,a.employee_number))
from scpp.sales_consultant_data a
where year_month = 201609
order by employee_number

select * from scpp.get_total_pay(201611)

--12/3 have not yet updated november total pay in sc_data
update scpp.sales_consultant_data a
set total_pay = b.total_pay
from (select * from scpp.get_total_pay(201611)) b
where a.employee_number = b.employee_number
  and a.year_month = b.year_month

-- actual gross pay
-- ok, this is going to get ugly, because the per_unit has been updated to reflect 
-- the correct metrics_qualified (fixed email capture)
--
select a.employee_number, full_name, full_months_employment, unit_count, pto_pay, draw, guarantee, metrics_qualified, 
  additional_comp, per_unit, unit_count_x_per_unit, --total_pay, employee_number,
  additional_comp + unit_count_x_per_unit as add_comp_plus_earned, 
  b.total_draw as total_draw_paycheck, b.total_commission as total_commission_paycheck, 
  c.admin_note,
  b.total as total_paycheck_comm_draw, 
  (select total_pay from scpp.get_total_pay(201611,a.employee_number)) as total_pay_function, aa.total_pay as total_pay_sc_data
from scpp.sales_Consultant_data a
left join lateral(
  select *
  from scpp.get_total_pay(201611,a.employee_number)) aa on a.employee_number = aa.employee_number
left join (
  select employee_name, employee_, check_month,
    sum(case when code_description = 'DRAWS' then amount else 0 end) as total_draw,
    sum(case when code_description = 'COMMISSIONS' then amount else 0 end) as total_commission,
    sum(amount) as total
  from _arkona
  where check_month = 11
  group by employee_name, employee_, check_month) b on a.employee_number = b.employee_  
left join (
  select employee_number, string_agg(note, '-') as admin_note
  from scpp.admin_notes
  where year_month = 201611
  group by employee_number) c on a.employee_number = c.employee_number  
where a.year_month = 201611
order by a.employee_number    

-- 12/5/16, ok, let's get done with this shit
-- figure out the one offs, recalc total_pay (function), move additional comp added by KD
-- to override the email fuckup to notes

1. shit forgot to update guarantee to accomodate the metrics correction

update scpp.sales_consultant_data
set guarantee = 2600
 -- select employee_number, full_name, full_months_employment, metrics_qualified, guarantee, unit_countfrom scpp.sales_consultant_data
 where full_months_employment > 3
   and year_month = 201611
   and metrics_qualified = true
   
2. tanner trosen shows 8 hours pto in november, do not know where that came from, 
      does not exist in pto or arkona
   remove additional comp
update scpp.sales_consultant_Data   
set pto_hours = 0, 
    pto_pay = 0     
-- select * from scpp.sales_consultant_Data
where employee_number = '1141642'
  and year_month = 201611;

delete
-- select *
from scpp.pto_hours
where year_month = 201611
  and employee_number = '1141642';

update scpp.sales_consultant_data
set additional_comp = 0
where employee_number = '1141642'
  and year_month = 201611;

update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(201611,'1109815'))
where employee_number = '1141642'
  and year_month = 201611;  

update scpp.admin_notes
set note = note || ' ** additional comp removed by jon after fixing metrics qualified **'
where employee_number = '1141642'
  and year_month = 201611;  

3. Scott Pearson: remove the additional comp
update scpp.sales_consultant_data
set additional_comp = 0
where employee_number = '1109815'
  and year_month = 201611;

update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(201611,'1109815'))
where employee_number = '1109815'
  and year_month = 201611;

update scpp.admin_notes
set note = note || ' ** additional comp removed by jon after fixing metrics qualified **'
where employee_number = '1109815'
  and year_month = 201611; 

4. Allen PReston: remove the additional comp
update scpp.sales_consultant_data
set additional_comp = 0
where employee_number = '1112410'
  and year_month = 201611;

update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(201611,'1109815'))
where employee_number = '1112410'
  and year_month = 201611;

update scpp.admin_notes
set note = note || ' ** additional comp removed by jon after fixing metrics qualified **'
where employee_number = '1112410'
  and year_month = 201611; 
  
5. Warmack: remove the additional comp
do
$$
 declare 
   _emp citext := '1147250';
   _ym integer := 201611;
begin
update scpp.sales_consultant_data
set additional_comp = 0
where employee_number = _emp
  and year_month = _ym;
update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(_ym,_emp))
where employee_number = _emp
  and year_month = _ym; 
-- update scpp.admin_notes
-- set note = note || ' ** additional comp removed by jon after fixing metrics qualified **'
-- where employee_number = _emp
--   and year_month = _ym;  
end
$$;  

6. Chris Carlson, remove additional comp
do
$$
 declare 
   _emp citext := '123425';
   _ym integer := 201611;
begin
update scpp.sales_consultant_data
set additional_comp = 0
where employee_number = _emp
  and year_month = _ym;
update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(_ym,_emp))
where employee_number = _emp
  and year_month = _ym; 
-- update scpp.admin_notes
-- set note = note || ' ** additional comp removed by jon after fixing metrics qualified **'
-- where employee_number = _emp
--   and year_month = _ym;  
end
$$;  

-- Logan Carter, remove additional comp
do
$$
 declare 
   _emp citext := '123600';
   _ym integer := 201611;
begin
update scpp.sales_consultant_data
set additional_comp = 0
where employee_number = _emp
  and year_month = _ym;
update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(_ym,_emp))
where employee_number = _emp
  and year_month = _ym; 
update scpp.admin_notes
set note = note || ' ** additional comp removed by jon after fixing metrics qualified **'
where employee_number = _emp
  and year_month = _ym;  
end
$$;  

-- Jesse Chavez, remove additional comp
do
$$
 declare 
   _emp citext := '124120';
   _ym integer := 201611;
begin
update scpp.sales_consultant_data
set additional_comp = 0
where employee_number = _emp
  and year_month = _ym;
update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(_ym,_emp))
where employee_number = _emp
  and year_month = _ym; 
update scpp.admin_notes
set note = note || ' ** additional comp removed by jon after fixing metrics qualified **'
where employee_number = _emp
  and year_month = _ym;  
end
$$; 

-- Croaker, changed additional comp from 1968 to 1173
do
$$
 declare 
   _emp citext := '128530';
   _ym integer := 201611;
begin
update scpp.sales_consultant_data
set additional_comp = 1173
where employee_number = _emp
  and year_month = _ym;
update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(_ym,_emp))
where employee_number = _emp
  and year_month = _ym; 
update scpp.admin_notes
set note = 'additional comp is sale apt contest  kc ** additional comp changed by jon after fixing metrics qualified, 1968 to 1173 **'
where employee_number = _emp
  and year_month = _ym;  
end
$$; 

-- Dockendorf, remove additional comp
-- got paid @310/unit, should have been 280, did not qualify for CSI
do
$$
 declare 
   _emp citext := '133017';
   _ym integer := 201611;
begin
update scpp.sales_consultant_data
set additional_comp = 0
where employee_number = _emp
  and year_month = _ym;
update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(_ym,_emp))
where employee_number = _emp
  and year_month = _ym; 
update scpp.admin_notes
set note = note || ' ** additional comp removed by jon after fixing metrics qualified **'
where employee_number = _emp
  and year_month = _ym;  
end
$$; 

-- Eden, remove additional comp
do
$$
 declare 
   _emp citext := '137220';
   _ym integer := 201611;
begin
update scpp.sales_consultant_data
set additional_comp = 0
where employee_number = _emp
  and year_month = _ym;
update scpp.sales_consultant_data
set total_pay = (select total_pay from scpp.get_total_pay(_ym,_emp))
where employee_number = _emp
  and year_month = _ym; 
update scpp.admin_notes
set note = note || ' ** additional comp removed by jon after fixing metrics qualified **'
where employee_number = _emp
  and year_month = _ym;  
end
$$; 





