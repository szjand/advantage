﻿drop function if exists scpp.save_admin_deals(integer,citext,numeric,date,citext,citext,citext,citext,citext);
create or replace function scpp.save_admin_deals(
  _id integer,
  _stock_number citext,
  _unit_count numeric(3,1),
  _deal_date date,
  _customer_name citext,
  _model_year citext,
  _make citext,
  _model citext,
  _notes citext)
returns setof json as
$BODY$
/*
add new row to tmp_s_c_deals
update tmp_s_c_date: 
  unit_count_ovr
  unit_count
  per_unit
  unit_count_x_per_unit
  total_pay
  notes
return list of deals

-- 2/22/16 need to update notes as well

use arden, 4 deals in january, id = 162
bryan seay, 10 deals, see the per_unit change id = 166

delete from scpp.tmp_s_c_data;
delete from scpp.tmp_s_c_deals;

select * from scpp.get_consultant_profile(162);
select scpp.get_consultant_deals(162)

delete from scpp.tmp_s_c_deals where deal_source = 'ovr';

select scpp.save_admin_deals(162, '666a',1,'01/18/2016','NEWPHEW, JAMES, DAVID','1997','CHEVROLET','SILVERADO 1550', 'deal note') 

*/
begin
if exists (
    select 1
    from scpp.tmp_s_c_deals
    where stock_number = _stock_number
      and deal_source = 'ovr') 
  then
    update scpp.tmp_s_c_deals
    set unit_count = _unit_count
    where stock_number = _stock_number
      and deal_source = 'ovr';
else
  insert into scpp.tmp_s_c_deals(year_month,employee_number,stock_number,unit_count,deal_date,customer_name,
    model_year,make,model,deal_source)
  select year_month, employee_number, _stock_number, _unit_count, _deal_date,
    _customer_name, _model_year, _make, _model, 'ovr'  
  from scpp.tmp_s_c_data
  where id = _id; 
end if;    

update scpp.tmp_s_c_data
  set unit_count_ovr = (select sum(unit_count) from scpp.tmp_s_c_deals where deal_source = 'ovr'),
      unit_count = (select sum(unit_count) from scpp.tmp_s_c_deals);
update scpp.tmp_s_c_data      
  set per_unit = (
    select 
      case
        when z.metrics_qualified then (
          select tar
          from scpp.per_unit_matrix a
          where year_month = z.year_month
            AND units @> floor(coalesce(z.unit_count,0))::integer
            and pay_plan_name = z.pay_plan_name) 
        else (
          select map
          from scpp.per_unit_matrix a
          where year_month = z.year_month
            and units @> floor(coalesce(z.unit_count,0))::integer
            and pay_plan_name = z.pay_plan_name) 
        end as per_unit  
    from scpp.tmp_s_c_data z), 
    notes =     
      case
        WHEn  length(trim(_notes)) > 0 then (
          select 
            case 
              when length(trim(a.notes)) > 0 then 
                concat(a.notes,chr(10), chr(13), (select to_char(current_date, 'MM/DD/YYYY')) || ': ' || _notes ||'. ')
              else (select to_char(current_date, 'MM/DD/YYYY')) || ': ' || _notes ||'. '
            end  
          from scpp.tmp_s_c_data a)
       end;  
return query 
select row_to_json(a)
from (
  select *
  from scpp.tmp_s_c_deals) a;
end;   
$BODY$
language plpgsql; 