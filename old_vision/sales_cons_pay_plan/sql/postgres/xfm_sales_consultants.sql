﻿drop table if exists scpp.xfm_sales_consultants;
create table if not exists scpp.xfm_sales_consultants (
  row_type citext,
  store_code citext NOT NULL,
  employee_number citext NOT NULL,
  last_name citext NOT NULL,
  first_name citext NOT NULL,
  full_name citext NOT NULL,
  hire_date date NOT NULL,
  term_date date NOT NULL,
  user_name citext,
  ry1_id citext,
  ry2_id citext,
  run_date date);

-- delete from scpp.xfm_sales_consultants
insert into scpp.xfm_sales_consultants
select 'New' as row_type, a.*, current_date   
from scpp.ext_sales_consultants a
where not exists (
  select 1
  from scpp.sales_consultants
  where employee_number = a.employee_number)
union
select 'Update', a.*, current_date
--   case
--     when a.first_name <> b.first_name then 'first_name'
--     when a.last_name <> b.last_name then 'last_name'
--     when a.full_name <> b.full_name then 'full_name'
--     when a.term_date <> b.term_date then 'term_date'
--     when a.user_name <> b.user_name then 'user_name'
--     when a.ry1_id <> b.ry1_id then 'ry1_id'
--     when a.ry2_id <> b.ry2_id then 'ry2_id'
--   end,
from scpp.ext_sales_consultants a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number
  and (
    a.first_name <> b.first_name
    or
    a.last_name <> b.last_name
    or
    a.full_name <> b.full_name
    or
    a.term_date <> b.term_date
    or
    a.user_name <> b.user_name
    or
    a.ry1_id <> b.ry1_id
    or 
    a.ry2_id <> b.ry2_id)

delete from scpp.xfm_sales_consultants 

select * from scpp.xfm_sales_consultants    

-- load_sales_consultants
-- new consultant
-- update the sales consultant shit ry1/ ry2, which means update dimSalesConsultants
-- 3/27 good enuf for now, all cons have ry1/ry2 ids
insert into scpp.sales_consultants (store_code, employee_number, last_name, first_name, full_name,
  hire_date, term_date, uiser_name, ry1_id, ry2_id)
select store_code, employee_number, last_name, first_name, full_name,
  hire_date, term_date, user_name, ry1_id, ry2_id
from scpp.xfm_sales_consultants
where row_type = 'New'; 

update scpp.sales_consultants x
set first_name = z.first_name,
    last_name = z.last_name,
    full_name = z.full_name,
    term_date = z.term_date,
    user_name = z.user_name
from (
  select *
  from scpp.xfm_sales_consultants
  where row_type = 'Update') z   
where x.store_code = z.store_code
  and x.ry1_id = z.ry1_id
  and x.ry2_id = z.ry2_id;  

 ---------------------------------------------------------------------------------------------
select * 
-- delete
from ops.task_log

select * from scpp.ext_sales_consultants
select * from scpp.xfm_sales_consultants


 fuck need to distinguish between new consultant in the middle of a month
 vs new month as a result of closing previous month

 new consultant: requires all the base rows like in open_new_month()
 sales_consultant_configuration
 
 need
 missing rows:

with open_month as (
  select *
  from scpp.months
  where open_closed = 'open')
select *
from scpp.sales_consultants a
left join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
  and b.year_month = (select year_month from open_month)
  and b.pay_plan_name = 'Standard'
where a.term_date > (select last_of_month from open_month)
  and a.store_Code = 'RY1'
  and b.employee_number is null 

-- 3/31 fuck, after a couple days off need to get back on track
primary conversations
1.
the difference between new month and the advent of a new consultant during the month
january is closed, an assumption is that due to nightly updates, there are no new consltants
hired in january that are not in the data
2. logging piece: what was supposed to have run and did it? feels like this is part of the categorization
      of tasks that i have yet to do
      so what i am thinking is a category of scpp
      the deal is that each task could be part of multiple task categories
      eg
      employees
      dimEmployee could be a category of task
      multiple levels of task dependencies
      so, is the categorization of tasks an attribute of dependcies or tasks
      or, is categorization a construct of task_schedules

      ops.schedules > ops.tasks
          ops.task_schedules
            
      schedule: scpp_nightly: ext_sales_consultants
                              xfm_sales_consultants
                              ...

3.

ah ah ah ah
maybe this is it
if the consultant in xfm_sales_consultants already exists in sales_Consultant_configuration
for the open month vs not exists 

select *
from scpp.xfm_sales_consultants a
left join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
inner join scpp.months c on b.year_month = c.year_month
  and c.open_closed = 'open'

if the consultant does not exist in config, then all the base rows (open_new_month()) must be added
not true - only a row in sc_config & sc_data (the other base rows in open_new_month() are based on
the open month NOT sales consultants)

so, modify load_sales_consultants to add rows to sc_config and sc_data

issue 1 there is nothing in arkona that differentiates between a sc on standard vs finance pay plan  
per email to all concerned, i am proceeding to add new sc, assume standard pay plan

-- sc_configuration
-- assume standard pay plan, 500 draw
insert into scpp.sales_consultant_configuration (employee_number, year_month, pay_plan_name,
    draw, full_months_employment, years_service)  
select a.employee_number, b.year_month, 'Standard', 500, 
  case 
    when b.seq - c.seq = 0 then 0
    else b.seq - c.seq - 1
  end as full_months_employment,
  (b.last_of_month - a.hire_date)/365 as years_service
from scpp.xfm_sales_consultants a
inner join scpp.months b on 1 = 1
  and b.open_closed = 'open'
inner join scpp.months c on a.hire_Date between c.first_of_month and c.last_of_month
where a.row_type = 'new'    


-- sales _consultant_data
  insert into scpp.sales_consultant_data (year_month,store_code,full_name,employee_number,
    hire_date,pay_plan_name,draw,
    full_months_employment,years_service,
    store_name)
  select c.year_month, a.store_code, a.full_name, a.employee_number, a.hire_Date, 
    b.pay_plan_name, b.draw, b.full_months_employment, 
    b.years_service, 
    case a.store_code
      when 'RY1' then 'GM'
      when 'RY2' then 'Honda Nissan'
    end as store_name
  from scpp.xfm_sales_consultants a
  inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
  inner join scpp.months c on b.year_month = c.year_month
    and c.open_closed = 'open'

-- ok, those 2 queries added to load_sales_consultants.py, seems to work ok
-- on to pto intervals/rate
                select store_code, employee_number, last_name, first_name, full_name,
                  hire_date, term_date, user_name, ry1_id, ry2_id
                from scpp.xfm_sales_consultants
                where row_type = 'New'

select *
from scpp.sales_consultants
where employee_number in ('137220','1106223','174021')                


select *
from scpp.sales_consultant_configuration
where employee_number in ('137220','1106223','174021')         


select *
from scpp.sales_consultant_data
where employee_number in ('137220','1106223','174021')       

select scpp.open_new_month()
select * from scpp.months where open_closed = 'open'

select * from scpp.pto_intervals


-- these are the relevant (and for now configured) codes
select c.code_id, c.description
from scpp.sales_consultants a
left join dds.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year > 14
left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
  and b.payroll_run_number = c.payroll_run_number  
  and c.code_type = '1'
where b.employee_ is not null
group by c.code_id, c.description                


select x.*, sum(amount) over(partition by employee_number) as month_total
from (
  select b.check_day, b.payroll_run_number, a.employee_number, a.last_name, 
    a.first_name, b.total_gross_pay,
    c.code_id, c.amount, c.description
  from scpp.sales_consultants a
  left join dds.ext_pyhshdta b on a.employee_number = b.employee_
    and b.check_year = 16
    and b.check_month = 1
  left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
    and b.payroll_run_number = c.payroll_run_number  
    and c.code_type = '1'
  where b.employee_ is not null) x   
order by x.employee_number, x.check_day

-- this could be it for persisting paycheck data
select b.check_day, a.employee_number, a.last_name, a.first_name, b.total_gross_pay,
  c.code_id, c.amount, c.description,
  b.check_year, b.check_month,
  ((2000 + b.check_year)::text || right(trim('0' || check_month::text), 2))::integer as check_year_month, 
  (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date
from scpp.sales_consultants a
left join dds.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year = 15p
--  and b.check_month = 12
left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
  and b.payroll_run_number = c.payroll_run_number  
where b.employee_ is not null   
order by a.employee_number, b.check_day

--ok, got the xfm_pto rows, now need to update the rate
select employee_number, count(*) from (
select *
from scpp.xfm_pto a
left join (
  select b.check_day, a.employee_number as emp_no, a.last_name, a.first_name, b.total_gross_pay,
    c.code_id, c.amount, c.description,
    b.check_year, b.check_month,
    ((2000 + b.check_year)::text || right(trim('0' || check_month::text), 2))::integer as check_year_month, 
    (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date
  from scpp.sales_consultants a
  left join dds.ext_pyhshdta b on a.employee_number = b.employee_
--    and b.check_year = 15
  --  and b.check_month = 12
  left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
    and b.payroll_run_number = c.payroll_run_number  
  where b.employee_ is not null) b on a.employee_number = b.emp_no
    and b.check_date between a.pto_period_from and a.pto_period_thru
) x group by employee_number    


  select a.employee_number as emp_no, aa.last_name, aa.first_name, sum(b.total_gross_pay), count(*)
    --c.code_id, c.amount, c.description,
--     b.check_year, b.check_month,
--     ((2000 + b.check_year)::text || right(trim('0' || check_month::text), 2))::integer as check_year_month, 
--     (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date
  from scpp.xfm_pto a
  inner join scpp.sales_consultants aa on a.employee_number = aa.employee_number
  left join dds.ext_pyhshdta b on a.employee_number = b.employee_
    and ((check_month || '-' || check_day || '-' || (2000 + check_year))::date) between a.pto_period_from and a.pto_period_thru
--    and b.check_year = 15
  --  and b.check_month = 12
--   left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
--     and b.payroll_run_number = c.payroll_run_number  
  where b.employee_ is not null
group by a.employee_number, aa.last_name, aa.first_name


select * from scpp.xfm_pto

update scpp.xfm_pto w
set pto_rate = x.pto_rate
from (
  select employee_number, year_month 
    case
      when most_recent_anniv = hire_date then 0
    end
  from scpp.xfm_pto) x
where w.employee_number = x.employee_number
  and w.year_month = x.year_month  

  select a.*, 
    --c.code_id, c.amount, c.description,
    b.check_year, b.check_month,
    ((2000 + b.check_year)::text || right(trim('0' || check_month::text), 2))::integer as check_year_month, 
    (check_month || '-' || check_day || '-' || (2000 + check_year))::date as check_date
  from scpp.sales_consultants a
  left join dds.ext_pyhshdta b on a.employee_number = b.employee_
--    and b.check_year = 15
  --  and b.check_month = 12
--   left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
--     and b.payroll_run_number = c.payroll_run_number  
where a.employee_number = '181665'

  where b.employee_ is not null
group by a.employee_number, a.last_name, a.first_name


-- from populate_and_backfill.sql
-- update scpp.sales_consultant_configuration
-- set pto_rate = z.pto_rate
-- from (
  select year_month, employee_number, count(*), sum(month_total_gross_pay),
    round(sum(month_total_gross_pay)/(4.3333 * count(*))/5/8, 2) as pto_rate
  from (
    select a.*, b.month_total_gross_pay
    from scpp.pto_intervals a
    left join (
      select b.store_code, b.employee_number, b.full_name, 
        100 * (payroll_ending_year + 2000) + payroll_ending_month as year_month,
        sum(total_gross_pay) as month_total_gross_pay
--         sum(case when payroll_ending_day < 20 then total_gross_pay end) as draw
      from dds.ext_pyhshdta a
      inner join scpp.sales_consultants b on a.employee_ = b.employee_number
      group by b.store_code, b.employee_number, b.full_name, 
        100 * (payroll_ending_year + 2000) + payroll_ending_month) b on a.employee_number = b.employee_number
          and b.year_month between a.from_year_month and a.thru_year_month) x group by year_month, employee_number-- ) z
-- where scpp.sales_consultant_configuration.employee_number = z.employee_number
--   and scpp.sales_consultant_configuration.year_month = z.year_month;

select * from dds.ext_pyhshdta where employee_ = '167600'

      select b.employee_number, b.full_name, 
        100 * (payroll_ending_year + 2000) + payroll_ending_month as year_month,
        sum(total_gross_pay) as month_total_gross_pay
      from dds.ext_pyhshdta a
      -- inner join scpp.sales_consultants b on a.employee_ = b.employee_number
      inner join scpp.xfm_pto
      group by b.store_code, b.employee_number, b.full_name, 
        100 * (payroll_ending_year + 2000) + payroll_ending_month
order by employee_number, year_month        




select * from scpp.xfm_pto

-- this will do for the first batch
-- where none are due pto, ie, rate = 0
-- subsequently this query will need to calculate the rate where applicable
update scpp.xfm_pto w
set pto_rate = x.pto_rate
from (
  select employee_number, year_month 
    case
      when most_recent_anniv = hire_date then 0
    end
  from scpp.xfm_pto) x
where w.employee_number = x.employee_number
  and w.year_month = x.year_month  

need to fix pto_intervals:  
  change interval to dates
  add pto_rate


 alter table scpp.pto_intervals
 add column from_date date,
 add column thru_date date,
 add column most_recent_anniv date,
 add column pto_rate numeric(12,2) default 0;

update scpp.pto_intervals w
set from_date = z.from_date,
    thru_date = z.thru_date,
    most_recent_anniv = z.most_recent_anniv,
    pto_rate = z.pto_rate
from (    
  WITH OPEN_MONTH AS (
    SELECT *
    FROM SCPP.MONTHS
    --WHERE OPEN_CLOSED = 'OPEN')
    WHERE YEAR_MONTH = 201601)
  select a.employee_number, a.year_month, a.from_year_month, a.thru_year_month, 
    (select first_of_month from scpp.months where year_month = a.from_year_month) as from_date,
    (select last_of_month from scpp.months where year_month = a.thru_year_month) as thru_date,
    b.most_recent_anniv, c.pto_rate
  from scpp.pto_intervals a
  left join (
    select a.employee_number, x.most_recent_anniv, c.hire_date
    from scpp.sales_consultant_configuration a
    inner join scpp.sales_consultants c on a.employee_number = c.employee_number
      and c.term_date > (select last_of_month from open_month)
    left join scpp.months d on 1 = 1
      and d.year_month = (select year_month from open_month)
    left join scpp.months e on c.hire_Date between e.first_of_month and e.last_of_month
    left join  lateral (
      select max(thedate) as most_recent_anniv
      from dds.day
      where monthofyear = extract(month from c.hire_Date)
        and dayofmonth = extract(day from c.hire_date)
        and thedate < (
          select first_of_month
          from scpp.months
          where year_month = (
              select year_month
              from open_month))) x on 1 =1
    where a.year_month = (select year_month from open_month)) b on a.employee_number = b.employee_number
  left join scpp.sales_consultant_data c on a.employee_number = c.employee_number
    and c.year_month = (select year_month from open_month)) z
where w.employee_number = z.employee_number
  and w.year_month = z.year_month;    

update scpp.pto_intervals
set pto_rate = 0
where employee_number = '150040';

-- 4/3
select * from scpp.pto_intervals

-- this will do for the first batch
-- where none are due pto, ie, rate = 0
-- subsequently this query will need to calculate the rate where applicable
--update scpp.xfm_pto_intervals w
set pto_rate = x.pto_rate
from (
  select employee_number, year_month 
    case
      when most_recent_anniv = hire_date then 0
    end
  from scpp.xfm_pto_intervals) x
where w.employee_number = x.employee_number
  and w.year_month = x.year_month  


-- okeedokee, let's figure the relevant pto_rate for each of these folks
select employee_number, year_month, total_gross, 
  -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
  round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate
from  ( 
  select a.employee_number, a.year_month, sum(b.total_gross_pay) as total_gross, 
    count(*),
    case
      when count(*) >= 24 then 24
      else count(*)
    end as adj_count 
  from scpp.xfm_pto_intervals a
  left join dds.ext_pyhshdta b on a.employee_number = b.employee_
    and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date 
      between a.pto_period_from and a.pto_period_thru
  where a.most_recent_anniv <> a.hire_date  
  group by a.employee_number, a.year_month) e

select * from scpp.xfm_pto_intervals

-- pto_hours

-- 4/4/ pto_hours good now                 
                  
pto_hours failing on load
issue is trying to load pto hours for year_month that does not yet exist in sc_config

select *
from scpp.ext_pto_hours a

select the_date, employee_number from (
select a.year_month, a.the_date, a.employee_number, a.pto_hours
from scpp.ext_pto_hours a
inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
where a.the_date <= (
  select last_of_month
  from scpp.months
  where open_closed = 'open')
AND not exists (
  select 1
  from scpp.pto_hours
  where the_date = a.the_date
    and employee_number = a.employee_number)
) x group by the_date, employee_number having count(*) > 1    

select *
from scpp.ext_pto_hours
where the_Date = '01/11/2016'
  and employee_number = '1147250'


with open_month as (
  select *
  from scpp.months
  where open_closed = 'open')
select a.year_month, a.the_date, a.employee_number, a.pto_hours
from scpp.ext_pto_hours a
inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
  and b.year_month = (select year_month from open_month)
where a.the_date <= (select last_of_month from open_month)
  AND not exists (
    select 1
    from scpp.pto_hours
    where the_date = a.the_date
      and employee_number = a.employee_number)

select employee_number, year_month, sum(pto_hours) as pto_hours
from scpp.pto_hours
group by employee_number, year_month

update scpp.sales_consultant_data a
set pto_hours = b.pto_hours
from (
  select employee_number, year_month, sum(pto_hours) as pto_hours
  from scpp.pto_hours
  group by employee_number, year_month) b
where a.year_month = b.year_month
  and a.employee_number = b.employee_number

delete from scpp.pto_hours  

select year_month, full_name, pto_rate, pto_hours
from scpp.sales_consultant_data
order by full_name, year_month

-- email
ok, eaxch day the sc_data will get updated with email cap % for the previous
90 days
so the data i am backfilling makes no sense
for s_c_data rows for 201602, these would be the email cap % for the 90 days previous to 2/29/16
so, alter the script accordingly
once we go live then the daily update for transactions between curdate()-90 and curdate() will be ok

select '01/31/2016'::date - 90
select '02/29/2016'::date - 90
select '03/31/2016'::date - 90

select *
from scpp.ext_email_capture

delete from ops.task_log where task = 'ext_email_capture'

with open_month as (
  select *
  from scpp.months
  where open_closed = 'open')


update scpp.sales_consultant_data d
set email_score = e.email_score, 
    email_qualified = e.email_qualified
from (    
  select a.*, b.year_month, 
    case when email_score >= c.metric_value then true else false end as email_qualified
  from ( 
    select employee_number, 
      round(100.0 * sum(case when has_valid_email then 1 else 0 end)/count(*), 2) as email_score
    from scpp.ext_email_capture
    group by employee_number) a
    inner join scpp.months b on 1 = 1
      and b.open_closed = 'open'  
    inner join scpp.metrics c on b.year_month = c.year_month
      and c.metric = 'email capture') e
where d.employee_number = e.employee_number
  and d.year_month = e.year_month    

-- 4/5
-- deals

-- 4/7 as i understand bopmast better, looks like this needs to be redone
-- initial scrape _0131
------------------------------------------------------------------------------------------
--------------- Jan synched take 2--------------------------------------------------------
------------------------------------------------------------------------------------------
drop table if exists scpp.xfm_deals;
CREATE TABLE if not exists scpp.xfm_deals
(
  row_type citext not null,
  run_date date NOT NULL,
  store_code citext NOT NULL,
  stock_number citext NOT NULL,
  vin citext NOT NULL,
  customer_name citext NOT NULL,
  primary_sc citext NOT NULL,
  secondary_sc citext,
  record_status citext,
  date_approved date,
  date_capped date,
  origination_date bigint NOT NULL,
  model_year citext,
  make citext,
  model citext,
  seq integer not null default 1,
  seq_note citext,
  constraint xfm_deals_pk primary key (stock_number,seq));

-- initial seeding of xfm_: the entire initial scrape from 0131
-- select * from scpp.xfm_Deals
-- select * from scpp.ext_deals where record_status <> 'U'
insert into scpp.xfm_deals
select 'New', a.*, 1, 'New Row'
from scpp.ext_deals a;

------------------------------------------------------------------------------------------
--------------- Jan synched take 2--------------------------------------------------------
------------------------------------------------------------------------------------------
select stock_number from scpp.xfm_deals group by stock_number having count(*) > 1
select * from scpp.xfm_deals where run_date <> '01/31/2016'
delete from scpp.xfm_Deals where run_date <> '01/31/2016'
delete from scpp.deals;
select * from scpp.deals where deal_date > '01/31/2016'
select * from scpp.xfm_deals where stock_number = '27308XX'
select * from scpp.ext_deals where date_capped > '01/29/2016'

-- ok, xfm is seeded
-- run ext_ for 0201 : new rows
-- 0202 -> new rows, date_capped changed on 2 deals
-- new rows:

insert into scpp.xfm_deals
select 'New', a.*, 1, 'New Row'
from scpp.ext_deals a
-- where date_capped > '01/31/2016'
--where a.run_date = '02/01/2016'
WHERE not exists (
    select 1
    from scpp.xfm_deals
    where stock_number = a.stock_number);

-- row changes from capped status to not capped
select *
from scpp.ext_deals a
inner join scpp.xfm_deals b on a.stock_number = b.stock_number
where b.record_status = 'U' 
  and coalesce(a.record_status, 'None') <> 'U'

-- sc change
select *
from scpp.ext_deals a
inner join scpp.xfm_deals b on a.stock_number = b.stock_number
-- limit to those rows already in deals
inner join scpp.deals c on b.stock_number = c.stock_number
where (  
    a.primary_sc <> b.primary_sc
    or 
    a.secondary_sc <> b.secondary_sc)
    
-- date_capped change
-- unit_count, year_month, sc don't change: type 1 update with a note
-- rethinking, no type 1 updates, do a new new row, inc seq, add seq_note
-- update scpp.xfm_deals x
-- set date_capped = y.date_capped
insert into scpp.xfm_deals (row_type,run_date,store_code,stock_number,
  vin,customer_name,primary_sc,secondary_sc,record_status,date_approved,
  date_capped,origination_date,model_year,make,model,seq,seq_note)
select 'Update', a.run_date, a.store_code, a.stock_number, a.vin,
  a.customer_name, a.primary_sc, a.secondary_sc, a.record_status,
  a.date_approved, a.date_capped, a.origination_date, a.model_year,
  a.make, a.model,
  (select max(seq) + 1 from scpp.xfm_deals where stock_number = a.stock_number),
  'Changed date capped'
from scpp.ext_deals a
inner join (
  select store_Code, stock_number, vin, customer_name, primary_sc, secondary_sc,
  record_Status, date_approved, date_capped, seq
  from scpp.xfm_deals x
  where seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = x.stock_number)) b on a.stock_number = b.stock_number
-- limit to those rows already in deals
inner join (
  select stock_number
  from scpp.deals
  group by stock_number) c on b.stock_number = c.stock_number
where a.date_capped <> b.date_capped
  and a.primary_sc = b.primary_sc
  and a.secondary_sc = b.secondary_sc
  and extract(year from a.date_capped) = extract(year from b.date_capped)
  and extract(month from a.date_capped) = extract(month from b.date_capped);

-- any change
select *
-- select *
from scpp.ext_deals a
inner join (
  select store_Code, stock_number, vin, customer_name, primary_sc, secondary_sc,
  record_Status, date_approved, date_capped, seq
  from scpp.xfm_deals x
  where seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = x.stock_number)) b on a.stock_number = b.stock_number
-- limit to those rows already in deals
inner join (
  select stock_number
  from scpp.deals
  group by stock_number) c on b.stock_number = c.stock_number
where (a.primary_sc <> b.primary_sc or a.secondary_sc <> b.secondary_sc or a.vin <> b.vin 
  or a.record_status <> b.record_status or a.date_approved <> b.date_approved 
  or a.date_capped <> b.date_capped)  



-- date_capped change
-- unit_count, year_month, sc DO change: FLAG MAINTENANCE

-------------------------------------------------------------------------------------------------------
----------- Load --------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- 0202 new deals
-- exclude garceau and olderbak deals
insert into scpp.deals (year_month,employee_number,stock_number,unit_count,
  deal_date,customer_name,model_year,make,model,deal_source,seq)
select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer, 
  coalesce(b.employee_number, 'House'), a.stock_number, 
  case
    when secondary_sc = 'None' then 1
    else .5
  end as unit_count,
  a.date_capped, a.customer_name, 
  a.model_year, a.make, a.model, 'Dealertrack', 1
from scpp.xfm_deals a
-- inner join, RY1 only: FK constraint ref s_c_config
inner join scpp.sales_consultants b on 
  case
    when a.store_code = 'RY1' then a.primary_sc = b.ry1_id
    when a.store_code = 'RY2' then a.primary_sc = b.ry2_id
  end 
  and b.employee_number not in ('150040','1106225')
where row_type = 'New'
  AND a.date_capped > '12/31/2015'
  AND not exists (
    select *
    from scpp.deals
    where stock_number = a.stock_number)
union 
select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer, 
  coalesce(b.employee_number, 'House'), a.stock_number, 
  0.5 as unit_count,
  a.date_capped, a.customer_name, 
  a.model_year, a.make, a.model, 'Dealertrack', 1
from scpp.xfm_deals a
inner join scpp.sales_consultants b on 
  case
    when a.store_code = 'RY1' then a.secondary_sc = b.ry1_id
    when a.store_code = 'RY2' then a.secondary_sc = b.ry2_id
  end 
  and b.employee_number not in ('150040','1106225')
where a.secondary_sc <> 'None' 
  and a.row_type = 'New'
  AND a.date_capped > '12/31/2015'
  AND not exists (
    select *
    from scpp.deals
    where stock_number = a.stock_number);  

--select * from scpp.xfm_deals where run_date = '02/01/2016'
-- 0202 updates

insert into scpp.deals (year_month,employee_number,stock_number,unit_count,
  deal_date,customer_name,model_year,make,model,deal_source,seq, notes)
select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer, 
  coalesce(b.employee_number, 'House'), a.stock_number, 
  0 as unit_count,
  a.date_capped, a.customer_name, 
  a.model_year, a.make, a.model, 'Dealertrack', a.seq, a.seq_note
from scpp.xfm_deals a
-- inner join, RY1 only: FK constraint ref s_c_config
inner join scpp.sales_consultants b on 
  case
    when a.store_code = 'RY1' then a.primary_sc = b.ry1_id
    when a.store_code = 'RY2' then a.primary_sc = b.ry2_id
  end 
  and b.employee_number not in ('150040','1106225')
where row_type = 'update'
  and run_date = '02/01/2016'
union 
select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer, 
  coalesce(b.employee_number, 'House'), a.stock_number, 
  0 as unit_count,
  a.date_capped, a.customer_name, 
  a.model_year, a.make, a.model, 'Dealertrack', a.seq, a.seq_note
from scpp.xfm_deals a
inner join scpp.sales_consultants b on 
  case
    when a.store_code = 'RY1' then a.secondary_sc = b.ry1_id
    when a.store_code = 'RY2' then a.secondary_sc = b.ry2_id
  end 
  and b.employee_number not in ('150040','1106225')
where row_type = 'update'
  and run_date = '02/01/2016';

select * from scpp.deals where stock_number in ('22545xxz','25806b')

select * from scpp.ext_deals a where date_capped > '01/31/2016' 

select * from scpp.xfm_deals where stock_number in ('26309','27618', 'H8728')

so why the fuck are these not being updatedated