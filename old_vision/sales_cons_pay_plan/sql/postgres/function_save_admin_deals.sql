﻿-- Function: scpp.save_admin_deals(integer, citext, numeric, date, citext, citext, citext, citext, citext)

-- DROP FUNCTION scpp.save_admin_deals(integer, citext, numeric, date, citext, citext, citext, citext, citext);

CREATE OR REPLACE FUNCTION scpp.save_admin_deals(
    _id integer,
    _stock_number citext,
    _unit_count numeric,
    _deal_date date,
    _customer_name citext,
    _model_year citext,
    _make citext,
    _model citext,
    _notes citext)
  RETURNS SETOF json AS
$BODY$
/*
add new row to tmp_s_c_deals
update tmp_s_c_date: 
  unit_count_ovr
  unit_count
  per_unit
  unit_count_x_per_unit
  total_pay
  notes
return list of deals

-- 2/22/16 need to update notes as well
-- 2/24 notes are in a separate table (admin_notes)
-- 3/3 deals.seq
-- 3/14 replace ovr with Override
-- 5/31/16 modify total_pay calc to include new hire pro-rate
-- 8/8/16 use scpp.get_total_pay() to update tmp_s_c_data.total_pay
use arden, 4 deals in january, id = 162
bryan seay, 10 deals, see the per_unit change id = 166

delete from scpp.tmp_s_c_data;
delete from scpp.tmp_s_c_deals;
delete from scpp.tmp_admin_notes;

select * from scpp.get_consultant_profile(159);

select *
from scpp.sales_consultant_configuration a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number
where id = 159

delete from scpp.tmp_s_c_deals where deal_source = 'Override';

-- alter an existing deal"
select scpp.save_admin_deals(162,'27430X',-.5,'01/05/2016','CORNEILLIE, DEREK GENE','2007','CHEVROLET','SILVERADO 1550', 'no really, it is a shared deal') 


select scpp.save_admin_deals(162, '666a',.5,'01/18/2016','NEWPHEW, JAMES, DAVID','1997','CHEVROLET','SILVERADO 1550', 'fixed 666a oops should have been positive') 

select *
from scpp.tmp_s_c_deals

select * from scpp.get_consultant_profile(144);

select scpp.save_admin_deals(144,'27110A',-.5,'2016-01-18','HEITMANN, PAUL MATTHEW','2004','BUICK','LESABRE','')


*/
begin
-- hmmm, along with the notion of a gl type recording of data, just as we don't
-- delete any rows, but enter a new row with the unit_count change and a new seq,
-- perhaps we do the same here, don't update the row, add a new one
-- still need the 2 steps if, 
-- in the update, the only attribute with a new value is unit_count, and subsequently a new seq
if exists (
    select 1
    from scpp.tmp_s_c_deals
    where stock_number = _stock_number)
--      and deal_source = 'Override') 
  then
--     update scpp.tmp_s_c_deals
--     set unit_count = _unit_count
--     where stock_number = _stock_number
--       and deal_source = 'Override';
    insert into scpp.tmp_s_c_deals(year_month,employee_number,stock_number,unit_count,deal_date,customer_name,
      model_year,make,model,deal_source, seq)
    select a.year_month, a.employee_number, _stock_number, _unit_count, _deal_date,
      _customer_name, _model_year, _make, _model, 'Override', 
      (
        select max(seq) + 1 
        from scpp.tmp_s_c_deals 
        where year_month = a.year_month
          and employee_number = a.employee_number
          and stock_number = _stock_number)
    from scpp.sales_consultant_configuration a
    where id = _id;       
else
  insert into scpp.tmp_s_c_deals(year_month,employee_number,stock_number,unit_count,deal_date,customer_name,
    model_year,make,model,deal_source, seq)
  select a.year_month, a.employee_number, _stock_number, _unit_count, _deal_date,
    _customer_name, _model_year, _make, _model, 'Override', 
-- if the stocknumber does not exist, seq = 1
    1 as seq
--     (
--       select max(seq) + 1 
--       from scpp.deals 
--       where year_month = a.year_month
--         and employee_number = a.employee_number
--         and stock_number = _stock_number)
  from scpp.sales_consultant_configuration a
  where id = _id; 
end if;    
update scpp.tmp_s_c_data
  set unit_count_ovr = (select sum(unit_count) from scpp.tmp_s_c_deals where deal_source = 'Override'),
      unit_count = (select sum(unit_count) from scpp.tmp_s_c_deals);
update scpp.tmp_s_c_data      
  set per_unit = (
    select 
      case
        when z.metrics_qualified then (
          select tier_2
          from scpp.per_unit_matrix a
          where year_month = z.year_month
            AND units @> floor(coalesce(z.unit_count,0))::integer
            and pay_plan_name = z.pay_plan_name) 
        else (
          select tier_1
          from scpp.per_unit_matrix a
          where year_month = z.year_month
            and units @> floor(coalesce(z.unit_count,0))::integer
            and pay_plan_name = z.pay_plan_name) 
        end as per_unit  
    from scpp.tmp_s_c_data z); 
    
update scpp.tmp_s_c_data
set unit_count_x_per_unit = unit_count * per_unit;

update scpp.tmp_s_c_data
-- passing 1, '1', 'tmp' returns total pay based on data in scpp.tmp_s_c_data
set total_pay = (
  select total_pay
  from scpp.get_total_pay(1,'1','tmp'));

if length(trim(_notes)) > 0 then
  insert into scpp.tmp_admin_notes (year_month, employee_number, the_date, note)
  select year_month, employee_number, current_date, _notes
  from scpp.sales_consultant_configuration
  where id = _id;
end if; 
return query    
select row_to_json (c)
from (
  select _id as id, b.*,
    (
      select coalesce(array_to_json(array_agg(row_to_json(a))), '[]')
      from (
        select the_date, note
        from scpp.tmp_admin_notes
      ) a  
    ) as notes,
    (
      select coalesce(array_to_json(array_agg(row_to_json(d))), '[]')
      from (
        select stock_number, unit_count,deal_date, 
          customer_name, model_year,make,model,deal_source
        from scpp.tmp_s_c_deals
      ) d
    ) as deals
  from scpp.tmp_s_c_data b
) as c; 
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION scpp.save_admin_deals(integer, citext, numeric, date, citext, citext, citext, citext, citext)
  OWNER TO rydell;
