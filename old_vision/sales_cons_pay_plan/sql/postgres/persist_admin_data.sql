﻿DROP FUNCTION if exists scpp.persist_admin_data(integer,boolean);

create or replace function scpp.persist_admin_data(
  _id integer,
  _complete boolean)
returns text as  
$BODY$
/*
updates sales_consultant_configuration with values from tmp_s_c_data
  attributes to update:
    additional_comp, unpaid_time_off,
    csi_score, csi_qualified,
    logged_score, logged_qualified,
    auto_alert_score, auto_alert_score,
    metrics_qualified, 
    guarantee, per_unit, 
    unit_count_ovr, unit_count
    notes,
    complete

inserts rows into deals from tmp_s_c_deals where deal_source = ovr
*/
declare _success text;
begin
update sales_consultant_configuration x
set additional_comp = z.additional_comp, 
    unpaid_time_off = z.unpaid_time_off,
    csi_score = z.csi_score, 
    csi_qualified = z.csi_qualified,
    logged_score = z.logged_score, 
    logged_qualified = z.logged_qualified,
    auto_alert_score = z.auto_alert_score, 
    auto_alert_qualified = z.auto_alert_qualified,
    metrics_qualified = z.metrics_qualified, 
    guarantee = z.guarantee, 
    per_unit = z.per_unit, 
    unit_count_ovr = z.unit_count_ovr, 
    unit_count = z.unit_count,
    notes = z.notes,
    complete = _complete
from tmp_s_c_data z
where x.id = z.id;  

if found then 
  _success := 'Pass';
else 
  _success := 'Fail';  
end if ;

end;
$BODY$
language plpgsql;
