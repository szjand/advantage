﻿-- Function: scpp.update_sales_consultant_configuration(integer, text, integer, integer, text)

DROP FUNCTION if exists scpp.save_admin_edit_state(integer, numeric, integer, numeric, numeric, numeric, citext);

CREATE OR REPLACE FUNCTION scpp.save_admin_edit_state(
    _id integer,
    _additional_comp numeric(12,2),
    _unpaid_time_off integer,
    _csi_score numeric(6,2),
    _logged_score numeric(6,2),
    _auto_alert_score numeric(6,2),
    _notes citext)   
RETURNS setof json as
$BODY$
/*
this will be used to update tmp_s_c_data 
updates: additional_comp, unpaid_time_off, 
  csi_score, csi_qualified
  logged_score, logged_qualified
  auto_aler_score, auto_alert_qualified
  metrics_qualified,
  guarantee, per_unit, notes

--update scpp.tmp_s_c_data set notes = null;
  
select scpp.save_admin_edit_state(162,0,0,0,0,0,'note 4')

*/
declare _year_month integer;
        _csi_metric numeric(6,2);
        _logged_metric numeric(6,2);
        _auto_alert_metric numeric(6,2);
begin        
_year_month := (
  select year_month 
  from scpp.months 
  where open_closed = 'open');
_csi_metric := (
  select metric_value
  from scpp.metrics
  where year_month = _year_month
    and metric = 'csi');
_logged_metric := (
  select metric_value
  from scpp.metrics
  where year_month = _year_month
    and metric = 'Logged Opportunity Minimum');    
_auto_alert_metric := (
  select metric_value
  from scpp.metrics
  where year_month = _year_month
    and metric = 'Auto Alert Calls per Month');        
update scpp.tmp_s_c_data
set additional_comp = _additional_comp,
    unpaid_time_off = _unpaid_time_off,
    csi_score = _csi_score,
    csi_qualified = case when _csi_score >= _csi_metric then true else false end,
    logged_score = _logged_score,
    logged_qualified = case when _logged_score >= _logged_metric then true else false end,
    auto_alert_score = _auto_alert_score,   
    auto_alert_qualified = case when _auto_alert_score >= _auto_alert_metric then true else false end;
update scpp.tmp_s_c_data
set metrics_qualified = ( 
  select 
    case
      when csi_qualified AND email_qualified AND logged_qualified AND auto_alert_qualified then true
      else false
    end
  from scpp.tmp_s_c_data);
update scpp.tmp_s_c_data
set guarantee = (
  select 
      case
        when a.full_months_employment < 4 then c.guarantee_new_hire
        when a.metrics_qualified = true then c.guarantee_with_qual
        else c.guarantee
      end as guarantee
  from scpp.tmp_s_c_data a
  inner join scpp.pay_plans b on a.year_month = b.year_month
    and a.pay_plan_name = b.pay_plan_name
  inner join scpp.guarantee_matrix c on b.pay_plan_name = c.pay_plan_name
    and b.year_month = c.year_month);   
update scpp.tmp_s_c_data
set per_unit = (
  select 
    case
      when z.metrics_qualified then (
        select tar
        from scpp.per_unit_matrix a
        where year_month = z.year_month
          AND units @> floor(coalesce(z.unit_count,0))::integer
          and pay_plan_name = z.pay_plan_name) 
      else (
        select map
        from scpp.per_unit_matrix a
        where year_month = z.year_month
          and units @> floor(coalesce(z.unit_count,0))::integer
          and pay_plan_name = z.pay_plan_name) 
      end as per_unit  
  from scpp.tmp_s_c_data z);   
if length(trim(_notes)) > 0 then
  update scpp.tmp_s_c_data
  set notes = (
    select 
      case 
        when length(trim(a.notes)) > 0 then 
          concat(a.notes,chr(10), chr(13), (select to_char(current_date, 'MM/DD/YYYY')) || ': ' || _notes ||'. ')
        else (select to_char(current_date, 'MM/DD/YYYY')) || ': ' || _notes ||'. '
      end  
    from scpp.tmp_s_c_data a); 
end if;             
return query
select row_to_json (a)
from (
  select *
  from scpp.tmp_s_c_data) a;
end;
 
$BODY$
  LANGUAGE plpgsql;