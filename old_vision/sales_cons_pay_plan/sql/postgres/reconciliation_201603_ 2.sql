﻿

select b.last_name, b.first_name, b.employee_number, sum(a.unit_count)
from scpp.deals a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number
where a.deal_Date between '03/01/2016' and '03/31/2016'  
group by b.last_name, b.first_name, b.employee_number    
order by last_name

/*
select *
from (
  select x.*, sum(unit_count) over(partition by last_name order by last_name) as total
  from (
    select b.last_name, b.first_name, b.employee_number, a.stock_number, a.unit_count, a.deal_Date, a.customer_name
    from scpp.deals a
    inner join scpp.sales_consultants b on a.employee_number = b.employee_number
    where a.deal_Date between '03/01/2016' and '03/31/2016'
      and b.employee_number = '1148583') x) s
full outer join (
  select *
  FROM scpp.s_c_spreadsheet_data_201603 
  where employee_number = '1148583') t on s.stock_number = t.stock_number
*/

for ben: 
select *
from (
  select last_name, first_name, stock_number, unit_count, deal_date, customer_name, sum(unit_count) over(partition by last_name order by last_name) as total
  from (
    select b.last_name, b.first_name, b.employee_number, a.stock_number, a.unit_count, a.deal_Date, a.customer_name
    from scpp.deals a
    inner join scpp.sales_consultants b on a.employee_number = b.employee_number
    where a.deal_Date between '03/01/2016' and '03/31/2016') x) s
order by last_name, deal_date    

      
-- aschnewitz GOOD

-- atkinson
scss: 15  vision 14

  27310XXA in arkona as logan carter deal:  scss -1
  scss 15 - 1 = 14
  vision = 14


 -- carlson GOOD 
 
 -- carter 
 scss: 13  vision: 13
 
  27432A in arkona as Preston/Carter scss -.5
  27379A in arkona as Preston/Carter scss +.5
  scss: 13 + .5 - .5 = 13
  vision: = 13

 -- chavez 
 scss: 6.5  vision 6

  H8688A: in arkona as GMSALES   vision + 1
  27118A: in arkona Trosen/Chavez  scss + .5
  scss: 6.5 + .5 = 7
  vision: 6 + 1 = 7
  
-- croaker GOOD

-- Davidson  
scss: 8   vision: 7.5

  H8851: in arkona as HSE       +1 vision      
  27752XA: in arkona as panzer/davidson  +.5 scss
  scss: 8 + .5 = 8.5
  vision:  7.5 + 1 = 8.5

-- dockendorf GOOD

-- eden GOOD
  
-- emmons GOOD

-- flaat
scss: 20.5   vision 17
--  i do not have steve's december paysheet

-- foster Good

-- garceau (joe) 
scss: 16   vision: 15
  27972: accpt 3/31
         accpt 4/6
         capped 4/13
         the accounting entry is dated 4/7
  scss 16 - 1 = 15
  vision = 15       
    
-- hanson  Good
  
-- homstad Good

-- johnson, brandon Good

-- johnson, eric  Good

-- kvasager Good

-- loven  Good

-- olson Good

-- panzer
scss: 14   vision: 13.5
  27752XA: in arkona as panzer/davidson  -.5 scss
  scss: 14 - .5 = 13.5
  vision = 13.5
  
-- pearson 
scss: 21.5   vision: 20.5

  H8773 in arkona as GMC  vision: +1
  Kueber typo in scss (27499 vs 27493): no change
  scss = 21.5
  vision: 20.5 + 1 = 21.5
  
-- preston
scss: 7   vision: 6
  H8733A in arkona as GMC: vision +1
  27432A in arkona as Preston/Carter scss +.5
  27379A in arkona as Preston/Carter scss -.5
  scss: 7 + .5 - .5 = 7
  vision: 6 + 1 = 7
  
-- seay
scss: 16  vision: 15.5
  26673 in arkona as Seay/Trosen   scss -.5
  scss: 16 - .5 = 15.5
  vision = 15.5

-- stadstad Good
  
-- tarr Good

-- trosen Good

-- warmack  Good

-- wheeler
scss: 12  vision 9.5
  H8681A in arkona as HSE  vision +1
  H5994B in arkona as HSE  vision +1
  224949C in arkona as Wheeler/Eden scss - .5
  scss: 12 - .5 = 11.5
  vision 9.5 + 2 = 11.5

 