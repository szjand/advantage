﻿select 
--  (select id from scpp.sales_consultant_configuration where employee_number = a.employee_number and year_month = 201602),
  b.last_name, b.first_name, a.employee_number, a.hire_date, a.pto_rate,a.pto_hours,a.pto_pay,a.guarantee,
  a.unit_count,a.per_unit,a.unit_count_x_per_unit,a.total_pay, a.full_months_employment
from scpp.sales_Consultant_data a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number
where year_month = 201703
order by a.full_months_employment

update scpp.sales_consultant_data
set additional_comp = 0,
    unpaid_time_off = 0
where year_month = 201604

select (select id from scpp.sales_consultant_configuration where employee_number = a.employee_number and year_month = 201602),
a.full_name, a.employee_number, hire_date, pto_rate,pto_hours,pto_pay,guarantee,a.unit_count,per_unit,unit_count_x_per_unit,
total_pay
from scpp.sales_Consultant_data a
left join scpp.deals b on a.employee_number = b.employee_number
  and b.year_month = 201703
where a.year_month = 201703
order by a.employee_number

-- last month - include adjustments/qualifiers
select 
--  (select id from scpp.sales_consultant_configuration where employee_number = a.employee_number and year_month = 201602),
  b.last_name, b.first_name, a.employee_number, a.hire_date, a.pto_rate,a.pto_hours,a.pto_pay,a.guarantee,
  a.unit_count,a.per_unit,a.unit_count_x_per_unit,a.total_pay,
  additional_comp, unpaid_time_off, csi_qualified, email_qualified,
  logged_qualified, auto_alert_qualified, metrics_qualified
from scpp.sales_Consultant_data a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number
where year_month = 201604
order by b.last_name

-- deals
select x.*, sum(unit_count) over(partition by last_name order by last_name) as total
from (
  select b.last_name, b.first_name, b.employee_number, a.stock_number, a.unit_count, a.deal_Date, a.customer_name
  from scpp.deals a
  inner join scpp.sales_consultants b on a.employee_number = b.employee_number
  where a.year_month = 201703
  order by b.last_name, stock_number) x

  
-- pay for ben reconcile
select 
--  (select id from scpp.sales_consultant_configuration where employee_number = a.employee_number and year_month = 201602),
  b.last_name, b.first_name, a.employee_number, a.hire_date, a.pto_rate,a.pto_hours,a.pto_pay,a.guarantee,
  a.unit_count,a.per_unit,a.unit_count_x_per_unit,a.total_pay
from scpp.sales_Consultant_data a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number
where year_month = 201603
order by b.last_name



