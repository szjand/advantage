﻿
select x.stock_number, 
  case
    when x.store_code = 'RY1' then
      (select employee_number from scpp.sales_consultants where ry1_id = x.primary_sc)
    when x.store_code = 'RY2' then 
      (select employee_number from scpp.sales_consultants where ry2_id = x.primary_sc)
    end as ps,
    'Primary',
    y.seq as deal_seq
from (
  select *
  from scpp.xfm_deals 
  where run_date = (select max(xd.run_date) from scpp.xfm_Deals xd)
    and row_type = 'update') x
inner join scpp.deals y on x.stock_number = y.stock_number
  and y.seq = (
    select max(g.seq)
    from scpp.deals g
    where g.stock_number = y.stock_number) 
WHERE x.status_change = 'No Change'
  and x.sc_change <> 'No Change'
  and y.deal_status = 'Capped' 
union
select x.stock_number, 
  case
    when x.store_code = 'RY1' then
      (select employee_number from scpp.sales_consultants where ry1_id = x.secondary_sc)
    when x.store_code = 'RY2' then 
      (select employee_number from scpp.sales_consultants where ry2_id = x.secondary_sc)
    end as sc,
    'Secondary', 
    y.seq
from (
  select *
  from scpp.xfm_deals 
  where run_date = (select max(xd.run_date) from scpp.xfm_Deals xd)
    and row_type = 'update') x
inner join scpp.deals y on x.stock_number = y.stock_number
  and y.seq = (
    select max(g.seq)
    from scpp.deals g
    where g.stock_number = y.stock_number) 
WHERE x.status_change = 'No Change'
  and x.sc_change <> 'No Change'
  and y.deal_status = 'Capped' 


 
select y.year_month, b.employee_number, y.stock_number, 
  case
    when secondary_sc = 'None' then 1
    else .5
  end as unit_count,
  y.deal_date, y.customer_name, y.model_year, y.make, y.model,
  y.deal_source, y.seq, y.notes, y.deal_status, x.run_date  
from (
  select *
  from scpp.xfm_deals 
  where run_date = (select max(xd.run_date) from scpp.xfm_Deals xd)
    and row_type = 'update') x
inner join scpp.deals y on x.stock_number = y.stock_number
  and y.seq = (
    select max(g.seq)
    from scpp.deals g
    where g.stock_number = y.stock_number) 
inner join scpp.sales_consultants b on
  case
    when x.store_code = 'RY1' then x.primary_sc = b.ry1_id
    when x.store_code = 'RY2' then x.primary_sc = b.ry2_id
  end    
  and b.employee_number not in ('150040','1106225')    
WHERE x.status_change = 'No Change'
  and x.sc_change <> 'No Change'
  and y.deal_status = 'Capped'
union  
select y.year_month, b.employee_number, y.stock_number, 
  0.5 as unit_count,
  y.deal_date, y.customer_name, y.model_year, y.make, y.model,
  y.deal_source, y.seq, y.notes, y.deal_status, x.run_date  
from (
  select *
  from scpp.xfm_deals 
  where run_date = (select max(xd.run_date) from scpp.xfm_Deals xd)
    and row_type = 'update') x
inner join scpp.deals y on x.stock_number = y.stock_number
  and y.seq = (
    select max(g.seq)
    from scpp.deals g
    where g.stock_number = y.stock_number) 
inner join scpp.sales_consultants b on
  case
    when x.store_code = 'RY1' then x.secondary_sc = b.ry1_id
    when x.store_code = 'RY2' then x.secondary_sc = b.ry2_id
  end    
  and b.employee_number not in ('150040','1106225')    
WHERE x.status_change = 'No Change'
  and x.sc_change <> 'No Change'
  and y.deal_status = 'Capped'

