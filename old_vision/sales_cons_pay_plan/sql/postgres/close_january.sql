﻿ 
-- ok, no s_c_config rows for feb
select a.full_name, c.*
from scpp.sales_Consultants a
inner join scpp.months b on b.open_closed = 'open'
  and a.hire_date > b.last_of_month
left join scpp.sales_Consultant_configuration c on a.employee_number = c.employee_number
  and b.year_month = c.year_month  

/*
delete sales_consultants hired in feb
shit no need to do that, suppose payroll doesn't get done until the 2nd, but there
were consultants hired on the first, 

or, the nightly consultant update does not add any rows to until the current month is the open month
so, payroll closed on the 2nd, new consultants hired on first, as a result of nightly, they
will exist in sales_consultants, but not in s_c_config.

eg nightly: if open month is not current month (payroll not done yet) add new rows to
scpp.sales_consultants but no other processing
*/
delete
from scpp.sales_Consultants
where employee_number in (
  select employee_number
  from scpp.sales_Consultants a
  inner join scpp.months b on b.open_closed = 'open'
    and a.hire_date > b.last_of_month);


update scpp.sales_consultant_data
set complete = true
where year_month = (
  select year_month
  from scpp.months
  where open_closed = 'open');


  