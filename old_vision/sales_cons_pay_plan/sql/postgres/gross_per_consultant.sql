﻿-- Jon need your help pulling some numbers together.  Need to know for each sales consultant for Sept 2016, Oct 2016, Dec 2016, 
-- and Jan 2017 (I skipped Nov 2016 on purpose.) How many units they sold, how much total front gross on those units, 
-- how much total F&I on those units, and what was there comp for that month.  I have attached an example spreadsheet 
-- if that helps.  Since we have salespeople that sell at both stores we need to count gross generated for either store 
-- in the total for that consultant.  Thanks man!
-- 
-- Ben

-- 2/6/17
-- I am wondering if it is now possible by sales consultant to figure there draw pay, commission, PTO, 
-- Lease Money, spiff pay or other pay.  You can see in the attached report on the far right how I 
-- took each consultant and just summed the totals they earned over the 4 month period.  I need to 
-- break those sums out, not month by month just a total.  For the 4 month period Scott Pearson earned 
-- X in draw, commission, lease, etc.

select b.full_name, a.year_month, a.unit_count, a.stock_number, c.termdate
from scpp.deals a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number 
left join ads.ext_dds_edwEmployeeDim c on b.employee_number = c.employeenumber
where a.year_month in (201609,201610,201612,201701)
  and c.termdate < current_date


select distinct b.full_name
from scpp.deals a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number 
left join ads.ext_dds_edwEmployeeDim c on b.employee_number = c.employeenumber
  and currentrow = true
where a.year_month in (201609,201610,201612,201701)
  and c.termdate < current_date

-- drop table scs;
create temp table scs as
select * 
from (
  select a.employeenumber, a.lastname, a.firstname, a.salespersonid as ry1_id, c.salespersonid as ry2_id
  from ads.dim_salesperson a
  inner join ads.ext_dds_edwEmployeeDim b on a.employeenumber = b.employeenumber
    and b.currentrow = true
    and b.distcode = 'SALE'
    and b.termdate > current_date
  left join ads.dim_salesperson c on a.employeenumber = c.employeenumber
    and c.storecode = 'RY2' 
  where a.salespersontypecode = 'S'
    and a.active = true
    and a.employeenumber like '1%'
    and b.termdate > current_date 
    and a.storecode = 'RY1'
    and a.lastname not in ('wheeler','gable')) e
full outer join (    
  select year_month
  from scpp.months
  where year_month in (201609,201610,201612,201701)) f on 1 = 1

select * from ads.ext_dds_edwEmployeeDim where employeenumber = '181665'

select * from ads.ext_dds_edwEmployeeDim where termdate between current_date and '3000-01-01'::date

update ads.ext_dds_edwEmployeeDim set termdate = '2016-10-06' where employeenumber = '181665'

select * from scs

-- pay (from intranet sqlscripts\sales_cons_pay_plan\sql\actual_pay_vs_system_pay.sql)
-- don't care about system pay, alter it to just use a list of all consultants
-- select e.full_name, e.employee_number, e.year_month, e.unit_count, e.total_pay as vision_total_pay, 
--   f.arkona_comm_draw, f.arkona_total_gross, f.cat
-- from scpp.sales_consultant_data e
drop table compensation;
create temp table compensation as
select e.employeenumber, e.lastname, e.firstname, e.ry1_id, e.ry2_id, e.year_month, 
  f.arkona_comm_draw, f.arkona_total_gross, f.cat
from scs e
left join ( -- f
  select m.*, n.arkona_comm_draw, n.cat
  from ( -- m  pyhshdta
    select a.employee_, check_month, (2000 + check_year)* 100 + check_month as year_month, sum(a.total_gross_pay) as arkona_total_gross
    from dds.ext_pyhshdta a
    where trim(a.distrib_code) = 'SALE'
--       and a.payroll_cen_year in (116, 117)
--       and a.payroll_ending_month in (9,10,12,1)
      and (
        (a.payroll_ending_year = 16 and payroll_ending_month in (9,10,12))
        or
        (a.payroll_ending_year = 17 and payroll_ending_month = 1))
      and a.company_number = 'RY1'
    group by a.employee_, check_month, check_year) m
  left join ( -- n  pyhscdta
    select employee_, check_month, 
      sum(case when q.code_id in ('79','78') then amount end) as arkona_comm_draw,
      string_agg(q.description || ':' || q.amount::citext, ' | ' order by q.description) as cat
    from ( -- q
      select a.employee_name, a.employee_, a.check_month, (2000 + a.check_year) + check_month as year_month,
        b.description, sum(b.amount) as amount, b.code_id
      --select distinct c.description
      from dds.ext_pyhshdta a
      inner join dds.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
        and a.company_number = b.company_number
        and a.employee_ = b.employee_number
        and b.code_type in ('0','1') -- 0,1: income, 2: deduction
      where trim(a.distrib_code) = 'SALE'
--       and a.payroll_cen_year in (116, 117)
--       and a.payroll_ending_month in (9,10,12,1)
      and (
        (a.payroll_ending_year = 16 and a.payroll_ending_month in (9,10,12))
        or
        (a.payroll_ending_year = 17 and a.payroll_ending_month = 1))
        and a.company_number = 'RY1'
      group by a.employee_name, a.employee_, a.check_month, a.check_year, b.description, b.code_id) q
    group by  employee_, check_month) n on m.employee_ = n.employee_ and m.check_month = n.check_month) f on e.employeenumber = f.employee_
      and e.year_month = f.year_month


select * from compensation

-- comp and count
select a.year_month, a.employeenumber, a.lastname, a.firstname, a.ry1_id, a.ry2_id, a.arkona_total_gross as total_gross_pay, a.cat as pay_cat, 
  coalesce(b.units, c.units) 
-- select *
from compensation a
left join (
  select b.employee_number, a.year_month, sum(a.unit_count) as units
  from scpp.deals a
  inner join scpp.sales_consultants b on a.employee_number = b.employee_number 
  where a.year_month in (201609,201610,201612,201701)
  group by b.employee_number, a.year_month) b on a.employeenumber = b.employee_number
    and a.year_month = b.year_month
left join other_deals c on a.employeenumber = c.employee_number
  and a.year_month = c.year_month   


-- drop table other_deals;
-- good enuf
create temp table other_deals as
select year_month, employee_number, count(*) as units
from (
  select b.year_month, 
    case 
      when a.primary_sc in ('JKO','JOL') then '1106225' 
      when a.primary_sc = 'CGA' then '150040'
    end as employee_number
  from scpp.ext_deals a
  inner join dds.dim_date b on a.date_capped = b.the_date
    and b.year_month in (201609,201610,201612,201701)
  where (
    (primary_sc in ('CGA','JOL','JKO'))
    or
    (secondary_sc in ('CGA','JOL','JKO')))) x
group by year_month, employee_number

-- need stocknumbers for gross
-- vision deals
-- need unit count
drop table all_deals;
create temp table all_deals as 
select employee_number, year_month, stock_number, sum(unit_count) as unit_count
from (
  select b.employee_number, a.year_month, stock_number, unit_count
  from scpp.deals a
  inner join scpp.sales_consultants b on a.employee_number = b.employee_number 
  where a.year_month in (201609,201610,201612,201701)) x
group by employee_number, year_month, stock_number
having sum(unit_count) > 0  
union  
-- other deals
select 
    case 
      when a.primary_sc in ('JKO','JOL') then '1106225' 
      when a.primary_sc = 'CGA' then '150040'
    end as employee_number, b.year_month, a.stock_number, 1 as unit_count
  from scpp.ext_deals a
  inner join dds.dim_date b on a.date_capped = b.the_date
    and b.year_month in (201609,201610,201612,201701)
  where (
    (primary_sc in ('CGA','JOL','JKO'))
    or
    (secondary_sc in ('CGA','JOL','JKO'))) 

select * from all_deals

-- need to get sales & f/i gross & cogs accounts
-- what are the new car gross accocunts
drop table sale_accounts;
create temp table sale_accounts as
select distinct d.gl_account, e.account_type
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page between 5 and 15 -- new cars
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
union
-- used car gross accounts
select distinct d.gl_account, e.account_type
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account

select * from sale_accounts

-- f/i accounts
drop table fi_accounts;
create temp table fi_accounts as
select distinct d.gl_account, e.account_type
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201612
  and b.page = 17
  and b.line between 1 and 20
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account;

select distinct account_type from accounts

--need distinct stocknumbers
-- front_gross
select a.stock_number, -1 * sum(b.amount) as front_gross
from (
  select distinct stock_number
  from all_deals) a
left join fin.fact_gl b on a.stock_number = b.control
inner join fin.dim_Account c on b.account_key = c.account_key
inner join fin.dim_journal d on b.journal_key = d.journal_key
inner join sale_accounts e on c.account = e.gl_account
group by a.stock_number

-- fi_gross
select a.stock_number, -1 * sum(b.amount) as front_gross
from (
  select distinct stock_number
  from all_deals) a
left join fin.fact_gl b on a.stock_number = b.control
inner join fin.dim_Account c on b.account_key = c.account_key
inner join fin.dim_journal d on b.journal_key = d.journal_key
inner join fi_accounts e on c.account = e.gl_account
group by a.stock_number
order by stock_number

drop table gross;
create temp table gross as
select employee_number, year_month, round(sum(front_gross), 0) as front_gross, 
  round(sum(fi_gross), 0) as fi_gross
from (
  select a.*, unit_count * b.front_gross as front_gross,  unit_count * coalesce(c.fi_gross, 0) as fi_gross
  from all_deals a
  left join (-- front_gross
    select a.stock_number, -1 * sum(b.amount) as front_gross
    from (
      select distinct stock_number
      from all_deals) a
    left join fin.fact_gl b on a.stock_number = b.control
    inner join fin.dim_Account c on b.account_key = c.account_key
    inner join fin.dim_journal d on b.journal_key = d.journal_key
    inner join sale_accounts e on c.account = e.gl_account
    group by a.stock_number) b on a.stock_number = b.stock_number
  left join (
    select a.stock_number, -1 * sum(b.amount) as fi_gross
    from (
      select distinct stock_number
      from all_deals) a
    left join fin.fact_gl b on a.stock_number = b.control
    inner join fin.dim_Account c on b.account_key = c.account_key
    inner join fin.dim_journal d on b.journal_key = d.journal_key
    inner join fi_accounts e on c.account = e.gl_account
    group by a.stock_number) c on b.stock_number = c.stock_number) z
group by employee_number, year_month;

select * from compensation

-- this is it
select a.lastname, a.firstname, a.year_month, c.unit_count, b.front_gross, b.fi_gross, 
  round(a.arkona_total_gross, 0) as total_comp
from compensation a
left join gross b on a.employeenumber = b.employee_number
  and a.year_month = b.year_month
left join (
  select employee_number, year_month, sum(unit_count) as unit_count 
  from all_deals
  group by employee_number, year_month) c on a.employeenumber = c.employee_number
    and a.year_month = c.year_month
order by lastname, year_month    

-- here is the gross break out ben requested on 2/6/17

select employee_name, employee_,
  sum(commissions) as commission,
  sum(holiday_pay) as holiday_pay,
  sum(pto_pay_out) as pto_pay_out,
  sum(draw) as draw,
  sum(sales_vacation) as sales_vacation,
  sum(spiff_pay_out) as spiff_pay_out,
  sum(lease_program) as lease_program,
  round(sum(commissions + holiday_pay + pto_pay_out + draw + sales_vacation + spiff_pay_out + lease_program), 0) as total_gross
from (  
  select a.employee_name, a.employee_, a.check_month, 100 * (2000 + a.check_year) + check_month as year_month,
    sum(case when description = 'COMMISSIONS' then amount else 0 end) as commissions,
    sum(case when description = 'HOLIDAY PAY' then amount else 0 end) as holiday_pay,
    sum(case when description = 'PAY OUT PTO' then amount else 0 end) as pto_pay_out,
    sum(case when description = 'DRAWS' then amount else 0 end) as draw,
    sum(case when description = 'SALES VACATION' then amount else 0 end) as sales_vacation,
    sum(case when description = 'SPIFF PAY OUTS' then amount else 0 end) as spiff_pay_out,
    sum(case when description = 'LEASE PROGRAM' then amount else 0 end) as lease_program
  --   b.description, sum(b.amount) as amount, b.code_id
  --select distinct c.description
  from dds.ext_pyhshdta a
  inner join dds.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
    and a.company_number = b.company_number
    and a.employee_ = b.employee_number
    and b.code_type in ('0','1') -- 0,1: income, 2: deduction
  where trim(a.distrib_code) = 'SALE'
  --       and a.payroll_cen_year in (116, 117)
  --       and a.payroll_ending_month in (9,10,12,1)
  and (
    (a.payroll_ending_year = 16 and a.payroll_ending_month in (9,10,12))
    or
    (a.payroll_ending_year = 17 and a.payroll_ending_month = 1))
    and a.company_number = 'RY1'
  group by a.employee_name, a.employee_, a.check_month, a.check_year) x
group by employee_name, employee_
order by employee_name
 