﻿select _opened_month, b.store_code, b.full_name, a.employee_number, b.hire_Date, 
  a.pay_plan_name, a.draw, 0 as draw_paid, a.full_months_employment, 
  a.years_service, 
  -- populate & backfill
  -- hmmm thinking, in new month don't add pto stuff, in the nightly
  -- check to see if it needs to be done (no pto_interval row for current month)
  -- and do it then
  -- pto_rate, pto_hours, pto_rate * pto_hours as pto_pay, 
  0 as additional_comp, 0 as unpaid_time_off,
  case 
    when a.full_months_employment > 3 then d.guarantee
    else d.guarantee_new_hire
  end as guarantee,
  0 as unit_count_sys, 0 as unit_count_ovr, 0 as unit_count,
  false as metrics_qualified,
  0 as csi_score, false as csi_qualified,
  0 as email_score, false as email_qualified,
  0 as logged_score, false as logged_qualified,
  0 as auto_alert_score, false as auto_alert_qualified,
  0 as unit_count_x_per_unit,
  0 as total_pay,
  0 as total_pay_paid,
  false as complete,
  case b.store_code
    when 'RY1' then 'GM'
    when 'RY2' then 'Honda Nissan'
  end as store_name
-- select *
from scpp.sales_consultant_configuration a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number
inner join scpp.pay_plans c on c.year_month = _closed_month
inner join scpp.guarantee_matrix d on c.year_month = d.year_month
  and c.pay_plan_name = d.pay_plan_name
where a.year_month = _closed_month;