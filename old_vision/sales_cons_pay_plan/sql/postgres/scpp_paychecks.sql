﻿select * 
from  dds.ext_pyhscdta
where payroll_cen_year = 115
  and code_id = '78'

select a.employee_number, a.code_id, a.amount, a.description
from  dds.ext_pyhscdta a
where payroll_run_number = 115160  

select *
from dds.ext_pyhshdta
where check_year = 16
  and check_month = 1
where payroll_run_number = 115160

-- this could be it for persisting paycheck data
select b.check_day, a.employee_number, a.last_name, a.first_name, b.total_gross_pay,
  c.code_id, c.amount, c.description,
  b.check_year, b.check_month,
  ((2000 + b.check_year)::text || right(trim('0' || check_month::text), 2))::integer as check_year_month
from scpp.sales_consultants a
left join dds.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year = 15
  and b.check_month = 12
left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
  and b.payroll_run_number = c.payroll_run_number  
where b.employee_ is not null   
order by a.employee_number, b.check_day


select x.*, sum(amount) over(partition by employee_number) as month_total
from (
  select b.check_day, b.payroll_run_number, a.employee_number, a.last_name, 
    a.first_name, b.total_gross_pay,
    c.code_id, c.amount, c.description
  from scpp.sales_consultants a
  left join dds.ext_pyhshdta b on a.employee_number = b.employee_
    and b.check_year = 16
    and b.check_month = 1
  left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
    and b.payroll_run_number = c.payroll_run_number  
    and c.code_type = '1'
  where b.employee_ is not null) x   
order by x.employee_number, x.check_day


select *
from dds.ext_pyhscdta
where payroll_run_number in (131160, 115160)
  and employee_number = '2150210'
  

-- just draw, for january
select a.employee_number, a.last_name, 
  a.first_name, c.amount as draw, c.description
from scpp.sales_consultants a
inner join dds.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year = 16
  and b.check_month = 1
inner join dds.ext_pyhscdta c on a.employee_number = c.employee_number
  and b.payroll_run_number = c.payroll_run_number  
  and c.code_id = '78'
where b.employee_ is not null
order by last_name

-- these are the relevant (and for now configured) codes
select c.code_id, c.description
from scpp.sales_consultants a
left join dds.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year > 14
left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
  and b.payroll_run_number = c.payroll_run_number  
  and c.code_type = '1'
where b.employee_ is not null
group by c.code_id, c.description

select c.code_id, c.description
from scpp.sales_consultants a
left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
  and c.payroll_cen_year > 114
  and c.code_type = '1'
where c.employee_number is not null
group by c.code_id, c.description


select c.full_name, b.check_year, b.check_month, b.check_day, a.amount
from dds.ext_pyhscdta a
inner join dds.ext_pyhshdta b on a.payroll_run_number = b.payroll_run_number
inner join scpp.sales_Consultants c on a.employee_number = c.employee_number
where code_id in (
  select c.code_id
  from scpp.sales_consultants a
  left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
    and c.payroll_cen_year > 114
    and c.code_type = '1'
  where c.employee_number is not null
  group by c.code_id) 
  and a.payroll_Cen_year = 115


select b.check_day, b.payroll_run_number, a.employee_number, a.last_name, 
  a.first_name, b.total_gross_pay,
  c.code_id, c.amount, c.description
from scpp.sales_consultants a
inner join dds.ext_pyhshdta b on a.employee_number = b.employee_
  and b.check_year = 16
  and b.check_month = 1
inner join dds.ext_pyhscdta c on a.employee_number = c.employee_number
  and b.payroll_run_number = c.payroll_run_number  
  and c.code_type = '1'
  and c.code_id = '78'
where b.employee_ is not null
  