﻿-- Function: scpp.save_admin_edit_state(integer, numeric, integer, numeric, numeric, numeric, citext)

-- DROP FUNCTION scpp.save_admin_edit_state(integer, numeric, integer, numeric, numeric, numeric, citext);

CREATE OR REPLACE FUNCTION scpp.save_admin_edit_state(
    _id integer,
    _additional_comp numeric,
    _unpaid_time_off integer,
    _csi_score numeric,
    _logged_score numeric,
    _auto_alert_score numeric,
    _notes citext)
  RETURNS SETOF json AS
$BODY$
/*
this will be used to update tmp_s_c_data 
updates: additional_comp, unpaid_time_off, 
  csi_score, csi_qualified
  logged_score, logged_qualified,
  unit_count_x_per_unit, 
  auto_aler_score, auto_alert_qualified
  metrics_qualified,
  
  guarantee, per_unit, notes

2/24 notes is now a separate table (tmp_admin_notes)
3/1 added update to total_pay
-- 5/31/16 modify total_pay calc to include new hire pro-rate
-- 8/8/16 use scpp.get_total_pay() to update tmp_s_c_data.total_pay

delete from scpp.tmp_s_c_data;
delete from scpp.tmp_s_c_deals;
delete from scpp.tmp_admin_notes;

select * from scpp.get_consultant_profile(145);
  
select scpp.save_admin_edit_state(145,0,0,100,100,0,'note 1')

*/
declare _year_month integer;
        _csi_metric numeric(6,2);
        _logged_metric numeric(6,2);
        _auto_alert_metric numeric(6,2);
        _employee_number citext;
begin        
_year_month := (
  select year_month 
  from scpp.months 
  where open_closed = 'open');
_csi_metric := (
  select metric_value
  from scpp.metrics
  where year_month = _year_month
    and metric = 'csi');
_logged_metric := (
  select metric_value
  from scpp.metrics
  where year_month = _year_month
    and metric = 'Logged Opportunity Minimum');    
_auto_alert_metric := (
  select metric_value
  from scpp.metrics
  where year_month = _year_month
    and metric = 'Auto Alert Calls per Month');   
_employee_number:= (
  select employee_number
  from scpp.sales_consultant_configuration
  where id = _id);
update scpp.tmp_s_c_data
set additional_comp = _additional_comp,
    unpaid_time_off = _unpaid_time_off,
    csi_score = _csi_score,
    csi_qualified = case when _csi_score >= _csi_metric then true else false end,
    logged_score = _logged_score,
    logged_qualified = case when _logged_score >= _logged_metric then true else false end,
    auto_alert_score = _auto_alert_score,   
    auto_alert_qualified = case when _auto_alert_score >= _auto_alert_metric then true else false end;
update scpp.tmp_s_c_data
set metrics_qualified = ( 
  select 
    case
      when csi_qualified AND email_qualified AND logged_qualified AND auto_alert_qualified then true
      else false
    end
  from scpp.tmp_s_c_data);
update scpp.tmp_s_c_data
set guarantee = (
  select 
    case
      when d.year_month is null then 
        case
          when a.full_months_employment < 4 then c.guarantee_new_hire
          when a.metrics_qualified = true then c.guarantee_with_qual
          else c.guarantee
        end
      else d.guarantee
    end as guarantee
  from scpp.tmp_s_c_data a
  inner join scpp.pay_plans b on a.year_month = b.year_month
    and a.pay_plan_name = b.pay_plan_name
  inner join scpp.guarantee_matrix c on b.pay_plan_name = c.pay_plan_name
    and b.year_month = c.year_month
  left join scpp.guarantee_individual d on a.year_month = d.year_month
    and a.employee_number = d.employee_number);   
update scpp.tmp_s_c_data
set per_unit = (
  select 
    case
      when z.metrics_qualified then (
        select tier_2
        from scpp.per_unit_matrix a
        where year_month = z.year_month
          AND units @> floor(coalesce(z.unit_count,0))::integer
          and pay_plan_name = z.pay_plan_name) 
      else (
        select tier_1
        from scpp.per_unit_matrix a
        where year_month = z.year_month
          and units @> floor(coalesce(z.unit_count,0))::integer
          and pay_plan_name = z.pay_plan_name) 
      end as per_unit  
  from scpp.tmp_s_c_data z);   
  
update scpp.tmp_s_c_data
set unit_count_x_per_unit = unit_count * per_unit;

update scpp.tmp_s_c_data
-- passing 1, '1', 'tmp' returns total pay based on data in scpp.tmp_s_c_data
set total_pay = (
  select total_pay
  from scpp.get_total_pay(1,'1','tmp'));

if length(trim(_notes)) > 0 then
  insert into scpp.tmp_admin_notes (year_month, employee_number, the_date, note)
  values(_year_month, _employee_number, current_date, _notes);
--   update scpp.tmp_s_c_data
--   set notes = (
--     select 
--       case 
--         when length(trim(a.notes)) > 0 then 
--           concat(a.notes,chr(10), chr(13), (select to_char(current_date, 'MM/DD/YYYY')) || ': ' || _notes ||'. ')
--         else (select to_char(current_date, 'MM/DD/YYYY')) || ': ' || _notes ||'. '
--       end  
--     from scpp.tmp_s_c_data a); 
end if;             
return query
select row_to_json (c)
from (
  select _id as id, b.*,
    (
      select coalesce(array_to_json(array_agg(row_to_json(a))), '[]')
      from (
        select the_date, note
        from scpp.tmp_admin_notes
      ) a  
    ) as notes,
    (
      select coalesce(array_to_json(array_agg(row_to_json(d))), '[]')
      from (
        select stock_number, unit_count,deal_date, 
          customer_name, model_year,make,model,deal_source
        from scpp.tmp_s_c_deals
      ) d
    ) as deals
  from scpp.tmp_s_c_data b
) as c; 
end;
 
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION scpp.save_admin_edit_state(integer, numeric, integer, numeric, numeric, numeric, citext)
  OWNER TO rydell;
