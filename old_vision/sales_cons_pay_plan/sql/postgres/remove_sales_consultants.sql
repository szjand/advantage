﻿/*
don't delete from scpp.sales_consultants, just config and downstream

shit, i am not crazy about deleting rows from deals, but the referenced FK will be deleted
so i have to, although, there should be no deleted deals: removing sales consultant 
after the last month in which they were employed - duh
*/
select * from scpp.sales_consultants where employee_number = '138411'

select a.employee_number
from scpp.sales_consultant_configuration a
where year_month = 201609
  and not exists (
    select 1
    from scpp.ext_sales_consultants
    where employee_number = a.employee_number)

'RY1';'1109521';'Panzer';'Jeffery';'Jeffery Panzer'
'RY1';'1124625';'Seay';'Bryn';'Bryn Seay'
'RY1';'167600';'Homstad';'Jared';'Jared Homstad'
'RY1';'174021';'Johnson';'Brandon';'Brandon Johnson'    

digital consultants are off the plan effect aug 1
elizabeth boushey termed

select * 
from scpp.sales_consultants 
where last_name in ('garceau') 
  or first_name = 'alexandria'

-- 8/18/16 Aschnewitz term, effective 7/30
-- 11/2/16 Joshua Hall, Robert Kvasager	
-- 12/2/16 Joe Garceau
do
$$
 declare 
   _emp citext := '150045';
   _ym integer := 201612;
begin
  delete from scpp.sales_consultant_data where year_month = _ym and employee_number = _emp;
  delete from scpp.pto_hours where year_month = _ym and employee_number = _emp;
  delete from scpp.admin_notes where year_month = _ym and employee_number = _emp;
  delete from scpp.deals where year_month = _ym and employee_number = _emp;
  delete from scpp.pto_intervals where year_month = _ym and employee_number = _emp;
  delete from scpp.guarantee_individual where year_month = _ym and employee_number = _emp;
  delete from scpp.sales_consultant_configuration where year_month = _ym and employee_number = _emp;
end
$$;


-- 8/22/16 steve emmons, effective 9/1

do
$$
 declare 
   _emp citext := '138411';
   _ym integer := 201609;
begin
  delete from scpp.sales_consultant_data where year_month = _ym and employee_number = _emp;
  delete from scpp.pto_hours where year_month = _ym and employee_number = _emp;
  delete from scpp.admin_notes where year_month = _ym and employee_number = _emp;
  delete from scpp.deals where year_month = _ym and employee_number = _emp;
  delete from scpp.pto_intervals where year_month = _ym and employee_number = _emp;
  delete from scpp.guarantee_individual where year_month = _ym and employee_number = _emp;
  delete from scpp.sales_consultant_configuration where year_month = _ym and employee_number = _emp;
end
$$;

-- 10/7/16 steve emmons, effective 10/1
do
$$
 declare 
   _emp citext := '17190';
   _ym integer := 201610;
begin
  delete from scpp.sales_consultant_data where year_month = _ym and employee_number = _emp;
  delete from scpp.pto_hours where year_month = _ym and employee_number = _emp;
  delete from scpp.admin_notes where year_month = _ym and employee_number = _emp;
  delete from scpp.deals where year_month = _ym and employee_number = _emp;
  delete from scpp.pto_intervals where year_month = _ym and employee_number = _emp;
  delete from scpp.guarantee_individual where year_month = _ym and employee_number = _emp;
  delete from scpp.sales_consultant_configuration where year_month = _ym and employee_number = _emp;
end
$$;

-- 1/3/17 lakeith strother eff 1/1/17
-- select * from scpp.sales_consultants where full_name like '%strot%'
do
$$
 declare 
   _emp citext := '1132420';
   _ym integer := 201701;
begin
  delete from scpp.sales_consultant_data where year_month = _ym and employee_number = _emp;
  delete from scpp.pto_hours where year_month = _ym and employee_number = _emp;
  delete from scpp.admin_notes where year_month = _ym and employee_number = _emp;
  delete from scpp.deals where year_month = _ym and employee_number = _emp;
  delete from scpp.pto_intervals where year_month = _ym and employee_number = _emp;
  delete from scpp.guarantee_individual where year_month = _ym and employee_number = _emp;
  delete from scpp.sales_consultant_configuration where year_month = _ym and employee_number = _emp;
end
$$;
