﻿drop function scpp.month_year(out month_year integer, out open_closed text );
create or replace function scpp.month_year() 
returns setof json as
$$
/*
select scpp.month_year()
*/
select row_to_json(a)
from (
  select month_yyyy, open_closed
  from scpp.months
  where seq 
      between (
        select seq - 5
        from scpp.months
        where open_closed = 'open')
      and (
        select seq 
        from scpp.months
        where open_closed = 'open') 
    and year_month > 201510       
  order by seq desc) a
$$
language sql; 


drop function scpp.sls_profile_search(search_string text);
create or replace function scpp.sls_profile_search(search_string text) 
-- returns table(id integer, store_code text, full_name text, hire_date date, 
--   sales_consultant_id text, pay_plan_name text) as
returns setof json as
$$
/*
select * from scpp.sls_profile_search('AM');
*/
select row_to_json(d)
from (
  select b.id, a.store_code, a.first_name, a.last_name, a.hire_date, a.sales_consultant_id,
    b.pay_plan_name
  from scpp.sales_consultants a
  inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
  inner join scpp.pay_plans c on b.pay_plan_name = c.pay_plan_name
    and b.year_month = c.year_month
  inner join scpp.months d on c.year_month = d.year_month
    and d.open_closed = 'open'  
  where lower(a.full_name) like '%' ||lower(search_string) || '%') d; 
$$
language sql;


drop function scpp.get_all_consultants(_store_code text);
create or replace function scpp.get_all_consultants(_store_code text default 'ALL') 
-- returns table(id integer, store_code text, last_name text, first_name text, hire_date date, 
--   sales_consultant_id text, pay_plan_name text) as
returns setof json as
$$
/*
this returns all active sales consultants for the currently open month
if a store is not passed in, returns all 
select * from scpp.get_all_consultants();
select * from scpp.get_all_consultants('RY2');
*/
select row_to_json(e)
from (
  select b.id, a.store_code, a.last_name, a.first_name, a.hire_date, a.sales_consultant_id,
    b.pay_plan_name
  from scpp.sales_consultants a
  inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
  inner join scpp.pay_plans c on b.pay_plan_name = c.pay_plan_name
    and b.year_month = c.year_month
  inner join scpp.months d on c.year_month = d.year_month
    and d.open_closed = 'open' 
  where 
    case _store_code
      when 'ALL' then 1 = 1
      else a.store_code = _store_code
    end   
  order by a.store_code, a.last_name) e;
$$
language sql;


drop function scpp.get_consultant_profile(_id integer);
create or replace function scpp.get_consultant_profile(_id integer) 
returns setof json as
$$
/*
the sales_consultant_configuration.id attribute identifies a sales consultant in a month
this returns a sales consultant profile for the currently open month
and populates the table used for maintaining state on the admin page
select * from scpp.get_consultant_profile(162);
*/
delete from scpp.tmp_s_c_data;
insert into scpp.tmp_s_c_data
select *
from scpp.sales_consultant_data
where id = _id;
select row_to_json (a)
from (
  select *
  from scpp.sales_consultant_data
  where id = _id) a;

$$
language sql;

drop function if exists scpp.get_play_plans();
create or replace function scpp.get_pay_plans()
returns setof json as
$$
-- returns pay plans for current open month
-- select scpp.get_pay_plans();
select row_to_json (c)
from (
select a.pay_plan_id, a.pay_plan_name
from scpp.pay_plans a
inner join scpp.months b on a.year_month = b.year_month
  and b.open_closed = 'open') c;
$$
language sql;

drop function if exists scpp.update_sales_consultant_configuration(
  _year_month integer,
  _employee_number text,
  _draw integer,
  _guarantee integer,
  _pay_plan text);
create or replace function scpp.update_sales_consultant_configuration(
  _year_month integer,
  _employee_number text,
  _draw integer,
  _guarantee integer,
  _pay_plan text)
returns void as
$$
-- select scpp.update_sales_consultant_configuration(201601,'1109521',500,2300,'Standard')
update scpp.sales_consultant_configuration
set draw = _draw,
    guarantee = _guarantee,
    pay_plan_name = _pay_plan
where year_month = _year_month
  and employee_number = _employee_number;    
$$
language sql;  

DROP FUNCTION if exists scpp.get_consultant_deals(integer);
CREATE OR REPLACE FUNCTION scpp.get_consultant_deals(_id integer)
  RETURNS SETOF json AS
$BODY$
/*
select scpp.get_consultant_deals(162)
*/
delete from scpp.tmp_s_c_deals;

insert into scpp.tmp_s_c_deals
select a.year_month, a.employee_number, b.stock_number, b.unit_count, b.deal_date, 
  b.customer_name, b.model_year, b.make, b.model, b.deal_source
from scpp.sales_consultant_configuration a
inner join scpp.deals b on a.year_month = b.year_month
  and a.employee_number = b.employee_number
where a.id  = _id; 

select row_to_json(c)
from (
  select b.unit_count, b.deal_date, b.customer_name, b.stock_number, b.model_year, b.make, b.model
  from scpp.sales_consultant_configuration a
  inner join scpp.deals b on a.year_month = b.year_month
    and a.employee_number = b.employee_number
  where a.id  = _id
  order by deal_date) c;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION scpp.get_consultant_deals(integer)
  OWNER TO rydell;

DROP FUNCTION if exists scpp.save_admin_edit_state(integer, numeric, integer, numeric, numeric, numeric, citext);

CREATE OR REPLACE FUNCTION scpp.save_admin_edit_state(
    _id integer,
    _additional_comp numeric(12,2),
    _unpaid_time_off integer,
    _csi_score numeric(6,2),
    _logged_score numeric(6,2),
    _auto_alert_score numeric(6,2),
    _notes citext)   
RETURNS setof json as
$BODY$
/*
this will be used to update tmp_s_c_data 
updates: additional_comp, unpaid_time_off, 
  csi_score, csi_qualified
  logged_score, logged_qualified
  auto_aler_score, auto_alert_qualified
  metrics_qualified,
  guarantee, per_unit, notes

--update scpp.tmp_s_c_data set notes = null;
  
select scpp.save_admin_edit_state(154,0,0,0,0,0,null)

*/
declare _year_month integer;
        _csi_metric numeric(6,2);
        _logged_metric numeric(6,2);
        _auto_alert_metric numeric(6,2);
begin        
_year_month := (
  select year_month 
  from scpp.months 
  where open_closed = 'open');
_csi_metric := (
  select metric_value
  from scpp.metrics
  where year_month = _year_month
    and metric = 'csi');
_logged_metric := (
  select metric_value
  from scpp.metrics
  where year_month = _year_month
    and metric = 'Logged Opportunity Minimum');    
_auto_alert_metric := (
  select metric_value
  from scpp.metrics
  where year_month = _year_month
    and metric = 'Auto Alert Calls per Month');        
update scpp.tmp_s_c_data
set additional_comp = _additional_comp,
    unpaid_time_off = _unpaid_time_off,
    csi_score = _csi_score,
    csi_qualified = case when _csi_score >= _csi_metric then true else false end,
    logged_score = _logged_score,
    logged_qualified = case when _logged_score >= _logged_metric then true else false end,
    auto_alert_score = _auto_alert_score,   
    auto_alert_qualified = case when _auto_alert_score >= _auto_alert_metric then true else false end;
update scpp.tmp_s_c_data
set metrics_qualified = ( 
  select 
    case
      when csi_qualified AND email_qualified AND logged_qualified AND auto_alert_qualified then true
      else false
    end
  from scpp.tmp_s_c_data);
update scpp.tmp_s_c_data
set guarantee = (
  select 
      case
        when a.full_months_employment < 4 then c.guarantee_new_hire
        when a.metrics_qualified = true then c.guarantee_with_qual
        else c.guarantee
      end as guarantee
  from scpp.tmp_s_c_data a
  inner join scpp.pay_plans b on a.year_month = b.year_month
    and a.pay_plan_name = b.pay_plan_name
  inner join scpp.guarantee_matrix c on b.pay_plan_name = c.pay_plan_name
    and b.year_month = c.year_month);   
update scpp.tmp_s_c_data
set per_unit = (
  select 
    case
      when z.metrics_qualified then (
        select tar
        from scpp.per_unit_matrix a
        where year_month = z.year_month
          AND units @> floor(coalesce(z.unit_count,0))::integer
          and pay_plan_name = z.pay_plan_name) 
      else (
        select map
        from scpp.per_unit_matrix a
        where year_month = z.year_month
          and units @> floor(coalesce(z.unit_count,0))::integer
          and pay_plan_name = z.pay_plan_name) 
      end as per_unit  
  from scpp.tmp_s_c_data z);   
if length(trim(_notes)) > 0 then
  update scpp.tmp_s_c_data
  set notes = (
    select 
      case 
        when length(trim(a.notes)) > 0 then 
          concat(a.notes,chr(10),  (select to_char(current_date, 'MM/DD/YYYY')) || ': ' || _notes)
        else (select to_char(current_date, 'MM/DD/YYYY')) || ': ' || _notes
      end  
    from scpp.tmp_s_c_data a); 
end if;             
return query
select row_to_json (a)
from (
  select *
  from scpp.tmp_s_c_data) a;
end;
 
$BODY$
  LANGUAGE plpgsql;


drop function if exists scpp.get_consultant_deals(_user_name citext);
create or replace function scpp.get_consultant_deals(_user_name citext) 
returns setof json as
$$
/*

select * from scpp.get_consultant_deals('aloven@rydellcars.com');

*/
select row_to_json(x)
from (
  select j.*,
    (
      select coalesce(array_to_json(array_agg(row_to_json(x))), '[]')
      from (
        select stock_number, unit_count, deal_date, customer_name,
          model_year, make, model, deal_source
        from scpp.deals 
        where employee_number = j.employee_number
          and year_month = j.year_month
        ) x
    ) as deals
  from (
    select a.full_name, a.employee_number, c.year_month, e.unit_count      
    from scpp.sales_consultants a
    inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
    inner join scpp.months c on b.year_month = c.year_month
      and c.open_closed = 'open'
    inner join scpp.sales_consultant_data e on b.employee_number = e.employee_number
      and b.year_month = e.year_month
    where a.user_name = _user_name) j
) as x;    

$$
language sql;


