﻿-- shit this does not handle terms: and a.activeCode = 'A'
-- termdate > first of open month
-- except that now includes 2 rows for each of the ry2 xfrs
-- add row to months: sc_first_working_day
-- now need first_full_month_employment
select a.storecode, a.employeenumber, a.lastname, a.firstname, trim(a.firstname) || ' ' || a.lastname,
  case 
    when (select dds.db2_integer_to_date(d.org_hire_date)) < a.hiredate 
      then (select dds.db2_integer_to_date(d.org_hire_date))
    else a.hiredate
  end,
  a.termdate, e.username,
  b.sales_person_id as ry1_id, c.sales_person_id as ry2_id, 
  a.hiredate as last_hire_date,
  case
    -- latest hiredate <= first wkg day of the month then yearmonth of latest hiredate
    when a.hiredate <= (-- aa.sc_first_working_day_of_month -- nope
      select sc_first_working_day_of_month
      from scpp.months
      where year_month = extract(year from a.hiredate) * 100 + extract(month from a.hiredate))
      then (
        select year_month
        from scpp.months
        where a.hiredate between first_of_month and last_of_month)
    else (
    -- latest hiredate > first wkg day of the month then yearmonth of latest hiredate + 1
      select year_month
      from scpp.months
      where seq = ( --aa.seq + 1)  
        select seq
        from scpp.months
        where year_month = extract(year from a.hiredate) * 100 + extract(month from a.hiredate)) + 1)
  end,
  (select dds.db2_integer_to_date(d.org_hire_date)) as orig_hire_date
from ads.ext_dds_edwEmployeeDim a
inner join scpp.months aa on 1 = 1
  and aa.open_closed = 'open'
left join dds.ext_bopslsp b on a.employeenumber = b.employee_number
  and b.company_number = 'RY1'
  and b.sales_person_type = 'S'
left join dds.ext_bopslsp c on a.employeenumber = c.employee_number
  and c.company_number = 'RY2' 
  and c.sales_person_type = 'S' 
left join dds.ext_pymast d on a.employeenumber  = d.pymast_employee_number
left join ads.ext_sco_tpemployees e on a.employeenumber = e.employeenumber
where a.currentrow = true
  and a.termdate > aa.sc_first_working_day_of_month
  and a.distcode = 'SALE'
order by a.hiredate


-- first working day of open month
select min(a.thedate)
from dds.day a
inner join scpp.months b on a.yearmonth = b.year_month
  and b.open_closed = 'open'
where a.dayofweek between 2 and 6
  and holiday = false

alter table scpp.months
add column sc_first_working_day_of_month date;

update scpp.months a
set sc_first_working_day_of_month = x.fst_wd
from (
  select *
  from scpp.months a
  left join (
    select yearmonth, min(a.thedate) fst_wd
    from dds.day a
    where a.dayofweek between 2 and 6
      and holiday = false
    group by yearmonth) b on a.year_month = b.yearmonth) x
where a.year_month = x.yearmonth    

can the logic for first_full_month go in this query 

-- add dependencies for ext_sales_consultants

insert into ops.task_dependencies 
values('ext_bopslsp','ext_sales_consultants'),
('ext_pymast','ext_sales_consultants'),
('ext_ads_dds_edwEmployeeDim','ext_sales_consultants'),
('ext_ads_sco_tpEmployees','ext_sales_consultants');

alter table scpp.ext_sales_consultants
add column last_hire_date date,
add column first_full_month_employment integer,
add column orig_hire_date date;

alter table scpp.sales_consultants
add column first_full_month_employment integer;

update scpp.sales_consultants a
set first_full_month_employment = x.first_full_month_employment
from (
  select e.employee_number, e.first_full_month_employment
  from scpp.ext_sales_consultants e) x
where a.employee_number = x.employee_number;  


  
DROP TABLE scpp.xfm_sales_consultants;
CREATE TABLE scpp.xfm_sales_consultants
(
  row_type citext,
  store_code citext NOT NULL,
  employee_number citext NOT NULL,
  last_name citext NOT NULL,
  first_name citext NOT NULL,
  full_name citext NOT NULL,
  hire_date date NOT NULL,
  term_date date NOT NULL,
  user_name citext,
  ry1_id citext,
  ry2_id citext,
  last_hire_date date,
  first_full_month_employment integer,
  orig_hire_date date,
  run_date date);

 update scpp.xfm_deals
 set primary_sc = 'JHA'
 where primary_sc = 'HAL'
   and store_code = 'RY1'; 

-- pto_rate doesn't calculate for hall(164050) & strother (1132420), need to manually generate using their
-- ry2 employeenumbers

select * from scpp.xfm_pto_intervals

-- from xfm_pto_intervals
-- strother:  15.48
select employee_, total_gross,
  -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
  round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate
from  (
  select employee_, sum(total_gross_pay) as total_gross, count(*) as adj_count
  from dds.ext_pyhshdta b
  where employee_ = '2132420'
    and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date between '03/01/2015' and '02/29/2016'
  group by employee_) x

-- hall: 12.01
 select employee_, total_gross,
  -- 4.333 weeks/month, 5 working days/month, 8 hrs/day
  round(total_gross/(4.333 * adj_count/2)/5/8, 2) as pto_rate
from  (
  select employee_, sum(total_gross_pay) as total_gross, count(*) as adj_count
  from dds.ext_pyhshdta b
  where employee_ = '264050'
    and (b.check_month || '-' || b.check_day || '-' || (2000 + b.check_year))::date between '03/01/2015' and '02/29/2016'
  group by employee_) x

update scpp.pto_intervals
set pto_rate = '15.48'
where employee_number = '1132420'
  and year_month = 201605;

update scpp.pto_intervals
set pto_rate = '12.01'
where employee_number = '164050'
  and year_month = 201605;  

update scpp.sales_Consultant_configuration 
set full_months_employment = 0
where full_months_employment < 0;

update scpp.sales_consultant_data
set full_months_Employment = 0
where full_months_employment < 0;

-- full_months_employment fucked up
-- fucked up in open_new_month()
-- the ones fucked up are thosee with hire_Date of 2/1/16
select a.full_name, a.employee_number, a.hire_date, a.full_months_employment, b.*
from scpp.sales_consultant_data a
left join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
  and a.year_month = b.year_month
WHERE exists (
  select 1 
  from scpp.sales_consultants
  where employee_number = a.employee_number
    and hire_date = '02/01/2016')
order by a.employee_number, a.year_month

update scpp.sales_consultant_data
set full_months_employment = full_months_employment + 1
-- select * from scpp.sales_consultant_data
where employee_number in ('1106223','137220')
  and year_month <> 201602






