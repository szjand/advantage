﻿/*
2/13/17
this path led to total confusion
have checked out what is in \gross_per_consultant.sql


*/

-- january deals
create temp table deals as
select a.stock_number, a.vin, a.customer_name, b.firstname, b.lastname
from scpp.xfm_deals a
left join ads.dim_salesperson b on a.primary_sc = b.salespersonid
  and a.store_code = b.storecode
where date_capped between '01/01/2017' and '01/31/2017'
  and seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = a.stock_number)

select * from scpp.xfm_Deals where stock_number = '30191'

select * from arkona.xfm_glptrns where control = '30191'
   
-- accounts
drop table accounts;
create temp table accounts as 
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page between 5 and 15 -- new cars
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
union
-- used car gross accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
union
-- f/i accounts
select distinct d.gl_account, e.account_type, e.department
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 17
  and b.line between 1 and 20
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account

-- these numbers are wacky --------------------------------------------------------------
select a.control, a.amount, b.customer_name, b.vin, b.lastname, b.firstname, d.*
from fin.fact_gl a
inner join deals b on a.control = b.stock_number
inner join fin.dim_account c on a.account_key = c.account_key
inner join accounts d on c.account = d.gl_account
where a.post_status = 'Y'

select department, account_type, round(sum(amount), 0)
from (
  select a.control, a.amount, b.customer_name, b.vin, b.lastname, b.firstname, d.*
  from fin.fact_gl a
  inner join deals b on a.control = b.stock_number
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts d on c.account = d.gl_account
  where a.post_status = 'Y') g
group by department, account_type  
order by department, account_type  


select department, round(sum(amount), 0)
from (
  select a.control, a.amount, b.customer_name, b.vin, b.lastname, b.firstname, d.*
  from fin.fact_gl a
  inner join deals b on a.control = b.stock_number
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join accounts d on c.account = d.gl_account
  inner join dds.dim_date e on a.date_key = e.date_key
    and e.year_month = 201701
  where a.post_status = 'Y') g
group by department
order by department
-----------------------------------------------------------------------------------

--New Sales/COGS/Gross
-- P3L2
create table new_accounts as
select gl_account from (
select a.*, b.page, b.line, b.col, b.line_label, d.gm_account, d.gl_account
-- select sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page between 5 and 15 -- new cars
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
inner join fin.dim_fs_account d on a.fs_Account_key = d.fs_account_key  
where amount <> 0
order by d.gl_account
) x group by gl_Account


-- select *
-- from ( -- shit now way these match fuck me
-- since this agrees with the statement, it will be the base
select a.control, a.amount, b.account
-- select sum(amount) -- yep, this is the new car gross P3L2
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join new_accounts bb on b.account = bb.gl_account
inner join dds.dim_date c on a.date_key = c.date_key
  and c.year_month = 201701  
where a.post_status = 'Y' 
order by a.control
-- ) m full outer join deals n on m.control = n.stock_number 

-- so now, just get the consultant for the deal

select *
from scpp.deals where stock_number = '30191'


select a.control, sum(a.amount)
-- select sum(amount) -- yep, this is the new car gross P3L2
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join new_accounts bb on b.account = bb.gl_account
inner join dds.dim_date c on a.date_key = c.date_key
  and c.year_month = 201701  
where a.post_status = 'Y' 
group by a.control

select new_used, count(*) from (
select a.*, 
case
  when right(trim(stock_number), 1) in ('0','1','2','3','4','5','6','7','8','9') then 'new'
  else 'used'
end as new_used
from deals a
) x group by new_used
