﻿create table scpp.zjon_csv (
  the_date citext,
  store citext,
  ry1_id citext,
  ry2_id citext,
  stock_number citext,
  sale_date date,
  customer citext);

-- unwinds
select stock_number, sale_date, min(data_date), max(data_date), count(*)
from (
  select stock_number, sale_date, (left(the_date, 2) || '-' || substring(the_date, 3,2) || '-' || '2016')::Date as data_date
  from scpp.zjon_csv) a
where stock_number in ('h8340b','h8435','25628','27215xx','h8802a')  
group by stock_number, sale_date
order by max(data_date)

  
select '0201', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0201 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0202', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0202 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0203', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0203 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0204', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0204 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0205', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0205 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0206', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0206 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0207', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0207 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0208', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0208 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0209', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0209 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0210', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0210 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0211', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0211 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0212', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0212 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0213', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0213 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0214', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0214 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0215', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0215 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0216', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0216 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0217', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0217 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0218', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0218 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0219', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0219 where date_capped > '12/31/2015' and sale_type <> 'W'
UNION ALL
select '0220', bopmast_company_number, coalesce(primary_salespers, 'XXX'), coalesce(secondary_slspers2, 'XXX'), trim(bopmast_Stock_number), date_capped, bopmast_vin
from test.ext_bopmast_0220 where date_capped > '12/31/2015' and sale_type <> 'W'

