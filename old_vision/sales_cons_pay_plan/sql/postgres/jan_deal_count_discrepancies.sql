﻿select *
from (
  select x.*, sum(unit_count) over(partition by consultant order by consultant) as total
  from (
    select bopmast_company_number, bopmast_stock_number, primary_salespers as consultant,
      date_capped, date_approved,
    CASE 
      when secondary_slspers2 is null then 1.0 
      else 0.5
    end as unit_count
    from foreign_table a
    where date_capped between '2016-01-01' and '2016-01-31'
      and sale_type <> 'w'
      and bopmast_company_number = 'RY1'
    union
    select bopmast_company_number, bopmast_stock_number, secondary_slspers2 as consultant,
      date_capped, date_approved, 0.5 as unit_count
    from foreign_table a
    where date_capped between '2016-01-01' and '2016-01-31'
      and sale_type <> 'w'
      and secondary_slspers2 is not null
      and bopmast_company_number = 'RY1') x) r
full outer join scpp.jan_sales_sheets s on r.bopmast_stock_number = s.stock_number
where (r.bopmast_stock_number is null or s.stock_number is null)
  and (sales_consultant = 'craig' or consultant = 'CRO')


update scpp.jan_sales_sheets
set sales_consultant = 'bradley'
where sales_consultant = 'brad';

update scpp.jan_sales_sheets
set sales_consultant = 'logan'
where sales_consultant = 'logain';

update scpp.jan_sales_sheets
set sales_consultant = 'jeffery'
where sales_consultant = 'jeffrey';

update scpp.jan_sales_sheets
set sales_consultant = 'joseph'
where sales_consultant = 'joe';

update scpp.jan_sales_sheets
set sales_consultant = 'samuel'
where sales_consultant = 'sam';

update scpp.jan_sales_sheets
set sales_consultant = 'james'
where sales_consultant = 'jim';

update scpp.jan_sales_sheets
set sales_consultant = 'steven'
where sales_consultant = 'steve';

update scpp.jan_sales_sheets
set sales_consultant = 'allen'
where sales_consultant = 'aj';

update scpp.jan_sales_sheets
set sales_consultant = 'christopher'
where sales_consultant = 'chris';

select *
from (
  select sales_consultant, unit_count
  from (
    select a.*, count(*) over(partition by sales_consultant order by sales_consultant) as unit_count
    from scpp.jan_sales_sheets a) b
  group by sales_consultant, unit_count) aa
full outer join (
  select y.consultant, y.total, z.full_name, z.first_name
  from (
    select x.*, sum(unit_count) over(partition by consultant order by consultant) as total
    from (
      select bopmast_company_number, bopmast_stock_number, primary_salespers as consultant,
        date_capped, date_approved,
      CASE 
        when secondary_slspers2 is null then 1.0 
        else 0.5
      end as unit_count
      from foreign_table a
      where date_capped between '2016-01-01' and '2016-01-31'
        and sale_type <> 'w'
        and bopmast_company_number = 'RY1'
      union
      select bopmast_company_number, bopmast_stock_number, secondary_slspers2 as consultant,
        date_capped, date_approved, 0.5 as unit_count
      from foreign_table a
      where date_capped between '2016-01-01' and '2016-01-31'
        and sale_type <> 'w'
        and secondary_slspers2 is not null      
        and bopmast_company_number = 'RY1') x) y
  left join scpp.sales_consultants z on y.consultant = z.sales_consultant_id     
  group by y.consultant, y.total, z.full_name, z.first_name) bb 
on aa.sales_consultant = bb.first_name
  