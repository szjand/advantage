﻿-- holy shit, it is all fucked up
-- how did it get this way
-- thinking of enforcing calculated fields with triggers in tmp tables only
-- but all this shit makes me wonder if that is adequate
select distinct year_month
from scpp.sales_consultant_data

select * 
from scpp.sales_consultant_data
where unit_count * per_unit <> unit_count_x_per_unit
/*
update scpp.sales_consultant_data
set unit_count_x_per_unit = unit_count * per_unit;
*/

select * 
from scpp.sales_consultant_data
where unit_count <> unit_count_sys + unit_count_ovr


select *
from scpp.sales_consultant_data a
left join (
  select employee_number, year_month,
    sum(case when deal_source = 'ovr' then 1 else 0 end) as ovr,
    sum(case when deal_source = 'sys' then 1 else 0 end) as sys,
    count(*) as unit_count
  from scpp.deals 
  group by employee_number, year_month) b on a.employee_number = b.employee_number
  and a.year_month = b.year_month
where a.unit_count_sys <> b.sys
  or a.unit_count_ovr <> b.ovr

update scpp.sales_consultant_data z
set unit_count_sys = x.sys,
    unit_count_ovr = x.ovr,
    unit_count = x.total_unit_count
from (
  select employee_number, year_month,
    sum(case when deal_source = 'ovr' then 1 else 0 end) as ovr,
    sum(case when deal_source = 'sys' then 1 else 0 end) as sys,
    count(*) as total_unit_count
  from scpp.deals 
  group by employee_number, year_month) x
where z.employee_number = x.employee_number
  and z.year_month = x.year_month;    
  


select a.full_name, a.employee_number, a.total_pay, b.*
from scpp.sales_consultant_data a
left join (
  select s.year_month, s.employee_number,  
    case
      when unit_count_x_per_unit > guarantee then
        unit_count_x_per_unit + pto_pay + additional_comp
      else
        case
          when unit_count_x_per_unit + pto_pay > guarantee then
            unit_count_x_per_unit + pto_pay + additional_comp
          else 
            guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
          end
      end as total_pay    
  from scpp.sales_consultant_data s
  inner join scpp.months t on s.year_month = t.year_month) b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
where a.total_pay <> b.total_pay    

  
update scpp.sales_consultant_data a
set total_pay = x.total_pay
from (
  select s.year_month, s.employee_number,  
    case
      when unit_count_x_per_unit > guarantee then
        unit_count_x_per_unit + pto_pay + additional_comp
      else
        case
          when unit_count_x_per_unit + pto_pay > guarantee then
            unit_count_x_per_unit + pto_pay + additional_comp
          else 
            guarantee - (unpaid_time_off * (round(guarantee/sc_working_days/8.0, 2))) + additional_comp
          end
      end as total_pay    
  from scpp.sales_consultant_data s
  inner join scpp.months t on s.year_month = t.year_month) x
 where a.employee_number = x.employee_number
   and a.year_month = x.year_month
  