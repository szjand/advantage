﻿select *
from (
  select *
  from scpp.xfm_deals 
  where run_date = (select max(xd.run_date) from scpp.xfm_Deals xd)
    and row_type = 'update') x
inner join scpp.deals y on x.stock_number = y.stock_number
  and (x.record_status <> y.deal_status or x.status_change = 'deleted')
  and y.seq = (
    select max(g.seq)
    from scpp.deals g
    where g.stock_number = y.stock_number) 
WHERE not ((x.record_status = 'Accepted' or x.record_status = 'None') and y.deal_status = 'Deleted')
  -- already busted down to status None, no need to process Delete in this case
  and not (x.status_change = 'Deleted' and y.deal_status = 'None')
  and x.status_change not in ('No Change', 'None to Accepted', 'Accepted to None')  
  and not (x.status_change = 'Accepted to Capped' and y.deal_status = 'Capped' and y.deal_date = x.gl_date)

-- ok, the only things that are going to change are sc and run_date
-- update scpp.deals directly




  
/*

delete from scpp.xfm_deals where run_date = '03/25/2016'

select max(run_date) from scpp.xfm_Deals

select * from scpp.xfm_deals where row_type = 'update' and run_Date = '03/08/2016'

select * from scpp.xfm_glptrns where stock_number = '27616'

select * from scpp.ext_glptrns_for_deals where gtctl_ = '27616'

select * from scpp.xfm_deals where stock_number = '27584'

select * from scpp.deals where stock_number = '26564B'


select * from scpp.deals where unit_count = .5

select * from scpp.deals where stock_number = '25698RA'

select * from scpp.deals where deal_status = 'deleted'

select * from scpp.xfm_Deals where run_date = '02/08/2016' and record_Status = 'U' and date_capped <> gl_date

select * from scpp.xfm_deals where row_type = 'new' and record_Status = 'U' and date_capped <> gl_date

select * from scpp.xfm_deals where row_type = 'new' and record_Status = 'U' order by run_date desc 

select * from scpp.xfm_deals where date_approved > date_capped and record_status = 'U'

select * from scpp.xfm_deals where stock_number in ('27164','27275')

select * from scpp.xfm_Deals where status_change  = 'no change' and date_capped <> gl_date

delete from scpp.xfm_deals where stock_number = '27215xx' and seq = 4

select * from scpp.xfm_Deals where stock_number in (
select stock_number from (select stock_number, gl_date from scpp.xfm_Deals where gl_date is not null group by stock_number, gl_date) a group by stock_number having count(*) > 1)
order by stock_number, run_Date


select * from scpp.deals where stock_number = '26080B'

select * from scpp.xfm_Deals where status_change = 'no change' and sc_change <> 'no change'

*/

/*
                select *
                from scpp.ext_deals a
                inner join scpp.xfm_deals b on a.stock_number = b.stock_number
                  and b.seq = (
                    select max(seq)
                    from scpp.xfm_deals
                    where stock_number = b.stock_number)
                  and b.record_status = 'U'
                where a.primary_sc <> b.primary_sc
                  or a.secondary_sc <> b.secondary_sc

select * from scpp.deals where stock_number = '26564B'

update scpp.deals
set unit_count = .5
where stock_number = '26564B'                  

insert into scpp.deals
select year_month, '17172', stock_number, unit_count, deal_date, customer_name, model_year,
  make, model, deal_source, seq::integer, notes::citext, deal_Status, run_date
from scpp.deals
where stock_number = '26564B'  
*/

                    