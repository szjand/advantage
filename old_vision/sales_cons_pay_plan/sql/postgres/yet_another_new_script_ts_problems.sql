﻿
/* < 4/19/16 xfm_deals fails due to multiple rows for vehicle/date in xfm_glptrns *****************************>
stepping throught april
4/6
xfm_deals fails for 26332R
*/
-- 1 row
select * from scpp.ext_deals where stock_number = '26332R'    

-- 1 row   
select store_Code, stock_number, vin, customer_name, primary_sc, secondary_sc,
record_Status, date_approved, date_capped, seq, gl_date, gl_count
from scpp.xfm_deals x
where seq = (
  select max(seq)
  from scpp.xfm_deals
  where stock_number = x.stock_number)  
and stock_number = '26332R'           

-- 2 rows
-- 2 rows with the same run date? 
select * from scpp.xfm_glptrns where stock_number = '26332r'     

-- and here is why
select * from scpp.ext_glptrns_for_deals where gtctl_ = '26332R'  

'RY1';3280435;4;'B';'M';'Y';'Y';' ';' ';'VSN';'2016-04-05';'2016-04-05';'9999-01-01';'1429001';'26332R';'26332R';'26332R';'B';'<NULL>';'26332R';'<NULL>';'WINTER, JENNIFER MARIE';-29676.00
'RY1';3280445;1;'B';'M';'Y';'Y';' ';' ';'VSN';'2016-04-06';'2016-04-06';'9999-01-01';'1429001';'26332R';'26332R';'26332R';'B';'<NULL>';'26332R';'<NULL>';'C/E';29676.00
'RY1';3280445;2;'B';'M';'Y';'Y';' ';' ';'VSN';'2016-04-06';'2016-04-06';'9999-01-01';'1429001';'26332R';'26332R';'26332R';'B';'<NULL>';'26332R';'<NULL>';'C/E';-32176.00
     
even though the gtdate on these 3 are over 2 days, they were actually all entered on 4/6 (ie nothing available from the 0405 scrape)

-- this is the query that generates xfm_glptrns:
-- insert into scpp.xfm_glptrns (row_type,stock_number,gl_date,gl_count,run_date)
select 'New',gtctl_, gtdate,
  sum(case when gttamt < 0 then 1 else -1 end) as unit_count
from scpp.ext_glptrns_for_deals a
where gtpost = 'Y'
--   AND not exists (
--     select 1
--     from scpp.xfm_glptrns
--     where stock_number = a.gtctl_
--       and gl_date = a.gtdate)
group by gtctl_, gtdate
order by gtctl_


select * from scpp.deals where stock_number = '26332R'

                
select * from scpp.xfm_glptrns where stock_number = '26332R'     

select * from scpp.ext_glptrns_for_deals where gtctl_ = '26885'


-- ok, these are the vehicles with multiple entries on a day that cancel each other out (by polarity)
select 'New', gtctl_, gtdate, sum(the_count)
from (
  select a.gtctl_, a.gtdate, 
    case 
      when a.gttamt > 0 then 1
      when a.gttamt < 0 then -1
    end as the_count
  from scpp.ext_glptrns_for_deals a
  inner join (
    select gtctl_, gtdate
    from scpp.ext_glptrns_for_deals
    where gtpost = 'Y'
    group by gtctl_, gtdate
    having count(*) > 1) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
  where a.gtpost = 'Y') x  
group by gtctl_, gtdate
having sum(the_count) = 0 

-- so, i should be able to safely exclude those rows from the ext_glptrns xfm to xfm_glptrns

-- this is the query that generates xfm_glptrns:
-- except proved to be too tricky, this left join is ok but i worry that it
-- might exclude rows that should not be excluded based on date and stock#, maybe not, we'll see
-- 26332R is correct now (in deals)
-- insert into scpp.xfm_glptrns (row_type,stock_number,gl_date,gl_count,run_date)
select 'New',a.gtctl_, a.gtdate,
  sum(case when a.gttamt < 0 then 1 else -1 end) as unit_count,
  '%s'
from scpp.ext_glptrns_for_deals a
left join ( -- ok, these are the vehicles with multiple entries on a day that cancel each other out (by polarity)
  select gtctl_, gtdate
  from (
    select a.gtctl_, a.gtdate,
      case when a.gttamt < 0 then 1 ELSE - 1 end as the_count
    from scpp.ext_glptrns_for_deals a
    inner join (
      select gtctl_, gtdate
      from scpp.ext_glptrns_for_deals
      where gtpost = 'Y'
      group by gtctl_, gtdate
      having count(*) > 1) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
    where a.gtpost = 'Y') x
  group by gtctl_, gtdate
  having sum(the_count) = 0) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
where gtpost = 'Y'
  AND not exists (
    select 1
    from scpp.xfm_glptrns
    where stock_number = a.gtctl_
      and gl_date = a.gtdate)
  and b.gtctl_ is null       
group by a.gtctl_, a.gtdate
order by a.gtctl_

/* /> 4/19/16 xfm_deals fails due to multiple rows for vehicle/date in xfm_glptrns *****************************/


/* < 4/19/16 27167 not getting recapped *******************************************************************/
-- fixed
4/14 capped
4/18 uncapped
4/19 capped : does not get into deals

select * from scpp.xfm_deals where stock_number = '27167'

select * from scpp.deals where stock_number = '27167'

select * from scpp.xfm_glptrns where stock_number = '27167'

hmm shows up in xfm_glptrns on 4-19 wth row_type = new and gl_Count = 1
so why does it not get into deals, new in this table signifies stock_number AND date

working on local host (backed up scpp(data & schema), restored to localhost

db_cnx.py: changed pg to localhost

-- delete from scpp.xfm_glptrns where run_date > '04/18/2016';
-- delete from scpp.xfm_deals where run_date > '04/18/2016';
-- delete from scpp.deals where run_date > '04/18/2016';

now, against localhost, rerun the deals stuff for 04/19 step by step
1. ext_deals
2. xfm_deals
  -- new rows
  should not (and does not) show up in "new rows" because it already exists in xfm_deals
                select 'New', a.run_date, a.store_code, a.stock_number, a.vin,
                  a.customer_name, a.primary_sc, a.secondary_sc, a.record_status,
                  a.date_approved, a.date_Capped, a.origination_date, a.model_year,
                  a.make, a.model,
                  1, 'No Change', 'No Change', b.gl_date, b.gl_count
                from scpp.ext_deals a
                left join scpp.xfm_glptrns b on a.run_date = b.run_date and a.stock_number = b.stock_number
                WHERE not exists (
                    select 1
                    from scpp.xfm_deals
                    where stock_number = a.stock_number);  


  -- deals where sc or status have changed - inner join to scpp.deals
  ok, good, shows up here, as it should, record status changing from none to capped

3. xfm_deals_for_update
  -- candidate rows for update, xfm_deals (x) joined with deals (y)
  fucking great, shows up here, just like it should, record_status changing from none to capped
  
4. load_deals
  select * from scpp.xfm_deals_for_update
  a fucking ha, i never implemented the "matrix 2": None to Capped
  that was the problem, fixed
  production: just need to insert the row in scpp.deals
                  
/* /> 4/19/16 27167 not getting recapped *******************************************************************/

/* < 4/29/16 27400XX: sc changed after capped *******************************************************************/
-- fixed
capped 4/6 with JOH as PSC
                EDE AS SSC
reset 4/15 with EDE as PSC
                NONE as SSC 

select * from scpp.xfm_deals where stock_number = '27400xx'                

'New';   '2016-04-05';'RY1';'27400XX';'2G1FK1EJXF9151480';'CHAMBERS-THOMAS, DYLAN';'JOH';'EDE'; 'A';'2016-04-05';'0001-01-01';20160405;'2015';'CHEVROLET';'CAMARO';1;'No Change';'No Change';'<NULL>';<NULL>;f
'Update';'2016-04-06';'RY1';'27400XX';'2G1FK1EJXF9151480';'CHAMBERS-THOMAS, DYLAN';'JOH';'EDE'; 'U';'2016-04-05';'2016-04-06';20160405;'2015';'CHEVROLET';'CAMARO';2;'No Change';'Accepted to Capped';'2016-04-05';1;f
'Update';'2016-04-15';'RY1';'27400XX';'2G1FK1EJXF9151480';'CHAMBERS-THOMAS, DYLAN';'EDE';'None';'U';'2016-04-05';'2016-04-06';20160405;'2015';'CHEVROLET';'CAMARO';3;'PSC from JOH to EDE and SSC from EDE to None';'No Change';'<NULL>';<NULL>;f

select a.*, b.full_name 
from scpp.deals a
left join scpp.sales_Consultants b on a.employee_number = b.employee_number
where stock_number = '27400xx'

201604;'137220';'27400XX';0.5;'2016-04-06';'CHAMBERS-THOMAS, DYLAN';'2015';'CHEVROLET';'CAMARO';'Dealertrack';1;'<NULL>';'Capped';'2016-04-06';'Corey Eden'
201604;'174021';'27400XX';0.5;'2016-04-05';'CHAMBERS-THOMAS, DYLAN';'2015';'CHEVROLET';'CAMARO';'Dealertrack';1;'<NULL>';'Capped';'2016-04-06';'Brandon Johnson'

to fix it :

DO
$$
BEGIN
  delete from scpp.deals where stock_number = '27400XX' and employee_number = '174021';
  update scpp.deals
  set unit_count = 1.0
  where stock_number = '27400XX'
    and employee_number = '137220';
END
$$;

but need to fix the logic that detects and persists post cap sc changes                

/* /> 4/29/16 27400XX: sc changed after capped *******************************************************************/

/* < 4/29/16 after fixing 27400XX: sc changed after capped *******************************************************/

brandon johnson, 174021, now has 0 deals for 201604
but this update does not change his unit_count in s_c_data to 0 because the subquery is a null set

need to change the subquery to handle this and return 0 instead of null
  make the base table s_c_config or s_c_data maybe

anyway, for now, quick fix
update scpp.sales_consultant_data 
set unit_count_sys = 0,
    unit_count = 0,
    unit_count_x_per_unit = 0
where employee_number = '174021'
  and year_month = 201604
  
update scpp.sales_consultant_data w
set unit_count_sys = coalesce(x.the_count, 0),
    unit_count = coalesce(x.the_count, 0) + w.unit_count_ovr
from (
  select a.year_month, a.employee_number, sum(a.unit_count) as the_count
  from scpp.deals a
  inner join scpp.months b on a.year_month = b.year_month
    and b.open_closed = 'open'
  group by a.year_month, a.employee_number) x
where w.year_month = x.year_month
  and w.employee_number = x.employee_number

select a.year_month, a.employee_number, sum(a.unit_count) as the_count
from scpp.deals a
inner join scpp.months b on a.year_month = b.year_month
  and b.open_closed = 'open'
where a.employee_number = '174021'  
group by a.year_month, a.employee_number                  

/* /> 4/29/16 after fixing 27400XX: sc changed after capped *******************************************************/

