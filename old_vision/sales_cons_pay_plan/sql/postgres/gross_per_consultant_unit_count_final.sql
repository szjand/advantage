﻿-- -- 2/21/17
-- still struggling with generating count by consultant
-- gross by consuiltant is not a problem, i don't care how or why the dough ended up in the account
-- for the vehicle but there it is, so there will be some weirdness, like a year old deal chargeback
-- not a problem

-- 2/21 end of the day
after myinitial exuberance about finally allocating fs count to sales consultants
i realized that's great but it's not good enuf
need the fucking sales consultant from the deal
its not enuf to get a subset of consultants from scpp
fuck
has to be bopslss or bopmast
bopslss will not work without bopmast
shit has to be bopmast
actually looks like scpp.ext_deals might work out
does not go back far enuf at this point in time to do january
does not go back far enuf at this point in time to do january
use xfm_deals with date limit

-- 2/22 got it
-- still need to do a consultant report from arkona to check the non-scpp consultant counts

-- this is the count query from gross_per_consultant_3
-- create temp table unit_counts as
select store, page, line, line_label, sum(unit_count) as unit_count
-- select sum(unit_count) -- total count
-- select  -- new/used
--   sum(case when page = 16 then unit_count else 0 end) as used,
--   sum(case when page <> 16 then unit_count else 0 end) as new
from ( -- unit count base
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201702
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'  ) h
group by store, page, line, line_label
order by store, page, line


drop table if exists step_1;
create temp table step_1 as
-- include stocknumber on each row
select store, page, line, line_label, control, sum(unit_count) as unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201702
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201702
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y') h
group by store, page, line, line_label, control
order by store, page, line

-- ok, count is still good
select store, page, line, line_label, sum(unit_count)
from (
  select *
  from step_1) a
group by store, page, line, line_label
order by store, page, line, line_label

-- wa fucking hoo, i think this is it
-- this looks good    
drop table if exists step_2;      
create temp table step_2 as
select a.*, coalesce(b.employee_number, 'HSE') as employee_number
from step_1 a
left join (
  select *
  from scpp.deals t
  where year_month = 201702
    AND seq = (
      select max(seq)
      from scpp.deals
      where year_month = 201702
        and stock_number = t.stock_number)) b on a.control = b.stock_number;

-- line count still good
select store,page,line,line_label,sum(unit_count)
from step_2
group by store,page,line,line_label
order by store,page,line,line_label

-- and this is good, as far as it goes, 
-- the only problem is that it only includes the sales consultants
-- from scpp
-- leaves out garceau, olderbak and honda consultants    
select store, employee_number, sum(unit_count)
from step_2
group by store, employee_number
order by employee_number

select * from step_2

     
drop table if exists step_2;
create temp table step_2 as
select a.*, coalesce(b.primary_sc, 'HSE') as primary_sc, coalesce(b.secondary_sc, 'None') as secondary_sc
from step_1 a
left join (
  select *
  from scpp.xfm_deals a
  where seq = (
    select max(seq)
    from scpp.xfm_deals
    where stock_number = a.stock_number
      and run_date between '02/01/2017' and '02/28/2017')) b on a.control = b.stock_number

-- line count
select store,page,line,line_label,sum(unit_count)
from step_2
group by store,page,line,line_label
order by store,page,line,line_label

-- page count
select store,page,sum(unit_count)
from step_2
group by store,page
order by store,page

-- 2/22 so, as usual, the count by page/line are correct, the issue is the 
-- correct allocation of the count to consultants

-- 1/22 they finally match
-- from count
select store, primary_sc, secondary_sc, sum(unit_count)
from step_2
group by store, primary_sc, secondary_sc
order by primary_sc, secondary_sc

-- from payroll
select ry1_id, ry2_id, sum(unit_count)
from scpp.deals a
inner join scpp.sales_consultants b on a.employee_number = b.employee_number
where a.year_month = 201701
group by ry1_id, ry2_id
order by ry1_id, ry2_id

select * from step_2 where primary_sc = 'sea' order by control
-- 
-- do count for several months in one query at line level
-- it works
select store, page, line, line_label, 
  sum(case when year_month = 201601 then unit_count else 0 end) as January,
  sum(case when year_month = 201604 then unit_count else 0 end) as April,
  sum(case when year_month = 201608 then unit_count else 0 end) as August,
  sum(case when year_month = 201611 then unit_count else 0 end) as November
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount, b.year_month,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month in (201601, 201604, 201608, 201611)
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701 -- only need one year_month here to generate page/line/label/accounts
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'  ) h
group by store, page, line, line_label
order by store, page, line


-- 3/29/17
-- try to match the doc
-- break out new fleet and internal vs retail

select store, --page, line, line_label, 
  sum(case when page = 5 and (line between 1 and 20 or line between 25 and 40) then unit_count else 0 end) as "New Chev",
  sum(case when page = 8 and (line between 1 and 20 or line between 25 and 40) then unit_count else 0 end) as "New Buick",
  sum(case when page = 10 and (line between 1 and 20 or line between 25 and 40) then unit_count else 0 end) as "New GMC",
  sum(case when page = 9 and (line between 1 and 20 or line between 25 and 40) then unit_count else 0 end) as "New Cad",
  sum(case when page = 16 and line between 1 and 7 then unit_count else 0 end) as "Used Retail",
  sum(case when page = 16 and line between 8 and 12 then unit_count else 0 end) as "Used Whsl"
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount, b.year_month,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201703 
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201702 -- only need one year_month here to generate page/line/label/accounts
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'  ) h
group by store--, page, line, line_label
order by store, page, line

-- chev off by one
-- traverse missing 1
-- trax appeared to be missing, but it is page 5 line 37 and my line label says BO NOT USE
-- but it matches deal analysis exactly
traverse missing one
select store, page, line, line_label, sum(unit_count)
-- select control, unit_count
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount, b.year_month,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201703 
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701 -- only need one year_month here to generate page/line/label/accounts
      and (
        (b.page between 5 and 15 and b.line between 1 and 45)) 
--         or
--         (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'ry1') d on c.account = d.gl_account
  where a.post_status = 'Y'  ) h
-- where page = 5  
-- order by length(control), control
group by store, page, line, line_label
order by store, page, line

-- hmm showed trax for 2015 only
select * from arkona.ext_eisglobal_sypfflout where flpage = 5 and flflne = 37

-- 4/1/17: ry1 used car retail: doc: 202 deal analysis: 205 fact_gl: 200
-- this general approack to grouping and counting doesn't work for 29910XXA, sold in february
-- unwound and sold in march
-- able to match deal analysis count (205)
-- need some kind of algorithm to do high level count
select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
  a.control, a.amount, b.year_month,
  case when a.amount < 0 then 1 else -1 end as unit_count
-- select control-- , case when a.amount < 0 then 1 else -1 end as unit_count
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201703 
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- d: fs gm_account page/line/acct description
  select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201702 -- only need one year_month here to generate page/line/label/accounts
    and  b.page = 16 
    and b.line between 1 and 7 -- used cars
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code = '4'
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
    and f.store = 'ry1') d on c.account = d.gl_account
inner join fin.dim_journal e on a.journal_key = e.journal_key    
  and e.journal_code = 'VSU'
where a.post_status = 'Y'
-- group by control having sum(case when a.amount < 0 then 1 else -1 end) > 0
order by control

select *
from scpp.xfm_deals where vin = '1GCVKREC1EZ245738'