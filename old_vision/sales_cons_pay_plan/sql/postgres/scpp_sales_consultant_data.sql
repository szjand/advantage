﻿select *
from scpp.sales_consultant_Data

insert into scpp.sales_consultant_Data
select year_month, employee_number
from scpp.sales_consultant_configuration
where year_month = 201601

-- draw_paid 201601
update scpp.sales_consultant_data a
set draw_paid = x.draw
from (
  select a.employee_number, a.last_name, 
    a.first_name, c.amount as draw, c.description
  from scpp.sales_consultants a
  inner join dds.ext_pyhshdta b on a.employee_number = b.employee_
    and b.check_year = 16
    and b.check_month = 1
  inner join dds.ext_pyhscdta c on a.employee_number = c.employee_number
    and b.payroll_run_number = c.payroll_run_number  
    and c.code_id = '78'
  where b.employee_ is not null) x
where a.year_month = 201601
  and a.employee_number = x.employee_number

-- 201601 paychecks draws, commission & pto only, leave out lease program
-- 2/17/16 this has to be determined
-- of these codes, what should this app expose
-- '400';'PAY OUT PTO'
-- '78';'DRAWS'
-- '82';'SPIFF PAY OUTS'
-- '74';'SALES VACATION'
-- '500';'LEASE PROGRAM'
-- '66';'Training'
-- '79';'COMMSSIONS'
-- '79';'COMMISSIONS'
-- '62';'EXTRA (MISC)'

select x.*, sum(amount) over(partition by employee_number) as month_total
from (
  select b.check_day, b.payroll_run_number, a.employee_number, a.last_name, 
    a.first_name, b.total_gross_pay,
    c.code_id, c.amount, c.description
  from scpp.sales_consultants a
  left join dds.ext_pyhshdta b on a.employee_number = b.employee_
    and b.check_year = 16
    and b.check_month = 1
  left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
    and b.payroll_run_number = c.payroll_run_number  
    and c.code_type = '1'
--    and c.code_id in ('78','79')
    and c.code_id in (
      select c.code_id
      from scpp.sales_consultants a
      left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
        and c.payroll_cen_year > 114
        and c.code_type = '1'
      where c.employee_number is not null
      group by c.code_id)   
  where b.employee_ is not null) x   
order by x.employee_number, x.check_day



  select c.code_id
  from scpp.sales_consultants a
  left join dds.ext_pyhscdta c on a.employee_number = c.employee_number
    and c.payroll_cen_year > 114
    and c.code_type = '1'
  where c.employee_number is not null
  group by c.code_id





/* 
pto clock hours
*/
-- first get some rows in sc_data
insert into scpp.sales_consultant_data (year_month, employee_number)
select year_month, employee_number
from scpp.sales_consultant_configuration;

drop table scpp.pto_hours cascade;
create table scpp.pto_hours (
  year_month integer not null,
  the_date date not null,
  employee_number citext not null,
  pto_hours numeric(5,2) default 0,
constraint pto_hours_pk primary key (the_date,employee_number)); 

-- update the pto_hours
update scpp.sales_consultant_data
set pto_hours = x.pto_hours
from (
  select aa.employee_number, b.year_month, sum(pto_hours) as pto_hours
  from scpp.sales_consultants aa
  inner join scpp.pto_hours b ON aa.employee_number = b.employee_number 
  group by aa.employee_number, b.year_month) x
where scpp.sales_consultant_data.employee_number = x.employee_number
  and scpp.sales_consultant_data.year_month = x.year_month  

update scpp.sales_consultant_data
set pto_pay_calc = z.pto
from (
  select a.year_month, a.employee_number, a.pto_hours, b.pto_rate, a.pto_hours * b.pto_rate as pto
  from scpp.sales_consultant_data a
  inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
    and a.year_month = b.year_month
  where a.pto_hours > 0) z
where scpp.sales_consultant_data.employee_number = z.employee_number
  and scpp.sales_consultant_data.year_month = z.year_month  
      


/*            
email capture
*/
-- 90 day window
-- 2/12/16 sales_consultant_metric_data
-- fuck it initially, everybody wins
-- insert into scpp.sales_consultant_metric_data (year_month,metric,employee_number,metric_value,metric_qualified)
-- select a.year_month, b.metric, a.employee_number, metric_value, true
-- from scpp.sales_consultant_configuration a
-- left join scpp.metrics b on b.year_month = 201601
-- where a.year_month = 201601

select * from rv.cstfb_email_data
select * 
from scpp.sales_consultant_metric_data
where metric = 'email capture'
order by metric_value desc 

update scpp.sales_consultant_metric_data
set metric_value = x.percentage,
    metric_qualified = case when x.percentage >= (
      select metric_value
      from scpp.metrics
      where year_month = 201601
        and metric = 'email capture') then true else false end
from (    
  SELECT employee_name, employee_number, '2016-01-31'::date - 90, '2016-01-31'::date, COUNT(*),
    SUM(CASE WHEN has_valid_email THEN 1 ELSE 0 END)  as email_capture,
    round(100.0 * SUM(CASE WHEN has_valid_email THEN 1 ELSE 0 END)/COUNT(*), 1) as percentage
  FROM rv.cstfb_email_data a
  WHERE a.transaction_date BETWEEN '2016-01-31'::date - 90 and '2016-01-31'::date
    AND a.transaction_type = 'sale'
  GROUP BY employee_name, employee_number, '2016-01-31'::date - 90, '2016-01-31'::date) x
where sales_consultant_metric_data.year_month = 201601
  and metric = 'email capture'
  and sales_consultant_metric_data.employee_number = x.employee_number;

--and update the rest to 0
update scpp.sales_consultant_metric_Data
set metric_value = 0,
    metric_qualified = false
where year_month = 201601
  and metric <> 'email capture';


 -- 2/17 so all this shit is great, put it into a single script to simulate
 --     the nightly update, this then will be the source data for populating
 --     the admin page

-- unit count
update scpp.sales_consultant_data a
set unit_count_sys = x.unit_count,
    unit_count_ovr = 0,
    unit_count = x.unit_count
from (  
  select employee_number, sum(unit_count) as unit_count
  from scpp.deals a
  where year_month = 201601
  group by employee_number) x 
where a.employee_number = x.employee_number 

-- metrics
with open_month 
  as (
    select year_month
    from scpp.months
    where open_closed = 'open')
select a.employee_number, 
  case 
    when a.metric_qualified = true and b.metric_qualified = true 
      and c.metric_qualified = true and d.metric_qualified = true then true
    else false
  end as metrics_qualified,
  a.metric_value as auto_alert_score,
  a.metric_qualified as auto_alert_qualified,
  b.metric_value as csi_score,
  b.metric_qualified as csi_qualified,  
  c.metric_value as email_score,
  c.metric_qualified as email_qualified,
  d.metric_value as logged_score,
  d.metric_qualified as logged_qualified
from scpp.sales_consultant_metric_data a
inner join open_month aa on 1 = 1
left join scpp.sales_consultant_metric_data b on a.employee_number = b.employee_number
  and b.year_month = aa.year_month
  and b.metric = 'csi'
left join scpp.sales_consultant_metric_data c on a.employee_number = c.employee_number
  and c.year_month = aa.year_month
  and c.metric = 'Email Capture'  
left join scpp.sales_consultant_metric_data d on a.employee_number = d.employee_number
  and d.year_month = aa.year_month
  and d.metric = 'Logged Opportunity Minimum'    
where a.year_month = aa.year_month
  and a.metric = 'Auto Alert Calls per Month'


 -- 2/18  ok, after refactoring sales_Consultant_data to be the whole magilla
 -- this is the script for overnight update to s_c_data
 -- 1. assume config already done for the month
 insert into scpp.sales_consultant_data (year_month, store_code, full_name, employee_number,
   hire_date, pay_plan_naume, draw, full_months_employment, years_service



insert into scpp.sales_consultant_data
select id,year_month,store_code,full_name,employee_number,hire_date,pay_plan_name,
  draw,draw_paid,full_months_employment,years_service,pto_rate,pto_hours,pto_pay,
  additional_comp,unpaid_time_off,guarantee,unit_count_sys,unit_count_ovr,unit_count,
  per_unit,metrics_qualified,csi_score,csi_qualified,email_score,email_qualified,
  logged_score,logged_qualified,auto_alert_score,auto_alert_qualified,
  unit_count * per_unit asunit_count_x_per_unit,
  total_pay,total_pay_paid,notes,complete, store_name
from (   
with open_month 
  as (
    select year_month
    from scpp.months
    where open_closed = 'open')
select b.id, aa.year_month, a.store_code, a.full_name, a.employee_number, a.hire_date, 
  b.pay_plan_name, b.draw, 
  0 as draw_paid, 
  b.full_months_employment, b.years_service,
  b.pto_rate, coalesce(c.pto_hours,0) as pto_hours, round(b.pto_rate * coalesce(c.pto_hours,0), 2) as pto_pay,
  0 as additional_comp, 0 as unpaid_time_off,
    case
      when b.full_months_employment < 4 then i.guarantee_new_hire
      when e.metrics_qualified = true then i.guarantee_with_qual
      else i.guarantee
    end as guarantee,    
    coalesce(g.unit_count_sys, 0) as unit_count_sys, 
    coalesce(g.unit_count_ovr, 0) as unit_count_ovr, 
    coalesce(g.unit_count, 0) as unit_count, 
  case
    when e.metrics_qualified then (
      select tar
      from scpp.per_unit_matrix a
      inner join open_month aa on a.year_month = aa.year_month
      where units @> floor(coalesce(g.unit_count,0))::integer
        and pay_plan_name = b.pay_plan_name) 
    else (
      select map
      from scpp.per_unit_matrix a
      inner join open_month aa on a.year_month = aa.year_month
      where units @> floor(coalesce(g.unit_count,0))::integer
        and pay_plan_name = b.pay_plan_name) 
    end as per_unit,     
    e.metrics_qualified, e.auto_alert_score, e.auto_alert_qualified,
    e.csi_score, e.csi_qualified,  
    e.email_score, e.email_qualified,
    e.logged_score, e.logged_qualified,
    0 as total_pay, 0 as total_pay_paid,
    '' as notes,
    false as complete,
    case
      when store_code = 'RY1' then 'GM'
      when store_code = 'RY2' then 'Honda Nissan'
    end as store_name
-- select full_name  
from scpp.sales_consultants a
inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
inner join open_month aa on b.year_month = aa.year_month
left join (
  select employee_number, sum(pto_hours) as pto_hours
  from scpp.pto_hours a
  inner join open_month aa on a.year_month = aa.year_month
  group by employee_number) c on a.employee_number = c.employee_number  
  inner join (
    select a.employee_number, 
      case 
        when a.metric_qualified = true and b.metric_qualified = true 
          and c.metric_qualified = true and d.metric_qualified = true then true
        else false
      end as metrics_qualified,
      a.metric_value as auto_alert_score,
      a.metric_qualified as auto_alert_qualified,
      b.metric_value as csi_score,
      b.metric_qualified as csi_qualified,  
      c.metric_value as email_score,
      c.metric_qualified as email_qualified,
      d.metric_value as logged_score,
      d.metric_qualified as logged_qualified
    from scpp.sales_consultant_metric_data a
    inner join open_month aa on 1 = 1
    left join scpp.sales_consultant_metric_data b on a.employee_number = b.employee_number
      and b.year_month = aa.year_month
      and b.metric = 'csi'
    left join scpp.sales_consultant_metric_data c on a.employee_number = c.employee_number
      and c.year_month = aa.year_month
      and c.metric = 'Email Capture'  
    left join scpp.sales_consultant_metric_data d on a.employee_number = d.employee_number
      and d.year_month = aa.year_month
      and d.metric = 'Logged Opportunity Minimum'    
    where a.year_month = aa.year_month
      and a.metric = 'Auto Alert Calls per Month') e on a.employee_number = e.employee_number 
  left join (
    select employee_number, 
      sum(case when deal_source = 'sys' then unit_count else 0 end) as unit_count_sys,
      sum(case when deal_source = 'ovr' then unit_count else 0 end) as unit_count_ovr,
      sum(unit_count) as unit_count
    from scpp.deals a
    inner join open_month aa on a.year_month = aa.year_month
    group by employee_number) g on a.employee_number = g.employee_number      
  left join scpp.guarantee_matrix i on aa.year_month = i.year_month  
    and b.pay_plan_name = i.pay_plan_name) x    
where x.pay_plan_name = 'standard'
  and x.store_code = 'RY1'      
order by full_name  


select *
from scpp.sales_consultant_data
 
 