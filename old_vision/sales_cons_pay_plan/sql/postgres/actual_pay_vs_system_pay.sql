﻿select a.year_month, a.employee_number, d.last_name, d.first_name, a.total_pay as system_pay, b.total_paid, unit_count, per_unit, csi_score, email_score, logged_score, auto_alert_score, c.note
from scpp.sales_Consultant_data a
left join (
  select 100 * (2000 + payroll_ending_year) + payroll_ending_month as year_month, 
    a.employee_, sum(total_gross_pay) as total_paid
  from dds.ext_pyhshdta a
  group by 100 * (2000 + payroll_ending_year) + payroll_ending_month, 
    a.employee_) b on a.year_month = b.year_month and a.employee_number = b.employee_
left join (
  select year_month, employee_number, string_agg(trim(note), ' - ') as note
  from scpp.admin_notes
  group by year_month, employee_number) c  on a.employee_number = c.employee_number and a.year_month = c.year_month
left join scpp.sales_consultants d on a.employee_number = d.employee_number  
where a.year_month between 201602 and 201609
order by d.last_name, d.first_name, a.year_month    



select year_month, employee_number, string_agg(trim(note), ' - ') 
from scpp.admin_notes
where employee_number = '128530'
  and year_month = 201604
group by year_month, employee_number  

select * from scpp.sales_consultant_data where full_name like '%stro%'

update scpp.sales_consultant_data
set full_name = 'Lakeith Strother'
where full_name = 'Lekeith Strother'


-- as a result of all the shit i had to fix for november 2016


-- from arkona, commissions and draw only (based on codes in pyhscdta)
-- does not include spiffs (Spiff pay outs)
drop table if exists _arkona;
create temp table _arkona as
select a.employee_name, a.employee_, a.total_gross_pay as check_total, b.description as check_description, 
    check_month,check_day,check_year, 
    c.amount, c.description as code_description
--select *
from dds.ext_pyhshdta a
inner join  dds.ext_PYPTBDTA b on a.payroll_run_number = b.payroll_run_number
  and a.company_number = b.company_number
inner join dds.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number
  and a.company_number = c.company_number
  and a.employee_ = c.employee_number
  and trim(c.code_id) in ('79','78') --commissions, draws
-- where trim(a.employee_) = '128530'
where trim(a.distrib_code) = 'SALE'
  and a.payroll_cen_year = 116
  and a.payroll_ending_month between 9 and 11
  and a.company_number = 'RY1'
order by check_month, employee_  

select a.employee_number, full_name, full_months_employment, unit_count, pto_pay, draw, guarantee, metrics_qualified, 
  additional_comp, per_unit, unit_count_x_per_unit, --total_pay, employee_number,
  additional_comp + unit_count_x_per_unit as add_comp_plus_earned, 
  b.total_draw as total_draw_paycheck, b.total_commission as total_commission_paycheck, 
  c.admin_note,
  b.total as total_paycheck_comm_draw, 
  (select total_pay from scpp.get_total_pay(201611,a.employee_number)) as total_pay_function, 
  aa.total_pay as total_pay_sc_data
from scpp.sales_Consultant_data a
left join lateral(
  select *
  from scpp.get_total_pay(201611,a.employee_number)) aa on a.employee_number = aa.employee_number
left join (
  select employee_name, employee_, check_month,
    sum(case when code_description = 'DRAWS' then amount else 0 end) as total_draw,
    sum(case when code_description = 'COMMISSIONS' then amount else 0 end) as total_commission,
    sum(amount) as total
  from _arkona
  where check_month = 11
  group by employee_name, employee_, check_month) b on a.employee_number = b.employee_  
left join (
  select employee_number, string_agg(note, '-') as admin_note
  from scpp.admin_notes
  where year_month = 201611
  group by employee_number) c on a.employee_number = c.employee_number  
where a.year_month = 201611
order by a.employee_number   



-- 12/66/16 arkona the real deal
-- different grains: pyhshdta, pyhscdta
-- need 1 row per employee/month
-- leave out name, pyhshdta has 2 versions of jeff tarr's name
select * from (
select m.*, n.cat, n.total
from ( -- pyhshdta
  select a.employee_, check_month, sum(a.total_gross_pay) as total_gross
  from dds.ext_pyhshdta a
  where trim(a.distrib_code) = 'SALE'
    and a.payroll_cen_year = 116
    and a.payroll_ending_month between 9 and 11
    and a.company_number = 'RY1'
  group by a.employee_, check_month) m
left join ( -- pyhscdta
  select employee_, check_month, 
    string_agg(description || ':' || amount::citext, ' | ') as cat,
    sum(amount) as total
  from ( 
    select a.employee_name, a.employee_, a.check_month, b.description, sum(b.amount) as amount
    --select distinct c.description
    from dds.ext_pyhshdta a
    inner join dds.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
      and a.company_number = b.company_number
      and a.employee_ = b.employee_number
      and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    where trim(a.distrib_code) = 'SALE'
      and a.payroll_cen_year = 116
      and a.payroll_ending_month between 9 and 11
      and a.company_number = 'RY1'
    group by a.employee_name, a.employee_, a.check_month, b.description) q
  group by  employee_, check_month) n on m.employee_ = n.employee_ and m.check_month = n.check_month
 ) x where total_gross <> total
order by m.employee_, m.check_month

-- and combine it with scpp
-- allen preston 201611 fixed
-- scott pearson september 
-- tanner trosen november
select e.full_name, e.employee_number, e.year_month, e.unit_count, e.total_pay as vision_total_pay, 
  f.arkona_comm_draw, f.arkona_total_gross, f.cat
from scpp.sales_consultant_data e
left join (
  select m.*, n.arkona_comm_draw, n.cat
  from ( -- pyhshdta
    select a.employee_, check_month, 201600 + check_month as year_month, sum(a.total_gross_pay) as arkona_total_gross
    from dds.ext_pyhshdta a
    where trim(a.distrib_code) = 'SALE'
      and a.payroll_cen_year = 116
      and a.payroll_ending_month between 9 and 11
      and a.company_number = 'RY1'
    group by a.employee_, check_month) m
  left join ( -- pyhscdta
    select employee_, check_month, 
      sum(case when q.code_id in ('79','78') then amount end) as arkona_comm_draw,
      string_agg(q.description || ':' || q.amount::citext, ' | ' order by q.description) as cat
    from ( 
      select a.employee_name, a.employee_, a.check_month, b.description, sum(b.amount) as amount, b.code_id
      --select distinct c.description
      from dds.ext_pyhshdta a
      inner join dds.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
        and a.company_number = b.company_number
        and a.employee_ = b.employee_number
        and b.code_type in ('0','1') -- 0,1: income, 2: deduction
      where trim(a.distrib_code) = 'SALE'
        and a.payroll_cen_year = 116
        and a.payroll_ending_month between 9 and 11
        and a.company_number = 'RY1'
      group by a.employee_name, a.employee_, a.check_month, b.description, b.code_id) q
    group by  employee_, check_month) n on m.employee_ = n.employee_ and m.check_month = n.check_month) f on e.employee_number = f.employee_
      and e.year_month = f.year_month
where e.year_month between 201609 and 201611
  and f.year_month is not null
order by e.full_name, e.year_month

