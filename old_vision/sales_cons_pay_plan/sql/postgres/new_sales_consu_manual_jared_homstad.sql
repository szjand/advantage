﻿10/7/16
jared homstad has been moved from desking mgr to sales cons
this morning load deals failed because he sold a car as a sc, but his status has
not been changed, in either payroll or compli
i have verified that this is all true, 
so, process him into scpp manually

1. ext_sales_consultants
insert into scpp.ext_sales_consultants
select a.storecode, a.employeenumber, a.lastname, a.firstname, trim(a.firstname) || ' ' || a.lastname,
  case
    when (select dds.db2_integer_to_date(d.org_hire_date)) < a.hiredate
      then (select dds.db2_integer_to_date(d.org_hire_date))
    else a.hiredate
  end,
  a.termdate, e.username,
  b.sales_person_id as ry1_id, c.sales_person_id as ry2_id,
  a.hiredate as last_hire_date,
  case
    -- latest hiredate <= first wkg day of the month then yearmonth of latest hiredate
    when a.hiredate <= (-- aa.sc_first_working_day_of_month -- nope
      select sc_first_working_day_of_month
      from scpp.months
      where year_month = extract(year from a.hiredate) * 100 + extract(month from a.hiredate))
      then (
        select year_month
        from scpp.months
        where a.hiredate between first_of_month and last_of_month)
    else (
    -- latest hiredate > first wkg day of the month then yearmonth of latest hiredate + 1
      select year_month
      from scpp.months
      where seq = ( --aa.seq + 1)
        select seq
        from scpp.months
        where year_month = extract(year from a.hiredate) * 100 + extract(month from a.hiredate)) + 1)
  end,
  (select dds.db2_integer_to_date(d.org_hire_date)) as orig_hire_date
from ads.ext_dds_edwEmployeeDim a
inner join scpp.months aa on 1 = 1
  and aa.open_closed = 'open'
left join dds.ext_bopslsp b on a.employeenumber = b.employee_number
  and b.company_number = 'RY1'
  and b.sales_person_type = 'S'
left join dds.ext_bopslsp c on a.employeenumber = c.employee_number
  and c.company_number = 'RY2'
  and c.sales_person_type = 'S'
left join dds.ext_pymast d on a.employeenumber  = d.pymast_employee_number
left join ads.ext_sco_tpemployees e on a.employeenumber = e.employeenumber
where a.currentrow = true
  and a.employeenumber = '167600';

2. xfm_Sales_consultants
insert into scpp.xfm_sales_consultants
select 'New' as row_type, a.*, current_date
from scpp.ext_sales_consultants a
where employee_number = '167600'

3. load_sales_Consultants
-- a little goofy, he is already in sales_consultant_configuration for201601 - 201604
select *
from scpp.sales_consultant_configuration
where employee_number = '167600'
-- sales_consultant_configuration
insert into scpp.sales_consultant_configuration (employee_number, year_month, pay_plan_name,
                    draw, full_months_employment, years_service)
values('167600', 201610, 'Standard', 500, 18, 1);

-- sales_consultant_data
insert into scpp.sales_consultant_data (year_month,store_code,full_name,employee_number,
  hire_date,pay_plan_name,draw,
  full_months_employment,years_service,
  store_name)
select c.year_month, a.store_code, a.full_name, a.employee_number, a.hire_Date,
b.pay_plan_name, b.draw, b.full_months_employment,
b.years_service,
case a.store_code
  when 'RY1' then 'GM'
  when 'RY2' then 'Honda Nissan'
end as store_name
from scpp.xfm_sales_consultants a
inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
inner join scpp.months c on b.year_month = c.year_month
  and c.open_closed = 'open'
where a.employee_number = '167600';

-----------------------------------------------------------------------------------------------------
-- 1/7/17 -------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------

select *
from scpp.sales_consultants
order by last_name


jared homstad is back to mgmt (digital)
bryn seay is back to consultant

homstad: 167600
seay: 1124625
they both exist in scpp.sales_consultants, fine, 

select *
from scpp.sales_consultant_data
where year_month = 201701

select *
from scpp.sales_consultant_configuration
where year_month = 201701

1. remove jared from scpp.sales_consultant_data & scpp.sales_consultant_configuration

delete
-- select *
from scpp.pto_intervals
where employee_number = '167600'
  and year_month = 201701;
  
delete 
-- select * 
from scpp.sales_consultant_data
where employee_number = '167600'
  and year_month = 201701;

delete 
-- select * 
from scpp.sales_consultant_configuration
where employee_number = '167600'
  and year_month = 201701;

2. add bryn
1. ext_sales_consultants
insert into scpp.ext_sales_consultants
select a.storecode, a.employeenumber, a.lastname, a.firstname, trim(a.firstname) || ' ' || a.lastname,
  case
    when (select dds.db2_integer_to_date(d.org_hire_date)) < a.hiredate
      then (select dds.db2_integer_to_date(d.org_hire_date))
    else a.hiredate
  end,
  a.termdate, e.username,
  b.sales_person_id as ry1_id, c.sales_person_id as ry2_id,
  a.hiredate as last_hire_date,
  case
    -- latest hiredate <= first wkg day of the month then yearmonth of latest hiredate
    when a.hiredate <= (-- aa.sc_first_working_day_of_month -- nope
      select sc_first_working_day_of_month
      from scpp.months
      where year_month = extract(year from a.hiredate) * 100 + extract(month from a.hiredate))
      then (
        select year_month
        from scpp.months
        where a.hiredate between first_of_month and last_of_month)
    else (
    -- latest hiredate > first wkg day of the month then yearmonth of latest hiredate + 1
      select year_month
      from scpp.months
      where seq = ( --aa.seq + 1)
        select seq
        from scpp.months
        where year_month = extract(year from a.hiredate) * 100 + extract(month from a.hiredate)) + 1)
  end,
  (select dds.db2_integer_to_date(d.org_hire_date)) as orig_hire_date
from ads.ext_dds_edwEmployeeDim a
inner join scpp.months aa on 1 = 1
  and aa.open_closed = 'open'
left join dds.ext_bopslsp b on a.employeenumber = b.employee_number
  and b.company_number = 'RY1'
  and b.sales_person_type = 'S'
left join dds.ext_bopslsp c on a.employeenumber = c.employee_number
  and c.company_number = 'RY2'
  and c.sales_person_type = 'S'
left join dds.ext_pymast d on a.employeenumber  = d.pymast_employee_number
left join ads.ext_sco_tpemployees e on a.employeenumber = e.employeenumber
where a.currentrow = true
  and a.employeenumber = '1124625';

2. xfm_Sales_consultants
insert into scpp.xfm_sales_consultants
select 'New' as row_type, a.*, '01/01/2017'
from scpp.ext_sales_consultants a
where employee_number = '1124625';


3. load_sales_Consultants
-- -- a little goofy, he is already in sales_consultant_configuration for201601 - 201604
-- select *
-- from scpp.sales_consultant_configuration
-- where employee_number = '1124625'
-- sales_consultant_configuration
insert into scpp.sales_consultant_configuration (employee_number, year_month, pay_plan_name,
                    draw, full_months_employment, years_service)
select a.employee_number, b.year_month, 'Standard', 500,
  case
    when b.seq - c.seq < 0 then 0
    else b.seq - c.seq - 1
  end as full_months_employment,
  (b.last_of_month - a.hire_date)/365 as years_service
from scpp.xfm_sales_consultants a
inner join scpp.months b on 1 = 1
  and b.open_closed = 'open'
-- inner join scpp.months c on a.hire_Date between c.first_of_month and c.last_of_month
inner join scpp.months c on a.first_full_month_employment = c.year_month
where a.row_type = 'New';

4. sales_consultant_data
insert into scpp.sales_consultant_data (year_month,store_code,full_name,employee_number,
  hire_date,pay_plan_name,draw,
  full_months_employment,years_service,
  store_name)
select c.year_month, a.store_code, a.full_name, a.employee_number, a.hire_Date,
b.pay_plan_name, b.draw, b.full_months_employment,
b.years_service,
case a.store_code
  when 'RY1' then 'GM'
  when 'RY2' then 'Honda Nissan'
end as store_name
from scpp.xfm_sales_consultants a
inner join scpp.sales_consultant_configuration b on a.employee_number = b.employee_number
inner join scpp.months c on b.year_month = c.year_month
  and c.open_closed = 'open'
where a.employee_number = '1124625';

                