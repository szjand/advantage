﻿
-------------------------------------------------------------------------------------------------------
----------- Load --------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
-- 0202 new deals
-- exclude garceau and olderbak deals
-- this will actually be new deals as well as deals that previously existed
-- in xfm_ but were not capped (ie not in deals)
-- any deal not already in deals, therefor seq = 1

insert into scpp.deals (year_month,employee_number,stock_number,unit_count,
  deal_date,customer_name,model_year,make,model,deal_source,seq, deal_status)
select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer, 
  coalesce(b.employee_number, 'House'), a.stock_number, 
  case
    when secondary_sc = 'None' then 1
    else .5
  end as unit_count,
  a.date_capped, a.customer_name, 
  a.model_year, a.make, a.model, 'Dealertrack', 1, 'Capped'
from scpp.xfm_deals a
-- inner join, RY1 only: FK constraint ref s_c_config
inner join scpp.sales_consultants b on 
  case
    when a.store_code = 'RY1' then a.primary_sc = b.ry1_id
    when a.store_code = 'RY2' then a.primary_sc = b.ry2_id
  end 
  and b.employee_number not in ('150040','1106225')
-- where row_type = 'New'
WHERE a.date_capped > '12/31/2015'
  AND not exists (
    select *
    from scpp.deals
    where stock_number = a.stock_number)
union 
select (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer, 
  coalesce(b.employee_number, 'House'), a.stock_number, 
  0.5 as unit_count,
  a.date_capped, a.customer_name, 
  a.model_year, a.make, a.model, 'Dealertrack', 1, 'Capped'
from scpp.xfm_deals a
inner join scpp.sales_consultants b on 
  case
    when a.store_code = 'RY1' then a.secondary_sc = b.ry1_id
    when a.store_code = 'RY2' then a.secondary_sc = b.ry2_id
  end 
  and b.employee_number not in ('150040','1106225')
where a.secondary_sc <> 'None' 
--  and a.row_type = 'New'
  AND a.date_capped > '12/31/2015'
  AND not exists (
    select *
    from scpp.deals
    where stock_number = a.stock_number);  



--select * from scpp.xfm_deals where run_date = '02/01/2016'
-- 0202 updates
-- blowing off notes for now
-- deals seq not necessarily the same as xfm_ seq
-- have to figure out unit count
-- so, for unwinds does this mean that deals will have to include non capped deals,
-- i am thinking so, probably should add a deal_status field (Capped, Accepted, None
-- 2/3 should provide some clarity with the unwind

-- have to compare to most recent row
-- 4/10
-- how to deal with decoding the new sc_change and status_change in xfm
-- how to deal with rows going in with a capped date of 01/01/0001
-- -- 2/16 27273 deleted deal example, not picked up in deals
-- -- think the problem is the inner join to deals on emp#, date or status being different
-- -- added to the inner join on deals
-- -- due to the nature of the xfm extract, none of those change in xfm, just the deleted tag
-- -- added status_change to x query, and to outer case for unit_count and deal_status
-- 27273: deleted on 2/16, on 2/20 load bombs: xfm status_change = Capped to Accepted because
-- -- the deleted row still has the record status of U
-- -- tack this where clause on the end of the script
-- -- where not (x.status_change = 'Capped to Accepted' and y.deal_status = 'Deleted')
-- 27273: 2/23 changes to capped load fails with null unit_count
-- -- added this to unit_count case: when x.status_change = 'Accepted to Capped' then 1
26080B: 3/4 capped to Joe Waldorf
        3/8 unwound, accpt to camerea peterson with diff sc
            fails load, no run date because no cap date
            the update to deals should be to the waldorf deal, add a line with a unit count of -1
            decoding has to understand both the xfm_ state as well as the pre update deal state
Foundation: the GL approach, deals are not edited, new rows are added as 
  required with incremented seq
The basic issue is to determine the correct unit_count for the new row as well as 
  the basic make up of the row 
rethink the entire update query, put the candidate rows in a cte,
then a select casing the different combinations, but the candidate rows would need
all possible fields from which to construct a row to insert into deals
so, first: what are the qualifications of a candidate row,
1. the row (stock_number) must exist in deals
2. 2 basic categories:
     1. deal deleted from bopmast
     2. PSC or SSC or deal_Status has changed
3. other variables
     1. deal date in same month or a previous month

Looking at the data, it does not appear that once a deal is capped, the sc changes
so, (i know, it is plural anecdotal, but anyway, i am leaving the sc change
out of the change matrix) and subsequently out of the candidate rows
sc changes will be recorded in xfm, deals will simply show the correct consultant
for each seq of the deal

the variables now become, status_change and deal date

add run_date to deals
alter table scpp.deals
add column run_date date;

tried CTE, not a good idea, store the candidate rows in scpp.xfm_deals_for_update

it all seems to work well now
deals current through 3/16

-- 4/14
-- new deals  -- done
-- now that the gl_date determines both status and date_capped, things are a bit confusing
-- if there is a non null gl_date:
--     deal_date = gl_date
-- else
--     deal_date = date_capped
select 
  case
    when a.gl_date is null then 
      (100* extract(year from a.date_capped) + extract(month from a.date_capped)):: integer
    else
      (100* extract(year from a.gl_date) + extract(month from a.gl_date)):: integer
    end as year_month,
  coalesce(b.employee_number, 'House'), a.stock_number,
  case
    when secondary_sc = 'None' then 1
    else .5
  end as unit_count,
  case
    when a.gl_date is null then a.date_capped
    else a.gl_date
  end as deal_date, a.customer_name,
  a.model_year, a.make, a.model, 'Dealertrack', 1, 'Capped', a.run_date
from scpp.xfm_deals a
-- inner join, RY1 only: FK constraint ref s_c_config
inner join scpp.sales_consultants b on
  case
    when a.store_code = 'RY1' then a.primary_sc = b.ry1_id
    when a.store_code = 'RY2' then a.primary_sc = b.ry2_id
  end
  and b.employee_number not in ('150040','1106225')
where coalesce(a.gl_date, a.date_capped) > '12/31/2015' --i think
  AND not exists (
    select *
    from scpp.deals
    where stock_number = a.stock_number)
