﻿


-- 2/10/16 deals
select year_month, b.employee_number, stock_number,
  case
    when secondary_sc = '' then 1
    else .5
  end as unit_count,
  deal_date, customer_name, model_year, make, model, 'normal'
from scpp.tmp_deals a
inner join scpp.tmp_sales_consultants b on a.store_code = b.store_code
  and a.primary_sc = b.sales_consultant_id
-- left join scpp.tmp_sales_consultants c on a.store_code = c.store_code
--   and a.secondary_sc = c.sales_consultant_id;
union
select year_month, c.employee_number, stock_number,
  .5 AS unit_count,
  deal_date, customer_name, model_year, make, model, 'normal'
from scpp.tmp_deals a
-- left join scpp.tmp_sales_consultants b on a.store_code = b.store_code
--   and a.primary_sc = b.sales_consultant_id
inner join scpp.tmp_sales_consultants c on a.store_code = c.store_code
  and a.secondary_sc = c.sales_consultant_id
where secondary_sc <> '';

drop table if exists scpp.jan_sales_sheets;
create table if not exists scpp.jan_sales_sheets(
  sales_consultant citext,
  stock_number citext);
insert into scpp.jan_sales_sheets values('steve','22545XXZ');
insert into scpp.jan_sales_sheets values('dale','24746RB');
insert into scpp.jan_sales_sheets values('tanner','24765RA');
insert into scpp.jan_sales_sheets values('nate','24920ayc');
insert into scpp.jan_sales_sheets values('steve','24949B');
insert into scpp.jan_sales_sheets values('craig','24949c');
insert into scpp.jan_sales_sheets values('logain','25010ra');
insert into scpp.jan_sales_sheets values('BRYN','25067XXRA');
insert into scpp.jan_sales_sheets values('chris','25075XX');
insert into scpp.jan_sales_sheets values('dale','25152A');
insert into scpp.jan_sales_sheets values('dale','25152B');
insert into scpp.jan_sales_sheets values('craig','25208d');
insert into scpp.jan_sales_sheets values('steve','25276B');
insert into scpp.jan_sales_sheets values('craig','25359ab');
insert into scpp.jan_sales_sheets values('scott','25607a');
insert into scpp.jan_sales_sheets values('jim','25636b');
insert into scpp.jan_sales_sheets values('sam','25667A');
insert into scpp.jan_sales_sheets values('arden','25703b');
insert into scpp.jan_sales_sheets values('tanner','25706bb');
insert into scpp.jan_sales_sheets values('eric','25723');
insert into scpp.jan_sales_sheets values('jesse','25806A');
insert into scpp.jan_sales_sheets values('steve','25806B');
insert into scpp.jan_sales_sheets values('eric','25855a');
insert into scpp.jan_sales_sheets values('dale','25857B');
insert into scpp.jan_sales_sheets values('brett','25867b');
insert into scpp.jan_sales_sheets values('eric','25896');
insert into scpp.jan_sales_sheets values('steve','25901C');
insert into scpp.jan_sales_sheets values('jared','25928b');
insert into scpp.jan_sales_sheets values('larry','25967r');
insert into scpp.jan_sales_sheets values('jim','25991');
insert into scpp.jan_sales_sheets values('nate','26006a');
insert into scpp.jan_sales_sheets values('jared','26049a');
insert into scpp.jan_sales_sheets values('tanner','26052a');
insert into scpp.jan_sales_sheets values('joe','26074');
insert into scpp.jan_sales_sheets values('logain','26074A');
insert into scpp.jan_sales_sheets values('eric','26074b');
insert into scpp.jan_sales_sheets values('tanner','26084C');
insert into scpp.jan_sales_sheets values('nate','26117b');
insert into scpp.jan_sales_sheets values('brad','26132');
insert into scpp.jan_sales_sheets values('larry','26210b');
insert into scpp.jan_sales_sheets values('jesse','26223B');
insert into scpp.jan_sales_sheets values('steve','26225A');
insert into scpp.jan_sales_sheets values('chris','26228XXA');
insert into scpp.jan_sales_sheets values('brad','26228xxb');
insert into scpp.jan_sales_sheets values('steve','26229XX');
insert into scpp.jan_sales_sheets values('chris','26229XXA');
insert into scpp.jan_sales_sheets values('aj','26242b');
insert into scpp.jan_sales_sheets values('scott','26251');
insert into scpp.jan_sales_sheets values('jim','26271r');
insert into scpp.jan_sales_sheets values('joe','26298xxa');
insert into scpp.jan_sales_sheets values('jesse','26302A');
insert into scpp.jan_sales_sheets values('chris','26318A');
insert into scpp.jan_sales_sheets values('craig','26330a');
insert into scpp.jan_sales_sheets values('eric','26330b');
insert into scpp.jan_sales_sheets values('logain','26336aa');
insert into scpp.jan_sales_sheets values('jim','26345');
insert into scpp.jan_sales_sheets values('sam','26346A');
insert into scpp.jan_sales_sheets values('jared','26347c');
insert into scpp.jan_sales_sheets values('BRYN','26351');
insert into scpp.jan_sales_sheets values('jeff p','26355r');
insert into scpp.jan_sales_sheets values('nate','26358pa');
insert into scpp.jan_sales_sheets values('tanner','26371xx');
insert into scpp.jan_sales_sheets values('craig','26385b');
insert into scpp.jan_sales_sheets values('jeff p','26391a');
insert into scpp.jan_sales_sheets values('chris','26401A');
insert into scpp.jan_sales_sheets values('joe','26406a');
insert into scpp.jan_sales_sheets values('jared','26408a');
insert into scpp.jan_sales_sheets values('scott','26408b');
insert into scpp.jan_sales_sheets values('eric','26441');
insert into scpp.jan_sales_sheets values('chris','26457');
insert into scpp.jan_sales_sheets values('BRYN','26462A');
insert into scpp.jan_sales_sheets values('aj','26462b');
insert into scpp.jan_sales_sheets values('jeff p','26469b');
insert into scpp.jan_sales_sheets values('aj','26496a');
insert into scpp.jan_sales_sheets values('dale','26496B');
insert into scpp.jan_sales_sheets values('brett','26500');
insert into scpp.jan_sales_sheets values('jeff p','26523');
insert into scpp.jan_sales_sheets values('sam','26531A');
insert into scpp.jan_sales_sheets values('jared','26565a');
insert into scpp.jan_sales_sheets values('jim','26582xx');
insert into scpp.jan_sales_sheets values('joe','26620');
insert into scpp.jan_sales_sheets values('scott','26631a');
insert into scpp.jan_sales_sheets values('tanner','26637a');
insert into scpp.jan_sales_sheets values('chris','26652');
insert into scpp.jan_sales_sheets values('steve','26666A');
insert into scpp.jan_sales_sheets values('chris','26683A');
insert into scpp.jan_sales_sheets values('chris','26686A');
insert into scpp.jan_sales_sheets values('steve','26686A');
insert into scpp.jan_sales_sheets values('craig','26688a');
insert into scpp.jan_sales_sheets values('chris','26690');
insert into scpp.jan_sales_sheets values('craig','26699b');
insert into scpp.jan_sales_sheets values('craig','26717a');
insert into scpp.jan_sales_sheets values('tanner','26718a');
insert into scpp.jan_sales_sheets values('jared','26731b');
insert into scpp.jan_sales_sheets values('craig','26734xx');
insert into scpp.jan_sales_sheets values('aj','26740xxa');
insert into scpp.jan_sales_sheets values('aj','26740xxb');
insert into scpp.jan_sales_sheets values('aj','26772a');
insert into scpp.jan_sales_sheets values('jared','26773b');
insert into scpp.jan_sales_sheets values('sam','26785A');
insert into scpp.jan_sales_sheets values('chris','26799A');
insert into scpp.jan_sales_sheets values('chris','26802XX');
insert into scpp.jan_sales_sheets values('scott','26836');
insert into scpp.jan_sales_sheets values('tanner','26852a');
insert into scpp.jan_sales_sheets values('jared','26864xxa');
insert into scpp.jan_sales_sheets values('tanner','26879p');
insert into scpp.jan_sales_sheets values('steve','26881A');
insert into scpp.jan_sales_sheets values('BRYN','26888A');
insert into scpp.jan_sales_sheets values('BRYN','26893');
insert into scpp.jan_sales_sheets values('brett','26894');
insert into scpp.jan_sales_sheets values('nate','26896b');
insert into scpp.jan_sales_sheets values('aj','26896c');
insert into scpp.jan_sales_sheets values('jim','26899a');
insert into scpp.jan_sales_sheets values('jim','26902b');
insert into scpp.jan_sales_sheets values('jared','26903a');
insert into scpp.jan_sales_sheets values('chris','26903AA');
insert into scpp.jan_sales_sheets values('tanner','26906a');
insert into scpp.jan_sales_sheets values('brad','26918a');
insert into scpp.jan_sales_sheets values('chris','26920B');
insert into scpp.jan_sales_sheets values('dale','26926A');
insert into scpp.jan_sales_sheets values('nate','26928xxa');
insert into scpp.jan_sales_sheets values('tanner','26932');
insert into scpp.jan_sales_sheets values('scott','26934');
insert into scpp.jan_sales_sheets values('logain','26935A');
insert into scpp.jan_sales_sheets values('aj','26938a');
insert into scpp.jan_sales_sheets values('aj','26942xx');
insert into scpp.jan_sales_sheets values('dale','26955xxa');
insert into scpp.jan_sales_sheets values('logain','26956xxa');
insert into scpp.jan_sales_sheets values('tanner','26956xxb');
insert into scpp.jan_sales_sheets values('craig','26959xx');
insert into scpp.jan_sales_sheets values('jim','26960xx');
insert into scpp.jan_sales_sheets values('jesse','26964xxb');
insert into scpp.jan_sales_sheets values('BRYN','26966A');
insert into scpp.jan_sales_sheets values('BRYN','26971A');
insert into scpp.jan_sales_sheets values('jared','26975p');
insert into scpp.jan_sales_sheets values('aj','26980a');
insert into scpp.jan_sales_sheets values('jared','26982');
insert into scpp.jan_sales_sheets values('sam','26985B');
insert into scpp.jan_sales_sheets values('jared','26993a');
insert into scpp.jan_sales_sheets values('chris','26997');
insert into scpp.jan_sales_sheets values('jesse','27001');
insert into scpp.jan_sales_sheets values('jared','27026xxa');
insert into scpp.jan_sales_sheets values('steve','27035');
insert into scpp.jan_sales_sheets values('tanner','27040a');
insert into scpp.jan_sales_sheets values('brad','27041a');
insert into scpp.jan_sales_sheets values('nate','27046xx');
insert into scpp.jan_sales_sheets values('craig','27049xx');
insert into scpp.jan_sales_sheets values('jesse','27064xx');
insert into scpp.jan_sales_sheets values('craig','27067xx');
insert into scpp.jan_sales_sheets values('BRYN','27067XXA');
insert into scpp.jan_sales_sheets values('sam','27069XX');
insert into scpp.jan_sales_sheets values('nate','27074p');
insert into scpp.jan_sales_sheets values('chris','27082');
insert into scpp.jan_sales_sheets values('BRYN','27085');
insert into scpp.jan_sales_sheets values('larry','27101');
insert into scpp.jan_sales_sheets values('craig','27105a');
insert into scpp.jan_sales_sheets values('brad','27110a');
insert into scpp.jan_sales_sheets values('craig','27116xxa');
insert into scpp.jan_sales_sheets values('scott','27119');
insert into scpp.jan_sales_sheets values('jared','27125');
insert into scpp.jan_sales_sheets values('arden','27130a');
insert into scpp.jan_sales_sheets values('jared','27137a');
insert into scpp.jan_sales_sheets values('brett','27155');
insert into scpp.jan_sales_sheets values('steve','27157B');
insert into scpp.jan_sales_sheets values('arden','27180b');
insert into scpp.jan_sales_sheets values('joe','27188');
insert into scpp.jan_sales_sheets values('eric','27188x');
insert into scpp.jan_sales_sheets values('aj','27188xa');
insert into scpp.jan_sales_sheets values('nate','27198xx');
insert into scpp.jan_sales_sheets values('tanner','27207p');
insert into scpp.jan_sales_sheets values('larry','27216xa');
insert into scpp.jan_sales_sheets values('nate','27224');
insert into scpp.jan_sales_sheets values('eric','27232');
insert into scpp.jan_sales_sheets values('dale','27236');
insert into scpp.jan_sales_sheets values('jeff p','27243');
insert into scpp.jan_sales_sheets values('brett','27244');
insert into scpp.jan_sales_sheets values('BRYN','27246A');
insert into scpp.jan_sales_sheets values('craig','27248a');
insert into scpp.jan_sales_sheets values('larry','27255a');
insert into scpp.jan_sales_sheets values('sam','27260A');
insert into scpp.jan_sales_sheets values('tanner','27260b');
insert into scpp.jan_sales_sheets values('sam','27261');
insert into scpp.jan_sales_sheets values('sam','27281');
insert into scpp.jan_sales_sheets values('jared','27291pa');
insert into scpp.jan_sales_sheets values('logain','27304');
insert into scpp.jan_sales_sheets values('jeff p','27304a');
insert into scpp.jan_sales_sheets values('aj','27308xx');
insert into scpp.jan_sales_sheets values('sam','27321XX');
insert into scpp.jan_sales_sheets values('craig','27326xx');
insert into scpp.jan_sales_sheets values('chris','27336P');
insert into scpp.jan_sales_sheets values('nate','27340a');
insert into scpp.jan_sales_sheets values('joe','27341');
insert into scpp.jan_sales_sheets values('jeff p','27342');
insert into scpp.jan_sales_sheets values('jim','27342xx');
insert into scpp.jan_sales_sheets values('jim','27347');
insert into scpp.jan_sales_sheets values('joe','27350');
insert into scpp.jan_sales_sheets values('jared','27357');
insert into scpp.jan_sales_sheets values('scott','27361');
insert into scpp.jan_sales_sheets values('larry','27362');
insert into scpp.jan_sales_sheets values('craig','27367');
insert into scpp.jan_sales_sheets values('jeff p','27373');
insert into scpp.jan_sales_sheets values('tanner','27378');
insert into scpp.jan_sales_sheets values('nate','27385p');
insert into scpp.jan_sales_sheets values('craig','27387p');
insert into scpp.jan_sales_sheets values('eric','27389');
insert into scpp.jan_sales_sheets values('eric','27391a');
insert into scpp.jan_sales_sheets values('craig','27409');
insert into scpp.jan_sales_sheets values('jesse','27420');
insert into scpp.jan_sales_sheets values('tanner','27425x');
insert into scpp.jan_sales_sheets values('craig','27428p');
insert into scpp.jan_sales_sheets values('jesse','27429X');
insert into scpp.jan_sales_sheets values('arden','27430x');
insert into scpp.jan_sales_sheets values('aj','27450x');
insert into scpp.jan_sales_sheets values('BRYN','27453P');
insert into scpp.jan_sales_sheets values('scott','27455');
insert into scpp.jan_sales_sheets values('craig','27459x');
insert into scpp.jan_sales_sheets values('larry','27460');
insert into scpp.jan_sales_sheets values('dale','27460A');
insert into scpp.jan_sales_sheets values('jim','27461xx');
insert into scpp.jan_sales_sheets values('jeff p','27465xx');
insert into scpp.jan_sales_sheets values('craig','27465xxa');
insert into scpp.jan_sales_sheets values('craig','27502x');
insert into scpp.jan_sales_sheets values('scott','27504');
insert into scpp.jan_sales_sheets values('brett','27525');
insert into scpp.jan_sales_sheets values('chris','27560X');
insert into scpp.jan_sales_sheets values('sam','27562A');
insert into scpp.jan_sales_sheets values('brad','27596x');
insert into scpp.jan_sales_sheets values('larry','27597');
insert into scpp.jan_sales_sheets values('joe','27599');
insert into scpp.jan_sales_sheets values('craig','27616');
insert into scpp.jan_sales_sheets values('brett','27617');
insert into scpp.jan_sales_sheets values('larry','h8448');
insert into scpp.jan_sales_sheets values('larry','h8490');
insert into scpp.jan_sales_sheets values('larry','h8540');
insert into scpp.jan_sales_sheets values('larry','h8711');


select sales_consultant, count(*)
select * 
from scpp.jan_sales_sheets
group by sales_consultant
order by sales_consultant

select *
from (
  select bopmast_stock_number, primary_salespers, secondary_slspers2
  -- select count(*)
  from foreign_table a
  where date_capped between '2016-01-01' and '2016-01-31'
    and sale_type <> 'w'
    and bopmast_company_number = 'RY1') r
full outer join scpp.jan_sales_sheets s on r.bopmast_stock_number = s.stock_number
where r.bopmast_stock_number is null or s.stock_number is null
  


select bopmast_company_number, primary_salespers, count(*)
-- select count(*)
from foreign_table a
where date_capped between '2016-01-01' and '2016-01-31'
  and sale_type <> 'w'
group by bopmast_company_number, primary_salespers
order by bopmast_company_number, primary_salespers


select x.*, sum(unit_count) over(partition by consultant order by consultant) as total
from (
  select bopmast_company_number, bopmast_stock_number, primary_salespers as consultant,
    date_capped, 
  CASE 
    when secondary_slspers2 is null then 1.0 
    else 0.5
  end as unit_count
  from foreign_table a
  where date_capped between '2016-01-01' and '2016-01-31'
    and sale_type <> 'w'
  union
  select bopmast_company_number, bopmast_stock_number, secondary_slspers2 as consultant,
    date_capped, 0.5 as unit_count
  from foreign_table a
  where date_capped between '2016-01-01' and '2016-01-31'
    and sale_type <> 'w'
    and secondary_slspers2 is not null) x  
order by bopmast_company_number, consultant, date_capped    



select *
from (
  select x.*, sum(unit_count) over(partition by consultant order by consultant) as total
  from (
    select bopmast_company_number, bopmast_stock_number, primary_salespers as consultant,
      date_capped, date_approved,
    CASE 
      when secondary_slspers2 is null then 1.0 
      else 0.5
    end as unit_count
    from foreign_table a
    where date_capped between '2016-01-01' and '2016-01-31'
      and sale_type <> 'w'
      and bopmast_company_number = 'RY1'
    union
    select bopmast_company_number, bopmast_stock_number, secondary_slspers2 as consultant,
      date_capped, date_approved, 0.5 as unit_count
    from foreign_table a
    where date_capped between '2016-01-01' and '2016-01-31'
      and sale_type <> 'w'
      and secondary_slspers2 is not null
      and bopmast_company_number = 'RY1') x) r
full outer join scpp.jan_sales_sheets s on r.bopmast_stock_number = s.stock_number
where (r.bopmast_stock_number is null or s.stock_number is null)
  and (sales_consultant = 'brad' or consultant = 'asc')


select *
from scpp.jan_Sales_sheets





select * from foreign_table where bopmast_stock_number = '27073p'