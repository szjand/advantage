﻿-- from ben's request to genereate front/back gross per consultant
-- this is an attempt to match fs vehicle counts
-- this is the whole deal from scpp\ext_deals.py
                select 'New',a.gtctl_, a.gtdate,
                  sum(case when a.gttamt < 0 then 1 else -1 end) as unit_count,
                  '%s'
                from scpp.ext_glptrns_for_deals a
                left join (
                -- ok, these are the vehicles with multiple entries on a day that cancel each other out (by polarity)
                  select gtctl_, gtdate
                  from (
                    select a.gtctl_, a.gtdate,
                      case when a.gttamt < 0 then 1 ELSE - 1 end as the_count
                    from scpp.ext_glptrns_for_deals a
                    inner join (
                      select gtctl_, gtdate
                      from scpp.ext_glptrns_for_deals
                      where gtpost = 'Y'
                      group by gtctl_, gtdate
                      having count(*) > 1) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
                    where a.gtpost = 'Y') x
                  group by gtctl_, gtdate
                  having sum(the_count) = 0) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
                where gtpost = 'Y'
                  AND not exists (
                    select 1
                    from scpp.xfm_glptrns
                    where stock_number = a.gtctl_
                      and gl_date = a.gtdate)
                  and b.gtctl_ is null
                group by a.gtctl_, a.gtdate

-- based on \scpp\ext_deals.py
select account, description, count(*)
from (
  select a.*, b.account, b.description
  from fin.fact_gl a 
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join dds.dim_date c on a.date_key = c.date_key
    and c.year_month = 201701
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('VSN','VSU')
  where b.account_type_code = '4'
    and a.post_status = 'Y'
    and b.department_code in ('NC','UC')
    and b.description like '%WH%') a
group by account, description
order by account
Page 6    fs    fact
line 1   17     17
line 2   26     26
line 4   64     64
line 5   86     90

line 5 account 145001
compare to executrack sales analysis
diff: 
   fact includes 29104A twice, 30040XXB, 30075X
  select a.control, a.amount
  from fin.fact_gl a 
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join dds.dim_date c on a.date_key = c.date_key
    and c.year_month = 201701
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('VSN','VSU')
  where b.account = '145001'
    and a.post_status = 'Y'
    and b.department_code in ('NC','UC')
    and b.description not like '%%WH%%'
order by control    

-- Used Sales 
-- P6L1-14 here's the dough and it is spot on
select c.store, b.year_month, b.line,
  sum(case when col = 1 then -amount end) as sales,
  sum(case when col = 3 then -amount end) as cogs,
  sum(-amount) as gross
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201701
  and b.page = 16
  and b.line between 1 and 14
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  and c.store = 'ry1'
group by c.store, b.year_month, b.line
having sum(amount) <> 0
order by b.line;

-- so, let's see the dough directly from fact_gl
-- yep 90 (instead of 86) and the dough is right on
-- select count(*), sum(amount) -- yep 90 (instead of 86) and the dough is right on
-- this query gives the correct count
select sum(unit_count) from (
select sum(case when a.amount < 0 then 1 else -1 end) as unit_count
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201701
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '145001'
where a.post_status = 'Y'  ) x


select a.control, sum(a.amount), 
  sum(case when a.amount < 0 then 1 else -1 end) as unit_count
-- select sum(amount)  
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201701
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '145001'
where a.post_status = 'Y'
group by a.control
order by a.control
order by sum(a.amount) desc 

-- ok, line, account, acct desc, count for 5 months, count is correct
-- which is great, have to figure out which fucking ones are being counted
-- i think i do it in scpp with the notion of
create temp table uc_counts as
select d.line, c.account, c.description, 
  sum(
    case 
      when b.year_month = 201601 then
        case 
          when a.amount < 0 then 1 else -1 
        end
    end) as "201601",
  sum(
    case 
      when b.year_month = 201606 then
        case 
          when a.amount < 0 then 1 else -1 
        end
    end) as "201606",
  sum(
    case 
      when b.year_month = 201611 then
        case 
          when a.amount < 0 then 1 else -1 
        end
    end) as "201611",    
  sum(
    case 
      when b.year_month = 201612 then
        case 
          when a.amount < 0 then 1 else -1 
        end
    end) as "201612",
  sum(
    case 
      when b.year_month = 201701 then
        case 
          when a.amount < 0 then 1 else -1 
        end
    end) as "201701"
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month in (201601, 201606, 201611, 201612, 201701)
inner join fin.dim_account c on a.account_key = c.account_key
inner join (
  select d.gl_account, b.line, e.description
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201701
    and b.page = 16
    and b.line between 1 and 14
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code = '4'
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
    and f.store = 'ry1' ) d on c.account = d.gl_account
where a.post_status = 'Y'
group by d.line, c.account, c.description
order by d.line 

select * from uc_counts


i am majorly mind fucking on getting the correct list of vehicles
145001 my shit includes 30045XXB and it appears that arkona does not
but once afuckinggain, how can my dough be correct and the vehicles not
hmmmmmm
my dough numbers match the statement
the doc count matches but the dough does not

select *
from (
select a.control, c.account, c.description, 
  sum(case when a.amount < 0 then 1 else -1 end) as unit_count
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201701
inner join fin.dim_account c on a.account_key = c.account_key
  and c.account = '145001'
where a.post_status = 'Y'
group by a.control, c.account, c.description
) x
where unit_count = 1
order by control
 


  

--29104A in twice
-- 6 rows where amount > 0

-- \scpp\ext_deals.py 
select *
from (
  select a.*, b.account, b.description
  from fin.fact_gl a 
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join dds.dim_date c on a.date_key = c.date_key
    and c.year_month = 201701
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('VSN','VSU')
  where b.account_type_code = '4'
    and a.post_status = 'Y'
    and b.department_code in ('NC','UC')
    and b.description not like '%%WH%%') a    
where account = '145001'
order by control


select *
from (
  select a.*, b.account, b.description
  from fin.fact_gl a 
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join dds.dim_date c on a.date_key = c.date_key
    and c.year_month = 201701
  inner join fin.dim_journal d on a.journal_key = d.journal_key
    and d.journal_code in ('VSN','VSU')
  where b.account_type_code = '4'
    and a.post_status = 'Y'
    and b.department_code in ('NC','UC')
    and b.description not like '%%WH%%') a    
where amount > 0   



select 'New',a.gtctl_, a.gtdate,
  sum(case when a.gttamt < 0 then 1 else -1 end) as unit_count,
  '%s'
from scpp.ext_glptrns_for_deals a
left join (
-- ok, these are the vehicles with multiple entries on a day that cancel each other out (by polarity)
  select gtctl_, gtdate
  from (
    select a.gtctl_, a.gtdate,
      case when a.gttamt < 0 then 1 ELSE - 1 end as the_count
    from scpp.ext_glptrns_for_deals a
    inner join (
      select gtctl_, gtdate
      from scpp.ext_glptrns_for_deals
      where gtpost = 'Y'
      group by gtctl_, gtdate
      having count(*) > 1) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
    where a.gtpost = 'Y') x
  group by gtctl_, gtdate
  having sum(the_count) = 0) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
where gtpost = 'Y'
--   AND not exists (
--     select 1
--     from scpp.xfm_glptrns
--     where stock_number = a.gtctl_
--       and gl_date = a.gtdate)
--   and b.gtctl_ is null
group by a.gtctl_, a.gtdate     

how can my count be off but my $$ be correct

---- 2/19/17 ------------------------------------------------------------------------------------------------------
-- thought the doc disagreed with the statement
-- but as i look at it today
-- used cars doc aggrees with statement both count and dough
-- printed list of 145001
-- 90 rows, 2 of which are positive amounts
-- so 90 - the 2 which don't count - 2 for the -1 unit count for those 2 and bingo 86


-- this is the whole deal from scpp\ext_deals.py
-- need to translate this into fin.fact_gl, which i have done, but have not yet implemented the
-- conversion of the multiple entries part into fact_gl
                select 'New',a.gtctl_, a.gtdate,
                  sum(case when a.gttamt < 0 then 1 else -1 end) as unit_count,
                  '%s'
                from scpp.ext_glptrns_for_deals a
                left join (
                -- ok, these are the vehicles with multiple entries on a day that cancel each other out (by polarity)
                  select gtctl_, gtdate
                  from (
                    select a.gtctl_, a.gtdate,
                      case when a.gttamt < 0 then 1 ELSE - 1 end as the_count
                    from scpp.ext_glptrns_for_deals a
                    inner join (
                      select gtctl_, gtdate
                      from scpp.ext_glptrns_for_deals
                      where gtpost = 'Y'
                      group by gtctl_, gtdate
                      having count(*) > 1) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
                    where a.gtpost = 'Y') x
                  group by gtctl_, gtdate
                  having sum(the_count) = 0) b on a.gtctl_ = b.gtctl_ and a.gtdate = b.gtdate
                where gtpost = 'Y'
                  AND not exists (
                    select 1
                    from scpp.xfm_glptrns
                    where stock_number = a.gtctl_
                      and gl_date = a.gtdate)
                  and b.gtctl_ is null
                group by a.gtctl_, a.gtdate
                
-- so, line 5 (145001) -- the dough is right on, returning 90 rows
-- 29104a twice (one + and one -), and 30075X w/ + amount, should be -
-- voila 86
-- somehow i still haven't quite cleanly separated count vs amount
-- i need to extrapolate the count to actual vehicles, what i am trying to accomplish is
-- in a given month, a consultant delivered specific vehicles, ben wants to know the front/back/fi gross
-- for those vehicles by sales consultant
-- i think where i am mind fucking is the unwinds, eg, in 201701 29104A, 30075X
select * 
-- select sum(a.amount)
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join (
  select d.gl_account, b.line, e.description
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201701
    and b.page = 16
    and b.line = 5
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code = '4'
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
    and f.store = 'ry1' ) d on c.account = d.gl_account
where a.post_status = 'Y'
order by a.control

-- 2/20/17 
so the mind fuck with the dough is correct but the count is not is due to a faulty count algorithm
allright, lets get on with used car count, line 5

-- this gives the correct count
select sum(unit_count) 
from (
  select a.control, 
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201701
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join (
    select d.gl_account, b.line, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701
      and b.page = 16
      and b.line = 5
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'ry1' ) d on c.account = d.gl_account
  where a.post_status = 'Y') x

-- this returns 87 rows
-- this is the mind fuck
-- the dough is correct, the count s/b 86
-- ?? so i can get the right dough, get the right count, but can't list the right vehciles???
select control from (
select a.control, 
  case when a.amount < 0 then 1 else -1 end as unit_count
-- select sum(a.amount)  
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join (
  select d.gl_account, b.line, e.description
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201701
    and b.page = 16
    and b.line = 5
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code = '4'
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
    and f.store = 'ry1' ) d on c.account = d.gl_account
where a.post_status = 'Y'
) x
group by control
having sum(unit_count) > 0
order by length(control), control -- to match excel sorting


-- ok, so this dawned on me in the middle of schubert
-- generate the count with the count query, oh shit, need count attributed to consultant
-- use the dough query linked to something to give me sales consultant for the total front/back/fi/gross


-- the count
select sum(unit_count) 
from (
  select a.control, 
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201701
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join (
    select d.gl_account, b.line, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701
      and b.page = 16
      and b.line = 5
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'ry1' ) d on c.account = d.gl_account
  where a.post_status = 'Y') x


select sum(unit_count) 
from (
  select a.control, 
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201701
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select d.gl_account, b.page, b.line, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701
      and (
        (b.page between 5 and 15) -- new cars
        or
        (b.page = 16 and b.line between 1 and 14) -- used cars
        or
        (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store = 'ry1' ) d on c.account = d.gl_account
  where a.post_status = 'Y') x  


-- do finance separately and join, finance is irrelevant to count
-- this is the count be page/line
-- count is good at every level
-- total: 349, new: 114, used: 235, all the way down to line
-- includes both stores
create temp table unit_counts as
select store, page, line, line_label, sum(unit_count) as unit_count
-- select sum(unit_count) -- total count
-- select  -- new/used
--   sum(case when page = 16 then unit_count else 0 end) as used,
--   sum(case when page <> 16 then unit_count else 0 end) as new
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, a.control, a.amount,
    case when a.amount < 0 then 1 else -1 end as unit_count
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201701
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701
      and (
        (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)) -- used cars
--         or
--         (b.page = 17 and b.line between 1 and 20))-- f/i
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code = '4'
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
  where a.post_status = 'Y'  ) h
group by store, page, line, line_label
order by store, page, line


-- now, with the same base query, the dough
-- this works for sales amounts, need to add cogs, gross, & f/i
-- ben says include accessories, but like f/i, this will be a separate data set
-- looks good
-- shit no need for separate queries
-- everything good for dough
create temp table dough as
select store, page, line, line_label,
  sum(case when account_type_code in ('4','7') then amount else 0 end) as sales,
  sum(case when account_type_code in ('5','9') then amount else 0 end) as cogs,
  sum(amount) as gross
-- select  -- new/used
--   sum(case when page = 16 then amount else 0 end) as used,
--   sum(case when page <> 16 then amount else 0 end) as new
from (
  select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, 
    a.control, a.amount, d.account_type_code
  from fin.fact_gl a
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = 201701
  inner join fin.dim_account c on a.account_key = c.account_key
  inner join ( -- d: fs gm_account page/line/acct description
    select f.store, d.gl_account, b.page, b.line, b.line_label, e.description, e.account_type_code
    from fin.fact_fs a
    inner join fin.dim_fs b on a.fs_key = b.fs_key
      and b.year_month = 201701
      and (
        (b.page between 5 and 13) -- new cars -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
        or
        (b.page = 16 and b.line between 1 and 14)
        or
        (b.page = 17 and b.line between 1 and 20))-- f/i       
    inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
    inner join fin.dim_account e on d.gl_account = e.account
      and e.account_type_code in('4','7','5', '9') -- include Other Income type accounts to include accessories
    inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
      and f.store in ('ry1', 'ry2' )) d on c.account = d.gl_account
  where a.post_status = 'Y'  ) h  
group by store, page, line, line_label
order by store, page, line

got the count, got the dough, need consultants for both

create temp table vehicles as
select a.control
from fin.fact_gl a
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = 201701
inner join fin.dim_account c on a.account_key = c.account_key
inner join ( -- d: fs gm_account page/line/acct description
  select f.store, d.gl_account, b.page, b.line, b.line_label, e.description, e.account_type_code
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201701
    and (
      (b.page between 5 and 13) -- new cars -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
      or
      (b.page = 16 and b.line between 1 and 14)
      or
      (b.page = 17 and b.line between 1 and 20))-- f/i       
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
    and e.account_type_code in('4','7','5', '9') -- include Other Income type accounts to include accessories
  inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key
    and f.store in ('ry1', 'ry2' )) d on c.account = d.gl_account
where a.post_status = 'Y' 
group by a.control 

-- there will be some wierdness
-- eg 24068xx 884 ins chargeback (185101) deal from 201503
select a.*, b.*
from vehicles a
left join scpp.xfm_deals b on a.control = b.stock_number
inner join (
  select stock_number, max(seq) as seq
  from scpp.xfm_deals
  group by stock_number) c on b.stock_number = c.stock_number
    and b.seq = c.seq
order by a.control


-- how can i get a consultant count
-- this gives the count, but not a list of units that comprise the count
select u.*, v.*
from (
  select control, sum(unit_count) as unit_count
  from (
    select d.store, d.page, d.line, d.line_label, d.description, d.gl_account, a.control, a.amount,
      case when a.amount < 0 then 1 else -1 end as unit_count
    from fin.fact_gl a
    inner join dds.dim_date b on a.date_key = b.date_key
      and b.year_month = 201701
    inner join fin.dim_account c on a.account_key = c.account_key
    inner join ( -- d: fs gm_account page/line/acct description
      select f.store, d.gl_account, b.page, b.line, b.line_label, e.description
      from fin.fact_fs a
      inner join fin.dim_fs b on a.fs_key = b.fs_key
        and b.year_month = 201701
        and (
          (b.page between 5 and 15 and b.line between 1 and 45) -- ?? page 14 should be other autos but returns lots of SLS AFTERMARKET NEW acct 144500 but no amount> 
          or
          (b.page = 16 and b.line between 1 and 14)) -- used cars
  --         or
  --         (b.page = 17 and b.line between 1 and 20))-- f/i
      inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
      inner join fin.dim_account e on d.gl_account = e.account
        and e.account_type_code = '4'
      inner join fin.dim_fs_org f on a.fs_org_key = f.fs_org_key) d on c.account = d.gl_account
    where a.post_status = 'Y' ) h 
  group by control) u
left join scpp.xfm_deals v on u.control = v.stock_number
inner join (
  select stock_number, max(seq) as seq
  from scpp.xfm_deals
  group by stock_number) w on v.stock_number = w.stock_number and v.seq = w.seq


  
     
order by u.control
