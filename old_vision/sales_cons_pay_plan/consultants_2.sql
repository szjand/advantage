SELECT a.*, b.termdate
FROM dimsalesperson a
LEFT JOIN edwEmployeeDim b on a.storecode = b.storecode
  AND a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.employeenumber not in ('N/A', 'XXX')
ORDER BY a.lastname, a.firstname

SELECT a.*, b.termdate
FROM dimsalesperson a
LEFT JOIN edwEmployeeDim b on a.storecode = b.storecode
  AND a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.employeenumber not in ('N/A', 'XXX')
  AND EXISTS (
    SELECT 1
	FROM dimSalesPerson
	WHERE salespersonid = a.salespersonid
	  AND termdate IS NULL)
ORDER BY a.lastname, a.firstname


SELECT salespersonid, lastname, firstname
FROM dimsalesperson
WHERE employeenumber not in ('N/A', 'XXX')
GROUP BY salespersonid, lastname, firstname
HAVING COUNT(*) > 1

/*
-- this appears to WORK AS desired, eg this picks up ALL the oct terms
SELECT a.storecode, a.employeenumber, a.name, a.lastname, a.firstname, a.hiredate, termdate 
FROM edwEmployeeDim a
WHERE distcode = 'sale'
  AND currentrow = true
  AND termdate > ( -- last day of current  month
    SELECT thedate
	FROM day
	WHERE yearmonth = (SELECT yearmonth FROM day WHERE thedate = '10/15/2015')
	  AND lastdayofmonth = true)
ORDER BY lastname	  
*/	  
	  
SELECT a.storecode, a.employeenumber, a.name, a.lastname, a.firstname, a.hiredate, termdate 
FROM edwEmployeeDim a
WHERE distcode = 'sale'
  AND currentrow = true
  AND termdate > ( -- last day of current  month
    SELECT thedate
	FROM day
	WHERE yearmonth = (SELECT yearmonth FROM day WHERE thedate = curdate())
	  AND lastdayofmonth = true);


/*	  
for now, an ry2 vehicle sold be an ry1 consultant, the ry2 deal
shows the consultant AS house
so, no way to extract those sales.

SELECT a.storecode, a.employeenumber, a.name, a.lastname, a.firstname, 
  a.hiredate, a.termdate,
  b.*, c.* 
FROM edwEmployeeDim a
LEFT JOIN dimSalesPerson b on a.storecode = b.storecode
  AND a.employeenumber = b.employeenumber
LEFT JOIN dimSalesPerson c on a.storecode <> c.storecode
  AND a.employeenumber = c.employeenumber  
WHERE a.distcode = 'sale'
  AND a.currentrow = true
  AND a.termdate > ( -- last day of current  month
    SELECT thedate
	FROM day
	WHERE yearmonth = (SELECT yearmonth FROM day WHERE thedate = curdate())
	  AND lastdayofmonth = true)  
ORDER BY a.lastname, a.firstname	  
*/


SELECT a.storecode, a.employeenumber, a.name, a.lastname, a.firstname, 
  a.hiredate, a.termdate,
  b.salespersonid
FROM edwEmployeeDim a
LEFT JOIN dimSalesPerson b on a.storecode = b.storecode
  AND a.employeenumber = b.employeenumber
  AND b.active = true
  AND b.salesPersonType = 'Consultant'
WHERE a.distcode = 'sale'
  AND a.currentrow = true
  AND a.termdate > ( -- last day of current  month
    SELECT thedate
	FROM day
	WHERE yearmonth = (SELECT yearmonth FROM day WHERE thedate = curdate())
	  AND lastdayofmonth = true)
ORDER BY a.lastname	  

-- shit looks LIKE i need orig hiredate, eg chris garceau
-- margo sorenson has no salespersonid but still needs to be paid
-- does NOT get added to dimSalesPerson until there IS a deal
SELECT a.storecode, a.employeenumber, a.lastname, a.firstname, 
  TRIM(a.firstname) + ' ' + a.lastname AS fullname, 
  CASE -- default to orig hired date
    WHEN c.ymhdto < a.hiredate THEN c.ymhdto
	ELSE a.hiredate
  END AS hiredate,
  b.salespersonid
FROM edwEmployeeDim a
LEFT JOIN dimSalesPerson b on a.storecode = b.storecode
  AND a.employeenumber = b.employeenumber
  AND b.active = true
  AND b.salesPersonType = 'Consultant'
LEFT JOIN stgArkonaPYMAST c on a.storecode = c.ymco#
  AND a.employeenumber = c.ymempn
WHERE a.distcode = 'sale'
  AND a.currentrow = true
  AND a.termdate > ( -- last day of current  month
    SELECT thedate
	FROM day
	WHERE yearmonth = (SELECT yearmonth FROM day WHERE thedate = curdate())
	  AND lastdayofmonth = true) 
ORDER BY a.lastname	  
