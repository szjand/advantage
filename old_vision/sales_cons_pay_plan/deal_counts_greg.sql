select sum(unitcount) 
from (
  select page, line, description, sum(unitcount) as unitcount 
  from (
    select page, line, description, gtctl#, gtacct, gttrn#, gtseq#, case when sum(gttamt) < 0 then 1 else -1 end as unitcount 
    from stgArkonaGLPTRNS a
    inner join gmfs_accounts b on a.gtacct = b.gl_account 
    where gtco# = 'RY1'
      and gtdate between '11/1/2015' and '11/30/2015'
      and gtjrnl = 'VSN'
  	  and gtpost <> 'V'
  	  and gtacct in (
        select gl_account 
        from gmfs_accounts 
        where storecode = 'RY1' 
          and account_type = '4' 
          and department = 'NC')
    group by page, line, description, gtctl#, gtacct, gttrn#, gtseq#
    ) x
  group by page, line, description
) x


select page, line, description, sum(unitcount) from (
select page, line, description, gtctl#, gtacct, gttrn#, gtseq#, case when sum(gttamt) < 0 then 1 else -1 end as unitcount 
from stgArkonaGLPTRNS a
inner join gmfs_accounts b on a.gtacct = b.gl_account 
where
  gtco# = 'RY1'
    and gtdate between '12/01/2015' and '12/31/2015'
    and gtjrnl = 'VSN'
	and gtpost <> 'V'
	and gtacct in (select gl_account from gmfs_accounts where storecode = 'RY1' and account_type = '4' and department = 'NC')
group by page, line, description, gtctl#, gtacct, gttrn#, gtseq#
) x
group by page, line, description


select *
from stgArkonaGLPTRNS
where
  gtco# = 'RY1'
    and gtdate between '11/1/2015' and '11/30/2015'
    and gtjrnl = 'VSN'
	and gtpost <> 'V'
	and gtacct in ('1431001')

	
select sum(unitcount) from (
select page, line, description, sum(unitcount) as unitcount from (
select page, line, description, gtctl#, gtacct, gttrn#, gtseq#, case when sum(gttamt) < 0 then 1 else -1 end as unitcount 
from stgArkonaGLPTRNS a
inner join gmfs_accounts b on a.gtacct = b.gl_account 
where
  gtco# = 'RY1'
    and gtdate between '11/1/2015' and '11/30/2015'
    and gtjrnl = 'VSU'
	and gtpost <> 'V'
	and gtacct in (select gl_account from gmfs_accounts where storecode = 'RY1' and account_type = '4' and department = 'UC')
group by page, line, description, gtctl#, gtacct, gttrn#, gtseq#
) x
group by page, line, description
) x

-- good for dec 15
select page, line, description, sum(unitcount) from (
select page, line, description, gtctl#, gtacct, gttrn#, gtseq#, case when sum(gttamt) < 0 then 1 else -1 end as unitcount 
from stgArkonaGLPTRNS a
inner join gmfs_accounts b on a.gtacct = b.gl_account 
where
  gtco# = 'RY1'
    and gtdate between '12/01/2015' and '12/31/2015'
    and gtjrnl = 'VSU'
	and gtpost <> 'V'
	and gtacct in (select gl_account from gmfs_accounts where storecode = 'RY1' and account_type = '4' and department = 'UC')
group by page, line, description, gtctl#, gtacct, gttrn#, gtseq#
) x
group by page, line, description


-- 12/8/15
 � in bopmast (bnkey = 32952)

SELECT *
FROM (
  SELECT gtctl#
  FROM (
    select page, line, description, gtctl#, gtacct, gttrn#, gtseq#, 
      case when sum(gttamt) < 0 then 1 else -1 end as unitcount 
    from stgArkonaGLPTRNS a
    inner join gmfs_accounts b on a.gtacct = b.gl_account 
    where gtco# = 'RY1'
      and gtdate between '11/1/2015' and '11/30/2015'
      and gtjrnl = 'VSN'
     and gtpost <> 'V'
     and gtacct in (
        select gl_account 
        from gmfs_accounts 
        where storecode = 'RY1' 
          and account_type = '4' 
          and department = 'NC')
    group by page, line, description, gtctl#, gtacct, gttrn#, gtseq#) w 
  GROUP BY gtctl# 
  HAVING SUM(unitcount) > 0) r  
LEFT JOIN factVehicleSale b on r.gtctl# = b.stocknumber  
WHERE b.stocknumber IS NULL 


SELECT * 
FROM stgArkonaGLPTRNS
WHERE gtctl# = '26566a'


SELECT a.gttrn#, gtjrnl, gtdate, gtacct, gtctl#, gtdoc#
FROM stgArkonaGLPTRNS a
INNER JOIN (
  select gl_account
  FROM gmfs_accounts
  WHERE department = 'nc' 
    AND storecode = 'RY1'
    AND account_type = '4') b on a.gtacct = b.gl_account
LEFT JOIN stgArkonaGLPDTIM c on a.gttrn# = c.gqtrn#    
WHERE a.gtdate BETWEEN '09/01/2015' AND '11/30/2015'
  AND a.gtjrnl = 'VSN'
  AND a.gtctl# <> a.gtdoc#    
  
  
