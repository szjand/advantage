-- remove ry2 service calls

sp.swMetricDataYesterday: removed UPDATE to RY2ROsForCalls;
remove sp:
  GetRY2ROCalls
  GetRY2ROsForCalls 
  GetRY2ROToUpdate
  InsertRY2ServiceCall
EXECUTE PROCEDURE sp_RenameDDObject('GetRY2ROCalls','zUnused_GetRY2ROCalls', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject('GetRY2ROsForCalls','zUnused_GetRY2ROsForCalls', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject('GetRY2ROToUpdate','zUnused_GetRY2ROToUpdate', 10, 0);
EXECUTE PROCEDURE sp_RenameDDObject('InsertRY2ServiceCall','zUnused_InsertRY2ServiceCall', 10, 0);  
remove tables: 
  RY2ROCals
  RY2ROsForCalls
EXECUTE PROCEDURE sp_RenameDDObject('RY2ROCalls','zUnused_RY2ROCals', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject('RY2ROsForCalls','zUnused_RY2ROsForCalls', 1, 0);  

-- App Authentication

SELECT a.appcode, b.role, c.functionality
FROM applications a
LEFT JOIN applicationRoles b on a.appcode = b.appcode
LEFT JOIN applicationMetaData c on b.appcode = c.appcode AND b.role = c.approle
WHERE a.appcode = 'sco'


SELECT * 
-- DELETE 
FROM employeeAppAuthorization
WHERE appcode = 'sco'
AND approle = 'servicemisc';

SELECT *
-- delete
FROM applicationMetaData
WHERE appcode = 'sco'
  AND approle = 'servicemisc'; 
  
SELECT * 
-- DELETE 
FROM applicationRoles
WHERE appcode = 'sco'
  AND role = 'servicemisc';
  
-- Misc
TABLE employeeAppRoles no longer used  
EXECUTE PROCEDURE sp_RenameDDObject('employeeAppRoles','zUnused_employeeAppRoles', 1, 0);

EXECUTE PROCEDURE sp_RenameDDObject('snippets','zUnused_snippets', 1, 0);

EXECUTE PROCEDURE sp_RenameDDObject('deptNS','zUnused_deptNS', 1, 0);

EXECUTE PROCEDURE sp_RenameDDObject('log','zUnused_log', 1, 0);

EXECUTE PROCEDURE sp_RenameDDObject('getRoDetail','zUnused_getRoDetail', 10, 0);




  


