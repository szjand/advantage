﻿drop function rv.dip_get_stores()
/*
create or replace function rv.dip_get_stores()
  returns table (store_id integer, store_number text, store_name text) as
$BODY$
  select store_id, store_number, store_name
  from rv.dip_stores;
$BODY$
  language sql;
*/

create or replace function rv.dip_get_stores()
  returns setof json as
$BODY$
/*
  select * from rv.dip_get_stores();
*/
  select row_to_json(a)
  from (
    select store_id, store_number, store_name
    from rv.dip_stores) a;
$BODY$
  language sql;  

/* this works, but must return all columns
create or replace function rv.dip_get_stores()
  returns setof rv.dip_stores as
$BODY$

--  select * from rv.dip_get_stores();

  select * --store_id, store_number, store_name
  from rv.dip_stores;
$BODY$
  language sql;
*/

drop function rv.dip_get_contacts();
/*
create or replace function rv.dip_get_contacts()
  returns setof rv.dip_contacts as
$BODY$
-- select * from rv.dip_get_contacts();
  select *
  from rv.dip_contacts;
$BODY$
  language sql;  
*/
create or replace function rv.dip_get_contacts()
  returns setof json as
$BODY$

-- select * from rv.dip_get_contacts();
  select row_to_json(a)
  from (
    select b.*, 
      (select store_name from rv.dip_stores where store_id = b.store_id)
    from rv.dip_contacts b ) a;
$BODY$
  language sql;    

/*create or replace function rv.dip_get_contacts()
  returns table (contact_id integer, store_id integer, contact_name text,email text, 
    offic_phone text, cell_phone text, crm text) as
$BODY$

-- select * from rv.dip_get_contacts();

  select *
  from rv.dip_contacts;
$BODY$
  language sql;
*/    

drop function rv.dip_insert_contact(integer,text) 

drop function rv.dip_insert_contact(integer,text,text,text) 

-- delete from rv.dip_contacts

create or replace function rv.dip_insert_contact(_store_id integer, _contact_name text,
  _email text, _office_phone text, _cell_phone text, _crm text)
  returns void as
$BODY$
/*
select * from rv.dip_insert_contact(98, 'Ben Foster','','','','drive velocity');
*/
  insert into rv.dip_contacts(store_id, contact_name, email, office_phone,
    cell_phone, crm)
  values(_store_id, _contact_name, _email, _office_phone, _cell_phone, _crm);
$BODY$  
  language sql;


create or replace function rv.dip_update_contact(_contact_id integer, _store_id integer, _contact_name text,
  _email text, _office_phone text, _cell_phone text, _crm text)
  returns void as
$BODY$ 
/*
select * from rv.dip_update_contact(5, 98, 'Ben Foster', 'bfoster@cartiva.com','','','drive velocity');
*/
update rv.dip_contacts
set store_id = _store_id,
    contact_name = _contact_name,
    email = _email,
    office_phone = _office_phone,
    cell_phone = _cell_phone,
    crm = _crm    
where contact_id = _contact_id;    
$BODY$  
  language sql;  


drop function rv.dip_search(text);
/*
create or replace function rv.dip_search(_search_string text)
  returns table (store_id integer, store_name text, contact_id integer, contact_name text, crm text) as
$BODY$  
  select a.store_id, a.store_name, b.contact_id, b.contact_name, b.crm
  from rv.dip_stores a
  left join rv.dip_contacts b on a.store_id = b.store_id
  where a.store_name like '%' || _search_string || '%'
    or b.contact_name like '%' || _search_string || '%'
    or b.crm like '%' || _search_string || '%';
$BODY$ 
  language sql;
*/
create or replace function rv.dip_search(_search_string text)
  returns setof json as
$BODY$  
/*
select * from rv.dip_search('city');
*/ 
  select row_to_json(c)
  from (
    select a.store_id, a.store_name, b.contact_id, b.contact_name, b.crm
    from rv.dip_stores a
    left join rv.dip_contacts b on a.store_id = b.store_id
    where a.store_name like '%' || _search_string || '%'
      or b.contact_name like '%' || _search_string || '%'
      or b.crm like '%' || _search_string || '%') c;
$BODY$ 
  language sql;  
  