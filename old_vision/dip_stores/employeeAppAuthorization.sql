SELECT *
FROM employeeappauthorization
WHERE appname = 'digitalmetrics'
  AND functionality = 'directory'
  

SELECT DISTINCT appname FROM  employeeappauthorization

INSERT INTO employeeappauthorization
SELECT *
FROM (
  SELECT DISTINCT username
  FROM employeeappauthorization a
  WHERE appname = 'digitalmetrics'
    AND NOT EXISTS (
      SELECT 1
      FROM employeeappauthorization
      WHERE username = a.username
        AND functionality = 'directory')) b
LEFT JOIN  (     
  SELECT DISTINCT appname, appseq, appcode, approle, functionality, CAST(NULL AS sql_integer)
  FROM employeeappauthorization
  WHERE appname = 'digitalmetrics'
    AND functionality = 'directory') c on  1 = 1
  