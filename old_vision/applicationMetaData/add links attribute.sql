ADD an attribute called links
category: configMapMarkupKey = mainlink...
detail: configMapMarkupKey = sublink...
for each category/detail (app, approle, appseq)
category: links IS a comma separated list of the ids for the sublinks
detail: id of the mainlink

ALTER TABLE applicationMetaData
ADD COLUMN links cichar(24);


-- used this query to GROUP the relevant rows together, THEN entered data
SELECT *
FROM applicationMetaData
WHERE navtype = 'leftnavbar'
  AND appcode = 'warad' AND appseq = 400 AND approle = 'admin'
ORDER BY appcode, approle, appseq

SELECT appcode, approle, appseq, navseq, id
FROM applicationMetaData
WHERE navtype = 'leftnavbar'
ORDER BY id

ORDER BY appcode, approle, appseq, navseq
-- 8/7 attempting to comply with jsonapi.org specification
-- updated basic links (list of ids) manually, now trying to format them AS desired
enclose links IN square brackets
SELECT *
FROM applicationMetaData
WHERE links IS NOT NULL 

UPDATE applicationMetaData
  SET links = '[' + TRIM(links) + ']'
WHERE links IS NOT NULL 

sublink
"menuCategory" : [1]

mainlink
"menuItems" : [13,29,82]

ALTER TABLE applicationMetaData
ALTER COLUMN links links cichar(100)

UPDATE applicationMetaData
SET links = '"menuCategory" : [' + trim(links) + ']'
FROM applicationMetaData
WHERE links IS NOT NULL
  AND configMapMarkupKey LIKE 'sub%'
  
  
  
  
UPDATE applicationMetaData
SET links = '"menuItems" : [' + trim(links) + ']'
FROM applicationMetaData
WHERE links IS NOT NULL
  AND configMapMarkupKey LIKE 'main%'  
  
-- now what IS missing, the ids IN links should ALL be surrounded BY doublequites

SELECT links,
  replace(replace(links, '[', '[ "'), ']', '" ]')
FROM applicationMetaData  

-- think this IS it
SELECT links,
  replace(replace(replace(links, '[', '[ "'), ']', '" ]'), ',','", "')
FROM applicationMetaData  

-- check spacing, looks ok
SELECT links,
  replace(replace(replace(replace(links, '[', '[ "'), ']', '" ]'), ',','", "'), ' ', '_')
FROM applicationMetaData  

UPDATE applicationMetaData
SET links = trim(replace(replace(replace(links, '[', '[ "'), ']', '" ]'), ',','", "'))
WHERE links IS NOT NULL 

shit, that fucked up, data truncated, of course, ran it again AND now links are fucked up
need to fix that

SELECT links, replace(replace(links, '" "','"'), '""','"')
FROM applicationMetaData
WHERE links IS NOT NULL 


-- ADD {}
UPDATE applicationMetaData
SET links = '{' + TRIM(links) + '}'
WHERE links IS NOT NULL 