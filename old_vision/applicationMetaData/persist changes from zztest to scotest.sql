-- navtext
SELECT a.navtext, b.navtext
FROM applicationMetaData a
inner join zztest.applicationMetaData b on a.appcode = b.appcode
  AND a.approle = b.approle
  AND a.functionality = b.functionality
  AND a.appseq = b.appseq
WHERE coalesce(a.navtext,'') <> coalesce(b.navtext,'')

-- navtext
UPDATE applicationMetaData
SET navtext = b.navtext
FROM zztest.applicationMetaData b
WHERE applicationMetaData.appcode = b.appcode
  AND applicationMetaData.approle = b.approle
  AND applicationMetaData.functionality = b.functionality
  AND applicationMetaData.appseq = b.appseq
  AND coalesce(applicationMetaData.navtext, '') <> coalesce(b.navtext, '') 

-- links
SELECT a.links, b.links
FROM applicationMetaData a
inner join zztest.applicationMetaData b on a.appcode = b.appcode
  AND a.approle = b.approle
  AND a.functionality = b.functionality
  AND a.appseq = b.appseq
WHERE coalesce(a.links, '') <> coalesce(b.links, '')  

-- links
UPDATE applicationMetaData
SET links = b.links
FROM zztest.applicationMetaData b
WHERE applicationMetaData.appcode = b.appcode
  AND applicationMetaData.approle = b.approle
  AND applicationMetaData.functionality = b.functionality
  AND applicationMetaData.appseq = b.appseq
  AND coalesce(applicationMetaData.links, '') <> coalesce(b.links, '')   