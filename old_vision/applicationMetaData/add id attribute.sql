for ember apps (working on LEFT nav USING ember data), need an "id" field
needs to represent 1 to many with unique integers such that a mainlink AND the
  relevant (app/approle/appseq) sublinks are groupable

tried to generate dynamically in sp emberTestGetApplicationNavigation, but flamed
  out, added an id attribute to TABLE applicationMetaData AND manually entered 
  suitable values

ALTER TABLE applicationMetaData
ADD COLUMN id integer;

-- used this query to GROUP the relevant rows together, THEN entered data
SELECT *
FROM applicationMetaData
WHERE navtype = 'leftnavbar'
ORDER BY appcode, approle, appseq, navseq

-- manually updated zztest, now UPDATE scotest FROM zztest

UPDATE applicationMetaData
  SET id = x.id
FROM (  
  SELECT appName, appCode, appRole, functionality, appSeq, id
  FROM zztest.applicationMetadata
  WHERE id IS NOT NULL) x
WHERE applicationMetaData.appName = x.appName
  AND applicationMetaData.appCode = x.appCode
  AND applicationMetaData.appRole = x.appRole
  AND applicationMetaData.functionality = x.functionality
  AND applicationMetaData.appSeq = x.appSeq
  
-- 8/6
further conversation with greg
ember requires a unique integer id for each row, but the bad assummption i made 
  was that those ids needed to be encoded with a notion of grouping
  that was incorrect
  simply need a unique integer identifier 
  change IN strategy, id becomes a simple autoinc COLUMN AND that would be good enough
  
so, save the data i currently have, IN CASE i fuck up
SELECT appname, appcode, approle, functionality, appseq, id
FROM applicationMetaData
WHERE id IS NOT NULL    

DROP TABLE tmpApplicationMetaData;
CREATE TABLE tmpApplicationMetaData ( 
      appName CIChar( 50 ),
      appCode CIChar( 6 ),
      appSeq Integer,
      appRole CIChar( 25 ),
      functionality CIChar( 50 ),
      id Integer) IN DATABASE;
INSERT INTO tmpApplicationMetaData
SELECT appname, appcode, appseq, approle, functionality, id
FROM applicationMetaData
WHERE id IS NOT NULL;     

-- here are the rows FROM ApplicationMetaData that have an id value
SELECT * FROM tmpApplicationMetaData ORDER BY appcode, appseq, approle

ALTER TABLE ApplicationMetaData
DROP COLUMN id;

ALTER TABLE ApplicationMetaData
ADD COLUMN id autoinc constraint NOT NULL;

EXECUTE PROCEDURE sp_CreateIndex90( 
   'ApplicationMetaData','applicationMetaData.adi','id','id','',2051,512,'' );

ha, it fucking worked.