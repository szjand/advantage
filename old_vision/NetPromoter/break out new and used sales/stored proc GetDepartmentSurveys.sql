ALTER PROCEDURE GetDepartmentSurveys
   ( 
      StoreCode CICHAR ( 3 ),
      Department CICHAR ( 12 ),
      Employee CICHAR ( 52 ) OUTPUT,
      theDate DATE OUTPUT,
      RO_Stock CICHAR ( 12 ) OUTPUT,
      CustomerName CICHAR ( 50 ) OUTPUT,
      ReferralLikelihood Integer OUTPUT,
      CustomerExperience Memo OUTPUT
   ) 
BEGIN 
/*

12/15/13
  modified to used a fixed date range
2/16/14: name fixes 
11/14/14
  break out sales: new AND used   
  service: detail  
    
EXECUTE PROCEDURE GetDepartmentSurveys('RY1', 'detail'); 

*/
DECLARE @store string;
DECLARE @department string;
DECLARE @fromDate date;
DECLARE @thruDate date;

@store = (SELECT StoreCode FROM __input);
@department = UPPER((SELECT Department FROM __input));
@fromDate = (
  SELECT MAX(thedate) - 89 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());

INSERT INTO __output
SELECT employeeName, transactionDate, transactionNumber, customerName, 
  referralLikelihood, customerExperience  
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND customerResponse = true
  AND storeCode = @store
  AND 
    CASE @department
      WHEN 'SALES' THEN transactionType = 'Sales'
      WHEN 'NEW' THEN subDepartment = 'NEW'
      WHEN 'USED' THEN subDepartment = 'USED'
      WHEN 'SERVICE' THEN transactionType = 'Service'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
      WHEN 'DETAIL' THEN subDepartment = 'RE'
    END;
	
	
END;

