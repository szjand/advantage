alter PROCEDURE populateCstfbNpData
   ( 
   ) 
BEGIN 
/*
runs once a week on monday morning

4/13
  ADD cstfbNpDataRolling 
  
11/11/14
  *a*
  break out new AND used sales (AS sub departments)  
11/14/14
  break out detail, only affect cstfbNpDataRolling  
  
EXECUTE PROCEDURE populateCstfbNpData();  
*/
BEGIN TRANSACTION;
TRY 
  DELETE FROM cstfbNpData;
  -- sales
  -- stop being lazy, specify the fields IN the INSERT statement
  INSERT INTO cstfbNpData (transactionDate, customerResponse, storeCode, 
    subDepartment, employeeName, transactionNumber, transactionType, 
    customerName, referralLikelihood, customerExperience)
  SELECT m.transactionDate, m.customerResponse, m.storeCode, 
    /*'None'*/ n.subDepartment, n.fullname,  n.stocknumber, 'Sales', 
    m.customerName, m.referralLikelihood,
    m.customerExperience
  FROM (    
    SELECT CAST(surveySentTs AS sql_date) AS transactionDate,  
      CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN true 
        ELSE false END AS customerResponse,
      CASE dealername
        WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
        ELSE 'RY2'
      END AS storeCode,
      roNumber AS transactionNumber, a.vin, a.transactionType, a.customerName,
      referralLikelihood, customerExperience
    FROM xfmDigitalAirStrike a
    INNER JOIN ( -- remove duplicate surveys
      SELECT max(CAST(surveySentTS AS sql_date)) AS theDate, customerName, vin, 
        transactionType
      FROM xfmDigitalAirStrike
      WHERE transactionType = 'sales'
      GROUP BY customerName, vin, transactionType) b 
        on CAST(a.surveySentTS AS sql_date) = b.theDate
          AND a.customerName = b.customerName
          AND a.transactionType = b.transactionType
          AND a.vin = b.vin
    WHERE a.transactionType = 'Sales') m  
  LEFT JOIN (
    SELECT b.stocknumber, c.vin, d.fullname, dd.thedate AS capDate, 
      ddd.thedate AS appDate, dddd.thedate AS origDate,
-- *a*
      upper(e.vehicleType) AS subDepartment      
    FROM dds.factVehicleSale b
    INNER JOIN dds.dimVehicle c on b.vehicleKey = c.vehicleKey
    INNER JOIN dds.dimSalesPerson d on b.consultantKey = d.salesPersonKey
    INNER JOIN dds.day dd on b.cappedDateKey = dd.datekey
    INNER JOIN dds.day ddd on b.approveddatekey = ddd.datekey
    INNER JOIN dds.day dddd on b.originationdatekey = dddd.datekey
-- *a*
    INNER JOIN dds.dimCarDealInfo e on b.carDealInfoKey = e.carDealInfoKey) n 
      on m.vin = n.vin
        AND (ABS(m.transactionDate - n.capdate) < 14  
          OR ABS(m.transactiondate - n.appdate) < 14 
          OR ABS(m.transactiondate - n.origdate) < 14
          OR m.transactionDate = '02/08/2014')
  WHERE n.fullname IS NOT NULL;    
  -- service  
  INSERT INTO cstfbNpData    
  SELECT m.transactionDate, m.customerResponse, m.storeCode, 
    coalesce(n.censusDept, 'XX'), 
    coalesce(n.fullname, 'Other'), m.transactionNumber, 'Service', 
    m.customerName, m.referralLikelihood, m.customerExperience
  FROM (   
    SELECT CAST(surveySentTs AS sql_date) AS transactionDate,  
      CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN true 
        ELSE false END AS customerResponse,
      CASE dealername
        WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
        ELSE 'RY2'
      END AS storeCode,
      'subDepartment', 'employeename',
      left(roNumber, 9) AS transactionNumber, a.vin, a.transactionType, a.customerName,
      referralLikelihood, customerExperience
    FROM xfmDigitalAirStrike a
    WHERE a.transactiontype = 'service'
      AND roNumber NOT IN ( -- eliminate dup ro
        SELECT roNumber
        FROM xfmdigitalairstrike
        GROUP BY roNumber
        HAVING COUNT(*) > 1)) m
  LEFT JOIN (
    SELECT distinct b.ro, c.censusDept, TRIM(d.firstname) + ' ' + TRIM(d.lastname) AS fullName
    FROM dds.factRepairOrder b
    INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.serviceWriterKey
    LEFT JOIN dds.edwEmployeeDim d 
  --    on c.storecode = d.storecode -- can't use store, writers may NOT be emp of the store
      ON c.employeenumber = d.employeenumber 
      AND d.currentrow = true 
    WHERE b.ro IN (
      SELECT ronumber
      FROM xfmdigitalairstrike )) n on m.transactionNumber = n.ro;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE cstfbNpData(1, 'cstfbNpData ' + __errtext);
END TRY;  
         
BEGIN TRANSACTION;
TRY
  DELETE FROM cstfbNpDataRolling;		
  
  INSERT INTO cstfbNpDataRolling  
  -- market
  SELECT 'GF' AS market, 'All' as store, 'All' as department, 'All' as subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,    
    round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
      .01/COUNT(*)), 2)  AS responseRate,
    (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
    -
    SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
      /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT top 20 theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= curdate()
      ORDER BY theDate DESC) a) b
  LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY fromDate, thruDate;   
  
  INSERT INTO cstfbNpDataRolling  
  -- store
  SELECT 'GF' as market, storecode AS store, 'All' as department, 'All' as subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
    round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
      .01/COUNT(*)), 2)  AS responseRate,
    (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
    -
    SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
      /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT top 20 theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= curdate()
      ORDER BY theDate DESC) a) b
  LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY storecode, fromDate, thruDate;
  
  INSERT INTO cstfbNpDataRolling  
  -- department
  SELECT 'GF' as market, storecode AS store, left(transactionType, 12) as department, 'All' as subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
    round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
      .01/COUNT(*)), 2)  AS responseRate,
    (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
    -
    SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
      /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 AS score
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT top 20 theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= curdate()
      ORDER BY theDate DESC) a) b
  LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
  GROUP BY storecode, transactionType, fromDate, thruDate;
  
  INSERT INTO cstfbNpDataRolling  
  -- subDepartment
  SELECT 'GF' as market, storecode AS store, left(transactionType, 12) as department, 
    subDepartment,
    fromDate, thruDate, 
    COUNT(*) AS sent,
    SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) AS returned,
-- *a* adding new/used generated divide BY zero errors
    CASE
      WHEN COUNT(*) = 0 THEN 0
      ELSE     
        round(100 * (SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) * 
            .01/COUNT(*)), 2) 
     END AS responseRate,
     CASE 
       WHEN SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) = 0 THEN 0
     ELSE
      (SUM(CASE WHEN referralLikelihood > 8 THEN 1 ELSE 0 END) * 
            100/SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END) 
          -
          SUM(CASE WHEN referralLikelihood < 7 AND customerResponse = true THEN 1 ELSE 0 END) * 100
            /SUM(CASE WHEN customerResponse = true THEN 1 ELSE 0 END))/100.0 
     END AS score
  -- SELECT *    
  FROM (
    SELECT mondays - 90 AS fromDate, mondays AS thruDate
    FROM (  
      SELECT top 20 theDate as mondays
      FROM dds.day
      WHERE dayofWeek = 2
        AND theDate <= curdate()
      ORDER BY theDate DESC) a) b
  LEFT JOIN cstfbNpData c on c.transactiondate BETWEEN b.fromdate AND b.thruDate
-- *a*  
  WHERE c.subDepartment IN ('BS','MR','QL','NEW','USED','RE')
  GROUP BY storecode, transactionType, subDepartment, fromDate, thruDate;
   
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE cstfbNpData(2, 'cstfbNpDataRolling ' + __errtext);
END TRY;  

END;

