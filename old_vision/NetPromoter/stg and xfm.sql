-- multiple rows per vin, transactiontype, surveysentts 
SELECT *
FROM stgDigitalAirStrike a
WHERE EXISTS (
  SELECT 1
  FROM (
    SELECT vin, transactiontype, surveysentts 
    FROM stgDigitalAirStrike
    GROUP BY vin, transactiontype, surveysentts 
    HAVING COUNT(*) = 2) b
  WHERE b.vin = a.vin
    AND b.transactiontype = a.transactiontype
    AND b.surveysentts = a.surveysentts)
ORDER BY vin        
 
 
-- duplicate rows including surveyResponseTS
SELECT *
FROM stgDigitalAirStrike a
WHERE EXISTS (
  SELECT 1
  FROM (
    SELECT vin, transactiontype, surveysentts, surveyresponsets 
    FROM stgDigitalAirStrike
    GROUP BY vin, transactiontype, surveysentts, surveyresponsets 
    HAVING COUNT(*) = 2 ) b
  WHERE b.vin = a.vin
    AND b.transactiontype = a.transactiontype
    AND b.surveysentts = a.surveysentts
    AND b.surveyresponsets = a.surveyresponsets)
ORDER BY vin       

-- so, what values can change over time
-- from duplicate rows including surveyResponseTS we find
DealerResponse
DealerResponseTS
-- from multiple rows per vin, transactiontype, surveysentts 
SurveyResponseTS
ConsentToPublish
StarRating
ReferralLikelihood
CustomerExperience
PassProfanity
DealerResponseTS
DealerResponse
PublicationDate
PublicationStatus

SELECT *
FROM stgDigitalAirStrike a
WHERE NOT EXISTS (
  SELECT 1
  FROM xfmDigitalAirStrike
  WHERE vin = a.vin
    AND transactiontype = a.transactiontype
    AND surveyresponsets <> a.surveyresponsets)

-- xfmDigitalAirStrike: unique index: VIN, TransactionType, SurveySentTS
-- initial feed INTO xfm
DELETE FROM xfmDigitalAirStrike;
INSERT INTO xfmDigitalAirStrike (
  DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
  CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
  StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
  DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
  PublicationStatus)
SELECT distinct a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,cast(a.CustomerExperience AS sql_Char),a.PassProfanity,
  a.DealerResponseTS,cast(a.DealerResponse AS sql_char),a.SalesPersonName,a.PublicationDate,
  a.PublicationStatus
FROM stgDigitalAirStrike a 
INNER JOIN (
  SELECT vin, transactiontype, surveySentts
  FROM stgDigitalAirStrike
  GROUP BY vin, transactiontype, SurveySentTS
  HAVING COUNT(*) = 1) b on a.vin = b.vin AND a.TransactionType = b.TransactionType
    AND a.SurveySentTS = b.SurveySentTS;
    
-- now, get the rest of them IN there AND figure out the MERGE

-- these surveys exist IN stg but NOT xfm
-- need to make them one row
-- DROP TABLE #wtf;
SELECT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,cast(a.CustomerExperience AS sql_Char) AS CustomerExperience,a.PassProfanity,
  a.DealerResponseTS,cast(a.DealerResponse AS sql_char) AS DealerResponse,a.SalesPersonName,a.PublicationDate,
  a.PublicationStatus
INTO #wtf  
FROM stgDigitalAirStrike a
WHERE NOT EXISTS (
  SELECT 1
  FROM xfmDigitalAirStrike
  WHERE vin = a.vin
    AND transactiontype = a.transactiontype
    AND surveysentTS = a.SurveySentTS)   
    
-- those rows with no response that are multiples
-- they are exact duplicates
SELECT DISTINCT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,cast(a.CustomerExperience AS sql_Char),a.PassProfanity,
  a.DealerResponseTS,cast(a.DealerResponse AS sql_char),a.SalesPersonName,a.PublicationDate,
  a.PublicationStatus
FROM #wtf a
WHERE vin IN (    
  SELECT vin
  FROM #wtf
  WHERE year(CAST(surveyresponsets AS sql_date)) = 1969  
  GROUP BY vin, transactiontype, surveySentts
  HAVING COUNT(*) > 1) 
    
-- these 3 can go INTO xfm
INSERT INTO xfmDigitalAirStrike (
  DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
  CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
  StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
  DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
  PublicationStatus)
SELECT distinct a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,cast(a.CustomerExperience AS sql_Char),a.PassProfanity,
  a.DealerResponseTS,cast(a.DealerResponse AS sql_char),a.SalesPersonName,a.PublicationDate,
  a.PublicationStatus  
FROM #wtf a
WHERE vin IN (    
  SELECT vin
  FROM #wtf
  WHERE year(CAST(surveyresponsets AS sql_date)) = 1969  
  GROUP BY vin, transactiontype, surveySentts
  HAVING COUNT(*) > 1)    
  
-- regenerate #wtf  
-- #wtf are rows that exist IN stg but NOT xfm
SELECT * 
FROM #wtf

-- have to make INTO a single row these multiple row survey
-- combine base vin/transtype/sentTS with 
-- the row with no response
SELECT *
FROM #wtf
WHERE year(CAST(surveyresponsets AS sql_date)) = 1969 


-- multiple rows for surveys with a responseTS
SELECT vin, transactiontype, surveySentts
FROM #wtf
WHERE year(CAST(surveyresponsets AS sql_date)) <> 1969 
GROUP BY vin, transactiontype, surveySentts
HAVING COUNT(*) > 1
-- these are goofy, 3 have diff reponseTS
-- on each of the 3, the earlier response has a pubStatus of Opt-Out, the later response
-- has a pubStatus of NULL, which makes me think i really don't give a shit about publication date AND status
-- what i care about
SELECT *
FROM #wtf a
WHERE year(CAST(surveyresponsets AS sql_date)) <> 1969 
AND EXISTS (
  SELECT 1
  FROM (
    SELECT vin, TransactionType
    FROM #wtf
    WHERE year(CAST(surveyresponsets AS sql_date)) <> 1969 
    GROUP BY vin, transactiontype, surveySentts
    HAVING COUNT(*) > 1) b 
  WHERE b.vin = a.vin
    AND b.TransactionType = a.TransactionType)
ORDER BY vin  

-- vins 477, 440 dealer response, IF a MAX them DO i get the comment
-- yep, this IS good to go INTO xfm
INSERT INTO xfmDigitalAirStrike (
  DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
  CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
  StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
  DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
  PublicationStatus)
SELECT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,max(a.SurveyResponseTS),a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,cast(a.CustomerExperience AS sql_Char),a.PassProfanity,
  max(a.DealerResponseTS),max(cast(a.DealerResponse AS sql_char)),max(a.SalesPersonName),max(a.PublicationDate),
  max(a.PublicationStatus)  
FROM #wtf a
WHERE year(CAST(surveyresponsets AS sql_date)) <> 1969 
AND EXISTS (
  SELECT 1
  FROM (
    SELECT vin, TransactionType
    FROM #wtf
    WHERE year(CAST(surveyresponsets AS sql_date)) <> 1969 
    GROUP BY vin, transactiontype, surveySentts
    HAVING COUNT(*) > 1) b 
  WHERE b.vin = a.vin
    AND b.TransactionType = a.TransactionType)  
GROUP BY a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,a.UnsubscribeTS,a.ConsentToPublish,
  a.StarRating,a.ReferralLikelihood,cast(a.CustomerExperience AS sql_Char),a.PassProfanity

regenerate #wtf 
SELECT COUNT(*) FROM #wtf
-- excellent, exactly WHERE i wanted to be, 2 rows for each survey
SELECT vin, transactiontype, surveySentts, COUNT(*)
FROM #wtf
GROUP BY vin, transactiontype, surveySentts

SELECT * FROM #wtf ORDER BY vin

-- so, what values can change over time
-- from duplicate rows including surveyResponseTS we find
DealerResponse
DealerResponseTS
-- from multiple rows per vin, transactiontype, surveysentts 
SurveyResponseTS
ConsentToPublish
StarRating
ReferralLikelihood
CustomerExperience
PassProfanity
DealerResponseTS
DealerResponse
PublicationDate
PublicationStatus

so JOIN the rows with no response to the rows with response, take the fields FROM w/response to form a single row

SELECT COUNT(*) FROM #wtf WHERE year(CAST(surveyresponsets AS sql_date)) = 1969  -- 125
SELECT COUNT(*) FROM #wtf WHERE year(CAST(surveyresponsets AS sql_date)) <> 1969  -- 125
SELECT * FROM #wtf ORDER BY vin

-- should NOT have to DO a DISTINCT on this one
INSERT INTO xfmDigitalAirStrike (
  DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
  CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
  StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
  DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
  PublicationStatus)
SELECT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
  a.CustomerEmail,
  b.UnsubscribeTS,b.SurveyResponseTS,b.ConsentToPublish,
  b.StarRating,b.ReferralLikelihood,b.CustomerExperience,b.PassProfanity,
  b.DealerResponseTS,b.DealerResponse,b.SalesPersonName,b.PublicationDate,
  b.PublicationStatus  
FROM (
  SELECT *
  FROM #wtf 
  WHERE year(CAST(surveyresponsets AS sql_date)) = 1969) a
LEFT JOIN (    
  SELECT *
  FROM #wtf 
  WHERE year(CAST(surveyresponsets AS sql_date)) <> 1969) b on a.vin = b.vin AND a.transactiontype = b.transactiontype
AND a.SurveySentTS = b.SurveySentTS

-- bingo, it IS ALL IN there
SELECT *
FROM stgDigitalAirStrike a
WHERE NOT EXISTS (
  SELECT 1
  FROM xfmDigitalAirStrike
  WHERE vin = a.vin
    AND transactiontype = a.transactiontype
    AND surveySentTS = a.SurveySentTS)
