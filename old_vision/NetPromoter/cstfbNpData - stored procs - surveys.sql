/*
      Employee CICHAR ( 50 ) OUTPUT,
      theDate CICHAR ( 9 ) OUTPUT,
      RO_Stock CICHAR ( 12 ) OUTPUT,
      CustomerName CICHAR ( 50 ) OUTPUT,
      ReferralLikelihood CICHAR ( 12 ) OUTPUT,
      CustomerExperience Memo OUTPUT
*/   
-- marketsurveys  
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = (
  SELECT MAX(thedate) - 90 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate()); 
SELECT employeeName, transactionDate, transactionNumber, customerName, 
  referralLikelihood, customerExperience  
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND customerResponse = true
ORDER BY transactionDate DESC;    
        
-- storesurvesy   
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @store string;
@store = 'RY1';
@fromDate = (
  SELECT MAX(thedate) - 90 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate()); 
SELECT employeeName, transactionDate, transactionNumber, customerName, 
  referralLikelihood, customerExperience  
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND customerResponse = true
  AND storeCode = @store
ORDER BY transactionDate DESC;          

-- departmentsurveys  
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @store string;
DECLARE @department string;
@department = 'SALES';
@store = 'RY2';
@fromDate = (
  SELECT MAX(thedate) - 90 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate()); 
SELECT employeeName, transactionDate, transactionNumber, customerName, 
  referralLikelihood, customerExperience  
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND customerResponse = true
  AND storeCode = @store
  AND 
    CASE @department
      WHEN 'SALES' THEN transactionType = 'Sales'
      WHEN 'SERVICE' THEN transactionType = 'Service'
      WHEN 'MAINSHOP' THEN subDepartment = 'MR'
      WHEN 'PDQ' THEN subDepartment = 'QL'
      WHEN 'BODYSHOP' THEN subDepartment = 'BS'
    END
ORDER BY transactionDate DESC;  

-- employeesurveys  
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @store string;
DECLARE @employee string;
@employee = 'Fred Spencer';
@fromDate = (
  SELECT MAX(thedate) - 90 AS fromDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate());	
@thruDate = (
  SELECT MAX(thedate) AS thruDate
  FROM dds.day
  WHERE dayofweek = 1
    AND thedate <= curdate()); 
SELECT employeeName, transactionDate, transactionNumber, customerName, 
  referralLikelihood, customerExperience  
FROM cstfbNpData
WHERE transactionDate BETWEEN @fromDate AND @thruDate
  AND customerResponse = true
  AND employeename = @employee
ORDER BY transactionDate DESC;  