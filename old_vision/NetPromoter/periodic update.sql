alter PROCEDURE ProcessNewSurveyData(
  Anomalies integer output)
BEGIN
/*
EXECUTE PROCEDURE ProcessNewSurveyData();
*/
TRY 
  -- surveys IN stgDigitalAirStrike that DO NOT exist IN xfmDigitalAirStrike
  DELETE FROM xfmNewSurveys;     
  INSERT INTO xfmNewSurveys (
    DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
    CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
    StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
    DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
    PublicationStatus)
  SELECT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
    a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
    a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
    a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
    a.PublicationStatus 
  FROM stgDigitalAirStrike a
  WHERE NOT EXISTS (
    SELECT 1
    FROM xfmDigitalAirStrike
    WHERE vin = a.vin
      AND transactionType = a.TransactionType
      AND SurveySentTS = a.SurveySentTS);
  -- INSERT INTO xfmAnomalies FROM xfmNewSurveys WHERE there IS more than one row per VIN/TranType/SentTS 
  INSERT INTO xfmAnomalies  
  SELECT curdate(), 'New',a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
    a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
    a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
    a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
    a.PublicationStatus
  FROM xfmNewSurveys a 
  WHERE EXISTS (
    SELECT 1
    FROM (   
      SELECT vin, TransactionType, SurveySentTS
      FROM xfmNewSurveys
      GROUP BY vin, TransactionType, SurveySentTS
      HAVING COUNT(*) > 1) b 
    WHERE b.vin = a.vin
      AND b.TransactionType = a.TransactionType
      AND b.SurveySentTS = a.SurveySentTS);  
  -- existing surveys
  DELETE FROM xfmSurveysToUpdate;     
  INSERT INTO xfmSurveysToUpdate (
    DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
    CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
    StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
    DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
    PublicationStatus)
  SELECT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
    a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
    a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
    a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
    a.PublicationStatus 
  FROM stgDigitalAirStrike a
  WHERE EXISTS (
    SELECT 1
    FROM xfmDigitalAirStrike
    WHERE vin = a.vin
      AND transactionType = a.TransactionType
      AND SurveySentTS = a.SurveySentTS);         
  -- INSERT INTO xfmAnomalies FROM xfmSurveysToUpdate WHERE there IS more than one row per VIN/TranType/SentTS 
  INSERT INTO xfmAnomalies  
  SELECT curdate(), 'New',a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
    a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
    a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
    a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
    a.PublicationStatus
  FROM xfmSurveysToUpdate a 
  WHERE EXISTS (
    SELECT 1
    FROM (   
      SELECT vin, TransactionType, SurveySentTS
      FROM xfmSurveysToUpdate
      GROUP BY vin, TransactionType, SurveySentTS
      HAVING COUNT(*) > 1) b 
    WHERE b.vin = a.vin
      AND b.TransactionType = a.TransactionType
      AND b.SurveySentTS = a.SurveySentTS);      
CATCH ALL 
  RAISE DigitalAirStrike(1, 'Uh Oh: ' + __errtext);
END TRY;    
    
BEGIN TRANSACTION;
TRY     
  -- INSERT new rows
  INSERT INTO xfmDigitalAirStrike (
    DealerName,VIN,RONumber,SurveySentTS,TransactionType,CustomerName,
    CustomerEmail,UnsubscribeTS,SurveyResponseTS,ConsentToPublish,
    StarRating,ReferralLikelihood,CustomerExperience,PassProfanity,
    DealerResponseTS,DealerResponse,SalesPersonName,PublicationDate,
    PublicationStatus)
  SELECT a.DealerName,a.VIN, a.RONumber,a.SurveySentTS,a.TransactionType,a.CustomerName,
    a.CustomerEmail,a.UnsubscribeTS,a.SurveyResponseTS,a.ConsentToPublish,
    a.StarRating,a.ReferralLikelihood,a.CustomerExperience,a.PassProfanity,
    a.DealerResponseTS,a.DealerResponse,a.SalesPersonName,a.PublicationDate,
    a.PublicationStatus
  FROM xfmNewSurveys a 
  WHERE NOT EXISTS ( -- exclude WHERE there IS more than one row per VIN/TranType/SentTS 
    SELECT 1
    FROM (   
      SELECT vin, TransactionType, SurveySentTS
      FROM xfmNewSurveys
      GROUP BY vin, TransactionType, SurveySentTS
      HAVING COUNT(*) > 1) b 
    WHERE b.vin = a.vin
      AND b.TransactionType = a.TransactionType
      AND b.SurveySentTS = a.SurveySentTS);  
  -- UPDATE existing rows with the new values    
  UPDATE xfmDigitalAirStrike
    SET UnsubscribeTS = c.UnsubscribeTS,
        SurveyResponseTS = c.SurveyResponseTS,
        ConsentToPublish = c.ConsentToPublish,
        StarRating = c.StarRating,
        ReferralLikelihood = c.ReferralLikelihood,
        CustomerExperience = c.CustomerExperience,
        PassProfanity = c.PassProfanity,
        DealerResponseTS = c.DealerResponseTS,
        DealerResponse = c.DealerResponse,
        SalesPersonName = c.SalesPersonName,
        PublicationDate = c.PublicationDate,
        PublicationStatus = c.PublicationStatus
  FROM (      
    SELECT a.VIN, a.SurveySentTS,a.TransactionType,
      b.UnsubscribeTS,b.SurveyResponseTS,b.ConsentToPublish,
      b.StarRating,b.ReferralLikelihood,b.CustomerExperience,b.PassProfanity,
      b.DealerResponseTS,b.DealerResponse,b.SalesPersonName,b.PublicationDate,
      b.PublicationStatus 
    FROM xfmDigitalAirStrike a
    INNER JOIN xfmSurveysToUpdate b on a.vin = b.vin
      AND a.TransactionType = b.TransactionType
      AND a.SurveySentTS = b.SurveySentTS 
    WHERE NOT EXISTS ( -- exclude WHERE there IS more than one row per VIN/TranType/SentTS 
      SELECT 1
      FROM (   
        SELECT vin, TransactionType, SurveySentTS
        FROM xfmSurveysToUpdate
        GROUP BY vin, TransactionType, SurveySentTS
        HAVING COUNT(*) > 1) b 
      WHERE b.vin = a.vin
        AND b.TransactionType = a.TransactionType
        AND b.SurveySentTS = a.SurveySentTS)) c
  WHERE xfmDigitalAirStrike.vin = c.vin
    AND xfmDigitalAirStrike.TransactionType = c.TransactionType
    AND xfmDigitalAirStrike.SurveySentTS = c.SurveySentTS;     
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE DigitalAirStrike(2, 'xfmDigitalAirStrike ' + __errtext);   
END TRY;    

TRY 
  INSERT INTO DASHistory
  SELECT *
  FROM stgDigitalAirStrike;
CATCH ALL 
  RAISE DigitalAirStrike(3, 'DASHistory: ' + __errtext);
END TRY;

INSERT INTO __ouput
SELECT COUNT(*)
FROM xfmAnomalies
WHERE theDate = curdate();

END;
