SELECT dealername, COUNT(*)
FROM xfmDigitalAirStrike
GROUP BY dealername

SELECT dealername, transactiontype, vin, ronumber, salespersonname, surveysentts
FROM xfmDigitalAirStrike
WHERE CAST(surveysentts AS sql_date) > curdate() - 90
  AND transactiontype = 'sales'
  
  
SELECT dealername, transactiontype, vin, ronumber, salespersonname, surveysentts
FROM xfmDigitalAirStrike
WHERE CAST(surveysentts AS sql_date) > curdate() - 90 
--  AND Dealername = 'Honda of Grand Forks'
ORDER BY surveysentts  


SELECT bmdtor, bmdtaprv, bmdtcap, bmco#, bmkey, bmstat, bmtype, bmvtyp, bmwhsl, bmstk#, bmvin, 
  bmsnam, bmpslp, b.bqspid, b.bqname
FROM dds.stgArkonaBOPMAST a
LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
  AND b.bqtype = 'S'
WHERE year(bmdtor) = 2013

SELECT * FROM xfmDigitalAirStrike
  
SELECT dealername, transactiontype, vin, ronumber, salespersonname, surveysentts, b.*
FROM xfmDigitalAirStrike a
LEFT JOIN (
  SELECT bmdtor, bmdtaprv, bmdtcap, /*bmco#, bmkey, bmstat, bmtype, bmvtyp, bmwhsl,*/ bmstk#, bmvin, 
    bmpslp, b.bqspid, b.bqname
  FROM dds.stgArkonaBOPMAST a
  LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
    AND b.bqtype = 'S'
  WHERE year(bmdtor) = 2013) b on a.vin = b.bmvin 
WHERE CAST(surveysentts AS sql_date) > curdate() - 90 

-- uh oh, shit load of dups
SELECT dealername, transactiontype, vin, COUNT(*)
FROM (
  SELECT dealername, transactiontype, vin, ronumber, salespersonname, surveysentts, b.*
  FROM xfmDigitalAirStrike a
  LEFT JOIN (
    SELECT bmdtor, bmdtaprv, bmdtcap, /*bmco#, bmkey, bmstat, bmtype, bmvtyp, bmwhsl,*/ bmstk#, bmvin, 
      bmpslp, b.bqspid, b.bqname
    FROM dds.stgArkonaBOPMAST a
    LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
      AND b.bqtype = 'S'
    WHERE year(bmdtor) = 2013) b on a.vin = b.bmvin 
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90) x
GROUP BY dealername, transactiontype, vin
HAVING COUNT(*) > 1  
ORDER BY vin

ok, eliminate 
  WHERE vin IS blank, 
  WHERE bmdtcap = '12/31/9999'
  AND bmdtcap = MAX(bmdtcap)

  -- ok, the remaining dups are due to multiple sales consultants
  -- it IS NOT ok to include the dups at least IN the summary queries because of
  -- double counting
  -- adding the bqspid doesn't fix it what the fuck

-- no more dups
SELECT bmco#, bmvin FROM (
SELECT *
FROM (
  SELECT bmco#, bmdtor, bmdtaprv, bmdtcap, /*bmco#, bmkey, bmstat, bmtype, bmvtyp, bmwhsl,*/ bmstk#, bmvin, 
    bmpslp, b.bqspid, b.bqname
  FROM dds.stgArkonaBOPMAST a
  LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
    AND b.bqtype = 'S'
  WHERE year(bmdtcap) = 2013  
    AND TRIM(bmvin) <> '') a
INNER JOIN ( 
  SELECT bmco#,bmvin, max(bmdtcap) AS bmdtcap, max(b.bqname) AS bqname
  FROM dds.stgArkonaBOPMAST a
  LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
    AND b.bqtype = 'S'
  WHERE year(bmdtcap) = 2013  
    AND TRIM(bmvin) <> '' 
  GROUP BY bmco#,bmvin) b on a.bmco# = b.bmco# AND a.bmvin = b.bmvin 
    AND a.bmdtcap = b.bmdtcap AND a.bqname = b.bqname
) x GROUP BY bmco#, bmvin HAVING COUNT(*) > 1

-- hmmm thinking UNION sales query with service query
-- definitely
SELECT dealername, transactiontype, vin, ronumber, salespersonname, surveysentts, b.*
FROM xfmDigitalAirStrike a
LEFT JOIN (
SELECT *
FROM (
  SELECT bmco#, bmdtor, bmdtaprv, bmdtcap, /*bmco#, bmkey, bmstat, bmtype, bmvtyp, bmwhsl,*/ bmstk#, bmvin, 
    bmpslp, b.bqspid, b.bqname
  FROM dds.stgArkonaBOPMAST a
  LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
    AND b.bqtype = 'S'
  WHERE year(bmdtcap) = 2013  
    AND TRIM(bmvin) <> '') a
INNER JOIN ( 
  SELECT bmco#,bmvin, max(bmdtcap) AS bmdtcap, max(b.bqname) AS bqname
  FROM dds.stgArkonaBOPMAST a
  LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
    AND b.bqtype = 'S'
  WHERE year(bmdtcap) = 2013  
    AND TRIM(bmvin) <> '' 
  GROUP BY bmco#,bmvin) b on a.bmco# = b.bmco# AND a.bmvin = b.bmvin 
    AND a.bmdtcap = b.bmdtcap AND a.bqname = b.bqname) b on a.vin = b.bmvin 
WHERE CAST(surveysentts AS sql_date) > curdate() - 90 

-- ok, lets go, separate queries
-- market/service
-- need to get honda writer names
SELECT 'Market' AS Level, 'Grand Forks' AS who, 'Service' AS Dept,
  cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST( 
    -- Promoters
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore,            
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS ScoreAsInteger   
FROM xfmDigitalAirStrike a
LEFT JOIN dds.factRepairOrder b on a.ronumber = b.ro
INNER JOIN dds.dimServiceWriter c on b.ServiceWriterKey = c.ServiceWriterKey
WHERE CAST(a.surveysentts AS sql_date) > curdate() - 90
  AND a.transactiontype = 'Service'

  -- market/sales
SELECT 'Market' AS Level, 'Grand Forks' AS who, 'Sales' AS Dept,
  cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST( 
    -- Promoters
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore,            
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS ScoreAsInteger   
FROM xfmDigitalAirStrike a
LEFT JOIN (
    SELECT bmco#,bmvin, max(bmdtcap) AS bmdtcap, max(b.bqname) AS bqname
    FROM dds.stgArkonaBOPMAST a
    LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
      AND b.bqtype = 'S'
    WHERE year(bmdtcap) = 2013  
      AND TRIM(bmvin) <> '' 
    GROUP BY bmco#,bmvin) b on a.vin = b.bmvin
WHERE CAST(a.surveysentts AS sql_date) > curdate() - 90
  AND a.transactiontype = 'Sales'     
  
  
-- 10/23/13 ----------------------------------------------------------------------
after a shit load of mind fucking, here IS the new approach

instead of prematurely segragating sales AND service,
initial segregation IS scores AND surveys

fuckety fuck fuck
DO NOT need detail shit for scores

so, start over
for market scores
ALL the necessary data IS IN xfmDigitalAirStrike
output IS 
  sent
  returned
  response rate
  score
to generate those i need
SurveySentTS
SurveyResponseTS
ReferralLikelihood

for surveys
need 
  sc/writer
  date (sent)
  ro/stk#
  customer
  referral likelihood
  text
  
-- basic scores query
-- 1 row per survey
SELECT DealerName, vin, ronumber, TransactionType, CustomerName, max(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
  MAX(ReferralLikelihood) AS ReferralLikelihood   
FROM xfmDigitalAirStrike
WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
GROUP BY DealerName, vin, ronumber, TransactionType, CustomerName

-- market scores
SELECT 
  cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST( 
    -- Promoters
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore
FROM (      
  SELECT DealerName, vin, ronumber, TransactionType, CustomerName, max(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
    MAX(ReferralLikelihood) AS ReferralLikelihood   
  FROM xfmDigitalAirStrike
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
  GROUP BY DealerName, vin, ronumber, TransactionType, CustomerName) x;

-- store scores
SELECT DealerName,
  cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST( 
    -- Promoters
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore,            
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS ScoreAsInteger 
FROM (      
  SELECT DealerName, vin, ronumber, TransactionType, CustomerName, max(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
    MAX(ReferralLikelihood) AS ReferralLikelihood   
  FROM xfmDigitalAirStrike
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
  GROUP BY DealerName, vin, ronumber, TransactionType, CustomerName) x  
GROUP BY DealerName;  

-- store/dept scores
SELECT DealerName, TransactionType,
  cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST( 
    -- Promoters
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore,            
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS ScoreAsInteger 
FROM (      
  SELECT DealerName, vin, ronumber, TransactionType, CustomerName, max(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
    MAX(ReferralLikelihood) AS ReferralLikelihood   
  FROM xfmDigitalAirStrike
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
  GROUP BY DealerName, vin, ronumber, TransactionType, CustomerName) x  
GROUP BY DealerName, TransactionType;  

-- IF the department IS sales, need to return scores for the individual salespeople AS well
-- but, digitalairstrike does NOT have names for RY2 (yet, hopefully)
-- so i need to get them FROM bopmast

SELECT bmco#, bmvin FROM (
  SELECT bmco#,bmvin, max(bmdtcap) AS bmdtcap, max(b.bqname) AS bqname
  FROM dds.stgArkonaBOPMAST a
  LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
    AND b.bqtype = 'S'
  WHERE year(bmdtcap) = 2013  
    AND TRIM(bmvin) <> '' 
  GROUP BY bmco#,bmvin
) x GROUP BY bmco#, bmvin HAVING COUNT(*) > 1
-- this works
SELECT a.*, b.bqname
FROM (
  SELECT DealerName, vin, ronumber, TransactionType, CustomerName, 
    max(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
    MAX(ReferralLikelihood) AS ReferralLikelihood
  FROM xfmDigitalAirStrike
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
    AND TransactionType = 'Sales'
  GROUP BY DealerName, vin, ronumber, TransactionType, CustomerName) a
LEFT JOIN (
  SELECT bmco#,bmvin, max(b.bqname) AS bqname
  FROM dds.stgArkonaBOPMAST a
  LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
    AND b.bqtype = 'S'
  WHERE year(bmdtcap) = 2013  
    AND TRIM(bmvin) <> '' 
  GROUP BY bmco#,bmvin) b on a.vin = b.bmvin AND iif(a.DealerName = 'Honda of Grand Forks', 'RY2', 'RY1') = b.bmco#
  
    SELECT DealerName, vin, ronumber, TransactionType, CustomerName, 
      max(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
      MAX(ReferralLikelihood) AS ReferralLikelihood,
      b.bqname AS Employee
    FROM xfmDigitalAirStrike a
    LEFT JOIN (
      SELECT bmco#,bmvin, max(b.bqname) AS bqname
      FROM dds.stgArkonaBOPMAST a
      LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
        AND b.bqtype = 'S'
      WHERE year(bmdtcap) = 2013  
        AND TRIM(bmvin) <> '' 
      GROUP BY bmco#,bmvin) b on a.vin = b.bmvin AND iif(a.DealerName = 'Honda of Grand Forks', 'RY2', 'RY1') = b.bmco#
    WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
      AND TransactionType = @department
      AND DealerName = 
        CASE
          WHEN @store = 'RY1' THEN 'Rydell Chevrolet Buick GMC Cadillac'
          WHEN @store = 'RY2' THEN 'Honda of Grand Forks'
        END   
        
          
-- ok, rethinking
-- separate dept totals AND dept people
-- need to talk to libby AND greg about separate displays for people/depts
-- the above gives me sales people
-- need service people  

SELECT DealerName, vin, ronumber, TransactionType, 
  MAX(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
  MAX(ReferralLikelihood) AS ReferralLikelihood, c.censusdept
FROM xfmDigitalAirStrike a
LEFT JOIN dds.factRepairOrder b on a.ronumber = b.ro
LEFT JOIN dds.dimServiceWriter c on b.ServiceWriterKey = c.ServiceWriterKey
WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
  AND TransactionType = 'Service'
GROUP BY DealerName, vin, ronumber, TransactionType, c.censusdept


SELECT dealername, censusdept,
  cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  CASE -- divide BY 0 prevention
    WHEN SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) = 0 THEN '0%'
    ELSE 
    trim(CAST( 
      -- Promoters
      SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
        100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
        THEN 1 ELSE 0 END) 
      -    
      -- Detractors
      SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
        THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
        THEN 1 ELSE 0 END) AS sql_char)) + '%' 
   END AS NetPromoterScore
FROM (
  SELECT DealerName, vin, ronumber, TransactionType, 
    MAX(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
    MAX(ReferralLikelihood) AS ReferralLikelihood, c.censusdept
  FROM xfmDigitalAirStrike a
  LEFT JOIN dds.factRepairOrder b on a.ronumber = b.ro
  LEFT JOIN dds.dimServiceWriter c on b.ServiceWriterKey = c.ServiceWriterKey
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90  
    AND TransactionType = 'Service'
  GROUP BY DealerName, vin, ronumber, TransactionType, c.censusdept) x   
GROUP BY dealername, censusdept  

-- 10/24 ---------------------------------------------------------------------
change it up a bit, 
GetDepartmentScores: returns only dept level
GetDepartmentPeopleScores returns people for a dept

CREATE TABLE npDepartments (
  Department cichar(12)) IN database;
INSERT INTO npDepartments values ('Sales');
INSERT INTO npDepartments values ('Service');
INSERT INTO npDepartments values ('Main Shop');
INSERT INTO npDepartments values ('PDQ');
INSERT INTO npDepartments values ('Body Shop');

-- ok, dept & dept people are ALL cool,
-- need to DO surveys
  
for surveys
need 
  sc/writer
  date (sent)
  ro/stk#
  customer
  referral likelihood
  text
  
the biggie, of course IS market surveys  

SELECT a.*, b.CustomerExperience
FROM (
  SELECT DealerName, vin, ronumber, TransactionType, 
    MAX(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
    MAX(ReferralLikelihood) AS ReferralLikelihood
  -- SELECT *
  FROM xfmDigitalAirStrike
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90
  GROUP BY DealerName, vin, ronumber, TransactionType) a
LEFT JOIN xfmDigitalAirStrike b on a.Dealername = b.Dealername AND a.vin = b.vin
  AND a.ronumber = b.ronumber AND a.SurveySentTS = b.SurveySentTS
LEFT JOIN (
  SELECT bmco#,bmvin, bmstk#, max(b.bqname) AS bqname
  FROM dds.stgArkonaBOPMAST a
  LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
    AND b.bqtype = 'S'
--  WHERE year(bmdtcap) = 2013  
  WHERE bmdtcap BETWEEN curdate() - 90 AND curdate()
    AND TRIM(bmvin) <> '' 
  GROUP BY bmco#,bmvin, bmstk#) c on a.vin = c.bmvin AND iif(a.DealerName = 'Honda of Grand Forks', 'RY2', 'RY1') = c.bmco#     
LEFT JOIN (
  SELECT a.ro, max(b.name) AS name, max(b.censusdept) AS censusdept
  FROM dds.factRepairOrder a
  INNER JOIN dds.dimServiceWriter b on a.ServiceWriterKey = b.ServiceWriterKey
  WHERE a.ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike
    WHERE CAST(surveysentts AS sql_date) > curdate() - 90)
  GROUP BY a.RO) d on a.ronumber = d.ro
  
-- fucking bingo? 
SELECT 
  CASE a.TransactionType
    WHEN 'Sales' THEN c.bqname
    WHEN 'Service' THEN d.name
  END AS Employee, 
  cast(a.SurveySentTS AS sql_date) AS Date,
  CASE a.TransactionType
    WHEN 'Sales' THEN c.bmstk#
    WHEN 'Service' THEN a.ronumber
  END AS "RO/Stock",
  b.CustomerName, a.ReferralLikelihood, b.CustomerExperience
FROM (
  SELECT DealerName, vin, ronumber, TransactionType, 
    MAX(SurveySentTS) AS SurveySentTS, MAX(SurveyResponseTS) AS SurveyResponseTS,
    MAX(ReferralLikelihood) AS ReferralLikelihood
  -- SELECT *
  FROM xfmDigitalAirStrike
  WHERE CAST(surveysentts AS sql_date) > curdate() - 90
    AND year(CAST(surveyresponsets AS sql_date)) <> 1969
  GROUP BY DealerName, vin, ronumber, TransactionType) a
LEFT JOIN xfmDigitalAirStrike b on a.Dealername = b.Dealername AND a.vin = b.vin
  AND a.ronumber = b.ronumber AND a.SurveySentTS = b.SurveySentTS
LEFT JOIN (
  SELECT bmco#,bmvin, bmstk#, max(b.bqname) AS bqname
  FROM dds.stgArkonaBOPMAST a
  LEFT JOIN dds.stgArkonabopslss b on a.bmco# = b.bqco# AND a.bmkey = b.bqkey
    AND b.bqtype = 'S'
--  WHERE year(bmdtcap) = 2013  
  WHERE bmdtcap BETWEEN curdate() - 90 AND curdate()
    AND TRIM(bmvin) <> '' 
  GROUP BY bmco#,bmvin, bmstk#) c on a.vin = c.bmvin AND iif(a.DealerName = 'Honda of Grand Forks', 'RY2', 'RY1') = c.bmco#     
LEFT JOIN (
  SELECT a.ro, max(b.name) AS name, max(b.censusdept) AS censusdept
  FROM dds.factRepairOrder a
  INNER JOIN dds.dimServiceWriter b on a.ServiceWriterKey = b.ServiceWriterKey
  WHERE a.ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike
    WHERE CAST(surveysentts AS sql_date) > curdate() - 90
      AND year(CAST(surveyresponsets AS sql_date)) <> 1969)
  GROUP BY a.RO) d on a.ronumber = d.ro  

-- 10/25
employee surveys, clicking some of the results IN no data  