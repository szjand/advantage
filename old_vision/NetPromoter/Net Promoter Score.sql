SELECT * FROM sco.servicewriters
  
-- 9/9
modify the netpromoter score based on the article FROM fredrick reichheld

Extremely                 Neutral         Not Likely At All
Likely

-- 10 -- 9 -- 8 --- 7 -- 6 -- 5 -- 4 -- 3 -- 2 -- 1 --
-- |-----|----------|-----------------------------|
Promoters  Passively       Detractors
           Satisfied  
           
Promoter % (> 8) - Detractor % (< 7) = Net Promoter Score   
-- this works
SELECT 'Dept' AS "Dept/Writer", 'Main Shop' AS "Area/Writer", cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST(
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore 
FROM xfmDigitalAirStrike a
INNER JOIN (
  select ro, servicewriterkey
  from dds.factRepairOrder
  WHERE ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike)
  GROUP BY ro, servicewriterkey) b on a.ronumber = b.ro
INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.servicewriterkey
  AND c.censusdept = 'MR'
  AND CAST(surveysentts AS sql_date) BETWEEN c.servicewriterkeyfromdate AND c.servicewriterkeythrudate
WHERE a.transactionType = 'Service'
  AND cast(a.surveysentts AS sql_Date) > curdate() - 90       
  
ALTER PROCEDURE GetNetPromoterScores (
  DeptWriter cichar(12) output,
  AreaWriter cichar(30) output,
  Sent cichar(12) output,
  Returned cichar(12) output,
  ResponseRate cichar(12) output,
  NetPromoterScore cichar(12) output)
BEGIN
/*
EXECUTE PROCEDURE GetNetPromoterScores();

modify the netpromoter score based on the article FROM fredrick reichheld

Extremely                 Neutral         Not Likely At All
Likely

-- 10 -- 9 -- 8 --- 7 -- 6 -- 5 -- 4 -- 3 -- 2 -- 1 --
-- |-----|----------|-----------------------------|
Promoters  Passively       Detractors
           Satisfied  
           
Promoter % (> 8) - Detractor % (< 7) = Net Promoter Score   


*/  
INSERT INTO __output
SELECT 'Dept' AS "Dept/Writer", 'Main Shop' AS "Area/Writer", cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST( 
    -- Promoters
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore 
FROM xfmDigitalAirStrike a
INNER JOIN (
  select ro, servicewriterkey
  from dds.factRepairOrder
  WHERE ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike)
  GROUP BY ro, servicewriterkey) b on a.ronumber = b.ro
INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.servicewriterkey
  AND c.censusdept = 'MR'
  AND CAST(surveysentts AS sql_date) BETWEEN c.servicewriterkeyfromdate AND c.servicewriterkeythrudate
WHERE a.transactionType = 'Service'
  AND cast(a.surveysentts AS sql_Date) > curdate() - 90
UNION 
SELECT 'Writer' AS "Area/Writer", c.name, cast(COUNT(*) AS sql_char)  AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST(
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore 
FROM xfmDigitalAirStrike a
INNER JOIN (
  select ro, servicewriterkey
  from dds.factRepairOrder
  WHERE ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike)
  GROUP BY ro, servicewriterkey) b on a.ronumber = b.ro
INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.servicewriterkey
  AND c.censusdept = 'MR'
  AND CAST(surveysentts AS sql_date) BETWEEN c.servicewriterkeyfromdate AND c.servicewriterkeythrudate
WHERE a.transactionType = 'Service'
  AND cast(a.surveysentts AS sql_Date) > curdate() - 90  
GROUP BY c.name;
END;   