--select * FROM xfmdigitalairstrike

-- thinking cust name/email FROM net promoter
DROP TABLE cstfbNpData;
CREATE TABLE cstfbNpData (
  transactionDate date constraint NOT NULL,
  customerResponse logical constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  subDepartment cichar(12) constraint NOT NULL, -- none/mainshop/pdq/bodyshop 
  employeeName cichar(52) constraint NOT NULL, 
  transactionNumber cichar(9) constraint NOT NULL, -- stock#/ro
  transactionType cichar(14) constraint NOT NULL, -- sales/service
  customerName cichar(50) constraint NOT NULL,
  referralLikelihood integer constraint NOT NULL,
  customerExperience memo,
  constraint PK Primary Key (customerName, transactionNumber, transactionDate)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpData','cstfbNpData.adi','transactionDate','transactionDate',
   '',2,512, '' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpData','cstfbNpData.adi','storeCode','storeCode',
   '',2,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpData','cstfbNpData.adi','subDepartment','subDepartment',
   '',2,512, '' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpData','cstfbNpData.adi','employeeName','employeeName',
   '',2,512, '' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpData','cstfbNpData.adi','transactionType','transactionType',
   '',2,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpData','cstfbNpData.adi','transactionNumber','transactionNumber',
   '',2,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'cstfbNpData','cstfbNpData.adi','customerResponse','customerResponse',
   '',2,512, '' );   
   
SELECT CAST(surveySentTs AS sql_date) AS transactionDate,  
  CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN true ELSE false END AS customerResponse,
  CASE dealername
    WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
    ELSE 'RY2'
  END AS storeCode,
  'subDepartment', 'employeename',
  roNumber AS transactionNumber, vin, transactionType, customerName,
  referralLikelihood, customerExperience
--INTO #wtf  
FROM xfmDigitalAirStrike  
ORDER BY transactiondate;

-- ok, just the vin IS NOT good enuf, people have bought same vehicle 
--    multiple times (lease returns) 


DO NOT want to go too far down the mind fuck path, but the key issue IN assuring
unique rows IN cstfbNpData IS
  xfmDigitalAirStrike has vin for sales, NOT stocknumber
shit LIKE casing customerrepsonse IN the WHERE clause AND returning just the row 
  with a response, which handles exactly 1 CASE IN the above dup rows
  
ok, DO an INNER JOIN on vin, customer, transactiontyp WHERE date = MAX grouped datte

SELECT max(CAST(surveySentTS AS sql_date)) AS theDate, customerName, vin, transactionType
FROM xfmDigitalAirStrike
GROUP BY customerName, vin, transactionType

-- ok, this IS feeling better
SELECT customername, transactionnumber, vin FROM (
SELECT CAST(surveySentTs AS sql_date) AS transactionDate,  
  CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN true ELSE false END AS customerResponse,
  CASE dealername
    WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
    ELSE 'RY2'
  END AS storeCode,
  'subDepartment', 'employeename',
  roNumber AS transactionNumber, a.vin, a.transactionType, a.customerName,
  referralLikelihood, customerExperience
FROM xfmDigitalAirStrike a
INNER JOIN (
    SELECT max(CAST(surveySentTS AS sql_date)) AS theDate, customerName, vin, transactionType
    FROM xfmDigitalAirStrike
    GROUP BY customerName, vin, transactionType) b on CAST(a.surveySentTS AS sql_date) = b.theDate
  AND a.customerName = b.customerName
  AND a.transactionType = b.transactionType
  AND a.vin = b.vin
) x GROUP BY customername, transactionnumber, vin HAVING COUNT(*) > 1

-- DROP TABLE #wtf;
SELECT CAST(surveySentTs AS sql_date) AS transactionDate,  
  CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN true ELSE false END AS customerResponse,
  CASE dealername
    WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
    ELSE 'RY2'
  END AS storeCode,
  'subDepartment', 'employeename',
  roNumber AS transactionNumber, a.vin, a.transactionType, a.customerName,
  referralLikelihood, customerExperience
INTO #wtf 
FROM xfmDigitalAirStrike a
INNER JOIN ( -- remove duplicate surveys
  SELECT max(CAST(surveySentTS AS sql_date)) AS theDate, customerName, vin, transactionType
  FROM xfmDigitalAirStrike
  WHERE transactionType = 'sales'
  GROUP BY customerName, vin, transactionType) b 
    on CAST(a.surveySentTS AS sql_date) = b.theDate
      AND a.customerName = b.customerName
      AND a.transactionType = b.transactionType
      AND a.vin = b.vin
WHERE a.transactionType = 'Sales'  

DROP TABLE #wtf1; 
select *
INTO #wtf1
FROM #wtf a
LEFT JOIN (
    SELECT b.stocknumber, c.vin, d.fullname, dd.thedate AS capDate, ddd.thedate AS appDate, dddd.thedate AS origDate
    FROM dds.factVehicleSale b
    INNER JOIN dds.dimVehicle c on b.vehicleKey = c.vehicleKey
    INNER JOIN dds.dimSalesPerson d on b.consultantKey = d.salesPersonKey
    INNER JOIN dds.day dd on b.cappedDateKey = dd.datekey
    INNER JOIN dds.day ddd on b.approveddatekey = ddd.datekey
    INNER JOIN dds.day dddd on b.originationdatekey = dddd.datekey) e on a.vin = e.vin
  AND (ABS(a.transactionDate - e.capdate) < 14  
    OR ABS(a.transactiondate - e.appdate) < 14 
    OR ABS(a.transactiondate - e.origdate) < 14
    OR a.transactionDate = '02/08/2014')  

-- this IS good
SELECT stocknumber, vin, customername
FROM #wtf1
GROUP BY stocknumber, vin, customername
HAVING COUNT(*) > 1

-- this IS NOT good
-- a handfull of deals with transactionDate = 02/08/14, wtf
-- guessing this IS some catchup stuff FROM ry1 getting back on the program,
-- adding 2/8/14 to the WHERE clause got this down to 11, good enuf
SELECT *
-- SELECT COUNT(*)
FROM #wtf1 
WHERE fullname IS NULL 

DELETE FROM #wtf1
WHERE fullname IS NULL;

DELETE FROM cstfbNpData;
INSERT INTO cstfbNpData
SELECT m.transactionDate, m.customerResponse, m.storeCode, 'None', n.fullname,
  n.stocknumber, 'Sales', m.customerName, m.referralLikelihood,
  m.customerExperience
FROM (    
  SELECT CAST(surveySentTs AS sql_date) AS transactionDate,  
    CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN true ELSE false END AS customerResponse,
    CASE dealername
      WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
      ELSE 'RY2'
    END AS storeCode,
    roNumber AS transactionNumber, a.vin, a.transactionType, a.customerName,
    referralLikelihood, customerExperience
  FROM xfmDigitalAirStrike a
  INNER JOIN ( -- remove duplicate surveys
    SELECT max(CAST(surveySentTS AS sql_date)) AS theDate, customerName, vin, transactionType
    FROM xfmDigitalAirStrike
    WHERE transactionType = 'sales'
    GROUP BY customerName, vin, transactionType) b 
      on CAST(a.surveySentTS AS sql_date) = b.theDate
        AND a.customerName = b.customerName
        AND a.transactionType = b.transactionType
        AND a.vin = b.vin
  WHERE a.transactionType = 'Sales') m  
LEFT JOIN (
  SELECT b.stocknumber, c.vin, d.fullname, dd.thedate AS capDate, ddd.thedate AS appDate, dddd.thedate AS origDate
  FROM dds.factVehicleSale b
  INNER JOIN dds.dimVehicle c on b.vehicleKey = c.vehicleKey
  INNER JOIN dds.dimSalesPerson d on b.consultantKey = d.salesPersonKey
  INNER JOIN dds.day dd on b.cappedDateKey = dd.datekey
  INNER JOIN dds.day ddd on b.approveddatekey = ddd.datekey
  INNER JOIN dds.day dddd on b.originationdatekey = dddd.datekey) n 
    on m.vin = n.vin
      AND (ABS(m.transactionDate - n.capdate) < 14  
        OR ABS(m.transactiondate - n.appdate) < 14 
        OR ABS(m.transactiondate - n.origdate) < 14
        OR m.transactionDate = '02/08/2014')
WHERE n.fullname IS NOT NULL;           

-- service
-- DROP TABLE #wtf;
--only 1 dup ro, so just DO NOT even bother, eliminate them
DROP TABLE #wtf;
SELECT CAST(surveySentTs AS sql_date) AS transactionDate,  
  CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN true ELSE false END AS customerResponse,
  CASE dealername
    WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
    ELSE 'RY2'
  END AS storeCode,
  'subDepartment', 'employeename',
  roNumber AS transactionNumber, a.vin, a.transactionType, a.customerName,
  referralLikelihood, customerExperience
INTO #wtf 
FROM xfmDigitalAirStrike a
WHERE a.transactiontype = 'service'
  AND roNumber NOT IN ( -- eliminate dup ro
    SELECT roNumber
    FROM xfmdigitalairstrike
    GROUP BY roNumber
    HAVING COUNT(*) > 1);

DROP TABLE #wtf1;    
SELECT *
INTO #wtf1
FROM #wtf a
LEFT JOIN (
  SELECT distinct b.ro, c.censusDept, TRIM(d.firstname) + ' ' + TRIM(d.lastname) AS employee
  FROM dds.factRepairOrder b
  INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.serviceWriterKey
  LEFT JOIN dds.edwEmployeeDim d 
--    on c.storecode = d.storecode -- can't use store, writers may NOT be emp of the store
    ON c.employeenumber = d.employeenumber 
    AND d.currentrow = true 
  WHERE b.ro IN (
    SELECT transactionNumber
    FROM #wtf)) e on a.transactionNumber = e.ro;
-- good    
SELECT transactionNumber
FROM #wtf1
GROUP BY transactionNumber
HAVING COUNT(*) > 1    
-- good enuf, a few, mostly ALL cashier
-- coalesce writer name to other
select *
FROM #wtf1
WHERE employee IS NULL 
ORDER BY ro

 
-- cstfbNpData ----------------------------------------------------------------
DELETE FROM cstfbNpData;
-- sales
INSERT INTO cstfbNpData
SELECT m.transactionDate, m.customerResponse, m.storeCode, 'None', n.fullname,
  n.stocknumber, 'Sales', m.customerName, m.referralLikelihood,
  m.customerExperience
FROM (    
  SELECT CAST(surveySentTs AS sql_date) AS transactionDate,  
    CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN true 
      ELSE false END AS customerResponse,
    CASE dealername
      WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
      ELSE 'RY2'
    END AS storeCode,
    roNumber AS transactionNumber, a.vin, a.transactionType, a.customerName,
    referralLikelihood, customerExperience
  FROM xfmDigitalAirStrike a
  INNER JOIN ( -- remove duplicate surveys
    SELECT max(CAST(surveySentTS AS sql_date)) AS theDate, customerName, vin, 
      transactionType
    FROM xfmDigitalAirStrike
    WHERE transactionType = 'sales'
    GROUP BY customerName, vin, transactionType) b 
      on CAST(a.surveySentTS AS sql_date) = b.theDate
        AND a.customerName = b.customerName
        AND a.transactionType = b.transactionType
        AND a.vin = b.vin
  WHERE a.transactionType = 'Sales') m  
LEFT JOIN (
  SELECT b.stocknumber, c.vin, d.fullname, dd.thedate AS capDate, 
    ddd.thedate AS appDate, dddd.thedate AS origDate
  FROM dds.factVehicleSale b
  INNER JOIN dds.dimVehicle c on b.vehicleKey = c.vehicleKey
  INNER JOIN dds.dimSalesPerson d on b.consultantKey = d.salesPersonKey
  INNER JOIN dds.day dd on b.cappedDateKey = dd.datekey
  INNER JOIN dds.day ddd on b.approveddatekey = ddd.datekey
  INNER JOIN dds.day dddd on b.originationdatekey = dddd.datekey) n 
    on m.vin = n.vin
      AND (ABS(m.transactionDate - n.capdate) < 14  
        OR ABS(m.transactiondate - n.appdate) < 14 
        OR ABS(m.transactiondate - n.origdate) < 14
        OR m.transactionDate = '02/08/2014')
WHERE n.fullname IS NOT NULL;    
-- service  
INSERT INTO cstfbNpData    
SELECT m.transactionDate, m.customerResponse, m.storeCode, 
  coalesce(n.censusDept, 'XX'), 
  coalesce(n.fullname, 'Other'), m.transactionNumber, 'Service', 
  m.customerName, m.referralLikelihood, m.customerExperience
FROM (   
  SELECT CAST(surveySentTs AS sql_date) AS transactionDate,  
    CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN true 
      ELSE false END AS customerResponse,
    CASE dealername
      WHEN 'Rydell Chevrolet Buick GMC Cadillac' THEN 'RY1'
      ELSE 'RY2'
    END AS storeCode,
    'subDepartment', 'employeename',
    left(roNumber, 9) AS transactionNumber, a.vin, a.transactionType, a.customerName,
    referralLikelihood, customerExperience
  FROM xfmDigitalAirStrike a
  WHERE a.transactiontype = 'service'
    AND roNumber NOT IN ( -- eliminate dup ro
      SELECT roNumber
      FROM xfmdigitalairstrike
      GROUP BY roNumber
      HAVING COUNT(*) > 1)) m
LEFT JOIN (
  SELECT distinct b.ro, c.censusDept, TRIM(d.firstname) + ' ' + TRIM(d.lastname) AS fullName
  FROM dds.factRepairOrder b
  INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.serviceWriterKey
  LEFT JOIN dds.edwEmployeeDim d 
--    on c.storecode = d.storecode -- can't use store, writers may NOT be emp of the store
    ON c.employeenumber = d.employeenumber 
    AND d.currentrow = true 
  WHERE b.ro IN (
    SELECT ronumber
    FROM xfmdigitalairstrike )) n on m.transactionNumber = n.ro;