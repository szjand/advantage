SELECT vin, ronumber, surveysentts, transactiontype, surveyresponsets,
  referrallikelihood, customerexperience, salespersonname, c.censusdept
--SELECT *  


SELECT c.censusdept, cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST( 
    -- Promoters
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore        
FROM xfmDigitalAirStrike a
INNER JOIN (
  select ro, servicewriterkey
  from dds.factRepairOrder
  WHERE ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike)
  GROUP BY ro, servicewriterkey) b on a.ronumber = b.ro
INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.servicewriterkey
WHERE cast(a.surveysentts AS sql_Date) > curdate() - 90 
GROUP BY c.censusdept
UNION 

SELECT 'Sales', cast(COUNT(*) AS sql_char) AS Sent,
  cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END) AS sql_char) AS Returned,
  trim(cast(SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 THEN 1 ELSE 0 END)*100/COUNT(*) AS sql_char)) + '%' AS ResponseRate,
  trim(CAST( 
    -- Promoters
    SUM(CASE WHEN ReferralLikelihood > 8 THEN 1 ELSE 0 END) * 
      100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) 
    -    
    -- Detractors
    SUM(CASE WHEN ReferralLikelihood < 7 AND year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) * 100/SUM(CASE WHEN year(CAST(surveyresponsets AS sql_date)) <> 1969 
      THEN 1 ELSE 0 END) AS sql_char)) + '%' AS NetPromoterScore
FROM xfmDigitalAirStrike a
WHERE cast(a.surveysentts AS sql_Date) > curdate() - 90 

-- what IS up with the XX
SELECT vin, ronumber, surveysentts, transactiontype, surveyresponsets,
  referrallikelihood, customerexperience, salespersonname, c.censusdept
FROM xfmDigitalAirStrike a
INNER JOIN (
  select ro, servicewriterkey
  from dds.factRepairOrder
  WHERE ro IN (
    SELECT ronumber
    FROM xfmDigitalAirStrike)
  GROUP BY ro, servicewriterkey) b on a.ronumber = b.ro
INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.servicewriterkey
WHERE cast(a.surveysentts AS sql_Date) > curdate() - 90 
  AND c.censusdept = 'XX' 
