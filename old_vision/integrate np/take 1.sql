/*

adding department to roles IS enticing
SELECT * FROM zdepartments 
it will take care of the al berry question, eg dept IS ry1-fixed-mainshop-drive 

*/

applications: netpromoter
roles: writer
applicationRoles:
  np/manager
  np/writer
ApplicationMetaData:
  row for manager/writer: summary, surveys, about
Greg already added ALL the above except roles
INSERT INTO roles values('writer');  

issues:
-  current NP stored procs are run against a connection to the NetPromoter db
   which has a link to sco
   that will NOT WORK, because we are developing against scotest
-  current NP stored procs are USING the PositionFulfillment/people/eeUserName
   construct FROM sco, that will NOT WORK
   
need to generate new stored procs FROM sco(test) utilizing the tpEmployees/
ApplicationRoles/EmployeeAppRoles construct
1. need writers IN tpEmployees

DROP TABLE #wtf;
SELECT eeusername AS userName, space(25) AS firstname, space(25) AS lastname, space(9) AS employeenumber, 
  password, membergroup, storecode
INTO #wtf
FROM People a
WHERE membergroup = 'ServiceWriter';

UPDATE #wtf manually with values FROM edwEmployeeDim 

INSERT INTO tpEmployees
SELECT * FROM #wtf

2. need EmployeeAppRoles
INSERT INTO EmployeeAppRoles (username, appname, role, fromdate)
SELECT a.username, 'netpromoter', 'writer', '09/08/2013'
FROM tpEmployees a
LEFT JOIN EmployeeAppRoles b on a.username = b.username
WHERE b.username IS NULL 



/*
SELECT *
FROM people a
LEFT JOIN positionfulfillment b on a.eeusername = b.eeusername
WHERE membergroup = 'ServiceWriter'

remove wiebusch, he was NOT a main shop writer since
delete FROM tpemployees WHERE lastname LIKE 'wie%'
*/

--< GetNetPromoterSurveys -----------------------------------------------------< NP GetNetPromoterSurveys
need to ADD a link to np
EXECUTE PROCEDURE 
  sp_CreateLink ( 
     'np',
     '\\96.3.202.12:6363\advantage\netpromoter\netpromoter.add',
     TRUE, TRUE, TRUE, '', '');
     
SELECT * FROM dds.dimservicewriter     
SELECT * FROM dds.dimtech
     
SELECT ronumber
FROM np.xfmdigitalairstrike
GROUP BY ronumber HAVING COUNT(*) > 1 

SELECT * from np.xfmdigitalairstrike WHERE ronumber = '16129019'

alter PROCEDURE GetNpSurveys( 
      sessionID CICHAR ( 50 ),
      WriterName CICHAR ( 52 ) OUTPUT,
      theDate date OUTPUT,
      Ro CICHAR ( 12 ) OUTPUT,
      CustomerName CICHAR ( 50 ) OUTPUT,
      ReferralLikelihood integer OUTPUT,
      CustomerExperience Memo OUTPUT
   ) 
BEGIN
/*
execute procedure GetNpSurveys('[fb927d8b-aab3-9740-9451-d0d08d380936]');
*/   
DECLARE @sessionID string;
DECLARE @userName string;
DECLARE @role string;
DECLARE @app string;
@role = 'writer';
@app = 'netpromoter';
@sessionID = (SELECT sessionID FROM __input);
@userName = (SELECT username FROM sessions WHERE sessionid = @sessionID);
INSERT INTO __output
SELECT d.fullName, cast(a.SurveySentTS as sql_date), a.ronumber,   
  a.CustomerName, a.ReferralLikelihood, a.CustomerExperience
FROM np.xfmDigitalAirStrike a   
INNER JOIN (
  SELECT m.ro, m.servicewriterkey
  FROM dds.factRepairorder m
  INNER JOIN np.xfmdigitalairstrike n on m.ro = n.ronumber
  WHERE n.transactionType = 'Service'
    AND cast(n.surveysentts AS sql_Date) > curdate() - 90
    AND year(surveyresponsets) <> 1969
    AND n.DealerName = 'Rydell Chevrolet Buick GMC Cadillac'
  GROUP BY m.ro, m.servicewriterkey) b on a.ronumber = b.ro 
INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.servicewriterkey
INNER JOIN tpEmployees d on c.employeenumber = d.employeenumber
WHERE c.censusdept = 'MR'  
  AND 
    CASE 
      WHEN EXISTS (
        SELECT 1
        FROM EmployeeAppRoles
        WHERE username = @userName
          AND appName = @app
          AND role = @role) THEN d.fullName = (
            SELECT fullName
            FROM tpEmployees
            WHERE username = @userName)
      ELSE 1 = 1
    END;
END;    


--/> GetNetPromoterSurveys ----------------------------------------------------/> GetNetPromoterSurveys
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--< GetNPSummary --------------------------------------------------------------< GetNPSummary

CREATE PROCEDURE GetNpSummary( 
      sessionID CICHAR ( 50 ),
      sent integer output,
      returned integer output,
      responseRate double output,
      score double output) 
BEGIN
/*
execute procedure GetNpSummary('[fb927d8b-aab3-9740-9451-d0d08d380936]');
execute procedure GetNpSummary('[c2ae52e6-7917-ce4e-b6b1-56db32f7645b]');
*/   
DECLARE @sessionID string;
DECLARE @userName string;
DECLARE @role string;
DECLARE @app string;
@role = 'writer';
@app = 'netpromoter';
@sessionID = (SELECT sessionID FROM __input);
@userName = (SELECT username FROM sessions WHERE sessionid = @sessionID);
IF EXISTS (
  SELECT 1
  FROM EmployeeAppRoles
  WHERE username = @userName
    AND appName = @app
    AND role = @role) 
THEN INSERT INTO __output    
  SELECT sent, returned, round(1.0*returned/sent, 4) AS ResponseRate,
    round(1.0*promoters/returned, 4) - round(1.0*detractors/returned, 4) AS npScore 
  FROM (
    SELECT COUNT(*) AS sent,
      SUM(CASE WHEN year(surveyresponsets) <> 1969 THEN 1 ELSE 0 END) AS Returned,
      SUM(CASE WHEN ReferralLikelihood > 8 AND year(surveyresponsets) <> 1969 THEN 1 ELSE 0 END) AS promoters,
      SUM(CASE WHEN ReferralLikelihood < 7 AND year(surveyresponsets) <> 1969 THEN 1 ELSE 0 END) AS detractors
    FROM np.xfmDigitalAirStrike a   
    INNER JOIN (
      SELECT distinct m.ro, m.servicewriterkey
      FROM dds.factRepairorder m
      INNER JOIN np.xfmdigitalairstrike n on m.ro = n.ronumber
      WHERE n.transactionType = 'Service'
        AND cast(n.surveysentts AS sql_Date) > curdate() - 90
        AND n.DealerName = 'Rydell Chevrolet Buick GMC Cadillac') b on a.ronumber = b.ro   
    INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.servicewriterkey
    INNER JOIN tpEmployees d on c.employeenumber = d.employeenumber
    WHERE c.censusdept = 'MR'
      AND d.username = @userName) x;
ELSE INSERT INTO __output      
  SELECT sent, returned, round(1.0*returned/sent, 4) AS ResponseRate,
    round(1.0*promoters/returned, 4) - round(1.0*detractors/returned, 4) AS npScore 
  FROM (
    SELECT COUNT(*) AS sent,
      SUM(CASE WHEN year(surveyresponsets) <> 1969 THEN 1 ELSE 0 END) AS Returned,
      SUM(CASE WHEN ReferralLikelihood > 8 AND year(surveyresponsets) <> 1969 THEN 1 ELSE 0 END) AS promoters,
      SUM(CASE WHEN ReferralLikelihood < 7 AND year(surveyresponsets) <> 1969 THEN 1 ELSE 0 END) AS detractors
    FROM np.xfmDigitalAirStrike a   
    INNER JOIN (
      SELECT distinct m.ro, m.servicewriterkey
      FROM dds.factRepairorder m
      INNER JOIN np.xfmdigitalairstrike n on m.ro = n.ronumber
      WHERE n.transactionType = 'Service'
        AND cast(n.surveysentts AS sql_Date) > curdate() - 90
        AND n.DealerName = 'Rydell Chevrolet Buick GMC Cadillac') b on a.ronumber = b.ro   
    INNER JOIN dds.dimServiceWriter c on b.servicewriterkey = c.servicewriterkey
    INNER JOIN tpEmployees d on c.employeenumber = d.employeenumber
    WHERE c.censusdept = 'MR') x;
ENDIF;
END;    
--/> GetNPSummary -------------------------------------------------------------/> GetNPSummary
