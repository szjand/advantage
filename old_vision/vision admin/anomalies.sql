-- no tpemployee row for ptoemployee
SELECT *
FROM ptoemployees a
WHERE NOT EXISTS (
  SELECT 1
  FROM tpemployees
  WHERE employeenumber = a.employeenumber)