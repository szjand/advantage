-- 12/2/15
-- need to CREATE a TABLE to store pto stats at termination
-- Melissa Lawson was termed from Honda Sales as of 11/28/2015. 
-- tpEmployees, employeeAppAuthorization, ptoEmployees
-- ri tpEmployees->employeeAppAuthorization

-- need to figure pto balance: allocation  + adjustment - used, i think
-- DROP TABLE pto_term_history;
/*
CREATE TABLE pto_term_history (
  employee_number cichar(7),
  term_date date,
  pto_allocation double,
  pto_adjustment double,
  pto_used double,
  from_date date,
  thru_date date,
  balance double) IN database;
*/  


DECLARE @user_name string;
DECLARE @employee_number string;
DECLARE @first_name string;
DECLARE @last_name string;
DECLARE @term_date date;
DECLARE @party_id string;
-- select * FROM dps.users WHERE username LIKE '%bati%'
-- SELECT * FROM tpemployees WHERE lastname = 'cespedes' AND firstname = 'dale'
@first_name = 'kierra';
@last_name = 'batiste';
@term_date = '11/30/2015';
@party_id = '959bf32d-6a0d-ed46-8abe-2eca06ebed7c';
@user_name = (SELECT username FROM tpemployees WHERE lastname = @last_name AND firstname = @first_name);
@employee_number = (SELECT employeenumber FROM tpemployees WHERE lastname = @last_name AND firstname = @first_name);
BEGIN TRANSACTION;
TRY

  INSERT INTO pto_term_history
  SELECT a.employeenumber, @term_date, a.hours AS allocated, 
    coalesce(b.hours, 0) AS adjusted, 
    coalesce(SUM(c.hours), 0) AS used,
    a.fromdate, a.thrudate, 
    coalesce(a.hours, 0) + coalesce(b.hours, 0) - coalesce(SUM(c.hours), 0) AS balance 
  FROM pto_employee_pto_allocation a
  LEFT JOIN pto_adjustments b on a.employeenumber = b.employeenumber
    AND b.fromdate = a.fromdate
  LEFT JOIN pto_used c on a.employeenumber = c.employeenumber   
    AND thedate BETWEEN a.fromdate AND a.thrudate
  WHERE a.employeenumber = @employee_number
    AND @term_date BETWEEN a.fromdate AND a.thrudate
  GROUP BY a.employeenumber, a.hours, b.hours, a.fromdate, a.thrudate;
  
  DELETE 
  -- SELECT *
  FROM employeeAppAuthorization
  WHERE username = @user_name;
  DELETE 
  -- SELECT *
  FROM tpEmployees
  WHERE username = @user_name;
  DELETE 
  -- SELECT *
  FROM ptoEmployees
  WHERE employeenumber = @employee_number;
  
  UPDATE dps.partyRelationships
  SET thruts = now()
  WHERE partyid2 = @party_id
    AND thruts IS null;
    
  UPDATE dps.partyprivileges 
  SET thruts = now()
  WHERE partyid = @party_id
    AND thruts IS NULL;
    
  UPDATE dps.applicationusers
  SET thruts = now()
  WHERE partyid = @party_id
    AND thruts IS NULL;
  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   

SELECT * FROM pto_term_history;
  
  