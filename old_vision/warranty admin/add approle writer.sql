

INSERT INTO applicationRoles (appName, role, appcode)
values('warrantyAdmin','writer','warad');

INSERT INTO applicationMetadata(appname,appcode,appseq,approle,functionality,
  configmapmarkupkey,pubsub,navtype,navtext,navseq,id,links)
SELECT appname,appcode,appseq,'writer',functionality,
  configmapmarkupkey,pubsub,navtype,navtext,navseq,id,links
FROM applicationMetaData
WHERE appname = 'warrantyadmin';    
 
/*
SELECT DISTINCT username
FROM employeeAppAuthorization
WHERE appname = 'warrantyAdmin'
*/

UPDATE employeeAppAuthorization
SET approle = 'writer'
WHERE appname = 'warrantyAdmin'
  AND username IN ('aneumann@rydellcars.com','kespelund@rydellchev.com','mhuot@rydellchev.com');
  
  


alter PROCEDURE getApplicationNavigation
   ( 
      sessionId CICHAR ( 50 ),
      appCode CICHAR ( 6 ),
      appCode CICHAR ( 6 ) OUTPUT,
      configMapMarkupKey CICHAR ( 50 ) OUTPUT,
      navType CICHAR ( 50 ) OUTPUT,
      pubSub CICHAR ( 50 ) OUTPUT,
      applicationDept CICHAR ( 24 ) OUTPUT,
      appSeq Integer OUTPUT,
      appRole cichar(25) output
   ) 
BEGIN 
/*
1/24/14
  replaced employeeAppRoles & applicationRoles with employeeAppAuthorization
2/19  
  added appseq & applicationDept: hacking to get body shop working
11/8/14
  ADD appRole to output
EXECUTE PROCEDURE getApplicationNavigation ('[d27d66d2-13ea-5547-90e1-4329ec6a863c]','ALL')

EXECUTE PROCEDURE getApplicationNavigation (
(SELECT top 1 sessionid 
  FROM sessions 
  WHERE username = 'mhuot@rydellchev.com' AND validthruts > now()), 'ALL')
*/
DECLARE @username string;
DECLARE @appCode string;
@appCode = (SELECT upper(appCode) FROM __input);
@username = (
  SELECT username
  FROM sessions
  WHERE sessionid = (SELECT sessionId FROM __input) 
    AND now() <= validThruTS);
INSERT INTO __output
  SELECT top 100 distinct a.appCode, a.configMapMarkupKey, a.navType, a.pubSub, 
    bb.dl4, a.appSeq, a.appRole
  FROM applicationmetadata a
  INNER JOIN employeeAppAuthorization b ON a.appname = b.appname 
    AND a.appCode = b.appCode AND a.approle = b.approle
	AND a.functionality = b.functionality
-- ADD appseq	
	AND a.appseq = b.appseq
  LEFT JOIN zdepartments bb on b.appdepartmentkey = bb.departmentkey
  INNER JOIN tpEmployees c on b.username = c.username
  WHERE c.username = @userName
    AND 
      CASE
    	  WHEN @appCode = 'ALL' THEN 1 = 1
    	  WHEN @appCode <> 'ALL' THEN a.appCode = @appCode
    	END
  ORDER BY a.appSeq, a.navSeq;    

END;
