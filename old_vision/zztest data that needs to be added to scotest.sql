/*
run against scotest, shit that gets added to zztest throughout the day
that needs to be persisted IN scotest prior to daily overwriting of zztest
*/

BEGIN TRANSACTION;
try
-- applications missing FROM scotest
INSERT INTO applications
select a.*
FROM zztest.applications a
LEFT JOIN applications b on a.appname = b.appname
  AND a.appcode = b.appcode
WHERE b.appname IS NULL;

-- approles missing FROM scotest
INSERT INTO applicationRoles
select a.*
FROM zztest.applicationRoles a
LEFT JOIN applicationRoles b on a.appname = b.appname
  AND a.appcode = b.appcode
  AND a.role = b.role
WHERE b.appname IS NULL;

-- appMetaData
INSERT INTO applicationMetaData
select a.*
FROM zztest.applicationMetaData a
LEFT JOIN applicationMetaData b on a.appname = b.appname
  AND a.appcode = b.appcode
  AND a.approle = b.approle
  AND a.functionality = b.functionality
  AND a.appSeq = b.appSeq
WHERE b.appname IS NULL;  

-- employeeAppAuthorization
INSERT INTO employeeAppAuthorization
select a.*
FROM zztest.employeeAppAuthorization a
LEFT JOIN employeeAppAuthorization b on a.appname = b.appname
  AND a.appcode = b.appcode
  AND a.approle = b.approle
  AND a.functionality = b.functionality
  AND a.appSeq = b.appSeq
  AND a.username = b.username
WHERE b.appname IS NULL;  
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  


/*
AND the other way, data to be deleted FROM scotest

select a.*
FROM applications a
LEFT JOIN zztest.applications b on a.appname = b.appname
  AND a.appcode = b.appcode
WHERE b.appname IS NULL;

select a.*
FROM applicationRoles a
LEFT JOIN zztest.applicationRoles b on a.appname = b.appname
  AND a.appcode = b.appcode
  AND a.role = b.role
WHERE b.appname IS NULL;

select a.*
FROM applicationMetaData a
LEFT JOIN zztest.applicationMetaData b on a.appname = b.appname
  AND a.appcode = b.appcode
  AND a.approle = b.approle
  AND a.functionality = b.functionality
  AND a.appSeq = b.appSeq
WHERE b.appname IS NULL;  

select a.*
FROM employeeAppAuthorization a
LEFT JOIN zztest.employeeAppAuthorization b on a.appname = b.appname
  AND a.appcode = b.appcode
  AND a.approle = b.approle
  AND a.functionality = b.functionality
  AND a.appSeq = b.appSeq
  AND a.username = b.username
WHERE b.appname IS NULL;  
*/