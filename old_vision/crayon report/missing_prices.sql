/*
greg noticed that nothing IS priced IN used_Vehicle_daily_snapshot for 1/1/16 & 1/8/16
for those 2 days, prices exist (for most vehicles) IN sco.ucinv_price_by_day,
which IS populated BY sp ucinv_update_daily

are there any other days for which there are no prices? (go to postgres AND see)
turns out we are missing prices (both stores) for 12/31/15, 1/1/16, 1/8/16,/1/14/16

gettin a letlle eager beaver here
what about date priced & days since priced & price band
need priceband IN #pr_by_day
*//
SELECT a.thedate, left(trim(b.stocknumber), 9), c.VehiclePricingTS, 
  SUM(CASE WHEN d.typ = 'VehiclePricingDetail_BestPrice' THEN cast(d.amount as sql_integer) END) AS bestPrice,
  SUM(CASE WHEN d.typ = 'VehiclePricingDetail_Invoice' THEN cast(d.amount as sql_integer) END) AS invoice,
  timestampdiff(sql_tsi_day, CAST(VehiclePricingTS AS sql_date), a.thedate) AS days
  
DROP table #pr_by_day; 
SELECT m.*, n.priceband
INTO #pr_by_day
FROM (
  SELECT a.thedate, left(trim(b.stocknumber), 9) AS stock_number, 
    cast(c.VehiclePricingTS AS sql_date) AS date_priced, 
    d.typ, d.amount,
    timestampdiff(sql_tsi_day, CAST(VehiclePricingTS AS sql_date), a.thedate) AS days_since_priced  
  --INTO #pr_by_day
  FROM dds.day a
  INNER JOIN dps.VehicleInventoryItems b on a.thedate 
      between CAST(b.fromTS AS sql_date) AND 
        cast(coalesce(thruts, CAST('12/31/9999 20:00:00' AS sql_timestamp)) AS sql_date)
    AND b.owninglocationid IN ('B6183892-C79D-4489-A58C-B526DF948B06','4CD8E72C-DC39-4544-B303-954C99176671')
  INNER JOIN dps.VehiclePricings c on b.VehicleInventoryItemID = c.VehicleInventoryItemID   
    AND c.VehiclePricingTS = ( -- the price at 8PM of theDate
      SELECT MAX(VehiclePricingTS)
      FROM dps.VehiclePricings 
      WHERE VehicleInventoryItemID = c.VehicleInventoryItemID 
        AND VehiclePricingTS < CAST(timestampadd(sql_tsi_hour,20,a.thedate) AS sql_timestamp))
  INNER JOIN dps.VehiclePricingDetails d on c.VehiclePricingID = d.VehiclePricingID 
  WHERE a.thedate in ('12/31/2015', '01/01/2016', '01/08/2016', '01/14/2016')) m
LEFT JOIN ucinv_price_bands n on m.amount BETWEEN n.pricefrom AND n.pricethru

select * FROM #pr_by_day

SELECT * from ucinv_tmp_avail_2 WHERE thedate = '01/01/2016'

select a.thedate, a.stocknumber, b.best_price, b.invoice, b.date_priced, 
  b.days_since_priced, b.priceband
-- SELECT COUNT(*)
FROM ucinv_tmp_avail_2 a
INNER JOIN (
  SELECT thedate, stock_number, 
    max(CASE WHEN typ = 'VehiclePricingDetail_BestPrice' THEN amount END) AS best_price,
    max(CASE WHEN typ = 'VehiclePricingDetail_Invoice' THEN amount END) AS invoice
  -- SELECT COUNT(*)
  FROM #pr_by_day a
  WHERE vehiclepricingts = (
      SELECT MAX(vehiclepricingts)
  	FROM #pr_by_day
  	WHERE thedate = a.thedate
  	  AND stock_number = a.stock_number)
  GROUP BY thedate, stock_number)b on a.thedate = b.thedate and a.stocknumber = b.stock_number
WHERE a.best_price < 0


-- 6/12
need to UPDATE:

damnit, seem to be getting confused
1. ucinv_tmp_avail_2 vehicles that need to be updated
DROP TABLE #vehicles;
select thedate, stocknumber
INTO #vehicles
FROM ucinv_tmp_avail_2
WHERE thedate IN ('12/31/2015', '01/01/2016', '01/08/2016', '01/14/2016')

the problem IS that ucinv_price_by_day has no rows for these date/vehicles
SELECT DISTINCT best_price FROM #vehicles

select * from ucinv_price_by_day WHERE thedate IN ('12/31/2015', '01/01/2016', '01/08/2016', '01/14/2016')


select *
FROM #vehicles a
INNER JOIN ucinv_price_by_day b on a.thedate = b.thedate
  AND a.stocknumber = b.stocknumber
  
SELECT a.*, 
  CAST(c.VehiclePricingTS AS sql_date) AS date_priced,
  timestampdiff(sql_tsi_day, CAST(c.VehiclePricingTS AS sql_date), a.thedate) AS days_since_priced,
  CASE when d.typ = 'VehiclePricingDetail_BestPrice' THEN amount END AS best_price,
  CASE when d.typ = 'VehiclePricingDetail_Invoice' THEN amount END AS invoice
-- SELECT COUNT(*)
FROM (
  select thedate, stocknumber
  FROM ucinv_tmp_avail_2
  WHERE thedate IN ('12/31/2015', '01/01/2016', '01/08/2016', '01/14/2016')) a
INNER JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
INNER JOIN dps.VehiclePricings c on b.VehicleInventoryItemID = c.VehicleInventoryItemID   
  AND c.VehiclePricingTS = ( -- the price at 8PM of theDate
    SELECT MAX(VehiclePricingTS)
    FROM dps.VehiclePricings 
    WHERE VehicleInventoryItemID = c.VehicleInventoryItemID 
      AND VehiclePricingTS < CAST(timestampadd(sql_tsi_hour,20,a.thedate) AS sql_timestamp))
INNER JOIN dps.VehiclePricingDetails d on c.VehiclePricingID = d.VehiclePricingID       

select m.*, n.priceband
INTO #fixers
FROM (
  SELECT thedate, b.stocknumber, 
    CAST(c.VehiclePricingTS AS sql_date) AS date_priced,
    timestampdiff(sql_tsi_day, CAST(c.VehiclePricingTS AS sql_date), a.thedate) AS days_since_priced,
    max(CASE when d.typ = 'VehiclePricingDetail_BestPrice' THEN amount END) AS best_price,
    max(CASE when d.typ = 'VehiclePricingDetail_Invoice' THEN amount END) AS invoice
  -- SELECT COUNT(*)
  FROM (
    select thedate, stocknumber
    FROM ucinv_tmp_avail_2
    WHERE thedate IN ('12/31/2015', '01/01/2016', '01/08/2016', '01/14/2016')) a  
  INNER JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
  INNER JOIN dps.VehiclePricings c on b.VehicleInventoryItemID = c.VehicleInventoryItemID   
    AND c.VehiclePricingTS = ( -- the price at 8PM of theDate
      SELECT MAX(VehiclePricingTS)
      FROM dps.VehiclePricings 
      WHERE VehicleInventoryItemID = c.VehicleInventoryItemID 
        AND VehiclePricingTS < CAST(timestampadd(sql_tsi_hour,20,a.thedate) AS sql_timestamp))
  INNER JOIN dps.VehiclePricingDetails d on c.VehiclePricingID = d.VehiclePricingID  
  GROUP BY thedate, b.stocknumber, 
    CAST(c.VehiclePricingTS AS sql_date),
    timestampdiff(sql_tsi_day, CAST(c.VehiclePricingTS AS sql_date), a.thedate)) m 
LEFT JOIN ucinv_price_bands n on m.best_price BETWEEN n.pricefrom AND n.pricethru    

-- #fixers looks good
-- now which tables ALL need to be updated
select * FROM #fixers 

select distinct best_price from ucinv_tmp_avail_2 WHERE thedate IN ('12/31/2015', '01/01/2016', '01/08/2016', '01/14/2016')

select distinct best_price from ucinv_tmp_avail_4 WHERE thedate IN ('12/31/2015', '01/01/2016', '01/08/2016', '01/14/2016')

SELECT COUNT(*) -- 2268
FROM ucinv_tmp_avail_2 a
INNER JOIN #fixers b on a.thedate = b.thedate AND a.stocknumber = b.stocknumber

SELECT COUNT(*) -- 2268
FROM ucinv_tmp_avail_4 a
INNER JOIN #fixers b on a.thedate = b.thedate AND a.stocknumber = b.stocknumber

UPDATE ucinv_tmp_avail_2
SET date_priced = x.date_priced,
    days_since_priced = x.days_since_priced,
    best_price = x.best_price,
    invoice = x.invoice,
    price_band = x.priceband
FROM #fixers x
WHERE ucinv_tmp_avail_2.thedate = x.thedate
  AND ucinv_tmp_avail_2.stocknumber = x.stocknumber;   
      
UPDATE ucinv_tmp_avail_4
SET date_priced = x.date_priced,
    days_since_priced = x.days_since_priced,
    best_price = x.best_price,
    invoice = x.invoice,
    price_band = x.priceband
FROM #fixers x
WHERE ucinv_tmp_avail_4.thedate = x.thedate
  AND ucinv_tmp_avail_4.stocknumber = x.stocknumber;       


