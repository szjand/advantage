/*
SELECT * FROM ucinv_tmp_avail_1;

SELECT * FROM ucinv_tmp_avail_2

SELECT * FROM ucinv_tmp_avail_3

SELECT * FROM ucinv_tmp_avail_4 WHERE sale_date = thedate

SELECT * FROM ucinv_tmp_avail_5

SELECT * FROM ucinv_tmp_avail_6



SELECT a.storecode, a.stocknumber, c.yearmodel, left(c.make, 14), 
  left(c.model, 26), c.vin, 
  left(left(a.shape_and_size, position('-' in a.shape_and_size) - 1), 12) as shape, 
  substring(a.shape_and_size,position('-' IN a.shape_and_size) + 1, 12) AS size,
  a.shape_and_size, a.from_Date, a.sale_date, a.sale_type, 
  a.price_band, a.best_price, a.invoice, a.date_priced, a.days_since_priced,
  a.days_owned, a.days_avail, a.sold_from_status, a.disposition AS avail_disposition,
  coalesce(d.miles, -1) AS miles, 
  coalesce(e.sale_amount, 0) AS sale_amount, 
  coalesce(e.cost_of_sale, 0) AS cost_of_sale, 
  coalesce(e.sales_gross, 0) AS sales_gross,
  coalesce(e.fi_sale_amount, 0) AS fi_sale_amount,
  coalesce(e.fi_cost, 0) AS fi_cost,
  coalesce(e.fi_gross, 0) AS fi_gross
  -- SELECT a.*, c.yearmodel, c.make, c.model, coalesce(d.miles, -1), c.vin
FROM ucinv_tmp_avail_4 a 
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
LEFT JOIN (
  select VehicleInventoryItemID, stocknumber, MAX(value) AS miles
  FROM (
    SELECT a.VehicleItemmileagets, a.value, b.stocknumber, b.VehicleInventoryItemID,
      b.fromts, b.thruts
    FROM dps.vehicleitemmileages a
    INNER JOIN dps.VehicleInventoryItems b on a.VehicleItemID = b.VehicleItemID 
      AND a.VehicleItemmileagets BETWEEN b.fromts AND coalesce(b.thruts, CAST('12/31/9999 00:00:00' AS sql_timestamp))
    INNER JOIN ucinv_tmp_avail_4 c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
      AND c.sale_date = c.thedate) d
  GROUP BY VehicleInventoryItemID, stocknumber) d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
LEFT JOIN ucinv_tmp_gross_2 e on a.stocknumber = e.stocknumber  
WHERE a.thedate = a.sale_date
*/
-- 7/26/15 added fixed gross

DROP TABLE ucinv_tmp_sales_activity;
CREATE TABLE ucinv_tmp_sales_activity (
  storecode cichar(3) constraint NOT NULL,
  stocknumber cichar(20) constraint NOT NULL,
  model_year cichar(4) constraint NOT NULL,  
  make cichar(14) constraint NOT NULL,
  model cichar(26) constraint NOT NULL,
  vin cichar(24) constraint NOT NULL,
  shape cichar(12) constraint NOT NULL,
  size cichar(12) constraint NOT NULL, 
  shape_and_size cichar(24) constraint NOT NULL,
  from_date date constraint NOT NULL,
  sale_date date constraint NOT NULL,
  sale_type cichar(9) constraint NOT NULL,
  price_band cichar(20) constraint NOT NULL,  
  best_price integer constraint NOT NULL,
  invoice integer constraint NOT NULL,
  date_priced date constraint NOT NULL,
  days_since_priced integer constraint NOT NULL,  
  days_owned integer constraint NOT NULL,
  days_avail integer constraint NOT NULL,
  sold_from_status cichar(12) constraint NOT NULL,
  avail_disposition cichar(18) constraint NOT NULL,
  miles integer constraint NOT NULL,
  sale_amount integer constraint NOT NULL,
  cost_of_sale integer constraint NOT NULL,
  sales_gross integer constraint NOT NULL,
  fi_sale_amount integer constraint NOT NULL,
  fi_cost integer constraint NOT NULL,
  fi_gross integer constraint NOT NULL,
  sd_labor_sales integer default '0' constraint NOT NULL,
  sd_labor_cogs integer default '0' constraint NOT NULL,
  sd_labor_gross integer default '0' constraint NOT NULL, 
  bs_labor_sales integer default '0' constraint NOT NULL,
  bs_labor_cogs integer default '0' constraint NOT NULL,
  bs_labor_gross integer default '0' constraint NOT NULL,
  bs_paint_mat_sales integer default '0' constraint NOT NULL,
  bs_paint_mat_cogs integer default '0' constraint NOT NULL,
  bs_paint_mat_gross integer default '0' constraint NOT NULL,
  re_labor_sales integer default '0' constraint NOT NULL,
  re_labor_cogs integer default '0' constraint NOT NULL,
  re_labor_gross integer default '0' constraint NOT NULL,
  ql_labor_sales integer default '0' constraint NOT NULL,
  ql_labor_cogs integer default '0' constraint NOT NULL,
  ql_labor_gross integer default '0' constraint NOT NULL,
  parts_sales integer default '0' constraint NOT NULL,
  parts_cogs integer default '0' constraint NOT NULL,
  parts_gross integer default '0' constraint NOT NULL,  
  recon_sales integer default '0' constraint NOT NULL,
  recon_cogs integer default '0' constraint NOT NULL,
  recon_gross integer default '0' constraint NOT NULL,
  constraint pk primary key (stocknumber)) IN database;

INSERT INTO ucinv_tmp_sales_activity
SELECT a.storecode, a.stocknumber, c.yearmodel, left(c.make, 14), 
  left(c.model, 26), c.vin, 
  left(left(a.shape_and_size, position('-' in a.shape_and_size) - 1), 12) as shape, 
  substring(a.shape_and_size,position('-' IN a.shape_and_size) + 1, 12) AS size,
  a.shape_and_size, a.from_Date, a.sale_date, a.sale_type, 
  a.price_band, a.best_price, a.invoice, a.date_priced, a.days_since_priced,
  a.days_owned, a.days_avail, a.sold_from_status, a.disposition AS avail_disposition,
  coalesce(d.miles, -1) AS miles, 
  coalesce(e.sale_amount, 0) AS sale_amount, 
  coalesce(e.cost_of_sale, 0) AS cost_of_sale, 
  coalesce(e.sales_gross, 0) AS sales_gross,
  coalesce(e.fi_sale_amount, 0) AS fi_sale_amount,
  coalesce(e.fi_cost, 0) AS fi_cost,
  coalesce(e.fi_gross, 0) AS fi_gross,
  coalesce(f.sd_labor_sales, 0),
  coalesce(f.sd_labor_cogs, 0),
  coalesce(f.sd_labor_gross, 0),
  coalesce(f.bs_labor_sales, 0),
  coalesce(f.bs_labor_cogs, 0),
  coalesce(f.bs_labor_gross, 0),
  coalesce(f.bs_paint_mat_sales, 0),
  coalesce(f.bs_paint_mat_cogs, 0),
  coalesce(f.bs_paint_mat_gross, 0),
  coalesce(f.re_labor_sales, 0),
  coalesce(f.re_labor_cogs, 0),
  coalesce(f.re_labor_gross, 0),
  coalesce(f.ql_labor_sales, 0),
  coalesce(f.ql_labor_cogs, 0),
  coalesce(f.ql_labor_gross, 0),
  coalesce(f.parts_sales, 0),
  coalesce(f.parts_cogs, 0),
  coalesce(f.parts_gross , 0),
  coalesce(f.sd_labor_sales, 0) + coalesce(f.bs_labor_sales, 0) + 
    coalesce(f.bs_paint_mat_sales, 0) + coalesce(f.re_labor_sales, 0) +
    coalesce(f.ql_labor_sales, 0) + coalesce(f.parts_sales, 0),
  coalesce(f.sd_labor_cogs, 0) + coalesce(f.bs_labor_cogs, 0) + 
    coalesce(f.bs_paint_mat_cogs, 0) + coalesce(f.re_labor_cogs, 0) +
    coalesce(f.ql_labor_cogs, 0) + coalesce(f.parts_cogs, 0),    
  coalesce(f.sd_labor_gross, 0) + coalesce(f.bs_labor_gross, 0) + 
    coalesce(f.bs_paint_mat_gross, 0) + coalesce(f.re_labor_gross, 0) +
    coalesce(f.ql_labor_gross, 0) + coalesce(f.parts_gross, 0)    
  -- SELECT a.*, c.yearmodel, c.make, c.model, coalesce(d.miles, -1), c.vin
FROM ucinv_tmp_avail_4 a 
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
LEFT JOIN (
  select VehicleInventoryItemID, stocknumber, MAX(value) AS miles
  FROM (
    SELECT a.VehicleItemmileagets, a.value, b.stocknumber, b.VehicleInventoryItemID,
      b.fromts, b.thruts
    FROM dps.vehicleitemmileages a
    INNER JOIN dps.VehicleInventoryItems b on a.VehicleItemID = b.VehicleItemID 
      AND a.VehicleItemmileagets BETWEEN b.fromts AND coalesce(b.thruts, CAST('12/31/9999 00:00:00' AS sql_timestamp))
    INNER JOIN ucinv_tmp_avail_4 c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
      AND c.sale_date = c.thedate) d
  GROUP BY VehicleInventoryItemID, stocknumber) d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
LEFT JOIN ucinv_tmp_gross_2 e on a.stocknumber = e.stocknumber  
LEFT JOIN ucinv_tmp_fixed_gross_1 f on a.stocknumber = f.stocknumber
WHERE a.thedate = a.sale_date;  
  
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','storecode','storecode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','model_year','model_year','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','make','make','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','model','model','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','vin','vin','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','shape','shape','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','size','size','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','shape_and_size','shape_and_size','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','from_date','from_date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','sale_date','sale_date','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','sale_type','sale_type','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_sales_activity','ucinv_tmp_sales_activity.adi','price_band','price_band','',2,512,'');

--SELECT *
--FROM ucinv_tmp_sales_activity ORDER BY sale_date DESC 