-- FROM weekly aging profile.sql -----------------------------------------------------
-- 11/24 ADD stocknumber, shape & size to ucinv_available
-- 11/27 ADD storecode
-- 3/7/15 increase ucinv_tmp_sales/ucinv_sales(& _previous).saleType to cichar(24) for wsim
DROP TABLE ucinv_available;  
CREATE TABLE ucinv_available (
  VehicleInventoryItemID cichar(38) constraint NOT NULL,
  fromTS timestamp constraint NOT NULL,
  thruTS timestamp constraint NOT NULL,
  owningLocationID char(38) constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  stockNumber cichar(9) constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  constraint PK primary key (VehicleInventoryItemID,fromTS)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','VehicleInventoryItemID', 
   'VehicleInventoryItemID','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','fromTS', 
   'fromTS','',2,512,'');     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','thruTS', 
   'thruTS','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','owningLocationID', 
   'owningLocationID','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','stockNumber', 
   'stockNumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','vehicleShape', 
   'vehicleShape','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','vehicleSize', 
   'vehicleSize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','storeCode', 
   'storeCode','',2,512,'');   

INSERT INTO ucinv_available(VehicleInventoryItemID,fromTs,thruTs,
  owningLocationID, storeCode, stockNumber, vehicleShape, vehicleSize)
SELECT a.VehicleInventoryItemID, a.fromTS, 
  coalesce(a.thruTS,CAST('12/31/9999 01:00:00' AS sql_timestamp)), 
  b.owningLocationID, 
  CASE b.owningLocationID
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1' 
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2' 
  END AS storeCode, left(trim(b.stocknumber), 9) AS stocknumber,
  e.description AS vehicleShape, f.description AS vehicleSize 
-- SELECT COUNT(*) -- 7943
FROM dps.VehicleInventoryItemStatuses a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN dps.MakeModelClassifications d on c.make = d.make
  AND c.model = d.model
INNER JOIN dps.typDescriptions e on d.vehicleType = e.typ
INNER JOIN dps.typDescriptions f on d.vehicleSegment = f.typ
WHERE a.category = 'RMFlagAV'
  AND b.owningLocationID <> '1B6768C6-03B0-4195-BA3B-767C0CAC40A6';  
  
-----------------------------------------------------------------------------------
-- FROM sales rate stocking priceand - sales rate.sql
DELETE FROM ucinv_tmp_sales;
INSERT INTO ucinv_tmp_sales(storeCode,stockNumber,saleDate,saleType,vin,vehicleCost,
  vehiclePRice,vehicleShape,vehicleSize,priceBand)
SELECT a.storeCode, 
  CASE  
    WHEN a.stockNumber = '' THEN k.imstk#
    ELSE a.stocknumber
  END AS stockNumber, 
  c.thedate AS saleDate, d.saleType, e.vin,
  cast(round(a.vehicleCost, 0) AS sql_integer) as vehicleCost, 
  cast(round(a.vehiclePrice, 0) as sql_integer) AS vehiclePrice,
  h.description AS vehicleShape, i.description AS vehicleSize,
  j.priceBand 
-- SELECT COUNT(*)  -- 13,121
-- INTO #wtf
FROM dds.factVehicleSale a
INNER JOIN dds.day c on a.approvedDateKey = c.datekey
  AND c.theDate > '01/01/2011'
INNER JOIN dds.dimCarDealInfo d on a.carDealInfoKey = d.carDealInfoKey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey  
INNER JOIN dps.VehicleItems f on e.vin = f.vin
INNER JOIN dps.MakeModelClassifications g on f.make = g.make
  AND f.model = g.model
INNER JOIN dps.typDescriptions h on g.vehicleType = h.typ  
INNER JOIN dps.typDescriptions i on g.vehicleSegment = i.typ
INNER JOIN dps.priceBands j on a.vehiclePrice BETWEEN j.priceFrom AND j.priceThru
INNER JOIN dds.stgArkonaINPMAST k on e.vin = k.imvin -- blank stocknumbers IN factVehicleSales
WHERE d.vehicleType = 'used';
-- fuck it, this IS NOT 4 decimal place inventory resolution
DELETE FROM ucinv_tmp_sales
WHERE stocknumber IN (
  SELECT stocknumber 
  FROM ucinv_tmp_sales
  GROUP BY stocknumber HAVING COUNT(*) > 1);
DROP TABLE ucinv_sales;  
CREATE TABLE ucinv_sales (
  storeCode cichar(3) constraint NOT NULL,
  stockNumber cichar(9) constraint NOT NULL,
  saleDate date constraint NOT NULL,
  saleType cichar(24) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  vehicleCost integer constraint NOT NULL,
  vehiclePrice integer constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  priceBand cichar(20) constraint NOT NULL,
  constraint PK primary key (stocknumber)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','storeCode','storeCode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','stockNumber','stockNumber','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','saleDate','saleDate','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','saleType','saleType','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','vin','vin','',2,512,'');    
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','vehiclePrice','vehiclePrice','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','vehicleShape','vehicleShape','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','vehicleSize','vehicleSize','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','priceBand','priceBand','',2,512,'');   

INSERT INTO ucinv_sales
select * FROM ucinv_tmp_sales;   

DELETE FROM ucinv_sales_previous_30 WHERE thedate = curdate(); 
INSERT INTO ucinv_sales_previous_30
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 30 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 30 AND a.thedate - 1
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
  WHERE a.thedate = curdate())x
GROUP BY theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand;

  
DELETE FROM ucinv_sales_previous_60 WHERE thedate = curdate(); 
INSERT INTO ucinv_sales_previous_60
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 60 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 60 AND a.thedate - 1
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
  WHERE a.thedate = curdate()) x
GROUP BY thedate, storeCode, saleType, vehicleshape, vehiclesize, priceBand; 

DELETE FROM ucinv_sales_previous_90 WHERE thedate = curdate(); 
INSERT INTO ucinv_sales_previous_90
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 90 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 90 AND a.thedate - 1
  WHERE  a.thedate = curdate()) x
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
GROUP BY thedate, storeCode, saleType, vehicleshape, vehiclesize, priceBand;

DELETE FROM ucinv_sales_previous_365 WHERE thedate = curdate(); 
INSERT INTO ucinv_sales_previous_365
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 365 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 365 AND a.thedate - 1
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
  WHERE a.thedate = curdate()) X
GROUP BY thedate, storeCode, saleType, vehicleshape, vehiclesize, priceBand;

DELETE FROM ucinv_glump_ranking WHERE thedate = curdate();
INSERT INTO ucinv_glump_ranking  
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, 
  shapeAndSize, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 90 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand, 
    c.shapeAndSize 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 90 AND a.thedate - 1
  LEFT JOIN ucinv_shapes_and_sizes c on b.vehicleShape = c.shape 
    AND b.vehicleSize = c.size
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
  WHERE a.thedate = curdate()) x
GROUP BY thedate, storeCode, saleType, vehicleshape, vehiclesize, shapeAndSize;  

-------------------------------------------------------------------------------------
-- FROM sales rate stocking priceband - stocking.sql
DROP TABLE ucinv_pulled;  
CREATE TABLE ucinv_pulled (
  VehicleInventoryItemID cichar(38) constraint NOT NULL,
  fromTS timestamp constraint NOT NULL,
  thruTS timestamp constraint NOT NULL,
  owningLocationID char(38) constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  stockNumber cichar(9) constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  constraint PK primary key (VehicleInventoryItemID,fromTS)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','VehicleInventoryItemID', 
   'VehicleInventoryItemID','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','fromTS', 
   'fromTS','',2,512,'');     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','thruTS', 
   'thruTS','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','owningLocationID', 
   'owningLocationID','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','stockNumber', 
   'stockNumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','vehicleShape', 
   'vehicleShape','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','vehicleSize', 
   'vehicleSize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','storeCode', 
   'storeCode','',2,512,'');   
   
INSERT INTO ucinv_pulled(VehicleInventoryItemID,fromTs,thruTs,
  owningLocationID, storeCode, stockNumber, vehicleShape, vehicleSize)
SELECT a.VehicleInventoryItemID, a.fromTS, 
  coalesce(a.thruTS,CAST('12/31/9999 01:00:00' AS sql_timestamp)), 
  b.owningLocationID, 
  CASE b.owningLocationID
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1' 
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2' 
  END AS storeCode,  left(trim(b.stocknumber), 9) AS stocknumber,
  e.description AS vehicleShape, f.description AS vehicleSize 
-- SELECT COUNT(*) -- 7943
FROM dps.VehicleInventoryItemStatuses a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN dps.MakeModelClassifications d on c.make = d.make
  AND c.model = d.model
INNER JOIN dps.typDescriptions e on d.vehicleType = e.typ
INNER JOIN dps.typDescriptions f on d.vehicleSegment = f.typ
WHERE a.category = 'RMFlagPulled'
  AND b.owningLocationID <> '1B6768C6-03B0-4195-BA3B-767C0CAC40A6';   
  
----------------------------------------------------------------------------------------
-- FROM speed up price bands.sql

-- ok, put it IN a TABLE, one year at a time
-- daily UPDATE
DELETE FROM ucinv_price_by_day WHERE thedate = curdate() - 1;
INSERT INTO ucinv_price_by_day(thedate,stocknumber,pricingTS,bestprice,
  invoice,dayssincepriced)
SELECT a.thedate, left(trim(b.stocknumber), 9), c.VehiclePricingTS, 
  SUM(CASE WHEN d.typ = 'VehiclePricingDetail_BestPrice' THEN cast(d.amount as sql_integer) END) AS bestPrice,
  SUM(CASE WHEN d.typ = 'VehiclePricingDetail_Invoice' THEN cast(d.amount as sql_integer) END) AS invoice,
  timestampdiff(sql_tsi_day, CAST(VehiclePricingTS AS sql_date), a.thedate) AS days
FROM dds.day a
-- INNER JOIN should be ok, there IS inventory on every date
INNER JOIN dps.VehicleInventoryItems b on a.thedate 
    between CAST(b.fromTS AS sql_date) AND 
      cast(coalesce(thruts, CAST('12/31/9999 20:00:00' AS sql_timestamp)) AS sql_date)
  AND b.owninglocationid IN ('B6183892-C79D-4489-A58C-B526DF948B06','4CD8E72C-DC39-4544-B303-954C99176671')
INNER JOIN dps.VehiclePricings c on b.VehicleInventoryItemID = c.VehicleInventoryItemID   
  AND c.VehiclePricingTS = ( -- the price at 8PM of theDate
    SELECT MAX(VehiclePricingTS)
    FROM dps.VehiclePricings 
    WHERE VehicleInventoryItemID = c.VehicleInventoryItemID 
      AND VehiclePricingTS < CAST(timestampadd(sql_tsi_hour,20,a.thedate) AS sql_timestamp))
INNER JOIN dps.VehiclePricingDetails d on c.VehiclePricingID = d.VehiclePricingID 
--WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1
--WHERE a.thedate BETWEEN '01/01/2012' AND '05/31/2012' 
--WHERE a.thedate BETWEEN '06/01/2012' AND '09/30/2012' 
--WHERE a.thedate BETWEEN '10/01/2012' AND '12/31/2012'
-- AND the daily updae
WHERE a.thedate = curdate()- 1
GROUP BY a.thedate, b.stocknumber, c.VehiclePricingTS;

UPDATE ucinv_price_by_day
SET priceband = (
  SELECT priceband
  FROM ucinv_price_bands
  WHERE ucinv_price_by_day.bestprice BETWEEN priceFrom AND priceThru)
WHERE theDate = curdate() - 1;
 