DROP TABLE ucinv_tmp_sales_by_status;
CREATE TABLE ucinv_tmp_sales_by_status (
  thedate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  sales_total integer constraint NOT NULL,
  avail_fresh integer constraint NOT NULL,
  avail_aged integer constraint NOT NULL,
  pulled integer constraint NOT NULL,
  raw integer constraint NOT NULL,
  avail_fresh_perc numeric(8,2) constraint NOT NULL,
  avail_aged_perc numeric(8,2) constraint NOT NULL,
  pulled_perc numeric(8,2) constraint NOT NULL,
  raw_perc numeric(8,2) constraint NOT NULL,
  constraint pk primary key(thedate, storecode)) IN database;
INSERT INTO ucinv_tmp_sales_by_status
SELECT aa.*, coalesce(bb.sales_total, 0) AS sales_total, 
  coalesce(bb.avail_fresh, 0) AS avail_fresh, 
  coalesce(bb.avail_aged, 0) AS avail_aged,
  coalesce(bb.pulled, 0) AS pulled, coalesce(bb.raw, 0) AS raw,
  CASE 
    WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
    ELSE round(1.0 * coalesce(bb.avail_fresh, 0)/coalesce(bb.sales_total, 0), 2) 
  END AS avail_fresh_perc,
  CASE 
    WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
    ELSE round(1.0 * coalesce(bb.avail_aged, 0)/coalesce(bb.sales_total, 0), 2) 
  END AS avail_aged_perc,
  CASE 
    WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
    ELSE round(1.0 * coalesce(bb.pulled, 0)/coalesce(bb.sales_total, 0), 2) 
  END AS pulled_perc,
  CASE 
    WHEN coalesce(bb.sales_total, 0) = 0 THEN 0
    ELSE round(1.0 * coalesce(bb.raw, 0)/coalesce(bb.sales_total, 0), 2) 
  END AS raw_perc      
FROM (
  SELECT thedate, 'RY1' AS storecode
  FROM dds.day 
  WHERE thedate BETWEEN '01/01/2014' AND curdatE() - 1
  UNION 
  SELECT thedate, 'RY2' AS storecode
  FROM dds.day 
  WHERE thedate BETWEEN '01/01/2014' AND curdatE() - 1) aa
LEFT JOIN (
  SELECT a.*, b.avail_fresh, b.avail_aged, b.pulled, b.raw
  FROM (
    SELECT thedate, storecode, COUNT(*) AS sales_total
    -- SELECT *
    FROM ucinv_tmp_avail_4
    WHERE thedate = sale_date
      AND sale_type = 'retail'
    GROUP BY thedate, storecode) a
  LEFT JOIN (
  SELECT thedate, storecode, 
    SUM(CASE WHEN status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_fresh,
    SUM(CASE WHEN status = 'avail_aged' THEN 1 ELSE 0 END) AS avail_aged,
    SUM(CASE WHEN status = 'pull' THEN 1 ELSE 0 END) AS pulled,
    SUM(CASE WHEN status = 'raw' THEN 1 ELSE 0 END) AS raw
  -- SELECT *
  FROM ucinv_tmp_avail_4
  WHERE thedate = sale_date
    AND sale_type = 'retail'
  GROUP BY thedate, storecode) b on a.thedate = b.thedate AND a.storecode = b.storecode) bb on aa.thedate = bb.thedate AND aa.storecode = bb.storecode collate ads_Default_ci