SELECT a.*, b.thedate, c.thedate, d.*, e.vin
FROM dds.factVehicleSale a
INNER JOIN dds.day b on a.cappedDateKey = b.datekey 
INNER JOIN dds.day c on a.approvedDateKey = c.datekey
INNER JOIN dds.dimCarDealInfo d on a.carDealInfoKey = d.carDealInfoKey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey

-- there are no deals IN factVehicleSale that are NOT capped AND are NOT approved
SELECT a.*, b.thedate, c.thedate, d.*, e.vin
FROM dds.factVehicleSale a
INNER JOIN dds.day b on a.cappedDateKey = b.datekey 
INNER JOIN dds.day c on a.approvedDateKey = c.datekey
INNER JOIN dds.dimCarDealInfo d on a.carDealInfoKey = d.carDealInfoKey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey
WHERE b.thedate = '12/31/9999'
  AND c.thedate = '12/31/9999'

-- accepted NOT capped deals BY date  
-- damned few, mostly "current" deals
SELECT a.*, b.thedate, c.thedate, d.*, e.vin
FROM dds.factVehicleSale a
INNER JOIN dds.day b on a.cappedDateKey = b.datekey 
INNER JOIN dds.day c on a.approvedDateKey = c.datekey
INNER JOIN dds.dimCarDealInfo d on a.carDealInfoKey = d.carDealInfoKey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey
WHERE d.dealstatus = 'accepted'
ORDER BY c.thedate  
  
-- actually believe i will go with the accepted date, closest to sales
-- department perception of the actual sale, capped dates layers on the
-- overhead of accounting processing  
-- at this time, i DO NOT know what IS used on the financial statement
-- limit to used sales since 1/1/2011
SELECT a.storeCode, a.stockNumber, c.thedate AS saleDate, d.saleType, e.vin,
  a.vehicleCost, a.vehiclePrice
FROM dds.factVehicleSale a
-- INNER JOIN dds.day b on a.cappedDateKey = b.datekey 
INNER JOIN dds.day c on a.approvedDateKey = c.datekey
INNER JOIN dds.dimCarDealInfo d on a.carDealInfoKey = d.carDealInfoKey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey  
WHERE vehicleType = 'used'
  AND c.thedate BETWEEN '01/01/2011' AND curdate()

-- DROP TABLE #wtf;  
DELETE FROM ucinv_tmp_sales;
INSERT INTO ucinv_tmp_sales(storeCode,stockNumber,saleDate,saleType,vin,vehicleCost,
  vehiclePRice,vehicleShape,vehicleSize,priceBand)
SELECT a.storeCode, 
  CASE  
    WHEN a.stockNumber = '' THEN k.imstk#
    ELSE a.stocknumber
  END AS stockNumber, 
  c.thedate AS saleDate, d.saleType, e.vin,
  cast(round(a.vehicleCost, 0) AS sql_integer) as vehicleCost, 
  cast(round(a.vehiclePrice, 0) as sql_integer) AS vehiclePrice,
  h.description AS vehicleShape, i.description AS vehicleSize,
  j.priceBand 
-- SELECT COUNT(*)  -- 13,121
-- INTO #wtf
FROM dds.factVehicleSale a
INNER JOIN dds.day c on a.approvedDateKey = c.datekey
  AND c.theDate > '01/01/2011'
INNER JOIN dds.dimCarDealInfo d on a.carDealInfoKey = d.carDealInfoKey
INNER JOIN dds.dimVehicle e on a.vehicleKey = e.vehicleKey  
INNER JOIN dps.VehicleItems f on e.vin = f.vin
INNER JOIN dps.MakeModelClassifications g on f.make = g.make
  AND f.model = g.model
INNER JOIN dps.typDescriptions h on g.vehicleType = h.typ  
INNER JOIN dps.typDescriptions i on g.vehicleSegment = i.typ
INNER JOIN dps.priceBands j on a.vehiclePrice BETWEEN j.priceFrom AND j.priceThru
INNER JOIN dds.stgArkonaINPMAST k on e.vin = k.imvin -- blank stocknumbers IN factVehicleSales
WHERE d.vehicleType = 'used';

SELECT * FROM ucinv_tmp_sales
-- duplicate stocknumbers
SELECT stocknumber FROM ucinv_tmp_sales GROUP BY stocknumber HAVING COUNT(*) > 1
-- blank stocknumbers
SELECT * FROM #wtf WHERE stocknumber = ''
-- this looks ok for updating missing stocknumbers
SELECT a.*, b.imstk# FROM ucinv_tmp_sales a left join dds.stgArkonaINPMAST b on a.vin = b.imvin WHERE stocknumber = ''
SELECT vin FROM (
SELECT a.*, b.imstk# FROM ucinv_tmp_sales a left join dds.stgArkonaINPMAST b on a.vin = b.imvin WHERE stocknumber = ''
) x GROUP BY vin HAVING COUNT(*) > 1

select * FROM ucinv_tmp_sales WHERE stocknumber IN ('13744XX','17064A', '20373X')
-- fuck it, this IS NOT 4 decimal place inventory resolution
DELETE FROM ucinv_tmp_sales
WHERE stocknumber IN (
  SELECT stocknumber 
  FROM ucinv_tmp_sales
  GROUP BY stocknumber HAVING COUNT(*) > 1)

-- DROP TABLE ucinv_tmp_sales;  
-- DO clean up on tmp TABLE prior to inserting INTO ucinv_sales which will have
-- a PK of stocknumber
CREATE TABLE ucinv_tmp_sales (
  storeCode cichar(3),
  stockNumber cichar(9),
  saleDate date,
  saleType cichar(12),
  vin cichar(17),
  vehicleCost integer,
  vehiclePrice integer,
  vehicleShape cichar(30),
  vehicleSize cichar(30),
  priceBand cichar(20)) IN database;

DROP TABLE ucinv_sales;  
CREATE TABLE ucinv_sales (
  storeCode cichar(3) constraint NOT NULL,
  stockNumber cichar(9) constraint NOT NULL,
  saleDate date constraint NOT NULL,
  saleType cichar(12) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL,
  vehicleCost integer constraint NOT NULL,
  vehiclePrice integer constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  priceBand cichar(20) constraint NOT NULL,
  constraint PK primary key (stocknumber)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','storeCode','storeCode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','stockNumber','stockNumber','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','saleDate','saleDate','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','saleType','saleType','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','vin','vin','',2,512,'');    
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','vehiclePrice','vehiclePrice','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','vehicleShape','vehicleShape','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','vehicleSize','vehicleSize','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales','ucinv_sales.adi','priceBand','priceBand','',2,512,'');   

 
INSERT INTO ucinv_sales
select * FROM ucinv_tmp_sales; 

SELECT *
FROM ucinv_sales;

-- prev 30 day sales going back 2 years
-- thedate drives this TABLE, for each day, going back to 1/1/12 years, the uc sales
-- that transpired IN the previous 30 days
SELECT a.thedate, a.thedate - 30 as fromDate, a.theDate - 1 AS thruDate,
  b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
FROM dds.day a
LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 30 AND a.thedate - 1
WHERE a.thedate BETWEEN '01/01/2012' AND curdate()

SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 30 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 30 AND a.thedate - 1
  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
GROUP BY thedate, storeCode, saleType, vehicleshape, vehiclesize, priceBand;  

/*

the thing to remember about these tables
the date IS NOT the date of sale, but the the date for which we are showing
the previous xx days of sale

*/

DROP TABLE ucinv_sales_previous_30;
CREATE TABLE ucinv_sales_previous_30 (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  saleType cichar(12) constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  priceBand cichar(20) constraint NOT NULL,
  units integer  constraint NOT NULL,
  constraint pk primary key(thedate,storecode,saletype,vehicleShape,vehicleSize,priceBand)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_30','ucinv_sales_previous_30.adi','theDate','theDate','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_30','ucinv_sales_previous_30.adi','vehicleShape','vehicleShape','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_30','ucinv_sales_previous_30.adi','vehicleSize','vehicleSize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_30','ucinv_sales_previous_30.adi','units','units','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_30','ucinv_sales_previous_30.adi','storeCode','storeCode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_30','ucinv_sales_previous_30.adi','saleType','saleType','',2,512,'');  
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_30','ucinv_sales_previous_30.adi','priceBand','priceBand','',2,512,'');  

DELETE FROM ucinv_sales_previous_30 WHERE thedate = curdate(); 
INSERT INTO ucinv_sales_previous_30
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 30 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 30 AND a.thedate - 1
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
  WHERE a.thedate = curdate())x
GROUP BY theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand;
  
DROP TABLE ucinv_sales_previous_60;
CREATE TABLE ucinv_sales_previous_60 (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  saleType cichar(12) constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  priceBand cichar(20) constraint NOT NULL,
  units integer  constraint NOT NULL,
  constraint pk primary key(thedate,storecode,saletype,vehicleShape,vehicleSize,priceBand)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_60','ucinv_sales_previous_60.adi','theDate','theDate','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_60','ucinv_sales_previous_60.adi','vehicleShape','vehicleShape','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_60','ucinv_sales_previous_60.adi','vehicleSize','vehicleSize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_60','ucinv_sales_previous_60.adi','units','units','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_60','ucinv_sales_previous_60.adi','storeCode','storeCode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_60','ucinv_sales_previous_60.adi','saleType','saleType','',2,512,'');  
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_60','ucinv_sales_previous_60.adi','priceBand','priceBand','',2,512,'');
  
DELETE FROM ucinv_sales_previous_60 WHERE thedate = curdate(); 
INSERT INTO ucinv_sales_previous_60
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 60 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 60 AND a.thedate - 1
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
  WHERE a.thedate = curdate()) x
GROUP BY thedate, storeCode, saleType, vehicleshape, vehiclesize, priceBand;  

DROP TABLE ucinv_sales_previous_90;
CREATE TABLE ucinv_sales_previous_90 (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  saleType cichar(12) constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  priceBand cichar(20) constraint NOT NULL,
  units integer  constraint NOT NULL,
  constraint pk primary key(thedate,storecode,saletype,vehicleShape,vehicleSize,priceBand)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_90','ucinv_sales_previous_90.adi','theDate','theDate','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_90','ucinv_sales_previous_90.adi','vehicleShape','vehicleShape','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_90','ucinv_sales_previous_90.adi','vehicleSize','vehicleSize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_90','ucinv_sales_previous_90.adi','units','units','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_90','ucinv_sales_previous_90.adi','storeCode','storeCode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_90','ucinv_sales_previous_90.adi','saleType','saleType','',2,512,'');  
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_90','ucinv_sales_previous_90.adi','priceBand','priceBand','',2,512,''); 
 
DELETE FROM ucinv_sales_previous_90 WHERE thedate = curdate(); 
INSERT INTO ucinv_sales_previous_90
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 90 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 90 AND a.thedate - 1
  WHERE  a.thedate = curdate()) x
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
GROUP BY thedate, storeCode, saleType, vehicleshape, vehiclesize, priceBand;

DROP TABLE ucinv_sales_previous_365;
CREATE TABLE ucinv_sales_previous_365 (
  theDate date constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  saleType cichar(12) constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  priceBand cichar(20) constraint NOT NULL,
  units integer  constraint NOT NULL,
  constraint pk primary key(thedate,storecode,saletype,vehicleShape,vehicleSize,priceBand)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_365','ucinv_sales_previous_365.adi','theDate','theDate','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_365','ucinv_sales_previous_365.adi','vehicleShape','vehicleShape','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_365','ucinv_sales_previous_365.adi','vehicleSize','vehicleSize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_365','ucinv_sales_previous_365.adi','units','units','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_365','ucinv_sales_previous_365.adi','storeCode','storeCode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_365','ucinv_sales_previous_365.adi','saleType','saleType','',2,512,'');  
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_sales_previous_365','ucinv_sales_previous_365.adi','priceBand','priceBand','',2,512,'');  

DELETE FROM ucinv_sales_previous_365 WHERE thedate = curdate(); 
INSERT INTO ucinv_sales_previous_365
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 365 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 365 AND a.thedate - 1
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
  WHERE a.thedate = curdate()) X
GROUP BY thedate, storeCode, saleType, vehicleshape, vehiclesize, priceBand;
  

select count(*), 90 from ucinv_sales_previous_90
UNION
select count(*), 60 from ucinv_sales_previous_60
UNION
select count(*), 30 from ucinv_sales_previous_30  
 
-- glump ranking, base it on previous 90 days sales
-- going back to 1/1/12
DROP TABLE ucinv_glump_ranking;
CREATE TABLE ucinv_glump_ranking (
  theDate date constraint NOT NULL, 
  storeCode cichar(3) constraint NOT NULL,
  saleType cichar(12) constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  shapeAndSize cichar(63) constraint NOT NULL,
  units integer constraint NOT NULL,
  constraint pk primary key (theDate,storeCode,saleType,vehicleShape,vehicleSize)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_glump_ranking','ucinv_glump_ranking.adi','theDate','theDate','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_glump_ranking','ucinv_glump_ranking.adi','vehicleShape','vehicleShape','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_glump_ranking','ucinv_glump_ranking.adi','vehicleSize','vehicleSize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_glump_ranking','ucinv_glump_ranking.adi','units','units','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_glump_ranking','ucinv_glump_ranking.adi','storeCode','storeCode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_glump_ranking','ucinv_glump_ranking.adi','saleType','saleType','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_glump_ranking','ucinv_glump_ranking.adi','shapeAndSize','shapeAndSize','',2,512,'');

DELETE FROM ucinv_glump_ranking WHERE thedate = curdate();
INSERT INTO ucinv_glump_ranking  
SELECT theDate, storeCode, saleType, vehicleShape, vehicleSize, 
  shapeAndSize, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 90 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand, 
    c.shapeAndSize 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 90 AND a.thedate - 1
  LEFT JOIN ucinv_shapes_and_sizes c on b.vehicleShape = c.shape 
    AND b.vehicleSize = c.size
--  WHERE a.thedate BETWEEN '01/01/2012' AND curdate()) x
  WHERE a.thedate = curdate()) x
GROUP BY thedate, storeCode, saleType, vehicleshape, vehiclesize, shapeAndSize;  

-- fuck it, i want to ADD a rank column
SELECT *
FROM ucinv_glump_ranking
ORDER BY thedate, storecode, saletype, units desc

-- on 11/22, the top 10 glumps with sales BY price band
-- TOO SLOW AND leaves out pricebands with no activity
DECLARE @date date;
DECLARE @saleType string;
DECLARE @location string;
@date = '11/22/2014';
@saleType = 'retail';
@location = 'ry1';
SELECT c.*
FROM (
  SELECT top 10 a.vehicleShape, a.vehicleSize
  FROM ucinv_glump_ranking a
  WHERE a.thedate = @date
    AND a.saleType = @saleType
    AND a.storeCode = @location
  ORDER BY a.units DESC) b    
LEFT JOIN ucinv_sales_previous_30 c on b.vehicleShape = c.vehicleShape
  AND b.vehicleSize = c.vehicleSize
WHERE c.thedate = @date
  AND c.saleType = @saleType
  AND c.storeCode = @location
ORDER BY b.vehicleShape, b.vehicleSize, c.priceBand DESC     
 
-- on 11/22, the top 10 glumps with sales BY price band
-- TOO SLOW AND leaves out pricebands with no activity
-- this IS better, but sorting becomes an issue
-- wtf, top 10 IS fast, top 2 is fast, top 1 IS slow
-- added ucinv_price_bands with correct field type (cichar), no help
DECLARE @date date;
DECLARE @saleType string;
DECLARE @location string;
@date = '11/22/2014';
@saleType = 'retail';
@location = 'ry1';
SELECT b.vehicleShape, b.vehicleSize, c.priceband, coalesce(d.units,0) AS units
FROM (
  SELECT top 2 a.vehicleShape, a.vehicleSize
  FROM ucinv_glump_ranking a
  WHERE a.thedate = @date
    AND a.saleType = @saleType
    AND a.storeCode = @location
  ORDER BY a.units DESC) b    
LEFT JOIN ucinv_price_bands c on 1 = 1
LEFT JOIN ucinv_sales_previous_30 d on b.vehicleShape = d.vehicleShape
  AND b.vehicleSize = d.vehicleSize
  AND c.priceBand = d.priceBand 
  AND d.theDate = @date
  AND d.saleType = @saleType
  AND d.storecode = @location
ORDER BY d.priceband DESC 

DROP TABLE ucinv_price_bands;
CREATE TABLE ucinv_price_bands (
  priceBand cichar(20) constraint NOT NULL,
  priceFrom integer constraint NOT NULL,
  priceThru integer constraint NOT NULL,
  constraint pk primary key (priceBand)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_price_bands','ucinv_price_bands.adi','priceband','priceband','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_price_bands','ucinv_price_bands.adi','pricefrom','pricefrom','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_price_bands','ucinv_price_bands.adi','pricethru','pricethru','',2,512,''); 
INSERT INTO ucinv_price_bands
SELECT * FROM dps.pricebands; 
  
SELECT *
FROM ucinv_sales_previous_90
WHERE thedate = '01/15/2014'
  AND saletype = 'retail'
  AND storecode = 'ry1'
 


DECLARE @date date;
@date = '11/22/2014';
SELECT vehicleShape, vehicleSize, priceBand, COUNT(*) AS units
FROM (
  SELECT a.thedate, a.thedate - 30 as fromDate, a.theDate - 1 AS thruDate,
    b.storecode, b.saleType, b.vehicleShape, b.vehicleSize, b.priceBand 
  FROM dds.day a
  LEFT JOIN ucinv_sales b on b.saleDate BETWEEN a.theDate - 30 AND a.thedate - 1
  WHERE a.thedate BETWEEN curdate()-365 AND curdate() - 1) x
WHERE theDate = @date  
  AND storecode = 'RY1'
  AND saleType = 'retail'
GROUP BY vehicleShape, vehicleSize, priceBand
ORDER BY units DESC 


SELECT MIN(saleDate) FROM ucinv_sales

-- ALL ry1 retail sales since 1/1/11
-- shape-size-priceband
SELECT k.vehicleShape, k.vehicleSize, priceband, COUNT(*)
FROM ucinv_sales k 
WHERE k.storecode = 'ry1'
  AND k.saleType = 'retail' 
GROUP BY k.vehicleShape, k.vehicleSize, priceband 
ORDER BY COUNT(*) DESC
-- priceband
SELECT priceband, COUNT(*)
FROM ucinv_sales k 
WHERE k.storecode = 'ry1'
  AND k.saleType = 'retail' 
GROUP BY priceband 
ORDER BY COUNT(*) DESC
-- shape-size
SELECT k.vehicleShape, k.vehicleSize, COUNT(*)
FROM ucinv_sales k 
WHERE k.storecode = 'ry1'
  AND k.saleType = 'retail' 
GROUP BY k.vehicleShape, k.vehicleSize
ORDER BY COUNT(*) DESC
