﻿-- ben wants to see a wash sheet for the november new car sales
select c.store, b.year_month, col, sum(amount)
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201611
  and (
    (b.page between 5 and 15) -- new cars
--     or
--     (b.page = 16 and b.line between 1 and 14) -- used cars
     or
     (b.page = 17 and b.line between 1 and 20))-- f/i
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
group by c.store, b.year_month, col
having sum(amount) <> 0
order by c.store, b.year_month;


select c.store, b.year_month, line, col, amount, e.account, e.account_type, e.description
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201611
   and b.page betwee
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
where amount <> 0
order by c.store, line, col


select c.store, b.year_month, b.line, b.col, a.amount, e.account, e.account_type, e.description, f.*
from fin.fact_fs a
inner join fin.dim_fs b on a.fs_key = b.fs_key
  and b.year_month = 201611
   and b.page between 5 and 15
inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
inner join fin.dim_account e on d.gl_account = e.account
left join (
  select a.control, b.account, sum(a.amount) as amount
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join dds.dim_date c on a.date_key = c.date_key
    and c.year_month = 201611
  where a.post_status = 'Y' 
    and b.department_code = 'NC'
  group by a.control, b.account) f on e.account = f.account
where a.amount <> 0
order by c.store, b.line, b.col




-- november new car sales with gross (front end)
drop table nc_sales;
create temp table nc_sales as
select g.*, -(sales + cogs) as gross
from (
  select f.control,
    sum(case when f.account_type = 'Sale' then f.amount end)::integer as Sales,
    sum(case when f.account_type = 'COGS' then f.amount end)::integer as COGS
  from fin.fact_fs a
  inner join fin.dim_fs b on a.fs_key = b.fs_key
    and b.year_month = 201611
     and b.page between 5 and 15
  inner join fin.dim_fs_org c on a.fs_org_key = c.fs_org_key  
  inner join fin.dim_fs_account d on a.fs_account_key = d.fs_account_key
  inner join fin.dim_account e on d.gl_account = e.account
  left join (
    select a.control, b.account, b.account_type, sum(a.amount) as amount
    from fin.fact_gl a
    inner join fin.dim_account b on a.account_key = b.account_key
    inner join dds.dim_date c on a.date_key = c.date_key
      and c.year_month = 201611
    where a.post_status = 'Y' 
      and b.department_code = 'NC'
    group by a.control, b.account, b.account_type) f on e.account = f.account
  where a.amount <> 0
  group by f.control) g
where control is not null 
  and sales is not null
  and cogs is not null;

drop table nc_sales_1;
create temp table nc_sales_1 as
select a.control, a.gross as front_end_gross, b.year, b.make, b.model, b.body_style
from nc_sales a
left join dds.ext_inpmast b on a.control = b.inpmast_stock_number

select * from nc_Sales_1 order by year


--trades on nov nc sales
drop table trades;
create temp table trades as
select a.*, coalesce(b.stocknumber, 'No Trade') as stock_number
from nc_sales_1 a
left join greg.ucinv_tmp_vehicles b on a.control = 
  replace(replace(replace(replace(replace(stocknumber,'a','')::citext,'b','')::citext,'c','')::citext,'d','')::citext,'e','')
left join greg.ucinv_tmp_sales_activity c on b.stocknumber = c.stocknumber  

select * from trades;

create temp table sold_trades as
select a.*, b.model_year, b.make as tr_make, b.model as tr_model, b.sale_date, b.sale_type, sales_gross, fi_gross, recon_gross, 
  sales_gross + fi_gross + recon_gross as total_uc_gross
from trades a
left join greg.ucinv_tmp_sales_activity b on a.stock_number = b.stocknumber
order by a.control

select a.*, b.plg
from sold_trades a
left join greg.used_vehicle_daily_snapshot b on a.stock_number = b.stocknumber
  and b.the_Date = current_Date - 1
where a.sale_date is null 
  and a.stock_number <> 'no trade'

select * from greg.used_vehicle_daily_snapshot where stocknumber = '28898B' order by the_date desc 

-- select * 
-- from trades a
-- left join arkona.ext_inpmast b on a.stock_number = b.inpmast_stock_number
-- 
-- select * 
-- from greg.used_vehicle_Daily_snapshot a
-- where the_date = (
--     select max(the_date)
--     from greg.used_vehicle_Daily_snapshot
--     where stocknumber = a.stocknumber)
-- 
-- select stocknumber, max(the_date) as the_date
-- from greg.used_vehicle_Daily_snapshot
-- group by stocknumber
-- 
-- with 
--   nc as (
--     select a.*, coalesce(b.stocknumber, 'No Trade') as stock_number, c.*
--     from nc_sales a
--     left join greg.ucinv_tmp_vehicles b on a.control = 
--       replace(replace(replace(replace(replace(stocknumber,'a','')::citext,'b','')::citext,'c','')::citext,'d','')::citext,'e','')
--     left join greg.ucinv_tmp_sales_activity c on b.stocknumber = c.stocknumber),
--   uc_date as (
--   select stocknumber, max(the_date) as the_date
--   from greg.used_vehicle_Daily_snapshot
--   group by stocknumber)      
-- select * 
-- from greg.used_vehicle_Daily_snapshot a
-- inner join nc b on a.stocknumber = b.stocknumber
-- inner join uc_date c on a.stocknumber = c.stocknumber
--   and a.the_date = c.the_date