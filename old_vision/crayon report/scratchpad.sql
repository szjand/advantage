SELECT *
FROM zcb
WHERE name = 'gmfs gross'
AND dl4 = 'sales'
AND dl5 ='used'
AND al2 = 'sales'
--  AND glaccount = '144600'
ORDER BY glaccount  


SELECT gtacct, gtjrnl, SUM(gttamt), COUNT(*)
FROM stgArkonaGLPTRNS
WHERE gtdate BETWEEN '04/01/2015' AND '04/30/2015'
AND gtacct IN (
  SELECT glaccount
  FROM zcb
  WHERE name = 'gmfs gross'
  AND dl4 = 'sales'
  AND dl5 ='used'
  AND al2 = 'sales')
GROUP BY gtacct, gtjrnl  


SELECT *
FROM (
SELECT gtacct, gtjrnl, SUM(gttamt), COUNT(*)
FROM stgArkonaGLPTRNS
WHERE gtdate BETWEEN '04/01/2015' AND '04/30/2015'
AND gtacct IN (
  SELECT glaccount
  FROM zcb
  WHERE name = 'gmfs gross'
  AND dl4 = 'sales'
  AND dl5 ='used'
  AND al2 = 'sales')
GROUP BY gtacct, gtjrnl) a
LEFT JOIN stgArkonaGLPMAST b on a.gtacct = b.gmacct
  AND b.gmyear = 2015
  
  
  
SELECT *
FROM stgArkonaGLPTRNS
WHERE gtdate BETWEEN '04/01/2015' AND '04/30/2015'
AND gtacct IN (
  SELECT glaccount
  FROM zcb
  WHERE name = 'gmfs gross'
  AND dl4 = 'sales'
  AND dl5 ='used'
  AND al2 = 'sales')
  
  
-- recon on inventory (pre sale only)
SELECT *
FROM stgArkonaGLPTRNS
WHERE gtdate BETWEEN '04/01/2015' AND '04/30/2015'
AND gtacct IN ('124000','124100')
  AND gtjrnl IN ('svi','sca','swa')
  
-- april uc COGS transactions
-- DROP TABLE #wtf;
SELECT a.gtdate, a.gtjrnl, a.gtacct, a.gtctl#, a.gtdoc#, a.gtdesc, a.gttamt, b.*
INTO #wtf
FROM dds.stgArkonaGLPTRNS a
INNER JOIN  (  
  SELECT dl7, dl8, al3, glaccount, page, line
  FROM dds.zcb
  WHERE name = 'gmfs gross'
    AND dl4 = 'sales'
    AND dl5 = 'used'
--    AND al2 = 'cogs' 
    AND storecode = 'ry1') b on a.gtacct = b.glaccount
WHERE a.gtdate BETWEEN '04/01/2015' AND '04/30/2015'  

SELECT *
FROM #wtf
ORDER BY gtctl#


SELECT gtacct, al3, line, page, SUM(gttamt)
FROM #wtf
GROUP BY gtacct, al3, line, page

-- april uc COGS transactions
-- grouped BY acct, jrnl
SELECT gtjrnl, gtacct, al3, line, page, COUNT(*) trans_count, SUM(gttamt)
FROM #wtf
GROUP BY gtjrnl, gtacct, al3, line, page
ORDER BY gtacct, gtjrnl

SELECT * FROM #wtf WHERE gtacct = '164601' and gtjrnl = 'sca'

SELECT * FROM #wtf WHERE gtacct = '165000' AND gtjrnl = 'gje'

SELECT * FROM #wtf WHERE gtjrnl = 'gje'

-- april uc COGS transactions
-- grouped BY acct, jrnl, WHERE jrnl NOT VSU OR SVI
SELECT gtjrnl, gtacct, al3, line, page, COUNT(*) trans_count, SUM(gttamt)
FROM #wtf
WHERE gtjrnl NOT IN ('VSU','SVI')
GROUP BY gtjrnl, gtacct, al3, line, page
ORDER BY gtacct, gtjrnl

SELECT *
FROM dds.stgArkonaGLPTRNS
WHERE gtdate BETWEEN '04/01/2015' AND '04/30/2015'
AND gtacct IN (
  SELECT glaccount
  FROM dds.zcb
  WHERE name = 'gmfs gross'
  AND dl4 = 'sales'
  AND dl5 ='used'
  AND al2 = 'cogs')
  AND gtjrnl NOT IN ('VSU','SVI')
--  AND gtjrnl = 'pot'
ORDER BY gtacct, gtjrnl


SELECT MAX(ptdate) FROM stgArkonaPDPTDET WHERE ptdate <= curdate()