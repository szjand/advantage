DROP TABLE ucinv_tmp_avail_disposition;
CREATE TABLE ucinv_tmp_avail_disposition (
  thedate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  avail_fresh integer constraint NOT NULL,
  avail_aged integer constraint NOT NULL,
  avail_total integer constraint NOT NULL,
  sold_0_to_30 integer constraint NOT NULL,
  sold_31_to_60 integer constraint NOT NULL,
  sold_over_60 integer constraint NOT NULL,
  ws_from_avail integer constraint NOT NULL,
  constraint pk primary key (thedate,storecode)) IN database;
INSERT INTO ucinv_tmp_avail_disposition 
SELECT a.*, b.sold_0_to_30, b.sold_31_to_60, b.sold_over_60, b.ws_from_avail
  FROM (
  SELECT thedate, storecode, 
    SUM(CASE WHEN status = 'avail_fresh' THEN 1 ELSE 0 END) AS avail_fresh,
    SUM(CASE WHEN status = 'avail_aged' THEN 1 ELSE 0 END) AS avail_aged,
    COUNT(*) AS avail_total
  FROM ucinv_tmp_Avail_4
  WHERE status IN ('avail_fresh','avail_aged')
  GROUP BY thedate, storecode) a
LEFT JOIN (
  SELECT thedate, storecode,
    SUM(CASE WHEN disposition = 'sold 0-30' THEN 1 ELSE 0 END) AS sold_0_to_30,
    SUM(CASE WHEN disposition = 'sold 31-60' THEN 1 ELSE 0 END) AS sold_31_to_60,
    SUM(CASE WHEN disposition = 'sold > 60' THEN 1 ELSE 0 END) AS sold_over_60,
    SUM(CASE WHEN disposition = 'W/S from available' THEN 1 ELSE 0 END) AS ws_from_avail
  FROM ucinv_tmp_Avail_4
  WHERE status IN ('avail_fresh','avail_aged')
  GROUP BY thedate, storecode) b on a.storecode = b.storecode
    AND a.thedate = b.thedate;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_disposition','ucinv_tmp_avail_disposition.adi','storecode','storecode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_avail_disposition','ucinv_tmp_avail_disposition.adi','thedate','thedate','',2,512,'');      