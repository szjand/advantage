-- 7/25/15
-- 7/29/15 added honda bs ros
-- think i can generalize it be simply adding pot to both inventory AND cogs
DROP TABLE ucinv_tmp_recon_ros_1;
CREATE TABLE ucinv_tmp_recon_ros_1 (
  stocknumber cichar(20) constraint NOT NULL,
  ro cichar(9) constraint NOT NULL,
  ro_total integer constraint NOT NULL,
  constraint pk primary key (stocknumber,ro)) IN database;
INSERT INTO ucinv_tmp_recon_ros_1  
SELECT gtctl#, gtdoc#, sum(gttamt) AS gttamt
-- SELECT COUNT(*) -- 61100, 44734
FROM dds.stgArkonaGLPTRNS a
INNER JOIN ucinv_tmp_gross_2 b on a.gtctl# = b.stocknumber 
WHERE 
  CASE 
    WHEN gtacct IN ( -- inventory accts
      SELECT gl_account
      FROM dds.gmfs_accounts 
      WHERE page = 1 
        AND line IN (25,26) 
--        AND col = 2) THEN a.gtjrnl = 'svi'
        AND col = 2) THEN a.gtjrnl IN ('svi', 'pot')    
    WHEN gtacct IN ( -- uc cogs accts
      SELECT gl_account
      FROM dds.gmfs_accounts 
      WHERE page = 16
        AND department = 'uc'
--        AND account_type = '5') THEN a.gtjrnl IN ('svi','sca')   
        AND account_type = '5') THEN a.gtjrnl IN ('svi','sca','pot')     
  END
  AND EXISTS ( -- limit to those vehicles for which we have factRepairOrder records
    SELECT 1
    FROM dds.factRepairOrder
    WHERE ro = a.gtdoc#)   
GROUP BY gtctl#, gtdoc#
HAVING SUM(gttamt) <> 0;

-- recon sales
-- 7/26 leave out car wash
DROP TABLE ucinv_tmp_recon_sales;
CREATE TABLE ucinv_tmp_recon_sales (
  stocknumber cichar(20) constraint NOT NULL,
  ro cichar(9) constraint NOT NULL,
  gl_account cichar(10) constraint NOT NULL,
  description cichar(30) constraint NOT NULL,
  department cichar(2) constraint NOT NULL,
  amount integer constraint NOT NULL,
  cogs_account cichar(10) constraint NOT NULL,
  constraint pk primary key (stocknumber,ro,gl_account)) IN database;
INSERT INTO ucinv_tmp_recon_sales  
SELECT a.stocknumber, a.ro, b.gtacct, c.description, c.department, 
 -1 * cast(round(SUM(b.gttamt), 0) AS sql_integer) AS sales, c.cogs_account
FROM ucinv_tmp_recon_ros_1  a
LEFT JOIN dds.stgArkonaGLPTRNS b on a.ro = b.gtctl#
LEFT JOIN dds.gmfs_Accounts c on b.gtacct = c.gl_account
  AND c.page = 16
--  AND c.department IN ('bs','cw','ql','re','sd','pd')   
  AND c.department IN ('bs','ql','re','sd','pd') 
  AND c.account_type = '4' -- sales
WHERE c.gl_account IS NOT NULL
GROUP BY a.stocknumber, a.ro, b.gtacct, c.description, c.department, c.cogs_account;

EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_sales','ucinv_tmp_recon_sales.adi','stocknumber','stocknumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_sales','ucinv_tmp_recon_sales.adi','ro','ro','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_sales','ucinv_tmp_recon_sales.adi','gl_account','gl_account','',2,512,'');
--EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_sales','ucinv_tmp_recon_sales.adi','stocknumber','stocknumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_sales','ucinv_tmp_recon_sales.adi','department','department','',2,512,'');

-- 7/26 leave out car wash
DROP TABLE ucinv_tmp_recon_cogs;
CREATE TABLE ucinv_tmp_recon_cogs (
  stocknumber cichar(20) constraint NOT NULL,
  ro cichar(9) constraint NOT NULL,
  gl_account cichar(10) constraint NOT NULL,
  description cichar(30) constraint NOT NULL,
  department cichar(2) constraint NOT NULL,
  amount integer constraint NOT NULL,
  constraint pk primary key (stocknumber,ro,gl_account)) IN database;
INSERT INTO ucinv_tmp_recon_cogs
SELECT a.stocknumber, a.ro, b.gtacct, c.description, c.department,
 -1 *  cast(round(SUM(b.gttamt), 0) AS sql_integer) AS cogs
FROM ucinv_tmp_recon_ros_1   a
LEFT JOIN dds.stgArkonaGLPTRNS b on a.ro = b.gtctl#
LEFT JOIN dds.gmfs_Accounts c on b.gtacct = c.gl_account
  AND c.page = 16
--  AND c.department IN ('bs','cw','ql','re','sd','pd')  
  AND c.department IN ('bs','ql','re','sd','pd')  
  AND c.account_type = '5' -- cogs
WHERE c.gl_account IS NOT NULL 
GROUP BY a.stocknumber, a.ro, b.gtacct, c.description, c.department;

EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_cogs','ucinv_tmp_recon_sales.adi','stocknumber','stocknumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_cogs','ucinv_tmp_recon_sales.adi','ro','ro','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_cogs','ucinv_tmp_recon_sales.adi','gl_account','gl_account','',2,512,'');
--EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_sales','ucinv_tmp_recon_sales.adi','stocknumber','stocknumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_tmp_recon_cogs','ucinv_tmp_recon_sales.adi','department','department','',2,512,'');

-- *********** does NOT include shop supplies 16104C, 16105C ********************* --

DROP TABLE ucinv_tmp_fixed_gross_1;
CREATE TABLE ucinv_tmp_fixed_gross_1 (
  stocknumber cichar(20) constraint NOT NULL,
  sd_labor_sales integer default '0' constraint NOT NULL,
  sd_labor_cogs integer default '0' constraint NOT NULL,
  sd_labor_gross integer default '0' constraint NOT NULL, 
  bs_labor_sales integer default '0' constraint NOT NULL,
  bs_labor_cogs integer default '0' constraint NOT NULL,
  bs_labor_gross integer default '0' constraint NOT NULL,
  bs_paint_mat_sales integer default '0' constraint NOT NULL,
  bs_paint_mat_cogs integer default '0' constraint NOT NULL,
  bs_paint_mat_gross integer default '0' constraint NOT NULL,
  re_labor_sales integer default '0' constraint NOT NULL,
  re_labor_cogs integer default '0' constraint NOT NULL,
  re_labor_gross integer default '0' constraint NOT NULL,
  ql_labor_sales integer default '0' constraint NOT NULL,
  ql_labor_cogs integer default '0' constraint NOT NULL,
  ql_labor_gross integer default '0' constraint NOT NULL,
  parts_sales integer default '0' constraint NOT NULL,
  parts_cogs integer default '0' constraint NOT NULL,
  parts_gross integer default '0' constraint NOT NULL,
  constraint pk primary key (stocknumber)) IN database;
INSERT INTO ucinv_tmp_fixed_gross_1
-- 7/26 leave out car wash
SELECT a.stocknumber, -- , a.ro, b.department, b.amount AS sales, c.amount AS cogs
  SUM(CASE WHEN b.department = 'sd' THEN b.amount ELSE 0 END) AS sd_labor_sales,
  SUM(CASE WHEN b.department = 'sd' THEN c.amount ELSE 0 END) AS sd_labor_cogs,
  SUM(CASE WHEN b.department = 'sd' THEN b.amount + c.amount ELSE 0 END) AS sd_labor_gross,
  SUM(CASE WHEN b.department = 'bs' AND b.gl_account <> '147900' THEN b.amount ELSE 0 END) AS bs_labor_sales,
  SUM(CASE WHEN b.department = 'bs' AND c.gl_account <> '167900' THEN c.amount ELSE 0 END) AS bs_labor_cogs,
  SUM(CASE WHEN b.department = 'bs' AND c.gl_account <> '167900' THEN b.amount + c.amount ELSE 0 END) AS bs_labor_gross,
  SUM(CASE WHEN b.department = 'bs' AND b.gl_account = '147900' THEN b.amount ELSE 0 END) AS bs_paint_mat_sales,
  SUM(CASE WHEN b.department = 'bs' AND c.gl_account = '167900' THEN c.amount ELSE 0 END) AS bs_paint_mat_cogs,
  SUM(CASE WHEN b.department = 'bs' AND b.gl_account = '147900' THEN b.amount + c.amount ELSE 0 END) AS bs_paint_mat_gross,
  SUM(CASE WHEN b.department = 're' THEN b.amount ELSE 0 END) AS re_labor_sales,
  SUM(CASE WHEN b.department = 're' THEN c.amount ELSE 0 END) AS re_labor_cogs,
  SUM(CASE WHEN b.department = 're' THEN b.amount + c.amount ELSE 0 END) AS re_labor_gross,
--  SUM(CASE WHEN b.department = 'cw' THEN b.amount ELSE 0 END) AS cw_labor_sales,
--  SUM(CASE WHEN b.department = 'cw' THEN c.amount ELSE 0 END) AS cw_labor_cogs,
--  SUM(CASE WHEN b.department = 'cw' THEN b.amount + c.amount ELSE 0 END) AS cw_labor_gross,
  SUM(CASE WHEN b.department = 'ql' THEN b.amount ELSE 0 END) AS ql_labor_sales,
  SUM(CASE WHEN b.department = 'ql' THEN c.amount ELSE 0 END) AS ql_labor_cogs,
  SUM(CASE WHEN b.department = 'ql' THEN b.amount + c.amount ELSE 0 END) AS ql_labor_gross,
  SUM(CASE WHEN b.department = 'pd' THEN b.amount ELSE 0 END) AS parts_sales,
  SUM(CASE WHEN b.department = 'pd' THEN c.amount ELSE 0 END) AS parts_cogs,
  SUM(CASE WHEN b.department = 'pd' THEN b.amount + c.amount ELSE 0 END) AS parts_gross
-- SELECT *  
FROM ucinv_tmp_recon_ros_1 a
LEFT JOIN ucinv_tmp_recon_sales b on a.stocknumber = b.stocknumber AND a.ro = b.ro
LEFT JOIN ucinv_tmp_recon_cogs c on b.stocknumber = c.stocknumber AND b.ro = c.ro AND b.cogs_account = c.gl_account
GROUP BY a.stocknumber;

SELECT * FROM ucinv_tmp_fixed_gross_1

