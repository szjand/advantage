-- 7/29 body shop fixed

-- what about detail
-- at least for H7441A, it IS a Honda RO, 2716802, with opcode SUB
-- that references ry1 ro 16194140 WHERE accounting IS broken out
-- l
SELECT * FROM dds.stgArkonaSDPRHDR WHERE ptro# = '2716802'

SELECT * FROM ucinv_tmp_Sales_Activity WHERE storecode = 'ry2' AND re_labor_gross <> 0

SELECT COUNT(*),
  SUM(CASE WHEN re_labor_gross <> 0 THEN 1 ELSE 0 END) AS has_detail,
  SUM(CASE WHEN re_labor_gross = 0 THEN 1 ELSE 0 END) AS no_detail
FROM ucinv_tmp_Sales_Activity
WHERE storecode = 'ry2'

SELECT *
FROM dds.gmfs_accounts 
WHERE page = 16
  AND department = 'uc'
  AND account_type = '5'
  AND storecode = 'ry2'
  
  
SELECT *
FROM dds.gmfs_accounts 
where gl_account = '224600'

SELECT *
FROM dds.gmfs_accounts 
WHERE description LIKE '%INV%'


SELECT a.*, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt
FROM dds.gmfs_accounts a
INNER JOIN dds.stgArkonaGLPTRNS b on a.gl_Account = b.gtacct
WHERE b.gtdate BETWEEN '04/01/2015' AND '05/31/2015'
  AND a.page = 1
  AND a.line = 31
  AND a.storecode = 'ry2'
ORDER BY b.gtctl#  
  
  
ok, this shit IS confusing me
H7645A
16184774
2712891
  
-- 7/30
think i was just mind fucking on my lack of understanding of accounting

what i DO NOT get IS 2714774 IS for $195, 16189497 IS for $135 - what IS up with the $60 difference
what i need first IS the list of ros for honda detail recon
-- this appears to give it to me  
-- acct: 224600, jrnl: POT, doc: ry1 detail ro
-- what it does NOT give me IS the stocknumber
-- also a couple of exceptions: doc IS NOT an ro, doc IS NOT a detail ro (18035606)
SELECT a.*, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt
FROM dds.gmfs_accounts a
INNER JOIN dds.stgArkonaGLPTRNS b on a.gl_Account = b.gtacct
WHERE b.gtdate BETWEEN '04/01/2015' AND '04/30/2015'
  AND a.page = 1
  AND a.line = 31
  AND a.storecode = 'ry2'
  AND b.gtjrnl = 'POT'
ORDER BY gtdoc# 

SELECT a.*, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt,
(
  SELECT DISTINCT gtctl#
  FROM dds.stgArkonaGLPTRNS
  WHERE gtdoc# = b.gtctl#
    AND (
      gtacct IN (
        SELECT gl_account
        FROM dds.gmfs_accounts
        WHERE page = 1
        AND line = 31
        AND storecode = 'ry2')
      OR gtacct IN (
        SELECT gl_account
        FROM dds.gmfs_accounts 
        WHERE page = 1 
          AND line IN (25,26) 
          AND col = 2)
      OR gtacct IN (        
        SELECT gl_account
        FROM dds.gmfs_accounts 
        WHERE page = 16
          AND department = 'uc'  
          AND account_type = '5' )))
FROM dds.gmfs_accounts a
INNER JOIN dds.stgArkonaGLPTRNS b on a.gl_Account = b.gtacct
WHERE b.gtdate BETWEEN '04/01/2015' AND '04/30/2015'
  AND a.page = 1
  AND a.line = 31
  AND a.storecode = 'ry2'
  AND b.gtjrnl = 'POT'


SELECT *
FROM (
  SELECT a.*, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt
  FROM dds.gmfs_accounts a
  INNER JOIN dds.stgArkonaGLPTRNS b on a.gl_Account = b.gtacct
  WHERE b.gtdate BETWEEN '04/01/2015' AND '04/30/2015'
    AND a.page = 1
    AND a.line = 31
    AND a.storecode = 'ry2'
    AND b.gtjrnl = 'POT') x
LEFT JOIN ( -- this eliminates the non detail pot (bs, non-ro) but also misses some detail ros ** 
  SELECT gtctl#, gtdoc# -- stock, ry2 ro
  FROM dds.stgArkonaGLPTRNS a
  WHERE gtdoc# IN ( -- sublet ros to POT
    SELECT b.gtctl#
    FROM dds.gmfs_accounts a
    INNER JOIN dds.stgArkonaGLPTRNS b on a.gl_Account = b.gtacct
    WHERE b.gtdate BETWEEN '04/01/2015' AND '04/30/2015'
      AND a.page = 1
      AND a.line = 31
      AND a.storecode = 'ry2'
      AND b.gtjrnl = 'POT') 
    AND (
      gtacct IN ( -- inventory account
        SELECT gl_account
        FROM dds.gmfs_accounts 
        WHERE page = 1 
          AND line IN (25,26) 
          AND col = 2)
      OR
      gtacct IN (  
        SELECT gl_account
        FROM dds.gmfs_accounts 
        WHERE page = 16
          AND department = 'uc'  
          AND account_type = '5' )       
        )) y  on x.gtctl# = y.gtdoc#
        
**
bad
2714728(op:4/3 cl:4/8)/16189376(op:4/3 cl:4/3) H7834A del 4/2/15, detail done
SELECT *
FROM dds.stgArkonaGLPTRNS
WHERE gtctl# = '2714728' 

good
2715966/16192352    

bad : bad acct entry: 264400 (CST NEW OTHER PLANS NISSAN)
2715060/16190270

SELECT *
FROM dds.stgArkonaGLPTRNS
WHERE gtdoc# = '2715966'   
UNION
SELECT *
FROM dds.stgArkonaGLPTRNS
WHERE gtdoc# = '2715914' 

the problem IS
  on the good H6668B/2715966 IS to acct 224100 (inventory)
  on the bad H7834A/2714728 IS to acct 265100 (CST U/T-RTL RECOND)
