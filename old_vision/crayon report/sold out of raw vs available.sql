-- this might be CLOSE
-- ry1 6/305/15  pickup-large 34-36k doesn't seem to ADD up, it's the only
-- one i've checked so far

SELECT b.storecode, b.thedate, b.shape_and_size, b.price_band, 
  SUM(CASE WHEN c.storecode IS NULL THEN 0 ELSE 1 END) AS avail_vehicles
-- SELECT *  
FROM (
  SELECT a.storecode, a.thedate, a.shape_and_size, a.price_band, a.days_owned 
  FROM ucinv_tmp_avail_4 a
  WHERE a.thedate = sale_Date
    AND a.sold_from_status = 'raw') b
LEFT JOIN ucinv_tmp_avail_4 c on b.storecode = c.storecode
  AND b.thedate = c.thedate
  AND b.shape_and_size = c.shape_and_size
  AND b.price_band = c.price_band 
  AND c.status IN ('avail_fresh','avail_aged')   
WHERE b.storecode = 'ry1' 
  AND b.thedate = '06/30/2015'
  AND b.shape_and_size = 'pickup - large'
  AND b.price_band = '34-36k'
GROUP BY b.storecode, b.thedate, b.shape_and_size, b.price_band

-- hmm, 2 sold FROM raw, 1 sold FROM avail_aged  
SELECT *
from ucinv_tmp_avail_4
WHERE sale_date = thedate
  AND sale_Date = '06/30/2015'
  AND storecode = 'ry1'
  AND shape_and_size = 'pickup - large'
  AND price_band = '34-36k'  

-- so, it looks LIKE 3 were sold on 6/30, but only 2 availabe on that date, not
--   4 LIKE IN the top query, wy-dat
SELECT *
from ucinv_tmp_avail_4
WHERE sale_Date >= '06/30/2015'
  AND storecode = 'ry1'
  AND shape_and_size = 'pickup - large'
  AND price_band = '34-36k'    
  AND thedate = '06/30/2015'