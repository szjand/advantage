greg wants a version that takes only a date for input, 
return location, shape, size, shape/Size IN addition to the 
other values



DECLARE @date date;
@date = curdate() - 13;

SELECT x.*, y.shapeAndSize
FROM (
  SELECT e.priceband, e.pricefrom, e.location, e.shape, e.size, 
    coalesce(SUM(f.units), 0) AS previous30,
    coalesce(sum(round(1.0 * g.units/12, 1)), 0) AS previous365,
    COUNT(h.stocknumber) AS fresh,
    COUNT(i.stocknumber) AS aged
  FROM (
    SELECT *
    FROM (
      SELECT * 
      FROM ucinv_price_bands) a,
    (
      SELECT 'RY1' as location FROM system.iota
      UNION SELECT 'RY2' FROM system.iota
      UNION SELECT 'Market' FROM system.iota) b,
    (
      SELECT distinct shape
      FROM ucinv_shapes_and_sizes) c,  
    (
      SELECT distinct size
      FROM ucinv_shapes_and_sizes) d) e
  LEFT JOIN ucinv_sales_previous_30 f on f.thedate = @date
    AND f.saleType = 'Retail' 
    AND e.shape = f.vehicleShape
    AND e.size = f.vehicleSize 
    AND e.priceband = f.priceband 
    AND 
      CASE e.location  --collate ads_default_ci
        WHEN 'Market' THEN 1 = 1 
        ELSE e.location = f.storecode collate ads_default_ci
      END        
  LEFT JOIN ucinv_sales_previous_365 g on g.thedate = @date
    AND g.saleType = 'Retail' 
    AND e.shape = g.vehicleShape
    AND e.size = g.vehicleSize 
    AND e.priceband = g.priceband 
    AND 
      CASE e.location  --collate ads_default_ci
        WHEN 'Market' THEN 1 = 1 
        ELSE e.location = g.storecode collate ads_default_ci
      END  
  LEFT JOIN (
    SELECT a.stocknumber
    FROM ucinv_available a
    LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
      AND b.theDate = @date
    WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
      AND timestampdiff(sql_tsi_day, CAST(a.fromts AS sql_date), @date) <= 30) h on e.shape = h.vehicleShape
      AND e.size = h.vehicleSize
      AND e.priceband = h.priceband  
      AND 
        CASE e.location  --collate ads_default_ci
          WHEN 'Market' THEN 1 = 1 
          ELSE e.location = h.storecode collate ads_default_ci
        END    
  LEFT JOIN (
    SELECT a.stocknumber
    FROM ucinv_available a
    LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
      AND b.theDate = @date
    WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
      AND timestampdiff(sql_tsi_day, CAST(a.fromts AS sql_date), @date) > 30) i on e.shape = i.vehicleShape
      AND e.size = i.vehicleSize
      AND e.priceband = i.priceband  
      AND 
        CASE e.location  --collate ads_default_ci
          WHEN 'Market' THEN 1 = 1 
          ELSE e.location = i.storecode collate ads_default_ci
        END      

  LEFT JOIN (
    SELECT a.stocknumber
    FROM ucinv_pulled a
    LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
      AND thedate = @date
    WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) 
      BETWEEN a.fromts AND a.thruts) j on e.shape = j.vehicleSahpe
      AND e.size = j.vehicleSize
      AND e.priceband = j.priceband
      AND 
        CASE e.@location
          WHEN 'MARKET' THEN 1 = 1
          ELSE e.storeCode = j.storecode collate ads_default_ci
        END     
  GROUP BY e.priceband, e.pricefrom, e.location, e.shape, e.size) x
LEFT JOIN ucinv_shapes_and_sizes y on x.shape = y.shape
  AND x.size = y.size


















*/
DECLARE @date date;
DECLARE @location string;
DECLARE @shape string;
DECLARE @size string;
DECLARE @shapeAndSize string;
@date = (SELECT theDate FROM __input);
@shapeAndSize = 
  CASE
    WHEN (SELECT shapeAndSize FROM __input) = '' THEN 
    (SELECT shapeAndSize
      FROM ( 
        SELECT top 1 shapeAndSize, SUM(units)
        FROM ucinv_glump_ranking
        WHERE thedate = curdate()
          AND saletype = 'retail'
          GROUP BY shapeAndSize
        ORDER BY SUM(units) DESC) a)
     ELSE (SELECT shapeAndSize FROM __input)
   END; 

@location = 
  CASE
    WHEN (SELECT location FROM __input) = '' THEN 'MARKET'
    ELSE (SELECT upper(location) FROM __input)
  END;

@shape = (SELECT shape FROM ucinv_shapes_and_sizes WHERE shapeAndSize = @shapeAndSize);
@size = (SELECT size FROM ucinv_shapes_and_sizes WHERE shapeAndSize = @shapeAndSize);
  INSERT INTO __output    
  SELECT top 30 aa.priceband, coalesce(bb.fresh, 0) AS fresh, coalesce(cc.aged, 0) AS aged,
    coalesce(dd.sales30, 0) AS sales30, coalesce(ee.sales365, 0) AS sales365,
    coalesce(ff.pulled, 0) AS pulled, @shapeAndSize
  FROM ucinv_price_bands aa
  LEFT JOIN (
    SELECT b.priceband, COUNT(*) AS fresh
    --SELECT *
    FROM ucinv_available a
    LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
      AND thedate = @date
    WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
      AND timestampdiff(sql_tsi_day, CAST(a.fromts AS sql_date), @date) <= 30
      AND 
        CASE @location
          WHEN 'MARKET' THEN 1 = 1
          ELSE  storeCode = @location
        END 
      AND a.vehicleshape = @shape
      AND a.vehiclesize = @size
    GROUP BY b.priceband) bb on aa.priceband = bb.priceband 
  LEFT JOIN (
    SELECT b.priceband, COUNT(*) AS aged
    --SELECT *
    FROM ucinv_available a
    LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
      AND thedate = @date
    WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
      AND timestampdiff(sql_tsi_day, CAST(a.fromts AS sql_date), @date) > 30
      AND 
        CASE @location
          WHEN 'MARKET' THEN 1 = 1
          ELSE  storeCode = @location
        END 
      AND a.vehicleshape = @shape
      AND a.vehiclesize = @size
    GROUP BY b.priceband) cc on aa.priceband = cc.priceband   
  LEFT JOIN (
    SELECT b.priceband, COUNT(*) as pulled
    FROM ucinv_pulled a
    LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
      AND thedate = @date
    WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
      AND 
        CASE @location
          WHEN 'MARKET' THEN 1 = 1
          ELSE  storeCode = @location
        END 
      AND a.vehicleshape = @shape
      AND a.vehiclesize = @size
    GROUP BY b.priceband) ff on aa.priceband = ff.priceband    
  LEFT JOIN ( -- sales30  -- OUTER grouping necessary for market
    SELECT priceband, SUM(sales30) AS sales30
    FROM (
      SELECT priceBand, units AS sales30
      from ucinv_sales_previous_30 
      WHERE theDate = @date
        AND vehicleShape = @shape
        AND vehicleSize = @size
        AND 
          CASE @location
            WHEN 'MARKET' THEN 1 = 1
            ELSE  storeCode = @location
          END 
        AND saleType = 'Retail') x
      GROUP BY priceband) dd on aa.priceband = dd.priceband   
  LEFT JOIN ( -- sales365 -- OUTER grouping necessary for market
    SELECT priceband, SUM(sales365) AS sales365
    FROM (
      SELECT priceBand, round(1.0*units/12, 1) AS sales365
      FROM ucinv_sales_previous_365   
      WHERE theDate = @date
        AND vehicleShape = @shape
        AND vehicleSize = @size
        AND 
          CASE @location
            WHEN 'MARKET' THEN 1 = 1
            ELSE  storeCode = @location
          END 
        AND saleType = 'Retail') z
      GROUP BY priceband) ee on aa.priceband = ee.priceband           
  ORDER BY aa.priceFrom DESC;  
END IF;
