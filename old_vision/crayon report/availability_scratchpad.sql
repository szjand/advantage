  SELECT thedate, storecode, 
    TRIM(vehicleshape) + ' - ' + TRIM(vehiclesize) AS shape_and_size, priceband,
    units AS prev_30_retail
  FROM ucinv_sales_previous_30
  WHERE saletype = 'retail'
  
select * FROM ucinv_tmp_avail_2 a WHERE thedate BETWEEN '03/15/2015' - 30 AND '03/14/2015' AND storecode = 'ry1' AND shape_and_size = 'pickup - large' AND price_band = '00-6k' AND thedate = sale_date


SELECT a.thedate, a.storecode, b.sale_type, b.shape_and_size, b.price_band, 
  COUNT(*) AS units
FROM (
  SELECT thedate, 'RY1' AS storecode
  FROM dds.day
  WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1
  union
  SELECT thedate, 'RY2' AS storecode
  FROM dds.day
  WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1) a
LEFT JOIN (
  SELECT *
  FROM ucinv_tmp_avail_2
  WHERE thedate = sale_date) b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1  
    AND a.storecode = b.storecode collate ads_default_ci
GROUP BY a.thedate, a.storecode, b.sale_type, b.shape_and_size, b.price_band    


SELECT * FROM (
SELECT a.thedate, a.storecode, b.shape_and_size, b.price_band,
  SUM(CASE WHEN sale_type = 'retail' THEN 1 ELSE 0 END) AS prev_30_retail,
  SUM(CASE WHEN sale_type = 'wholesale' THEN 1 ELSE 0 END) AS prev_30_ws
FROM (
  SELECT thedate, 'RY1' AS storecode
  FROM dds.day
  WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1
  union
  SELECT thedate, 'RY2' AS storecode
  FROM dds.day
  WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1) a
LEFT JOIN (
  SELECT *
  FROM ucinv_tmp_avail_2
  WHERE thedate = sale_date) b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1  
    AND a.storecode = b.storecode collate ads_default_ci
GROUP BY a.thedate, a.storecode, b.shape_and_size, b.price_band    
) x
WHERE thedate = '03/15/2015' AND storecode = 'RY1' AND shape_and_size = 'pickup - large' AND price_band = '00-6k'


SELECT * FROM (

SELECT a.*, 
  coalesce(c.prev_30_retail, 0) AS prev_30_retail,
  coalesce(c.prev_30_ws, 0) AS prev_30_ws, 
  coalesce(d.prev_365_retail, 0) AS prev_365_retail,
  coalesce(e.prev_365_ws, 0) AS prev_365_ws
FROM ucinv_tmp_avail_5  a
LEFT JOIN (
  SELECT a.thedate, a.storecode, b.shape_and_size, b.price_band,
    SUM(CASE WHEN sale_type = 'retail' THEN 1 ELSE 0 END) AS prev_30_retail,
    SUM(CASE WHEN sale_type = 'wholesale' THEN 1 ELSE 0 END) AS prev_30_ws
  FROM (
    SELECT thedate, 'RY1' AS storecode
    FROM dds.day
    WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1
    union
    SELECT thedate, 'RY2' AS storecode
    FROM dds.day
    WHERE thedate BETWEEN '01/31/2014' AND curdate() - 1) a
  LEFT JOIN (
    SELECT *
    FROM ucinv_tmp_avail_2
    WHERE thedate = sale_date) b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1  
      AND a.storecode = b.storecode collate ads_default_ci
  GROUP BY a.thedate, a.storecode, b.shape_and_size, b.price_band) c on a.thedate = c.thedate 
      AND a.storecode = c.storecode collate ads_default_ci
      AND a.shape_and_size = c.shape_and_size
      AND a.price_band = c.price_band 
LEFT JOIN (
  SELECT thedate, storecode, 
    TRIM(vehicleshape) + ' - ' + TRIM(vehiclesize) AS shape_and_size, priceband,
    round(1.0 * units/12, 1) AS prev_365_retail
  FROM ucinv_sales_previous_365
  WHERE saletype = 'retail') d on a.thedate = d.thedate 
      AND a.storecode = d.storecode
      AND a.shape_and_size = d.shape_and_size
      AND a.price_band = d.priceband  
LEFT JOIN (
  SELECT thedate, storecode, 
    TRIM(vehicleshape) + ' - ' + TRIM(vehiclesize) AS shape_and_size, priceband,
    round(1.0 * units/12, 1) AS prev_365_ws
  FROM ucinv_sales_previous_365
  WHERE saletype = 'wholesale') e on a.thedate = e.thedate 
      AND a.storecode = e.storecode
      AND a.shape_and_size = e.shape_and_size
      AND a.price_band = e.priceband 
) x WHERE thedate = '03/15/2015' AND storecode = 'RY1' AND shape_and_size = 'pickup - large' AND price_band = '00-6k'
