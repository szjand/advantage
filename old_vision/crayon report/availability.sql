i/*
greg wants
for each day, 
avg fresh avail prev 30
avg aged avail prev 30
avg total avail prev 30

sales
  thedate
  prev30
  prev60
  prev90
  prev365
 
sold FROM avail - fresh
                  aged
          pulled
          raw                    
*/
SELECT a.thedate, 
  b.storecode, b.stocknumber, cast(b.fromts as sql_date) as dateAvail, 
  left(b.vehicleshape, 15) as shape, left(b.vehiclesize, 15) AS size,
  c.priceband,
  d.saledate, d.saleType, d.vehicleCost, d.vehiclePrice,
  d.vehicleprice - d.vehiclecost AS gross
FROM dds.day a
LEFT JOIN ucinv_available b on a.thedate BETWEEN CAST(b.fromts AS sql_date) 
  AND CAST(b.thruts AS sql_date)
LEFT JOIN ucinv_price_by_day c on a.thedate = c.thedate
  AND b.stocknumber = c.stocknumber  
LEFT JOIN ucinv_sales d on b.stocknumber = d.stocknumber
WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1

-- DROP TABLE #avail;
SELECT a.thedate, b.storecode, b.stocknumber, CAST(b.fromts AS sql_date) AS dateAvail, 
  d.shapeAndSize, c.priceBand,
--  left(b.vehicleshape, 15) as shape, 
--  left(b.vehiclesize, 15) AS size, c.priceband,
  a.thedate - CAST(b.fromts AS sql_date) AS daysAvail,
  case
    when a.thedate - CAST(b.fromts AS sql_date) < 31 THEN 'fresh'
    WHEN a.thedate - CAST(b.fromts AS sql_date) > 30 THEN 'aged'
  END 
INTO #avail  
FROM dds.day a
LEFT JOIN ucinv_available b on a.thedate BETWEEN CAST(b.fromts AS sql_date)  
  AND CAST(b.thruts AS sql_date)
LEFT JOIN ucinv_price_by_day c on a.thedate = c.thedate
  AND b.stocknumber = c.stocknumber  
LEFT JOIN ucinv_shapes_and_sizes d on b.vehicleshape = d.shape
  AND b.vehiclesize = d.size 
WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1

SELECT * FROM #avail 
WHERE storecode = 'ry1'
AND thedate = curdate() - 1
AND shapeandsize = 'pickup - large'
ORDER BY priceband

SELECT thedate, storecode, shapeandsize, priceband,
  SUM(CASE WHEN expr = 'fresh' THEN 1 ELSE 0 END) AS fresh,
  SUM(CASE WHEN expr = 'aged' THEN 1 ELSE 0 END) AS aged,
  COUNT(*) AS total
FROM #avail 
WHERE storecode = 'ry1'
AND thedate = curdate() - 2
AND shapeandsize = 'pickup - large'
GROUP BY thedate, storecode, shapeandsize, priceband


SELECT * FROM #avail


DROP TABLE ucinv_base;
CREATE TABLE ucinv_base (
  thedate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  priceBand cichar(10) constraint NOT NULL,
  shapeAndSize cichar(24) constraint NOT NULL,
  shape cichar(10) constraint NOT NULL,
  size cichar(10) constraint NOT NULL,
  fresh_avail integer default '0',
  aged_avail integer default '0',
  total_avail integer default '0',
  constraint pk primary key (thedate,storecode,priceBand,shapeAndSize)) IN database;
INSERT INTO ucinv_base (thedate,storecode,priceband,shapeandsize,shape,size) 
SELECT a.thedate, 'RY1', left(b.priceband, 10), left(c.shapeAndSize, 24),
  LEFT(c.shape, 10), LEFT(c.size, 10)
FROM dds.day a, ucinv_price_bands b, ucinv_shapes_and_sizes c
WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1
UNION
SELECT a.thedate, 'RY2', left(b.priceband, 10), left(c.shapeAndSize, 24),
  LEFT(c.shape, 10), LEFT(c.size, 10)
FROM dds.day a, ucinv_price_bands b, ucinv_shapes_and_sizes c
WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1; 

EXECUTE PROCEDURE sp_CreateIndex90('ucinv_base','ucinv_base.adi','thedate','thedate','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_base','ucinv_base.adi','storecode','storecode','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_base','ucinv_base.adi','priceband','priceband','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_base','ucinv_base.adi','shapeandsize','shapeandsize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_base','ucinv_base.adi','shape','shape','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_base','ucinv_base.adi','size','size','',2,512,'');

UPDATE ucinv_base
SET fresh_avail = x.fresh_avail,
    aged_avail = x.aged_avail,
    total_avail = x.total_avail
FROM (    
  select a.thedate, a.storecode, a.priceband, a.shapeandsize, coalesce(b.fresh_avail, 0) AS fresh_avail, 
    coalesce(b.aged_avail, 0) AS aged_avail, coalesce(b.total_avail, 0) AS total_avail 
  FROM ucinv_base a
  LEFT JOIN (
    SELECT thedate, storecode, shapeandsize, priceband,
      SUM(CASE WHEN expr = 'fresh' THEN 1 ELSE 0 END) AS fresh_avail,
      SUM(CASE WHEN expr = 'aged' THEN 1 ELSE 0 END) AS aged_avail,
      COUNT(*) AS total_avail
    FROM #avail 
    GROUP BY thedate, storecode, shapeandsize, priceband) b on a.thedate = b.thedate
      AND a.storecode = b.storecode
      AND a.priceband = b.priceband
      AND a.shapeandsize = b.shapeandsize) x  
WHERE ucinv_base.thedate = x.thedate
  AND ucinv_base.storecode = x.storecode
  AND ucinv_base.priceband = x.priceband
  AND ucinv_base.shapeandsize = x.shapeandsize;

SELECT * FROM ucinv_base
WHERE storecode = 'ry1'
  AND thedate = curdate() - 2
  AND shape = 'pickup'
  AND shapeandsize = 'pickup - large'

avg fresh avail prev 30
avg aged avail prev 30
avg total avail prev 30

SELECT a.thedate, a.thedate - 30 AS fromdate, a.thedate - 1 AS thrudate, b.*
FROM dds.day a
LEFT JOIN ucinv_base b on b.shapeandsize = 'pickup - large'
  AND b.priceband = '00-6k'
  AND b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
WHERE a.thedate BETWEEN '06/10/2015' AND curdate() - 1
  AND b.storecode = 'ry1'
ORDER BY priceband

DROP TABLE ucinv_tmp_avail_prev_30;
CREATE TABLE ucinv_tmp_avail_prev_30 (
  thedate date constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  priceband cichar(10) constraint NOT NULL,
  shapeandsize cichar(24) constraint NOT NULL,
  fresh_avail_prev_30_avg numeric(8,1) constraint NOT NULL,
  aged_avail_prev_30_avg numeric(8,1) constraint NOT NULL,
  total_avail_prev_30_avg numeric(8,1) constraint NOT NULL) IN database;
INSERT INTO ucinv_tmp_avail_prev_30 
SELECT thedate, storecode, priceband, shapeandsize, 
  round(SUM(fresh_avail)/30.0, 1) AS fresh_avail_prev_30_avg, 
  round(SUM(aged_avail)/30.0, 1)AS aged_avail_prev_30_avg, 
  round(SUM(total_avail)/30.0, 1) AS total_avail_prev_30_avg
FROM (
  SELECT a.thedate, a.thedate - 30 AS fromdate, a.thedate - 1 AS thrudate, b.*
  FROM dds.day a
  LEFT JOIN ucinv_base b on b.thedate BETWEEN a.thedate - 30 AND a.thedate - 1
  WHERE a.thedate BETWEEN '01/31/2014' AND curdate() - 1) x
--  WHERE a.thedate = curdate() - 2) x
GROUP BY thedate, storecode, priceband, shapeandsize;

SELECT *
FROM ucinv_base a
LEFT JOIN ucinv_tmp_avail_prev_30 b on a.thedate = b.thedate
  AND a.storecode = b.storecode
  AND a.priceband = b.priceband
  AND a.shapeandsize = b.shapeandsize

ALTER TABLE ucinv_base
ADD COLUMN fresh_avail_prev_30_avg numeric(8,1) default '0'
ADD COLUMN aged_avail_prev_30_avg numeric(8,1) default '0'
ADD COLUMN total_avail_prev_30_avg numeric(8,1) default '0';

UPDATE ucinv_base
SET fresh_avail_prev_30_avg = 0,
    aged_avail_prev_30_avg = 0,
    total_avail_prev_30_avg = 0;

UPDATE ucinv_base
SET fresh_avail_prev_30_avg = x.fresh_avail_prev_30_avg,
    aged_avail_prev_30_avg = x.aged_avail_prev_30_avg,
    total_avail_prev_30_avg = x.total_avail_prev_30_avg
FROM ( 
  SELECT *
  FROM ucinv_tmp_avail_prev_30) x
WHERE ucinv_base.storecode = x.storecode
  AND ucinv_base.thedate = x.thedate
  AND ucinv_base.priceband = x.priceband
  AND ucinv_base.shapeandsize = x.shapeandsize;
          
SELECT * FROM ucinv_base where storecode = 'ry1' ORDER BY thedate DESC 

SELECT * FROM ucinv_tmp_avail_prev_30

-- dps statuses
-- dcu
select a.fromts, a.thruts, b.*
FROM dps.VehicleInventoryItems a 
INNER JOIN  dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE a.VehicleInventoryItemID = '44a39568-7d95-4541-8fa7-e9ec192865dd'

-- raw   rawmaterials_rawmaterials
select a.fromts, a.thruts, b.*
FROM dps.VehicleInventoryItems a 
INNER JOIN  dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE a.VehicleInventoryItemID = '6f79a20c-4735-4b46-83e9-3381ea0fd54d'

-- avail
select a.fromts, a.thruts, b.*
FROM dps.VehicleInventoryItems a 
INNER JOIN  dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE a.VehicleInventoryItemID = '78f5de10-446f-4026-bfa4-3dc7bde54aab'

-- insp pend
select a.fromts, a.thruts, b.*
FROM dps.VehicleInventoryItems a 
INNER JOIN  dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE a.VehicleInventoryItemID = '65a0e046-27fd-4b2a-aad0-6cd2e96fba2f'

-- delivered
select a.fromts, a.thruts, b.*
FROM dps.VehicleInventoryItems a 
INNER JOIN  dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE a.VehicleInventoryItemID = '172995a1-e5ef-44a4-92e5-1468c0019d08'

select a.stocknumber, a.fromts, a.thruts, b.fromts, b.thruts
FROM dps.VehicleInventoryItems a 
INNER JOIN  dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'RawMaterials_RawMaterials'
--WHERE minute(a.fromts) <> minute(b.fromts)
WHERE extract(day FROM a.fromts) <> extract(day FROM b.fromts) 

/* 
the affect of IMWS
the VehicleInventoryItem IS created at time of WS, but raw materials status
IS NOT initiated until it IS booked at the destination store

unwinds: VehicleInventoryItems fromTS IS original acq, raw materials IS post unwind
*/

ok IN an effort to configure status precedence, DO statuses with dates, NOT timestamps
SELECT *
FROM (
SELECT a.stocknumber, CAST(b.fromts AS sql_date) AS status_from_date, 
  CAST(b.thruts AS sql_date) AS status_thru_date, 'available' AS status
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.status = 'RMFlagAV_Available'
  AND coalesce(CAST(a.thruts AS sql_date), CAST('12/31/9999' AS sql_date)) > '01/01/2014') a
LEFT JOIN (
SELECT a.stocknumber, CAST(b.fromts AS sql_date) AS status_from_date, 
  CAST(b.thruts AS sql_date) AS status_thru_date, 'pulled' AS status
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.status = 'RMFlagPulled_Pulled'  
  AND coalesce(CAST(a.thruts AS sql_date), CAST('12/31/9999' AS sql_date)) > '01/01/2014') b on a.stocknumber = b.stocknumber

/*
unwinds
SELECT year(soldts), month(soldts), COUNT(*)
FROM dps.vehiclesales
WHERE status = 'VehicleSale_SaleCanceled'
GROUP BY year(soldts), month(soldts)
*/

time to understand unwinds: multiple instances of raw, pulled & available are possible

h4036R 'f87efcf0-c99f-45a8-8f19-efc4e4c8a092'

17988XX 'ecac4322-f45f-4cbf-933a-c923ad4a921c' 4 instances of available
SELECT VehicleInventoryItemID 
FROM (
SELECT VehicleInventoryItemID, COUNT(*) 
FROM dps.VehicleInventoryItemStatuses
WHERE status = 'RMFlagAV_Available'
GROUP BY VehicleInventoryItemID
ORDER BY COUNT(*) DESC 

 
which should be ok IF i DO statuses AS a UNION
can NOT simoultaneously be IN multiple statuses
on any given day, a vehicle will have a single status


-- go with vehiclesales sold date (may be earlier than VehicleInventoryItems thrudate) 
-- SELECT stocknumber FROM (
-- base TABLE, stocknumbers are unique
-- base TABLE: inventory from date & sale date
-- DROP TABLE #base;
SELECT a.stocknumber, a.VehicleInventoryItemID, CAST(a.fromts AS sql_date) AS from_date,
  coalesce(CAST(b.soldts AS sql_date), CAST('12/31/9999' AS sql_date)) AS sale_date,
  substring(b.typ, position('_' IN b.typ) + 1, 9) AS sale_type
INTO #base  
FROM dps.VehicleInventoryItems a
LEFT JOIN dps.vehicleSales b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'VehicleSale_Sold' 
WHERE coalesce(CAST(a.thruts AS sql_date), CAST('12/31/9999' AS sql_date)) > '01/01/2014';

SELECT * FROM #base WHERE stocknumber = '21667xx'

-- so, on 1/1/15, here IS inventory
SELECT a.thedate, b.*, coalesce(bestprice,0) AS best_price, 
  coalesce(invoice,0) AS invoice, coalesce(priceband, 'Not Priced') AS priceband
FROM dds.day a
LEFT JOIN #base b on a.thedate BETWEEN b.from_date AND b.sale_date
LEFT JOIN ucinv_price_by_day c on a.thedate = c.thedate AND b.stocknumber = c.stocknumber
--WHERE a.thedate = '01/01/2015'
WHERE a.thedate BETWEEN '01/01/2014' AND curdate() -1
--  AND b.stocknumber = '21667xx'
  
SELECT *
FROM #base a
LEFT JOIN ucinv_price_by_day b on a.thedate = b.thedate AND a.stocknumber = b.stocknumber

SELECT *
FROM #avail
WHERE thedate = '06/18/2015'
  AND shapeandsize = 'Pickup - Large'
  AND storecode = 'ry1'
ORDER BY priceband, expr 