SELECT a.thedate, a.stocknumber, a.priceband,  
  e.description AS shape, f.description AS size,
  CASE 
    WHEN right(TRIM(a.stocknumber), 1) IN ('a','b','c','d','e') THEN 'trade'
    ELSE 'purchase'
  END AS source,
  CASE
    WHEN g.stocknumber is not null
      AND cast(timestampadd(sql_tsi_hour, 20, a.theDate) AS sql_timestamp) BETWEEN g.fromts AND g.thruts
      AND timestampdiff(sql_tsi_day, CAST(g.fromts AS sql_date), a.theDate) <= 30  THEN 'fresh'
    WHEN g.stocknumber is not null
      AND cast(timestampadd(sql_tsi_hour, 20, a.theDate) AS sql_timestamp) BETWEEN g.fromts AND g.thruts
      AND timestampdiff(sql_tsi_day, CAST(g.fromts AS sql_date), a.theDate) > 30  THEN 'aged'
    WHEN h.stocknumber IS NOT NULL
      AND cast(timestampadd(sql_tsi_hour, 20, a.thedate) AS sql_timestamp) BETWEEN h.fromts AND h.thruts
        THEN 'pulled'
    ELSE 'raw'              
  END AS status
FROM ucinv_price_by_day a
LEFT JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN dps.MakeModelClassifications d on c.make = d.make
  AND c.model = d.model
INNER JOIN dps.typDescriptions e on d.vehicleType = e.typ
INNER JOIN dps.typDescriptions f on d.vehicleSegment = f.typ
LEFT JOIN ucinv_available g on a.stocknumber = g.stocknumber
LEFT JOIN ucinv_pulled h on a.stocknumber = h.stocknumber
 

SELECT thedate, timestampdiff(sql_tsi_day, '01/01/2012', thedate) AS dayspassed
FROM dds.day a
WHERE thedate BETWEEN '01/01/2012' AND curdate()
  
SELECT n - 29 as theFrom, n as theThru
FROM dds.tally
WHERE mod(n, 30) = 0
AND n BETWEEN 1 AND  2000  

SELECT thedate, b.*
FROM dds.day a
  LEFT JOIN (
  SELECT n - 29 as theFrom, n as theThru
  FROM dds.tally
  WHERE mod(n, 30) = 0
  AND n BETWEEN 1 AND  2000  ) b on timestampdiff(sql_tsi_day, '01/30/2012', thedate) BETWEEN b.theFrom AND b.theThru
WHERE thedate BETWEEN '01/01/2012' AND curdate()

SELECT MIN(theDate) AS fromDate, MAX(theDate) AS thruDate
FROM (
  SELECT thedate, b.*
  FROM dds.day a
    LEFT JOIN (
    SELECT n - 29 as theFrom, n as theThru
    FROM dds.tally
    WHERE mod(n, 30) = 0
    AND n BETWEEN 1 AND  3000  ) b on timestampdiff(sql_tsi_day, '01/30/2012', thedate) BETWEEN b.theFrom AND b.theThru
  WHERE thedate BETWEEN '01/01/2012' AND curdate()) c
GROUP BY c.theThru  
HAVING MAX(thedate) - MIN(thedate) = 29

SELECT theDate, SUM(units) AS sold
FROM ucinv_sales_previous_30 
WHERE storecode = 'ry1'
  AND saletype = 'retail'
GROUP BY thedate  
ORDER BY SUM(units) DESC, thedate

-- this IS fine, but NOT what IS needed for the first cut
SELECT fromDate, thruDate, SUM(units), SUM(units)/ 30
FROM ucinv_sales_previous_30 aa
INNER JOIN (
  SELECT MIN(theDate) AS fromDate, MAX(theDate) AS thruDate
  FROM (
    SELECT thedate, b.*
    FROM dds.day a
      LEFT JOIN (
      SELECT n - 29 as theFrom, n as theThru
      FROM dds.tally
      WHERE mod(n, 30) = 0
      AND n BETWEEN 1 AND  3000  ) b on timestampdiff(sql_tsi_day, '01/30/2012', thedate) BETWEEN b.theFrom AND b.theThru
    WHERE thedate BETWEEN '01/01/2012' AND curdate()) c
  GROUP BY c.theThru  
  HAVING MAX(thedate) - MIN(thedate) = 29) bb on aa.thedate BETWEEN bb.fromdate AND bb.thrudate
WHERE aa.storecode = 'ry1'
  AND aa.saletype = 'retail'  
GROUP BY fromdate, thrudate  

-- this IS actually what IS needed
-- x axis IS one day increments starting with 1/30
-- each data point IS subsequently the next day

SELECT theDate, SUM(units) AS soldPrevious30
FROM ucinv_sales_previous_30 
WHERE storecode = 'ry1'
  AND saletype = 'retail'
GROUP BY thedate  
--ORDER BY SUM(units) DESC, thedate

SELECT theDate, COUNT(*) AS unitsOwned
FROM ucinv_price_by_day
GROUP BY thedate

mind fuck
USING ucinv_price_by_day
AS the base for inventory, based on from/thru on dps.VehicleInventoryItems 

-- vehicles IN price_by_day past sale date
SELECT *
FROM ucinv_sales a
INNER JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
WHERE b.thedate > a.saledate

-- aahhh fuck me, why so many sold vehicle not HAVING been IN tool
SELECT a.*, c.fromts, c.thruts, d.VehiclePricingTS
-- SELECT COUNT(*) -- ok, only 58 that don't exist IN VehicleInventoryItems 
FROM ucinv_sales a
LEFT JOIN  ucinv_price_by_day b on a.stocknumber = b.stocknumber
--LEFT JOIN dps.VehicleInventoryItems c on a.stocknumber = c.stocknumber
--LEFT JOIN dps.VehiclePricings d on c.VehicleInventoryItemID = d.VehicleInventoryItemID 
WHERE b.stocknumber IS NULL 
  AND c.stocknumber IS NULL 
ORDER BY saledate DESC 

-- 12/ articulate the issues
1. ark does NOT DO a deal for intra market ws
   so those are missing FROM factVehicleSale AND subs ucinv_sales
2. i am possibly stuck on asserting that ark sale date IS the base standard   
   possibly a misguided attempt to conform to the financial statement
3. IF a vehicle IS acq AND sold on the same day, what IF any inventory record EXISTS
   

-- 12/2/14
fundamental bad assumptions
IF a vehicle IS acq AND sold on the same day there will be no row IN ucinv_price_by_day 
  IF it IS priced before 8 pm
  
the whole factVehicleAcquisition conundrum  

any inventort data must NOT conflict with sales data, eg, no inventory data for
a vehicle past the sales.saledate

-- vehicles IN _pulled on OR past sale date
SELECT COUNT(*) -- 1095
-- SELECT *
FROM ucinv_sales a
LEFT JOIN ucinv_pulled b on a.stocknumber = b.stocknumber
  AND CAST(b.thruts AS sql_date) >= a.saledate
WHERE b.stocknumber IS NOT  NULL   

-- vehicles IN _available on OR past sale date
SELECT COUNT(*) -- 5472
-- SELECT *
FROM ucinv_sales a
LEFT JOIN ucinv_available b on a.stocknumber = b.stocknumber
  AND CAST(b.thruts AS sql_date) > a.saledate
WHERE b.stocknumber IS NOT  NULL   

-- on a given date a vehicle can be sold OR inventory, NOT both
-- sales takes precedence

-- IF the vehicle EXISTS IN ucinv_sales AND the status thru date IS >= to the
-- sale date, make the status thru date 11:59:59 on the day before the sale date
SELECT a.VehicleInventoryItemID, a.fromTS, 
  coalesce(a.thruTS,CAST('12/31/9999 01:00:00' AS sql_timestamp)), 
  g.saledate,
  CAST(coalesce(a.thruTS,CAST('12/31/9999 01:00:00' AS sql_timestamp)) AS sql_date),
  b.owningLocationID, 
  CASE b.owningLocationID
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1' 
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2' 
  END AS storeCode, left(trim(b.stocknumber), 9) AS stocknumber,
  e.description AS vehicleShape, f.description AS vehicleSize 
INTO #wtf  
-- SELECT COUNT(*) -- 7943
FROM dps.VehicleInventoryItemStatuses a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN dps.MakeModelClassifications d on c.make = d.make
  AND c.model = d.model
INNER JOIN dps.typDescriptions e on d.vehicleType = e.typ
INNER JOIN dps.typDescriptions f on d.vehicleSegment = f.typ
LEFT JOIN ucinv_sales g on b.stocknumber = g.stocknumber
WHERE a.category = 'RMFlagAV'
  AND b.owningLocationID <> '1B6768C6-03B0-4195-BA3B-767C0CAC40A6'
-- veh that were available, are NOT currently available,
-- but DO NOT show up IN sales, based on stocknumber
AND g.stocknumber IS NULL and a.thruts is not null ORDER BY a.thruts DESC

-- #wtf is
-- veh that were available, are NOT currently available,
-- but DO NOT show up IN ucinv_sales, based on stocknumber
unwind - dps.vehiclesales record _SaleCanceled AND current status other than available (23233PB, 21563B) 
imws
ws buffer (closes available?)

SELECT * FROM #wtf;

SELECT a.stocknumber, b.soldts, left(b.typ, 30), left(b.status, 30), left(b.soldto, 12), 
  left(c.name, 12),
  d.category, d.status, d.fromts, d.thruts, d.basistable
FROM #wtf a
LEFT JOIN dps.vehiclesales b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN dps.organizations c on b.soldto = c.partyid collate ads_default_ci
LEFT JOIN dps.VehicleInventoryItemStatuses d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.thruts IS NULL 
  AND d.category NOT LIKE '%ocess'
  
  
-- ws buffer (closes available?)
SELECT DISTINCT category FROM dps.VehicleInventoryItemStatuses

SELECT VehicleInventoryItemID, category, status
FROM dps.VehicleInventoryItemStatuses a
WHERE category LIKE 'RMFlag%'
  AND thruTS IS NULL 
  AND category NOT IN ('RMFlagIP','RMFlagIP','RMFlagWP','RMFlagTNA')
  AND EXISTS (
    SELECT 1
    FROM dps.VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
    AND category = 'RMFlagAV'
      AND thruts IS NULL)
ORDER BY VehicleInventoryItemID   
  
SELECT *
FROM dps.VehicleInventoryItemStatuses a
LEFT JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagAV'
WHERE a.category = 'RMFlagWSB'
  AND a.thruTS IS NULL 