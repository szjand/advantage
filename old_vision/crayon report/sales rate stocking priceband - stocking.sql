SELECT a.stocknumber, a.fromts, a.thruts, b.saledate, b.saletype
FROM dps.VehicleInventoryItems a
INNER JOIN ucinv_sales b on a.stocknumber = b.stocknumber
WHERE CAST(a.thruts AS sql_date) <> b.saledate
-- sale date tool vs arkona
SELECT 
  SUM(CASE WHEN CAST(a.thruts AS sql_date) = b.saledate THEN 1 ELSE 0 END) AS agree,
  SUM(CASE WHEN CAST(a.thruts AS sql_date) <> b.saledate THEN 1 ELSE 0 END) AS disagree
FROM dps.VehicleInventoryItems a
INNER JOIN ucinv_sales b on a.stocknumber = b.stocknumber


CREATE 
SELECT *
FROM ucinv_available

SELECT *
FROM ucinv_sales

need to include storecode, stocknumber, saledate, saletype, 

the kicker may be , i need the price on the date for which the data IS being requested
to get the priceband
SELECT a.*, b.stocknumber, e.description AS vehicleShape, f.description AS vehicleSize
-- SELECT COUNT(*)
FROM ucinv_available a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID collate ads_default_ci
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN dps.MakeModelClassifications d on c.make = d.make
  AND c.model = d.model
INNER JOIN dps.typDescriptions e on d.vehicleType = e.typ
INNER JOIN dps.typDescriptions f on d.vehicleSegment = f.typ

SELECT * FROM ucinv_available

SELECT a.VehicleInventoryItemID, a.vehiclePricingTS, b.amount
FROM dps.vehiclePricings a
INNER JOIN dps.vehiclePRicingDetails b on a.VehiclePricingID = b.VehiclePricingID 
INNER JOIN dps.typDescriptions c on b.typ = c.typ
  WHERE c.typ = 'VehiclePricingDetail_BestPrice'
  
-- greg wants shape AND size concatenated, so
CREATE TABLE ucinv_shapes_and_sizes (
  shape cichar(30) constraint NOT NULL,
  size cichar(30) constraint NOT NULL,
  shapeAndSize cichar(63) constraint NOT NULL,
  constraint pk primary key (shape, size)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_shapes_and_sizes','ucinv_shapes_and_sizes.adi','shape','shape','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_shapes_and_sizes','ucinv_shapes_and_sizes.adi','size','size','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_shapes_and_sizes','ucinv_shapes_and_sizes.adi','shapeAndSize','shapeAndSize','',2,512,'');
INSERT INTO ucinv_shapes_and_sizes (shape,size,shapeAndSize)
SELECT DISTINCT c.description, b.description, TRIM(c.description) + ' - ' + trim(b.description)
FROM dps.MakeModelClassifications a
INNER JOIN dps.typDescriptions b on a.vehicleSegment = b.typ
INNER JOIN dps.typDescriptions c on a.vehicleType = c.typ;  
  

  -- for the crayon
  -- available vehicles for the date, location, size & shape: need the price
  -- eventually base TABLE will be pricebands, forget that initially, 
  -- once i have the above i can inject price bands AS the base TABLE
  
alter PROCEDURE ucinv_get_price_bands
   ( 
      theDate DATE,
      location CICHAR ( 6 ),
      shapeAndSize CICHAR ( 63 ),
      priceBand CICHAR ( 20 ) OUTPUT,
      freshAvailable Integer OUTPUT,
      agedAvailable Integer OUTPUT,
      sales30 Integer OUTPUT,
      sales365 DOUBLE ( 15 ) OUTPUT,
      pulled Integer OUTPUT,
      shapeAndSize CICHAR ( 63 ) OUTPUT
   ) 
BEGIN 
/*
still needs pulled,
transition to concatenated shape AND size
too slow

BY default, loads page with current date,current top glump,market 
11/26 added IF for curdate
now need to reformulate previous date avail/pull to already have the price
EXECUTE PROCEDURE ucinv_get_price_bands(curdate(),'ry1','car - large');
*/
DECLARE @date date;
DECLARE @location string;
DECLARE @shape string;
DECLARE @size string;
DECLARE @shapeAndSize string;
@date = (SELECT theDate FROM __input);
@shapeAndSize = 
  CASE
    WHEN (SELECT shapeAndSize FROM __input) = '' THEN 
    (SELECT shapeAndSize
      FROM ( 
        SELECT top 1 shapeAndSize, SUM(units)
        FROM ucinv_glump_ranking
        WHERE thedate = curdate()
          AND saletype = 'retail'
          GROUP BY shapeAndSize
        ORDER BY SUM(units) DESC) a)
     ELSE (SELECT shapeAndSize FROM __input)
   END; 

@location = 
  CASE
    WHEN (SELECT location FROM __input) = '' THEN 'MARKET'
    ELSE (SELECT upper(location) FROM __input)
  END;

@shape = (SELECT shape FROM ucinv_shapes_and_sizes WHERE shapeAndSize = @shapeAndSize);
@size = (SELECT size FROM ucinv_shapes_and_sizes WHERE shapeAndSize = @shapeAndSize);

IF @date = curdate() THEN 
INSERT into __output
SELECT top 30 k.priceband, k.fresh, k.aged, coalesce(j.sales30, 0) AS sales30,
  coalesce(m.sales365, 0), coalesce(n.pulled, 0) AS pulled, @shapeAndSize
FROM ( -- available
  SELECT bb.priceband, coalesce(aged, 0) AS aged, coalesce(fresh, 0) AS fresh
  FROM  ucinv_price_bands bb
  LEFT JOIN ( -- available
    SELECT priceband,
      SUM(CASE WHEN avail = 'aged' THEN 1 ELSE 0 END) AS Aged,
      SUM(CASE WHEN avail = 'fresh' THEN 1 ELSE 0 END) AS Fresh
    FROM (  
      SELECT 
        (SELECT priceband FROM ucinv_price_bands WHERE amount BETWEEN pricefrom AND pricethru) AS priceband,
        CASE 
          WHEN curdate() - CAST(a.fromTS AS sql_date) <= 30 THEN 'fresh'
          ELSE 'aged'
        END as avail     
      FROM dps.VehicleInventoryItemStatuses  a  
      INNER JOIN dps.VehicleInventoryItems aa on a.VehicleInventoryItemID  = aa.VehicleInventoryItemID 
      LEFT JOIN dps.VehiclePricings b on aa.VehicleInventoryItemID = b.VehicleInventoryItemID 
        AND b.VehiclePricingTS = (
          SELECT MAX(VehiclePricingTS)
          FROM dps.VehiclePricings 
          WHERE VehicleInventoryItemID = b.VehicleInventoryItemID
            AND CAST(VehiclePricingTS AS sql_date) < curdate())
      LEFT JOIN dps.VehiclePricingDetails c on b.VehiclePricingID = c.VehiclePricingID
      INNER JOIN dps.VehicleItems d on aa.VehicleItemID = d.VehicleItemID 
      INNER JOIN dps.MakeModelClassifications e on d.make = d.make
        AND d.model = e.model
      INNER JOIN dps.typDescriptions f on e.vehicleType = f.typ
      INNER JOIN dps.typDescriptions g on e.vehicleSegment = g.typ  
      WHERE a.category = 'RMFlagAV'
        AND a.thruts IS NULL  
        AND c.typ = 'VehiclePricingDetail_BestPrice'
        AND f.description = @shape
        AND g.description = @size
        AND 
          CASE @location
            WHEN 'RY1' THEN aa.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'rydells')
            WHEN 'RY2' THEN aa.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'honda cartiva')
            WHEN 'MARKET' THEN 1 = 1
          END) x GROUP BY priceband) y on bb.priceband = y.priceband) k     
LEFT JOIN ( -- sales30  -- OUTER grouping necessary for market
  SELECT priceband, SUM(sales30) AS sales30
  FROM (
    SELECT priceBand, units AS sales30
    from ucinv_sales_previous_30 
    WHERE theDate = @date
      AND vehicleShape = @shape
      AND vehicleSize = @size
      AND 
        CASE @location
          WHEN 'MARKET' THEN 1 = 1
          ELSE  storeCode = @location
        END 
      AND saleType = 'Retail') x
    GROUP BY priceband) j on k.priceband = j.priceband   
LEFT JOIN ( -- sales365 -- OUTER grouping necessary for market
  SELECT priceband, SUM(sales365) AS sales365
  FROM (
    SELECT priceBand, round(1.0*units/12, 1) AS sales365
    FROM ucinv_sales_previous_365   
    WHERE theDate = @date
      AND vehicleShape = @shape
      AND vehicleSize = @size
      AND 
        CASE @location
          WHEN 'MARKET' THEN 1 = 1
          ELSE  storeCode = @location
        END 
      AND saleType = 'Retail') z
    GROUP BY priceband) m on k.priceband = m.priceband   
LEFT JOIN ( -- pulled
  SELECT priceband, COUNT(*) AS pulled
  FROM (
    SELECT 
      (SELECT priceband FROM ucinv_price_bands WHERE amount BETWEEN pricefrom AND pricethru) AS priceband
    FROM dps.VehicleInventoryItemStatuses  a  
    LEFT JOIN dps.VehicleInventoryItems aa on a.VehicleInventoryItemID  = aa.VehicleInventoryItemID 
    LEFT JOIN dps.VehiclePricings b on aa.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND b.VehiclePricingTS = ( -- most recent price
        SELECT top 1 VehiclePricingTS
        FROM dps.VehiclePricings 
        WHERE VehicleInventoryItemID = b.VehicleInventoryItemID
        ORDER BY VehiclePricingTS DESC )
    LEFT JOIN dps.VehiclePricingDetails c on b.VehiclePricingID = c.VehiclePricingID
    INNER JOIN dps.VehicleItems d on aa.VehicleItemID = d.VehicleItemID 
    INNER JOIN dps.MakeModelClassifications e on d.make = d.make
      AND d.model = e.model
    INNER JOIN dps.typDescriptions f on e.vehicleType = f.typ
    INNER JOIN dps.typDescriptions g on e.vehicleSegment = g.typ  
    WHERE a.category = 'RMFlagPulled'
      AND a.thruts IS NULL  
      AND c.typ = 'VehiclePricingDetail_BestPrice'
      AND f.description = @shape
      AND g.description = @size
      AND 
        CASE @location
          WHEN 'RY1' THEN aa.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'rydells')
          WHEN 'RY2' THEN aa.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'honda cartiva')
          WHEN 'MARKET' THEN 1 = 1
        END) h
    GROUP BY priceband) n  on k.priceband = n.priceband
ORDER BY k.priceband DESC;

ELSE 
INSERT into __output
SELECT top 30 k.priceband, k.fresh, k.aged, coalesce(j.sales30, 0) AS sales30,
  coalesce(m.sales365, 0), coalesce(n.pulled, 0) AS pulled, @shapeAndSize
FROM ( -- available
  SELECT h.priceband, 
    SUM(CASE i.avail WHEN 'Fresh' THEN 1 ELSE 0 END) AS Fresh,
    SUM(CASE i.avail WHEN 'Aged' THEN 1 ELSE 0 END) AS Aged, 
    COUNT(*)
  FROM ucinv_price_bands h
  LEFT JOIN (
    SELECT a.vehicleShape, a.vehicleSize, c.amount,
      CASE 
        WHEN @date - CAST(a.fromTS AS sql_date) <= 30 THEN 'Fresh'
        ELSE 'Aged'
      END avail
    FROM ucinv_available a
    INNER JOIN dps.VehicleInventoryItems aa on a.stocknumber = aa.stocknumber
    LEFT JOIN dps.VehiclePricings b on aa.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND b.VehiclePricingTS = (
        SELECT MAX(VehiclePricingTS)
        FROM dps.VehiclePricings 
        WHERE VehicleInventoryItemID = b.VehicleInventoryItemID
          AND CAST(VehiclePricingTS AS sql_date) < @date)
    LEFT JOIN dps.VehiclePricingDetails c on b.VehiclePricingID = c.VehiclePricingID
      AND c.typ = 'VehiclePricingDetail_BestPrice'
    WHERE @date BETWEEN CAST(a.fromts AS sql_date) AND CAST(a.thruts AS sql_date)
      AND a.vehicleShape = @shape
      AND a.vehicleSize = @size
      AND 
        CASE @location
          WHEN 'RY1' THEN a.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'rydells')
          WHEN 'RY2' THEN a.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'honda cartiva')
          WHEN 'MARKET' THEN 1 = 1
        END) i on i.amount BETWEEN h.priceFrom AND h.priceThru
    GROUP BY h.priceband) k     
LEFT JOIN ( -- sales30  -- OUTER grouping necessary for market
  SELECT priceband, SUM(sales30) AS sales30
  FROM (
    SELECT priceBand, units AS sales30
    from ucinv_sales_previous_30 
    WHERE theDate = @date
      AND vehicleShape = @shape
      AND vehicleSize = @size
      AND 
        CASE @location
          WHEN 'MARKET' THEN 1 = 1
          ELSE  storeCode = @location
        END 
      AND saleType = 'Retail') x
    GROUP BY priceband) j on k.priceband = j.priceband   
LEFT JOIN ( -- sales365 -- OUTER grouping necessary for market
  SELECT priceband, SUM(sales365) AS sales365
  FROM (
    SELECT priceBand, round(1.0*units/12, 1) AS sales365
    FROM ucinv_sales_previous_365   
    WHERE theDate = @date
      AND vehicleShape = @shape
      AND vehicleSize = @size
      AND 
        CASE @location
          WHEN 'MARKET' THEN 1 = 1
          ELSE  storeCode = @location
        END 
      AND saleType = 'Retail') z
    GROUP BY priceband) m on k.priceband = m.priceband   
LEFT JOIN ( -- pulled
  SELECT h.priceband, COUNT(*) AS pulled
  FROM ucinv_price_bands h
  LEFT JOIN (
    SELECT a.vehicleShape, a.vehicleSize, c.amount
    FROM ucinv_pulled a
    INNER JOIN dps.VehicleInventoryItems aa on a.stocknumber = aa.stocknumber
    LEFT JOIN dps.VehiclePricings b on aa.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND b.VehiclePricingTS = (
        SELECT MAX(VehiclePricingTS)
        FROM dps.VehiclePricings 
        WHERE VehicleInventoryItemID = b.VehicleInventoryItemID
          AND CAST(VehiclePricingTS AS sql_date) < @date)
    LEFT JOIN dps.VehiclePricingDetails c on b.VehiclePricingID = c.VehiclePricingID
      AND c.typ = 'VehiclePricingDetail_BestPrice'
    WHERE @date BETWEEN CAST(a.fromts AS sql_date) AND CAST(a.thruts AS sql_date)
      AND a.vehicleShape = @shape
      AND a.vehicleSize = @size
      AND 
        CASE @location
          WHEN 'RY1' THEN a.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'rydells')
          WHEN 'RY2' THEN a.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'honda cartiva')
          WHEN 'MARKET' THEN 1 = 1
        END ) i on i.amount BETWEEN h.priceFrom AND h.priceThru
    WHERE i.vehicleShape IS NOT NULL   
  GROUP BY h.priceband) n  on k.priceband = n.priceband
ORDER BY k.priceband DESC;
END IF;
END;




    
-- pulled
SELECT * FROM ucinv_pulled
WHERE vehicleShape = 'Pickup'
  AND vehicleSize = 'Large'
  AND owninglocationid = (SELECT partyid FROM dps.organizations WHERE name = 'rydells')
  AND curdate() BETWEEN CAST(fromTS AS sql_date) AND CAST(thruTS AS sql_date)

DROP TABLE ucinv_pulled;  
CREATE TABLE ucinv_pulled (
  VehicleInventoryItemID cichar(38) constraint NOT NULL,
  fromTS timestamp constraint NOT NULL,
  thruTS timestamp constraint NOT NULL,
  owningLocationID char(38) constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  stockNumber cichar(9) constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  constraint PK primary key (VehicleInventoryItemID,fromTS)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','VehicleInventoryItemID', 
   'VehicleInventoryItemID','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','fromTS', 
   'fromTS','',2,512,'');     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','thruTS', 
   'thruTS','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','owningLocationID', 
   'owningLocationID','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','stockNumber', 
   'stockNumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','vehicleShape', 
   'vehicleShape','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','vehicleSize', 
   'vehicleSize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_pulled','ucinv_pulled.adi','storeCode', 
   'storeCode','',2,512,'');   
   
INSERT INTO ucinv_pulled(VehicleInventoryItemID,fromTs,thruTs,
  owningLocationID, storeCode, stockNumber, vehicleShape, vehicleSize)
SELECT a.VehicleInventoryItemID, a.fromTS, 
  coalesce(a.thruTS,CAST('12/31/9999 01:00:00' AS sql_timestamp)), 
  b.owningLocationID, 
  CASE b.owningLocationID
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1' 
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2' 
  END AS storeCode,  left(trim(b.stocknumber), 9) AS stocknumber,
  e.description AS vehicleShape, f.description AS vehicleSize 
-- SELECT COUNT(*) -- 7943
FROM dps.VehicleInventoryItemStatuses a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN dps.MakeModelClassifications d on c.make = d.make
  AND c.model = d.model
INNER JOIN dps.typDescriptions e on d.vehicleType = e.typ
INNER JOIN dps.typDescriptions f on d.vehicleSegment = f.typ
WHERE a.category = 'RMFlagPulled'
  AND b.owningLocationID <> '1B6768C6-03B0-4195-BA3B-767C0CAC40A6';     
   

alter PROCEDURE ucinv_get_shape_and_size_ordered_list (
  theDate date,
  location cichar(6),
  shapeAndSize cichar(63) output)
BEGIN
/*
EXECUTE PROCEDURE ucinv_get_shape_and_size_ordered_list (curdate(), 'ry2');
*/
DECLARE @date date;
DECLARE @location string;
@location = 
  CASE
    WHEN (SELECT location FROM __input) = '' THEN 'MARKET'
    ELSE (SELECT upper(location) FROM __input)
  END;
@date = (SELECT theDate FROM __input);
INSERT INTO __output  
SELECT shapeAndSize
FROM (
  SELECT top 30 shapeAndSize, SUM(units) AS units
  FROM ucinv_glump_ranking
  WHERE thedate = @date
    AND 
      CASE @location
        WHEN 'MARKET' THEN 1 = 1
        ELSE storecode = @location
      END 
    AND saletype = 'retail'
  GROUP BY shapeAndSize
  ORDER BY SUM(units) DESC) x;
END;  
    
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
-- 11/25
-- shit it IS the same issue, what IS the price band of a pulled vehicle
-- thinking of going towards:
/*
for each day since 1/1/2011
each owned vehicle will be on one of these statuses:
  owned AND NOT priced
  raw materials
  pulled
  avail fresh
  avail aged
will have to sort out real time versus a previous date
date     vehicle   status   price   shape    size   price band  
this might give me what i am looking for, easy AND fast to roll up
AND fast and easy access to a list of vehicles IN any of the categories
*/
-- 
SELECT timestampdiff(sql_tsi_day, CAST(fromts AS sql_date), CAST(thruts AS sql_date)))
FROM ucinv_pulled

SELECT MIN(fromts)
FROM ucinv_pulled

SELECT COUNT(*) FROM (
SELECT a.thedate, COUNT(*)
FROM dds.day a
LEFT JOIN ucinv_pulled b on a.thedate BETWEEN CAST(fromts AS sql_date) AND CAST(thruts AS sql_date)
WHERE a.thedate BETWEEN '01/01/2011' AND curdate()
GROUP BY a.thedate
) x

SELECT a.*, b.status, b.fromts, b.thruts
-- SELECT DISTINCT b.status
FROM ucinv_pulled a
LEFT JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID collate ads_default_ci 
  AND b.category NOT IN ('RawMaterials','RMFlagIP','RMFlagWP','MechanicalReconProcess','BodyReconProcess','AppearanceReconProcess','RMFlagPulled')
WHERE a.thruts > now()
  AND b.fromts <= a.thruts
  AND coalesce(b.thruts, CAST('12/31/9999 01:00:00' AS sql_timestamp)) > a.fromts
ORDER BY a.VehicleInventoryItemID 

SELECT a.*, b.status, b.fromts, b.thruts
-- SELECT DISTINCT b.status
FROM ucinv_pulled a
LEFT JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID collate ads_default_ci 
  AND b.category = 'RMFlagSB'
WHERE a.thruts > now()
ORDER BY a.VehicleInventoryItemID 