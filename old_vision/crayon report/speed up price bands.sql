
this looks good, probably DO a UNION with ucinv_pulled
pk: date,stocknumber,status

SELECT d.*,
  (SELECT priceband
    FROM ucinv_price_bands
    WHERE d.price BETWEEN priceFrom AND priceThru) AS priceband
FROM(
  SELECT a.thedate, b.stocknumber, b.vehicleShape, b.vehicleSize, 
    CASE b.owninglocationid 
      WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
      WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
    END AS storecode,
    timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_Date), a.thedate) AS days,
    CASE
      WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_Date), a.thedate) BETWEEN 0 AND 30 THEN 'fresh'
      ELSE 'aged'
    END AS status,
  (SELECT amount
    FROM #wtf g
    WHERE stocknumber = b.stocknumber
      AND VehiclePricingTS = (
        SELECT MAX(VehiclePricingTS)
        FROM #wtf 
        WHERE stocknumber = g.stocknumber
          AND VehiclePricingTS < CAST(timestampadd(sql_tsi_hour,20,a.thedate) AS sql_timestamp))) AS price 
  FROM dds.day a
  LEFT JOIN ucinv_available b on CAST(timestampadd(sql_tsi_hour,20,a.thedate) AS sql_timestamp) BETWEEN b.fromts AND b.thruts
  WHERE thedate BETWEEN '01/01/2012' AND curdate() - 1) d

SELECT d.*,
  (SELECT priceband
    FROM ucinv_price_bands
    WHERE d.price BETWEEN priceFrom AND priceThru) AS priceband
FROM(
  SELECT a.thedate, b.stocknumber, b.vehicleShape, b.vehicleSize, 
    CASE b.owninglocationid 
      WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1'
      WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2'
    END AS storecode,
    timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_Date), a.thedate) AS days,
    'pulled' status,
  (SELECT amount
    FROM #wtf g
    WHERE stocknumber = b.stocknumber
      AND VehiclePricingTS = (
        SELECT MAX(VehiclePricingTS)
        FROM #wtf 
        WHERE stocknumber = g.stocknumber
          AND VehiclePricingTS < CAST(timestampadd(sql_tsi_hour,20,a.thedate) AS sql_timestamp))) AS price 
  FROM dds.day a
  LEFT JOIN ucinv_pulled b on CAST(timestampadd(sql_tsi_hour,20,a.thedate) AS sql_timestamp) BETWEEN b.fromts AND b.thruts
  WHERE thedate BETWEEN '01/01/2012' AND curdate() - 1) d
   
  
SELECT * FROM ucinv_pulled

SELECT * FROM #wtf

SELECT stocknumber, VehiclePricingTS
FROM #wtf
GROUP BY stocknumber, VehiclePricingTS
HAVING COUNT(*) > 1
ORDER BY VehiclePricingTS DESC 

DROP TABLE #wtf;  
SELECT a.stocknumber, c.VehiclePricingTS, d.amount
INTO #wtf
FROM dps.VehicleInventoryItems a
-- JOIN to the appropriate ucinv table
--INNER JOIN ucinv_available b on a.stocknumber = b.stocknumber
INNER JOIN ucinv_pulled b on a.stocknumber = b.stocknumber
INNER JOIN dps.VehiclePricings c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
INNER JOIN dps.VehiclePricingDetails d on c.VehiclePricingID = d.VehiclePricingID 
WHERE d.typ = 'VehiclePricingDetail_BestPrice'
GROUP BY a.stocknumber, c.VehiclePricingTS, d.amount

-- 11/27 thinking just DO a date-vehicle-price TABLE
-- this looks really promising
-- shows each pricing, whether the price changed OR NOT
-- one row for each date, stocknumber WHILE IN inventory
-- first run, for ALL dates took -- uh oh, after a couple minutes failed with
--   2146 unable to DO ORDER BY (from help: Verify that the disk contains enough space for the sorting.)
--   TRY it for smaller SET (2014), that worked, took 3 minutes
-- what about intransit, INNER JOIN may fuck that up OR, this IS only vehicles
-- with prices, IF NOT IN here, THEN no price
-- i am also a little gitchy about basing this ALL on stocknumber

-- ok, put it IN a TABLE, one year at a time
-- daily UPDATE
DELETE FROM ucinv_price_by_day WHERE thedate = curdate() - 1;
INSERT INTO ucinv_price_by_day(thedate,stocknumber,pricingTS,bestprice,
  invoice,dayssincepriced)
SELECT a.thedate, left(trim(b.stocknumber), 9), c.VehiclePricingTS, 
  SUM(CASE WHEN d.typ = 'VehiclePricingDetail_BestPrice' THEN cast(d.amount as sql_integer) END) AS bestPrice,
  SUM(CASE WHEN d.typ = 'VehiclePricingDetail_Invoice' THEN cast(d.amount as sql_integer) END) AS invoice,
  timestampdiff(sql_tsi_day, CAST(VehiclePricingTS AS sql_date), a.thedate) AS days
FROM dds.day a
-- INNER JOIN should be ok, there IS inventory on every date
INNER JOIN dps.VehicleInventoryItems b on a.thedate 
    between CAST(b.fromTS AS sql_date) AND 
      cast(coalesce(thruts, CAST('12/31/9999 20:00:00' AS sql_timestamp)) AS sql_date)
  AND b.owninglocationid IN ('B6183892-C79D-4489-A58C-B526DF948B06','4CD8E72C-DC39-4544-B303-954C99176671')
INNER JOIN dps.VehiclePricings c on b.VehicleInventoryItemID = c.VehicleInventoryItemID   
  AND c.VehiclePricingTS = ( -- the price at 8PM of theDate
    SELECT MAX(VehiclePricingTS)
    FROM dps.VehiclePricings 
    WHERE VehicleInventoryItemID = c.VehicleInventoryItemID 
      AND VehiclePricingTS < CAST(timestampadd(sql_tsi_hour,20,a.thedate) AS sql_timestamp))
INNER JOIN dps.VehiclePricingDetails d on c.VehiclePricingID = d.VehiclePricingID 
--WHERE a.thedate BETWEEN '01/01/2014' AND curdate() - 1
--WHERE a.thedate BETWEEN '01/01/2012' AND '05/31/2012' 
--WHERE a.thedate BETWEEN '06/01/2012' AND '09/30/2012' 
--WHERE a.thedate BETWEEN '10/01/2012' AND '12/31/2012'
-- AND the daily updae
WHERE a.thedate = curdate()- 1
-- AND b.stocknumber = '22986A' 
GROUP BY a.thedate, b.stocknumber, c.VehiclePricingTS

SELECT thedate, stocknumber
-- SELECT *
FROM ucinv_price_by_day
GROUP BY thedate, stocknumber
HAVING COUNT(*) > 1

SELECT *
FROM ucinv_price_by_day
WHERE year(thedate) = 2012

CREATE TABLE ucinv_price_by_day (
  theDate date, 
  stocknumber cichar(9),
  pricingTS timestamp,
  bestPrice integer,
  invoice integer,
  daysSincePriced integer) IN database;
-- after doing the inserts

ALTER TABLE ucinv_price_by_day
ADD COLUMN priceBand cichar(20);
UPDATE ucinv_price_by_day
SET priceband = (
  SELECT priceband
  FROM ucinv_price_bands
  WHERE ucinv_price_by_day.bestprice BETWEEN priceFrom AND priceThru)
WHERE theDate = curdate() - 1  
  
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_price_by_day','ucinv_price_by_day.adi','PK','theDate;stocknumber','',2051,512,'');  
EXECUTE PROCEDURE sp_ModifyTableProperty ('ucinv_price_by_day','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR','');
  
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_price_by_day','ucinv_price_by_day.adi','theDate','theDate','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_price_by_day','ucinv_price_by_day.adi','stocknumber','stocknumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90('ucinv_price_by_day','ucinv_price_by_day.adi','priceBand','priceBand','',2,512,'');

  
  
so, what DO we have now
DO i want/need to shape ucinv_pulled/availble LIKE ucinv_price_by_day?
lets figger out whati need to produce
core focus IS on pricebands page for a date other than curdate()
on a date for each shapeandsize,priceband and location need COUNT of fresh,aged,pulled,sales30,sales365

SELECT * FROM ucinv_available

DECLARE @date date;
DECLARE @shapeAndSize string;
DECLARE @location string;
@date = curdate() -6;
@shapeAndSize = 'pickup - large';
@location = 'RY1';

SELECT b.priceband, COUNT(*)
FROM ucinv_available a
LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
  AND thedate = @date
WHERE @date BETWEEN CAST(a.fromts AS sql_date) AND CAST(a.thruts AS sql_date)
  AND vehicleShape = 'pickup'
  AND vehicleSize = 'large'
  AND owninglocationid = (SELECT partyid FROM dps.organizations WHERE name = 'rydells')
ORDER BY a.stocknumber

-- SELECT a.priceband
SELECT *
FROM ucinv_price_bands a
INNER JOIN ucinv_available b on  @date BETWEEN CAST(b.fromts AS sql_date) AND CAST(b.thruts AS sql_date)
LEFT JOIN ucinv_price_by_day c on b.stocknumber = c.stocknumber
  AND a.priceband = c.priceband
  AND c.thedate = @date
WHERE c.thedate IS NOT NULL   
ORDER BY priceFrom DESC  


SELECT * FROM ucinv_price_by_day WHERE stocknumber = '23297a'

SELECT * 
FROM ucinv_pulled  a
LEFT JOIN ucinv_price_by_day b on a.thedate = b.thedate
  AND a.stocknumber = b.stocknumber

SELECT thedate, COUNT(*) FROM (
SELECT a.thedate, b.*
FROM dds.day a
LEFT JOIN ucinv_pulled b on cast(timestampadd(sql_tsi_hour, 20, a.thedate) AS sql_timestamp) BETWEEN b.fromts AND b.thruts
WHERE thedate BETWEEN '01/01/2012' AND curdate() - 1
) x GROUP BY thedate

SELECT * FROM ucinv_sales_previous_30


FFFFFFFFFFFFFUUUUUUUUUUUUUUUUUUCCCCCCCCCCCCCCCCCCCCKKKKKKKKKKKKKKKKKKKKKKKK
i am NOT seeing it

SELECT * FROM ucinv_sales_previous_30
SELECT * FROM ucinv_price_by_day  -- date, stk#, priceband  -- no shape/size, but also AS pricTS, invoice, daysSincePRiced
SELECT * FROM ucinv_available  -- NOT date driven, stocknumber, shape/size, from/thru
SELECT * FROM ucinv_pulled  -- same AS available

so how about a ucinv_inventory_by_day TABLE

date storecode  stocknumber  shape   size   status  daysinstatus   price  dayssincepriced  priceband      units

would/should easily (AND quickly) for any date give me counts for status/shapesize/priceband

ALL this conveniently ignoring the whole slew of difficulties with factAcquisition
AND relies exclusively on the tool for info an statuses
which begs the question of sorting out incoming (tna etc) AND outgoing (salesbuffer)

SELECT priceBand, COUNT(*) 
FROM ucinv_price_by_day
WHERE thedate = '11/10/2014'
GROUP BY priceband


--------------------------------------------------------------------------------------------
fuck it, get back to fixing the stored proc

SELECT *
FROM ucinv_sales_previous_30

DECLARE @date date;
DECLARE @shape string;
DECLARE @size string;
DECLARE @location string;

@date = curdate() - 130;
@shape = 'pickup';
@size = 'large';
@location = 'RY1';

/*
reqd enhancements
  avail, pulled ADD storecode
  
*/  


SELECT aa.priceband, coalesce(bb.fresh, 0) AS fresh, coalesce(cc.aged, 0) AS aged,
  coalesce(dd.sales30, 0) AS sales30, coalesce(ee.sales365, 0) AS sales365,
  coalesce(ff.pulled, 0) AS pulled
FROM ucinv_price_bands aa
LEFT JOIN (
  SELECT b.priceband, COUNT(*) AS fresh
  --SELECT *
  FROM ucinv_available a
  LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
    AND thedate = @date
  WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
    AND timestampdiff(sql_tsi_day, CAST(a.fromts AS sql_date), @date) <= 30
    AND storeCode = @location
    AND a.vehicleshape = @shape
    AND a.vehiclesize = @size
  GROUP BY b.priceband) bb on aa.priceband = bb.priceband 
LEFT JOIN (
  SELECT b.priceband, COUNT(*) AS aged
  --SELECT *
  FROM ucinv_available a
  LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
    AND thedate = @date
  WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
    AND timestampdiff(sql_tsi_day, CAST(a.fromts AS sql_date), @date) > 30
    AND storeCode = @location
    AND a.vehicleshape = @shape
    AND a.vehiclesize = @size
  GROUP BY b.priceband) cc on aa.priceband = cc.priceband   
LEFT JOIN (
  SELECT b.priceband, COUNT(*) as pulled
  FROM ucinv_pulled a
  LEFT JOIN ucinv_price_by_day b on a.stocknumber = b.stocknumber
    AND thedate = @date
  WHERE cast(timestampadd(sql_tsi_hour, 20, @date) AS sql_timestamp) BETWEEN a.fromts AND a.thruts
    AND storeCode = @location
    AND a.vehicleshape = @shape
    AND a.vehiclesize = @size
  GROUP BY b.priceband) ff on aa.priceband = ff.priceband    
LEFT JOIN ( -- sales30  -- OUTER grouping necessary for market
  SELECT priceband, SUM(sales30) AS sales30
  FROM (
    SELECT priceBand, units AS sales30
    from ucinv_sales_previous_30 
    WHERE theDate = @date
      AND vehicleShape = @shape
      AND vehicleSize = @size
      AND storecode = 'RY1'
--        CASE @location
--          WHEN 'MARKET' THEN 1 = 1
--          ELSE  storeCode = @location
--        END 
      AND saleType = 'Retail') x
    GROUP BY priceband) dd on aa.priceband = dd.priceband   
LEFT JOIN ( -- sales365 -- OUTER grouping necessary for market
  SELECT priceband, SUM(sales365) AS sales365
  FROM (
    SELECT priceBand, round(1.0*units/12, 1) AS sales365
    FROM ucinv_sales_previous_365   
    WHERE theDate = @date
      AND vehicleShape = @shape
      AND vehicleSize = @size
      AND storecode = 'RY1'
--        CASE @location
--          WHEN 'MARKET' THEN 1 = 1
--          ELSE  storeCode = @location
--        END 
      AND saleType = 'Retail') z
    GROUP BY priceband) ee on aa.priceband = ee.priceband           
ORDER BY aa.priceFrom DESC 