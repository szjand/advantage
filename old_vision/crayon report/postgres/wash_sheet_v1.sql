﻿select base_stock_number, sum(sales_gross) as sales_gross, 
  sum(fi_gross) as fi_gross, sum(recon_gross) as recon_gross,
  sum(sales_gross+fi_gross+recon_gross) as total_gross, 
  count(*) as vehicles
from ( 
SELECT a.base_stock_number, b.stocknumber, b.sales_gross, b.fi_gross, b.recon_gross
FROM (  
  SELECT LEFT(stocknumber, 5) AS base_stock_number
  FROM greg.ucinv_tmp_sales_activity
  WHERE position('A' IN stocknumber) <> 0
    AND position('N' IN stocknumber) = 0
    AND position('X' IN stocknumber) = 0
    AND position('Z' IN stocknumber) = 0
    AND position('G' IN stocknumber) = 0  
    AND position('P' IN stocknumber) = 0 
  GROUP BY LEFT(stocknumber, 5)) a  
LEFT JOIN (
  SELECT i.*, LEFT(stocknumber, 5) AS base_stock_number
  FROM greg.ucinv_tmp_sales_activity i) b ON a.base_stock_number = b.base_stock_number) x
group by base_stock_number 
having count(*) > 1 -- multiples only
order by count(*) desc

-- let's look at one
select *
from greg.ucinv_tmp_sales_activity
where stocknumber like '%25494%'

-- let's see them all
select m.*, n.stocknumber, n.make, n.model, n.sales_gross, n.fi_gross, 
  n.recon_gross, n.sales_gross+n.fi_gross+n.recon_gross  as veh_total_gross
from (
  select base_stock_number, sum(sales_gross) as sales_gross, 
    sum(fi_gross) as fi_gross, sum(recon_gross) as recon_gross,
    sum(sales_gross+fi_gross+recon_gross) as total_gross, 
    count(*) as vehicles
  from ( 
  SELECT a.base_stock_number, b.stocknumber, b.sales_gross, b.fi_gross, b.recon_gross
  FROM (  
    SELECT LEFT(stocknumber, 5) AS base_stock_number
    FROM greg.ucinv_tmp_sales_activity
    WHERE position('A' IN stocknumber) <> 0
      AND position('N' IN stocknumber) = 0
      AND position('X' IN stocknumber) = 0
      AND position('Z' IN stocknumber) = 0
      AND position('G' IN stocknumber) = 0  
      AND position('P' IN stocknumber) = 0 
    GROUP BY LEFT(stocknumber, 5)) a  
  LEFT JOIN (
    SELECT i.*, LEFT(stocknumber, 5) AS base_stock_number
    FROM greg.ucinv_tmp_sales_activity i) b ON a.base_stock_number = b.base_stock_number) x
  group by base_stock_number 
  having count(*) > 1) m -- only multiples
left join greg.ucinv_tmp_sales_activity n on m.base_stock_number = left(n.stocknumber, 5)
order by vehicles desc, left(n.stocknumber, 5)       

-- what was the original new vehicle?
select m.*, n.stocknumber, n.make, n.model, n.sales_gross, n.fi_gross, 
  n.recon_gross, n.sales_gross+n.fi_gross+n.recon_gross  as veh_total_gross, 
  p.*
from (
  select base_stock_number, sum(sales_gross) as sales_gross, 
    sum(fi_gross) as fi_gross, sum(recon_gross) as recon_gross,
    sum(sales_gross+fi_gross+recon_gross) as total_gross, 
    count(*) as vehicles
  from ( 
  SELECT a.base_stock_number, b.stocknumber, b.sales_gross, b.fi_gross, b.recon_gross
  FROM (  
    SELECT LEFT(stocknumber, 5) AS base_stock_number
    FROM greg.ucinv_tmp_sales_activity
    WHERE position('A' IN stocknumber) <> 0
      AND position('N' IN stocknumber) = 0
      AND position('X' IN stocknumber) = 0
      AND position('Z' IN stocknumber) = 0
      AND position('G' IN stocknumber) = 0  
      AND position('P' IN stocknumber) = 0 
    GROUP BY LEFT(stocknumber, 5)) a  
  LEFT JOIN (
    SELECT i.*, LEFT(stocknumber, 5) AS base_stock_number
    FROM greg.ucinv_tmp_sales_activity i) b ON a.base_stock_number = b.base_stock_number) x
  group by base_stock_number 
  having count(*) > 1) m -- only multiples
left join greg.ucinv_tmp_sales_activity n on m.base_stock_number = left(n.stocknumber, 5)
left join greg.ncinv_vehicle_sale o on m.base_stock_number = o.stocknumber
left join greg.dimvehicle p on o.vehiclekey = p.vehiclekey
order by vehicles desc, left(n.stocknumber, 5)   




