﻿-- greg's complaint
-- discoverd that ucinv_ext_glptrns nightly update had not been running
-- that took care of most of it, but there are still a LARGE number of units with no purchase amount
-- somehow, i missed a shitload when backfilling, i guess
select *
from greg.used_vehicle_daily_snapshot
where the_date = '2016-05-31'
    and store_code = 'RY1'
    and plg_running = 0
  

select * from greg.ucinv_ext_glptrns where gtctl_ in ('25767xx','26049ab')  



select * from greg.used_vehicle_daily_snapshot a left join greg.ucinv_ext_glptrns b on a.stocknumber = b.gtctl_ where stocknumber = '25767xx'

/*  GLPTRNS */                        
FIELD    Field Def      VALUE      Generated From                
GTDTYP   Doc Type        B          Deals                
GTDTYP   Doc Type        C          Checks                
GTDTYP   Doc Type        D          Deposits                
GTDTYP   Doc Type        I          Inventory Purchase/Stock In            
GTDTYP   Doc Type        J          Conversion or GJE                
GTDTYP   Doc Type        O          Invoice/PO Entry                
GTDTYP   Doc Type        P          Parts Ticket                
GTDTYP   Doc Type        R          Cash Receipts                
GTDTYP   Doc Type        S          Service Tickets                
GTDTYP   Doc Type        W          Handwrittn Checks                
GTDTYP   Doc Type        X          Bank Rec (Fee, Service Charges, etc.)                       
GTTYPE   Record Type                Other*   *Any account that does not require a special reconciliation program.             
GTTYPE   Record Type     $          Entry to a Bank Account              
GTTYPE   Record Type     C          Entry from a Cash Drawer              
GTTYPE   Record Type     M          Other*                
GTTYPE   Record Type     P          Entry to/from Payables              
GTTYPE   Record Type     R          Entry to/from Receiveables    

-- inventory without cost
-- drop table inv;
create temp table inv as
select distinct stocknumber, from_Date, sale_date 
from greg.used_Vehicle_daily_snapshot  --where plg_purchase <> 0 order by the_Date desc limit 100
where plg_purchase_running = 0
  and store_code = 'RY1'
  and the_date <> sale_date;
create index on inv(stocknumber);

drop table gl;
create temp table gl as
select gtctl_, gtpost, gtdate, gtdtyp, gttype, gtjrnl, gttamt, gtdesc 
from greg.ucinv_ext_glptrns 
WHERE (
  (gtjrnl = 'vsu' and gtctl_ <> gtdoc_ and gttamt > 0)
  or
  (gtjrnl in ('vsn', 'eft', 'gje', 'pvu') and gttamt > 0));
create index on gl(gtctl_);

/* inpmast not really any help
select c.*, d.inpmast_vehicle_cost
from (
select stocknumber, sum(gttamt)
from inv a
left join gl b on a.stocknumber = gtctl_
group by a.stocknumber) c
left join dds.ext_inpmast d on c.stocknumber = d.inpmast_Stock_number
*/

select stocknumber, from_date, sale_date, sum(gttamt), min(gtdate), max(gtdate)
from inv a
left join gl b on a.stocknumber = gtctl_
group by a.stocknumber, from_date, sale_date

select *
from (
select stocknumber, from_date, sale_date, sum(gttamt), min(gtdate), max(gtdate)
from inv a
left join gl b on a.stocknumber = gtctl_
group by a.stocknumber, from_date, sale_date) x
where stocknumber in ('22043xx','24269RA','24128A')

select *
from greg.used_vehicle_daily_snapshot
where stocknumber in ('22043xx','24269RA','24128A')



            CREATE TEMPORARY TABLE wtf
            as
            select x.*,
              sum( plg_purchase) over w as plg_purchase_running,
              sum( plg_writedown) over w as plg_writedown_running,
              sum( plg_misc) over w as plg_misc_running,
              sum( plg_recon) over w as plg_recon_running,
              sum( plg_recon +  plg_purchase::integer + plg_writedown::integer + plg_misc::integer) over w as plg_running,
              case
                when best_price > 0 then
                  best_price - sum( plg_recon::integer +  plg_purchase::integer +  plg_writedown::integer +  plg_misc::integer) over w
                else 0
              END as plg
            from (
              select m.stocknumber, m.the_date,
                coalesce(o.plg_purchase, 0) as plg_purchase,
                coalesce(o.plg_writedown, 0) as plg_writedown,
                coalesce(o.plg_misc, 0) as plg_misc,
                coalesce(n.amount, 0) as plg_recon,
                m.best_price
              from greg.used_vehicle_daily_snapshot m
              left join ( -- recon
                select gtdate, gtctl_, sum(gttamt::integer) as amount
                from greg.ucinv_ext_glptrns
                where gtjrnl in ('SCA','SVI','AFM','POT') group by gtdate, gtctl_)n on m.stocknumber = n.gtctl_
                and m.the_date = n.gtdate
              left join (
                select a.*, coalesce(b.amount::integer, 0) as plg_purchase,
                  coalesce(c.amount::integer, 0) as plg_writedown, coalesce(d.amount::integer, 0) as plg_misc
                from (
                  select min(the_date) as the_date, stocknumber
                  from greg.used_vehicle_daily_snapshot
                  where store_code = 'RY1'
                  group by stocknumber) a
                left join ( -- purchase
                  select gtctl_, sum(amount) as amount
                  from (
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsu' and gtctl_ <> gtdoc_ group by gtctl_ having sum(gttamt) > 0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsn' group by gtctl_ having sum(gttamt) > 0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'eft' group by gtctl_ having sum(gttamt) > 1.0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'cdh' group by gtctl_ having sum(gttamt) <> 0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'gje' and gtdesc not like 'WRITE%' group by gtctl_ having sum(gttamt) > 0
                    union
                    select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'pvu' group by gtctl_ having sum(gttamt) > 0
                  ) a
                  group by gtctl_) b on a.stocknumber = b.gtctl_
                left join ( --writedown
                  select gtctl_, sum(gttamt) as amount
                  from greg.ucinv_ext_glptrns
                  where gtpost = 'y' and (gtjrnl = 'WTD' or (gtjrnl = 'GJE' and gtdesc like 'WRITE%'))
                  group by gtctl_
                  having sum(gttamt) < 0) c on a.stocknumber = c.gtctl_
                left join ( -- misc
                  select gtctl_, sum(gttamt) as amount from
                  greg.ucinv_ext_glptrns
                  where gtpost = 'y'
                    and gtjrnl in ('CRC','PCA')
                  group by gtctl_
                  having sum(gttamt) > 0) d on a.stocknumber = d.gtctl_) o on m.stocknumber = o.stocknumber
                  and m.the_date = o.the_date
              where m.stocknumber in (select stocknumber from inv)) x
            window w as (partition by stocknumber order by the_date);

select * from wtf a 

select * from wtf a
where exists (
  select 1
  from wtf
  where plg_purchase_running = 0
    and stocknumber = a.stocknumber)


            UPDATE greg.used_vehicle_daily_snapshot a
            set plg_purchase = x.plg_purchase,
                plg_writedown = x.plg_writedown,
                plg_misc = x.plg_misc,
                plg_recon = x.plg_recon,
                plg_purchase_running = x.plg_purchase_running,
                plg_writedown_running = x.plg_writedown_running,
                plg_misc_running = x.plg_misc_running,
                plg_recon_running = x.plg_recon_running,
                plg_running = x.plg_running,
                plg = x.plg
            from wtf x
            where a.stocknumber = x.stocknumber
              and a.the_date = x.the_date;         

