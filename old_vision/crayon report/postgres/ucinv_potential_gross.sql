﻿select * 
from greg.ucinv_tmp_avail_4
where sale_date - from_date between 20 and 40 
  and thedate <> sale_date
order by stocknumber, thedate
limit 1000


select *
from greg.ucinv_tmp_avail_4 a
where (sale_date - from_date between 25 and 45 or sale_date > current_Date)
  and thedate <> sale_date
  and exists (
    select 1 
    from greg.ucinv_tmp_avail_4
    where stocknumber = a.stocknumber
      and sale_date >  05/01/2016 )
order by from_Date
limit 10000

      

create index on greg.ucinv_ext_glptrns (gtco_);
create index on greg.ucinv_ext_glptrns (gtctl_);
create index on greg.ucinv_ext_glptrns (gtdate);
create index on greg.ucinv_ext_glptrns (gtjrnl);
create index on greg.ucinv_ext_glptrns (gtdoc_);

-- change this to group on date, journal, control only, exclude voids
-- drop table if exists greg.ucinv_gl;
-- create table if not exists greg.ucinv_gl (
--   the_date date not null,
--   journal citext not null,
--   control citext not null,
--   doc citext,
--   refer citext,
--   amount numeric(12,2) not null,
--   void citext not null);

drop table if exists greg.ucinv_gl;
create table if not exists greg.ucinv_gl (
  the_date date not null,
  journal citext not null,
  control citext not null,
  amount numeric(12,2) not null,
  constraint ucinv_gl_pk primary key (the_date,journal,control));

create index on greg.ucinv_gl (the_date);
create index on greg.ucinv_gl (journal);
create index on greg.ucinv_gl (control);


-- change this to group on date, journal, control only, exclude voids
-- insert into greg.ucinv_gl 
-- SELECT gtdate, gtjrnl, gtctl_, gtdoc_, gtref_, SUM(gttamt) AS gttamt, gtpost
-- FROM greg.ucinv_ext_glptrns
-- GROUP BY gtjrnl, gtdate, gtctl_, gtdoc_, gtref_, gtpost;

insert into greg.ucinv_gl 
SELECT gtdate, gtjrnl, gtctl_, SUM(gttamt) AS gttamt
FROM greg.ucinv_ext_glptrns
where gtpost = 'Y'
GROUP BY gtdate, gtjrnl, gtctl_;

select journal, count(*)
from greg.ucinv_gl
group by journal


select a.thedate, a.stocknumber, a.from_Date, a.sale_date, a.best_price, a.sold_amount,
  b.the_date, b.journal, b.doc, b.refer, b.amount
from greg.ucinv_tmp_avail_4 a
left join greg.ucinv_gl b on a.stocknumber = b.control
  and a.thedate = b.the_date
  and b.void <>  V 
  and b.amount <> 0
where sale_date between  04/01/2016  and  04/30/2016 
  and sale_type =  Retail 
  and storecode =  RY1 
  and thedate <> sale_date
order by stocknumber, thedate
limit 1000






-- </ sort out journals ---------------------------------------------------- </

-- journals per stocknumber
select *
from (
  select stocknumber
  from greg.ucinv_tmp_avail_4
  WHERE sale_type =  'Retail'
    and storecode = 'RY1'
  group by stocknumber) a
left join (
  select control, count(*) as journals
  from (
    select control, journal
    from greg.ucinv_gl
    group by control, journal) x
  group by control) b on a.stocknumber = b.control
order by journals desc   

-- entries per journals
select journal, count(*), min(control), max(control)
from greg.ucinv_gl
where control in (
  select stocknumber
  from greg.ucinv_tmp_avail_4)
and void <> 'V'  
group by journal
order by journal

CRC;   99: ?? (auction reimburse for transport, ...)
SCA;  369: recon
VSN; 3146: purchase (trade in on new car: control<>doc)
SVI;26703: recon
EFT;   42: when + then purchase, when - sale
PCA;   81: confusing, many are parts reversals
CDH;  660: purchase
DRI;    1: 
PVI;    5: new car purchase, obviously mistakes, nothing recent
VSU;10254: when +: then purchase (trade on sale of uc), when -: sale
AFM;   27: aftermarket (recon)
PVU; 1333: purchase
CNS;    1:
GJE;  239:
POT;  998: recon
WTD;  554: writedown

select a.the_date, a.control, a.journal, a.amount
from greg.ucinv_gl a
inner join (
  select stocknumber
  from greg.ucinv_tmp_avail_4
  group by stocknumber) b on a.control = b.stocknumber
where journal = 'VSU'
order by a.control

select x.* 
from greg.ucinv_gl x
inner join (
  select distinct a.control
  from greg.ucinv_gl a
  inner join (
    select stocknumber
    from greg.ucinv_tmp_avail_4
    group by stocknumber) b on a.control = b.stocknumber
  where journal = 'VSU') y on x.control = y.control
where x.void = 'Y'
order by x.control, x.the_date 

-- /> sort out journals ---------------------------------------------------- />



select a.thedate, a.stocknumber, a.from_date, a.sale_date, a.best_price, 
  b.*
from greg.ucinv_tmp_avail_4 a
left join greg.ucinv_gl b on a.stocknumber = b.control
  and a.thedate = b.the_Date
where a.stocknumber = '27440A'  

--hmmm get rid of the corrections
-- select the_date, control, sum(amount) as pot_gross_misc -- 367
-- from greg.ucinv_gl
-- where journal in ('CRC','PCA','DRO','PVI', 'CNS')
-- group by the_date, control  
-- order by control
-- group on control
select control, sum(amount) as pot_gross_misc -- down to 87 rows
from greg.ucinv_gl
where journal in ('CRC','PCA','DRO','CNS')
group by control having sum(amount) > 0

-- move PVI to purchase
select * from greg.ucinv_ext_glptrns a
where exists (
  select 1
  from greg.ucinv_ext_glptrns
  where gtctl_ = a.gtctl_
    and gtjrnl = 'PVI')
order by gtctl_, gtdate




select the_date, control, sum(amount) as pot_gross_recon -- 44394
from greg.ucinv_gl
where journal in ('SCA','SVI','AFM','POT')
group by the_date, control  

-- purchases are adversely affected by corrections
-- 25519XX: between 7/2 and 7/14 shows purch amt of 733350 until corrected on 7/15
-- maybe only recon & misc should show changing over time
-- so, this would become
select min(the_date) as the_date, control, sum(amount) as pot_gross_purchase -- 8896
-- select the_date, control, sum(amount) as pot_gross_purchase -- 8896
from greg.ucinv_gl
WHERE (
    (journal = 'EFT' and amount > 0)
    or 
    journal in ('CDH', 'PVU', 'VSN', 'GJE', 'PVI'))
group by control   
order by sum(amount) desc 

-- don't need a date
select control, sum(amount) as pot_gross_writedown --947
from greg.ucinv_gl
where journal = 'WTD'
group by control  




-- once priced, at some later date shows as not priced
select * -- 700
from (
  select stocknumber, max(thedate) as no_price
  from greg.ucinv_tmp_avail_4
  where best_price = -1
  group by stocknumber) a     
inner join (
  select stocknumber, min(thedate) as has_price
  from greg.ucinv_tmp_avail_4
  where best_price <> -1
  group by stocknumber) b on a.stocknumber = b.stocknumber
    and a.no_price > b.has_price 
order by no_price desc    


-- 5/22 ------------------------------------------------------------------------------------------
-- pick some likely subset of vehicles (different acq types, maybe) and start putting
-- ucinv_tmp_avail_4 together with the accounting

-- stocknumber, days owned for vehicles currently in inventory and those sold after 4/30
select stocknumber, count(*) /*days owned*/, max(sale_Date) as sale_date 
from greg.ucinv_tmp_avail_4 
where sale_date > '04/30/2016' 
  and storecode = 'RY1'
group by stocknumber order by count(*) desc

select * 
from greg.ucinv_tmp_avail_4 a
where a.stocknumber = '27738A'

select * 
from greg.ucinv_gl
where control = '27738a'
order by the_date

select * 
from greg.ucinv_ext_glptrns 
where gtctl_ = '27738A' 
order by gtdate

select * from greg.used_Vehicle_daily_snapshot where stocknumber = '27738a' order by the_date


select a.thedate, a.stocknumber, a.from_date, a.sale_date, a.best_price, 
  b.*
from greg.ucinv_tmp_avail_4 a
left join greg.ucinv_gl b on a.stocknumber = b.control and a.thedate = b.the_date
where a.stocknumber = '27738A'

-- this looks close for total
select x.*, sum(amount) over (partition by stocknumber order by thedate) as total
from (
  select a.thedate, a.stocknumber, a.from_date, a.sale_date, a.best_price, 
    b.journal, b.amount
  from greg.ucinv_tmp_avail_4 a
  left join greg.ucinv_gl b on a.stocknumber = b.control and a.thedate = b.the_date
  where a.stocknumber = '27738A') x
order by thedate



-- this looks close for total
select x.*, sum(amount) over (partition by stocknumber order by thedate) as total
from (
  select a.thedate, a.stocknumber, a.from_date, a.sale_date, a.best_price, 
    b.journal, b.amount
  from greg.ucinv_tmp_avail_4 a
  left join greg.ucinv_gl b on a.stocknumber = b.control and a.thedate = b.the_date
  where a.stocknumber = '27738A') x
order by thedate


/*

since all the inventory stuff is derived from tool, dates are not going to conicide gl data
what about when the dates don't coincide
separate interim tables for inventory and gl
then use min and max dates?
*/
select min(the_date) from greg.ucinv_gl-- 1/2/12

select distinct(a.stocknumber, a.from_date, a.sale_date), coalesce(b.pot_gross_writedown, 0) as write_down,
  coalesce(c.pot_gross_purchase, 0) as purch
from greg.used_vehicle_daily_snapshot a
left join (
  select control, sum(amount) as pot_gross_writedown --947
  from greg.ucinv_gl
  where journal = 'WTD'
  group by control) b on a.stocknumber = b.control 
left join (
  select control, sum(amount) as pot_gross_purchase -- 8896
  from greg.ucinv_gl
  WHERE (
      (journal = 'EFT' and amount > 0)
      or 
      journal in ('CDH', 'PVU', 'VSN', 'GJE', 'PVI', 'VSU'))
  group by control) c on a.stocknumber = c.control 
  Limit 100

  select * from greg.ucinv_ext_glptrns where gtctl_ = '19888XXZA'

  select * from greg.ucinv_gl where control = '19888XXZA'

it appears that including VSU in purchase is a problem in as much as ucinv_gl does not include the
doc, and the only wat to differentiate sale from purchase in vsu is control <> doc  

insert into greg.ucinv_gl 
SELECT gtdate, gtjrnl, gtctl_, SUM(gttamt) AS gttamt
FROM greg.ucinv_ext_glptrns
where gtpost = 'Y'
  and case when gtjrnl = 'VSU' then gtctl_ <> gtdoc_ else gtctl_ = gtdoc_ end 
GROUP BY gtdate, gtjrnl, gtctl_
order by gtdate desc 

-- 5/24 --------------------------------------------------------------------------------------------------------------------------
inventory categories based on journal:
  Purchase: 
    VSU, VSN, EFT, CDH, GJE, PVU
  Recon:
    SCA, SVI, AFM, POT
  Writedown
    WTD, EFT
  Misc
    CRC, PCA,DRO, CNS
only recon will be will have  a date, all others will be assigned at acquisition
  
looking like i will need to do a union of separate purchase journal options

-- Purchase: VSU: acquire trade on used car sale
select * from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsu' and gtctl_ <> gtdoc_ order by gtctl_, gtdate
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsu' and gtctl_ <> gtdoc_ group by gtctl_ having sum(gttamt) > 0 order by gtctl_
-- Purchase: VSN: acquire trade on new car sale
select * from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsn' order by gtctl_, gtdate
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsn' group by gtctl_ having sum(gttamt) > 0 order by gtctl_
-- Purchase: PVU: purchase used car
select * from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'pvu' order by gtctl_, gtdate
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'pvu' group by gtctl_ having sum(gttamt) > 0 order by gtctl_
-- Purchase: EFT: purchase
select * from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'eft' order by gtctl_, gtdate
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'eft' group by gtctl_ having sum(gttamt) > 1.0 order by gtctl_
-- Purchase: CDH: misc check purchase, this one is yucky, these amounts may be part of the purchase price, misc expenses (17472XX), or outright purchase (20761X)
select * from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'cdh' order by gtctl_, gtdate
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'cdh' group by gtctl_ having sum(gttamt) <> 0 order by sum(gttamt)
-- Purchase: GJE: move rental/coach/DE to inventory (99507R), lease buyout (28204X), street purchase 22515P
-- some writedowns to the wrong journal (s/b WTD) when description like 'WRITE%', hmmm michel did all those
-- and a bunch of $10 GM INVOICE entries
select * from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'gje' and gtdesc not like 'WRITE%' order by gtctl_, gtdate
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'gje' and gtdesc not like 'WRITE%' group by gtctl_ having sum(gttamt) > 0 order by sum(gttamt)
-- Misc: CRC, PCA: beats me
select * from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl in ('CRC','PCA') order by gtctl_, gtdate
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl in ('CRC','PCA') group by gtctl_ having sum(gttamt) > 0 order by sum(gttamt)
-- Recon
select * from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl in ('SCA','SVI','AFM','POT') order by gtctl_, gtdate
select the_date, control, sum(amount) as amount from greg.ucinv_gl where journal in ('SCA','SVI','AFM','POT') group by the_date, control  
-- Writedown
select * from greg.ucinv_ext_glptrns where gtpost = 'y' and (gtjrnl = 'WTD' or (gtjrnl = 'GJE' and gtdesc like 'WRITE%')) order by gtctl_, gtdate
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and (gtjrnl = 'WTD' or (gtjrnl = 'GJE' and gtdesc like 'WRITE%')) group by gtctl_ having sum(gttamt) < 0 order by sum(gttamt)



select * from greg.ucinv_ext_glptrns WHERE gtctl_ = '23725YX'

drop table if exists  plg_purchase;
create temporary table  plg_purchase as 
select gtctl_, sum(amount) as amount
from (
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsu' and gtctl_ <> gtdoc_ group by gtctl_ having sum(gttamt) > 0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsn' group by gtctl_ having sum(gttamt) > 0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'eft' group by gtctl_ having sum(gttamt) > 1.0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'cdh' group by gtctl_ having sum(gttamt) <> 0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'gje' and gtdesc not like 'WRITE%' group by gtctl_ having sum(gttamt) > 0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'pvu' group by gtctl_ having sum(gttamt) > 0 
) a
group by gtctl_

select * 
from greg.ucinv_tmp_avail_4 a
left join  plg_purchase b on a.stocknumber = b.gtctl_
order by a.stocknumber
limit 1000

select distinct a.stocknumber, 
from greg.ucinv_tmp_avail_4 a
left join  plg_purchase b on a.stocknumber = b.gtctl_
limit 1000

drop table if exists no_purch;
create temporary table no_purch as
select * 
from (
  select stocknumber, from_date, sale_date
  from greg.ucinv_tmp_avail_4
  where storecode = 'RY1'
  group by stocknumber, from_date, sale_date) a
left join  plg_purchase b on a.stocknumber = b.gtctl_
where b.gtctl_ is null

-- good enuf
select * -- down to 39 units
from no_purch a
left join greg.ucinv_ext_glptrns b on a.stocknumber = b.gtctl_
order by stocknumber

select * from greg.ucinv_ext_glptrns a where gtdesc like 'SMART %' and not exists (select 1 from no_purch where stocknumber = a.gtctl_)

select * from greg.ucinv_ext_glptrns where gtctl_ = '28292X'


select * from greg.ucinv_ext_glptrns where gtdoc_ like '28232%'

have a purchase amount on all but 39 units, some of those i can figure out
good enough

now, need to model this shit out and persist it and make it part of the update

-- purchase
drop table if exists  plg_purchase;
create temporary table  plg_purchase as 
select gtctl_, sum(amount) as amount
from (
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsu' and gtctl_ <> gtdoc_ group by gtctl_ having sum(gttamt) > 0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsn' group by gtctl_ having sum(gttamt) > 0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'eft' group by gtctl_ having sum(gttamt) > 1.0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'cdh' group by gtctl_ having sum(gttamt) <> 0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'gje' and gtdesc not like 'WRITE%' group by gtctl_ having sum(gttamt) > 0 
  union
  select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'pvu' group by gtctl_ having sum(gttamt) > 0 
) a
group by gtctl_;
create unique index on  plg_purchase(gtctl_);

-- writedown
drop table if exists  plg_writedown;
create temporary table  plg_writedown as
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and (gtjrnl = 'WTD' or (gtjrnl = 'GJE' and gtdesc like 'WRITE%')) group by gtctl_ having sum(gttamt) < 0;
create unique index on  plg_writedown(gtctl_);

-- recon
drop table if exists  plg_recon;
create temporary table  plg_recon as
select gtdate, gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtjrnl in ('SCA','SVI','AFM','POT') group by gtdate, gtctl_;
create unique index on  plg_recon(gtdate, gtctl_);

-- misc
drop table if exists  plg_misc;
create temporary table  plg_misc as
select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl in ('CRC','PCA') group by gtctl_ having sum(gttamt) > 0;
create unique index on  plg_misc(gtctl_);

-- if i am going to get a running total, then either have to join up the  plg tables separately
-- or join the gtctl  plg tables to first date only
select the_date, stocknumber, best_price, coalesce(b.amount, 0) as  plg_purchase, 
  coalesce(c.amount, 0) as  plg_writedown
from greg.used_vehicle_daily_snapshot a
left join  plg_purchase b on a.stocknumber = b.gtctl_
left join  plg_writedown c on a.stocknumber = c.gtctl_
where a.the_date > '12/31/2015'
order by stocknumber, the_date
limit 10000

select a.*, coalesce(b.amount, 0) as  plg_purchase, 
  coalesce(c.amount, 0) as  plg_writedown, coalesce(d.amount, 0) as  plg_misc
from (  
  select min(the_date) as the_date, stocknumber
  from greg.used_vehicle_daily_snapshot
  where store_code = 'RY1'
  group by stocknumber) a
left join  plg_purchase b on a.stocknumber = b.gtctl_ 
left join  plg_writedown c on a.stocknumber = c.gtctl_
left join  plg_misc d on a.stocknumber = d.gtctl_
order by a.the_date


select a.stocknumber, a.the_date, coalesce(b.amount, 0) as  plg_recon
from greg.used_vehicle_daily_snapshot a
left join  plg_recon b on a.stocknumber = b.gtctl_
  and a.the_date = b.gtdate
where a.store_code = 'RY1'
--  and a.stocknumber = '15711b'
order by a.stocknumber, a.the_date
limit 10000

-- ah, inv starts at 1/1/2014, this had recon in 2013
select * from  plg_recon where gtctl_ = '15711b'

-- 5/25 ------------------------------------------------------------------------------------
i think my last mind fuck distraction: first recon date vs first inv date
looks ok

-- 500 rows
select *
from (
select stocknumber, min(the_date) as the_date
from greg.used_vehicle_daily_snapshot
where store_code = 'RY1'
group by stocknumber) a
left join (
select gtctl_, min(gtdate) as the_date
from  plg_recon
group by gtctl_) b on a.stocknumber = b.gtctl_
where b.the_date < a.the_date
order by a.the_date desc

-- the vast majority are inventory from jan 2014
select extract(year from a.the_Date) * 100 + extract(month from a.the_date), count(*)
from (
select stocknumber, min(the_date) as the_date
from greg.used_vehicle_daily_snapshot
where store_code = 'RY1'
group by stocknumber) a
left join (
select gtctl_, min(gtdate) as the_date
from  plg_recon
group by gtctl_) b on a.stocknumber = b.gtctl_
where b.the_date < a.the_date
group by extract(year from a.the_Date) * 100 + extract(month from a.the_date)
order by count(*) desc 


-- how many had recon done on sale date -- only 42
select *
from (
select stocknumber, min(the_date) as the_date
from greg.used_vehicle_daily_snapshot
where store_code = 'RY1'
  and the_date = sale_date
group by stocknumber) a
left join (
select gtctl_, min(gtdate) as the_date
from  plg_recon
group by gtctl_) b on a.stocknumber = b.gtctl_
where b.the_date = a.the_date
order by a.the_date desc


select * from greg.used_vehicle_daily_snapshot where stocknumber = '28126XXA'
select * from  plg_recon where gtctl_ = '28126XXA'

select * from greg.ucinv_ext_glptrns where gtctl_ = '28126XXA'

-- so now construct it

select a.*, coalesce(b.amount, 0) as  plg_purchase, 
  coalesce(c.amount, 0) as  plg_writedown, coalesce(d.amount, 0) as  plg_misc
from (  
  select min(the_date) as the_date, stocknumber
  from greg.used_vehicle_daily_snapshot
  where store_code = 'RY1'
  group by stocknumber) a
left join  plg_purchase b on a.stocknumber = b.gtctl_ 
left join  plg_writedown c on a.stocknumber = c.gtctl_
left join  plg_misc d on a.stocknumber = d.gtctl_
order by a.the_date

-- this huge fucker is what i need as the basis for running totals (325104 rows)
-- don't include the sale_date row, knocks it down to 317967 rows
drop table if exists plg_base;
create temporary table plg_base as
select m.stocknumber, m.the_date, 
  coalesce(o.plg_purchase::integer, 0) as plg_purchase,
  coalesce(o.plg_writedown::integer, 0) as plg_writedown,
  coalesce(o.plg_misc::integer, 0) as plg_misc, 
  coalesce(n.amount::integer, 0) as plg_recon,
  m.best_price 
from greg.used_vehicle_daily_snapshot m
left join plg_recon n on m.stocknumber = n.gtctl_
  and m.the_date = n.gtdate
left join (
  select a.*, coalesce(b.amount::integer, 0) as plg_purchase, 
    coalesce(c.amount::integer, 0) as plg_writedown, coalesce(d.amount::integer, 0) as plg_misc
  from (  
    select min(the_date) as the_date, stocknumber
    from greg.used_vehicle_daily_snapshot
    where store_code = 'RY1'
    group by stocknumber) a
  left join plg_purchase b on a.stocknumber = b.gtctl_ 
  left join plg_writedown c on a.stocknumber = c.gtctl_
  left join plg_misc d on a.stocknumber = d.gtctl_) o on m.stocknumber = o.stocknumber 
    and m.the_date = o.the_date
where m.store_code = 'RY1'
  and m.the_date <> m.sale_date;
create unique index on plg_base(the_Date, stocknumber);

-- i want a column of total  plg's and a column for pot gross
select x.*,
  sum( plg_recon) over w as plg_recon_running,
  sum( plg_recon +  plg_purchase +  plg_writedown +  plg_misc) over w as plg_running,
  case
    when best_price > 0 then
      best_price - sum( plg_recon +  plg_purchase +  plg_writedown +  plg_misc) over w
    else 0
  END as  plg
from (
  select * 
  from  plg_base
  where stocknumber = '27119a') x
window w as (partition by stocknumber order by the_date)
order by stocknumber, the_date

select * from greg.used_vehicle_daily_snapshot where stocknumber = '27633b'

select stocknumber, count(*)
from plg_base
where stocknumber like '27%'
  and plg_writedown <> 0
group by stocknumber

/*
columns i propose to add to greg.used_vehicle_daily_snapshot (plg::potential lot gross):
  plg_purchase: one entry on first date of owning the vehicle
  plg_writedown: one entry on first date of owning the vehicle
  plg_misc: one entry on first date of owning the vehicle
  plg_recon: one entry per vehicle and date pf recon entry in gl
  plg_recon_running: running total of recon for vehicle
  plg_running: running total of all plg factors
  plg: if vehicle not priced then 0 else best_price - plg_running

on the sale date, all plg vales will be 0

query with windows functions that makes this pretty simple:

select x.*,
  sum( plg_recon) over w as plg_recon_running,
  sum( plg_recon +  plg_purchase +  plg_writedown +  plg_misc) over w as plg_running,
  case
    when best_price > 0 then
      best_price - sum( plg_recon +  plg_purchase +  plg_writedown +  plg_misc) over w
    else 0
  END as  plg
from (
  select * 
  from  plg_base
  where stocknumber = '27721XXA') x
window w as (partition by stocknumber order by the_date)
order by stocknumber, the_date;
*/

after conversation with greg, add 3 more column plg_purchase_running, plg_writedown_running, plg_misc_running, 
have values for those factors for every day in inventory


-- so let's define the process
1. add attributes greg.used_vehicle_daily_snapshot
plg_recon
plg_recon_running
plg_purchase
plg_purchase_running
plg_misc
plg_misc_running
plg_writedown
plg_writedown_running
plg_running
plg

alter table greg.used_vehicle_daily_snapshot
add column plg_purchase integer not null default 0,
add column plg_writedown integer not null default 0,
add column plg_misc integer not null default 0,
add column plg_recon integer not null default 0,
add column plg_purchase_running integer not null default 0,
add column plg_writedown_running integer not null default 0,
add column plg_misc_running integer not null default 0,
add column plg_recon_running integer not null default 0,
add column plg_running integer not null default 0,
add column plg integer not null default 0;

select * from greg.used_vehicle_daily_snapshot limit 500

2. modify ucinv_ext_glptrns.py to include logging to greg.ucinv_log

3. modify greg_availability.py to update plg attirbutes in greg.used_vehicle_daily_snapshot

4. deploy ucinv_ext_glptrns.py & greg_availability.py
   schedule run of ucinv_ext_glptrns daily at 2:00 AM (in Daily group)

select x.*,
  sum( plg_purchase::integer) over w as plg_purchase_running,
  sum( plg_writedown::integer) over w as plg_writedown_running,
  sum( plg_misc::integer) over w as plg_misc_running,
  sum( plg_recon::integer) over w as plg_recon_running,
  sum( plg_recon::integer +  plg_purchase::integer +  plg_writedown::integer +  plg_misc::integer) over w as plg_running,
  case
    when best_price > 0 then
      best_price - sum( plg_recon::integer +  plg_purchase::integer +  plg_writedown::integer +  plg_misc::integer) over w
    else 0
  END as  plg
from (
  select * 
  from  plg_base
  where stocknumber = '27119a') x
window w as (partition by stocknumber order by the_date)
order by stocknumber, the_date


-- one big honking query?
-- fucking a, 5 seconds to create, populate and index 317,967 rows
drop table if exists plg_base;
create temporary table plg_base as
select m.stocknumber, m.the_date, 
  coalesce(o.plg_purchase, 0) as plg_purchase,
  coalesce(o.plg_writedown, 0) as plg_writedown,
  coalesce(o.plg_misc, 0) as plg_misc, 
  coalesce(n.amount, 0) as plg_recon,
  m.best_price 
from greg.used_vehicle_daily_snapshot m
left join ( -- recon
  select gtdate, gtctl_, sum(gttamt::integer) as amount 
  from greg.ucinv_ext_glptrns 
  where gtjrnl in ('SCA','SVI','AFM','POT') group by gtdate, gtctl_)n on m.stocknumber = n.gtctl_
  and m.the_date = n.gtdate
left join (
  select a.*, coalesce(b.amount::integer, 0) as plg_purchase, 
    coalesce(c.amount::integer, 0) as plg_writedown, coalesce(d.amount::integer, 0) as plg_misc
  from (  
    select min(the_date) as the_date, stocknumber
    from greg.used_vehicle_daily_snapshot
    where store_code = 'RY1'
    group by stocknumber) a
  left join ( -- purchase
    select gtctl_, sum(amount) as amount
    from (
      select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsu' and gtctl_ <> gtdoc_ group by gtctl_ having sum(gttamt) > 0 
      union
      select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsn' group by gtctl_ having sum(gttamt) > 0 
      union
      select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'eft' group by gtctl_ having sum(gttamt) > 1.0 
      union
      select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'cdh' group by gtctl_ having sum(gttamt) <> 0 
      union
      select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'gje' and gtdesc not like 'WRITE%' group by gtctl_ having sum(gttamt) > 0 
      union
      select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'pvu' group by gtctl_ having sum(gttamt) > 0 
    ) a
    group by gtctl_) b on a.stocknumber = b.gtctl_ 
  left join ( --writedown
    select gtctl_, sum(gttamt) as amount 
    from greg.ucinv_ext_glptrns 
    where gtpost = 'y' and (gtjrnl = 'WTD' or (gtjrnl = 'GJE' and gtdesc like 'WRITE%')) 
    group by gtctl_ 
    having sum(gttamt) < 0) c on a.stocknumber = c.gtctl_
  left join ( -- misc
    select gtctl_, sum(gttamt) as amount from 
    greg.ucinv_ext_glptrns 
    where gtpost = 'y' 
      and gtjrnl in ('CRC','PCA') 
    group by gtctl_ 
    having sum(gttamt) > 0) d on a.stocknumber = d.gtctl_) o on m.stocknumber = o.stocknumber 
    and m.the_date = o.the_date
where m.store_code = 'RY1'
--  and m.stocknumber like '27%'
  and m.the_date <> m.sale_date;
create unique index on plg_base(the_Date, stocknumber);  

-- 12 seconds using plg_base temp table
drop table if exists plg_update;
create temporary table plg_update as
select x.*,
  sum( plg_purchase) over w as plg_purchase_running,
  sum( plg_writedown) over w as plg_writedown_running,
  sum( plg_misc) over w as plg_misc_running,
  sum( plg_recon) over w as plg_recon_running,
  sum( plg_recon +  plg_purchase::integer +  plg_writedown::integer +  plg_misc::integer) over w as plg_running,
  case
    when best_price > 0 then
      best_price - sum( plg_recon::integer +  plg_purchase::integer +  plg_writedown::integer +  plg_misc::integer) over w
    else 0
  END as plg
from (
  select * 
  from  plg_base) x
window w as (partition by stocknumber order by the_date);
create unique index on plg_base(the_Date, stocknumber);  

-- 15 sec
-- 10 sec with date of oldest as a limit
-- select count(*) from plg_update -- only 126372 rows
drop table if exists tmp_plg_data;
create temporary table tmp_plg_data 
as
select x.*,
  sum( plg_purchase) over w as plg_purchase_running,
  sum( plg_writedown) over w as plg_writedown_running,
  sum( plg_misc) over w as plg_misc_running,
  sum( plg_recon) over w as plg_recon_running,
  sum( plg_recon +  plg_purchase::integer +  plg_writedown::integer +  plg_misc::integer) over w as plg_running,
  case
    when best_price > 0 then
      best_price - sum( plg_recon::integer +  plg_purchase::integer +  plg_writedown::integer +  plg_misc::integer) over w
    else 0
  END as plg
from (
  select m.stocknumber, m.the_date, 
    coalesce(o.plg_purchase, 0) as plg_purchase,
    coalesce(o.plg_writedown, 0) as plg_writedown,
    coalesce(o.plg_misc, 0) as plg_misc, 
    coalesce(n.amount, 0) as plg_recon,
    m.best_price 
  from greg.used_vehicle_daily_snapshot m
  left join ( -- recon
    select gtdate, gtctl_, sum(gttamt::integer) as amount 
    from greg.ucinv_ext_glptrns 
    where gtjrnl in ('SCA','SVI','AFM','POT') group by gtdate, gtctl_)n on m.stocknumber = n.gtctl_
    and m.the_date = n.gtdate
  left join (
    select a.*, coalesce(b.amount::integer, 0) as plg_purchase, 
      coalesce(c.amount::integer, 0) as plg_writedown, coalesce(d.amount::integer, 0) as plg_misc
    from (  
      select min(the_date) as the_date, stocknumber
      from greg.used_vehicle_daily_snapshot
      where store_code = 'RY1'
      group by stocknumber) a
    left join ( -- purchase
      select gtctl_, sum(amount) as amount
      from (
        select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsu' and gtctl_ <> gtdoc_ group by gtctl_ having sum(gttamt) > 0 
        union
        select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'vsn' group by gtctl_ having sum(gttamt) > 0 
        union
        select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'eft' group by gtctl_ having sum(gttamt) > 1.0 
        union
        select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'cdh' group by gtctl_ having sum(gttamt) <> 0 
        union
        select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'gje' and gtdesc not like 'WRITE%' group by gtctl_ having sum(gttamt) > 0 
        union
        select gtctl_, sum(gttamt) as amount from greg.ucinv_ext_glptrns where gtpost = 'y' and gtjrnl = 'pvu' group by gtctl_ having sum(gttamt) > 0 
      ) a
      group by gtctl_) b on a.stocknumber = b.gtctl_ 
    left join ( --writedown
      select gtctl_, sum(gttamt) as amount 
      from greg.ucinv_ext_glptrns 
      where gtpost = 'y' and (gtjrnl = 'WTD' or (gtjrnl = 'GJE' and gtdesc like 'WRITE%')) 
      group by gtctl_ 
      having sum(gttamt) < 0) c on a.stocknumber = c.gtctl_
    left join ( -- misc
      select gtctl_, sum(gttamt) as amount from 
      greg.ucinv_ext_glptrns 
      where gtpost = 'y' 
        and gtjrnl in ('CRC','PCA') 
      group by gtctl_ 
      having sum(gttamt) > 0) d on a.stocknumber = d.gtctl_) o on m.stocknumber = o.stocknumber 
      and m.the_date = o.the_date
  where m.store_code = 'RY1'
  --  and m.stocknumber like '27%'
    and m.the_date <> m.sale_date -- no plg on sale date
    and m.the_date > ( -- already did backfill, 
      select min(the_date)
      from greg.used_vehicle_daily_snapshot
      where sale_date > current_date - 30
      and store_code = 'RY1')) x
window w as (partition by stocknumber order by the_date);
create unique index on plg_base(the_Date, stocknumber);  

-- only 2.08 minutes to update all 317967 rows
-- 33.67 sec w/126000 rows
update greg.used_vehicle_daily_snapshot a
set plg_purchase = x.plg_purchase,
    plg_writedown = x.plg_writedown,
    plg_misc = x.plg_misc,
    plg_recon = x.plg_recon,
    plg_purchase_running = x.plg_purchase_running, 
    plg_writedown_running = x.plg_writedown_running,
    plg_misc_running = x.plg_misc_running,
    plg_recon_running = x.plg_recon_running,
    plg_running = x.plg_running,
    plg = x.plg
from tmp_plg_data x
where a.stocknumber = x.stocknumber
  and a.the_date = x.the_date;    


select the_date, count(*), sum(plg), sum(plg)/count(*)
from greg.used_vehicle_daily_snapshot a
where the_date between '04/01/2016' and '04/30/2016'
  and store_code = 'RY1'
  and status = 'avail_fresh'
  and sale_date <> the_date
group by the_date
order by the_date

-- how many priced on each day
-- need to look at this with window lead()/lag()
select a.*, b.priced
from (
  select the_date, status, count(*), sum(plg), sum(plg)/count(*)
  from greg.used_vehicle_daily_snapshot 
  where the_date between '04/01/2016' and '04/30/2016'
    and store_code = 'RY1'
--     and status = 'avail_fresh'
    and sale_date <> the_date
  group by the_date, status
  order by the_date) a
left join (
  select the_date, status, count(*) as priced
  from greg.used_vehicle_daily_snapshot 
  where the_date between '04/01/2016' and '04/30/2016'
    and store_code = 'RY1'
--     and status = 'avail_fresh'
    and sale_date <> the_date
    and days_since_priced = 0
  group by the_date, status) b on a.the_date = b.the_date and a.status = b.status
order by a.status, a.the_date  

-- nov vs april
select * 
from (
  select the_date, status, count(*), sum(plg) as total_pot_gross, 
    sum(plg)/count(*) as avg_pot_gross
  from greg.used_vehicle_daily_snapshot
  where the_date between '11/01/2015' and '11/30/2015'
    and store_code = 'RY1'
    and sale_date <> the_date
  group by the_date, status 
  order by the_date, status) a
left join (
  select the_date, status, count(*), sum(plg) as total_pot_gross, 
    sum(plg)/count(*) as avg_pot_gross
  from greg.used_vehicle_daily_snapshot
  where the_date between '04/01/2016' and '04/30/2016'
    and store_code = 'RY1'
    and sale_date <> the_date
  group by the_date, status 
  order by the_date, status) b on extract(day from a.the_date) = extract(day from b.the_date) 
    and a.status = b.status

-- yesterday plt
select the_date, status, count(*), sum(plg), sum(plg)/count(*)
from greg.used_vehicle_daily_snapshot
where the_date = current_date - 1
  and store_code = 'RY1'
  and sale_date <> the_date
group by the_date, status

-- from_date of the oldest vehicle on the lot
-- this can be used to limit the scrape from inventory
select min(the_date)
from greg.used_vehicle_daily_snapshot
where sale_date > current_date - 30
and store_code = 'RY1'


-- current inventory with negative plg
select *
from greg.used_vehicle_daily_snapshot
where sale_date > current_date - 1
  and store_code = 'RY1'
  and the_date = current_date - 1
  and plg < 0
  
select stocknumber, days_owned
from greg.used_vehicle_daily_snapshot
where sale_date > current_date - 30
  and the_date = current_Date - 1
  and store_code = 'RY1'
order by days_owned desc 

order by days_owned desc 

  
