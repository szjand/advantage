
-- today
DECLARE @store string;
@store = (SELECT partyid FROM dps.organizations WHERE name = 'rydells');
SELECT a.stocknumber, timestampdiff(sql_tsi_day, b.fromts, CAST(curdate() AS sql_timestamp))
FROM dps.VehicleInventoryItems a 
INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
WHERE b.category = 'RMFlagAV'
  AND b.thruTS IS NULL 
  AND a.owningLocationID = @store;

DECLARE @store string;
DECLARE @date date;
@store = (SELECT partyid FROM dps.organizations WHERE name = 'rydells');
@date = curdate();
SELECT 
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) BETWEEN 0 AND 7 THEN 1 ELSE 0 END) AS "0 - 7",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) BETWEEN 8 AND 14 THEN 1 ELSE 0 END) AS "8 - 14",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) BETWEEN 15 AND 21 THEN 1 ELSE 0 END) AS "15 - 21",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) BETWEEN 22 AND 28 THEN 1 ELSE 0 END) AS "22 - 28",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) BETWEEN 29 AND 35 THEN 1 ELSE 0 END) AS "29 - 35",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) BETWEEN 36 AND 42 THEN 1 ELSE 0 END) AS "36 - 42",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) BETWEEN 43 AND 49 THEN 1 ELSE 0 END) AS "43 - 49",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) BETWEEN 50 AND 56 THEN 1 ELSE 0 END) AS "50 - 56",  
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) BETWEEN 57 AND 1000 THEN 1 ELSE 0 END) AS "57+"         
FROM dps.VehicleInventoryItems a 
INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
WHERE b.category = 'RMFlagAV'
  AND b.thruTS IS NULL 
  AND a.owningLocationID = @store

  
 
-- hmm, thinking a single TABLE for this, maybe UPDATE curdate() row WHEN called
-- OR, the TABLE does NOT contain curdate(), only previous rows, curdate() 
-- fast enuf to generate dynamically
-- anyway

-- previous date  
-- at some "arbitrary" point IN time for each day, how many available IN each category
-- start of day OR END of day?, let's go with the END of the day
DECLARE @store string;
DECLARE @date date;
DECLARE @ts timestamp;
@store = (SELECT partyid FROM dps.organizations WHERE name = 'rydells');
@date = '11/20/2014';
@ts = timestampadd(sql_tsi_hour, 20, @date);
SELECT @ts FROM system.iota;
SELECT 
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 0 AND 7 THEN 1 ELSE 0 END) AS "0 - 7",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 8 AND 14 THEN 1 ELSE 0 END) AS "8 - 14",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 15 AND 21 THEN 1 ELSE 0 END) AS "15 - 21",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 22 AND 28 THEN 1 ELSE 0 END) AS "22 - 28",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 29 AND 35 THEN 1 ELSE 0 END) AS "29 - 35",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 36 AND 42 THEN 1 ELSE 0 END) AS "36 - 42",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 43 AND 49 THEN 1 ELSE 0 END) AS "43 - 49",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 50 AND 56 THEN 1 ELSE 0 END) AS "50 - 56",  
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 57 AND 1000 THEN 1 ELSE 0 END) AS "57+"         
FROM dps.VehicleInventoryItems a 
INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
WHERE b.category = 'RMFlagAV'
  AND @ts BETWEEN b.fromTS AND coalesce(b.thruTS, CAST('12/31/9999 00:00:00' AS sql_timestamp))
  AND a.owningLocationID = @store


  

-- DROP TABLE #wtf;
SELECT a.VehicleInventoryItemID, a.fromTS, a.thruTS, b.owningLocationID 
INTO #wtf  
FROM dps.VehicleInventoryItemStatuses a
LEFT JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE a.category = 'RMFlagAV'
  AND cast(coalesce(a.thruTS, CAST('12/31/9999 00:00:00' AS sql_timestamp)) AS sql_date) > curdate() - 365
 

DECLARE @store string;
DECLARE @date date;
DECLARE @ts timestamp;
@store = (SELECT partyid FROM dps.organizations WHERE name = 'rydells');
@date = '08/26/2014';
@ts = timestampadd(sql_tsi_hour, 20, @date);  
SELECT 
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 0 AND 7 THEN 1 ELSE 0 END) AS "0 - 7",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 8 AND 14 THEN 1 ELSE 0 END) AS "8 - 14",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 15 AND 21 THEN 1 ELSE 0 END) AS "15 - 21",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 22 AND 28 THEN 1 ELSE 0 END) AS "22 - 28",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 29 AND 35 THEN 1 ELSE 0 END) AS "29 - 35",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 36 AND 42 THEN 1 ELSE 0 END) AS "36 - 42",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 43 AND 49 THEN 1 ELSE 0 END) AS "43 - 49",
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 50 AND 56 THEN 1 ELSE 0 END) AS "50 - 56",  
  sum(case when timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), @date) BETWEEN 57 AND 1000 THEN 1 ELSE 0 END) AS "57+"  
FROM #wtf b
WHERE @ts BETWEEN b.fromTS AND coalesce(b.thruTS, CAST('12/31/9999 00:00:00' AS sql_timestamp))
  AND b.owningLocationID = @store

SELECT *
FROM #wtf  
  
-- very very CLOSE, some possible discrepancies IN numbers
-- what other statuses can a vehicle have WHILE it IS available that might be kinking
-- fucking sales buffer
SELECT a.VehicleInventoryItemID, a.status, a.fromts, a.thruts, b.status, b.fromts, b.thruts
-- SELECT DISTINCT b.status
FROM dps.VehicleInventoryItemStatuses a
LEFT JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category <> 'RMFlagAV'
  AND b.thruTS IS NULL 
  AND b.status NOT LIKE '%Recon%'
WHERE a.category = 'RMFlagAV'
  AND a.thruTS IS NULL   
ORDER BY b.status  

  

  

   
-- TABLE for previous dates
-- DROP TABLE #wtf;
SELECT a.VehicleInventoryItemID, a.fromTS, a.thruTS, b.owningLocationID
INTO #wtf  
FROM dps.VehicleInventoryItemStatuses a
LEFT JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE a.category = 'RMFlagAV'
  AND cast(coalesce(a.thruTS, CAST('12/31/9999 00:00:00' AS sql_timestamp)) AS sql_date) > curdate() - 365

-- 11/24 ADD stocknumber, shape & size to ucinv_available
-- 11/27 ADD storecode
DROP TABLE ucinv_available;  
CREATE TABLE ucinv_available (
  VehicleInventoryItemID cichar(38) constraint NOT NULL,
  fromTS timestamp constraint NOT NULL,
  thruTS timestamp constraint NOT NULL,
  owningLocationID char(38) constraint NOT NULL,
  storeCode cichar(3) constraint NOT NULL,
  stockNumber cichar(9) constraint NOT NULL,
  vehicleShape cichar(30) constraint NOT NULL,
  vehicleSize cichar(30) constraint NOT NULL,
  constraint PK primary key (VehicleInventoryItemID,fromTS)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','VehicleInventoryItemID', 
   'VehicleInventoryItemID','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','fromTS', 
   'fromTS','',2,512,'');     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','thruTS', 
   'thruTS','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','owningLocationID', 
   'owningLocationID','',2,512,''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','stockNumber', 
   'stockNumber','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','vehicleShape', 
   'vehicleShape','',2,512,'');   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','vehicleSize', 
   'vehicleSize','',2,512,'');
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_available','ucinv_available.adi','storeCode', 
   'storeCode','',2,512,'');   

INSERT INTO ucinv_available(VehicleInventoryItemID,fromTs,thruTs,
  owningLocationID, storeCode, stockNumber, vehicleShape, vehicleSize)
SELECT a.VehicleInventoryItemID, a.fromTS, 
  coalesce(a.thruTS,CAST('12/31/9999 01:00:00' AS sql_timestamp)), 
  b.owningLocationID, 
  CASE b.owningLocationID
    WHEN 'B6183892-C79D-4489-A58C-B526DF948B06' THEN 'RY1' 
    WHEN '4CD8E72C-DC39-4544-B303-954C99176671' THEN 'RY2' 
  END AS storeCode, left(trim(b.stocknumber), 9) AS stocknumber,
  e.description AS vehicleShape, f.description AS vehicleSize 
-- SELECT COUNT(*) -- 7943
FROM dps.VehicleInventoryItemStatuses a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN dps.MakeModelClassifications d on c.make = d.make
  AND c.model = d.model
INNER JOIN dps.typDescriptions e on d.vehicleType = e.typ
INNER JOIN dps.typDescriptions f on d.vehicleSegment = f.typ
WHERE a.category = 'RMFlagAV'
  AND b.owningLocationID <> '1B6768C6-03B0-4195-BA3B-767C0CAC40A6';  
 
SELECT n, 
  CASE n
    WHEN 9 then '57+'
    ELSE TRIM(CAST(fr AS sql_char)) + '-' + TRIM(CAST(thru AS sql_char))
  END AS label,
  COUNT(*) AS units
FROM (  
  SELECT n, fr,
    CASE 
      WHEN n = 1 THEN 7
      WHEN n BETWEEN 2 AND 8 THEN fr + 6
      WHEN n = 9 THEN 1000
    END AS thru
  FROM (  
    SELECT a.n,
      CASE a.n
        WHEN 1 THEN 0 
        WHEN 2 THEN 8 
        WHEN 3 THEN 15
        WHEN 4 THEN 22
        WHEN 5 THEN 29
        WHEN 6 THEN 36
        WHEN 7 THEN 43
        WHEN 8 THEN 50
        WHEN 9 THEN 57
      END AS fr
    FROM dds.tally a
    WHERE n BETWEEN 1 AND 9) b) c
LEFT JOIN (
  SELECT a.VehicleInventoryItemID, a.owningLocationID,
    timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) AS days  
  FROM dps.VehicleInventoryItems a 
  INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  WHERE b.category = 'RMFlagAV'
    AND b.thruTS IS NULL) d on  d.days BETWEEN c.fr AND c.thru
GROUP BY n, fr, thru  
 
-- DROP PROCEDURE ucinv_get_weekly_aging_profile() 
ALTER PROCEDURE ucinv_get_weekly_aging_profile (
  theDate date,
  location cichar(6),
  label cichar(8) output,
  units integer output)
BEGIN
/*
need to account for sales buffer vehicles

EXECUTE PROCEDURE ucinv_get_weekly_aging_profile ('08/12/2014', 'market');
*/
DECLARE @date date;
DECLARE @location string;
DECLARE @ts timestamp;
@date = (SELECT theDate FROM __input);
@location = (SELECT UPPER(location) FROM __input);
-- for previous days, take status at 8:00 PM
@ts = timestampadd(sql_tsi_hour, 20, @date); 
IF @date = curdate() 
THEN 
  INSERT INTO __output
  SELECT 
    CASE n
      WHEN 9 then '57+'
      ELSE TRIM(CAST(fr AS sql_char)) + '-' + TRIM(CAST(thru AS sql_char))
    END AS label,
    COUNT(*) AS units
  FROM (  
    SELECT n, fr,
      CASE 
        WHEN n = 1 THEN 7
        WHEN n BETWEEN 2 AND 8 THEN fr + 6
        WHEN n = 9 THEN 3000000
      END AS thru
    FROM (  
      SELECT a.n,
        CASE a.n
          WHEN 1 THEN 0 
          WHEN 2 THEN 8 
          WHEN 3 THEN 15
          WHEN 4 THEN 22
          WHEN 5 THEN 29
          WHEN 6 THEN 36
          WHEN 7 THEN 43
          WHEN 8 THEN 50
          WHEN 9 THEN 57
        END AS fr
      FROM dds.tally a
      WHERE n BETWEEN 1 AND 9) b) c
  LEFT JOIN (
    SELECT a.VehicleInventoryItemID,
      timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) AS days  
    FROM dps.VehicleInventoryItems a 
    INNER JOIN dps.VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
    WHERE b.category = 'RMFlagAV'
      AND b.thruTS IS NULL 
      AND 
        CASE @location
          WHEN 'RY1' THEN a.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'rydells')
          WHEN 'RY2' THEN a.owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'honda cartiva')
          WHEN 'MARKET' THEN 1 = 1
        END) d on  d.days BETWEEN c.fr AND c.thru 
  GROUP BY n, fr, thru;
ELSE 
  INSERT INTO __output  
  SELECT 
    CASE n
      WHEN 9 then '57+'
      ELSE TRIM(CAST(fr AS sql_char)) + '-' + TRIM(CAST(thru AS sql_char))
    END AS label,
    COUNT(*) AS units
  FROM (  
    SELECT n, fr,
      CASE 
        WHEN n = 1 THEN 7
        WHEN n BETWEEN 2 AND 8 THEN fr + 6
        WHEN n = 9 THEN 3000000
      END AS thru
    FROM (  
      SELECT a.n,
        CASE a.n
          WHEN 1 THEN 0 
          WHEN 2 THEN 8 
          WHEN 3 THEN 15
          WHEN 4 THEN 22
          WHEN 5 THEN 29
          WHEN 6 THEN 36
          WHEN 7 THEN 43
          WHEN 8 THEN 50
          WHEN 9 THEN 57
        END AS fr
      FROM dds.tally a
      WHERE n BETWEEN 1 AND 9) b) c
  LEFT JOIN (
    SELECT VehicleInventoryItemID, 
      timestampdiff(sql_tsi_day, cast(fromts AS sql_date), @date) AS days 
    FROM ucinv_available
    WHERE @ts BETWEEN fromTS AND thruTS
      AND 
        CASE @location
          WHEN 'RY1' THEN owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'rydells')
          WHEN 'RY2' THEN owningLocationID = (SELECT partyid FROM dps.organizations WHERE name = 'honda cartiva')
          WHEN 'MARKET' THEN 1 = 1
        END) d on d.days BETWEEN c.fr AND c.thru 
  GROUP BY n, fr, thru;       
END IF;  
END;