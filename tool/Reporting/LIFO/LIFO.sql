

/*

SELECT viix.stocknumber, g.ControlNumber, n.net, r.recon
FROM VehicleInventoryItems viix 
LEFT JOIN (
  select distinct ControlNumber
  from GlDetailsTable) g ON viix.stocknumber = g.ControlNumber
LEFT JOIN (
  select ControlNumber, sum(TransactionAmount) as Net
  from gldetailstable
  where AccountNumber in ('124000', '124100', '224000','224100') -- Inventory accounts only
  --  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by ControlNumber
    having sum(TransactionAmount) > 0) n ON g.ControlNumber = n.ControlNumber
    
left join (	
  select ControlNumber, sum(TransactionAmount) as Recon
  from gldetailstable
  where AccountNumber in ('124000', '124100', '224000','224100') -- Inventory accounts only
--    and trim(gtpost) <> 'V'  -- Ignore VOIDS
	and Journal in ('SVI', 'SWA', 'SCA') -- Journals = Service Sales Internal, Service Sales Warranty, Service Sales Retail
  group by ControlNumber
    having sum(TransactionAmount) > 0) r on g.ControlNumber = r.ControlNumber	
WHERE viix.ThruTS IS NULL     
ORDER BY stocknumber

SELECT distinct(controlnumber) 
FROM gldetailstable g
WHERE (
  SELECT SUM(transactionAmount)
  FROM gldetailstable
  WHERE controlnumber = g.controlnumber
  and AccountNumber in ('124000', '124100', '224000','224100')
  GROUP BY controlnumber) <> 0
ORDER by controlnumber
*/
/*
SELECT *
FROM zJonLifoArkona 
WHERE dateacquired > '12/31/2010'

DELETE FROM zJonLifoArkona 
WHERE dateacquired > '12/31/2010'
*/
-- 1/6/12
-- this IS the query to generate the spreadsheet
-- 1/5/14 generated a separate TABLE for the year zJonLifoArkona2014 (fuck, should have been 2013, oh well)
-- db2 query IS current inventory plus vehicles sold IN january
-- 1/3/15 moved zJonLifoArkona2014 to zJonLifoArkona2013, reuse the zJonLifoArkona2014 TABLE
-- run aqt query: Inventory - Lifo.sql, export to excel, generate INSERT statement (see lifo notes.txt)
-- 1/5/15 nothing new, just put the arkona data INTO zJonLifoArkona2015
-- 1/2/18 didn't even bother to make a year specific TABLE, data IN zJonLifoArkona
-- jan 2019 created table zJonLifoArkona2018, populate FROM arkona
-- only 5 NOT IN tool, they are goofy, just DELETE them

/*
jan 2 2020
IN AQT query: Inventory - Lifo - ArkonaReport
export AS an INSERT statement
INSERT TABLE: zJonLifoArkona2019
after creating that TABLE IN DPSVseries, run the INSERT statement generated
BY AQT
508 rows
AND this query generates the spreadsheet data
now its a matter of remembering how to format the spreadsheet AND printing it

this IS very cool
made a copy of the spreadsheet lifo_2018 (fully formatted AND ready to print)
renamed it to lifo_2019
saved the result of this query INTO lifo_2019_data.xlsx
copy special values only INTO lifo_2019 AND we are good to go

AS for the data 10 NOT IN tool, i will check those out

jan 6 2021
nothing new

jan 3 2021
IN the midst of the ukg transition on top of payroll,month END AND year END
wahoo
409 rows

jan 3 2023
		1. generate the INSERT statement IN AQT
		2. CREATE the advantage TABLE zjonLifoArkona2022
		3. run the generated INSERT statement
jan 2 2024
		1. generate the data IN AQT(change dates): Inventory - Lifo.sql
			 save the data AS lifo_2023_ext.xlsx
			 formula to generate insert statements: (paste this INTO COLUMN N of lifo_2023_ext.xlsx
			 !!! don't forget to change the TABLE name IN the INSERT formula
			 =CONCATENATE("insert into zJonLifoArkona2023 values('",A2,"','",B2,"','",C2,"','",D2,"','",E2,"','",F2,"','",G2,"','",H2,"',",I2,",'",J2,"',",K2,",",L2,",",M2,");")
		2. CREATE the advantage TABLE DpsVseries.zjonLifoArkona2023
		3. run the INSERT statements generated IN 2.
		4. save the output of the below query to lifo_2023_data.xlsx
		5. copy lifo_2022.xlsx, rename to lifo_2023.xlsx
		6. DELETE the existing data FROM lifo_2023.xlsx AND copy the data FROM lifo_2023_data.xlsx INTO it
*/
-- DELETE FROM zJonLifoArkona2022
SELECT 
  z.source AS Source, left(z.stocknumber, 12) AS Stock#, z.vin AS VIN, 
  left(trim(z.yearmodel) + ' ' + trim(z.make) + ' ' + trim(z.model) + ' ' 
    + coalesce(TRIM(vix.TRIM), '') + ' ' + coalesce(TRIM(vix.BodyStyle), ''), 50) AS Vehicle, 
  left(vix.engine, 25) AS Engine, left(vix.transmission, 6) AS Trans, 
  replace(cast(vio.VehicleOptions AS sql_varchar), ',', ', ') AS Options, --replace(vio.VehicleOptions, ',', ', ') AS Options, 
  cast(vio.AdditionalEquipment AS sql_varchar) AS "Addtl Equp", z.miles AS Miles, 
  z.dateacquired AS "Date Acqd",
  z.net AS Net, z.recon AS Recon, z.cost AS Cost,
  CASE 
    WHEN viix.VehicleItemId IS NULL AND net IS NOT NULL THEN 'Not in Tool'
    WHEN viix.VehicleItemId IS NULL AND net IS NULL THEN 'No Cost & Not in Tool'
    WHEN viix.VehicleItemID IS NOT NULL AND net IS NULL THEN 'No Cost'
  END AS Note , 'Good  Average  Rough' AS Condition   
--INTO #wtf  
FROM zJonLifoArkona2023 z ------------------------------------------
LEFT JOIN VehicleInventoryItems viix ON z.stocknumber = viix.stocknumber
LEFT JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
LEFT JOIN VehicleItemOptions vio ON viix.VehicleItemID = vio.VehicleItemID 
ORDER BY z.STOCKNUMBER  


-- NOT IN tool, only 4
SELECT *
FROM (
SELECT 
  z.source AS Source, left(z.stocknumber, 12) AS Stock#, z.vin AS VIN, 
  left(trim(z.yearmodel) + ' ' + trim(z.make) + ' ' + trim(z.model) + ' ' 
    + coalesce(TRIM(vix.TRIM), '') + ' ' + coalesce(TRIM(vix.BodyStyle), ''), 50) AS Vehicle, 
  left(vix.engine, 25) AS Engine, left(vix.transmission, 6) AS Trans, 
  replace(cast(vio.VehicleOptions AS sql_varchar), ',', ', ') AS Options, --replace(vio.VehicleOptions, ',', ', ') AS Options, 
  cast(vio.AdditionalEquipment AS sql_varchar) AS "Addtl Equp", z.miles AS Miles, 
  z.dateacquired AS "Date Acqd",
  z.net AS Net, z.recon AS Recon, z.cost AS Cost,
  CASE 
    WHEN viix.VehicleItemId IS NULL AND net IS NOT NULL THEN 'Not in Tool'
    WHEN viix.VehicleItemId IS NULL AND net IS NULL THEN 'No Cost & Not in Tool'
    WHEN viix.VehicleItemID IS NOT NULL AND net IS NULL THEN 'No Cost'
  END AS Note , 'Good  Average  Rough' AS Condition   
--INTO #wtf  
FROM zJonLifoArkona2023 z ------------------------------------------
LEFT JOIN VehicleInventoryItems viix ON z.stocknumber = viix.stocknumber
LEFT JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
LEFT JOIN VehicleItemOptions vio ON viix.VehicleItemID = vio.VehicleItemID 
) aa
WHERE note IS NOT null

-- 1/5/2014
-- sold IN january 2014
SELECT stock#, vin, vehicle, net, recon, cost, note, CAST(c.soldts AS sql_date)
FROM #wtf a
INNER JOIN VehicleInventoryItems b on a.stock# = b.stocknumber
INNER JOIN vehiclesales c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE c.status = 'VehicleSale_Sold'
  AND year(soldts) = 2015


-- NOT IN tool BY stocknumber

SELECT z.stocknumber, z.vin, z.dateacquired, vix.VehicleItemID,
  (SELECT stocknumber FROM VehicleInventoryItems WHERE VehicleItemID = vix.VehicleItemID),
  CASE 
    WHEN vix.VehicleItemId IS NULL AND net IS NOT NULL THEN 'Not in Tool'
    WHEN vix.VehicleItemId IS NULL AND net IS NULL THEN 'No Cost & Not in Tool'
    WHEN vix.VehicleItemID IS NOT NULL AND net IS NULL THEN 'No Cost'
  END AS note   
FROM zJonLifoArkona2019 z
LEFT JOIN VehicleItems vix ON z.vin = vix.vin
WHERE NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItems
  WHERE stocknumber = z.stocknumber)
ORDER BY z.stocknumber  



--------------------------------------------------------------------------------

SELECT z.stocknumber, z.vin, z.dateacquired, vix.VehicleItemID,
  (SELECT stocknumber FROM VehicleInventoryItems WHERE VehicleItemID = vix.VehicleItemID),
  CASE 
    WHEN vix.VehicleItemId IS NULL AND net IS NOT NULL THEN 'Not in Tool'
    WHEN vix.VehicleItemId IS NULL AND net IS NULL THEN 'No Cost & Not in Tool'
    WHEN vix.VehicleItemID IS NOT NULL AND net IS NULL THEN 'No Cost'
  END AS note, wtf.status  
FROM zJonLifoArkona z
LEFT JOIN VehicleItems vix ON z.vin = vix.vin
LEFT JOIN (
    SELECT top 1 vi.VehicleItemID, 
      CASE ve.CurrentStatus
        WHEN 'VehicleEvaluation_GettingData' THEN 'Eval: Getting Data'
        WHEN 'VehicleEvaluation_DataDone' THEN 'Eval: Data Collection Done'
        WHEN 'VehicleEvaluation_InAppraisal' THEN 'Eval: In  Appraisal'
        WHEN 'VehicleEvaluation_Finished' THEN 'Eval: Finished'
      END AS status  
    FROM VehicleItems vi
    LEFT JOIN VehicleEvaluations ve ON ve.VehicleitemID = vi.VehicleItemID
    WHERE ve.CurrentStatus <> 'VehicleEvaluation_Booked'
    ORDER BY ve.VehicleEvaluationTS DESC)  wtf ON vix.VehicleItemID = wtf.VehicleItemID 
WHERE NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItems
  WHERE stocknumber = z.stocknumber)
ORDER BY z.stocknumber      


-- NOT IN tool BY stocknumber, JOIN ON vin 
SELECT z.stocknumber, z.vin, z.dateacquired, vix.VehicleItemID,
  (SELECT stocknumber FROM VehicleInventoryItems WHERE VehicleItemID = vix.VehicleItemID),
  CASE 
    WHEN vix.VehicleItemId IS NULL AND net IS NOT NULL THEN 'Not in Tool'
    WHEN vix.VehicleItemId IS NULL AND net IS NULL THEN 'No Cost & Not in Tool'
    WHEN vix.VehicleItemID IS NOT NULL AND net IS NULL THEN 'No Cost'
  END AS note,  
  
  ( 
    SELECT top 1 
      CASE ve.CurrentStatus
        WHEN 'VehicleEvaluation_GettingData' THEN 'Eval: Getting Data'
        WHEN 'VehicleEvaluation_DataDone' THEN 'Eval: Data Collection Done'
        WHEN 'VehicleEvaluation_InAppraisal' THEN 'Eval: In  Appraisal'
        WHEN 'VehicleEvaluation_Finished' THEN 'Eval: Finished'
      END AS status  
    FROM VehicleItems vi
    LEFT JOIN VehicleEvaluations ve ON ve.VehicleitemID = vi.VehicleItemID
    WHERE ve.CurrentStatus <> 'VehicleEvaluation_Booked'
    AND vi.VehicleItemID = vix.VehicleItemID 
    ORDER BY ve.VehicleEvaluationTS DESC)   
FROM zJonLifoArkona z
LEFT JOIN VehicleItems vix ON z.vin = vix.vin
WHERE NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItems
  WHERE stocknumber = z.stocknumber)
ORDER BY z.stocknumber  

/*
SELECT VehicleItemID  FROM vehicleitemoptions GROUP BY VehicleItemID HAVING COUNT(*) > 1

SELECT * FROM vehicleitemoptions


SELECT * FROM blackbookvalues            
SELECT * FROM blackbookaddsdeducts        

REPLACE( str1, str2, str3 )	Replace all occurrences of str2 in str1 with str3
SELECT VehicleOptions, replace(VehicleOptions, ',', ', ')
FROM vehicleitemoptions
WHERE VehicleItemID = 'ab22052f-5811-094e-974d-b4e3d694ae4b'
*/       
