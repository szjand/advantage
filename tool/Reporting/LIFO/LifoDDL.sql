-- DROP TABLE zJonLifoArkona;
CREATE TABLE zJonLifoArkona (
  Source CIChar(10),
  StockNumber CIChar(20),
  VIN CIChar(24),
  YearModel integer,
  Make CIChar(60),
  Model CIChar(60),
  Trim CIChar(60),
  BodyStyle CIChar(30),
  Miles integer,
  DateAcquired Date,
  Net integer,
  Recon integer,
  Cost integer) IN database;
  
--ALTER TABLE zJonLifoArkona
--ALTER COLUMN DateAcquired DateAcquired date;  

-- CREATE the TABLE
-- populate with data FROM db2 (Inventory - Lifo.sql)
-- THEN change YearModel data type
ALTER TABLE zJonLifoArkona
ALTER COLUMN YearModel YearModel CIChar(4);
  
/*
SELECT * FROM zJonLifoArkona  ORDER BY stocknumber

SELECT cast(dateacquired AS sql_date) FROM  zJonLifoArkona 



  
  
  