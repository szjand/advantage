SELECT month(solddate) AS Month, COUNT(*) AS Sales,
  SUM(CASE WHEN availtosolddays <= 30 THEN 1 ELSE 0 END) AS LessThan30,
  SUM(CASE WHEN availtosolddays > 30 THEN 1 ELSE 0 END) AS Over30
FROM (  
  select stocknumber, cast(soldts AS sql_date) AS SoldDate, MAX(CAST(c.fromts AS sql_Date)) AS AvailDate,
   timestampdiff(sql_tsi_day, MAX(CAST(c.fromts AS sql_Date)), cast(soldts AS sql_date)) AS AvailToSoldDays
  FROM VehicleInventoryItems a
  INNER JOIN vehiclesales b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN VehicleInventoryItemStatuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  --  AND c.status = 'RMFlagAV_Available'
  WHERE owninglocationid = '4CD8E72C-DC39-4544-B303-954C99176671'
    AND b.typ = 'VehicleSale_Retail'
    AND b.status = 'VehicleSale_Sold'
    AND year(b.soldts) = 2013
    AND c.status = 'RMFlagAV_Available'
  group by stocknumber, cast(soldts AS sql_date)) x
GROUP BY month(solddate)  



SELECT month(solddate) AS Month, COUNT(*) AS Sales,
  SUM(CASE WHEN availtosolddays <= 30 THEN 1 ELSE 0 END) AS LessThan30,
  trim(cast(100*SUM(CASE WHEN availtosolddays <= 30 THEN 1 ELSE 0 END)/COUNT(*) AS sql_char)) + '%' AS PercentLessThan30,
  SUM(CASE WHEN availtosolddays > 30 THEN 1 ELSE 0 END) AS Over30, 
  trim(cast(100*SUM(CASE WHEN availtosolddays > 30 THEN 1 ELSE 0 END)/COUNT(*) as sql_char)) + '%' AS PercentOver30
FROM (  
  select stocknumber, cast(soldts AS sql_date) AS SoldDate, coalesce(MAX(CAST(c.fromts AS sql_Date)), cast('12/31/9999' AS sql_date)) AS AvailDate,
   timestampdiff(sql_tsi_day, MAX(CAST(c.fromts AS sql_Date)), cast(soldts AS sql_date)) AS AvailToSoldDays
  FROM VehicleInventoryItems a
  INNER JOIN vehiclesales b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  LEFT JOIN VehicleInventoryItemStatuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.status = 'RMFlagAV_Available'
  WHERE owninglocationid = '4CD8E72C-DC39-4544-B303-954C99176671'
    AND b.typ = 'VehicleSale_Retail'
    AND b.status = 'VehicleSale_Sold'
    AND year(b.soldts) = 2013
  group by stocknumber, cast(soldts AS sql_date)) x
GROUP BY month(solddate)  