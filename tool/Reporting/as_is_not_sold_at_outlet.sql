SELECT e.stocknumber, CAST(e.soldts AS sql_date) AS sold, trim(lastname) + '/' + coalesce(trim(lastname_1), '') AS sold_by
FROM (
  SELECT c.VehicleInventoryItemID, c.stocknumber, c.thruts, a.typ, a.soldts, b.lastname, bb.lastname, d.*
  FROM vehiclesales a 
  LEFT JOIN people b on a.managerpartyid = b.partyid
  LEFT JOIN people bb on a.soldby = bb.partyid
  LEFT JOIN VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  LEFT JOIN (
      SELECT c.description, a.stocknumber
      FROM VehicleInventoryItems a
      INNER JOIN selectedReconPackages b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      INNER JOIN typDescriptions c on b.typ = c.typ
      WHERE CAST(a.thruts AS sql_date) BETWEEN '10/01/2018' AND '10/31/2018'
        AND SelectedReconPackageTS = (
          select MAX(SelectedReconPackageTS)
          FROM SelectedReconPackages 
           WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)) d on c.stocknumber = d.stocknumber
  WHERE CAST(soldts AS sql_date) BETWEEN '10/01/2018' AND '10/31/2018') e
INNER JOIN VehicleWalks f on e.VehicleInventoryItemID = f.VehicleInventoryItemID   
WHERE description = 'As-Is'
  AND typ = 'VehicleSale_Retail'
  AND lastname NOT IN ('holland','olderbak')
ORDER BY stocknumber