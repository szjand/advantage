SELECT top 20 left(i.make, 12) AS make, left(i.model, 20) AS model, i.yearmodel, COUNT(*)
FROM VehicleInventoryItems v
INNER JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
--  AND model = 'impala'
WHERE CAST(fromTS AS sql_DATE) < '12/22/2011'
  AND coalesce(cast(thruts AS sql_date), cast('12/31/9999' AS sql_date)) > '12/22/2011'
  AND v.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
group by i.make, i.model, i.yearmodel
-- ORDER BY make, model
ORDER BY COUNT(*) desc



SELECT left(i.make, 12) AS make, left(i.model, 20) AS model, i.yearmodel, COUNT(*),
-- trying to ADD previous/next 30 days sales count
--  SUM(CASE WHEN ((s.VehicleInventoryItemID IS NOT NULL) AND (CAST(s.SoldTS AS sql_date) BETWEEN cast('01/22/11' AS sql_date) and cast('02/22/2010' AS sql_date))) THEN 1 ELSE 0 end)
--  SUM(CASE WHEN s.VehicleInventoryItemID IS NOT NULL THEN 1 ELSE 0 END)
  SUM(CASE WHEN ((s.VehicleInventoryItemID IS NOT NULL) AND (CAST(s.SoldTS AS sql_date) > '12/22/2010') and (cast(s.SoldTS as sql_date) < '01/22/11')) THEN 1 ELSE 0 end)      
FROM VehicleInventoryItems v
INNER JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
--  AND model = 'impala'
LEFT JOIN vehiclesales s ON v.VehicleInventoryItemID = s.VehicleInventoryItemID 
WHERE CAST(fromTS AS sql_DATE) < '1/22/2011'
  AND coalesce(cast(thruts AS sql_date), cast('12/31/9999' AS sql_date)) > '1/22/2011'
  AND v.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
group by i.make, i.model, i.yearmodel
ORDER BY COUNT(*) DESC


SELECT top 20 left(i.make, 12) AS make, left(i.model, 20) AS model, i.yearmodel, COUNT(*)
FROM VehicleInventoryItems v
INNER JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
  AND model = 'impala'
WHERE CAST(fromTS AS sql_DATE) < '12/22/2011'
  AND coalesce(cast(thruts AS sql_date), cast('12/31/9999' AS sql_date)) > '12/22/2011'
  AND v.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
group by i.make, i.model, i.yearmodel
-- ORDER BY make, model
ORDER BY COUNT(*) DESC


SELECT *
FROM vehiclesales
WHERE CAST(soldts AS sql_date) BETWEEN cast('12/22/2010' AS sql_date)  and cast('01/22/11' AS sql_date) 

SELECT *
FROM vehiclesales
WHERE CAST(soldts AS sql_date) >= '12/22/2010'
  AND CAST(soldts AS sql_date) <= '01/22/2011' 
  
  
SELECT left(i.make, 12) AS make, left(i.model, 20) AS model, i.yearmodel, COUNT(*),
  SUM(CASE WHEN i.model = 'impala' THEN 1 ELSE 0 END)
FROM VehicleInventoryItems v
INNER JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
--  AND model = 'impala'
WHERE CAST(fromTS AS sql_DATE) < '12/22/2011'
  AND coalesce(cast(thruts AS sql_date), cast('12/31/9999' AS sql_date)) > '12/22/2011'
  AND v.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
group by i.make, i.model, i.yearmodel
-- ORDER BY make, model
ORDER BY COUNT(*) desc