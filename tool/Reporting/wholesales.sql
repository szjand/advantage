SELECT c.stocknumber, d.vin, d.VehicleItemID, cast(c.fromts as sql_date) as date_acquired, 
  CAST(soldts AS sql_date) AS date_sold, d.yearmodel, d.make, d.model, e.value AS miles
-- SELECT COUNT(*)
FROM vehiclesales a
left JOIN organizations b on a.soldto = b.partyid collate ads_default_ci
LEFT JOIN VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN VehicleItems d on c.VehicleItemID = d.VehicleItemID 
LEFT JOIN vehicleitemmileages e on d.VehicleItemID = e.VehicleItemID
  AND e.vehicleitemmileagets between c.fromts AND a.soldts
WHERE typ = 'VehicleSale_Wholesale'
  AND CAST(soldts AS sql_date) > '12/31/2015'
  and a.soldto NOT IN ( '4CD8E72C-DC39-4544-B303-954C99176671','B6183892-C79D-4489-A58C-B526DF948B06') -- exclude intra market
ORDER BY stocknumber

