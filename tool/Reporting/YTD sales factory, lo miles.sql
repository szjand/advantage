-- ytd ry1 certified retail sales
SELECT count(a.VehicleInventoryItemid)
FROM vehiclesales a
WHERE a.typ = 'VehicleSale_Retail'
  AND a.status = 'VehicleSale_Sold'
  AND year(soldts) = 2014
  AND VehicleInventoryItemID IN ( -- ry1 sales
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItems  
    WHERE owningLocationID = 'B6183892-C79D-4489-A58C-B526DF948B06') 
  AND VehicleInventoryItemID IN ( -- certified
    SELECT VehicleInventoryItemID 
    FROM selectedreconpackages s
    WHERE typ = 'ReconPackage_Factory'
    AND selectedReconPAckageTS = (
      SELECT MAX(selectedReconPAckageTS)
      FROM selectedreconpackages
      WHERE VehicleInventoryItemID = s.VehicleInventoryItemID))
      
      
-- ytd ry1 retail sales w/ < 80k miles
-- DROP TABLE #wtf;
SELECT a.VehicleInventoryItemID, a.soldts, b.stocknumber, c.vin, d.basistable,
  d.value, d.vehicleitemmileagets, b.VehicleItemID 
INTO #wtf  
FROM vehiclesales a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID -- ry1 sales
  AND b.owningLocationID = 'B6183892-C79D-4489-A58C-B526DF948B06'
INNER JOIN VehicleItems c on b.VehicleItemID = c.VehicleItemID  
INNER JOIN vehicleitemmileages d on c.VehicleItemID = d.VehicleItemID 
WHERE a.typ = 'VehicleSale_Retail'
  AND a.status = 'VehicleSale_Sold'    
  AND year(a.soldts) = 2014       
         
-- ytd ry1 retail sales w/ < 80k miles
SELECT COUNT(*) 
FROM #wtf a
WHERE vehicleitemmileagets = (
  SELECT MAX(vehicleitemmileagets)
  FROM #wtf
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
    AND vehicleitemmileagets <= a.soldTS
  GROUP BY VehicleInventoryItemID)    
AND value < 80000     


SELECT 
  SUM(CASE WHEN value BETWEEN 0 AND 40000 THEN 1 ELSE 0 END ) AS "0-40k",
  SUM(CASE WHEN value BETWEEN 40001 AND 60000 THEN 1 ELSE 0 END ) AS "40k-60k",
  SUM(CASE WHEN value BETWEEN 60001 AND 80000 THEN 1 ELSE 0 END ) AS "60-80k",
  SUM(CASE WHEN value > 80000 THEN 1 ELSE 0 END ) AS "over 80k",
  COUNT(*)
FROM #wtf a
WHERE vehicleitemmileagets = (
  SELECT MAX(vehicleitemmileagets)
  FROM #wtf
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
    AND vehicleitemmileagets <= a.soldTS
  GROUP BY VehicleInventoryItemID)  
  
-- anthony wants
YTD
   cert 0-30k       cert 30-80
   non-cert 0-30    non-cert 30-80
   
so, rethinking, tmp TABLE will be used vehicles sold retail ytd with package AND miles


SELECT a.soldts, b.stocknumber, c.typ, e.value, e.VehicleItemMileageTS
FROM vehicleSales a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.owningLocationID = 'B6183892-C79D-4489-A58C-B526DF948B06'
INNER JOIN SelectedReconPackages c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS)
    FROM SelectedReconPackages 
    WHERE VehicleInventoryItemID  = c.VehicleInventoryItemID)
INNER JOIN VehicleItems d on b.VehicleItemID = d.VehicleItemID 
INNER JOIN VehicleItemMileages e on d.VehicleItemID = e.VehicleItemID
  AND e.VehicleItemMileageTS = (
    SELECT max(VehicleItemMileageTS)
    FROM VehicleItemMileages
    WHERE VehicleItemID = e.VehicleItemID 
      AND VehicleItemMileageTS <= a.soldTS)
WHERE a.typ = 'VehicleSale_Retail'
  AND a.status = 'VehicleSale_Sold'
  AND year(a.soldts) = 2014    
  

SELECT 
  SUM(CASE WHEN typ = 'ReconPackage_Factory' AND value < 30000 THEN 1 ELSE 0 END) AS "Cert 0-30K",
  SUM(CASE WHEN typ = 'ReconPackage_Factory' AND value between 30000 and 80000 THEN 1 ELSE 0 END) AS "Cert 30-80K",
  SUM(CASE WHEN typ <> 'ReconPackage_Factory' AND value < 30000 THEN 1 ELSE 0 END) AS "NonCert 0-30K",
  SUM(CASE WHEN typ <> 'ReconPackage_Factory' AND value between 30000 and 80000 THEN 1 ELSE 0 END) AS "NonCert 30-80K"  
FROM (  
  SELECT a.soldts, b.stocknumber, c.typ, e.value, e.VehicleItemMileageTS
  FROM vehicleSales a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.owningLocationID = 'B6183892-C79D-4489-A58C-B526DF948B06'
  INNER JOIN SelectedReconPackages c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.SelectedReconPackageTS = (
      SELECT MAX(SelectedReconPackageTS)
      FROM SelectedReconPackages 
      WHERE VehicleInventoryItemID  = c.VehicleInventoryItemID)
  INNER JOIN VehicleItems d on b.VehicleItemID = d.VehicleItemID 
  INNER JOIN VehicleItemMileages e on d.VehicleItemID = e.VehicleItemID
    AND e.VehicleItemMileageTS = (
      SELECT max(VehicleItemMileageTS)
      FROM VehicleItemMileages
      WHERE VehicleItemID = e.VehicleItemID 
        AND VehicleItemMileageTS <= a.soldTS)
  WHERE a.typ = 'VehicleSale_Retail'
    AND a.status = 'VehicleSale_Sold'
    AND year(a.soldts) = 2014) x      
    
  
  