SELECT stocknumber, p.VehiclePricingTS, d.amount
FROM VehicleInventoryItems i
INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND cast(p.VehiclePricingTS AS sql_date) > curdate() - 450
  AND p.VehiclePricingTS = (
    SELECT MAX(vehiclePricingTS)
	FROM VehiclePricings
	WHERE VehicleInventoryItemID = p.VehicleInventoryItemID) 
INNER JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID 
  AND d.Typ = 'VehiclePricingDetail_BestPrice'
  AND amount <= 8000
ORDER BY stocknumber  

SELECT stocknumber, right(trim(stocknumber), 3) AS Right3, p.VehiclePricingTS, d.amount
FROM VehicleInventoryItems i
INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND cast(p.VehiclePricingTS AS sql_date) > curdate() - 450
  AND p.VehiclePricingTS = (
    SELECT MAX(vehiclePricingTS)
	FROM VehiclePricings
	WHERE VehicleInventoryItemID = p.VehicleInventoryItemID) 
INNER JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID 
  AND d.Typ = 'VehiclePricingDetail_BestPrice'
  AND amount <= 8000
ORDER BY stocknumber 


SELECT right(TRIM(stocknumber), 1), COUNT(*)
FROM (
 SELECT stocknumber, p.VehiclePricingTS, d.amount
 FROM VehicleInventoryItems i
 INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
   AND cast(p.VehiclePricingTS AS sql_date) > curdate() - 450
   AND p.VehiclePricingTS = (
     SELECT MAX(vehiclePricingTS)
 	FROM VehiclePricings
 	WHERE VehicleInventoryItemID = p.VehicleInventoryItemID) 
 INNER JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID 
   AND d.Typ = 'VehiclePricingDetail_BestPrice'
   AND amount <= 8000) x
GROUP BY right(TRIM(stocknumber), 1)   


SELECT stocknumber, 
  substring(stocknumber, 6, 5) StkEnd,
  p.VehiclePricingTS, d.amount
FROM VehicleInventoryItems i
INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND cast(p.VehiclePricingTS AS sql_date) > curdate() - 450
  AND p.VehiclePricingTS = (
    SELECT MAX(vehiclePricingTS)
	FROM VehiclePricings
	WHERE VehicleInventoryItemID = p.VehicleInventoryItemID) 
INNER JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID 
  AND d.Typ = 'VehiclePricingDetail_BestPrice'
  AND amount <= 8000
ORDER BY stocknumber 


SELECT  substring(stocknumber, 6, 5) StkEnd, COUNT(*) AS [Count]
FROM VehicleInventoryItems i
INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND cast(p.VehiclePricingTS AS sql_date) > curdate() - 450
  AND p.VehiclePricingTS = (
    SELECT MAX(vehiclePricingTS)
	FROM VehiclePricings
	WHERE VehicleInventoryItemID = p.VehicleInventoryItemID) 
INNER JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID 
  AND d.Typ = 'VehiclePricingDetail_BestPrice'
  AND amount <= 8000
GROUP BY substring(stocknumber, 6, 5) 
ORDER BY COUNT(*) DESC 


SELECT x.LowEndVehicle,
  (SELECT LEFT(TRIM(yearmodel) + ' ' + TRIM(make) + ' ' + TRIM(model), 25)
    FROM VehicleItems
	WHERE VehicleItemID = x.VehicleItemID),
  x.LowEndVehicleSource,
  (SELECT LEFT(TRIM(yearmodel) + ' ' + TRIM(make) + ' ' + TRIM(model), 25)
    FROM VehicleItems
	WHERE VehicleItemID = (
	  SELECT VehicleItemID 
	  FROM VehicleInventoryItems
	  WHERE stocknumber = x.LowEndVehicleSource))  
FROM (
 SELECT stocknumber AS LowEndVehicle,
   LEFT(trim(stocknumber), 5) + 'A' AS LowEndVehicleSource,
   i.VehicleItemID 
 FROM VehicleInventoryItems i
 INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
   AND cast(p.VehiclePricingTS AS sql_date) > curdate() - 450
   AND p.VehiclePricingTS = (
     SELECT MAX(vehiclePricingTS)
 	FROM VehiclePricings
 	WHERE VehicleInventoryItemID = p.VehicleInventoryItemID) 
 INNER JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID 
   AND d.Typ = 'VehiclePricingDetail_BestPrice'
   AND amount <= 8000
 WHERE substring(stocknumber, 6, 5) = 'B') x  


