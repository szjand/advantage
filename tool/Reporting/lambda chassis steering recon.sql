SELECT CAST(VehicleEvaluationTS AS sql_date) AS evalDate,
  bb.stocknumber, left(b.make, 12), left(b.model, 12), 
  b.yearmodel, c.value AS miles,
  CAST(d.soldTS AS sql_date) AS saleDate, 
  e.description
FROM VehicleEvaluations a
INNER JOIN VehicleItems b on a.VehicleItemID = b.VehicleItemID
LEFT JOIN VehicleInventoryItems bb on a.VehicleInventoryItemID = bb.VehicleInventoryItemID 
LEFT JOIN VehicleItemMileages c on b.VehicleItemID = c.VehicleItemID
  AND VehicleItemMileageTS = (
    SELECT MAX(VehicleItemMileageTS)
    FROM VehicleItemMileages
    WHERE VehicleItemID = b.VehicleItemID)
LEFT JOIN vehicleSales d on a.VehicleInventoryItemID = d.VehicleInventoryItemID  
LEFT JOIN typDescriptions e on d.typ = e.typ  
WHERE yearmodel IN ('2008','2009','2010','2011')
  AND (
    model LIKE '%enclave%'
    OR model LIKE '%acadia%'
    OR model LIKE '%traverse%')
  AND c.value > 30000    