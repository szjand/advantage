SELECT year(s.soldTS) AS YearSold, i.make, x.typ
-- SELECT COUNT(*) -- 4979 VehicleInventoryItems: 3075, VehicleItems: 420
--INTO #jon
FROM VehicleSales s 
INNER JOIN VehicleInventoryItems v ON s.VehicleInventoryItemID = v.VehicleInventoryItemID
  AND v.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')
INNER JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
  AND i.make IN ('Pontiac','Saturn')  
LEFT JOIN (
  SELECT VehicleInventoryItemID, typ
  FROM SelectedReconPackages r
  WHERE SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS)
    FROM SelectedReconPackages
    WHERE VehicleInventoryItemID = r.VehicleInventoryItemID
    GROUP BY VehicleInventoryItemID)) x ON s.VehicleInventoryItemID = x.VehicleInventoryItemID  
WHERE s.typ = 'VehicleSale_Retail'
  AND year(s.SoldTS) IN (2010, 2011)
  
SELECT make, yearsold,
  CASE
    WHEN typ LIKE '%Factory' THEN 'Factory'
	ELSE 'Other'
  END AS Package, COUNT(*)
FROM (
SELECT year(s.soldTS) AS YearSold, i.make, x.typ
-- SELECT COUNT(*) -- 4979 VehicleInventoryItems: 3075, VehicleItems: 420
--INTO #jon
FROM VehicleSales s 
INNER JOIN VehicleInventoryItems v ON s.VehicleInventoryItemID = v.VehicleInventoryItemID
  AND v.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')
INNER JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
  AND i.make IN ('Pontiac','Saturn')  
LEFT JOIN (
  SELECT VehicleInventoryItemID, typ
  FROM SelectedReconPackages r
  WHERE SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS)
    FROM SelectedReconPackages
    WHERE VehicleInventoryItemID = r.VehicleInventoryItemID
    GROUP BY VehicleInventoryItemID)) x ON s.VehicleInventoryItemID = x.VehicleInventoryItemID  
WHERE s.typ = 'VehicleSale_Retail'
  AND year(s.SoldTS) IN (2010, 2011)) y  
GROUP BY make, yearsold, package


 

