SELECT *
FROM VehicleInspections

SELECT year(CAST(a.ts as sql_date)) 
FROM VehicleReconItems a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
WHERE a.basistable = 'vehicleinspections'
  AND year(CAST(a.ts as sql_date)) = 2018
 
DROP TABLE #vehicles;

-- nice OR certified sold retail IN 2018
SELECT a.VehicleInventoryItemID, b.stocknumber, c.vin, CAST(soldts AS sql_date) AS sale_date, d.typ
INTO #vehicles
-- SELECT COUNT(*) -- 3033, 2974, 2045
FROM vehiclesales a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN SelectedReconPackages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS)
	FROM SelectedReconPackages 
	WHERE VehicleInventoryItemID = d.VehicleInventoryItemID)
--  AND d.typ IN ('ReconPackage_Nice','ReconPackage_Factory')
WHERE a.typ = 'VehicleSale_Retail'
  AND year(CAST(a.soldts as sql_date)) = 2018
  AND a.status = 'VehicleSale_Sold'
  
select * 
FROM #vehicles a WHERE LEFT(stocknumber, 1) = 'h'



SELECT sum(v.TotalPartsAmount + v.LaborAmount) AS Est, 'Not Auth'
FROM VehicleReconItems v
LEFT JOIN AuthorizedReconItems a ON v.VehicleReconItemID = a.VehicleReconItemID 
WHERE VehicleInventoryItemID = '9dc78bf7-8587-4bb0-88bb-03394ea67084'
  AND a.VehicleReconItemID IS NULL
UNION 
SELECT sum(v.TotalPartsAmount + v.LaborAmount) AS Est, 'Removed'
FROM VehicleReconItems v
INNER JOIN AuthorizedReconItems a ON v.VehicleReconItemID = a.VehicleReconItemID
WHERE VehicleInventoryItemID = '9dc78bf7-8587-4bb0-88bb-03394ea67084'
  AND a.typ = 'AuthorizedReconItem_Removed'
UNION 
SELECT sum(v.TotalPartsAmount + v.LaborAmount) AS Est, 'Due Bill'
FROM VehicleReconItems v
INNER JOIN AuthorizedReconItems a ON v.VehicleReconItemID = a.VehicleReconItemID
WHERE VehicleInventoryItemID = '9dc78bf7-8587-4bb0-88bb-03394ea67084'
  AND a.typ = 'AuthorizedReconItem_TransferredToDueBill';  