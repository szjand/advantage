CREATE TABLE zBOPTRAD (
  btco# cichar(3),
  btkey integer, 
  btvin cichar(17),
  btstk# cichar(20),
  bttrad money, 
  btacv money) IN database;
  
  
SELECT a.stocknumber, b.vin, CAST(a.Fromts AS sql_Date) AS FromDate, 
  left(e.fullname, 25) AS Evaluator, d.selectedacv AS EvalACV,
  h.selectedacv AS WalkACV, 
  cast(f.btacv as sql_integer) AS ArkonaACV
-- SELECT COUNT(*)  
FROM VehicleInventoryItems a
LEFT JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID 
LEFT JOIN VehicleEvaluations c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN acvs d ON c.VehicleEvaluationID = d.tablekey
LEFT JOIN people e ON c.vehicleevaluatorid = e.partyid
LEFT JOIN zboptrad f ON a.stocknumber = trim(f.btstk#)
LEFT JOIN VehicleWalks g ON a.VehicleInventoryItemID = g.VehicleInventoryItemID 
LEFT JOIN acvs h ON g.VehicleWalkID = h.tablekey
WHERE cast(a.fromts AS sql_date) > curdate() - 90
  AND c.VehicleEvaluationID IS NOT NULL 
  AND d.basistable IS NOT NULL 
  AND f.btco# IS NOT NULL
  AND d.selectedacv <> f.btacv
ORDER BY a.fromts desc  