SELECT cast(VehicleEvaluationTS AS SQL_DATE) AS "Eval Date", 
  ReconMechanicalAmount + ReconBodyAmount + ReconAppearanceAmount + 
    ReconTireAmount + ReconGlassAmount + ReconInspectionAmount AS "Est Recon",
  p.fullname AS Evaluator, 
  trim(vioi.Firstname) + ' ' + TRIM(vioi.LastName) AS Customer,
  TRIM(vi.yearmodel) + ' ' + TRIM(Make) + ' ' + TRIM(model) AS Vehicle,
  (SELECT Amount FROM VehiclePriceComponents WHERE tablekey = ve.VehicleEvaluationID AND typ = 'VehiclePriceComponent_ACV') AS ACV,
  (SELECT Amount FROM VehiclePriceComponents WHERE tablekey = ve.VehicleEvaluationID AND typ = 'VehiclePriceComponent_Finished') AS Finished,
  AmountShown 
-- SELECT * 
FROM vehicleevaluations ve
LEFT JOIN people p ON p.PartyID = ve.VehicleEvaluatorID
LEFT JOIN VehicleItems vi ON vi.VehicleItemID = ve.VehicleItemID
LEFT JOIN VehicleItemOwnerInfo vioi ON vioi.VehicleItemID = ve.VehicleItemID AND vioi.ThruTS IS NULL 
WHERE CurrentStatus = 'VehicleEvaluation_Finished'
AND ReconMechanicalAmount + ReconBodyAmount + ReconAppearanceAmount + 
    ReconTireAmount + ReconGlassAmount + ReconInspectionAmount > 2000
AND VehicleEvaluationTS > '01/01/2010 00:00:00'    
ORDER BY vehicleevaluationts DESC

