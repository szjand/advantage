-- sales last 30
SELECT line, age, COUNT(*)
FROM (
  SELECT a.soldts, b.fromts, c.make,
    CASE 
      WHEN c.make IN ('Chevrolet', 'Saturn', 'Oldsmobile', 'GMC', 'Pontiac', 'Cadillac','Buick') THEN 'GM'
      ELSE 'Other'
    END AS Line,
    CASE
      WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_date), CAST(a.soldts AS sql_date)) BETWEEN 0 AND 30 THEN '0-30'
      WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_date), CAST(a.soldts AS sql_date)) BETWEEN 31 AND 59 THEN '31-59'
      WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_date), CAST(a.soldts AS sql_date)) > 59 THEN '60 and Older'
    END AS age
  FROM vehiclesales a
  INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.locationid = (
      SELECT partyid 
      FROM organizations
      WHERE name = 'rydells')
  LEFT JOIN VehicleItems c ON b.VehicleItemID = c.VehicleItemID     
  WHERE a.typ = 'VehicleSale_Retail'
    AND a.status = 'VehicleSale_Sold'
    AND CAST(a.soldts AS sql_Date) BETWEEN curdate() -31 AND curdate() -1) z
WHERE age IS NOT NULL   
GROUP BY line, age  

-- current inventory
SELECT line, age, COUNT(*)
FROM (
SELECT b.fromts, c.make,
  CASE 
    WHEN c.make IN ('Chevrolet', 'Saturn', 'Oldsmobile', 'GMC', 'Pontiac', 'Cadillac','Buick') THEN 'GM'
    ELSE 'Other'
  END AS Line,
  CASE
    WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_date), curdate()) BETWEEN 0 AND 30 THEN '0-30'
    WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_date), curdate()) BETWEEN 31 AND 59 THEN '31-59'
    WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_date), curdate()) > 59 THEN '60 and Older'
  END AS age
FROM VehicleInventoryItems b 
LEFT JOIN VehicleItems c ON b.VehicleItemID = c.VehicleItemID     
WHERE b.ThruTS IS null
  AND b.locationid = (
    SELECT partyid 
    FROM organizations
    WHERE name = 'rydells')) z
GROUP BY line, age    

-- cost of current inventory
SELECT gtctl#, SUM(gttamt) AS cost
INTO #cost
FROM dds.stgArkonaGLPTRNS
WHERE gtacct IN ('124000', '124100')
  AND gtpost <> 'V'
GROUP BY gtctl#
HAVING SUM(gttamt) > 0  

SELECT SUM(cost)
FROM (
  SELECT gtctl#, SUM(gttamt) AS cost
  FROM dds.stgArkonaGLPTRNS
  WHERE gtacct IN ('124000', '124100')
    AND gtpost <> 'V'
  GROUP BY gtctl#
  HAVING SUM(gttamt) > 0) b  

SELECT SUM(cost)
FROM (
SELECT coalesce(d.cost, 0) AS cost, b.stocknumber, b.fromts, c.make,
  CASE 
    WHEN c.make IN ('Chevrolet', 'Saturn', 'Oldsmobile', 'GMC', 'Pontiac', 'Cadillac','Buick') THEN 'GM'
    ELSE 'Other'
  END AS Line,
  CASE
    WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_date), curdate()) BETWEEN 0 AND 30 THEN '0-30'
    WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_date), curdate()) BETWEEN 31 AND 59 THEN '31-59'
    WHEN timestampdiff(sql_tsi_day, CAST(b.fromts AS sql_date), curdate()) > 59 THEN '60 and Older'
  END AS age
FROM VehicleInventoryItems b 
LEFT JOIN VehicleItems c ON b.VehicleItemID = c.VehicleItemID   
LEFT JOIN #cost d ON b.stocknumber = d.gtctl#
WHERE b.ThruTS IS null
  AND b.locationid = (
    SELECT partyid 
    FROM organizations
    WHERE name = 'rydells')) z
WHERE line = 'Other'  
  AND age = '60 and Older'  
    
