﻿drop table if exists sales cascade;
-- 10699 rows
create temp table sales as
select substring(d.vehicletype from position('_' in d.vehicletype) + 1 for 12)::citext as shape,
  substring(d.vehiclesegment from position('_' in d.vehiclesegment) + 1 for 12)::citext as size,
  e.price_band::citext,
  a.soldts::date as sale_date
from ads.ext_vehicle_sales a
inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join ads.ext_vehicle_items c on b.vehicleitemid = c.vehicleitemid
inner join ads.ext_make_model_classifications d on c.make = d.make 
  and c.model = d.model
left join greg.uc_price_bands e on a.soldamount between e.price_from and e.price_thru  
where a.status = 'VehicleSale_Sold'  
  and a.typ = 'VehicleSale_Retail'
--   and d.vehicletype like '%Pickup'
  and b.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  and a.soldts::date >= '01/01/2013';
create index on sales(shape);
create index on sales(size);
create index on sales(price_band);
create index on sales(sale_date);


select *,
  round(100.00 * pickup_ytd_2013/total_ytd_2013, 1) as pickup_perc_total_2013,
  round(100.00 * pickup_ytd_2014/total_ytd_2014, 1) as pickup_perc_total_2014,
  round(100.00 * pickup_ytd_2015/total_ytd_2015, 1) as pickup_perc_total_2015,
  round(100.00 * pickup_ytd_2016/total_ytd_2016, 1) as pickup_perc_total_2016,
  round(100.00 * pickup_ytd_2017/total_ytd_2017, 1) as pickup_perc_total_2017
from (
  select 
    sum(case when b.the_year = 2013 then 1 else 0 end) as total_ytd_2013,
    sum(case when b.the_year = 2014 then 1 else 0 end) as total_ytd_2014,
    sum(case when b.the_year = 2015 then 1 else 0 end) as total_ytd_2015,
    sum(case when b.the_year = 2016 then 1 else 0 end) as total_ytd_2016,
    sum(case when b.the_year = 2017 then 1 else 0 end) as total_ytd_2017,
    sum(case when b.the_year = 2013 and a.shape = 'Pickup' then 1 else 0 end) as pickup_ytd_2013,
    sum(case when b.the_year = 2014 and a.shape = 'Pickup' then 1 else 0 end) as pickup_ytd_2014,
    sum(case when b.the_year = 2015 and a.shape = 'Pickup' then 1 else 0 end) as pickup_ytd_2015,
    sum(case when b.the_year = 2016 and a.shape = 'Pickup' then 1 else 0 end) as pickup_ytd_2016,
    sum(case when b.the_year = 2017 and a.shape = 'Pickup' then 1 else 0 end) as pickup_ytd_2017,
    sum(case when b.year_month = 201310 and a.shape = 'Pickup' then 1 else 0 end) as pickup_mtd_2013,
    sum(case when b.year_month = 201410 and a.shape = 'Pickup' then 1 else 0 end) as pickup_mtd_2014,
    sum(case when b.year_month = 201510 and a.shape = 'Pickup' then 1 else 0 end) as pickup_mtd_2015,
    sum(case when b.year_month = 201610 and a.shape = 'Pickup' then 1 else 0 end) as pickup_mtd_2016,
    sum(case when b.year_month = 201710 and a.shape = 'Pickup' then 1 else 0 end) as pickup_mtd_2017
  from sales a
  inner join dds.dim_date b on a.sale_date = b.the_date
  where (
    sale_date between '01/01/2013' and '10/20/2013' or 
    sale_date between '01/01/2014' and '10/20/2014' or
    sale_date between '01/01/2015' and '10/20/2015' or
    sale_date between '01/01/2016' and '10/20/2016' or
    sale_date between '01/01/2017' and '10/20/2017')) C


-- pickup/glump/ytd
select *
from (
  select size, price_band
  from sales
  where size <> 'car'
    and price_band is not null 
  group by size, price_band) a
left join (
  select size, price_band,
    sum(case when b.the_year = 2013 then 1 else 0 end) as total_ytd_2013,
    sum(case when b.the_year = 2014 then 1 else 0 end) as total_ytd_2014,
    sum(case when b.the_year = 2015 then 1 else 0 end) as total_ytd_2015,
    sum(case when b.the_year = 2016 then 1 else 0 end) as total_ytd_2016,
    sum(case when b.the_year = 2017 then 1 else 0 end) as total_ytd_2017
  from sales a
  inner join dds.dim_date b on a.sale_date = b.the_date
  where shape = 'pickup'
    and (
      sale_date between '01/01/2013' and '10/20/2013' or 
      sale_date between '01/01/2014' and '10/20/2014' or
      sale_date between '01/01/2015' and '10/20/2015' or
      sale_date between '01/01/2016' and '10/20/2016' or
      sale_date between '01/01/2017' and '10/20/2017')  
  group by size, price_band) aa on a.size = aa.size and a.price_band = aa.price_band
where aa.size is not null  
order by a.size, a.price_band

-- pickup/glump/mtd
select *
from (
  select size, price_band
  from sales
  where size <> 'car'
    and price_band is not null 
  group by size, price_band) a
left join (
  select size, price_band,
    sum(case when b.year_month = 201310 then 1 else 0 end) as mtd_2013,
    sum(case when b.year_month = 201410 then 1 else 0 end) as mtd_2014,
    sum(case when b.year_month = 201510 then 1 else 0 end) as mtd_2015,
    sum(case when b.year_month = 201610 then 1 else 0 end) as mtd_2016,
    sum(case when b.year_month = 201710 then 1 else 0 end) as mtd_2017
  from sales a
  inner join dds.dim_date b on a.sale_date = b.the_date
  where shape = 'pickup'
    and (
      sale_date between '01/01/2013' and '10/20/2013' or 
      sale_date between '01/01/2014' and '10/20/2014' or
      sale_date between '01/01/2015' and '10/20/2015' or
      sale_date between '01/01/2016' and '10/20/2016' or
      sale_date between '01/01/2017' and '10/20/2017')  
  group by size, price_band) aa on a.size = aa.size and a.price_band = aa.price_band
where aa.size is not null  
order by a.size, a.price_band

-- pickup/size/ytd
select *
from (
  select size
  from sales
  where size <> 'car'
    and price_band is not null 
  group by size) a
left join (
  select size, 
    sum(case when b.the_year = 2013 then 1 else 0 end) as ytd_2013,
    sum(case when b.the_year = 2014 then 1 else 0 end) as ytd_2014,
    sum(case when b.the_year = 2015 then 1 else 0 end) as ytd_2015,
    sum(case when b.the_year = 2016 then 1 else 0 end) as ytd_2016,
    sum(case when b.the_year = 2017 then 1 else 0 end) as ytd_2017
  from sales a
  inner join dds.dim_date b on a.sale_date = b.the_date
  where shape = 'pickup'
    and (
      sale_date between '01/01/2013' and '10/20/2013' or 
      sale_date between '01/01/2014' and '10/20/2014' or
      sale_date between '01/01/2015' and '10/20/2015' or
      sale_date between '01/01/2016' and '10/20/2016' or
      sale_date between '01/01/2017' and '10/20/2017')  
  group by size) aa on a.size = aa.size 
where aa.size is not null   
order by a.size


-- pickup/size/mtd
select *
from (
  select size
  from sales
  where size <> 'car'
    and price_band is not null 
  group by size) a
left join (
  select size, 
    sum(case when b.year_month = 201310 then 1 else 0 end) as mtd_2013,
    sum(case when b.year_month = 201410 then 1 else 0 end) as mtd_2014,
    sum(case when b.year_month = 201510 then 1 else 0 end) as mtd_2015,
    sum(case when b.year_month = 201610 then 1 else 0 end) as mtd_2016,
    sum(case when b.year_month = 201710 then 1 else 0 end) as mtd_2017
  from sales a
  inner join dds.dim_date b on a.sale_date = b.the_date
  where shape = 'pickup'
    and (
      sale_date between '01/01/2013' and '10/20/2013' or 
      sale_date between '01/01/2014' and '10/20/2014' or
      sale_date between '01/01/2015' and '10/20/2015' or
      sale_date between '01/01/2016' and '10/20/2016' or
      sale_date between '01/01/2017' and '10/20/2017')  
  group by size) aa on a.size = aa.size 
where aa.size is not null   
order by a.size