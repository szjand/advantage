SELECT p.*, j.EstimateDate, j.RepairsComplete, j.ClosedDate
FROM (
  SELECT jobid, COUNT(*) as howmany
  FROM bsjobphotos p
  GROUP BY jobid) p
LEFT JOIN bsjobs j ON p.jobid = j.jobid  


SELECT p.jobid
FROM (
  SELECT jobid, COUNT(*) as howmany
  FROM bsjobphotos p
  GROUP BY jobid) p
INNER JOIN bsjobs j ON p.jobid = j.jobid  
  AND CAST(j.estimatedate AS sql_date) < '01/01/2010'
  
  
DELETE 
FROM bsjobphotos
WHERE jobid IN (
  SELECT p.jobid
  FROM (
    SELECT jobid, COUNT(*) as howmany
    FROM bsjobphotos p
    GROUP BY jobid) p
  INNER JOIN bsjobs j ON p.jobid = j.jobid  
    AND CAST(j.estimatedate AS sql_date) < '01/01/2010')
    
EXECUTE PROCEDURE sp_PackTable('BSJobPhotos')    