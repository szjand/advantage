SELECT 
  (SELECT Utilities.CurrentViiReconPromise(vii.VehicleInventoryItemID) FROM system.iota) AS ReconProm,
  CASE
    WHEN MechStatus = 'No Open Items' AND BodyStatus = 'No Open Items' AND AppearanceStatus = 'No Open Items' THEN 
      'All Recon Complete'
    ELSE 
      '<B>Mech:</B> ' + TRIM(MechStatus)+ '</br>   <B>Body:</B> ' + TRIM(BodyStatus) + '</br>   <B>Appearance:</B> ' + TRIM(AppearanceStatus)
    END AS ReconStatus,    
  vii.VehicleInventoryItemID, 
  vii.LocationID,
  vii.OwningLocationID,
  vii.StockNumber,
  vi.VIN,
  Coalesce(Current_Date() - Convert(viispulled.FromTS, SQL_DATE), 0) AS DaysSincePulled,
  vi.YearModel,
  vi.Make, 
  vi.Model,
  trim(vi.ExteriorColor) + '/' + TRIM(vi.InteriorColor) AS Color, 
/*  
  (SELECT TOP 1 vpd.Amount
      FROM VehiclePricings vp
      inner join VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID 
	    and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
      WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
      ORDER BY VehiclePricingTS DESC) as Price,        
  vii.CurrentPriority,
  
  (SELECT Description
    FROM TypDescriptions
	WHERE typ = (
      SELECT top 1 Typ 
        FROM SelectedReconPackages
        WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
        ORDER BY SelectedReconPackageTS DESC)
	 AND ThruTS IS NULL) AS SelectedPackage,  
*/      

    
--  Utilities.CurrentViiReconStatus(vii.VehicleInventoryItemID) AS ReconStatus,
--  rs.ReconStatus,

  ps.PartsStatus 
FROM VehicleItems vi
INNER JOIN VehicleInventoryItems vii
 ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc
          on mmc.Make = vi.Make
          AND mmc.Model = vi.Model
          AND mmc.ThruTS IS NULL
LEFT OUTER JOIN VehicleInventoryItemStatuses viispulled
  ON viispulled.VehicleInventoryItemID = vii.VehicleInventoryItemID
    AND viispulled.Status = 'RMFlagPulled_Pulled'
    AND viispulled.ThruTS IS NULL    
LEFT JOIN ( -- Parts Status for a VehicleInventoryItem
  SELECT * FROM (EXECUTE PROCEDURE GetViiPartsStatus()) wtf) ps ON vii.VehicleInventoryItemID = ps.VehicleInventoryItemID  
/*  
LEFT JOIN SelectedReconPackages srpx ON vii.VehicleInventoryItemID = srpx.VehicleInventoryItemID
  AND srpx.SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS)
    FROM SelectedReconPackages
    WHERE VehicleInventoryItemID = srpx.VehicleInventoryItemID 
    GROUP BY VehicleInventoryItemID)
*/    
LEFT JOIN ( -- Recon Status
  SELECT VehicleInventoryItemID,
          (SELECT 
            CASE status
              WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
              WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
              WHEN 'MechanicalReconProcess_NotStarted' THEN --'Not Started'
                CASE 
                  WHEN EXISTS (
                    SELECT 1 
                    FROM VehicleInventoryItemStatuses 
                    WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
                    AND category = 'MechanicalReconDispatched'
                    AND ThruTS IS NULL) THEN 'Dispatched'
                  ELSE 'Not Started'
                END 
            END         
            FROM VehicleInventoryItemStatuses
            WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID 
            AND category = 'MechanicalReconProcess'
            AND ThruTS IS NULL) AS MechStatus,
          (SELECT 
            CASE status
              WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
              WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
              WHEN 'BodyReconProcess_NotStarted' THEN --'Not Started'
                CASE 
                  WHEN EXISTS (
                    SELECT 1 
                    FROM VehicleInventoryItemStatuses 
                    WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
                    AND category = 'BodyReconDispatched'
                    AND ThruTS IS NULL) THEN 'Dispatched'
                  ELSE 'Not Started'
                END           
            END         
            FROM VehicleInventoryItemStatuses
            WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID 
            AND category = 'BodyReconProcess'
            AND ThruTS IS NULL) AS BodyStatus,
          (SELECT 
            CASE status
              WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
              WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
              WHEN 'AppearanceReconProcess_NotStarted' THEN --'Not Started'
                CASE 
                  WHEN EXISTS (
                    SELECT 1 
                    FROM VehicleInventoryItemStatuses 
                    WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
                    AND category = 'AppearanceReconDispatched'
                    AND ThruTS IS NULL) THEN 'Dispatched'
                  ELSE 'Not Started'
                END           
            END         
            FROM VehicleInventoryItemStatuses
            WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID 
            AND category = 'AppearanceReconProcess'
            AND ThruTS IS NULL) AS AppearanceStatus
  FROM VehicleInventoryItemStatuses vii 
    WHERE vii.ThruTS IS NULL
    AND vii.category IN ( 'MechanicalReconProcess', 'BodyReconProcess', 'AppearanceReconProcess')) rs ON vii.VehicleInventoryItemID = rs.VehicleInventoryItemID 

WHERE vii.ThruTS IS NULL
AND vii.VehicleInventoryItemID IN (
  SELECT viis_a.VehicleInventoryItemID 
  FROM VehicleInventoryItemStatuses viis_a
  WHERE viis_a.Status = 'RMFlagPulled_Pulled'
  AND viis_a.ThruTS IS NULL);

