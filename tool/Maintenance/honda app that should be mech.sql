-- honda vehicles with appearance WORK that needs to be recategorized AS mechanical
SELECT *
FROM (
  SELECT c.stocknumber, a.VehicleInventoryItemID, a.typ, LEFT(CAST(description AS sql_char),100) AS description
  FROM VehicleReconItems a
  INNER JOIN AuthorizedReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
    AND b.status = 'AuthorizedReconItem_NotStarted'
    AND EXISTS (
      SELECT 1
      FROM ReconAuthorizations 
      WHERE ReconAuthorizationID = b.ReconAuthorizationID 
      AND thruts IS NULL)
  INNER JOIN VehicleInventoryItems c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND LEFT(c.stocknumber, 1) = 'H'    
  WHERE (a.typ = 'PartyCollection_AppearanceReconItem' OR a.typ LIKE 'Appear%')) e
WHERE description NOT IN ('Car Buff','Full Detail','Truck Buff','Deluxe Wash')  



