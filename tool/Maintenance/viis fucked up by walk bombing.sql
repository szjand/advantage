-- here they are:
-- recon status = no OPEN items but there are OPEN items
-- this IS a symptom of the walk bombing occasionally
-- the status being wrong IS a result of VehicleInventoryItemStatuses NOT being
-- updated WHEN the walk bombs with the last CATCH IN submit error
SELECT *
FROM (
  SELECT a.*, Utilities.CurrentViiReconStatus(a.VehicleInventoryItemID) AS status 
  FROM (  
    SELECT a.stocknumber, a.VehicleInventoryItemID,
      SUM(CASE WHEN c.ReconAuthorizationID IS NULL THEN 0 ELSE 1 END) AS OpenItems
    FROM VehicleInventoryItems a
    LEFT JOIN VehicleReconItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    LEFT JOIN AuthorizedReconItems c ON c.VehicleReconItemID = b.VehicleReconItemID 
      AND c.status <> 'AuthorizedReconItem_Complete'
      AND c.completets IS NULL 
      AND EXISTS (
        SELECT 1
        FROM ReconAuthorizations 
        WHERE ReconAuthorizationID = c.ReconAuthorizationID
          AND ThruTS IS NULL)
    WHERE a.thruts IS NULL 
    GROUP BY a.stocknumber, a.VehicleInventoryItemID) a ) b 
WHERE status = 'All Recon Complete' AND openitems <> 0 


DECLARE @viiID string;
DECLARE @NowTS Timestamp;
DECLARE @UserID string;
@viiID = 'd7837fd7-9f0d-4925-9473-1a84e0b54b0f';
@UserID = (SELECT partyid FROM users WHERE username = 'jon');
@NowTS = Now();  
BEGIN TRANSACTION;
TRY
  EXECUTE procedure UpdateViis(@viiID, @UserID, @NowTS);
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;  
END;


SELECT a.stocknumber, utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID)
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.status = 'RMFlagWSB_WholesaleBuffer'
  AND b.thruts IS NULL
WHERE a.currentpriority = 0 
  
SELECT DISTINCT status FROM VehicleInventoryItemStatuses   
