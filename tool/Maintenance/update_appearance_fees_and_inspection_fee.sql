-------------------------------------------------------------------------
--< 7/16/22 Toyota
-------------------------------------------------------------------------
select * FROM partyattributes WHERE Typ = 'PartyAttribute_MechanicalReconInspectionFee'
SELECT * FROM organizations WHERE name = 'toyota'  -- '77a18168-998f-8747-ae24-3efc540cd069'

-- inspection fee
INSERT INTO partyattributes (partyid,typ,value)
values('77a18168-998f-8747-ae24-3efc540cd069','PartyAttribute_MechanicalReconInspectionFee','335');

SELECT * FROM partycollections WHERE thruts IS NULL ORDER BY partyid, typ
-- detail prices
INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts)
SELECT '77a18168-998f-8747-ae24-3efc540cd069',typ,value,amount,sequence,now()
FROM partycollections
WHERE partyid = '4CD8E72C-DC39-4544-B303-954C99176671'
  AND thruts IS NULL;
-------------------------------------------------------------------------
--/> 7/16/22 Toyota
-------------------------------------------------------------------------

SELECT b.name, a.* -- a.value, a.amount, a.sequence
FROM partycollections a
JOIN organizations b on a.partyid = b.partyid
--  AND name = 'Rydells'
WHERE a.thruts IS NULL
ORDER BY name, sequence 

full detail 220 -> 225
ADD full detail (with 3rd seat)  275
ADD truck buff (with 3rd seat) 375
UPDATE partycollections

sequence becomes
1 pricing rinse
2 deluxe wash
3 full detail  220 -> 225
*4 full detail (w/3rd seat) new 275
*4->5 car buff
*5->6 truck buff
*7 truck buff (w 3rd seat) new 375
*6->8 program vehicle 195 -> 165

select * 
FROM partyattributes
WHERE Typ = 'PartyAttribute_MechanicalReconInspectionFee'
  AND partyid IN ('B6183892-C79D-4489-A58C-B526DF948B06','4CD8E72C-DC39-4544-B303-954C99176671');
  
PartyCollections PK: PartyID;Typ;Sequence;ThruTS  
-- this looks good test it on backup first


DECLARE @ts timestamp;
@ts = now();

BEGIN TRANSACTION;
TRY 

-- first the inspection fee
UPDATE partyAttributes
SET value = '335'
WHERE typ = 'PartyAttribute_MechanicalReconInspectionFee'
  AND partyid IN ('B6183892-C79D-4489-A58C-B526DF948B06','4CD8E72C-DC39-4544-B303-954C99176671');
  
 -- now appearance fees 
UPDATE partycollections
SET thruts = now()
-- select * FROM partycollections
WHERE typ = 'PartyCollection_AppearanceReconItem'
  AND value = 'Full Detail'
  AND thruts IS null;   

UPDATE partycollections
SET thruts = @ts
-- select * FROM partycollections
WHERE typ = 'PartyCollection_AppearanceReconItem'
  AND value = 'Program Vehicle'
  AND thruts IS null;   

  
--UPDATE partycollections
--SET sequence = 8
--WHERE typ = 'PartyCollection_AppearanceReconItem'
--  AND value = 'Program Vehicle';   

UPDATE partycollections
SET sequence = 6
WHERE typ = 'PartyCollection_AppearanceReconItem'
  AND value = 'Truck Buff'
  AND thruts IS null;
      
UPDATE partycollections
SET sequence = 5
WHERE typ = 'PartyCollection_AppearanceReconItem'
  AND value = 'Car Buff'
  AND thruts IS null;

INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts)  
values('4CD8E72C-DC39-4544-B303-954C99176671','PartyCollection_AppearanceReconItem','Full Detail',225,3,TIMESTAMPADD( SQL_TSI_SECOND, 1, @ts));

INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts)  
values('B6183892-C79D-4489-A58C-B526DF948B06','PartyCollection_AppearanceReconItem','Full Detail',225,3,TIMESTAMPADD( SQL_TSI_SECOND, 1, @ts));

INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts)  
values('4CD8E72C-DC39-4544-B303-954C99176671','PartyCollection_AppearanceReconItem','Full Detail w/3rd Seat',275,4,TIMESTAMPADD( SQL_TSI_SECOND, 1, @ts));

INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts)  
values('B6183892-C79D-4489-A58C-B526DF948B06','PartyCollection_AppearanceReconItem','Full Detail w/3rd Seat',275,4,TIMESTAMPADD( SQL_TSI_SECOND, 1, @ts));

INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts)  
values('4CD8E72C-DC39-4544-B303-954C99176671','PartyCollection_AppearanceReconItem','Truck Buff w/3rd Seat',375,7,TIMESTAMPADD( SQL_TSI_SECOND, 1, @ts));

INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts)  
values('B6183892-C79D-4489-A58C-B526DF948B06','PartyCollection_AppearanceReconItem','Truck Buff w/3rd Seat',375,7,TIMESTAMPADD( SQL_TSI_SECOND, 1, @ts));

INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts)  
values('B6183892-C79D-4489-A58C-B526DF948B06','PartyCollection_AppearanceReconItem','Program Vehicle',165,8,TIMESTAMPADD( SQL_TSI_SECOND, 1, @ts));

INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts)  
values('4CD8E72C-DC39-4544-B303-954C99176671','PartyCollection_AppearanceReconItem','Program Vehicle',165,8,TIMESTAMPADD( SQL_TSI_SECOND, 1, @ts));

COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;   