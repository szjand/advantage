-- need to exclude off hours
SELECT v.stocknumber, p.fullname, m.fromts, m.thruts, trim(cast(timestampdiff(sql_tsi_hour, m.fromts, m.thruts) AS sql_char)) + ' Hours' AS Waiting, status
-- SELECT * 
FROM VehiclesToMove m
LEFT JOIN VehicleInventoryItems v ON m.VehicleInventoryItemID = v.VehicleInventoryItemID 
LEFT JOIN people p ON m.schedulerid = p.partyid
WHERE m.ThruTS > CAST('08/18/2011 01:01:01' AS sql_timestamp)