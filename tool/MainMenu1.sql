SELECT COUNT(Total) AS Total, COUNT(InTransit) AS InTransit, MAX(bookingpending) AS BookingPending,
  COUNT(TradeBuffer) AS TradeBuffer, COUNT(InspectionPending) AS InspectionPending,
  COUNT(WalkPending) AS WalkPending, COUNT(RawMaterialsPool) AS RawMaterialsPool,
  COUNT(ReconBuffer) AS ReconBuffer

FROM (
  SELECT viix.VehicleInventoryItemID AS Total,
    it.VehicleInventoryItemID AS InTransit,
	(SELECT count(VehicleInventoryItemID) -- vii.thruts IS NOT null
	  FROM VehicleInventoryItemStatuses 
	  WHERE status = 'RawMaterials_BookingPending'
	  AND ThruTS IS NULL) AS BookingPending,
	tb.VehicleInventoryItemID AS TradeBuffer,
	ip.VehicleInventoryItemID AS InspectionPending,
    wp.VehicleInventoryItemID AS WalkPending,
    rm.VehicleInventoryItemID AS RawMaterialsPool,
    rb.VehicleInventoryItemID AS ReconBuffer
  FROM VehicleInventoryItems viix
  LEFT JOIN VehicleInventoryItemStatuses it ON viix.VehicleInventoryItemID = it.VehicleInventoryItemID
    AND it.status = 'RMFlagPIT_PurchaseInTransit'
    AND it.thruts IS NULL
  LEFT JOIN VehicleInventoryItemStatuses tb ON viix.VehicleInventoryItemID = tb.VehicleInventoryItemID
    AND tb.status = 'RMFlagPIT_PurchaseInTransit'
    AND tb.thruts IS NULL	
  LEFT JOIN VehicleInventoryItemStatuses ip ON viix.VehicleInventoryItemID = ip.VehicleInventoryItemID
    AND ip.status = 'RMFlagIP_InspectionPending'
    AND ip.thruts IS NULL	
	AND NOT EXISTS (
	  SELECT 1
	  FROM VehicleInventoryItemStatuses 
	  WHERE VehicleInventoryItemID = ip.VehicleInventoryItemID
      AND (status = 'RMFlagTNA_TradeNotAvailable' OR status = 'RMFlagPIT_PurchaseInTransit') 
      AND ThruTS IS NULL)
  LEFT JOIN VehicleInventoryItemStatuses wp ON viix.VehicleInventoryItemID = wp.VehicleInventoryItemID 
    AND wp.status = 'RMFlagWP_WalkPending'
    AND wp.thruts IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = wp.VehicleInventoryItemID
      AND status = 'RMFlagIP_InspectionPending'
      AND ThruTS IS NULL)
  LEFT JOIN VehicleInventoryItemStatuses rm ON viix.VehicleInventoryItemID = rm.VehicleInventoryItemID 
    AND rm.status = 'RMFlagRMP_RawMaterialsPool'
    AND rm.thruts IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = rm.VehicleInventoryItemID
      AND status = 'RMFlagRB_ReconBuffer'
      AND ThruTS IS NULL) 
  LEFT JOIN VehicleInventoryItemStatuses rb ON viix.VehicleInventoryItemID = rb.VehicleInventoryItemID 
    AND rb.status = 'RMFlagRB_ReconBuffer'
    AND rb.thruts IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = rb.VehicleInventoryItemID
--      AND status LIKE '%ReconProcess_InProcess'
      AND status NOT IN('MechanicalReconProcess_InProcess','AppearanceReconProcess_InProcess','BodyReconProcess_InProcess')
      AND ThruTS IS NULL)            
  WHERE viix.ThruTS IS NULL) wtf
  

