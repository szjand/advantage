SELECT month(vii.ThruTS), rp.typ, COUNT(*)
FROM VehicleInventoryItems vii
INNER JOIN vehiclesales vs ON vs.VehicleInventoryItemID = vii.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, typ
  FROM SelectedReconPackages srp
  WHERE SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS)
    FROM SelectedReconPackages
    WHERE VehicleInventoryItemID = srp.VehicleInventoryItemID 
    GROUP BY VehicleInventoryItemID)) rp ON rp.VehicleInventoryItemID = vii.VehicleInventoryItemID 
WHERE vii.thruts IS not NULL
AND year(vii.thruts) = 2010
AND vs.typ = 'VehicleSale_Retail'
GROUP BY month(vii.ThruTS), rp.typ
ORDER BY rp.typ

SELECT 
  CASE typ
    WHEN 'ReconPackage_AsIs' THEN 'As Is'
    WHEN 'ReconPackage_AsIsWS' THEN 'As Is WS'
    WHEN 'ReconPackage_Factory' THEN 'Factory'
    WHEN 'ReconPackage_Nice' THEN 'Nice'
    WHEN 'ReconPackage_WS' THEN 'WS'
  END AS Package, HowMany AS [Count]
FROM ( 
SELECT rp.typ, COUNT(*) AS HowMany
FROM VehicleInventoryItems vii
INNER JOIN vehiclesales vs ON vs.VehicleInventoryItemID = vii.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, typ
  FROM SelectedReconPackages srp
  WHERE SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS)
    FROM SelectedReconPackages
    WHERE VehicleInventoryItemID = srp.VehicleInventoryItemID 
    GROUP BY VehicleInventoryItemID)) rp ON rp.VehicleInventoryItemID = vii.VehicleInventoryItemID 
WHERE vii.thruts IS not NULL
AND year(vii.thruts) = 2010
AND vs.typ = 'VehicleSale_Retail'
GROUP BY rp.typ) wtf
