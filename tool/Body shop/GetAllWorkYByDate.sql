SELECT 
  CASE WHEN (SELECT Top 1 1 FROM JobTasks jt WHERE jt.JobID = bsj.JobID AND Task = 'BodyShop_Metal') is NULL 
         THEN 1 //Never been a task
       WHEN (SELECT top 1 1 FROM JobTasks jt WHERE jt.JobID = bsj.JobID AND Task = 'BodyShop_Metal' AND CompletedTS IS NULL) IS NOT NULL
         THEN // There IS a job that IS NOT complete. What state IS it in?  
            CASE WHEN (SELECT 1 FROM JobTasks  jt WHERE jt.JobID = bsj.JobID AND Task = 'BodyShop_Metal' AND jt.TaskGroupID IS NULL AND CompletedTS IS null) IS not NULL
             THEN 2 //it IS a task but has NOT been assigned
             ELSE
               CASE WHEN (SELECT top 1 1 FROM TechClockTimes tct WHERE tct.TaskGroupID = (SELECT jt.TaskGroupID                                                                                            
                                                                                           FROM JobTasks jt 
                                                                                           WHERE jt.JobID = bsj.JobID 
                                                                                             AND Task = 'BodyShop_Metal' 
                                                                                             AND CompletedTS IS NULL)) IS NOT NULL  //Are there no clock times - NOT started    
                 THEN 4 //assigned AND started, but NOT finished
                 ELSE 3 //assigned but NOT started
               END
           END
         ELSE 5 //completed task
  END AS M, bsj.*
FROM bsjobs bsj    


SELECT a.jobid, b.task, b.taskgroupid, b.completedts, c.fromts, c.thruts,
  CASE
    WHEN b.task IS NULL THEN 1
    WHEN c.fromts IS NOT NULL THEN 2
  END AS metal
FROM bsjobs a
LEFT JOIN jobtasks b ON a.jobid = b.jobid
LEFT JOIN techclocktimes c ON b.taskgroupid = c.taskgroupid
WHERE a.active = true


SELECT *
FROM JobTasks
WHERE taskgroupid IS NOT NULL 

-- only 3 rows, must be anomalous data
SELECT jobid, task, taskgroupid
from jobtasks
GROUP BY jobid, task, taskgroupid
HAVING COUNT(*) > 1

JobTasks PK: JobID;Task;CompletedTS
SELECT *
FROM jobtasks a
INNER JOIN jobtasks b ON a.jobid = b.jobid
  AND a.task = b.task
WHERE a.completedts IS NULL
  AND b.completedts IS NOT NULL   
  
SELECT jobid, task,   
  