CREATE VIEW 
   vUsedCarsWalkPending_new
AS 
   SELECT 
/*
replace pre wash with factory
DROP view vUsedCarsWalkPending_new;
SELECT * FROM vUsedCarsWalkPending_new
*/

  viisIP.ThruTS AS InspThruTS,
  vii.VehicleInventoryItemID,
  iif(vi.VinResolved = True,
    'WalkVehicle.aspx?id=' + Trim(vii.VehicleInventoryItemID),
    'WalkVehicleAlt.aspx?id=' + Trim(vii.VehicleInventoryItemID)) AS RedirectString,
  iif(vi.VinResolved = True, '', 'ALT') AS PrintString, 
  vii.LocationID,
  vii.OwningLocationID, 
  (SELECT Name FROM Organizations WHERE PartyID = vii.OwningLocationID) AS Location,
  vii.StockNumber,
  vi.VIN,
  Coalesce(Current_Date() - cast(vii.FromTS as SQL_DATE), 0) AS DaysOwned,
  Current_Date() - cast(viisIP.ThruTS AS SQL_DATE) AS DaysInStatus,
  Coalesce(Trim(vi.Trim),'') + ' '  + Coalesce(Trim(vi.BodyStyle), '') + ' ' + 
  Coalesce(Trim(vi.Engine), '') AS VehicleDescription,
  vi.YearModel,
  vi.Make, 
  vi.Model,
--  CASE 
--    WHEN l.FlowTaskTS IS NOT NULL THEN True
--	ELSE False
--  END AS PreWash,
--  CASE 
--    WHEN l.FlowTaskTS IS NOT NULL THEN False
--	ELSE True
--  END AS NotPreWash,
  iif(position('Factory' IN o.typ) <> 0, 'X', '') AS factory,
  coalesce(a.keystatus, 'Unknown') AS KeyStatus,
  exteriorcolor AS color,
  CASE 
    WHEN length(Trim(m.locationdetail)) = 0 THEN n.locationShortName
    else trim(n.locationShortName) + ' - ' + LEFT(m.locationDetail, 25)
  END AS vehicle_location        
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii ON vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
INNER JOIN VehicleEvaluations ve ON ve.VehicleInventoryItemID = viis.VehicleInventoryItemID 
INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc
  on mmc.Make = vi.Make
  AND mmc.Model = vi.Model
  AND mmc.ThruTS IS NULL
LEFT JOIN VehicleInventoryItemStatuses viisIP ON viisIP.VehicleInventoryItemID = viis.VehicleInventoryItemID
  AND viisIP.status = 'RMFlagIP_InspectionPending'   
LEFT JOIN ViiFlowTasks l ON vii.VehicleInventoryItemID = l.VehicleInventoryItemID 
  AND l.typ = 'ViiFlowTask_PreWash'		
LEFT JOIN keyperkeystatus a ON vii.stocknumber = a.stocknumber         
LEFT JOIN VehicleItemPhysicalLocations m on vii.VehicleInventoryItemID = m.VehicleInventoryItemID 
  AND m.thruts IS NULL 
LEFT JOIN LocationPhysicalVehicleLocations n on m.physicalVehicleLocationID = n.physicalVehicleLocationID   
LEFT JOIN selectedreconpackages o on viis.VehicleInventoryItemID = o.VehicleInventoryItemID 
  AND o.SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS)
    FROM SelectedReconPackages
    WHERE VehicleInventoryItemID = o.VehicleInventoryItemID)
WHERE viis.Status = 'RMFlagWP_WalkPending'
  AND viis.ThruTS IS NULL
  AND NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
    AND status = 'RMFlagIP_InspectionPending'
    AND ThruTS IS NULL)

