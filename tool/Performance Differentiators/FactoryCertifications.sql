-- 240 ms
SELECT viix.stocknumber, fc.*
FROM FactoryCertifications fc
INNER JOIN VehicleInventoryItems viix ON fc.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.ThruTS IS NULL 
  AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'Rydells')
INNER JOIN (  
  SELECT VehicleInventoryItemID, MAX(SelectedReconPackageTS) 
  FROM selectedreconpackages
  WHERE typ = 'ReconPackage_Factory'
  AND VehicleInventoryItemID IS NOT NULL 
  GROUP BY VehicleInventoryItemID) p ON fc.VehicleInventoryItemID = p.VehicleInventoryItemID 
AND fc.cert = ''

 
-- 50 msec 
SELECT viix.stocknumber, 
  (SELECT vin FROM VehicleItems WHERE VehicleItemID = viix.VehicleItemID),
  (SELECT model FROM VehicleItems WHERE VehicleItemID = viix.VehicleItemID)
FROM VehicleInventoryItems viix
INNER JOIN selectedreconpackages p ON viix.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND p.typ = 'ReconPackage_Factory'
  AND p.SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS) 
    FROM selectedreconpackages
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    GROUP BY VehicleInventoryItemID)   
INNER JOIN FactoryCertifications fc ON viix.VehicleInventoryItemID = fc.VehicleInventoryItemID 
  AND fc.cert = ''  
WHERE viix.ThruTS IS NULL
AND viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'Rydells')  
ORDER BY stocknumber
   