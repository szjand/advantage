SELECT category, COUNT(*)
FROM VehicleInventoryItemStatuses
WHERE ThruTS IS NULL
AND category NOT LIKE '%econ%'
GROUP BY category
-- 140 msec
SELECT viix.stocknumber
FROM VehicleInventoryItems viix
WHERE viix.ThruTS IS NULL
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND category = 'RMFlagPulled'
  AND ThruTS IS NULL)
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND category = 'RMFlagAtAuction'
  AND ThruTS IS NULL)  
  
-- 7 msec  
SELECT viix.stocknumber
FROM VehicleInventoryItems viix
INNER JOIN  VehicleInventoryItemStatuses p on p.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND p.category = 'RMFlagPulled'
  AND p.ThruTS IS NULL
INNER JOIN  VehicleInventoryItemStatuses a on a.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND a.category = 'RMFlagAtAuction'
  AND a.ThruTS IS NULL 
WHERE viix.ThruTS IS NULL     