/*
SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID = 'eb151bc2-21c9-495a-80d8-e070c21d47fd'
SELECT * FROM AuthorizedReconItems
SELECT * FROM ReconAuthorizations



SELECT VehicleInventoryItemID, COUNT(*)
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID
WHERE vrix.typ LIKE 'Mec%'
AND arix.status = 'AuthorizedReconItem_NotStarted'
AND EXISTS (
  SELECT 1
  FROM ReconAuthorizations
  WHERE thruts IS NULL
  AND ReconAuthorizationID = arix.ReconAuthorizationID)
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItems
  WHERE VehicleInventoryItemID = vrix.VehicleInventoryItemID
  AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells'))  
GROUP BY VehicleInventoryItemID 
ORDER BY COUNT(*) DESC
*/
/*
SELECT DISTINCT vrix.VehicleInventoryItemID, vrix.VehicleReconItemID, vrix.PartsInStock AS PartsReqd,
  po.OrderedTS, po.ReceivedTS,
  CASE
    WHEN vrix.PartsInStock = false THEN 'No Parts Required'
	WHEN po.OrderedTS IS NULL THEN 'Not Ordered'
	WHEN po.ReceivedTS IS NULL THEN 'Ordered'
	ELSE 'Received'
  END AS PartsStatus
*/
/*
SELECT DISTINCT vii.Stocknumber,
  CASE
    WHEN NOT EXISTS ( -- 
	  SELECT 1
	  FROM VehicleReconItems
	  WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
	  AND PartsInStock = true) THEN 'None Reqd'

    WHEN NOT EXISTS (
	  SELECT 1 FROM PartsOrders
	  WHERE VehicleReconItemID = vrix.VehicleReconItemID) THEN 'Not Ordered'
	  
	WHEN EXISTS ( -- must be at least one line requiring parts
	  SELECT 1
	  FROM VehicleReconItems vri1
	  WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
	  AND NOT EXISTS (
	    SELECT 1
		FROM PartsOrders
		WHERE VehicleReconItemID = vri1.VehicleReconItemID 
		AND ReceivedTS IS NULL)) THEN 'Received'
	ELSE 'WTF'
  END AS PartsStatus
*/  
-- SELECT *	  
SELECT DISTINCT vrix.VehicleInventoryItemID, vrix.VehicleReconItemID, vrix.PartsInStock AS PartsReqd,
  po.OrderedTS, po.ReceivedTS,
  CASE
    WHEN vrix.PartsInStock = false THEN 'No Parts Required'
	WHEN po.OrderedTS IS NULL THEN 'Not Ordered'
	WHEN po.ReceivedTS IS NULL THEN 'Ordered'
	ELSE 'Received'
  END AS PartsStatus  
   
FROM VehicleInventoryItems vii
INNER JOIN VehicleInventoryItemStatuses viis ON viis.VehicleInventoryItemID = vii.VehicleInventoryItemID 
  AND viis.ThruTS IS NULL  
INNER JOIN VehicleItems vi on vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc on mmc.Make = vi.Make
  AND mmc.Model = vi.Model
  AND mmc.ThruTS IS NULL
INNER JOIN (
  SELECT ra.VehicleInventoryItemID
  FROM ReconAuthorizations ra
  WHERE EXISTS (
    SELECT 1
    FROM AuthorizedReconItems 
    WHERE ReconAuthorizationID = ra.ReconAuthorizationID
    AND status <> 'AuthorizedReconItem_Complete')) r ON r.VehicleInventoryItemID = vii.VehicleInventoryItemID 
LEFT JOIN VehicleReconItems vrix ON vii.VehicleInventoryItemID = vrix.VehicleInventoryItemID
LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 	
WHERE vii.ThruTS IS NULL
AND vii.VehicleInventoryItemID IN (
  '99bbc90f-f1ee-4866-ac25-40fa33ff4506' /*11012B*/,
  '822996d5-053b-45b3-92d2-9c2b85d20614' /*12890a*/,
  'd01c6153-c205-4408-86f9-674c8515674a' /*12658a*/,
  'fd03719b-7121-48af-9ff0-4b13265d4e3d' /*11040b*/)
  
  
SELECT 
  sum(CASE WHEN VehicleSegment = 'VehicleSegment_Unknown' THEN 1 ELSE 0 end) AS SegmentCountUnknown
FROM   

SELECT (select stocknumber from VehicleInventoryItems WHERE VehicleInventoryItemID = vrix.VehicleInventoryItemID) AS Stock, 
  vrix.VehicleInventoryItemID,
  vrix.description, vrix.PartsInStock AS "Parts Reqd", 
  po.OrderedTS, po.ReceivedTS
FROM VehicleReconItems vrix
LEFT JOIN PartsOrders po ON po.VehicleReconItemID = vrix.VehicleReconItemID 
WHERE vrix.VehicleInventoryItemID IN (
  'eb151bc2-21c9-495a-80d8-e070c21d47fd' /*11080d*/,
  '822996d5-053b-45b3-92d2-9c2b85d20614' /*12890a*/,
  'd01c6153-c205-4408-86f9-674c8515674a' /*12658a*/,
  'fd03719b-7121-48af-9ff0-4b13265d4e3d' /*11040b*/)  
AND vrix.typ LIKE 'Me%' 
AND vrix.typ <> 'MechanicalReconItem_Inspection'
AND EXISTS (
  SELECT 1
  FROM AuthorizedReconItems ar
  WHERE VehicleReconItemID = vrix.VehicleReconItemID 
  AND status <> 'AuthorizedReconItem_Complete'
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations
	WHERE ReconAuthorizationID = ar.ReconAuthorizationID
	AND ThruTS IS NULL))
ORDER BY vrix.VehicleInventoryItemID   




SELECT vrix.VehicleInventoryItemID, 
  SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) AS PartsRequd,
  SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NOT NULL THEN 1 ELSE 0 END) AS Ordered,
  SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) AS Received
FROM VehicleReconItems vrix
LEFT JOIN PartsOrders po ON po.VehicleReconItemID = vrix.VehicleReconItemID 
WHERE vrix.VehicleInventoryItemID IN (
  'eb151bc2-21c9-495a-80d8-e070c21d47fd' /*11080d*/,
  '822996d5-053b-45b3-92d2-9c2b85d20614' /*12890a*/,
  'd01c6153-c205-4408-86f9-674c8515674a' /*12658a*/,
  'fd03719b-7121-48af-9ff0-4b13265d4e3d' /*11040b*/)  
AND vrix.typ LIKE 'Me%' 
AND vrix.typ <> 'MechanicalReconItem_Inspection'
AND EXISTS (
  SELECT 1
  FROM AuthorizedReconItems ar
  WHERE VehicleReconItemID = vrix.VehicleReconItemID 
  AND status <> 'AuthorizedReconItem_Complete'
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations
	WHERE ReconAuthorizationID = ar.ReconAuthorizationID
	AND ThruTS IS NULL))
group BY vrix.VehicleInventoryItemID 


-- Parts status for a vehicle

  
  SELECT vrix.VehicleInventoryItemID, 
    CASE
	  WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = 0 THEN 'None'
	  ELSE
 	    CASE 
    	  WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 'Kitted'
    	  ELSE
    	    CASE 
    		  WHEN SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ordered'
    		  ELSE 'Ordered'
            END 
		END 
	END AS PartsStatus
  FROM VehicleReconItems vrix
  LEFT JOIN PartsOrders po ON po.VehicleReconItemID = vrix.VehicleReconItemID 
  WHERE vrix.VehicleInventoryItemID IN (
    'eb151bc2-21c9-495a-80d8-e070c21d47fd' /*11080d*/,
    '822996d5-053b-45b3-92d2-9c2b85d20614' /*12890a*/,
    'd01c6153-c205-4408-86f9-674c8515674a' /*12658a*/,
    'fd03719b-7121-48af-9ff0-4b13265d4e3d' /*11040b*/)  
  AND vrix.typ LIKE 'Me%' 
  AND vrix.typ <> 'MechanicalReconItem_Inspection'
  AND EXISTS (
    SELECT 1
    FROM AuthorizedReconItems ar
    WHERE VehicleReconItemID = vrix.VehicleReconItemID 
    AND status <> 'AuthorizedReconItem_Complete'
    AND EXISTS (
      SELECT 1
  	  FROM ReconAuthorizations
  	  WHERE ReconAuthorizationID = ar.ReconAuthorizationID
  	  AND ThruTS IS NULL))
  GROUP BY vrix.VehicleInventoryItemID  
  
  
  
SELECT vrix.VehicleInventoryItemID, 
  CASE
    WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = 0 THEN 'None'
    ELSE
      CASE 
  	    WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 'Kitted'
  	    ELSE
  	      CASE 
  		    WHEN SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ordered'
  		    ELSE 'Ordered'
          END 
      END 
  END AS PartsStatus
-- SELECT COUNT(DISTINCT vrix.VehicleReconItemID )   
FROM VehicleReconItems vrix
INNER JOIN PartsOrders po ON po.VehicleReconItemID = vrix.VehicleReconItemID 
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations 
	WHERE ReconAuthorizationID = arix.ReconAuthorizationID
	AND thruts IS NULL)
AND vrix.typ LIKE 'Me%' 
AND vrix.typ <> 'MechanicalReconItem_Inspection'
GROUP BY vrix.VehicleInventoryItemID    