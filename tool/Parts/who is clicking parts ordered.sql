SELECT p.fullname Who, COUNT(*) AS "How Many"
-- SELECT *
FROM partsorders o
LEFT JOIN people p ON o.orderedby = p.partyid
WHERE o.VehicleReconItemID IN (
  SELECT VehicleReconItemID
  FROM VehicleReconItems
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItems
    WHERE LEFT(stocknumber, 1) = '1'))
AND CAST(orderedts AS sql_date) > '04/08/2012'    
GROUP BY p.fullname


SELECT p.fullname Who, COUNT(*) AS "How Many"
-- SELECT *
FROM partsorders o
LEFT JOIN people p ON o.receivedby = p.partyid
WHERE o.VehicleReconItemID IN (
  SELECT VehicleReconItemID
  FROM VehicleReconItems
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItems
    WHERE LEFT(stocknumber, 1) = '1'))
AND CAST(receivedts AS sql_date) > '04/08/2012'    
GROUP BY p.fullname



SELECT *
FROM (
  SELECT p.fullname AS [Ordered By], COUNT(*) AS "How Many"
  -- SELECT *
  FROM partsorders o
  LEFT JOIN people p ON o.orderedby = p.partyid
  WHERE o.VehicleReconItemID IN (
  SELECT VehicleReconItemID
  FROM VehicleReconItems
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItems
    WHERE LEFT(stocknumber, 1) = '1'))
  AND CAST(orderedts AS sql_date) > '04/08/2012'    
  GROUP BY p.fullname) a
LEFT JOIN (
  SELECT p.fullname AS [Received By], COUNT(*) AS "How Many"
  -- SELECT *
  FROM partsorders o
  LEFT JOIN people p ON o.receivedby = p.partyid
  WHERE o.VehicleReconItemID IN (
    SELECT VehicleReconItemID
    FROM VehicleReconItems
    WHERE VehicleInventoryItemID IN (
      SELECT VehicleInventoryItemID 
      FROM VehicleInventoryItems
      WHERE LEFT(stocknumber, 1) = '1'))
  AND CAST(receivedts AS sql_date) > '04/08/2012'    
  GROUP BY p.fullname)b ON a.[Ordered By] = b.[Received By]
  