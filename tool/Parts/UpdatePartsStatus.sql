/*
3/12
IF parts totalParts + PartsQuantity > 0 SET PartsInStock = True
*/

/*
SELECT * FROM VehicleReconItems
SELECT * FROM AuthorizedReconItems 
*/

/*
SELECT vrix.VehicleReconItemID,
  viix.stocknumber, left(cast(vrix.description AS sql_char),100) AS Description,
  '' AS "Parts Required",
  '' AS Ordered,
  '' AS Received
-- SELECT COUNT (DISTINCT viix.stocknumber)
FROM VehicleReconItems vrix

INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.status <> 'AuthorizedReconItem_Complete'
LEFT JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID  
WHERE EXISTS (
  SELECT 1
  FROM ReconAuthorizations
  WHERE ReconAuthorizationID = arix.ReconAuthorizationID
  AND ThruTS IS NULL)
AND vrix.typ LIKE 'Me%' 
AND viix.stocknumber LIKE '1%'
ORDER BY viix.stocknumber  
*/

/*
BY design, this selects one VehicleReconItem per VehicleInventoryItem

hmmm IN the CURSOR, make sure the VehicleReconItemID IS still ON an OPEN AuthorizedReconItem
*/

/* 
-- UPDATE PartsRequired based ON vri.TotalPartsAmount OR vri.PartsQuantity being <> 0
--  AND VehicleReconItem IS still OPEN 
DECLARE @VehicleReconItemID string;
DECLARE @RequiredCur CURSOR AS
  SELECT vrix.VehicleReconItemID
  FROM VehicleReconItems vrix
  LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
  WHERE vrix.typ LIKE 'M%'
  AND EXISTS (
    SELECT 1
    FROM AuthorizedReconItems arix
    WHERE VehicleReconItemID = vrix.VehicleReconItemID 
    AND status <> 'AuthorizedReconItem_Complete'
    AND EXISTS (
      SELECT 1
  	  FROM ReconAuthorizations
  	  WHERE ReconAuthorizationID = arix.ReconAuthorizationID
  	  AND thruts IS NULL))
  AND coalesce(totalPartsAmount,0) + coalesce(PartsQuantity,0) <> 0;
OPEN @RequiredCur;
TRY
  WHILE FETCH @RequiredCur do
    @VehicleReconItemID = @RequiredCur.VehicleReconItemID;
	UPDATE VehicleReconItems
	SET PartsInStock = True
	WHERE VehicleReconItemID = @VehicleReconItemID;
  END WHILE;
FINALLY
  CLOSE @RequiredCur;
END TRY;
*/

DECLARE @VehicleReconItemID string;
DECLARE @Required Logical;
DECLARE @OrderedTS Timestamp;
DECLARE @ReceivedTS Timestamp;
DECLARE @UserID string;



DECLARE @Cur CURSOR AS
  SELECT *
  FROM (
    SELECT stocknumber, VehicleInventoryItemID,
      (SELECT top 1 VehicleReconItemID 
    	  FROM VehicleReconItems vrix
    	  WHERE vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID
        AND vrix.typ LIKE 'M%'
        AND EXISTS (
          SELECT 1
          FROM AuthorizedReconItems arix
          WHERE VehicleReconItemID = vrix.VehicleReconItemID 
          AND EXISTS (
            SELECT 1
        	  FROM ReconAuthorizations
        	  WHERE ReconAuthorizationID = arix.ReconAuthorizationID
        	  AND thruts IS NULL))) AS VehicleReconItemID 
    FROM VehicleInventoryItems viix
    WHERE stocknumber IN (
/*  
-- Reqd only  	
  '11040B','11887X','12186A','12218XXA','12298XXA','12312XX','12398A','12452XX',
  '12483A','12521C','12556A','12650A')) wtf WHERE VehicleReconItemID IS NOT NULL;
*/
/*  
-- ordered only
  '12274XA','13120AA','13292XX','13326XX','13520A','13327XX','13334A','12880A',
  '12584XX')) wtf WHERE VehicleReconItemID IS NOT NULL;
*/ 
-- received  
  '12376A','12261A','12881A','13366P','11860B','11962B','11987XXA','12062A','12223XXA',
  '12379A','12393B','12440XXA','12448XX','124634A','12586XXC','12612A','12810XXA',
  '12817AA','12838A','12838A','12913BA','12938A','13003A','13156B','')) wtf WHERE VehicleReconItemID IS NOT NULL;




@Required = True;
@OrderedTS = '03/03/2011 08:00:00';
@ReceivedTS = '03/03/2011 13:00:00';
@UserID = (SELECT partyid FROM users WHERE username = 'jon');

OPEN @Cur;
TRY
  BEGIN TRANSACTION;
    TRY
      WHILE FETCH @Cur DO
        @VehicleReconItemID = @Cur.VehicleReconItemID;
    
/* 3/4 6:19	  
    	    -- start with a clean slate: no parts reqd for anything;
    		-- run one time only;
    	    UPDATE VehicleReconItems 
    		SET PartsInStock = False
    		WHERE EXISTS(
    		  SELECT 1
    		  FROM VehicleInventoryItems
    		  WHERE VehicleInventoryItemID = VehicleReconItems.VehicleInventoryItemID
    		  AND ThruTS IS NULL)
    		AND typ LIKE 'M%';
*/
    	    -- Reqd 		
            UPDATE VehicleReconItems
            SET PartsInStock = @Required
            WHERE VehicleReconItemID = @VehicleReconItemID;
/*		
    		-- Ordered
    		INSERT INTO PartsOrders (VehicleReconItemID, OrderedTS, OrderedBy)
    		  Values(@VehicleReconItemID, @OrderedTS, @UserID);
*/	  
    		-- Received
    		TRY 
        	  INSERT INTO PartsOrders (VehicleReconItemID, OrderedTS, ReceivedTS, OrderedBy, ReceivedBy)
        	    Values(@VehicleReconItemID, @OrderedTS, @ReceivedTS, @UserID, @UserID);
            CATCH ALL
              Raise ReceivedExc(1004, 'VriID  : ' + @VehicleReconItemID + ' : ' + __errText);
            END; 		  
      END WHILE;
        COMMIT WORK;
      CATCH ALL
        ROLLBACK WORK;
        RAISE;  
    END ;  
FINALLY
  CLOSE @Cur;
END TRY;




