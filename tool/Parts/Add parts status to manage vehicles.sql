-- for Parts Status ON line item
DECLARE @VehicleInventoryItemID string;
@VehicleInventoryItemID = 'd1b814a3-f124-4a89-af29-157496f66987';

  SELECT vri.VehicleReconItemID, vri.Sequence, 
    case 
      when vri.Typ IN (SELECT Typ
                         FROM TypCategories 
                         WHERE Category = 'BodyReconItem') THEN 'Body'
      WHEN vri.Typ IN (SELECT Typ
                         FROM TypCategories 
                         WHERE Category = 'MechanicalReconItem') THEN 'Mechanical'
      WHEN vri.Typ IN (SELECT Typ
                         FROM TypCategories tc1 
                         WHERE tc1.Category = 'AppearanceReconItem'
                            OR tc1.Typ = 'PartyCollection_AppearanceReconItem') THEN 'Appearance'
    END AS Typ,
    CASE WHEN (select Category from TypCategories where typ = td.typ) in ('AppearanceReconItem', 'PartyCollection') THEN 
      vri.Description
      ELSE trim(td.description) collate ADS_DEFAULT_CI + ': ' + vri.Description 
    END AS Description,
    vri.TotalPartsAmount, vri.LaborAmount, 
    (Select top 1 sd.Description from StatusDescriptions sd WHERE sd.Status = ari.Status AND sd.ThruTS IS null) AS Status,
    ari.Status AS StatusStatus, 
	CASE  
	  WHEN vri.typ LIKE 'M%' THEN 
        CASE 
           WHEN vri.PartsInStock = false THEN 'No Parts Required'
      	   WHEN po.OrderedTS IS NULL THEN 'Not Ordered'
      	   WHEN po.ReceivedTS IS NULL THEN 'Ordered'
      	   ELSE 'Received'  
        END 
	  ELSE ''
	END AS PartsStatus
--    ari.ReconAuthorizationID

  FROM VehicleReconItems vri
  INNER JOIN AuthorizedReconItems ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
    AND ari.Status <> 'AuthorizedReconItem_Complete'
    AND ari.ReconAuthorizationID = (SELECT ra.ReconAuthorizationID 
	                                   FROM ReconAuthorizations ra 
									   WHERE ra.VehicleInventoryItemID = @VehicleInventoryItemID 
									     AND ra.ThruTS IS null)
	AND ari.CompleteTS IS null
  LEFT JOIN TypDescriptions td ON td.Typ = vri.typ
LEFT JOIN PartsOrders po ON vri.VehicleReconItemID = po.VehicleReconItemID 



