-- start with a clean slate
-- SET PartsInStock to false for ALL Mechanical VehicleReconItems 
-- history of PartsInStock IS irrelevant, has NOT been used for anything

UPDATE VehicleReconItems 
  SET PartsInStock = False
WHERE typ LIKE 'M%';  



-- UPDATE PartsRequired based ON vri.TotalPartsAmount OR vri.PartsQuantity being <> 0
--  AND VehicleReconItem IS still OPEN 
DECLARE @VehicleReconItemID string;
DECLARE @RequiredCur CURSOR AS
  SELECT vrix.VehicleReconItemID, partsinstock, totalpartsamount, partsquantity
  FROM ReconAuthorizations rax -- only OPEN ReconAuthorizations 
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
    AND arix.Status <> 'AuthorizedReconItem_Complete' 
	AND arix.status <> 'AuthorizedReconItem_InProcess'
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
    AND vrix.typ LIKE 'M%'
  WHERE rax.ThruTS IS NULL 
  AND coalesce(totalPartsAmount,0) + coalesce(PartsQuantity,0) <> 0;
OPEN @RequiredCur;
TRY
  WHILE FETCH @RequiredCur do
    @VehicleReconItemID = @RequiredCur.VehicleReconItemID;
	UPDATE VehicleReconItems
	SET PartsInStock = True
	WHERE VehicleReconItemID = @VehicleReconItemID;
  END WHILE;
FINALLY
  CLOSE @RequiredCur;
END TRY;


-- OPEN VehicleReconItems (not WIP) with non zero partsamount OR partsquantity 
-- use it to generate a report for updating 
SELECT viix.stocknumber, left(cast(vrix.description AS sql_char), 75), 
  vrix.totalpartsamount, vrix.partsquantity,
  '' AS "Parts not Req'd",
  '' AS Ordered, '' AS Received
  FROM ReconAuthorizations rax -- only OPEN ReconAuthorizations 
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
    AND arix.Status <> 'AuthorizedReconItem_Complete' 
	AND arix.status <> 'AuthorizedReconItem_InProcess'
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
    AND vrix.typ LIKE 'M%'
  LEFT JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID 
  WHERE rax.ThruTS IS NULL 
AND coalesce(totalPartsAmount,0) + coalesce(PartsQuantity,0) <> 0  
ORDER BY viix.stocknumber  
