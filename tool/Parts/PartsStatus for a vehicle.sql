  SELECT rax.VehicleInventoryItemID, 
    CASE
      WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = 0 THEN 'None Req''d'
      ELSE
        CASE 
          WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 'Kitted'
          ELSE
            CASE 
              WHEN SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ordered'
              ELSE 'Ordered'
            END 
     END 
  END AS PartsStatus
  FROM ReconAuthorizations rax -- only OPEN ReconAuthorizations 
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
    AND arix.Status <> 'AuthorizedReconItem_Complete' 
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
    AND vrix.typ LIKE 'M%'
  LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
  WHERE rax.ThruTS IS NULL 
  GROUP BY rax.VehicleInventoryItemID