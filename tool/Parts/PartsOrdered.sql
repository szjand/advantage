-- Parts Ordered
/*
SELECT typ, status 
FROM AuthorizedReconItems 
GROUP BY typ, status 
*/
DECLARE @MarketID string;
DECLARE @LocationID string;

@LocationID = (SELECT partyid FROM organizations WHERE name = 'rydells');

SELECT viix.stocknumber, 
  ps.PartsStatus, 
  'Receve',
  'Cancel',
  utilities.CurrentViiSalesStatus(viix.VehicleInventoryItemID)
FROM VehicleInventoryItems viix
INNER JOIN (
  SELECT vrix.VehicleInventoryItemID, 
    CASE
      WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = 0 THEN 'None Req''d'
      ELSE
        CASE 
    	  WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 'Kitted'
    	  ELSE
    	    CASE 
    		  WHEN SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ordered'
    		  ELSE 'Ordered'
            END 
        END 
    END AS PartsStatus
  FROM VehicleReconItems vrix
  LEFT JOIN (
    SELECT status, VehicleReconItemID
	FROM AuthorizedReconItems arix 
	WHERE EXISTS ( -- only the current authorization
	  SELECT 1
	  FROM ReconAuthorizations
	  WHERE ReconAuthorizationID = arix.ReconAuthorizationID
	  AND thruts IS NULL)) ar ON vrix.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN PartsOrders po ON po.VehicleReconItemID = vrix.VehicleReconItemID
    AND CancelledTS IS NULL 
	AND ReturnedTS IS NULL  

    AND vrix.typ LIKE 'Me%' 
--    AND vrix.typ <> 'MechanicalReconItem_Inspection'
  GROUP BY vrix.VehicleInventoryItemID) ps ON viix.VehicleInventoryItemID = ps.VehicleInventoryItemID   
WHERE OwningLocationID = @LocationID
