SELECT *
FROM (
  SELECT VehicleInventoryItemID, status
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPulled'
  AND ThruTS IS NULL) AS Pulled,
  (SELECT VehicleInventoryItemID, status
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagAV'
    AND ThruTS IS NULL) AS Avail
    
SELECT status, COUNT(*)
-- the assumption IS that these are ALL current statuses
FROM ( -- pulled 
  SELECT VehicleInventoryItemID, status, fromts
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPulled'
  AND ThruTS IS NULL
  UNION -- available
  SELECT VehicleInventoryItemID, status, fromts
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagAV'
  AND ThruTS IS NULL
  UNION -- wsb
  SELECT VehicleInventoryItemID, status fromts
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagWSB'
  AND ThruTS IS NULL
  UNION -- sold?
  SELECT VehicleInventoryItemID, status, fromts
  FROM VehicleSales 
  WHERE status = 'VehicleSale_Sold') wtf
GROUP BY status

SELECT VehicleInventoryItemID, MIN(fromTS) -- shit, slow already, 469ms
-- shit, the fucking UNION alone IS 300ms
-- the assumption IS that these are ALL current statuses
FROM ( -- pulled 
  SELECT VehicleInventoryItemID, status, fromts
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPulled'
  AND ThruTS IS NULL
  UNION -- available
  SELECT VehicleInventoryItemID, status, fromts
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagAV'
  AND ThruTS IS NULL
  UNION -- wsb
  SELECT VehicleInventoryItemID, status, fromts
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagWSB'
  AND ThruTS IS NULL
  UNION -- sold?
  SELECT VehicleInventoryItemID, status, SoldTS
  FROM VehicleSales 
  WHERE status = 'VehicleSale_Sold') wtf
GROUP BY VehicleInventoryItemID  

-- ok This works for a logical PullSignal: T/F
SELECT viix.VehicleInventoryItemID 
-- SELECT COUNT(*) 
FROM VehicleInventoryItems viix
WHERE ThruTS IS NULL  
AND (
  EXISTS ( 
    SELECT 1 -- pulled
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    AND ThruTS IS NULL)
  OR EXISTS (
    SELECT 1 -- available 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagAV'
    AND ThruTS IS NULL)
  OR EXISTS (  
    SELECT 1 -- wsb 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagWSB'
    AND ThruTS IS NULL)
  OR EXISTS (
    SELECT 1 -- sold/dcu
    FROM VehicleSales 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND status = 'VehicleSale_Sold')
  OR EXISTS (
    SELECT 1 -- sales buffer
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFLagSB'
    AND ThruTS IS NULL)) 

--but what IF i want more info, specifically, the date
-- this IS about FromTS only, does NOT give the status
-- Pull Signal: T/F, if true; since FromTS

-- damn, this still 200ms but it works
SELECT VehicleInventoryItemID, MIN(fromTS)
-- SELECT * 
FROM VehicleInventoryItemStatuses viix
WHERE ThruTS IS NULL  
--AND category NOT LIKE '%ReconProcess' -- ooh cuts it down to 100ms
--AND category <> 'RawMaterials' -- AND down to 80ms GOOD ENUF
AND category LIKE 'RMFlag%' -- ooh, even better, 60ms
AND (
  EXISTS ( 
    SELECT 1 -- pulled
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    AND ThruTS IS NULL)
  OR EXISTS (
    SELECT 1 -- available 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagAV'
    AND ThruTS IS NULL)
  OR EXISTS (  
    SELECT 1 -- wsb 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagWSB'
    AND ThruTS IS NULL)
  OR EXISTS (
    SELECT 1 -- sold/dcu
    FROM VehicleSales 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND status = 'VehicleSale_Sold')
  OR EXISTS (
    SELECT 1 -- sales buffer
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFLagSB'
    AND ThruTS IS NULL)) 
GROUP BY viix.VehicleInventoryItemID     


SELECT category, COUNT(*) 
FROM VehicleInventoryItemStatuses viix
WHERE ThruTS IS NULL  
-- AND category NOT LIKE '%ReconProcess' -- ooh cuts it down to 100ms
AND (
  EXISTS ( 
    SELECT 1 -- pulled
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    AND ThruTS IS NULL)
  OR EXISTS (
    SELECT 1 -- available 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagAV'
    AND ThruTS IS NULL)
  OR EXISTS (  
    SELECT 1 -- wsb 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagWSB'
    AND ThruTS IS NULL)
  OR EXISTS (
    SELECT 1 -- sold/dcu
    FROM VehicleSales 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND status = 'VehicleSale_Sold')
  OR EXISTS (
    SELECT 1 -- sales buffer
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFLagSB'
    AND ThruTS IS NULL)) 
GROUP BY category    
