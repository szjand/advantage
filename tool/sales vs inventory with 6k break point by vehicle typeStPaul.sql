

SELECT x.VehicleType, x.PriceBand, y.sales, x.Inventory
FROM (

SELECT VehicleType, PriceBand, COUNT(*) AS Inventory
FROM (  
  SELECT VehicleType, 
    CASE
      WHEN price < 6000 THEN 'Under 6K'
      ELSE 'Over 6K'
    END AS PriceBand
  FROM usedcars
  WHERE market = 'SP-St. Paul'
  AND salesstatus NOT IN ('Sold','Wholesaled')) i
GROUP BY VehicleType, PriceBand) x

LEFT JOIN (
SELECT VehicleType, PriceBand, COUNT(*) AS Sales
FROM (  
  SELECT VehicleType, 
    CASE
      WHEN price < 6000 THEN 'Under 6K'
      ELSE 'Over 6K'
    END AS PriceBand
  FROM usedcars
  WHERE market = 'SP-St. Paul'
  AND salesstatus = 'Sold'
  AND datesold BETWEEN curdate() - 30 AND curdate()) s 
GROUP BY VehicleType, PriceBand) y
ON x.VehicleType = y.VehicleType
  AND x.PriceBand = y.PriceBand