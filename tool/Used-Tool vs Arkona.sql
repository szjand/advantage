-- legacy.inpmast popluated with current uc inventory (inpmast)
SELECT vii.stocknumber, vi.vin, vi.yearmodel, vi.make, vi.model, cast(vii.FromTS AS sql_date)
FROM VehicleInventoryItems vii
LEFT JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
WHERE vii.ThruTS IS NULL

SELECT *
FROM legacy.inpmast

-- IN tool NOT IN arkona
SELECT *
FROM (
  SELECT vii.stocknumber, vi.vin, vi.yearmodel, vi.make, vi.model, cast(vii.FromTS AS sql_date)
  FROM VehicleInventoryItems vii
  LEFT JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
  WHERE vii.ThruTS IS NULL) t
LEFT JOIN (  
  SELECT *
  FROM legacy.inpmast) i ON i.vin = t.vin
WHERE i.vin IS null  

-- IN arkona NOT IN tool
SELECT *
FROM (
  SELECT *
  FROM legacy.inpmast) i 
LEFT JOIN (  
  SELECT vii.stocknumber, vi.vin, vi.yearmodel, vi.make, vi.model, cast(vii.FromTS AS sql_date)
  FROM VehicleInventoryItems vii
  LEFT JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
  WHERE vii.ThruTS IS NULL) t ON i.vin = t.vin
WHERE t.vin IS null  
ORDER BY t.stocknumber