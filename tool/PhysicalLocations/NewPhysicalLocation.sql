--< 07/09/21 created a location called CTP for GM & Honda

DECLARE @LocationID string;
@LocationID = (
  SELECT partyid
  FROM organizations
  WHERE name = 'honda cartiva');
--	WHERE name = 'Rydells');

INSERT INTO LocationPhysicalVehicleLocations values(
@LocationID ,
(SELECT newidstring(d) FROM system.iota),
false,
true,
'CTP',
'CTP');

--/> 07/09/21 created a location called CTP for GM & Honda


SELECT *
FROM LocationPhysicalVehicleLocations 
where active = true 
ORDER BY locationshortname

-- SELECT * FROM organizations
-- rydells: 'B6183892-C79D-4489-A58C-B526DF948B06'
-- honda cartiva: '4CD8E72C-DC39-4544-B303-954C99176671'
-- 

UPDATE LocationPhysicalVehicleLocations
SET active = false
-- SELECT * FROM LocationPhysicalVehicleLocations
WHERE locationpartyid = 'B6183892-C79D-4489-A58C-B526DF948B06'
AND physicalvehiclelocationid = 'fcaa1531-08b4-e14f-90d3-f081cb81b2d2'
AND locationshortname = 'Simonsons';

SELECT *
FROM LocationPhysicalVehicleLocations
WHERE locationpartyid = 'B6183892-C79D-4489-A58C-B526DF948B06'
AND active = true
ORDER BY locationshortname

SELECT *
FROM organizations
WHERE partyid = '1B6768C6-03B0-4195-BA3B-767C0CAC40A6'

SELECT *
FROM organizations
WHERE partyid = 'B6183892-C79D-4489-A58C-B526DF948B06'

DECLARE @LocationID string;
@LocationID = (
  SELECT * partyid
  FROM organizations
  WHERE name = 'honda cartiva');

INSERT INTO LocationPhysicalVehicleLocations values(
@LocationID ,
(SELECT newidstring(d) FROM system.iota),
false,
true,
'Auto Outlet',
'');


SELECT *
FROM LocationPhysicalVehicleLocations
ORDER BY locationshortname

SELECT *
FROM LocationPhysicalVehicleLocations
WHERE locationshortname LIKE 'Honda F%'


UPDATE LocationPhysicalVehicleLocations
SET Active = false
WHERE locationshortname LIKE 'Honda F%'

select * from LocationPhysicalVehicleLocations WHERE active

UPDATE LocationPhysicalVehicleLocations
SET locationshortname = 'Pricing - East of new car'
-- SELECT * FROM LocationPhysicalVehicleLocations
WHERE locationshortname = 'Pricing - South of used cars'

UPDATE LocationPhysicalVehicleLocations
SET LocationDescription = 'in the Shop'
-- SELECT * FROM LocationPhysicalVehicleLocations
WHERE locationshortname = 'Rydell Detail'



-- SELECT * FROM organizations
-- rydells: 'B6183892-C79D-4489-A58C-B526DF948B06'
-- honda cartiva: '4CD8E72C-DC39-4544-B303-954C99176671'
-- 
-- 4/19/18 new location for both stores: Hugos
DECLARE @LocationID string;
@LocationID = (
  SELECT partyid
  FROM organizations
  WHERE name = 'rydells');

INSERT INTO LocationPhysicalVehicleLocations values(
@LocationID ,
(SELECT newidstring(d) FROM system.iota),
false,
true,
'Hugos',
'');

select * from LocationPhysicalVehicleLocations WHERE active ORDER BY locationshortname

-- 11/16/18  Honda South
UPDATE LocationPhysicalVehicleLocations
SET active = false
WHERE locationshortname IN ('The House','South Farm');

DECLARE @LocationID string;
@LocationID = (
  SELECT partyid
  FROM organizations
  WHERE name = 'rydells');

INSERT INTO LocationPhysicalVehicleLocations values(
@LocationID ,
(SELECT newidstring(d) FROM system.iota),
false,
true,
'Honda South',
'');

DECLARE @LocationID string;
@LocationID = (
  SELECT partyid
  FROM organizations
  WHERE name = 'honda cartiva');

INSERT INTO LocationPhysicalVehicleLocations values(
@LocationID ,
(SELECT newidstring(d) FROM system.iota),
false,
true,
'Honda South',
'');