SELECT *
FROM LocationPhysicalVehicleLocations

SELECT PhysicalVehicleLocationID, COUNT(*) AS howmany
FROM vehicleitemPhysicallocations
WHERE ThruTS IS NULL
GROUP BY PhysicalVehicleLocationID


SELECT PhysicalVehicleLocationID, LocationShortName
FROM LocationPhysicalVehicleLocations
WHERE Active = True


SELECT left(trim(l.location) + ':' + trim(l.LocationShortname), 35), coalesce(v.howmany, 0)
FROM (
  SELECT PhysicalVehicleLocationID, 
  (select name from organizations where partyid = locationpartyid) as Location,
  LocationShortName
  FROM LocationPhysicalVehicleLocations
  WHERE Active = True) l
LEFT JOIN (  
  SELECT PhysicalVehicleLocationID, COUNT(*) AS howmany
  FROM vehicleitemPhysicallocations
  WHERE ThruTS IS NULL
  GROUP BY PhysicalVehicleLocationID) v ON v.PhysicalVehicleLocationID = l.PhysicalVehicleLocationID
ORDER BY v.howmany  


-- revised to include only OPEN VehicleInventoryItems 
SELECT left(trim(l.location) + ':' + trim(l.LocationShortname), 35), coalesce(v.howmany, 0),
  l.*
FROM (
  SELECT PhysicalVehicleLocationID, 
  (select name from organizations where partyid = locationpartyid) as Location,
  LocationShortName
  FROM LocationPhysicalVehicleLocations
  WHERE Active = True) l
LEFT JOIN (  
  SELECT PhysicalVehicleLocationID, COUNT(*) AS howmany
  FROM vehicleitemPhysicallocations a
  INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.ThruTS IS NULL 
  WHERE a.ThruTS IS NULL
  GROUP BY PhysicalVehicleLocationID) v ON v.PhysicalVehicleLocationID = l.PhysicalVehicleLocationID
ORDER BY v.howmany  


SELECT b.stocknumber, a.* 
FROM VehicleItemPhysicalLocations a
LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID   = b.VehicleInventoryItemID 
WHERE physicalvehiclelocationid = '3a6a3177-3d66-d742-9c92-207228d5cc81' 
  AND a.thruts IS null
  