-- 4/7 
-- 12045A, (12579XX) - 4/6:$24300, 4/5:24300, 4/4:23999, was showing no previous price
-- what we are really looking for IS a vehicle WHERE the price has changed IN the last x days
  DECLARE @MarketID string;
  DECLARE @LocationID string;
  DECLARE @DaysBack integer;
  DECLARE @AllRecords logical;
  @MarketID = 'A4847DFC-E00E-42E7-89EE-CD4368445A82';
  @LocationID = 'B6183892-C79D-4489-A58C-B526DF948B06';
  @DaysBack = 28;  
  @AllRecords = false;
  DROP TABLE #vptt;
  DROP TABLE #vptt1;
  SELECT DISTINCT viix.stocknumber, viix.VehicleInventoryItemID, vpx.vehiclepricingts, vpdx.amount
  INTO #vptt
  FROM VehiclePricings vpx -- pricings ON current inventory
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND viix.ThruTS IS NULL 
	AND -- selected market/Location
      CASE 
        WHEN @LocationID <> '' THEN viix.LocationID = @LocationID
        ELSE viix.LocationID IN (
          SELECT PartyID2
          FROM PartyRelationships
          WHERE PartyID1 = @MarketID
          AND typ = 'PartyRelationship_MarketLocations'
		  AND ThruTS IS NULL)
      END	    
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  INNER JOIN ( -- vehicle was priced IN last x days
    SELECT viix.VehicleInventoryItemID, vpx.VehicleInventoryItemID, vpx.VehiclePricingTS, vpdx.amount
    FROM VehiclePricings vpx
    INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
      AND viix.ThruTS IS NULL  
    INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
      AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
    WHERE CAST(vpx.VehiclePricingTS AS sql_date) >= curdate() - @DaysBack) dp ON vpx.VehicleInventoryItemID = dp.VehicleInventoryItemID 
  INNER JOIN ( -- more than one price per VehicleInventoryItem
    SELECT VehicleInventoryItemID 
    FROM ( -- unique viiid/price
      SELECT vpx.VehicleInventoryItemID
      FROM VehiclePricings vpx 
      INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
        AND viix.ThruTS IS NULL -- current inventory only 
      INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
        AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
      GROUP BY vpx.VehicleInventoryItemID, vpdx.amount) wtf
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)  mp ON dp.VehicleInventoryItemID = mp.VehicleInventoryItemID;

    
SELECT *
INTO #vptt1
FROM #vptt j
WHERE -- IF the current price IS different than the previous price, we want it
    (SELECT top 1 amount FROM #vptt WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC) -- current price
      <>
    (SELECT top 1 start at 2 amount FROM #vptt WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC) -- next previous price
UNION
SELECT *
FROM #vptt j
WHERE -- this IS the rest, current price = previous price
    (SELECT top 1 amount FROM #vptt WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC)
      =
    (SELECT top 1 start at 2 amount FROM #vptt WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC)
AND EXISTS ( -- but IF there IS a price that IS different than the current price AND was generated within the last x days, we want it
  SELECT 1
  FROM #vptt
  WHERE VehicleInventoryItemID = j.VehicleInventoryItemID
  AND CAST(VehiclePricingTS AS sql_date) >= curdate() - @DaysBack
  AND amount <> (SELECT top 1 amount FROM #vptt WHERE VehicleInventoryItemID  = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC));
      
/*
SELECT * 
FROM #vptt
ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC 

WHERE VehicleInventoryItemID = '3437fc16-001c-47c9-9c2c-b50732ff189c'
        
SELECT * 
FROM #vptt1
ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC 
WHERE VehicleInventoryItemID = '3437fc16-001c-47c9-9c2c-b50732ff189c'
ORDER BY VehiclePricingTS DESC 
*/        
  SELECT viix.stocknumber,  wtf.CurrentPrice, wtf.PreviousPrice, wtf.PriceDate,
    wtf.CurrentPrice - wtf.PreviousPrice AS Difference
  FROM (
    SELECT distinct VehicleInventoryItemID, 
      (SELECT top 1 amount 
        FROM #vptt1 
        WHERE VehicleInventoryItemID  = j1.VehicleInventoryItemID 
        ORDER BY VehiclePricingTS DESC) AS CurrentPrice,
      CASE
        WHEN ( -- 2 price records
          SELECT COUNT(*) 
          FROM #vptt1 
          WHERE VehicleInventoryItemID = j1.VehicleInventoryItemID 
          GROUP BY VehicleInventoryItemID) = 2 THEN 
            (SELECT top 1 start at 2 amount 
              FROM #vptt1 
              WHERE VehicleInventoryItemID = j1.VehicleInventoryItemID 
              ORDER BY VehiclePricingTS DESC) 
        ELSE ( -- more than 2 price records
          SELECT top 1 amount 
          FROM #vptt1 
          WHERE VehicleInventoryItemID = j1.VehicleInventoryItemID 
          AND amount <> ( -- current price
            SELECT top 1 amount 
            FROM #vptt1 
            WHERE VehicleInventoryItemID = j1.VehicleInventoryItemID 
            ORDER BY VehiclePricingTS DESC)
          ORDER BY VehiclePricingTS DESC)            
      END AS PreviousPrice,
      (SELECT top 1 VehiclePricingTS 
        FROM #vptt1 
        WHERE VehicleInventoryItemID = j1.VehicleInventoryItemID 
        ORDER BY VehiclePricingTS DESC) AS PriceDate
    FROM #vptt1 j1) wtf
  INNER JOIN VehicleInventoryItems viix ON wtf.VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND viix.ThruTS IS NULL 
  INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
  INNER JOIN SelectedReconPackages srp ON viix.VehicleInventoryItemID = srp.VehicleInventoryItemID  
    AND srp.SelectedReconPackageTS = (
      SELECT MAX(SelectedReconPackageTS)
      FROM SelectedReconPackages
  	  WHERE VehicleInventoryItemID = srp.VehicleInventoryItemID 
  	  GROUP BY VehicleInventoryItemID)
  WHERE  
    CASE  
      WHEN  @AllRecords = true then 1=1
      ELSE wtf.CurrentPrice - wtf.PreviousPrice < 0
    END; 

