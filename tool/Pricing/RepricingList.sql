SELECT viix.stocknumber AS "Stock #", vix.yearmodel AS Year, vix.make AS Make, vix.model AS Model,
  vix.ExteriorColor AS Color, 
  (SELECT top 1 value
    FROM VehicleitemMileages
    WHERE VehicleItemID = vix.VehicleItemID
    ORDER BY vehicleItemMileageTS DESC) AS Miles,
  utilities.CurrentViiSalesStatus(viix.VehicleInventoryItemID) AS SalesStatus,
  utilities.CurrentViiPricingDetail(viix.VehicleInventoryItemID, 'VehiclePricingDetail_Invoice') AS "Current Invoice",
  utilities.CurrentViiPricingDetail(viix.VehicleInventoryItemID, 'VehiclePricingDetail_BestPrice') AS "Current Price",
  '' AS "New Invoice", '' AS "New Price"
FROM VehicleInventoryItems viix
INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
WHERE viix.ThruTS IS NULL 
  AND viix.OwningLocationID = (
    SELECT partyid
    FROM organizations
    WHERE name = 'Rydells')


