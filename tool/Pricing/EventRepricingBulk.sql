
DECLARE @VehiclePricingID string;
DECLARE @VehicleInventoryItemID string;
DECLARE @BasisTable string;
DECLARE @TableKey string;
DECLARE @PricedBy string;
DECLARE @Price Integer;
DECLARE @Invoice Integer;
DECLARE @PricingStrategy string;
DECLARE @Notes string;
DECLARE @NowTS TIMESTAMP;
DECLARE @VehicleItemID string;
DECLARE @Cur CURSOR AS 
  SELECT rl.newprice,
    viix.VehicleInventoryItemID, 
    viix.VehicleItemID
  FROM repricinglist rl
  LEFT JOIN VehicleInventoryItems viix ON rl.stocknumber = viix.stocknumber
  WHERE rl.newprice > 1;

@BasisTable = 'VehiclePricings'; 
@PricedBy = (SELECT partyid FROM users WHERE username = 'dwilkie'); 
@Notes = 'Big event pricing';
 

BEGIN TRANSACTION;
TRY 
  OPEN @Cur;
  TRY
    WHILE FETCH @Cur DO
      @VehicleInventoryItemID = @Cur.VehicleInventoryItemID;
      @Price = @Cur.newprice;
      @VehiclePricingID = (SELECT newidstring(d) FROM system.iota);
      @TableKey = @VehiclePricingID; 
      @NowTS = now();
      @VehicleItemID = @Cur.VehicleItemID; 
      @Invoice = (
        SELECT Amount 
        FROM VehiclePricingDetails
        WHERE VehiclePricingID = (
          SELECT top 1 VehiclePricingID
          FROM VehiclePricings
          WHERE VehicleInventoryItemID = @VehicleInventoryItemID
          ORDER BY VehiclePricingTS DESC)
        AND typ = 'VehiclePricingDetail_Invoice');          
      @PricingStrategy = (  
        SELECT top 1 VehiclePricingStrategy 
        FROM vehiclepricings
        WHERE VehicleInventoryItemID = @VehicleInventoryItemID
        ORDER BY vehiclepricingts DESC); 
      
      TRY      
        INSERT INTO VehiclePricings (VehiclePricingID, VehicleInventoryItemID, PricedBy,
          VehiclePricingTS, BasisTable, TableKey, VehicleItemID, VehiclePricingStrategy)
        VALUES (@VehiclePricingID, @VehicleInventoryItemID, @PricedBy,  @NowTS,
          @BasisTable, @TableKey, @VehicleItemID, @PricingStrategy);
      CATCH ALL
        RAISE PricingException(1001, __errText);
      END;
      TRY 
        INSERT INTO VehiclePricingDetails
        VALUES(@VehiclePricingID, 'VehiclePricingDetail_BestPrice', @Price);
      CATCH ALL
        RAISE PricingException(1002, __errText);
      END;        
      TRY 
        INSERT INTO VehiclePricingDetails
        VALUES(@VehiclePricingID, 'VehiclePricingDetail_Invoice', @Invoice);
      CATCH ALL
        RAISE PricingException(1003, __errText);
      END;        
      TRY 
        EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubCategory(
          @VehicleInventoryItemID, @PricedBy,
          @BasisTable, @TableKey, 'VehiclePricing', 'VehiclePricing_Pricing',
          @NowTS, @Notes);         
      CATCH ALL
        RAISE PricingException(1004, __errText);
      END;    
    END WHILE;
  FINALLY
    CLOSE @Cur;
  END;
	
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  