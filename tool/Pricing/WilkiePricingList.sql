   SELECT 
   
 /*
 DROP view vUsedCarsInventory
 SELECT Count(*) FROM vUsedCarsInventory WHERE  sold IS NOT null
 */
  CASE
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagSB_SalesBuffer'
      AND ThruTS IS NULL) = 1 THEN  'Sales Buffer'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagWSB_WholesaleBuffer'
      AND ThruTS IS NULL) = 1 THEN 'Wholesale Buffer' 
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagPIT_PurchaseInTransit'
      AND ThruTS IS NULL) = 1 THEN 'In Transit'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagTNA_TradeNotAvailable'
      AND ThruTS IS NULL) = 1 THEN 'Trade Buffer'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagIP_InspectionPending'
      AND ThruTS IS NULL) = 1 THEN 'Inspection Pending'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagWP_WalkPending'
      AND ThruTS IS NULL) = 1 THEN 'Walk Pending'  
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagRMP_RawMaterialsPool'
      AND ThruTS IS NULL) = 1 THEN 'Raw Materials'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagAV_Available'
      AND ThruTS IS NULL) = 1 THEN 'Available'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagPB_PricingBuffer'
      AND ThruTS IS NULL) = 1 THEN 'Pricing Buffer'
    WHEN (    
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagPulled_Pulled'
      AND ThruTS IS NULL) = 1 THEN 'Pulled'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RawMaterials_BookingPending'
      AND ThruTS IS NULL) = 1 THEN 'Booking Pending'
                
  END AS Status,
  CASE
    WHEN (
      SELECT COUNT(*)
      FROM ReconAuthorizations
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND ThruTS IS NULL) = 0 THEN 'No Open Recon'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'MechanicalReconDispatched_Dispatched'
      AND ThruTS IS NULL) = 1 THEN 'Mech Dispatch'
    WHEN (  
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'BodyReconDispatched_Dispatched'
      AND ThruTS IS NULL
      AND EXISTS (
        SELECT 1
        FROM VehicleInventoryItemStatuses viis
        WHERE viis.status = 'RMFlagRB_ReconBuffer'
        AND VehicleInventoryItemID = vii.VehicleInventoryItemID 
        AND viis.ThruTS IS NULL)) = 1 THEN 'Body Dispatch'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'AppearanceReconDispatched_Dispatched'
      AND ThruTS IS NULL) = 1 THEN 'App Dispatch'       
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'MechanicalReconProcess_InProcess'
      AND ThruTS IS NULL)  = 1 THEN 'Mech WIP' 
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'BodyReconProcess_InProcess'
      AND ThruTS IS NULL) = 1 THEN 'Body WIP' 
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'AppearanceReconProcess_InProcess'
      AND ThruTS IS NULL) = 1 THEN 'App WIP' 
    WHEN (
      SELECT COUNT(*)
      FROM ReconAuthorizations
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND ThruTS IS NULL) <> 0 THEN 'Unfinished'         
  END AS ReconStatus,

       vii.StockNumber,
       vi.VIN,
       Coalesce(Current_Date() - Convert(vii.FromTS, SQL_DATE), 0) AS DaysOwned,
       Coalesce(Trim(vi.Trim),'') + ' '  + 
         Coalesce(Trim(vi.Engine), '') AS VehicleDescription,
       vi.YearModel,
       vi.Make, 
       vi.Model,
       vi.BodyStyle,
      (SELECT TOP 1 vpd.Amount
          FROM VehiclePricings vp
          inner join VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
          WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
          ORDER BY VehiclePricingTS DESC) as Price
FROM VehicleInventoryItems vii
INNER join VehicleItems vi on vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc
          on mmc.Make = vi.Make
          AND mmc.Model = vi.Model
          AND mmc.ThruTS IS NULL
WHERE vii.ThruTS IS NULL
and vii.locationid = (SELECT partyid FROM organizations WHERE name = 'Rydells')