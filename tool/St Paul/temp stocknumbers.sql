--6/16
-- fucking had to TRIM Market to make this work
SELECT t.stocknumber, t.vin, t.year, t.make, t.dateacquired, t.salesstatus, u.stocknumber, u.dateacquired, u.salesstatus
FROM usedcars t
LEFT JOIN (
  SELECT stocknumber, vin, salesstatus, dateacquired
  FROM usedcars
  WHERE trim(market) = 'SP-St. Paul'
  AND LEFT(TRIM(stocknumber),1)IN ('0','1','2','3','4','5','6','7','8','9')) u ON t.vin = u.vin
    AND t.dateacquired = u.dateacquired 
    AND t.salesstatus <> u.salesstatus
WHERE trim(market) = 'SP-St. Paul'
AND LEFT(TRIM(t.stocknumber),1) NOT IN ('0','1','2','3','4','5','6','7','8','9')
AND t.salesstatus NOT IN ('Sold','Wholesaled')
ORDER BY t.dateacquired DESC 


SELECT * FROM usedcars WHERE vin = 'WVWAK73C48P116706'


SELECT min(dateacquired), max(dateacquired), COUNT(*) -- 617
FROM usedcars
WHERE market = 'SP-St. Paul'

SELECT min(dateacquired), max(dateacquired), COUNT(*) -- 27331
FROM usedcars
WHERE trim(market) = 'SP-St. Paul'

SELECT DISTINCT market FROM usedcars WHERE market LIKE '%Pa%' -- only 1

SELECT DISTINCT market, location FROM usedcars WHERE market LIKE '%Pa%' -- only 1

SELECT market, location,min(dateacquired), max(dateacquired), COUNT(*)
FROM usedcars
WHERE market LIKE '%Pa%'
GROUP BY market, location


SELECT market, COUNT(*) -- st paul only once
FROM usedcars
GROUP BY market

SELECT COUNT(*) -- 609
FROM (
  SELECT vin, stocknumber, COUNT(*)
  FROM usedcars
  WHERE trim(market) = 'SP-St. Paul'
  GROUP BY vin, stocknumber
  HAVING COUNT(*) > 1) wtf
  
SELECT COUNT(*) -- ZERO
FROM (
  SELECT vin, stocknumber, COUNT(*)
  FROM usedcars
  WHERE market = 'SP-St. Paul'
  GROUP BY vin, stocknumber
  HAVING COUNT(*) > 1) wtf  
  
-- here are the dups
-- except some are NOT exact dups: different salesstatus  
SELECT vehicleid, stocknumber, vin, dateacquired, salesstatus
FROM usedcars
WHERE vin IN (
  SELECT vin
  FROM usedcars
  WHERE trim(market) = 'SP-St. Paul'
  GROUP BY vin, stocknumber
  HAVING COUNT(*) > 1)
AND stocknumber IN (
  SELECT stocknumber
  FROM usedcars
  WHERE trim(market) = 'SP-St. Paul'
  GROUP BY vin, stocknumber
  HAVING COUNT(*) > 1)
ORDER BY stocknumber

SELECT stocknumber, vin, salesstatus, COUNT(*)
FROM usedcars
WHERE TRIM(market) = 'SP-St. Paul'
GROUP BY stocknumber, vin, salesstatus
HAVING COUNT(*) > 1

-- ok this limits it to exact duplicates, including salesstatus, only
--SELECT COUNT(*) FROM (
SELECT t.vehicleid, t.stocknumber, t.vin, t.salesstatus
FROM usedcars t
INNER JOIN (
  SELECT stocknumber, vin, salesstatus
  FROM usedcars
  WHERE TRIM(market) = 'SP-St. Paul'
  GROUP BY stocknumber, vin, salesstatus
  HAVING COUNT(*) > 1) d ON t.stocknumber = d.stocknumber
    AND t.vin = d.vin
    AND t.salesstatus = d.salesstatus
WHERE TRIM(market) = 'SP-St. Paul'--) wtf  
ORDER BY t.vin    

-- pick one of the duplicates arbitrarily (MIN(vehicleID) AND DELETE it
-- fuck they ALL came back, use the other vehicleid
/*6-16 */
/*6-17 greg figured it out - reindexed usedcars AND dup generating problem seems to be fixed*/
-- AND after that, seems LIKE they keep coming back
DELETE 
-- SELECT COUNT(*) -- 575
FROM usedcars 
WHERE vehicleid in (
  SELECT max(t.vehicleid)
  FROM usedcars t
  INNER JOIN (
    SELECT stocknumber, vin, salesstatus
    FROM usedcars
    WHERE TRIM(market) = 'SP-St. Paul'
    GROUP BY stocknumber, vin, salesstatus
    HAVING COUNT(*) > 1) d ON t.stocknumber = d.stocknumber
      AND t.vin = d.vin
      AND t.salesstatus = d.salesstatus
  WHERE TRIM(market) = 'SP-St. Paul'  
  GROUP BY t.stocknumber, t.vin, t.salesstatus)
  
-- now LEFT with these dups that have different salesstatuses
63181

-- guess just manually verify status IN notes AND DELETE the one that doesn't match
DELETE FROM usedcars WHERE vehicleid IN (
'{16BC39AF-0BFB-46AE-A477-B6F8EC93F12A}',
'{484A618E-5378-4758-ACD9-8E2992C57E6D}',
'{78562C86-E743-4C16-BBEC-E46FC8F3E9D0}',
'{6216B39F-09BF-4033-8276-CD28E8B2750E}')



-- ok, now ALL the fucking superfluous temp records

SELECT t.stocknumber, t.vin, t.year, t.make, t.dateacquired, t.salesstatus, u.stocknumber, u.dateacquired, u.salesstatus
FROM usedcars t
LEFT JOIN (
  SELECT stocknumber, vin, salesstatus, dateacquired
  FROM usedcars
  WHERE trim(market) = 'SP-St. Paul'
  AND LEFT(TRIM(stocknumber),1)IN ('0','1','2','3','4','5','6','7','8','9')) u ON t.vin = u.vin
    AND t.dateacquired = u.dateacquired 
    AND t.salesstatus <> u.salesstatus
WHERE trim(market) = 'SP-St. Paul'
AND LEFT(TRIM(t.stocknumber),1) NOT IN ('0','1','2','3','4','5','6','7','8','9')
  AND u.stocknumber IS NOT NULL
  AND u.salesstatus NOT IN ('Sold','Wholesaled')
--AND t.salesstatus NOT IN ('Sold','Wholesaled')
ORDER BY t.dateacquired DESC 

-- take an axe to it
DELETE
--SELECT stocknumber, vin, dateacquired, salesstatus
FROM usedcars
WHERE trim(market) = 'SP-St. Paul'
AND LEFT(TRIM(stocknumber),1) NOT IN ('0','1','2','3','4','5','6','7','8','9')
AND salesstatus = 'In Transit'
AND dateacquired < '05/01/2011'


DELETE
--SELECT stocknumber, vin, dateacquired, salesstatus
FROM usedcars
WHERE trim(market) = 'SP-St. Paul'
AND LEFT(TRIM(stocknumber),1) NOT IN ('0','1','2','3','4','5','6','7','8','9')
AND salesstatus <> 'In Transit'

DELETE
--SELECT stocknumber, vin, dateacquired, salesstatus
FROM usedcars
WHERE trim(market) = 'SP-St. Paul'
AND stocknumber IN ('VWD992','VWD522','VWD521')


--63921X
SELECT u1.vehicleid, u2.vehicleid, u1.market, u2.market

FROM usedcars u1

INNER JOIN (
  select vehicleid, market
  from usedcars
  WHERE vehicleid = '{987B49B9-4A59-4B96-B8C9-369B0B4AF369}') u2 ON u1.market = u2.market
WHERE u1.vehicleid = '{0951FA83-785D-4B0D-9341-0F44399D8C1A}'  


SELECT 
  (SELECT length(market) FROM usedcars WHERE vehicleid = '{987B49B9-4A59-4B96-B8C9-369B0B4AF369}'),

  (SELECT length(market) FROM usedcars WHERE vehicleid = '{0951FA83-785D-4B0D-9341-0F44399D8C1A}')
FROM system.iota  

EXECUTE PROCEDURE sp_reindex('usedcars', 1024)
  
--9/20/11
-- fucking had to TRIM Market to make this work
SELECT t.vehicleid, t.stocknumber, t.vin, t.year, t.make, t.dateacquired, t.salesstatus, u.stocknumber, u.dateacquired, u.salesstatus, u.vehicleid
FROM usedcars t
LEFT JOIN (
  SELECT stocknumber, vin, salesstatus, dateacquired, vehicleid
  FROM usedcars
  WHERE trim(market) = 'SP-St. Paul'
  AND LEFT(TRIM(stocknumber),1)IN ('0','1','2','3','4','5','6','7','8','9')) u ON t.vin = u.vin
    AND t.dateacquired = u.dateacquired 
    AND t.salesstatus <> u.salesstatus
WHERE trim(market) = 'SP-St. Paul'
AND LEFT(TRIM(t.stocknumber),1) NOT IN ('0','1','2','3','4','5','6','7','8','9')
AND t.salesstatus NOT IN ('Sold','Wholesaled')
AND u.stocknumber IS NOT NULL   
-- DELETE the temp WHERE there IS a corresponding current stk # 
DELETE FROM usedcars
WHERE vehicleid IN (
  SELECT t.vehicleid
  FROM usedcars t
  LEFT JOIN (
    SELECT stocknumber, vin, salesstatus, dateacquired, vehicleid
    FROM usedcars
    WHERE trim(market) = 'SP-St. Paul'
    AND LEFT(TRIM(stocknumber),1)IN ('0','1','2','3','4','5','6','7','8','9')) u ON t.vin = u.vin
      AND t.dateacquired = u.dateacquired 
      AND t.salesstatus <> u.salesstatus
  WHERE trim(market) = 'SP-St. Paul'
  AND LEFT(TRIM(t.stocknumber),1) NOT IN ('0','1','2','3','4','5','6','7','8','9')
  AND t.salesstatus NOT IN ('Sold','Wholesaled')
  AND u.stocknumber IS NOT NULL)
  
  