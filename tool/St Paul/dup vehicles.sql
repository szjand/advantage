SELECT *
FROM (
SELECT vehicleid, vin, make, model, stocknumber, salesstatus, dateacquired
FROM usedcars u
WHERE market = 'SP-St. Paul'
  AND vin IN (
    SELECT vin
    FROM usedcars
    WHERE market = 'SP-St. Paul'
      AND LEFT(TRIM(stocknumber), 1) IN ('1','2','3','4','5','6','7','8','9')
    GROUP BY vin, dateacquired 
      HAVING COUNT(*) > 1)
  AND EXISTS (
    SELECT 1
    FROM usedcars
    WHERE market = 'SP-St. Paul'
    AND vin = u.vin
    AND salesstatus NOT IN ('Sold','Wholesaled'))) x
WHERE salesstatus NOT IN ('Sold','Wholesaled')   
ORDER BY vin
          
   
SELECT vehicleid, vin, make, model, stocknumber, salesstatus, dateacquired
FROM usedcars 
WHERE vin = '1FTZR15E15PA68085'

DELETE
FROM usedcars
WHERE vehicleid = '{754CC4B8-F23D-457B-9935-9CFE454D98F2}'