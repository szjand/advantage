SELECT * FROM (
   SELECT 
/*
DROP view vUsedCarsOpenRecon

SELECT * FROM vUsedCarsOpenRecon 

SELECT stocknumber, mReconPlan, MechStatusDisplay, MechSched, 
  trim(cast(month(MechSched) AS sql_char)) + '/' + cast(dayofmonth(MechSched) AS sql_char),
  mSchedDisplay
FROM vUsedCarsOpenRecon 
WHERE mechstatus IS not NULL 
AND OwningLocationID = 'B6183892-C79D-4489-A58C-B526DF948B06'
AND mReconPlan <> ''

3/29/12: fb 82 parts status
5/1/12: repl uph with typ of detail 
6/23/12: need to ADD uph back
*/
  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN rs.MechStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN rs.MechStatus = 'MechanicalReconProcess_InProcess' THEN ''
            ELSE
              CASE -- NOT IN WIP
                WHEN MechSched IS NULL THEN 'NS:'
                ELSE
                  CASE -- scheduled
                    WHEN TimeStampDiff(sql_tsi_second, MechSched, now()) > 0  THEN 'OD:'
                    ELSE trim(cast(month(MechSched) AS sql_char)) + '/' + trim(cast(dayofmonth(MechSched) AS sql_char)) + ':'
                  END 
              END 
          END 
      END 
  END AS mReconPlan,
  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN rs.BodyStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN rs.BodyStatus = 'BodyReconProcess_InProcess' THEN ''
            ELSE
              CASE -- NOT IN WIP
                WHEN BodySched IS NULL THEN 'NS:'
                ELSE
                  CASE -- scheduled
                    WHEN TimeStampDiff(sql_tsi_second, BodySched, now()) > 0  THEN 'OD:'
                    ELSE trim(cast(month(BodySched) AS sql_char)) + '/' + trim(cast(dayofmonth(BodySched) AS sql_char)) + ':'
                  END 
              END 
          END 
      END 
  END AS bReconPlan,
  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN rs.AppStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN rs.AppStatus = 'AppearanceReconProcess_InProcess' THEN ''
            ELSE
              CASE -- NOT IN WIP
                WHEN AppSched IS NULL THEN 'NS:'
                ELSE
                  CASE -- scheduled
                    WHEN TimeStampDiff(sql_tsi_second, AppSched, now()) > 0  THEN 'OD:'
                    ELSE trim(cast(month(AppSched) AS sql_char)) + '/' + trim(cast(dayofmonth(AppSched) AS sql_char)) + ':'
                  END 
              END 
          END 
      END 
  END AS aReconPlan, 
  viixx.stocknumber, 
  CASE WHEN uph.VehicleInventoryItemID IS NOT NULL THEN 'X' END AS Upholstery,
  aw.App,
  left(substring(rs.mechstatus, position('_' IN rs.mechstatus) + 1, length(TRIM(rs.mechstatus)) - position('_' IN rs.mechstatus) + 1), 12) AS Mech,
  CASE
    WHEN rs.MechStatus = 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
    WHEN rs.MechStatus = 'MechanicalReconProcess_InProcess' THEN 'WIP'
    ELSE 'No Open Items'
  END AS MechStatusDisplay,
  left(substring(rs.BodyStatus, position('_' IN rs.BodyStatus) + 1, length(TRIM(rs.BodyStatus)) - position('_' IN rs.BodyStatus) + 1), 12) AS Body,
  CASE
    WHEN rs.BodyStatus = 'BodyReconProcess_NotStarted' THEN 'Not Started - $' + cast(coalesce(bd.BodyAmt, 0) AS sql_char)
    WHEN rs.BodyStatus = 'BodyReconProcess_InProcess' THEN 'WIP'
    ELSE 'No Open Items'
  END AS BodyStatusDisplay,
  left(substring(rs.AppStatus, position('_' IN rs.AppStatus) + 1, length(TRIM(rs.AppStatus)) - position('_' IN rs.AppStatus) + 1), 12) AS App,
    CASE
    WHEN rs.AppStatus = 'AppearanceReconProcess_NotStarted' THEN 'Not Started'
    WHEN rs.AppStatus = 'AppearanceReconProcess_InProcess' THEN 'WIP'
    ELSE 'No Open Items'
  END AS AppStatusDisplay,
  CASE
    WHEN mechstatus IS NOT NULL THEN ''
    WHEN bodystatus IS NOT NULL THEN ''
    WHEN rs.appstatus = 'AppearanceReconProcess_InProcess' THEN ''
    ELSE 'X'
  END AS G2G,
  currentpriority,
  coalesce(a.keystatus, 'Unknown') AS KeyStatus
FROM VehicleInventoryItems viixx -- OPEN VehicleInventoryItems only 
INNER JOIN ( -- limit to only VehicleInventoryItems with OPEN recon 
  SELECT viix.stocknumber, mrs.MechStatus, brs.BodyStatus, ars.AppStatus, viix.VehicleInventoryItemID 
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS MechStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'MechanicalReconProcess'
    AND status <> 'MechanicalReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) mrs ON viix.VehicleInventoryItemID = mrs.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS BodyStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'BodyReconProcess'
    AND status <> 'BodyReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) brs ON viix.VehicleInventoryItemID = brs.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS AppStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'AppearanceReconProcess'
    AND status <> 'AppearanceReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) ars ON viix.VehicleInventoryItemID = ars.VehicleInventoryItemID     
  WHERE viix.ThruTS IS NULL  
  AND (mrs.MechStatus IS NOT NULL OR brs.BodyStatus IS NOT NULL OR ars.AppStatus IS NOT NULL)) rs ON viixx.VehicleInventoryItemID = 

rs.VehicleInventoryItemID
LEFT JOIN ( -- parts status
  SELECT rax.VehicleInventoryItemID, 
    CASE
      WHEN SUM(CASE WHEN po.VehicleReconItemID IS NOT NULL THEN 1 ELSE 0 END) = 0 THEN '' 
      ELSE
        CASE 
          WHEN SUM(CASE WHEN po.VehicleReconItemID IS NOT NULL THEN 1 ELSE 0 END) = SUM(CASE WHEN po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 

'Rcvd'
          ELSE
            CASE 
              WHEN SUM(CASE WHEN po.VehicleReconItemID IS NOT NULL AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ord'
              ELSE 'Ord'
            END 
       END 
    END AS PartsStatus
  FROM ReconAuthorizations rax -- only OPEN ReconAuthorizations 
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
    AND arix.Status <> 'AuthorizedReconItem_Complete' 
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
    AND vrix.typ LIKE 'M%'
  LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
  WHERE rax.ThruTS IS NULL 
  GROUP BY rax.VehicleInventoryItemID) ps ON viixx.VehicleInventoryItemID = ps.VehicleInventoryItemID 
LEFT JOIN -- Move List
  VehiclesToMove vtm ON viixx.VehicleInventoryItemID = vtm.VehicleInventoryItemID 
  AND vtm.ThruTS IS NULL 
/**/  
LEFT JOIN ( -- upholstery
  SELECT distinct vrix.VehicleInventoryItemID -- 8/7/11: without DISTINCT generated multiple records, don't know why
  FROM VehicleReconItems vrix
  INNER JOIN AuthorizedReconItems arix on vrix.VehicleReconItemID = arix.VehicleReconItemID 
    AND arix.status <> 'AuthorizedReconItem_Complete'
  INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID      
  WHERE vrix.typ = 'AppearanceReconItem_Upholstry')uph ON viixx.VehicleInventoryItemID = uph.VehicleInventoryItemID   
/**/      
LEFT JOIN ( -- App
  SELECT r.VehicleInventoryItemID, 
    MAX(
      CASE 
        WHEN v1.VehicleInventoryItemID IS NOT NULL THEN
          CASE 
            WHEN upper(v1.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
            WHEN upper(v1.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
            WHEN upper(v1.description) LIKE 'AU%' THEN 'Auction'
            WHEN upper(v1.Description) = 'PRICING RINSE' THEN 'PR'
          END 
      END) AS App 
  FROM ReconAuthorizations r 
  INNER JOIN AuthorizedReconItems a ON r.ReconAuthorizationID = a.ReconAuthorizationID
    AND a.status <> 'AuthorizedReconItem_Complete'
  INNER JOIN VehicleReconItems v1 ON a.VehicleReconItemID = v1.VehicleReconItemID 
    AND v1.typ = 'PartyCollection_AppearanceReconItem'
  WHERE r.ThruTS IS NULL 
  GROUP BY r.VehicleInventoryItemID) aw ON viixx.VehicleInventoryItemID = aw.VehicleInventoryItemID  
LEFT JOIN (-- yr make model color,
  SELECT yearmodel, make, model, exteriorcolor, VehicleItemID  
  FROM vehicleitems) vix ON viixx.VehicleItemID = vix.VehicleItemID 
/**/  
LEFT JOIN ( -- tires
  SELECT distinct vr.VehicleInventoryItemID, vr.VehicleReconItemID -- 8/7/11: without DISTINCT generated multiple records, don't know why
  FROM VehicleReconItems vr
  INNER JOIN AuthorizedReconItems arix ON vr.VehicleReconItemID = arix.VehicleReconItemID
  WHERE ((vr.typ = 'MechanicalReconItem_Tires')
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%'))
--  AND vr.partsinstock = true
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations raix
	INNER JOIN AuthorizedReconItems arix ON raix.ReconAuthorizationID = arix.ReconAuthorizationID
	  AND arix.Status <> 'AuthorizedReconItem_Complete' 
	WHERE raix.VehicleInventoryItemID = vr.VehicleInventoryItemID 
	AND ThruTS IS NULL)) tvr ON viixx.VehicleInventoryItemID = tvr.VehicleInventoryItemID 	
LEFT JOIN -- for tire status of D
  AuthorizedReconItems arix ON tvr.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.Status = 'AuthorizedReconItem_Complete'   
LEFT JOIN ( -- body $$
  SELECT SUM(vrix.TotalPartsAmount + vrix.LaborAmount) AS BodyAmt, vrix.VehicleInventoryItemID
  FROM VehicleReconItems vrix
  INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
  WHERE vrix.typ LIKE 'B%' 
  AND rax.ThruTS IS NULL 
  GROUP BY vrix.VehicleInventoryItemID) bd ON viixx.VehicleInventoryItemID = bd.VehicleInventoryItemID    
LEFT JOIN ( -- ReconPlans
  SELECT v.VehicleInventoryItemID, 
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS 

MechSched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS 

BodySched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS 

AppSched,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS 

MechProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS 

BodyProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS 

AppProm    
  FROM (
    SELECT DISTINCT VehicleInventoryItemID
    FROM reconplans
    WHERE ThruTS IS NULL) v) rp ON viixx.VehicleInventoryItemID = rp.VehicleInventoryItemID   
LEFT JOIN keyperkeystatus a ON viixx.stocknumber = a.stocknumber    
WHERE viixx.ThruTS IS NULL) x
WHERE g2g = 'X'
ORDER BY currentpriority desc