--DROP TABLE zJonUpdVIIS
CREATE TABLE zUpdVIIS (
  runtime timestamp, 
  what cichar(60),
  fromts timestamp,
  thruts timestamp) IN database; 

  
alter PROCEDURE zJonUVIIS(
  runtime timestamp, 
  what CIChar(50),
  fromts Timestamp, 
  thruts timestamp)
  
BEGIN

IF (SELECT thruts FROM __input) IS NULL THEN
  INSERT INTO zUpdVIIS (runtime, what, fromts)
    values(
      (select runtime FROM __input),
      (select what FROM __input), 
      (select fromts FROM __input));
ELSE       
  UPDATE zUpdVIIS
  SET thruts = (SELECT thruts FROM __input)
  where what = (SELECT what FROM __input)
    AND runtime = (SELECT runtime FROM __input)
    AND ThruTS IS NULL;  
END IF;    
END; 

delete FROM zUpdVIIS

SELECT z1.runtime, z1.what, z2.elapsed
FROM zupdviis z1
INNER JOIN (
  SELECT runtime, max(timestampdiff(sql_tsi_frac_second, fromts, thruts)) AS elapsed 
  FROM zUpdVIIS 
  GROUP BY runtime) z2 ON z1.runtime = z2.runtime 
    AND z2.elapsed = timestampdiff(sql_tsi_frac_second, z1.fromts, z1.thruts)
WHERE z2.elapsed > 1000   

--the @status routine IS the culprit: > 8 sec consistently
--  @StatusFlagSet IS 2nd @ ~ .7 sec
SELECT z.*, timestampdiff(sql_tsi_frac_second, fromts, thruts) AS elapsed  FROM zUpdVIIS z

-- i'm pretty sure the issue IS sp.VehicleInventoryItemStatusProcessReconDepartment
/*
6/21/12 --
refactor sp.VehicleInventoryItemStatusProcessReconDepartment
2 purposes:
    1. reconcile / update VehicleInventoryItemStatuses based ON state of ReconAuthorizations AND AuthorizedReconItems
    2. return @Status
*/
SELECT a.VehicleInventoryItemID, a.ReconAuthorizationID, b.status, b.startts, b.completets, c.typ
FROM ReconAuthorizations a
LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
WHERE a.VehicleInventoryItemID = 'a57b6418-bb50-4559-9f62-14b9b9cd1777'
  AND a.ThruTS IS NULL 


SELECT a.VehicleInventoryItemID, a.ReconAuthorizationID, b.status, b.startts, b.completets, c.typ, d.category
FROM ReconAuthorizations a
LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
LEFT JOIN TypCategories d ON c.typ = d.typ
WHERE a.VehicleInventoryItemID = 'd9661790-e125-41de-a0a6-663f454429c8'
  AND a.ThruTS IS NULL 

-- ReconAuthorizations AuthorizedReconItems combinations
SELECT b.status, coalesce(b.startts, '12/31/9999 00:00:01'), coalesce(b.completetes, '12/31/9999 00:00:01'), d.category, COUNT(*)

-- ahh, includes a couple removed
-- removed IS actually a typ, NOT a status
SELECT b.status, d.category, COUNT(*)
FROM ReconAuthorizations a
LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
LEFT JOIN TypCategories d ON c.typ = d.typ
--WHERE a.ThruTS IS NULL 
GROUP BY b.status, d.category

SELECT status, COUNT(*)
FROM AuthorizedReconItems 
GROUP BY status 

SELECT typ, COUNT(*)
FROM AuthorizedReconItems
GROUP BY typ

SELECT * FROM AuthorizedReconItems 

SELECT *
FROM VehicleReconItems 

-- questions
-- WHERE are upholstry etc decoded
--
SELECT *
FROM typcategories
WHERE typ LIKE 'Mechani%'

SELECT category, COUNT(*)
FROM VehicleInventoryItemStatuses 
GROUP BY category

VehicleReconItems categories:
'AppearanceReconItem'
'BodyReconItem'
'MechanicalReconItem'
'PartyCollection'



SELECT b.category, COUNT(*)
FROM VehicleReconItems a
LEFT JOIN typcategories b ON a.typ = b.typ
GROUP BY b.category
  
the categories of statuses to be upd/ins are
AppearanceReconProcess
BodyReconProcess
MechanicalReconProcess

-- these are ALL the statuses that need to be updated/ins
-- based ON what combination of ReconAuthorizations AND AuthorizedReconItems 
SELECT category, status, COUNT(*)
FROM statuscategories
WHERE category IN ('AppearanceReconProcess','BodyReconProcess','MechanicalReconProcess')
group by category, status

SELECT * 
FROM VehicleInventoryItemStatuses
WHERE category IN ('AppearanceReconProcess','BodyReconProcess','MechanicalReconProcess') 

-- we are NOT changing anything IN AuthorizedReconItems, ONly VehicleInventoryItemStatuses 
SELECT a.VehicleInventoryItemID, b.status, d.category
FROM ReconAuthorizations a
INNER JOIN VehicleInventoryItems e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID 
LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
LEFT JOIN TypCategories d ON c.typ = d.typ
WHERE a.ThruTS IS NULL 
  AND e.thruts IS NULL 
GROUP BY a.VehicleInventoryItemID, b.status, d.category 

CASE WHEN there EXISTS for a vii/category a status of NOT started
what''s the precedence of AuthorizedReconItems statuses 
IF any wip THEN wip
  ELSE any NOT started THEN NOT started
  ELSE complete

ari.statuses
  'AuthorizedReconItem_Complete'
  'AuthorizedReconItem_InProcess'
  'AuthorizedReconItem_NotStarted'

SELECT e.stocknumber, category,
  MAX(
    case 
      when b.status = 'AuthorizedReconItem_InProcess' THEN 'WIP'
      WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 'Not Started'
      WHEN b.status = 'AuthorizedReconItem_Complete' THEN 'Complete'
      ELSE 'WTF'
    END) AS status
FROM ReconAuthorizations a
INNER JOIN VehicleInventoryItems e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID 
LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
LEFT JOIN TypCategories d ON c.typ = d.typ
--WHERE a.VehicleInventoryItemID = '01053898-5e2c-449b-8c1a-2515012ec118'
WHERE a.ThruTS IS NULL 
  AND e.thruts IS NULL 
GROUP BY e.stocknumber, d.category
ORDER BY e.stocknumber, category

SELECT DISTINCT status
FROM ReconAuthorizations a
INNER JOIN VehicleInventoryItems e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID 
LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
LEFT JOIN TypCategories d ON c.typ = d.typ
WHERE a.ThruTS IS NULL 
  AND e.thruts IS NULL 


SELECT *
FROM VehicleInventoryItems 
WHERE stocknumber = '10359R'

-- want a row for each VehicleInventoryItems/category
SELECT a.category, b.VehicleInventoryItemID 
FROM typcategories a
full OUTER JOIN ReconAuthorizations b ON 1 = 1
WHERE category IN (
    'AppearanceReconItem',
    'BodyReconItem',
    'MechanicalReconItem',
    'PartyCollection')
  AND b.ThruTS IS NULL 
  AND b.VehicleInventoryItemID = '01053898-5e2c-449b-8c1a-2515012ec118'
GROUP BY VehicleInventoryItemID , category

SELECT VehicleInventoryItemID, COUNT(*)
FROM (
SELECT a.category, b.VehicleInventoryItemID 
FROM (
  SELECT DISTINCT category
  FROM typcategories
  WHERE category IN (
      'AppearanceReconItem',
      'BodyReconItem',
      'MechanicalReconItem',
      'PartyCollection')) a, ReconAuthorizations b
WHERE b.ThruTS IS NULL) x
GROUP BY VehicleInventoryItemID 
HAVING COUNT(*) <> 4

-- here's 1 row per VehicleInventoryItemID w/OPEN ReconAuthorizationID AND category
SELECT d.*, e.*
FROM (
  SELECT a.category, b.stocknumber, b.VehicleInventoryItemID
  FROM (
    SELECT DISTINCT category
    FROM typcategories
    WHERE category IN (
        'AppearanceReconItem',
        'BodyReconItem',
        'MechanicalReconItem',
        'PartyCollection')) a, VehicleInventoryItems b
  WHERE b.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850') d
LEFT JOIN ReconAuthorizations e ON d.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE e.ThruTS IS NULL 

-- oh shit, why the fuck does 12506B NOT have an open ReconAuthorizations 
-- becuase it's at auction, yep
SELECT * FROM ReconAuthorizations WHERE VehicleInventoryItemID = '8470e261-cda8-4fcf-b9d6-ab94b4e7e30b'

-- here's 1 row per VehicleInventoryItemID w/OPEN ReconAuthorizationID AND category
SELECT stocknumber, d.category,
  MAX(
    case 
      when f.status = 'AuthorizedReconItem_InProcess' THEN 'WIP'
      WHEN f.status = 'AuthorizedReconItem_NotStarted' THEN 'Not Started'
      WHEN f.status = 'AuthorizedReconItem_Complete' THEN 'Complete'
      ELSE 'WTF'
    END) AS status
FROM (
  SELECT a.category, b.stocknumber, b.VehicleInventoryItemID
  FROM (
    SELECT DISTINCT category
    FROM typcategories
    WHERE category IN (
        'AppearanceReconItem',
        'BodyReconItem',
        'MechanicalReconItem',
        'PartyCollection')) a, VehicleInventoryItems b
  WHERE b.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850') d
LEFT JOIN ReconAuthorizations e ON d.VehicleInventoryItemID = e.VehicleInventoryItemID 
  AND e.ThruTS IS NULL
LEFT JOIN AuthorizedReconItems f ON e.ReconAuthorizationID = f.ReconAuthorizationID 
LEFT JOIN VehicleReconItems g ON f.VehicleReconItemID = g.VehicleReconItemID 
  AND (
    SELECT category
    FROM typcategories
    WHERE typ = g.typ) = d.category
GROUP BY stocknumber, d.category


SELECT a.VehicleInventoryItemID, a.ReconAuthorizationID, b.status, d.category
FROM ReconAuthorizations a
LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
LEFT JOIN typcategories d ON c.typ = d.typ
WHERE a.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
  AND a.thruts IS NULL 


SELECT *
  FROM (
    SELECT DISTINCT category
    FROM typcategories
    WHERE category IN (
        'AppearanceReconItem',
        'BodyReconItem',
        'MechanicalReconItem',
        'PartyCollection')) a, ReconAuthorizations b
WHERE b.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
  AND b.thruts IS NULL

SELECT x.VehicleInventoryItemID, x.category,
  coalesce (y.status, 'No ARI')
FROM (
SELECT *
  FROM (
    SELECT DISTINCT category
    FROM typcategories
    WHERE category IN (
        'AppearanceReconItem',
        'BodyReconItem',
        'MechanicalReconItem',
        'PartyCollection')) a, ReconAuthorizations b
WHERE b.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
  AND b.thruts IS NULL) x
LEFT JOIN (
  SELECT a.VehicleInventoryItemID, d.category,
    MAX(
      case 
        when b.status = 'AuthorizedReconItem_InProcess' THEN 'WIP'
        WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 'Not Started'
        WHEN b.status = 'AuthorizedReconItem_Complete' THEN 'Complete'
        ELSE 'WTF'
      END) AS status
  FROM ReconAuthorizations a
  LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
  LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
  LEFT JOIN typcategories d ON c.typ = d.typ
  WHERE a.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
    AND a.thruts IS NULL 
  GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category



SELECT VehicleInventoryItemID,
  max(
    CASE WHEN category = 'MechanicalReconItem' THEN 
      CASE EXPR 
        when 3 then 'MechanicalReconProcess_InProcess' 
        when 2 then 'MechanicalReconProcess__NotStarted'
        when 1 then 'MechanicalReconProcess___NoIncompleteReconItems'
       END 
     END) AS MechStatus,
  max(
    CASE WHEN category = 'BodyReconItem' THEN 
      CASE EXPR 
        when 3 then 'BodyReconProcess_InProcess' 
        when 2 then 'BodyReconProcess__NotStarted'
        when 1 then 'BodyReconProcess___NoIncompleteReconItems'
       END 
     END) AS BodyStatus,
  max(
    CASE WHEN category IN ('AppearanceReconItem','PartyCollection') THEN 
        CASE EXPR 
          when 3 then 'AppearanceReconProcess_InProcess' 
          when 2 then 'AppearanceReconProcess__NotStarted'
          when 1 then 'AppearanceReconProcess___NoIncompleteReconItems'
         END 
     END) AS AppStatus

SELECT VehicleInventoryItemID,
  max(CASE WHEN category = 'MechanicalReconItem' THEN EXPR END) AS MechStatus,
  max(CASE WHEN category = 'BodyReconItem' THEN expr END) AS BodyStatus,
  MAX(CASE WHEN category IN ('AppearanceReconItem','PartyCollection') THEN expr end) AS AppStatus
FROM (-- this final grouping IS necessary to put the 2 app categories together
  SELECT x.VehicleInventoryItemID, x.category,
    coalesce (y.status, 1)
  FROM (
  SELECT *
    FROM (
      SELECT DISTINCT category
      FROM typcategories
      WHERE category IN (
          'AppearanceReconItem',
          'BodyReconItem',
          'MechanicalReconItem',
          'PartyCollection')) a, ReconAuthorizations b
  WHERE b.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
    AND b.thruts IS NULL) x
  LEFT JOIN (
    SELECT a.VehicleInventoryItemID, d.category,
      MAX(
        case 
          when b.status = 'AuthorizedReconItem_InProcess' THEN 3
          WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 2
          WHEN b.status = 'AuthorizedReconItem_Complete' THEN 1
          ELSE 1
        END) AS status
    FROM ReconAuthorizations a
    LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
    LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
    LEFT JOIN typcategories d ON c.typ = d.typ
    WHERE a.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
      AND a.thruts IS NULL 
    GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category) w
GROUP BY VehicleInventoryItemID



SELECT 
  case coalesce (y.status, 1) 
    WHEN 3 THEN 'MechanicalReconProcess_InProcess'
    WHEN 2 THEN 'MechanicalReconProcss_NotStarted'
    ELSE 'MechanicalReconProcess_NoIncompleteReconItems'
  END AS MechStatus
FROM (
  SELECT *
    FROM (
      SELECT DISTINCT category
      FROM typcategories
      WHERE category = 'MechanicalReconItem') a, ReconAuthorizations b
      WHERE b.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
        AND b.thruts IS NULL) x
  LEFT JOIN (
    SELECT a.VehicleInventoryItemID, d.category,
      MAX(
        case 
          when b.status = 'AuthorizedReconItem_InProcess' THEN 3
          WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 2
          WHEN b.status = 'AuthorizedReconItem_Complete' THEN 1
          ELSE 1
        END) AS status
    FROM ReconAuthorizations a
    LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
    LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
    LEFT JOIN typcategories d ON c.typ = d.typ
    WHERE a.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
--      AND a.thruts IS NULL 
    GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category
    
  SELECT 
    MAX(
      CASE coalesce (y.status, 1)
        WHEN 3 THEN 'AppearanceReconProcess_InProcess'
        WHEN 2 THEN 'AppearanceReconProcss_NotStarted'
        ELSE 'AppearanceReconProcess_NoIncompleteReconItems'
      END )
  FROM (
    SELECT *
      FROM (
        SELECT DISTINCT category
        FROM typcategories
        WHERE category IN ('AppearanceReconItem','PartyCollection')) a, ReconAuthorizations b
        WHERE b.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
          AND b.thruts IS NULL) x
    LEFT JOIN (
      SELECT a.VehicleInventoryItemID, d.category,
        MAX(
          case 
            when b.status = 'AuthorizedReconItem_InProcess' THEN 3
            WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 2
            WHEN b.status = 'AuthorizedReconItem_Complete' THEN 1
            ELSE 1
          END) AS status
      FROM ReconAuthorizations a
      LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
      LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
      LEFT JOIN typcategories d ON c.typ = d.typ
      WHERE a.VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
        AND a.thruts IS NULL 
      GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category;       

ok, this works well
now UPDATE VehicleInventoryItemStatuses based upon the statuses

SELECT category, status, COUNT(*)
FROM VehicleInventoryItemStatuses
WHERE category IN ('AppearanceReconProcess','BodyReconProcess','MechanicalReconProcess') 
GROUP BY category, status

SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'
  AND category IN ('AppearanceReconProcess','BodyReconProcess','MechanicalReconProcess') 
  AND ThruTS IS NULL 
  
IF @MechanicalStatus = 'MechanicalReconProcess_InProcess'  
IF @MechanicalStatus = 'MechanicalReconProcess_NoIncompleteReconItems'
IF @MechanicalStatus = 'MechanicalReconProcess_NotStarted'
IF @BodyStatus = 'BodyReconProcess_InProcess'
IF @BodyStatus = 'BodyReconProcess_NoIncompleteReconItems'
IF @BodyStatus = 'BodyReconProcess_NotStarted'
IF @AppearanceStatus = 'AppearanceReconProcess_InProcess'
IF @AppearanceStatus = 'AppearanceReconProcess_NoIncompleteReconItems'
IF @AppearanceStatus = 'AppearanceReconProcess_NotStarted'

VehicleInventoryItemStatuses with more than one status per category
nope
SELECT VehicleInventoryItemID, category
FROM (
  SELECT VehicleInventoryItemID, category, status
  FROM VehicleInventoryItemStatuses
  WHERE category IN ('AppearanceReconProcess','BodyReconProcess','MechanicalReconProcess') 
    AND thruts IS null
  GROUP BY VehicleInventoryItemID, category, status) a
GROUP BY VehicleInventoryItemID, category
HAVING COUNT(*) > 1

IF @ReconAuthorizationID IS NULL THEN

SELECT *
FROM VehicleInventoryItemStatuses 
WHERE category IN ('AppearanceReconProcess','BodyReconProcess','MechanicalReconProcess') 
  AND ThruTS IS NULL 
  AND VehicleInventoryItemID = 'd14faca2-fa21-474f-909b-6620ae61e850'



