 
-- ok, here are the raw sales for the last 30 days  
SELECT d.TheDate, r.stocknumber AS rmStockNumber, 
  left(TRIM(r.YearModel) + ' ' + TRIM(r.Make) + ' ' + TRIM(r.Model),25) AS rmVehicle,
  r.SoldAmount AS rmSoldAmount
FROM ( 
  SELECT TheDate
  FROM dds.day
  WHERE TheDate between curdate() - 30 AND curdate()) d
LEFT JOIN ( -- retail sales, NOT intramarket acquisitions, that were never available 
  SELECT i.stocknumber, v.YearModel, v.Make, v.Model, 
    vs.SoldAmount, CAST(vs.SoldTS AS sql_date) AS SoldDate, 
    m.VehicleSegment, m.VehicleType
  FROM VehicleInventoryItems i
  INNER JOIN VehicleSales vs ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID
    AND vs.Typ = 'VehicleSale_Retail' AND vs.Status = 'VehicleSale_Sold'
  INNER JOIN VehicleAcquisitions va ON i.VehicleInventoryItemID = va.VehicleInventoryItemID
    AND va.typ <> 'VehicleAcquisition_IntraMarketPurchase'
  INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID
  INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
    AND o.name = 'rydells'
  LEFT JOIN MakeModelClassifications m ON v.make = m.make
    AND v.model = m.model
  WHERE NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = i.VehicleInventoryItemID 
      AND category = 'RMFlagAV')) r  ON d.TheDate = r.SoldDate
WHERE r.stocknumber IS NOT NULL 

-- ON those days ON which vehicles were sold out of raw, what comparable vehicles were available
-- what comprises a comparable vehicle
-- start with mmc.typ/segment
-- this matches available typ/seg with sold raw typ/seg
SELECT d.TheDate, r.stocknumber AS rmStockNumber, 
  left(TRIM(r.YearModel) + ' ' + TRIM(r.Make) + ' ' + TRIM(r.Model),25) AS rmVehicle,
  r.SoldAmount AS rmSoldAmount, a.stocknumber AS aStockNumber, a.VehicleInventoryItemID AS aViiID,
  left(TRIM(a.YearModel) + ' ' + TRIM(a.Make) + ' ' + TRIM(a.Model),25) AS aVehicle
--INTO #jon  
FROM ( 
  SELECT TheDate
  FROM dds.day
  WHERE TheDate between curdate() - 30 AND curdate()) d
LEFT JOIN (  -- r: retail sales, NOT intramarket acquisitions, that were never available 
  SELECT i.stocknumber, v.YearModel, v.Make, v.Model, 
    vs.SoldAmount, CAST(vs.SoldTS AS sql_date) AS SoldDate, 
    m.VehicleSegment, m.VehicleType
  FROM VehicleInventoryItems i
  INNER JOIN VehicleSales vs ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID
    AND vs.Typ = 'VehicleSale_Retail' AND vs.Status = 'VehicleSale_Sold'
  INNER JOIN VehicleAcquisitions va ON i.VehicleInventoryItemID = va.VehicleInventoryItemID
    AND va.typ <> 'VehicleAcquisition_IntraMarketPurchase'
  INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID
  INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
    AND o.name = 'rydells'
  LEFT JOIN MakeModelClassifications m ON v.make = m.make
    AND v.model = m.model
  WHERE NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = i.VehicleInventoryItemID 
      AND category = 'RMFlagAV')) r  ON d.TheDate = r.SoldDate
LEFT JOIN ( -- a: which vehicle were available ON TheDate
  SELECT i.VehicleInventoryItemID, i.stocknumber, m.vehiclesegment, m.vehicletype,
    cast(s.FromTS AS sql_date) AS FromTS, 
    coalesce(cast(s.ThruTS AS sql_date), CAST('12/31/3030' AS sql_date)) AS ThruTS,
    v.YearModel, v.Make, v.Model
  FROM VehicleInventoryItemStatuses s
  INNER JOIN VehicleInventoryItems i ON s.VehicleInventoryItemID = i.VehicleInventoryItemID 
  INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
    AND o.name = 'rydells'
  INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID
  LEFT JOIN MakeModelClassifications m ON v.make = m.make
    AND v.model = m.model  
  WHERE s.category = 'RMFlagAV') a ON d.TheDate BETWEEN a.FromTS AND a.ThruTS
    AND r.VehicleSegment = a.VehicleSegment
    AND r.VehicleType = a.VehicleType
WHERE r.stocknumber IS NOT NULL   
       
-- need to ADD price of available vehicles
-- the issue IS what was the price ON a particular day
-- MAX pricingts that IS <= than TheDate 
-- return a single record with the appropriate VehiclePricingTS

SELECT e.TheDate, e.rmStocknumber, e.rmVehicle, e.rmSoldAmount, e.aStocknumber, 
  e.aVehicle, d.Amount AS aPrice
FROM (
  SELECT d.TheDate, r.stocknumber AS rmStockNumber, 
    left(TRIM(r.YearModel) + ' ' + TRIM(r.Make) + ' ' + TRIM(r.Model),25) AS rmVehicle,
    r.SoldAmount AS rmSoldAmount, a.stocknumber AS aStockNumber, a.VehicleInventoryItemID AS aViiID,
    left(TRIM(a.YearModel) + ' ' + TRIM(a.Make) + ' ' + TRIM(a.Model),25) AS aVehicle
  FROM ( 
    SELECT TheDate
    FROM dds.day
    WHERE TheDate between curdate() - 60 AND curdate()) d
  LEFT JOIN (  -- r: retail sales, NOT intramarket acquisitions, that were never available 
    SELECT i.stocknumber, v.YearModel, v.Make, v.Model, 
      vs.SoldAmount, CAST(vs.SoldTS AS sql_date) AS SoldDate, 
      m.VehicleSegment, m.VehicleType
    FROM VehicleInventoryItems i
    INNER JOIN VehicleSales vs ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID
      AND vs.Typ = 'VehicleSale_Retail' AND vs.Status = 'VehicleSale_Sold'
    INNER JOIN VehicleAcquisitions va ON i.VehicleInventoryItemID = va.VehicleInventoryItemID
      AND va.typ <> 'VehicleAcquisition_IntraMarketPurchase'
    INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID
    INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
      AND o.name = 'rydells'
    LEFT JOIN MakeModelClassifications m ON v.make = m.make
      AND v.model = m.model
    WHERE NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = i.VehicleInventoryItemID 
        AND category = 'RMFlagAV')) r  ON d.TheDate = r.SoldDate
  LEFT JOIN ( -- a: which vehicle were available ON TheDate
    SELECT i.VehicleInventoryItemID, i.stocknumber, m.vehiclesegment, m.vehicletype,
      cast(s.FromTS AS sql_date) AS FromTS, 
      coalesce(cast(s.ThruTS AS sql_date), CAST('12/31/3030' AS sql_date)) AS ThruTS,
      v.YearModel, v.Make, v.Model
    FROM VehicleInventoryItemStatuses s
    INNER JOIN VehicleInventoryItems i ON s.VehicleInventoryItemID = i.VehicleInventoryItemID 
--    INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
--      AND o.name = 'rydells'
    INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID
    LEFT JOIN MakeModelClassifications m ON v.make = m.make
      AND v.model = m.model  
    WHERE s.category = 'RMFlagAV') a ON d.TheDate BETWEEN a.FromTS AND a.ThruTS
      AND r.VehicleSegment = a.VehicleSegment
      AND r.VehicleType = a.VehicleType) e
LEFT JOIN VehiclePricings p ON e.aViiID = p.VehicleInventoryItemID  
LEFT JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID
  AND d.typ = 'VehiclePricingDetail_BestPrice'
WHERE e.rmStocknumber IS NOT NULL   
  AND p.VehiclePRicingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings
    WHERE VehicleInventoryItemID = e.aViiID
      AND cast(VehiclePricingTS AS sql_date) <= e.TheDate
    GROUP BY VehicleInventoryItemID)   
  AND ABS(e.rmSoldAmount - d.Amount) < 2001   
    
  
-- what percentage of last 30 days sales were out of raw
-- ok, here are the raw sales for the last 30 days  
SELECT d.TheDate, COUNT(distinct r.stocknumber) AS RawSales, 
  COUNT(distinct s.VehicleInventoryItemID) AS TotalSales,
  round(COUNT(DISTINCT r.stocknumber)*100.0/COUNT(DISTINCT s.VehicleInventoryItemID), 0) AS PercRaw
-- SELECT *
FROM ( 
  SELECT TheDate
  FROM dds.day
  WHERE TheDate between curdate() - 30 AND curdate()) d
LEFT JOIN ( -- r: retail sales, NOT intramarket acquisitions, that were never available 
  SELECT i.stocknumber, CAST(vs.SoldTS AS sql_date) AS SoldDate
  FROM VehicleInventoryItems i
  INNER JOIN VehicleSales vs ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID
    AND vs.Typ = 'VehicleSale_Retail' AND vs.Status = 'VehicleSale_Sold'
  INNER JOIN VehicleAcquisitions va ON i.VehicleInventoryItemID = va.VehicleInventoryItemID
    AND va.typ <> 'VehicleAcquisition_IntraMarketPurchase'
  INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
    AND o.name = 'rydells'
  WHERE NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = i.VehicleInventoryItemID 
      AND category = 'RMFlagAV')) r  ON d.TheDate = r.SoldDate
LEFT JOIN ( -- s: ALL retail sales
  SELECT vs.VehicleInventoryItemID, CAST(SoldTS AS sql_date) AS SoldDate
  FROM VehicleSales vs
  INNER JOIN VehicleInventoryItems i ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID 
  INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
    AND o.name = 'rydells'
  WHERE Typ = 'VehicleSale_Retail' AND Status = 'VehicleSale_Sold') s ON d.TheDate = s.SoldDate
WHERE r.stocknumber IS NOT NULL   
GROUP BY d.TheDate


-- what percentage of last 30 days sales were out of raw
-- per week, last 21 weeks
SELECT d.TheWeek, COUNT(distinct r.stocknumber) AS RawSales, 
  COUNT(distinct s.VehicleInventoryItemID) AS TotalSales,
  round(COUNT(DISTINCT r.stocknumber)*100.0/COUNT(DISTINCT s.VehicleInventoryItemID), 0) AS PercRaw
-- SELECT *
FROM ( 
  SELECT da.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17 
      WHEN TheDate >= CurDate() - 125 AND TheDate <= CurDate() - 119 THEN 18 
      WHEN TheDate >= CurDate() - 132 AND TheDate <= CurDate() - 120 THEN 19 
      WHEN TheDate >= CurDate() - 139 AND TheDate <= CurDate() - 127 THEN 20 
      WHEN TheDate >= CurDate() - 146 AND TheDate <= CurDate() - 134 THEN 21 
    END AS TheWeek
  FROM dds.day da
  WHERE TheDate >= CurDate() - 146
  AND TheDate <= CurDate()) d 
LEFT JOIN ( -- r: retail sales, NOT intramarket acquisitions, that were never available 
  SELECT i.stocknumber, CAST(vs.SoldTS AS sql_date) AS SoldDate
  FROM VehicleInventoryItems i
  INNER JOIN VehicleSales vs ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID
    AND vs.Typ = 'VehicleSale_Retail' AND vs.Status = 'VehicleSale_Sold'
  INNER JOIN VehicleAcquisitions va ON i.VehicleInventoryItemID = va.VehicleInventoryItemID
    AND va.typ <> 'VehicleAcquisition_IntraMarketPurchase'
  INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
    AND o.name = 'rydells'
  WHERE NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = i.VehicleInventoryItemID 
      AND category = 'RMFlagAV')) r  ON d.TheDate = r.SoldDate
LEFT JOIN ( -- s: ALL retail sales
  SELECT vs.VehicleInventoryItemID, CAST(SoldTS AS sql_date) AS SoldDate
  FROM VehicleSales vs
  INNER JOIN VehicleInventoryItems i ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID 
  INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
    AND o.name = 'rydells'
  WHERE Typ = 'VehicleSale_Retail' AND Status = 'VehicleSale_Sold') s ON d.TheDate = s.SoldDate
WHERE r.stocknumber IS NOT NULL   
GROUP BY d.TheWeek
ORDER BY d.TheWeek DESC 

-- sale date, acq date, insp date
SELECT d.TheDate, s.stocknumber, s.AcqDate, s.InsDate,
  s.SoldDate, timestampdiff(sql_tsi_day, AcqDate, SoldDate) AS AcqToSale,
  timestampdiff(sql_tsi_day, InsDate, SoldDate) AS InsToSale
-- SELECT *
FROM ( 
  SELECT TheDate
  FROM dds.day
  WHERE TheDate between curdate() - 30 AND curdate()) d
LEFT JOIN ( -- s: ALL retail sales
  SELECT vs.VehicleInventoryItemID, i.stocknumber, cast(i.FromtS AS sql_date) AS AcqDate, 
  CAST(n.VehicleInspectionTS AS sql_date) AS InsDate,
  CAST(SoldTS AS sql_date) AS SoldDate
  FROM VehicleSales vs
  INNER JOIN VehicleInventoryItems i ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID 
  INNER JOIN Organizations o ON i.OwningLocationID = o.partyid
    AND o.name = 'rydells'
  LEFT JOIN VehicleInspections n ON i.VehicleInventoryItemID = n.VehicleInventoryItemID 
  WHERE Typ = 'VehicleSale_Retail' AND Status = 'VehicleSale_Sold') s ON d.TheDate = s.SoldDate
WHERE s.stocknumber IS NOT NULL   

