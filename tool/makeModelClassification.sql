     
-- usually the issue IS non classified vehicles    
SELECT b.make, b.model, i.stocknumber, i.VehicleInventoryItemID, b.VehicleItemID, b.vin, CAST(i.fromts AS sql_date)
FROM VehicleInventoryItems i
LEFT JOIN VehicleItems b on i.VehicleItemID = b.VehicleItemID 
WHERE ThruTs IS NULL
AND NOT EXISTS (
  SELECT 1
  FROM MakeModelClassifications
  WHERE model = (
    SELECT model
    FROM VehicleItems
    WHERE VehicleItemID = i.VehicleItemID))    
    
SELECT *
FROM MakeModelClassifications -- WHERE model = 'fusion'  
WHERE make = 'buick'
  AND model LIKE 'reg%'
ORDER BY model

SELECT * FROM VehicleItems WHERE vin = '1GTW7AFG7K1215131 '

select * FROM MakeModelClassifications WHERE make = 'volvo'

SELECT DISTINCT vehiclesegment, vehicletype FROM MakeModelClassifications 
SELECT * FROM MakeModelClassifications WHERE vehicletype = 'VehicleType_Crossover'
-- Black book now separates RAM out FROM dodge AS a make
INSERT INTO MakeModelClassifications 
values('Ram', '1500', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
INSERT INTO MakeModelClassifications 
values('Ram', '3500', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
-- chevy sonic
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Sonic', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
-- 1952 chevy deluxe
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Deluxe', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
-- Volks CC
INSERT INTO MakeModelClassifications 
values('Volkswagen', 'CC', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Import')
-- buick encore
INSERT INTO MakeModelClassifications 
values('Buick', 'Encore', 'VehicleSegment_Compact','VehicleType_Crossover',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- toyota Prius V
INSERT INTO MakeModelClassifications 
values('Toyota', 'Prius V', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Subaru BRZ
INSERT INTO MakeModelClassifications 
values('Subaru', 'BRZ', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- KIA Forte
INSERT INTO MakeModelClassifications 
values('KIA', 'Forte', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- impala limited
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Impala Limited LTZ', 'VehicleSegment_Large','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- buick encore gx
INSERT INTO MakeModelClassifications 
values('Buick', 'Encore GX', 'VehicleSegment_Compact','VehicleType_Crossover',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- cadillac ct5
INSERT INTO MakeModelClassifications 
values('Cadillac', 'CT5', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')



SELECT * FROM MakeModelClassifications WHERE model like 'Impala%'

SELECT * FROM MakeModelClassifications WHERE model = 'Impala Limited LTZ'

SELECT *
FROM VehicleItems
WHERE vin = 'NM0LS7DN7AT010012'

SELECT *
FROM blackbookresolver
WHERE vin = '1C6RD7LT'

-- ford transit connect van
INSERT INTO MakeModelClassifications 
values('Ford', 'Transit Connect', 'VehicleSegment_Midsize','VehicleType_Van',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- missing FROM MakeModelClassifications 
SELECT a.stocknumber, b.vin, b.yearmodel, b.make, b.model, CAST(a.fromts AS sql_date)
FROM VehicleInventoryItems a
INNER JOIN VehicleItems b on a.VehicleItemID = b.VehicleItemID
WHERE a.thruTS IS NULL
AND NOT EXISTS (
  SELECT 1
  FROM MakeModelClassifications
  WHERE make = b.make
    AND model = b.model)

-- suzuki kizashi   
INSERT INTO MakeModelClassifications 
values('Suzuki', 'Kizashi', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- subaru xv crosstrek
SELECT * FROM MakeModelClassifications WHERE model = 'juke'

INSERT INTO MakeModelClassifications 
values('Subaru', 'XV Crosstrek', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Meredecs-Benz GLK Class

SELECT DISTINCT make, model
FROM MakeModelClassifications
WHERE make LIKE 'merced%'

SELECT DISTINCT make, model
FROM MakeModelClassifications
WHERE vehiclesegment = 'VehicleSegment_Compact'
  AND vehicleType = 'VehicleType_SUV'
  
SELECT * FROM MakeModelClassifications WHERE model = 'touareg'

INSERT INTO MakeModelClassifications 
values('Mercedes-Benz', 'GLK Class', 'VehicleSegment_Compact','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- volks tiguan
SELECT * FROM MakeModelClassifications WHERE make = 'volkswagen'
SELECT * FROM MakeModelClassifications WHERE make = 'mitsubishi' AND model = 'outlander'

INSERT INTO MakeModelClassifications 
values('Volkswagen', 'Tiguan', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- hyundai veloster
INSERT INTO dps.MakeModelClassifications 
values('Hyundai', 'Veloster', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- MV-1 van
INSERT INTO MakeModelClassifications 
values('MV-1', 'Deluxe', 'VehicleSegment_Midsize','VehicleType_Van',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Chevrolet Citation
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Citation', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Hyundai Santa Fe Sport (same AS buck enclave)
INSERT INTO MakeModelClassifications 
SELECT 'Hyundai','Santa Fe Sport',vehiclesegment,vehicletype,luxury,commercial,
  sport, '01/01/2014 08:00:00', ThruTS, 'MfgOriginTyp_Import' 
FROM MakeModelClassifications 
WHERE model = 'enclave';

-- isuzu flat bed
INSERT INTO MakeModelClassifications 
SELECT make,'Flat Bed',vehiclesegment,vehicletype,luxury,commercial,sport,fromts,
  thruts,mfgOriginTyp
FROM MakeModelClassifications
WHERE make = 'isuzu'
AND model = 'i-370'

-- Chev city express
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'City Express', 'VehicleSegment_Midsize','VehicleType_Van',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- prius C
INSERT INTO MakeModelClassifications 
values('Toyota', 'Prius C', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- subaru wrx
INSERT INTO MakeModelClassifications 
values('Subaru', 'WRX', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '11/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Chev Trax
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Trax', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Audi Q5
INSERT INTO MakeModelClassifications 
values('Audi', 'Q5', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Fiat 500
INSERT INTO MakeModelClassifications 
values('Fiat', '500', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- ford fiesta
INSERT INTO MakeModelClassifications 
values('Ford', 'Fiesta', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- ram dakota
INSERT INTO MakeModelClassifications 
values('Ram', 'Dakota', 'VehicleSegment_Midsize','VehicleType_Pickup',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Mitsubishi outlander sport
INSERT INTO MakeModelClassifications 
values('Mitsubishi', 'Outlander Sport', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '11/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Nissan Versa Note
INSERT INTO MakeModelClassifications 
values('Nissan', 'Versa Note', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '11/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Cadillac ELR
INSERT INTO MakeModelClassifications 
values('Cadillac', 'ELR', 'VehicleSegment_Large','VehicleType_Car',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Jeep Renegade
INSERT INTO MakeModelClassifications 
values('Jeep', 'Renegade', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- cruze limited
INSERT INTO MakeModelClassifications 
select make, 'Cruze Limited', vehiclesegment,vehicletype,luxury,commercial,
  sport,now(),cast(NULL AS sql_timestamp), mfgorigintyp
-- select *  
FROM MakeModelClassifications 
WHERE model = 'cruze';

-- malibu limited
INSERT INTO MakeModelClassifications 
select make, 'Malibu Limited', vehiclesegment,vehicletype,luxury,commercial,
  sport,now(),cast(NULL AS sql_timestamp), mfgorigintyp
-- select *  
FROM MakeModelClassifications 
WHERE model = 'malibu';

-- Infinity JX
INSERT INTO MakeModelClassifications 
values('Infiniti', 'JX', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '11/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Datsun 280Z
INSERT INTO MakeModelClassifications 
values('Datsun', '280Z', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Buick Cascada
INSERT INTO MakeModelClassifications 
values('Buick', 'Cascada', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- GMC Acadia Limited
INSERT INTO MakeModelClassifications
values('GMC', 'Acadia Limited', 'VehicleSegment_Midsize','VehicleType_Crossover',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Cadillac XT5
INSERT INTO MakeModelClassifications
values('Cadillac', 'XT5', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Ford C-Max
INSERT INTO MakeModelClassifications
values('Ford', 'C-Max', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Kia Cadenza
INSERT INTO MakeModelClassifications
values('Kia', 'Cadenza', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Buick Envision
INSERT INTO MakeModelClassifications
values('Buick', 'Envision', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Chevrolet Biscayne
INSERT INTO MakeModelClassifications
values('Chevrolet', 'Biscayne', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Honda Crosstour
INSERT INTO MakeModelClassifications
values('Honda', 'Crosstour', 'VehicleSegment_Compact','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- subaru crosstrek
INSERT INTO MakeModelClassifications
values('Subaru', 'Crosstrek', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Honda HR-V
INSERT INTO MakeModelClassifications
values('Honda', 'HR-V', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Ford F550SD
INSERT INTO MakeModelClassifications 
values('Ford', 'F550SD', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Ford F650SD
INSERT INTO MakeModelClassifications 
values('Ford', 'F650', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Datsun Fairlady
INSERT INTO MakeModelClassifications 
values('Datsun', 'Fairlady', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- BMW 4-Series
INSERT INTO MakeModelClassifications 
values('BMW', '4-Series', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Ford T250
INSERT INTO MakeModelClassifications 
values('Ford', 'T250 Vans', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- RAM Promaster
INSERT INTO MakeModelClassifications 
values('Ram', '1500 ProMaster Vans', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Lincoln MKT
INSERT INTO MakeModelClassifications 
values('Lincoln', 'MKT', 'VehicleSegment_Large','VehicleType_Crossover',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Fiat 500L
INSERT INTO MakeModelClassifications 
values('Fiat', '500L', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Import')

-- chevy bolt
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Bolt EV', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Nissan Rogue Select
INSERT INTO MakeModelClassifications 
values('Nissan', 'Rogue Select', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '11/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- cadillac ct6
INSERT INTO MakeModelClassifications 
values('Cadillac', 'CT6', 'VehicleSegment_Large','VehicleType_Car',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- land rover lr4
INSERT INTO MakeModelClassifications 
values('Land Rover', 'LR4', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Nissan Titan XD
INSERT INTO MakeModelClassifications 
values('Nissan', 'Titan XD', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Infinity Q50
INSERT INTO MakeModelClassifications 
values('Infiniti', 'Q50', 'VehicleSegment_Large','VehicleType_Car',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- nissan rogue sport
INSERT INTO MakeModelClassifications 
values('Nissan', 'Rogue Sport', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- scion iM
INSERT INTO MakeModelClassifications 
values('Scion', 'iM', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- mini cooper countryman
INSERT INTO MakeModelClassifications 
values('Mini', 'Cooper Countryman', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- mini cooper countryman
INSERT INTO MakeModelClassifications 
values('Mini', 'Cooper Countryman', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- ford expedition MAX limited
INSERT INTO MakeModelClassifications 
values('Ford', 'Expedition Max', 'VehicleSegment_Extra_Large','VehicleType_SUV',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- toyota corolla iM
INSERT INTO MakeModelClassifications 
values('Toyota', 'Corolla iM', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '11/01/2018 08:00:00', NULL, 'MfgOriginTyp_Import')

-- ram promaster city
INSERT INTO MakeModelClassifications 
values('Ram', 'Promaster City', 'VehicleSegment_Small','VehicleType_Van',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- chevrolet express cargo
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Express Cargo', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Maserati Ghibli 
INSERT INTO MakeModelClassifications 
values('Maserati', 'Ghibli', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Import')

-- infiniti qx80
INSERT INTO MakeModelClassifications 
values('Infiniti', 'QX80', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- honda clarity
INSERT INTO MakeModelClassifications 
values('Honda', 'Clarity', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '11/01/2018 08:00:00', NULL, 'MfgOriginTyp_Import')

-- toyota corolla iM
INSERT INTO MakeModelClassifications 
values('Toyota', 'Yaris iA', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '11/01/2018 08:00:00', NULL, 'MfgOriginTyp_Import')

-- cadillac xt4
INSERT INTO MakeModelClassifications 
values('Cadillac', 'XT4', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- pontiac fiero
INSERT INTO MakeModelClassifications 
values('Pontiac', 'Fiero', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- mercedes-benz e-class
INSERT INTO MakeModelClassifications
values('Mercedes-Benz', 'E-Class', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- mercedes-benz GLS550
INSERT INTO MakeModelClassifications 
values('Mercedes-Benz', 'GLS', 'VehicleSegment_Compact','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- nissan nv200
INSERT INTO MakeModelClassifications 
values('Nissan', 'NV 200', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Import')

-- jeep ALL new compass
INSERT INTO MakeModelClassifications 
values('Jeep', 'All New Compass', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- nissan NV 3500
INSERT INTO MakeModelClassifications 
values('Nissan', 'NV 3500', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Lexus 450H
INSERT INTO MakeModelClassifications 
values('Lexus', 'RX 450h', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Import')

-- RAM 1500 DS
INSERT INTO MakeModelClassifications 
values('Ram', '1500 DS', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- nissan NV 1500
INSERT INTO MakeModelClassifications 
values('Nissan', 'NV 1500', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Import')

-- RAM 5500
INSERT INTO MakeModelClassifications 
values('Ram', '5500', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- chevrolet express cargo
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Express', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- buick regal tourx
INSERT INTO MakeModelClassifications 
values('Buick', 'Regal TourX', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '01/01/2020 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- GMC Savana Cargo G2500 Cargo Van
INSERT INTO MakeModelClassifications 
values('GMC', 'Savana Cargo', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2020 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- volvo xc60
INSERT INTO MakeModelClassifications 
values('Volvo', 'XC60', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '11/01/2018 08:00:00', NULL, 'MfgOriginTyp_Import')

-- subaru ascent
INSERT INTO MakeModelClassifications 
values('Subaru', 'Ascent', 'VehicleSegment_Midsize','VehicleType_Crossover',false, false, false, '11/01/2018 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Ford T150 Vans
INSERT INTO MakeModelClassifications 
values('Ford', 'T150 Vans', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Cadillac XT6
INSERT INTO MakeModelClassifications 
values('Cadillac', 'XT6', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- silverado 1500 LD
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Silverado 1500 LD', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- RAM 2500 Promaster
INSERT INTO MakeModelClassifications 
values('Ram', '2500 ProMaster Vans', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Jeep Gladiator
INSERT INTO MakeModelClassifications 
values('Jeep', 'Gladiator', 'VehicleSegment_Small','VehicleType_Pickup',false, false, false, '01/01/2020 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Lexus G460
INSERT INTO MakeModelClassifications 
values('Lexus', 'G460', 'VehicleSegment_Large','VehicleType_SUV',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Lexus GX460
INSERT INTO MakeModelClassifications
values('Lexus', 'GX 460', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Import')

-- ford EcoSport
INSERT INTO MakeModelClassifications 
values('Ford', 'EcoSport', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- nissan kicks
INSERT INTO MakeModelClassifications 
values('Nissan', 'Kicks', 'VehicleSegment_Compact','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Toyota C-HR
INSERT INTO MakeModelClassifications 
values('Toyota', 'C-HR', 'VehicleSegment_Compact','VehicleType_SUV',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- fiat 124 spider
INSERT INTO MakeModelClassifications 
values('Fiat', '124 Spider', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- kia telluride
INSERT INTO MakeModelClassifications
values('Kia', 'Telluride', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Import')

-- infiniti qx60
INSERT INTO MakeModelClassifications
values('Infiniti', 'QX60', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Volkswagen Tiguan
INSERT INTO MakeModelClassifications
values('Volkswagen', 'Tiguan Limited', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Hyundai Palisade
INSERT INTO MakeModelClassifications 
values('Hyundai', 'Palisade', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2021 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Hyundai Kona
INSERT INTO MakeModelClassifications 
values('Hyundai', 'Kona', 'VehicleSegment_Compact','VehicleType_SUV',false, false, false, '09/01/2020 08:00:00', NULL, 'MfgOriginTyp_Import')

-- mini cooper countryman
INSERT INTO MakeModelClassifications 
values('Mercedes-Benz', 'C-Class', 'VehicleSegment_Midsize','VehicleType_Car',true, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- mercedes-benz gla
INSERT INTO MakeModelClassifications 
values('Mercedes-Benz', 'GLA', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Silverado 1500 LTD', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- mercedes-benz glb
INSERT INTO MakeModelClassifications 
values('Mercedes-Benz', 'GLB', 'VehicleSegment_Medium','VehicleType_SUV',false, false, false, '01/01/2021 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Volkswagen atlas
INSERT INTO MakeModelClassifications 
values('Volkswagen', 'Atlas', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2021 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Hyundai Santa Fe XL
INSERT INTO MakeModelClassifications 
values('Hyundai', 'Santa Fe XL', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2021 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Kia Forge Koup
INSERT INTO MakeModelClassifications 
values('Kia', 'Forte Koup', 'VehicleSegment_Small','VehicleType_Car',true, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Lincoln MKC
INSERT INTO MakeModelClassifications 
values('Lincoln', 'MKC', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Jeep Grand Cherokee L
INSERT INTO MakeModelClassifications 
values('Jeep', 'Grand Cherokee L', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2021 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- GMC Sierra 1500 LTD
INSERT INTO MakeModelClassifications 
values('GMC', 'Sierra 1500 LTD', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '06/01/2022 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Ford Bronco Sport
INSERT INTO MakeModelClassifications 
values('Ford', 'Bronco Sport', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Ford T350 Vans
INSERT INTO MakeModelClassifications 
values('Ford', 'T350 Vans', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- BMW X2
INSERT INTO MakeModelClassifications 
values('BMW', 'X2', 'VehicleSegment_Compact','VehicleType_SUV',true, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Tesla Model Y
INSERT INTO MakeModelClassifications 
values('Tesla', 'Model Y', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '09/01/2022 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Tesla Model S
INSERT INTO MakeModelClassifications 
values('Tesla', 'Model S', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- audi s3
INSERT INTO MakeModelClassifications 
values('Audi', 'S3', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- lexus HS 250h
INSERT INTO MakeModelClassifications 
values('Lexus', 'HS 250h', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Import')

-- pontiac G3
INSERT INTO MakeModelClassifications 
values('Pontiac', 'G3', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Ram Promaster Cutaway 3500
INSERT INTO MakeModelClassifications 
values('Ram', 'Promaster Cutaway', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Mercedes-Benz  GL 
INSERT INTO MakeModelClassifications 
values('Mercedes-Benz', 'GLC', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Lexus RC 300
INSERT INTO MakeModelClassifications
values('Lexus', 'RC 300', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Jeep Wagoneer
INSERT INTO MakeModelClassifications 
values('Jeep', 'Wagoneer', 'VehicleSegment_Large','VehicleType_SUV',false, false, false, '01/01/2021 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- LExus NX200t
INSERT INTO MakeModelClassifications 
values('Lexus', 'NX 200t', 'VehicleSegment_Compact','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Infiniti QX70
INSERT INTO MakeModelClassifications 
values('Infiniti', 'QX70', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Jeep All-New Wranger
INSERT INTO MakeModelClassifications 
values('Jeep', 'All-New Wranger', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Kia K5
INSERT INTO MakeModelClassifications 
values('Kia', 'K5', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Import')

-- ford f250 s/d
INSERT INTO MakeModelClassifications 
values('Ford', 'F250 S/D', 'VehicleSegment_Extra_Large','VehicleType_Pickup',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- volswagen alltrack
INSERT INTO MakeModelClassifications 
values('Volkswagen', 'Golf Alltrack', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '08/04/2023 08:00:00', NULL, 'MfgOriginTyp_Import')

-- ford f250 s/d
INSERT INTO MakeModelClassifications 
values('Ford', 'F350 S/D', 'VehicleSegment_Extra_Large','VehicleType_Pickup',false, false, false, '09/01/2017 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- chevrolet cutaway van
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Cutaway Van', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2019 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- toyota bZ4X
INSERT INTO MakeModelClassifications 
values('Toyota', 'bZ4X', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- RAV4 Prime
INSERT INTO MakeModelClassifications 
values('Toyota', 'RAV4 Prime', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- cad DFT4
INSERT INTO MakeModelClassifications 
values('Cadillac', 'CT4', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Toyota Corolla Cross
INSERT INTO MakeModelClassifications 
values('Toyota', 'Corolla Cross', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Jeep New Grand Cherokee 
INSERT INTO MakeModelClassifications 
values('Jeep', 'New Grand Cherokee', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2021 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- MAzda CX-30
INSERT INTO MakeModelClassifications 
values('Mazda', 'CX-30', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2021 08:00:00', NULL, 'MfgOriginTyp_Domestic')


SELECT * FROM VehicleItems WHERE vin = '1C4RJHBG2N8619558'

SELECT * FROM MakeModelClassifications WHERE make = 'ford' AND model LIKE 'f%'

SELECT * FROM MakeModelClassifications WHERE vehiclesegment LIKE '%Extra%'

SELECT * FROM MakeModelClassifications WHERE model LIKE '%grand cherokee%'

UPDATE MakeModelClassifications
SET model = 'New Grand Cherokee'
WHERE model IN ('New Grand Cherokee L')

SELECT * FROM MakeModelClassifications 
WHERE model IN ('F450SD','F350SD','F350','F250SD','F250HD','F250','F550SD','F650SD','F650')


SELECT * FROM VehicleItems WHERE vin = '1FBZX2ZG9HKB07082'

SELECT * FROM VehicleItems WHERE vin = 'KM8K3CA56LU432676'

UPDATE MakeModelClassifications
SET vehiclesegment = 'VehicleSegment_Midsize'
-- SELECT * FROM MakeModelClassifications
WHERE model = 'GLB'


SELECT * FROM MakeModelClassifications WHERE model = 'Silverado 1500'
SELECT * FROM VehicleItems WHERE vin = '1C4RJKBG6M8141799'

select *  
FROM MakeModelClassifications 
WHERE model = 'GX460'

select * FROM VehicleItems WHERE vin = 'JTJBM7FX4H5165796'


select *  
FROM VehicleItems 
WHERE vin = '3C7WRNFL5EG225888'

select *  
FROM MakeModelClassifications 
WHERE model = 'compass'
ORDER BY model

select vehicletype,vehiclesegment, COUNT(*)
FROM MakeModelClassifications 
GROUP BY vehicletype,vehiclesegment
ORDER BY vehicletype,vehiclesegment

UPDATE VehicleItems
SET model = 'S Class'
-- SELECT * FROM VehicleItems 
WHERE vin = 'WDDUG8FB6GA241155';

SELECT * FROM VehicleItems 
WHERE vin = '1N6BF0KM2FN800766' 

VehicleSegment_Extra_Large



SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = '9422cd31-e74c-43c8-8dae-66549faff5ee'

-- inspection pending NOT IN MakeModelClassifications 
SELECT c.make, c.model, d.* FROM VehicleInventoryItemStatuses a 
inner join VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN VehicleItems c on b.VehicleItemID = c.VehicleItemID  
LEFT JOIN MakeModelClassifications d on c.make = d.make AND c.model = d.model
WHERE status = 'RMFlagIP_InspectionPending' AND a.thruts IS NULL
AND NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE status = 'RMFlagTNA_TradeNotAvailable'
    AND thruts IS NULL
    AND VehicleInventoryItemID = a.VehicleInventoryItemID)
AND NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE status = 'RMFlagPIT_PurchaseInTransit'
    AND thruts IS NULL
    AND VehicleInventoryItemID = a.VehicleInventoryItemID)