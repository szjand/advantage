-- sales status?
-- leave out WSB
alter PROCEDURE GetFreshInventory (
  MarketID Char(38),
  LocationID Char(38),
  DaysBack Integer,
  PictureAvailable Logical OUTPUT,
  PictureID integer OUTPUT, 
  Stocknumber CIChar(20) OUTPUT,
  Price Integer OUTPUT,
  YearModel CIChar(4) OUTPUT,
  Make CIChar(60) OUTPUT, 
  Model CIChar(60) OUTPUT, 
  Trim CIChar(60) OUTPUT, 
  Engine CIChar(60) OUTPUT, 
  VehicleOptions CIChar(400) OUTPUT, 
  Color CIChar(60) OUTPUT, 
  InteriorColor CIChar(60) OUTPUT, 
  Miles Integer OUTPUT,
  VehicleInventoryItemID Char(38) OUTPUT )

BEGIN
 
  
/*
EXECUTE PROCEDURE GetFreshInventory('A4847DFC-E00E-42E7-89EE-CD4368445A82', '', 14);

*/
DECLARE @LocationID string;
DECLARE @MarketID string;
DECLARE @DaysBack integer;

@LocationID = (SELECT LocationID FROM __input);
@MarketID = (SELECT MarketID FROM __input);
@DaysBack = (SELECT DaysBack FROM __input);

INSERT INTO __output
  SELECT
    (SELECT 
      CASE 
        WHEN (
          SELECT viip.Picture 
          FROM VIIPictures viip  
          INNER JOIN VIIPictureSequences viips 
          ON viips.SequenceNo = viip.SequenceNo 
            AND viips.RepresentativePicture = True 
    	      AND viips.SetType = (
              SELECT viipu.SetType 
              FROM VIIPictureUploads viipu 
              WHERE viipu.VehicleInventoryItemID = viix.VehicleInventoryItemID)  
          WHERE viip.VehicleInventoryItemID = viix.VehicleInventoryItemID) is NULL  
        THEN False  
        ELSE True  
      END FROM system.iota) AS PictureAvailable, 
    (SELECT viip.SequenceNo
      FROM VIIPictures viip
      INNER JOIN VIIPictureSequences viips
      ON viips.SequenceNo = viip.SequenceNo
      AND viips.RepresentativePicture = True
      AND viips.SetType = (
        SELECT viipu.SetType
        FROM VIIPictureUploads viipu
        WHERE viipu.VehicleInventoryItemID = viix.VehicleInventoryItemID)
        WHERE viip.VehicleInventoryItemID = viix.VehicleInventoryItemID) AS PictureID,  
      viix.stocknumber, vpdx.amount AS Price, vix.yearmodel, vix.make, vix.model, vix.TRIM,
      vix.engine, vio.VehicleOptions, vix.exteriorcolor, vix.interiorcolor,
      vim.Value AS Miles, viix.VehicleInventoryItemID
  FROM VehicleInventoryItems viix
  INNER JOIN VehicleWalks vwx ON viix.VehicleInventoryItemID = vwx.VehicleInventoryItemID 
    AND CAST(vwx.VehicleWalkTS AS sql_date) >= curdate() - @DaysBack
  INNER JOIN VehiclePricings vpx ON viix.VehicleInventoryItemID = vpx.VehicleInventoryItemID 
    AND vpx.VehiclePricingTS = (
      SELECT MAX(VehiclePricingTS)
      FROM VehiclePricings
      WHERE VehicleInventoryItemID  = viix.VehicleInventoryItemID)
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'    
  INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
  LEFT JOIN VehicleItemOptions vio ON vix.VehicleItemID = vio.VehicleItemID 
  LEFT JOIN VehicleItemMileages vim ON vix.VehicleItemID = vim.VehicleItemID 
    AND vim.VehicleItemMileageTS = (
      SELECT MAX(VehicleItemMileageTS)
      FROM VehicleItemMileages
      WHERE VehicleItemID = vix.VehicleItemID) 
  WHERE viix.ThruTS IS NULL
  AND 
    CASE 
      WHEN @LocationID <> '' THEN viix.OwningLocationID = @LocationID
      ELSE viix.OwningLocationID IN (
        SELECT PartyID2
        FROM PartyRelationships
        WHERE PartyID1 = @MarketID
        AND typ = 'PartyRelationship_MarketLocations'
        AND ThruTS IS NULL)
    END;
  
END;
  
