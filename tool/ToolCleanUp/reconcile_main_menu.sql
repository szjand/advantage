
/*
ben asks:
  should available + pulled + raw + walk + inspection + trade buffer + IN transit
	+ sales buffer + ws buffer = total
morning of 4/19
  total = 388, SUM of statuses = 394
8 vehicles IN sales buffer also showing up AS available




*/
--    SELECT stocknumber
--    FROM VehicleInventoryItems vii
--    WHERE ThruTS IS NULL
	
--SELECT a.VehicleInventoryItemID, stocknumber, b.*
--FROM VehicleInventoryItems a
--JOIN (
SELECT VehicleInventoryItemID FROM (		
    SELECT 'in transit' AS status,VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagPIT_PurchaseInTransit'
    AND ThruTS IS NULL		
		union
    SELECT 'trade buffer' AS status,VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagTNA_TradeNotAvailable'
    AND ThruTS IS NULL		
		UNION
    SELECT 'insp' AS status,VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagIP_InspectionPending'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND (status = 'RMFlagTNA_TradeNotAvailable' OR status = 'RMFlagPIT_PurchaseInTransit') 
      AND ThruTS IS NULL)		
UNION
    SELECT 'walk' AS status,VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagWP_WalkPending'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND status = 'RMFlagIP_InspectionPending'
      AND ThruTS IS NULL)
UNION
    SELECT 'raw' AS status,VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRMP_RawMaterialsPool'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status = 'RMFlagRB_ReconBuffer'
      AND ThruTS IS null)    
UNION
    SELECT 'pricing buffer' AS status,VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagPB_PricingBuffer'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status = 'RMFlagSB_SalesBuffer'
      AND ThruTS IS NULL)			
UNION
    SELECT 'avail' AS status,VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagAV_Available' 
    AND ThruTS IS NULL		
		AND NOT EXISTS (  -- raw/avail fix
		  SELECT 1
			FROM VehicleInventoryItemStatuses 
			WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
			  AND status = 'RMFlagSB_SalesBuffer'
				AND thruts IS NULL)
	
UNION
    SELECT 'sales buffer' AS status,VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagSB_SalesBuffer'
    AND ThruTS IS NULL		
/*		
UNION
    SELECT 'ws buffer' AS status,VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagWSB_WholesaleBuffer'
    AND ThruTS IS NULL			
*/			
			
			
			
			
			
			
			
			
						
) x
GROUP BY VehicleInventoryItemID
HAVING COUNT(*) > 1 
-- ) b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 		

/*
SELECT * FROM VehicleInventoryItems 
WHERE VehicleInventoryItemID IN (
'03f2e45c-1ca2-435d-b08d-4d1eaf6610f3',
'1381f6a3-0d17-4931-a0d8-c0958f49fafd',
'1c7e1c03-86b4-40e2-ab08-218e3df91ad9',
'2d5ad2d9-6338-4edc-b16b-3dca3da00ed8',
'52e62cc5-594a-4ad6-8052-492788c91f43',
'8351c4e8-9ed3-4db7-9850-404b75c11ea7',
'baf684a4-6250-4de2-87c0-13263124dc70',
'eeefd257-37f8-4770-a0c0-eec0808aff55',
'f0479459-eab1-49b9-8a24-61904a5f23e3',
'f4f4ef5e-2298-4e6f-b8be-e1e9923cbf4c')
*/

SELECT * FROM VehicleInventoryItems WHERE VehicleInventoryItemID IN ('1381f6a3-0d17-4931-a0d8-c0958f49fafd','1c7e1c03-86b4-40e2-ab08-218e3df91ad9')