-- 1/2/13
-- intramarket wholesaled c4363A to ry1, should NOT have
-- 3/22/18 32391A -> H11149G
-- first, the newly created ry1 VehicleInventoryItems 
VehicleInventoryItemID '40cea3e3-771e-49eb-8c2d-204a66130c4c'
SELECT VehicleItemID FROM VehicleInventoryItems WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c'

SELECT * FROM VehicleItemstatuses WHERE VehicleItemID = '10812846-489a-f741-8570-c51b9f2c9e2d'

DELETE 
FROM VehicleItemstatuses
WHERE VehicleItemID = '10812846-489a-f741-8570-c51b9f2c9e2d'
  AND category = 'Inventory'
  AND cast(fromts AS sql_date) = '11/13/2017'
  
UPDATE VehicleItemstatuses
SET thruts = NULL 
WHERE VehicleItemID = '10812846-489a-f741-8570-c51b9f2c9e2d'
  AND category = 'Inventory';
  
DELETE 
FROM vehiclepricingdetails
WHERE vehiclepricingid = (
SELECT VehiclePricingID 
FROM vehiclepricings   
WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c');
DELETE
FROM vehiclepricings   
WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c';  
DELETE
FROM VehicleReconItems 
WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c';
DELETE
FROM vehicleAcquisitions   
WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c';
DELETE
FROM VehicleInventoryItemStatuses  
WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c';
DELETE 
FROM SelectedReconPackages    
WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c';
DELETE
FROM vehicleappeals   
WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c';
DELETE
FROM VehicleInventoryItemNotes    
WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c';
DELETE 
FROM VehicleInventoryItems   
WHERE VehicleInventoryItemID = '40cea3e3-771e-49eb-8c2d-204a66130c4c';

-- now undo the ry3 ws
-- use sp.sub_wholesalevehicle
c4863a
VehicleInventoryItemID 'c2aa61d1-d830-4e0c-9efd-e4c61c810b57'

32391A 
VehicleInventoryItemID '5b59456b-7df1-4f23-a773-929057416324'
DELETE 
FROM vehiclesales
WHERE VehicleInventoryItemID = '5b59456b-7df1-4f23-a773-929057416324';

SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = '5b59456b-7df1-4f23-a773-929057416324'

DELETE FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '5b59456b-7df1-4f23-a773-929057416324'
  AND CAST(fromTS AS sql_date) = '03/14/2018';
UPDATE VehicleInventoryItemStatuses
SET thruts = NULL
WHERE VehicleInventoryItemID = '5b59456b-7df1-4f23-a773-929057416324' 
  AND CAST(thruTS AS sql_date) = '03/14/2018';

DELETE FROM VehicleItemPhysicalLocations
WHERE VehicleInventoryItemID = '5b59456b-7df1-4f23-a773-929057416324'
  AND CAST(fromTS AS sql_date) = '03/14/2018'
  
UPDATE VehicleItemPhysicalLocations
SET thruts = NULL
WHERE VehicleInventoryItemID = '5b59456b-7df1-4f23-a773-929057416324'
  AND CAST(thruTS AS sql_date) = '03/14/2018';
  
update VehicleInventoryItems  
SET thruts = null  
WHERE VehicleInventoryItemID = '5b59456b-7df1-4f23-a773-929057416324';  





