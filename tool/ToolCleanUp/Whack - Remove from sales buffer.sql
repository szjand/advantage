
DECLARE @NowTS TimeStamp;
DECLARE @VehicleInventoryItemID String;
DECLARE @Notes String;
DECLARE @UserID string;

@NowTS = '07/08/2011 08:00:00';
@VehicleInventoryItemID = '81e7e565-1320-4645-8597-73ffb3325697';
@Notes = '';
@UserID = (SELECT partyid FROM people WHERE fullname = 'jon andrews');


  UPDATE VehicleInventoryItemSalePending
    SET [ThruTS] = @NowTS,
        [RemovedByPartyID] = @UserID
    WHERE [ThruTS] IS NULL 
    AND [VehicleInventoryItemID] = @VehicleInventoryItemID;
      
  EXECUTE PROCEDURE CloseVehicleInventoryItemStatusByCategory(@VehicleInventoryItemID, 
                                                              'RMFlagSB', 
                                                              @NowTS);       
           
   
  IF (@Notes IS NOT NULL) AND (@Notes <> '') then   
    //Write the Repricing note
    EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubCategory(@VehicleInventoryItemID, 
                                                                        @UserID,
                                                                        'VehicleInventoryItemSalePending',
                                                                        @VehicleInventoryItemID,
                                                                        'RMFlagSB',
                                                                        'RMFlagSB_SalesBufferRemoved',
                                                                        @NowTS,
                                                                        @Notes);
  END IF;   


   
   
   
   
   
   
