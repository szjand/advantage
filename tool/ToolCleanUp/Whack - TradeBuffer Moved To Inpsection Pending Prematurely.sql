/*
vehicle was tradebuffer
updated to inspection pending (tna received)
should NOT have been
needs to be restored to trade buffer

SELECT * FROM tnavehicles where VehicleInventoryItemID = 'fad3d771-788f-4f48-aaf9-181bf3f989f1'

SELECT * FROM VehicleInventoryItemStatuses  where VehicleInventoryItemID = 'fad3d771-788f-4f48-aaf9-181bf3f989f1'

SELECT * FROM VehicleInventoryItemNotes  where VehicleInventoryItemID = 'fad3d771-788f-4f48-aaf9-181bf3f989f1'

SELECT * FROM VehicleItemPhysicalLocations where VehicleInventoryItemID = 'fad3d771-788f-4f48-aaf9-181bf3f989f1';

This only applies IF the vehicle IS still IN inspection pending
need to INSERT some assert typ tests to verify the proper condition
-- ALL VehicleInventoryItemStatuses status records should have a NULL ThruTS except TradeBuffer
*/
DECLARE @VehicleInventoryItemID string;
DECLARE @ExpectedDate string;
DECLARE @Note string;
DECLARE @NowTS timestamp;
DECLARE @UserID string;

@VehicleInventoryItemID = '3b13e358-61fa-417b-9422-6501148dd932';
@ExpectedDate = '01/10/2012';
@Note = '';
@NowTS = (SELECT now() FROM system.iota);
@UserID = (SELECT partyid FROM users WHERE username = 'jon');

BEGIN TRANSACTION;
TRY

  UPDATE tnaVehicles
  SET ReceivedByID = NULL
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  IF @ExpectedDate <> '' THEN
    UPDATE tnaVehicles
    SET ExpectedDate = cast(@ExpectedDate AS sql_date)
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID; 
  END IF; 
  
  UPDATE VehicleInventoryItemStatuses
  SET ThruTS = NULL
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND category = 'RMFlagTNA'
  AND ThruTS IS NOT NULL;
  
  IF @Note <> '' THEN
    EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubCategory(
      @VehicleInventoryItemID,
      @UserID,
      'VehicleInventoryItems',
      @VehicleInventoryItemID,
      'TNA/PIT',
      'TNA/PIT_Status',
      @NowTS,
      @Note);  
  END IF;  
  
  DELETE
  FROM VehicleItemPhysicalLocations
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID; 

  
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY      