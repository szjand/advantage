-- 15681XX pulled BY wilke ON 3/19
-- vehicle ws to ry3 12/28/11
-- retailed at ry3 ON 12/29/11 AS C4745x

-- 14802XX  viid: 'b75210e6-3640-42f8-8663-7bc5f924d83b'
-- ws to ry3 11/1 AS c4697x
-- retail at ry3 ON 11/1

-- 14802XX  no statuses after 8/29
SELECT *
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b'
AND (
  CAST(fromts AS sql_date) = '03/19/2012'
  OR
  CAST(thruts AS sql_date) = '03/19/2012')

delete  
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = '8e4ad599-a94b-4d1f-a9a1-11a43068103c'
AND CAST(fromts AS sql_date) = '03/19/2012' 

UPDATE VehicleInventoryItemStatuses
SET ThruTS = NULL
WHERE VehicleInventoryItemID = '8e4ad599-a94b-4d1f-a9a1-11a43068103c'
AND CAST(thruts AS sql_date) = '03/19/2012' 

-- priced BY wilkie ON 2/15
-- 14802XX priced 7 times since 10/18
SELECT *
FROM VehiclePricings 
WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b'
  AND CAST(VehiclePricingTS AS sql_Date) > '11/01/2011'


SELECT * 
FROM system.storedprocedures
WHERE name LIKE '%price%'
-- PriceVehicleInventoryItem
--1. INSERT price INTO VehiclePricing
--2. INSERT BestPrice INTO VehiclePricingDetails
--3. INSERT Invoice INTO VehiclePricingDetails
--4. INSERT Manheim INTO VehicleThirdPartyValues
--5. INSERT Appeal INTO VehicleAppeals
--6. INSERT Notes INTO VehicleInventoryItemNotes 

DELETE 
-- SELECT *
FROM VehiclePricingDetails
WHERE VehiclePricingID in (
  SELECT VehiclePricingID 
  FROM VehiclePricings 
  WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b'
    AND CAST(VehiclePricingTS AS sql_Date) > '11/01/2011');
    
DELETE 
-- SELECT *
FROM VehiclePricings 
WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b'
  AND CAST(VehiclePricingTS AS sql_date) > '11/01/2011';

    
DELETE 
-- SELECT *
FROM VehicleThirdPartyValues 
WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b'
  AND cast(VehicleThirdPartyValueTS AS sql_date) > '11/01/2011';
  
DELETE 
-- SELECT *
FROM VehicleAppeals
WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b'
  AND cast(VehicleAppealTS AS sql_date) > '11/01/2011';
  
  
DELETE 
-- SELECT *
FROM VehicleInventoryItemNotes 
WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b' 
  AND cast(notesTS AS sql_date) > '11/01/2011';
    
-- Locations

DELETE 
-- SELECT *
FROM VehicleItemPhysicalLocations
WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b' 
  AND cast(FromTS AS sql_date)  > '11/01/2011';
  
UPDATE VehicleItemPhysicalLocations
SET ThruTS = NULL
WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b' 
  AND ThruTS = (
    SELECT MAX(thruTS)
    FROM VehicleItemPhysicalLocations
    WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b');
    
-- 14802XX  beaucoup black book values ?  
delete
-- SELECT *
FROM BlackBookValues
WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleInventoryItems 
    WHERE VehicleInventoryItemID = 'b75210e6-3640-42f8-8663-7bc5f924d83b')
  AND CAST(blackbookvalueTS AS sql_date) > '11/01/2011' 
-- ok, tbut there IS still one FROM 3/21/12 -- wtf, duh, BY default it shows todays value
    
-- now run 
-- WhackWholesaleWithinMarket    
-- WhackBookPending
-- WhackSellAndDeliver
 
    