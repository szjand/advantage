/*
SELECT * FROM VehicleEvaluations WHERE VehicleEvaluationID = '145feda5-e73b-4121-a181-eaf56b167234'
select  typ, count(*) FROM VehicleAcquisitions GROUP BY typ
SELECT * FROM LocationPhysicalVehicleLocations

2000 Chevrolet Silverado 1500 - Y1299121
2007 Buick LaCrosse - 71207702

SELECT * 
FROM vehicleitemstatuses
WHERE VehicleItemID = (
select VehicleItemID FROM VehicleEvaluations WHERE VehicleEvaluationID =  '26af5ad0-e271-47f8-9c36-2d6106ac7b58')

*/
 
DECLARE @VehicleEvaluationID String;
DECLARE @NowTS TimeStamp;
DECLARE @VehicleItemID String;
DECLARE @VehicleInventoryItemID String;
DECLARE @BasisTable String, @TableKey String,
      @StockNumber String, @VIN String, @BookerID String,
      @TnaStatus String, @AmountPaid Integer, 
      @LocationID String, @Source String;
DECLARE @LocationDetail String;
DECLARE @DataCollectionNote String;
DECLARE @AdjustACVNote String;
DECLARE @DemandOverrideNote String;
DECLARE @ReconNote String;
DECLARE @SalesConsultantID string;
DECLARE @EvaluatorID string;
DECLARE @PhysicalVehicleLocationId String; 
DECLARE @UserID string;

--DECLARE @ExpectedDate date;
--DECLARE @TradeBufferReason string;
-- DECLARE @Cur CURSOR AS SELECT * FROM __input;
-- DECLARE @Transportation Integer, @AuctionFee Integer;
-- DECLARE @AuctionID String, @BuyerID String;
-- DECLARE @StatusType String; //@StatusType will either be TNAorPIT OR blank 


/************ watch out for needing to change locationid **********************/

@VehicleEvaluationID = 'ac68b245-7556-450a-ae32-64b45beda002'; 
@NowTS = '06/13/2012 07:54:00';
@StockNumber = '16447A'; 
@UserID = (SELECT partyid FROM users WHERE username = 'jon'); -- Booker
@Source = 'VehicleAcquisition_Trade';
@PhysicalVehicleLocationId = 'c6e55ee7-0b6e-9c48-93e8-84e2dc7e5e6c'; -- M
--@PhysicalVehicleLocationID = '9bbc3dd1-81e9-0648-9e8a-02df83a7321d';  -- Honda Front Lot
@LocationDetail = NULL;

@VehicleItemID = (SELECT VehicleItemID FROM VehicleEvaluations WHERE VehicleEvaluationID = @VehicleEvaluationID);
@VehicleInventoryItemID = (SELECT newidstring(d) FROM system.iota);
@VIN = (SELECT vin FROM vehicleitems WHERE VehicleItemID = @VehicleItemID);
@LocationID = (SELECT LocationID FROM VehicleEvaluations WHERE VehicleEvaluationID = @VehicleEvaluationID);


  


@SalesConsultantID = (
  SELECT SalesConsultantID
  FROM VehicleEvaluations
  WHERE VehicleEvaluationID = @VehicleEvaluationID);
@EvaluatorID = (
  SELECT VehicleEvaluatorID
  FROM VehicleEvaluations
  WHERE VehicleEvaluationID = @VehicleEvaluationID);
--  @ExpectedDate = cast(@Cur.ExpectedDate AS sql_date);
--  @TradeBufferReason = TRIM(@Cur.TradeBufferReason);
--  @TnaStatus = 'Expected: ' + cast(@ExpectedDate AS sql_char) + '.  ' + Trim(@Cur.TnaStatus);  
--  @AmountPaid = @Cur.AmountPaid;
--  @AuctionID = Trim(@Cur.AuctionID);
--  @Transportation = @Cur.Transportation;
--  @AuctionFee = @Cur.AuctionFee;
--  @BuyerID = Trim(@Cur.BuyerID); 
--@StatusType = iif(@Status = 'RMFlagTNA_TradeNotAvailable' OR @Status = 'RMFlagPIT_PurchaseInTransit', 'TNAorPIT', '');
--  @Status = Trim(@Cur.Status);


  
BEGIN TRANSACTION;

TRY 

  TRY     
    //CREATE a VehicleInventoryItem
    INSERT INTO VehicleInventoryItems (VehicleInventoryItemID, TableKey, 
      BasisTable, StockNumber, FromTS, VehicleItemID, BookerID, LocationID, OwningLocationID)
    VALUES (@VehicleInventoryItemID, @VehicleEvaluationID, 'VehicleEvaluations', @StockNumber, @NowTS, 
      (SELECT VehicleItemID FROM VehicleEvaluations WHERE VehicleEvaluationID = @VehicleEvaluationID),
      @UserID, @LocationID, @LocationID);
  CATCH ALL 
    Raise BookingException(2560, 'This vehicle has already been booked. You may have accidentally clicked the Book It button twice!');
  END;
  
 //INSERT the RawMaterials status
  TRY 
    EXECUTE PROCEDURE SetVehicleInventoryItemsStatusFlag(@VehicleInventoryItemID, @UserID,
      'RawMaterials', 'RawMaterials_RawMaterials', @NowTS, 'VehicleInventoryItems', 
      @VehicleInventoryItemID);  
  CATCH ALL
    Raise BookingException(2559, 'VehicleInventoryItemStatus of RawMaterials');  
  END;
      
  TRY   
    //SET currentstatus AND VehicleInventoryItemID field of VehicleEvaluation 
    UPDATE VehicleEvaluations
      SET [CurrentStatus] = 'VehicleEvaluation_Booked',
        [VehicleInventoryItemID] = @VehicleInventoryItemID
      WHERE [VehicleEvaluationID] = @VehicleEvaluationID;
  CATCH ALL 
    Raise BookingException(2561, 'Booking - UPDATE VehicleEvaluations : ' + __errText);
  END;
      
  TRY   
    //SET VehicleInventoryItemID field of VehicleAppeals
    UPDATE VehicleAppeals
    SET [VehicleInventoryItemID] = @VehicleInventoryItemID
    WHERE [TableKey] = @VehicleEvaluationID;
  CATCH ALL 
    Raise BookingException(2561, 'Booking - UPDATE VehicleAppeals : ' + __errText);
  END;
  
  TRY     
    //CLOSE out current VehicleItemStatuses
    //First, CLOSE out current status record for this vehicle evaluataion
    UPDATE VehicleItemStatuses
      SET [ThruTS] = @NowTS
      WHERE [ThruTS] IS NULL
        AND [VehicleItemID] = @VehicleItemID
        AND [Category] = 'VehicleEvaluation';
  CATCH ALL 
    Raise BookingException(2562, 'Booking - UPDATE VehicleItemStatuses : ' + __errText);
  END;

  //Increment the timestamp
  @NowTS = (SELECT TIMESTAMPADD(SQL_TSI_SECOND, 1, @NowTS) FROM system.iota);
  
  TRY      
    //CREATE a VehicleItemStatuses value of Booked
    INSERT into VehicleItemStatuses 
      (VehicleItemID, Status, Category, FromTS, ThruTS, BasisTable, TableKey, UserID)
      VALUES 
      (@VehicleItemID, 'VehicleEvaluation_Booked', 'VehicleEvaluation', @NowTS, @NowTS,  
      'VehicleEvaluations', @VehicleEvaluationID, @UserID);
  CATCH ALL 
    Raise BookingException(2563, 'Booking - INSERT into VehicleItemStatuses : ' + __errText);
  END;
     
  TRY   
    //CREATE a VehicleItemStatuses record for VehicleInventoryItem
    //TODO: This section contains a new value. Inventory category has only one active value,
    //which IS Inventory_Inventory. WHEN the vehicle IS sold, Inventory_Inventory IS made
    //inactive, AND Inventory_Sold IS added AS an inactive status 
    //(FromTS AND ThruTS containing the same timestamp)
    INSERT INTO VehicleItemStatuses 
      (VehicleItemID, Status, Category, FromTS, BasisTable, TableKey, UserID)
      VALUES 
      (@VehicleItemID, 'Inventory_Inventory',   
         'Inventory', @NowTS, 
         'VehicleInventoryItems', @VehicleInventoryItemID, @UserID);
  CATCH ALL 
    Raise BookingException(2564, 'Booking - INSERT INTO VehicleItemStatuses : ' + __errText);
  END;
/*           
  IF @StatusType = 'TNAorPIT' THEN
    //IF TNA, INSERT a record INTO TNAVehicles
--      INSERT INTO TnaVehicles (VehicleInventoryItemID, Notes, ExpectedDate, TradeBufferReason)
--        VALUES (@VehicleInventoryItemID, @TnaStatus, @ExpectedDate, @TradeBufferReason);
--    CATCH ALL 
--      Raise BookingException(2565, 'Booking - INSERT INTO TnaVehicles : ' + __errText);
--    END TRY;
    IF @Status = 'RMFlagTNA_TradeNotAvailable' THEN
      TRY 
        EXECUTE PROCEDURE SetVehicleInventoryItemsStatusFlag(@VehicleInventoryItemID, @BookerID,
        'RMFlagTNA', @Status, @NowTS, 'VehicleEvaluations', @VehicleEvaluationID); 
        INSERT INTO TnaVehicles (VehicleInventoryItemID, Notes, ExpectedDate, TradeBufferReason)
          VALUES (@VehicleInventoryItemID, @TnaStatus, @ExpectedDate, @TradeBufferReason);        
      CATCH ALL
        RAISE BookingException(25651, 'Booking - TNA Status : ' + __errText);
      END TRY;
    END IF;
    IF @Status = 'RMFlagPIT_PurchaseInTransit' THEN
      TRY 
        EXECUTE PROCEDURE SetVehicleInventoryItemsStatusFlag(@VehicleInventoryItemID, @BookerID,
        'RMFlagPIT', @Status, @NowTS, 'VehicleEvaluations', @VehicleEvaluationID); 
        INSERT INTO TnaVehicles (VehicleInventoryItemID, Notes)
          VALUES (@VehicleInventoryItemID, @TnaStatus);        
      CATCH ALL
        RAISE BookingException(25651, 'Booking - PIT Status : ' + __errText);
      END TRY;
    END IF;
  END IF;
*/  
  //INSERT the basic flags
  EXECUTE PROCEDURE SetVehicleInventoryItemsStatusFlag(@VehicleInventoryItemID, @UserID,
                                                       'RMFlagIP',
                                                       'RMFlagIP_InspectionPending', 
                                                       @NowTS, 
                                                       'VehicleEvaluations', 
                                                       @VehicleEvaluationID);
    
  EXECUTE PROCEDURE SetVehicleInventoryItemsStatusFlag(@VehicleInventoryItemID, @UserID,
                                                       'RMFlagWP',
                                                       'RMFlagWP_WalkPending', 
                                                       @NowTS, 
                                                       'VehicleEvaluations', 
                                                       @VehicleEvaluationID);

/*    
  TRY     
    //CREATE a VehicleCostComponents entry for VehicleCostComponent_PurchaseAmount
    // 
    INSERT INTO VehicleCostComponents
      (Typ, VehicleInventoryItemID, VehicleCostComponentTS, Amount)
      VALUES
      ('VehicleCostComponent_PurchaseAmount', @VehicleInventoryItemID, @NowTS, @AmountPaid);
  CATCH ALL 
    Raise BookingException(2566, 'Booking - INSERT INTO VehicleCostComponents (VehicleCostComponent_PurchaseAmount) : ' + __errText);
  END;
   
  //CREATE a VehicleCostComponents entry for VehicleCostComponent_Transportation
  IF @Transportation > 0 then
    TRY     
      INSERT INTO VehicleCostComponents
        (Typ, VehicleInventoryItemID, VehicleCostComponentTS, Amount)
        VALUES
        ('VehicleCostComponent_Transportation', @VehicleInventoryItemID, @NowTS, @Transportation);
    CATCH ALL 
      Raise BookingException(2567, 'Booking - INSERT INTO VehicleCostComponents (VehicleCostComponent_Transportation) : ' + __errText);
    END;
  END;

  //CREATE an AuctionVehicles record IF auction purchase
  IF (@AuctionID <> '') THEN 
    TRY     
      INSERT INTO AuctionVehicles
        (VehicleInventoryItemID, AuctionID, BuyerID)
        VALUES
        (@VehicleInventoryItemID, @AuctionID, @BuyerID);
    CATCH ALL 
      Raise BookingException(2568, 'Booking - INSERT INTO AuctionVehicles : ' + __errText);
    END;
  END;


  //CREATE a VehicleCostComponents entry for VehicleCostComponent_AuctionFee
  //TODO: IS the Amount listed IN the following query really @AmountPaid OR @ActionFee
  IF @AuctionFee > 0 then
    TRY     
      INSERT INTO VehicleCostComponents
        (Typ, VehicleInventoryItemID, VehicleCostComponentTS, Amount)
        VALUES
        ('VehicleCostComponent_AuctionFee', @VehicleInventoryItemID, @NowTS, @AuctionFee);
    CATCH ALL 
      Raise BookingException(2569, 'Booking - INSERT INTO VehicleCostComponents (VehicleCostComponent_AuctionFee) : ' + __errText);
    END;
  END;
*/  
  // CREATE a VehicleAcquisitions record
  TRY
    INSERT INTO VehicleAcquisitions (VehicleInventoryItemID, Typ)
    VALUES (@VehicleInventoryItemID, @Source);
  CATCH ALL
    Raise BookingException(2580, 'Booking - INSERT INTO VehicleAcquisitions : ' + __errText);
  END TRY;
  TRY    
    //CLOSE out all VehicleItem...Info tables for this VehicleEvaluationID
 //VehicleItemInsuranceInfo
    UPDATE VehicleItemInsuranceInfo
      SET [ThruTS] = @NowTS
      WHERE [VehicleItemID] = @VehicleItemID
        AND [ThruTS] IS NULL;
  CATCH ALL 
    Raise BookingException(2570, 'Booking - UPDATE VehicleItemInsuranceInfo : ' + __errText);
  END;
  //VehicleItemLicenseInfo   
  TRY    
    UPDATE VehicleItemLicenseInfo
      SET [ThruTS] = @NowTS
      WHERE [VehicleItemID] = @VehicleItemID
        AND [ThruTS] IS NULL;
  CATCH ALL 
    Raise BookingException(2571, 'Booking - UPDATE VehicleItemLicenseInfo : ' + __errText);
  END;
  //VehicleItemOwnerInfo
  TRY  
    UPDATE VehicleItemOwnerInfo
      SET [ThruTS] = @NowTS
      WHERE [VehicleItemID] = @VehicleItemID
        AND [ThruTS] IS NULL;
  CATCH ALL 
    Raise BookingException(2572, 'Booking - UPDATE VehicleItemOwnerInfo : ' + __errText);
  END;
  //VehicleItemTitleInfo   
  TRY   
    UPDATE VehicleItemTitleInfo
      SET [ThruTS] = @NowTS
      WHERE [VehicleItemID] = @VehicleItemID
        AND [ThruTS] IS NULL;
  CATCH ALL 
    Raise BookingException(2573, 'Booking - UPDATE VehicleItemTitleInfo : ' + __errText);
  END;
    
  //UPDATE the VehicleEvaluation tables that include a VehicleInventoryItemID
  //VehicleThirdPartyComponents
  TRY     
    UPDATE VehicleThirdPartyValues
      SET [VehicleInventoryItemID] = @VehicleInventoryItemID
      WHERE [TableKey] = @VehicleEvaluationID;
  CATCH ALL 
    Raise BookingException(2574, 'Booking - UPDATE VehicleThirdPartyValues : ' + __errText);
  END;
  //VehiclePriceComponents
  TRY    
    UPDATE VehiclePriceComponents
      SET [VehicleInventoryItemID] = @VehicleInventoryItemID
      WHERE [TableKey] = @VehicleEvaluationID
      AND ThruTS IS NULL;
  CATCH ALL 
    Raise BookingException(2575, 'Booking - UPDATE VehiclePriceComponents : ' + __errText);
  END;
  //ReconNotes
  TRY    
    UPDATE ReconNotes
      SET [VehicleInventoryItemID] = @VehicleInventoryItemID
      WHERE [TableKey] = @VehicleEvaluationID;
  CATCH ALL 
    Raise BookingException(2576, 'Booking - UPDATE ReconNotes : ' + __errText);
  END;
  //SelectedReconPackages
  TRY    
    UPDATE SelectedReconPackages
      SET [VehicleInventoryItemID] = @VehicleInventoryItemID
      WHERE [TableKey] = @VehicleEvaluationID;
  CATCH ALL 
    Raise BookingException(2576, 'Booking - UPDATE SelectedReconPackages : ' + __errText);
  END;
  //ACVs
  TRY    
    UPDATE ACVs
      SET [VehicleInventoryItemID] = @VehicleInventoryItemID
      WHERE [TableKey] = @VehicleEvaluationID;
  CATCH ALL 
    Raise BookingException(2577, 'Booking - UPDATE ACVs : ' + __errText);
  END;
  // Vehicle Physical Location IF NOT TNA
--  IF @StatusType <> 'TNAorPIT' THEN
    TRY
    INSERT INTO VehicleItemPhysicalLocations
      (VehicleItemID, VehicleInventoryItemID, Category, PhysicalVehicleLocationID, 
  	 LocationDetail, UserPartyID, FromTS)
    VALUES (@VehicleItemID, @VehicleInventoryItemID, 'Inventory', @PhysicalVehicleLocationId, 
            @LocationDetail, @UserID, @NowTS);  
    CATCH ALL
      RAISE BookingException(2578, 'Booking - Vehicle Physical Location: ' + __errText);
    END;  
--  END IF;
  
// VehicleInventoryItemNotes
  TRY 
    @DataCollectionNote = (
      SELECT Notes
      FROM VehicleEvaluations
      WHERE VehicleEvaluationID = @VehicleEvaluationID);
    @AdjustACVNote = (
      SELECT Notes
      FROM VehiclePriceComponents
      WHERE Typ = 'VehiclePriceComponent_ACV'
      AND TableKey = @VehicleEvaluationID
      AND ThruTS IS NULL);
    @DemandOverrideNote = (
      SELECT Notes
      FROM DemandScores
      WHERE TableKey = @VehicleEvaluationID);
    @ReconNote = (
      SELECT Note
      FROM ReconNotes
      WHERE TableKey = @VehicleEvaluationID);
  // Data Collection Notes
    IF @DataCollectionNote IS NOT NULL THEN 
      EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubcategory(@VehicleInventoryItemID, @SalesConsultantID,
        'VehicleEvaluations', @VehicleEvaluationID, 'VehicleEvaluation',
        'VehicleEvaluation_DataCollectionNotes', @NowTS, @DataCollectionNote);
    END IF;
  // Adjusted ACV Reason
	  IF @AdjustACVNote IS NOT NULL THEN 
      EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubcategory(@VehicleInventoryItemID, @EvaluatorID,
        'VehicleEvaluations', @VehicleEvaluationID, 'VehicleEvaluation',
        'VehicleEvaluation_AdjustACVReason', @NowTS, @AdjustACVNote);
  		END IF;
  // Demand Override Reason
    IF @DemandOverrideNote IS NOT NULL THEN 
      EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubcategory(@VehicleInventoryItemID, @EvaluatorID,
        'VehicleEvaluations', @VehicleEvaluationID, 'VehicleEvaluation',
        'VehicleEvaluation_DemandOverrideReason', @NowTS, @DemandOverrideNote);
    END IF;
  // Recon Note
    IF @ReconNote IS NOT NULL THEN 
      EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubcategory(@VehicleInventoryItemID, @EvaluatorID,
        'VehicleEvaluations', @VehicleEvaluationID, 'VehicleEvaluation',
        'VehicleEvaluation_ReconNote', @NowTS, @ReconNote);
    END IF;
/*    
  // TNA/PIT
    IF @StatusType = 'TNAorPIT' THEN
      EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubcategory(@VehicleInventoryItemID, @BookerID,
        'VehicleInventoryItems', @VehicleInventoryItemID, 'TNA/PIT', 
        'TNA/PIT_Status', @NowTS, @TnaStatus);
    END IF;
*/     
  CATCH ALL 
    Raise BookingException(2578, 'Booking - VehicleInventoryItemNotes : ' + __errText);
  END TRY;
  
// ADD flowtasks
  INSERT INTO ViiFlowTasks(VehicleInventoryItemID, Typ) values(@VehicleInventoryItemID, 'ViiFlowTask_LOF');
  INSERT INTO ViiFlowTasks(VehicleInventoryItemID, Typ) values(@VehicleInventoryItemID, 'ViiFlowTask_PreWash');
  
   
// Call UpdateVehicleInventoryItemStatuses

  TRY 
    EXECUTE PROCEDURE UpdateVehicleInventoryItemStatuses(@VehicleInventoryItemID, @UserID, @NowTS);
  CATCH ALL
    Raise BookingException(2578, 'Booking - UpdateVehicleInventoryItemStatuses : ' + __errText);
  END TRY;
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY
COMMIT WORK; 
/**/


   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   


   
   
   
   
   
   
   


