/*
EXECUTE PROCEDURE deletevehicleinventoryitem('5543b62a-55b6-4ec3-a9b2-b0d7ff211096')

SELECT *
FROM VehicleInventoryItems 
WHERE VehicleInventoryItemID = '4947080d-01f9-474a-82a8-e4e2d15b52d6'

SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '4947080d-01f9-474a-82a8-e4e2d15b52d6'

--DELETE 
--SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '4947080d-01f9-474a-82a8-e4e2d15b52d6'
AND fromts > timestampadd(sql_tsi_day, -20, now())

SELECT *
FROM VehicleSales
WHERE VehicleInventoryItemID = '24fba16b-1f55-4beb-a7ec-902d621a290a'

SELECT *
FROM ReconAuthorizations
WHERE VehicleInventoryItemID = '24fba16b-1f55-4beb-a7ec-902d621a290a'


SELECT * 
FROM AuthorizedReconItems
WHERE ReconAuthorizationID IN (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE VehicleInventoryItemID = 'cec258f3-7ddb-4b42-bf4c-e2fc796094be')


DELETE FROM VehicleInventoryItemStatuses
WHERE fromts > timestampadd(sql_tsi_day, -30, now())
AND VehicleInventoryItemID = '24fba16b-1f55-4beb-a7ec-902d621a290a'
*/
DECLARE @VehicleInventoryItemID string;
DECLARE @VehicleSaleID String;
DECLARE @Note String;
DECLARE @NowTS TimeStamp;
DECLARE @NowTSvis Timestamp;
DECLARE @VehicleItemID String;
DECLARE @ManagerID string;
DECLARE @Price integer;
DECLARE @Buyer string;
DECLARE @BuyerID string;
DECLARE @FundingType string;
DECLARE @UserID string;

@VehicleInventoryItemID = 'caaa2f65-6f0e-41fc-b09d-79e57e9b754c';
@Buyer = 'Adesa Fargo';
@VehicleSaleID = (SELECT NewIdString(d) FROM system.iota);  
@NowTS = '11/16/2018 14:14:00';
/********** don't forget the price ***************/
@Price = 2340;

@ManagerID = (SELECT partyid FROM people WHERE fullname = 'Dave Wilkie');	

@Note = '';
@VehicleItemID = (
  SELECT vii.VehicleItemID 
  FROM VehicleInventoryItems vii 
  WHERE vii.VehicleInventoryItemID = @VehicleInventoryItemID);
@NowTSvis = (SELECT TimeStampAdd(SQL_TSI_SECOND, 1, @NowTS) FROM system.iota);					
@BuyerID = coalesce((SELECT PartyID FROM Organizations WHERE fullname = @Buyer), @Buyer collate ads_default_ci) ; -- TriState
@FundingType = 'DealFunding_Cash';	
@UserID = (SELECT partyid FROM users WHERE username = 'jon');	
		

  //INSERT the Vehicle sale 
  //Since wholesale IS a combination sale AND delivery, the VehicleSale_Delivered
  //status IS inserted here. IF we break these two operations apart, this
  //status should be SET to SoldNotDelivered  
BEGIN TRANSACTION;
  TRY 
    TRY   
      INSERT INTO VehicleSales 
        (VehicleSaleID, VehicleInventoryItemID, SoldTS, SoldAmount, Typ, Status, 
          ManagerPartyID, SoldTo, FundingTyp)
      VALUES (@VehicleSaleID, @VehicleInventoryItemID, @NowTS, 
        @Price,
        'VehicleSale_Wholesale',
        'VehicleSale_Sold',    
        @ManagerID,
        @BuyerID,
    	@FundingType);
    CATCH ALL
      RAISE WSExc(101, 'VehicleSales');
    END;
    
    TRY   
    //CLOSE out all VehicleInventoryItemStatuses
      UPDATE VehicleInventoryItemStatuses
        SET [ThruTS] = @NowTS
        WHERE VehicleInventoryItemID = @VehicleInventoryItemID
          AND ThruTS IS null;
    CATCH ALL
      RAISE WSExc(102, 'VehicleInventoryItemStatuses');
    END;  
        
    TRY 
    //CLOSE current recon authorization
      UPDATE ReconAuthorizations
        SET [ThruTS] = @NowTS
        WHERE VehicleInventoryItemID = @VehicleInventoryItemID
        AND ThruTS IS NULL; 
    CATCH ALL
      RAISE WSExc(103, 'ReconAuthorizations');
    END;
    
    TRY       
    //CLOSE out the VehicleItemStatuses record  
      UPDATE VehicleItemStatuses
        SET [ThruTS] = @NowTS
        WHERE VehicleItemID = @VehicleItemID
          AND Category = 'Inventory'
          AND ThruTS IS NULL; 
    CATCH ALL
      RAISE WSExc(104, 'VehicleItemStatuses');
    END;         

        
    //CLOSE any open VehicleItemPhysicalLocations record
    UPDATE VehicleItemPhysicalLocations
      SET [ThruTS] = @NowTS
    WHERE VehicleItemID = @VehicleItemID
      AND [ThruTS] IS NULL;         
        
    //IF the vehicle IS at an auction, CLOSE the auction record
    UPDATE VehiclesAtAuction
      SET [ThruTS] = @NowTS
      WHERE VehicleInventoryItemID = @VehicleInventoryItemID
        AND [ThruTS] IS NULL;
        
    IF (@Note IS NOT NULL) AND (@Note <> '') THEN
      EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubCategory
        (@VehicleInventoryItemID, @UserID, 'VehicleSales', @VehicleSaleID,  
        'RawMaterials', 'RawMaterials_Sold', 
        @NowTS, @Note); 
    END;
  
    INSERT INTO VehicleInventoryItemStatuses
      (VehicleInventoryItemID, Status, Category, FromTS, 
       ThruTS, BasisTable, TableKey)
     VALUES
       (@VehicleInventoryItemID, 'RawMaterials_Sold', 'RawMaterials',                                                 
        @NowTSvis, @NowTSvis, 'VehicleSales', @VehicleSaleID);

    //Increment timestamp    
    @NowTSvis = (SELECT TimeStampAdd(SQL_TSI_SECOND, 1, @NowTSvis) FROM system.iota);    
    INSERT INTO VehicleInventoryItemStatuses
      (VehicleInventoryItemID, Status, Category, FromTS, 
       ThruTS, BasisTable, TableKey)
     VALUES
       (@VehicleInventoryItemID, 'RawMaterials_Delivered', 'RawMaterials',                                                 
        @NowTSvis, @NowTSvis, 'VehicleSales', @VehicleSaleID);
  
    //CLOSE out the VehicleInventoryItem
    UPDATE VehicleInventoryItems
      SET [ThruTS] = @NowTS
    WHERE [VehicleInventoryItemID] = @VehicleInventoryItemID
    AND [ThruTS] IS NULL; 
    
    UPDATE VehicleItemStatuses
    SET ThruTS = @NowTSVis
    WHERE VehicleItemID = @VehicleItemID
    AND ThruTS IS NULL;
    
  COMMIT WORK; 
  CATCH ALL
    ROLLBACK WORK;  
    RAISE; //This re-raises the exception
  END; //TRY	



