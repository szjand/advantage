/*

manually correct ts for statuses ON the ws
ie: viis for both old AND new vii
    VehicleInventoryItems both old AND new
    VehicleITemStatuses old AND new
first ON the originating vii
THEN ON the final VehicleInventoryItems

7/29/11
H2915A -> 14571X
-- ws entered IN tool ON the 25th should have been the 22nd
UPDATE VehicleInventoryItemStatuses
SET FromTS = timestampadd(sql_tsi_day, -3, fromts)
-- SELECT * FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = '28cd27df-379a-4d14-b390-1a0f45e7d214'
AND month(fromTS) = 7;

UPDATE VehicleInventoryItemStatuses
SET ThruTS = timestampadd(sql_tsi_day, -3, ThruTS)
-- SELECT * FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = '28cd27df-379a-4d14-b390-1a0f45e7d214'
AND month(ThruTS) = 7;

THEN can manually UPDATE IN VehicleInventoryItemStatuses the single status RawMaterials_BookingPending
VehicleInventoryItemStatuses:
SELECT * FROM VehicleInventoryItemStatuses WHERE vehicleinventoryitemid = '5203299b-726d-4492-87e1-ce9ef20fe62b'
VehicleInventoryItems:
SELECT * FROM VehicleInventoryItems WHERE vehicleinventoryitemid = '5203299b-726d-4492-87e1-ce9ef20fe62b'
VehicleItemStatus:
SELECT * FROM VEhicleItemStatuses WHERE VehicleItemID = 'bd3ff39a-22a0-f241-8472-43a20102eaf6'
*/



DECLARE @VehicleInventoryItemID string;
DECLARE @BookerID string;
DECLARE @Stocknumber string;
DECLARE @NowTS timestamp;
DECLARE @Note string;

@VehicleInventoryItemID = '8fe7304d-93b7-5148-b76b-f48a5563087c';
@Stocknumber = 'H4900G';
@NowTS = now();-- '11/01/2011 14:00:00';
@BookerID = (SELECT partyid FROM users WHERE username = 'jon');
@Note = 'Manually book intra market purchase';

BEGIN TRANSACTION;
TRY
  TRY 
    EXECUTE PROCEDURE BookPendingVehicle(
      @VehicleInventoryItemID,
      @BookerID,
      @StockNumber,
      @NowTS);
  CATCH ALL
    RAISE WhackException('101', 'BookPending: ' + __errText);
  END TRY;
  TRY 
    EXECUTE PROCEDURE UpdateVehicleInventoryItemStatuses(
      @VehicleInventoryItemID,
      @BookerID,
      @NowTS);
  CATCH ALL
    RAISE WhackException('102', 'viiStatuses: ' + __errText);
  END TRY;  
  TRY 
    EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubCategory(
      @VehicleInventoryItemID,
      @BookerID,
      'VehicleInventoryItems',
      @VehicleInventoryItemID, 
      'Booking',
      'Booking_Intra Market Purchase',
      @NowTS,
      @Note);
  CATCH ALL
    RAISE WhackException('103', 'viiStatuses: ' + __errText);
  END TRY;    
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY
