DECLARE @VehicleInventoryItemID string;
DECLARE @ManagerPartyID string;
DECLARE @Price integer;
DECLARE @SoldToID string;
DECLARE @Note string;
DECLARE @NowTS Timestamp;
DECLARE @UserID string;
DECLARE @ReconPackage string;
DECLARE @VehicleSaleID string;

@VehicleInventoryItemID = '8c69fe27-7363-4e87-b2b6-962aecd51720';
@ManagerPartyID = (SELECT PartyID FROM people WHERE fullname = 'John Olderbak');
@Price = 27699;
@SoldToID = (SELECT partyid FROM Organizations WHERE name = 'Crookston');
@Note = 'Manually wholesaled 17042A to RY2 - H4900G, for retail';
@NowTS = '07/25/2012 13:00:00'; 
@UserID = (SELECT partyid FROM users WHERE username = 'jon');
@ReconPackage = (
  SELECT top 1 typ 
  FROM SelectedReconPAckages
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  ORDER BY SelectedReconPackageTS DESC);
@VehicleSaleID = (SELECT newidstring(d) FROM system.iota);

BEGIN TRANSACTION;
TRY
  TRY 
    EXECUTE PROCEDURE WholesaleVehicle(
      @VehicleSaleID,
      @VehicleInventoryItemID,
      @ManagerPartyID,
      @Price,
      @SoldToID,
      @Note,
      @NowTS);
  CATCH ALL
    Raise WhackException(100, 'Wholesale Vehicle: ' + __errText);
  END TRY; 
  TRY 
    EXECUTE PROCEDURE IntraMarketVehicleWholesale(
      @VehicleInventoryItemID,
      @UserID,
      @SoldToID,
      @ReconPackage,
      Null);
  CATCH ALL
    Raise WhackException(101, 'IntraMarket Wholesale Vehicle: ' + __errText);
  END TRY;  
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY        