
DECLARE @VehicleItemID string;
DECLARE @VehicleInventoryItemID string;
DECLARE @VehicleSaleID string;
DECLARE @StockNumber string;
DECLARE @SoldAmount integer;
DECLARE @InAndOutType string;
DECLARE @SaleType string;
DECLARE @SoldTo string;
DECLARE @LocationID string;
DECLARE @NowTS1 Timestamp;
DECLARE @UserID string;
DECLARE @NowTS2 Timestamp;
DECLARE @NowTS3 Timestamp;
DECLARE @NowTS4 Timestamp;
DECLARE @Funding string;
DECLARE @Note string;
DECLARE @SoldBy string;
DECLARE @SoldByID string;
DECLARE @Manager string;
DECLARE @ManagerID string;
DECLARE @Miles integer;
DECLARE @TNA logical;


/* verify that a VehicleItem EXISTS before continuing */
/* this IS a manual process CHECK IT !!!*/
@VehicleItemID = (
  SELECT VehicleItemID 
  FROM VehicleItems
  WHERE vin = '3GCUKSEC3EG159401');
/*
IF it doesn't EXISTS, must CREATE a VehicleItem
just DO it ON the inandout screen
*/  
  
@StockNumber = 'G34806A';
@SoldAmount = 27000;
/*
InAndOut_LeasePurchase
InAndOut_InAndOut
InAndOut_BuyBid
VehicleAcquisition_InAndOut
*/
@InAndOutType = 'InAndOut_InAndOut';
/*
VehicleSale_Retail
VehicleSale_Wholesale
*/
@SaleType = 'VehicleSale_Retail';
/*
DealFunding_Lease
DealFunding_Finance
DealFunding_Cash
DealFunding_OutsideLien
*/
@Funding = 'DealFunding_Cash';
@SoldTo = 'Lillian Livingood';
@NowTS1 = '10/05/2018 11:11:00';

@LocationID = (
  SELECT PartyID
  FROM Organizations
  WHERE name = 'Rydells');

@Miles = 19926;  
@Note = 'Manually inserted in and out';
@SoldBy = 'House';
@Manager = 'House';

@SoldByID = coalesce((SELECT PartyID FROM people WHERE fullname = @SoldBy), @SoldBy);
@ManagerID = coalesce((SELECT PartyID FROM people WHERE fullname = @Manager), @Manager);
@VehicleInventoryItemID = (SELECT newidstring(d) FROM system.iota);
@VehicleSaleID = (SELECT newidstring(d) FROM system.iota);
@UserID = (SELECT partyid FROM users WHERE username = 'jon');
@NowTS2 = (SELECT TimestampAdd(SQL_TSI_Second, 1, @NowTS1) FROM system.iota);
@NowTS3 = (SELECT TimestampAdd(SQL_TSI_Second, 1, @NowTS2) FROM system.iota);
@NowTS4 = (SELECT TimestampAdd(SQL_TSI_Second, 1, @NowTS3) FROM system.iota);

@TNA = (
  SELECT 
    CASE
      WHEN EXISTS (
        SELECT 1
        FROM VehicleInventoryItemStatuses 
        WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
          AND Category = 'RMFlagTNA'
          AND ThruTS IS NULL) THEN true
      ELSE false
    END
  FROM system.iota);

BEGIN TRANSACTION;
TRY 
  INSERT INTO InAndOuts(VehicleSaleID, Typ)
  VALUES(@VehicleSaleID, @InAndOutType);
   
  INSERT INTO VehicleInventoryItems(VehicleInventoryItemID, TableKey, 
    BasisTable, Stocknumber, FromTS, ThruTS, LocationID, VehicleItemID, BookerID,
    OwningLocationID)
  VALUES(@VehicleInventoryItemID, @VehicleSaleID, 'InAndOuts', @StockNumber, 
    @NowTS1, @NowTS2, @LocationID, @VehicleItemID, @UserID, @LocationID);
    
  INSERT INTO VehicleInventoryItemStatuses (VehicleInventoryItemID, Status, Category,
    FromTS, ThruTS, BasisTable, TableKey, UserID)
  VALUES (@VehicleInventoryItemID, 'RawMaterials_RawMaterials', 'RawMaterials',
    @NowTS1, @NowTS2, 'InAndOuts', @VehicleSaleID, @UserID);  
    
  INSERT INTO VehicleInventoryItemStatuses (VehicleInventoryItemID, Status, Category,
    FromTS, ThruTS, BasisTable, TableKey, UserID)
  VALUES (@VehicleInventoryItemID, 'RawMaterials_Sold', 'RawMaterials',
    @NowTS3, @NowTS3, 'InAndOuts', @VehicleSaleID, @UserID); 
    
  INSERT INTO VehicleInventoryItemStatuses (VehicleInventoryItemID, Status, Category,
    FromTS, ThruTS, BasisTable, TableKey, UserID)
  VALUES (@VehicleInventoryItemID, 'RawMaterials_Delivered', 'RawMaterials',
    @NowTS4, @NowTS4, 'InAndOuts', @VehicleSaleID, @UserID);      
    
  UPDATE VehicleItemInsuranceInfo
  SET ThruTS = @NowTS1
  WHERE VehicleItemID = @VehicleItemID
  AND ThruTS IS NULL;   
  
  UPDATE VehicleItemLicenseInfo
  SET ThruTS = @NowTS1
  WHERE VehicleItemID = @VehicleItemID
  AND ThruTS IS NULL; 
  
  UPDATE VehicleItemOwnerInfo
  SET ThruTS = @NowTS1
  WHERE VehicleItemID = @VehicleItemID
  AND ThruTS IS NULL; 
  
  UPDATE VehicleItemTitleInfo
  SET ThruTS = @NowTS1
  WHERE VehicleItemID = @VehicleItemID
  AND ThruTS IS NULL; 
  
  UPDATE VehicleItemStatuses
  SET ThruTS = @NowTS1
  WHERE VehicleItemID = @VehicleItemID
  AND ThruTS IS NULL; 
  
  INSERT INTO VehicleItemStatuses (VehicleItemID, Status, Category, FromTS, ThruTS,
    BasisTable, TableKey, UserID)
  VALUES (@VehicleItemID, 'Inventory_Inventory', 'Inventory', @NowTS1, @NowTS2,
    'InAndOuts', @VehicleSaleID, @UserID);
    
  INSERT INTO VehicleAcquisitions (VehicleInventoryItemID, Typ)
  VALUES (@VehicleInventoryItemID, 'VehicleAcquisition_InAndOut');
  
  INSERT INTO VehicleSales (VehicleSaleID, VehicleInventoryItemID, SoldTS, SoldAmount,
    Typ, Status, SoldBy, ManagerPartyID, SoldTo, FundingTyp)
  VALUES (@VehicleSaleID, @VehicleInventoryItemID, @NowTS2, @SoldAmount, 
    @SaleType, 'VehicleSale_Sold', @SoldByID, @ManagerID, @SoldTo, @Funding);  
    
  EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubCategory(@VehicleInventoryItemID, 
    @UserID, 'InAndOuts', @VehicleSaleID, 'VehicleSale', 'VehicleSale_InAndOut',
    @NowTS1, @Note); 

  INSERT INTO VehicleItemMileages(TableKey, BasisTable, VehicleItemID, Value, VehicleItemMileageTS)
  VALUES(@VehicleSaleID, 'InAndOuts', @VehicleItemID, @miles, @NowTS1);     

  IF @TNA = true THEN
    UPDATE TnaVehicles
    SET ReceivedByID = @ManagerID
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID
      AND @ManagerID IS NULL;
    UPDATE VehicleInventoryItemStatuses
    SET ThruTS = @NowTS1
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
      AND Category = 'RMFlagTNA'
      AND ThruTS IS NULL;
  END IF;
    
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END TRY;  
     

