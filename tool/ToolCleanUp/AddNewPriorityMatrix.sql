/*
SELECT o.fullname, pm.status, pm.value
FROM PriorityMatrices pm
INNER JOIN CurrentPriorityMatrices cpm ON pm.PriorityMatrixID = cpm.PriorityMatrixID
INNER JOIN Organizations o ON cpm.LocationID = o.PartyID

SELECT newidstring(d) FROM system.iota
SELECT newidstring() FROM system.iota
*/
-- CREATE a new PriorityMatrix
-- one row for each status
-- UPDATE CurrentPriorityMatrices, CLOSE old row, ADD new row
-- new matrix for honda , match ry1, pull = 300, wsb = 100
/* locations
ry1: 'B6183892-C79D-4489-A58C-B526DF948B06'
ry2: '4CD8E72C-DC39-4544-B303-954C99176671'
ry3: '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' 
*/
DECLARE @id string;
DECLARE @LocationID string;
DECLARE @TS timestamp;
@TS = (SELECT now() FROM system.iota);
@id = (SELECT newidstring(d) FROM system.iota);
@LocationID = '4CD8E72C-DC39-4544-B303-954C99176671';
BEGIN TRANSACTION;
TRY 
  INSERT INTO PriorityMatrices values(@id, 'RmFlagAV', 'RMFlagAV_Available', 400);
  INSERT INTO PriorityMatrices values(@id, 'RMFlagWSB', 'RMFlagWSB_WholesaleBuffer', 100);
  INSERT INTO PriorityMatrices values(@id, 'RMFlagSB', 'RMFlagSB_SalesBuffer', 900);
  INSERT INTO PriorityMatrices values(@id, 'RMFlagPulled', 'RMFlagPulled_Pulled', 300);
  INSERT INTO PriorityMatrices values(@id, 'RawMaterials', 'RawMaterials_Sold', 1000);
  UPDATE CurrentPriorityMatrices
  SET ThruTS = @TS
  WHERE LocationID = @LocationID;
  INSERT INTO CurrentPriorityMatrices values(@id, @LocationID, @TS, NULL);
  COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  