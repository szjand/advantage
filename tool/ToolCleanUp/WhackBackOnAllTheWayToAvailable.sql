/*
        UsedCarsDM.MoveVehicleFromSoldToRawMaterials(VehicleInventoryItemID,
          PhysVehLocAndNoteControl1.VehicleLocationValue, PhysVehLocAndNoteControl1.VehicleLocationDetail,
          UserSession.UserID, NowTS, PhysVehLocAndNoteControl1.Notes);
        UsedCarsDM.UpdateVehicleInventoryItemStatuses(VehicleInventoryItemID, UserSession.UserID, NowTS);
        UsedCarsDM.UpdateVehicleInventoryItemPriority(VehicleInventoryItemID);
*/
DECLARE @VehicleInventoryItemID string;
DECLARE @UserID string;
DECLARE @NowTS timestamp;
DECLARE @Note memo;
DECLARE @PhysLocationID string;
DECLARE @PhysLocationDetail string;
DECLARE @Miles integer;

@VehicleInventoryItemID = 'b0d3a95f-c56d-41aa-bfc3-0566649921cc';
@NowTS = '09/03/2010 08:08:08';
@Note = 'Manually put back on, pulled and put on lot. Sold to Steven Miller 8/31/10, back on 9/3';
@UserID = (SELECT partyid FROM users WHERE username = 'jon');
// Rydell Used Car Lot
@PhysLocationID = 'fce0b0e1-0a35-9b4b-a87c-d3bd2de45790';
@Miles = 190692;

BEGIN TRANSACTION;
TRY
  TRY
    EXECUTE PROCEDURE MoveVehicleFromSoldToRawMaterials(
      @VehicleInventoryItemID,
      @PhysLocationID,
      NULL,
      @UserID,
      @NowTS,
      @Note);
  CATCH ALL
    RAISE WhackException(100, 'MoveVehicleFromSoldToRawMaterials: ' + __errText);
  END TRY;
  TRY
    EXECUTE PROCEDURE PullVehicle(
      @VehicleInventoryItemID,
      @UserID,
      NULL, 
      @NowTS);
  CATCH ALL
    RAISE WhackException(102, 'PullVehicle: ' + __errText);
  END TRY;    
  
/*
        UsedCarsDM.SetVehicleInventoryItemsStatusFlag(VehicleInventoryItemID, UserSession.UserID,
          crsRMFlagAV, srsRMFlagAV_Available, NowTS, 'VehicleInventoryItems',
          VehicleInventoryItemID);
        UsedCarsDM.SetVehicleInventoryItemMileage(VehicleInventoryItemID, 'VehicleInventoryItems', MilesTextBox.text, NowTS);
        UsedCarsDM.UpdateVehicleInventoryItemPriority(VehicleInventoryItemID);
        DpsDM.UpdateVehicleInventoryItemPhysicalLocation(VehicleInventoryItemID, 'Inventory',
          PhysVehLocAndNoteControl1.VehicleLocationValue, PhysVehLocAndNoteControl1.VehicleLocationDetail,
          UserSession.UserID, NowTS);
        UsedCarsDM.SetVehicleInventoryItemNotesByCategorySubCategory(VehicleInventoryItemID, UserSession.UserID,
          'VehicleInventoryItems',VehicleInventoryItemID, 'Inventory', 'Inventory_PricingBuffer',
          NowTS, PhysVehLocAndNoteControl1.Notes);
        UsedCarsDM.UpdateVehicleInventoryItemStatuses(VehicleInventoryItemID, UserSession.UserID, NowTS);
*/
  TRY
    EXECUTE PROCEDURE SetVehicleInventoryItemsStatusFlag(
      @VehicleInventoryItemID,
      @UserID,
      'RMFlagAV',
      'RMFlagAV_Available', 
      @NowTS,
      'VehicleInventoryItems',
      @VehicleInventoryItemID);
  CATCH ALL
    RAISE WhackException(103, 'SetVehicleInventoryItemsStatusFlag: ' + __errText);
  END TRY;  
  TRY
    EXECUTE PROCEDURE SetVehicleInventoryItemMileage(
      @VehicleInventoryItemID,
      'VehicleInventoryItems',
      @Miles,
      @NowTS);
  CATCH ALL
    RAISE WhackException(104, 'SetVehicleInventoryItemMileage: ' + __errText);
  END TRY;  
  
  TRY
    EXECUTE PROCEDURE UpdateVehicleInventoryItemStatuses(
      @VehicleInventoryItemID,
      @UserID,
      @NowTS);
  CATCH ALL
    RAISE WhackException(101, 'UpdateVehicleInventoryItemStatuses: ' + __errText);
  END TRY; 
                
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY      


        
        