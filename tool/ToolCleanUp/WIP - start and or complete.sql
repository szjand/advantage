DECLARE @StartTS timestamp;
DECLARE @CompleteTS timestamp;
DECLARE @Started logical;
/* 
6/15/12
this IS NOT completed
6/18 - worked ok for closing a VehicleReconItem
*/

DECLARE @Completed logical;
DECLARE @VehicleReconItemID string;
DECLARE @VehicleInventoryItemID string;

@startts = now();
@completets = CAST('05/28/2012 17:00:00' AS sql_timestamp);
@started = false;
@completed = true;
@VehicleReconItemID = 'c051587e-4c03-ee4f-98a7-32c5557fb99b';
@VehicleInventoryItemID = '58729d37-8594-4aa7-9b6d-b293be8c4608';
/*
SELECT a.VehicleReconItemID, a.typ, LEFT(a.description, 20), b.status, b.startts, b.completets
--SELECT *
FROM VehicleReconItems a
LEFT JOIN AuthorizedReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
WHERE a.VehicleInventoryItemID = '58729d37-8594-4aa7-9b6d-b293be8c4608'
  AND a.typ LIKE 'Body%'
  AND b.ReconAuthorizationID = (
    SELECT ReconAuthorizationID
    FROM ReconAuthorizations
    WHERE VehicleInventoryItemID = '58729d37-8594-4aa7-9b6d-b293be8c4608'
      AND thruts IS NULL)

06/28/2012 8:36:00 AM      
SELECT * 
FROM AuthorizedReconItems
WHERE VehicleReconItemID = 'e527f901-62b7-431b-84a2-462bbff03294' 

SELECT *
FROM v
     
*/

/**/
IF @Started = True AND @Completed = True THEN
  UPDATE AuthorizedReconItems
    SET [Status] = 'AuthorizedReconItem_Complete',
        [StartTS] = @startts,
        [CompleteTS] = @completets
    WHERE [VehicleReconItemID] = @VehicleReconItemID
    AND ReconAuthorizationID = (
      SELECT ReconAuthorizationID
      FROM ReconAuthorizations 
      WHERE VehicleInventoryItemID = (
        SELECT VehicleInventoryItemID 
        FROM VehicleReconItems 
        WHERE VehicleReconItemID = @VehicleReconItemID
      AND ThruTS IS NULL));
      
ELSEIF @Started = True THEN
  UPDATE AuthorizedReconItems
    SET [Status] = 'AuthorizedReconItem_InProcess',
        [StartTS] = @NowTS
    WHERE [VehicleReconItemID] = @VehicleReconItemID
    AND ReconAuthorizationID = (
      SELECT ReconAuthorizationID
      FROM ReconAuthorizations 
      WHERE VehicleInventoryItemID = (
        SELECT VehicleInventoryItemID 
        FROM VehicleReconItems 
        WHERE VehicleReconItemID = @VehicleReconItemID
      AND ThruTS IS NULL));
/**/      
ELSEIF @Completed = True THEN
  UPDATE AuthorizedReconItems
    SET [Status] = 'AuthorizedReconItem_Complete',
        [CompleteTS] = @completets
    WHERE [VehicleReconItemID] = @VehicleReconItemID
    AND ReconAuthorizationID = (
      SELECT ReconAuthorizationID
      FROM ReconAuthorizations 
      WHERE VehicleInventoryItemID = (
        SELECT VehicleInventoryItemID 
        FROM VehicleReconItems 
        WHERE VehicleReconItemID = @VehicleReconItemID
      AND ThruTS IS NULL));    
ELSE
  Raise AuthorizedReconUpdateError(2821, 'Cannot call SetAuthorizedReconItemStatus with Start AND Complete logical parameters SET to False');
END;
   

   
   
   
   
   

