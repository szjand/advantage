/*
TS fields affected IN:
viiStatuses
VehicleItemMileages
vehicleitemphysicallocations
*/

SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = 'c2da0591-31d3-7f4d-b9de-0079c9b726b5'
ORDER BY fromts

SELECT * FROM vehicleitemmileages WHERE vehicleitemid = (SELECT vehicleitemid FROM VehicleInventoryItems WHERE VehicleInventoryItemID = '02019f0d-4e6d-4ab7-b98a-beb87f558bd3')

SELECT * FROM vehicleitemmileages WHERE vehicleitemid = '633cb550-5a15-a74a-9417-8ad613e962cf'

SELECT * FROM vehicleitemphysicallocations WHERE VehicleInventoryItemID = '02019f0d-4e6d-4ab7-b98a-beb87f558bd3'

SELECT * FROM VehicleInventoryItemNotes WHERE VehicleInventoryItemID = '02019f0d-4e6d-4ab7-b98a-beb87f558bd3'


select *
FROM ReconAuthorizations ra
LEFT JOIN AuthorizedReconItems ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
WHERE ra.VehicleInventoryItemID = 'eea7e056-011b-9748-964d-b87d28e72f32'

