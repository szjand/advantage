
SELECT a.vehicleitemid, a.fromts, a.thruts, b.*
FROM vehicleitemphysicallocations a
INNER JOIN locationphysicalvehiclelocations b on a.physicalvehiclelocationid = b.physicalvehiclelocationid
WHERE b.locationshortname LIKE '%outl%'
  AND a.thruts IS NOT null
ORDER BY a.vehicleitemid

SELECT a.vehicleitemid, cast(min(a.fromts) AS sql_date) AS from_Date, cast(max(a.thruts) AS sql_date) AS thru_date
FROM vehicleitemphysicallocations a
INNER JOIN locationphysicalvehiclelocations b on a.physicalvehiclelocationid = b.physicalvehiclelocationid
WHERE b.locationshortname LIKE '%outl%'
  AND a.thruts IS NOT NULL
GROUP BY vehicleitemid  
ORDER BY a.vehicleitemid

SELECT COUNT(*) FROM vehicleitemphysicallocations

SELECT a.vehicleitemid, a.fromts, a.thruts, b.*
FROM vehicleitemphysicallocations a
INNER JOIN locationphysicalvehiclelocations b on a.physicalvehiclelocationid = b.physicalvehiclelocationid
WHERE b.locationshortname LIKE '%outl%'
  AND a.thruts IS NOT NULL
  AND a.vehicleitemid = '000ebd21-9cd3-d44d-9a07-b922bef194fb'  
ORDER BY a.vehicleitemid


select * FROM VehicleInventoryItems WHERE VehicleInventoryItemID = '48014228-3d10-4ac6-b8da-31b6d20c37fb'

-- this IS ALL i really need
SELECT vehicleinventoryitemid, CAST(fromts AS sql_date) AS from_date, CAST(thruts AS sql_date) AS thru_date
-- SELECT COUNT(*)
FROM vehicleitemphysicallocations
WHERE physicalvehiclelocationid IN ('ed92e94b-1a2b-3848-9a50-3e78929f5acc','5b40bfa8-aafe-2946-9d4f-e2002695188a','fce790d0-6d0e-bd4c-bfbe-da9688f825a7')
  AND CAST(fromts AS sql_Date) > '06/01/2017'