duplicate minot records IN usedcars

first thing i notice, this IS general, IS 8000 + records with no market - whack 'em, of course they are Minot vehicles
/*
SELECT COUNT(*) FROM usedcars WHERE length

SELECT COUNT(*)
FROM usedcars
WHERE length(market) = 0

SELECT DISTINCT stocknumber
FROM usedcars
WHERE length(market) = 0

SELECT DISTINCT market FROM usedcars WHERE stocknumber in ('28511SA', '30076A')

SELECT * FROM usedcars WHERE stocknumber in ('28511SA', '30076A')

DELETE FROM usedcars WHERE length(market) = 0
*/
SELECT vehicleid, stocknumber, vin, make, model, salesstatus
FROM usedcars
WHERE market = 'MT-Minot'
AND salesstatus NOT IN ('Sold', 'Wholesaled')
ORDER BY stocknumber

SELECT *
FROM usedcars
WHERE stocknumber IN (
  SELECT stocknumber
  FROM usedcars
  WHERE market = 'MT-Minot'
  GROUP BY stocknumber
  HAVING COUNT(*) > 1)
ORDER BY stocknumber  

-- DISTINCT markets with duplicate stocknumbers
SELECT market, COUNT(*) -- holy shit, beaucoup markets
FROM usedcars
WHERE stocknumber IN (
  SELECT stocknumber
  FROM usedcars
  GROUP BY stocknumber
  HAVING COUNT(*) > 1)
GROUP BY Market  

-- ah, this clarifies the picture somewhat
-- eliminating the duplication of stocknumbers across markets
SELECT vehicleid, stocknumber, vin, make, model, salesstatus
FROM usedcars
WHERE market = 'SP-St. Paul'
AND stocknumber IN (
  SELECT stocknumber
  FROM usedcars
  WHERE market = 'SP-St. Paul'
  GROUP BY stocknumber
  HAVING COUNT(*) > 1)
ORDER BY stocknumber  

-- GROUP BY stocknumber & vin
-- narrows IN ON the real issue
SELECT market, COUNT(*)
FROM usedcars
WHERE trim(stocknumber) + TRIM(vin) IN (
  SELECT trim(stocknumber) + TRIM(vin)
  FROM usedcars
  GROUP BY trim(stocknumber) + TRIM(vin)
  HAVING COUNT(*) > 1)
GROUP BY market  

-- AND narrow it to Minot
-- hmm, they are NOT ALL exact duplicates, some "dups" have different statuses
SELECT vehicleid, stocknumber, vin, salesstatus
FROM usedcars
WHERE trim(stocknumber) + TRIM(vin) IN (
  SELECT trim(stocknumber) + TRIM(vin)
  FROM usedcars
  GROUP BY trim(stocknumber) + TRIM(vin)
  HAVING COUNT(*) > 1)
AND market = 'MT-Minot'  
ORDER BY stocknumber

-- AND narrow it to Minot
-- hmm, they are NOT ALL exact duplicates, some "dups" have different statuses
-- so, ADD salesstatus to the grouping
SELECT vehicleid, stocknumber, vin, make, model, salesstatus
FROM usedcars
WHERE trim(stocknumber) + TRIM(vin) + TRIM(salesstatus) IN (
  SELECT trim(stocknumber) + TRIM(vin) + TRIM(salesstatus)
  FROM usedcars
  GROUP BY trim(stocknumber) + TRIM(vin) + TRIM(salesstatus)
  HAVING COUNT(*) > 1)
AND market = 'MT-Minot'  
ORDER BY stocknumber
  
-- so now,  take the "max" vehicleid FROM this SET AND DELETE it
DELETE 
-- SELECT COUNT(*)
FROM usedcars
WHERE vehicleid IN (
  SELECT max(vehicleid)
  FROM usedcars
  WHERE market = 'MT-Minot'  
  GROUP BY stocknumber, vin, salesstatus
  HAVING COUNT(*) > 1)
  
 -- so, now how to DELETE the "dup" with different salesstatus
-- can't immediately think of a way, other than manually
-- picking the extraneous record 
SELECT vehicleid, stocknumber, vin, salesstatus
FROM usedcars
WHERE trim(stocknumber) + TRIM(vin) IN (
  SELECT trim(stocknumber) + TRIM(vin)
  FROM usedcars
  GROUP BY trim(stocknumber) + TRIM(vin)
  HAVING COUNT(*) > 1)
AND market = 'MT-Minot'  
ORDER BY stocknumber

DELETE 
FROM usedcars
WHERE vehicleid IN (
'{BFDECEBC-560E-4D3B-924C-02B272988E3F}')


SELECT stocknumber
FROM usedcars
WHERE market = 'MT-Minot'  
GROUP BY stocknumber
HAVING COUNT(*) > 1
ORDER BY stocknumber

SELECT vehicleid, stocknumber, vin, salesstatus
FROM usedcars
WHERE stocknumber = '35821AA'

