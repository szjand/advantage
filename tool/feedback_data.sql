SELECT notests, CAST(notests AS sql_date), notes
-- SELECT *  
FROM VehicleInventoryItemnotes
WHERE subcategory = 'VehicleWalk_EvaluatorFeedback'
  AND CAST(notests AS sql_date) > '10/13/2022'
	
SELECT * FROM VehicleEvaluations 	
select * FROM VehicleItems 
SELECT * FROM VehicleItemmileages
	
INSERT INTO vehicle_walk_feedback	
SELECT cast(a.VehicleWalkTS AS sql_date) AS walk_date, c.fullname AS walker, d.fullname AS evaluator,
  e.stocknumber, f.vin, f.yearmodel, f.make, f.model, f.TRIM, f.bodystyle, k.value AS miles, g.notes
FROM VehicleWalks a
JOIN VehicleEvaluations b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
JOIN people c on a.vehiclewalkerid = c.partyid
JOIN people d on b.vehicleevaluatorid = d.partyid
JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
JOIN VehicleItems f on e.VehicleItemID = f.VehicleItemID 
JOIN VehicleInventoryItemNotes g on a.VehicleWalkid = g.tablekey
  AND subcategory = 'VehicleWalk_EvaluatorFeedback' 
LEFT JOIN VehicleItemMileages k on b.VehicleEvaluationID = k.tablekey
WHERE CAST(notests AS sql_date) > '10/13/2022'
ORDER BY notests

UPDATE vehicle_walk_feedback
SET sent = false;