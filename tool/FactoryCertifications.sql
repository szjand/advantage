SELECT *
FROM system.tables
WHERE name LIKE '%ertific%'

select *
FROM factorycertifications

select VehicleInventoryItemID 
FROM factorycertifications
GROUP BY VehicleInventoryItemID
HAVING COUNT(*) > 1

-- 240 ms
SELECT viix.stocknumber, fc.*
FROM FactoryCertifications fc
INNER JOIN VehicleInventoryItems viix ON fc.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.ThruTS IS NULL 
  AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'Rydells')
INNER JOIN (  
  SELECT VehicleInventoryItemID, MAX(SelectedReconPackageTS) 
  FROM selectedreconpackages
  WHERE typ = 'ReconPackage_Factory'
  AND VehicleInventoryItemID IS NOT NULL 
  GROUP BY VehicleInventoryItemID) p ON fc.VehicleInventoryItemID = p.VehicleInventoryItemID 
AND fc.cert = ''

 
-- 50 msec 
SELECT viix.stocknumber, 
  (SELECT vin FROM VehicleItems WHERE VehicleItemID = viix.VehicleItemID),
  (SELECT model FROM VehicleItems WHERE VehicleItemID = viix.VehicleItemID)
FROM VehicleInventoryItems viix
INNER JOIN selectedreconpackages p ON viix.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND p.typ = 'ReconPackage_Factory'
  AND p.SelectedReconPackageTS = (
    SELECT MAX(SelectedReconPackageTS) 
    FROM selectedreconpackages
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    GROUP BY VehicleInventoryItemID)   
INNER JOIN FactoryCertifications fc ON viix.VehicleInventoryItemID = fc.VehicleInventoryItemID 
  AND fc.cert = ''  
WHERE viix.ThruTS IS NULL
AND viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'Rydells')  
ORDER BY stocknumber

--4/27/11
SELECT viix.stocknumber, vix.yearmodel, vix.make, vix.model, 
  cast(viix.fromts AS sql_date) AS "Date Acqd", 
  Utilities.CurrentViiSalesStatus(viix.VehicleInventoryItemID)
FROM VehicleInventoryItems viix
INNER JOIN SelectedReconPackages srpx ON viix.VehicleInventoryItemID = srpx.VehicleInventoryItemID
INNER JOIN VehicleInspections vinsp ON viix.VehicleInventoryItemID = vinsp.VehicleInventoryItemID -- only inspected vehicles
INNER JOIN VehicleWalks vwx ON viix.VehicleInventoryItemID = vwx.VehicleInventoryItemID -- only walked vehicles
LEFT JOIN FactoryCertifications f ON viix.VehicleInventoryItemID = f.VehicleInventoryItemID 
LEFT JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
WHERE viix.ThruTS IS NULL 
AND LEFT(viix.stocknumber, 1) NOT IN ('H','C')
AND srpx.typ = 'ReconPackage_Factory'
AND srpx.SelectedReconPackageTS = (
  SELECT MAX(SelectedReconPackageTS)
  FROM SelectedReconPackages
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
  GROUP BY VehicleInventoryItemID)
AND length(f.cert) = 0
ORDER BY viix.stocknumber
  

   
 
 
  
