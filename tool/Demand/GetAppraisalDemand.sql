/*
4/4
looks LIKE @Inventory IS the big hit
4/8
think @Inventory IS AS good AS it's going to get
*/

/*
CREATE PROCEDURE zzzGetAppraisalDemand
   ( 
      VehicleEvaluationID CHAR ( 38 ),
      MarketID CHAR ( 38 ),
      Make CHAR ( 60 ),
      Model CHAR ( 60 ),
      Appeal CHAR ( 5 ),
      BBRough Integer,
      BBAverage Integer,
      BBClean Integer,
      Demand Integer OUTPUT
   ) 
BEGIN 
                                                       
       
     

EXECUTE PROCEDURE GetAppraisalDemand('AAAAAAAAAAAAAAAAAA', 'A4847DFC-E00E-42E7-89EE-CD4368445A82',
                   'Chevrolet', 'Silverado 1500', '3', 10850, 12850, 14850);
				   
SELECT * FROM VehicleEvaluations WHERE VehicleItemID = (SELECT VehicleItemID FROM VehicleItems WHERE vin = '3GNFK16397G292109')	
SELECT * FROM VehicleItems WHERE vin = '3GNFK16397G292109'			   
*/       
  declare @VehicleEvaluationID String, @MarketID String, @Make String, @Model String, @Appeal String, @BBRough Integer, @BBAverage Integer, @BBClean Integer;
  declare @VehicleType String, @Price Integer, @PriceBand String, @Sales Double, @Inventory Double, @DaysSupply Double;
  declare @DemandScore Integer;
--  declare @Input cursor as select * from [__Input];
--open @Input;
--try
--  fetch @Input;
    @VehicleEvaluationID = 'e8b24362-ebf4-43f1-8199-09689f61318b'; --@Input.[VehicleEvaluationID];
    @MarketID = (SELECT partyid FROM organizations WHERE name = 'Grand Forks'); --@Input.[MarketID];
    @Make = 'Buick'; --@Input.[Make];
    @Model = 'Terraza'; -- @Input.[Model];
    @Appeal = '1'; --@Input.[Appeal];
    @BBRough = 8475; --@Input.[BBRough];
    @BBAverage = 10875; --@Input.[BBAverage];
    @BBClean = 13100; --@Input.[BBClean];
--finally
--  close @Input;
--end;

@VehicleType = (SELECT VehicleType FROM MakeModelClassifications WHERE Make = @Make AND Model = @Model);

@Price = (
  SELECT 
    CASE
      WHEN @Appeal = '1' THEN @BBRough + 1500
      WHEN @Appeal = '2' THEN @BBAverage + 2000
      WHEN @Appeal = '3' THEN @BBClean + 2500
    END
  FROM system.iota);
 
@PriceBand = (
  SELECT PriceBand 
  FROM PriceBands 
  WHERE @Price BETWEEN PriceFrom AND PriceThru);

@Sales = (
  SELECT COUNT(VehicleSaleID)
  FROM VehicleSales vs
  LEFT JOIN VehicleInventoryItems viix ON vs.VehicleInventoryItemID = viix.VehicleInventoryItemID 
  LEFT JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
  WHERE typ = 'VehicleSale_Retail'
  AND status = 'VehicleSale_Sold'
  AND cast(SoldTS AS sql_date) > Curdate() - 31
  AND viix.LocationID IN (
    SELECT PartyID2
    FROM PartyRelationships
    WHERE typ = 'PartyRelationship_MarketLocations'
    AND PartyID1 = @MarketID)
  AND vix.Model IN (
    SELECT Model
    FROM MakeModelClassifications 
    WHERE VehicleType = @VehicleType)
  AND (       
    SELECT TOP 1 vpd.Amount
    FROM VehiclePricings vp
    INNER JOIN VehiclePricingDetails vpd ON vpd.VehiclePricingID = vp.VehiclePricingID 
      AND vpd.Typ = 'VehiclePricingDetail_BestPrice'
    WHERE vp.VehicleInventoryItemID = vs.VehicleInventoryItemID
    ORDER BY VehiclePricingTS DESC)       
    BETWEEN (SELECT PriceFrom FROM PriceBands WHERE PriceBand = @PriceBand)
    AND (SELECT PriceThru FROM PriceBands WHERE PriceBand = @PriceBand));        
                 
@Inventory = (
  SELECT COUNT(*)
  FROM VehicleInventoryItemStatuses viisx
  INNER JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND viix.LocationID IN (
      SELECT PartyID2
      FROM PartyRelationships
      WHERE typ = 'PartyRelationship_MarketLocations'
      AND PartyID1 = @MarketID) 
  INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
    AND vix.Model IN (
      SELECT Model
      FROM MakeModelClassifications 
      WHERE VehicleType = @VehicleType)
  INNER JOIN VehiclePricings vpx ON viix.VehicleInventoryItemID = vpx.VehicleInventoryItemID
    AND vpx.VehiclePricingTS = (
      SELECT MAX(VehiclePricingTS)
      FROM VehiclePricings
      WHERE VehicleInventoryItemID = vpx.VehicleInventoryItemID 
      GROUP BY vpx.VehicleInventoryItemID)
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'		  
  WHERE ((
    viisx.Category = 'RMFlagAV'
      AND viisx.ThruTS IS NULL
      AND Curdate() - cast(viisx.FromTS AS sql_date) < 31)
    OR (
    viisx.Category IN (/*'RMFlagTNA',*/'RMFlagRMP','RMFlagRB','RMFlagPB')
      AND viisx.ThruTS IS NULL))
  AND vpdx.amount BETWEEN (SELECT PriceFrom FROM PriceBands WHERE PriceBand = @PriceBand)
        AND (SELECT PriceThru FROM PriceBands WHERE PriceBand = @PriceBand));
                   
IF @Sales <> 0 then
  @DaysSupply = Truncate(@Inventory / (@Sales / 30), 0);
ELSE 
  @DaysSupply = 60;                                 	  
END IF;

@DemandScore = (
  SELECT
    CASE 
      WHEN @DaysSupply < 30 THEN 3
      WHEN @DaysSupply < 45 THEN 2
      ELSE 1
    END
  FROM system.iota);

  //VehicleEvaluationID IS blank WHEN calculating demand for an auction
/*  commented this part out for testing, needs to be put back in  
  IF @VehicleEvaluationID <> '' THEN  
    insert into VehicleEvaluationDemands(VehicleEvaluationID, Appeal, BBRough,
      BBAverage, BBClean, VehicleType, Price, PriceBand, Sales, Inventory,
      DaysSupply, Demand, VehicleEvaluationDemandTS)
    values (@VehicleEvaluationID, trim(@Appeal), @BBRough, @BBAverage, @BBClean,
      trim(@VehicleType), @Price, trim(@PriceBand), @Sales, @Inventory,
      @DaysSupply, @DemandScore, now());
  END;
*/  
--  insert into [__Output] values (@DemandScore);   

SELECT @DemandScore, @Price, @PriceBand, @Sales, @DaysSupply, @Inventory FROM system.iota;
   
   

   
   
   
   
-- END;

