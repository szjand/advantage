SELECT vii.stocknumber, vii.fromts, vii.thruts, i.*
FROM VehicleInventoryItems vii
INNER JOIN legacy.inpmast i ON i.stocknumber = vii.stocknumber
WHERE vii.thruts IS NULL
AND i.status = 'C'
ORDER BY vii.stocknumber


SELECT wtf.stocknumber, viis.status
FROM VehicleInventoryItemStatuses viis
INNER JOIN (
  SELECT vii.stocknumber,vii.VehicleInventoryItemID, vii.fromts, vii.thruts, i.*
  FROM VehicleInventoryItems vii
  INNER JOIN legacy.inpmast i ON i.stocknumber = vii.stocknumber
  WHERE vii.thruts IS NULL
  AND i.status = 'C') wtf ON wtf.VehicleInventoryItemID = viis.VehicleInventoryItemID
WHERE viis.category LIKE 'RMFlag%'
AND viis.thruts IS NULL 
ORDER BY wtf.stocknumber