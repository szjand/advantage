SELECT stocknumber, currentpriority
FROM VehicleInventoryItems v 
WHERE EXISTS (
  SELECT 1
  FROM vusedcarswalkpending
  WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND daysinstatus = 0)
ORDER BY stocknumber    
  
