-- this IS getting closer
-- the old issue
-- IF the VehicleItem has been evaluated more than once
-- i only care if it was TNA for the relevant evaluation for this specific stock number 
-- IN the situation WHERE a vehicle IS evaluated-inventoried-sold, that same vehicle
-- could be evaluated again after the sale
/*
so, vis.FromTS has to be greater than vii.FromTS AND IF vii.ThruTS IS NOT NULL THEN vis.FromTS 
must also be less than vii.ThruTS

aha - coalesce thruts to '9999/01/01'

ok wiseguy, what about vehicleowner info
the larger question of FROM whom DO we obtain a vii
*/

SELECT LEFT(vii.stocknumber, 15) AS StockNumber, 
  (select left(YearModel + ' ' + trim(make) + ' ' + trim(model), 25)
    FROM VehicleItems
    WHERE VehicleItemID = vii.VehicleItemID) AS Vehicle,
  cast(vii.fromts AS SQL_DATE) AS Booked, 
  DayName(vii.FromTS),
  cast(vi.VehicleInspectionTS AS SQL_DATE) AS Inspected,
  DayName(vi.VehicleInspectionTS) AS "Day of Week",
  cast(vi.VehicleInspectionTS AS SQL_DATE) - cast(vii.fromts AS SQL_DATE) AS "Days to Inspect",
  CASE ve.typ
    WHEN 'VehicleEvaluation_Trade' THEN (
      SELECT top 1 left(LastName, 25)
	  FROM VehicleItemOwnerInfo
	  WHERE VehicleItemID = vii.VehicleItemID
	  ORDER BY FromTS desc)
	WHEN 'VehicleEvaluation_Auction' THEN (
	  SELECT left(Fullname, 25)
	  FROM Organizations
	  WHERE PartyID = (
	    SELECT AuctionID
		FROM AuctionVehicles
		WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID))
  END AS Source,
  CASE ve.typ -- IF trade, was it TNA
    WHEN 'VehicleEvaluation_Trade' THEN 
      CASE 
        WHEN (
          SELECT 'TNA'
          FROM system.iota
          WHERE EXISTS (
            SELECT 1
        	FROM VehicleItemStatuses
        	WHERE status = 'Inventory_TradeNotAvailable'
        	AND VehicleItemID = vii.VehicleItemID
    		AND ((FromTS >= vii.FromTS) AND (ThruTS <= coalesce(vii.ThruTS, cast('9999/01/01 00:00:00' AS SQL_Timestamp)))))) = 'TNA' THEN 'TNA'
        ELSE 'Available'
      END
    ELSE 'Auction'
  END AS "Trade Availability"
FROM VehicleInventoryItems vii
LEFT JOIN VehicleInspections vi ON vi.VehicleInventoryItemID = vii.VehicleInventoryItemID
LEFT JOIN VehicleEvaluations ve ON ve.VehicleInventoryItemID = vii.VehicleInventoryItemID
WHERE cast(vii.fromts AS SQL_DATE) > '06/30/2010'
AND vii.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
ORDER BY vii.fromts DESC

-- vUsedCarsInspectionPending

   SELECT 

  vii.VehicleInventoryItemID, vii.StockNumber,
  vii.CurrentPriority, 
  vi.VIN,
  Coalesce(Current_Date() - cast(viis.FromTS AS SQL_DATE), 0) AS DaysOwned,
  CASE
    WHEN EXISTS (
      SELECT 1 
      FROM TNAVehicles
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID) THEN (
        SELECT Current_Date() - cast(ThruTS AS SQL_DATE)
        FROM VehicleInventoryItemStatuses 
        WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
        AND category IN ('RMFlagTNA', 'RMFlagPIT'))
    ELSE Coalesce(Current_Date() - cast(viis.FromTS AS SQL_DATE), 0)
  END AS DaysInspectionPending,  
  Coalesce(Trim(vi.Trim),'') + ' '  + Coalesce(Trim(vi.BodyStyle), '') + ' ' + 
  Coalesce(Trim(vi.Engine), '') AS VehicleDescription,
  vii.OwningLocationID, vi.YearModel, vi.Make, vi.Model,
  (SELECT Description
    FROM TypDescriptions
    WHERE Typ = (
      SELECT top 1 Typ FROM SelectedReconPackages
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      ORDER BY SelectedReconPackageTS DESC)) AS SelectedPackage,
coalesce((
  SELECT fullname
  FROM people p
  WHERE PartyID = (
    SELECT coalesce(SalesConsultantID, VehicleEvaluatorID)
    FROM VehicleEvaluations
    WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID)), 'Unknown') AS TradedInBy       
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii
 on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
INNER JOIN VehicleItems vi 
  ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc
          on mmc.Make = vi.Make
          AND mmc.Model = vi.Model
          AND mmc.ThruTS IS NULL
WHERE viis.Status = 'RMFlagIP_InspectionPending'
AND viis.ThruTS IS NULL
AND NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
  AND (status = 'RMFlagTNA_TradeNotAvailable' OR status = 'RMFlagPIT_PurchaseInTransit') 
  AND ThruTS IS NULL)
AND vii.locationID = (SELECT partyid FROM organizations WHERE name = 'rydells')  

----
----
-- LEAVE out sold vehicles with no inspection performed
-- inpections performed / day
SELECT cast(VehicleInspectionTS AS sql_date), max(dayname(VehicleInspectionTS)), COUNT(*) AS "Insp Done"
FROM vehicleinspections vi
INNER JOIN VehicleInventoryItems vii ON vii.VehicleInventoryItemID = vi.VehicleInventoryItemID 
WHERE month(VehicleInspectionTS) > 7
AND year(VehicleInspectionTS) = 2010
AND vii.locationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
GROUP BY cast(VehicleInspectionTS AS sql_date)


--SELECT * FROM vehicleinspections

