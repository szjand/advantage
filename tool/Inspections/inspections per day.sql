SELECT CAST(vehicleinspectionts AS sql_date), 
  max(dayname(CAST(vehicleinspectionts AS sql_date))), COUNT(*)
FROM vehicleinspections i
INNER JOIN VehicleInventoryItems v ON i.VehicleInventoryItemID = v.VehicleInventoryItemID 
  AND v.owninglocationid = (
    SELECT partyid
	FROM organizations
	WHERE name = 'rydells')
where CAST(vehicleinspectionts AS sql_date) > curdate() - 365
GROUP BY CAST(vehicleinspectionts AS sql_date)
ORDER BY CAST(vehicleinspectionts AS sql_date) DESC 



SELECT cast(MIN(n.thedate) AS sql_char), sum(HowMany)
FROM (
  SELECT CAST(vehicleinspectionts AS sql_date) AS inspDate, COUNT(*) AS howMany
  FROM vehicleinspections i
  INNER JOIN VehicleInventoryItems v ON i.VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND v.owninglocationid = (
      SELECT partyid
  	FROM organizations
  	WHERE name = 'rydells')
  where CAST(vehicleinspectionts AS sql_date) > curdate() - 365
  GROUP BY CAST(vehicleinspectionts AS sql_date)) m
LEFT JOIN dds.day n on m.inspDate = n.thedate
GROUP BY n.sundayToSaturdayWeek