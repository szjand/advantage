-- insp avail 5/25 @ 8AM 
-- 5
select vii.VehicleInventoryItemID, vii.stocknumber, viis.FromTS, viis.ThruTS
--from dds.day
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii
 on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
WHERE viis.Status = 'RMFlagIP_InspectionPending'
  AND viis.fromts < '05/25/2012 16:00:00'
  AND coalesce(viis.ThruTS, now()) > '05/25/2012 16:00:00'
  AND NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
    AND (
      (status = 'RMFlagTNA_TradeNotAvailable' and coalesce(thruts, now()) > '05/25/2012 16:00:00')
      OR 
      (status = 'RMFlagPIT_PurchaseInTransit') and coalesce(thruts, now()) > '05/25/2012 16:00:00'))
  AND LEFT(stocknumber, 1) NOT IN ('C','H')    
ORDER BY stocknumber  
  

-- only 4 inspections were done ON the 25th  
SELECT v.stocknumber, i.vehicleinspectionts
FROM vehicleinspections i
INNER JOIN VehicleInventoryItems v ON i.VehicleInventoryItemID = v.VehicleInventoryItemID 
where v.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
  AND CAST(i.vehicleinspectionts AS sql_date) = '05/25/2012'
  
-- insp avail 5/25 @ 8AM   14
select vii.VehicleInventoryItemID, vii.stocknumber, viis.FromTS, viis.ThruTS
--from dds.day
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii
 on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
WHERE viis.Status = 'RMFlagIP_InspectionPending'
  AND viis.fromts < '05/25/2012 08:00:00'
  AND coalesce(viis.ThruTS, now()) > '05/25/2012 08:00:00'
  AND LEFT(stocknumber, 1) NOT IN ('C','H') 
ORDER BY stocknumber     
  
select vii.VehicleInventoryItemID, vii.stocknumber, viis.FromTS, viis.ThruTS
-- 7
--from dds.day
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii
 on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
WHERE viis.Status = 'RMFlagTNA_TradeNotAvailable'
  AND viis.fromts < CAST('05/25/2012 08:00:00' AS sql_timestamp)
  AND coalesce(viis.ThruTS, now()) > '05/25/2012 08:00:00'
  AND LEFT(stocknumber, 1) NOT IN ('C','H')    
ORDER BY stocknumber   
  
select vii.VehicleInventoryItemID, vii.stocknumber, viis.FromTS, viis.ThruTS
-- 2
--from dds.day
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii
 on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
WHERE viis.Status = 'RMFlagPIT_PurchaseInTransit'
  AND viis.fromts < CAST('05/25/2012 08:00:00' AS sql_timestamp)
  AND coalesce(viis.ThruTS, now()) > CAST('05/25/2012 08:00:00' AS sql_timestamp)
  AND LEFT(stocknumber, 1) NOT IN ('C','H')     
ORDER BY stocknumber  

  
SELECT *
FROM dds.timeofday  
WHERE thetime = '08:00:00'  


-- at what point does a vehicle become available for inspection
NOT TNA
NOT PIT
NOT WTF

-- this started out seeming promising
-- was thinking could DO a MAX ON fromts
-- duh no, eg, wtf only relevant IF it IS the interval
-- don't care WHEN fromts for TNA IS, care about WHEN it ends
SELECT stocknumber, VehicleInventoryItemID, fromts, coalesce(thruts, timestampadd(sql_tsi_year, 1987,now()))
FROM VehicleInventoryItems v
WHERE year(fromts) = 2012
  AND LEFT(stocknumber, 1) NOT IN ('C','H')    
UNION ALL 
SELECT 'IP', s.VehicleInventoryItemID , fromts, coalesce(thruts, timestampadd(sql_tsi_year, 1987,now()))
FROM VehicleInventoryItemStatuses s
INNER JOIN (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems 
  WHERE year(fromts) = 2012
    AND LEFT(stocknumber, 1) NOT IN ('C','H')) v ON s.VehicleInventoryItemID = v.VehicleInventoryItemID   
WHERE s.category = 'RMFlagIP'
UNION ALL 
SELECT 'PIT', s.VehicleInventoryItemID , fromts, coalesce(thruts, timestampadd(sql_tsi_year, 1987,now()))
FROM VehicleInventoryItemStatuses s
INNER JOIN (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems 
  WHERE year(fromts) = 2012
    AND LEFT(stocknumber, 1) NOT IN ('C','H')) v ON s.VehicleInventoryItemID = v.VehicleInventoryItemID   
WHERE s.category = 'RMFlagPIT'
UNION ALL  
SELECT 'TNA', s.VehicleInventoryItemID , fromts, coalesce(thruts, timestampadd(sql_tsi_year, 1987,now()))
FROM VehicleInventoryItemStatuses s
INNER JOIN (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems 
  WHERE year(fromts) = 2012
    AND LEFT(stocknumber, 1) NOT IN ('C','H')) v ON s.VehicleInventoryItemID = v.VehicleInventoryItemID   
WHERE s.category = 'RMFlagTNA'
UNION ALL 
SELECT 'WTF', s.VehicleInventoryItemID , fromts, coalesce(thruts, timestampadd(sql_tsi_year, 1987,now()))
FROM VehicleInventoryItemStatuses s
INNER JOIN (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems 
  WHERE year(fromts) = 2012
    AND LEFT(stocknumber, 1) NOT IN ('C','H')) v ON s.VehicleInventoryItemID = v.VehicleInventoryItemID   
WHERE s.category = 'RMFlagWTF'
ORDER BY VehicleInventoryItemID, fromts 

SELECT status, COUNT(*) FROM VehicleInventoryItemStatuses GROUP BY status

SELECT *
FROM viiwtf w
WHERE NOT EXISTS (
SELECT 1
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
  AND category = 'RMFlagWTF')  
  
-- 8/7/14
-- simply: each day, how may vehicles were IN ry1 inspection pending  

SELECT a.thedate, count(*) --b.*
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b on CAST(b.fromTS AS sql_date) <= a.thedate
  AND CAST(coalesce(b.thruTS, CAST('12/31/9999 00:00:00' AS sql_timestamp)) AS sql_date) > a.thedate
  AND b.category = 'RMFlagIP'
INNER JOIN VehicleInventoryItems c on b.VehicleInventoryItemID = c.VehicleInventoryItemID
  AND c.owningLocationID = (
    SELECT partyid
    FROM organizations
    WHERE name = 'rydells')    
WHERE a.thedate BETWEEN curdate() - 365 AND curdate()  
  AND dayOfWeek = 6
AND NOT EXISTS (
  SELECT 1 
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = b.VehicleInventoryItemID
    AND category IN ('RMFlagTNA', 'RMFlagPIT')
    AND CAST(coalesce(thruTS, CAST('12/31/9999 00:00:00' AS sql_timestamp))  AS sql_date) > a.thedate) 
GROUP BY theDate    

