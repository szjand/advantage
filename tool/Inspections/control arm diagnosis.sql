SELECT b.stocknumber, c.vin, c.yearmodel, c.make, c.model,
  a.description, 
  CASE
    WHEN f.VehicleReconItemID IS NOT NULL THEN 'Completed'
    ELSE 'NOT Authorized'
  END AS Disposition,
  CAST(b.fromts AS sql_date) AS Date,
  Utilities.CurrentViiSalesStatus(b.VehicleInventoryItemID)
FROM VehicleReconItems a
LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN VehicleItems c ON b.VehicleItemID = c.VehicleItemID 
LEFT JOIN AuthorizedReconItems f ON a.VehicleReconItemID = f.VehicleReconItemID 
  AND status = 'AuthorizedReconItem_Complete'
WHERE description LIKE '%arm%' AND description LIKE '%cont%'



SELECT month(ts), year(ts), COUNT(*)
-- SELECT COUNT(*)
FROM VehicleReconItems a
LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN VehicleItems c ON b.VehicleItemID = c.VehicleItemID 
LEFT JOIN AuthorizedReconItems f ON a.VehicleReconItemID = f.VehicleReconItemID 
  AND status = 'AuthorizedReconItem_Complete'
WHERE description LIKE '%arm%' AND description LIKE '%cont%'
GROUP BY month(ts), year(ts)
ORDER BY year(ts) DESC , month(ts) DESC


