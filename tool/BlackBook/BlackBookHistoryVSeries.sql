select te.acv, te.adjustedacv, te.finished, 
	te.reconbody + te.reconmechanical + te.reconappearance as Recon, 
	te.appeal, te.desirability,
	case te.Appeal
  	when '1' then (te.WholesaleRough + te.RoughAdjustment + te.AddDeductAmount - (te.reconbody + te.reconmechanical + te.reconappearance))
	  when '2' then (te.WholesaleAverage + te.AverageAdjustment + te.AddDeductAmount- (te.reconbody + te.reconmechanical + te.reconappearance))
	  when '3' then (te.WholesaleClean + te.CleanAdjustment + te.AddDeductAmount- (te.reconbody + te.reconmechanical + te.reconappearance))
	end as "BBACVAppeal", 
	case te.Desirability
  	when 'Low' then (te.WholesaleRough + te.RoughAdjustment + te.AddDeductAmount - (te.reconbody + te.reconmechanical + te.reconappearance))
	  when 'Medium' then (te.WholesaleAverage + te.AverageAdjustment + te.AddDeductAmount- (te.reconbody + te.reconmechanical + te.reconappearance))
	  when 'High' then (te.WholesaleClean + te.CleanAdjustment + te.AddDeductAmount- (te.reconbody + te.reconmechanical + te.reconappearance))
	end as "BBACVDesirability", 
	te.BlackBookDate, bb.BlackBookPubDate
from TradeEvaluationsTNA te
inner join VehicleItems vi on te.vin = vi.vin 
inner join VehicleEvaluations ve on vi.VehicleItemID = ve.VehicleItemID
inner join BlackBookResolver bbr on ve.VehicleItemID = bbr.VehicleItemID
inner join BlackBookADT bb on bb.vin = bbr.vin and bb.vinyear = bbr.vinyear 
and bb.uvc = bbr.uvc and te.BlackBookDate = bb.BlackBookPubDate 

 
select 
	case te.Appeal
  	when '1' then (te.WholesaleRough + te.RoughAdjustment + te.AddDeductAmount - (te.reconbody + te.reconmechanical + te.reconappearance))
	  when '2' then (te.WholesaleAverage + te.AverageAdjustment + te.AddDeductAmount- (te.reconbody + te.reconmechanical + te.reconappearance))
	  when '3' then (te.WholesaleClean + te.CleanAdjustment + te.AddDeductAmount- (te.reconbody + te.reconmechanical + te.reconappearance))
	end as "BBACVAppeal", 
	case te.Desirability
  	when 'Low' then (te.WholesaleRough + te.RoughAdjustment + te.AddDeductAmount - (te.reconbody + te.reconmechanical + te.reconappearance))
	  when 'Medium' then (te.WholesaleAverage + te.AverageAdjustment + te.AddDeductAmount- (te.reconbody + te.reconmechanical + te.reconappearance))
	  when 'High' then (te.WholesaleClean + te.CleanAdjustment + te.AddDeductAmount- (te.reconbody + te.reconmechanical + te.reconappearance))
	end as "BBACVDesirability", 
	te.BlackBookDate, bb.BlackBookPubDate,
	
from TradeEvaluationsTNA te
inner join VehicleItems vi on te.vin = vi.vin 
inner join VehicleEvaluations ve on vi.VehicleItemID = ve.VehicleItemID
inner join BlackBookResolver bbr on ve.VehicleItemID = bbr.VehicleItemID
inner join BlackBookADT bb on bb.vin = bbr.vin and bb.vinyear = bbr.vinyear 
and bb.uvc = bbr.uvc and te.BlackBookDate = bb.BlackBookPubDate 

SELECT 
CASE te.appeal
  WHEN '1' then
  	(SELECT bb.WholesaleRough
  	from TradeEvaluationsTNA te
  	inner join VehicleItems vi on te.vin = vi.vin 
  	inner join VehicleEvaluations ve on vi.VehicleItemID = ve.VehicleItemID
  	inner join BlackBookResolver bbr on ve.VehicleItemID = bbr.VehicleItemID
  	inner join BlackBookADT bb on bb.vin = bbr.vin and bb.vinyear = bbr.vinyear 
  	and bb.uvc = bbr.uvc and te.BlackBookDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE convert(ve.VehicleEvaluationTS, SQL_DATE) BETWEEN bbpd.FromDate AND bbpd.ThruDate)
		AND te.TradeEvaluationID = '89ce24a5-0260-46f7-9c20-18ab84837be2')
	WHEN '2' then 
  	(SELECT bb.WholesaleAverage
  	from TradeEvaluationsTNA te
  	inner join VehicleItems vi on te.vin = vi.vin 
  	inner join VehicleEvaluations ve on vi.VehicleItemID = ve.VehicleItemID
  	inner join BlackBookResolver bbr on ve.VehicleItemID = bbr.VehicleItemID
  	inner join BlackBookADT bb on bb.vin = bbr.vin and bb.vinyear = bbr.vinyear 
  	and bb.uvc = bbr.uvc and te.BlackBookDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE convert(ve.VehicleEvaluationTS, SQL_DATE) BETWEEN bbpd.FromDate AND bbpd.ThruDate)
		AND te.TradeEvaluationID = '89ce24a5-0260-46f7-9c20-18ab84837be2')
	WHEN '3' then 
  	(SELECT bb.WholesaleClean
  	from TradeEvaluationsTNA te
  	inner join VehicleItems vi on te.vin = vi.vin 
  	inner join VehicleEvaluations ve on vi.VehicleItemID = ve.VehicleItemID
  	inner join BlackBookResolver bbr on ve.VehicleItemID = bbr.VehicleItemID
  	inner join BlackBookADT bb on bb.vin = bbr.vin and bb.vinyear = bbr.vinyear 
  	and bb.uvc = bbr.uvc and te.BlackBookDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE convert(ve.VehicleEvaluationTS, SQL_DATE) BETWEEN bbpd.FromDate AND bbpd.ThruDate)
		AND te.TradeEvaluationID = '89ce24a5-0260-46f7-9c20-18ab84837be2')
END AS BlackBook,
CASE te.appeal
  WHEN '1' then
  	(SELECT bb.WholesaleRough
  	from TradeEvaluationsTNA te
  	inner join VehicleItems vi on te.vin = vi.vin 
  	inner join VehicleEvaluations ve on vi.VehicleItemID = ve.VehicleItemID
  	inner join BlackBookResolver bbr on ve.VehicleItemID = bbr.VehicleItemID
  	inner join BlackBookADT bb on bb.vin = bbr.vin and bb.vinyear = bbr.vinyear 
  	and bb.uvc = bbr.uvc and  bb.BlackBookPubDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE '05/01/2007' BETWEEN bbpd.FromDate AND bbpd.ThruDate)
		AND te.TradeEvaluationID = '89ce24a5-0260-46f7-9c20-18ab84837be2')
	WHEN '2' then 
  	(SELECT bb.WholesaleAverage
  	from TradeEvaluationsTNA te
  	inner join VehicleItems vi on te.vin = vi.vin 
  	inner join VehicleEvaluations ve on vi.VehicleItemID = ve.VehicleItemID
  	inner join BlackBookResolver bbr on ve.VehicleItemID = bbr.VehicleItemID
  	inner join BlackBookADT bb on bb.vin = bbr.vin and bb.vinyear = bbr.vinyear 
  	and bb.uvc = bbr.uvc and  bb.BlackBookPubDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE '05/01/2007' BETWEEN bbpd.FromDate AND bbpd.ThruDate)
		AND te.TradeEvaluationID = '89ce24a5-0260-46f7-9c20-18ab84837be2')
	WHEN '3' then 
  	(SELECT bb.WholesaleClean
  	from TradeEvaluationsTNA te
  	inner join VehicleItems vi on te.vin = vi.vin 
  	inner join VehicleEvaluations ve on vi.VehicleItemID = ve.VehicleItemID
  	inner join BlackBookResolver bbr on ve.VehicleItemID = bbr.VehicleItemID
  	inner join BlackBookADT bb on bb.vin = bbr.vin and bb.vinyear = bbr.vinyear 
  	and bb.uvc = bbr.uvc and  bb.BlackBookPubDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE '05/01/2007' BETWEEN bbpd.FromDate AND bbpd.ThruDate)
		AND te.TradeEvaluationID = '89ce24a5-0260-46f7-9c20-18ab84837be2')
END AS BlackBookPlus21
FROM TradeEvaluationsTNA te
WHERE TradeEvaluationID = '89ce24a5-0260-46f7-9c20-18ab84837be2'



SELECT AppealElement,
coalesce(sum(CASE WHEN AppealElement = 'ACHeater' THEN 1 end),0) AS ACH,
coalesce(sum(CASE WHEN AppealElement = 'ACHeater' THEN Score end),0) AS ACHScore,
coalesce(sum(CASE WHEN AppealElement = 'Brakes' THEN 1 end),0) AS Brakes,
coalesce(sum(CASE WHEN AppealElement = 'Brakes' THEN Score end),0) AS BrakesScore
FROM AppealElementScores
GROUP BY AppealElement

what i want to see: vin, evaluationdate, bb @ evaluationdate, bb @ 05/01

-- this works but IS slow -- down to 2.5 seconds WHEN a VehicleEvaluationID IS added
SELECT wtf.VIN, wtf.VehicleEvaluationTS, wtf.WholeSaleRough, wtf2.WholeSaleRough 
FROM 
(
SELECT vi.VIN, ve.VehicleEvaluationTS, bbr.VIN AS bbVIN, bbr.VINYear, bbr.uvc, bb.BlackBookPubDate, bb.WholeSaleRough
FROM VehicleItems vi
INNER JOIN VehicleEvaluations ve ON  vi.VehicleItemID = ve.VehicleItemID
INNER JOIN BlackBookResolver bbr ON ve.VehicleItemID = bbr.VehicleItemID
INNER JOIN BlackBookADT bb ON bbr.VIN = bb.VIN 
	AND bbr.VINYear = bb.VINYear
	AND bbr.UVC = bb.UVC
WHERE bb.BlackBookPubDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE CONVERT(ve.VehicleEvaluationTS, SQL_DATE) BETWEEN bbpd.FromDate AND bbpd.ThruDate)
AND VehicleEvaluationID = 'A9A114A4-EF33-4394-9440-E054A066B7D0'
) AS wtf
LEFT OUTER JOIN 
(
SELECT bb.VIN, bb.VINYear, bb.uvc, bb.WholeSaleRough
FROM BlackBookADT bb
WHERE bb.BlackBookPubDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE '05/01/2007' BETWEEN bbpd.FromDate AND bbpd.ThruDate)
) AS wtf2 
ON wtf.uvc = wtf2.uvc
AND wtf.bbVIN = wtf2.VIN
AND wtf.VINYear = wtf2.VINYear

SELECT bb.uvc, COUNT(*)
FROM BlackBookADT bb
WHERE bb.BlackBookPubDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE '05/01/2007' BETWEEN bbpd.FromDate AND bbpd.ThruDate)
GROUP BY bb.uvc


SELECT vi.VIN, ve.VehicleEvaluationTS, bbr.VIN AS bbVIN, bbr.VINYear, bbr.uvc, bb.BlackBookPubDate, bb.WholeSaleRough
FROM VehicleItems vi
INNER JOIN VehicleEvaluations ve ON  vi.VehicleItemID = ve.VehicleItemID
INNER JOIN BlackBookResolver bbr ON ve.VehicleItemID = bbr.VehicleItemID
INNER JOIN BlackBookADT bb ON bbr.VIN = bb.VIN 
	AND bbr.VINYear = bb.VINYear
	AND bbr.UVC = bb.UVC
WHERE bb.BlackBookPubDate = 
		  (SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE CONVERT(ve.VehicleEvaluationTS, SQL_DATE) BETWEEN bbpd.FromDate AND bbpd.ThruDate)
AND VehicleEvaluationID = 'A9A114A4-EF33-4394-9440-E054A066B7D0'		

SELECT BlackBookPubDate
			FROM BlackBookPubDates bbpd
			WHERE '05/04/2007' BETWEEN bbpd.FromDate AND bbpd.ThruDate
			or 
			('05/04/2007' BETWEEN bbpd.FromDate AND bbpd.ThruDate

