SELECT *
FROM blackbookresolver

SELECT v.vin, v.yearmodel, left(v.make, 12), left(v.model, 12), v.bodystyle, c.vinstylename
FROM VehicleItems v
INNER JOIN MakeModelClassifications m ON v.model = m.model
  AND m.VehicleType = 'VehicleType_Pickup'
--  AND m.VehicleType IN ('VehicleType_Pickup', 'VehicleType_SUV')
LEFT JOIN chrome.VinPattern c ON LEFT(v.vin, 8) = LEFT(c.vinpattern, 8)
  AND v.vin LIKE REPLACE(c.vinpattern, '*', '_') 
  AND v.yearmodel = c.year collate ADS_DEFAULT_CI
WHERE v.vinresolved = true  
-- AND v.BodyStyle NOT LIKE '%4%'
  AND v.BodyStyle NOT LIKE '%wd%'
ORDER BY v.make, v.model, v.yearmodel



-- pickups with no *WD IN either chrome OR bb
SELECT v.vin, v.yearmodel, left(v.make, 12), left(v.model, 12), v.bodystyle, c.vinstylename
FROM VehicleItems v
INNER JOIN MakeModelClassifications m ON v.model = m.model
  AND m.VehicleType = 'VehicleType_Pickup'
--  AND m.VehicleType IN ('VehicleType_Pickup', 'VehicleType_SUV')
LEFT JOIN chrome.VinPattern c ON LEFT(v.vin, 8) = LEFT(c.vinpattern, 8)
  AND v.vin LIKE REPLACE(c.vinpattern, '*', '_') 
  AND v.yearmodel = c.year collate ADS_DEFAULT_CI
WHERE v.vinresolved = true  
-- AND v.BodyStyle NOT LIKE '%4%'
  AND v.BodyStyle NOT LIKE '%wd%'
  AND c.vinstylename NOT LIKE '%wd%'
ORDER BY v.make, v.model, v.yearmodel

SELECT v.yearmodel, COUNT(*)
FROM VehicleItems v
INNER JOIN MakeModelClassifications m ON v.model = m.model
  AND m.VehicleType = 'VehicleType_Pickup'
--  AND m.VehicleType IN ('VehicleType_Pickup', 'VehicleType_SUV')
LEFT JOIN chrome.VinPattern c ON LEFT(v.vin, 8) = LEFT(c.vinpattern, 8)
  AND v.vin LIKE REPLACE(c.vinpattern, '*', '_') 
  AND v.yearmodel = c.year collate ADS_DEFAULT_CI
WHERE v.vinresolved = true  
-- AND v.BodyStyle NOT LIKE '%4%'
  AND v.BodyStyle NOT LIKE '%wd%'
  AND c.vinstylename NOT LIKE '%wd%'
GROUP BY v.yearmodel  


-- current inventory
-- AND *WD NOT IN bb
SELECT v.vin, v.yearmodel, left(v.make, 12), left(v.model, 12), v.bodystyle, c.vinstylename
FROM VehicleItems v
INNER JOIN MakeModelClassifications m ON v.model = m.model
  AND m.VehicleType = 'VehicleType_Pickup'
INNER JOIN VehicleInventoryItems i ON v.VehicleItemID = i.VehicleItemID
  AND i.ThruTS IS NULL  
LEFT JOIN chrome.VinPattern c ON LEFT(v.vin, 8) = LEFT(c.vinpattern, 8)
  AND v.vin LIKE REPLACE(c.vinpattern, '*', '_') 
  AND v.yearmodel = c.year collate ADS_DEFAULT_CI  
WHERE v.vinresolved = true  
  AND v.BodyStyle NOT LIKE '%wd%' 
  AND v.BodyStyle NOT LIKE '%4X4%'
