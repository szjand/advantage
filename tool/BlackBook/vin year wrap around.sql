
/*
CREATE PROCEDURE GetBBRecordsFromVIN
   ( 
      VIN Char ( 17 ),
      BlackBookPubDate Date,
      Year Char ( 4 ) OUTPUT,
      Make Char ( 15 ) OUTPUT,
      Model Char ( 20 ) OUTPUT,
      Series Char ( 20 ) OUTPUT,
      BodyStyle Char ( 25 ) OUTPUT,
      EngineDescription Char ( 30 ) OUTPUT,
      GroupNumbers Char ( 4 ) OUTPUT
   ) 
BEGIN 
 
      

drop procedure GetBBRecordsFromVIN 
execute procedure GetBBRecordsFromVIN('2G1FK1EJ3E9153733', '11/04/2013')
*/
    
   declare @VIN Char(17);
   declare @BlackBookPubDate Date;
--   declare @Cursor cursor as select * from [__input];
--   open @Cursor;
--   fetch @Cursor;
--   try
--     @VIN = @Cursor.[VIN];
--	 @BlackBookPubDate = @Cursor.[BlackBookPubDate];
--   finally
--     close @Cursor;
--   end;
@vin = '2G1FK1EJ3E9153733';
@blackbookpubdate = '11/04/2013';
/*
   open @Cursor as
     select * from [BlackBookADT]
       where [VIN] = substring(@VIN, 1, 8) 
	   AND [VINYear] = substring(@VIN, 10, 1)
	   AND [BlackBookPubDate] = @BlackBookPubDate; 
   try
     while fetch @Cursor do
	   insert into [__output]
	     values (@Cursor.Year, @Cursor.Make, @Cursor.Model, coalesce(@Cursor.Series, ''),  
		 @Cursor.BodyStyle, @Cursor.EngineDescription, @Cursor.GroupNumbers); 
   end while;
   finally
     close @Cursor;
   end;
*/   
SELECT 
  CASE
    WHEN substring(@vin, 7, 1) IN ('0','1','2','3','4','5','6','7','8','9') THEN year
    WHEN substring(@vin, 7, 1) NOT IN ('0','1','2','3','4','5','6','7','8','9') AND CAST(year AS sql_integer) < 2010 THEN cast(CAST(year AS sql_integer) + 30 AS sql_char)
  END AS year1,
year, make, model, coalesce(series, ''),bodystyle,enginedescription, groupnumbers
FROM BlackBookADT
WHERE vin = substring(@vin, 1, 8)
  AND vinyear = substring(@vin, 10, 1)
  AND blackbookpubdate = @blackbookpubdate
   
   
--END;



On April 30, 2008, the US National Highway Traffic Safety Administration adopted 
a final rule amending 49 CFR Part 565, 
"so that the current 17 character vehicle identification number (VIN) "
"system, which has been in place for almost 30 years, can continue in use for at least another 30 years", 
in the process making several changes to the VIN requirements applicable to all 
motor vehicles manufactured for sale in the United States. There are three 
notable changes to the VIN structure that affect VIN deciphering systems:
 1.The make may only be identified after looking at positions 1�3 and another position, 
   as determined by the manufacturer in the second section or 4�8 segment of the VIN.
 2.In order to identify exact year in passenger cars and multipurpose passenger 
   vehicles with a GVWR of 10,000 or less, one must read position 7 as well 
   as position 10. For passenger cars, and for multipurpose passenger vehicles 
   and trucks with a gross vehicle weight rating of 10,000 lb (4,500 kg) or less, 
   if position 7 is numeric, the model year in position 10 of the VIN refers to 
   a year in the range 1980�2009. If position 7 is alphabetic, the model year 
   in position 10 of VIN refers to a year in the range 2010�2039.
 3.The model year for vehicles with a GVWR greater than 10,000 lb (4,500 kg), 
   as well as buses, motorcycles, trailers and low speed vehicles may no longer 
   be identified within a 30-year range. VIN characters 1�8 and 10 that were 
   assigned from 1980�2009 can be repeated beginning with the 2010 model year.


A	=	1980		A	=	2010
B	=	1981		B	=	2011
C	=	1982		C	=	2012
D	=	1983		D	=	2013
E	=	1984		E	=	2014
F	=	1985		F	=	2015
G	=	1986		G	=	2016
H	=	1987		H	=	2017
J	=	1988		J	=	2018
K	=	1989		K	=	2019
L	=	1990		L	=	2020
M	=	1991		M	=	2021
N	=	1992		N	=	2022
P	=	1993		P	=	2023
R	=	1994		R	=	2024
S	=	1995		S	=	2025
T	=	1996		T	=	2026
V	=	1997		V	=	2027
W	=	1998		W	=	2028
X	=	1999		X	=	2029
Y	=	2000		Y	=	2030
1	=	2001		1	=	2031
2	=	2002		2	=	2032
3	=	2003		3	=	2033
4	=	2004		4	=	2034
5	=	2005		5	=	2035
6	=	2006		6	=	2036
7	=	2007		7	=	2037
8	=	2008		8	=	2038
9	=	2009		9	=	2039

CREATE TABLE VinYear2039 (
  TenthVinChar cichar(1) constraint NOT NULL,
  SeventhVinCharIsInteger logical constraint NOT NULL,
  theYear cichar(4)constraint NOT NULL,
  constraint PK primary key (TenthVinChar, SeventhVinCharIsInteger,TheYear)) IN database;
insert into vinyear2039 values('A',true,'1980');
insert into vinyear2039 values('B',true,'1981');
insert into vinyear2039 values('C',true,'1982');
insert into vinyear2039 values('D',true,'1983');
insert into vinyear2039 values('E',true,'1984');
insert into vinyear2039 values('F',true,'1985');
insert into vinyear2039 values('G',true,'1986');
insert into vinyear2039 values('H',true,'1987');
insert into vinyear2039 values('J',true,'1988');
insert into vinyear2039 values('K',true,'1989');
insert into vinyear2039 values('L',true,'1990');
insert into vinyear2039 values('M',true,'1991');
insert into vinyear2039 values('N',true,'1992');
insert into vinyear2039 values('P',true,'1993');
insert into vinyear2039 values('R',true,'1994');
insert into vinyear2039 values('S',true,'1995');
insert into vinyear2039 values('T',true,'1996');
insert into vinyear2039 values('V',true,'1997');
insert into vinyear2039 values('W',true,'1998');
insert into vinyear2039 values('X',true,'1999');
insert into vinyear2039 values('Y',true,'2000');
insert into vinyear2039 values('1',true,'2001');
insert into vinyear2039 values('2',true,'2002');
insert into vinyear2039 values('3',true,'2003');
insert into vinyear2039 values('4',true,'2004');
insert into vinyear2039 values('5',true,'2005');
insert into vinyear2039 values('6',true,'2006');
insert into vinyear2039 values('7',true,'2007');
insert into vinyear2039 values('8',true,'2008');
insert into vinyear2039 values('9',true,'2009');
insert into vinyear2039 values('A',false,'2010');
insert into vinyear2039 values('B',false,'2011');
insert into vinyear2039 values('C',false,'2012');
insert into vinyear2039 values('D',false,'2013');
insert into vinyear2039 values('E',false,'2014');
insert into vinyear2039 values('F',false,'2015');
insert into vinyear2039 values('G',false,'2016');
insert into vinyear2039 values('H',false,'2017');
insert into vinyear2039 values('J',false,'2018');
insert into vinyear2039 values('K',false,'2019');
insert into vinyear2039 values('L',false,'2020');
insert into vinyear2039 values('M',false,'2021');
insert into vinyear2039 values('N',false,'2022');
insert into vinyear2039 values('P',false,'2023');
insert into vinyear2039 values('R',false,'2024');
insert into vinyear2039 values('S',false,'2025');
insert into vinyear2039 values('T',false,'2026');
insert into vinyear2039 values('V',false,'2027');
insert into vinyear2039 values('W',false,'2028');
insert into vinyear2039 values('X',false,'2029');
insert into vinyear2039 values('Y',false,'2030');
insert into vinyear2039 values('1',false,'2031');
insert into vinyear2039 values('2',false,'2032');
insert into vinyear2039 values('3',false,'2033');
insert into vinyear2039 values('4',false,'2034');
insert into vinyear2039 values('5',false,'2035');
insert into vinyear2039 values('6',false,'2036');
insert into vinyear2039 values('7',false,'2037');
insert into vinyear2039 values('8',false,'2038');
insert into vinyear2039 values('9',false,'2039');  


declare @VIN Char(17);
declare @BlackBookPubDate Date;

@vin = '3GCUKSEC4EG107047';
@blackbookpubdate = '11/04/2013';

SELECT 
  (SELECT theyear 
    FROM VinYear2039
    WHERE TenthVinChar = substring(@vin,10,1) 
      AND SeventhVinCharIsInteger = 
        CASE 
          WHEN substring(@vin,7,1) IN ('0','1','2','3','4','5','6','7','8','9') THEN true
          ELSE false
        END) AS year1, 
year, make, model, coalesce(series, ''),bodystyle,enginedescription, groupnumbers
FROM BlackBookADT
WHERE vin = substring(@vin, 1, 8)
  AND vinyear = substring(@vin, 10, 1)
  AND blackbookpubdate = @blackbookpubdate