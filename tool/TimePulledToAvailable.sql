
/*
SELECT stocknumber,
  (SELECT top 1 FromTS
    FROM VehicleInventoryItemStatuses
    WHERE c
	ategory = 'RMFlagPulled'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID) AS Pulled,
  (SELECT top 1 FromTS
    FROM VehicleInventoryItemStatuses
    WHERE category = 'RMFlagAV'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID) AS Available,	
  timestampdiff(sql_tsi_day,
  (SELECT top 1 FromTS
    FROM VehicleInventoryItemStatuses
    WHERE category = 'RMFlagPulled'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID),
  (SELECT top 1 FromTS
    FROM VehicleInventoryItemStatuses
    WHERE category = 'RMFlagAV'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID))    
FROM VehicleInventoryItems viix
WHERE EXISTS (
  SELECT 1 
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
  AND category = 'RMFlagPulled')
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses  
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND category = 'RMFlagAV')  
AND viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
AND CAST(viix.fromts AS sql_date) > '12/31/2010'

-- how many become available per day
SELECT CAST(fromts AS sql_date), COUNT(*)
FROM VehicleInventoryItemStatuses
WHERE CAST(fromts AS sql_date)  > '12/31/2010'
AND category = 'RMFlagAV'
GROUP BY CAST(fromts AS sql_date)
ORDER BY CAST(fromts AS sql_date) DESC 

-- VehicleInventoryItems that became available ON 3/17/2011
SELECT VehicleInventoryItemID
FROM VehicleInventoryItemStatuses
WHERE category = 'RMFlagAV'
AND CAST(fromts AS sql_date) = '3/17/2011'

-- there may be multiple trips FROM raw to available
-- ?? SELECT just the most recent ??
-- ?? SELECT just the sequence of events that proceed ON a timeline ?
-- exclude VehicleInventoryItems WHERE there EXISTS a record IN vehiclesales
-- 		   which seems to help		   
-- VehicleInventoryItems that became available ON 3/17/2011
/* available vehicles with cancelled sales
SELECT viix.stocknumber
FROM VehicleInventoryItems viix
WHERE ThruTS IS NULL
AND EXISTS (
  SELECT 1
  FROM vehiclesales
  WHERE VehicleInventoryItemID  = viix.VehicleInventoryItemID)
*/  
SELECT distinct
  viix.stocknumber, CAST(viix.fromts AS sql_date) AS Acquired, 
  CAST(vinsp.VehicleInspectionTS AS sql_Date) AS Inspect,
  CAST(vwx.VehicleWalkTS AS sql_date) AS Walk,
  CAST(pull.FromTS AS sql_date) AS Pulled,
  CAST(mdisp.FromTS AS sql_date) AS MechDisp,
  CAST(bdisp.FromTS AS sql_date) AS BodyDisp,
  CAST(adisp.FromTS AS sql_date) AS AppDisp,
  CAST(pb.FromTS AS sql_date) AS PricingBuffer,  
  CAST(av.FromTS AS sql_date) AS Available,
  vtm.fromts, vtm.thruts,
  timestampdiff(sql_tsi_hour, vtm.fromts, vtm.thruts) AS "Hours ON Move List",
  
  CASE 
    WHEN vtm.fromts BETWEEN pull.fromts AND bdisp.fromts THEN 'Mech'
	WHEN vtm.fromts BETWEEN bdisp.fromts AND adisp.fromts THEN 'Body'
	WHEN vtm.fromts BETWEEN adisp.fromts AND pb.fromts THEN 'App'
  END AS "Waiting for Move"
FROM VehicleInventoryItems viix
INNER JOIN (
  SELECT VehicleInventoryItemID, fromts
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagAV'
  AND ThruTS IS NULL) av ON viix.VehicleInventoryItemID = av.VehicleInventoryItemID 
LEFT JOIN VehicleInspections vinsp ON viix.VehicleInventoryItemID = vinsp.VehicleInventoryItemID   
LEFT JOIN VehicleWalks vwx ON viix.VehicleInventoryItemID = vwx.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagPulled') pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagPB') pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts
  FROM VehicleInventoryItemStatuses
  WHERE category = 'MechanicalReconDispatched') mdisp ON viix.VehicleInventoryItemID = mdisp.VehicleInventoryItemID
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts
  FROM VehicleInventoryItemStatuses
  WHERE category = 'BodyReconDispatched') bdisp ON viix.VehicleInventoryItemID = bdisp.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts
  FROM VehicleInventoryItemStatuses
  WHERE category = 'AppearanceReconDispatched') adisp ON viix.VehicleInventoryItemID = adisp.VehicleInventoryItemID         
LEFT JOIN VehiclesToMove vtm ON viix.VehicleInventoryItemID = vtm.VehicleInventoryItemID  
  AND vtm.status <> 'VehicleMove_Cancelled' 
  AND vtm.Fromts > pull.fromts
WHERE viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells') 
AND viix.ThruTS IS null 
AND av.VehicleInventoryItemID IS NOT NULL 
-- AND CAST(av.FromTS AS sql_date) = '03/17/2011'
-- AND vinsp.VehicleInventoryItemID IS NOT NULL 
AND NOT EXISTS ( -- exclude backons/cancelled sales
  SELECT 1
  FROM VehicleSales
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID)
ORDER BY av.fromts DESC, vtm.fromts 

