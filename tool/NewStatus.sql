DECLARE @MarketID string;
@MarketID = 'A4847DFC-E00E-42E7-89EE-CD4368445A82';
SELECT 
  SUM(CASE WHEN status = 'RawMaterials_BookingPending' THEN 1 else 0 END) AS BP,
  SUM(CASE WHEN category = 'RMFlagPIT' THEN 1 END )AS "IN Transit", 
  SUM(CASE WHEN category = 'RMFlagTNA' THEN 1 END )AS "Trade Buffer",
--  SUM(CASE WHEN category = 'RMFlagIP' AND NOT EXISTS ( -- this IS WAY too slow
--    SELECT 1
--	FROM VehicleInventoryItemStatuses
--	WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID
--      AND (status = 'RMFlagTNA_TradeNotAvailable' OR status = 'RMFlagPIT_PurchaseInTransit')) THEN 1 END) AS InspPen,	
  ( --InspectionPending
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagIP_InspectionPending'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND (status = 'RMFlagTNA_TradeNotAvailable' OR status = 'RMFlagPIT_PurchaseInTransit') 
      AND ThruTS IS NULL)) AS InspectionPending,
  SUM(CASE WHEN category = 'RMFlagIP' AND status NOT IN ('RMFlagTNA_TradeNotAvailable','RMFlagPIT_PurchaseInTransit') THEN 1 END) AS InspPend,
  SUM(CASE WHEN category = 'RMFlagRMP' THEN 1 END )AS RMP,
--  SUM(CASE WHEN category = 'RMFlagRB' 
  SUM(CASE WHEN category = 'RMFlagAV' THEN 1 END )AS Available,
  SUM(CASE WHEN category = 'RMFlagRB' and category <> 'RMFlagTNA' THEN 1 END) AS wp

FROM VehicleInventoryItemStatuses viisx
WHERE ThruTS IS NULL
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItems
  WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND OwningLocationID IN (
   SELECT pr.PartyID2
   FROM PartyRelationships pr
   WHERE pr.Typ = 'PartyRelationship_MarketLocations'
   AND pr.PartyID1 = @MarketID
   AND pr.ThruTS IS NULL))  


-- 12440xxa
SELECT viix.stocknumber
-- SELECT COUNT(*)
FROM VehicleInventoryItemStatuses viisx
LEFT JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
--WHERE category = 'RMFlagIP'
WHERE viisx.status = 'RMFlagIP_InspectionPending'
AND not EXISTS (
  select 1
  from VehicleInventoryItemStatuses 
  where status in ('RMFlagTNA_TradeNotAvailable', 'RMFlagPIT_PurchaseInTransit')
  AND ThruTS IS NULL
  AND VehicleInventoryItemID = viisx.VehicleInventoryItemID )
AND viisx.ThruTS IS NULL 
ORDER BY viix.stocknumber

SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = 'ae9b02cf-cb47-4245-881f-8442a75a31f5'

/*
SELECT * FROM VehicleInventoryItemStatuses WHERE status = 'RawMaterials_BookingPending' AND thruts IS NULL
SELECT * FROM VehicleInventoryItems WHERE VehicleInventoryItemID = 'a8572530-76af-4549-86ce-b7618ebca010'
select * FROM people WHERE partyid = '8E998EAC-EFAD-4AFB-8998-2B67E4894AFD'

SELECT parent FROM system.columns WHERE name = 'VehicleInventoryItemID'


-- ok got a goofy one, status EXISTS of RMBookPending, but no VehicleInventoryItem
-- does the VehicleInventoryItemID show up anywhere else?
DECLARE @VehicleInventoryItemTables CURSOR AS -- Tables that include VehicleInventoryItemID
  SELECT parent
  FROM system.columns
  WHERE name = 'VehicleInventoryItemID'
  AND parent NOT LIKE 'z%';
DECLARE @TableName string; 
DECLARE @VehicleInventoryItemID string;
DECLARE @Stmt string;
SELECT Utilities.DropTablesIfExist('#jon') FROM system.iota; 
OPEN @VehicleInventoryItemTables;
CREATE TABLE #jon (
  tablename CIChar(100));
TRY
  WHILE FETCH @VehicleInventoryItemTables DO
    @TableName = trim(@VehicleInventoryItemTables.parent);
--    @Stmt = 'DELETE FROM ' + '' + @TableName + '' + ' WHERE VehicleInventoryItemID = ' + '''' + @VehicleInventoryItemID + '''';
--    EXECUTE IMMEDIATE @Stmt;
    @Stmt = 'insert INTO #jon
    INSERT INTO #jon values(@TableName);
  END WHILE;
FINALLY
  CLOSE @VehicleInventoryItemTables;
END TRY;
SELECT * FROM #jon;
*/