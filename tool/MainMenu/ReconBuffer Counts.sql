/*
6/22/12
Recon Buffer
*/
-- sp.GetUsedCarsMainMenuSummaryByLocation
-- GROUP count
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRB_ReconBuffer'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status LIKE '%ReconProcess_InProcess'
      AND ThruTS IS NULL)    
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND OwningLocationID IN (
        SELECT pr.PartyID2
	    FROM PartyRelationships pr
        WHERE pr.Typ = 'PartyRelationship_MarketLocations'
        AND pr.PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82'
        AND pr.ThruTS IS NULL))

-- sp.GetUsedCarsMainMenuSummaryByMarket
-- dept counts 
     
--RBBody (RB + not InProcess + Disp to Body)
    SELECT COUNT(status) -- 1
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRB_ReconBuffer'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status LIKE '%ReconProcess_InProcess'
      AND ThruTS IS NULL)     
    AND EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND status = 'BodyReconDispatched_Dispatched'
      AND ThruTS IS NULL)
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND OwningLocationID IN (
        SELECT pr.PartyID2
	    FROM PartyRelationships pr
        WHERE pr.Typ = 'PartyRelationship_MarketLocations'
        AND pr.PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82'
        AND pr.ThruTS IS NULL))
-- RBAppearance (RB + not InProcess + Disp to Body)
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRB_ReconBuffer'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND status = 'AppearanceReconDispatched_Dispatched'
      AND ThruTS IS NULL)
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status LIKE '%ReconProcess_InProcess'
      AND ThruTS IS NULL)   
    AND EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND status = 'AppearanceReconDispatched_Dispatched'
      AND ThruTS IS NULL)          
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND OwningLocationID IN (
        SELECT pr.PartyID2
	    FROM PartyRelationships pr
        WHERE pr.Typ = 'PartyRelationship_MarketLocations'
        AND pr.PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82'
        AND pr.ThruTS IS NULL))
  ( -- RBMechanical (RB + not InProcess + Disp to Mech)
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRB_ReconBuffer'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status LIKE '%ReconProcess_InProcess'
      AND ThruTS IS NULL)     
      AND EXISTS (
        SELECT 1 
        FROM VehicleInventoryItemStatuses
        WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
        AND status = 'MechanicalReconDispatched_Dispatched'
        AND ThruTS IS NULL)  
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND OwningLocationID IN (
        SELECT pr.PartyID2
	    FROM PartyRelationships pr
        WHERE pr.Typ = 'PartyRelationship_MarketLocations'
        AND pr.PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82'
        AND pr.ThruTS IS NULL))    