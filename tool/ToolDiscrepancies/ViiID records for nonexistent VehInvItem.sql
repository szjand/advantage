/*
***************** VehicleReconItems for non existent VehicleInventoryItems **************

SELECT DISTINCT VehicleInventoryItemID  -- '3d70957c-9d14-9e41-ba1b-f2c5d711fbc5' 'cf9c2612-5c82-412d-bd18-9bda41b2ff8e'
FROM VehicleReconItems vrix
WHERE NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItems
  WHERE VehicleInventoryItemID = vrix.VehicleInventoryItemID)
  
-- VehicleInventoryItem tables


so DO i stop, clean these up
AND possible redo sp.DELETEVehicleInventoryItem 
???
1. just DELETE the relevant VehicleReconItems * ReconAuthorizations


i want to know which of the VehicleInventoryItem tables have records for nonexistent VehicleInventoryItems 
which IS ok once i have a known bad VehicleInventoryItemID ,
more generally
i need to know what are the bad VehicleInventoryItemIDs
  any VehicleInventoryItemID for which there IS no record IN VehicleInventoryItems 

SELECT parent
FROM system.columns
WHERE name = 'VehicleInventoryItemID' 
AND parent NOT LIKE 'z%'  
ORDER BY parent

SELECT * FROM ReconAuthorizations   WHERE VehicleInventoryItemID IN ('3d70957c-9d14-9e41-ba1b-f2c5d711fbc5', 'cf9c2612-5c82-412d-bd18-9bda41b2ff8e')

  
CREATE TABLE zzjon (
  tablename cichar(100),
  VehInvItemID char(38)) IN database;
*/ 
  
-- here's the answer for a given VehicleInventoryItemID 

DECLARE @VehicleInventoryItemID string;
DECLARE @Stmt string;
DECLARE @List string;
DECLARE @i integer;
DECLARE @Tablename string;
DECLARE @cur CURSOR AS
  SELECT parent
  FROM system.columns
  WHERE name = 'VehicleInventoryItemID' 
  AND parent NOT LIKE 'z%';

@VehicleInventoryItemID = 'cf9c2612-5c82-412d-bd18-9bda41b2ff8e'; 
@List = '';
OPEN @cur;
TRY
  WHILE FETCH @Cur DO
    @Tablename = @Cur.parent;
	
/* -- identify the tables
    @Stmt = 'insert INTO zzjon SELECT ' + '''' + @Tablename + '''' + ', ' + '''' + @VehicleInventoryItemID  + '''' + 
	  ' from ' + '' + @Tablename + '' + ' WHERE VehicleInventoryItemID = ' + '''' + @VehicleInventoryItemID + '''';
*/
/* -- DELETE bad VehicleInventoryItemID record FROM tables 
    @Stmt = 'delete FROM ' + '' + @TableName + '' + ' WHERE VehicleInventoryItemID = ' + '''' + @VehicleInventoryItemID + '''';
*/	
/* -- ALL orphan VehicleInventoryItemIDs
    @Stmt = 'insert INTO zzjon SELECT ' + '''' + @Tablename + '''' + ',  t.VehicleInventoryItemID  ' + 
	  ' from ' + '' + @Tablename + '' + ' as t LEFT JOIN VehicleInventoryItems v ON t.VehicleInventoryItemID = v.VehicleInventoryItemID ' +
	  ' WHERE v.VehicleInventoryItemID IS NULL and t.VehicleInventoryItemID IS NOT NULL ';
*/	  

	EXECUTE immediate @Stmt;
  END WHILE;
FINALLY
  CLOSE @Cur;
END TRY;  
    

-- SELECT * FROM zzjon WHERE vehinvitemid = '13d0c69b-a5f7-405c-87e4-42aabbf5fa96'
-- DELETE FROM zzjon
/*
SELECT VehInvItemID, COUNT(*)
FROM zzjon
GROUP BY VehInvItemID

SELECT *
FROM VehicleReconItems t
LEFT JOIN VehicleInventoryItems v ON t.VehicleInventoryItemID = v.VehicleInventoryItemID
WHERE v.VehicleInventoryItemID IS NULL 

*/