SELECT a.vehicleinventoryitemid, a.stocknumber, 
  c.YearModel, c.Make,  c.Model, 
  trim(c.ExteriorColor) + '/' + TRIM(c.InteriorColor) AS Color,
  CASE e.status
        WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
        WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
        WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items' 
  END AS mechanical,
  CASE f.status
        WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
        WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
        WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items' 
  END AS body  
FROM VehicleInventoryItems a  
INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.category = 'RMFlagPulled'
  AND b.ThruTS IS NULL
LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID 
LEFT JOIN ReconAuthorizations d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.thruts IS NULL
LEFT JOIN VehicleInventoryItemStatuses e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
  AND e.category = 'MechanicalReconProcess'
  AND e.thruts IS null
LEFT JOIN VehicleInventoryItemStatuses f on a.VehicleInventoryItemID = f.VehicleInventoryItemID 
  AND f.category = 'BodyReconProcess'
  AND f.thruts IS NULL;

