-- Oranges decoded IN Chrome
SELECT length(vi.vin), vi.vin, vi.yearmodel, vi.make, vi.model, c.*
FROM vehicleitems vi
LEFT JOIN chrome.Vinpattern c ON LEFT(c.vinpattern,8) = LEFT(vi.vin,8) AND vi.vin LIKE replace(c.vinpattern, '*', '_')
WHERE vinresolved = false
ORDER BY length(vi.vin)

-- compare blackbook descriptions AND chrome descriptions
SELECT distinct vi.yearmodel, vi.make, vi.model, vi.trim, vi.bodystyle, vi.engine, c.year, c.vindivisionname, c.vinmodelname, c.vinstylename, c.enginesize, ec.description
FROM vehicleitems vi
LEFT JOIN chrome.Vinpattern c ON LEFT(c.vinpattern,8) = LEFT(vi.vin,8) AND vi.vin LIKE replace(c.vinpattern, '*', '_')
LEFT JOIN chrome.category ec ON c.EngineTypeCategoryID = ec.CategoryID 
WHERE vinresolved = true
-- ORDER BY vi.make, vi.model


DECLARE @vin string;
@vin = '2P4GH45R4SR175197';
SELECT *
FROM VinPattern
WHERE LEFT(VinPattern, 8) = LEFT(@vin, 8)
AND @vin LIKE REPLACE(vinpattern, '*', '_');


