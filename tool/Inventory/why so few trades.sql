
-- 7/9 ------------------------------------------------------------------------
what greg wants        
              (source)  (source)
               used    auction   trade IN                total       new         used        non         auction
year   month   sales   sales     sales      wholesales   trade ins   trade ins   trade ins   auction     trade ins
                                                                                     


/* 
june 2014 retail sales total:
  cross off says 149
  arkona says 152
  tool says 156
*/  

-- dps sales
SELECT yearmonth, COUNT(*) AS usedSales,
  SUM(CASE WHEN typ = 'VehicleSale_Retail' THEN 1 ELSE 0 END) AS retailSales,
  SUM(CASE WHEN typ = 'VehicleSale_Wholesale' THEN 1 ELSE 0 END) AS wsSales
FROM ( 
  SELECT c.stocknumber, CAST(soldTS AS sql_date), typ, yearmonth
  FROM dps.vehicleSales a
  INNER JOIN day b on CAST(a.soldTS AS sql_date) = b.thedate
  INNER JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID
    AND c.owninglocationid = (
      SELECT partyId
      FROM dps.organizations
      WHERE fullname = 'GF-Rydells')
  WHERE status = 'VehicleSale_Sold'
    AND year(CAST(soldTs AS sql_date)) > 2011 ) x 
WHERE yearmonth = 201406    
GROUP BY yearmonth     


-- obviously, dps sale dates and arkona sale dates do not 
-- always agree 
SELECT c.stocknumber, CAST(soldTS AS sql_date), d.bmvin, d.bmdtor, d.bmdtaprv, 
  d.bmdtcap, d.bmdtsv, bmdelvdt 
FROM dps.vehicleSales a
INNER JOIN day b on CAST(a.soldTS AS sql_date) = b.thedate
INNER JOIN dps.vehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID
  AND c.owninglocationid = (
    SELECT partyId
    FROM dps.organizations
    WHERE fullname = 'GF-Rydells')
LEFT JOIN stgArkonaBOPMAST d on c.stocknumber = d.bmstk#    
WHERE status = 'VehicleSale_Sold'
  AND yearmonth = 201406
  AND typ = 'VehicleSale_Retail'
ORDER BY d.bmdelvdt, c.stocknumber 


-- good enuf, orig date (bopmast.bmdtor) AS sale date, counts match arkona deal analysis screen
-- a row only EXISTS IN factVehicleSale IF bopmast.bmstat <> '' ('' = IN process)
-- see Z:\E\DDS2\Scripts\factVehicleSale\first thoughts.sql for more info
DROP TABLE #wtf;
select a.stockNumber, c.thedate AS orDate, d.thedate AS acDate, 
  e.thedate AS capDate, b.saleType, c.yearmonth
-- SELECT COUNT(*)
INTO #wtf
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey
INNER JOIN day c on a.originationDateKey = c.dateKey
INNER JOIN day d on a.cappedDateKey = d.datekey
INNER JOIN day e on cappedDateKey = e.dateKey
WHERE b.vehicleType = 'Used'
--  AND b.saleType <> 'Wholesale'
  AND a.storecode = 'ry1'
  AND c.yearmonth > 201112
  AND a.stocknumber <> ''
  
select * FROM #wtf
-- here are the base sales numbers 
SELECT yearmonth, COUNT(*),
  SUM(CASE WHEN saleType = 'Retail' THEN 1 ELSE 0 END) AS retail, 
  SUM(CASE WHEN saleType = 'Wholesale' THEN 1 ELSE 0 END) AS ws
FROM #wtf
WHERE yearmonth > 201112
GROUP BY yearmonth


  
-- 7/11 break it out more clearly
-- first, trade OR purchase  

-- good enuf, orig date AS sale date, counts match arkona deal analysis screen
-- a row only EXISTS IN factVehicleSale IF bopmast.bmstat <> '' ('' = IN process)
-- see Z:\E\DDS2\Scripts\factVehicleSale\first thoughts.sql for more info

  
  
-- categorize purchases: auction OR non auction


-- categorize trades: 
  -- trade on new car sale
  -- trade on sale purchased vehicle
  --    trade on sale of auction purchase
  --    trade on sale of non auction purchase
  -- trade on sale of a trade

-- the semantics of finer categorization of used car trades IS eluding me
-- for now, new sale vs used sale will be good enough for now to generate
-- gregs initial request, i believe  


  
SELECT saleYearMonth, COUNT(*) AS UsedSales, 
  SUM(CASE WHEN saleType = 'retail' THEN 1 ELSE 0 END) AS totalRetailSales,  
  SUM(CASE WHEN saleType = 'wholesale' THEN 1 ELSE 0 END) AS totalWsSales, 
  SUM(CASE WHEN saleType = 'retail' THEN 1 ELSE 0 END) AS totalRetailSales, 



-- acquisition dte
-- inventory date, NOT sure WHERE i am going to get it
-- NOT IN bopmast 

-- bopvref
-- bvedat (END date) WHERE bvtype = C IS the inventory date on this one
-- but that does NOT WORK for 22329a, inventory date (5/29/14) bvtype = S, AND 
--   IS one of 2 rows with bvtype = S, each with a diff date
-- bopvref.bvkey references bopname.bnkey



FROM arkona wiki regarding the vehicle information screen: 
  Inventory Date - The date the vehicle was received in inventory. Used to 
  calculate the age of the vehicle. Should be entered in mmddyy format, 
  without dashes or slashes. Is the date the GL entries post. Is left blank 
  to enter in-transit, ordered vehicles that have not arrived. If left blank,
  system WILL NOT prompt you to make journal entries. If date entered, 
  accounting month must be opened. Can be changed after stock in if 
  customer wants to �re-age� the vehicles. Is an important sort criteria 
  for Inventory Analysis Reports. For Trade Vehicles, this will be 
  the date the deal was Accepted. 


-- intramarket ws
i am pretty much feeling fucked
vin: '1G1PG5S9XB7130728'
traded IN to ry1 AS 20464B AND sold to alice taylor on 3/15, that stock number
  does NOT exist IN inpmast
subsequently traded IN to honda AND IN inventory AS H6599B

      


-- hmmm, every trade IS generated BY a car deal (bopmast)
ok, this IS good news, IN arkona, boptrad, AND key field references bopmast.bmkey

DROP TABLE extArkonaBOPTRAD;
CREATE TABLE extArkonaBOPTRAD (
  storeCode cichar(3),
  bopmastKey integer, 
  vin cichar(17),
  stocknumber cichar(9),
  constraint PK primary key (bopmastKey, stocknumber));
  

-- just a few WHERE there IS no row IN bopmast based on boptrad.key
SELECT a.*, b.bmstk#, b.bmvin, b.bmdtor
FROM extArkonaBOPTRAD a
LEFT JOIN stgArkonaBOPMAST b on a.bopmastKey = b.bmkey
  AND a.storeCode = b.bmco#
WHERE b.bmco# IS NULL   


-- shit load of NULL factVehicleSale
SELECT aa.*, a.storecode, a.stockNumber, c.thedate AS saleOriginDate, d.thedate AS saleApprovDate, 
  e.thedate AS saleCapDate, b.saleType, c.yearmonth AS saleYearMonth
FROM extArkonaBOPTRAD aa
LEFT JOIN factVehicleSale a on aa.storecode = a.storecode
  and aa.stocknumber = a.stocknumber
LEFT JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey
LEFT JOIN day c on a.originationDateKey = c.dateKey
LEFT JOIN day d on a.approvedDateKey = d.datekey
LEFT JOIN day e on cappedDateKey = e.dateKey
WHERE a.storecode IS NULL 



-- NULL factVehicleSale = inventory
select a.*, c.bmdtor, b.* 
FROM extArkonaBOPTRAD a
LEFT JOIN stgArkonaBOPMAST c on a.storecode = c.bmco# and a.bopmastkey = c.bmkey
LEFT JOIN factVehicleSale b on a.storecode = b.storecode and a.stocknumber = b.stocknumber
WHERE a.storecode = 'ry1'  
ORDER BY c.bmdtor DESC 
-- anomalies this generates: 
-- sometimes the stocknumber IN boptrad never gets updated
/*
  key 23662/ vin 2G1WD57C591291407 key 22996 vin 3GTP2VE73CG141463 : these are both "trade IN process" 
    so it "looks like" these are inventory, but IN fact are NOT
    SELECT * FROM stgArkonaINPMAST WHERE imvin = '2G1WD57C591291407'
    
  21407XXA 5250: stocknumber NOT found
           tool: wholesaled to honda on 1/20/14 (H6594G)
select * FROM stgARkonaBOPVREF a WHERE bvvin = '2G1WT58N089137240'       
select * FROM stgARkonaBOPVREF a left join stgArkonaBOPNAME b on a.bvkey = b.bnkey WHERE bvvin = '2G1WT58N089137240'    
SELECT * FROM stgArkonaINPMAST WHERE imvin = '2G1WT58N089137240'
 
SELECT * FROM stgArkonaGLPTRNS WHERE gtctl# = '21407XXA'  
SELECT * FROM stgArkonaGLPTRNS WHERE gtctl# = 'H6594G'         
*/           



-- used inventory accounts  '124000', '124100', '224000','224100'
-- per jeri, relevant journals are pvu, vsu, vsn, cdh

  
DROP TABLE #gl;
SELECT *
INTO #gl
-- SELECT COUNT(*) 
FROM stgArkonaGLPTRNS
WHERE gtjrnl IN ('pvu', 'vsu', 'vsn', 'cdh')
  AND  gtacct IN ('124000', '124100', '224000','224100')
  AND gtdate > '12/31/2011'
  AND gtpost = 'y'
    
-- cool, per jeri's heads up, this looks LIKE a shit load of non auction purchases
SELECT *
FROM #gl
WHERE gtjrnl = 'pvu'

SELECT *
FROM #gl a
WHERE gtacct IN ('124000', '124100', '224000','224100')
  AND EXISTS (
    SELECT 1 
    FROM #gl
    WHERE gtctl# = a.gtctl#
      AND gtjrnl = 'pvu')
      
-- per jeri, inclu jrnl CDH for street purchases      

-- the notion that gtctl# AND gtdoc# will be different on trade ins

-- trades
select *
FROM (
  SELECT gtctl#, MIN(gtdate) AS gtdate
  FROM #gl 
  WHERE gtctl# <> gtdoc#
    AND gtjrnl IN ('vsn','vsu')
  GROUP BY gtctl#) a
LEFT JOIN extArkonaBOPTRAD b on a.gtctl# = b.stocknumber  
WHERE b.storecode IS NULL 


--< dpsVSeries data -----------------------------------------------------------<
SELECT DISTINCT typ FROM dps.vehicleAcquisitions
SELECT typ, description, COUNT(*) FROM (
select a.VehicleInventoryItemID, a.stocknumber, a.fromTs, a.thruTs, b.typ, c.description
FROM dps.VehicleInventoryItems a
LEFT JOIN dps.VehicleAcquisitions b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN dps.typDescriptions c on b.typ = c.typ
) x GROUP BY typ, description
-- WHERE b.VehicleInventoryItemID IS NULL NOT bad, only 1 VehicleInventoryItems with no acq record 

-- due to the incompleteness of typDescriptions, AND the fact that we never
--   correctly implemented a street purchase process (ie, street purchases
--   generate an acq tgyp of auction purchase
-- The only categorization i am going to generate FROM this IS trade/purchase

/*
supercede dps source with boptrade, IF it EXISTS IN boptrad, it IS a trade
suffix P, should ALL be purchases
*/
-- DROP TABLE #wtf1;
/* 
acquisitions based on dps.VehicleInventoryItems, with source coerced BY boptrad
  (IF the stocknumber EXISTS IN boptrad, THEN source = trade)
force ALL P units to be source = purchase  
exclude Y AS part of suffix, has nothing to DO with source, etc, simply a 
  damaged title flag
*/
select a.stocknumber, 
  -- remove ALL numbers & Y FROM the stocknumber
  replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9',''), 'Y','') AS suffix,      
  CASE
    WHEN TRIM(
      replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
        (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
          '4',''),'5',''),'6',''),'7',''),'8',''),'9',''), 'Y','')) = '' THEN a.stockNumber
    ELSE replace(a.stockNumber, trim(
      replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
        (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
          '4',''),'5',''),'6',''),'7',''),'8',''),'9',''), 'Y','')), '') 
  END AS baseStockNumber,         
  cast(a.fromTs AS sql_date) AS dpsAcquisitionDate, 
  coalesce(CAST(g.soldTS AS sql_date), CAST('12/31/9999' AS sql_date)) AS dpsSaleDate,
  CASE -- IF the stocknumber EXISTS IN boptrad, it IS a trade
    WHEN h.storeCode IS NOT NULL THEN 'trade'
    ELSE 
      CASE b.typ
        WHEN 'VehicleAcquisition_Trade' THEN 'trade'
        ELSE 'purchase'
      END 
  END AS source,
  c.yearmodel AS modelYear, c.vin
--  c.yearmodel AS modelYear, c.vin, c.make, c.model,
--  e.description AS vehicleType, f.description AS vehicleSegment
INTO #wtf1  
FROM dps.VehicleInventoryItems a
LEFT JOIN dps.VehicleAcquisitions b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN dps.VehicleItems c on a.VehicleItemID = c.VehicleItemID 
LEFT JOIN dps.MakeModelClassifications d on c.make = d.make AND c.model = d.model
LEFT JOIN dps.typDescriptions e on d.vehicleType = e.typ
LEFT JOIN dps.typDescriptions f on d.vehicleSegment = f.typ
LEFT JOIN dps.vehicleSales g on a.VehicleInventoryItemID = g.VehicleInventoryItemID 
  AND g.status = 'VehicleSale_Sold'
LEFT JOIN extArkonaBOPTRAD h on a.stocknumber = h.stocknumber  
WHERE year(a.fromTS) BETWEEN 2012 AND 2014;
-- fix P units
UPDATE #wtf1
SET source = 'purchase'
WHERE suffix = 'p'
  AND source = 'trade';

SELECT * FROM #wtf1
  
-- DROP TABLE #suffix;
SELECT suffix, COUNT(*) AS howMany, --  MIN(stocknumber), MAX(stocknumber) 
  MIN(CASE WHEN left(a.stocknumber, 1) = 'h' THEN stocknumber END),
  MAX(CASE WHEN left(a.stocknumber, 1) = 'h' THEN stocknumber END),
  MIN(CASE WHEN left(a.stocknumber, 1) <> 'h' THEN stocknumber END),
  MAX(CASE WHEN left(a.stocknumber, 1) <> 'h' THEN stocknumber END)
INTO #suffix
FROM #wtf1 a 
WHERE LEFT(stocknumber, 1) <> 'c'
GROUP BY suffix
ORDER BY COUNT(*) DESC 

-- DROP TABLE #suffix_source;
SELECT suffix, source, COUNT(*) AS howMany, --  MIN(stocknumber), MAX(stocknumber) 
  MIN(CASE WHEN left(a.stocknumber, 1) = 'h' THEN stocknumber END),
  MAX(CASE WHEN left(a.stocknumber, 1) = 'h' THEN stocknumber END),
  MIN(CASE WHEN left(a.stocknumber, 1) <> 'h' THEN stocknumber END),
  MAX(CASE WHEN left(a.stocknumber, 1) <> 'h' THEN stocknumber END)
INTO #suffix_source  
FROM #wtf1 a 
WHERE LEFT(stocknumber, 1) <> 'c'
GROUP BY suffix, source
ORDER BY COUNT(*) DESC 

-- anomalies IN source
1.
-- fixed BY superceding dps source with trade WHERE the stocknumber EXISTS IN boptrad
SELECT * 
-- SELECT COUNT(*) -- only 1 goofy one
FROM #wtf1 WHERE suffix = 'a' AND source = 'purchase'
2. 
-- some suffix = P are trades some are purchase
-- fixed BY updating ALL P units to purchase
SELECT suffix, source, COUNT(*), --  MIN(stocknumber), MAX(stocknumber) 
  MIN(CASE WHEN left(a.stocknumber, 1) = 'h' THEN stocknumber END),
  MAX(CASE WHEN left(a.stocknumber, 1) = 'h' THEN stocknumber END),
  MIN(CASE WHEN left(a.stocknumber, 1) <> 'h' THEN stocknumber END),
  MAX(CASE WHEN left(a.stocknumber, 1) <> 'h' THEN stocknumber END)
FROM #wtf1 a
WHERE LEFT(stocknumber, 1) <> 'c'
  AND suffix = 'p'
GROUP BY suffix, source
 
3.
-- this one IS sketchy, vin can exist multiple times IN boptrad, does NOT
-- mean it IS the same trade
/*
select count(*) -- 260
from (SELECT vin, COUNT(*) FROM extArkonaBOPTRAD GROUP BY vin HAVING COUNT(*) > 1) X
*/
select a.stocknumber, b.stocknumber
-- SELECT COUNT(*) -- 1301
FROM #wtf1 a
LEFT JOIN extArkonaBOPTRAD b on a.vin = b.vin
WHERE a.stocknumber <> b.stocknumber
4.
-- storecode based on ownlinglocationid vs stocknumber
-- only 4 rows
SELECT *
FROM (
  SELECT stocknumber, fullname, name, CAST(a.fromTS AS sql_date)
  FROM dps.VehicleInventoryItems a
  LEFT JOIN dps.organizations b on a.owninglocationid = b.partyid
  WHERE CAST(a.fromTS AS sql_date) > '12/31/2011') X
WHERE (
  (name = 'rydells' AND LEFT(stocknumber, 1) NOT IN ('1','2','3','4','9'))
  OR
  (name = 'honda cartiva' AND LEFT(stocknumber, 1) <> 'h'))
5.
-- classified AS source purchase, but stocknumber EXISTS IN boptrad  
-- ALL but one fixed BY above fixes
SELECT a.stocknumber, a.dpsAcquisitionDate, a.vin, b.*
-- SELECT COUNT(*) -- 1
FROM #wtf1 a
LEFT JOIN extArkonaBOPTRAD b on a.stocknumber = b.stocknumber
WHERE source = 'purchase'
  AND b.storecode IS NOT NULL 
6. 
-- source shows trade, no row (stocknumber) IN BOPTRAD 
SELECT *
-- SELECT COUNT(*) -- 345
FROM #wtf1 a
WHERE source = 'trade'
  AND NOT EXISTS (
    SELECT 1
    FROM extArkonaBOPTRAD
    WHERE stocknumber = a.stocknumber)  

7.
-- sales, arkona shows 206, this shows 270 
SELECT COUNT(*)
FROM #wtf1
WHERE dpsSaleDate BETWEEN '06/01/2014' AND '06/30/2014'
8.
SELECT * FROM factVehicleSale
-- sold vehicles without a stocknumber match IN factVehicleSale
select a.stocknumber, a.vin, a.dpsacquisitionDate, a.dpsSaleDate, a.source 
-- SELECT COUNT(*) -- 717
FROM #wtf1 a
LEFT JOIN factVehicleSale b on a.stocknumber = b.stocknumber
WHERE a.dpsSaleDate < curdate()
  AND b.storecode IS NULL 
  AND LEFT(a.stocknumber, 1) <> 'c'
  
SELECT * FROM factVehicleSale WHERE stocknumber = '21748b'  
  
select a.stocknumber, a.vin, a.dpsacquisitionDate, a.dpsSaleDate, a.source   
-- SELECT COUNT(*) -- 95
FROM #wtf1 a
LEFT JOIN dimVehicle aa on a.vin = aa.vin
LEFT JOIN factVehicleSale b on aa.vehicleKey = b.vehicleKey
WHERE a.dpsSaleDate < curdate()
  AND b.storecode IS NULL   
  AND LEFT(a.stocknumber, 1) <> 'c'



--/> dpsVSeries data ----------------------------------------------------------/>
get a date on boptrad (bopmast.origdate)

SELECT * FROM factVehicleSale
-- looks LIKE these are the "pending" trades, mostly
-- these DO NOT show up IN factVehicleSales because the deal IS incomplete, 
-- these DO NOT have stocknumbers
SELECT a.*, c.thedate 
-- SELECT COUNT(*) -- 146
FROM extArkonaBOPTRAD a
LEFT JOIN factVehicleSale b on a.storecode = b.storecode and a.bopmastKey = b.bmkey
LEFT JOIN day c on b.originationDateKey = c.datekey
WHERE c.thedate IS NULL 
  AND right(TRIM(vin), 3) = right(TRIM(a.stocknumber), 3)

-- these have stocknumber  
SELECT a.*, c.thedate 
-- SELECT COUNT(*) -- 641
FROM extArkonaBOPTRAD a
LEFT JOIN factVehicleSale b on a.storecode = b.storecode and a.bopmastKey = b.bmkey
LEFT JOIN day c on b.originationDateKey = c.datekey
WHERE c.thedate IS NULL 
  AND right(TRIM(vin), 3) <> right(TRIM(a.stocknumber), 3)
  

-- yep, bopmast.bmstat = '' (except, of course, for crookston deals) 
SELECT a.*, c.thedate, d.bmco#, d.bmstat, d.bmdtor 
-- SELECT COUNT(*) -- 787
FROM extArkonaBOPTRAD a
LEFT JOIN factVehicleSale b on a.storecode = b.storecode and a.bopmastKey = b.bmkey
LEFT JOIN day c on b.originationDateKey = c.datekey
LEFT JOIN stgArkonaBOPMAST d on a.storecode = d.bmco# and a.bopmastKey = d.bmkey
WHERE c.thedate IS NULL   

-- this IS the subset of boptrad that needs to be balanced with dps.acquisitions
SELECT a.*, c.thedate
FROM extArkonaBOPTRAD a
LEFT JOIN factVehicleSale b on a.bopmastKey = b.bmkey
LEFT JOIN day c on b.originationDateKey = c.datekey
WHERE c.thedate BETWEEN '01/01/2012' AND curdate()

-- NOT too bad
SELECT a.*, c.thedate, d.*
FROM extArkonaBOPTRAD a
LEFT JOIN factVehicleSale b on a.storecode = b.storecode and a.bopmastKey = b.bmkey
LEFT JOIN day c on b.originationDateKey = c.datekey
LEFT JOIN #wtf1 d on a.stocknumber = d.stocknumber
WHERE c.thedate BETWEEN '01/01/2012' AND curdate()

/********* intra market wholesales *******************************************/

reminders:
  dps.vehicleAcquisitions: does NOT distinguish BETWEEN auction 
    AND non auction purchases consistently

h3577a -> 15722x -> h4427g

initially looks LIKE honda acq gm cars get a suffix of G
gm acq honda cars get a suffix of X

this IS getting INTO mind fuckville sorting out the notion of a vehicles lineage (wash list)


SELECT * FROM dps.vehicleacquisitions

SELECT a.typ, COUNT(*), MAX(a.acquiredFromPArtyID), MIN(b.stocknumber), MAX(b.stocknumber)
FROM dps.vehicleAcquisitions a
LEFT JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
GROUP BY a.typ

select left(b.name, 15) AS acqdFrom, c.stocknumber, CAST(c.fromTS AS sql_date), d.*
FROM dps.vehicleacquisitions a 
LEFT JOIN dps.organizations b on a.acquiredFromPartyID = b.partyid
LEFT JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN dps.VehicleSales d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
WHERE lower(a.typ) LIKE '%intra%'
  AND year(c.fromTS) > 2012


-- dps sales with sale type, acquisition info
SELECT a.VehicleInventoryItemID, c.stocknumber, 
  CAST(c.fromTS AS sql_date) AS dpsAcqDate,
  CAST(soldts AS sql_date) AS dpsSaleDate, 
  LEFT(e.description, 12) AS saleType, left(f.description, 24) AS acqType, 
  left(d.name, 15) AS acqFrom, 
  g.*
FROM dps.vehicleSales a
LEFT JOIN dps.vehicleAcquisitions b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN dps.organizations d on b.acquiredFromPartyID = d.partyID
LEFT JOIN dps.typDescriptions e on a.typ = e.typ
LEFT JOIN dps.typDescriptions f on b.typ = f.typ
LEFT JOIN #wtf1 g on c.stocknumber = g.stocknumber
WHERE year(a.soldts) > 2012


-- 7/24
arkona trades acquisition date

DROP TABLE #arkTrades;
SELECT a.*, b.bmdtor AS arkAcqDate, b.bmstk# AS tradedOnSaleOf 
INTO #arkTrades
FROM extArkonaBOPTRAD a
LEFT JOIN stgArkonaBOPMAST b on a.storecode = b.bmco#
  AND a.bopmastKey = b.bmkey
WHERE b.bmco# IS NOT NULL  -- eliminate the damned few "trades" with no bopmast row
  AND year(bmdtor) > 2011 -- 
  AND bmstat <> '' -- eliminates pending trades

-- goofy stocknumbers IN boptrad, some "K", some have temporary stocknumber (END of vin)
--   AND was never updated, eg   
select * FROM #arkTrades
WHERE position(trim(tradedOnSaleOf) IN trim(stocknumber)) = 0
  AND position('a' IN stocknumber) = 0
  AND position('b' IN stocknumber) = 0
  AND position('c' IN stocknumber) = 0
  AND position('d' IN stocknumber) = 0
  AND position('e' IN stocknumber) = 0
  AND position('f' IN stocknumber) = 0


/* one of the probems IS crookston, OPEN ALL of 2012, "lots" of intra market */

the intramarket problem

SELECT suffix, source, COUNT(*)
FROM #wtf1
WHERE length(TRIM(suffix)) = 1
GROUP BY suffix, source

select * FROM #wtf1 where suffix = 'x' AND source = 'trade'
SELECT *
FROM stgArkonaBOPMAST WHERE bmvin = '1FT8W3B65BEA46899'

-- 7/28
the shark with braces IS a canonical source for:
  sales 
  acquisitions
  inventory at any given point IN time

with the hope of generating:
  electronic cross off
  glump report
  wash sheet
  gregs trade report
   
available resources:
  dps.VehicleInventoryItems 
  dps.vehicleSales
  dps.vehicleAcquisitions
  BOPTRAD
  BOPMAST
  INPMAST
  BOPVREF  
 
  dds.factVehicleSales

so many questions so few answers
crookston went out of business 12/31/2012
intramarket wholesales

imws & purchases are NOT represented IN bopmast OR boptrad
so, anything IN inpmast that IS NOT IN boptrad OR bopmast? AS a representation of purchases? 
at this time i have no idea how to use the info IN bopvref

ok, so what IS the acquisition date FROM arkona on a purchased vehicle
  inpmast.imdinv
-- a cautious bingo
  this does NOT WORK, one row per vin IN inpmast, values LIKE imdinv get updated
  with the most recent value
  so, for a purchase that has subsequent changes, inpmast will NOT work
SELECT a.imstk#, a.imvin, a.imdinv, b.*
FROM stgArkonaINPMAST a
LEFT JOIN #wtf1 b on a.imstk# = b.stocknumber
WHERE imstk# <> ''
  AND imtype = 'U'
  AND imstat = 'i'
  AND NOT EXISTS (
    SELECT 1
    FROM #arkTrades
    WHERE stocknumber = a.imstk#)  
  
  
some magical combination of bopmast, boptrad, inpmast
a row IN bopmast that does NOT exist IN boptrad = purchase ?

some combination of sold vehicles AND inventory
the question IS, IF a vehicle was sold AND does NOT exist IN boptrad, IS
it necessarily a purchase?

uh oh, feels LIKE mindfuckville

SELECT a.bmco#, a.bmkey, a.bmvtyp, a.bmstk#, a.bmvin, a.bmwhsl, a.bmdtor
FROM stgArkonaBOPMAST a
WHERE year(a.bmdtor) BETWEEN 2013 AND 2014
  AND a.bmstat <> '' -- finished deals
  AND a.bmvtyp = 'u' -- used car sales
  AND NOT EXISTS (
    SELECT 1
    FROM extArkonaBOPTRAD
    WHERE storecode = a.bmco# 
      AND vin = a.bmvin)
 
going about it backwards   
the bvkey IN boptrad IS the dealkey for the used car on which the trade was acquired
  

SELECT a.bmco#, a.bmkey, a.bmvtyp, a.bmstk#, a.bmvin, a.bmwhsl, a.bmdtor, b.*
SELECT *
FROM stgArkonaBOPMAST a  
LEFT JOIN extArkonaBOPTRAD b on a.bmkey = b.bopmastkey
WHERE a.bmstk# = '23089'

UPDATE extArkonaBOPTRAD
SET stocknumber = TRIM(stocknumber),
    vin = TRIM(vin);

-- trades with acq date FROM bopmast
-- DROP TABLE #arkTrades;
SELECT a.*, b.bmdtor AS arkDateAcq
INTO #arkTrades
FROM extArkonaBOPTRAD a
LEFT JOIN stgArkonaBOPMAST b on a.storeCode = b.bmco#
  AND a.bopmastKey = b.bmkey
WHERE year(bmdtor) BETWEEN 2013 AND 2014  
  AND b.bmstat <> '' -- eliminates pending deals
  
-- anomalies: 
-- temp stocknumbers
SELECT *
-- SELECT COUNT(*)
FROM #arkTrades
WHERE right(TRIM(stocknumber), 3) = right(TRIM(vin), 3)
-- of these, which have been sold (get stocknumber FROM bopmast):
SELECT a.*, b.bmco#, b.bmkey, b.bmvtyp, b.bmstk#, b.bmvin, b.bmwhsl, b.bmdtor
FROM #arkTrades a
LEFT JOIN stgArkonaBOPMAST b on a.vin = b.bmvin
  AND b.bmstat <> ''
  AND b.bmvtyp = 'u'
WHERE right(TRIM(a.stocknumber), 3) = right(TRIM(a.vin), 3)
ORDER BY a.vin

i just fucking DO NOT know how to model this transaction
1FAHP241X7G153912 shows as a temp stocknumber acq by ry2 on 5/11
tool shows traded IN to ry2 AS H5744GA on 5/11/13
ws to ry1 on 6/18/13
ry1 retail 6/18/13 asa 20269X
5250 shows inv date AS 5/11

but it makes me believe that this initial step at generating acquisitions
solely FROM arkona IS tentative at best
but i fucking knew that - this IS an iterative process, i keep crash AND burning
trying to DO too much at once, this IS generating WHAT I CAN FROM arkona AS 
good enuf, THEN i will DO the same thing with the dps, THEN AND only THEN 
combine the 2 INTO the finished product 

this IS only the first step: get what i can FROM arkona boptrad

-- 2 such cases:
SELECT a.*, b.bmco#, b.bmkey, b.bmvtyp, b.bmstk#, b.bmvin, b.bmwhsl, b.bmdtor
FROM #arkTrades a
LEFT JOIN stgArkonaBOPMAST b on a.vin = b.bmvin
  AND b.bmstat <> ''
  AND b.bmvtyp = 'u'
WHERE right(TRIM(a.stocknumber), 3) = right(TRIM(a.vin), 3)
  AND storecode <> bmco#
  AND b.bmco# IS NOT NULL 


-- AND which are currently IN inventory
SELECT *
FROM #arkTrades a
LEFT JOIN stgArkonaINPMAST b on a.vin = b.imvin
  AND b.imstat = 'i'
WHERE right(TRIM(a.stocknumber), 3) = right(TRIM(a.vin), 3)
ORDER BY a.vin



-- used car deals that DO NOT exist IN boptrad based on stocknumber
-- crudely, these are purchased acquisitions
SELECT a.bmco#, a.bmkey, a.bmvtyp, a.bmstk#, a.bmvin, a.bmwhsl, a.bmdtor, b.*
FROM stgArkonaBOPMAST a
LEFT JOIN #arkTrades b on a.bmstk# = b.stocknumber
WHERE a.bmstat <> '' -- finished deals
  AND a.bmvtyp = 'u' -- used car sale
  AND year(a.bmdtor) BETWEEN 2013 AND 2014
  AND b.storecode IS NULL 

-- current inventory FROM inpmast  
SELECT imco#, imstk#, imvin, imdinv, imcost, imiact
-- SELECT COUNT(*)
FROM stgArkonaINPMAST  
WHERE imstat = 'i' -- currently IN inventory
  AND imtype = 'u' -- used cars
  
SELECT a.storecode, a.stocknumber, a.vin, a.arkDateAcq
FROM #arkTrades a
  
tmpAisArkAcq 
tmpAisArkInv 
factVehicleSale 

-- a vehicle IS NOT acquired until it has a stocknumber
-- a stocknumber can be acquired multiple times: back on, 
--   AND it could be on the same day
-- this interim TABLE could exhibit pk violations, final dim/fact will
--   have the acqType which may be enuf to enforce uniqueness
CREATE TABLE tmpAisArkAcq (
  storeCode cichar(3) constraint NOT NULL, -- dimKey
  stockNumer cichar(9) constraint NOT NULL,
  vin cichar(17) constraint NOT NULL, -- dimKey
  dateAcq date constraint NOT NULL, -- dimKey 
  acqCategory cichar(8) constraint NOT NULL, -- dimKey, trade/purchase
--  acqType cichar(30), -- dimKey, nctrade/uctrade/auctionpurchase/streetpurchase, etc
  constraint PK primary key (storeCode, stockNumber, vin, dateAcq, acqCategory))
IN database;  