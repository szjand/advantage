-- SELECT *
-- SELECT * FROM VehicleInventoryItems 
-- SELECT * FROM organizations
-- SELECT * FROM VehicleInventoryItemStatuses WHERE category = 'RMFlagAV'

-- SELECT * FROM VehicleInventoryItemStatuses WHERE category = 'RMFlagAV' ORDER BY fromts

--SELECT VehicleInventoryItemID FROM VehicleInventoryItemStatuses WHERE category = 'RMFlagAV' GROUP BY VehicleInventoryItemID HAVING COUNT(*) > 1
-- SELECT * FROM MakeModelClassifications 
-- SELECT * FROM VehicleItemMileages
-- SELECT * FROM VehiclePricings 
-- SELECT * FROM VehiclePricings 
-- SELECT * FROM VehiclePricingDetails 
-- SELECT * FROM vehicleacquisitions
-- SELECT * FROM people
-- SELECT * FROM VehicleEvaluations 
-- DROP TABLE #wtf
SELECT CASE WHEN s.VehicleSaleID IS NULL THEN 'N' ELSE 'Y' END AS InAndOut,
  b.thedate AS [Date Sold], CAST(c.fromts AS sql_date) AS [Date Acqd],
  coalesce(cast(f.fromts AS sql_date), cast('12/31/9999' AS sql_date)) AS [Date Avail],
  CASE 
    WHEN coalesce(cast(f.fromts AS sql_date), cast('12/31/9999' AS sql_date)) = '12/31/9999' THEN 0
    ELSE timestampdiff(sql_tsi_day, cast(f.fromts AS sql_date), b.thedate)
  END AS [Days to Sell], 
  CASE a.typ WHEN 'VehicleSale_Retail' THEN 'Retail' ELSE 'Wholesale' END AS [Ret/WS],
  c.stocknumber, 
  CASE d.name WHEN 'Rydells' THEN 'Rydells' ELSE 'Honda' END AS Store,
  e.vin, e.yearmodel, e.make, e.model, e.bodystyle, e.TRIM,
  h.description AS segment, i.description AS type,
  j.value AS Miles,
  m.amount AS Invoice, n.amount AS Price, o.priceband,
  p.description AS glump,
  r.bmpric AS [Sold Amt], r.bmvcst AS Cost, r.bmcgrs AS Gross, 
  r.bmfigrs AS [F&I],bmhgrs AS [Total Gross],
  CASE 
    WHEN u.description IS NOT NULL THEN 
      replace(u.description,'purchase','') 
    ELSE 
      CASE 
        WHEN right(trim(stocknumber),2) = 'XX' THEN 'Auction'
        WHEN right(trim(stocknumber),1) IN ('X','P') THEN 'Street'
        WHEN right(trim(stocknumber),1) = 'N' THEN 'National'
        WHEN right(trim(stocknumber),1) = 'R' THEN 'Rental'
        WHEN right(trim(stocknumber),1) = 'L' THEN 'Lease Ret'
        WHEN right(trim(stocknumber),1) IN ('A','B','C') THEN 'Trade'
      END 
  END AS source,
  v.fullname AS [Booked By]
INTO #wtf  
FROM vehiclesales a
INNER JOIN dds.day b ON cast(a.soldts AS sql_date) = b.thedate
INNER JOIN VehicleInventoryItems c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
INNER JOIN organizations d ON c.owninglocationid = d.partyid
  AND d.name <> 'Crookston'
INNER JOIN VehicleItems e ON c.VehicleItemID = e.VehicleItemID  
LEFT JOIN VehicleInventoryItemStatuses f ON c.VehicleInventoryItemID = f.VehicleInventoryItemID 
  AND category = 'RMFlagAV'
  AND f.fromts = ( -- need just the most recent available
    SELECT MAX(FromTS)
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = c.VehicleInventoryItemID
      AND category = 'RMFlagAV')
LEFT JOIN MakeModelClassifications g ON e.make = g.make AND e.model = g.model   
LEFT JOIN typdescriptions h ON g.vehiclesegment = h.typ
LEFT JOIN typdescriptions i ON g.vehicletype = i.typ
LEFT JOIN VehicleItemMileages j ON e.VehicleItemID = j.VehicleItemID 
  AND j.VehicleItemMileageTS = (-- need just the most recent
    SELECT MAX(VehicleItemMileageTS)
    FROM VehicleItemMileages
    WHERE VehicleItemID = e.VehicleItemID)
LEFT JOIN VehiclePricings k ON c.VehicleInventoryItemID = k.VehicleInventoryItemID 
  AND k.VehiclePricingTS = (-- need just the most recent
    SELECT MAX(VehiclePricingTS) 
    FROM VehiclePricings 
    WHERE VehicleInventoryItemID = c.VehicleInventoryItemID)
LEFT JOIN VehiclePricingDetails m ON k.VehiclePricingID = m.VehiclePricingID
  AND m.typ = 'VehiclePricingDetail_Invoice'        
LEFT JOIN VehiclePricingDetails n ON k.VehiclePricingID = n.VehiclePricingID
  AND n.typ = 'VehiclePricingDetail_BestPrice'   
LEFT JOIN pricebands o ON n.amount BETWEEN o.pricefrom AND o.pricethru    
LEFT JOIN glumplimits p ON c.owninglocationid = p.locationid
  AND g.vehicletype = p.vehicletyp
  AND n.amount BETWEEN p.lowerlimit AND p.upperlimit
LEFT JOIN dds.stgarkonabopmast r ON c.stocknumber = r.bmstk#  
LEFT JOIN inandouts s ON a.vehiclesaleid = s.vehiclesaleid
LEFT JOIN VehicleAcquisitions t ON c.VehicleInventoryItemID = t.VehicleInventoryItemID 
LEFT JOIN typdescriptions u ON t.typ = u.typ
LEFT JOIN people v ON c.bookerid = v.partyid
WHERE  a.status = 'VehicleSale_Sold' -- eliminate back ons
  AND b.thedate > '12/31/2010'
--) x GROUP BY stocknumber HAVING COUNT(*) > 1


/* good enuf for arkona numbers  
FROM cross off
stk        gross    f&i     total
19460X     2011     1484    3495
18364a     -1293    893     -400  
SELECT * 
SELECT bmstk#, bmpric, bmvcst, bmcgrs, bmfigrs, bmhgrs 
FROM dds.stgarkonabopmast
where bmstk# = '18364a'
*/

SELECT COUNT(*) FROM #wtf
SELECT COUNT(*) FROM #wtf WHERE cost = 0
SELECT COUNT(*) FROM #wtf WHERE cost IS NULL

SELECT *
FROM #wtf 
WHERE cost = 0

/*
-- why so many with NULL miles, pricing, etc
-- it IS the IN AND outs 
SELECT * FROM #wtf WHERE miles IS NULL ORDER BY miles DESC

SELECT * FROM inandouts

19315X
SELECT * FROM VehicleItems WHERE vin = '3GTP2VE39BG166170'

SELECT stocknumber, b.*
from #wtf a
LEFT JOIN inandouts b ON a.vehiclesaleid = b.vehiclesaleid
WHERE miles IS NULL  
*/

SELECT [days to sell], COUNT(*)
FROM #wtf
GROUP BY [days to sell]


-- how many were available ON each day
SELECT *
FROM VehicleInventoryItems a
LEFT JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND category
  
SELECT *
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON a.thedate BETWEEN CAST(b.fromts AS sql_date) AND CAST(b.thruts AS sql_date)
  AND b.category = 'RMFlagAV'
WHERE thedate > '12/31/2010'

SELECT thedate, 
  SUM(CASE WHEN b.VehicleInventoryItemID IS NOT NULL THEN 1 ELSE 0 END)
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON b.category = 'RMFlagAV'
  AND a.thedate BETWEEN CAST(b.fromts AS sql_date) AND coalesce(CAST(b.thruts AS sql_date), CAST('12/31/9999' AS sql_date))
WHERE thedate between '12/31/2010' AND curdate()
GROUP BY a.thedate

SELECT thedate
FROM (
SELECT *
FROM dds.day a
INNER JOIN VehicleInventoryItemStatuses b ON b.category = 'RMFlagAV'
  AND a.thedate BETWEEN CAST(b.fromts AS sql_date) AND coalesce(CAST(b.thruts AS sql_date), CAST('12/31/9999' AS sql_date))
WHERE thedate between '12/31/2010' AND curdate()) x 
GROUP BY thedate


SELECT thedate
FROM dds.day
WHERE thedate between '12/31/2010' AND curdate()
GROUP BY thedate

SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = '0c80010a-e21f-4455-82f6-ae577205e595' AND thruts IS NULL

-- 3/7
-- JOIN ON VIN
SELECT * FROM #wtf WHERE cost IS NULL
SELECT COUNT(*) FROM #wtf WHERE cost IS NULL

SELECT a.stocknumber, a.vin, [date sold], b.bmstk#, b.bmvin, bmdtaprv, bmdtcap
FROM #wtf a
LEFT JOIN dds.stgarkonabopmast b ON a.vin = b.bmvin
--WHERE a.vin = '2G1WB5EK1A1200072'
WHERE a.cost IS NULL 
-- AN issue IS that bopmast IS not
2G1WB5EK1A1200072 IN wtf AS 12541X, but also IN tool AS C4525X
IN arkona AS C4525X

ok, this IS one of the IN bopmast they simply changed the fucking stocknumber
so the wholesale TRANSACTION of 12541X being sold to crookston does NOT show up
IN bopmast

-- need to get the numbers FROM accounting NOT bopmast


SELECT *
FROM dds.zcb
WHERE name = 'gmfs gross'
  AND dl2 IN ('ry1','ry2')
  AND dl3 = 'variable'
  AND dl4 = 'sales'
  AND dl5 = 'used'
  AND dl6 = 'retail'
  

-- DROP TABLE #wtf1; 
SELECT c.stocknumber,
  b.thedate AS [Date Sold], CAST(c.fromts AS sql_date) AS [Date Acqd]
INTO #wtf1 
FROM vehiclesales a
INNER JOIN dds.day b ON cast(a.soldts AS sql_date) = b.thedate
INNER JOIN VehicleInventoryItems c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE  a.status = 'VehicleSale_Sold' -- eliminate back ons
--  AND b.yearmonth = 201302
  AND b.thedate > '12/31/2010'
  
SELECT a.*, b.gtdate, b.gtacct, b.gtctl#, b.gtjrnl, b.gttamt, c.al2, c.glaccount, 
  c.gmcoa, c.line, c.page, c.dl6, c.dl4
FROM #wtf1 a
LEFT JOIN dds.stgarkonaglptrns b ON a.stocknumber = b.gtctl# 
LEFT JOIN (
  SELECT *
  FROM dds.zcb
  WHERE name = 'gmfs gross'
    AND dl2 IN ('ry1','ry2')
    AND dl3 = 'variable'
--    AND dl4 = 'sales'
    AND dl5 = 'used') c ON b.gtacct = c.glaccount
--    AND dl6 = 'retail') c ON b.gtacct = c.glaccount
WHERE c.dl1 IS NOT NULL 

-- DROP TABLE #wtf2;
SELECT stocknumber, 
  coalesce(SUM(
    CASE dl4
      WHEN 'sales' THEN 
        CASE al2 
          WHEN 'cogs' THEN gttamt
        END
      END),0) AS [Sales Cost],
  coalesce(SUM(
    CASE dl4
      WHEN 'sales' THEN 
        CASE al2 
          WHEN 'sales' THEN gttamt
        END
      END),0) AS [Sale Amt],
  coalesce(SUM(
    CASE dl4
      WHEN 'F&I' THEN 
        CASE al2 
          WHEN 'cogs' THEN gttamt
        END
      END),0) AS [F&I Cost],
  coalesce(SUM(
    CASE dl4
      WHEN 'F&I' THEN 
        CASE al2 
          WHEN 'sales' THEN gttamt
        END
      END),0) AS [F&I Sale Amt]       
INTO #wtf2             
FROM (
  SELECT a.*, b.gtdate, b.gtacct, b.gtctl#, b.gtjrnl, b.gttamt, c.al2, c.glaccount, 
    c.gmcoa, c.line, c.page, c.dl6, c.dl4
  FROM #wtf1 a
  LEFT JOIN dds.stgarkonaglptrns b ON a.stocknumber = b.gtctl# 
  LEFT JOIN (
    SELECT *
    FROM dds.zcb
    WHERE name = 'gmfs gross'
      AND dl2 IN ('ry1','ry2')
      AND dl3 = 'variable'
  --    AND dl4 = 'sales'
      AND dl5 = 'used') c ON b.gtacct = c.glaccount
  --    AND dl6 = 'retail') c ON b.gtacct = c.glaccount
  WHERE c.dl1 IS NOT NULL) d
GROUP BY stocknumber  

SELECT a.[date sold], a.stocknumber, a.inandout, a.[ret/ws], a.[sold amt],
  a.cost, a.gross, a.[f&i], a.[total gross], b.[sales cost], 
  b.[sale amt], -(b.[sales cost] + b.[sale amt]) AS agross,
  b.[f&i cost], b.[f&i sale amt],
  -(b.[f&i cost] + b.[f&i sale amt]) AS afigross,
  -(b.[sales cost] + b.[sale amt]) + -(b.[f&i cost] + b.[f&i sale amt]) AS totalacgross
-- SELECT COUNT(*)  
FROM #wtf a
LEFT JOIN #wtf2 b ON a.stocknumber = b.stocknumber
WHERE a.[date sold] BETWEEN '02/01/2013' AND '02/28/2013'
--  AND LEFT(a.stocknumber, 1) = '1'


select * FROM #wtf2
-- what about the fucking COUNT

BMSTAT  Record Status             In Process Deal (Unaccepted)
BMSTAT  Record Status   A         Accepted Deal
BMSTAT  Record Status   U         Capped Deal
BMTYPE  Record Type     C         Cash, Wholesale, Fleet, or Dealer Xfer
BMTYPE  Record Type     F         Financed Retail Deal
BMTYPE  Record Type     L         Financed Lease Deal
BMTYPE  Record Type     O         Cash Deal w/ Owner Financing
BMVTYP  Vehicle Type    N         New  
BMVTYP  Vehicle Type    U         Used  
BMWHSL  Sale Type       F         Fleet Deal
BMWHSL  Sale Type       L         Lease Deal
BMWHSL  Sale Type       R         Retail Deal
BMWHSL  Sale Type       W         Wholesale Deal
BMWHSL  Sale Type       X         Dealer Xfer

SELECT bmco#, bmstat, bmtype, bmwhsl, bmstk#, bmvin, bmdtaprv, bmdtcap
FROM dds.stgarkonabopmast
WHERE year(bmdtaprv) = 2013
  AND year(bmdtcap) = 2013
  AND (month(bmdtaprv) = 2 OR month(bmdtcap) = 2)
  
SELECT bmco#, bmstat, bmvtyp, bmwhsl, COUNT(*)
FROM ( 
SELECT bmco#, bmstat, bmvtyp, bmwhsl, bmstk#, bmvin, bmdtaprv, bmdtcap
FROM dds.stgarkonabopmast
WHERE year(bmdtaprv) = 2013
  AND year(bmdtcap) = 2013
  AND (month(bmdtaprv) = 2 OR month(bmdtcap) = 2)) x 
WHERE bmvtyp = 'u'  
GROUP BY bmco#, bmstat, bmvtyp, bmwhsl  


-- posting date for the COUNT !?!?!?

SELECT *
FROM dds.stgarkonaglptrns
WHERE gtpost <> 'y'

-- first just get the relevant accounts
-- DROP TABLE #a;
SELECT dl4, dl6, dl7, dl8, al2,glaccount, b.gttamt, gtdate, gtrdate, gtctl#, gtdoc#, gtjrnl
INTO #a
FROM dds.zcb a
LEFT JOIN dds.stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND year(b.gtdate) = 2012
  AND year(b.gtrdate) = 2012
  AND (month(b.gtdate) = 9 OR month(b.gtrdate) = 9)
WHERE a.name = 'gmfs gross'
  AND a.dl2 = 'ry1'
  AND a.dl3 = 'variable'
  AND a.dl5 = 'used'
  
  
-- counts are CLOSE but NOT exact,
-- talked to jeri about how counts ON page 6 are generated FROM accounting,
-- sales accounts AND jrnl vsu
-- they verify the COUNT with doc: Marlee New/Used Units


SELECT gtctl#, dl4, dl6, dl7, dl8
-- SELECT *
FROM #a
WHERE dl4 = 'sales' 
  AND dl6 = 'retail'
  AND dl7 = 'cars'
  AND dl8 = 'other'
  AND gtjrnl = 'vsu'
GROUP BY dl4, dl6, dl7, dl8, gtctl#
ORDER BY gtctl#

-- jan, per jeri credit to sale acct counts AS +1, debit to sale account counts AS -1
-- 18982X ON the doc detail shows AS + $6490, jeri IS guessing that it IS a back ON
so 
-- fuck why does 18583A have 2 rows
SELECT gtctl# 
FROM #a 
  WHERE dl4 = 'sales' 
  AND al2 = 'sales' 
GROUP BY gtctl# HAVING COUNT(*) > 1  
-- shouldn't matter, offsetting entries
SELECT *
FROM #a 
  WHERE dl4 = 'sales' 
  AND al2 = 'sales' 
  AND gtctl# = '18583A'
  
-- this IS THE COUNT (AND coincidentally the correct sales numbers)
SELECT dl6, dl7, dl8, 
  SUM(CASE WHEN gttamt < 0 THEN 1 ELSE -1 END) AS thecount,
  SUM(gttamt)
FROM #a 
  WHERE dl4 = 'sales' 
  AND al2 = 'sales'  
GROUP BY dl6, dl7, dl8   

-- ok, let's DO it for the entire time period
-- DROP TABLE #a;
SELECT dl2, dl4, dl6, dl7, dl8, al2,glaccount, b.gttamt, gtdate, gtrdate, gtctl#, gtdoc#, gtjrnl
INTO #a
FROM dds.zcb a
LEFT JOIN dds.stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND year(b.gtdate) IN (2011,2012,2013)
WHERE a.name = 'gmfs gross'
  AND a.dl2 <> 'ry3'
  AND a.dl3 = 'variable'
  AND a.dl5 = 'used'
-- here's the counts AND sales account totals 
SELECT dl2, year(gtdate)*100 + month(gtdate), dl6, dl7, dl8, 
  SUM(CASE WHEN gttamt < 0 THEN 1 ELSE -1 END) AS thecount,
  SUM(gttamt)
FROM #a 
  WHERE dl4 = 'sales' 
  AND al2 = 'sales'  
GROUP BY dl2, year(gtdate)*100 + month(gtdate), dl6, dl7, dl8

SELECT * FROM #a
-- 7930 vehicles IN #a
SELECT COUNT(*) FROM (SELECT DISTINCT gtctl# FROM #a WHERE dl4 = 'sales' AND al2 = 'sales') a

-- 71 with no record (stocknumber) IN VehicleInventoryItems 
SELECT * 
-- SELECT COUNT(*)
FROM (
  SELECT gtctl#
  FROM #a
  WHERE dl4 = 'sales' 
    AND al2 = 'sales'    
  GROUP BY gtctl#) a
LEFT JOIN VehicleInventoryItems b ON a.gtctl# = b.stocknumber
WHERE b.VehicleInventoryItemID IS NULL 

SELECT * FROM #a WHERE gtjrnl <> 'VSU' AND al2 = 'sales'

-- ALL postings to the sales accounts are to VSU
SELECT *
FROM #a 
WHERE dl2 = 'ry1'
  AND dl6 = 'retail'
  AND dl7 = 'cars'
  AND dl8 = 'certified'
--  AND year(gtdate)*100 + month(gtdate) = 201209
  AND al2 = 'sales'
  AND gtjrnl <> 'vsu'
  
  
SELECT *
FROM #a 
WHERE dl2 = 'ry1'
  AND dl4 = 'f&i'
--  AND dl7 = 'cars'
--  AND dl8 = 'certified'
  AND year(gtdate)*100 + month(gtdate) = 201209
AND dl4 <> 'sales'
  
-- ok, what i want to DO IS make accounting the base TABLE for the gross log
-- accounting determines which vehicles are sold AND WHEN   

-- seems LIKE i am going to have to UNION accounting with bopmast
select bmco#, bmstat, bmtype, bmwhsl, bmstk#, bmvin, bmdtaprv, bmdtcap FROM dds.stgarkonabopmast WHERE bmstat = 'a'

SELECT * FROM #a WHERE gtctl# = '18782a'

SELECT * FROM dds.stgarkonaglptrns WHERE gtctl# = '18782a'

numbers needed (IF FROM bopmast THEN potential)
  cost, sold amt, sales gross, f&i sales, f&i cost, f& i gross, total gross
  
first verify #a gives me accurate f&i numbers (p7 ON fncl statement)
  

  
