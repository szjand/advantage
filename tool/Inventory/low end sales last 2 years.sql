SELECT *
FROM vehiclesales a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE typ = 'VehicleSale_Retail'
  AND status = 'VehicleSale_Sold'
  AND CAST(soldts AS sql_date) BETWEEN '05/01/2013' AND '05/31/2014'
  AND soldamount < 8001
  

select a.*, b.wholesale, a.retail + b.wholesale AS "Total Sales" ,
  100 * round(1.0 * a.retail/(a.retail + b.wholesale), 2) AS "Retail %" ,
  100 * round(1.0 * b.wholesale/(a.retail + b.wholesale), 2) AS "Wholesale %" 
FROM (
  SELECT (100 * year(soldts)) + month(soldTS) AS YearMonth, COUNT(*) AS Retail
  FROM vehiclesales a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND owningLocationID = 'B6183892-C79D-4489-A58C-B526DF948B06' -- ry1 only
  WHERE typ = 'VehicleSale_Retail'
    AND status = 'VehicleSale_Sold'
    AND CAST(soldts AS sql_date) BETWEEN '05/01/2012' AND '05/31/2014'
    AND soldamount < 8001  
  GROUP BY (100 * year(soldts)) + month(soldTS)) a   
LEFT JOIN (
  SELECT (100 * year(soldts)) + month(soldTS) AS YearMonth, COUNT(*) AS Wholesale
  FROM vehiclesales a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND owningLocationID = 'B6183892-C79D-4489-A58C-B526DF948B06' -- ry1 only
  WHERE typ = 'VehicleSale_Wholesale'
    AND status = 'VehicleSale_Sold'
    AND CAST(soldts AS sql_date) BETWEEN '05/01/2012' AND '05/31/2014'
    AND soldamount < 8001  
  GROUP BY (100 * year(soldts)) + month(soldTS)) b on a.yearmonth = b.yearmonth  
ORDER BY a.yearmonth