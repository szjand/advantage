#30RetailRaw
#dailyWS
#dailyRetail
#dailyOwned
#dailyFreshRetail
#prev30sales
#cal

SELECT * FROM #30RetailRaw
SELECT * FROM #dailyWS
SELECT * FROM #dailyRetail
SELECT * FROM #dailyOwned
SELECT * FROM #dailyFreshAvailable
SELECT * FROM #prev30sales
SELECT * FROM #cal


SELECT c.theDate, o.theCount AS Owned, f.theCount AS Available, r.theCount as Retail,
  w.theCount AS Wholesale, coalesce(r.theCount, 0) + coalesce(w.theCount, 0) as TotalSales, ps.theCount AS Sales30
FROM #cal c
LEFT JOIN #dailyOwned o ON c.theDate = o.theDate
LEFT JOIN #dailyFreshAvailable f ON c.theDate = f.theDate
LEFT JOIN #dailyRetail r ON c.theDate = r.theDate
LEFT JOIN #dailyWS w ON c.thedate = w.thedate
LEFT JOIN #prev30sales ps ON c.theDate = ps.theDate
ORDER BY c.theDate desc


