-- become available
SELECT cast(viis.fromts AS SQL_DATE), max(dayname(viis.fromts)), left(o.name, 12), COUNT(*) 
FROM VehicleInventoryItemStatuses viis
LEFT JOIN VehicleInventoryItems vii ON vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
LEFT JOIN Organizations o ON o.PartyID = vii.LocationID 
WHERE category = 'RMFlagAV'
AND year(viis.fromTS) = 2010
AND month(viis.fromTS) > 6
AND o.name = 'rydells'
GROUP BY cast(viis.fromts AS SQL_DATE), o.name
