--SELECT viix.stocknumber, viix.ThruTS, vix.BodyStyle, vs.SoldAmount
-- $14k AND over
SELECT month(viix.ThruTS), year(viix.ThruTS), vix.BodyStyle, COUNT(*)
FROM VehicleInventoryItems viix
INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID 
  AND vs.typ = 'VehicleSale_Retail'  
INNER JOIN (
SELECT VehicleItemID, 
  CASE 
    WHEN (
      (bodystyle LIKE '%Extended Cab%') OR 
      (bodystyle LIKE '%King Access%') OR 
      (bodystyle LIKE '%Ext Cab%') OR 
      (bodystyle LIKE '%Supercab%') OR 
      (bodystyle LIKE '%Club%') OR 
      (bodystyle LIKE '%Extra%')) THEN 'Extended' 
    WHEN ( 
      (bodystyle LIKE '%Crew Cab%') OR 
      (bodystyle LIKE '%Crew%') OR 
      (bodystyle LIKE '%Quad%') OR 
      (bodystyle LIKE '%Mega%') OR 
      (bodystyle LIKE '%Double%')) THEN 'Crew'
    ELSE 'Reg'    
  END AS BodyStyle  
FROM VehicleItems 
WHERE (
  (bodystyle LIKE '%Extended Cab%') OR 
  (bodystyle LIKE '%King Access%') OR 
  (bodystyle LIKE '%Ext Cab%') OR 
  (bodystyle LIKE '%Supercab%') OR 
  (bodystyle LIKE '%Club%') OR 
  (bodystyle LIKE '%Extra%') OR
  (bodystyle LIKE '%Crew Cab%') OR
  (bodystyle LIKE '%Crew%') OR
  (bodystyle LIKE '%Quad%') OR
  (bodystyle LIKE '%Mega%') OR
  (bodystyle LIKE '%Double%') OR
  (bodystyle LIKE '%Regular Cab%') OR
  (bodystyle LIKE '%Reg%'))) vix ON viix.VehicleItemID = vix.VehicleItemID 
WHERE cast(viix.ThruTS AS sql_date) > '12/31/2009'
AND vs.SoldAmount > 13999
GROUP BY month(viix.ThruTS), year(viix.ThruTS), vix.BodyStyle 

