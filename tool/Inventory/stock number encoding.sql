SELECT stocknumber, year(fromts)
FROM VehicleInventoryItems
WHERE year(fromTS) BETWEEN 2012 AND 2014
  AND LEFT(stocknumber, 1) <> 'c'
  
DISTINCT alpha suffixes

-- 1 get rid of H on honda units
SELECT stocknumber, substring(stocknumber, 5, 15) AS rtStock
FROM VehicleInventoryItems
WHERE year(fromTS) BETWEEN 2012 AND 2014
  AND LEFT(stocknumber, 1) <> 'c'
  
-- VehicleInventoryItems 
SELECT trim(suffix) AS suffix, COUNT(*) AS howmany, MIN(stocknumber) ex1, MAX(stocknumber) ex2
FROM (
  SELECT stocknumber, rtstock,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(rtStock,'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),
    '6',''),'7',''),'8',''),'9','') AS suffix
  FROM (
    SELECT stocknumber, substring(stocknumber, 5, 15) AS rtStock
    FROM VehicleInventoryItems
    WHERE year(fromTS) BETWEEN 2010 AND 2014
      AND LEFT(stocknumber, 1) <> 'c') a) b
WHERE suffix <> ''
  AND suffix NOT LIKE '%*%'
GROUP BY trim(suffix)

-- inpmast
select *
FROM dds.stgArkonaINPMAST
WHERE imstk# <> ''

SELECT trim(suffix) AS suffix, COUNT(*) AS howmany, MIN(imstk#) ex1, MAX(imstk#) ex2
FROM (
  SELECT imstk#, rtstock,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(rtStock,'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix
  FROM (
    SELECT imstk#, substring(imstk#, 5, 15) AS rtStock
    FROM dds.stgArkonaINPMAST
    WHERE LEFT(imstk#, 1) <> 'c'
      AND imstk# <> ''
      AND imtype = 'U') a) b
WHERE suffix <> ''      
GROUP BY trim(suffix)      

-- VehicleInventoryItems 
-- narrow it down to 2012-2014
SELECT trim(suffix) AS suffix, COUNT(*) AS howmany, MIN(stocknumber) ex1, MAX(stocknumber) ex2
FROM (
  SELECT stocknumber, rtstock,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(rtStock,'0',''),'1',''),'2',''),'3',''),'4',''),'5',''),
    '6',''),'7',''),'8',''),'9','') AS suffix
  FROM (
    SELECT stocknumber, substring(stocknumber, 5, 15) AS rtStock
    FROM VehicleInventoryItems
    WHERE year(fromTS) BETWEEN 2012 AND 2014
      AND LEFT(stocknumber, 1) <> 'c') a) b
WHERE suffix <> ''
  AND suffix NOT LIKE '%*%'
GROUP BY trim(suffix)
ORDER BY howmany desc
