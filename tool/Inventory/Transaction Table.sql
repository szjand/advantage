/*
-- for each day IN the past year
--------------------------------------------------------------------------------
-- Calendar
--------------------------------------------------------------------------------
-- DROP TABLE #cal
DECLARE @back integer;
CREATE TABLE #cal (
  theDate date);
@back = 0;
WHILE @back < 240 do
  INSERT INTO #cal 
  select curdate() - @back FROM system.iota;
  @back = @back + 1;
END WHILE;

--------------------------------------------------------------------------------
-- tally
--------------------------------------------------------------------------------
-- DROP TABLE #tally
DECLARE @back integer;
CREATE TABLE #tally (
  n integer);
@back = 0;
WHILE @back < 1001 do
  INSERT INTO #tally 
  select @back FROM system.iota;
  @back = @back + 1;
END WHILE;
--------------------------------------------------------------------------------
*/

-- stock#, Date, FromTS, ThurTS ------------------------------------------------
SELECT viix.stocknumber, viix.VehicleInventoryItemID, viix.FromTS, viix.ThruTS,
  c.theDate
-- SELECT COUNT(*) -- 1974389
FROM VehicleInventoryItems viix, #cal c  
WHERE cast(viix.FromTS AS sql_date) <= c.TheDate
--  AND CAST(coalesce(viix.ThruTS, timestampadd(sql_tsi_year, 5, now())) AS sql_date) >= c.TheDate
AND viix.LocationID = ( -- Responsible
  SELECT partyid
  FROM Organizations
  WHERE name = 'rydells')
  
/* alternative to cross JOIN using LEFT JOIN 
SELECT viix.stocknumber, viix.VehicleInventoryItemID, viix.FromTS, viix.ThruTS,
  c.theDate
--SELECT COUNT(*) -- 1974389
FROM #cal c  
LEFT JOIN VehicleInventoryItems viix ON 1 = 1
WHERE cast(viix.FromTS AS sql_date) <= c.TheDate
--  AND CAST(coalesce(viix.ThruTS, timestampadd(sql_tsi_year, 5, now())) AS sql_date) >= c.TheDate
AND viix.LocationID = ( -- Responsible
  SELECT partyid
  FROM Organizations
  WHERE name = 'rydells')  
*/  

-- instead of viix.ThruTS, need to accomodate DCU  
-- once a vehicle IS IN dcu it IS considered to be sold (IF NOT delivered)
-- the rational for this IS to match the understanding of the cross off board
-- it gets "crossed" WHEN it IS 1. sold AND NOT delivered 2. sold AND delivered
-- but NOT WHEN it IS put INTO sales buffer

shit, now i'm thinking that i don't need to limit it based ON any thruts
every stocknumber ever has a record for every day, regardless
so a vehicle sold a year ago will have a bod sales status of sold for every day
since it was sold
-- stock#, Date, FromTS, ThurTS ------------------------------------------------

/******************************************************************************/

-- Static Info -----------------------------------------------------------------

SELECT viix.stocknumber, vix.yearmodel AS ModelYear, vix.Make, vix.Model,
  vix.ExteriorColor, mmcx.VehicleSegment, mmcx.VehicleType
FROM VehicleInventoryItems viix
LEFT JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID
LEFT JOIN MakeModelClassifications mmcx ON vix.make = mmcx.make
  AND vix.model = mmcx.model
  AND mmcx.ThruTS IS NULL 
LEFT JOIN VehicleAcquisitions va ON viix.VehicleInventoryItemID = va.VehicleInventoryItemID   





-- Static Info -----------------------------------------------------------------

/******************************************************************************/

-- Source ----------------------------------------------------------------------

SELECT va.typ, right(TRIM(viix.stocknumber),1), COUNT(*) 
FROM VehicleInventoryItems viix 
LEFT JOIN vehicleacquisitions va ON viix.VehicleInventoryItemID = va.VehicleInventoryItemID 
GROUP BY va.typ, right(TRIM(viix.stocknumber),1)

-- Source ----------------------------------------------------------------------

/******************************************************************************/
  
--------------------------------------------------------------------------------
--- what IS the next status after sales buffer closes
--  why DO i want to know this?
--  came up IN figuring out TS timelines for sales, etc
-- so need to know IN the context of what happened aftrer a vehicle went INTO sales buffer, was it sold 
-- OR was the sales buffer cancelled - aha, that seems to be the most relevant issues
-- what IS the future of a vehicle that has been put INTO sales buffer
---
-- this one fails with subquery returns more than one row
-- which makes sense, a VehicleInventoryItem can be IN sales buffer multiple times
-- which IS NOT my issue with this task
SELECT viisx.category, viisx.status, viisx.fromts, viisx.thruts, viix.stocknumber, 
  viix.fromts,
  (SELECT ThruTS FROM VehicleInventoryItemStatuses WHERE category = 'RMFlagSB')
FROM VehicleInventoryItemStatuses viisx
INNER JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagSB'
  AND VehicleInventoryItemID = viisx.VehicleInventoryItemID)
--------------------------------------------------------------------------------  
  
  
  
SELECT VehicleInventoryItemID, FromTS, ThruTS
FROM VehicleInventoryItemStatuses
WHERE category = 'RMFlagSB' 
AND ThruTS IS NOT NULL 
ORDER BY FromTS

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

--------------------------------------------------------------------------------
-- statuses, BOD AND New Today
--------------------------------------------------------------------------------
SELECT c.TheDate, max(Dayname(c.theDate)),
  COUNT(CASE WHEN (v.category = 'RMFlagAV' AND CAST(v.FromTS AS sql_date) < c.TheDate) THEN 1 END) AS "Avail BOD",
  COUNT(CASE WHEN (v.category = 'RMFlagAV' AND CAST(v.FromTS AS sql_date) = c.TheDate) THEN 1 END) AS "Became Avail",
  COUNT(CASE WHEN (v.category = 'RMFlagPulled' AND CAST(v.FromTS AS sql_date) < c.TheDate) THEN 1 END) AS "Pulled BOD",
  COUNT(CASE WHEN (v.category = 'RMFlagPulled' AND CAST(v.FromTS AS sql_date) = c.TheDate) THEN 1 END) AS "Pulled Today"
FROM (
  SELECT viisx.FromTS, viisx.ThruTS, viisx.category
  FROM VehicleInventoryItems viix
  LEFT JOIN VehicleInventoryItemStatuses viisx ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID
      AND viisx.category in ('RMFlagAV', 'RMFlagPulled')
  WHERE viix.LocationID = ( -- Responsible
    SELECT partyid
    FROM Organizations
    WHERE name = 'rydells')) v,
#cal c
WHERE CAST(v.FromTS AS sql_date) <= c.TheDate
AND CAST(coalesce(v.ThruTS, timestampadd(sql_tsi_year, 5, now())) AS sql_date) > c.TheDate
AND DayOfWeek(c.TheDate) <> 1 -- exclude Sundays
GROUP BY c.theDate



--------------------------------------------------------------------------------
-- owned, statuses, BOD AND New Today
-- tried to throw IN sales, thinking, too much, WHERE clause looking for viiThruTS > c.TheDate
-- eliminates sales
--------------------------------------------------------------------------------
SELECT c.TheDate, max(Dayname(c.theDate)),
  COUNT(CASE WHEN (cast(viiFromTS AS sql_date) < c.TheDate AND CAST(coalesce(v.stThruTS, timestampadd(sql_tsi_year, 5, now())) AS sql_date) > c.TheDate) THEN 1 END) AS "Owned BOD",
  COUNT(CASE WHEN (CAST(viiFromTS AS sql_date) = c.TheDate) THEN 1 END) AS "Owned Today",
  COUNT(CASE WHEN (v.category = 'RMFlagAV' AND CAST(v.stFromTS AS sql_date) < c.TheDate AND CAST(coalesce(v.stThruTS, timestampadd(sql_tsi_year, 5, now())) AS sql_date) > c.TheDate) THEN 1 END) AS "Avail BOD",
  COUNT(CASE WHEN (v.category = 'RMFlagAV' AND CAST(v.stFromTS AS sql_date) = c.TheDate) THEN 1 END) AS "Became Avail",
  COUNT(CASE WHEN (v.category = 'RMFlagPulled' AND CAST(v.stFromTS AS sql_date) < c.TheDate AND CAST(coalesce(v.stThruTS, timestampadd(sql_tsi_year, 5, now())) AS sql_date) > c.TheDate) THEN 1 END) AS "Pulled BOD",
  COUNT(CASE WHEN (v.category = 'RMFlagPulled' AND CAST(v.stFromTS AS sql_date) = c.TheDate) THEN 1 END) AS "Pulled Today",
FROM (
  SELECT viix.FromTS as viiFromts, viix.ThruTS as viiThruTS, 
    viisx.FromTS AS stFromTS, viisx.ThruTS AS stThruTS, viisx.category, vs.Typ AS SaleType,
    vs.SoldTS
  FROM VehicleInventoryItems viix
  LEFT JOIN VehicleInventoryItemStatuses viisx ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID
    AND viisx.category in ('RMFlagAV', 'RMFlagPulled')
  LEFT JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID 
    AND vs.status = 'VehicleSale_Sold'
  WHERE viix.LocationID = ( -- Responsible
    SELECT partyid
    FROM Organizations
    WHERE name = 'rydells')) v,
#cal c
WHERE CAST(v.viiFromTS AS sql_date) <= c.TheDate
AND CAST(coalesce(v.viiThruTS, timestampadd(sql_tsi_year, 5, now())) AS sql_date) > c.TheDate
AND DayOfWeek(c.TheDate) <> 1 -- exclude Sundays
GROUP BY c.theDate


SELECT * FROM vehiclesales