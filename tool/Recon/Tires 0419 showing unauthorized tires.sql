-- this IS generating tire results for VehicleReconItems of tires that were NOT authorized
-- 13650a, 13435XXA
-- 1. ADD INNER JOIN 
SELECT vii.stocknumber, 
  CASE 
    WHEN arix.status IS NOT NULL THEN 'D'  
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.ReceivedTS IS NOT NULL then 'R'
	WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.OrderedTS IS NOT NULL THEN 'O'
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END AS Tires
FROM VehicleInventoryItems vii
LEFT JOIN (
  SELECT vr.VehicleInventoryItemID, vr.VehicleReconItemID 
  FROM VehicleReconItems vr
  WHERE ((vr.typ = 'MechanicalReconItem_Tires')
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%'))
  AND vr.partsinstock = true
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations raix
	INNER JOIN AuthorizedReconItems arix ON raix.ReconAuthorizationID = arix.ReconAuthorizationID
	  AND arix.Status <> 'AuthorizedReconItem_Complete' 
	WHERE raix.VehicleInventoryItemID = vr.VehicleInventoryItemID 
	AND ThruTS IS NULL)) tvr ON vii.VehicleInventoryItemID = tvr.VehicleInventoryItemID 	
LEFT JOIN PartsOrders po ON tvr.VehicleReconItemID = po.VehicleReconItemID 
LEFT JOIN AuthorizedReconItems arix ON tvr.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.Status = 'AuthorizedReconItem_Complete' 
WHERE vii.ThruTS IS NULL 
AND vii.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'rydells')
AND (
  CASE 
    WHEN arix.status IS NOT NULL THEN 'D'  
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.ReceivedTS IS NOT NULL then 'R'
	WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.OrderedTS IS NOT NULL THEN 'O'
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END) <> ''
ORDER BY stocknumber  

-- 1. ADD INNER JOIN VehicleReconItems to AuthorizedReconItems 
SELECT vii.stocknumber, 
  CASE 
    WHEN arix.status IS NOT NULL THEN 'D'  
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.ReceivedTS IS NOT NULL then 'R'
	WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.OrderedTS IS NOT NULL THEN 'O'
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END AS Tires
FROM VehicleInventoryItems vii
LEFT JOIN (
  SELECT vr.VehicleInventoryItemID, vr.VehicleReconItemID 
  FROM VehicleReconItems vr
  INNER JOIN AuthorizedReconItems arix ON vr.VehicleReconItemID = arix.VehicleReconItemID -- 1.
  WHERE ((vr.typ = 'MechanicalReconItem_Tires')
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%'))
  AND vr.partsinstock = true
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations raix
	INNER JOIN AuthorizedReconItems arix ON raix.ReconAuthorizationID = arix.ReconAuthorizationID
	  AND arix.Status <> 'AuthorizedReconItem_Complete' 
	WHERE raix.VehicleInventoryItemID = vr.VehicleInventoryItemID 
	AND ThruTS IS NULL)) tvr ON vii.VehicleInventoryItemID = tvr.VehicleInventoryItemID 	
LEFT JOIN PartsOrders po ON tvr.VehicleReconItemID = po.VehicleReconItemID 
LEFT JOIN AuthorizedReconItems arix ON tvr.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.Status = 'AuthorizedReconItem_Complete' 
WHERE vii.ThruTS IS NULL 
AND vii.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'rydells')
AND (
  CASE 
    WHEN arix.status IS NOT NULL THEN 'D'  
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.ReceivedTS IS NOT NULL then 'R'
	WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.OrderedTS IS NOT NULL THEN 'O'
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END) <> ''
ORDER BY stocknumber  