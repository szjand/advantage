-- what i need IS ALL raw material vehicles with authorized tires that are incomplete AND show ALL OPEN
-- AuthorizedReconItems for each vehicle

-- Rydell raw material cars with OPEN ReconAuthorization
SELECT viix.stocknumber, viix.VehicleInventoryItemID, rax.ReconAuthorizationID 
-- SELECT COUNT(*)
FROM VehicleInventoryItems viix
INNER JOIN (
  SELECT VehicleInventoryItemID
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagRMP'
  AND ThruTS IS NULL) st ON viix.VehicleInventoryItemID = st.VehicleInventoryItemID
INNER JOIN ReconAuthorizations rax ON viix.VehicleInventoryItemID = rax.VehicleInventoryItemID   
WHERE viix.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'rydells')  
AND rax.ThruTS IS NULL 
ORDER BY viix.stocknumber



SELECT *
FROM AuthorizedReconItems arix
WHERE EXISTS (
  SELECT 1
  FROM VehicleReconItems
  WHERE typ = 'MechanicalReconItem_Tires')
AND status = 'AuthorizedReconItem_NotStarted'
AND ReconAuthorizationID IN (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE ThruTS IS NULL 
  AND VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItems
    WHERE ThruTS IS NULL))

SELECT * 
FROM AuthorizedReconItems arix
INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
WHERE arix.VehicleReconItemID IN (

SELECT VehicleReconItemID 
FROM VehicleReconItems vrix
WHERE vrix.typ = 'MechanicalReconItem_Tires')
ORDER BY arix. ReconAuthorizationID 

-- need ALL VehicleReconItems for a VehicleInventoryItems
    
--FROM:  GetCurrentReconItemsForVehicleInventoryItem

  //Get all current items
SELECT vri.VehicleReconItemID, vri.Sequence, 
  case 
    when vri.Typ IN (
      SELECT Typ
      FROM TypCategories 
      WHERE Category = 'BodyReconItem') THEN 'Body'
    WHEN vri.Typ IN (
      SELECT Typ
      FROM TypCategories 
      WHERE Category = 'MechanicalReconItem') THEN 'Mechanical'
    WHEN vri.Typ IN (
      SELECT Typ
      FROM TypCategories tc1 
      WHERE tc1.Category = 'AppearanceReconItem'
        OR tc1.Typ = 'PartyCollection_AppearanceReconItem') THEN 'Appearance'
  END AS Typ,
  CASE 
    WHEN (
      select Category 
      from TypCategories 
      where typ = td.typ) in ('AppearanceReconItem', 'PartyCollection') THEN  vri.Description
    ELSE trim(td.description) collate ADS_DEFAULT_CI + ': ' + vri.Description 
  END AS Description,
  vri.TotalPartsAmount, vri.LaborAmount, 
  (Select top 1 sd.Description 
    from StatusDescriptions sd 
    WHERE sd.Status = ari.Status 
    AND sd.ThruTS IS null) AS Status,
  ari.Status AS StatusStatus

  FROM VehicleReconItems vri
  INNER JOIN AuthorizedReconItems ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
    AND ari.Status <> 'AuthorizedReconItem_Complete'
    AND ari.ReconAuthorizationID = (
    SELECT ra.ReconAuthorizationID 
	                                   FROM ReconAuthorizations ra 
									   WHERE ra.VehicleInventoryItemID = @VehicleInventoryItemID 
									     AND ra.ThruTS IS null)
	AND ari.CompleteTS IS null
  LEFT JOIN TypDescriptions td ON td.Typ = vri.typ;
--------------------------------------------------------------------------------    
SELECT vri.VehicleReconItemID, 
  trim(td.description) collate ADS_DEFAULT_CI + ': ' + vri.Description AS Description,
  vri.TotalPartsAmount, vri.LaborAmount, 
  (Select top 1 sd.Description 
    from StatusDescriptions sd 
    WHERE sd.Status = ari.Status 
    AND sd.ThruTS IS null) AS Status,
  ari.Status AS StatusStatus
FROM VehicleReconItems vri
INNER JOIN AuthorizedReconItems ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
    AND ari.Status <> 'AuthorizedReconItem_Complete'
    AND ari.ReconAuthorizationID = (
    SELECT ra.ReconAuthorizationID 
	                                   FROM ReconAuthorizations ra 
									   WHERE ra.VehicleInventoryItemID = @VehicleInventoryItemID 
									     AND ra.ThruTS IS null)
	AND ari.CompleteTS IS null
  LEFT JOIN TypDescriptions td ON td.Typ = vri.typ;  
 
-- any OPEN reconauthorization for a raw materials vehicle that includes tires AND IS incomplete


-- this IS starting to look close
SELECT rm.stocknumber, vrix.typ, vrix.description 
FROM (
  SELECT viix.stocknumber, viix.VehicleInventoryItemID, rax.ReconAuthorizationID, viix.thruts -- Rydell raw material cars with OPEN ReconAuthorization
  -- SELECT COUNT(*)
  FROM VehicleInventoryItems viix
  INNER JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses
    WHERE category = 'RMFlagRMP'
    AND ThruTS IS NULL) st ON viix.VehicleInventoryItemID = st.VehicleInventoryItemID
  INNER JOIN ReconAuthorizations rax ON viix.VehicleInventoryItemID = rax.VehicleInventoryItemID   
  WHERE viix.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'rydells')  
  AND rax.ThruTS IS NULL) rm
INNER JOIN AuthorizedReconItems arix ON rm.ReconAuthorizationID = arix.ReconAuthorizationID
INNER JOIN (
  SELECT *
  FROM VehicleReconItems
  WHERE typ LIKE 'Mechanical%') vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID   
ORDER BY rm.stocknumber  

-- this IS starting to look CLOSE
-- now, limit to those that have tires
-- this IS CLOSE but includes VehicleReconItems of tires that are NOT authorized 12020
SELECT rm.stocknumber, vrix.typ, vrix.description, arix.VehicleReconItemID  
FROM (
  SELECT viix.stocknumber, viix.VehicleInventoryItemID, rax.ReconAuthorizationID, viix.thruts -- Rydell raw material cars with OPEN ReconAuthorization
  -- SELECT COUNT(*)
  FROM VehicleInventoryItems viix
  INNER JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses
    WHERE category = 'RMFlagRMP'
    AND ThruTS IS NULL) st ON viix.VehicleInventoryItemID = st.VehicleInventoryItemID
  INNER JOIN ReconAuthorizations rax ON viix.VehicleInventoryItemID = rax.VehicleInventoryItemID   
  WHERE viix.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'rydells')  
  AND rax.ThruTS IS NULL) rm
INNER JOIN AuthorizedReconItems arix ON rm.ReconAuthorizationID = arix.ReconAuthorizationID
INNER JOIN (
  SELECT *
  FROM VehicleReconItems
  WHERE typ LIKE 'Mechanical%') vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID   
WHERE EXISTS (
  SELECT 1
  FROM VehicleReconItems 
  WHERE VehicleInventoryItemID = rm.VehicleInventoryItemID 
  AND typ = 'MechanicalReconItem_Tires')
ORDER BY rm.stocknumber 

--------------------------------------------------------------------------------
-- this IS starting to look CLOSE
-- now, limit to those that have tires
-- this IS CLOSE but includes VehicleReconItems of tires that are NOT authorized 12020
-- also does NOT cull out InProc AND Complete 11913a, 12478xa
SELECT rm.stocknumber, vrix.typ, vrix.description, arix.status
  -- SELECT COUNT(*) 
FROM (
  SELECT viix.stocknumber, viix.VehicleInventoryItemID, rax.ReconAuthorizationID, viix.thruts -- Rydell raw material cars with OPEN ReconAuthorization
  FROM VehicleInventoryItems viix
  INNER JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses
    WHERE category = 'RMFlagRMP'
    AND ThruTS IS NULL) st ON viix.VehicleInventoryItemID = st.VehicleInventoryItemID
  INNER JOIN ReconAuthorizations rax ON viix.VehicleInventoryItemID = rax.VehicleInventoryItemID   
  WHERE viix.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'rydells')  
  AND rax.ThruTS IS NULL) rm
INNER JOIN AuthorizedReconItems arix ON rm.ReconAuthorizationID = arix.ReconAuthorizationID
INNER JOIN (
  SELECT *
  FROM VehicleReconItems
  WHERE typ LIKE 'Mechanical%') vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID   
WHERE EXISTS (
  SELECT 1
  FROM VehicleReconItems 
  WHERE VehicleInventoryItemID = rm.VehicleInventoryItemID 
  AND typ = 'MechanicalReconItem_Tires')
--AND EXISTS ( -- this does nothing
--  SELECT 1
--  FROM AuthorizedReconItems 
--  WHERE ReconAuthorizationID = arix.ReconAuthorizationID)  
ORDER BY rm.stocknumber 


-- starting to feel LIKE the wrong approach
-- find ALL notstarted AuthorizedReconItems that are tires
-- THEN include ALL mech AuthorizedReconItems for those vehicles