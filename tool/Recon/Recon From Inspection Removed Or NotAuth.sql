SELECT *
FROM vehiclewalks

SELECT i.stocknumber, r.typ, left(r.description, 100), p.fullname
FROM VehicleReconItems r
INNER JOIN VehicleInventoryItems i ON r.VehicleInventoryItemID = i.VehicleInventoryItemID
  AND i.ThruTS IS NULL 
LEFT JOIN VehicleWalks w ON r.VehicleInventoryItemID = w.VehicleInventoryItemID   
LEFT JOIN People p ON w.VehicleWalkerID = p.partyid
WHERE r.basistable = 'VehicleInspections'
  AND r.Description <> 'Inspection Fee'
  AND NOT EXISTS (
    SELECT 1
    FROM AuthorizedReconItems
    WHERE VehicleReconItemID = r.VehicleReconItemID)
  AND NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = r.VehicleInventoryItemID
    AND category = 'RMFlagWP'
    AND ThruTS IS NULL)
ORDER BY i.stocknumber   
    
-- need to ADD the notion of RemovedRecon    
SELECT DISTINCT typ
FROM AuthorizedReconItems     

    