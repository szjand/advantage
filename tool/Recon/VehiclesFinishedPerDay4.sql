-- edited to include a JOIN to ReconAuthorizations WHERE ThruTS IS NULL 
-- AND removed AS irrelevant
SELECT da.TheDate, dayname(theDate),
  SUM(CASE WHEN category = 'MechanicalReconItem' THEN 1 ELSE 0 END) AS Mech, 
  SUM(CASE WHEN category = 'BodyReconItem' THEN 1 ELSE 0 END) AS Body,
  SUM(CASE WHEN category = 'PartyCollection' THEN 1 ELSE 0 END) AS App
FROM (
  SELECT *
  FROM dds.day d
  WHERE TheDate >= CurDate() - 21
    AND TheDate <= CurDate()) da 
LEFT JOIN (
  SELECT VehicleInventoryItemID , max(ar.completeTS) AS ThruTS, category 
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
    AND ar.startTS IS NOT NULL 
    AND ri.typ <> 'MechanicalReconItem_Inspection'
    AND ri.VehicleInventoryItemID IN (
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItems
      WHERE owninglocationid = (
        SELECT partyid
        FROM Organizations
        WHERE name = 'rydells'))
  GROUP BY VehicleInventoryItemID,category) v ON da.TheDate = CAST(v.ThruTS AS sql_date)  
WHERE category IS NOT NULL 
GROUP BY da.TheDate  
ORDER BY thedate DESC
-- AND NOT EXISTS AuthorizedReconItems NOT complete ?  

OR WHERE the ReconAuthorizations IS open?
 
DO i fucking care about the ReconAuthorization?

well, the fucking numbers are different, even more so going further back IN time
so which one IS right
including the JOIN to ReconAuthorizations, numbers are smaller

the question again, WHEN IS the ReconAuthorizations closed?
stays OPEN until superceded OR vehicle IS delivered
-- so check to see IF any of these diffs are delivered -------------------------
-- fuck, didn't account for rydells only
mech 10/4
without ReconAuthorizations > with BY 2
-- added rydells: 11 total, 1 delivered
SELECT stocknumber, fromts, thruts
FROM VehicleInventoryItems  
WHERE VehicleInventoryItemID IN ( 
SELECT VehicleInventoryItemID 
FROM (
  SELECT *
  FROM dds.day d
  WHERE TheDate >= CurDate() - 21
    AND TheDate <= CurDate()) da 
LEFT JOIN (
  SELECT VehicleInventoryItemID , max(ar.completeTS) AS ThruTS, category -- collapses multiple line items with the same startts AND completets INTO one row
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
    AND ar.startTS IS NOT NULL 
    AND ri.typ <> 'MechanicalReconItem_Inspection'
  GROUP BY VehicleInventoryItemID,category) v ON da.TheDate = CAST(v.ThruTS AS sql_date)  
WHERE category ='MechanicalReconItem'
and thedate = '10/04/2011') 
AND LEFT(stocknumber, 1) NOT IN ('H','C')

--------------------------------------------------------------------------------
takes me back to the semantics for the model
given a SET of circumstances, ...
what circumstances: 
  inventory vs sold: WHILE IN inventory, IF any recon has been authorized, there
  should be 1 ReconAuthorization

am i even sure that i'm NOT counting AuthorizedReconItems that may be complete
for a vehicle, but still have other OPEN ari?
 
select VehicleInventoryItemID 
FROM ReconAuthorizations
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
    AND ar.startTS IS NOT NULL 
    AND ri.typ <> 'MechanicalReconItem_Inspection'
  GROUP BY VehicleInventoryItemID,category) 
AND ThruTS IS NULL   
GROUP BY VehicleInventoryItemID HAVING COUNT(*) > 1   