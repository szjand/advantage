-- ALL ARI's
SELECT ra.ReconAuthorizationID, left(ra.BasisTable, 25) AS RABasisTable,/*ra.VehicleInventoryItemID,*/ ra.fromts AS RAFrom, ra.thruts AS RAThru,
  ari.VehicleReconItemID, substring(ari.typ, position('_' IN ari.typ) + 1, 18) AS AriTyp, 
  substring(ari.status, position('_' IN ari.status) + 1, 12) AS AriStatus, ari.startts AS AriStart, ari.completets AS AriComplete,
  CASE
    WHEN vri.typ LIKE '%Appear%' THEN 'Appearance'
    WHEN vri.typ LIKE 'Body%' THEN 'Body'
    WHEN vri.typ LIKE 'Mech%' THEN 'Mechanical'
  END AS Dept,
  trim(substring(vri.typ, position('_' IN vri.typ) + 1, 19)) + ': ' + vri.description,
  vri.TotalPartsAmount + vri.LaborAmount AS Amount,
  vri.PartsInStock AS PartsRequd
FROM reconAuthorizations ra
LEFT JOIN AuthorizedReconItems ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
LEFT JOIN VehicleReconItems vri ON ari.VehicleReconItemID = vri.VehicleReconItemID
WHERE ra.VehicleInventoryItemID = '3a2a1868-425f-484f-9915-2fe258d34275'
AND ra.BasisTable <> 'VehicleInspections'
ORDER BY ra.FromTS, ari.VehicleReconItemID


-- Due Bills
SELECT ra.VehicleDueBillAuthorizationID, ra.fromts AS DBFrom, ra.thruts AS DBThru,
  ari.VehicleReconItemID, substring(ari.typ, position('_' IN ari.typ) + 1, 18) AS AriTyp, 
  substring(ari.status, position('_' IN ari.status) + 1, 12) AS AriStatus, ari.startts AS AriStart, ari.Thruts AS AriComplete,
  CASE
    WHEN vri.typ LIKE '%Appear%' THEN 'Appearance'
    WHEN vri.typ LIKE 'Body%' THEN 'Body'
    WHEN vri.typ LIKE 'Mech%' THEN 'Mechanical'
  END AS Dept,
  trim(substring(vri.typ, position('_' IN vri.typ) + 1, 19)) + ': ' + vri.description,
  vri.TotalPartsAmount + vri.LaborAmount AS Amount
FROM VehicleDueBillAuthorizations ra
LEFT JOIN VehicleDueBillAuthorizedItems ari ON ari.VehicleDueBillAuthorizationID = ra.VehicleDueBillAuthorizationID
LEFT JOIN VehicleReconItems vri ON ari.VehicleReconItemID = vri.VehicleReconItemID
WHERE ra.VehicleSaleID = (select VehicleSaleID from VehicleSales where VehicleInventoryItemID = 'c120a4f6-d3b1-4c16-8426-0455995d5128')
ORDER BY ra.FromTS, ari.VehicleReconItemID


SELECT vrix.VehicleInventoryItemID,  COUNT(*)
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
WHERE EXISTS (
  SELECT 1
  FROM ReconAuthorizations 
  WHERE ReconAuthorizationID = arix.ReconAuthorizationID
  AND thruts IS NULL)
AND vrix.typ LIKE 'M%' 
AND vrix.PartsInStock = True 
GROUP BY vrix.VehicleInventoryItemID 
ORDER BY COUNT(*) DESC 