-- 10/7/11
-- FROM what i learned IN the wait queries, 0 time wip does NOT show up IN VehicleInventoryItemStatuses 
-- so here's the new one going back 21 days
-- moved this to VehiclesFinishedPerDay4

SELECT da.TheDate, dayname(theDate),
  SUM(CASE WHEN category = 'MechanicalReconItem' THEN 1 ELSE 0 END) AS Mech, 
  SUM(CASE WHEN category = 'BodyReconItem' THEN 1 ELSE 0 END) AS Body,
  SUM(CASE WHEN category = 'PartyCollection' THEN 1 ELSE 0 END) AS App
FROM (
  SELECT *
  FROM dds.day d
  WHERE TheDate >= CurDate() - 21
    AND TheDate <= CurDate()) da 
LEFT JOIN (
  SELECT VehicleInventoryItemID , max(ar.completeTS) AS ThruTS, category -- collapses multiple line items with the same startts AND completets INTO one row
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
    AND ar.startTS IS NOT NULL 
    AND ri.typ <> 'MechanicalReconItem_Inspection'
  GROUP BY VehicleInventoryItemID,category) v ON da.TheDate = CAST(v.ThruTS AS sql_date)  
WHERE category IS NOT NULL 
GROUP BY da.TheDate  
ORDER BY thedate DESC



SELECT DISTINCT category, status
FROM VehicleInventoryItemStatuses 
WHERE category LIKE 'A%'  


SELECT dayofyear(ThruTS), COUNT(*)
FROM VehicleInventoryItemStatuses
WHERE status = 'AppearanceReconProcess_InProcess'
AND ThruTS IS NOT NULL 
AND CAST(fromts AS sql_date) > curdate() - 90
GROUP BY dayofyear(ThruTS)
ORDER BY COUNT(*) DESC 

SELECT CAST(thruts AS sql_date), MAX(dayname(CAST(thruts AS sql_date))), COUNT(*)
FROM VehicleInventoryItemStatuses
WHERE status = 'AppearanceReconProcess_InProcess'
AND ThruTS IS NOT NULL 
AND CAST(thruts AS sql_date) > curdate() - 90  
group by CAST(thruts AS sql_date)

SELECT CAST(thruts AS sql_date) AS thedate, status, MAX(dayname(CAST(thruts AS sql_date))) AS dayname, COUNT(*) AS howmany
FROM VehicleInventoryItemStatuses
WHERE status like '%_InProcess'
AND ThruTS IS NOT NULL 
AND CAST(thruts AS sql_date) > curdate() - 90  
group by CAST(thruts AS sql_date), status

/*
this IS ALL using the viis.ThruTS WHERE status = _InProcess
-- IS this vehicles OR just lines?
*/
-- finished per day
SELECT thedate, theday,
  max(CASE WHEN status LIKE 'A%' THEN howmany END) AS "App Finished",
  max(CASE WHEN status LIKE 'B%' THEN howmany END) AS "Body Finished",
  max(CASE WHEN status LIKE 'M%' THEN howmany END) AS "Mech Finished"
FROM (  
  SELECT CAST(thruts AS sql_date) AS thedate, status, 
    MAX(dayname(CAST(thruts AS sql_date))) AS theday, 
	cast(COUNT(*) AS sql_char) AS howmany
  FROM VehicleInventoryItemStatuses viisx
  WHERE status like '%_InProcess'
  AND ThruTS IS NOT NULL -- finished
  AND CAST(thruts AS sql_date) > curdate() - 30  
  AND EXISTS (
    SELECT 1
	FROM VehicleInventoryItems 
	WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID 
    AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells'))
  group by CAST(thruts AS sql_date), status) wtf
WHERE theday NOT IN ('Saturday', 'Sunday')  
GROUP BY thedate, theday
ORDER BY thedate DESC

-- avg finished per day
SELECT 
  avg(coalesce(cast(app AS sql_numeric),0)) AS avgApp,
  SUM(coalesce(CAST(app AS sql_numeric),0)),
  avg(coalesce(cast(body AS sql_numeric),0)) AS avgBody,
  SUM(coalesce(CAST(body AS sql_numeric),0)),
  avg(coalesce(cast(mech AS sql_numeric),0)) AS avgMech,
  SUM(coalesce(CAST(mech AS sql_numeric),0))
-- SELECT COUNT(*) -- 64
from (
  SELECT thedate, theday,
    max(CASE WHEN status LIKE 'A%' THEN howmany END) AS App,
    max(CASE WHEN status LIKE 'B%' THEN howmany END) AS Body,
    max(CASE WHEN status LIKE 'M%' THEN howmany END) AS Mech
  FROM (  
    SELECT CAST(thruts AS sql_date) AS thedate, status, 
      MAX(dayname(CAST(thruts AS sql_date))) AS theday, 
  	cast(COUNT(*) AS sql_char) AS howmany
    FROM VehicleInventoryItemStatuses viisx
    WHERE status like '%_InProcess'
    AND ThruTS IS NOT NULL -- finished
    AND CAST(thruts AS sql_date) > curdate() - 7  
    AND EXISTS (
      SELECT 1
  	FROM VehicleInventoryItems 
  	WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID 
      AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells'))
    group by CAST(thruts AS sql_date), status) wtf
  WHERE theday NOT IN ('Saturday', 'Sunday')  
  GROUP BY thedate, theday) wtf

