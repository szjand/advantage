As part of our continuing effort to improve, please review the following observations regarding the trade: 
25348A 2008 GMC Acadia 1GKEV337X8J237963 (kern)
Evaluated by Nicholas Shirek on 05/22/2015 for the GF-Grand Forks market at the GF-Rydells location.
Vehicle Walk performed by Dave Wilkie on 05/27/2015.


Other observations
frt struts leaking 683, steering gear leaking 1300, upper control arm 357
these are mechanical nightmares. it seems like all ACADIAS,TRAVERSE, AND 
ENCLAVES HAVE THESE SAME PROBLEMS. This will probably be more throughput then 
anything and it seems like we are getting alot of these. Getting hard to 
keep these and not just shipping out


select a.stocknumber, b.vin, CAST(a.fromts AS sql_date) AS fromDate, 
  CAST(a.thruts AS sql_date) AS thruDate, b.yearmodel, 
  left(b.make, 12) AS make, left(b.model,20) AS model,
  c.vehiclekey
FROM VehicleInventoryItems a
INNER JOIN VehicleItems b on a.VehicleItemID = b.VehicleItemID 
LEFT JOIN dds.dimvehicle c on b.vin = c.vin
WHERE b.model = 'acadia'


SELECT vehiclekey, ro
FROM dds.factRepairOrder
GROUP BY vehiclekey, ro


select *
FROM dds.dimopcode
WHERE description LIKE '%control arm%'

select *
FROM dds.dimopcode
WHERE description LIKE '%strut%'

select *
FROM dds.dimopcode
WHERE description LIKE '%steering%' AND description LIKE '%gear%'

-- DROP TABLE #wtf;
SELECT aa.thedate, a.ro, b.vin, c.opcode, LEFT(c.description,25), d.fullname,
  e.opcode, LEFT(e.description,25)
INTO #wtf  
FROM dds.factrepairorder a
INNER JOIN dds.day aa on a.opendatekey = aa.datekeyas
INNER JOIN dds.dimvehicle b on a.vehiclekey = b.vehiclekey
  AND b.model = 'acadia'
INNER JOIN dds.dimopcode c on a.opcodekey = c.opcodekey  
  AND c.pdqcat1 = 'N/A'
INNER JOIN dds.dimcustomer d on a.customerkey = d.customerkey
INNER JOIN dds.dimopcode e on a.corcodekey = e.opcodekey
  AND e.pdqcat1 = 'N/A'
  
SELECT opcode, expr, opcode_1, expr_1, COUNT(*)
FROM #wtf
group by opcode, expr, opcode_1, expr_1
ORDER BY COUNT(*) DESC 
  
select *
FROM #wtf  
WHERE expr LIKE '%strut%'
  OR expr_1 LIKE '%strut%'
  OR expr LIKE '%control%'
  OR expr_1 LIKE '%control a%'
  OR (expr LIKE '%steering%' AND expr LIKE '%gear%') 
  OR (expr_1 LIKE '%steering%' AND expr_1 LIKE '%gear%') 
ORDER BY thedate DESC   

-- DROP TABLE #wtf;
SELECT aa.thedate, a.ro, a.line, b.vin, c.opcode, LEFT(c.description,25), d.fullname
--  e.opcode, LEFT(e.description,25)
--INTO #wtf  
-- SELECT c.*
FROM dds.factrepairorder a
INNER JOIN dds.day aa on a.opendatekey = aa.datekey
INNER JOIN dds.dimvehicle b on a.vehiclekey = b.vehiclekey
  AND b.model IN ('acadia','traverse','enclave')
INNER JOIN dds.dimopcode c on a.corcodekey = c.opcodekey  
  AND (
    c.description LIKE '%strut%'
    OR (c.description LIKE '%control%' AND c.description LIKE '%arm%')
    OR (c.description LIKE '%steering%' AND c.description LIKE '%gear%'))
INNER JOIN dds.dimcustomer d on a.customerkey = d.customerkey

  
SELECT *
FROM dds.dimopcode c
WHERE c.description LIKE '%strut%'
    OR (c.description LIKE '%control%' AND c.description LIKE '%arm%')
    OR (c.description LIKE '%steering%' AND c.description LIKE '%gear%')

    
    
    
    
    
    
    
    
    
    
    
    
    
SELECT *
FROM dds.factrepairorder a
--INNER JOIN dds.dimrocomment b on a.rocommentkey = b.rocommentkey
INNER JOIN dds.dimccc c on a.ccckey = c.ccckey
WHERE a.ro = '16193466'

-- this IS probably the best, USING CCC
-- DROP TABLE #wtf;
SELECT d.thedate, b.ro, b.line, c.modelyear, left(c.model, 15) as model,
  a.complaint, a.correction, e.fullname,
  f.opcode, LEFT(f.description, 50)
INTO #wtf
FROM dds.dimccc a
INNER JOIN dds.factRepairOrder b on a.ro = b.ro
  AND a.line = b.line
INNER JOIN dds.dimvehicle c on b.vehiclekey = c.vehiclekey
  AND c.model IN ('acadia','traverse','enclave') 
INNER JOIN dds.day d on b.opendatekey = d.datekey 
INNER JOIN dds.dimcustomer e on b.customerkey = e.customerkey
INNER JOIN dds.dimopcode f on b.corcodekey = f.opcodekey
INNER JOIN dds.day g on b.finalclosedatekey = g.datekey
  AND g.thedate < curdate()
WHERE (
  lower(cast(a.complaint AS sql_longvarchar)) LIKE '%control arm%'
  OR lower(cast(a.complaint AS sql_longvarchar)) LIKE '%strut%'
  OR (lower(cast(a.complaint AS sql_longvarchar)) LIKE '%steering%' AND lower(cast(a.complaint AS sql_longvarchar)) like '%gear%'))
  
OR (  
  lower(cast(a.correction AS sql_longvarchar)) LIKE '%control arm%'
  OR lower(cast(a.correction AS sql_longvarchar)) LIKE '%strut%'
  OR (lower(cast(a.correction AS sql_longvarchar)) LIKE '%steering%' AND lower(cast(a.correction AS sql_longvarchar)) like '%gear%'))

SELECT MIN(thedate) FROM #wtf -- 12/3/12
  
SELECT a.*, b.howmany FROM #wtf a
inner JOIN (
SELECT model, opcode, COUNT(*) AS howmany
FROM #wtf
GROUP BY model, opcode) b on a.model = b.model AND a.opcode = b.opcode
WHERE howmany > 15 AND a.opcode <> 'dia'
ORDER BY a.model, a.opcode


SELECT *
FROM (
SELECT a.*, b.howmany FROM #wtf a
inner JOIN (
SELECT model, opcode, COUNT(*) AS howmany
FROM #wtf
GROUP BY model, opcode) b on a.model = b.model AND a.opcode = b.opcode) x
WHERE ro = '16148912'