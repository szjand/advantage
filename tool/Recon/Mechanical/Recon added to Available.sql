SELECT VehicleInventoryItemID, fromts, thruts
FROM VehicleInventoryItemStatuses
WHERE category = 'RMFlagAV'
  AND CAST(fromts AS sql_date) > curdate() - 180
  
-- times during which a VehicleInventoryItems IS Available  
SELECT VehicleInventoryItemID, fromts, thruts
FROM VehicleInventoryItemStatuses
WHERE category = 'RMFlagAV'
  AND timestampdiff(sql_tsi_day, FromTS, now()) < 180

-- mechanical items added  
SELECT a.ReconAuthorizationID, a.VehicleInventoryItemID, a.fromts, a.thruts, b.*, c.typ
FROM ReconAuthorizations a  
INNER JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
  AND b.typ = 'AuthorizedReconItem_Added'
LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
INNER JOIN typcategories d ON c.typ = d.typ
  AND d.category = 'MechanicalReconItem'
WHERE timestampdiff(sql_tsi_day, a.FromTS, now()) < 180

-- this IS it
SELECT month, year, COUNT(*)
FROM (
  SELECT y.ReconAuthorizationID AS ra, month(x.fromTS) AS month, year(x.fromts) AS year 
  FROM (-- times during which a VehicleInventoryItems IS Available  
    SELECT VehicleInventoryItemID, fromts, thruts
    FROM VehicleInventoryItemStatuses
    WHERE category = 'RMFlagAV'
      AND timestampdiff(sql_tsi_day, FromTS, now()) < 365) x
  LEFT JOIN (-- mechanical items added
      SELECT a.ReconAuthorizationID, a.VehicleInventoryItemID, a.fromts, a.thruts, b.*, c.typ
      FROM ReconAuthorizations a  
      INNER JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
        AND b.typ = 'AuthorizedReconItem_Added'
      LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
      INNER JOIN typcategories d ON c.typ = d.typ
        AND d.category = 'MechanicalReconItem'
      WHERE timestampdiff(sql_tsi_day, a.FromTS, now()) < 365) y ON x.VehicleInventoryItemID = y.VehicleInventoryItemID 
  WHERE x.FromTS between y.FromTS AND y.ThruTS 
    AND y.VehicleInventoryItemID IS NOT NULL  
  GROUP BY y.ReconAuthorizationID, month(x.fromts), year(x.fromts)) z    
GROUP BY month, year  
ORDER BY year, month