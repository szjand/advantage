
DECLARE @Cur CURSOR AS 
  SELECT vii.VehicleInventoryItemID, vii.stocknumber, viis.FromTS
  FROM VehicleInventoryItemStatuses viis
  INNER JOIN VehicleInventoryItems vii ON vii.VehicleInventoryItemID = viis.VehicleInventoryItemID 
  WHERE status = 'RMFlagAtAuction_AtAuction'
  AND viis.thruts IS NULL
  AND EXISTS (
    SELECT 1 
	FROM ReconAuthorizations
	WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
	AND thruts IS NULL); 

OPEN @Cur;
TRY
  WHILE FETCH @Cur DO
    EXECUTE PROCEDURE UpdateReconAuthorization(@cur.VehicleInventoryItemID,@cur.FromTS);
  END WHILE;
FINALLY
  CLOSE @Cur;
END TRY;  

-----------------
-----------------
DECLARE @Cur CURSOR AS 
  SELECT vii.VehicleInventoryItemID, vii.stocknumber, viis.FromTS
  FROM VehicleInventoryItemStatuses viis
  INNER JOIN VehicleInventoryItems vii ON vii.VehicleInventoryItemID = viis.VehicleInventoryItemID 
  WHERE status = 'RMFlagAtAuction_AtAuction'
  AND viis.thruts IS NULL; 

OPEN @Cur;
TRY
  WHILE FETCH @Cur DO
    EXECUTE PROCEDURE updatevehicleinventoryitemstatuses(@cur.VehicleInventoryItemID,@cur.FromTS);
  END WHILE;
FINALLY
  CLOSE @Cur;
END TRY;  

