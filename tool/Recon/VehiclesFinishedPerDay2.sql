SELECT DISTINCT thedate, dayname
FROM auxcal.calendar
WHERE thedate < curdate()
AND year(thedate) = 2011
AND dayofweek <> 1

SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID), arix.VehicleReconItemID  
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/13/2011'
	  
	  
	    
SELECT vrix.VehicleInventoryItemID, /*cast(arix.CompleteTS as sql_date) AS FinshDate,*/ COUNT(*)
FROM AuthorizedReconItems arix
INNER JOIN VehicleReconItems vrix
  ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND vrix.typ LIKE 'Mech%'
  AND vrix.Description <> 'Inspection Fee'
AND arix.CompleteTS IS NOT NULL   	
GROUP BY vrix.VehicleInventoryItemID, cast(arix.CompleteTS as sql_date)   
ORDER BY cast(arix.CompleteTS as sql_date) DESC

-- this seems to WORK for the number, ~ 1 sec
SELECT dayname, thedate, COUNT(*) 
FROM (
SELECT vrix.VehicleInventoryItemID, max(cast(arix.CompleteTS as sql_date)) AS FinshDate, COUNT(*)
FROM AuthorizedReconItems arix
INNER JOIN VehicleReconItems vrix
  ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND vrix.typ LIKE 'Mech%'
  AND vrix.Description <> 'Inspection Fee'
AND arix.CompleteTS IS NOT NULL   	
GROUP BY vrix.VehicleInventoryItemID, cast(arix.CompleteTS as sql_date) ) a
INNER JOIN (
SELECT DISTINCT thedate, dayname
FROM auxcal.calendar
WHERE thedate <= curdate()
AND year(thedate) = 2011
AND dayofweek <> 1) b ON a.FinshDate = b.thedate
GROUP BY dayname, thedate
ORDER by thedate DESC


-- without auxcalendar
SELECT vrix.VehicleInventoryItemID, max(cast(arix.CompleteTS as sql_date)) AS FinshDate, COUNT(*)
FROM AuthorizedReconItems arix
INNER JOIN VehicleReconItems vrix
  ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND vrix.typ LIKE 'Mech%'
  AND vrix.Description <> 'Inspection Fee'
AND arix.CompleteTS IS NOT NULL   	
GROUP BY vrix.VehicleInventoryItemID, cast(arix.CompleteTS as sql_date) 
	  
	  


SELECT 
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/13/2011') AS "M Monday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/14/2011') AS "M Tuesday",      
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/15/2011') AS "M Wednesday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/17/2011') AS "M Thursday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/18/2011')AS "M Friday",     
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/19/2011')AS "M Saturday"                
FROM system.iota  