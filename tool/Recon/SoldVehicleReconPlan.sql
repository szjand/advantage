
/*select *
FROM AuthorizedReconItems arix
WHERE EXISTS (
  SELECT 1
  FROM VehicleReconItems
  WHERE typ LIKE 'Mechanical%'
  AND VehicleReconItemID = arix.VehicleReconItemID)
ORDER BY arix.completets DESC   


SELECT viix.VehicleInventoryItemID, viix.stocknumber, viix.currentpriority, 
  Utilities.CurrentViiSalesStatus(viix.VehicleInventoryItemID) AS SalesStatus,
  CASE 
    WHEN vs.VehicleInventoryItemID IS NOT NULL THEN cast(cast(vs.EstimatedDeliveryDate AS sql_date) AS sql_char)
    WHEN sb.VehicleInventoryItemID IS NOT NULL THEN cast(cast(sb.ExpectedDeliveryDate AS sql_Date) AS sql_Char)
  END AS "Promise Date"
FROM VehicleInventoryItems viix
LEFT JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID 
LEFT JOIN VehicleInventoryItemSalePending sb ON viix.VehicleInventoryItemID = sb.VehicleInventoryItemID 
WHERE viix.currentpriority >= 800
AND viix.thruts IS NULL
AND viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
ORDER BY "Promise Date"


LEFT JOIN (
  SELECT *
  FROM (EXECUTE PROCEDURE GetMechanicalReconItemsForVehicleInventoryItem(viix.VehicleInventoryItemID)) mech ON viix.VehicleInventoryItemID = mech.VehicleInventoryItemID 
*/  

SELECT viix.stocknumber, 
  (SELECT trim(exteriorColor) + ' ' + TRIM(yearmodel) + ' ' + TRIM(make) + ' ' + TRIM(model)
    FROM vehicleitems
	WHERE VehicleItemID = viix.VehicleItemID) AS Vehicle,
  viix.currentpriority, 
  Utilities.CurrentViiSalesStatus(viix.VehicleInventoryItemID) AS SalesStatus,
  CASE 
    WHEN vs.VehicleInventoryItemID IS NOT NULL THEN cast(cast(vs.EstimatedDeliveryDate AS sql_date) AS sql_char)
    WHEN sb.VehicleInventoryItemID IS NOT NULL THEN cast(cast(sb.ExpectedDeliveryDate AS sql_Date) AS sql_Char)
  END AS "Customer Promise Date",
  mech.Description AS Mechanical, mech.StartTS AS "Mech Start",
  body.Description AS Body, body.StartTS AS "Body Start",
  app.Description AS App, app.StartTS AS "App Start"
FROM VehicleInventoryItems viix
LEFT JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID 
LEFT JOIN VehicleInventoryItemSalePending sb ON viix.VehicleInventoryItemID = sb.VehicleInventoryItemID 
LEFT JOIN (
  SELECT vri.VehicleReconItemID, vri.VehicleInventoryItemID,
    ari.StartTS,
    trim(td.description) collate ADS_DEFAULT_CI + ': ' + vri.Description AS Description
  FROM VehicleReconItems vri
  INNER JOIN AuthorizedReconItems ari ON vri.VehicleReconItemID = ari.VehicleReconItemID
  LEFT JOIN TypDescriptions td ON td.Typ = vri.typ 
  WHERE  vri.Typ IN (
    SELECT Typ
    FROM TypCategories 
    WHERE Category = 'MechanicalReconItem')
  AND ari.Status <> 'AuthorizedReconItem_Complete'
  AND ari.ReconAuthorizationID in (
    SELECT ReconAuthorizationID
    FROM ReconAuthorizations
    WHERE VehicleInventoryItemID IN (
	  SELECT VehicleInventoryItemID 
	  FROM VehicleInventoryItems 
	  WHERE currentpriority > 800
	  AND thruts IS null)
    AND ThruTS IS NULL)) mech ON viix.VehicleInventoryItemID = mech.VehicleInventoryItemID 
LEFT JOIN (
  SELECT vri.VehicleReconItemID, vri.VehicleInventoryItemID,
    ari.StartTS,
    trim(td.description) collate ADS_DEFAULT_CI + ': ' + vri.Description AS Description
  FROM VehicleReconItems vri
  INNER JOIN AuthorizedReconItems ari ON vri.VehicleReconItemID = ari.VehicleReconItemID
  LEFT JOIN TypDescriptions td ON td.Typ = vri.typ 
  WHERE  vri.Typ IN (
    SELECT Typ
    FROM TypCategories 
    WHERE Category = 'BodyReconItem')
  AND ari.Status <> 'AuthorizedReconItem_Complete'
  AND ari.ReconAuthorizationID in (
    SELECT ReconAuthorizationID
    FROM ReconAuthorizations
    WHERE VehicleInventoryItemID IN (
	  SELECT VehicleInventoryItemID 
	  FROM VehicleInventoryItems 
	  WHERE currentpriority > 800
	  AND thruts IS null)
    AND ThruTS IS NULL)) body ON viix.VehicleInventoryItemID = body.VehicleInventoryItemID 	
LEFT JOIN (
  SELECT vri.VehicleReconItemID, vri.VehicleInventoryItemID,
    ari.StartTS,
    trim(td.description) collate ADS_DEFAULT_CI + ': ' + vri.Description AS Description
  FROM VehicleReconItems vri
  INNER JOIN AuthorizedReconItems ari ON vri.VehicleReconItemID = ari.VehicleReconItemID
  LEFT JOIN TypDescriptions td ON td.Typ = vri.typ 
  WHERE  vri.Typ IN (
    SELECT Typ
    FROM TypCategories 
    WHERE Category = 'AppearanceReconItem' OR Typ = 'PartyCollection_AppearanceReconItem')
  AND ari.Status <> 'AuthorizedReconItem_Complete'
  AND ari.ReconAuthorizationID in (
    SELECT ReconAuthorizationID
    FROM ReconAuthorizations
    WHERE VehicleInventoryItemID IN (
	  SELECT VehicleInventoryItemID 
	  FROM VehicleInventoryItems 
	  WHERE currentpriority > 800
	  AND thruts IS null)
    AND ThruTS IS NULL)) app ON viix.VehicleInventoryItemID = app.VehicleInventoryItemID 		
WHERE viix.currentpriority >= 800
AND viix.thruts IS NULL
AND viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
ORDER BY viix.stocknumber  


