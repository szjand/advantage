SELECT typ, left(cast(description AS sql_char),30), COUNT(*)
FROM VehicleReconItems
WHERE typ LIKE '%App%'
GROUP BY typ, left(cast(description AS sql_char),30)

SELECT length(TRIM(description))
FROM VehicleReconItems
GROUP BY length(TRIM(description))

SELECT *
FROM VehicleReconItems
WHERE length(TRIM(description)) > 70

SELECT vrix.Descr, 
  MAX(timestampdiff(sql_tsi_hour, startts, completets)), 
  MIN(timestampdiff(sql_tsi_hour, startts, completets)),
  AVG(timestampdiff(sql_tsi_hour, startts, completets))
FROM (
  SELECT LEFT(cast(description AS sql_char),30) AS Descr, VehicleReconItemID
  FROM VehicleReconItems 
  WHERE typ = 'PartyCollection_AppearanceReconItem') AS vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID
WHERE arix.StartTs IS NOT NULL
  AND arix.CompleteTS IS NOT NULL 
  AND cast(arix.StartTS AS sql_date) > '08/31/2010'
  AND timestampdiff(sql_tsi_hour, startts, completets) <> 0
GROUP BY vrix.Descr  
