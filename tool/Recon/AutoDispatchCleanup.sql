-- auto dispatch to body
-- status BodyReconDispatch_Dispatched can be deleted
SELECT viix.stocknumber, 
  Utilities.CurrentViiSalesStatus (viix.VehicleInventoryItemID), 
  Utilities.CurrentViiReconStatus (viix.VehicleInventoryItemID), viisx.*
-- SELECT COUNT(*)    
FROM VehicleInventoryItemStatuses viisx
inner JOIN VehicleInventoryItems viix ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID 
WHERE viisx.category = 'BodyReconDispatched'
AND viisx.ThruTS IS NULL 
AND NOT EXISTS ( -- NOT IN ReconBuffer
  SELECT 1
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID 
  AND category = 'RMFlagRB'
  AND ThruTS IS NULL)

-- done 11-18 against FB 
DELETE 
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID IN (
  SELECT viisx.VehicleInventoryItemID 
  -- SELECT COUNT(*)    
  FROM VehicleInventoryItemStatuses viisx
  inner JOIN VehicleInventoryItems viix ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID 
  WHERE viisx.category = 'BodyReconDispatched'
  AND viisx.ThruTS IS NULL 
  AND NOT EXISTS ( -- NOT IN ReconBuffer
    SELECT 1
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID 
    AND category = 'RMFlagRB'
    AND ThruTS IS NULL))
AND category = 'BodyReconDispatched'
AND ThruTS IS NULL 

-- historical autodispatch
-- was it dispatched to body at a time WHEN it was NOT recon buffer 
-- example: 12048A
-- 1st vehicle BDisp multiple times

SELECT viix.stocknumber , COUNT(*)
-- SELECT COUNT(*)    
FROM VehicleInventoryItemStatuses viisx
inner JOIN VehicleInventoryItems viix ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID 
WHERE viisx.category = 'BodyReconDispatched'
GROUP BY viix.stocknumber
HAVING COUNT(*) > 1

SELECT viix.stocknumber, viisx.*
FROM VehicleInventoryItemStatuses viisx
INNER JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE viisx.category = 'BodyReconDispatched'
  AND NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID
      AND category = 'RMFlagRB'
      AND FromTS >= viisx.FromTS
      AND ThruTS <= viisx.ThruTS)
  AND cast(viix.FromTS AS sql_date) > '08/01/2010'    
ORDER BY viix.stocknumber  