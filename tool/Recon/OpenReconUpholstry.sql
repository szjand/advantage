-- remove subquery joins involving VehicleReconItems, instead INNER JOIN VehicleReconItems to AuthorizedReconItems
--  & DO the magic IN the select
--SELECT DISTINCT stocknumber

-- leave VehicleInventoryItemStatuses out of the joins: this IS what's generating multiple records
-- shit, NOT just VehicleInventoryItemStatuses - could also be multiple AuthorizedReconItems , so leave out AuthorizedReconItems AND VehicleReconItems 
SELECT -- DISTINCT


  vii.stocknumber,
  CASE 
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.ReceivedTS IS NOT NULL then 'R'
	WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.OrderedTS IS NOT NULL THEN 'O'
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END AS Tires,  
  CASE WHEN uph.ReconAuthorizationID IS NOT NULL THEN 'X' END AS Upholstry,
  CASE WHEN vtm.VehicleInventoryItemID IS NOT NULL THEN 'X' END AS OnMoveList,
  ba.BodyAmt,( 
  
    SELECT 
      CASE
        WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = 0 THEN 'No Parts Req''d'
        ELSE
          CASE 
            WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 'Received'
            ELSE
              CASE 
                WHEN SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ordered'
                ELSE 'Ordered'
              END 
         END 
      END AS PartsStatus
    FROM ReconAuthorizations rax -- only OPEN ReconAuthorizations 
    INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
    INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
      AND vrix.typ LIKE 'M%'
    LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
    WHERE rax.VehicleInventoryItemID = vii.VehicleInventoryItemID 
    AND rax.ThruTS IS NULL 
    GROUP BY rax.VehicleInventoryItemID) AS PartsStatus, 

  CASE (
      SELECT status
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND category = 'BodyReconProcess'
      AND ThruTS IS NULL)
    WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
    WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'BodyReconProcess_NotStarted' THEN 
      CASE 
        WHEN (
          SELECT COUNT(VehicleInventoryItemID)
          FROM VehicleInventoryItemStatuses
          WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
          AND category = 'BodyReconDispatched'
          AND ThruTS IS NULL) = 0 THEN 'Not Started'
        ELSE
          'Dispatched'
        END
  END AS BodyStatus,  
  CASE (
      SELECT status
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND category = 'MechanicalReconProcess'
      AND ThruTS IS NULL)
    WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
    WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'MechanicalReconProcess_NotStarted' THEN 
      CASE 
        WHEN (
          SELECT COUNT(VehicleInventoryItemID)
          FROM VehicleInventoryItemStatuses
          WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
          AND category = 'MechanicalReconDispatched'
          AND ThruTS IS NULL) = 0 THEN 'Not Started'
        ELSE
          'Dispatched'
        END
  END AS MechanicalStatus,
  CASE (
      SELECT status
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND category = 'AppearanceReconProcess'
      AND ThruTS IS NULL)
    WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
    WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'AppearanceReconProcess_NotStarted' THEN 
      CASE 
        WHEN (
          SELECT COUNT(VehicleInventoryItemID)
          FROM VehicleInventoryItemStatuses
          WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
          AND category = 'AppearanceReconDispatched'
          AND ThruTS IS NULL) = 0 THEN 'Not Started'
        ELSE
          'Dispatched'
        END
  END AS AppearanceStatus,
  vii.VehicleInventoryItemID, 
  vii.LocationID, vii.OwningLocationID, vi.VIN, vi.YearModel,
  vi.Make, vi.Model, vii.CurrentPriority
	  
FROM VehicleInventoryItems vii
--INNER JOIN VehicleInventoryItemStatuses viis ON viis.VehicleInventoryItemID = vii.VehicleInventoryItemID 
--  AND viis.ThruTS IS NULL  
--  AND status LIKE '%Recon%'
--  AND status NOT LIKE 'RM%'
INNER JOIN VehicleItems vi on vi.VehicleItemID = vii.VehicleItemID -- will NOT be multiples because current inventory only
INNER JOIN MakeModelClassifications mmc on mmc.Make = vi.Make
  AND mmc.Model = vi.Model
  AND mmc.ThruTS IS NULL
/*  
INNER JOIN (
  SELECT ra.VehicleInventoryItemID
  FROM ReconAuthorizations ra
  WHERE EXISTS (
    SELECT 1
    FROM AuthorizedReconItems 
    WHERE ReconAuthorizationID = ra.ReconAuthorizationID
    AND status <> 'AuthorizedReconItem_Complete')) r ON r.VehicleInventoryItemID = vii.VehicleInventoryItemID 
*/	
INNER JOIN ReconAuthorizations rax ON vii.VehicleInventoryItemID = rax.VehicleInventoryItemID 
  AND rax.ThruTS IS NULL 
  
LEFT JOIN (
  SELECT rax.ReconAuthorizationID 
  FROM ReconAuthorizations rax
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID
  WHERE rax.ThruTS IS NULL
  AND vrix.typ = 'AppearanceReconItem_Upholstry'
  AND arix.status <> 'AuthorizedReconItem_Complete') uph ON rax.ReconAuthorizationID = uph.ReconAuthorizationID 
LEFT JOIN VehiclesToMove vtm ON vii.VehicleInventoryItemID = vtm.VehicleInventoryItemID 
  AND vtm.ThruTS IS NULL    
LEFT JOIN (  
  SELECT SUM(vrix.TotalPartsAmount + vrix.LaborAmount) AS BodyAmt, vrix.VehicleInventoryItemID
  FROM ReconAuthorizations rax
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID
  WHERE vrix.typ LIKE 'B%' 
  AND rax.ThruTS IS NULL 
  GROUP BY vrix.VehicleInventoryItemID) AS ba ON vii.VehicleInventoryItemID = ba.VehicleInventoryItemID   

LEFT JOIN (
  SELECT vrix.VehicleInventoryItemID, vrix.VehicleReconItemID 
  FROM ReconAuthorizations rax
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID
  WHERE ((vrix.typ = 'MechanicalReconItem_Tires')
    OR (vrix.typ = 'MechanicalReconItem_Other' AND vrix.Description LIKE '%tire%'))
  AND vrix.partsinstock = true) tvr ON vii.VehicleInventoryItemID = tvr.VehicleInventoryItemID   
LEFT JOIN PartsOrders po ON tvr.VehicleReconItemID = po.VehicleReconItemID   
WHERE vii.ThruTS IS NULL   

