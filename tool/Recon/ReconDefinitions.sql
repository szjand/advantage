-- Inspection items NOT authorized: --
-- Record EXISTS IN VehicleReconItems but NOT IN AuthorizedReconItems 

SELECT *
-- SELECT COUNT(*)
FROM VehicleReconItems vrix
WHERE NOT EXISTS (
  SELECT 1
  FROM AuthorizedReconItems
  WHERE VehicleReconItemID = vrix.VehicleReconItemID)


SELECT vrix.VehicleInventoryItemID, viix.stocknumber, COUNT(*)
FROM VehicleReconItems vrix
INNER JOIN VehicleInventoryItems viix ON viix.VehicleInventoryItemID = vrix.VehicleInventoryItemID 
WHERE NOT EXISTS (
  SELECT 1
  FROM AuthorizedReconItems
  WHERE VehicleReconItemID = vrix.VehicleReconItemID)
GROUP BY vrix.VehicleInventoryItemID, viix.stocknumber  
ORDER BY COUNT(*) DESC 

--> Inspection items NOT authorized: <--

SELECT *
FROM ReconAuthorizations
WHERE VehicleInventoryItemID = 'c067e026-1681-45b8-9d68-50bcdcecb892'

SELECT *
FROM AuthorizedReconItems arix
INNER JOIN ReconAuthorizations rax ON rax.ReconAuthorizationID = arix.ReconAuthorizationID
WHERE rax.VehicleInventoryItemID = 'c067e026-1681-45b8-9d68-50bcdcecb892'