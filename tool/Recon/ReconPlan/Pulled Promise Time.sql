
  select 
  stocknumber, viis.VehicleInventoryItemID, 
  (SELECT 
    CASE status
      WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
      WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'MechanicalReconProcess_NotStarted' THEN 'Open'      
    END 
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID 
    AND category = 'MechanicalReconProcess'
    AND ThruTS IS NULL ) AS mStatus1,
  (SELECT 
    CASE status
      WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
      WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'BodyReconProcess_NotStarted' THEN 'Open'      
    END 
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID 
    AND category = 'BodyReconProcess'
    AND ThruTS IS NULL ) AS bStatus1,    
  (SELECT 
    CASE status
      WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
      WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'AppearanceReconProcess_NotStarted' THEN 'Open'      
    END 
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID 
    AND category = 'AppearanceReconProcess'
    AND ThruTS IS NULL ) AS aStatus1,    
  (SELECT 
    CASE status
      WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
      WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'MechanicalReconProcess_NotStarted' THEN 'Open'      
    END 
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID 
    AND category = 'MechanicalReconProcess'
    AND ThruTS IS NULL ) AS mStatus,
  mSched, mProm,
  (SELECT 
    CASE status
      WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
      WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'BodyReconProcess_NotStarted' THEN 'Open'      
    END 
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID 
    AND category = 'BodyReconProcess'
    AND ThruTS IS NULL ) AS bStatus,   
  bSched, bProm,
  (SELECT 
    CASE status
      WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
      WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'AppearanceReconProcess_NotStarted' THEN 'Open'      
    END 
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID 
    AND category = 'AppearanceReconProcess'
    AND ThruTS IS NULL ) AS aStatus,
  aSched, aProm,            
  vii.VehicleInventoryItemID, 
  vii.LocationID,
  vii.OwningLocationID,
  vii.StockNumber,
  vi.VIN,
  Coalesce(Current_Date() - Convert(viis.FromTS, SQL_DATE), 0) AS DaysInStatus,
  Coalesce(Trim(vi.YearModel), '') + ' ' + Coalesce(Trim(vi.Make), '') + ' ' + 
    Coalesce(Trim(vi.Model), '') AS VehicleDescription,        
--  viisp.ExpectedDeliveryDate,
  (SELECT TOP 1 vpd.Amount
    FROM VehiclePricings vp
    inner join VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
    WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
    ORDER BY VehiclePricingTS DESC) as Price,
--  coalesce((
--    SELECT p.LastName
--    FROM People p 
--    WHERE p.PartyID = viisp.SalesConsultantPartyID), viisp.SalesConsultantPartyID collate ads_default_ci) AS SalesConsultantName,
-- (SELECT p.LastName
--    FROM People p 
--    WHERE p.PartyID = viisp.ManagerPartyID) AS TeamLeaderName,
  vi.ExteriorColor AS Color,
  CASE Utilities.IsViiWtf(vii.VehicleInventoryItemID)
    WHEN true THEN 'X'
    ELSE ''
  END AS WTF 
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii
  on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
INNER JOIN VehicleItems vi
  ON vi.VehicleItemID = vii.VehicleItemID 
INNER JOIN MakeModelClassifications mmc
          on mmc.Make = vi.Make
          AND mmc.Model = vi.Model
          AND mmc.ThruTS IS NULL
--INNER JOIN VehicleInventoryItemSalePending viisp ON viisp.VehicleInventoryItemID = viis.VehicleInventoryItemID
--  AND viisp.ThruTS IS NULL
LEFT JOIN ( -- ReconPlans
  SELECT v.VehicleInventoryItemID, 
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS mSched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS bSched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS aSched,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS mProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS bProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS aProm
  FROM (
    SELECT DISTINCT VehicleInventoryItemID
    FROM reconplans
    WHERE ThruTS IS NULL) v) rp ON vii.VehicleInventoryItemID = rp.VehicleInventoryItemID   
WHERE viis.Status = 'RMFlagPulled_Pulled'
AND viis.ThruTS IS NULL
ORDER BY stocknumber

--
      
      
      