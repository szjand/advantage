/*
SELECT stocknumber, tires , mechstatus 
FROM #jon 
WHERE tires <> '' 
AND mechstatus <> ''
order BY stocknumber

SELECT * FROM #jon WHERE tires <> '' order BY stocknumber

SELECT DISTINCT stocknumber, tires FROM vUsedCarsOpenRecon WHERE tires <> '' ORDER BY stocknumber

SELECT j.stocknumber, j.mechstatusdisplay, j.bodystatusdisplay, j.appstatusdisplay, 
  r.*  
FROM #jon j
LEFT JOIN reconplans r ON j.VehicleInventoryItemID = r.VehicleInventoryItemID and r.ThruTS is null --(
--  SELECT VehicleInventoryItemID, ScheduleTS AS MechSched, PromiseTS AS MechProm
--  FROM reconplans
--  WHERE typ = 'ReconPlan_Mechanical'
--  AND ThruTS IS NULL) mr ON j.VehicleInventoryItemID = mr.VehicleInventoryItemID
ORDER BY stocknumber  

-- back to , need a single reconplan status for each vehicle
-- SELECT one reconplan record for each vii/dept
-- REMEMBER - a reconplan record IS only created WHEN a pull signal IS issued

SELECT j.stocknumber, j.mechstatusdisplay, r.typ,
  r.ScheduleTS, r.PromiseTS,
  CASE 
    WHEN r.VehicleInventoryItemID IS NULL THEN 'NS - No Record'
    ELSE
      CASE
        WHEN r.ScheduleTS IS NULL THEN 'NS'
        ELSE 
        CASE 
          WHEN timestampdiff(sql_tsi_second, r.ScheduleTS, now()) > 0 and j.MechStatusDisplay <> 'WIP' THEN 'OverDue'
          ELSE 'Scheduled'
        END 
      END
  END AS MRPStatus
FROM #jon j
LEFT JOIN reconplans r ON j.VehicleInventoryItemID = r.VehicleInventoryItemID
  AND thruts IS NULL
  AND typ = 'ReconPlan_Mechanical'
WHERE j.mechstatusdisplay <> 'No Open Items'
ORDER BY stocknumber



-- left joining this to vUsedCarsReconPlans to limit the reconplan records BY VehicleInventoryItemID 
-- how to mix IN dept recon status 
-- DO that IN the view SELECT statement(combine parameters FROM 
SELECT v.VehicleInventoryItemID, 
  (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS MechSched,
  (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS BodSched,
  (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS AppSched
FROM (
  SELECT DISTINCT VehicleInventoryItemID
  FROM reconplans
  WHERE ThruTS IS NULL) v
ORDER BY v.VehicleInventoryItemID   
*/

SELECT j.stocknumber, 
/*
  j.mechstatus, j.mechstatusdisplay, rp.MechSched,
  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN j.MechStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN MechSched IS NULL THEN 'NS'
            ELSE
              CASE -- scheduled
                WHEN TimeStampDiff(sql_tsi_second, MechSched, now()) > 0 AND j.MechStatus  <> 'MechanicalReconProcess_InProcess' THEN 'OD'
                ELSE 'S'
              END 
          END 
      END 
  END AS mReconPlan,
  
  j.bodystatus, j.bodystatusdisplay, rp.BodySched,
  (SELECT TimeStampDiff(sql_tsi_second, BodySched, now()) FROM system.iota), 
  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN j.BodyStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN BodySched IS NULL THEN 'NS'
            ELSE
              CASE -- scheduled
                WHEN TimeStampDiff(sql_tsi_second, BodySched, now()) > 0 AND j.BodyStatus  <> 'BodyReconProcess_InProcess' THEN 'OD'
                ELSE 'S'
              END 
          END 
      END 
  END AS bReconPlan,
*/  
  j.appstatus, j.appstatusdisplay, rp.AppSched, 
  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN j.AppStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN AppSched IS NULL THEN 'NS'
            ELSE
              CASE -- scheduled
                WHEN TimeStampDiff(sql_tsi_second, AppSched, now()) > 0 AND j.AppStatus  <> 'AppearanceReconProcess_InProcess' THEN 'OD'
                ELSE 'S'
              END 
          END 
      END 
  END AS aReconPlan  
FROM #jon j
LEFT JOIN (
  SELECT v.VehicleInventoryItemID, 
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS MechSched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS BodySched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS AppSched,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS MechProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS BodyProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS AppProm    
  FROM (
    SELECT DISTINCT VehicleInventoryItemID
    FROM reconplans
    WHERE ThruTS IS NULL) v) rp ON j.VehicleInventoryItemID = rp.VehicleInventoryItemID 
WHERE (rp.mechsched IS NOT NULL OR rp.bodysched IS NOT NULL OR rp.appsched IS NOT NULL)   
ORDER BY stocknumber    



