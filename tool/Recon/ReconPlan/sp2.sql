DECLARE @VehicleInventoryItemID string;
DECLARE @mStatus string;
DECLARE @bStatus string;
DECLARE @aStatus string;
DECLARE @mProm string;
DECLARE @bProm string;
DECLARE @aProm string;
DECLARE @ViiProm string;
@mProm = 'Orig';
@bProm = 'Orig';
@aProm = 'Orig';
@ViiProm = 'Orig';
--@VehicleInventoryItemID = '7c0e9ccf-1c40-49bf-a6c0-831f42ced1a0';
--@VehicleInventoryItemID = 'e50eab64-bcee-44ca-aeca-85dca5559647'; --no OPEN
--@VehicleInventoryItemID = 'db84bad8-a50d-415c-8dd9-f2b93585e83c';
--@VehicleInventoryItemID = 'afbf5789-9621-4c76-abc6-2f4418c38054';

@VehicleInventoryItemID = '3425ba51-5de3-40b1-a7d3-33de43301923';
-- first determine the recon status for each dept
@mStatus = (
  SELECT 
    CASE status
      WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
      WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'MechanicalReconProcess_NotStarted' THEN 'Open'      
    END 
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND category = 'MechanicalReconProcess'
  AND ThruTS IS NULL);
@bStatus = (
  SELECT 
    CASE status 
      WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
      WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'BodyReconProcess_NotStarted' THEN 'Open'      
    END 
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND category = 'BodyReconProcess'
  AND ThruTS IS NULL);  
@aStatus = (
  SELECT 
    CASE status
      WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
      WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'AppearanceReconProcess_NotStarted' THEN 'Open'      
    END 
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND category = 'AppearanceReconProcess'
  AND ThruTS IS NULL); 
-- for mbaProm IRREL means no OPEN recon   
IF (@mStatus = 'None' AND @bStatus = 'None' AND @aStatus = 'None') THEN @ViiProm = 'vIRREL';
  ELSE
    IF @mStatus = 'None' 
      THEN @mProm = 'IRREL';
    END IF;
    IF @bStatus = 'None'
      THEN @bProm = 'IRREL';
    END IF;
    IF @aStatus = 'None'
      THEN @aProm = 'IRREL';
    END IF;
END IF;  
-- wrap it ALL IN @ViiProm <> IRREL
IF @ViiPRom <> 'IRREL' THEN 
-- for mbaProm IRREL means no OPEN recon
  IF @mProm <> 'IRREL' THEN 
    IF (SELECT COUNT(*) FROM reconplans WHERE VehicleInventoryItemID = @VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND PromiseTS IS NOT NULL AND ThruTS IS NULL) > 0 THEN 
      @mProm = (
        SELECT cast(PromiseTS AS sql_char)
        FROM reconplans 
        WHERE VehicleInventoryItemID  = @VehicleInventoryItemID  
        AND typ = 'ReconPlan_Mechanical' 
        AND ThruTS IS NULL);
    ELSE @mProm = 'NS'; -- NOT Scheduled
    END IF;
  END IF;
  IF @bProm <> 'IRREL' THEN 
    IF (SELECT COUNT(*) FROM reconplans WHERE VehicleInventoryItemID = @VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND PromiseTS IS NOT NULL AND ThruTS IS NULL) > 0 THEN    
      @bProm = (
        SELECT cast(PromiseTS AS sql_char)
        FROM reconplans 
        WHERE VehicleInventoryItemID  = @VehicleInventoryItemID 
        AND typ = 'ReconPlan_Body' 
        AND ThruTS IS NULL);
    ELSE @bProm = 'NS';
    END IF;
  END IF;
  IF @aProm <> 'IRREL' THEN 
    IF (SELECT COUNT(*) FROM reconplans WHERE VehicleInventoryItemID = @VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND PromiseTS IS NOT NULL AND ThruTS IS NULL) > 0 then   
      @aProm = (
        SELECT cast(PromiseTS AS sql_char)
        FROM reconplans 
        WHERE VehicleInventoryItemID  = @VehicleInventoryItemID 
        AND typ = 'ReconPlan_Appearance' 
        AND ThruTS IS NULL);
    ELSE @aProm = 'NS';
    END IF;
  END IF;   
-- so what are the possible values of @mProm at this point
-- IRREL
-- NS 
-- Orig  
-- TS AS Char                
  @mProm = (
    SELECT 
      CASE 
        WHEN @mProm = 'IRREL' THEN 'IRREL'
        WHEN @mProm = 'NS' THEN 'NS'
        WHEN @mProm = 'Orig' THEN 'NS'
        ELSE @mProm
      END 
    FROM system.iota); 
  @bProm = (
    SELECT 
      CASE 
        WHEN @bProm = 'IRREL' THEN 'IRREL'
        WHEN @bProm = 'NS' THEN 'NS'
        WHEN @bProm = 'Orig' THEN 'NS'
        ELSE @bProm
      END 
    FROM system.iota); 
  @aProm = (
    SELECT 
      CASE 
        WHEN @aProm = 'IRREL' THEN 'IRREL'
        WHEN @aProm = 'NS' THEN 'NS'
        WHEN @aProm = 'Orig' THEN 'NS'
        ELSE @aProm
      END 
    FROM system.iota); 
-- so what are the possible values of @mProm at this point
-- IRREL
-- NS 
-- TS AS Char 
     
-- ok, now, FINALLY what IS the ViiProm
-- first IF any IS NS, THEN ViiProm = NS

  IF (@mProm = 'NS' OR @bProm = 'NS' OR @aProm = 'NS') THEN
    @ViiProm = 'NS';
  ELSE 
-- so now NS IS out of the picture  
    @ViiProm = (
      SELECT
        CASE
          WHEN @mProm = 'IRREL' THEN                    --mIR
            CASE
              WHEN @bProm = 'IRREL' THEN                --mIR/bIR
                CASE 
                  WHEN @aProm = 'IRREL' THEN 'IRREL'    --mIR/bIR/aIR
                  ELSE @aProm                           --mIR/bIR/aTS
                END
              ELSE --                                    --mIR/bTS
                CASE
                  WHEN @aProm = 'IRREL' THEN @bProm      --mIR/bTS/aIR
                  ELSE 
                    CASE
                      WHEN @bProm > @aProm THEN @bProm   --mIR/bTS/aTS
                      ELSE @aProm
                    END 
                END 
            END
          ELSE                                          -- mTS
            CASE 
              WHEN @bProm = 'IRREL' THEN                --mTS/bIR
                CASE 
                  WHEN @aProm = 'IRREL' THEN @mProm     --mTS/bIR/aIR
                  ELSE                                  --mTS/bIR/aTS
                    CASE
                      WHEN @mProm > @aProm THEN @mProm
                      ELSE @aProm
                    END
                END
              ELSE
                CASE                                      --mTS/bTS
                  WHEN @aProm = 'IRREL' THEN              --mTS/bTS/aIR
                    CASE 
                      WHEN @mProm > @bProm THEN @mProm
                      ELSE @bProm 
                    END
                  ELSE                                    --mTS/bTS/aTS
                    CASE
                      WHEN @mProm > @bProm THEN             -- m>b
                        CASE 
                          WHEN @mProm > @aProm THEN @mProm  -- m>b & m>a :: v = m
                          ELSE @aProm 
                        END                                 -- m>b & a>m :: v = a
                      ELSE                                  -- b>m
                        CASE                                 
                          WHEN @bProm > @aProm THEN @bProm  -- b>m & b>a :: v = b
                          ELSE @aProm                       -- b>m & a>b :: v = a
                        END 
                    END 
                END 
            END 
        END
      FROM system.iota);  
   END IF; 
   
ELSE 
  @ViiProm = 'vIRREL';  
END IF; --IF NOT @ViiPRom <> 'vIRREL' 
 SELECT @ViiProm FROM system.iota;
  