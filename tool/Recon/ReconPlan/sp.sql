/*
1st, what IS the m/b/aStatus

SELECT promisets, coalesce(promiseTS, now())
FROM reconplans
WHERE VehicleInventoryItemID = '610a8710-6b86-4cf3-aef7-90247d5c3d40'
AND typ = 'ReconPlan_Body'
AND thruts IS NULL

-- result IS NOT a NULL value IN the field
-- it IS an empty recordset

*/



DECLARE @VehicleInventoryItemID string;
DECLARE @mStatus string;
DECLARE @bStatus string;
DECLARE @aStatus string;
--DECLARE @mSched string;
--DECLARE @bSched string;
--DECLARE @aSched string;
DECLARE @mProm string;
DECLARE @bProm string;
DECLARE @aProm string;
DECLARE @ViiProm string;
/*
DECLARE @ViiCur CURSOR AS 
  SELECT VehicleInventoryItemID 
  FROM vUsedCarsPulledVehicles;

OPEN @ViiCur;
TRY 
  WHILE fetch @ViiCur DO
    @VehicleInventoryItemID = @ViiCur.VehicleInventoryItemID;
*/
@VehicleInventoryItemID = '610a8710-6b86-4cf3-aef7-90247d5c3d40';
--@VehicleInventoryItemID = (SELECT VehicleInventoryItemID FROM VehicleInventoryItems WHERE thruts IS NULL);

@mStatus = (
  SELECT 
    CASE status
      WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
      WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'MechanicalReconProcess_NotStarted' THEN 'Open'      
    END 
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND category = 'MechanicalReconProcess'
  AND ThruTS IS NULL);
@bStatus = (
  SELECT 
    CASE status 
      WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
      WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'BodyReconProcess_NotStarted' THEN 'Open'      
    END 
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND category = 'BodyReconProcess'
  AND ThruTS IS NULL);  
@aStatus = (
  SELECT 
    CASE status
      WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
      WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'None'
      WHEN 'AppearanceReconProcess_NotStarted' THEN 'Open'      
    END 
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND category = 'AppearanceReconProcess'
  AND ThruTS IS NULL);  
/*  
  @mSched = (
    SELECT cast(ScheduleTS AS sql_char)
    FROM reconplans 
    WHERE VehicleInventoryItemID  = @VehicleInventoryItemID 
    AND typ = 'ReconPlan_Mechanical' 
    AND ThruTS IS NULL);
--  @mSched = CAST(@mSched AS sql_char);
*/
IF (SELECT COUNT(*) FROM reconplans WHERE VehicleInventoryItemID = @VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) > 0 THEN 
  @mProm = (
    SELECT cast(PromiseTS AS sql_char)
    FROM reconplans 
    WHERE VehicleInventoryItemID  = @VehicleInventoryItemID  
    AND typ = 'ReconPlan_Mechanical' 
    AND ThruTS IS NULL);
--ELSE @mProm = cast(cast('01/01/1950 00:00:01' AS sql_timestamp) AS sql_char);
ELSE @mProm = NULL;
END IF;
  
--  @mProm = CAST(@mProm AS sql_char);    
/*    
  @bSched = (
    SELECT cast(ScheduleTS AS sql_char)
    FROM reconplans 
    WHERE VehicleInventoryItemID  = @VehicleInventoryItemID 
    AND typ = 'ReconPlan_Body' 
    AND ThruTS IS NULL);
*/ 
IF (SELECT COUNT(*) FROM reconplans WHERE VehicleInventoryItemID = @VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) > 0 THEN    
  @bProm = (
    SELECT cast(PromiseTS AS sql_char)
    FROM reconplans 
    WHERE VehicleInventoryItemID  = @VehicleInventoryItemID 
    AND typ = 'ReconPlan_Body' 
    AND ThruTS IS NULL);
--ELSE @bProm = cast(cast('01/01/1950 00:00:01' AS sql_timestamp) AS sql_char);
ELSE @bProm = NULL;
END IF;
/*    
  @aSched = (
    SELECT cast(ScheduleTS AS sql_char)
    FROM reconplans 
    WHERE VehicleInventoryItemID  = @VehicleInventoryItemID 
    AND typ = 'ReconPlan_Appearance' 
    AND ThruTS IS NULL);
*/  
IF (SELECT COUNT(*) FROM reconplans WHERE VehicleInventoryItemID = @VehicleInventoryItemID AND typ = 'ReconPlan_Apeparance' AND ThruTS IS NULL) > 0 then   
  @aProm = (
    SELECT cast(PromiseTS AS sql_char)
    FROM reconplans 
    WHERE VehicleInventoryItemID  = @VehicleInventoryItemID 
    AND typ = 'ReconPlan_Appearance' 
    AND ThruTS IS NULL);
--ELSE @aProm = cast(cast('01/01/1950 00:00:01' AS sql_timestamp) AS sql_char);
ELSE @aProm = NULL;
END IF;                    

@mProm = (
  SELECT 
    CASE @mStatus 
      WHEN 'None' THEN NULL--''
--      WHEN 'None' THEN cast(cast('01/01/1950 00:00:01' AS sql_timestamp) AS sql_char)
    ELSE
      CASE @mProm
        WHEN NULL THEN 'NS'
        ELSE @mProm
      END 
    END
  FROM system.iota); 
@bProm = (
  SELECT 
    CASE @bStatus 
      WHEN 'None' THEN NULL--''
    ELSE
      CASE @bProm
        WHEN NULL THEN 'NS'
        ELSE @bProm
      END 
    END 
  FROM system.iota); 
@aProm = (
  SELECT 
    CASE @aStatus 
      WHEN 'None' THEN NULL--''
    ELSE
      CASE @aProm
        WHEN NULL THEN 'NS'
        ELSE @aProm
      END 
    END 
  FROM system.iota); 

-- trim(cast(month(MechSched) AS sql_char)) + '/' + trim(cast(dayofmonth(MechSched) AS sql_char))

@ViiProm = (  
  SELECT 
    CASE 
      WHEN @mProm = 'NS' OR @bProm = 'NS' OR @aProm = 'NS' THEN 'NS'
      ELSE
        CASE 
          WHEN cast(coalesce(@aProm, '01/01/1900') AS sql_date) > cast(coalesce(@BProm, '01/01/1900') AS sql_date) THEN
            CASE
              WHEN cast(coalesce(@aProm, '01/01/1900') AS sql_date) > cast(coalesce(@mProm, '01/01/1900') AS sql_date) THEN CAST(@aProm AS sql_char)
              ELSE CAST(@mProm AS sql_char)
            END 
          ELSE 
            CASE -- bProm > aProm
              WHEN cast(coalesce(@bProm, '01/01/1900') AS sql_date) > cast(coalesce(@aProm, '01/01/1900') AS sql_date) THEN CAST(@bProm AS sql_char)
              ELSE CAST(@aProm AS sql_char)            
            END 
        END 

    END 
  FROM system.iota);    
/*
@ViiProm = (
  SELECT
    CASE 
    END
  FROM system.iota;  
*/

--SELECT @mStatus, @mSched, @mProm, @bStatus, @aStatus FROM system.iota; 
  
--SELECT @mStatus, @mSched, @mProm, @bStatus, @bSched, @bProm, @aStatus, @aSched, @aProm FROM system.iota;

 SELECT @ViiProm, @aProm > @aProm AS "A > B", @aProm > @mProm AS "A > M", @bProm > @aProm AS "B > A",
--   CAST(coalesce(@aProm AS sql_date, '01/01/1900')),
--   coalesce(@aProm, '01/01/1900'),
--   CAST(@bProm AS sql_date),
--   CAST(@mProm AS sql_date), 
   @mStatus, @mProm, @bStatus, @bProm, @aStatus, @aProm FROM system.iota;
     
--END WHILE;  
--FINALLY
--  CLOSE @ViiCur;
--END;  