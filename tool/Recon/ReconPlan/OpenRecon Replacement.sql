CREATE VIEW 
   vUsedCarsOpenRecon
AS 
   SELECT 
/*

DROP view vUsedCarsOpenRecon
SELECT * FROM vUsedCarsOpenRecon WHERE stocknumber = '12510A'
  ORDER BY CurrentPriority DESC 
      WHERE category = 'MechanicalReconProcess'
      AND status <> 'MechanicalProcess_NoIncompleteReconItems'

*/

/*
these field for the grid:
-stocknumber
-partsstatus
-upholstery
-tires
WORK link
-priority
-mech status
-body status
-body $
-app status
-year
-make
-model
-color
-move list
** Schedule Status **

FUCK, grid doesn't even display sales status
so, take out the salesstatus
*/

--SELECT stocknumber, COUNT(*)
--FROM (
--
  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN rs.MechStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN MechSched IS NULL THEN 'NS'
            ELSE
              CASE -- scheduled
                WHEN TimeStampDiff(sql_tsi_second, MechSched, now()) > 0 AND rs.MechStatus  <> 'MechanicalReconProcess_InProcess' THEN 'OD'
                ELSE 'S'
              END 
          END 
      END 
  END AS mReconPlan,

  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN rs.BodyStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN BodySched IS NULL THEN 'NS'
            ELSE
              CASE -- scheduled
                WHEN TimeStampDiff(sql_tsi_second, BodySched, now()) > 0 AND rs.BodyStatus  <> 'BodyReconProcess_InProcess' THEN 'OD'
                ELSE 'S'
              END 
          END 
      END 
  END AS bReconPlan,

  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN rs.AppStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN AppSched IS NULL THEN 'NS'
            ELSE
              CASE -- scheduled
                WHEN TimeStampDiff(sql_tsi_second, AppSched, now()) > 0 AND rs.AppStatus  <> 'AppearanceReconProcess_InProcess' THEN 'OD'
                ELSE 'S'
              END 
          END 
      END 
  END AS aReconPlan,  
 
  viixx.VehicleInventoryItemID,
  viixx.stocknumber, ps.partsstatus, 
  CASE WHEN uph.VehicleInventoryItemID IS NOT NULL THEN 'X' END AS Upholstery,
  
  
  CASE -- Tires
    WHEN arix.status IS NOT NULL THEN 'D'  
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND ps.partsstatus = 'Received' then 'R'
	WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND ps.partsstatus = 'Ordered' THEN 'O'
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END AS Tires, 
  viixx.CurrentPriority,
  rs.MechStatus,
  CASE
    WHEN rs.MechStatus = 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
    WHEN rs.MechStatus = 'MechanicalReconProcess_InProcess' THEN 'WIP'
    ELSE 'No Open Items'
  END AS MechStatusDisplay,
  
  rs.BodyStatus, 
  
  CASE
    WHEN rs.BodyStatus = 'BodyReconProcess_NotStarted' THEN 'Not Started - $' + cast(coalesce(bd.BodyAmt, 0) AS sql_char)
    WHEN rs.BodyStatus = 'BodyReconProcess_InProcess' THEN 'WIP'
    ELSE 'No Open Items'
  END AS BodyStatusDisplay,
    
  rs.AppStatus,
    CASE
    WHEN rs.AppStatus = 'AppearanceReconProcess_NotStarted' THEN 'Not Started'
    WHEN rs.AppStatus = 'AppearanceReconProcess_InProcess' THEN 'WIP'
    ELSE 'No Open Items'
  END AS AppStatusDisplay,
  vix.yearmodel, vix.make, vix.model, vix.exteriorcolor,
  
  CASE -- Move List'
    WHEN vtm.VehicleInventoryItemID IS NOT NULL THEN 'X'
	ELSE ''
  END AS OnMoveList  
/**/ 
-- DROP TABLE #jon  
--INTO #jon    
FROM VehicleInventoryItems viixx -- OPEN only 
INNER JOIN ( -- limit to only VehicleInventoryItems with OPEN recon 
  SELECT viix.stocknumber, mrs.MechStatus, brs.BodyStatus, ars.AppStatus, viix.VehicleInventoryItemID 
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS MechStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'MechanicalReconProcess'
    AND status <> 'MechanicalReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) mrs ON viix.VehicleInventoryItemID = mrs.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS BodyStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'BodyReconProcess'
    AND status <> 'BodyReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) brs ON viix.VehicleInventoryItemID = brs.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS AppStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'AppearanceReconProcess'
    AND status <> 'AppearanceReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) ars ON viix.VehicleInventoryItemID = ars.VehicleInventoryItemID     
  WHERE viix.ThruTS IS NULL  
  AND (mrs.MechStatus IS NOT NULL OR brs.BodyStatus IS NOT NULL OR ars.AppStatus IS NOT NULL)) rs ON viixx.VehicleInventoryItemID = rs.VehicleInventoryItemID
-- 220 ms  
--LEFT JOIN (
--  SELECT * FROM (EXECUTE PROCEDURE GetViiPartsStatus()) wtf ) ps ON viixx.VehicleInventoryItemID = ps.VehicleInventoryItemID   
/**/
LEFT JOIN ( -- parts status
  SELECT rax.VehicleInventoryItemID, 
    CASE
      WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = 0 THEN 'No Parts Req''d'
      ELSE
        CASE 
          WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 'Received'
          ELSE
            CASE 
              WHEN SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ordered'
              ELSE 'Ordered'
            END 
       END 
    END AS PartsStatus
  FROM ReconAuthorizations rax -- only OPEN ReconAuthorizations 
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
    AND arix.Status <> 'AuthorizedReconItem_Complete' 
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
    AND vrix.typ LIKE 'M%'
  LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
  WHERE rax.ThruTS IS NULL 
  GROUP BY rax.VehicleInventoryItemID) ps ON viixx.VehicleInventoryItemID = ps.VehicleInventoryItemID 
/**/  
LEFT JOIN -- Move List
  VehiclesToMove vtm ON viixx.VehicleInventoryItemID = vtm.VehicleInventoryItemID 
  AND vtm.ThruTS IS NULL 
/**/  
LEFT JOIN ( -- upholstery
  SELECT distinct vrix.VehicleInventoryItemID -- 8/7/11: without DISTINCT generated multiple records, don't know why
  FROM VehicleReconItems vrix
  INNER JOIN AuthorizedReconItems arix on vrix.VehicleReconItemID = arix.VehicleReconItemID 
    AND arix.status <> 'AuthorizedReconItem_Complete'
  INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID      
  WHERE vrix.typ = 'AppearanceReconItem_Upholstry')uph ON viixx.VehicleInventoryItemID = uph.VehicleInventoryItemID   
/**/       
LEFT JOIN (-- yr make model color,
  SELECT yearmodel, make, model, exteriorcolor, VehicleItemID  
  FROM vehicleitems) vix ON viixx.VehicleItemID = vix.VehicleItemID 
/**/  
LEFT JOIN ( -- tires
  SELECT distinct vr.VehicleInventoryItemID, vr.VehicleReconItemID -- 8/7/11: without DISTINCT generated multiple records, don't know why
  FROM VehicleReconItems vr
  INNER JOIN AuthorizedReconItems arix ON vr.VehicleReconItemID = arix.VehicleReconItemID
  WHERE ((vr.typ = 'MechanicalReconItem_Tires')
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%'))
  AND vr.partsinstock = true
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations raix
	INNER JOIN AuthorizedReconItems arix ON raix.ReconAuthorizationID = arix.ReconAuthorizationID
	  AND arix.Status <> 'AuthorizedReconItem_Complete' 
	WHERE raix.VehicleInventoryItemID = vr.VehicleInventoryItemID 
	AND ThruTS IS NULL)) tvr ON viixx.VehicleInventoryItemID = tvr.VehicleInventoryItemID 	
LEFT JOIN -- for tire status of D
  AuthorizedReconItems arix ON tvr.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.Status = 'AuthorizedReconItem_Complete'   
LEFT JOIN ( -- body $$
  SELECT SUM(vrix.TotalPartsAmount + vrix.LaborAmount) AS BodyAmt, vrix.VehicleInventoryItemID
  FROM VehicleReconItems vrix
  INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
  WHERE vrix.typ LIKE 'B%' 
  AND rax.ThruTS IS NULL 
  GROUP BY vrix.VehicleInventoryItemID) bd ON viixx.VehicleInventoryItemID = bd.VehicleInventoryItemID    
LEFT JOIN ( -- ReconPlans
  SELECT v.VehicleInventoryItemID, 
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS MechSched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS BodySched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS AppSched,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS MechProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS BodyProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS AppProm    
  FROM (
    SELECT DISTINCT VehicleInventoryItemID
    FROM reconplans
    WHERE ThruTS IS NULL) v) rp ON viixx.VehicleInventoryItemID = rp.VehicleInventoryItemID   
/**/      
WHERE viixx.ThruTS IS NULL; 
-- AND MechStatus IS NOT NULL 

--AND appstatus IS NOT NULL 
--AND viixx.stocknumber IN ('13505a','13809a','14446a','14510xx','h3689la')
--ORDER BY viixx.stocknumber
/**/
--) wtf
--GROUP BY stocknumber
--HAVING COUNT(*) > 1 
/**/    