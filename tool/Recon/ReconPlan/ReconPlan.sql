CREATE TABLE ReconPlans (
  VehicleInventoryItemID Char(38),
  Typ Char(60),
  ScheduleTS Timestamp,
  PromiseTS Timestamp,
  FromTS Timestamp, 
  ThruTS Timestamp) IN database;
  
  
SELECT timestampadd(sql_tsi_minute, 1, '02/19/2011 11:59:00') FROM system.iota -- noon IS pm

SELECT viix.stocknumber, rax.ReconAuthorizationID 
FROM ReconAuthorizations rax
INNER JOIN VehicleInventoryItems viix ON rax.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.ThruTS IS NULL
WHERE rax.ThruTS IS NULL 
AND EXISTS (
  SELECT 1
  FROM AuthorizedReconItems
  WHERE ReconAuthorizationID = rax.ReconAuthorizationID 
  AND status <> 'AuthorizedReconItem_Complete')
ORDER BY viix.stocknumber

SELECT * FROM AuthorizedReconItems WHERE ReconAuthorizationID = '64997745-185b-450c-b3a6-b10d67bebabd'

one record IN recon plan for VehicleInventoryItem/recon dept with OPEN recon
INSERT RP record at authorization OR pull signal?
thinking pull signal, no unnecessary body records for WORK that gets done IN raw materials
the plan IS actually only required ON pulled vehicles
OPEN mech:
-- this gives me ALL OPEN mech
SELECT distinct viix.stocknumber, vrix.VehicleInventoryItemID, rax.FromTS 
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.status <>'AuthorizedReconItem_Complete'
INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
  AND rax.ThruTS IS NULL 
INNER JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.thruts IS null  
WHERE vrix.typ LIKE 'Mech%'
ORDER BY stocknumber

-- but only need OPEN mech with pull signal (pulled, sb, OR dcu)
SELECT distinct viix.stocknumber, vrix.VehicleInventoryItemID, rax.FromTS 
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.status <>'AuthorizedReconItem_Complete'
INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
  AND rax.ThruTS IS NULL 
INNER JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.thruts IS null  
WHERE vrix.typ LIKE 'Mech%'
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = vrix.VehicleInventoryItemID
  AND status IN ('RMFlagPulled_Pulled', 'RMFlagSB_SB', 'RawMaterials_Sold','RMFlagWSB_WholesaleBuffer','RMFlagAV_Available')
  AND thruts IS NULL)
ORDER BY stocknumber

-- back fill ReconPlans TABLE
-- once for each dept
-- DELETE FROM reconplans
INSERT INTO ReconPlans (VehicleInventoryItemID, typ, FromTS)
SELECT distinct vrix.VehicleInventoryItemID, 'ReconPlan_Body', rax.FromTS 
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.status <>'AuthorizedReconItem_Complete'
INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
  AND rax.ThruTS IS NULL 
INNER JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.thruts IS null  
WHERE vrix.typ LIKE '%Body%'
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = vrix.VehicleInventoryItemID
  AND status IN ('RMFlagPulled_Pulled', 'RMFlagSB_SB', 'RawMaterials_Sold','RMFlagWSB_WholesaleBuffer','RMFlagAV_Available')
  AND thruts IS NULL)


-- ReconPlanPage
-- which dept has OPEN recon - which panels to make visible
-- what DO i need to know?
-- which dept(s) has OPEN recon?
DECLARE @VehicleInventoryItemID string;
@VehicleInventoryItemID = '4a0df162-50b8-49fe-b00b-4ac26664ebf8';
SELECT 
  coalesce(SUM(CASE WHEN vrix.typ LIKE 'Mech%' THEN 1 ELSE 0 END), 0) AS Mechanical,
  coalesce(SUM(CASE WHEN vrix.typ LIKE 'Body%' THEN 1 ELSE 0 END), 0) AS Body,
  coalesce(SUM(CASE WHEN vrix.typ LIKE '%Appear%' THEN 1 ELSE 0 END), 0) AS Appearance
FROM VehicleReconItems vrix
INNER JOIN  AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID
  AND arix.status <> 'AuthorizedReconItem_Complete'  
INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID
  AND rax.ThruTS IS NULL
WHERE vrix.VehicleInventoryItemID = @VehicleInventoryItemID

-- single query
-- use above AS a derived TABLE IN JOIN to reconplans
SELECT DISTINCT typ FROM reconplans
-- existing recon plan info
-- this seems to WORK (sp.GetReconPlans)
DECLARE @VehicleInventoryItemID string;
@VehicleInventoryItemID = 'a2850b4a-ff1f-4d91-8751-06668bfa23a3';
SELECT mCount, bCount, aCount, mSchedule, mPromise, bSchedule, bPromise, aSchedule, aPromise
FROM (
  SELECT 
    coalesce(SUM(CASE WHEN vrix.typ LIKE 'Mech%' THEN 1 ELSE 0 END), 0) AS mCount,
    coalesce(SUM(CASE WHEN vrix.typ LIKE 'Body%' THEN 1 ELSE 0 END), 0) AS bCount,
    coalesce(SUM(CASE WHEN vrix.typ LIKE '%Appear%' THEN 1 ELSE 0 END), 0) AS aCount,
    @VehicleInventoryItemID  AS VehicleInventoryItemID  
  FROM VehicleReconItems vrix
  INNER JOIN  AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID
    AND arix.status <> 'AuthorizedReconItem_Complete'  
  INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID
    AND rax.ThruTS IS NULL
  WHERE vrix.VehicleInventoryItemID = @VehicleInventoryItemID) theCount
LEFT JOIN (
  SELECT ScheduleTS AS mSchedule, PromiseTS AS mPromise, @VehicleInventoryItemID AS VehicleInventoryItemID 
  FROM ReconPlans
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND typ = 'ReconPlan_Mechanical'
  AND ThruTS IS NULL) mTS ON theCount.VehicleInventoryItemID = mTS.VehicleInventoryItemID  
LEFT JOIN (
  SELECT ScheduleTS AS bSchedule, PromiseTS AS bPromise, @VehicleInventoryItemID AS VehicleInventoryItemID 
  FROM ReconPlans
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND typ = 'ReconPlan_Body'
  AND ThruTS IS NULL) bTS ON theCount.VehicleInventoryItemID = bTS.VehicleInventoryItemID  
LEFT JOIN (
  SELECT ScheduleTS AS aSchedule, PromiseTS AS aPromise, @VehicleInventoryItemID AS VehicleInventoryItemID 
  FROM ReconPlans
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND typ = 'ReconPlan_Appearance'
  AND ThruTS IS NULL) aTS ON theCount.VehicleInventoryItemID = aTS.VehicleInventoryItemID    







SELECT DISTINCT typ FROM reconplans

SELECT * FROM reconplans WHERE VehicleInventoryItemID = '4a0df162-50b8-49fe-b00b-4ac26664ebf8' AND typ = 'ReconPlan_Mechanical'

EXECUTE PROCEDURE getreconplans('a2850b4a-ff1f-4d91-8751-06668bfa23a3')


-- to be added IN SQLScripts
INSERT INTO Categories VALUES ('ReconPlan', 'ReconPlan');
INSERT INTO CategoryDescriptions VALUES ('ReconPlan', 'Recon plan', '1901-01-01 01:00:00', null);
INSERT INTO Typs VALUES ('ReconPlan_Mechanical', 'Mechanical');  
INSERT INTO Typs VALUES ('ReconPlan_Body', 'Body');  
INSERT INTO Typs VALUES ('ReconPlan_Appearance', 'Appearance');  
INSERT INTO TypCategories VALUES ('ReconPlan', 'ReconPlan_Mechanical', True); 
INSERT INTO TypCategories VALUES ('ReconPlan', 'ReconPlan_Body', True); 
INSERT INTO TypCategories VALUES ('ReconPlan', 'ReconPlan_Appearance', True); 
INSERT INTO TypDescriptions VALUES ('ReconPlan_Mechanical', '1901-01-01 01:00:00', null, 'Mechanical', 1);  
INSERT INTO TypDescriptions VALUES ('ReconPlan_Body', '1901-01-01 01:00:00', null, 'Body', 2);  
INSERT INTO TypDescriptions VALUES ('ReconPlan_Appearance', '1901-01-01 01:00:00', null, 'Appearance', 3);  

DELETE FROM TypCategories WHERE Category = 'ReconPlan'; 
DELETE FROM TypDescriptions WHERE Typ = 'ReconPlan_Mechanical';  
DELETE FROM TypDescriptions WHERE Typ = 'ReconPlan_Body';  
DELETE FROM TypDescriptions WHERE Typ = 'ReconPlan_Appearance';  
DELETE FROM Typs WHERE Typ = 'ReconPlan_Mechanical';  
DELETE FROM Typs WHERE Typ = 'ReconPlan_Body';  
DELETE FROM Typs WHERE Typ = 'ReconPlan_Appearance';  
DELETE FROM CategoryDescriptions WHERE Category = 'ReconPlan';
DELETE FROM Categories WHERE Category = 'ReconPlan';


