 DECLARE @ViiProm string;
 DECLARE @mProm string;
 DECLARE @bProm string;
 DECLARE @aProm string;
 @mProm = CAST(now() AS sql_char); --'IRREL';--
 @bProm = CAST(timestampadd(sql_tsi_day, 1, now()) AS sql_char);-- 'IRREL';--
 @aProm = 'IRREL';--CAST(now() AS sql_char);--
    @ViiProm = (
      SELECT
        CASE
          WHEN @mProm = 'IRREL' THEN                    --mIR
            CASE
              WHEN @bProm = 'IRREL' THEN                --mIR/bIR
                CASE 
                  WHEN @aProm = 'IRREL' THEN 'IRREL'    --mIR/bIR/aIR
                  ELSE @aProm                           --mIR/bIR/aTS
                END
              ELSE --                                    --mIR/bTS
                CASE
                  WHEN @aProm = 'IRREL' THEN @bProm      --mIR/bTS/aIR
                  ELSE 
                    CASE
                      WHEN @bProm > @aProm THEN @bProm   --mIR/bTS/aTS
                      ELSE @aProm
                    END 
                END 
            END
          ELSE                                          -- mTS
            CASE 
              WHEN @bProm = 'IRREL' THEN                --mTS/bIR
                CASE 
                  WHEN @aProm = 'IRREL' THEN @mProm     --mTS/bIR/aIR
                  ELSE                                  --mTS/bIR/aTS
                    CASE
                      WHEN @mProm > @aProm THEN @mProm
                      ELSE @aProm
                    END
                END
              ELSE
                CASE                                      --mTS/bTS
                  WHEN @aProm = 'IRREL' THEN              --mTS/bTS/aIR
                    CASE 
                      WHEN @mProm > @bProm THEN @mProm
                      ELSE @bProm 
                    END
                  ELSE                                    --mTS/bTS/aTS
                    CASE
                      WHEN @mProm > @bProm THEN             -- m>b
                        CASE 
                          WHEN @mProm > @aProm THEN @mProm  -- m>b & m>a :: v = m
                          ELSE @aProm 
                        END                                 -- m>b & a>m :: v = a
                      ELSE                                  -- b>m
                        CASE                                 
                          WHEN @bProm > @aProm THEN @bProm  -- b>m & b>a :: v = b
                          ELSE @aProm                       -- b>m & a>b :: v = a
                        END 
                    END 
                END 
            END 
        END
      FROM system.iota); 
SELECT @ViiProm FROM system.iota;       