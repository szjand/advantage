  SELECT distinct vr.VehicleInventoryItemID, vr.VehicleReconItemID -- 8/7/11: without DISTINCT generated multiple records, don't know why
-- SELECT *  
  FROM VehicleReconItems vr
  INNER JOIN AuthorizedReconItems arix ON vr.VehicleReconItemID = arix.VehicleReconItemID
  WHERE ((vr.typ = 'MechanicalReconItem_Tires')
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%'))
  AND vr.partsinstock = true
  AND EXISTS ( -- there IS a currently unfinished AuthorizedReconItem with an OPEN ReconAuthorization
  -- probably better to JOIN to ra
    SELECT 1
	FROM ReconAuthorizations raix
	INNER JOIN AuthorizedReconItems arix ON raix.ReconAuthorizationID = arix.ReconAuthorizationID
	  AND arix.Status <> 'AuthorizedReconItem_Complete' 
	WHERE raix.VehicleInventoryItemID = vr.VehicleInventoryItemID 
	AND ThruTS IS NULL)
AND vr.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID  
  FROM #jon
  WHERE partsstatus = 'Rcvd' AND tires = 'X')    
ORDER BY VehicleInventoryItemID   


-- this gives me the OPEN tire work
SELECT distinct vr.VehicleInventoryItemID, vr.VehicleReconItemID -- 8/7/11: without DISTINCT generated multiple records, don't know why
-- SELECT *  
FROM VehicleReconItems vr
INNER JOIN AuthorizedReconItems arix ON vr.VehicleReconItemID = arix.VehicleReconItemID
WHERE ((vr.typ = 'MechanicalReconItem_Tires')
  OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%'))
AND vr.partsinstock = true
AND arix.ReconAuthorizationID = (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations 
  WHERE VehicleInventoryItemID = vr.VehicleInventoryItemID 
  AND thruts IS NULL)
AND vr.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID  
  FROM #jon
  WHERE partsstatus = 'Rcvd' AND tires = 'X')    
ORDER BY VehicleInventoryItemID   

-- parts status
SELECT rax.VehicleInventoryItemID, 
  CASE
    WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = 0 THEN 'None'
    ELSE
      CASE 
        WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 'Rcvd'
        ELSE
          CASE 
            WHEN SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ord'
            ELSE 'Ord'
          END 
     END 
  END AS PartsStatus
FROM ReconAuthorizations rax -- only OPEN ReconAuthorizations 
INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
  AND arix.Status <> 'AuthorizedReconItem_Complete' 
INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
  AND vrix.typ LIKE 'M%'
LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
WHERE rax.ThruTS IS NULL 
AND rax.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID  
  FROM #jon
  WHERE partsstatus = 'Rcvd' AND tires = 'X')   
GROUP BY rax.VehicleInventoryItemID

SELECT * FROM partsorders
WHERE VehicleReconItemID IN (
  SELECT VehicleReconItemID
  FROM VehicleReconItems
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID  
    FROM #jon
    WHERE partsstatus = 'Rcvd' AND tires = 'X')) 

  
  CASE -- Tires
    WHEN arix.status IS NOT NULL THEN 'D'  
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND ps.partsstatus = 'Received' then 'R'
	WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND ps.partsstatus = 'Ordered' THEN 'O'
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END AS Tires
  
LEFT JOIN -- for tire status of D
  AuthorizedReconItems arix ON tvr.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.Status = 'AuthorizedReconItem_Complete'   
     
-- ok lets TRY to hone IN ON 13498a
--viiid 
SELECT * FROM VehicleInventoryItems WHERE stocknumber = '13498a'       
'77629eb9-fa18-4d66-b3d2-b569a4c907be'
--raid
SELECT * FROM ReconAuthorizations WHERE VehicleInventoryItemID = '77629eb9-fa18-4d66-b3d2-b569a4c907be' AND ThruTS IS NULL
'3d58ea1d-dc14-4202-9a14-ca4dd3c623ab'

SELECT a.VehicleReconItemID, description, status, startts, completets 
FROM AuthorizedReconItems a
LEFT JOIN VehicleReconItems vr ON a.VehicleReconItemID = vr.VehicleReconItemID 
WHERE a.ReconAuthorizationID = '3d58ea1d-dc14-4202-9a14-ca4dd3c623ab'
--vrid (tires)
'09467505-a9a5-4023-93fe-fc3d381ced60'

-- at this point i know that there IS an AuthorizedReconItem for tires that IS NOT started

-- tires ordered AND received
SELECT * FROM partsorders WHERE VehicleReconItemID = '09467505-a9a5-4023-93fe-fc3d381ced60'
