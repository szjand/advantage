/*
SELECT * FROM vehiclesales vs
WHERE vs.status = 'VehicleSale_Sold'
AND vs.typ = 'VehicleSale_Retail'
AND month(vs.soldts) > 9
AND year(vs.soldts) = 2010

SELECT * FROM vehicleduebillauthorizations ORDER BY thruts DESC
SELECT * FROM VehicleDueBillAuthorizedItems
SELECT * FROM VehicleReconItems

SELECT DISTINCT typ FROM VehicleDueBillAuthorizedItems
*/

-- deliveries with OPEN recon


SELECT DISTINCT viix.stocknumber, SoldTo,
  left(trim(mgr.fullname) + '/' +Trim(coalesce(sc.fullname, 'House')), 35) AS "Sold By",
  cast(vs.soldts AS sql_date) AS "Delivered on",
  vdb.Dept, LEFT(vdb.Recon, 50) AS Recon, vdb.Amount
FROM vehiclesales vs
INNER JOIN VehicleInventoryItems  viix ON vs.VehicleInventoryItemID = viix.VehicleInventoryItemID
LEFT JOIN people sc ON vs.SoldBy = sc.partyid
LEFT JOIN people mgr ON vs.ManagerPartyID = mgr.PartyID 
INNER JOIN VehicleDueBillAuthorizations vdba ON vs.VehicleSaleID = vdba.VehicleSaleID
INNER JOIN VehicleDueBillAuthorizedItems vdbat ON vdba.VehicleDueBillAuthorizationID = vdbat.VehicleDueBillAuthorizationID
LEFT JOIN (
  SELECT ra.VehicleDueBillAuthorizationID, 
    CASE
      WHEN vri.typ LIKE '%Appear%' THEN 'Appearance'
      WHEN vri.typ LIKE 'Body%' THEN 'Body'
      WHEN vri.typ LIKE 'Mech%' THEN 'Mechanical'
    END AS Dept,
    cast(trim(substring(vri.typ, position('_' IN vri.typ) + 1, 19)) + ': ' + vri.description AS sql_char) AS Recon,
    vri.TotalPartsAmount + vri.LaborAmount AS Amount
  FROM VehicleDueBillAuthorizations ra
  LEFT JOIN VehicleDueBillAuthorizedItems ari ON ari.VehicleDueBillAuthorizationID = ra.VehicleDueBillAuthorizationID
  LEFT JOIN VehicleReconItems vri ON ari.VehicleReconItemID = vri.VehicleReconItemID) vdb ON vdbat.VehicleDueBillAuthorizationID = vdb.VehicleDueBillAuthorizationID

WHERE vs.status = 'VehicleSale_Sold'
AND vs.typ = 'VehicleSale_Retail'
AND month(vs.soldts) > 9
AND year(vs.soldts) = 2010
AND viix.ThruTS IS NOT null
