SELECT DISTINCT status
FROM VehicleInventoryItemStatuses 
WHERE status LIKE '%Dispa%'

SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '1681f71a-e785-4121-a5e2-70fec6f20a8c'

WHERE dispatched starts ON the day
OR dispatched IS opon ON the day

SELECT d.
FROM day d, VehicleInventoryItemStatuses  a
INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations c ON b.owninglocationid = c.partyid
  AND c.name = 'rydells'
WHERE d. 
WHERE a.status LIKE '%ReconDispatched_Dispatched'


AND a.ThruTS IS NULL
AND NOT EXISTS (
  SELECT 1 
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
  AND status LIKE '%ReconProcess_InProcess'
  AND ThruTS IS NULL);
  
  
  
  
-- this IS really fucking CLOSE
-- 11/30: 36,  31 IN buffer, 4 IN wip, what IS the other one?
SELECT d.thedate, COUNT(a.VehicleInventoryItemID) --c.name, 
-- SELECT COUNT(*) -- 1797060
FROM dds.day d, VehicleInventoryItemStatuses  a
INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations c ON b.owninglocationid = c.partyid
  AND c.name in ('rydells', 'honda cartiva')
WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
  AND a.status = 'AppearanceReconDispatched_Dispatched'  
  AND d.thedate BETWEEN CAST(a.fromts AS sql_date) AND coalesce(CAST(a.thruts AS sql_date), cast('12/31/9999' AS sql_date))
--  AND d.thedate = '11/30/2012'
GROUP BY d.thedate--, c.name
ORDER BY d.thedate DESC 

-- 11/30: 36,  31 IN buffer, 4 IN wip, what IS the other one?
-- 17238a -- finished ON 11/30 @ 8:27 AM
SELECT d.thedate, b.stocknumber, b.currentpriority
-- SELECT COUNT(*) -- 1797060
FROM dds.day d, VehicleInventoryItemStatuses  a
INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations c ON b.owninglocationid = c.partyid
  AND c.name = 'rydells'
WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
  AND a.status = 'AppearanceReconDispatched_Dispatched'  
  AND d.thedate BETWEEN CAST(a.fromts AS sql_date) AND coalesce(CAST(a.thruts AS sql_date), cast('12/31/9999' AS sql_date))
  AND d.thedate = '11/30/2012'
ORDER BY b.stocknumber

-- want to include finished per day
-- maybe instead of figuring out what ALL appearance WORK got done IN a day,
-- how many of the cars IN buffer got done per day

-- available WORK BY day
SELECT d.thedate, b.VehicleInventoryItemID, 'Available' 
FROM dds.day d, VehicleInventoryItemStatuses  a
INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations c ON b.owninglocationid = c.partyid
  AND c.name in ('rydells', 'honda cartiva')
WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
  AND a.status = 'AppearanceReconDispatched_Dispatched'  
  AND d.thedate BETWEEN CAST(a.fromts AS sql_date) AND coalesce(CAST(a.thruts AS sql_date), cast('12/31/9999' AS sql_date))
  
-- available WORK BY day
-- of these vehicles, how many were finished/day
-- what does finished mean
-- dispatch status ends WHEN WORK IS done!
SELECT d.thedate, b.VehicleInventoryItemID, 'Finished' 
FROM dds.day d, VehicleInventoryItemStatuses  a
INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations c ON b.owninglocationid = c.partyid
  AND c.name in ('rydells', 'honda cartiva')
WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
  AND a.status = 'AppearanceReconDispatched_Dispatched'  
  AND d.thedate = CAST(a.thruts AS sql_date)

  
SELECT d.thedate, count(b.VehicleInventoryItemID) AS howmany, 'Finished' 
FROM dds.day d, VehicleInventoryItemStatuses  a
INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations c ON b.owninglocationid = c.partyid
  AND c.name in ('rydells', 'honda cartiva')
WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
  AND a.status = 'AppearanceReconDispatched_Dispatched'  
  AND d.thedate = CAST(a.thruts AS sql_date)  
GROUP BY d.thedate  

-- finish depends ON someone closing the vehicle out IN the tool
-- exclude saturdays AND sundays
SELECT a.thedate, a.howmany AS Available, coalesce(b.howmany, 0) AS Finished 
FROM (
  SELECT d.thedate, count(b.VehicleInventoryItemID) AS howmany, 'Available' 
  FROM dds.day d, VehicleInventoryItemStatuses  a
  INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN organizations c ON b.owninglocationid = c.partyid
    AND c.name in ('rydells', 'honda cartiva')
  WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
    AND a.status = 'AppearanceReconDispatched_Dispatched'  
    AND d.thedate BETWEEN CAST(a.fromts AS sql_date) AND coalesce(CAST(a.thruts AS sql_date), cast('12/31/9999' AS sql_date))
    AND d.dayofweek NOT IN (1,7)
  GROUP BY d.thedate) a 
LEFT JOIN (
  SELECT d.thedate, count(b.VehicleInventoryItemID) AS howmany, 'Finished' 
  FROM dds.day d, VehicleInventoryItemStatuses  a
  INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN organizations c ON b.owninglocationid = c.partyid
    AND c.name in ('rydells', 'honda cartiva')
  WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
    AND a.status = 'AppearanceReconDispatched_Dispatched'  
    AND d.thedate = CAST(a.thruts AS sql_date)  
  GROUP BY d.thedate) b ON a.thedate = b.thedate  
  
  -- finish depends ON someone closing the vehicle out IN the tool
-- exclude saturdays AND sundays
-- ADD became available per day?
SELECT a.thedate, a.howmany AS Available, coalesce(b.howmany, 0) AS Finished, 
  coalesce(c.howmany, 0) AS Became 
FROM ( -- available
  SELECT d.thedate, count(b.VehicleInventoryItemID) AS howmany, 'Available' 
  FROM dds.day d, VehicleInventoryItemStatuses  a
  INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN organizations c ON b.owninglocationid = c.partyid
    AND c.name in ('rydells', 'honda cartiva')
  WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
    AND a.status = 'AppearanceReconDispatched_Dispatched'  
    AND d.thedate BETWEEN CAST(a.fromts AS sql_date) AND coalesce(CAST(a.thruts AS sql_date), cast('12/31/9999' AS sql_date))
    AND d.dayofweek NOT IN (1,7)
  GROUP BY d.thedate) a 
LEFT JOIN ( -- finished
  SELECT d.thedate, count(b.VehicleInventoryItemID) AS howmany, 'Finished' 
  FROM dds.day d, VehicleInventoryItemStatuses  a
  INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN organizations c ON b.owninglocationid = c.partyid
    AND c.name in ('rydells', 'honda cartiva')
  WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
    AND a.status = 'AppearanceReconDispatched_Dispatched'  
    AND d.thedate = CAST(a.thruts AS sql_date)  
  GROUP BY d.thedate) b ON a.thedate = b.thedate  
LEFT JOIN ( -- became available
  SELECT d.thedate, count(b.VehicleInventoryItemID) AS howmany, 'Became' 
  FROM dds.day d, VehicleInventoryItemStatuses  a
  INNER JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN organizations c ON b.owninglocationid = c.partyid
    AND c.name in ('rydells', 'honda cartiva')
  WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
    AND a.status = 'AppearanceReconDispatched_Dispatched'  
    AND d.thedate = CAST(a.fromts AS sql_date) 
  GROUP BY d.thedate) c ON a.thedate = c.thedate   
  
  
  -- 1/11/13
  -- back at it, detail IS way fucked up, greg wants for each day:
  --  WORK available (at, say, 8 AM)
  --  WORK completed
  --  WORK added
  --  what was LEFT
  --  so, start @ 8AM, WORK available for the day, additional WORK that became
  --  available thruout the day, WORK completed for the day,
  --  at the END of the day (10 pm?), WORK available
  
  -- but it's NOT just the number of vehicles, it IS the type of WORK versus capacity
  -- that we want to look at
  -- so need to decode what WORK IS requ'd for the vehicle each day

  
  
   
SELECT a.thedate, b.*
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON a.thedate = cast(b.fromts as sql_date)
  AND category = 'AppearanceReconDispatched'
  AND fromts < timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp))
WHERE a.thedate = '01/09/2013'  


-- CREATE an 8AM timestamp for a date
SELECT fromts, CAST(fromts AS sql_date) AS thedate, 
  timestampadd(sql_tsi_hour, 8, cast(CAST(fromts AS sql_date) AS sql_timestamp))
FROM VehicleInventoryItemStatuses

-- can be dispatched multiple times IN a day :(

-- available WORK at 8AM
SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts 
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND b.thruts
  AND b.category = 'AppearanceReconDispatched'
WHERE a.thedate BETWEEN '01/01/2013' AND curdate()

-- struggling with what WORK was dispatched at 8AM
-- because of course this gives me multiple rows for a VehicleInventoryItemID ON a date
SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts, c.VehicleReconItemID, c.description, d.status, d.typ, d.startts, d.completets
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND b.thruts
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
WHERE a.thedate BETWEEN '01/01/2013' AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
--  AND b.VehicleInventoryItemID = '50aa2c61-28c6-460a-aa0f-f62b61773a6e'
ORDER BY a.thedate, b.VehicleInventoryItemID 

-- these are a problem
-- 
SELECT thedate, VehicleInventoryItemID 
FROM (
SELECT a.thedate, b.VehicleInventoryItemID, c.VehicleReconItemID
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND b.thruts
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed'
WHERE a.thedate BETWEEN '01/01/2013' AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
GROUP BY a.thedate, b.VehicleInventoryItemID, c.VehicleReconItemID) x 
GROUP BY thedate, VehicleInventoryItemID 
HAVING COUNT(*) > 1


/*
struggling with what WORK was dispatched at 8AM
-- because of course this gives me multiple rows for a VehicleInventoryItemID ON a date
*/
-- hmmm some of the dups, at least, completets must be < than thedate ????
-- OR, duh, why am i NOT looking at ReconAuthorizations 
-- the authorization that IS current ON a given datetime IS the one WHERE the thedatetime BETWEEN ra.from/thru
SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts, 
  c.VehicleReconItemID, c.description, d.status, d.typ, d.startts, d.completets,
  e.fromts, e.thruts
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
WHERE a.thedate BETWEEN '01/01/2013' AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization
ORDER BY a.thedate, b.VehicleInventoryItemID   


-- great dups fixed (only 2 IN past year) but there are NOT enough records
SELECT a.thedate, b.VehicleInventoryItemID
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
WHERE a.thedate BETWEEN '01/01/2012' AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization
GROUP BY a.thedate, b.VehicleInventoryItemID 
HAVING COUNT(*) > 1 

-- allright, lets look at those 2 dups, see IF i can tell what's up
-- fuck it CLOSE enough
SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts, 
  c.VehicleReconItemID, c.description, d.status, d.typ, d.startts, d.completets,
  e.fromts, e.thruts
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
WHERE b.VehicleInventoryItemID IN ('15b365a2-df12-4b7e-875e-26468ef7bc38','c5fd0c3e-8e82-49a9-b840-d7c78407844c')
  AND a.thedate BETWEEN '01/01/2012' AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization
ORDER BY a.thedate, b.VehicleInventoryItemID   



-- ok this IS it
-- detail WORK dispatched at 8 AM each day for the past 6 months
SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts, 
  c.VehicleReconItemID, c.description, 
  e.fromts, e.thruts
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
WHERE a.thedate BETWEEN curdate() - 180 AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization
ORDER BY a.thedate, b.VehicleInventoryItemID   

SELECT upper(job), COUNT(*), MIN(thedate), MAX(thedate) FROM (
SELECT a.thedate, count(b.VehicleInventoryItemID), cast(left(c.description, 25) AS sql_char) AS job 
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
WHERE a.thedate BETWEEN '06/01/2012' AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization
GROUP BY a.thedate, cast(left(c.description, 25) AS sql_char)
) x GROUP BY upper(job)

-- ok, need daily WORK IN terms of hours

per Ben 
Job	              D/S	    Touch Time
Pricing Rinse	    S	      .25HRS
Deluxe Wash	      S/D	    1HR
Full Detail	      D	      4HRS
Car Buff	        D	      5HRS
Truck Buff	      D	      6HRS
DROP TABLE ztouchtimes;
CREATE TABLE zTouchTimes (
  glDeptCode cichar(2),
  glDept cichar(15),
  job cichar(45),
  resource cichar(3),
  touchtime numeric(12,2));
-- will need to ADD another row for deluxe wash resource D  
-- hmmm, modelling multiple resource yikes
-- break out the combination stuff separa
-- req S 23  req D 46  req S OR D  13
-- fuck it, change resource for deluxe wash to SD
-- change resource FROM 2 rows (1 S & 1 D) to 1 row w/resource = SD
EXECUTE PROCEDURE sp_zaptable('ztouchtimes');
DELETE FROM ztouchtimes;
INSERT INTO ztouchtimes 
SELECT 'RE', 'Detail', trim(value), 
  CASE value
    WHEN 'Pricing Rinse' THEN 'S'
    WHEN 'Deluxe Wash' THEN 'SD'
    WHEN 'Auction detail car' THEN 'D'
    WHEN 'Auction buff car' THEN 'D'
    WHEN 'Auction detail truck' THEN 'D'
    WHEN 'Car detail' THEN 'D'
    WHEN 'Full Detail' THEN 'D'
    WHEN 'Truck Detail' THEN 'D'
    WHEN 'Auction buff truck' THEN 'D'
    WHEN 'Car Buff' THEN 'D'
    WHEN 'Truck Buff' THEN 'D'
  END AS resource,
  CASE value
    WHEN 'Pricing Rinse' THEN .25
    WHEN 'Deluxe Wash' THEN 1
    WHEN 'Auction detail car' THEN 1
    WHEN 'Auction buff car' THEN 5
    WHEN 'Auction detail truck' THEN 1
    WHEN 'Car detail' THEN 4
    WHEN 'Full Detail' THEN 4
    WHEN 'Truck Detail' THEN 4
    WHEN 'Auction buff truck' THEN 6
    WHEN 'Car Buff' THEN 5
    WHEN 'Truck Buff' THEN 6    
  END AS touchtime
--  amount, value
--SELECT DISTINCT value
FROM partycollections
--WHERE thruts IS NULL 
GROUP BY value;
-- mult resource
--INSERT INTO ztouchtimes
--SELECT gldeptcode, gldept, job, 'D', 1
--FROM ztouchtimes
--WHERE job = 'Deluxe Wash';
-- Auction Wash Supreme
INSERT INTO ztouchtimes
SELECT gldeptcode, gldept, 'Auction Wash Supreme', 'S', 1
FROM ztouchtimes
WHERE job = 'Deluxe Wash';

SELECT * FROM ztouchtimes ORDER BY job

-- ok this IS it
-- detail WORK dispatched at 8 AM each day for the past 6 months
-- ADD touchtimes
SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts, 
  c.VehicleReconItemID, c.description, 
  e.fromts, e.thruts, resource, touchtime
-- SELECT DISTINCT cast(description AS sql_char)
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 180 AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization
--  AND touchtime IS NULL -- oops Auction Wash Supreme
ORDER BY a.thedate, b.VehicleInventoryItemID   

-- fuck it, change resource for deluxe wash to SD

SELECT thedate, resource, COUNT(*), SUM(touchtime)
FROM (
  SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts, 
    c.VehicleReconItemID, c.description, 
    e.fromts, e.thruts, resource, touchtime
  -- SELECT DISTINCT cast(description AS sql_char)
  FROM dds.day a
  LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
    AND b.category = 'AppearanceReconDispatched'
  LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
    AND c.typ = 'PartyCollection_AppearanceReconItem'
  LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
    AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
  LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
  LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
  WHERE a.thedate BETWEEN curdate() - 61 AND curdate()
    AND d.status IS NOT NULL -- only want AuthorizedReconItems 
    AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))) x
WHERE dayofweek(thedate) BETWEEN 2 AND 6    
GROUP BY thedate, resource    

-- 1/13-- ok got WORK disp each day at 8AM
-- yesterday went off INTO timeclock & flag hours (dds\reports\detail\flag hours.sql
-- here i need, WORK added during the day
--      WORK completed during the day
--      OPEN WORK dispatched at END of day 
--      IN wip at the END of theday
SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts, 
  c.VehicleReconItemID, c.description, 
  e.fromts, e.thruts, resource, touchtime
-- SELECT DISTINCT cast(description AS sql_char)
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 180 AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization
--  AND touchtime IS NULL -- oops Auction Wash Supreme
ORDER BY a.thedate, b.VehicleInventoryItemID   

SELECT thedate, COUNT(*),
  SUM(CASE WHEN resource = 'D' THEN touchtime ELSE 0 END) AS D,
  SUM(CASE WHEN resource = 'S' THEN touchtime ELSE 0 END) AS S,
  SUM(CASE WHEN resource = 'SD' THEN touchtime ELSE 0 END) AS SD
FROM (
  SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts, 
    c.VehicleReconItemID, c.description, 
    e.fromts, e.thruts, resource, touchtime
  -- SELECT DISTINCT cast(description AS sql_char)
  FROM dds.day a
  LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
    AND b.category = 'AppearanceReconDispatched'
  LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
    AND c.typ = 'PartyCollection_AppearanceReconItem'
  LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
    AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
  LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
  LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
  WHERE a.thedate BETWEEN curdate() - 180 AND curdate()
    AND d.status IS NOT NULL -- only want AuthorizedReconItems 
    AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization) 
) x GROUP BY thedate  
  


-- finished BY day ------------------------------------------------------------
-- don't care WHEN it was dispatched OR authorized
-- but it seems that i DO care that it was authorized
  
SELECT a.thedate, b.VehicleInventoryItemID , b.fromts, b.thruts, f.job
--SELECT a.thedate, b.VehicleInventoryItemID -- there are a few dups, good enuf
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON CAST(b.thruts AS sql_date) = a.thedate
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem' -- just detail 
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed  
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID 
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 60 AND curdate()  
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization
--ORDER BY a.thedate, b.VehicleInventoryItemID 
--GROUP BY a.thedate, b.VehicleInventoryItemID 
--HAVING COUNT(*) > 1

-- finished BY day ------------------------------------------------------------

-- added BY day ---------------------------------------------------------------
-- added after 8 AM
SELECT a.thedate, b.VehicleInventoryItemID , b.fromts, b.thruts, f.job
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON CAST(b.fromts AS sql_date) = a.thedate
  AND CAST(b.fromts AS sql_time) > '08:00:00'
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem' -- just detail 
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed  
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID 
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 180 AND curdate()  
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
--ORDER BY CAST(b.fromts AS sql_time) desc  
-- added BY day ---------------------------------------------------------------

-- wip @ 8PM   ---------------------------------------------------------------

SELECT a.thedate, b.VehicleInventoryItemID , b.fromts, b.thruts, f.job
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON CAST(b.fromts AS sql_date) <= a.thedate
  AND b.status = 'AppearanceReconProcess_InProcess'
  AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp)) > timestampadd(sql_tsi_hour, 20, cast(a.thedate AS sql_timestamp))
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem' -- just detail 
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 60 AND curdate()  

-- wip @ 8PM   ---------------------------------------------------------------

-- dispatched @ 8PM   ---------------------------------------------------------
SELECT a.thedate, b.VehicleInventoryItemID, b.fromts, b.thruts, f.job, /*cast(left(c.description, 45) AS sql_char) AS Job,*/ 'Disp8PM'
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 20, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 60 AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 20, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization

-- dispatched @ 8PM   ---------------------------------------------------------
DROP TABLE #wtf;
SELECT * INTO #wtf FROM(
SELECT a.thedate, b.VehicleInventoryItemID, f.job, /*cast(left(c.description, 45) AS sql_char) AS Job,*/ 'Disp8AM'
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 60 AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))-- for the current authorization
UNION 
SELECT a.thedate, b.VehicleInventoryItemID , f.job, /*cast(left(c.description, 45) AS sql_char) AS Job,*/ 'Finished'
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON CAST(b.thruts AS sql_date) = a.thedate
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem' -- just detail 
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed  
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID 
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 60 AND curdate()  
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
UNION
SELECT a.thedate, b.VehicleInventoryItemID, f.job, /*cast(left(c.description, 45) AS sql_char) AS Job,*/ 'Added'
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON CAST(b.fromts AS sql_date) = a.thedate
  AND CAST(b.fromts AS sql_time) > '08:00:00'
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem' -- just detail 
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed  
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID 
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 60 AND curdate()  
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 8, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))  
UNION
SELECT a.thedate, b.VehicleInventoryItemID, f.job, 'WIPatDayEnd'
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON CAST(b.fromts AS sql_date) <= a.thedate
  AND b.status = 'AppearanceReconProcess_InProcess'
  AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp)) > timestampadd(sql_tsi_hour, 20, cast(a.thedate AS sql_timestamp))
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem' -- just detail 
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 60 AND curdate()   
UNION
SELECT a.thedate, b.VehicleInventoryItemID, f.job, 'Disp8PM'
FROM dds.day a
LEFT JOIN VehicleInventoryItemStatuses b ON timestampadd(sql_tsi_hour, 20, cast(a.thedate AS sql_timestamp)) BETWEEN b.fromts AND coalesce(b.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
  AND b.category = 'AppearanceReconDispatched'
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID  
  AND c.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID   
  AND d.typ <> 'AuthorizedReconItem_Removed' -- nothing that has been removed
LEFT JOIN ReconAuthorizations e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
LEFT JOIN ztouchtimes f ON UPPER(TRIM(c.description)) = upper(f.job) collate ads_default_ci
WHERE a.thedate BETWEEN curdate() - 60 AND curdate()
  AND d.status IS NOT NULL -- only want AuthorizedReconItems 
  AND timestampadd(sql_tsi_hour, 20, cast(a.thedate AS sql_timestamp)) BETWEEN e.fromts AND coalesce(e.thruts, CAST(cast('12/31/9999' AS sql_date) AS sql_timestamp))
) x

SELECT thedate, job, expr FROM #wtf GROUP BY thedate, job, expr

SELECT job FROM #wtf GROUP BY job

SELECT expr FROM #wtf GROUP BY expr

SELECT * FROM #wtf


SELECT a.thedate, a.dayname, b.*, c.*
FROM dds.day a
LEFT JOIN (
  SELECT thedate,
    SUM(CASE WHEN expr = 'Disp8AM' THEN 1 ELSE 0 END) AS "Pricing Rinse Disp8AM",
    SUM(CASE WHEN expr = 'Added' THEN 1 ELSE 0 END) AS "Pricing Rinse Added",
    SUM(CASE WHEN expr = 'Finished' THEN 1 ELSE 0 END) AS "Pricing Rinse Finished"
  FROM #wtf
  WHERE job = 'Pricing Rinse'  
  GROUP BY thedate, job) b ON a.thedate = b.thedate
LEFT JOIN (
  SELECT thedate, 
    SUM(CASE WHEN expr = 'Disp8AM' THEN 1 ELSE 0 END) AS "Detail Disp8AM",
    SUM(CASE WHEN expr = 'Added' THEN 1 ELSE 0 END) AS "Detail Added",
    SUM(CASE WHEN expr = 'Finished' THEN 1 ELSE 0 END) AS "Detail Finished"
  FROM #wtf
  WHERE job IN ('Full Detail','Truck detail')
  GROUP BY thedate, job) c ON a.thedate = c.thedate  
WHERE a.thedate BETWEEN curdate() - 60 AND curdate() 

SELECT *
FROM #wtf WHERE job = 'pricing rinse' ORDER BY VehicleInventoryItemID, thedate 
  
  SELECT thedate,
    SUM(CASE WHEN expr = 'Disp8AM' THEN 1 ELSE 0 END) AS "Pricing Rinse Disp8AM",
    SUM(CASE WHEN expr = 'Added' THEN 1 ELSE 0 END) AS "Pricing Rinse Added",
    SUM(CASE WHEN expr = 'Finished' THEN 1 ELSE 0 END) AS "Pricing Rinse Finished"
  FROM #wtf
  WHERE job = 'Pricing Rinse'  
  GROUP BY thedate, job
  
SELECT thedate, expr, COUNT(*)
FROM #wtf
GROUP BY thedate, expr

CREATE TABLE zDetail (
  thedate date,
  VehicleInventoryItemID char(38),
  job cichar(45),
  status cichar(12)) IN database;
  
INSERT INTO zDetail
SELECT * FROM #wtf;  

-- currently contains 60 days of data
SELECT a.*, b.resource, b.touchtime
FROM zdetail a
LEFT JOIN ztouchtimes b ON a.job = b.job

-- for each day, at 8AM, what WORK IS dispatched to Detail
-- dispatched means no other recon WORK needs to be done
-- AND there IS an expectation for the vehicle (pulled, sold, etc):
SELECT thedate, a.job, COUNT(*), round(SUM(touchtime),0)
FROM zdetail a
LEFT JOIN ztouchtimes b ON a.job = b.job
WHERE status = 'disp8am'
GROUP BY thedate, a.job

statuses:
  Added      
  Disp8AM    
  Disp8PM    
  Finished   
  WIPatDayEnd
  
jobs:
  Auction Wash Supreme                         
  Car Buff                                     
  Deluxe Wash                                  
  Full Detail                                  
  Pricing Rinse                                
  Truck Buff                                   
  Truck detail   



