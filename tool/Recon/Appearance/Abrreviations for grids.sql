SELECT *
FROM VehicleReconItems

SELECT typ, COUNT(*)
FROM VehicleReconItems
GROUP BY typ


SELECT typ, cast(description AS sql_char), COUNT(*)
FROM VehicleReconItems r
WHERE typ LIKE '%Ap%'
  AND typ <> 'AppearanceReconItem_Other'
  AND typ <> 'AppearanceReconItem_Upholstry'
  AND EXISTS (
    SELECT 1
	FROM VehicleInventoryItems 
	WHERE VehicleInventoryItemID = r.VehicleInventoryItemID 
	AND ThruTS IS NULL) 
GROUP BY typ, cast(description AS sql_char)



SELECT typ, left(cast(description AS sql_char), 20),
  CASE 
    WHEN typ = 'AppearanceReconItem_Other' THEN 'Other'
	WHEN typ = 'AppearanceReconItem_Upholstry' THEN 'Uph'
	WHEN upper(Description) LIKE '%BUFF%' THEN 'Buff'
	WHEN upper(Description) LIKE '%DETAIL%' THEN 'Detail'
	WHEN upper(Description) LIKE '%RINSE%' THEN 'PR' 
	ELSE ''
  END 
FROM VehicleReconItems r
WHERE typ LIKE '%Ap%'
  AND EXISTS (
    SELECT 1
	FROM VehicleInventoryItems 
	WHERE VehicleInventoryItemID = r.VehicleInventoryItemID 
	AND ThruTS IS NULL) 

    
    
  SELECT distinct vrix.VehicleInventoryItemID -- 8/7/11: without DISTINCT generated multiple records, don't know why
  FROM VehicleReconItems vrix
  INNER JOIN AuthorizedReconItems arix on vrix.VehicleReconItemID = arix.VehicleReconItemID 
    AND arix.status <> 'AuthorizedReconItem_Complete'
  INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
    AND rax.ThruTS IS NULL      
  WHERE vrix.typ LIKE '%Ap%' 
  
  -- IN vs LIKE  MUCH MUCH faster
SELECT vrix.VehicleInventoryItemID,
  MAX(
    CASE 
      WHEN vrix.typ = 'AppearanceReconItem_Other' THEN 'Other'
      WHEN vrix.typ = 'AppearanceReconItem_Upholstry' THEN 'Uph'
      WHEN upper(vrix.Description) LIKE '%BUFF%' THEN 'Buff'
      WHEN upper(vrix.Description) LIKE '%DETAIL%' THEN 'Detail'
      WHEN upper(vrix.Description) LIKE '%RINSE%' THEN 'PR' 
  	ELSE ''
    END) 
  FROM VehicleReconItems vrix
  INNER JOIN AuthorizedReconItems arix on vrix.VehicleReconItemID = arix.VehicleReconItemID 
    AND arix.status <> 'AuthorizedReconItem_Complete'
  INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
    AND rax.ThruTS IS NULL      
  WHERE vrix.typ IN ('AppearanceReconItem_Other','AppearanceReconItem_Upholstry','PartyCollection_AppearanceReconItem')
GROUP BY vrix.VehicleInventoryItemID 
  
  SELECT DISTINCT typ FROM VehicleReconItems 
  
  SELECT DISTINCT status FROM AuthorizedReconItems 
  
SELECT DISTINCT left(cast(description AS sql_char), 25) FROM VehicleReconItems WHERE typ = 'PartyCollection_AppearanceReconItem'

SELECT expr, COUNT(*)
FROM (
  SELECT left(cast(description AS sql_char), 25) 
  FROM VehicleReconItems 
  WHERE typ = 'PartyCollection_AppearanceReconItem') x
GROUP BY expr  
ORDER BY COUNT(*) DESC 

  -- IN vs LIKE  MUCH MUCH faster
-- 15881XXB has app other AND full detail, decodes AS 2 records
-- AND THEN maxing it above generates a random answer, which IN this CASE decodes
-- to Other, should be Detail?  
SELECT vrix.VehicleInventoryItemID,

    CASE 
      WHEN upper(vrix.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
      WHEN upper(vrix.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
      WHEN vrix.description = 'AUCTION WASH SUPREME' THEN 'Auction'
      WHEN upper(vrix.Description) = 'PRICING RINSE' THEN 'PR'
      WHEN vrix.typ = 'AppearanceReconItem_Other' THEN 'Other'
      WHEN vrix.typ = 'AppearanceReconItem_Upholstry' THEN 'Uph'
  	ELSE '??????'
    END

FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix on vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.status <> 'AuthorizedReconItem_Complete'
INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
  AND rax.ThruTS IS NULL      
WHERE vrix.typ  = 'PartyCollection_AppearanceReconItem'
  AND rax.VehicleInventoryItemID = '39e60762-c6e0-493e-9440-74bc7d35adce'



  
  

  -- IN vs LIKE  MUCH MUCH faster
SELECT vrix.VehicleInventoryItemID, vrix.description, 
  CASE
    WHEN vrix.typ = 'PartyCollection_AppearanceReconItem' THEN 
      CASE 
        WHEN vrix.Description in ('Full Detail', 'Car detail','Truck detail') THEN 'Detail'
        WHEN upper(vrix.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
        WHEN vrix.description = 'Auction Wash Supreme' THEN 'Auction'
      END 
    
      WHEN upper(vrix.Description) LIKE '%RINSE%' THEN 'PR'
      WHEN vrix.typ = 'AppearanceReconItem_Other' THEN 'Other'
      WHEN vrix.typ = 'AppearanceReconItem_Upholstry' THEN 'Uph'
  	ELSE '??????'
    END) 
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix on vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.status <> 'AuthorizedReconItem_Complete'
INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
  AND rax.ThruTS IS NULL      
WHERE vrix.typ IN ('AppearanceReconItem_Other','AppearanceReconItem_Upholstry','PartyCollection_AppearanceReconItem')
  AND rax.VehicleInventoryItemID = '39e60762-c6e0-493e-9440-74bc7d35adce'
  
-- multiple instances of VehicleReconItems

SELECT r.VehicleInventoryItemID,
  CASE 
    WHEN v1.VehicleInventoryItemID IS NOT NULL THEN
      CASE 
        WHEN v1.Description in ('Full Detail', 'Car detail','Truck detail') THEN 'Detail'
        WHEN upper(v1.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
        WHEN v1.description = 'Auction Wash Supreme' THEN 'Auction'
        WHEN upper(v1.DescriptiLIKE = 'PRICING RINSE' THEN 'PR'
      END 
    WHEN v2.VehicleInventoryItemID IS NOT NULL THEN 'Other'
    WHEN v3.VehicleInventoryItemID IS NOT NULL THEN 'Uph'
  ELSE '??????'
  END AS ApWk

SELECT *
FROM (  
SELECT r.VehicleInventoryItemID,
  CASE 
    WHEN v1.VehicleInventoryItemID IS NOT NULL THEN
      CASE 
        WHEN v1.Description in ('Full Detail', 'Car detail','Truck detail') THEN 'Detail'
        WHEN upper(v1.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
        WHEN v1.description = 'Auction Wash Supreme' THEN 'Auction'
        WHEN upper(v1.Description) = 'PRICING RINSE' THEN 'PR'
      END 
    WHEN v2.VehicleInventoryItemID IS NOT NULL THEN 'Other'
    WHEN v3.VehicleInventoryItemID IS NOT NULL THEN 'Uph'
--  ELSE '??????'
  END AS ApWk     
FROM ReconAuthorizations r
INNER JOIN AuthorizedReconItems a ON r.ReconAuthorizationID = a.ReconAuthorizationID
  AND a.status <> 'AuthorizedReconItem_Complete'
LEFT JOIN VehicleReconItems v1 ON a.VehicleReconItemID = v1.VehicleReconItemID 
  AND v1.typ = 'PartyCollection_AppearanceReconItem'
LEFT JOIN VehicleReconItems v2 ON a.VehicleReconItemID = v2.VehicleReconItemID 
  AND v2.typ = 'AppearanceReconItem_Other'
LEFT JOIN VehicleReconItems v3 ON a.VehicleReconItemID = v3.VehicleReconItemID 
  AND v3.typ = 'AppearanceReconItem_Upholstry'    
WHERE r.ThruTS IS NULL) x
WHERE apwk IS NOT NULL  
  AND VehicleInventoryItemID = '39e60762-c6e0-493e-9440-74bc7d35adce'
  
SELECT r.VehicleInventoryItemID, v1.description 
FROM ReconAuthorizations r 
INNER JOIN AuthorizedReconItems a ON r.ReconAuthorizationID = a.ReconAuthorizationID
  AND a.status <> 'AuthorizedReconItem_Complete'
INNER JOIN VehicleReconItems v1 ON a.VehicleReconItemID = v1.VehicleReconItemID 
WHERE r.VehicleInventoryItemID = '39e60762-c6e0-493e-9440-74bc7d35adce'
-- nope, doesn't work   
SELECT r.VehicleInventoryItemID 
FROM ReconAuthorizations r 
INNER JOIN AuthorizedReconItems a ON r.ReconAuthorizationID = a.ReconAuthorizationID
  AND a.status <> 'AuthorizedReconItem_Complete'
INNER JOIN VehicleReconItems v1 ON a.VehicleReconItemID = v1.VehicleReconItemID 
  AND 
    CASE 
      WHEN v1.typ = 'PartyCollection_AppearanceReconItem' THEN v1.typ = 'PartyCollection_AppearanceReconItem'
      WHEN v1.typ = 'AppearanceReconItem_Other' THEN v1.typ = 'AppearanceReconItem_Other'
      WHEN v1.typ = 'AppearanceReconItem_Upholstry' THEN v1.typ = 'AppearanceReconItem_Upholstry'
    END 
WHERE r.VehicleInventoryItemID = '39e60762-c6e0-493e-9440-74bc7d35adce' 
-- what i want IS 1 row per VehicleInventoryItemID 
--  

SELECT r.VehicleInventoryItemID 
FROM ReconAuthorizations r 
INNER JOIN AuthorizedReconItems a ON r.ReconAuthorizationID = a.ReconAuthorizationID
  AND a.status <> 'AuthorizedReconItem_Complete'
INNER JOIN VehicleReconItems v1 ON a.VehicleReconItemID = v1.VehicleReconItemID 
  AND 
    CASE 
      WHEN EXISTS (
        SELECT 1 
        FROM VehicleReconItems
        WHERE VehicleReconItemID = v1.VehicleReconItemID 
        AND typ = 'PartyCollection_AppearanceReconItem') THEN v1.typ = 'PartyCollection_AppearanceReconItem'
--      WHEN v1.typ = 'AppearanceReconItem_Other' THEN v1.typ = 'AppearanceReconItem_Other'
--      WHEN v1.typ = 'AppearanceReconItem_Upholstry' THEN v1.typ = 'AppearanceReconItem_Upholstry'
    END 
GROUP BY r.VehicleInventoryItemID     
WHERE r.VehicleInventoryItemID = '39e60762-c6e0-493e-9440-74bc7d35adce' 

-- back to thinking of a separate COLUMN for typ of detail work
SELECT r.VehicleInventoryItemID, v1.description,
  CASE 
    WHEN v1.VehicleInventoryItemID IS NOT NULL THEN
      CASE 
        WHEN v1.Description in ('Full Detail', 'Car detail','Truck detail') THEN 'Detail'
        WHEN upper(v1.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
        WHEN v1.description = 'Auction Wash Supreme' THEN 'Auction'
        WHEN upper(v1.Description) = 'PRICING RINSE' THEN 'PR'
      END 
  end 
FROM ReconAuthorizations r 
INNER JOIN AuthorizedReconItems a ON r.ReconAuthorizationID = a.ReconAuthorizationID
  AND a.status <> 'AuthorizedReconItem_Complete'
INNER JOIN VehicleReconItems v1 ON a.VehicleReconItemID = v1.VehicleReconItemID 
  AND v1.typ = 'PartyCollection_AppearanceReconItem'
WHERE r.ThruTS IS NULL   
-- WHERE r.VehicleInventoryItemID = '39e60762-c6e0-493e-9440-74bc7d35adce'
-- 5/1 talked to tim, he sees no need to persist the uph COLUMN
-- therefore, this should WORK fine IN vUCOpenRecon
 
  
SELECT r.VehicleInventoryItemID, 
  MAX(
    CASE 
      WHEN v1.VehicleInventoryItemID IS NOT NULL THEN
        CASE 
          WHEN upper(v1.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
          WHEN upper(v1.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
          WHEN upper(v1.description) LIKE 'AU%' THEN 'Auction'
          WHEN upper(v1.Description) = 'PRICING RINSE' THEN 'PR'
        END 
    END) AS App 
FROM ReconAuthorizations r 
INNER JOIN AuthorizedReconItems a ON r.ReconAuthorizationID = a.ReconAuthorizationID
  AND a.status <> 'AuthorizedReconItem_Complete'
INNER JOIN VehicleReconItems v1 ON a.VehicleReconItemID = v1.VehicleReconItemID 
  AND v1.typ = 'PartyCollection_AppearanceReconItem'
WHERE r.ThruTS IS NULL 
GROUP BY r.VehicleInventoryItemID  


-- this IS ok,  
SELECT r.VehicleInventoryItemID
FROM ReconAuthorizations r 
INNER JOIN AuthorizedReconItems a ON r.ReconAuthorizationID = a.ReconAuthorizationID
  AND a.status <> 'AuthorizedReconItem_Complete'
INNER JOIN VehicleReconItems v1 ON a.VehicleReconItemID = v1.VehicleReconItemID 
  AND v1.typ = 'PartyCollection_AppearanceReconItem'
WHERE r.ThruTS IS NULL  
GROUP BY r.VehicleInventoryItemID 
HAVING COUNT(*) > 1

-- but, it IS possible to ADD more than one Full Detail to a vehicle
-- should the restraint be, one row per OPEN ra/vri.typ?
