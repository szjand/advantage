-- g2g detail raw materials, 
-- don't car IF it's raw OR NOT, this should be ALL g2g whether OR NOT it's IN buffer

curr VehicleInventoryItems
     with curr ReconAuthorizations 
          with NOT started detail WORK AND no outstanding mech OR body work

SELECT * FROM typdescriptions
SELECT b.category, a.* 
SELECT * FROM typcategories WHERE category = 'PartyCollection'
SELECT * FROM statuscategories WHERE category = 'AuthorizedReconItem'
SELECT * FROM VehicleReconItems 

SELECT category, COUNT(*)
FROM VehicleReconItems a
INNER JOIN typcategories b ON a.typ = b.typ
INNER JOIN VehicleInventoryItems c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.thruts IS NULL 
GROUP BY category  

SELECT typ, COUNT(*) FROM AuthorizedReconItems GROUP BY typ
SELECT status, COUNT(*) FROM AuthorizedReconItems GROUP BY status 

SELECT a.stocknumber, c.status, d.typ, d.description
--SELECT *
FROM VehicleInventoryItems a
INNER JOIN ReconAuthorizations b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID -- with OPEN ReconAuthorizations 
  AND b.ThruTS IS NULL -- with OPEN ReconAuthorizations 
INNER JOIN AuthorizedReconItems c ON b.ReconAuthorizationID = c.ReconAuthorizationID 
  AND c.status = 'AuthorizedReconItem_NotStarted'
INNER JOIN VehicleReconItems d ON d.VehicleInventoryItemID = a.VehicleInventoryItemID
  AND c.VehicleReconItemID = d.VehicleReconItemID  
--  AND d.typ IN ('AppearanceReconItem','PartyCollection')   
INNER JOIN typcategories e ON d.typ = e.typ
  AND e.category IN ('AppearanceReconItem','PartyCollection')   
WHERE a.thruts IS NULL -- current inventory
ORDER BY stocknumber

eliminate mec & body based ON VehicleInventoryItemStatuses 

18163A Other & AppReconItem
18860A, 19412A uph & AppReconItem
H5507GA uph & other

curr VehicleInventoryItems
     with curr ReconAuthorizations 
          with NOT started detail WORK AND no outstanding mech OR body WORK

          
SELECT * FROM VehicleInventoryItemStatuses  
SELECT DISTINCT category FROM VehicleInventoryItemStatuses       
SELECT DISTINCT status FROM VehicleInventoryItemStatuses WHERE category LIKE '%Process'   
SELECT COUNT(*) FROM vusedcarsopenrecon WHERE g2g = 'X'

-- wait a minute, how does this deal with the notion of mult appear items, some OPEN, some closed
-- ok, good, these are the G2G vehicles
SELECT a.stocknumber, a.currentpriority,
  Utilities.CurrentViiLocationWithoutStore(a.VehicleInventoryItemID) AS PhysLocation,
  coalesce(f.keystatus, 'Unknown') AS KeyStatus,
  e.*
--SELECT COUNT(*)
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'BodyReconProcess_NoIncompleteReconItems'
  AND b.thruts IS NULL 
INNER JOIN VehicleInventoryItemStatuses c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.status = 'MechanicalReconProcess_NoIncompleteReconItems'
  AND c.thruts IS NULL   
INNER JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.status = 'AppearanceReconProcess_NotStarted'
  AND d.thruts IS NULL  
LEFT JOIN VehicleReconItems e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID 
INNER JOIN typcategories g ON e.typ = g.typ
  AND g.category IN ('AppearanceReconItem','PartyCollection')    
LEFT JOIN keyperkeystatus f ON a.stocknumber = f.stocknumber   
WHERE a.thruts IS NULL   
ORDER BY a.stocknumber   

-- 3/9

identify the desired vehicles
GROUP BY vehicle to get the WORK categories
JOIN those results to the rest of the shit flesh out the page

this IS CLOSE, 
but, 16271A show uph, but uphostry IS already finished
so, VehicleInventoryItemStatuses shows vehicles with ANY OPEN WORK
THEN need to determine what WORK IS OPEN
adding AuthorizedReconItems does it
SELECT a.VehicleInventoryItemID, a.stocknumber,
  MAX(
  CASE
    WHEN e.typ = 'PartyCollection_AppearanceReconItem' THEN 
      CASE
            WHEN upper(e.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
            WHEN upper(e.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
            WHEN (upper(e.description) LIKE 'DELUXE%' OR upper(e.description) LIKE 'AUCTION%') THEN 'Deluxe'
            WHEN upper(e.Description) = 'PRICING RINSE' THEN 'PR'  
      END 
  END) AS [WORK],
  max(CASE WHEN e.typ = 'AppearanceReconItem_Upholstry' THEN 'X' else '' END) AS UPH,
  max(CASE WHEN e.typ = 'AppearanceReconItem_Other' THEN 'X' ELSE '' END) AS Other 
  
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'BodyReconProcess_NoIncompleteReconItems'
  AND b.thruts IS NULL 
INNER JOIN VehicleInventoryItemStatuses c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.status = 'MechanicalReconProcess_NoIncompleteReconItems'
  AND c.thruts IS NULL   
INNER JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.status = 'AppearanceReconProcess_NotStarted'
  AND d.thruts IS NULL  
LEFT JOIN VehicleReconItems e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID 
INNER JOIN AuthorizedReconItems f ON e.VehicleReconItemID = f.VehicleReconItemID 
  AND f.status = 'AuthorizedReconItem_NotStarted'
INNER JOIN typcategories g ON e.typ = g.typ
  AND g.category IN ('AppearanceReconItem','PartyCollection')      
GROUP BY a.VehicleInventoryItemID, a.stocknumber  
ORDER BY a.stocknumber


SELECT a.stocknumber, a.currentpriority AS priority, 
  Utilities.CurrentViiLocationWithoutStore(a.VehicleInventoryItemID) AS PhysLocation,
  coalesce(b.keystatus, 'Unknown') AS KeyStatus,
  a.detail, a.uph, a.other, 
  CASE WHEN c.VehicleInventoryItemID IS NOT NULL THEN 'X' ELSE ''  END AS OnMoveList,
  CASE WHEN d.VehicleInventoryItemID IS NULL THEN '' ELSE 'X' END AS WTF,
  e.exteriorcolor AS color, 
  e.yearmodel + ' ' + TRIM(e.make) + ' ' + TRIM(e.model) AS vehicle   
FROM ( -- 1 row per G2G veh with what WORK IS needed
  SELECT a.VehicleInventoryItemID, a.stocknumber,  MAX(currentPriority) AS currentpriority,
    MAX(a.VehicleItemID) AS VehicleItemID,
    MAX(
    CASE
      WHEN e.typ = 'PartyCollection_AppearanceReconItem' THEN 
        CASE
          WHEN upper(e.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
          WHEN upper(e.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
          WHEN (upper(e.description) LIKE 'DELUXE%' OR upper(e.description) LIKE 'AUCTION%') THEN 'Deluxe'
          WHEN upper(e.Description) = 'PRICING RINSE' THEN 'PR'  
        END 
    END) AS Detail,
    max(CASE WHEN e.typ = 'AppearanceReconItem_Upholstry' THEN 'X' else '' END) AS UPH,
    max(CASE WHEN e.typ = 'AppearanceReconItem_Other' THEN 'X' ELSE '' END) AS Other 
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.status = 'BodyReconProcess_NoIncompleteReconItems'
    AND b.thruts IS NULL 
  INNER JOIN VehicleInventoryItemStatuses c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.status = 'MechanicalReconProcess_NoIncompleteReconItems'
    AND c.thruts IS NULL   
  INNER JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.status = 'AppearanceReconProcess_NotStarted'
    AND d.thruts IS NULL  
  LEFT JOIN VehicleReconItems e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID 
  INNER JOIN AuthorizedReconItems f ON e.VehicleReconItemID = f.VehicleReconItemID 
    AND f.status = 'AuthorizedReconItem_NotStarted'
  INNER JOIN typcategories g ON e.typ = g.typ
    AND g.category IN ('AppearanceReconItem','PartyCollection')      
  GROUP BY a.VehicleInventoryItemID, a.stocknumber) a
LEFT JOIN keyperkeystatus b ON a.stocknumber = b.stocknumber 
LEFT JOIN VehiclesToMove c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.ThruTS IS NULL   
LEFT JOIN viiWTF d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.ResolvedTS IS NULL 
LEFT JOIN VehicleItems e ON a.VehicleItemID = e.VehicleItemID    

/*
H5507GA shows uph but should not
SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID = '033caa44-83c1-408e-bcab-07bea0b977ec'
*/
