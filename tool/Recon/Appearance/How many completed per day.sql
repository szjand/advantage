SELECT typ, COUNT(*)
FROM VehicleReconItems 
GROUP BY typ

SELECT a.VehicleReconItemID, a.description, b.completets
FROM dds.day d
LEFT JOIN  VehicleReconItems a ON 1 = 1
LEFT JOIN AuthorizedReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
  AND CAST(completets AS sql_date) = d.thedate
WHERE d.thedate BETWEEN curdate() - 30 AND curdate()
  AND a.typ = 'PartyCollection_AppearanceReconItem'
  AND b.status = 'AuthorizedReconItem_Complete'
  
  

SELECT c.date, c.store, c.description, COUNT(*) AS [completed]
--SELECT *
FROM dds.day d
LEFT JOIN (  
  SELECT 
    (SELECT left(name, 15) AS Store
      FROM organizations
      WHERE partyid = (
        SELECT owninglocationid
        FROM VehicleInventoryItems WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)) AS store,
     left(cast(a.description AS sql_char), 25) AS description, 
     cast(b.completets AS sql_date) AS [Date]
  FROM VehicleReconItems a
  LEFT JOIN AuthorizedReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
  where a.typ = 'PartyCollection_AppearanceReconItem'
    AND b.status = 'AuthorizedReconItem_Complete') c ON d.thedate = c.Date
WHERE d.thedate BETWEEN curdate() - 7 AND curdate()
GROUP BY c.Date, c.store, c.description 

SELECT 
  (SELECT left(name, 15) AS Store
    FROM organizations
    WHERE partyid = (
      SELECT owninglocationid
      FROM VehicleInventoryItems WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)) AS store,
   left(cast(a.description AS sql_char), 25) AS description, 
   cast(b.completets AS sql_date) AS [Date]
FROM VehicleReconItems a
LEFT JOIN AuthorizedReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
where a.typ = 'PartyCollection_AppearanceReconItem'
  AND b.status = 'AuthorizedReconItem_Complete'
  AND cast(b.completets AS sql_date) BETWEEN curdate() - 7 AND curdate()

  
SELECT store, date, description, COUNT(*)
FROM (
  SELECT 
    (SELECT left(name, 15) AS Store
      FROM organizations
      WHERE partyid = (
        SELECT owninglocationid
        FROM VehicleInventoryItems WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)) AS store,
     left(cast(a.description AS sql_char), 25) AS description, 
     cast(b.completets AS sql_date) AS [Date]
  FROM VehicleReconItems a
  LEFT JOIN AuthorizedReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
  where a.typ = 'PartyCollection_AppearanceReconItem'
    AND b.status = 'AuthorizedReconItem_Complete'
    AND cast(b.completets AS sql_date) = curdate()-1) c  
GROUP BY store, date, description    

-- WHERE are Rydell truck buff coming from?
SELECT right(TRIM(CAST(curdate() AS sql_char)), 5),cast(NULL AS sql_char), cast(NULL AS sql_integer) 
FROM system.iota
UNION 
SELECT 
  CASE store
    WHEN 'Rydells' THEN 'Rydell'
    WHEN 'Honda Cartiva' THEN 'Honda'
  end AS store, left(description, 25), COUNT(*) AS [How Many]
FROM (
  SELECT c.name AS Store, CAST(d.CompleteTS AS sql_date) AS [The Date], 
     left(cast(a.description AS sql_char), 25) AS description
  FROM VehicleReconItems a
  LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  LEFT JOIN organizations c ON b.owninglocationid = c.partyid
  INNER JOIN AuthorizedReconItems d ON a.VehicleReconItemID = d.VehicleReconItemID 
  WHERE CAST(d.CompleteTS AS sql_date) = curdate() - 1-- BETWEEN '06/01/2012' AND '06/10/2012' --
    AND a.typ = 'PartyCollection_AppearanceReconItem'
    AND d.status = 'AuthorizedReconItem_Complete') z
WHERE store IN ('Rydells','Honda Cartiva')  
GROUP BY store, description 

SELECT * FROM VehicleReconItems 

SELECT c.stocknumber, LEFT(CAST(a.description AS sql_char), 25), b.typ, b.status, b.startts, b.completets
--SELECT *
FROM VehicleReconItems a
INNER JOIN AuthorizedReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
INNER JOIN VehicleInventoryItems c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE a.typ = 'PartyCollection_AppearanceReconItem'
  AND CAST(b.CompleteTS AS sql_date) BETWEEN '06/01/2012' AND '06/10/2012'
  AND LEFT(c.stocknumber, 1) <> 'c' 
ORDER BY LEFT(CAST(a.description AS sql_char), 25)  

SELECT b.name, a.value
-- SELECT *
FROM partycollections a
INNER JOIN organizations b ON a.partyid = b.partyid
WHERE b.name <> 'Crookston'
  AND a.typ = 'PartyCollection_AppearanceReconItem'
  AND a.ThruTS IS NULL 

SELECT curdate(), CAST(curdate() AS sql_char),
  right(TRIM(CAST(curdate() AS sql_char)), 5)
FROM system.iota


SELECT c.name, c.value, CAST(f.CompleteTS AS sql_date), COUNT(*)
FROM (
--  SELECT b.name, a.value
  SELECT *
  FROM partycollections a
  INNER JOIN organizations b ON a.partyid = b.partyid
  WHERE b.name <> 'Crookston'
    AND a.typ = 'PartyCollection_AppearanceReconItem'
    AND a.ThruTS IS NULL) c
LEFT JOIN VehicleInventoryItems d ON c.partyid = d.owninglocationid 
LEFT JOIN VehicleReconItems e ON d.VehicleInventoryItemID = e.VehicleInventoryItemID
  AND TRIM(cast(e.description AS sql_varchar)) = c.value collate ads_default_ci  
INNER JOIN AuthorizedReconItems f ON e.VehicleReconItemID = f.VehicleReconItemID 
  AND f.Completets IS NOT NULL   
WHERE e.description IS NOT NULL 
  AND CAST(f.CompleteTS AS sql_date) BETWEEN '06/01/2012' AND '06/10/2012'  
GROUP BY c.name, c.value, CAST(f.CompleteTS AS sql_date)
ORDER BY CAST(f.CompleteTS AS sql_date), name, value

-- 6/11
-- want one row for each date/store/job
DROP TABLE #a;
--SELECT * FROM #a
SELECT d.thedate, b.*
INTO #a
FROM dds.day d, (
  SELECT b.name, a.value, a.partyid
  FROM partycollections a
  INNER JOIN organizations b ON a.partyid = b.partyid
  WHERE b.name <> 'Crookston'
    AND a.typ = 'PartyCollection_AppearanceReconItem'
    AND a.ThruTS IS NULL) b
WHERE d.thedate BETWEEN '06/06/2012' AND '06/07/2012';  

SELECT thedate, name, value, 
  SUM(CASE WHEN d.VehicleReconItemID IS NOT NULL THEN 1 ELSE 0 END)
FROM #a a
LEFT JOIN VehicleInventoryItems b ON a.partyid = b.owninglocationid
LEFT JOIN VehicleReconItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID
  AND TRIM(cast(c.description AS sql_varchar)) = a.value collate ads_default_ci
LEFT JOIN AuthorizedReconItems d ON c.VehicleReconItemID = d.VehicleReconItemID  
  AND CAST(d.CompleteTS AS sql_date) BETWEEN '06/08/2012' AND '06/09/2012'   
GROUP BY thedate, name, value 

DROP TABLE #b; 
SELECT trim(cast(b.description AS sql_varchar)) AS Description, CAST(a.completets AS sql_date) AS FinishDate, c.owninglocationid
INTO #b
FROM AuthorizedReconItems a
LEFT JOIN VehicleReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
LEFT JOIN VehicleInventoryItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE a.status = 'AuthorizedReconItem_Complete'
  AND CAST(a.CompleteTS AS sql_date) BETWEEN '06/06/2012' AND '06/07/2012'; 
  
SELECT *
FROM #a a
LEFT JOIN #b ON a.thedate = b.FinishDate
  AND a.value = TRIM(description) collate ads_default_ci
  AND a.partyid = b.owninglocationid

  -- i think this looks good
select thedate, name, value,
  SUM(CASE WHEN description IS NOT NULL THEN 1 ELSE 0 END)
FROM #a a
LEFT JOIN #b ON a.thedate = b.FinishDate
  AND a.value = TRIM(description) collate ads_default_ci
  AND a.partyid = b.owninglocationid
GROUP BY thedate, name, value  

-- this totally rocks it
-- DROP TABLE #wtf
DECLARE @date date;
@date = curdate() - 1;
DROP TABLE #wtf;
SELECT thedate, 
  case when name = 'Honda Cartiva' then 'Honda' else name END AS store, 
  value,
  SUM(CASE WHEN description IS NOT NULL THEN 1 ELSE 0 END) AS Completed
INTO #wtf  
FROM (  
  SELECT d.thedate, b.*
  FROM dds.day d, (
    SELECT b.name, a.value, a.partyid
    FROM partycollections a
    INNER JOIN organizations b ON a.partyid = b.partyid
    WHERE b.name <> 'Crookston'
      AND a.typ = 'PartyCollection_AppearanceReconItem'
      AND a.ThruTS IS NULL) b
  WHERE d.thedate BETWEEN '12/01/2012' AND '12/31/2012') x --= @date) x
LEFT JOIN (
  SELECT trim(cast(b.description AS sql_varchar)) AS Description, CAST(a.completets AS sql_date) AS FinishDate, c.owninglocationid
  FROM AuthorizedReconItems a
  LEFT JOIN VehicleReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
  LEFT JOIN VehicleInventoryItems c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID 
  WHERE a.status = 'AuthorizedReconItem_Complete'
    AND CAST(a.CompleteTS AS sql_date) = @date) y ON x.thedate = y.finishdate
  AND x.value = TRIM(y.description) collate ads_default_ci
  AND x.partyid = y.owninglocationid
GROUP BY x.thedate, x.name, x.value;  

SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID = 'f66f81fd-8710-44f2-9f8c-0a7aa62d2ec7'

SELECT typ, COUNT(*)
FROM VehicleReconItems
WHERE typ LIKE '%Appearance%'
GROUP BY typ


SELECT * 
FROM AuthorizedReconItems 
WHERE complete