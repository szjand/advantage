SELECT ari.typ, ari.status, ari.startts, ari.completets,
  (SELECT description
  FROM VehicleReconItems
  WHERE VehicleReconItemID = ari.VehicleReconItemID)
FROM AuthorizedReconItems ari
WHERE ReconAuthorizationID IN (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE VehicleInventoryItemID = '101141ef-0617-4baf-a47e-cc124a59f2cb')
  
  
SELECT typ, status, COUNT(*)
FROM AuthorizedReconItems
GROUP BY typ,status
  
select *
FROM AuthorizedReconItems
WHERE status = 'AuthorizedReconItem_Removed'
  
SELECT *
FROM VehicleInventoryItems
WHERE VehicleInventoryItemID = (
  SELECT VehicleInventoryItemID
  FROM ReconAuthorizations
  WHERE ReconAuthorizationID = 'bf06b31f-593d-42a5-80fe-0272d86d25eb')

SELECT * FROM VehicleReconItems 
SELECT basistable, COUNT(*)
FROM VehicleReconItems
group BY basistable    
	
-- this looks LIKE the query i need
SELECT arix.VehicleReconItemID, 'AuthorizedReconItem_Initial', 'AuthorizedReconItem_NotStarted'
FROM AuthorizedReconItems arix
INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID
WHERE vrix.VehicleInventoryItemID = 'ce87ee15-216a-4a2a-ad4e-4e8aa0944e61'
AND arix.status = 'AuthorizedReconItem_NotStarted'
AND arix.ReconAuthorizationID = (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE VehicleInventoryItemID = 'ce87ee15-216a-4a2a-ad4e-4e8aa0944e61'
  AND ThruTS IS NULL)	

-- AuthorizedReconItems  
SELECT @NewReconAuthorizationID, arix.VehicleReconItemID, 'AuthorizedReconItem_Initial', 'AuthorizedReconItem_NotStarted'
FROM AuthorizedReconItems arix
INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID
WHERE vrix.VehicleInventoryItemID = 'ce87ee15-216a-4a2a-ad4e-4e8aa0944e61'
AND arix.status = 'AuthorizedReconItem_NotStarted'
AND arix.ReconAuthorizationID = (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE VehicleInventoryItemID = 'ce87ee15-216a-4a2a-ad4e-4e8aa0944e61'
  AND ThruTS IS NULL)	  
  
SELECT vrix.*
FROM AuthorizedReconItems arix
INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID
WHERE vrix.VehicleInventoryItemID = 'ce87ee15-216a-4a2a-ad4e-4e8aa0944e61'
AND arix.status = 'AuthorizedReconItem_NotStarted'
AND arix.ReconAuthorizationID = (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE VehicleInventoryItemID = 'ce87ee15-216a-4a2a-ad4e-4e8aa0944e61'
  AND ThruTS IS NULL)	    
  
  
--------------------------------------------------------------------------------
DECLARE @VRIcursor CURSOR AS
  SELECT vrix.sequence, vrix.typ, vrix.description, 
    vrix.TotalPartsAmount, vrix.LaborAmount, vrix.PartsQuantity, vrix.PartsInStock,
	vrix.UnderWarranty, vrix.WorkDone, vrix.ApplicableToFactory, vrix.ApplicableToNice,
	vrix.ApplicableToAsIs, vrix.ApplicableToAsIsWS, vrix.ApplicableToWS,	
	'NewReconAuthorizationID', 'ReconAuthorizations','ce87ee15-216a-4a2a-ad4e-4e8aa0944e61'
  FROM AuthorizedReconItems arix
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID
  WHERE vrix.VehicleInventoryItemID = 'ce87ee15-216a-4a2a-ad4e-4e8aa0944e61'
  AND arix.status = 'AuthorizedReconItem_NotStarted'
  AND arix.ReconAuthorizationID = (
    SELECT ReconAuthorizationID
    FROM ReconAuthorizations
    WHERE VehicleInventoryItemID = 'ce87ee15-216a-4a2a-ad4e-4e8aa0944e61'
    AND ThruTS IS NULL);	
DECLARE @VehicleReconItemID string;
  SELECT Utilities.DropTablesIfExist('#jon') FROM system.iota;
  
  CREATE TABLE #jon ( 
      VehicleReconItemID Char( 38 ),
      Sequence Short,
      Typ Char( 60 ),
      Description Memo,
      TotalPartsAmount Integer,
      LaborAmount Integer,
      PartsQuantity Integer,
      PartsInStock Logical,
      UnderWarranty Logical,
      WorkDone Logical,
      ApplicableToFactory Logical,
      ApplicableToNice Logical,
      ApplicableToAsIs Logical,
      ApplicableToAsIsWS Logical,
      ApplicableToWS Logical,
      TableKey Char( 38 ),
      BasisTable CIChar( 45 ),
      VehicleInventoryItemID Char( 38 ));  
  
OPEN @VRIcursor;
TRY
  WHILE FETCH @VRIcursor DO
    @VehicleReconItemID = (SELECT newidstring(d) FROM system.iota);
	INSERT INTO #jon values(
      @VehicleReconItemID, 
	  @VRIcursor.sequence, 
	  @VRIcursor.typ, 
	  @VRIcursor.description, 
      @VRIcursor.TotalPartsAmount, 
	  @VRIcursor.LaborAmount, 
	  @VRIcursor.PartsQuantity, 
	  @VRIcursor.PartsInStock,
  	  @VRIcursor.UnderWarranty, 
	  @VRIcursor.WorkDone, 
	  @VRIcursor.ApplicableToFactory, 
	  @VRIcursor.ApplicableToNice,
  	  @VRIcursor.ApplicableToAsIs, 
	  @VRIcursor.ApplicableToAsIsWS, 
	  @VRIcursor.ApplicableToWS,	
  	  'NewReconAuthorizationID', 'ReconAuthorizations','ce87ee15-216a-4a2a-ad4e-4e8aa0944e61');
  END WHILE;
FINALLY
  CLOSE @VRIcursor;
END;    
	
SELECT * FROM #jon;  