SELECT distinct viix.stocknumber, po.OrderedTS, po.ReceivedTS 
FROM partsorders po
LEFT JOIN VehicleReconItems vrix ON po.VehicleReconItemID = vrix.VehicleReconItemID
LEFT JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID

-- looking for MAX duration of parts ORDER per vehicle
-- so, that would be MIN(OrderedTS) to MAX(ReceivedTS)
-- for any GROUP of VehicleReconItem for a VehicleInventoryItem
-- except, that won't WORK for multiple parts orders originated at different times

-- so maybe a different tack, start with the Dispatch duration
-- hmm, multiple dispatches
SELECT status, VehicleInventoryItemID, COUNT(*)
FROM VehicleInventoryItemStatuses
WHERE status = 'MechanicalReconDispatched_Dispatched'
GROUP BY status, VehicleInventoryItemID
HAVING COUNT(*) > 1
-- yep, ON a spot check, these look LIKE valid multiple dispatches
-- what IS the unique identifier of a dispatch (VehicleInventoryItemID AND FromTS)
SELECT stocknumber, fromts, thruts
FROM VehicleInventoryItems
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM VehicleInventoryItemStatuses
  WHERE status = 'MechanicalReconDispatched_Dispatched'
  GROUP BY status, VehicleInventoryItemID
  HAVING COUNT(*) > 1)
ORDER BY stocknumber  
 
-- the question IS, WHILE a vehicle was dispatched to mechanical, how much of that
-- time was waiting for parts 

-- returns more than one row
/*
SELECT VehicleInventoryItemID, category, fromts, thruts,
  (SELECT orderedts
    FROM partsorders
    WHERE VehicleReconItemID IN (
      SELECT VehicleReconItemID
      FROM VehicleReconItems
      WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID))
FROM VehicleInventoryItemStatuses viisx
WHERE category = 'MechanicalRecounDispatched'
*/

-- this IS potentially useful
-- compare this to VehicleInventoryItemStatuses of MechDispatch
SELECT VehicleInventoryItemID, orderedts, MAX(receivedts) AS receivedts
FROM (
  SELECT VehicleReconItemID, orderedts, receivedts,
    (SELECT VehicleInventoryItemID
      FROM VehicleReconItems
      WHERE VehicleReconItemID = po.VehicleReconItemID) AS VehicleInventoryItemID  
  FROM partsorders po) wtf
GROUP BY VehicleInventoryItemID, orderedts  

SELECT * FROM partsorders ORDER BY Orderedts

-- this IS pretty good, but does NOT effectively represent multiple dispatch/orders
-- this may NOT be possible without RO numbers
-- 12921XXAu
SELECT stock, DispTS, ThruTS AS MechFinished, OrderedTS, ReceivedTS,
  TimeStampDiff(sql_tsi_day, cast(DispTS AS sql_date), cast(ThruTS AS sql_date)) AS "Mech Days",
  CASE
    WHEN OrderedTS IS NULL THEN 'Capacity'
    WHEN coalesce(ReceivedTS, cast('01/01/2020 00:00:00' AS sql_timestamp)) > DispTS THEN 'Parts'
    ELSE 'Capacity'
  END AS "Waiting For",
  COALESCE(
    CASE 
      WHEN OrderedTS IS NOT NULL THEN 
        CASE 
          WHEN coalesce(ReceivedTS, cast('01/01/2020 00:00:00' AS sql_timestamp)) > DispTS THEN
            TimeStampDiff(sql_tsi_day, CAST(DispTS AS sql_date), CAST(ReceivedTS AS sql_date)) 
        END 
    END, 0) AS "Parts Days"
FROM (
  SELECT viisx.VehicleInventoryItemID,
    (SELECT stocknumber 
      FROM VehicleInventoryItems 
      WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID) AS Stock,
    category, FromTS AS DispTS, ThruTS 
  FROM VehicleInventoryItemStatuses viisx
  WHERE viisx.category = 'MechanicalReconDispatched'
  AND CAST(viisx.FromTS AS sql_date) > '03/10/2011') AS d
LEFT JOIN (
  SELECT VehicleInventoryItemID, orderedts, MAX(receivedts) AS receivedts
  FROM (
    SELECT VehicleReconItemID, orderedts, receivedts,
      (SELECT VehicleInventoryItemID
        FROM VehicleReconItems
        WHERE VehicleReconItemID = po.VehicleReconItemID) AS VehicleInventoryItemID  
    FROM partsorders po) wtf
  GROUP BY VehicleInventoryItemID, orderedts) p ON d.VehicleInventoryItemID = p.VehicleInventoryItemID 
ORDER BY stock  








SELECT "Waiting For", avg("Mech Days") AS "Avg Mech Days", avg("Parts Days") as "Avg Parts Days", COUNT(*)
FROM (
SELECT stock, DispTS, ThruTS AS MechFinished, OrderedTS, ReceivedTS,
  TimeStampDiff(sql_tsi_day, cast(DispTS AS sql_date), cast(ThruTS AS sql_date)) AS "Mech Days",
  CASE
    WHEN OrderedTS IS NULL THEN 'Capacity'
    WHEN coalesce(ReceivedTS, cast('01/01/2020 00:00:00' AS sql_timestamp)) > DispTS THEN 'Parts'
    ELSE 'Capacity'
  END AS "Waiting For",
  COALESCE(
    CASE 
      WHEN OrderedTS IS NOT NULL THEN 
        CASE 
          WHEN coalesce(ReceivedTS, cast('01/01/2020 00:00:00' AS sql_timestamp)) > DispTS THEN
            TimeStampDiff(sql_tsi_day, CAST(DispTS AS sql_date), CAST(ReceivedTS AS sql_date)) 
        END 
    END, 0) AS "Parts Days"
FROM (
  SELECT viisx.VehicleInventoryItemID,
    (SELECT stocknumber 
      FROM VehicleInventoryItems 
      WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID) AS Stock,
    category, FromTS AS DispTS, ThruTS 
  FROM VehicleInventoryItemStatuses viisx
  WHERE viisx.category = 'MechanicalReconDispatched'
  AND CAST(viisx.FromTS AS sql_date) > '03/10/2011') AS d
LEFT JOIN (
  SELECT VehicleInventoryItemID, orderedts, MAX(receivedts) AS receivedts
  FROM (
    SELECT VehicleReconItemID, orderedts, receivedts,
      (SELECT VehicleInventoryItemID
        FROM VehicleReconItems
        WHERE VehicleReconItemID = po.VehicleReconItemID) AS VehicleInventoryItemID  
    FROM partsorders po) wtf
  GROUP BY VehicleInventoryItemID, orderedts) p ON d.VehicleInventoryItemID = p.VehicleInventoryItemID) wtf GROUP BY "Waiting For"
  

-- 7/23
-- which VehicleReconItems are authorized at time of dispatch

SELECT viix.stocknumber, viisx.VehicleInventoryItemID, CAST(viisx.FromTS AS sql_date) AS DispFrom, 
  CAST(viisx.ThruTS AS sql_Date) AS MechFinished, vrix.VehicleReconItemID, 
  vrix.typ, vrix.Description
FROM VehicleInventoryItemStatuses viisx
LEFT JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
LEFT JOIN VehicleReconItems vrix ON viisx.VehicleInventoryItemID = vrix.VehicleInventoryItemID
  AND vrix.typ LIKE 'Mech%'
  AND vrix.typ <> 'MechanicalReconItem_Inspection' 
WHERE viisx.Category = 'MechanicalReconDispatched'
AND CAST(viisx.FromTS AS sql_date) > '03/10/2011'
ORDER BY viix.stocknumber

-- want only authorized VehicleReconItems 
SELECT DISTINCT (
  SELECT stocknumber
  FROM VehicleInventoryItems 
  WHERE VehicleInventoryItemID = vrix.VehicleInventoryItemID),
  vrix.VehicleInventoryItemID, vrix.VehicleReconItemID, vrix.typ, 
  left(cast(vrix.Description AS sql_char),25), rax.ReconAuthorizationID, rax.FromTS, rax.ThruTS 
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
LEFT JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID 
INNER JOIN VehicleInventoryItemStatuses viisx ON vrix.VehicleInventoryItemID = viisx.VehicleInventoryItemID 
  AND viisx.category = 'MechanicalReconDispatched'
  AND CAST(viisx.FromTS AS sql_date) > '03/10/2011'
WHERE vrix.typ LIKE 'Mech%'
AND vrix.typ <> 'MechanicalReconItem_Inspection'
ORDER BY (
  SELECT stocknumber
  FROM VehicleInventoryItems 
  WHERE VehicleInventoryItemID = vrix.VehicleInventoryItemID) 



