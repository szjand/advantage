select DISTINCT status FROM VehicleInventoryItemStatuses WHERE lower(status) LIKE '%disp%'


select * 
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = 'fccbe225-b36d-4b57-a491-408c10fb7548'
ORDER BY fromts


select *
FROM VehicleInventoryItems a
WHERE cast(fromts AS sql_date) > '12/31/2015'

SELECT category, avg(hours), COUNT(*)
FROM (
SELECT b.stocknumber, a.category, timestampdiff(sql_tsi_hour, a.fromts, a.thruts) AS hours
FROM VehicleInventoryItemStatuses a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE lower(a.category) LIKE '%dispatched'
  AND a.thruts IS NOT NULL 
  AND cast(a.fromts AS sql_date) > '12/31/2014'
) x
GROUP BY category 

SELECT a.VehicleInventoryItemID, b.stocknumber, a.category, 
  a.fromts, a.thruts, 
  timestampdiff(sql_tsi_hour, a.fromts, a.thruts) AS hours
FROM VehicleInventoryItemStatuses a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE lower(a.category) LIKE '%dispatched' 
  AND a.thruts IS not NULL 
  AND cast(a.fromts AS sql_date) > '12/31/2015'
ORDER BY hours DESC   



SELECT a.VehicleInventoryItemID, b.stocknumber, a.category, a.status,
  a.fromts, a.thruts
FROM VehicleInventoryItemStatuses a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE (lower(a.category) LIKE '%dispatched' OR lower(a.status) LIKE '%inprocess')
  AND a.thruts IS not NULL 
  AND cast(a.fromts AS sql_date) > '12/31/2015'
ORDER BY b.stocknumber, a.fromts



SELECT a.VehicleInventoryItemID, b.stocknumber, a.category, a.status,
  a.fromts, a.thruts,
  LEFT(category, position('R' IN category) - 1) AS department
FROM VehicleInventoryItemStatuses a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE (lower(a.category) LIKE '%dispatched' OR lower(a.status) LIKE '%inprocess')
  AND a.thruts IS not NULL 
  AND cast(a.fromts AS sql_date) > '12/31/2015'
  AND EXISTS (
    SELECT 1 
	FROM VehicleInventoryItemStatuses
	WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
	  AND lower(category) LIKE '%dispatched')
  AND EXISTS (
     SELECT 1 
	FROM VehicleInventoryItemStatuses
	WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
	  AND lower(status) LIKE '%inprocess')
ORDER BY b.stocknumber, a.fromts

subtract wip time? yeah lets DO it IN minutes

SELECT stocknumber, department, SUM(disp_minutes) AS disp_minutes, 
  SUM(wip_minutes) AS wip_minutes
FROM (
  SELECT a.VehicleInventoryItemID, b.stocknumber, a.category, a.status,
    coalesce(CASE WHEN lower(a.category) LIKE '%dispatched' THEN 
      timestampdiff(sql_tsi_minute, a.fromts, a.thruts) END, 0) AS disp_minutes,
    coalesce(CASE WHEN lower(a.category) LIKE '%process' THEN 
      timestampdiff(sql_tsi_minute, a.fromts, a.thruts) END, 0) AS wip_minutes, 
    LEFT(category, position('R' IN category) - 1) AS department
  FROM VehicleInventoryItemStatuses a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND LEFT(b.stocknumber, 1) <> 'H'
  WHERE (lower(a.category) LIKE '%dispatched' OR lower(a.status) LIKE '%inprocess')
    AND a.thruts IS not NULL 
    AND cast(a.fromts AS sql_date) > '12/31/2015'
    AND EXISTS (
      SELECT 1 
  	FROM VehicleInventoryItemStatuses
  	WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
  	  AND lower(category) LIKE '%dispatched')
    AND EXISTS (
       SELECT 1 
  	FROM VehicleInventoryItemStatuses
  	WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
  	  AND lower(status) LIKE '%inprocess')) x
WHERE stocknumber LIKE '19888%'	  
--WHERE disp_minutes > 0	  

GROUP BY stocknumber, department	  
ORDER BY stocknumber, department

CLOSE, but there are wip instances unrelated to dispatch events
how to specifiy only those wip instances relevant to the dispatch events

for example: wip events for body AND appearance but no dispatch
SELECT a.VehicleInventoryItemID, b.stocknumber, a.category, a.status,
  a.fromts, a.thruts,
  LEFT(category, position('R' IN category) - 1) AS department
FROM VehicleInventoryItemStatuses a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE (lower(a.category) LIKE '%dispatched' OR lower(a.status) LIKE '%inprocess')
  AND a.thruts IS not NULL 
  AND cast(a.fromts AS sql_date) > '12/31/2015'
  AND EXISTS (
    SELECT 1 
	FROM VehicleInventoryItemStatuses
	WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
	  AND lower(category) LIKE '%dispatched')
  AND EXISTS (
     SELECT 1 
	FROM VehicleInventoryItemStatuses
	WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
	  AND lower(status) LIKE '%inprocess')
AND stocknumber = '19888xxz'	  

-- so, first just get the dispatch
SELECT a.VehicleInventoryItemID, b.stocknumber, 
  LEFT(category, position('R' IN category) - 1) AS department,
  a.fromts AS disp_from, a.thruts AS disp_thru,
  timestampdiff(sql_tsi_minute, a.fromts, a.thruts) AS disp_minutes
FROM VehicleInventoryItemStatuses a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE (lower(a.category) LIKE '%dispatched' OR lower(a.status) LIKE '%inprocess')
  AND a.thruts IS not NULL 
  AND cast(a.fromts AS sql_date) > '12/31/2015'
  AND EXISTS (
    SELECT 1 
	FROM VehicleInventoryItemStatuses
	WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
	  AND lower(category) LIKE '%dispatched')
	  
-- get the relevant wip events	
SELECT department, sum(disp_minutes - wip_minutes), COUNT(*),
  sum(disp_minutes - wip_minutes)/COUNT(*)/60 AS avg_buffer_hours
FROM ( 
  SELECT e.*, f.fromts AS wip_from, f.thruts AS wip_thru, 
    coalesce(timestampdiff(sql_tsi_minute, f.fromts, f.thruts), 0) AS wip_minutes
  FROM (
    SELECT a.VehicleInventoryItemID, b.stocknumber, 
      LEFT(category, position('R' IN category) - 1) AS department,
      a.fromts AS disp_from, a.thruts AS disp_thru,
      timestampdiff(sql_tsi_minute, a.fromts, a.thruts) AS disp_minutes
    FROM VehicleInventoryItemStatuses a
    INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    WHERE (lower(a.category) LIKE '%dispatched' OR lower(a.status) LIKE '%inprocess')
      AND a.thruts IS not NULL 
      AND cast(a.fromts AS sql_date) > '12/31/2014'
      AND EXISTS (
        SELECT 1 
    	FROM VehicleInventoryItemStatuses
    	WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
    	  AND lower(category) LIKE '%dispatched')) e
  LEFT JOIN VehicleInventoryItemStatuses f on e.VehicleInventoryItemID = f.VehicleInventoryItemID 
    AND lower(f.status) LIKE '%inprocess'
    AND f.thruts = e.disp_thru 
    AND LEFT(f.category, position('R' IN f.category) - 1) = e.department) g
WHERE disp_minutes - wip_minutes >= 0
GROUP BY department