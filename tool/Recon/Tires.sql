-- VehicleReconItems of Tires

SELECT *
FROM VehicleReconItems 
WHERE typ = 'MechanicalReconItem_Tires'

-- that are authorized AND NOT started
SELECT *
-- SELECT COUNT(*)
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
WHERE vrix.typ = 'MechanicalReconItem_Tires'
AND arix.Status = 'AuthorizedReconItem_NotStarted'

-- AND are part of a current authorization
SELECT *
-- SELECT COUNT(*)
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
WHERE vrix.typ = 'MechanicalReconItem_Tires'
AND arix.Status = 'AuthorizedReconItem_NotStarted'
AND EXISTS(
  SELECT 1
  FROM ReconAuthorizations
  WHERE ReconAuthorizationID = arix.ReconAuthorizationID 
  AND ThruTS IS NULL)




-- Tires NOT authorized
SELECT viix.stocknumber, fromts, vrix.description,
  left(utilities.CurrentViiSalesStatus(vrix.VehicleInventoryItemID),25) AS SalesStatus,
  srp.typ
-- SELECT COUNT(*) 
FROM VehicleReconItems vrix
INNER JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID 
LEFT JOIN (
  SELECT typ, VehicleInventoryItemID 
  FROM SelectedReconPAckages s
  WHERE VehicleInventoryItemID IS NOT NULL
  AND SelectedReconPAckageTS = (
    SELECT MAX(SelectedReconPackageTS)
    FROM SelectedReconPackages
    WHERE VehicleInventoryItemID = s.VehicleInventoryItemID 
    GROUP BY VehicleInventoryItemID)) srp ON vrix.VehicleInventoryItemID = srp.VehicleInventoryItemID 
WHERE vrix.typ = 'MechanicalReconItem_Tires'
AND NOT EXISTS (
  SELECT 1
  FROM AuthorizedReconItems
  WHERE VehicleReconItemID = vrix.VehicleReconItemID)
AND viix.fromts > '12/31/2009 00:00:00' 
AND viix.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'rydells') 
ORDER BY srp.typ


-- Tires Note Authorized BY Package
SELECT typ, COUNT(*)
FROM (
  SELECT viix.stocknumber, fromts, vrix.description,
    left(utilities.CurrentViiSalesStatus(vrix.VehicleInventoryItemID),25) AS SalesStatus,
    srp.typ
  -- SELECT COUNT(*) 
  FROM VehicleReconItems vrix
  INNER JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT typ, VehicleInventoryItemID 
    FROM SelectedReconPAckages s
    WHERE VehicleInventoryItemID IS NOT NULL
    AND SelectedReconPAckageTS = (
      SELECT MAX(SelectedReconPackageTS)
      FROM SelectedReconPackages
      WHERE VehicleInventoryItemID = s.VehicleInventoryItemID 
      GROUP BY VehicleInventoryItemID)) srp ON vrix.VehicleInventoryItemID = srp.VehicleInventoryItemID 
  WHERE vrix.typ = 'MechanicalReconItem_Tires'
  AND NOT EXISTS (
    SELECT 1
    FROM AuthorizedReconItems
    WHERE VehicleReconItemID = vrix.VehicleReconItemID)
  AND viix.fromts > '12/31/2009 00:00:00' 
  AND viix.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'rydells'))wtf
GROUP BY typ   