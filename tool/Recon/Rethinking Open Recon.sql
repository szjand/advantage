/*
SELECT DISTINCT
  vii.stocknumber,
  viis.Category,
  viis.Status,
  CASE 
    WHEN viis.category = 'BodyReconProcess' and viis.ThruTS is null THEN
      CASE
        WHEN viis.status = 'BodyReconProcess_InProcess' and viis.ThruTS is null THEN 'WIP'
        WHEN status = 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN status = 'BodyReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM VehicleInventoryItemStatuses
              WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
              AND category = 'BodyReconDispatched'
              AND ThruTS IS NULL) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
          END		
      END
  END AS BodyStatus,
  CASE 
    WHEN viis.category = 'MechanicalReconProcess' and viis.ThruTS is null THEN
      CASE
        WHEN viis.status = 'MechanicalReconProcess_InProcess' and viis.ThruTS is null THEN 'WIP'
        WHEN status = 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN status = 'MechanicalReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM VehicleInventoryItemStatuses
              WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
              AND category = 'MechanicalReconDispatched'
              AND ThruTS IS NULL) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
          END		
      END
  END AS MechanicalStatus,  
  CASE 
    WHEN viis.category = 'AppearanceReconProcess' and viis.ThruTS is null THEN
      CASE
        WHEN viis.status = 'AppearanceReconProcess_InProcess' and viis.ThruTS is null THEN 'WIP'
        WHEN status = 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN status = 'AppearanceReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM VehicleInventoryItemStatuses
              WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
              AND category = 'AppearanceReconDispatched'
              AND ThruTS IS NULL) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
          END		
      END
  END AS AppearanceStatus             
          
FROM VehicleInventoryItems vii
INNER JOIN VehicleInventoryItemStatuses viis ON viis.VehicleInventoryItemID = vii.VehicleInventoryItemID 
  AND viis.ThruTS IS NULL  
INNER JOIN VehicleItems vi on vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc on mmc.Make = vi.Make
  AND mmc.Model = vi.Model
  AND mmc.ThruTS IS NULL
INNER JOIN (
  SELECT ra.VehicleInventoryItemID
  FROM ReconAuthorizations ra
  WHERE EXISTS (
    SELECT 1
    FROM AuthorizedReconItems 
    WHERE ReconAuthorizationID = ra.ReconAuthorizationID
    AND status <> 'AuthorizedReconItem_Complete')) r ON r.VehicleInventoryItemID = vii.VehicleInventoryItemID 
LEFT JOIN (
  SELECT vrix.VehicleInventoryItemID, 
    CASE
	  WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = 0 THEN 'Kitted'
	  ELSE
 	    CASE 
    	  WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 'Kitted'
    	  ELSE
    	    CASE 
    		  WHEN SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NOT NULL THEN 1 ELSE 0 END) = 0 THEN 'Not Ordered'
    		  ELSE 'Ordered'
            END 
		END 
	END AS PartsStatus
  FROM VehicleReconItems vrix
  LEFT JOIN PartsOrders po ON po.VehicleReconItemID = vrix.VehicleReconItemID 
  WHERE vrix.typ LIKE 'Me%' 
  AND vrix.typ <> 'MechanicalReconItem_Inspection'
  GROUP BY vrix.VehicleInventoryItemID) ps ON vii.VehicleInventoryItemID = ps.VehicleInventoryItemID 	
WHERE vii.ThruTS IS NULL

ORDER  BY BodyStatus
*/


   SELECT DISTINCT 
  vii.stocknumber, 
--  viis.Category,
--  viis.Status,

  CASE (
      SELECT status
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND category = 'MechanicalReconProcess'
      AND ThruTS IS NULL)
    WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
    WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'MechanicalReconProcess_NotStarted' THEN 
      CASE 
        WHEN (
          SELECT COUNT(VehicleInventoryItemID)
          FROM VehicleInventoryItemStatuses
          WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
          AND category = 'MechanicalReconDispatched'
          AND ThruTS IS NULL) = 0 THEN 'Not Started'
        ELSE
          'Dispatched'
      END
  END AS MechanicalStatus,
  CASE
    WHEN viis.Category LIKE 'M%' THEN
	  CASE
	    WHEN viis.status = 'MechanicalReconProcess_InProcess' THEN 'WIP'
		WHEN viis.status = 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
	  END
  END AS ms2
/*  
  CASE
    WHEN viis.category = 'MechanicalReconProcess' AND viis.ThruTS IS NULL THEN 
	  CASE 
	    WHEN viis.status = 'MechanicalReconProcess_InProcess' THEN 'WIP'
		WHEN viis.status = 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
		WHEN viis.status = 'MechanicalReconProcess_NotStarted' THEN 
		  CASE
		    WHEN(
              SELECT COUNT(VehicleInventoryItemID)
              FROM VehicleInventoryItemStatuses
              WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
              AND category = 'MechanicalReconDispatched'
              AND ThruTS IS NULL) = 0 THEN 'Not Started'			
			ELSE 'Dispatched'
		  END 
      END 
*/				
-- the question IS how to distinguish dispatched FROM NOT started		
--		  CASE 
--		    WHEN category = 'MechanicalReconDispatched' AND viis.ThruTS IS NULL THEN 'Dispatched'
--			ELSE 'Not Started'
--		  END
--      END  
--    ELSE  'No OPEN Items'
--  END AS MS2

  
      
FROM VehicleInventoryItems vii
INNER JOIN VehicleInventoryItemStatuses viis ON viis.VehicleInventoryItemID = vii.VehicleInventoryItemID 
  AND viis.ThruTS IS NULL  
INNER JOIN VehicleItems vi on vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc on mmc.Make = vi.Make
  AND mmc.Model = vi.Model
  AND mmc.ThruTS IS NULL
INNER JOIN (
  SELECT ra.VehicleInventoryItemID
  FROM ReconAuthorizations ra
  WHERE EXISTS (
    SELECT 1
    FROM AuthorizedReconItems 
    WHERE ReconAuthorizationID = ra.ReconAuthorizationID
    AND status <> 'AuthorizedReconItem_Complete')) r ON r.VehicleInventoryItemID = vii.VehicleInventoryItemID 
WHERE viis.category like 'Me%'  
AND viis.status <> 'MechanicalReconProcess_NoIncompleteReconItems'
AND vii.ThruTS IS NULL;

/*
SELECT DISTINCT status
FROM VehicleInventoryItemStatuses
WHERE category like 'M%'

SELECT * FROM VehicleInventoryItems WHERE stocknumber = '11012b'
SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = 'f52fcbe9-21ae-4868-b854-576fd18fbc84' AND category LIKE 'M%' AND ThruTS IS null
*/

SELECT DISTINCT vii.stocknumber,

  ms.ms2
FROM VehicleInventoryItems vii
INNER JOIN VehicleInventoryItemStatuses viis ON viis.VehicleInventoryItemID = vii.VehicleInventoryItemID 
  AND viis.ThruTS IS NULL  
INNER JOIN VehicleItems vi on vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc on mmc.Make = vi.Make
  AND mmc.Model = vi.Model
  AND mmc.ThruTS IS NULL
INNER JOIN (
  SELECT ra.VehicleInventoryItemID
  FROM ReconAuthorizations ra
  WHERE EXISTS (
    SELECT 1
    FROM AuthorizedReconItems 
    WHERE ReconAuthorizationID = ra.ReconAuthorizationID
    AND status <> 'AuthorizedReconItem_Complete')) r ON r.VehicleInventoryItemID = vii.VehicleInventoryItemID 
INNER JOIN (
  SELECT viisx.VehicleInventoryItemID, 
    CASE
      WHEN SUM(CASE WHEN viisx.status = 'MechanicalReconProcess_InProcess' THEN 1 ELSE 0 END) <> 0 THEN 'WIP'
      ELSE 
  	    CASE
  	      WHEN SUM(CASE WHEN viisx.status = 'MechanicalReconProcess_NoIncompleteReconItems' THEN 1 ELSE 0 END) <> 0 THEN 'No Open Items'
  		  ELSE 
  		    CASE 
  		      WHEN SUM(CASE WHEN viisx.status = 'MechanicalReconDispatched_Dispatched' THEN 1 ELSE 0 END) <> 0 THEN 'Dispatched'
  			  ELSE 'Not Started'
  		    END 
  	    END 
    END AS MS2
  FROM VehicleInventoryItemStatuses viisx
  INNER JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND viix.ThruTS IS NULL 
  WHERE viisx.category LIKE 'M%'
  AND viisx.ThruTS IS NULL
  GROUP BY viisx.VehicleInventoryItemID ) ms ON vii.VehicleInventoryItemID = ms.VehicleInventoryItemID 	
-- WHERE viis.category like 'Me%'  
-- AND viis.status <> 'MechanicalReconProcess_NoIncompleteReconItems'
WHERE vii.ThruTS IS NULL

  
  
  
  
  
  
  
  
SELECT viisx.VehicleInventoryItemID, 
  CASE
    WHEN SUM(CASE WHEN viisx.status = 'MechanicalReconProcess_InProcess' THEN 1 ELSE 0 END) <> 0 THEN 'WIP'
    ELSE 
	  CASE
	    WHEN SUM(CASE WHEN viisx.status = 'MechanicalReconProcess_NoIncompleteReconItems' THEN 1 ELSE 0 END) <> 0 THEN 'No Open Items'
		ELSE 
		  CASE 
		    WHEN SUM(CASE WHEN viisx.status = 'MechanicalReconDispatched_Dispatched' THEN 1 ELSE 0 END) <> 0 THEN 'Dispatched'
			ELSE 'Not Started'
		  END 
	  END 
  END AS MS
FROM VehicleInventoryItemStatuses viisx
INNER JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.ThruTS IS NULL 
WHERE viisx.category LIKE 'M%'
AND viisx.ThruTS IS NULL
GROUP BY viisx.VehicleInventoryItemID 
