-- 13166xxxa - tires installed should be a different tire status (D for done)
-- thinking going to need to DO an INNER JOIN ON AuthorizedReconItems 
-- but to get just the right AuthorizedReconItems, ReconAuthorizations will also need to be a JOIN, NOT an EXISTS 
SELECT *
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = '62722d46-1e67-4c56-99dd-a893045e63ad'

  SELECT 
  CASE 
    WHEN vr.VehicleInventoryItemID  IS NOT NULL AND po.ReceivedTS IS NOT NULL then 'R'
	WHEN vr.VehicleInventoryItemID  IS NOT NULL AND po.OrderedTS IS NOT NULL THEN 'O'
    WHEN vr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END AS Tires,  vr.VehicleInventoryItemID, vr.VehicleReconItemID, arix.*
  FROM VehicleReconItems vr
  INNER JOIN AuthorizedReconItems arix ON vr.VehicleReconItemID = arix.VehicleReconItemID 
  LEFT JOIN PartsOrders po ON vr.VehicleReconItemID = po.VehicleReconItemID  
  WHERE ((vr.typ = 'MechanicalReconItem_Tires')
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%'))
  AND vr.partsinstock = true
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations raix
	INNER JOIN AuthorizedReconItems arix ON raix.ReconAuthorizationID = arix.ReconAuthorizationID
	  AND arix.Status <> 'AuthorizedReconItem_Complete' 
	WHERE raix.VehicleInventoryItemID = vr.VehicleInventoryItemID 
	AND ThruTS IS NULL)
  AND vr.VehicleInventoryItemID = '62722d46-1e67-4c56-99dd-a893045e63ad'

-- thinking going to need to DO an INNER JOIN ON AuthorizedReconItems 
-- but to get just the right AuthorizedReconItems, ReconAuthorizations will also need to be a JOIN, NOT an EXISTS   
--, ok but returns records for
  SELECT distinct
  left(cast(vr.description AS sql_char),25),
  CASE 
    WHEN vr.VehicleInventoryItemID IS NOT NULL  AND po.ReceivedTS IS NOT NULL AND arix.status = 'AuthorizedReconItem_Complete' THEN 'D'
    WHEN vr.VehicleInventoryItemID  IS NOT NULL AND po.ReceivedTS IS NOT NULL then 'R'
	WHEN vr.VehicleInventoryItemID  IS NOT NULL AND po.OrderedTS IS NOT NULL THEN 'O'
    WHEN vr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END AS Tires,  vr.VehicleInventoryItemID, vr.VehicleReconItemID, arix.*
  FROM VehicleReconItems vr
  INNER JOIN AuthorizedReconItems arix ON vr.VehicleReconItemID = arix.VehicleReconItemID 
  INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID
    AND rax.ThruTS IS NULL  
  LEFT JOIN PartsOrders po ON vr.VehicleReconItemID = po.VehicleReconItemID  
  WHERE ((vr.typ = 'MechanicalReconItem_Tires')
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%'))
  AND vr.partsinstock = true
  ORDER BY vr.VehicleInventoryItemID 

  AND vr.VehicleInventoryItemID = '62722d46-1e67-4c56-99dd-a893045e63ad'