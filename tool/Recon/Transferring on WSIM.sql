SELECT * FROM VehicleItems WHERE vin = '2HGFA16538H320308'

SELECT *
FROM AuthorizedReconItems a
WHERE ReconAuthorizationID IN (
  SELECT ReconAuthorizationID  
  FROM ReconAuthorizations r
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItems
    WHERE VehicleItemID = (
      SELECT VehicleItemID  
      FROM VehicleItems 
      WHERE vin = '2HGFA16538H320308')))
    
    
    
SELECT *  
FROM ReconAuthorizations r
LEFT JOIN AuthorizedReconItems a ON r.ReconAuthorizationID = a.ReconAuthorizationID 
LEFT JOIN VehicleReconItems v ON a.VehicleReconItemID = v.VehicleReconItemID 
WHERE r.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems
  WHERE VehicleItemID = (
    SELECT VehicleItemID  
    FROM VehicleItems 
    WHERE vin = '2A4GP54L56R716252'))    