raw materials with OPEN warranty mech WORK

SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '32fb103b-fefd-421d-a64c-3ddaae06d432'

SELECT v.stocknumber,
  (SELECT utilities.CurrentViiReconStatus(v.VehicleInventoryItemID) FROM system.iota) AS recon
-- SELECT COUNT(*)
FROM VehicleInventoryItems v
INNER JOIN organizations o ON v.owninglocationid = o.partyid
  AND o.name = 'rydells'
WHERE EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND category = 'RMFlagRMP'
    AND ThruTS IS NULL)    

-- raw materials with OPEN recon    
SELECT stocknumber, VehicleInventoryItemID 
INTO #jon
FROM (
SELECT v.stocknumber, v.VehicleInventoryItemID,
  (SELECT utilities.CurrentViiReconStatus(v.VehicleInventoryItemID) FROM system.iota) AS recon
-- SELECT COUNT(*)
FROM VehicleInventoryItems v
INNER JOIN organizations o ON v.owninglocationid = o.partyid
  AND o.name = 'rydells'
WHERE EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND category = 'RMFlagRMP'
    AND ThruTS IS NULL)) x
WHERE recon LIKE '%Mech:</B> Not Started%'       

SELECT COUNT(*) FROM #jon

what IS the authorized recon for these vehicles



SELECT j.stocknumber, left(v.description, 35) AS description, v.underwarranty
FROM #jon j
LEFT JOIN VehicleReconItems v ON j.VehicleInventoryItemID = v.VehicleInventoryItemID 
  AND v.typ LIKE 'MechanicalReconItem%'
  AND v.typ <> 'MechanicalReconItem_Inspection'
WHERE EXISTS (
  SELECT 1
  FROM AuthorizedReconItems
  WHERE VehicleReconItemID = v.VehicleReconItemID 
  AND status <> 'AuthorizedReconItem_Complete') 
ORDER BY stocknumber  
 
SELECT * FROM VehicleReconItems 
SELECT * FROM AuthorizedReconItems 
SELECT * FROM ReconAuthorizations 
SELECT * FROM keyperkeystatus

-- fuck, ON 1/18 did a spreadsheet for bev, can't find the sql, start over

SELECT d.stocknumber, 
  left(trim(e.yearmodel) + ' ' + trim(e.make) + ' ' + trim(e.model), 35) AS Vehicle, 
  left(cast(a.description AS sql_char), 100) AS [Warranty Work], keystatus,
  cast(Utilities.CurrentViiLocationWithoutStore(a.VehicleInventoryItemID) AS sql_char) AS PhysLocation
FROM VehicleReconItems a
INNER JOIN AuthorizedReconItems b ON a.VehicleReconItemID = b.VehicleReconItemID 
  AND b.status = 'AuthorizedReconItem_NotStarted'
INNER JOIN ReconAuthorizations c ON b.ReconAuthorizationID = c.ReconAuthorizationID 
  AND c.thruts IS NULL 
INNER JOIN VehicleInventoryItems d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID   
INNER JOIN VehicleItems e ON d.VehicleItemID = e.VehicleItemID 
LEFT JOIN keyperkeystatus f ON d.stocknumber = f.stocknumber
WHERE a.typ <> 'MechanicalReconItem_Inspection'
  AND a.typ LIKE 'Mechanical%' 
  AND a.UnderWarranty = true
  AND EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
      AND category = 'RMFlagRMP'
      AND ThruTS IS NULL)
