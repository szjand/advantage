DECLARE @VehicleInventoryItemID string;
@VehicleInventoryItemID = (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems 
  WHERE stocknumber = '11964a');

SELECT 
  'Mech: ' + TRIM(MechStatus)+ '  Body: ' + TRIM(BodyStatus) + '  Appearance: ' + TRIM(AppearanceStatus)
  FROM (  
    SELECT 
      CASE (
          SELECT status
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND category = 'MechanicalReconProcess'
          AND ThruTS IS NULL)
        WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
        WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN 'MechanicalReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM VehicleInventoryItemStatuses
              WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
              AND category = 'MechanicalReconDispatched'
              AND ThruTS IS NULL) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
            END
      END AS MechStatus,
      CASE (
          SELECT status
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND category = 'BodyReconProcess'
          AND ThruTS IS NULL)
        WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
        WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN 'BodyReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM VehicleInventoryItemStatuses
              WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
              AND category = 'BodyReconDispatched'
              AND ThruTS IS NULL) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
            END
      END AS BodyStatus,
      CASE (
          SELECT status
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND category = 'AppearanceReconProcess'
          AND ThruTS IS NULL)
        WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
        WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
        WHEN 'AppearanceReconProcess_NotStarted' THEN 
          CASE 
            WHEN (
              SELECT COUNT(VehicleInventoryItemID)
              FROM VehicleInventoryItemStatuses
              WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
              AND category = 'AppearanceReconDispatched'
              AND ThruTS IS NULL) = 0 THEN 'Not Started'
            ELSE
              'Dispatched'
            END
      END AS AppearanceStatus    
    FROM VehicleInventoryItems viix
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID) wtf;