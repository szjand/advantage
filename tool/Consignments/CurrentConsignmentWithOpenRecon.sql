SELECT stocknumber, viix.fromts, viix.thruts, vic.fromts, vic.thruts
FROM VehicleInventoryItems viix
INNER JOIN VehicleInventoryConsignments vic 
  ON vic.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND vic.ThruTS IS NULL 
INNER JOIN (
  SELECT VehicleInventoryItemID 
  FROM ReconAuthorizations rax
  WHERE ThruTS IS NULL 
  AND EXISTS (
    SELECT 1 
    FROM AuthorizedReconItems 
    WHERE ReconAuthorizationID = rax.ReconAuthorizationID
    AND status <> 'AuthorizedReconItem_Complete')) wtf ON wtf.VehicleInventoryItemID = viix.VehicleInventoryItemID 
    