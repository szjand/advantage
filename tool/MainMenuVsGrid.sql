/*
AVAILABLE

*/
-- Grid: vUsedCarsAvailable 
select count(*) -- 131
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc on mmc.Make = vi.Make
  AND mmc.Model = vi.Model
  AND mmc.ThruTS IS NULL
WHERE viis.Status = 'RMFlagAV_Available'
AND viis.ThruTS IS NULL;

-- MainMenu: sp.GetUsedCarsMainMenuSummaryByMarket
SELECT COUNT(status)
FROM VehicleInventoryItemStatuses viis
WHERE status = 'RMFlagAV_Available'
AND ThruTS IS NULL
AND EXISTS (
  SELECT 1 
  FROM VehicleInventoryItems
  WHERE ThruTS IS NULL
  AND VehicleInventoryItemID = viis.VehicleInventoryItemID
  AND OwningLocationID IN (
    SELECT pr.PartyID2
    FROM PartyRelationships pr
    WHERE pr.Typ = 'PartyRelationship_MarketLocations'
    AND pr.PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82'
    AND pr.ThruTS IS NULL))
    
/*
Inspection Pending

*/
-- Grid: vUsedCarsAvailable 
select count(*) -- 45
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc on mmc.Make = vi.Make
  AND mmc.Model = vi.Model
  AND mmc.ThruTS IS NULL
WHERE viis.Status = 'RMFlagIP_InspectionPending'
AND viis.ThruTS IS NULL;

-- MainMenu: sp.GetUsedCarsMainMenuSummaryByMarket
SELECT COUNT(status)
FROM VehicleInventoryItemStatuses viis
WHERE status = 'RMFlagIP_InspectionPending'
AND ThruTS IS NULL
AND EXISTS (
  SELECT 1 
  FROM VehicleInventoryItems
  WHERE ThruTS IS NULL
  AND VehicleInventoryItemID = viis.VehicleInventoryItemID
  AND OwningLocationID IN (
    SELECT pr.PartyID2
    FROM PartyRelationships pr
    WHERE pr.Typ = 'PartyRelationship_MarketLocations'
    AND pr.PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82'
    AND pr.ThruTS IS NULL))   
     
-- usually the issue IS non classified vehicles    
SELECT i.stocknumber, b.make, b.model
FROM VehicleInventoryItems i
INNER JOIN VehicleItems b on i.VehicleItemID = b.VehicleItemID 
WHERE ThruTs IS NULL
AND NOT EXISTS (
  SELECT 1
  FROM MakeModelClassifications
  WHERE model = (
    SELECT model
    FROM VehicleItems
    WHERE VehicleItemID = i.VehicleItemID))    
    
SELECT *
FROM MakeModelClassifications     
WHERE make = 'toyota'
ORDER BY model

select * FROM MakeModelClassifications 
SELECT DISTINCT vehiclesegment, vehicletype FROM MakeModelClassifications 
SELECT * FROM MakeModelClassifications WHERE vehicletype = 'VehicleType_Crossover'
-- Black book now separates RAM out FROM dodge AS a make
INSERT INTO MakeModelClassifications 
values('Ram', '1500', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
INSERT INTO MakeModelClassifications 
values('Ram', '3500', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
-- chevy sonic
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Sonic', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
-- 1952 chevy deluxe
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Deluxe', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
-- Volks CC
INSERT INTO MakeModelClassifications 
values('Volkswagen', 'CC', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Import')
-- buick encore
INSERT INTO MakeModelClassifications 
values('Buick', 'Encore', 'VehicleSegment_Compact','VehicleType_Crossover',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- toyota Prius V
INSERT INTO MakeModelClassifications 
values('Toyota', 'Prius V', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Subaru BRZ
INSERT INTO MakeModelClassifications 
values('Subaru', 'BRZ', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Mazda CX-5
INSERT INTO MakeModelClassifications 
values('Mazda', 'CX-5', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Kia Borrego
INSERT INTO MakeModelClassifications 
values('Kia', 'Borrego', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')


SELECT * FROM MakeModelClassifications WHERE model LIKE 'equino%'

SELECT *
FROM VehicleItems
WHERE vin = '1C6RD7LT3CS195506'

SELECT *
FROM blackbookresolver
WHERE vin = '1C6RD7LT'

/* 6/14/12 no pictures */
-- fb 115
-- main menu shows 2 veh w/no pics, page shows 1
-- FROM GetUsedCarsMainMenuSummaryByLocation (crookston)
-- feeds main menu
--  SELECT COUNT(*)
    SELECT viix.stocknumber
    FROM VehicleInventoryItems viix
    LEFT JOIN (
      SELECT count(viip.VehicleInventoryItemID) AS CountOfPic, viip.VehicleInventoryItemID
      FROM viipictures viip
      GROUP BY viip.VehicleInventoryItemID) t ON viix.VehicleInventoryItemID = t.VehicleInventoryItemID  
    LEFT JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID 
    WHERE viix.ThruTS IS NULL 
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
      AND ThruTS IS NULL
      AND status IN (
        'RMFlagWSB_WholesaleBuffer',
    	'RMFlagPIT_PurchaseInTransit',
    	'RMFlagTNA_TradeNotAvailable',
    	'RMFlagIP_InspectionPending',
    	'RMFlagWP_WalkPending'))
    AND coalesce(t.CountOfPic, 0) = 0
    AND vs.VehicleInventoryItemID IS NULL
    AND LocationID = '1B6768C6-03B0-4195-BA3B-767C0CAC40A6'

-- page	(MissingPicturesReport.aspx)
UsedCarsDM.GetVehiclePictureCounts(UserSession.MarketID, LocationDropDownList.SelectedValue, Convert.toInt32(LessThanRadioButtonList.SelectedValue));
EXECUTE PROCEDURE GetVehiclePictureCounts('A4847DFC-E00E-42E7-89EE-CD4368445A82','1B6768C6-03B0-4195-BA3B-767C0CAC40A6',1);

-- shit, the issue IS a consigned vehicle
16324XXA IS consigned to crookston, has no pictures

-- changed GetVehiclePictureCounts to use locationid
...
WHERE vii.ThruTS IS NULL
AND 
  CASE 
--     WHEN @LocationID <> '' THEN vii.OwningLocationID = @LocationID
 WHEN @LocationID <> '' THEN vii.LocationID = @LocationID
    ELSE vii.OwningLocationID IN (
      SELECT PartyID2
      FROM PartyRelationships
      WHERE PartyID1 = @MarketID
      AND typ = 'PartyRelationship_MarketLocations'
      AND ThruTS IS NULL)
  END
... 

    


