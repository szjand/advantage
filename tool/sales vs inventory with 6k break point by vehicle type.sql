
SELECT VehicleType, COUNT(*)
FROM MakeModelClassifications 
GROUP BY VehicleType

SELECT * FROM VehicleSales
SELECT DISTINCT status FROM vehiclesales


-- a TABLE of sales with sold amount, AND vehicletype

-- last 30 days sales
SELECT CAST(vs.SoldTS AS sql_date) AS dateSold, vs.SoldAmount, 
  CASE 
    WHEN vs.SoldAmount < 6000 THEN 'Under 6K'
    ELSE 'Over 6K'
  END AS PriceBand,
  m.VehicleType, 
  i.stocknumber, o.name,
  CASE vs.typ
    WHEN 'VehicleSale_Retail' THEN 'Retail'
    ELSE 'Wholesale'
  END AS saleType
FROM VehicleSales vs
INNER JOIN VehicleInventoryItems i ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID 
INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
INNER JOIN (
  SELECT make, model,
    CASE VehicleType
      WHEN 'VehicleType_Car' THEN 'Car'
      WHEN 'VehicleType_Pickup' THEN 'Truck'
      WHEN 'VehicleType_SUV' THEN 'SUV'
      ELSE 'Other'
    END AS VehicleType
  FROM MakeModelClassifications) m ON v.make = m.make
    AND v.model = m.model
LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID
WHERE vs.status = 'VehicleSale_Sold'
  AND CAST(vs.SoldTS AS sql_date) BETWEEN curdate()-30 AND curdate()      
  
SELECT VehicleType, PriceBand, saleType, left(name, 12), COUNT(*) AS "Last 30 Days Sales"
FROM ( 
  SELECT CAST(vs.SoldTS AS sql_date) AS dateSold, vs.SoldAmount, 
    CASE 
      WHEN vs.SoldAmount < 6000 THEN 'Under 6K'
      ELSE 'Over 6K'
    END AS PriceBand,
    m.VehicleType, 
    i.stocknumber, o.name,
    CASE vs.typ
      WHEN 'VehicleSale_Retail' THEN 'Retail'
      ELSE 'Wholesale'
    END AS saleType
  FROM VehicleSales vs
  INNER JOIN VehicleInventoryItems i ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID 
  INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
  INNER JOIN (
    SELECT make, model,
      CASE VehicleType
        WHEN 'VehicleType_Car' THEN 'Car'
        WHEN 'VehicleType_Pickup' THEN 'Truck'
        WHEN 'VehicleType_SUV' THEN 'SUV'
        ELSE 'Other'
      END AS VehicleType
    FROM MakeModelClassifications) m ON v.make = m.make
      AND v.model = m.model
  LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID
  WHERE vs.status = 'VehicleSale_Sold'
    AND CAST(vs.SoldTS AS sql_date) BETWEEN curdate()-30 AND curdate()) x 
WHERE x.name = 'Rydells'
  AND x.saleType = 'Retail'    
GROUP BY VehicleType, PriceBand, saleType, name  
ORDER BY COUNT(*)  


-- current inventory
-- current price
SELECT * FROM VehiclePricingDetails 

SELECT m.VehicleType, pd.Amount,
  CASE
    WHEN pd.Amount < 6000 THEN 'Under 6K'
    ELSE 'Over 6K'
  END AS Price,
  o.name
-- SELECT i.VehicleInventoryItemID 
FROM VehicleInventoryItems i
INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
INNER JOIN (
  SELECT make, model,
    CASE VehicleType
      WHEN 'VehicleType_Car' THEN 'Car'
      WHEN 'VehicleType_Pickup' THEN 'Truck'
      WHEN 'VehicleType_SUV' THEN 'SUV'
      ELSE 'Other'
    END AS VehicleType
  FROM MakeModelClassifications) m ON v.make = m.make
    AND v.model = m.model
INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings 
    WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
    GROUP BY VehicleInventoryItemID) 
INNER JOIN VehiclePricingDetails pd ON p.VehiclePricingID = pd.VehiclePricingID 
  AND pd.Typ = 'VehiclePricingDetail_BestPrice'         
LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID    
WHERE i.ThruTS IS NULL 
-- GROUP BY i.VehicleInventoryItemID HAVING COUNT(*) > 1


SELECT VehicleType, Price, name, COUNT(*) AS "Current Inventory" 
FROM (
  SELECT m.VehicleType, pd.Amount,
    CASE
      WHEN pd.Amount < 6000 THEN 'Under 6K'
      ELSE 'Over 6K'
    END AS Price,
    o.name
  FROM VehicleInventoryItems i
  INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
  INNER JOIN (
    SELECT make, model,
      CASE VehicleType
        WHEN 'VehicleType_Car' THEN 'Car'
        WHEN 'VehicleType_Pickup' THEN 'Truck'
        WHEN 'VehicleType_SUV' THEN 'SUV'
        ELSE 'Other'
      END AS VehicleType
    FROM MakeModelClassifications) m ON v.make = m.make
      AND v.model = m.model
  INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
    AND VehiclePricingTS = (
      SELECT MAX(VehiclePricingTS)
      FROM VehiclePricings 
      WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
      GROUP BY VehicleInventoryItemID) 
  INNER JOIN VehiclePricingDetails pd ON p.VehiclePricingID = pd.VehiclePricingID 
    AND pd.Typ = 'VehiclePricingDetail_BestPrice'         
  LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID    
  WHERE i.ThruTS IS NULL) y
WHERE name = 'rydells'  
GROUP BY VehicleType, Price, name
ORDER BY COUNT(*)



SELECT w.VehicleType, w.PriceBand,  w."Last 30 Days Sales", y."Current Inventory" 
FROM (
  SELECT VehicleType, PriceBand, saleType, name, COUNT(*) AS "Last 30 Days Sales"
  FROM ( 
    SELECT CAST(vs.SoldTS AS sql_date) AS dateSold, vs.SoldAmount, 
      CASE 
        WHEN vs.SoldAmount < 6000 THEN 'Under 6K'
        ELSE 'Over 6K'
      END AS PriceBand,
      m.VehicleType, 
      i.stocknumber, o.name,
      CASE vs.typ
        WHEN 'VehicleSale_Retail' THEN 'Retail'
        ELSE 'Wholesale'
      END AS saleType
    FROM VehicleSales vs
    INNER JOIN VehicleInventoryItems i ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID 
    INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
    INNER JOIN (
      SELECT make, model,
        CASE VehicleType
          WHEN 'VehicleType_Car' THEN 'Car'
          WHEN 'VehicleType_Pickup' THEN 'Truck'
          WHEN 'VehicleType_SUV' THEN 'SUV'
          ELSE 'Other'
        END AS VehicleType
      FROM MakeModelClassifications) m ON v.make = m.make
        AND v.model = m.model
    LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID
    WHERE vs.status = 'VehicleSale_Sold'
      AND CAST(vs.SoldTS AS sql_date) BETWEEN curdate()-30 AND curdate()) x 
  WHERE x.name = 'Rydells'
    AND x.saleType = 'Retail'    
  GROUP BY VehicleType, PriceBand, saleType, name) w
LEFT JOIN (
  SELECT VehicleType, Price, name, COUNT(*) AS "Current Inventory" 
  FROM (
    SELECT m.VehicleType, pd.Amount,
      CASE
        WHEN pd.Amount < 6000 THEN 'Under 6K'
        ELSE 'Over 6K'
      END AS Price,
      o.name
    FROM VehicleInventoryItems i
    INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
    INNER JOIN (
      SELECT make, model,
        CASE VehicleType
          WHEN 'VehicleType_Car' THEN 'Car'
          WHEN 'VehicleType_Pickup' THEN 'Truck'
          WHEN 'VehicleType_SUV' THEN 'SUV'
          ELSE 'Other'
        END AS VehicleType
      FROM MakeModelClassifications) m ON v.make = m.make
        AND v.model = m.model
    INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
      AND VehiclePricingTS = (
        SELECT MAX(VehiclePricingTS)
        FROM VehiclePricings 
        WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
        GROUP BY VehicleInventoryItemID) 
    INNER JOIN VehiclePricingDetails pd ON p.VehiclePricingID = pd.VehiclePricingID 
      AND pd.Typ = 'VehiclePricingDetail_BestPrice'         
    LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID    
    WHERE i.ThruTS IS NULL) y
  WHERE name = 'rydells'  
  GROUP BY VehicleType, Price, name) y 
ON w.VehicleType = y.VehicleType
  AND w.PriceBand = y.price
  AND w.name = y.name

  
-- ADD ON the lot (available)

SELECT m.VehicleType, pd.Amount,
  CASE
    WHEN pd.Amount < 6000 THEN 'Under 6K'
    ELSE 'Over 6K'
  END AS Price,
  o.name
-- SELECT i.
-- SELECT i.VehicleInventoryItemID 
FROM VehicleInventoryItems i
INNER JOIN VehicleInventoryItemStatuses s ON i.VehicleInventoryItemID = s.VehicleInventoryItemID
  AND s.category = 'RMFlagAV'
  AND s.ThruTS IS NULL 
INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
INNER JOIN (
  SELECT make, model,
    CASE VehicleType
      WHEN 'VehicleType_Car' THEN 'Car'
      WHEN 'VehicleType_Pickup' THEN 'Truck'
      WHEN 'VehicleType_SUV' THEN 'SUV'
      ELSE 'Other'
    END AS VehicleType
  FROM MakeModelClassifications) m ON v.make = m.make
    AND v.model = m.model
INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings 
    WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
    GROUP BY VehicleInventoryItemID) 
INNER JOIN VehiclePricingDetails pd ON p.VehiclePricingID = pd.VehiclePricingID 
  AND pd.Typ = 'VehiclePricingDetail_BestPrice'         
LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID   
-- GROUP BY i.VehicleInventoryItemID HAVING COUNT(*) > 1 


SELECT VehicleType, Price, name, COUNT(*) AS Available 
FROM (
  SELECT m.VehicleType, pd.Amount,
    CASE
      WHEN pd.Amount < 6000 THEN 'Under 6K'
      ELSE 'Over 6K'
    END AS Price,
    o.name
  FROM VehicleInventoryItems i
  INNER JOIN VehicleInventoryItemStatuses s ON i.VehicleInventoryItemID = s.VehicleInventoryItemID
    AND s.category = 'RMFlagAV'
    AND s.ThruTS IS NULL 
  INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
  INNER JOIN (
    SELECT make, model,
      CASE VehicleType
        WHEN 'VehicleType_Car' THEN 'Car'
        WHEN 'VehicleType_Pickup' THEN 'Truck'
        WHEN 'VehicleType_SUV' THEN 'SUV'
        ELSE 'Other'
      END AS VehicleType
    FROM MakeModelClassifications) m ON v.make = m.make
      AND v.model = m.model
  INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
    AND VehiclePricingTS = (
      SELECT MAX(VehiclePricingTS)
      FROM VehiclePricings 
      WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
      GROUP BY VehicleInventoryItemID) 
  INNER JOIN VehiclePricingDetails pd ON p.VehiclePricingID = pd.VehiclePricingID 
    AND pd.Typ = 'VehiclePricingDetail_BestPrice'         
  LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID) t
WHERE name = 'Rydells'
GROUP BY VehicleType, Price, name
     
     
SELECT w.VehicleType, w.PriceBand,  w."Last 30 Days Sales", y."Current Inventory", a.Available 
FROM (
  SELECT VehicleType, PriceBand, saleType, name, COUNT(*) AS "Last 30 Days Sales"
  FROM ( 
    SELECT CAST(vs.SoldTS AS sql_date) AS dateSold, vs.SoldAmount, 
      CASE 
        WHEN vs.SoldAmount < 6000 THEN 'Under 6K'
        ELSE 'Over 6K'
      END AS PriceBand,
      m.VehicleType, 
      i.stocknumber, o.name,
      CASE vs.typ
        WHEN 'VehicleSale_Retail' THEN 'Retail'
        ELSE 'Wholesale'
      END AS saleType
    FROM VehicleSales vs
    INNER JOIN VehicleInventoryItems i ON vs.VehicleInventoryItemID = i.VehicleInventoryItemID 
    INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
    INNER JOIN (
      SELECT make, model,
        CASE VehicleType
          WHEN 'VehicleType_Car' THEN 'Car'
          WHEN 'VehicleType_Pickup' THEN 'Truck'
          WHEN 'VehicleType_SUV' THEN 'SUV'
          ELSE 'Other'
        END AS VehicleType
      FROM MakeModelClassifications) m ON v.make = m.make
        AND v.model = m.model
    LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID
    WHERE vs.status = 'VehicleSale_Sold'
      AND CAST(vs.SoldTS AS sql_date) BETWEEN curdate()-30 AND curdate()) x 
  WHERE x.name = 'Rydells'
    AND x.saleType = 'Retail'    
  GROUP BY VehicleType, PriceBand, saleType, name) w
LEFT JOIN (
  SELECT VehicleType, Price, name, COUNT(*) AS "Current Inventory" 
  FROM (
    SELECT m.VehicleType, pd.Amount,
      CASE
        WHEN pd.Amount < 6000 THEN 'Under 6K'
        ELSE 'Over 6K'
      END AS Price,
      o.name
    FROM VehicleInventoryItems i
    INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
    INNER JOIN (
      SELECT make, model,
        CASE VehicleType
          WHEN 'VehicleType_Car' THEN 'Car'
          WHEN 'VehicleType_Pickup' THEN 'Truck'
          WHEN 'VehicleType_SUV' THEN 'SUV'
          ELSE 'Other'
        END AS VehicleType
      FROM MakeModelClassifications) m ON v.make = m.make
        AND v.model = m.model
    INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
      AND VehiclePricingTS = (
        SELECT MAX(VehiclePricingTS)
        FROM VehiclePricings 
        WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
        GROUP BY VehicleInventoryItemID) 
    INNER JOIN VehiclePricingDetails pd ON p.VehiclePricingID = pd.VehiclePricingID 
      AND pd.Typ = 'VehiclePricingDetail_BestPrice'         
    LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID    
    WHERE i.ThruTS IS NULL) y
  WHERE name = 'rydells'  
  GROUP BY VehicleType, Price, name) y 
ON w.VehicleType = y.VehicleType
  AND w.PriceBand = y.price
  AND w.name = y.name     
LEFT JOIN (
  SELECT VehicleType, Price, name, COUNT(*) AS Available 
  FROM (
    SELECT m.VehicleType, pd.Amount,
      CASE
        WHEN pd.Amount < 6000 THEN 'Under 6K'
        ELSE 'Over 6K'
      END AS Price,
      o.name
    FROM VehicleInventoryItems i
    INNER JOIN VehicleInventoryItemStatuses s ON i.VehicleInventoryItemID = s.VehicleInventoryItemID
      AND s.category = 'RMFlagAV'
      AND s.ThruTS IS NULL 
    INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
    INNER JOIN (
      SELECT make, model,
        CASE VehicleType
          WHEN 'VehicleType_Car' THEN 'Car'
          WHEN 'VehicleType_Pickup' THEN 'Truck'
          WHEN 'VehicleType_SUV' THEN 'SUV'
          ELSE 'Other'
        END AS VehicleType
      FROM MakeModelClassifications) m ON v.make = m.make
        AND v.model = m.model
    INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
      AND VehiclePricingTS = (
        SELECT MAX(VehiclePricingTS)
        FROM VehiclePricings 
        WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
        GROUP BY VehicleInventoryItemID) 
    INNER JOIN VehiclePricingDetails pd ON p.VehiclePricingID = pd.VehiclePricingID 
      AND pd.Typ = 'VehiclePricingDetail_BestPrice'         
    LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID) t
  WHERE name = 'Rydells'
  GROUP BY VehicleType, Price, name) a  
ON y.VehicleType = a.VehicleType
  AND y.Price = a.Price
  AND y.name = a.name 
  
  

SELECT st.VehicleType, st.name, CAST(vs.SoldTS AS sql_date) AS dateSold, s.VehicleInventoryItemID,
  st.amount AS price, vs.SoldAmount, vs.typ 
FROM (  
  SELECT i.stocknumber, m.VehicleType, o.name, i.VehicleInventoryItemID,
    i.FromTS, i.ThruTS, pd.Amount
  FROM VehicleInventoryItems i
  INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
  INNER JOIN (
    SELECT make, model,
      CASE VehicleType
        WHEN 'VehicleType_Car' THEN 'Car'
        WHEN 'VehicleType_Pickup' THEN 'Truck'
        WHEN 'VehicleType_SUV' THEN 'SUV'
        ELSE 'Other'
      END AS VehicleType
    FROM MakeModelClassifications) m ON v.make = m.make
      AND v.model = m.model
  INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
    AND VehiclePricingTS = (
      SELECT MAX(VehiclePricingTS)
      FROM VehiclePricings 
      WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
      GROUP BY VehicleInventoryItemID) 
  INNER JOIN VehiclePricingDetails pd ON p.VehiclePricingID = pd.VehiclePricingID 
    AND pd.Typ = 'VehiclePricingDetail_BestPrice'           
  LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID
  WHERE (i.ThruTS IS NULL OR Timestampdiff(sql_tsi_day, i.ThruTS, now()) < 30)) st
LEFT JOIN VehicleSales vs ON st.VehicleInventoryItemID = vs.VehicleInventoryItemID   
  AND vs.status = 'VehicleSale_Sold'
LEFT JOIN VehicleInventoryItemStatuses s ON st.VehicleInventoryItemID = s.VehicleInventoryItemID 
  AND s.category = 'RMFlagAV'
  AND s.ThruTS IS NULL   
 


SELECT m.VehicleType, o.name, 
  vs.SoldAmount, pd.Amount AS Price
FROM VehicleInventoryItems i
INNER JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
INNER JOIN (
  SELECT make, model,
    CASE VehicleType
      WHEN 'VehicleType_Car' THEN 'Car'
      WHEN 'VehicleType_Pickup' THEN 'Truck'
      WHEN 'VehicleType_SUV' THEN 'SUV'
      ELSE 'Other'
    END AS VehicleType
  FROM MakeModelClassifications) m ON v.make = m.make
    AND v.model = m.model
INNER JOIN VehiclePricings p ON i.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings 
    WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
    GROUP BY VehicleInventoryItemID) 
INNER JOIN VehiclePricingDetails pd ON p.VehiclePricingID = pd.VehiclePricingID 
  AND pd.Typ = 'VehiclePricingDetail_BestPrice'           
LEFT JOIN organizations o ON i.OwningLocationID = o.PartyID
LEFT JOIN VehicleSales vs ON i.VehicleInventoryItemID = vs.VehicleInventoryItemID   
  AND vs.status = 'VehicleSale_Sold'
  AND vs.typ = 'VehicleSale_Retail'
LEFT JOIN VehicleInventoryItemStatuses s ON i.VehicleInventoryItemID = s.VehicleInventoryItemID 
  AND s.category = 'RMFlagAV'
  AND s.ThruTS IS NULL  
WHERE (i.ThruTS IS NULL OR Timestampdiff(sql_tsi_day, i.ThruTS, now()) < 30) 
  AND name = 'Rydells'
