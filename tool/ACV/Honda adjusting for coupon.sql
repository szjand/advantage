SELECT *
FROM VehicleInventoryItems
WHERE CAST(fromts AS sql_date) BETWEEN curdate() - 30 AND curdate()
  AND LEFT(stocknumber, 1) = 'H'
  
  
SELECT p.fullname, 
  (SELECT stocknumber FROM VehicleInventoryItems WHERE VehicleInventoryItemID = c.VehicleInventoryItemID) AS stocknumber, 
  right(TRIM(correction), 20)
FROM correctionlog c
LEFT JOIN people p ON c.partyid = p.partyid
WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItems
    WHERE CAST(fromts AS sql_date) BETWEEN curdate() - 30 AND curdate()
      AND LEFT(stocknumber, 1) = 'H')
  AND Description = 'Eval ACV correction'    
    
select VehicleInventoryItemID, MIN(selectedACV), MAX(selectedACV), MAX(selectedACV) - MIN(selectedACV)
FROM acvs    
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems
  WHERE CAST(fromts AS sql_date) BETWEEN curdate() - 30 AND curdate()
    AND LEFT(stocknumber, 1) = 'H')
GROUP BY VehicleInventoryItemID 
ORDER BY VehicleInventoryItemID     


SELECT *
FROM VehicleInventoryItemNotes 
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems
  WHERE CAST(fromts AS sql_date) BETWEEN curdate() - 30 AND curdate()
    AND LEFT(stocknumber, 1) = 'H')
AND notes LIKE '%coupon%'    