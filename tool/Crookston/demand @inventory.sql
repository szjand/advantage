SELECT vs.soldts, vs.soldamount, vix.yearmodel, vix.make, vix.model
FROM vehiclesales vs
INNER JOIN VehicleInventoryItems viix ON vs.VehicleInventoryItemID = viix.VehicleInventoryItemID
 AND viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'Crookston')
INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
WHERE vs.typ = 'VehicleSale_Retail'
ORDER BY vs.soldts DESC 