-- this IS part of sp.GetGlumpExtract_New, which IS used BY reportprocessor2 to generate
-- the crayon report
-- the problem IS IN generating the MileageCategory
-- eg, 9/4/2012, 2013 modelyear results IN the divisor being = 0
-- boom
-- the short term fix, edited the month/day that gets concantenated FROM -09-01 to 08-01, 
-- which results IN the divisor being = 12
SELECT *
FROM (
select a.VehicleItemID, a.vin, a.yearmodel,
TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(a.yearmodel, SQL_INTEGER) - 1, SQL_CHAR)) + '-08-01', SQL_DATE), CurDate()) * 12 AS divisor
FROM VehicleItems a) a
--WHERE divisor = 0
WHERE VehicleItemID IN ('24b78e2a-520a-f644-b913-0c02bdf85fbd','8fb18093-1a3c-e547-b4fb-a26e0e24c8cc','03095f6d-922c-ec4e-9ad8-33718b984857','72d42c78-3c7f-874b-92a2-d8a0d2028d72')

SELECT * FROM VehicleItems

SELECT VehicleItemID, value 
FROM VehicleItemMileages b
WHERE VehicleItemMileageTS = (
  SELECT MAX(VehicleItemMileageTS)
  FROM VehicleItemMileages
  WHERE VehicleItemID = b.VehicleItemID
  GROUP BY VehicleItemID)
  
SELECT value  

SELECT vii.*,
  case
    when (c.Mileage = 0) or (Length(Trim(vii.YearModel)) <> 4) or (Locate('.', vii.YearModel) <> 0) or (Locate('C', vii.Yearmodel) <> 0) or (Locate('/', vii.Yearmodel) <> 0) or ((vii.Yearmodel < '1901') OR (vii.Yearmodel > '2050')) then Null
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 4000 then 'Freaky Low'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 8000 then 'Very Low'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 12000 then 'Low'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 15000 then 'Average'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 18000 then 'High'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 22000 then 'Very High'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 999999 then 'Freaky High'
    else
      'Unknown'
  end as "MileageCategory"
FROM VehicleItems vii  
LEFT JOIN (
  SELECT VehicleItemID, value AS mileage 
  FROM VehicleItemMileages b
  WHERE VehicleItemMileageTS = (
    SELECT MAX(VehicleItemMileageTS)
    FROM VehicleItemMileages
    WHERE VehicleItemID = b.VehicleItemID
    GROUP BY VehicleItemID)) c ON vii.VehicleItemID = c.VehicleItemID 
WHERE vii.VehicleItemID IN (
  SELECT VehicleItemID
  FROM VehicleInventoryItems
  WHERE thruts IS NULL)    
  
  
  
  
  
  
 12/16/17
 since 12/4, car-midsize DO NOT show up IN ry1 reports, eg no malibus
 data looks good
 stumped
  ---------------------------------------------------------------------------------------------------
execute procedure GetGlumpExtract_New('GF-Grand Forks', curdate()); 
SELECT COUNT(*) FROM [#adf6e0a3-73ce-f94c-9b32-06079b982652]

SELECT max(dateacquired), MAX(datesold) FROM [#adf6e0a3-73ce-f94c-9b32-06079b982652] -- 12/15, 12/15

SELECT location, vehicletype, segment, disposition, COUNT(*) FROM [#adf6e0a3-73ce-f94c-9b32-06079b982652]
GROUP BY location, vehicletype, segment, disposition
ORDER BY COUNT(*) DESC 

select * FROM [#adf6e0a3-73ce-f94c-9b32-06079b982652] WHERE model = 'malibu' 

select * FROM [#adf6e0a3-73ce-f94c-9b32-06079b982652] WHERE stocknumber = '32337xx'

SELECT location, disposition, COUNT(*) FROM zzz_glump_extract GROUP BY location, disposition

execute procedure GetGlumpExtract_New('GF-Grand Forks', curdate()-30); 
SELECT COUNT(*) FROM [#0caf0a8a-334c-0c47-b17e-9b174143f5f6]
SELECT * FROM [#0caf0a8a-334c-0c47-b17e-9b174143f5f6]

SELECT max(dateacquired), MAX(datesold) FROM [#0caf0a8a-334c-0c47-b17e-9b174143f5f6] -- 11/16, 11/16

SELECT location, vehicletype, segment, disposition, COUNT(*) FROM [#0caf0a8a-334c-0c47-b17e-9b174143f5f6]
GROUP BY location, vehicletype, segment, disposition
ORDER BY COUNT(*) DESC 

select * FROM [#0caf0a8a-334c-0c47-b17e-9b174143f5f6] WHERE model = 'malibu' 

select '11/16', a.* FROM [#0caf0a8a-334c-0c47-b17e-9b174143f5f6] a WHERE stocknumber = '32337xx'
UNION all
select '12/15', a.* FROM [#adf6e0a3-73ce-f94c-9b32-06079b982652] a WHERE stocknumber = '32337xx'

execute procedure GetGlumpExtract_New('GF-Grand Forks', '11/30/2017'); --#06e6c619-5a46-2d44-abbe-70eda4f90983

select '11/30', a.* FROM [#06e6c619-5a46-2d44-abbe-70eda4f90983] a WHERE stocknumber = '32337xx'
UNION all
select '12/15', a.* FROM [#adf6e0a3-73ce-f94c-9b32-06079b982652] a WHERE stocknumber = '32337xx'
---------------------------------------------------------------------------------------------

execute procedure GetGlumpExtract_New('GF-Grand Forks', curdate()); 
execute procedure GetGlumpExtract_New('GF-Grand Forks', '11/30/2017');

SELECT * FROM [#49abd004-418f-df4b-82a0-2e4bab033243]


SELECT 'curdate',stocknumber, make, model, dateacquired,datesold, daystosell, daysonlot, daysowned,
  gross, disposition, salesstatus, glump, sellingweek, pricebanddesc
-- INTO #1218  
FROM [#49abd004-418f-df4b-82a0-2e4bab033243]
WHERE vehicletype = 'car'
  AND segment = 'midsize'
  AND location = 'GF-Rydells'
union  
SELECT '11-30',stocknumber, make, model, dateacquired,datesold, daystosell, daysonlot, daysowned,
  gross, disposition, salesstatus, glump, sellingweek, pricebanddesc
-- INTO #1130  
FROM [#49abd004-418f-df4b-82a0-2e4bab033243]
WHERE vehicletype = 'car'
  AND segment = 'midsize'
  AND location = 'GF-Rydells'  
ORDER BY dateacquired DESC 
-- looks LIKE setting a date IN the SP doesn't WORK, vehicles acquired 12/15 IN #1130
-- but the graphs show malibus WHEN date IS SET to 11/30, but NOT since 12/4
SELECT *
FROM #1218 a
left JOIN #1130 b on a.stocknumber = b.stocknumber
WHERE b.stocknumber IS NULL



SELECT * FROM grosslogtable ORDER BY thedate DESC 

SELECT year, week, MIN(thedate), MAX(thedate), COUNT(*)
FROM grosslogtable
WHERE vehiclesegment = 'Midsize'
  AND vehicletype = 'Car'
  AND locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
GROUP BY year,week  

SELECT year, week, vehiclesegment, vehicletype, MIN(thedate), MAX(thedate), COUNT(*)
FROM grosslogtable
WHERE  locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  AND year = 2017
GROUP BY year, week, vehiclesegment, vehicletype
ORDER BY COUNT(*) DESC 

-- midsize sales
SELECT a.stocknumber, c.make, c.model, b.fromts, f.isoweek
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'RawMaterials_Sold'
INNER JOIN VehicleItems c on a.VehicleItemID = c.VehicleItemID
INNER JOIN MakeModelClassifications d on c.make = d.make
  AND c.model = d.model
  AND d.vehiclesegment = 'VehicleSegment_Midsize'
  AND d.vehicletype = 'VehicleType_Car'
INNER JOIN dds.day f on cast(b.fromts AS sql_date) = f.thedate 
WHERE cast(b.fromts AS sql_date) > '10/31/2017'  
  AND a.stocknumber NOT LIKE 'h%'
ORDER BY f.isoweek  


SELECT * FROM vehiclecosts ORDER BY datesoldfromtool DESC

--------------------------------------------------------------------------------------
12/18
converting to python
first need glptrns -> stgArkonaGlptrns to get updated
-- DELETE FROM stg_Arkona_GLPTRNS         
SELECT COUNT(*) FROM stg_Arkona_GLPTRNS   
5: 5486: 14 sec  
100: 5515: 13 sec
100: 62929 2min 26sec
1000:  202302 12min 17sec -- 32 days, slows down AS size of return SET increases

5000 3 days 1 MIN 5 sec
1000 5 days 1 MIN 36 sec
1000 30 days 6 MIN 25 sec
SELECT gtdate, COUNT(*) FROM stg_Arkona_GLPTRNS GROUP BY gtdate



SELECT gtco#, gttrn#, gtseq#
FROM stg_Arkona_GLPTRNS   
GROUP BY gtco#, gttrn#, gtseq#
HAVING COUNT(*) > 1

10/1 = 78
SELECT curdate() - 30 FROM system.iota

backfill 78 - 3-


SELECT gtdate, COUNT(*) FROM dds.stgArkonaGLPTRNS where gtdate > '11/18/2017' GROUP BY gtdate
