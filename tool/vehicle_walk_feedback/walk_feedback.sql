                SELECT cast(a.VehicleWalkTS AS sql_date) AS walk_date, c.fullname AS walker, d.fullname AS evaluator,
                  e.stocknumber, f.vin, f.yearmodel, f.make, f.model, f.TRIM, f.bodystyle, k.value AS miles, g.notes,
                  false
                FROM VehicleWalks a
                JOIN VehicleEvaluations b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
                JOIN people c on a.vehiclewalkerid = c.partyid
                JOIN people d on b.vehicleevaluatorid = d.partyid
                JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID
                JOIN VehicleItems f on e.VehicleItemID = f.VehicleItemID
                JOIN VehicleInventoryItemNotes g on a.VehicleWalkid = g.tablekey
                  AND g.subcategory = 'VehicleWalk_EvaluatorFeedback'
                LEFT JOIN VehicleItemMileages k on b.VehicleEvaluationID = k.tablekey
                -- WHERE CAST(notests AS sql_date) > '10/13/2022'
                WHERE CAST(notests AS sql_date) = curdate()
                    AND NOT EXISTS (
                        SELECT 1
                        FROM vehicle_walk_feedback
                        WHERE stock_number = e.stocknumber);
												
SELECT * FROM vehicle_walk_Feedback												
/*
the issue is i only want to copy the walks into pg once, but i am going to scrape the data multiple times 
a day, could handle it with a timestamp
so, what i did, add a field copied_to_pg that is set to false when the record is inserted into
vehicle_walk_feedback, but then set to true after writing the output of vehicle_walk_feedback to file
for copying to pg
*/
UPDATE vehicle_walk_feedback
SET copied_to_pg = true
WHERE walk_date < curdate();

DELETE FROM vehicle_walk_Feedback
WHERE walk_date = curdate();


select * 
from vehicle_walk_feedback
where walk_date = curdate()
  and copied_to_pg = false;
