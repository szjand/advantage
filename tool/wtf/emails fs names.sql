created tables on the wtf connection
saved sql IN sql\wtf

CREATE TABLE nameList (
  firstName cichar(25),
  lastName cichar(25)) IN database;
  
CREATE TABLE emailList (
  email cichar(50),
  emailFirstInitial cichar(1),
  emailLastName cichar(25)) IN database;

Names: James Kuefer was listed twice  
SELECT firstname, lastname FROM namelist GROUP BY firstname, lastname HAVING COUNT(*) > 1

SELECT email FROM emaillist GROUP BY email HAVING COUNT(*) > 1

folks with multiple emails:
SELECT emailfirstinitial, emaillastname FROM emaillist GROUP BY emailfirstinitial, emaillastname HAVING COUNT(*) > 1

SELECT a.email
FROM emaillist a
INNER JOIN (
  SELECT emailfirstinitial, emaillastname 
  FROM emaillist 
  GROUP BY emailfirstinitial, emaillastname HAVING COUNT(*) > 1) b on a.emailfirstinitial = b.emailfirstinitial AND a.emaillastname = b.emaillastname
  
SELECT a.lastname, a.firstname, b.email
FROM namelist a
LEFT JOIN emaillist b on trim(ucase(a.lastname)) = trim(ucase(b.emaillastname))
  AND LEFT(a.firstname, 1) = b.emailfirstinitial
AND b.email NOT IN (
  SELECT a.email
  FROM emaillist a
  INNER JOIN (
    SELECT emailfirstinitial, emaillastname 
    FROM emaillist 
    GROUP BY emailfirstinitial, emaillastname HAVING COUNT(*) > 1) b on a.emailfirstinitial = b.emailfirstinitial AND a.emaillastname = b.emaillastname)  
ORDER BY a.lastname, a.firstname  


SELECT email FROM emaillist a
LEFT JOIN namelist b on a.emaillastname = b.lastname
WHERE b.lastname IS NULL 