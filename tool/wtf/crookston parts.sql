DROP TABLE partlist;
CREATE TABLE partlist (
  partnumber cichar(12),
  RY3qty integer) IN database;

DROP TABLE partlocation;
CREATE TABLE partlocation (
  partnumber cichar(12),
  location cichar(12),
  status cichar(1),
  RY1qty integer) IN database; 

DROP TABLE replacementpart;  
CREATE TABLE replacementpart (
  oldpartnumber cichar(12),
  newpartnumber cichar(12),
  location cichar(12),
  status cichar(1),
  RY1qty integer,
  sales integer) IN database;
    
DROP table parthistory;   
CREATE TABLE parthistory (
  partnumber cichar(12),
  sales integer) IN database;

CREATE TABLE replacementparthistory (
  oldpartnumber cichar(12),
  newPartnumber cichar(12),
  sales integer) IN database;     
CREATE TABLE replacementpartlocation (
  oldpartnumber cichar(12),
  newPartnumber cichar(12),
  location cichar(12)) IN database;    

  
SELECT *
FROM partlist a
LEFT JOIN parthistory b ON a.partnumber = b.partnumber 
LEFT JOIN partlocation c ON a.partnumber = c.partnumber
LEFT JOIN replacementpart d ON a.partnumber = d.oldpartnumber
ORDER BY a.partnumber

SELECT a.partnumber, 
  CASE 
    WHEN d.oldpartnumber IS NULL THEN 
      CASE 
        WHEN c.status = 'A' THEN 'Active'
        WHEN c.status = 'N' THEN 'Non Stock'
      END 
    ELSE 
      CASE 
        WHEN d.status = 'A' THEN 'Active'
        WHEN d.status = 'N' THEN 'Non Stock'
      END     
  END AS status,
  newpartnumber, 

  ry3qty AS "RY3 ON HAND", 
  coalesce(c.ry1qty, 0) AS "RY1 ON HAND",
  CASE 
    WHEN d.oldpartnumber IS NULL THEN coalesce(b.sales, 0)
    ELSE coalesce(b.sales, 0) + coalesce(d.sales, 0)
  END AS sales,
  c.location AS "Bin Location",
  newpartnumber,
  coalesce(d.ry1qty, 0) AS "New Part# ON HAND",
  d.location AS "New Part# Bin Location"
--SELECT COUNT(*)  
FROM partlist a
LEFT JOIN parthistory b ON a.partnumber = b.partnumber 
LEFT JOIN partlocation c ON a.partnumber = c.partnumber
LEFT JOIN replacementpart d ON a.partnumber = d.oldpartnumber
ORDER BY a.partnumber     

select * FROM replacementpart WHERE newpartnumber = '89060416'