Pull signals AND termination
pull signal can change during the interval
a pulled vehicle may be sold, unpulled, etc
pulled -> pricing buffer pull
available -> recon complete
wsb -> recon complete
sold/dcu -> delivered
sb -> delivered

-- pull signal used for parts orders
      SELECT VehicleInventoryItemID, MIN(fromTS)
      FROM VehicleInventoryItemStatuses viix
      WHERE ThruTS IS NULL  
      AND category LIKE 'RMFlag%' -- ooh, even better, 60ms
      AND (
        EXISTS ( 
          SELECT 1 -- pulled
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
          AND category = 'RMFlagPulled'
          AND ThruTS IS NULL)
        OR EXISTS (
          SELECT 1 -- available 
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
          AND category = 'RMFlagAV'
          AND ThruTS IS NULL)
        OR EXISTS (  
          SELECT 1 -- wsb 
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
          AND category = 'RMFlagWSB'
          AND ThruTS IS NULL)
        OR EXISTS (
          SELECT 1 -- sold/dcu
          FROM VehicleSales 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
          AND status = 'VehicleSale_Sold')
        OR EXISTS (
          SELECT 1 -- sales buffer
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
          AND category = 'RMFLagSB'
          AND ThruTS IS NULL)) 
      GROUP BY viix.VehicleInventoryItemID) PullSignal ON vrix.VehicleInventoryItemID = PullSignal.VehicleInventoryItemID  
      
-- pulled -> pricing buffer
-- for now, only those that go FROM pulled to pb
SELECT viix.stocknumber, pull.PulledTS, PBTS, viix.VehicleInventoryItemID 
FROM VehicleInventoryItems viix
LEFT JOIN (
  SELECT VehicleInventoryItemID, FromTS AS PulledTS
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
INNER JOIN (
  SELECT VehicleInventoryItemID, FromTS AS PBTS
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
WHERE cast(viix.FromTS AS sql_date) >  curdate() - 90  
AND viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
AND pb.pbts IS NOT NULL 

  
-- pulled to pricing puffer
SELECT stocknumber, VehicleInventoryItemID, pTS, pbTS, TotalHours, (WeekDayHours + SundayHours + SaturdayHours + Day1Hours + LastDayHours) AS OpenHours
INTO #jon
FROM ( -- any query that generates 2 timestamps
  SELECT stocknumber, VehicleInventoryItemID, 
    MAX(@ts1) AS pTS, MAX(@ts2) AS pbTS,
    MAX(timestampdiff(sql_tsi_hour, @ts1, @ts2)) AS TotalHours,
    SUM(CASE WHEN d.DayOfWeek IN (2,3,4,5,6) THEN wh.TotalHours ELSE 0 END) AS WeekDayHours,
    SUM(CASE WHEN d.DayOfWeek = 1 THEN wh.TotalHours ELSE 0 END) AS SundayHours,
    SUM(CASE WHEN d.DayOfWeek = 7 THEN wh.TotalHours ELSE 0 END) AS SaturdayHours, 
    MAX(
      CASE
          WHEN 
            CASE hour(wh.closes)
              WHEN 0 THEN (24 - hour(@ts1))
              ELSE (hour(wh.closes) - hour(@ts1))
            END < 0 THEN 0
          ELSE 
            CASE hour(wh.closes) 
              WHEN 0 then (24 - hour(@ts1))
              ELSE (hour(wh.closes) - hour(@ts1))
            END      
      END) AS Day1Hours,
    MAX(hour(@ts2) - hour((SELECT opens FROM workinghours WHERE dayofweek = dayofweek(@ts2)))) AS LastDayHours
  FROM (
    SELECT viix.stocknumber, pull.PulledTS AS @ts1, PBTS AS @ts2, viix.VehicleInventoryItemID 
    FROM VehicleInventoryItems viix
    LEFT JOIN (
      SELECT VehicleInventoryItemID, FromTS AS PulledTS
      FROM VehicleInventoryItemStatuses 
      WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
    INNER JOIN (
      SELECT VehicleInventoryItemID, FromTS AS PBTS
      FROM VehicleInventoryItemStatuses 
      WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
    WHERE cast(viix.FromTS AS sql_date) >  curdate() - 90  
    AND viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
    AND pb.pbts IS NOT NULL ) ts,
    dds.Day d,
    WorkingHours wh
  WHERE wh.DayOfWeek = d.DayOfWeek  
  AND d.TheDate > CAST(ts.@ts1 AS sql_date)
  AND d.TheDate < CAST(ts.@ts2 AS sql_date)
  GROUP BY stocknumber, VehicleInventoryItemID) wtf  

-- DROP TABLE #jon  

-- so the one FROM #jon with a wtf IN the interval IS 3d6f9334-dec3-4a3a-9005-5755594d33d8
-- ADD another one within the interval to test grouping
-- AND THEN ADD one outside of the interval
INSERT INTO viiwtf (VehicleInventoryItemID, typ, createdby, createdts, resolvedby, resolvedts, issue)
values ('3d6f9334-dec3-4a3a-9005-5755594d33d8', 'WTF_viiOther', (SELECT partyid FROM users WHERE username = 'jon'),
  '07/06/2011 13:00:00', (SELECT partyid FROM users WHERE username = 'jon'), '07/09/2011 13:00:00', 'adding extra wtf within pull to term interval')  
  
INSERT INTO viiwtf (VehicleInventoryItemID, typ, createdby, createdts, resolvedby, resolvedts, issue)
values ('3d6f9334-dec3-4a3a-9005-5755594d33d8', 'WTF_viiOther', (SELECT partyid FROM users WHERE username = 'jon'),
  '07/23/2011 13:00:00', (SELECT partyid FROM users WHERE username = 'jon'), '07/24/2011 13:00:00', 'adding extra wtf outside pull to term interval')    
  
-- SELECT * FROM #jon
-- also need to TRIM wtf time down to OPEN hours 
/*
            ts1                                 ts2         Test                                                        Output 
               |-----------------------------------|
1.    startTS|-------------|StopTS                            startTS < ts1 AND stopTS > ts1 AND stopTS <= ts2            stopTS - ts1
2.    startTS|--------------------------------------|stopTS   startTS < ts1 AND stopTS >= ts2                             ts2 - ts1           
3.      startTS|------------------|stopTS                     startTS >= ts1 AND startTS < ts2 AND stopTS <= ts2          stopTS - startTS
4.      startTS|-------------------------------------|stopTS  startTS >= ts1 AND startTS < ts2 AND stopTS >= ts2           ts2 - startTS                                                         
*/

-- this works before refactoring
SELECT distinct j.*, 
  CASE
    WHEN w.VehicleInventoryItemID IS NOT NULL THEN 
      CASE
        WHEN CreatedTS < pTS AND ResolvedTS > pTS AND ResolvedTS <= pbTS THEN ( -- 1. 
          SELECT SUM(timestampdiff(sql_tsi_hour, pTS, ResolvedTS))
          FROM viiwtf
          WHERE VehicleInventoryItemID = w.VehicleInventoryItemID 
            AND CreatedTS < pTS 
            AND ResolvedTS > pTS 
            AND ResolvedTS <= pbTS          
          GROUP BY VehicleInventoryItemID)
        WHEN CreatedTS < pTS AND ResolvedTS >= pbTS THEN ( -- 2. 
          SELECT SUM(timestampdiff(sql_tsi_hour, pTS, pbTS))
          FROM viiwtf
          WHERE VehicleInventoryItemID = w.VehicleInventoryItemID 
            AND CreatedTS < pTS
            AND ResolvedTS >= pbTS
          GROUP BY VehicleInventoryItemID)   
        WHEN CreatedTS >= pTS AND CreatedTS < pbTS AND ResolvedTS <= pbTS THEN ( -- 3.  
          SELECT SUM(timestampdiff(sql_tsi_hour, CreatedTS, ResolvedTS))
          FROM viiwtf
          WHERE VehicleInventoryItemID = w.VehicleInventoryItemID 
            AND CreatedTS >= pTS
            AND CreatedTS < pbTS
            AND ResolvedTS <= pbTS
          GROUP BY VehicleInventoryItemID)   
        WHEN CreatedTS >= pTS AND CreatedTS < pbTS AND ResolvedTS >= pbTS THEN ( --4.  
          SELECT SUM(timestampdiff(sql_tsi_hour, CreatedTS, pbTS))
          FROM viiwtf
          WHERE VehicleInventoryItemID = w.VehicleInventoryItemID 
            AND CreatedTS >= pTS
            AND CreatedTS < pbTS
            AND ResolvedTS >= pbTS
          GROUP BY VehicleInventoryItemID)                        
        ELSE 0
      END
    ELSE 0 
  END AS wtfWait        
FROM #jon j
LEFT JOIN ViiWTF w ON j.VehicleInventoryItemID = w.VehicleInventoryItemID  
  AND ((CreatedTS < pTS AND ResolvedTS > pTS AND ResolvedTS <= pbTS)
    OR (CreatedTS < pTS AND ResolvedTS >= pbTS)
    OR (CreatedTS >= pTS AND CreatedTS < pbTS AND ResolvedTS <= pbTS)
    OR (CreatedTS >= pTS AND CreatedTS < pbTS AND ResolvedTS >= pbTS))  

-- Greg says moving hours should match service hours
-- so the question becomes WHERE IN this morass DO i CONVERT total hours to working hours
-- lets look at move list first   
-- cool lots of data
SELECT distinct j.*, 
  CASE
    WHEN m.VehicleInventoryItemID IS NOT NULL THEN 
      CASE
        WHEN FromTS < pTS AND ThruTS > pTS AND ThruTS <= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, pTS, ThruTS))
          FROM VehiclesToMove
          WHERE VehicleInventoryItemID = m.VehicleInventoryItemID 
            AND FromTS < pTS 
            AND ThruTS > pTS 
            AND ThruTS <= pbTS          
          GROUP BY VehicleInventoryItemID)
        WHEN FromTS < pTS AND ThruTS >= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, pTS, pbTS))
          FROM VehiclesToMove
          WHERE VehicleInventoryItemID = m.VehicleInventoryItemID 
            AND FromTS < pTS
            AND ThruTS >= pbTS
          GROUP BY VehicleInventoryItemID)   
        WHEN FromTS >= pTS AND FromTS < pbTS AND ThruTS <= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, FromTS, ThruTS))
          FROM VehiclesToMOve
          WHERE VehicleInventoryItemID = m.VehicleInventoryItemID 
            AND FromTS >= pTS
            AND FromTS < pbTS
            AND ThruTS <= pbTS
          GROUP BY VehicleInventoryItemID)   
        WHEN FromTS >= pTS AND FromTS < pbTS AND ThruTS >= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, FromTs, pbTS))
          FROM VehiclesToMove
          WHERE VehicleInventoryItemID = m.VehicleInventoryItemID 
            AND FromTS >= pTS
            AND FromTS < pbTS
            AND ThruTS >= pbTS
          GROUP BY VehicleInventoryItemID)                        
        ELSE 0
      END
    ELSE 0 
  END AS MoveWait        
FROM #jon j
LEFT JOIN VehiclesToMove m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID  
  AND ((m.FromTS < pTS AND m.ThruTS > pTS AND m.ThruTS <= pbTS)
    OR (m.FromTS < pTS AND m.ThruTS >= pbTS)
    OR (m.FromTS >= pTS AND m.FromTS < pbTS AND m.ThruTS <= pbTS)
    OR (m.FromTS >= pTS AND m.FromTS < pbTS AND m.ThruTS >= pbTS))  
  

-- maybe a function to return working hours
-- pass it a pair of timestamps    


    
SELECT j.*, m.status, m.fromts, m.thruts
FROM #jon j
LEFT JOIN VehiclesToMove m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID     
WHERE j.VehicleInventoryItemID = 'a9d50354-f38f-4229-b327-44dc567ac09e' -- perfect, 3 records within interval 

SELECT DISTINCT j.*,
  CASE
    WHEN FromTS >= pTS AND FromTS < pbTS AND ThruTS <= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, FromTS, ThruTS))
--      SELECT SUM(SELECT Utilities.BusinessHoursFromInterval(pTS, ThruTS) FROM system.iota)))
--      SELECT SUM(SELECT Utilities.BusinessHoursFromInterval(cast('05/27/2011 08:40:42' AS sql_timestamp), cast('05/27/2011 10:44:52' as sql_timestamp)) FROM system.iota)
      FROM VehiclesToMOve
      WHERE VehicleInventoryItemID = m.VehicleInventoryItemID 
        AND FromTS >= pTS
        AND FromTS < pbTS
        AND ThruTS <= pbTS
      GROUP BY VehicleInventoryItemID) 
  END 
FROM #jon j
LEFT JOIN VehiclesToMove m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID  
WHERE j.VehicleInventoryItemID = 'a9d50354-f38f-4229-b327-44dc567ac09e'

SELECT timestampdiff(sql_tsi_hour, cast('05/26/2011 08:40:42' AS sql_timestamp), cast('05/27/2011 10:44:52' as sql_timestamp)) FROM system.iota
 


















  
  
-- AND the 2 together 
-- a tentative wahoo right here folks
SELECT distinct j.*, 
  CASE
    WHEN m.VehicleInventoryItemID IS NOT NULL THEN 
      CASE
        WHEN FromTS < pTS AND ThruTS > pTS AND ThruTS <= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, pTS, ThruTS))
          FROM VehiclesToMove
          WHERE VehicleInventoryItemID = m.VehicleInventoryItemID 
            AND FromTS < pTS 
            AND ThruTS > pTS 
            AND ThruTS <= pbTS          
          GROUP BY VehicleInventoryItemID)
        WHEN FromTS < pTS AND ThruTS >= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, pTS, pbTS))
          FROM VehiclesToMove
          WHERE VehicleInventoryItemID = m.VehicleInventoryItemID 
            AND FromTS < pTS
            AND ThruTS >= pbTS
          GROUP BY VehicleInventoryItemID)   
        WHEN FromTS >= pTS AND FromTS < pbTS AND ThruTS <= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, FromTS, ThruTS))
          FROM VehiclesToMOve
          WHERE VehicleInventoryItemID = m.VehicleInventoryItemID 
            AND FromTS >= pTS
            AND FromTS < pbTS
            AND ThruTS <= pbTS
          GROUP BY VehicleInventoryItemID)   
        WHEN FromTS >= pTS AND FromTS < pbTS AND ThruTS >= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, FromTs, pbTS))
          FROM VehiclesToMove
          WHERE VehicleInventoryItemID = m.VehicleInventoryItemID 
            AND FromTS >= pTS
            AND FromTS < pbTS
            AND ThruTS > pbTS
          GROUP BY VehicleInventoryItemID)                        
        ELSE 0
      END
    ELSE 0 
  END AS MoveWait,
  CASE
    WHEN w.VehicleInventoryItemID IS NOT NULL THEN 
      CASE
        WHEN CreatedTS < pTS AND ResolvedTS > pTS AND ResolvedTS <= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, pTS, ResolvedTS))
          FROM viiwtf
          WHERE VehicleInventoryItemID = w.VehicleInventoryItemID 
            AND CreatedTS < pTS 
            AND ResolvedTS > pTS 
            AND ResolvedTS <= pbTS          
          GROUP BY VehicleInventoryItemID)
        WHEN CreatedTS < pTS AND ResolvedTS >= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, pTS, pbTS))
          FROM viiwtf
          WHERE VehicleInventoryItemID = w.VehicleInventoryItemID 
            AND CreatedTS < pTS
            AND ResolvedTS >= pbTS
          GROUP BY VehicleInventoryItemID)   
        WHEN CreatedTS >= pTS AND CreatedTS < pbTS AND ResolvedTS <= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, CreatedTS, ResolvedTS))
          FROM viiwtf
          WHERE VehicleInventoryItemID = w.VehicleInventoryItemID 
            AND CreatedTS >= pTS
            AND CreatedTS < pbTS
            AND ResolvedTS <= pbTS
          GROUP BY VehicleInventoryItemID)   
        WHEN CreatedTS >= pTS AND CreatedTS < pbTS AND ResolvedTS >= pbTS THEN (  
          SELECT SUM(timestampdiff(sql_tsi_hour, CreatedTS, pbTS))
          FROM viiwtf
          WHERE VehicleInventoryItemID = w.VehicleInventoryItemID 
            AND CreatedTS >= pTS
            AND CreatedTS < pbTS
            AND ResolvedTS >= pbTS
          GROUP BY VehicleInventoryItemID)                        
        ELSE 0
      END
    ELSE 0 
  END AS wtfWait            
FROM #jon j
LEFT JOIN VehiclesToMove m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID  
  AND ((m.FromTS < pTS AND m.ThruTS > pTS AND m.ThruTS <= pbTS)
    OR (m.FromTS < pTS AND m.ThruTS >= pbTS)
    OR (m.FromTS >= pTS AND m.FromTS < pbTS AND m.ThruTS <= pbTS)
    OR (m.FromTS >= pTS AND m.FromTS < pbTS AND m.ThruTS >= pbTS))  
LEFT JOIN ViiWTF w ON j.VehicleInventoryItemID = w.VehicleInventoryItemID  
  AND ((CreatedTS < pTS AND ResolvedTS > pTS AND ResolvedTS <= pbTS)
    OR (CreatedTS < pTS AND ResolvedTS >= pbTS)
    OR (CreatedTS >= pTS AND CreatedTS < pbTS AND ResolvedTS <= pbTS)
    OR (CreatedTS >= pTS AND CreatedTS < pbTS AND ResolvedTS >= pbTS))     
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  


    
    
    
    
    
    
    
    
    
    
    
    
    
