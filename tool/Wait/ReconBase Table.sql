/* Each of these vehicles has multiple start times IN wip */
SELECT VehicleInventoryItemID, ar.startts, ar.completets
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
WHERE ri.VehicleInventoryItemID IN (  
  'fdf96d70-a6ae-4d1f-8a67-e0c806e5037c',
  'fde53ea0-aa94-41b5-8753-b0d3bf1e1073',
  'ff48d766-a350-4cdd-bc41-b21b699bac54',
  'fb15a2c6-bb57-415b-b167-697e45284c47',
  'f22c1dca-6255-49da-b995-4eca8832a005')   
AND ri.typ <> 'MechanicalReconItem_Inspection'
AND ar.status  = 'AuthorizedReconItem_Complete'
AND ri.typ in (  
  SELECT Typ 
  FROM TypCategories
  WHERE Category = 'MechanicalReconItem')
   
  
SELECT VehicleInventoryItemID , ar.startts, ar.completets, timestampdiff(sql_tsi_hour, startts, completets)
-- SUM(timestampdiff(sql_tsi_hour, ar.startts, ar.completets))
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
WHERE ri.VehicleInventoryItemID IN (  -- 'f22c1dca-6255-49da-b995-4eca8832a005' 
  'fdf96d70-a6ae-4d1f-8a67-e0c806e5037c',
  'fde53ea0-aa94-41b5-8753-b0d3bf1e1073',
  'ff48d766-a350-4cdd-bc41-b21b699bac54',
  'fb15a2c6-bb57-415b-b167-697e45284c47',
  'f22c1dca-6255-49da-b995-4eca8832a005')   
AND ri.typ <> 'MechanicalReconItem_Inspection'
AND ar.status  = 'AuthorizedReconItem_Complete'
AND ri.typ in (  
  SELECT Typ 
  FROM TypCategories
  WHERE Category = 'MechanicalReconItem')  

-- GROUP BY VehicleInventoryItemID, startts, completets
-- collapses multiple line items with the same startts AND completets INTO one row
SELECT VehicleInventoryItemID , ar.startts, ar.completets
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
WHERE ri.VehicleInventoryItemID IN (  -- 'f22c1dca-6255-49da-b995-4eca8832a005' 
  'fdf96d70-a6ae-4d1f-8a67-e0c806e5037c',
  'fde53ea0-aa94-41b5-8753-b0d3bf1e1073',
  'ff48d766-a350-4cdd-bc41-b21b699bac54',
  'fb15a2c6-bb57-415b-b167-697e45284c47',
  'f22c1dca-6255-49da-b995-4eca8832a005')   
AND ri.typ <> 'MechanicalReconItem_Inspection'
AND ar.status  = 'AuthorizedReconItem_Complete'
AND ri.typ in (  
  SELECT Typ 
  FROM TypCategories
  WHERE Category = 'MechanicalReconItem')
GROUP BY VehicleInventoryItemID, ar.startts, ar.completets  


-- total time per VehicleInventoryItemID
-- doesn't help
-- need the DISTINCT intervals, check for overlap with external interval (pull -> pb)
-- AND THEN total time per VehicleInventoryItemID 
SELECT VehicleInventoryItemID, SUM(timestampdiff(sql_tsi_hour, startts, completets))
FROM (
  SELECT VehicleInventoryItemID, ar.startts, ar.completets
  --  SUM(timestampdiff(sql_tsi_hour, ar.startts, ar.completets))
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  WHERE ri.VehicleInventoryItemID IN (  -- 'f22c1dca-6255-49da-b995-4eca8832a005' 
    'fdf96d70-a6ae-4d1f-8a67-e0c806e5037c',
    'fde53ea0-aa94-41b5-8753-b0d3bf1e1073',
    'ff48d766-a350-4cdd-bc41-b21b699bac54',
    'fb15a2c6-bb57-415b-b167-697e45284c47',
    'f22c1dca-6255-49da-b995-4eca8832a005')      
  AND ri.typ <> 'MechanicalReconItem_Inspection'
  AND ar.status  = 'AuthorizedReconItem_Complete'
  AND ri.typ in (  
    SELECT Typ 
    FROM TypCategories
    WHERE Category = 'MechanicalReconItem')
  GROUP BY ri.VehicleInventoryItemID, ar.startts, ar.completets) wtf GROUP BY VehicleInventoryItemID

-- #jon AS the interval  
-- oh fucking great, none of those vehicles match the #jon interval
SELECT j.VehicleInventoryItemID, j.stocknumber, j.pulledTS, j.pbTS, r.* 
FROM #jon j
LEFT JOIN (
  SELECT VehicleInventoryItemID , ar.startts, ar.completets
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  WHERE ri.VehicleInventoryItemID IN (  -- 'f22c1dca-6255-49da-b995-4eca8832a005' 
    'fdf96d70-a6ae-4d1f-8a67-e0c806e5037c',
    'fde53ea0-aa94-41b5-8753-b0d3bf1e1073',
    'ff48d766-a350-4cdd-bc41-b21b699bac54',
    'fb15a2c6-bb57-415b-b167-697e45284c47',
    'f22c1dca-6255-49da-b995-4eca8832a005')   
  AND ri.typ <> 'MechanicalReconItem_Inspection'
  AND ar.status  = 'AuthorizedReconItem_Complete'
  AND ri.typ in (  
    SELECT Typ 
    FROM TypCategories
    WHERE Category = 'MechanicalReconItem')
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets ) r ON j.VehicleInventoryItemID = r.VehicleInventoryItemID
      AND (r.completeTS > j.pulledTS OR r.startTS >= j.pulledTS)
WHERE r.VehicleInventoryItemID IS NOT NULL 

-- so let's DO it ON ALL vehicles
-- cool
SELECT j.VehicleInventoryItemID, j.stocknumber, j.pulledTS, j.pbTS, r.* 
FROM #jon j
LEFT JOIN (
  SELECT VehicleInventoryItemID , ar.startts, ar.completets
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  WHERE ri.typ <> 'MechanicalReconItem_Inspection'
  AND ar.status  = 'AuthorizedReconItem_Complete'
  AND ri.typ in (  
    SELECT Typ 
    FROM TypCategories
    WHERE Category = 'MechanicalReconItem')
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets ) r ON j.VehicleInventoryItemID = r.VehicleInventoryItemID
    AND (r.startTS <= j.pbTS AND (r.completeTS > j.pulledTS OR r.startTS >= j.pulledTS))
WHERE r.VehicleInventoryItemID IS NOT NULL 
--ORDER BY j.stocknumber

-- this IS getting real CLOSE
-- but for the notion of padding 0 time wip
-- plus i don't see a clear path to integrating this with what i already have IN Pull to PB.sql
-- thinking of just generating a temp TABLE to JOIN to #jon
-- so, ALL 3 IN one?
SELECT j.VehicleInventoryItemID,
  sum(timestampdiff(sql_tsi_minute, r.startTS, r.completeTS)) AS MechBusHours
FROM #jon j
LEFT JOIN ( -- Mech startTS, completeTS
  SELECT VehicleInventoryItemID , ar.startts, ar.completets
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  WHERE ri.typ <> 'MechanicalReconItem_Inspection'
  AND ar.status  = 'AuthorizedReconItem_Complete'
  AND ri.typ in (  
    SELECT Typ 
    FROM TypCategories
    WHERE Category = 'MechanicalReconItem')
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets ) r ON j.VehicleInventoryItemID = r.VehicleInventoryItemID
    AND (r.startTS <= j.pbTS AND (r.completeTS > j.pulledTS OR r.startTS >= j.pulledTS))
WHERE r.VehicleInventoryItemID IS NOT NULL 
GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS --each vehicles trip to wip


-- so, ALL 3 IN one?
SELECT j.VehicleInventoryItemID, category,
  CASE 
    WHEN category = 'MechanicalReconItem' then sum(timestampdiff(sql_tsi_minute, r.startTS, r.completeTS)) 
    else 0
  END AS MechBusHours,
  CASE 
    WHEN category = 'BodyReconItem' then sum(timestampdiff(sql_tsi_minute, r.startTS, r.completeTS)) 
    else 0
  END AS BodyBusHours  
  
FROM #jon j
LEFT JOIN ( -- Mech startTS, completeTS
  SELECT VehicleInventoryItemID , ar.startts, ar.completets, category -- yes, this needs to be grouped
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) r ON j.VehicleInventoryItemID = r.VehicleInventoryItemID
    AND (r.startTS <= j.pbTS AND (r.completeTS > j.pulledTS OR r.startTS >= j.pulledTS))
WHERE r.VehicleInventoryItemID IS NOT NULL 
GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS, category--each vehicles trip to wip

-- this piece seems right
-- would LIKE to speed it up, limit the SET somehow
-- but that's NOT most important
-- what i need FROM this IS total elapsed time for each vehicle IN each dept
-- AND i am struggling with doing it IN one query
-- so, this could be a temp TABLE
-- but what diff would that make, still selecting against the same SET of data
  SELECT VehicleInventoryItemID , ar.startts, ar.completets, category -- yes, this needs to be grouped
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category

-- ok, let's play with this
-- verify, that yes, it does indeed produce multiple records
-- for a vehicle IN a dept
SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- yes, this needs to be grouped
INTO #base 
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
LEFT JOIN TypCategories t ON ri.typ = t.typ
WHERE ar.status  = 'AuthorizedReconItem_Complete'
GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category  

select * FROM #base

-- absolutely, lots AND lots
SELECT VehicleInventoryItemID, category, COUNT(*)
FROM #base
GROUP BY VehicleInventoryItemID, category
HAVING COUNT(*) > 1

SELECT *
FROM #base b
WHERE VehicleInventoryItemID IN  (
  SELECT VehicleInventoryItemID
  FROM #base
  GROUP BY VehicleInventoryItemID, category
  HAVING COUNT(*) > 1)
AND startTS IS NOT null  
ORDER BY VehicleInventoryItemID, category

-- so the notion of just summing the data doesn't WORK
-- because we need to know IF start-complete overlaps the interval

-- compare this data
-- with the interval
-- base data IS already grouped ON VehicleInventoryItemID, startTS, completeTS AND category
-- this now gives me only the wip within the interval
SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category
FROM #jon j 
LEFT JOIN (
  SELECT * 
  FROM #base
  WHERE startTS IS NOT NULL) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS)) 
  
-- now i need to SUM the resulting wip BY VehicleInventoryItemID, category
/*
              pulledTS                     pbTS                          Test                                                            Output 
                |----------------------------|
1.  startTS|-------------|completeTS                            startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS      completeTS - pulledTS
2.  startTS|-------------------------------------|completeTS    startTS < pulledTS AND completeTS >= pbTS                                pbTS - pulledTS           
3        startTS|--------------|completeTS                      startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS            completeTS - startTS
4.       startTS|--------------------------------|completeTS    startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS           pbTS - startTS                                                       
*/ 

-- CLOSE, now need a single record per vehicle 
SELECT VehicleInventoryItemID,
  SUM(
    CASE 
      WHEN category = 'MechanicalReconItem' THEN
        CASE  
          WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS,completeTS)
          WHEN startTS < pulledTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS, pbTS)
          WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, startTS, completeTS)
          WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, startTS, pbTS)
        END 
    END) AS MechBusHours,
  SUM(
    CASE 
      WHEN category = 'BodyReconItem' THEN
        CASE  
          WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS,completeTS)
          WHEN startTS < pulledTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS, pbTS)
          WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, startTS, completeTS)
          WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, startTS, pbTS)
        END 
    END) AS BodyBusHours,  
  SUM(
    CASE 
      WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN
        CASE  
          WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS,completeTS)
          WHEN startTS < pulledTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS, pbTS)
          WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, startTS, completeTS)
          WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, startTS, pbTS)
        END 
    END) AS AppBusHours       
FROM (
  SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category
  FROM #jon j 
  LEFT JOIN (
    SELECT * 
    FROM #base
    WHERE startTS IS NOT NULL) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
GROUP BY VehicleInventoryItemID, category      

-- looking good
SELECT VehicleInventoryItemID, MAX(MechHours) AS MechHours, MAX(BodyHours) AS BodyHours, 
  MAX(AppHours) AS AppHours
FROM (
  SELECT VehicleInventoryItemID,
    SUM(
      CASE 
        WHEN category = 'MechanicalReconItem' THEN
          CASE  
            WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS,completeTS)
            WHEN startTS < pulledTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS, pbTS)
            WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, startTS, completeTS)
            WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, startTS, pbTS)
          END 
      END) AS MechHours,
    SUM(
      CASE 
        WHEN category = 'BodyReconItem' THEN
          CASE  
            WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS,completeTS)
            WHEN startTS < pulledTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS, pbTS)
            WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, startTS, completeTS)
            WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, startTS, pbTS)
          END 
      END) AS BodyHours,  
    SUM(
      CASE 
        WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN
          CASE  
            WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS,completeTS)
            WHEN startTS < pulledTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS, pbTS)
            WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, startTS, completeTS)
            WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, startTS, pbTS)
          END 
      END) AS AppHours       
  FROM (
    SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category
    FROM #jon j 
    LEFT JOIN (
      SELECT * 
      FROM #base
      WHERE startTS IS NOT NULL) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
        AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
  GROUP BY VehicleInventoryItemID, category) x    
GROUP BY VehicleInventoryItemID   

-- AND the final results
SELECT j.stocknumber, j.VehicleInventoryItemID, j.pulledTS, j.pbTS, h.MechHours, h.BodyHours, h.AppHours
FROM #jon j
LEFT JOIN (
  SELECT VehicleInventoryItemID, MAX(MechHours) AS MechHours, MAX(BodyHours) AS BodyHours, 
    MAX(AppHours) AS AppHours
  FROM (
    SELECT VehicleInventoryItemID,
      SUM(
        CASE 
          WHEN category = 'MechanicalReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS,completeTS)
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS, pbTS)
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, startTS, completeTS)
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, startTS, pbTS)
            END 
        END) AS MechHours,
      SUM(
        CASE 
          WHEN category = 'BodyReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS,completeTS)
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS, pbTS)
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, startTS, completeTS)
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, startTS, pbTS)
            END 
        END) AS BodyHours,  
      SUM(
        CASE 
          WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS,completeTS)
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, pulledTS, pbTS)
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN timestampdiff(sql_tsi_hour, startTS, completeTS)
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN timestampdiff(sql_tsi_hour, startTS, pbTS)
            END 
        END) AS AppHours       
    FROM (
      SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category
      FROM #jon j 
      LEFT JOIN (
        SELECT * 
        FROM #base
        WHERE startTS IS NOT NULL) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
          AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
    GROUP BY VehicleInventoryItemID, category) x    
  GROUP BY VehicleInventoryItemID) h ON j.VehicleInventoryItemID = h.VehicleInventoryItemID 

  
-- AND the final results
-- oops, NOT quite final, need business hours
/*
              pulledTS                     pbTS                          Test                                                            Output 
                |----------------------------|
1.  startTS|-------------|completeTS                            startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS      completeTS - pulledTS
2.  startTS|-------------------------------------|completeTS    startTS < pulledTS AND completeTS >= pbTS                                pbTS - pulledTS           
3        startTS|--------------|completeTS                      startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS            completeTS - startTS
4.       startTS|--------------------------------|completeTS    startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS           pbTS - startTS                                                       
*/ 
SELECT j.stocknumber, j.VehicleInventoryItemID, j.pulledTS, j.pbTS, h.MechBusHours, h.BodyBusHours, h.AppBusHours
FROM #jon j
LEFT JOIN (
  SELECT VehicleInventoryItemID, MAX(MechBusHours) AS MechBusHours, MAX(BodyBusHours) AS BodyBusHours, MAX(AppBusHours) AS AppBusHours
  FROM (
    SELECT VehicleInventoryItemID,
      SUM(
        CASE 
          WHEN category = 'MechanicalReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'Service') FROM system.iota)
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota)
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'Service') FROM system.iota)
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'Service') FROM system.iota)
            END 
        END) AS MechBusHours,
      SUM(
        CASE 
          WHEN category = 'BodyReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'BodyShop') FROM system.iota)
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'BodyShop') FROM system.iota)
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'BodyShop') FROM system.iota)
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'BodyShop') FROM system.iota)
            END 
        END) AS BodyBusHours,  
      SUM(
        CASE 
          WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'Detail') FROM system.iota)
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Detail') FROM system.iota)
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'Detail') FROM system.iota)
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'Detail') FROM system.iota)
            END 
        END) AS AppBusHours       
    FROM (
      SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category
      FROM #jon j 
      LEFT JOIN (
        SELECT * 
        FROM #base
        WHERE startTS IS NOT NULL) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
          AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
    GROUP BY VehicleInventoryItemID, category) x    
  GROUP BY VehicleInventoryItemID) h ON j.VehicleInventoryItemID = h.VehicleInventoryItemID 
