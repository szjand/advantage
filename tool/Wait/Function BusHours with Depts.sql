/*
            Opens                         Closes         Test                                                  Output 
              |----------------------------|
1.    ts1|-------------|ts2                            ts1 < Opens AND ts2 > Opens AND ts2 <= Closes          ts2 - Opens
2.    ts1|-------------------------------------|ts2    ts1 < Opens AND ts2 >= Closes                          Closes- ts1           
3          ts1|--------------|ts2                      ts1 >= Opens AND ts1 < Closes AND ts2 <= Closes        ts2 - ts1
4.         ts1|--------------------------------|ts2    ts1 >= Opens AND Opens < Closes AND ts2 >= Closes      Closes - ts1                                                       
*/  
DECLARE @ts1 timestamp;
DECLARE @ts2 timestamp; 
DECLARE @Dept string;   
--@ts1 = cast('07/06/2011 08:06:32' AS sql_timestamp);
--@ts2 = cast('07/07/2011 07:37:53' as sql_timestamp);
-- DO a one day interval ON a sunday
@ts1 = cast('08/21/2011 08:06:32' AS sql_timestamp);
@ts2 = cast('08/21/2011 15:37:53' as sql_timestamp);
-- ok, WHEN dept = bodyshop, returns NULL, so coalesce the whole damned thing ?
--@Dept = 'BodyShop';
--@Dept = 'Service';
@Dept = 'Detail';

  SELECT 
    CASE dayofyear(@ts2) - dayofyear(@ts1)
      WHEN 0 THEN -- interval covers single day 
      coalesce( -- for closed working days, eg BodyShop Sunday, return 0
        MAX(
          CASE
            WHEN hour(@ts1) < hour(wh.opens) --1
              AND hour(@ts2) > hour(wh.opens) 
              AND hour(@ts2) <= iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN hour(@ts2) - hour(wh.opens)  
            WHEN hour(@ts1) < hour(wh.opens) --2
              AND hour(@ts2) > iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1)  
            WHEN hour(@ts1) >= hour(wh.opens) --3
              AND hour(@ts1) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) 
              AND hour(@ts2) <= iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN hour(@ts2) - hour(@ts1)
            WHEN hour(@ts1) > hour(wh.opens) -- 4
              AND hour(wh.opens) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes))  
              AND hour(@ts2) >= iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1)
          END), 0) 
      WHEN 1 THEN -- interval covers 2 days 
      coalesce(-- for closed working days, eg BodyShop Sunday, return 0
        MAX(
          CASE  -- day 1
            WHEN hour(@ts1) < hour(wh.Opens) THEN wh.TotalHours --2
            ELSE iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1)
          END 
        +
          CASE -- day 2
            WHEN hour(@ts2) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN hour(@ts2) - hour(wh.Opens) --3
            ELSE wh.TotalHours
          END), 0) 
      ELSE -- interval convers more than 2 days  
        coalesce(-- for closed working days, eg BodyShop Sunday, return 0
          SUM(CASE WHEN d.DayOfWeek IN (2,3,4,5,6) THEN wh.TotalHours ELSE 0 END) 
          +
          SUM(CASE WHEN d.DayOfWeek = 1 THEN wh.TotalHours ELSE 0 END) 
          +
          SUM(CASE WHEN d.DayOfWeek = 7 THEN wh.TotalHours ELSE 0 END) 
          -
          MAX(
            CASE           
              WHEN DayOfYear(@ts1) = d.DayOfYear THEN -- matches first day of interval to the correct day
                CASE 
                  WHEN hour(@ts1) < hour(wh.Opens) THEN 0 -- nothing to subtract, interval covers entire day 1 -- 24 - wh.TotalHours --2
                  ELSE wh.TotalHours - (iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1))  
                END
              ELSE 0
            END 
            +
            CASE 
              WHEN DayOfYear(@ts2) = d.DayOfYear THEN -- matches last day of interval to the correct day
                CASE -- day 2
                  WHEN hour(@ts2) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN wh.TotalHours - ((hour(@ts2) - hour(wh.Opens))) --3
                  ELSE 0 -- 24 - wh.TotalHours
                END
              ELSE 0
            END), 0)
    END AS BusHours
  FROM (-- any query that generates 2 timestamps: @ts1 < @ts2
    SELECT  @ts1,  @ts2
    FROM system.iota) ts,
    dds.Day d,
    WorkingHours wh
  WHERE wh.DayOfWeek = d.DayOfWeek 
  AND d.TheDate >= CAST(@ts1 AS sql_date)  
  AND d.TheDate <= CAST(@ts2 AS sql_date)
  AND
    CASE
      WHEN @Dept = 'Service' THEN wh.Dept = 'Service'
      WHEN @Dept = 'BodyShop' THEN wh.Dept = 'BodyShop'
      WHEN @Dept = 'Detail' THEN wh.Dept = 'Detail'
      ELSE 1 = 1
    END


