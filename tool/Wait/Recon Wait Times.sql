/*
exceptions: 
  14319X pulled to pb, THEN recon added after pb
  13904B seq = amb, AppWait = -2, appearance started before pull
  
 13663XXA: ba, app = -1 :: app started before body finished
 12401B: ba, body = -97 :: body started before pull
 13655A: ba, body = -500 :: body started before pull
 14455A: ba, body = -24 :: body started before pull
since a vehicle can be IN wip IN multiple depts, force negative values to zero
*/

*/
-- raw AuthorizedReconItems data
SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category 
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
LEFT JOIN TypCategories t ON ri.typ = t.typ
WHERE ar.status  = 'AuthorizedReconItem_Complete'
AND ar.startTS IS NOT NULL 
ORDER BY VehicleInventoryItemID 

-- #jon IS the base interval TABLE, IN this CASE pull -> pb
-- AND FINALLY one row per vehicle with start times for each dept
-- need to expose pullTS & pbTS

SELECT VehicleInventoryItemID, 
  MAX(pulledTS) AS pulledTS, MAX(pbTS) AS pbTS,
  MAX(MechStart) AS MechStart, MAX(MechEnd) AS MechEnd, 
  MAX(BodyStart) AS BodyStart, MAX(BodyEnd) AS BodyEnd, 
  MAX(AppStart) AS AppStart, MAX(AppEnd) AS AppEnd
INTO #WipStartTimes
FROM (
  SELECT VehicleInventoryItemID,-- this gives us one row for each vehicle/dept
    MAX(pulledTS) AS pulledTS,
    MAX(pbTS) AS pbTS,
    MAX(CASE WHEN category = 'MechanicalReconItem' THEN startTS END) AS MechStart,
    MAX(CASE WHEN category = 'BodyReconItem' THEN startTS END) AS BodyStart,  
    MAX(CASE  WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN startTS END) AS AppStart, 
    MAX(CASE WHEN category = 'MechanicalReconItem' THEN completeTS END) AS MechEnd,
    MAX(CASE WHEN category = 'BodyReconItem' THEN completeTS END) AS BodyEnd,  
    MAX(CASE  WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN completeTS END) AS AppEnd          
  FROM (
    SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
    FROM #jon j 
    LEFT JOIN (
      SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
      FROM VehicleReconItems ri
      INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
      LEFT JOIN TypCategories t ON ri.typ = t.typ
      WHERE ar.status  = 'AuthorizedReconItem_Complete'
      AND ar.startTS IS NOT NULL 
      GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
        AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
  GROUP BY VehicleInventoryItemID, category) x
GROUP BY VehicleInventoryItemID; 

-- DROP TABLE #WipStartTimes
/*
-- hmm, a seq: -m *
               -mb *
               -ma *
               -mba *
               -mab *
               -b
               -bm *
               -ba
               -bma *
               -bam *
               -a
               -ab
               -am *
               -abm *
               -amb
*/ 


\
              
SELECT w.*,
  CASE
    WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
    WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
    WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
    WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
    WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
      CASE
        WHEN MechStart < BodyStart THEN 'mb'
        ELSE 'bm'
      END
    WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
      CASE
        WHEN MechStart < AppStart THEN 'ma'
        ELSE 'am'
      END
    WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
      CASE
        WHEN BodyStart < AppStart THEN 'ba'
        ELSE 'ab'
      END 
    WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
      CASE 
        WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
        WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
        WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
        WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
        WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
        WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
      END 
  END AS seq     
FROM #WipStartTimes w

SELECT seq, COUNT(*)
FROM (
  SELECT w.*,
    CASE
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
        CASE
          WHEN MechStart < BodyStart THEN 'mb'
          ELSE 'bm'
        END
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN MechStart < AppStart THEN 'ma'
          ELSE 'am'
        END
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN BodyStart < AppStart THEN 'ba'
          ELSE 'ab'
        END 
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
        CASE 
        WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
        WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
        WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
        WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
        WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
        WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
        END 
    END AS seq     
  FROM #WipStartTimes w) x GROUP BY seq
ORDER BY COUNT(*) desc
-- how to ADD move overlaps, hmmm
-- start with

-- pull -- waiting for mech -- mech wip -- waiting for body --body wip -- waiting for app -- app wip

SELECT VehicleInventoryItemID, seq,
-- mech wait
  CASE 
    WHEN left(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, MechStart) < 0, 0, timestampdiff(sql_tsi_hour, pulledTS, MechStart))
   
    WHEN substring(seq, 2, 1) = 'm' THEN
      CASE   
        WHEN LEFT(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, MechStart) < 0, 0, timestampdiff(sql_tsi_hour, BodyEnd, MechStart))
        WHEN LEFT(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, MechStart) < 0, 0, timestampdiff(sql_tsi_hour, AppEnd, MechStart))
      END
    WHEN seq = 'bam' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, MechStart) < 0, 0, timestampdiff(sql_tsi_hour, AppEnd, MechStart))
    WHEN seq = 'abm' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, MechStart) <0, 0, timestampdiff(sql_tsi_hour, BodyEnd, MechStart))
    
  END AS MechWait,
  CASE -- body wait
    WHEN left(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, pulledTS, BodyStart))
    WHEN substring(seq, 2, 1) = 'b' THEN
      CASE
        WHEN LEFT(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, MechEnd, BodyStart))
        WHEN LEFT(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, AppEnd, BodyStart))
      END 
    WHEN seq = 'amb' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, MechEnd, BodyStart)) 
    WHEN seq = 'mab' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, AppEnd, BodyStart))
  END AS BodyWait,
  CASE -- app wait
    WHEN left(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, pulledTS, AppStart))
    WHEN substring(seq, 2, 1) = 'a' THEN
      CASE
        WHEN LEFT(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, MechEnd, AppStart))
        WHEN LEFT(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, BodyEnd, AppStart)) 
      END 
    WHEN seq = 'mba' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, BodyEnd, AppStart))
    WHEN seq = 'bma' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, MechEnd, AppStart))
  END AS AppWait,
  
  CASE -- mech wait bus
    WHEN left(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, MechStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(pulledTS, MechStart, 'Service') FROM system.iota))
    WHEN substring(seq, 2, 1) = 'm' THEN
      CASE   
        WHEN LEFT(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, MechStart) < 0, 0,(select Utilities.BusinessHoursFromInterval(BodyEnd, MechStart, 'Service') FROM system.iota))
        WHEN LEFT(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, MechStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(AppEnd, MechStart, 'Service') FROM system.iota))
      END
    WHEN seq = 'bam' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, MechStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(AppEnd, MechStart, 'Service') FROM system.iota))
    WHEN seq = 'abm' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, MechStart) <0, 0, (select Utilities.BusinessHoursFromInterval(BodyEnd, MechStart, 'Service') FROM system.iota))
  END AS MechWaitBus,
  CASE -- body wait bus
    WHEN left(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(pulledTS, BodyStart, 'BodyShop') FROM system.iota))
    WHEN substring(seq, 2, 1) = 'b' THEN
      CASE
        WHEN LEFT(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(MechEnd, BodyStart, 'BodyShop') FROM system.iota))
        WHEN LEFT(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(AppEnd, BodyStart, 'BodyShop') FROM system.iota))
      END 
    WHEN seq = 'amb' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(MechEnd, BodyStart, 'BodyShop') FROM system.iota)) 
    WHEN seq = 'mab' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(AppEnd, BodyStart, 'BodyShop') FROM system.iota))
  END AS BodyWaitBus,
  CASE -- app wait bus
    WHEN left(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(pulledTS, AppStart, 'Detail') FROM system.iota)) 
    WHEN substring(seq, 2, 1) = 'a' THEN
      CASE
        WHEN LEFT(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(MechEnd, AppStart, 'Detail') FROM system.iota))
        WHEN LEFT(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(BodyEnd, AppStart, 'Detail') FROM system.iota)) 
      END 
    WHEN seq = 'mba' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(BodyEnd, AppStart, 'Detail') FROM system.iota))
    WHEN seq = 'bma' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(MechEnd, AppStart, 'Detail') FROM system.iota))
  END AS AppWaitBus 
INTO #ReconWait    
--  p.hours
FROM ( -- recon sequence
  SELECT w.*,
    CASE
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
        CASE
          WHEN MechStart < BodyStart THEN 'mb'
          ELSE 'bm'
        END
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN MechStart < AppStart THEN 'ma'
          ELSE 'am'
        END
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN BodyStart < AppStart THEN 'ba'
          ELSE 'ab'
        END 
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
        CASE 
          WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
          WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
        END 
    END AS seq     
  FROM #WipStartTimes w) x 
-- LEFT JOIN #partsholdbushours p ON x.VehicleInventoryItemID = p.id  

SELECT * FROM #ReconWait