-- UPDATE seq COLUMN of #WipBase
DECLARE @Cur CURSOR AS 
  SELECT * 
  FROM #WIPBase
  WHERE VehicleInventoryItemID = @CurGroup.VehicleInventoryItemID 
  ORDER BY  FromTS; 

DECLARE @CurGroup CURSOR AS   
  SELECT VehicleInventoryItemID 
  FROM #WIPBase
  GROUP BY VehicleInventoryItemID;   

DECLARE @i integer;
@i = 1;
OPEN @CurGroup;
TRY 
  WHILE FETCH @CurGroup DO 
    @i = 1;
    OPEN @Cur;
    TRY 
      WHILE FETCH @Cur DO 
        UPDATE #WIPBase
        SET seq = @i
        WHERE VehicleInventoryItemID = @Cur.VehicleInventoryItemID AND category = @Cur.category AND FromTS = @Cur.FromTS;
        @i = @i + 1;
      END WHILE;
    FINALLY
      CLOSE @Cur;
    END;
  END WHILE; 
  
FINALLY
  CLOSE @CurGroup;
END;     


-- generate #ReconSeq
SELECT VehicleInventoryItemID,  
  trim(
    coalesce(
      MAX((
        SELECT 
          CASE category
            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END 
        FROM #WipBase WHERE seq = 1 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),''))
  + 
  trim(
    coalesce(
      MAX((
        SELECT 
          CASE category
            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END         
        FROM #WipBase WHERE seq = 2 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),''))
  + 
  trim(
    coalesce(
      MAX((
        SELECT 
          CASE category
            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END          
        FROM #WipBase WHERE seq = 3 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),''))
  + 
  trim(
    coalesce(
      MAX((
      SELECT 
          CASE category
            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END        
      FROM #WipBase WHERE seq = 4 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),'')) AS ReconSeq
INTO #ReconSeq      
FROM #WipBase w
GROUP BY VehicleInventoryItemID;

SELECT * FROM #ReconSeq
DROP TABLE #ReconSeq
SELECT ReconSeq, COUNT(*) FROM #ReconSeq GROUP BY reconseq ORDER BY COUNT(*)

SELECT * FROM #WipBase