-- this IS the first version
-- which eliminates multiple instances of wip
-- which caused Greg to notice that the MechCount seemed low IN Put Together 2
-- changes to make: don't elim mult/

-- may have to go with a separate base recon TABLE ...
--
/*
SELECT da.*, v.*,
  (SELECT FromTS
    FROM VehicleInventoryItemStatuses vmf
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID
    AND Status = 'MechanicalReconProcess_InProcess'
    AND FromTS >= pulledts -- ?only WORK started after the pull ?
    AND ThruTS <= pbts
    AND NOT EXISTS( -- eliminates vehicles with multiple instances of wip
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = v.VehicleInventoryItemID
      AND Status = 'MechanicalReconProcess_InProcess'
      GROUP BY VehicleInventoryItemID
      HAVING COUNT(*) > 1)) AS MechFrom,
  (SELECT ThruTS
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID
    AND Status = 'MechanicalReconProcess_InProcess'
    AND FromTS >= pulledts -- ?only WORK started after the pull ?
    AND ThruTS <= pbts 
    AND NOT EXISTS(
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = v.VehicleInventoryItemID
      AND Status = 'MechanicalReconProcess_InProcess'
      GROUP BY VehicleInventoryItemID
      HAVING COUNT(*) > 1)) AS MechThru
FROM (
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17      
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate >= CurDate() - 118
  AND TheDate <= CurDate()) da  
LEFT JOIN (
  SELECT *
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS PulledTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS pbTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
  WHERE viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
  AND pb.pbts IS NOT NULL -- made it ALL the way to the lot   
  AND NOT EXISTS (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)
  AND NOT EXISTS (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPB'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)) v ON da.TheDate = CAST(v.pbTS AS sql_date) 
    
*/

--SELECT 
/*
  (SELECT  
    SUM(
      CASE
        WHEN FromTS < PulledTS THEN timestampdiff(sql_tsi_minute, pulledts, ThruTS) 
        ELSE timestampdiff(sql_tsi_minute, Fromts, ThruTS) 
      END) AS MechTime
    FROM VehicleInventoryItemStatuses  
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND status = 'MechanicalReconProcess_InProcess' 
    AND ThruTS >= pulledts
    AND ThruTS <= pbts
    GROUP BY VehicleInventoryItemID),  
*/    
--select  da.*, v.*   
-- DROP TABLE #jon 
--INTO #jon  
/*  
FROM (
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() -   6 AND TheDate <= CurDate() -   0 THEN 1
      WHEN TheDate >= CurDate() -  13 AND TheDate <= CurDate() -   7 THEN 2
      WHEN TheDate >= CurDate() -  20 AND TheDate <= CurDate() -  14 THEN 3
      WHEN TheDate >= CurDate() -  27 AND TheDate <= CurDate() -  21 THEN 4
      WHEN TheDate >= CurDate() -  34 AND TheDate <= CurDate() -  28 THEN 5
      WHEN TheDate >= CurDate() -  41 AND TheDate <= CurDate() -  35 THEN 6
      WHEN TheDate >= CurDate() -  48 AND TheDate <= CurDate() -  42 THEN 7
      WHEN TheDate >= CurDate() -  55 AND TheDate <= CurDate() -  49 THEN 8
      WHEN TheDate >= CurDate() -  62 AND TheDate <= CurDate() -  56 THEN 9
      WHEN TheDate >= CurDate() -  69 AND TheDate <= CurDate() -  63 THEN 10
      WHEN TheDate >= CurDate() -  76 AND TheDate <= CurDate() -  70 THEN 11
      WHEN TheDate >= CurDate() -  83 AND TheDate <= CurDate() -  77 THEN 12
      WHEN TheDate >= CurDate() -  90 AND TheDate <= CurDate() -  84 THEN 13
      WHEN TheDate >= CurDate() -  97 AND TheDate <= CurDate() -  91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() -  98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17      
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate >= CurDate() - 118
  AND TheDate <= CurDate()) da  
LEFT JOIN (
  SELECT *
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS PulledTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS pbTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
  WHERE viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
  AND pb.pbts IS NOT NULL -- made it ALL the way to the lot   
  AND NOT EXISTS ( 
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)
  AND NOT EXISTS (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPB'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)) v ON da.TheDate = CAST(v.pbTS AS sql_date) 
*/
--here's the problem
--2 instances of wip within the interval (8/11 - 8/22)
/*
SELECT description, startts, completets, s.*
FROM AuthorizedReconItems a
INNER JOIN VehicleReconItems v ON a.VehicleReconItemID = v.VehicleReconItemID 
LEFT JOIN VehicleInventoryItemStatuses s ON v.VehicleInventoryItemID = s.VehicleInventoryItemID 
  AND s.status = 'MechanicalReconProcess_InProcess'
WHERE v.VehicleInventoryItemID = '53faef46-eb05-4301-b038-fd0d90c737d3'
AND v.typ LIKE 'Mechan%'   
*/ 

-- so this give me the vehicles AND the interval (pulledTS - pbTS)
--SELECT j.VehicleInventoryItemID, j.pulledts, j.pbts 
--FROM #jon j 

--veering away FROM joins because of multiple records

SELECT stocknumber,
-- (select Utilities.BusinessHoursFromInterval(BodyFrom, BodyThru, 'BodyShop')
  (SELECT  
    SUM(
      CASE
--        WHEN FromTS < PulledTS THEN timestampdiff(sql_tsi_minute, pulledts, ThruTS)
        WHEN FromTS < PulledTS THEN (select Utilities.BusinessHoursFromInterval(PulledTS, ThruTS, 'Mechanical') FROM system.iota)
--        ELSE timestampdiff(sql_tsi_minute, Fromts, ThruTS)
        ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Mechanical') FROM system.iota)
      END) AS MechTime
    FROM VehicleInventoryItemStatuses  
    WHERE VehicleInventoryItemID = j.VehicleInventoryItemID 
    AND status = 'MechanicalReconProcess_InProcess' 
    AND ThruTS >= pulledts
    AND ThruTS <= pbts
    GROUP BY VehicleInventoryItemID) AS MechBusHours,
  (SELECT  
    SUM(
      CASE
--        WHEN FromTS < PulledTS THEN timestampdiff(sql_tsi_minute, pulledts, ThruTS)
        WHEN FromTS < PulledTS THEN (select Utilities.BusinessHoursFromInterval(PulledTS, ThruTS, 'Body') FROM system.iota)
--        ELSE timestampdiff(sql_tsi_minute, Fromts, ThruTS)
        ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Body') FROM system.iota)
      END) AS MechTime
    FROM VehicleInventoryItemStatuses  
    WHERE VehicleInventoryItemID = j.VehicleInventoryItemID 
    AND status = 'BodyReconProcess_InProcess' 
    AND ThruTS >= pulledts
    AND ThruTS <= pbts
    GROUP BY VehicleInventoryItemID) AS BodyBusHours,    
  (SELECT  
    SUM(
      CASE
--        WHEN FromTS < PulledTS THEN timestampdiff(sql_tsi_minute, pulledts, ThruTS)
        WHEN FromTS < PulledTS THEN (select Utilities.BusinessHoursFromInterval(PulledTS, ThruTS, 'Detail') FROM system.iota)
--        ELSE timestampdiff(sql_tsi_minute, Fromts, ThruTS)
        ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Detail') FROM system.iota)
      END) AS MechTime
    FROM VehicleInventoryItemStatuses  
    WHERE VehicleInventoryItemID = j.VehicleInventoryItemID 
    AND status = 'AppearanceReconProcess_InProcess' 
    AND ThruTS >= pulledts
    AND ThruTS <= pbts
    GROUP BY VehicleInventoryItemID) AS AppBusHours    
FROM #jon j 
--WHERE j.VehicleInventoryItemID = '53faef46-eb05-4301-b038-fd0d90c737d3'  

    
    