-- Level 1
-- week -- fr/th -- COUNT -- days
-- requires:
--    #PullToPb
--    #WipBase
SELECT TheWeek, MIN(TheDate) AS Beginning, MAX(TheDate) AS Ending,
  COUNT(DISTINCT p.VehicleInventoryItemID) AS [Count],
  SUM(timestampdiff(sql_tsi_day, cast(p.PulledTS AS sql_date), cast(p.pbTS AS sql_date)))/COUNT(DISTINCT p.VehicleInventoryItemID) AS [Avg Days/Vehicle],
  SUM(timestampdiff(sql_tsi_hour, p.PulledTS, p.pbTS))/COUNT(DISTINCT p.VehicleInventoryItemID) AS [Avg Hours/Vehicle]
FROM #PullToPB p
INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
  SELECT DISTINCT(VehicleInventoryItemID)
  FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
GROUP BY TheWeek

SELECT * 
FROM #PullToPB p
INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
  SELECT DISTINCT(VehicleInventoryItemID)
  FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  
  
-- total dept time base
SELECT x.theweek, x.VehicleInventoryItemID, x.reconseq,
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, mThru)
    WHEN 2 THEN -- *m*
      CASE
        -- bm*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)
        -- am*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru)
      END 
    WHEN 3 THEN -- **m
      CASE
        -- *bm
        WHEN substring(ReconSeq, 2, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)
        -- *am
        WHEN substring(ReconSeq, 2, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru)
      END     
  END AS MechHours,
  CASE position('b' IN ReconSeq) -- b**
    WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, bThru)
    WHEN 2 THEN -- *b*
      CASE
        -- mb*
        WHEN substring(ReconSeq, 1, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, bThru)
        -- ab*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, bThru)
      END 
    WHEN 3 THEN -- **b
      CASE
        -- *mb
        WHEN substring(ReconSeq, 2, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, bThru)
        -- *ab
        WHEN substring(ReconSeq, 2, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, bThru)
      END     
  END AS BodyHours,
  CASE position('a' IN ReconSeq) -- a**
    WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, aThru)
    WHEN 2 THEN -- *a*
      CASE
        -- ma*
        WHEN substring(ReconSeq, 1, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, aThru)
        -- ba*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, aThru)
      END 
    WHEN 3 THEN -- **a
      CASE
        -- *ma
        WHEN substring(ReconSeq, 2, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, aThru)
        -- *ba
        WHEN substring(ReconSeq, 2, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, aThru)
      END     
  END + coalesce(timestampdiff(sql_tsi_hour, oFrom, oThru), 0) AS AppHours  
FROM ( -- one row per vehicle
  SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFrom, o.ThrutS AS oThru
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL) x 
  
  
-- total dept time grouped
SELECT a.*, b.*,
  MechHours/mCount AS mAvgHours,
  BodyHours/bCount AS bAvgHours,
  AppHours/aCount AS aAvgHours,
  MechHours/mCount + BodyHours/bCount + AppHours/aCount AS "Dept Sum"
FROM (
  SELECT x.TheWeek,
    SUM(
      CASE position('m' IN ReconSeq) -- m**
        WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, mThru)
        WHEN 2 THEN -- *m*
          CASE
            -- bm*
            WHEN substring(ReconSeq, 1, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)
            -- am*
            WHEN substring(ReconSeq, 1, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru)
          END 
        WHEN 3 THEN -- **m
          CASE
            -- *bm
            WHEN substring(ReconSeq, 2, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)
            -- *am
            WHEN substring(ReconSeq, 2, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru)
          END     
      END) AS MechHours,
    SUM(
      CASE position('b' IN ReconSeq) -- b**
        WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, bThru)
        WHEN 2 THEN -- *b*
          CASE
            -- mb*
            WHEN substring(ReconSeq, 1, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, bThru)
            -- ab*
            WHEN substring(ReconSeq, 1, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, bThru)
          END 
        WHEN 3 THEN -- **b
          CASE
            -- *mb
            WHEN substring(ReconSeq, 2, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, bThru)
            -- *ab
            WHEN substring(ReconSeq, 2, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, bThru)
          END     
      END) AS BodyHours,
    SUM(
      CASE position('a' IN ReconSeq) -- a**
        WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, aThru)
        WHEN 2 THEN -- *a*
          CASE
            -- ma*
            WHEN substring(ReconSeq, 1, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, aThru)
            -- ba*
            WHEN substring(ReconSeq, 1, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, aThru)
          END 
        WHEN 3 THEN -- **a
          CASE
            -- *ma
            WHEN substring(ReconSeq, 2, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, aThru)
            -- *ba
            WHEN substring(ReconSeq, 2, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, aThru)
          END     
      END + coalesce(timestampdiff(sql_tsi_hour, oFrom, oThru), 0)) AS AppHours
  FROM ( -- one row per vehicle
    SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
      m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
      a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFrom, o.ThrutS AS oThru 
    FROM #PullToPB p 
    INNER JOIN ( -- exclusions FROM multiple wip/category
      SELECT DISTINCT VehicleInventoryItemID 
      FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
    LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
    LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
    LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
    LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
    LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
    WHERE p.VehicleInventoryItemID IS NOT NULL) x 
  GROUP BY TheWeek) a
  LEFT JOIN ( -- COUNT based ON seq
    SELECT p.TheWeek, 
      MIN(TheDate) AS Beginning, MAX(TheDate) AS Ending,
      SUM(CASE WHEN s.ReconSeq LIKE '%m%' THEN 1 ELSE 0 END) AS mCount,
      SUM(CASE WHEN s.ReconSeq LIKE '%b%' THEN 1 ELSE 0 END) AS bCount,
      SUM(CASE WHEN s.ReconSeq LIKE '%a%' THEN 1 ELSE 0 END) AS aCount,
      COUNT(DISTINCT p.VehicleInventoryItemID) AS [Count],
      SUM(timestampdiff(sql_tsi_hour, p.PulledTS, p.pbTS))/COUNT(DISTINCT p.VehicleInventoryItemID) AS [Avg Hours/Vehicle]
    FROM #PullToPB p 
    INNER JOIN ( -- exclusions FROM multiple wip/category
      SELECT DISTINCT VehicleInventoryItemID 
      FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
    LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
    WHERE p.VehicleInventoryItemID IS NOT NULL
    GROUP BY TheWeek) b ON a.TheWeek = b.TheWeek   

 
/*
SELECT * FROM #wipbase

SELECT category, COUNT(*)
FROM #Wipbase
GROUP BY category

SELECT 
  CASE 
    WHEN category
    
    
  SELECT p.TheWeek, 
    SUM(CASE WHEN s.ReconSeq LIKE '%m%' THEN 1 ELSE 0 END) AS mCount2,
    SUM(CASE WHEN s.ReconSeq LIKE '%b%' THEN 1 ELSE 0 END) AS bCount2,
    SUM(CASE WHEN s.ReconSeq LIKE '%a%' THEN 1 ELSE 0 END) AS aCount2
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  WHERE p.VehicleInventoryItemID IS NOT NULL
  GROUP BY TheWeek   
*/