-- total dept hours > pulltopb hours
SELECT y.*, MechHours + BodyHours + AppHours AS DeptTotals,
  PullToPb - (MechHours + BodyHours + AppHours)
FROM (
SELECT x.theweek, x.VehicleInventoryItemID, x.reconseq,
  timestampdiff(sql_tsi_hour, pulledts, pbts) AS PullToPb,
  coalesce(
    CASE position('m' IN ReconSeq) -- m**
      WHEN 1 THEN iif(timestampdiff(sql_tsi_hour, pulledTS, mThru) < 1, 1, timestampdiff(sql_tsi_hour, pulledTS, mThru))
      WHEN 2 THEN -- *m*
        CASE
          -- bm*
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, bThru, mThru) < 1, 1, timestampdiff(sql_tsi_hour, bThru, mThru))
          -- am*
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, aThru, mThru) < 1, 1, timestampdiff(sql_tsi_hour, aThru, mThru))
        END 
      WHEN 3 THEN -- **m
        CASE
          -- *bm
          WHEN substring(ReconSeq, 2, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, bThru, mThru) < 1, 1, timestampdiff(sql_tsi_hour, bThru, mThru))
          -- *am
          WHEN substring(ReconSeq, 2, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, aThru, mThru) < 1, 1, timestampdiff(sql_tsi_hour, aThru, mThru))
        END     
    END, 0) AS MechHours,
  coalesce(
    CASE position('b' IN ReconSeq) -- b**
      WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, bThru)
      WHEN 2 THEN -- *b*
        CASE
          -- mb*
          WHEN substring(ReconSeq, 1, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, mThru, bThru) < 1, 1, timestampdiff(sql_tsi_hour, mThru, bThru))
          -- ab*
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, aThru, bThru) < 1, 1, timestampdiff(sql_tsi_hour, aThru, bThru))
        END 
      WHEN 3 THEN -- **b
        CASE
          -- *mb
          WHEN substring(ReconSeq, 2, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, mThru, bThru) < 1, 1, timestampdiff(sql_tsi_hour, mThru, bThru))
          -- *ab
          WHEN substring(ReconSeq, 2, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, aThru, bThru) < 1, 1, timestampdiff(sql_tsi_hour, aThru, bThru))
        END     
    END, 0) AS BodyHours,
  coalesce(
    CASE position('a' IN ReconSeq) -- a**
      WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, aThru)
      WHEN 2 THEN -- *a*
        CASE
          -- ma*
          WHEN substring(ReconSeq, 1, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, mThru, aThru) < 1, 1, timestampdiff(sql_tsi_hour, mThru, aThru)) 
          -- ba*
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, bThru, aThru) < 1, 1, timestampdiff(sql_tsi_hour, bThru, aThru))
        END 
      WHEN 3 THEN -- **a
        CASE
          -- *ma
          WHEN substring(ReconSeq, 2, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, mThru, aThru) < 1, 1, timestampdiff(sql_tsi_hour, mThru, aThru))
          -- *ba
          WHEN substring(ReconSeq, 2, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, bThru, aThru) < 1, 1, timestampdiff(sql_tsi_hour, bThru, aThru))
        END     
    END, 0) AS AppHours  /*+ coalesce(timestampdiff(sql_tsi_hour, oFrom, oThru), 0)*/ 
FROM ( -- one row per vehicle
  SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFrom, o.ThrutS AS oThru
  FROM #PullToPB p 
--  INNER JOIN ( -- exclusions FROM multiple wip/category
--    SELECT DISTINCT VehicleInventoryItemID 
--    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
    SELECT DISTINCT(VehicleInventoryItemID)
    FROM #WipBase x
    WHERE NOT EXISTS (
      SELECT 1
      FROM #wipbase
      WHERE VehicleInventoryItemID = x.VehicleInventoryItemID 
      AND category = 'AppearanceReconItem')) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL) x) y
WHERE PullToPb - (MechHours + BodyHours + AppHours) < 4 -- why the big diff? 
ORDER BY PullToPb - (MechHours + BodyHours + AppHours) desc  
--WHERE MechHours + BodyHours + AppHours > pulltopb 

-- big diffs IN dept vs pulltopb
SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
  m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
  a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFrom, o.ThrutS AS oThru
FROM #PullToPB p 
--  INNER JOIN ( -- exclusions FROM multiple wip/category
--    SELECT DISTINCT VehicleInventoryItemID 
--    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
  SELECT DISTINCT(VehicleInventoryItemID)
  FROM #WipBase x
  WHERE NOT EXISTS (
    SELECT 1
    FROM #wipbase
    WHERE VehicleInventoryItemID = x.VehicleInventoryItemID 
    AND category = 'AppearanceReconItem')) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
WHERE p.VehicleInventoryItemID IN(
'06c212d1-1b16-496a-8e73-cebe9435ed60',
'a21529bf-0545-4967-913a-d023420e7e04',
'29dff275-b7f0-46e4-af56-d2387933699b',
'5bfbbacf-e501-4171-8070-e6239d033965',
'cd4261ee-9a80-4b9c-9a20-e9676b5d74ba',
'3425ba51-5de3-40b1-a7d3-33de43301923',
'178d3d13-468b-4758-976b-70bf5044281c',
'7220fe9d-7887-4c6a-a79b-ed991eaae53d',
'785bb6e7-59bb-4d2e-a9ee-92b7ea13c5c5')
  



-- these are the ones WHERE depts > pulltopb
-- it's the fucking other apperance shit coming thru -- yep, verified that
-- which IS the answer for these few, but what IS so fucked up IN the grouping query of level2, the week2 notes
SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, 
  p.pulledts, p.pbts, timestampdiff(sql_tsi_hour,p.pulledts, p.pbts),
  m.FromtS AS mFrom, m.ThruTS AS mThru,  timestampdiff(sql_tsi_hour, m.FromTS, m.ThruTS), 
  b.FromTs AS bFrom, b.ThruTS AS bThru, timestampdiff(sql_tsi_hour, b.FromTS, b.ThruTS), 
  a.FromTS AS aFrom, a.ThruTS AS aThru, timestampdiff(sql_tsi_hour, a.FromTS, a.ThruTS), 
  o.FromTS AS oFrom, o.ThrutS AS oThru, timestampdiff(sql_tsi_hour, o.FromTS, o.ThruTS)
FROM #PullToPB p 
INNER JOIN ( -- exclusions FROM multiple wip/category
  SELECT DISTINCT VehicleInventoryItemID 
  FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
WHERE p.VehicleInventoryItemID IN (
'fc29b454-e03d-4c7a-b908-e9e796de6811',
'd9fbfbc7-65f0-4460-8196-9f3adae6a894',
'3d6f9334-dec3-4a3a-9005-5755594d33d8',
'6d35e9ad-73ac-4fae-a860-2044916ad620',
'f6e76de4-fbc8-4484-a86d-5075ecc74c2f',
'90b01ec1-cd72-4fd1-a002-6e5ad67b9c05',
'3686f5e1-bfe0-474b-804c-9e698d2fc49e',
'8bc7edfc-5b5b-4ef8-a06a-58295d4de4d3',
'c020d2d0-b756-4694-b739-05b1f0195772',
'98703876-bf31-4fd9-8ddb-de6c4c2c033c')

-- ok, so i have LEFT out app other FROM dept times

SELECT x.theweek, x.VehicleInventoryItemID, x.reconseq,
  timestampdiff(sql_tsi_hour, pulledts, pbts) AS PullToPb,
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, mThru)
    WHEN 2 THEN -- *m*
      CASE
        -- bm*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)
        -- am*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru)
      END 
    WHEN 3 THEN -- **m
      CASE
        -- *bm
        WHEN substring(ReconSeq, 2, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)
        -- *am
        WHEN substring(ReconSeq, 2, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru)
      END     
  END AS MechHours,
  CASE position('b' IN ReconSeq) -- b**
    WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, bThru)
    WHEN 2 THEN -- *b*
      CASE
        -- mb*
        WHEN substring(ReconSeq, 1, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, bThru)
        -- ab*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, bThru)
      END 
    WHEN 3 THEN -- **b
      CASE
        -- *mb
        WHEN substring(ReconSeq, 2, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, bThru)
        -- *ab
        WHEN substring(ReconSeq, 2, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, bThru)
      END     
  END AS BodyHours,
  CASE position('a' IN ReconSeq) -- a**
    WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, aThru)
    WHEN 2 THEN -- *a*
      CASE
        -- ma*
        WHEN substring(ReconSeq, 1, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, aThru)
        -- ba*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, aThru)
      END 
    WHEN 3 THEN -- **a
      CASE
        -- *ma
        WHEN substring(ReconSeq, 2, 1) = 'm' THEN timestampdiff(sql_tsi_hour, mThru, aThru)
        -- *ba
        WHEN substring(ReconSeq, 2, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, aThru)
      END     
  END /*+ coalesce(timestampdiff(sql_tsi_hour, oFrom, oThru), 0)*/ AS AppHours  -- treat AppOther AS an OtherActivity, the problem IS that IF it 
-- overlaps service, THEN service times are inflated 
-- need to determine the extent AND effect of overlapping 
-- which means i don't have a clear unamibuous sequence
FROM ( -- one row per vehicle
  SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFrom, o.ThrutS AS oThru
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL) x 
  
  
-- ok, here are the others
SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
  m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
  a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFrom, o.ThrutS AS oThru
FROM #PullToPB p 
INNER JOIN ( -- exclusions FROM multiple wip/category
  SELECT DISTINCT VehicleInventoryItemID 
  FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
WHERE p.VehicleInventoryItemID IS NOT NULL
AND o.FromTS IS NOT NULL 

SELECT COUNT(DISTINCT VehicleInventoryItemID) FROM #WipBase -- 484
SELECT COUNT(DISTINCT VehicleInventoryItemID) FROM #WipBase WHERE category = 'AppearanceReconItem' -- 18

-- this excludes the AppOther
SELECT COUNT(*) -- 484
FROM #PullToPB p
INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
  SELECT DISTINCT(VehicleInventoryItemID)
  FROM #WipBase x
  WHERE NOT EXISTS (
    SELECT 1
    FROM #wipbase
    WHERE VehicleInventoryItemID = x.VehicleInventoryItemID 
    AND category = 'AppearanceReconItem')) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  
  SELECT category, COUNT(*)
  FROM #WipBase
  WHERE category <> 'AppearanceReconItem' 
    GROUP BY category 
     
-- OR IN the SELECT, subtract the Other times WHERE relevant      
SELECT x.theweek, x.VehicleInventoryItemID, x.reconseq,
  timestampdiff(sql_tsi_hour, pulledts, pbts) AS PullToPb,
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, mThru) - CASE WHEN oFrom BETWEEN pulledTS AND mFrom THEN timestampdiff(sql_tsi_hour, oFrom, oThru) ELSE 0 END 
    WHEN 2 THEN -- *m*
      CASE
        -- bm*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)  - CASE WHEN oFrom BETWEEN bThru AND mFrom THEN timestampdiff(sql_tsi_hour, oFrom, oThru) ELSE 0 END 
        -- am*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru) - CASE WHEN oFrom BETWEEN aThru AND mFrom THEN timestampdiff(sql_tsi_hour, oFrom, oThru) ELSE 0 END 
      END 
    WHEN 3 THEN -- **m
      CASE
        -- *bm
        WHEN substring(ReconSeq, 2, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)
        -- *am
        WHEN substring(ReconSeq, 2, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru)
      END     
  END AS MechHoursMinus,
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, mThru) 
    WHEN 2 THEN -- *m*
      CASE
        -- bm*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)
        -- am*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru)
      END 
    WHEN 3 THEN -- **m
      CASE
        -- *bm
        WHEN substring(ReconSeq, 2, 1) = 'b' THEN timestampdiff(sql_tsi_hour, bThru, mThru)
        -- *am
        WHEN substring(ReconSeq, 2, 1) = 'a' THEN timestampdiff(sql_tsi_hour, aThru, mThru)
      END     
  END AS MechHours  
FROM ( -- one row per vehicle
  SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFrom, o.ThrutS AS oThru
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL) x 
WHERE oFrom IS NOT NULL  
  