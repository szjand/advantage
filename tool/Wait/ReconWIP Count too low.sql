/*
SELECT v.*, da.*, m.*, w.*,
  (SELECT  
    SUM(
      CASE
--        WHEN FromTS < PulledTS THEN timestampdiff(sql_tsi_minute, pulledts, ThruTS)
        WHEN FromTS < PulledTS THEN (select Utilities.BusinessHoursFromInterval(PulledTS, ThruTS, 'Mechanical') FROM system.iota)
--        ELSE timestampdiff(sql_tsi_minute, Fromts, ThruTS)
        ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Mechanical') FROM system.iota)
      END) AS MechTime
    FROM VehicleInventoryItemStatuses  
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND status = 'MechanicalReconProcess_InProcess' 
    AND ThruTS >= pulledts
    AND ThruTS <= pbts
    GROUP BY VehicleInventoryItemID) AS MechBusHours,
  (SELECT  
    SUM(
      CASE
--        WHEN FromTS < PulledTS THEN timestampdiff(sql_tsi_minute, pulledts, ThruTS)
        WHEN FromTS < PulledTS THEN (select Utilities.BusinessHoursFromInterval(PulledTS, ThruTS, 'Body') FROM system.iota)
--        ELSE timestampdiff(sql_tsi_minute, Fromts, ThruTS)
        ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Body') FROM system.iota)
      END) AS MechTime
    FROM VehicleInventoryItemStatuses  
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND status = 'BodyReconProcess_InProcess' 
    AND ThruTS >= pulledts
    AND ThruTS <= pbts
    GROUP BY VehicleInventoryItemID) AS BodyBusHours,    
  (SELECT  
    SUM(
      CASE
--        WHEN FromTS < PulledTS THEN timestampdiff(sql_tsi_minute, pulledts, ThruTS)
        WHEN FromTS < PulledTS THEN (select Utilities.BusinessHoursFromInterval(PulledTS, ThruTS, 'Detail') FROM system.iota)
--        ELSE timestampdiff(sql_tsi_minute, Fromts, ThruTS)
        ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Detail') FROM system.iota)
      END) AS MechTime
    FROM VehicleInventoryItemStatuses  
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND status = 'AppearanceReconProcess_InProcess' 
    AND ThruTS >= pulledts
    AND ThruTS <= pbts
    GROUP BY VehicleInventoryItemID) AS AppBusHours  
*/
/*   
SELECT v.*, da.*   
INTO #jon
FROM (
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17      
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate >= CurDate() - 118
  AND TheDate <= CurDate()) da  
LEFT JOIN (
  SELECT *
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS PulledTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS pbTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
  WHERE viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
  AND pb.pbts IS NOT NULL -- made it ALL the way to the lot   
  AND NOT EXISTS ( -- only vehicles with a single pull
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)
  AND NOT EXISTS ( -- only vehicles with a single pb
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPB'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)) v ON da.TheDate = CAST(v.pbTS AS sql_date) 
--LEFT JOIN (
--  SELECT VehicleInventoryItemID,
--  SUM((select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)) AS MoveBusHours 
--  FROM VehiclesToMove
--  WHERE CAST(FromTS AS sql_date) >= curdate() - 118
--  GROUP BY VehicleInventoryItemID) m ON v.VehicleInventoryItemID = m.VehicleInventoryItemID  
--LEFT JOIN (
--  SELECT VehicleInventoryItemID AS wtfid,
--  SUM((select Utilities.BusinessHoursFromInterval(CreatedTS, ResolvedTS, 'Service') FROM system.iota)) AS WTFBusHours 
--  FROM ViiWTF
--  WHERE CAST(CreatedTS AS sql_date) >= curdate() - 118
--  GROUP BY VehicleInventoryItemID ) w ON v.VehicleInventoryItemID = w.wtfid  
*/

/*
SELECT  
    SUM(
      CASE
        WHEN (FromTS IS NOT NULL AND FromTS = ThruTS) THEN 1
        ELSE
        CASE 
          WHEN FromTS < PulledTS THEN (select Utilities.BusinessHoursFromInterval(PulledTS, ThruTS, 'Mechanical') FROM system.iota)
          ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Mechanical') FROM system.iota)
        END 
      END) AS MechTime
    FROM VehicleInventoryItemStatuses  
    WHERE VehicleInventoryItemID = '3dc8c35a-33a3-430f-928a-9856b5ac5879'
    AND status = 'MechanicalReconProcess_InProcess' 
    AND ThruTS >= pulledts
    AND ThruTS <= pbts
    GROUP BY VehicleInventoryItemID
*/    
    
/*
SELECT description, startts, completets, s.*
FROM AuthorizedReconItems a
INNER JOIN VehicleReconItems v ON a.VehicleReconItemID = v.VehicleReconItemID 
LEFT JOIN VehicleInventoryItemStatuses s ON v.VehicleInventoryItemID = s.VehicleInventoryItemID 
  AND s.status = 'MechanicalReconProcess_InProcess'
WHERE v.VehicleInventoryItemID = '3dc8c35a-33a3-430f-928a-9856b5ac5879'
AND v.typ LIKE 'Mechan%'   
*/

/*
-- wtf, how can there be empty records i #jon
-- of course there can be
-- base TABLE IS day
SELECT count(*) --'k', j.* 
FROM #jon j
WHERE theweek = 1
AND stocknumber IS NULL 
*/
  
  
/*
-- vehiclehistory recon query:
  SELECT distinct vri.VehicleReconItemID, vri.Sequence, 
    CASE 
      WHEN vri.Typ IN (
        SELECT Typ
        FROM TypCategories 
        WHERE Category = 'BodyReconItem') THEN 'Body'
      WHEN vri.Typ IN (
        SELECT Typ
        FROM TypCategories 
        WHERE Category = 'MechanicalReconItem') THEN 'Mechanical'
      WHEN vri.Typ IN (
        SELECT Typ
        FROM TypCategories tc1 
        WHERE tc1.Category = 'AppearanceReconItem'
        OR tc1.Typ = 'PartyCollection_AppearanceReconItem') THEN 'Appearance'
    END AS Typ,
    CAST (
      CASE 
        WHEN (select Category from TypCategories where typ = td.typ) in ('AppearanceReconItem', 'PartyCollection') THEN vri.Description
        ELSE trim(td.description) collate ADS_DEFAULT_CI + ': ' + vri.Description 
    END as sql_char) AS Description,
    vri.TotalPartsAmount, vri.LaborAmount, 
    (Select top 1 sd.Description from StatusDescriptions sd WHERE sd.Status = ari.Status AND sd.ThruTS IS null) AS Status,
    ari.Status AS StatusStatus,
    '' AS PartsStatus,
    now() AS EtaTS 
  FROM VehicleReconItems vri
  INNER JOIN AuthorizedReconItems ari ON ari.VehicleReconItemID = vri.VehicleReconItemID
    AND ari.Status = 'AuthorizedReconItem_Complete'
  LEFT JOIN TypDescriptions td ON td.Typ = vri.typ
  WHERE vri.VehicleInventoryItemID = @VehicleInventoryItemID;
*/ 
-- SELECT * FROM VehicleReconItems 
-- SELECT typ, COUNT(*) FROM VehicleReconItems GROUP BY typ
-- SELECT LEFT(typ, 4), COUNT(*) FROM VehicleReconItems GROUP BY LEFT(typ, 4)
-- SELECT * FROM ReconAuthorizations 
-- SELECT * FROM AuthorizedReconItems 
-- multiple mech '74d736b2-f2fe-42b6-aa88-8b5f64c98f74'
-- but ALL done at the same time
-- find one with multiple done at different times
--   fdf96d70-a6ae-4d1f-8a67-e0c806e5037c
--   fde53ea0-aa94-41b5-8753-b0d3bf1e1073
--   ff48d766-a350-4cdd-bc41-b21b699bac54
--   fb15a2c6-bb57-415b-b167-697e45284c47
--   f22c1dca-6255-49da-b995-4eca8832a005

SELECT TheWeek,
  MIN(TheDate) AS "Week First Day", MAX(TheDate) AS "Week Last Day",
  COUNT(j.VehicleInventoryItemID) AS "to PB (Count)",
  SUM(timestampdiff(sql_tsi_day, cast(pulledts AS sql_date), cast(pbts AS sql_date)))/count(j.VehicleInventoryItemID) AS "Avg Days Pull->PB per Vehicle",
  SUM((select Utilities.BusinessHoursFromInterval(PulledTS, PBTS, 'Service') FROM system.iota))/count(j.VehicleInventoryItemID) AS "Avg Bus Hours Pull->PB per Vehicle",
  SUM(CASE WHEN HasMech IS NOT NULL THEN 1 ELSE 0 END) AS MechCount,
  SUM(CASE WHEN HasBody IS NOT NULL THEN 1 ELSE 0 END) AS BodyCount,
  SUM(CASE WHEN HasApp IS NOT NULL THEN 1 ELSE 0 END) AS AppCount
FROM #jon j
GROUP BY TheWeek  

/*
SELECT COUNT(*) FROM VehicleReconItems -- 51632  
SELECT COUNT(*) FROM VehicleReconItems WHERE typ <> 'MechanicalReconItem_Inspection' --43106
SELECT COUNT(*) FROM VehicleReconItems WHERE typ <> 'MechanicalReconItem_Inspection' -- 18025
  AND typ in (SELECT Typ FROM TypCategories WHERE Category = 'MechanicalReconItem')
SELECT COUNT(*) FROM AuthorizedReconItems  -- 47325
SELECT COUNT(*) FROM AuthorizedReconItems WHERE status  = 'AuthorizedReconItem_Complete' -- 42932
*/
/*
-- looking for vehicles with multiple mech wip
SELECT ri.VehicleInventoryItemID, ri.typ, ri.description, ar.status, ar.startts, ar.completets
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
-- WHERE ri.VehicleInventoryItemID = '74d736b2-f2fe-42b6-aa88-8b5f64c98f74'
WHERE ri.typ <> 'MechanicalReconItem_Inspection'
AND ar.status  = 'AuthorizedReconItem_Complete'
AND ri.typ in (  
  SELECT Typ 
  FROM TypCategories
  WHERE Category = 'MechanicalReconItem')
AND ri.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID 
  FROM VehicleReconItems v
  INNER JOIN AuthorizedReconItems a ON v.VehicleReconItemID = a.VehicleReconItemID 
  WHERE v.typ <> 'MechanicalReconItem_Inspection'
  AND a.status  = 'AuthorizedReconItem_Complete'  
  GROUP BY VehicleInventoryItemID
  HAVING COUNT(*) > 4)
AND startts IS NOT NULL   
ORDER BY VehicleInventoryItemID, startts  
*/    
/*
-- AuthorizedReconItems before August 2010 have NULL start/complete    
SELECT a.*, (SELECT fromts FROM ReconAuthorizations WHERE ReconAuthorizationID = a.ReconAuthorizationID)
FROM AuthorizedReconItems a
WHERE status  = 'AuthorizedReconItem_Complete'
AND startts IS null  
ORDER BY (SELECT fromts FROM ReconAuthorizations WHERE ReconAuthorizationID = a.ReconAuthorizationID) DESC
*/

SELECT left(stocknumber, 12) AS stock, 
  CASE
    WHEN HasMech.VehicleInventoryItemID IS NOT NULL THEN 'X'
  end AS X
FROM #jon j
LEFT JOIN (
  SELECT VehicleInventoryItemID
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  WHERE ri.typ <> 'MechanicalReconItem_Inspection'
  AND ar.status  = 'AuthorizedReconItem_Complete'
  AND ri.typ in (  
    SELECT Typ 
    FROM TypCategories
    WHERE Category = 'MechanicalReconItem')
  GROUP BY VehicleInventoryItemID) HasMech ON j.VehicleInventoryItemID = HasMech.VehicleInventoryItemID 
WHERE j.stocknumber IS NOT NULL
AND TheWeek = 1