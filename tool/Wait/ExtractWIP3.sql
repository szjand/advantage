/*
-- raw AuthorizedReconItems data
SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category 
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
LEFT JOIN TypCategories t ON ri.typ = t.typ
WHERE ar.status  = 'AuthorizedReconItem_Complete'
AND ar.startTS IS NOT NULL 

-- a vehicle with multiple lines IN a dept 
-- often ALL lines are started AND completed at the same time
-- grouping BY VehicleInventoryItemID, ar.startts, ar.completets, category
-- collapses multiple line items with the same startts AND completets INTO one row
SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category 
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
LEFT JOIN TypCategories t ON ri.typ = t.typ
WHERE ar.status  = 'AuthorizedReconItem_Complete'
AND ar.startTS IS NOT NULL 
AND VehicleInventoryItemID = '192fa224-dfb3-494e-9c72-547dd9f0c8d2'
GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category  

-- #jon IS the base interval TABLE, IN this CASE pull -> pb
-- this gives a row for every time a vehicle IS IN WIP IN any dept
SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category
FROM #jon j 
LEFT JOIN (
  SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category 
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
  AND ar.startTS IS NOT NULL 
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))

-- this gives us one row for each vehicle/dept     
SELECT VehicleInventoryItemID,
      SUM(
        CASE 
          WHEN category = 'MechanicalReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'Service') FROM system.iota)
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota)
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'Service') FROM system.iota)
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'Service') FROM system.iota)
            END 
        END) AS MechBusHours,
      SUM(
        CASE 
          WHEN category = 'BodyReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'BodyShop') FROM system.iota)
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'BodyShop') FROM system.iota)
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'BodyShop') FROM system.iota)
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'BodyShop') FROM system.iota)
            END 
        END) AS BodyBusHours,  
      SUM(
        CASE 
          WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'Detail') FROM system.iota)
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Detail') FROM system.iota)
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'Detail') FROM system.iota)
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'Detail') FROM system.iota)
            END 
        END) AS AppBusHours       
    FROM (
      SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category
      FROM #jon j 
      LEFT JOIN (
        SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category 
        FROM VehicleReconItems ri
        INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
        LEFT JOIN TypCategories t ON ri.typ = t.typ
        WHERE ar.status  = 'AuthorizedReconItem_Complete'
        AND ar.startTS IS NOT NULL 
        GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
          AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
    GROUP BY VehicleInventoryItemID, category
        
*/
-- CONVERT this INTO one row for each vehicle that has WIP IN any dept
-- with a COLUMN for business hours IN wip for each dept
-- what to DO about 0 time WIP: IF startTS = completeTS THEN make it 1 hour
SELECT VehicleInventoryItemID, MAX(MechBusHours) AS MechBusHours, MAX(BodyBusHours) AS BodyBusHours, MAX(AppBusHours) AS AppBusHours,
  SUM(CASE WHEN MechBusHours > 0 THEN 1 ELSE 0 END) AS MechCount,
  SUM(CASE WHEN BodyBusHours > 0 THEN 1 ELSE 0 END) AS BodyCount,
  SUM(CASE WHEN AppBusHours > 0 THEN 1 ELSE 0 END) AS AppCount
INTO #ReconWip
FROM (
  SELECT VehicleInventoryItemID,
    coalesce(
      SUM(
        CASE 
          WHEN category = 'MechanicalReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'Service') FROM system.iota)
                END 
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota)
                END  
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'Service') FROM system.iota)
                END
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'Service') FROM system.iota)
                END 
            END 
        END), 0) AS MechBusHours,
    coalesce(
      SUM(
        CASE 
          WHEN category = 'BodyReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'BodyShop') FROM system.iota)
                END 
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'BodyShop') FROM system.iota)
                END  
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'BodyShop') FROM system.iota)
                END
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'BodyShop') FROM system.iota)
                END 
            END 
        END), 0) AS BodyBusHours,
    coalesce(
      SUM(
        CASE 
          WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'Detail') FROM system.iota)
                END 
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Detail') FROM system.iota)
                END  
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'Detail') FROM system.iota)
                END
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'Detail') FROM system.iota)
                END 
            END 
        END), 0) AS AppBusHours 
  FROM (
    SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category
    FROM #jon j 
    LEFT JOIN (
      SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category 
      FROM VehicleReconItems ri
      INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
      LEFT JOIN TypCategories t ON ri.typ = t.typ
      WHERE ar.status  = 'AuthorizedReconItem_Complete'
      AND ar.startTS IS NOT NULL 
      GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
        AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
  GROUP BY VehicleInventoryItemID, category) x    
GROUP BY VehicleInventoryItemID  

/*
-- DROP TABLE #ReconWIP
-- SELECT * FROM #ReconWIP
-- SELECT * FROM #jon
SELECT TheWeek,
  MIN(TheDate) AS "Week First Day", MAX(TheDate) AS "Week Last Day",
  COUNT(j.VehicleInventoryItemID) AS "to PB (Count)",
  SUM(timestampdiff(sql_tsi_day, cast(pulledTS AS sql_date), cast(pbTS AS sql_date)))/COUNT(j.VehicleInventoryItemID) AS "Avg Days Pull->PB per Vehicle",
  SUM((SELECT Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota))/COUNT(j.VehicleInventoryItemID) AS "Avg Bus Hours Pull->PB per Vehicle",
  SUM(MechCount) AS MechCount,
  CASE 
    WHEN SUM(MechCount) <> 0 THEN SUM(MechBusHours)/SUM(MechCount) 
    ELSE 0
  END  AS "Avg Mech WIP BusHours per Vehicle",
  SUM(BodyCount) AS BodyCount,
  CASE
    WHEN SUM(BodyCount) <> 0 THEN SUM(BodyBusHours)/SUM(BodyCount) 
    ELSE 0
  END AS "Avg Body WIP BusHours per Vehicle",
  SUM(AppCount) AS AppCount,
  CASE
    WHEN SUM(AppCount) <> 0 THEN SUM(AppBusHours)/SUM(AppCount) 
    ELSE 0
  END AS "Avg App WIP BusHours per Vehicle"
--  SUM(MoveBusHours)/COUNT(j.VehicleInventoryItemID) AS "Avg BusHours on Move List per Vehicle",
--  SUM(WTFBusHours)/COUNT(j.VehicleInventoryItemID) AS "Avg BusHours as a WTF per Vehicle",
FROM #jon j
LEFT JOIN #ReconWip r ON j.VehicleInventoryItemID = r.VehicleInventoryItemID 
GROUP BY TheWeek  
*/
    