SELECT TheWeek AS "The Week",
  MIN(TheDate) AS "Week First Day", MAX(TheDate) AS "Week Last Day",
  COUNT(j.VehicleInventoryItemID) AS "to PB (Count)",
  SUM(timestampdiff(sql_tsi_day, cast(pulledTS AS sql_date), cast(pbTS AS sql_date)))/COUNT(j.VehicleInventoryItemID) AS "Avg Days per Vehicle",
  SUM((SELECT Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota))/COUNT(j.VehicleInventoryItemID) AS "Avg Bus Hours per Vehicle",
  SUM(MechCount) AS MechCount,
  CASE
    WHEN SUM(MechCount) <> 0 THEN SUM(p.Hours)/SUM(MechCount)
    ELSE 0
  END AS "Avg Parts Hold per Vehicle",
  CASE
    WHEN SUM(MechCount) <> 0 THEN SUM(m.Mech)/SUM(MechCount)
    ELSE 0 
  END AS "Avg Move Wait per Vehicle",
  CASE
    WHEN SUM(MechCount) <> 0 THEN SUM(w.MechWaitBus)/SUM(MechCount)
    ELSE 0 
  END AS "Avg Capacity Wait per Vehicle",  
  CASE 
    WHEN SUM(MechCount) <> 0 THEN SUM(MechBusHours)/SUM(MechCount) 
    ELSE 0
  END  AS "Avg WIP per Vehicle",
  SUM(BodyCount) AS BodyCount,
  CASE
    WHEN SUM(BodyCount) <> 0 THEN SUM(m.Body)/SUM(BodyCount)
    ELSE 0 
  END AS "Avg Move Wait per Vehicle", 
  CASE
    WHEN SUM(BodyCount) <> 0 THEN SUM(w.BodyWaitBus)/SUM(BodyCount)
    ELSE 0 
  END AS "Avg Capacity Wait per Vehicle",   
  CASE
    WHEN SUM(BodyCount) <> 0 THEN SUM(BodyBusHours)/SUM(BodyCount) 
    ELSE 0
  END AS "Avg WIP per Vehicle",
  SUM(AppCount) AS AppCount,
  CASE
    WHEN SUM(AppCount) <> 0 THEN SUM(m.App)/SUM(AppCount)
    ELSE 0 
  END AS "Avg Move Wait per Vehicle",  
  CASE
    WHEN SUM(AppCount) <> 0 THEN SUM(w.AppWaitBus)/SUM(AppCount)
    ELSE 0 
  END AS "Avg Capacity Wait per Vehicle",  
  CASE
    WHEN SUM(AppCount) <> 0 THEN SUM(AppBusHours)/SUM(AppCount) 
    ELSE 0
  END AS "Avg WIP per Vehicle"  
FROM #jon j
LEFT JOIN #ReconWip r ON j.VehicleInventoryItemID = r.VehicleInventoryItemID
LEFT JOIN #partsholdbushours p ON j.VehicleInventoryItemID = p.id
LEFT JOIN #move m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID 
LEFT JOIN #ReconWait w ON j.VehicleInventoryItemID = w.VehicleInventoryItemID 
--WHERE j.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'
GROUP BY j.TheWeek
