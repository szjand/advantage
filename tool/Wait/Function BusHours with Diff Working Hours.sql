alter FUNCTION Utilities.BusinessHoursFromInterval 
   ( 
   @ts1 Timestamp,
   @ts2 Timestamp,
   @Dept string
   )
   RETURNS integer
DESCRIPTION 'Returns the business hours FROM a day/hour interval'
BEGIN
                                               
/*
************* 9/23/11 this must be modified to accomodate changing WorkingHours record ********************
use the WorkingHours record that applies to the interval being requested, what were
the business hours during this interval

WHERE ts1 between wh.FromTS AND coalesce(wh.ThruTS, CAST('12/31'3030 00:00:00' AS sql_timestamp))

also need to include the notion of holidays, which should be IN the dds.day table
***********************************************************************************************************
OPEN: start of business day
CLOSE: END of business day 
            Opens                         Closes         Test                                                  Output 
              |----------------------------|
1.    ts1|-------------|ts2                            ts1 < Opens AND ts2 > Opens AND ts2 <= Closes          ts2 - Opens
2.    ts1|-------------------------------------|ts2    ts1 < Opens AND ts2 >= Closes                          Closes- ts1           
3          ts1|--------------|ts2                      ts1 >= Opens AND ts1 < Closes AND ts2 <= Closes        ts2 - ts1
4.         ts1|--------------------------------|ts2    ts1 >= Opens AND Opens < Closes AND ts2 >= Closes      Closes - ts1                                                       


SELECT utilities.BusinessHoursFromInterval(cast('05/25/2011 08:40:42' AS sql_timestamp), cast('05/27/2011 10:44:52' as sql_timestamp), 'Detail') FROM system.iota
-- test sunday service, closed before 04/01/2011, 3/27 sunday
SELECT utilities.BusinessHoursFromInterval(cast('03/27/2011 08:00:00' AS sql_timestamp), cast('03/27/2011 17:00:00' as sql_timestamp), 'Service') FROM system.iota

*/  

RETURN (
  SELECT 
    CASE timestampdiff(sql_tsi_day, CAST(@ts1 AS sql_date), CAST(@ts2 AS sql_date))
      WHEN 0 THEN -- interval covers single day 
      coalesce( -- for closed working days, eg BodyShop Sunday, return 0
        MAX(
          CASE
            WHEN hour(@ts1) < hour(wh.opens) --1
              AND hour(@ts2) > hour(wh.opens) 
              AND hour(@ts2) <= iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN hour(@ts2) - hour(wh.opens)  
            WHEN hour(@ts1) < hour(wh.opens) --2
              AND hour(@ts2) > iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1)  
            WHEN hour(@ts1) >= hour(wh.opens) --3
              AND hour(@ts1) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) 
              AND hour(@ts2) <= iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN hour(@ts2) - hour(@ts1)
            WHEN hour(@ts1) > hour(wh.opens) -- 4
              AND hour(wh.opens) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes))  
              AND hour(@ts2) >= iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1)
          END), 0) 
      WHEN 1 THEN -- interval covers 2 days 
      coalesce(-- for closed working days, eg BodyShop Sunday, return 0
        MAX(
          CASE  -- day 1
            WHEN hour(@ts1) < hour(wh.Opens) THEN wh.TotalHours --2
            ELSE iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1)
          END 
        +
          CASE -- day 2
            WHEN hour(@ts2) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN hour(@ts2) - hour(wh.Opens) --3
            ELSE wh.TotalHours
          END), 0) 
      ELSE -- interval convers more than 2 days  
        coalesce(-- for closed working days, eg BodyShop Sunday, return 0
          SUM(CASE WHEN d.DayOfWeek IN (2,3,4,5,6) THEN wh.TotalHours ELSE 0 END) -- total working hours for each day
          +
          SUM(CASE WHEN d.DayOfWeek = 1 THEN wh.TotalHours ELSE 0 END) 
          +
          SUM(CASE WHEN d.DayOfWeek = 7 THEN wh.TotalHours ELSE 0 END) 
          -
          MAX( -- adjust for first day AND last day of interval
            CASE           
              WHEN (DayOfYear(@ts1) = d.DayOfYear and Year(@ts1) = d.TheYear) THEN -- matches first day of interval to the correct day
                CASE 
                  WHEN hour(@ts1) < hour(wh.Opens) THEN 0 -- nothing to subtract, interval covers entire day 1 -- 24 - wh.TotalHours --2
                  ELSE wh.TotalHours - (iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1))  
                END
              ELSE 0
            END 
            +
            CASE 
              WHEN (DayOfYear(@ts2) = d.DayOfYear and Year(@ts2) = d.TheYear) THEN -- matches last day of interval to the correct day
                CASE -- day 2
                  WHEN hour(@ts2) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN wh.TotalHours - ((hour(@ts2) - hour(wh.Opens))) --3
                  ELSE 0 
                END
              ELSE 0
            END), 0)
    END AS BusHours
  FROM (-- any query that generates 2 timestamps: @ts1 < @ts2
    SELECT  @ts1,  @ts2
    FROM system.iota) ts,
    dds.Day d,
    WorkingHours wh
  WHERE wh.DayOfWeek = d.DayOfWeek 
  AND d.TheDate >= CAST(@ts1 AS sql_date)  
  AND d.TheDate <= CAST(@ts2 AS sql_date)
  AND
    CASE
      WHEN @Dept = 'Service' THEN wh.Dept = 'Service'
      WHEN @Dept = 'BodyShop' THEN wh.Dept = 'BodyShop'
      WHEN @Dept = 'Detail' THEN wh.Dept = 'Detail'
      ELSE 1 = 1
    END);









END;
