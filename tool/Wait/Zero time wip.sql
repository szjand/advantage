-- complete AuthorizedReconItems last 120 days WHERE start = complete
SELECT a.StartTS, a.CompleteTS, v.*
FROM AuthorizedReconItems a
INNER JOIN VehicleReconItems v ON a.VehicleReconItemID = v.VehicleReconItemID 
WHERE a.startts is NOT NULL 
AND v.typ <> 'MechanicalReconItem_Inspection'
AND a.startts = a.completets
AND a.status = 'AuthorizedReconItem_Complete'
AND CAST(a.startts AS sql_date) > curdate() - 120
AND v.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM VehicleInventoryItems
  WHERE LEFT(stocknumber, 1) NOT IN ('H','C'))

-- complete AuthorizedReconItems last 120 days WHERE start = complete
-- grouped BY dept
SELECT LEFT(v.typ, 4), COUNT(*)
FROM AuthorizedReconItems a
INNER JOIN VehicleReconItems v ON a.VehicleReconItemID = v.VehicleReconItemID 
WHERE a.startts is NOT NULL 
AND v.typ <> 'MechanicalReconItem_Inspection'
AND a.startts = a.completets
AND a.status = 'AuthorizedReconItem_Complete'
AND CAST(a.startts AS sql_date) > curdate() - 120
AND v.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM VehicleInventoryItems
  WHERE LEFT(stocknumber, 1) NOT IN ('H','C'))
GROUP BY LEFT(v.typ, 4)


-- total number of line items BY dept
SELECT LEFT(v.typ, 4), COUNT(*)
FROM AuthorizedReconItems a
INNER JOIN VehicleReconItems v ON a.VehicleReconItemID = v.VehicleReconItemID 
WHERE a.startts is NOT NULL 
AND v.typ <> 'MechanicalReconItem_Inspection'
AND a.status = 'AuthorizedReconItem_Complete'
AND CAST(a.startts AS sql_date) > curdate() - 120
AND v.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM VehicleInventoryItems
  WHERE LEFT(stocknumber, 1) NOT IN ('H','C'))
GROUP BY LEFT(v.typ, 4)


Mech:  374 of  835
Body:  276 of 1301
App:    31 of  660

-- the mech line items
SELECT a.StartTS, a.CompleteTS, v.*
FROM AuthorizedReconItems a
INNER JOIN VehicleReconItems v ON a.VehicleReconItemID = v.VehicleReconItemID 
WHERE a.startts is NOT NULL 
AND v.typ <> 'MechanicalReconItem_Inspection'
AND a.startts = a.completets
AND a.status = 'AuthorizedReconItem_Complete'
AND CAST(a.startts AS sql_date) > curdate() - 120
AND LEFT(v.typ,4) = 'Mech'
AND v.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM VehicleInventoryItems
  WHERE LEFT(stocknumber, 1) NOT IN ('H','C'))
ORDER BY startts DESC   

-- mech grouped BY vri.typ
SELECT v.typ, COUNT(*)
FROM AuthorizedReconItems a
INNER JOIN VehicleReconItems v ON a.VehicleReconItemID = v.VehicleReconItemID 
WHERE a.startts is NOT NULL 
AND v.typ <> 'MechanicalReconItem_Inspection'
AND a.startts = a.completets
AND a.status = 'AuthorizedReconItem_Complete'
AND CAST(a.startts AS sql_date) > curdate() - 120
AND LEFT(v.typ,4) = 'Mech'
AND v.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM VehicleInventoryItems
  WHERE LEFT(stocknumber, 1) NOT IN ('H','C'))
GROUP BY v.typ  
 
