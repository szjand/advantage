/*
ok, this generates: #pulltopb
                    #wipbase
                    #allwip
                    #reconseq
                    #otheractivities
                    #xx
what to DO with them?    

DROP TABLE #pulltopb;
DROP TABLE #wipbase;
DROP TABLE #reconseq;
DROP TABLE #otheractivities;
DROP TABLE #allwip;
DROP TABLE #xx;
                
*/
DECLARE @Cur CURSOR AS 
  SELECT * 
  FROM #WIPBase
  WHERE VehicleInventoryItemID = @CurGroup.VehicleInventoryItemID 
  AND category <> 'AppearanceReconItem'
  ORDER BY  FromTS;   

DECLARE @CurGroup CURSOR AS   
  SELECT VehicleInventoryItemID 
  FROM #WIPBase
  GROUP BY VehicleInventoryItemID;   

DECLARE @i integer;
DECLARE @location string;
-- ry1: 'B6183892-C79D-4489-A58C-B526DF948B06'
-- ry2: '4CD8E72C-DC39-4544-B303-954C99176671'
-- ry3: '1B6768C6-03B0-4195-BA3B-767C0CAC40A6'
@Location = '4CD8E72C-DC39-4544-B303-954C99176671';


-- DROP TABLE #PullToPB
-- SELECT * FROM #PullToPB
-- 1 row for every day of interval(21 weeks)
-- 1 row for every vehicle that was pulled and became pb during the interval (pb within interval)
-- rydells only
-- only vehicles with a single pull, AND vehicles that became pb only once
SELECT  v.VehicleInventoryItemID, pulledTS, pbTS, da.*
INTO #PullToPB 
FROM (
-- rolling weeks
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17 
      WHEN TheDate >= CurDate() - 125 AND TheDate <= CurDate() - 119 THEN 18 
      WHEN TheDate >= CurDate() - 132 AND TheDate <= CurDate() - 120 THEN 19 
      WHEN TheDate >= CurDate() - 139 AND TheDate <= CurDate() - 127 THEN 20 
      WHEN TheDate >= CurDate() - 146 AND TheDate <= CurDate() - 134 THEN 21 
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate >= CurDate() - 146
  AND TheDate <= CurDate()) da  
-- isoweek -- this IS NOT good enough
-- need to account for year change 
/*
  SELECT *
  FROM dds.day d 
  WHERE theyear = 2011
  AND isoweek BETWEEN isoweek(curdate()) - 21 AND isoweek(curdate())) da
*/   
LEFT JOIN ( -- Pull -> PB
  SELECT *
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS PulledTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS pbTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
  WHERE viix.LocationID = @Location  
  AND pb.pbts IS NOT NULL -- made it ALL the way to the lot   
  AND NOT EXISTS ( -- only vehicles with a single pull
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)
  AND NOT EXISTS ( -- only vehicles with a single pb
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPB'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)) v ON da.TheDate = CAST(v.pbTS AS sql_date);
    

/*

*/



-- right now thinking separate wip FROM move/parts/wtf, those 3 can ALL be multiple, no problem
-- the problem with multiple wip IS determining the sequence
-- so 1 TABLE of WIP times
-- AND 1 TABLE of other activity times

-- so 1 TABLE of WIP times
-- this IS WHERE i need to determine whether to consolidate OR eliminate dups
-- 1 row for each vehicle/category/FromTS
-- 9/25/11 truncate FromTS to PullTS, ThruTS to pbTS
-- DROP TABLE #AllWip
-- SELECT * FROM #AllWip
SELECT j.VehicleInventoryItemID, MAX(j.PulledTS) AS PulledTS, MAX(pbTS) AS pbTs, 
  CASE -- truncate StartTS to PulledTS
    WHEN b.startTS < j.PulledTS THEN j.PulledTS
    ELSE b.startTS
  END AS FromTS,
  MAX(
    CASE -- truncate completeTS to pbTS
      WHEN b.CompleteTS > j.pbTS THEN j.pbTS
      ELSE b.CompleteTS
    END) AS ThruTS,  
  b.category
INTO #AllWip  
FROM #PullToPB j
LEFT JOIN ( -- ALL recon WORK performed during pull->pb interval
  SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
  AND ar.startTS IS NOT NULL 
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completeTS, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))
WHERE b.VehicleInventoryItemID IS NOT null    
GROUP BY j.VehicleInventoryItemID, category, FromTS;



-- #AllWip with no multiples
-- unioned #AllWip with consolidated multiples
-- 1 row per vehicle/category
-- DROP TABLE #WipBase
-- 9/22 rework #WipBase to use temp TABLE approach to eliminate multiples that can NOT be consolidated

-- ALL multiples with those that have matching ThruTS consolidated 
-- DROP TABLE #xx
SELECT VehicleInventoryItemID, category, ThruTS, MIN(FromTS) AS FromTS
INTO #xx
FROM #AllWip 
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM #AllWIP
  GROUP BY VehicleInventoryItemID, category, ThruTS
  HAVING COUNT(*) > 1)
GROUP BY VehicleInventoryItemID, category, ThruTS;  

-- DROP TABLE #WipBase
SELECT VehicleInventoryItemID, category, FromTS, ThruTS, 0 AS seq
INTO #WIPBase
FROM #AllWIP
WHERE VehicleInventoryItemID NOT IN ( -- eliminates ALL multiples
  SELECT VehicleInventoryItemID
  FROM #AllWIP
  GROUP BY VehicleInventoryItemID, category
  HAVING COUNT(*) > 1)
UNION
SELECT VehicleInventoryItemID, category, FromTS, ThruTS, 0
FROM #xx -- ALL multiples with those that have matching ThruTS consolidated 
WHERE VehicleInventoryItemID NOT IN ( -- AND eliminate those that were NOT consolidated
  SELECT VehicleInventoryItemID
  FROM #xx
  GROUP BY VehicleInventoryItemID, category
  HAVING COUNT(*) > 1);

@i = 1;
OPEN @CurGroup; // each VehicleInventoryItemID FROM #wipbase
TRY 
  WHILE FETCH @CurGroup DO 
    @i = 1;
    OPEN @Cur;
    TRY 
      WHILE FETCH @Cur DO 
        UPDATE #WIPBase -- assign a seq integer
        SET seq = @i
        WHERE VehicleInventoryItemID = @Cur.VehicleInventoryItemID AND category = @Cur.category AND FromTS = @Cur.FromTS;
        @i = @i + 1;
      END WHILE;
    FINALLY
      CLOSE @Cur;
    END;
  END WHILE; 
  
FINALLY
  CLOSE @CurGroup;
END;     


-- generate #ReconSeq
-- since there are no multiples LEFT
-- maximum sequence will be 4 (4 categories)
-- DROP TABLE #ReconSeq
SELECT VehicleInventoryItemID,  
  trim(
    coalesce(
      MAX((
        SELECT 
          CASE category
--            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END 
        FROM #WipBase WHERE seq = 1 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),''))
  + 
  trim(
    coalesce(
      MAX((
        SELECT 
          CASE category
--            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END         
        FROM #WipBase WHERE seq = 2 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),''))
  + 
  trim(
    coalesce(
      MAX((
        SELECT 
          CASE category
--            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END          
        FROM #WipBase WHERE seq = 3 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),'')) AS ReconSeq
--  + 
--  trim(
--    coalesce(
--      MAX((
--      SELECT 
--          CASE category
--            WHEN 'AppearanceReconItem' THEN 'o' 
--            WHEN 'BodyReconItem' THEN 'b'
--            WHEN 'MechanicalReconItem' THEN 'm'
--            WHEN 'PartyCollection' THEN 'a'
--          END        
--      FROM #WipBase WHERE seq = 4 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),'')) AS ReconSeq
INTO #ReconSeq      
FROM #WipBase w
GROUP BY VehicleInventoryItemID;
-- DROP TABLE #ReconSeq


-- #OtherActivites
-- 1 row/vehicle/activity/fromTS :: each instance of parts/move/wtf
-- 9/25: iif(FromTS < PulledTS, pulledTS, FromtS)  iff(ThruTS > pbTS, pbTS, ThruTS)
-- DROP TABLE #OtherActivities
SELECT j1.VehicleInventoryItemID, 
  CASE
    WHEN m.FromTS < PulledTS THEN PulledTS
    ELSE m.FromTS
  END AS FromTS,
  CASE
    WHEN m.ThruTS > pbTS THEN pbTS
    ELSE m.ThruTS
  END AS ThruTS,
  'Move' AS Activity
INTO #OtherActivities
FROM #PullToPB j1
LEFT JOIN VehiclesToMove m ON j1.VehicleInventoryItemID = m.VehicleInventoryItemID 
  AND (m.FromTS < j1.pbTS AND (m.ThruTS > j1.pulledTS OR m.FromTS >= j1.pulledTS)) -- move requested ON OR before PB AND ((move completed after Pull) OR (move requested on or after Pull))
WHERE m.VehicleInventoryItemID IS NOT NULL -- don't care about a vehicle NOT ON move list during interval
UNION -- each parts ORDER for a vehicle during the interval - a parts ORDER IS for a VehicleReconItem
SELECT j1.VehicleInventoryItemID, 
  CASE
    WHEN p.orderedTS < PulledTS THEN PulledTS
    ELSE p.orderedTS 
  END,
  CASE
    WHEN p.receivedTS > pbTS THEN pbTS
    ELSE p.receivedTS
  END, 
  'Parts'
FROM #PullToPB j1
LEFT JOIN (
  SELECT po.*, (SELECT VehicleInventoryItemID FROM VehicleReconItems WHERE VehicleReconItemID = po.VehicleReconItemID) AS VehicleInventoryItemID 
  FROM partsorders po
  WHERE po.CancelledTS IS NULL) p ON j1.VehicleInventoryItemID = p.VehicleInventoryItemID -- exclude Cancelled Orders
    AND (p.OrderedTS < j1.pbTS AND (p.ReceivedTS > j1.pulledTS OR p.OrderedTS >= j1.pulledTS)) -- ordered before PB AND ((received after pulled) OR (ordered ON OR after Pull))
WHERE p.VehicleInventoryItemID IS NOT NULL 
UNION -- each time a vehicle IS a WTF during the interval
SELECT j1.VehicleInventoryItemID, 
  CASE
    WHEN w.CreatedTS < PulledTS THEN PulledTS
    ELSE w.CreatedTS
  END,
  CASE
    WHEN w.ResolvedTS > pbTS THEN pbTS
    ELSE w.ResolvedTS
  END,
  'WTF'
FROM #PullToPB j1
LEFT JOIN ViiWTF w ON j1.VehicleInventoryItemID = w.VehicleInventoryItemID
  AND (w.CreatedTS < j1.pbTS AND (w.ResolvedTS > j1.pulledTS OR w.CreatedTS >= j1.pulledTS)) -- created before PB AND ((resolved after Pull) OR (created ON OR after pull))
WHERE w.VehicleInventoryItemID IS NOT NULL;  
