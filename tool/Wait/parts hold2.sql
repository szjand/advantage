SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS, max(p.receivedTS) AS receivedTS
FROM #jon j
  LEFT JOIN (
  SELECT v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS
  FROM partsorders po
  LEFT JOIN VehicleReconItems v ON v.VehicleReconItemID = po.VehicleReconItemID
  WHERE orderedTS <> receivedTS
  GROUP BY v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS) p ON j.VehicleInventoryItemID = p.VehicleInventoryItemID 
WHERE j.VehicleInventoryItemID IS NOT NULL 
AND p.receivedTS > j.pulledTS 
AND p.receivedTS <= j.pbTS -- eliminates parts ord after pb
GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS -- handle multiple orders ordered at the same time, but received at diff times
ORDER BY j.VehicleInventoryItemID   

GROUP BY orderedts, THEN MAX(received) to handle multiple orders ordered at the same time, but received at diff times


SELECT w.VehicleInventoryItemID, w.pulledTS, w.pbTS, MechStart, MechEnd, pt.orderedTS, pt.receivedts 
FROM #WipStartTimes w
LEFT JOIN (
  SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS, max(p.receivedTS) AS receivedTS
  FROM #jon j
    LEFT JOIN (
    SELECT v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS
    FROM partsorders po
    LEFT JOIN VehicleReconItems v ON v.VehicleReconItemID = po.VehicleReconItemID
    WHERE orderedTS <> receivedTS
    GROUP BY v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS) p ON j.VehicleInventoryItemID = p.VehicleInventoryItemID 
  WHERE j.VehicleInventoryItemID IS NOT NULL 
  AND p.receivedTS > j.pulledTS 
  AND p.receivedTS <= j.pbTS -- eliminates parts ord after pb
  GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS) pt ON w.VehicleInventoryItemID = pt.VehicleInventoryItemID 
WHERE pt.orderedTS IS NOT NULL 

-- what i need to know IS WHEN IS the vehicle ready for mechanical
-- overlap with mech wait times

SELECT x.*, pt.orderedTS, pt.receivedTS, position('m' IN seq) AS mPos 
INTO #MechParts
FROM (
  SELECT w.*,
    CASE
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
        CASE
          WHEN MechStart < BodyStart THEN 'mb'
          ELSE 'bm'
        END
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN MechStart < AppStart THEN 'ma'
          ELSE 'am'
        END
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN BodyStart < AppStart THEN 'ba'
          ELSE 'ab'
        END 
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
        CASE 
          WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
          WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
        END 
    END AS seq -- recon dept sequence    
  FROM #WipStartTimes w) x 
LEFT JOIN (
  SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS, max(p.receivedTS) AS receivedTS
  FROM #jon j
    LEFT JOIN (
    SELECT v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS
    FROM partsorders po
    LEFT JOIN VehicleReconItems v ON v.VehicleReconItemID = po.VehicleReconItemID
    WHERE orderedTS <> receivedTS
    GROUP BY v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS) p ON j.VehicleInventoryItemID = p.VehicleInventoryItemID 
  WHERE j.VehicleInventoryItemID IS NOT NULL 
  AND p.receivedTS > j.pulledTS 
  AND p.receivedTS <= j.pbTS -- eliminates parts ord after pb
  GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS) pt ON x.VehicleInventoryItemID = pt.VehicleInventoryItemID   
WHERE pt.orderedTS IS NOT NULL  
  
  

-- SELECT * FROM #MechParts  
--IF seq m** THEN pull - mechstart
--IF seq *m* OR **m THEN the previous endTS
-- DROP TABLE #MechParts
SELECT  m.*, 
--  VehicleInventoryItemID, pulledTS, MechStart, orderedTS, receivedTS,
  CASE 
    WHEN mPos = 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, receivedTS, 'Service') FROM system.iota) --timestampdiff(sql_tsi_hour, pulledTS, receivedTS)
    
    WHEN mPos = 2 THEN 
      CASE
        WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN (select Utilities.BusinessHoursFromInterval(BodyEnd, receivedTS, 'Service') FROM system.iota) --timestampdiff(sql_tsi_hour, BodyEnd, receivedTS)
        WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN (select Utilities.BusinessHoursFromInterval(AppEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, AppEnd, receivedTS)
      END
     
    WHEN mPos = 3 THEN
      CASE
        WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN (select Utilities.BusinessHoursFromInterval(BodyEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, BodyEnd, receivedTS)
        WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN (select Utilities.BusinessHoursFromInterval(AppEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, AppEnd, receivedTS)
      END  
      
  END AS PartsHoldBusHours
FROM #MechParts m
ORDER BY mPos, seq


2 records for 977ef457-18ad-4627-84fc-a185925c192d
-- there are 3 of them
SELECT VehicleInventoryItemID 
FROM #MechParts
GROUP BY VehicleInventoryItemID
HAVING COUNT(*) > 1

-- DROP TABLE #orders
SELECT  VehicleInventoryItemID AS id, 
  CASE 
    WHEN mPos = 1 THEN pulledTS
    WHEN mPos = 2 THEN 
      CASE
        WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN BodyEnd
        WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN AppEnd
      END
    WHEN mPos = 3 THEN
      CASE
        WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN BodyEnd
        WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN AppEnd
      END  
  END AS d1, receivedTS AS d2
INTO #orders  
FROM #MechParts m
WHERE
  CASE 
    WHEN mpos = 1 AND receivedTS > pulledTS THEN 1 = 1
    WHEN mPos = 2 THEN 
      CASE
        WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN 1 = 1
        WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN 1 = 1
      END
    WHEN mPos = 3 THEN
      CASE
        WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN 1 = 1
        WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN 1 = 1
      END 
  END;
 
  

SELECT ID, MIN(d1) AS d1, MAX(d2) AS d2, SUM(hours) AS hours
INTO #partsholdbushours
FROM ( --#tablePeriods 
  SELECT C.ID, MIN(C.d1) AS d1, C.d2,
    (select Utilities.BusinessHoursFromInterval(MIN(c.d1), c.d2, 'Service') FROM system.iota) AS hours
  FROM (
    SELECT A.ID, A.d1, MAX(A.d2) AS d2
    FROM ( -- #tableProtoPeriods A
      SELECT A.ID, A.date AS d1, B.date AS d2
      FROM ( -- #tableStacked A
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( --#tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date        
        GROUP BY A.ID, A.date) A     
      INNER JOIN ( --#tableStacked B ON B.ID = A.ID AND B.date > A.date
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( -- #tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date      
        GROUP BY A.ID, A.date) B ON B.ID = A.ID AND B.date > A.date      
      INNER JOIN ( -- #tableStacked C ON C.ID = A.ID AND C.date > A.date AND C.date <= B.date
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( -- #tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date        
        GROUP BY A.ID, A.date)  C ON C.ID = A.ID AND C.date > A.date AND C.date <= B.date      
      WHERE A.thecount = 0
      AND B.thecount = 1
      GROUP BY A.ID, A.date, B.date
      HAVING MIN(C.thecount) > 0) A    
    GROUP BY A.ID, A.d1) C    
  GROUP BY C.ID, C.d2, hours) A
GROUP BY ID; 

SELECT m.pulledTS, m.mechstart, m.mechend, m.bodystart, m.bodyend, m.appstart, m.append, 
  m.orderedts, m.receivedts, m.seq, p.hours 
FROM #MechParts m
LEFT JOIN #partsholdbushours p ON m.VehicleInventoryItemID = p.id
WHERE p.id IS NOT NULL 
