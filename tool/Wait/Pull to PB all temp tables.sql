-- #jon FROM Pull to PB.sql
-- #ReconWip FROM ExtractWIP3.sql
-- @WipStartTimes FROM ReconWaitTimes.sql
-- #ReconWait FROM ReconWaitTimes.sql
-- #move FROM MoveList.sql
-- #MechParts FROM parts hold2.sql
-- #orders FROM parts hold2.sql
-- #partsholdbushours FROM parts hold2.sql
----------
-- #jon  DROP TABLE #jon
----------
SELECT  v.VehicleInventoryItemID, pulledTS, pbTS, da.*
INTO #jon  
FROM (
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17      
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate >= CurDate() - 118
  AND TheDate <= CurDate()) da  
LEFT JOIN ( -- Pull -> PB
  SELECT *
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS PulledTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS pbTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
  WHERE viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
  AND pb.pbts IS NOT NULL -- made it ALL the way to the lot   
  AND NOT EXISTS ( -- only vehicles with a single pull
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)
  AND NOT EXISTS ( -- only vehicles with a single pb
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPB'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)) v ON da.TheDate = CAST(v.pbTS AS sql_date);
----------
-- #ReconWip
----------
SELECT VehicleInventoryItemID, MAX(MechBusHours) AS MechBusHours, MAX(BodyBusHours) AS BodyBusHours, MAX(AppBusHours) AS AppBusHours,
  SUM(CASE WHEN MechBusHours > 0 THEN 1 ELSE 0 END) AS MechCount,
  SUM(CASE WHEN BodyBusHours > 0 THEN 1 ELSE 0 END) AS BodyCount,
  SUM(CASE WHEN AppBusHours > 0 THEN 1 ELSE 0 END) AS AppCount
INTO #ReconWip
FROM (
  SELECT VehicleInventoryItemID,
    coalesce(
      SUM(
        CASE 
          WHEN category = 'MechanicalReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'Service') FROM system.iota)
                END 
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota)
                END  
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'Service') FROM system.iota)
                END
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'Service') FROM system.iota)
                END 
            END 
        END), 0) AS MechBusHours,
    coalesce(
      SUM(
        CASE 
          WHEN category = 'BodyReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'BodyShop') FROM system.iota)
                END 
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'BodyShop') FROM system.iota)
                END  
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'BodyShop') FROM system.iota)
                END
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'BodyShop') FROM system.iota)
                END 
            END 
        END), 0) AS BodyBusHours,
    coalesce(
      SUM(
        CASE 
          WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN
            CASE  
              WHEN startTS < pulledTS AND completeTS > pulledTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, completeTS, 'Detail') FROM system.iota)
                END 
              WHEN startTS < pulledTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Detail') FROM system.iota)
                END  
              WHEN startTS >= pulledTS AND startTS < pbTS AND completeTS <= pbTS THEN 
                CASE 
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, completeTS, 'Detail') FROM system.iota)
                END
              WHEN startTS >= pulledTS AND pulledTS < pbTS AND completeTS >= pbTS THEN 
                CASE
                  WHEN startTS = completeTS THEN 1
                  ELSE (select Utilities.BusinessHoursFromInterval(startTS, pbTS, 'Detail') FROM system.iota)
                END 
            END 
        END), 0) AS AppBusHours       
  FROM (
    SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category
    FROM #jon j 
    LEFT JOIN (
      SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category 
      FROM VehicleReconItems ri
      INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
      LEFT JOIN TypCategories t ON ri.typ = t.typ
      WHERE ar.status  = 'AuthorizedReconItem_Complete'
      AND ar.startTS IS NOT NULL 
      GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
        AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
  GROUP BY VehicleInventoryItemID, category) x    
GROUP BY VehicleInventoryItemID;
----------
-- @WipStartTimes
----------
SELECT VehicleInventoryItemID, 
  MAX(pulledTS) AS pulledTS, MAX(pbTS) AS pbTS,
  MAX(MechStart) AS MechStart, MAX(MechEnd) AS MechEnd, 
  MAX(BodyStart) AS BodyStart, MAX(BodyEnd) AS BodyEnd, 
  MAX(AppStart) AS AppStart, MAX(AppEnd) AS AppEnd
INTO #WipStartTimes
FROM (
  SELECT VehicleInventoryItemID,-- this gives us one row for each vehicle/dept
    MAX(pulledTS) AS pulledTS,
    MAX(pbTS) AS pbTS,
    MAX(CASE WHEN category = 'MechanicalReconItem' THEN startTS END) AS MechStart,
    MAX(CASE WHEN category = 'BodyReconItem' THEN startTS END) AS BodyStart,  
    MAX(CASE  WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN startTS END) AS AppStart, 
    MAX(CASE WHEN category = 'MechanicalReconItem' THEN completeTS END) AS MechEnd,
    MAX(CASE WHEN category = 'BodyReconItem' THEN completeTS END) AS BodyEnd,  
    MAX(CASE  WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN completeTS END) AS AppEnd          
  FROM (
    SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
    FROM #jon j 
    LEFT JOIN (
      SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
      FROM VehicleReconItems ri
      INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
      LEFT JOIN TypCategories t ON ri.typ = t.typ
      WHERE ar.status  = 'AuthorizedReconItem_Complete'
      AND ar.startTS IS NOT NULL 
      GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
        AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
  GROUP BY VehicleInventoryItemID, category) x
GROUP BY VehicleInventoryItemID; 
    
----------
-- -- #ReconWait
----------
SELECT * FROM #ReconWait
SELECT VehicleInventoryItemID, seq,
  CASE -- mech wait
    WHEN left(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, MechStart) < 0, 0, timestampdiff(sql_tsi_hour, pulledTS, MechStart))
    WHEN substring(seq, 2, 1) = 'm' THEN
      CASE   
        WHEN LEFT(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, MechStart) < 0, 0, timestampdiff(sql_tsi_hour, BodyEnd, MechStart))
        WHEN LEFT(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, MechStart) < 0, 0, timestampdiff(sql_tsi_hour, AppEnd, MechStart))
      END
    WHEN seq = 'bam' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, MechStart) < 0, 0, timestampdiff(sql_tsi_hour, AppEnd, MechStart))
    WHEN seq = 'abm' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, MechStart) <0, 0, timestampdiff(sql_tsi_hour, BodyEnd, MechStart))
  END AS MechWait,
  CASE -- body wait
    WHEN left(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, pulledTS, BodyStart))
    WHEN substring(seq, 2, 1) = 'b' THEN
      CASE
        WHEN LEFT(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, MechEnd, BodyStart))
        WHEN LEFT(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, AppEnd, BodyStart))
      END 
    WHEN seq = 'amb' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, MechEnd, BodyStart)) 
    WHEN seq = 'mab' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, BodyStart) < 0, 0, timestampdiff(sql_tsi_hour, AppEnd, BodyStart))
  END AS BodyWait,
  CASE -- app wait
    WHEN left(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, pulledTS, AppStart))
    WHEN substring(seq, 2, 1) = 'a' THEN
      CASE
        WHEN LEFT(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, MechEnd, AppStart))
        WHEN LEFT(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, BodyEnd, AppStart)) 
      END 
    WHEN seq = 'mba' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, BodyEnd, AppStart))
    WHEN seq = 'bma' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, AppStart) < 0, 0, timestampdiff(sql_tsi_hour, MechEnd, AppStart))
  END AS AppWait,
  CASE -- mech wait bus
    WHEN left(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, MechStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(pulledTS, MechStart, 'Service') FROM system.iota))
    WHEN substring(seq, 2, 1) = 'm' THEN
      CASE   
        WHEN LEFT(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, MechStart) < 0, 0,(select Utilities.BusinessHoursFromInterval(BodyEnd, MechStart, 'Service') FROM system.iota))
        WHEN LEFT(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, MechStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(AppEnd, MechStart, 'Service') FROM system.iota))
      END
    WHEN seq = 'bam' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, MechStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(AppEnd, MechStart, 'Service') FROM system.iota))
    WHEN seq = 'abm' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, MechStart) <0, 0, (select Utilities.BusinessHoursFromInterval(BodyEnd, MechStart, 'Service') FROM system.iota))
  END AS MechWaitBus,
  CASE -- body wait bus
    WHEN left(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(pulledTS, BodyStart, 'BodyShop') FROM system.iota))
    WHEN substring(seq, 2, 1) = 'b' THEN
      CASE
        WHEN LEFT(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(MechEnd, BodyStart, 'BodyShop') FROM system.iota))
        WHEN LEFT(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(AppEnd, BodyStart, 'BodyShop') FROM system.iota))
      END 
    WHEN seq = 'amb' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(MechEnd, BodyStart, 'BodyShop') FROM system.iota)) 
    WHEN seq = 'mab' THEN iif(timestampdiff(sql_tsi_hour, AppEnd, BodyStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(AppEnd, BodyStart, 'BodyShop') FROM system.iota))
  END AS BodyWaitBus,
  CASE -- app wait bus
    WHEN left(seq, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, pulledTS, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(pulledTS, AppStart, 'Detail') FROM system.iota)) 
    WHEN substring(seq, 2, 1) = 'a' THEN
      CASE
        WHEN LEFT(seq, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(MechEnd, AppStart, 'Detail') FROM system.iota))
        WHEN LEFT(seq, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(BodyEnd, AppStart, 'Detail') FROM system.iota)) 
      END 
    WHEN seq = 'mba' THEN iif(timestampdiff(sql_tsi_hour, BodyEnd, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(BodyEnd, AppStart, 'Detail') FROM system.iota))
    WHEN seq = 'bma' THEN iif(timestampdiff(sql_tsi_hour, MechEnd, AppStart) < 0, 0, (select Utilities.BusinessHoursFromInterval(MechEnd, AppStart, 'Detail') FROM system.iota))
  END AS AppWaitBus 
INTO #ReconWait    
FROM (   
  SELECT w.*,
    CASE
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
        CASE
          WHEN MechStart < BodyStart THEN 'mb'
          ELSE 'bm'
        END
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN MechStart < AppStart THEN 'ma'
          ELSE 'am'
        END
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN BodyStart < AppStart THEN 'ba'
          ELSE 'ab'
        END 
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
        CASE 
          WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
          WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
        END 
    END AS seq     
  FROM #WipStartTimes w) x; 
----------
-- #Move
----------  
SELECT x.VehicleInventoryItemID, 
  SUM(
    CASE 
      WHEN LEFT(seq, 1) = 'm' THEN 
        CASE
          WHEN FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
      WHEN substring(seq, 2, 1) = 'm' THEN
        CASE   
          WHEN LEFT(seq, 1) = 'b' THEN 
            CASE
              WHEN FromTS >= BodyEnd AND FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
          WHEN LEFT(seq, 1) = 'a' THEN 
            CASE
              WHEN FromTS >= AppEnd AND FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
        END
      WHEN seq = 'bam' THEN
        CASE 
          WHEN FromTS >= AppEnd AND FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END  
      WHEN seq = 'abm' THEN 
        CASE
          WHEN FromTS >= BodyEnd AND FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
      END) AS "Mech",
  SUM(
    CASE
      WHEN LEFT(seq, 1) = 'b' THEN 
        CASE
          WHEN FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
     WHEN substring(seq, 2, 1) = 'b' THEN
        CASE   
          WHEN LEFT(seq, 1) = 'm' THEN 
            CASE
              WHEN FromTS >= MechEnd AND FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
          WHEN LEFT(seq, 1) = 'a' THEN 
            CASE
              WHEN FromTS >= AppEnd AND FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
        END
    WHEN seq = 'mab' THEN
      CASE 
        WHEN FromTS >= AppEnd AND FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END  
    WHEN seq = 'amb' THEN 
      CASE
        WHEN FromTS >= MechEnd AND FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END     
    END) AS "Body",
  SUM(
    CASE
      WHEN LEFT(seq, 1) = 'a' THEN 
        CASE
          WHEN FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
     WHEN substring(seq, 2, 1) = 'a' THEN
        CASE   
          WHEN LEFT(seq, 1) = 'm' THEN 
            CASE
              WHEN FromTS >= MechEnd AND FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
          WHEN LEFT(seq, 1) = 'b' THEN 
            CASE
              WHEN FromTS >= BodyEnd AND FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
        END
    WHEN seq = 'mba' THEN
      CASE 
        WHEN FromTS >= BodyEnd AND FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END  
    WHEN seq = 'bma' THEN 
      CASE
        WHEN FromTS >= MechEnd AND FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END     
    END) AS "App"
-- SELECT * 
INTO #move
FROM (
  SELECT w.*, m.FromTS, m.ThruTS,
    CASE
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
        CASE
          WHEN MechStart < BodyStart THEN 'mb'
          ELSE 'bm'
        END
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN MechStart < AppStart THEN 'ma'
          ELSE 'am'
        END
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN BodyStart < AppStart THEN 'ba'
          ELSE 'ab'
        END 
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
        CASE 
          WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
          WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
        END 
    END AS seq   
  FROM #WipStartTimes w
  LEFT JOIN vehiclestomove m ON w.VehicleInventoryItemID = m.VehicleInventoryItemID 
  WHERE m.ThruTS > w.pulledTS
  AND m.FromTS <= w.pbTS) x  
-- this IS why there were no move times for body AND appearance
--WHERE LEFT(seq, 1) = 'm' -- AND VehicleInventoryItemID = '0c550b55-5eb4-42e5-965e-0a83786152d3' 
GROUP BY VehicleInventoryItemID;
----------
-- #mechparts
-- SELECT * FROM #mechparts
----------  
SELECT x.*, pt.orderedTS, pt.receivedTS, position('m' IN seq) AS mPos 
INTO #MechParts
FROM (
  SELECT w.*,
    CASE
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
        CASE
          WHEN MechStart < BodyStart THEN 'mb'
          ELSE 'bm'
        END
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN MechStart < AppStart THEN 'ma'
          ELSE 'am'
        END
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN BodyStart < AppStart THEN 'ba'
          ELSE 'ab'
        END 
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
        CASE 
          WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
          WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
        END 
    END AS seq     
  FROM #WipStartTimes w) x 
LEFT JOIN (
  SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS, max(p.receivedTS) AS receivedTS
  FROM #jon j
    LEFT JOIN (
    SELECT v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS
    FROM partsorders po
    LEFT JOIN VehicleReconItems v ON v.VehicleReconItemID = po.VehicleReconItemID
    WHERE orderedTS <> receivedTS
    GROUP BY v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS) p ON j.VehicleInventoryItemID = p.VehicleInventoryItemID 
  WHERE j.VehicleInventoryItemID IS NOT NULL 
  AND p.receivedTS > j.pulledTS 
  AND p.receivedTS <= j.pbTS -- eliminates parts ord after pb
  GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS) pt ON x.VehicleInventoryItemID = pt.VehicleInventoryItemID   
WHERE pt.orderedTS IS NOT NULL;
----------
-- #orders
----------  
SELECT  VehicleInventoryItemID AS id, 
  CASE 
    WHEN mPos = 1 THEN pulledTS
    WHEN mPos = 2 THEN 
      CASE
        WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN BodyEnd
        WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN AppEnd
      END
    WHEN mPos = 3 THEN
      CASE
        WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN BodyEnd
        WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN AppEnd
      END  
  END AS d1, receivedTS AS d2
INTO #orders  
FROM #MechParts m
WHERE
  CASE 
    WHEN mpos = 1 AND receivedTS > pulledTS THEN 1 = 1
    WHEN mPos = 2 THEN 
      CASE
        WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN 1 = 1
        WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN 1 = 1
      END
    WHEN mPos = 3 THEN
      CASE
        WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN 1 = 1
        WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN 1 = 1
      END 
  END;
----------
-- #partsholdbushours 
-- select * FROM #partsholdbushours 
----------  
SELECT ID, MIN(d1) AS d1, MAX(d2) AS d2, SUM(hours) AS hours
INTO #partsholdbushours
FROM ( --#tablePeriods 
  SELECT C.ID, MIN(C.d1) AS d1, C.d2,
    (select Utilities.BusinessHoursFromInterval(MIN(c.d1), c.d2, 'Service') FROM system.iota) AS hours
  FROM (
    SELECT A.ID, A.d1, MAX(A.d2) AS d2
    FROM ( -- #tableProtoPeriods A
      SELECT A.ID, A.date AS d1, B.date AS d2
      FROM ( -- #tableStacked A
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( --#tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date        
        GROUP BY A.ID, A.date) A     
      INNER JOIN ( --#tableStacked B ON B.ID = A.ID AND B.date > A.date
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( -- #tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date      
        GROUP BY A.ID, A.date) B ON B.ID = A.ID AND B.date > A.date      
      INNER JOIN ( -- #tableStacked C ON C.ID = A.ID AND C.date > A.date AND C.date <= B.date
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( -- #tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date        
        GROUP BY A.ID, A.date)  C ON C.ID = A.ID AND C.date > A.date AND C.date <= B.date      
      WHERE A.thecount = 0
      AND B.thecount = 1
      GROUP BY A.ID, A.date, B.date
      HAVING MIN(C.thecount) > 0) A    
    GROUP BY A.ID, A.d1) C    
  GROUP BY C.ID, C.d2, hours) A
GROUP BY ID;   