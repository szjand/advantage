-- a continuation of the multiple instances of wip issue
-- DO i simply DELETE those vehicles IN a period with mult instances
-- the appearance problem: 2 categories, AS opposed to body AND mech, each of which has one
-- so i'm thinking of passing the data generated IN (20110912-milt wip inst) to the Alex query
-- thereby determing the amount of time with no current activity

-- Alex:
/*
Right after I posted that reply in the stackoverflow. I spot a problem with 
my solultion. If there are duplicate end dates that are the start of a no 
hold period, that period will be double counted. For example, if there is 
another part hold starting on 8/8 and ended on 8/10, then the statement will 
give incorrect result. To counter that, we need to get the distinct dates 
where there is no part hold. The modify the statement does the trick:

Another thing is that although this works, I am not certain how efficient 
this is going to be on a large table. On the other hand, this is getting 
the result for all ids in the table, If actual id is substituded in the statement 
to retrieve the result for one vehicle, then the performance should be good.
*/
--
CREATE TABLE #t (
  id integer, 
  d1 date,
  d2 date);

-- DELETE FROM #t  
-- pattern 1 
/*
d1---------d2
    d1------------d2
               d1-----------d2
                                  d1--------d2
                             |-----| this IS the inteval of no activity (WHEN NOT ON parts hold)     

*/
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/04/2011' AS sql_date)); 
INSERT INTO #t values (1, CAST('08/02/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/05/2011' AS sql_date), CAST('08/08/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/10/2011' AS sql_date), CAST('08/12/2011' AS sql_date));

INSERT INTO #t values (2, CAST('08/01/2011' AS sql_date), CAST('08/04/2011' AS sql_date)); 
INSERT INTO #t values (2, CAST('08/10/2011' AS sql_date), CAST('08/12/2011' AS sql_date));

SELECT
   d.id, 
   d.duration, 
   d.duration - 
   IFNULL((
     SELECT Sum(timestampdiff(SQL_TSI_DAY, no_hold.d2, (SELECT min(d1) FROM #t t4 WHERE t4.id = no_hold.id and t4.d1 > no_hold.d2)))
     FROM (
       SELECT DISTINCT id, d2 
       FROM #t t1 
       WHERE (
         SELECT sum(IIF(t1.d2 between t2.d1 and t2.d2, 1, 0 )) 
         FROM #t t2 
         WHERE t2.id = t1.id 
         AND t2.d2 <> t1.d2 ) = 0 
       AND d2 <> (
         SELECT  MAX(d2) 
         FROM #t t3 
         WHERE t3.id = t1.id))/* this was the missing paren*/ no_hold
     WHERE no_hold.id = d.id ), 0) "parts hold"
FROM (
  SELECT id, timestampdiff( SQL_TSI_DAY, min(d1), max(d2)) duration
  FROM #t GROUP BY id) d  

--The outer query gets the duration of the repair work. The complex subquery calculates 
--the total number of days not waiting for parts. 
--This is done by locating the start dates where the vehicle is not waiting for parts, 
--and then count the number of days until it begins to wait for parts again:  

// 1) The query for finding the starting dates when the vehicle is not waiting for parts, 
// i.e. finding all d2 that is not within any date range where the vehicle is waiting for part.
// The DISTINCT is needed to removed duplicate starting "no hold" period.
--*-- SELECT * FROM #t
SELECT DISTINCT id, d2 
FROM #t t1
WHERE ( --*-- return only those values of d2 (interval end date) for each id where
  SELECT sum(IIF(t1.d2 between t2.d1 and t2.d2, 1, 0)) --*- d2 NOT BETWEEN any d1/d2 interval per row
  FROM #t t2 
  WHERE t2.id = t1.id and t2.d2 <> t1.d2) = 0 
  AND d2 <> ( --*-- and d2 IS NOT the largest date
    SELECT max(d2) 
    FROM #t t3 
    WHERE t3.id = t1.id) 

      
// 2) The days where it vehicle is not waiting for part is the date from the above query till the vehicle is // waiting for part again      
timestampdiff( SQL_TSI_DAY, no_hold.d2, ( SELECT min(d1) FROM t t4 WHERE t4.id = no_hold.id and t4.d1 > no_hold.d2 ) )

--Combining the two above and aggregating all such periods gives the number of days 
--that the vehicle is not waiting for parts. The final query adds an extra 
--condition to calculate result for each id from the outer query.
--This probably is not terribly efficient on very large table with many ids. 
--It should fine if the id is limited to one or just a few.

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

so step 1 #t becomes a TABLE of VehicleInventoryItemID, category, fromts, thruts
SELECT * FROM #wtf
-- need to add category
-- of course it breaks, so, dissect

-- dissect
SELECT * from #wtf

-- this much IS ok
       SELECT DISTINCT VehicleInventoryItemID, category, thruts 
       FROM #wtf t1 
       WHERE (
         SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
         FROM #wtf t2 
         WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
         AND t2.category = t1.category
         AND t2.ThruTS <> t1.ThruTS ) = 0 
       AND ThruTS <> (
         SELECT  MAX(ThruTS) 
         FROM #wtf t3 
         WHERE t3.VehicleInventoryItemID  = t1.VehicleInventoryItemID 
         AND t3.category = t1.category)

SELECT * FROM #wtf

-- oops, need to CONVERT to hours 
-- oh shit, don't really need category, this IS inactive time for a vehicle
SELECT
   d.VehicleInventoryItemID,
   d.category,
   d.duration, 
   d.duration - 
   IFNULL((
     SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTS) FROM #wtf t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.category = no_hold.category and t4.FromTS > no_hold.ThruTS)))
     FROM (
       SELECT DISTINCT VehicleInventoryItemID, category, thruts 
       FROM #wtf t1 
       WHERE (
         SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
         FROM #wtf t2 
         WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
         AND t2.category = t1.category
         AND t2.ThruTS <> t1.ThruTS ) = 0 
       AND ThruTS <> (
         SELECT  MAX(ThruTS) 
         FROM #wtf t3 
         WHERE t3.VehicleInventoryItemID  = t1.VehicleInventoryItemID 
         AND t3.category = t1.category))/* this was the missing paren*/ no_hold
     WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID), 0) "parts hold"
FROM (
  SELECT VehicleInventoryItemID , category, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) duration
  FROM #wtf GROUP BY VehicleInventoryItemID , category) d     
  
-- oh shit, don't really need category, this IS inactive time for a vehicle  
SELECT
   d.VehicleInventoryItemID,
   d.duration, 
   d.duration - 
   IFNULL((
     SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTS) FROM #wtf t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
     FROM (
       SELECT DISTINCT VehicleInventoryItemID, thruts 
       FROM #wtf t1 
       WHERE (
         SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
         FROM #wtf t2 
         WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
         AND t2.ThruTS <> t1.ThruTS ) = 0 
       AND ThruTS <> (
         SELECT  MAX(ThruTS) 
         FROM #wtf t3 
         WHERE t3.VehicleInventoryItemID  = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
     WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID), 0) "parts hold"
FROM (
  SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) duration
  FROM #wtf GROUP BY VehicleInventoryItemID) d     
  
  
  
  
  
-- the problem with converting this to business hours IS the differing business
-- hours for the different depts
-- makes me wonder IF these are even relevant numbers
-- should i break each dept out separately to generate business hours
-- that makes no sense,   
-- no numbers seem to make sense outside the context of a timeline
-- looking for performance of individual depts, so knowing a car was ON the move list
-- for 38 hours of the entire 235 hour pull->pb interval doesn't tell me
-- which dept was impacted
-- AND that matters because, IF a car IS pulled, NOT ON parts hold, NOT a wtf, the mech capacity(mech wait) hours
-- number will be FROM pull -> wip, but IF it was ON the move list during that interval
-- that will decrease the capacity hours
-- Parts Hold during the interval in which the vehicle was waiting to start mech WORK
-- IF the sequence IN which the WORK was done was bma, AND ALL the parts hold was finished
-- before the END of body WIP, THEN the parts hold time IS irrelevant to mech wait time
-- so that takes me back to carving out a timeline
SELECT *
FROM #wtf w
LEFT JOIN (
  SELECT
     d.VehicleInventoryItemID,
     d.duration, 
     d.duration - 
     IFNULL((
       SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTS) FROM #wtf t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventorypItemID and t4.FromTS > no_hold.ThruTS)))
       FROM (
         SELECT DISTINCT VehicleInventoryItemID, thruts 
         FROM #wtf t1 
         WHERE (
           SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
           FROM #wtf t2 
           WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
           AND t2.ThruTS <> t1.ThruTS ) = 0 
         AND ThruTS <> (
           SELECT  MAX(ThruTS) 
           FROM #wtf t3 
           WHERE t3.VehicleInventoryItemID  = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
       WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID), 0) Activity
  FROM (
    SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) duration
    FROM #wtf GROUP BY VehicleInventoryItemID) d) y ON w.VehicleInventoryItemID = y.VehicleInventoryItemID

--*--  
/*
-- trying to rework that tricky WHERE clause INTO JOIN OR something
-- AND striking out to the extent that i'm about to say fuck it  

SELECT *
FROM #t t1, #t t2

SELECT *
FROM #t t1, #t t2
-- remove MAX d2
SELECT *
FROM #t t1, #t t2
WHERE t1.d2 <> (
  SELECT MAX(d2)
  FROM #t t3
  WHERE t3.id = t1.id)
AND t1.id = t2.id
AND t1.d2 <> t2.d2 


SELECT *
FROM #t t1, #t t2
WHERE t1.d2 <> (
  SELECT MAX(d2)
  FROM #t t3
  WHERE t3.id = t1.id)
AND t1.id = t2.id
AND t1.d2 <> t2.d2 
AND NOT EXISTS (
  SELECT d2
  FROM #t 
  WHERE id = t1.id
  AND d2 = t1.d2
  AND d2 BETWEEN t2.d1 AND t2.d2)

  

-- these next 2 seem to be going nowhere
SELECT *
FROM #t t1
INNER JOIN (  
  SELECT *
  FROM #t) t2 ON t1.id = t2.id 
    AND t1.d2 <> t2.d2
WHERE t1.d2 <> (
  SELECT MAX(d2)
  FROM #t t3
  WHERE t3.id = t1.id)  
AND t1.d2 NOT BETWEEN t2.d1 AND t2.d2  
    
  
SELECT DISTINCT t1.id, t1.d2
FROM #t t1
LEFT JOIN (  
  SELECT *
  FROM #t) t2 ON t1.id = t2.id 
    AND t1.d2 <> t2.d2
    AND NOT t1.d2 BETWEEN t2.d1 AND t2.d2
WHERE t1.d2 <> (
  SELECT MAX(d2)
  FROM #t t3
  WHERE t3.id = t1.id)  
--*--   
*/