
--SELECT  v.*, da.*--, m.*, w.*,
/*
  (SELECT VehicleInventoryItemID -- HasMech
    FROM VehicleReconItems ri
    INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND ri.typ <> 'MechanicalReconItem_Inspection'
    AND ar.status  = 'AuthorizedReconItem_Complete'
    AND ri.typ in (  
      SELECT Typ 
      FROM TypCategories
      WHERE Category = 'MechanicalReconItem')
    GROUP BY VehicleInventoryItemID) AS HasMech,
  (SELECT VehicleInventoryItemID -- HasBody
    FROM VehicleReconItems ri
    INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND ar.status  = 'AuthorizedReconItem_Complete'
    AND ri.typ in (  
      SELECT Typ 
      FROM TypCategories
      WHERE Category = 'BodyReconItem')
    GROUP BY VehicleInventoryItemID) AS HasBody,  
  (SELECT VehicleInventoryItemID -- HasApp
    FROM VehicleReconItems ri
    INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND ar.status  = 'AuthorizedReconItem_Complete'
    AND ri.Typ IN ( 
      SELECT Typ
      FROM TypCategories tc1 
      WHERE tc1.Category = 'AppearanceReconItem' 
        OR tc1.Typ = 'PartyCollection_AppearanceReconItem') 
    GROUP BY VehicleInventoryItemID) AS HasApp
*/  
-- DROP TABLE #jon
SELECT  v.VehicleInventoryItemID, pulledTS, pbTS, da.*
INTO #jon  
FROM (
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17      
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate >= CurDate() - 118
  AND TheDate <= CurDate()) da  
LEFT JOIN ( -- Pull -> PB
  SELECT *
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS PulledTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS pbTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
  WHERE viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
  AND pb.pbts IS NOT NULL -- made it ALL the way to the lot   
  AND NOT EXISTS ( -- only vehicles with a single pull
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)
  AND NOT EXISTS ( -- only vehicles with a single pb
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPB'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)) v ON da.TheDate = CAST(v.pbTS AS sql_date)
/*     
LEFT JOIN ( -- Move List
  SELECT VehicleInventoryItemID,
  SUM((select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)) AS MoveBusHours 
  FROM VehiclesToMove
  WHERE CAST(FromTS AS sql_date) >= curdate() - 118
  GROUP BY VehicleInventoryItemID) m ON v.VehicleInventoryItemID = m.VehicleInventoryItemID  
LEFT JOIN ( -- WTF
  SELECT VehicleInventoryItemID AS wtfid,
  SUM((select Utilities.BusinessHoursFromInterval(CreatedTS, ResolvedTS, 'Service') FROM system.iota)) AS WTFBusHours 
  FROM ViiWTF
  WHERE CAST(CreatedTS AS sql_date) >= curdate() - 118
  GROUP BY VehicleInventoryItemID ) w ON v.VehicleInventoryItemID = w.wtfid 
*/  
/*  
-- DROP TABLE #jon  
-- SELECT * FROM #jon
SELECT TheWeek,
  MIN(TheDate) AS "Week First Day", MAX(TheDate) AS "Week Last Day",
  COUNT(VehicleInventoryItemID) AS "to PB (Count)",
  SUM(timestampdiff(sql_tsi_day, cast(pulledTS AS sql_date), cast(pbTS AS sql_date)))/COUNT(VehicleInventoryItemID) AS "Avg Days Pull->PB per Vehicle",
  SUM((SELECT Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota))/COUNT(VehicleInventoryItemID) AS "Avg Bus Hours Pull->PB per Vehicle"
--  SUM(MoveBusHours)/COUNT(VehicleInventoryItemID) AS "Avg BusHours on Move List per Vehicle",
--  SUM(WTFBusHours)/COUNT(VehicleInventoryItemID) AS "Avg BusHours as a WTF per Vehicle",
--  SUM(CASE WHEN HasMech IS NOT NULL THEN 1 ELSE 0 END) AS MechCount,
--  SUM(CASE WHEN HasBody IS NOT NULL THEN 1 ELSE 0 END) AS BodyCount,
--  SUM(CASE WHEN HasApp IS NOT NULL THEN 1 ELSE 0 END) AS AppCount  
FROM #jon j
GROUP BY TheWeek  

*/  