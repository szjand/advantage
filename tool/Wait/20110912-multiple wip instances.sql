-- base TABLE FROM @WipStartTimes
SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
FROM #jon j 
LEFT JOIN (
  SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
  AND ar.startTS IS NOT NULL 
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))
-- WHERE j.VehicleInventoryItemID IN (SELECT VehicleInventoryItemID FROM #mult)
WHERE b.VehicleInventoryItemID IS NOT NULL 
ORDER BY j.VehicleInventoryItemID, startTS

-- *****************************
-- wait a fucking minute, recon base, GROUP BY startts only (NOT completeTS) AND use MAX(completeTS)
-- this could eliminate mult records IN a dept that start at the same time but don't END at the same time
-- SELECT * FROM #jon

-- first how many records with orig grouping
SELECT COUNT(*) FROM ( -- 881
SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
FROM #jon j 
LEFT JOIN (
  SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
  AND ar.startTS IS NOT NULL 
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))
-- WHERE j.VehicleInventoryItemID IN (SELECT VehicleInventoryItemID FROM #mult)
WHERE b.VehicleInventoryItemID IS NOT NULL) wtf 

-- ooh, maybe GROUP ON start times after grouping ON start AND stop (which will eliminate the duplicates)
-- SELECT COUNT(*) FROM ( -- 871
SELECT  VehicleInventoryItemID, MAX(pulledTS)AS pulledTS, MAX(pbTS) AS pbTS, startTS AS FromTS, MAX(completeTS) AS ThruTS, category
-- DROP TABLE #wtf
INTO #wtf
FROM (
  SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
  FROM #jon j 
  LEFT JOIN (
    SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
    FROM VehicleReconItems ri
    INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
    LEFT JOIN TypCategories t ON ri.typ = t.typ
    WHERE ar.status  = 'AuthorizedReconItem_Complete'
    AND ar.startTS IS NOT NULL 
    GROUP BY VehicleInventoryItemID, ar.startts, ar.completeTS, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))
  -- WHERE j.VehicleInventoryItemID IN (SELECT VehicleInventoryItemID FROM #mult)
  WHERE b.VehicleInventoryItemID IS NOT NULL) wtf
GROUP BY VehicleInventoryItemID, category, startTS
ORDER BY VehicleInventoryItemID, startts

-- now FROM this result, what does mult wip look LIKE
-- this doesn't show the vehicles with wip IN each app category, because of grouping BY category
SELECT * 
FROM #wtf
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM #wtf
  GROUP BY VehicleInventoryItemID, category
  HAVING COUNT(*) >1 )
AND category IN (
  SELECT category 
  FROM #wtf
  GROUP BY VehicleInventoryItemID, category
  HAVING COUNT(*) > 1)
-- *****************************
-- 9/13-1
-- so let's go down the looking for periods of no activity route
-- activities:  parts hold, move, wtf, wip
-- use #wtf AS the base: WIP Activity, NOT interested IN any vehicle that isn't IN wip at some point IN the pull->pb interval
-- UNION the other activities

-- DROP TABLE #xxx
SELECT *
INTO #xxx
FROM #wtf 
UNION 
SELECT j1.VehicleInventoryItemID, j1.pulledTS, j1.pbTS, m.FromTS, m.ThruTS, 'Move'
FROM #jon j1
LEFT JOIN VehiclesToMove m ON j1.VehicleInventoryItemID = m.VehicleInventoryItemID 
  AND (m.FromTS <= j1.pbTS AND (m.ThruTS > j1.pulledTS OR m.FromTS > j1.pulledTS))
WHERE m.VehicleInventoryItemID IS NOT NULL -- don't care about a vehicle NOT ON move list during interval
UNION 
SELECT j1.VehicleInventoryItemID, j1.pulledTs, j1.pbTS, p.orderedTS, p.ReceivedTS, 'Parts'
FROM #jon j1
LEFT JOIN (
  SELECT po.*, (SELECT VehicleInventoryItemID FROM VehicleReconItems WHERE VehicleReconItemID = po.VehicleReconItemID) AS VehicleInventoryItemID 
  FROM partsorders po
  WHERE po.CancelledTS IS NULL) p ON j1.VehicleInventoryItemID = p.VehicleInventoryItemID 
    AND (p.OrderedTS < j1.pbTS AND (p.ReceivedTS > j1.pulledTS OR p.OrderedTS > j1.pulledTS))
WHERE p.VehicleInventoryItemID IS NOT NULL 
UNION 
SELECT j1.VehicleInventoryItemID, j1.pulledTs, j1.pbTS, w.CreatedTS, w.ResolvedTS, 'WTF'
FROM #jon j1
LEFT JOIN ViiWTF w ON j1.VehicleInventoryItemID = w.VehicleInventoryItemID
  AND (w.CreatedTS <= j1.pbTS AND (w.ResolvedTS > j1.pulledTS OR w.CreatedTS > j1.pulledTS))
WHERE w.VehicleInventoryItemID IS NOT NULL   
ORDER BY VehicleInventoryItemID, FromTS

-- ok here's the data

-- want one row for each VehicleInventoryItemID, category
SELECT DISTINCT x.VehicleInventoryItemID, y.category
FROM #xxx x, (SELECT DISTINCT category FROM #xxx) y

-- what i want now IS a single row for each VehicleInventoryItemID , with a FromTS AND ThruTS for each Category
-- don't forget multiple instances
-- so IS what i want a single row for each VehicleInventoryItemID , with a FromTS AND ThruTS for each Category INSTANCE?
SELECT a.* 
FROM #xxx a
WHERE a.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'
 

-- SELECT COUNT(*) FROM( -- 11823
SELECT *
FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
ORDER BY VehicleInventoryItemID, fromts

-- don't forget multiple instances
-- ADD a seq COLUMN to y ?
-- separate TABLE for each category? -- which, IF this IS the CASE, probably don't need #xxx, just the
-- individual tables unioned to make #xxx ??? -- maybe NOT

-- GROUP BY fromts?

-- am i confusing the need for total category time AND sequence
-- period of time IN which there IS no activity

-- so let's just start with total category time per vehicle
SELECT *
FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
WHERE x.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'
AND cat = category

SELECT VehicleInventoryItemID, cat, fromts, thruts
FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
WHERE x.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'

SELECT VehicleInventoryItemID, cat,
  SUM(
    CASE
      WHEN category = cat THEN timestampdiff(sql_tsi_hour, fromts, thruts)
      ELSE 0
    END)
FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
WHERE x.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'
GROUP BY VehicleInventoryItemID, cat    

-- double check output
SELECT *
FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
WHERE x.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'
AND cat = category
ORDER BY cat

-- business hours
-- need to match cat to input parameter for BusinessHoursFromInterval
--      WHEN @Dept = 'Service' THEN wh.Dept = 'Service'
--      WHEN @Dept = 'BodyShop' THEN wh.Dept = 'BodyShop'
--      WHEN @Dept = 'Detail' THEN wh.Dept = 'Detail'

SELECT VehicleInventoryItemID, cat, MAX(pulledTS) AS pulledTS, MAX(pbTS) AS pbTS,
  SUM(
    CASE
      WHEN category = cat THEN 
        CASE 
          WHEN cat = 'BodyReconItem' THEN 
            (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'BodyShop') FROM system.iota)
          WHEN cat = 'AppearanceReconItem' OR cat = 'PartyCollection' THEN 
            (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Detail') FROM system.iota)
          ELSE 
            (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
      ELSE 0
    END) AS BusHours
FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
WHERE x.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'
GROUP BY VehicleInventoryItemID, cat 

-- AND transform it to one row per vehicle
-- 9/14 this IS WHERE i LEFT off
-- this AND looking at the big difference IN parts hold
-- check output against Pull to PB stats.sql
-- parts looks way off
-- parts hold IS only relevant IF it during time IN which no other activity IS current
-- 
-- i need a COLUMN for each category
-- fuck, i'm stuck IN thinking i need a dynamic number of columns
-- well, fuck it, DO it the dummy way, after ALL, i DO know the 7 fucking categories
SELECT VehicleInventoryItemID, max(timestampdiff(sql_tsi_day, cast(pulledTS AS sql_date),cast(pbTS AS sql_date))),
  SUM(
    CASE 
      WHEN cat = 'AppearanceReconItem' THEN BusHours
    END) AS "Detail Other WIP",
  SUM(
    CASE 
      WHEN cat = 'Parts' THEN BusHours
    END) AS "Parts Hold",
  SUM(
    CASE 
      WHEN cat = 'BodyReconItem' THEN BusHours
    END) AS "Body WIP",
  SUM(
    CASE 
      WHEN cat = 'MechanicalReconItem' THEN BusHours
    END) AS "Mech WIP" ,
  SUM(
    CASE 
      WHEN cat = 'Move' THEN BusHours
    END) AS Move ,
  SUM(
    CASE 
      WHEN cat = 'PartyCollection' THEN BusHours
    END) AS "Detail WIP" ,
  SUM(
    CASE 
      WHEN cat = 'WTF' THEN BusHours
    END) AS WTF                      
-- etc etc etc      
FROM(  
  SELECT VehicleInventoryItemID, cat, MAX(pulledTS) AS pulledTS, MAX(pbTS) AS pbTS,
    SUM(
      CASE
        WHEN category = cat THEN 
          CASE 
            WHEN cat = 'BodyReconItem' THEN 
              (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'BodyShop') FROM system.iota)
            WHEN cat = 'AppearanceReconItem' OR cat = 'PartyCollection' THEN 
              (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Detail') FROM system.iota)
            ELSE 
              (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
          END 
        ELSE 0
      END) AS BusHours
  FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
--  WHERE x.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'
  GROUP BY VehicleInventoryItemID, cat) e
GROUP BY VehicleInventoryItemID   
  
-- simplify it further
-- return each category AS a COLUMN FROM:
-- DO i have to know the COLUMN names OR the number of columns ahead of time
-- hopefully not
some sort of self JOIN 
SELECT * 
FROM (SELECT DISTINCT category as cat FROM #xxx) a, (SELECT DISTINCT category as cat FROM #xxx) b  

SELECT * 
FROM (SELECT DISTINCT category as cat FROM #xxx) a, (SELECT DISTINCT category as cat FROM #xxx) b 
mind fucking i won't know the number of colums ahead of time, so it can't be
multiple subselects IN the SELECT clause
  

-- check output against Pull to PB stats.sql
-- parts looks way off
-- parts hold IS only relevant IF it during time IN which no other activity IS current



-- IF this data IS to be used elsewhere, need to think about 0 time wip





















-- these are the VehicleInventoryItemIDs that have multiple wip instances IN any give category
SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
FROM #jon j 
LEFT JOIN (
  SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
  AND ar.startTS IS NOT NULL 
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))
WHERE j.VehicleInventoryItemID in (
  -- 32 VehicleInventoryItemIDs     
  SELECT VehicleInventoryItemID
  FROM (
    SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
    FROM #jon j 
    LEFT JOIN (
      SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
      FROM VehicleReconItems ri
      INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
      LEFT JOIN TypCategories t ON ri.typ = t.typ
      WHERE ar.status  = 'AuthorizedReconItem_Complete'
      AND ar.startTS IS NOT NULL 
      GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
        AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) wtf 
  WHERE VehicleInventoryItemID IS NOT NULL    
  GROUP BY VehicleInventoryItemID, category HAVING COUNT(*) > 1)      
ORDER BY j.VehicleInventoryItemID, startTS


