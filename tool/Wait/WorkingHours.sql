-- how to handle closed days
-- take 1: no record for the day
-- take 2: record with NULL Opens/Closes, 0 TotalHours
UPDATE workinghours
SET FromTS = timestampadd(sql_tsi_year, -5, now())
WHERE Dept = 'Service';

INSERT INTO WorkingHours values (
6,
CAST('07:00:00' AS sql_time),
CAST('18:00:00' AS sql_time),
11,
'BodyShop',
timestampadd(sql_tsi_year, -5, now()),
NULL)

INSERT INTO WorkingHours values (
7,
CAST('08:00:00' AS sql_time),
CAST('17:00:00' AS sql_time),
9,
'BodyShop',
timestampadd(sql_tsi_year, -5, now()),
NULL)



INSERT INTO WorkingHours values (
6,
CAST('07:00:00' AS sql_time),
CAST('21:00:00' AS sql_time),
14,
'Detail',
timestampadd(sql_tsi_year, -5, now()),
NULL)

INSERT INTO WorkingHours values (
7,
CAST('08:00:00' AS sql_time),
CAST('18:00:00' AS sql_time),
10,
'Detail',
timestampadd(sql_tsi_year, -5, now()),
NULL)

INSERT INTO WorkingHours values (
1,
CAST('12:00:00' AS sql_time),
CAST('17:00:00' AS sql_time),
5,
'Detail',
timestampadd(sql_tsi_year, -5, now()),
NULL)