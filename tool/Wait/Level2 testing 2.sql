-- total dept hours > pulltopb hours
/*
SELECT *
INTO #Level2
FROM (
SELECT x.theweek, x.VehicleInventoryItemID, x.reconseq,
  timestampdiff(sql_tsi_hour, pulledts, pbts) AS PullToPb,
  coalesce(
    CASE position('m' IN ReconSeq) -- m**
      WHEN 1 THEN iif(timestampdiff(sql_tsi_hour, pulledTS, mThru) < 1, 1, timestampdiff(sql_tsi_hour, pulledTS, mThru))
      WHEN 2 THEN -- *m*
        CASE
          -- bm*
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, bThru, mThru) < 1, 1, timestampdiff(sql_tsi_hour, bThru, mThru))
          -- am*
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, aThru, mThru) < 1, 1, timestampdiff(sql_tsi_hour, aThru, mThru))
        END 
      WHEN 3 THEN -- **m
        CASE
          -- *bm
          WHEN substring(ReconSeq, 2, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, bThru, mThru) < 1, 1, timestampdiff(sql_tsi_hour, bThru, mThru))
          -- *am
          WHEN substring(ReconSeq, 2, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, aThru, mThru) < 1, 1, timestampdiff(sql_tsi_hour, aThru, mThru))
        END     
    END, 0) AS MechHours,
  coalesce(
    CASE position('b' IN ReconSeq) -- b**
      WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, bThru)
      WHEN 2 THEN -- *b*
        CASE
          -- mb*
          WHEN substring(ReconSeq, 1, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, mThru, bThru) < 1, 1, timestampdiff(sql_tsi_hour, mThru, bThru))
          -- ab*
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, aThru, bThru) < 1, 1, timestampdiff(sql_tsi_hour, aThru, bThru))
        END 
      WHEN 3 THEN -- **b
        CASE
          -- *mb
          WHEN substring(ReconSeq, 2, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, mThru, bThru) < 1, 1, timestampdiff(sql_tsi_hour, mThru, bThru))
          -- *ab
          WHEN substring(ReconSeq, 2, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, aThru, bThru) < 1, 1, timestampdiff(sql_tsi_hour, aThru, bThru))
        END     
    END, 0) AS BodyHours,
  coalesce(
    CASE position('a' IN ReconSeq) -- a**
      WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, aThru)
      WHEN 2 THEN -- *a*
        CASE
          -- ma*
          WHEN substring(ReconSeq, 1, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, mThru, aThru) < 1, 1, timestampdiff(sql_tsi_hour, mThru, aThru)) 
          -- ba*
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, bThru, aThru) < 1, 1, timestampdiff(sql_tsi_hour, bThru, aThru))
        END 
      WHEN 3 THEN -- **a
        CASE
          -- *ma
          WHEN substring(ReconSeq, 2, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, mThru, aThru) < 1, 1, timestampdiff(sql_tsi_hour, mThru, aThru))
          -- *ba
          WHEN substring(ReconSeq, 2, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, bThru, aThru) < 1, 1, timestampdiff(sql_tsi_hour, bThru, aThru))
        END     
    END, 0) AS AppHours 
FROM ( -- one row per vehicle
  SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFrom, o.ThrutS AS oThru
  FROM #PullToPB p 
--  INNER JOIN ( -- exclusions FROM multiple wip/category
--    SELECT DISTINCT VehicleInventoryItemID 
--    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
    SELECT DISTINCT(VehicleInventoryItemID)
    FROM #WipBase x
    WHERE NOT EXISTS (
      SELECT 1
      FROM #wipbase
      WHERE VehicleInventoryItemID = x.VehicleInventoryItemID 
      AND category = 'AppearanceReconItem')) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL) x)y
WHERE PullToPb - (MechHours + BodyHours + AppHours) < 4
*/

-- the problem IS going to be IN matching numbers FROM level 1 AND level 2
-- to be solved BY using the same base for both levels
-- feels LIKE cheating to just use level 2 without understanding the differences BETWEEN levels 1 & 2
-- but yet once again AND still, avg dept hours <> avg ppb hours
SELECT TheWeek, COUNT(VehicleInventoryItemID) AS [Count],
  SUM(PullToPB)/COUNT(VehicleInventoryItemID) AS HoursPerVeh,
  SUM(CASE WHEN ReconSeq LIKE '%m%' THEN 1 ELSE 0 END) AS mCount,
  SUM(MechHours)/SUM(CASE WHEN ReconSeq LIKE '%m%' THEN 1 ELSE 0 END) mHoursPerVeh,
  SUM(CASE WHEN ReconSeq LIKE '%b%' THEN 1 ELSE 0 END) AS bCount,
  SUM(BodyHours)/SUM(CASE WHEN ReconSeq LIKE '%b%' THEN 1 ELSE 0 END) bHoursPerVeh,
  SUM(CASE WHEN ReconSeq LIKE '%a%' THEN 1 ELSE 0 END) AS aCount,
  SUM(AppHours)/SUM(CASE WHEN ReconSeq LIKE '%a%' THEN 1 ELSE 0 END) aHoursPerVeh,
  SUM(MechHours)/SUM(CASE WHEN ReconSeq LIKE '%m%' THEN 1 ELSE 0 END) 
    +  SUM(BodyHours)/SUM(CASE WHEN ReconSeq LIKE '%b%' THEN 1 ELSE 0 END) 
    + SUM(AppHours)/SUM(CASE WHEN ReconSeq LIKE '%a%' THEN 1 ELSE 0 END)
-- select *
FROM #level2
GROUP BY theweek

-- need to divide BY total vehicles?
-- yep
-- because i'm looking for what portion of the total time IS spent IN each dept
SELECT TheWeek, COUNT(VehicleInventoryItemID) AS [Count],
  SUM(PullToPB)/COUNT(VehicleInventoryItemID) AS HoursPerVeh,
  SUM(CASE WHEN ReconSeq LIKE '%m%' THEN 1 ELSE 0 END) AS mCount,
  SUM(MechHours)/COUNT(VehicleInventoryItemID) mHoursPerVeh,
  SUM(CASE WHEN ReconSeq LIKE '%b%' THEN 1 ELSE 0 END) AS bCount,
  SUM(BodyHours)/COUNT(VehicleInventoryItemID) bHoursPerVeh,
  SUM(CASE WHEN ReconSeq LIKE '%a%' THEN 1 ELSE 0 END) AS aCount,
  SUM(AppHours)/COUNT(VehicleInventoryItemID) aHoursPerVeh,
  SUM(MechHours)/COUNT(VehicleInventoryItemID) 
    +  SUM(BodyHours)/COUNT(VehicleInventoryItemID)
    + SUM(AppHours)/COUNT(VehicleInventoryItemID)
-- select *
FROM #level2
GROUP BY theweek

-- % of ppb IN each dept
SELECT TheWeek, COUNT(VehicleInventoryItemID) AS [Count],
  SUM(PullToPB)/COUNT(VehicleInventoryItemID) AS HoursPerVeh,
  round((SUM(MechHours)/COUNT(VehicleInventoryItemID))*100.0/(SUM(PullToPB)/COUNT(VehicleInventoryItemID)), 0) AS mPerc,
  round((SUM(BodyHours)/COUNT(VehicleInventoryItemID))*100.0/(SUM(PullToPB)/COUNT(VehicleInventoryItemID)), 0) AS bPerc,
  round((SUM(AppHours)/COUNT(VehicleInventoryItemID))*100.0/(SUM(PullToPB)/COUNT(VehicleInventoryItemID)), 0) AS aPerc
FROM #level2
--WHERE reconseq like '%m%'
GROUP BY theweek

-- % of ppb vehicles requiring each dept
SELECT TheWeek, COUNT(VehicleInventoryItemID) AS [Count],

  round(SUM(CASE WHEN ReconSeq LIKE '%m%' THEN 1 ELSE 0 END)*100.0/COUNT(VehicleInventoryItemID), 0) AS mReqd,

  round(SUM(CASE WHEN ReconSeq LIKE '%b%' THEN 1 ELSE 0 END)*100.0/COUNT(VehicleInventoryItemID), 0) AS bReqd,

  round(SUM(CASE WHEN ReconSeq LIKE '%a%' THEN 1 ELSE 0 END)*100.0/COUNT(VehicleInventoryItemID), 0) AS aReqd

-- select *
FROM #level2
GROUP BY theweek

SELECT reconseq, COUNT(*)
FROM #level2
GROUP BY reconseq

SELECT * 
FROM #level2 l
LEFT JOIN #WipBase w ON l.VehicleInventoryItemID = l.VehicleInventoryItemID 

SELECT VehicleInventoryItemID,
  SUM(CASE WHEN category = 'BodyReconItem' THEN  
FROM #wipbase w
GROUP BY w.VehicleInventoryItemID 

SELECT VehicleInventoryItemID, category
from #wipBase
GROUP BY VehicleInventoryItemID, category HAVING COUNT(*) > 1

SELECT p.TheWeek, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
  m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
  a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFrom, o.ThrutS AS oThru
FROM #PullToPB p 
--  INNER JOIN ( -- exclusions FROM multiple wip/category
--    SELECT DISTINCT VehicleInventoryItemID 
--    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
  SELECT DISTINCT(VehicleInventoryItemID)
  FROM #WipBase x
  WHERE NOT EXISTS (
    SELECT 1
    FROM #wipbase
    WHERE VehicleInventoryItemID = x.VehicleInventoryItemID 
    AND category = 'AppearanceReconItem')) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
WHERE p.VehicleInventoryItemID IS NOT NULL