
-- DROP TABLE #orders
SELECT  VehicleInventoryItemID AS id, 
  CASE 
    WHEN mPos = 1 THEN pulledTS
    WHEN mPos = 2 THEN 
      CASE
        WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN BodyEnd
        WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN AppEnd
      END
    WHEN mPos = 3 THEN
      CASE
        WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN BodyEnd
        WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN AppEnd
      END  
  END AS d1, receivedTS AS d2
INTO #orders  
FROM #MechParts m
WHERE
  CASE 
    WHEN mpos = 1 AND receivedTS > pulledTS THEN 1 = 1
    WHEN mPos = 2 THEN 
      CASE
        WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN 1 = 1
        WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN 1 = 1
      END
    WHEN mPos = 3 THEN
      CASE
        WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN 1 = 1
        WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN 1 = 1
      END 
  END;
 
-- TRY the alex solution with bus hours --
-- SELECT * FROM #orders
SELECT d.id,
   d.duration, 
   d.duration - 
      IFNULL(
        (
--        SELECT SUM(timestampdiff(SQL_TSI_hour, t1.d2, (SELECT MIN(d1) FROM #orders t4 WHERE t4.id = t1.id AND t4.d1 > t1.d2))) 
          SELECT SUM(
            (
              SELECT Utilities.BusinessHoursFromInterval(
                t1.d2, (
                  SELECT MIN(d1) FROM #orders t4 WHERE t4.id = t1.id AND t4.d1 > t1.d2
                  ), 'Service'
                ) FROM system.iota)
              )
            )

--        SELECT SUM((SELECT Utilities.BusinessHoursFromInterval(t1.d2, (SELECT MIN(d1) FROM #orders t4 WHERE t4.id = t1.id AND t4.d1 > t1.d2)), 'Service') FROM )
        FROM #orders t1 
        WHERE t1.id = d.id 
        AND (
          SELECT SUM(IIF(t1.d2 BETWEEN t2.d1 AND t2.d2, 1, 0)) 
          FROM #orders t2 
          WHERE t2.id = t1.id 
          AND t2.d2 <> t1.d2 ) = 0 
          AND  d2 <> (
            SELECT MAX(d2) 
            FROM #orders t3 WHERE t3.id = t1.id)), 0) "parts hold"
FROM (
  SELECT id, timestampdiff(SQL_TSI_hour, MIN(d1), MAX(d2)) duration
  FROM #orders GROUP BY id) d;
  
SELECT *
FROM (
  SELECT id, timestampdiff(SQL_TSI_hour, MIN(d1), MAX(d2)) duration
  FROM #orders GROUP BY id) d  
-- TRY the alex solution with bus hours -- 
  
-- TRY the alex solution --
-- SELECT * FROM #orders
SELECT d.id,
   d.duration, 
   d.duration - 
      IFNULL((
        SELECT SUM(timestampdiff(SQL_TSI_hour, t1.d2, (SELECT MIN(d1) FROM #orders t4 WHERE t4.id = t1.id AND t4.d1 > t1.d2))) 
        FROM #orders t1 
        WHERE t1.id = d.id 
        AND (
          SELECT SUM(IIF(t1.d2 BETWEEN t2.d1 AND t2.d2, 1, 0)) 
          FROM #orders t2 
          WHERE t2.id = t1.id 
          AND t2.d2 <> t1.d2 ) = 0 
          AND  d2 <> (
            SELECT MAX(d2) 
            FROM #orders t3 WHERE t3.id = t1.id)), 0) "parts hold"
FROM (
  SELECT id, timestampdiff(SQL_TSI_hour, MIN(d1), MAX(d2)) duration
  FROM #orders GROUP BY id) d;
-- TRY the alex solution --


SELECT ID, MIN(d1) AS d1, MAX(d2) AS d2, SUM(hours) AS hours
INTO #partsholdbushours
FROM ( --#tablePeriods 
  SELECT C.ID, MIN(C.d1) AS d1, C.d2,
    (select Utilities.BusinessHoursFromInterval(MIN(c.d1), c.d2, 'Service') FROM system.iota) AS hours
  FROM (
    SELECT A.ID, A.d1, MAX(A.d2) AS d2
    FROM ( -- #tableProtoPeriods A
      SELECT A.ID, A.date AS d1, B.date AS d2
      FROM ( -- #tableStacked A
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( --#tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date        
        GROUP BY A.ID, A.date) A     
      INNER JOIN ( --#tableStacked B ON B.ID = A.ID AND B.date > A.date
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( -- #tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date      
        GROUP BY A.ID, A.date) B ON B.ID = A.ID AND B.date > A.date      
      INNER JOIN ( -- #tableStacked C ON C.ID = A.ID AND C.date > A.date AND C.date <= B.date
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( -- #tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date        
        GROUP BY A.ID, A.date)  C ON C.ID = A.ID AND C.date > A.date AND C.date <= B.date      
      WHERE A.thecount = 0
      AND B.thecount = 1
      GROUP BY A.ID, A.date, B.date
      HAVING MIN(C.thecount) > 0) A    
    GROUP BY A.ID, A.d1) C    
  GROUP BY C.ID, C.d2, hours) A
GROUP BY ID; 

SELECT m.pulledTS, m.mechstart, m.mechend, m.bodystart, m.bodyend, m.appstart, m.append, 
  m.orderedts, m.receivedts, m.seq, p.hours 
FROM #MechParts m
LEFT JOIN #partsholdbushours p ON m.VehicleInventoryItemID = p.id
WHERE p.id IS NOT NULL 
