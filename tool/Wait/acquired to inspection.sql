u
SELECT VehicleInventoryItemID, stocknumber, Acquired, "Have Vehicle", VehicleInspectionTS,
  timestampdiff(sql_tsi_hour, "Have Vehicle", VehicleInspectionTS)
/*  
what i want IS to subtract after hours FROM that diff
after hours: 
ON hours: 
  mon - frid: 7am to midnight
  sat: 8am to 5pm
  sun: noon to 5pm
shit, going toward analyzing each day

OR how many hours within this interval are within the offtime intevals
off time intervals
mon 12:00:01 am -> 7:00:00 am
tues 12:00:01 am -> 7:00:00 am
wed 12:00:01 am -> 7:00:00 am
thru 12:00:01 am -> 7:00:00 am
frid 12:00:01 am -> 7:00:00 am
sat 12:00:01 am - > 8:00:00 am
sat 5:00:01 pm -> 12:00:00 am
sun 12:00:01 am -> 12:00:00 pm
sun 5:00:00 pm -> 12:00:00 am

find the total number of hours IN the interval that are within the off hours

subtract 24 hours for holiday

SELECT VehicleInventoryItemID, stocknumber, 
  timestampdiff(sql_tsi_hour, fromts, thruts)
FROM VehicleInventoryItems 
WHERE cast(thruts AS sql_date) > curdate() - 90
ORDER BY timestampdiff(sql_tsi_hour, fromts, thruts)
14300A, 2963e32d-4d1a-4d72-9d53-ae8ecc344c1a: owned it for 25 days

11888XA , 1102c0df-ef86-4c21-9dda-c97795c53767  acq 11/26 insp 12/06  229 hours

*/ 
  
FROM (
SELECT viix.stocknumber, viix.FromTS AS Acquired, viix.VehicleInventoryItemID, 
  CASE 
    WHEN tna.VehicleInventoryItemID IS NOT NULL THEN tna.ThruTS
    WHEN pit.VehicleInventoryItemID IS NOT NULL THEN pit.ThruTS
    ELSE viix.FromTS
  END AS "Have Vehicle",    
  vinsp.VehicleInspectionTS
FROM VehicleInventoryItems viix
INNER JOIN ( -- currently available vehicles
  SELECT VehicleInventoryItemID, fromts
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagAV'
  AND ThruTS IS NULL) av ON viix.VehicleInventoryItemID = av.VehicleInventoryItemID 
LEFT JOIN VehicleInspections vinsp ON viix.VehicleInventoryItemID = vinsp.VehicleInventoryItemID   
LEFT JOIN VehicleInventoryItemStatuses tna ON viix.VehicleInventoryItemID = tna.VehicleInventoryItemID
  AND tna.category =  'RMFlagTNA'
LEFT JOIN VehicleInventoryItemStatuses pit ON viix.VehicleInventoryItemID = pit.VehicleInventoryItemID 
  AND pit.category = 'RMFlagPIT'
WHERE viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')) wtf  