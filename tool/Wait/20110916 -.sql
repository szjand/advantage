DECLARE @Cur CURSOR AS 
  SELECT * 
  FROM #WIPBase
  WHERE VehicleInventoryItemID = @CurGroup.VehicleInventoryItemID 
  AND category <> 'AppearanceReconItem'
  ORDER BY  FromTS;   

DECLARE @CurGroup CURSOR AS   
  SELECT VehicleInventoryItemID 
  FROM #WIPBase
  GROUP BY VehicleInventoryItemID;   

DECLARE @i integer;
-- DROP TABLE #PullToPB
-- SELECT * FROM #PullToPB
-- 1 row for every day of interval(21 weeks)
-- 1 row for every vehicle that was pulled and became pb during the interval (pb within interval)
-- rydells only
-- only vehicles with a single pull, AND vehicles that became pb only once
SELECT  v.VehicleInventoryItemID, pulledTS, pbTS, da.*
INTO #PullToPB 
FROM (
-- rolling weeks
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17 
      WHEN TheDate >= CurDate() - 125 AND TheDate <= CurDate() - 119 THEN 18 
      WHEN TheDate >= CurDate() - 132 AND TheDate <= CurDate() - 120 THEN 19 
      WHEN TheDate >= CurDate() - 139 AND TheDate <= CurDate() - 127 THEN 20 
      WHEN TheDate >= CurDate() - 146 AND TheDate <= CurDate() - 134 THEN 21 
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate >= CurDate() - 146
  AND TheDate <= CurDate()) da  
-- isoweek -- this IS NOT good enough
-- need to account for year change 
/*
  SELECT *
  FROM dds.day d 
  WHERE theyear = 2011
  AND isoweek BETWEEN isoweek(curdate()) - 21 AND isoweek(curdate())) da
*/   
LEFT JOIN ( -- Pull -> PB
  SELECT *
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS PulledTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, FromTS AS pbTS
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
  WHERE viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
  AND pb.pbts IS NOT NULL -- made it ALL the way to the lot   
  AND NOT EXISTS ( -- only vehicles with a single pull
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPulled'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)
  AND NOT EXISTS ( -- only vehicles with a single pb
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND category = 'RMFlagPB'
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)) v ON da.TheDate = CAST(v.pbTS AS sql_date);
    

/*
    -- uh oh, trouble IN paradise ? MAX(ThruTS) danger danger
    -- maybe ok, move/parts/wtf can NOT be multiple with the same FromTS
      SELECT  VehicleInventoryItemID, MAX(pulledTS)AS pulledTS, MAX(pbTS) AS pbTS, startTS AS FromTS, MAX(completeTS) AS ThruTS, category
      -- one row for each vehicle/Activity/unique FromTS
      -- Activities: AppWIP, AppOtherWIP, MechWIP, BodyWIP, Move, Parts (mech only), WTF
      -- #PullToPB: vehicle AND pull->PB interval
      -- DROP TABLE #wtf
      -- INTO #wtf
      INTO #ActivityFromThru
      FROM ( 
        SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
        FROM #PullToPB j 
        LEFT JOIN (
          SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
          FROM VehicleReconItems ri
          INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
          LEFT JOIN TypCategories t ON ri.typ = t.typ
          WHERE ar.status  = 'AuthorizedReconItem_Complete'
          AND ar.startTS IS NOT NULL 
          GROUP BY VehicleInventoryItemID, ar.startts, ar.completeTS, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
            AND (b.startTS < j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS)) -- WORK started before PB AND ((WORK was completed after Pull) OR (WORK was started on or after Pull))
        WHERE b.VehicleInventoryItemID IS NOT NULL) wtf
      GROUP BY VehicleInventoryItemID, category, startTS
      UNION -- each time a vehicle IS ON the move list during the interval
        SELECT j1.VehicleInventoryItemID, j1.pulledTS, j1.pbTS, m.FromTS, m.ThruTS, 'Move'
        FROM #PullToPB j1
        LEFT JOIN VehiclesToMove m ON j1.VehicleInventoryItemID = m.VehicleInventoryItemID 
          AND (m.FromTS < j1.pbTS AND (m.ThruTS > j1.pulledTS OR m.FromTS >= j1.pulledTS)) -- move requested ON OR before PB AND ((move completed after Pull) OR (move requested on or after Pull))
        WHERE m.VehicleInventoryItemID IS NOT NULL -- don't care about a vehicle NOT ON move list during interval
      UNION -- each parts ORDER for a vehicle during the interval - a parts ORDER IS for a VehicleReconItem
        SELECT j1.VehicleInventoryItemID, j1.pulledTs, j1.pbTS, p.orderedTS, p.ReceivedTS, 'Parts'
        FROM #PullToPB j1
        LEFT JOIN (
          SELECT po.*, (SELECT VehicleInventoryItemID FROM VehicleReconItems WHERE VehicleReconItemID = po.VehicleReconItemID) AS VehicleInventoryItemID 
          FROM partsorders po
          WHERE po.CancelledTS IS NULL) p ON j1.VehicleInventoryItemID = p.VehicleInventoryItemID -- exclude Cancelled Orders
            AND (p.OrderedTS < j1.pbTS AND (p.ReceivedTS > j1.pulledTS OR p.OrderedTS >= j1.pulledTS)) -- ordered before PB AND ((received after pulled) OR (ordered ON OR after Pull))
        WHERE p.VehicleInventoryItemID IS NOT NULL 
      UNION -- each time a vehicle IS a WTF during the interval
        SELECT j1.VehicleInventoryItemID, j1.pulledTs, j1.pbTS, w.CreatedTS, w.ResolvedTS, 'WTF'
        FROM #PullToPB j1
        LEFT JOIN ViiWTF w ON j1.VehicleInventoryItemID = w.VehicleInventoryItemID
          AND (w.CreatedTS < j1.pbTS AND (w.ResolvedTS > j1.pulledTS OR w.CreatedTS >= j1.pulledTS)) -- created before PB AND ((resolved after Pull) OR (created ON OR after pull))
      WHERE w.VehicleInventoryItemID IS NOT NULL   
*/



-- right now thinking separate wip FROM move/parts/wtf, those 3 can ALL be multiple, no problem
-- the problem with multiple wip IS determining the sequence
-- so 1 TABLE of WIP times
-- AND 1 TABLE of other activity times

-- so 1 TABLE of WIP times
-- this IS WHERE i need to determine whether to consolidate OR eliminate dups
-- 1 row for each vehicle/category/FromTS
-- 9/25/11 truncate FromTS to PullTS, ThruTS to pbTS
-- DROP TABLE #AllWip
-- SELECT * FROM #AllWip
SELECT j.VehicleInventoryItemID, MAX(j.PulledTS) AS PulledTS, MAX(pbTS) AS pbTs, 
  CASE -- truncate StartTS to PulledTS
    WHEN b.startTS < j.PulledTS THEN j.PulledTS
    ELSE b.startTS
  END AS FromTS,
  MAX(
    CASE -- truncate completeTS to pbTS
      WHEN b.CompleteTS > j.pbTS THEN j.pbTS
      ELSE b.CompleteTS
    END) AS ThruTS,  
  b.category
INTO #AllWip  
FROM #PullToPB j
LEFT JOIN ( -- ALL recon WORK performed during pull->pb interval
  SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
  AND ar.startTS IS NOT NULL 
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completeTS, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))
WHERE b.VehicleInventoryItemID IS NOT null    
GROUP BY j.VehicleInventoryItemID, category, FromTS;

/* previous version -- same results
          SELECT  VehicleInventoryItemID, MAX(pulledTS)AS pulledTS, MAX(pbTS) AS pbTS,  startTS AS FromTS, MAX(completeTS) AS ThruTS, category
          INTO #AllWIP
          FROM ( 
            SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, 
              CASE -- truncate StartTS to PulledTS
                WHEN b.startTS < j.PulledTS THEN j.PulledTS
                ELSE b.startTS
              END AS StartTS, 
              CASE -- truncate completeTS to pbTS
                WHEN b.CompleteTS > j.pbTS THEN j.pbTS
                ELSE b.CompleteTS
              END AS CompleteTS,
              b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
            FROM #PullToPB j 
            LEFT JOIN (
              SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
              FROM VehicleReconItems ri
              INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
              LEFT JOIN TypCategories t ON ri.typ = t.typ
              WHERE ar.status  = 'AuthorizedReconItem_Complete'
              AND ar.startTS IS NOT NULL 
              GROUP BY VehicleInventoryItemID, ar.startts, ar.completeTS, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
                AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS)) -- WORK started before PB AND ((WORK was completed after Pull) OR (WORK was started on or after Pull))
            WHERE b.VehicleInventoryItemID IS NOT NULL) wtf
          GROUP BY VehicleInventoryItemID, category, startTS;
*/          
-- SELECT * FROM #AllWip
            /*
            -- #AllWip with no multiples
            -- unioned #AllWip with consolidated multiples
            -- 1 row per vehicle/category
            -- DROP TABLE #WipBase
            SELECT VehicleInventoryItemID, category, FromTS, ThruTS, 0 AS seq
            INTO #WIPBase
            FROM #AllWIP
            WHERE VehicleInventoryItemID NOT IN ( -- exclude all vehicles with multiples instances of Vehicle/category
              SELECT VehicleInventoryItemID  
              FROM #AllWIP
              WHERE VehicleInventoryItemID IN (
                SELECT VehicleInventoryItemID
                FROM #AllWIP
                GROUP BY VehicleInventoryItemID, category
                HAVING COUNT(*) > 1)
              AND Category IN (  
                SELECT category
                FROM #AllWIP
                GROUP BY VehicleInventoryItemID, category
                HAVING COUNT(*) > 1))
            UNION -- consolidated multiples    
            -- these are salvageable - turn multiple instances INTO one
            -- have common ThruTS, combine overlapping intervals
            -- COUNT IS 371 of a total of 384 IN #AllWIP: leaving out 13 vehicles
            SELECT VehicleInventoryItemID, category, MIN(FromTS) AS FromTS, ThruTS, 0 AS seq
            FROM #AllWIP
            WHERE VehicleInventoryItemID IN (
              SELECT VehicleInventoryItemID
              FROM (
                SELECT * 
                FROM #AllWIP
                WHERE VehicleInventoryItemID IN (
                  SELECT VehicleInventoryItemID
                  FROM #AllWIP
                  GROUP BY VehicleInventoryItemID, category
                  HAVING COUNT(*) > 1)
                AND Category IN (  
                  SELECT category
                  FROM #AllWIP
                  GROUP BY VehicleInventoryItemID, category
                  HAVING COUNT(*) > 1)) y
                GROUP BY VehicleInventoryItemID, thruts, category
                HAVING COUNT(*) > 1)
            GROUP BY VehicleInventoryItemID, category, ThruTS
            -- HAVING COUNT(*) > 1 -- this IS what was causing the none mult dept lines to be LEFT out
            -- SELECT * FROM #WipBase
            */

-- #AllWip with no multiples
-- unioned #AllWip with consolidated multiples
-- 1 row per vehicle/category
-- DROP TABLE #WipBase
-- 9/22 rework #WipBase to use temp TABLE approach to eliminate multiples that can NOT be consolidated

-- ALL multiples with those that have matching ThruTS consolidated 
-- DROP TABLE #xx
SELECT VehicleInventoryItemID, category, ThruTS, MIN(FromTS) AS FromTS
INTO #xx
FROM #AllWip 
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM #AllWIP
  GROUP BY VehicleInventoryItemID, category, ThruTS
  HAVING COUNT(*) > 1)
GROUP BY VehicleInventoryItemID, category, ThruTS;  

-- DROP TABLE #WipBase
SELECT VehicleInventoryItemID, category, FromTS, ThruTS, 0 AS seq
INTO #WIPBase
FROM #AllWIP
WHERE VehicleInventoryItemID NOT IN ( -- eliminates ALL multiples
  SELECT VehicleInventoryItemID
  FROM #AllWIP
  GROUP BY VehicleInventoryItemID, category
  HAVING COUNT(*) > 1)
UNION
SELECT VehicleInventoryItemID, category, FromTS, ThruTS, 0
FROM #xx -- ALL multiples with those that have matching ThruTS consolidated 
WHERE VehicleInventoryItemID NOT IN ( -- AND eliminate those that were NOT consolidated
  SELECT VehicleInventoryItemID
  FROM #xx
  GROUP BY VehicleInventoryItemID, category
  HAVING COUNT(*) > 1);
    


-- UPDATE seq COLUMN of #WipBase sequentially, starting with 1, based ON ViiID, FromTS
--DECLARE @Cur CURSOR AS 
--  SELECT * 
--  FROM #WIPBase
--  WHERE VehicleInventoryItemID = @CurGroup.VehicleInventoryItemID 
--  ORDER BY  FromTS; 
-- exclude oApp FROM any sequence 
-- this now give category AppearanceReconItem (oApp) a seq of 0
/*
DECLARE @Cur CURSOR AS 
  SELECT * 
  FROM #WIPBase
  WHERE VehicleInventoryItemID = @CurGroup.VehicleInventoryItemID 
  AND category <> 'AppearanceReconItem'
  ORDER BY  FromTS;   

DECLARE @CurGroup CURSOR AS   
  SELECT VehicleInventoryItemID 
  FROM #WIPBase
  GROUP BY VehicleInventoryItemID;   

DECLARE @i integer;
*/
@i = 1;
OPEN @CurGroup;
TRY 
  WHILE FETCH @CurGroup DO 
    @i = 1;
    OPEN @Cur;
    TRY 
      WHILE FETCH @Cur DO 
        UPDATE #WIPBase
        SET seq = @i
        WHERE VehicleInventoryItemID = @Cur.VehicleInventoryItemID AND category = @Cur.category AND FromTS = @Cur.FromTS;
        @i = @i + 1;
      END WHILE;
    FINALLY
      CLOSE @Cur;
    END;
  END WHILE; 
  
FINALLY
  CLOSE @CurGroup;
END;     


-- generate #ReconSeq
-- since there are no multiples LEFT
-- maximum sequence will be 4 (4 categories)
-- DROP TABLE #ReconSeq
SELECT VehicleInventoryItemID,  
  trim(
    coalesce(
      MAX((
        SELECT 
          CASE category
--            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END 
        FROM #WipBase WHERE seq = 1 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),''))
  + 
  trim(
    coalesce(
      MAX((
        SELECT 
          CASE category
--            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END         
        FROM #WipBase WHERE seq = 2 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),''))
  + 
  trim(
    coalesce(
      MAX((
        SELECT 
          CASE category
--            WHEN 'AppearanceReconItem' THEN 'o' 
            WHEN 'BodyReconItem' THEN 'b'
            WHEN 'MechanicalReconItem' THEN 'm'
            WHEN 'PartyCollection' THEN 'a'
          END          
        FROM #WipBase WHERE seq = 3 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),'')) AS ReconSeq
--  + 
--  trim(
--    coalesce(
--      MAX((
--      SELECT 
--          CASE category
--            WHEN 'AppearanceReconItem' THEN 'o' 
--            WHEN 'BodyReconItem' THEN 'b'
--            WHEN 'MechanicalReconItem' THEN 'm'
--            WHEN 'PartyCollection' THEN 'a'
--          END        
--      FROM #WipBase WHERE seq = 4 AND VehicleInventoryItemID = w.VehicleInventoryItemID AND category = w.category)),'')) AS ReconSeq
INTO #ReconSeq      
FROM #WipBase w
GROUP BY VehicleInventoryItemID;
-- DROP TABLE #ReconSeq



-- #OtherActivites
-- 1 row/vehicle/activity/fromTS :: each instance of parts/move/wtf
-- 9/25: iif(FromTS < PulledTS, pulledTS, FromtS)  iff(ThruTS > pbTS, pbTS, ThruTS)
-- DROP TABLE #OtherActivities
SELECT j1.VehicleInventoryItemID, 
  CASE
    WHEN m.FromTS < PulledTS THEN PulledTS
    ELSE m.FromTS
  END AS FromTS,
  CASE
    WHEN m.ThruTS > pbTS THEN pbTS
    ELSE m.ThruTS
  END AS ThruTS,
  'Move' AS Activity
INTO #OtherActivities
FROM #PullToPB j1
LEFT JOIN VehiclesToMove m ON j1.VehicleInventoryItemID = m.VehicleInventoryItemID 
  AND (m.FromTS < j1.pbTS AND (m.ThruTS > j1.pulledTS OR m.FromTS >= j1.pulledTS)) -- move requested ON OR before PB AND ((move completed after Pull) OR (move requested on or after Pull))
WHERE m.VehicleInventoryItemID IS NOT NULL -- don't care about a vehicle NOT ON move list during interval
UNION -- each parts ORDER for a vehicle during the interval - a parts ORDER IS for a VehicleReconItem
SELECT j1.VehicleInventoryItemID, 
  CASE
    WHEN p.orderedTS < PulledTS THEN PulledTS
    ELSE p.orderedTS 
  END,
  CASE
    WHEN p.receivedTS > pbTS THEN pbTS
    ELSE p.receivedTS
  END, 
  'Parts'
FROM #PullToPB j1
LEFT JOIN (
  SELECT po.*, (SELECT VehicleInventoryItemID FROM VehicleReconItems WHERE VehicleReconItemID = po.VehicleReconItemID) AS VehicleInventoryItemID 
  FROM partsorders po
  WHERE po.CancelledTS IS NULL) p ON j1.VehicleInventoryItemID = p.VehicleInventoryItemID -- exclude Cancelled Orders
    AND (p.OrderedTS < j1.pbTS AND (p.ReceivedTS > j1.pulledTS OR p.OrderedTS >= j1.pulledTS)) -- ordered before PB AND ((received after pulled) OR (ordered ON OR after Pull))
WHERE p.VehicleInventoryItemID IS NOT NULL 
UNION -- each time a vehicle IS a WTF during the interval
SELECT j1.VehicleInventoryItemID, 
  CASE
    WHEN w.CreatedTS < PulledTS THEN PulledTS
    ELSE w.CreatedTS
  END,
  CASE
    WHEN w.ResolvedTS > pbTS THEN pbTS
    ELSE w.ResolvedTS
  END,
  'WTF'
FROM #PullToPB j1
LEFT JOIN ViiWTF w ON j1.VehicleInventoryItemID = w.VehicleInventoryItemID
  AND (w.CreatedTS < j1.pbTS AND (w.ResolvedTS > j1.pulledTS OR w.CreatedTS >= j1.pulledTS)) -- created before PB AND ((resolved after Pull) OR (created ON OR after pull))
WHERE w.VehicleInventoryItemID IS NOT NULL;  

--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
/*
-- how many AND which vehicles have AppOther
SELECT a.VehicleInventoryItemID, a.FromTS AS WipFrom, a.ThruTS AS WipThru, 
  a.category, r. ReconSeq --, o.activity, o.FromTS AS ActFromTS, o.ThruTS AS ActThruTS 
FROM #WipBase a
--INNER JOIN #OtherActivities o ON a.VehicleInventoryItemID = o.VehicleInventoryItemID
INNER JOIN #ReconSeq r ON a.VehicleInventoryItemID = r.VehicleInventoryItemID 
WHERE ReconSeq LIKE '%o%'
AND category IN ('AppearanceReconItem', 'PartyCollection')
ORDER BY a.VehicleInventoryItemID, r.ReconSeq --, ActFromTS
-- AppOther IN sequence
SELECT DISTINCT POSITION('o' IN ReconSeq)
FROM #WipBase a
INNER JOIN #OtherActivities o ON a.VehicleInventoryItemID = o.VehicleInventoryItemID
INNER JOIN #ReconSeq r ON a.VehicleInventoryItemID = r.VehicleInventoryItemID 
WHERE ReconSeq LIKE '%o%'
AND category IN ('AppearanceReconItem', 'PartyCollection')


fucking have to deal with AppOther
IF it's first, followed BY mech:: deducts FROM mech time, but goes IN total appearance time
IF it's last OK
IF it's next to a ok
-- narrow this down, look at the overlap BETWEEN app AND appother
-- so, looking at sorting BY FromTS AND THEN ThruTS
-- no 2 start at the same time, but, 1 veh does END at the same time
SELECT a.VehicleInventoryItemID, a.FromTS AS WipFrom, a.ThruTS AS WipThru, category
FROM #WipBase a
WHERE EXISTS (
  SELECT 1
  FROM #WipBase
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND category = 'AppearanceReconItem')
AND NOT EXISTS (
  SELECT 1
  FROM #WipBase
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
  AND category = 'PartyCollection')  
ORDER BY a.VehicleInventoryItemID, a.ThruTS

SELECT * FROM #WipBase WHERE VehicleInventoryItemID = '8bc7edfc-5b5b-4ef8-a06a-58295d4de4d3'

SELECT * 
FROM #allWip 
GROUP BY VehicleInventoryItemID  
SELECT * FROM #WipBase       

-- this seems to be a larger exclusion than i thought
SELECT COUNT(*) FROM #PullToPB -- 436

SELECT COUNT(*) FROM #PullToPB -- aha, 407, that's more LIKE what i expected
WHERE VehicleInventoryItemID IS NOT NULL 

SELECT COUNT(DISTINCT VehicleInventoryItemID) FROM #WipBase  -- 370

SELECT * 
FROM #PullToPB p

-- this flattens it
SELECT p.VehicleInventoryItemID, MAX(p.PulledTS) AS pulledTS, MAX(p.pbTS) AS pbTS, 
  MAX(CASE WHEN category = 'AppearanceReconItem' THEN FromTS END) AS AppOtherFromts, 
  MAX(CASE WHEN category = 'AppearanceReconItem' THEN ThruTS END) AS AppOtherThruTS,
  MAX(CASE WHEN category = 'BodyReconItem' THEN FromTS END) AS BodyFromts, 
  MAX(CASE WHEN category = 'BodyReconItem' THEN ThruTS END) AS BodyThruTS,
  MAX(CASE WHEN category = 'MechanicalReconItem' THEN FromTS END) AS MechFromts, 
  MAX(CASE WHEN category = 'MechanicalReconItem' THEN ThruTS END) AS MechThruTS,
  MAX(CASE WHEN category = 'PartyCollection' THEN FromTS END) AS AppFromts, 
  MAX(CASE WHEN category = 'PartyCollection' THEN ThruTS END) AS AppThruTS
FROM #PullToPB p
INNER JOIN #WipBase w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
GROUP BY p.VehicleInventoryItemID 


-- 1 row per veh/category
SELECT p.VehicleInventoryItemID, p.PulledTS, p.pbTS, w.*
FROM #PullToPB p
INNER JOIN #WipBase w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
ORDER BY p.VehicleInventoryItemID, w.seq

-- vehicle COUNT
-- oops, can't DO the week stuff with the INNER JOIN, get goofy BEGIN END dates
-- but i need to exclude those vehicles FROM the COUNT

SELECT TheWeek, 
  COUNT(DISTINCT p.VehicleInventoryItemID) AS [Count],
  SUM(timestampdiff(sql_tsi_day, cast(p.PulledTS AS sql_date), cast(p.pbTS AS sql_date)))/COUNT(DISTINCT p.VehicleInventoryItemID) AS [Avg Days/Vehicle],
  SUM(timestampdiff(sql_tsi_hour, p.PulledTS, p.pbTS))/COUNT(DISTINCT p.VehicleInventoryItemID) AS [Avg Hours/Vehicle]
FROM #PullToPB p
INNER JOIN #WipBase w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
GROUP BY TheWeek


-- ALL counts: Vehicles, depts
SELECT d.TheWeek, [From], [Thru], [count],[Avg Days/Vehicle], [Avg BusHours/Vehicle],
  mbc.MechCount, mbc.BodyCount, a.AppCount 
INTO #Counts  
FROM ( -- include ALL days for generating sensible FROM AND Thru dates for the week
  SELECT TheWeek, MIN(TheDate) AS [From], MAX(TheDate) AS [Thru]
  FROM #PullToPB p
  GROUP BY TheWeek) d
LEFT JOIN ( -- dates AND vehicle counts 
  SELECT TheWeek, 
    COUNT(DISTINCT p.VehicleInventoryItemID) AS [Count],
    SUM(timestampdiff(sql_tsi_day, cast(p.PulledTS AS sql_date), cast(p.pbTS AS sql_date)))/COUNT(DISTINCT p.VehicleInventoryItemID) AS [Avg Days/Vehicle],
    SUM((select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Detail') FROM system.iota))/COUNT(DISTINCT p.VehicleInventoryItemID) AS [Avg BusHours/Vehicle]
  FROM #PullToPB p
  INNER JOIN ( -- restrict to those without multiples
    SELECT DISTINCT VehicleInventoryItemID
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
  GROUP BY TheWeek) c ON d.TheWeek = c.TheWeek
LEFT JOIN ( -- mech AND body count
  SELECT p.TheWeek,
    SUM(CASE WHEN category = 'MechanicalReconItem' THEN 1 ELSE 0 END) AS MechCount,
    SUM(CASE WHEN category = 'BodyReconItem' THEN 1 ELSE 0 END) AS BodyCount 
  FROM #PullToPB p
  LEFT JOIN #WipBase w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  GROUP BY p.TheWeek) mbc ON d.TheWeek = mbc.TheWeek
LEFT JOIN (-- app COUNT
  SELECT p.TheWeek, COUNT(*) AS AppCount 
  FROM #PullToPB p
  INNER JOIN (-- must be an INNER JOIN to eliminate vehicles without appearance
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase
    WHERE category IN ('AppearanceReconItem','PartyCollection')) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  GROUP BY p.TheWeek) a ON d.TheWeek = a.TheWeek  
  
-- WIP times Body/Mech
SELECT * FROM #WipBase
select * FROM #AllWip WHERE VehicleInventoryItemID = '14e9259a-3343-4fdb-b89a-ab51b3ffbe59'

SELECT a.VehicleInventoryItemID,
  coalesce(
    SUM(
      CASE
        WHEN category = 'MechanicalReconItem' THEN 
          CASE  
            WHEN FromTS < pulledTS AND ThruTS > pulledTS AND ThruTS <= pbTS THEN 
              CASE -- WORK started before pull AND finished after pull AND finished before OR ON pb 
                WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1 -- (ThruTS - FromTS)
                ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, ThruTS, 'Service') FROM system.iota)
              END 
            WHEN FromTS < pulledTS AND ThruTS >= pbTS THEN 
              CASE -- WORK started before pull AND finished ON OR after pb
                WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
                ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota) -- (pbTS - pulledTS)
              END  
            WHEN FromTS >= pulledTS AND FromTS < pbTS AND ThruTS <= pbTS THEN 
              CASE -- WORK started ON OR after pull AND started before pb AND finished before OR ON pb
                WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
                ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota) -- (ThruTS - FromTS)
              END
            WHEN FromTS >= pulledTS AND pulledTS < pbTS AND ThruTS >= pbTS THEN -- what the fuck, WHEN IS pull ever < pb? (feels LIKE safety)
              CASE -- WORK started ON OR after pull (AND pull < pb) AND finished ON OR after pb
                WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
                ELSE (select Utilities.BusinessHoursFromInterval(FromTS, pbTS, 'Service') FROM system.iota) -- (pb - FromTS)
              END 
          END
      END), 0) AS MechBusHours,
  coalesce(
    SUM(
      CASE
        WHEN category = 'BodyReconItem' THEN 
          CASE  
            WHEN FromTS < pulledTS AND ThruTS > pulledTS AND ThruTS <= pbTS THEN 
              CASE 
                WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
                ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, ThruTS, 'BodyShop') FROM system.iota)
              END 
            WHEN FromTS < pulledTS AND ThruTS >= pbTS THEN 
              CASE
                WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
                ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'BodyShop') FROM system.iota)
              END  
            WHEN FromTS >= pulledTS AND FromTS < pbTS AND ThruTS <= pbTS THEN 
              CASE 
                WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
                ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'BodyShop') FROM system.iota)
              END
            WHEN FromTS >= pulledTS AND pulledTS < pbTS AND ThruTS >= pbTS THEN 
              CASE
                WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
                ELSE (select Utilities.BusinessHoursFromInterval(FromTS, pbTS, 'BodyShop') FROM system.iota)
              END 
          END
      END), 0) AS BodyBusHours 
INTO #bmWIPHours 
FROM #AllWip a -- using allwip to include pullTS AND pbTS
WHERE EXISTS ( -- exclude multiples
  SELECT 1
  FROM #WipBase
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)
AND category in ('MechanicalReconItem', 'BodyReconItem')
GROUP BY VehicleInventoryItemID        

-- now have to DO wip times for app

-- how many AND which vehicles have AppOther
SELECT a.VehicleInventoryItemID, a.FromTS AS WipFrom, a.ThruTS AS WipThru, 
  a.category, r. ReconSeq --, o.activity, o.FromTS AS ActFromTS, o.ThruTS AS ActThruTS 
FROM #WipBase a
--INNER JOIN #OtherActivities o ON a.VehicleInventoryItemID = o.VehicleInventoryItemID
INNER JOIN #ReconSeq r ON a.VehicleInventoryItemID = r.VehicleInventoryItemID 
WHERE ReconSeq LIKE '%o%'
AND category IN ('AppearanceReconItem', 'PartyCollection')
ORDER BY a.VehicleInventoryItemID, r.ReconSeq --, ActFromTS
-- AppOther IN sequence
SELECT DISTINCT POSITION('o' IN ReconSeq)
FROM #WipBase a
INNER JOIN #OtherActivities o ON a.VehicleInventoryItemID = o.VehicleInventoryItemID
INNER JOIN #ReconSeq r ON a.VehicleInventoryItemID = r.VehicleInventoryItemID 
WHERE ReconSeq LIKE '%o%'
AND category IN ('AppearanceReconItem', 'PartyCollection')


fucking have to deal with AppOther
IF it's first, followed BY mech:: deducts FROM mech time, but goes IN total appearance time
IF it's last OK
IF it's next to a ok
-- narrow this down, look at the overlap BETWEEN app AND appother
-- so, looking at sorting BY FromTS AND THEN ThruTS
-- no 2 start at the same time, but, 1 veh does END at the same time
-- ok, each of these vehicles has wip IN AppRI AND PartyColl
SELECT * FROM #wipbase

SELECT a.VehicleInventoryItemID, a.FromTS AS WipFrom, a.ThruTS AS WipThru, category
FROM #WipBase a
WHERE EXISTS (
  SELECT 1
  FROM #WipBase
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND category = 'AppearanceReconItem')
AND EXISTS (
  SELECT 1
  FROM #WipBase
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
  AND category = 'PartyCollection')  
AND category IN ('AppearanceReconItem', 'PartyCollection')  
ORDER BY a.VehicleInventoryItemID, a.ThruTS

SELECT a.VehicleInventoryItemID, -- a.FromTS AS WipFrom, a.ThruTS AS WipThru, category
  MAX(CASE WHEN category = 'AppearanceReconItem' THEN FromTS END) AS oFromTS,
  MAX(CASE WHEN category = 'AppearanceReconItem' THEN ThruTS END) AS oThruTS,
  MAX(CASE WHEN category = 'PartyCollection' THEN FromTS END) AS aFromTS,
  MAX(CASE WHEN category = 'PartyCollection' THEN ThruTS END) AS aThruTS
FROM #WipBase a
WHERE EXISTS (
  SELECT 1
  FROM #WipBase
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
  AND category = 'AppearanceReconItem')
AND EXISTS (
  SELECT 1
  FROM #WipBase
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
  AND category = 'PartyCollection')  
AND category IN ('AppearanceReconItem', 'PartyCollection')  
GROUP BY VehicleInventoryItemID 


  

SELECT a.VehicleInventoryItemID,
  coalesce(
    SUM(
      CASE  
        WHEN FromTS < pulledTS AND ThruTS > pulledTS AND ThruTS <= pbTS THEN 
          CASE -- WORK started before pull AND finished after pull AND finished before OR ON pb 
            WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1 -- (ThruTS - FromTS)
            ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, ThruTS, 'Detail') FROM system.iota)
          END 
        WHEN FromTS < pulledTS AND ThruTS >= pbTS THEN 
          CASE -- WORK started before pull AND finished ON OR after pb
            WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
            ELSE (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Detail') FROM system.iota) -- (pbTS - pulledTS)
          END  
        WHEN FromTS >= pulledTS AND FromTS < pbTS AND ThruTS <= pbTS THEN 
          CASE -- WORK started ON OR after pull AND started before pb AND finished before OR ON pb
            WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
            ELSE (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Detail') FROM system.iota) -- (ThruTS - FromTS)
          END
        WHEN FromTS >= pulledTS AND pulledTS < pbTS AND ThruTS >= pbTS THEN -- what the fuck, WHEN IS pull ever < pb? (feels LIKE safety)
          CASE -- WORK started ON OR after pull (AND pull < pb) AND finished ON OR after pb
            WHEN timestampdiff(sql_tsi_hour, FromTS, ThruTS) < 1 THEN 1
            ELSE (select Utilities.BusinessHoursFromInterval(FromTS, pbTS, 'Detail') FROM system.iota) -- (pb - FromTS)
          END 
      END), 0) AS AppBusHours
FROM #AllWip a 
WHERE EXISTS (
  SELECT 1
  FROM #WipBase
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)
AND category in ('AppearanceReconItem', 'PartyCollection')  
AND VehicleInventoryItemID NOT IN ( -- excludes those vehicles with wip IN both app categories
  SELECT a.VehicleInventoryItemID
  FROM #WipBase a
  WHERE EXISTS (
    SELECT 1
    FROM #WipBase
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
    AND category = 'AppearanceReconItem')
  AND EXISTS (
    SELECT 1
    FROM #WipBase
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
    AND category = 'PartyCollection')  
  AND category IN ('AppearanceReconItem', 'PartyCollection')  
  GROUP BY VehicleInventoryItemID)
GROUP BY VehicleInventoryItemID    
UNION 
-- total app wip hours for vehicles with both categories of appearance wip
-- no comparisons to pull OR pb for appearance
SELECT VehicleInventoryItemID, 
  CASE 
    -- non overlapping
    WHEN oThruTS <= aFromTS or aThruTS <= oFromTS THEN iif(timestampdiff(sql_tsi_hour, oFromTS, oThruTS) < 1, 1, (select Utilities.BusinessHoursFromInterval(oFromTS, oThruTS, 'Detail') FROM system.iota)) 
      +  iif(timestampdiff(sql_tsi_hour, aFromTS, aThruTS) < 1, 1, (select Utilities.BusinessHoursFromInterval(aFromTS, aThruTS, 'Detail') FROM system.iota))
    -- containing  
    -- a IS container 
    WHEN oFromTS >= aFromTS AND oThruTS <= aThruTS THEN iif(timestampdiff(sql_tsi_hour, aFromTS, aThruTS) < 1, 1, (select Utilities.BusinessHoursFromInterval(aFromTS, aThruTS, 'Detail') FROM system.iota))
    -- o IS container 
    WHEN aFromTS >= oFromTS AND aThruTS <= oThruTS THEN iif(timestampdiff(sql_tsi_hour, oFromTS, oThruTS) < 1, 1, (select Utilities.BusinessHoursFromInterval(oFromTS, oThruTS, 'Detail') FROM system.iota))    
    -- overlap
    WHEN oFromTS < aFromTS AND oThruTS > aFromTS THEN iif(timestampdiff(sql_tsi_hour, oFromTS, aThruTS) < 1, 1, (select Utilities.BusinessHoursFromInterval(oFromTS, aThruTS, 'Detail') FROM system.iota))
    WHEN aFromTS < oFromTS AND aThruTS > oFromTS THEN iif(timestampdiff(sql_tsi_hour, aFromTS, oThruTS) < 1, 1, (select Utilities.BusinessHoursFromInterval(aFromTS, oThruTS, 'Detail') FROM system.iota))
  END AS AppBusHours
FROM ( -- vehicles FROM #WipBase with both categories of appearance
  SELECT a.VehicleInventoryItemID, 
    MAX(CASE WHEN category = 'AppearanceReconItem' THEN FromTS END) AS oFromTS,
    MAX(CASE WHEN category = 'AppearanceReconItem' THEN ThruTS END) AS oThruTS,
    MAX(CASE WHEN category = 'PartyCollection' THEN FromTS END) AS aFromTS,
    MAX(CASE WHEN category = 'PartyCollection' THEN ThruTS END) AS aThruTS
  FROM #WipBase a
  WHERE EXISTS (
    SELECT 1
    FROM #WipBase
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
    AND category = 'AppearanceReconItem')
  AND EXISTS (
    SELECT 1
    FROM #WipBase
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
    AND category = 'PartyCollection')  
  AND category IN ('AppearanceReconItem', 'PartyCollection')  
  GROUP BY VehicleInventoryItemID) x 
  
  
-----------------------

-- ok, here's the deal, o takes away FROM the wait time for the dept it precedes
-- AND contributes to the total App Wip timess
SELECT w.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts, w.FromTS as oFrom, 
  w.ThruTS as oThru, m.FromtS as mFrom, m.ThruTS as mThru, b.FromTs AS bFrom, b.ThruTS AS bThru
FROM #WipBase w
LEFT JOIN #PullToPB p ON w.VehicleInventoryItemID = p.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON w.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #WipBase b ON w.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
LEFT JOIN #WipBase m ON w.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
WHERE w.category = 'AppearanceReconItem'

-- this IS NOT what i want
-- 1 row for each vehicle/category
SELECT w.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts, 
  CASE WHEN w.category = 'MechanicalReconItem' THEN w.FromTS END AS mFrom,
  CASE WHEN w.category = 'MechanicalReconItem' THEN w.ThruTS END AS mThru,
  CASE WHEN w.category = 'BodyReconItem' THEN w.FromTS END AS bFrom,
  CASE WHEN w.category = 'BodyReconItem' THEN w.ThruTS END AS bThru,
  CASE WHEN w.category = 'PartyCollection' THEN w.FromTS END AS aFrom,
  CASE WHEN w.category = 'PartyCollection' THEN w.ThruTS END AS aThru,
  CASE WHEN w.category = 'AppearanceReconItem' THEN w.FromTS END AS oFrom,
  CASE WHEN w.category = 'AppearanceReconItem' THEN w.ThruTS END AS oThru 
FROM #WipBase w
LEFT JOIN #PullToPB p ON w.VehicleInventoryItemID = p.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON w.VehicleInventoryItemID = s.VehicleInventoryItemID 

-- what i want IS 1 row per vehicle
-- check for dups -- check OK 
-- *1* what's up with p records with no s records?
-- IS this an issue of excluded vehicles NOT being excluded
-- yep, that's it exactly
-- INNER JOIN #WipBase to #PullToPB to exclude vehicles
-- SELECT COUNT(*) FROM #PullToPB WHERE VehicleInventoryItemID IS NOT NULL -- 528
-- SELECT COUNT(DISTINCT VehicleInventoryItemID) FROM #WipBase -- 483
-- so 45 of 528 vehicles are excluded
SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
  m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
  a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFromt, o.ThrutS AS oThru
FROM #PullToPB p 
INNER JOIN ( -- exclusions FROM multiple wip/category
  SELECT DISTINCT VehicleInventoryItemID 
  FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
WHERE p.VehicleInventoryItemID IS NOT NULL 
--

SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
  m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
  a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFromt, o.ThrutS AS oThru,
  oa.FromTS, oa.ThrutS, oa.Activity
FROM #PullToPB p 
INNER JOIN ( -- exclusions FROM multiple wip/category
  SELECT DISTINCT VehicleInventoryItemID 
  FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
LEFT JOIN #OtherActivities oa ON p.VehicleInventoryItemID = oa.VehicleInventoryItemID 
WHERE p.VehicleInventoryItemID IS NOT NULL 

SELECT * FROM #OtherActivities
SELECT COUNT(*) FROM #OtherActivities  -- 833
-- this gives 832 which makes no sense
-- i am assuming there should be at least several dup parts orders
SELECT COUNT(*) FROM(
SELECT VehicleInventoryItemID, MIN(FromTS), ThruTS, Activity
FROM #OtherActivities
GROUP BY VehicleInventoryItemID, ThruTS, Activity)c
-- so dissect parts IN #OtherActivites
SELECT * FROM #OtherActivities WHERE activity = 'Parts'
SELECT COUNT(*) FROM #OtherActivities WHERE activity = 'Parts' -- 113
SELECT COUNT(*) FROM ( -- there IS only one, oh well, no big deal, but go ahead AND consolidate it
  SELECT VehicleInventoryItemID, ThruTS
  FROM #OtherActivities
  WHERE Activity = 'Parts'
  GROUP BY VehicleInventoryItemID, ThruTS
  HAVING Count(*) > 1) s
  
-- ok, hammer away at mech, parts first  
SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
  m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
  a.FromTS AS aFrom, a.ThruTS AS aThru, o.FromTS AS oFromt, o.ThrutS AS oThru,
  oa.FromTS, oa.ThrutS, oa.Activity
FROM #PullToPB p 
INNER JOIN ( -- exclusions FROM multiple wip/category
  SELECT DISTINCT VehicleInventoryItemID 
  FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
LEFT JOIN (
  SELECT VehicleInventoryItemID, MIN(FromTS) AS FromTS, ThruTS, Activity
  FROM #OtherActivities 
  WHERE Activity = 'Parts'
  GROUP BY VehicleInventoryItemID, ThruTS, Activity) oa ON p.VehicleInventoryItemID = oa.VehicleInventoryItemID 
WHERE p.VehicleInventoryItemID IS NOT NULL 
AND ReconSeq LIKE '%m%'


-- 
-- Total mechanical time
SELECT x.*, 
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, mThru, 'Service') FROM system.iota)
    WHEN 2 THEN -- *m*
      CASE
        -- bm*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
        -- am*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
      END 
    WHEN 3 THEN -- **m
      CASE
        -- *bm
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
        -- *am
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
      END     
  END AS MechBusHours
FROM ( -- one row per vehicle
  SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru--, o.FromTS AS oFromt, o.ThrutS AS oThru
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
--  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL 
  AND ReconSeq LIKE '%m%') x -- mechanical only

-- total mech hours
-- mWip hours
-- parts  
SELECT x.*,
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, mThru, 'Service') FROM system.iota)
    WHEN 2 THEN -- *m*
      CASE
        -- bm*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
        -- am*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
      END 
    WHEN 3 THEN -- **m
      CASE
        -- *bm
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
        -- *am
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
      END     
  END AS mBusHoursTotal,
  w.MechBusHours AS mWipBusHours,
  p.fromts, p.thruts, p.activity
FROM ( -- one row per vehicle
  SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru--, o.FromTS AS oFromt, o.ThrutS AS oThru
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
--  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL 
  AND ReconSeq LIKE '%m%') x -- mechanical only
LEFT JOIN #bmWIpHours w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID
LEFT JOIN (
  SELECT VehicleInventoryItemID, MIN(FromTS) AS FromTS, ThruTS, Activity
  FROM #OtherActivities 
  WHERE Activity = 'Parts'
  GROUP BY VehicleInventoryItemID, ThruTS, Activity) p ON x.VehicleInventoryItemID = p.VehicleInventoryItemID  
  
  
-- total mech hours
-- mWip hours
-- parts  
-- instead of just parts, ALL other activities
SELECT x.*,
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, mThru, 'Service') FROM system.iota)
    WHEN 2 THEN -- *m*
      CASE
        -- bm*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
        -- am*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
      END 
    WHEN 3 THEN -- **m
      CASE
        -- *bm
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
        -- *am
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
      END     
  END AS mBusHoursTotal,
  w.MechBusHours AS mWipBusHours,
  p.fromts, p.thruts, p.activity,
  CASE 
    WHEN Activity = 'Move' THEN 
      CASE position ('m' IN ReconSeq)
        WHEN 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, mFrom, 'Service') FROM system.iota)
      END 
  END AS mMove
FROM ( -- one row per vehicle
  SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru--, o.FromTS AS oFromt, o.ThrutS AS oThru
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
--  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL 
  AND ReconSeq LIKE '%m%') x -- mechanical only
LEFT JOIN #bmWIpHours w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID
LEFT JOIN (
  SELECT VehicleInventoryItemID, MIN(FromTS) AS FromTS, ThruTS, Activity
  FROM #OtherActivities 
  GROUP BY VehicleInventoryItemID, ThruTS, Activity) p ON x.VehicleInventoryItemID = p.VehicleInventoryItemID    



  
-- grouping  
SELECT x.VehicleInventoryItemID, MAX(ReconSeq) AS seq,
  MAX(
    CASE position('m' IN ReconSeq) -- m**
      WHEN 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, mThru, 'Service') FROM system.iota)
      WHEN 2 THEN -- *m*
        CASE
          -- bm*
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
          -- am*
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
        END 
      WHEN 3 THEN -- **m
        CASE
          -- *bm
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
          -- *am
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
        END     
    END) AS mBusHoursTotal,
  max(w.MechBusHours) AS mWipBusHours
FROM ( -- one row per vehicle
  SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru--, o.FromTS AS oFromt, o.ThrutS AS oThru
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
--  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL 
  AND ReconSeq LIKE '%m%') x -- mechanical only
LEFT JOIN #bmWIpHours w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID
GROUP BY x.VehicleInventoryItemID


  
-- grouping  
-- build ON to grouping
SELECT x.VehicleInventoryItemID, MAX(ReconSeq) AS seq,
  MAX(
    CASE position('m' IN ReconSeq) -- m**
      WHEN 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, mThru, 'Service') FROM system.iota)
      WHEN 2 THEN -- *m*
        CASE
          -- bm*
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
          -- am*
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
        END 
      WHEN 3 THEN -- **m
        CASE
          -- *bm
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
          -- *am
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
        END     
    END) AS mBusHoursTotal,
  max(w.MechBusHours) AS mWipBusHours,
  
  SUM(
    CASE 
      WHEN LEFT(ReconSeq, 1) = 'm' THEN 
        CASE
          WHEN FromTS < mFrom THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
      WHEN substring(ReconSeq, 2, 1) = 'm' THEN
        CASE   
          WHEN LEFT(ReconSeq, 1) = 'b' THEN 
            CASE
              WHEN FromTS >= bThru AND FromTS < mFrom THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
          WHEN LEFT(ReconSeq, 1) = 'a' THEN 
            CASE
              WHEN FromTS >= aThru AND FromTS < mFrom THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
        END
      WHEN ReconSeq = 'bam' THEN
        CASE 
          WHEN FromTS >= aThru AND FromTS < mFrom THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END  
      WHEN ReconSeq = 'abm' THEN 
        CASE
          WHEN FromTS >= bThru AND FromTS < mFrom THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
      END) AS mMove

-- SELECT *      
FROM ( -- one row per vehicle
  SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru--, o.FromTS AS oFromt, o.ThrutS AS oThru
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
--  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL 
  AND ReconSeq LIKE '%m%') x -- mechanical only
LEFT JOIN #bmWIpHours w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID
LEFT JOIN ( -- move only
  SELECT VehicleInventoryItemID, MIN(FromTS) AS FromTS, ThruTS, Activity
  FROM #OtherActivities 
  WHERE Activity = 'Move'
  GROUP BY VehicleInventoryItemID, ThruTS, Activity) p ON x.VehicleInventoryItemID = p.VehicleInventoryItemID    

GROUP BY x.VehicleInventoryItemID




  SELECT VehicleInventoryItemID, MIN(FromTS) AS FromTS, ThruTS, Activity
  FROM #OtherActivities 
  GROUP BY VehicleInventoryItemID, ThruTS, Activity
*/  