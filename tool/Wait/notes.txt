/* 8/24/12
Honda: Lear thinks body shop is why vehicles take so long to get to the lot
e:\sql\wait\PullToPB.sql (copy of 20110916.sql)
just run the whole fucking thing
ok, this generates: #pulltopb
                    #wipbase
                    #allwip
                    #reconseq
                    #otheractivities
                    #xx

e:\sql\wait\Level1.sql
to populate XmRLevel1.xlsx page Level 1 Days
which shows me avg days per vehicle going back 21 weeks



*/
/*
02/21/2012
open 20110916.sql and simply run it

LevelBase.sql
first run the entire commented section at top of file
-- Pull -> PB XmR chart
-- AverageHoursPerVehicle by Week

paste values into XmrLevel1.Leve 1 AvgHoursPerVeh

Level1.sql -> Xmr Level1.xlsx


*/

Last 90 days at Rydells
pull to pb
vehicles with one pull only
 ** and each vehicle in wip in each dept no more than once ** this has to go away
and wip is started and stopped in the pull-pb interval

the query that generates #ReconWIP (in ExtractWIP.sql)
limits the WIP to that which occurred during the pull-pb interval

so now, i have a clean request
for a call to a business hour function
no need to be concerned about overlapping intervals
that's already handled

2 orthogonal issues: interval overlap, interval business hours
function to handle the bus hours interval only

ALL INTERVALS ARE DAY/HOUR

do the defining for setup
run which queries to generate what temp tables
ExtractWIP.sql
  commented in header: creates #wip
  main query creates #ReconWip


Things to know about SQL time shit
  using timestampdiff(sql_tsi_day, @ts1, @ts2), IF difference IS less than 24 hours, diff returns 0
  so use: dayofyear(@ts2) - dayofyear(@ts1) to determine number of days enclosed IN the interval
  fuck, that's wrong too, day of year won't WORK across years
  but this works: timestampdiff(sql_tsi_day, CAST(@ts1 AS sql_date), CAST(@ts2 AS sql_date))

Put Together 2 Group By Week.sql: use dds.day as a derived table, with the added column of week, as the base table
this enables grouping by week

8/23/11
ViiStatuses won't work for recon: if ari.startts = ari.completets no status of _InProcess is generated
So, use ari with status = complete
the catch is, before August 2010,  ari.starts & ari.completets are null
-- 
wip: if started before interval, include, but make the bushours from start of interval, not when started
--
need to pad wip (mostly mechanical, i'm thinking) where start=complete (0 time), and also expose the frequency

8/24
Pull to PB.sql is now the base query
8/25
where are the discrete parts for: business hours/mintes/seconds
                                  determining overlap
currently at Pull to PB.sql: gives me interval and data relevant for that interval, by virtue
                             of the day table being the base table
                             currently returns one row for each vehicle and day
                             for which that vehicle arrived at pricing buffer
                             data: day(all info), viiid, stocknumber, pulledTS, pbTS,
                                   how many bushours for that vehicle on that day in wtf or move
                                   where that vehicle had work done in m/b/a
                             
 & ReconBaseTable.sql

-- shit, fucked up recon, again
should be (total WIP hours WITHIN interval)/(# of vehicles with WIP WITHIN interval)
8/26
THE ISSUE is to look at data for the defined interval only
so, mech count, is only those vehicles that go into wip during the interval

so where am i
Pull to PB is an interval query/table,
and need to return only the vehicles and the interval
the interval table, for now, is #jon
Then
tables/queries to determine information about that interval
recon count
recon wip bushours
move list bushours
wtf bushours
parts hold bushours

8/27
Pull to PB.sql: generates #jon (base interval) (day left join vehicle)
ExtractWIP3.sql: generates #ReconWIP
MoveList.sql: generates #move
recon wait times.sql: generates #WipStartTimes

8/28
parts hold.sql
1. pull to pb.sql : creates #jon (base interval)
2. recon wait times.sql: generates #WipStartTimes :: 
3. parts hold.sql : generates #MechParts

8/29
parts hold2.sql
totally rocks with the sql from stackoverflow
all these numbers need to be dept specific
#jon (pull to pb.sql)
#ReconWip (ExtractWIP3.sql)
#ReconWait (Recon Wait Times.sql)
#WipStartTimes (Recon Wait Times.sql)
#MechParts (parts hold2.sql)
#Orders (parts hold2.sql)

8/30
fucking alex sent an even cooler parts hold solution
new query: Pull to BP stats.sql for generating the spreadsheet
needs:
  #jon 
  #ReconWIP
  #WipStartTimes (does not require #ReconWip)
  #ReconWait (requires only #WipStartTimes)
  #move (requires only #WipStartTimes)
  #MechParts (parts hold2.sql - requires #WipStartTimes & #jon)
  #orders (parts hold2.sql - requires #MechParts)
  #partsholdbushours (parts hold2.sql - requires #orders)

so let's put these all in one place
Pull to PB all temp tables.sql

table notes:
#jon: only vehicles that were pulled once and made it to pricing buffer once 

Bus Hrs:
		Service		Body	App
Mon - Frid	7 - Midnight	7 - 6	7 - 9
Saturday	7 - 5		8 - 5	8 - 6
Sunday		12 - 5		closed	12 - 5

Concise column definitions:
1. PB Count:  the number of vehicles that reached pricing buffer during the week
2. Avg Days Pull -> PB per Vehicle: (pbTS - PullTS)/PB Count  the average number of days from pull to pb for the vehicles that reached pb during the week
3. Avg Bus Hrs Pull -> PB per Vehicle:  using Service Bus Hrs
4. Mech Count: ? only work that is performed during the week
   
3. 

9/12/11
Fucking appearance reconitems have 2 categories:
  AppearanceReconItem: _Other: misc
                       _Upholstry: uph
  PartyCollection: _AppearanceReconItem: Buffs/Details
so grouping on category gives 2 columns
1. ExtractWIP3
	base query that returns raw ARI data, lump both appearance categories 
        into one

9/13/11
major refactor
shooting for one row for each vii with fr/thr columns for each activity
so what i'm looking for is the period of times during which there is 
no current activity

9/16/11
back tracking to original queries with better accounting for multiples
and that will be good enough
20110916 -.sql
1. #PullToPB: basic interval table (was #jon)
	1. row for each date for the last 118 days/each vehicle that became PB on that date
        2. left joined to vehicles that
		1. became PB on one of the 118 days
		2. Rydells only
		3. vehicles that have been pulled only once
		4. vehicles that have been pb only once
	ViiID
	PulledTS
	PBTS
	All Date info
2. #ActivityFromThru (was #wtf, #xxx, this is a different direction from #WipStartTimes)
	one row for each vehicle/activity/unique FromTS during the interval 
oh shit, now how do i get sequence
which is a separate issued from
or which is a prerequisity to wait times

feels like i need to go back to #WipStartTimes and factor dups in/out as requ'd

generate 2 separate tables
	1. WIP only, these need to be separate because i need to figure WIP sequence
		based on a maximum of 1 WIP/Vehicle/Dept
	2. All other activities (mover, parts, wtf) there will often be multiple 
		instances of these activities, which need to be analyzed in terms
		of WIP sequence: which dept does this move impact? etc etc
9/17/11
continuing in 20110916 -.sql
WIP only
	generates 
	#AllWip: 1 row for each vehicle/category/FromTS
		Columns: ViiID, pulledTS, pbTS, FromTS, ThruTS, category
	as a foundation for 
 	#WipBase: 1 row for each vehicle/category 
		Consolidated multiple veh/cat instances
		Columns: ViiID, category, FromTS, ThruTS, seq (all set to 0)
	which is the foundation for
	#ReconSeq: 1 row for each vehicle
		Columns: ViiID, ReconSeq (mba, bma, etc)
		since there are no multiples LEFT, maximum sequence will be 4 (4 categories)
Other Activites
	#OtherActivites: 1 row/vehicle/activity/fromTS :: each instance of parts/move/wtf
		Collumns: ViiID, FromTS, ThruTS, category

now how to put all this shit together
9/24/11
function BusinessHoursFromInteral: added accomodation for changing WorkingHours over time

fuck, go ahead and redo ReconSeq without o

9/25/11
discovered several vehicles where wip, or other FromTS < PulledTS or ThruTS > pbTS
edited #AllWip and #OtherActivities truncating the extended FromTS and ThruTS to
PulledTS and pbTS

		