-- AppOther, get that too
SELECT a.VehicleInventoryItemID, s.reconseq, a.category, a.pulledts, a.pbts, 
  a.FromTS, a.ThruTS, o.*
FROM #AllWip a
INNER JOIN ( -- restrict to those without multiples
  SELECT DISTINCT VehicleInventoryItemID
  FROM #WipBase) w ON a.VehicleInventoryItemID = w.VehicleInventoryItemID -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
LEFT JOIN #ReconSeq s ON a.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #OtherActivities o ON a.VehicleInventoryItemID = o.VehicleInventoryItemID 
WHERE a.category = 'MechanicalReconItem'



-- Total mechanical time, based ON sequence of WORK performed
-- seq = m*, pull -> mThru
-- seq = bm* OR *bm, bThru -> mThru
-- seq = am* OR *am, aThru -> mThru
-- mWip
SELECT x.*, 
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, mThru, 'Service') FROM system.iota)
    WHEN 2 THEN -- *m*
      CASE
        -- bm*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
        -- am*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
      END 
    WHEN 3 THEN -- **m
      CASE
        -- *bm
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
        -- *am
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
      END     
  END AS MechBusHours,
  iif(timestampdiff(sql_tsi_hour, mFrom, mThru) = 0, 1, (select Utilities.BusinessHoursFromInterval(mFrom, mThru, 'Service') FROM system.iota)) AS mWip,
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN (
      SELECT SUM((select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota))
      FROM #OtherActivities
      WHERE Activity = 'Move'
      AND ThruTS > PulledTS 
      AND FromTS < mFrom
      AND VehicleInventoryItemID = x.VehicleInventoryItemID) 
    WHEN 2 THEN
      CASE
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN (
          SELECT 
            CASE
              WHEN FromTS >= bFrom THEN SUM((select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota))
              ELSE SUM((select Utilities.BusinessHoursFromInterval(bThru, ThruTS, 'Service') FROM system.iota))
            END 
          FROM #OtherActivities
          WHERE Activity = 'Move'
          AND ThruTS > bThru
          AND FromTS < mFrom
          AND VehicleInventoryItemID = x.VehicleInventoryItemID)         
      END
      
  END AS mMove
FROM ( -- one row per vehicle
  SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
    m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
    a.FromTS AS aFrom, a.ThruTS AS aThru--, o.FromTS AS oFromt, o.ThrutS AS oThru
  FROM #PullToPB p 
  INNER JOIN ( -- exclusions FROM multiple wip/category
    SELECT DISTINCT VehicleInventoryItemID 
    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
  LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
  LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
  LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
  LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
--  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
  WHERE p.VehicleInventoryItemID IS NOT NULL 
  AND ReconSeq LIKE '%m%') x -- mechanical only
 
 
  
-- select * FROM #OtherActivities
-- only want those move hours that are IN the mInterval 
SELECT VehicleInventoryItemID, FromTS, ThruTS
--  SUM((select Utilities.BusinessHoursFromInterval(FromTs, ThruTS, 'Service') FROM system.iota))
FROM #OtherActivities 
WHERE Activity = 'Move'
GROUP BY VehicleInventoryItemID  









SELECT t.VehicleInventoryItemID, MechBusHours, mWip, 
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN PulledTS 
    WHEN 2 THEN
      CASE
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN bThru
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN aThru
      END
    WHEN 3 THEN
      CASE
        WHEN substring(ReconSeq, 1, 1) = 'b'  
      END  
  END AS mIntFrom,
  mThru AS mIntThru
FROM (
  SELECT x.*, 
    CASE position('m' IN ReconSeq) -- m**
      WHEN 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, mThru, 'Service') FROM system.iota)
      WHEN 2 THEN -- *m*
        CASE
          -- bm*
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
          -- am*
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
        END 
      WHEN 3 THEN -- **m
        CASE
          -- *bm
          WHEN substring(ReconSeq, 2, 1) = 'b' THEN (select Utilities.BusinessHoursFromInterval(bThru, mThru, 'Service') FROM system.iota)
          -- *am
          WHEN substring(ReconSeq, 2, 1) = 'a' THEN (select Utilities.BusinessHoursFromInterval(aThru, mThru, 'Service') FROM system.iota)
        END     
    END AS MechBusHours,
    iif(timestampdiff(sql_tsi_hour, mFrom, mThru) = 0, 1, (select Utilities.BusinessHoursFromInterval(mFrom, mThru, 'Service') FROM system.iota)) AS mWip
-- SELECT *  


-- this IS promising AS a base: VehicleInventoryItemID, mIntFrom, mFrom, mThru
SELECT VehicleInventoryItemID, 
  CASE position('m' IN ReconSeq) -- m**
    WHEN 1 THEN PulledTS 
    WHEN 2 THEN
      CASE
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN bThru
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN aThru
      END
    WHEN 3 THEN
      CASE
        WHEN substring(ReconSeq, 2, 1) = 'b' THEN bThru  
        WHEN substring(ReconSeq, 2, 1) = 'a' THEN aThru
      END  
  END AS mIntFrom,
  mFrom, mThru  
INTO #mBase  
FROM ( -- one row per vehicle
    SELECT p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
      m.FromtS AS mFrom, m.ThruTS AS mThru, b.FromTs AS bFrom, b.ThruTS AS bThru, 
      a.FromTS AS aFrom, a.ThruTS AS aThru--, o.FromTS AS oFromt, o.ThrutS AS oThru
    FROM #PullToPB p 
    INNER JOIN ( -- exclusions FROM multiple wip/category
      SELECT DISTINCT VehicleInventoryItemID 
      FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
    LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
    LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
    LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
    LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection'
  --  LEFT JOIN #WipBase o ON p.VehicleInventoryItemID = o.VehicleInventoryItemID AND o.category = 'AppearanceReconItem'
    WHERE p.VehicleInventoryItemID IS NOT NULL 
    AND ReconSeq LIKE '%m%') x
    
SELECT * FROM #mBase    
 
-- ADD moves that finish after mIntFrom AND start before mFrom    
-- accomodate truncating the start AND END ie, IF a move starts before mIntFrom, THEN the actual impact IS FROM mIntFrom

SELECT mb.VehicleInventoryItemID, mb.mIntFrom, mb.mFrom, mb.mThru, 
  (SELECT Utilities.BusinessHoursFromInterval(mb.mIntFrom, mb.mThru, 'Service') FROM system.iota) AS mTotal,  
  CASE -- 0 time wip
    WHEN timestampdiff(sql_tsi_hour, mFrom, mThru) < 1 THEN 1  
    ELSE (SELECT Utilities.BusinessHoursFromInterval(mb.mFrom, mb.mThru, 'Service') FROM system.iota) 
  END AS mWip, 
  CASE
    WHEN m.FromtS < mb.mIntFrom THEN mb.mIntFrom
    ELSE m.FromTS
  END AS FromTS,
  CASE
    WHEN m.ThruTS > mb.mThru THEN mb.mIntFrom
    ELSE m.ThruTS
  END AS Thru
FROM #mBase mb 
LEFT JOIN #OtherActivities m ON mb.VehicleInventoryItemID = m.VehicleInventoryItemID
  AND m.Activity = 'Move'
  AND m.ThruTS > mb.mIntFrom
  AND m.FromTS <= mb.mFrom
ORDER BY mb.VehicleInventoryItemID   

-- i'm NOT crazy about this notion of continuing to ADD grouping levels
-- but don't see way around it yet, AND it works
-- AND GROUP it
SELECT VehicleInventoryItemID, MAX(mTotal) AS mTotal, MAX(mWip) AS mWip,
  SUM((SELECT Utilities.BusinessHoursFromInterval(MoveFromTS, MoveThruTS, 'Service') FROM system.iota)) AS mMove
-- SELECT *  
FROM (
  SELECT mb.VehicleInventoryItemID, mb.mIntFrom, mb.mFrom, mb.mThru, 
    (SELECT Utilities.BusinessHoursFromInterval(mb.mIntFrom, mb.mThru, 'Service') FROM system.iota) AS mTotal,  
    CASE -- 0 time wip
      WHEN timestampdiff(sql_tsi_hour, mFrom, mThru) < 1 THEN 1  
      ELSE (SELECT Utilities.BusinessHoursFromInterval(mb.mFrom, mb.mThru, 'Service') FROM system.iota) 
    END AS mWip, 
    CASE
      WHEN m.FromtS < mb.mIntFrom THEN mb.mIntFrom
      ELSE m.FromTS
    END AS MoveFromTS,
    CASE
      WHEN m.ThruTS > mb.mThru THEN mb.mIntFrom
      ELSE m.ThruTS
    END AS MoveThruTS
  FROM #mBase mb 
  LEFT JOIN #OtherActivities m ON mb.VehicleInventoryItemID = m.VehicleInventoryItemID
    AND m.Activity = 'Move'
    AND m.ThruTS > mb.mIntFrom
    AND m.FromTS <= mb.mFrom) x
GROUP BY VehicleInventoryItemID     

-- ADD moves that finish after mIntFrom AND start before mFrom    
-- accomodate truncating the start AND END ie, IF a move starts before mIntFrom, THEN the actual impact IS FROM mIntFrom
-- ADD WTF

-- this IS WHERE it gets sticky, account for overlap BETWEEN wtf AND move
-- WHERE i start to think capacity vs activity IS relevant
SELECT mb.VehicleInventoryItemID, mb.mIntFrom, mb.mFrom, mb.mThru, 
  (SELECT Utilities.BusinessHoursFromInterval(mb.mIntFrom, mb.mThru, 'Service') FROM system.iota) AS mTotal,  
  CASE -- 0 time wip
    WHEN timestampdiff(sql_tsi_hour, mFrom, mThru) < 1 THEN 1  
    ELSE (SELECT Utilities.BusinessHoursFromInterval(mb.mFrom, mb.mThru, 'Service') FROM system.iota) 
  END AS mWip, 
  CASE
    WHEN m.FromtS < mb.mIntFrom THEN mb.mIntFrom
    ELSE m.FromTS
  END AS FromTS,
  CASE
    WHEN m.ThruTS > mb.mThru THEN mb.mIntFrom
    ELSE m.ThruTS
  END AS Thru,
  w.*
FROM #mBase mb 
LEFT JOIN #OtherActivities m ON mb.VehicleInventoryItemID = m.VehicleInventoryItemID
  AND m.Activity = 'Move'
  AND m.ThruTS > mb.mIntFrom
  AND m.FromTS <= mb.mFrom
LEFT JOIN #OtherActivities w ON mb.VehicleInventoryItemID = w.VehicleInventoryItemID
  AND w.Activity = 'WTF'
  AND w.ThruTS > mb.mIntFrom
  AND w.FromTS <= mb.mFrom  
ORDER BY mb.VehicleInventoryItemID 

SELECT *
FROM #OtherActivities

SELECT distinct activity
FROM #OtherActivities

SELECT VehicleInventoryItemID,
  SUM(
    CASE
      WHEN Activity = 'Move' THEN 
        (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota) 
    END) AS Move,
  SUM(
    CASE
      WHEN Activity = 'Parts' THEN 
        (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota) 
    END) AS Parts, 
  SUM(
    CASE
      WHEN Activity = 'WTF' THEN 
        (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota) 
    END) AS WTF         
FROM #OtherActivities
GROUP BY VehicleInventoryItemID


-- ask greg, what IS the capacity wait time for this scenario

--       pull                                               MechStart                       MechFinish
--        |-----------------------------------------------------|---------------------------------|
--  parts                             wtf
--    |-------------|                  |--------------|
--                           move
--                             |----------------|
--                               








 



-- DO the shit IN the SELECT 
-- won't WORK, SELECT clause IN right TABLE doesn't recognize fields FROM LEFT table
SELECT mb.VehicleInventoryItemID, mb.mIntFrom, mb.mFrom, mb.mThru, 
  (SELECT Utilities.BusinessHoursFromInterval(mb.mIntFrom, mb.mThru, 'Service') FROM system.iota) AS mTotal,  
  CASE -- 0 time wip
    WHEN timestampdiff(sql_tsi_hour, mFrom, mThru) < 1 THEN 1  
    ELSE (SELECT Utilities.BusinessHoursFromInterval(mb.mFrom, mb.mThru, 'Service') FROM system.iota) 
  END AS mWip, 
  CASE
    WHEN m.FromtS < mb.mIntFrom THEN mb.mIntFrom
    ELSE m.FromTS
  END AS FromTS,
  CASE
    WHEN m.ThruTS > mb.mThru THEN mb.mIntFrom
    ELSE m.ThruTS
  END AS Thru,  
  m.*
FROM #mBase mb 
LEFT JOIN (
  SELECT VehicleInventoryItemID, 
    CASE
      WHEN FromtS < mb.mIntFrom THEN mb.mIntFrom
      ELSE FromTS
    END AS FromTS,
    ThruTS
  FROM #OtherActivities
  WHERE Activity = 'Move') m ON mb.VehicleInventoryItemID = m.VehicleInventoryItemID
    AND ThruTS > mb.mIntFrom
    AND FromTS <= mb.mFrom
ORDER BY mb.VehicleInventoryItemID 