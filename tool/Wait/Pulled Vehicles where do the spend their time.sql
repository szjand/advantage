once a vehicle IS pulled, what IS it waitin for AND for how long
limbo mech body detail wtf pricing buffer
-- pull -> WSB
limbo: open mech & body
body & no mech
SELECT a.stocknumber, timestampdiff(sql_tsi_day, b.fromts, now()) AS [days pulled]
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagPulled'
  AND b.ThruTS IS NULL 
ORDER BY a.stocknumber  



at the time of pull what recon IS OPEN

SELECT * FROM AuthorizedReconItems 

SELECT * FROM viiwtf WHERE VehicleInventoryItemID = 'f60d06ee-4e8a-4af9-ba06-1ec9e12cdda1'
SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = 'f60d06ee-4e8a-4af9-ba06-1ec9e12cdda1'

SELECT DISTINCT category FROM VehicleInventoryItemStatuses 
-- additional appearance added - pricing buffer
-- dispatched
SELECT a.stocknumber, timestampdiff(sql_tsi_day, b.fromts, now()) AS [days pulled],
  b.fromts, 
  c.fromts AS MechDispFrom, c.thruts AS MechDispThru,
  d.fromts AS BodyDispFrom, d.thruts AS BodyDispThru,
  e.fromts AS AppDispFrom, e.thruts AS AppDispThru,
  f.fromts AS PBFrom, f.thruts AS PBThru
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagPulled'
  AND b.ThruTS IS NULL 
LEFT JOIN VehicleInventoryItemStatuses c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.category = 'MechanicalReconDispatched'
  AND c.FromTS >= b.FromTS  
LEFT JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.category = 'BodyReconDispatched'
  AND d.FromTS >= b.FromTS    
LEFT JOIN VehicleInventoryItemStatuses e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID 
  AND e.category = 'AppearanceReconDispatched'
  AND e.FromTS >= b.FromTS 
LEFT JOIN VehicleInventoryItemStatuses f ON a.VehicleInventoryItemID = f.VehicleInventoryItemID 
  AND f.category = 'RMFlagPB'
  AND f.FromTS >= b.FromTS   
ORDER BY a.stocknumber  
/*
Things to know about SQL time shit
  using timestampdiff(sql_tsi_day, @ts1, @ts2), IF difference IS less than 24 hours, diff returns 0
  so use: dayofyear(@ts2) - dayofyear(@ts1) to determine number of days enclosed IN the interval
  fuck, that's wrong too, day of year won't WORK across years
  but this works: timestampdiff(sql_tsi_day, CAST(@ts1 AS sql_date), CAST(@ts2 AS sql_date))
*/  
-- need to find CLOSE enuf, maybe start with just SUM from/thru for each
-- ok, but need to SUM the dept time separately FROM pulled
-- yes, this looking good
SELECT a.stocknumber, timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) AS [days pulled],
  timestampdiff(sql_tsi_day, coalesce(cast(c.fromts AS sql_date), curdate()), coalesce(cast(c.thruts AS sql_date), curdate())) AS [Mech Days],
  timestampdiff(sql_tsi_day, coalesce(cast(d.fromts AS sql_date), curdate()), coalesce(cast(d.thruts AS sql_date), curdate())) AS [Body Days],
  timestampdiff(sql_tsi_day, coalesce(cast(e.fromts AS sql_date), curdate()), coalesce(cast(e.thruts AS sql_date), curdate())) AS [App Days],
  timestampdiff(sql_tsi_day, coalesce(cast(g.fromts AS sql_date), curdate()), coalesce(cast(g.thruts AS sql_date), curdate())) AS [WTF Days],
  b.fromts, 
  c.fromts AS MechDispFrom, c.thruts AS MechDispThru,
  d.fromts AS BodyDispFrom, d.thruts AS BodyDispThru,
  e.fromts AS AppDispFrom, e.thruts AS AppDispThru
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagPulled'
  AND b.ThruTS IS NULL 
LEFT JOIN VehicleInventoryItemStatuses c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.category = 'MechanicalReconDispatched'
  AND c.FromTS >= b.FromTS  
LEFT JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.category = 'BodyReconDispatched'
  AND d.FromTS >= b.FromTS    
LEFT JOIN VehicleInventoryItemStatuses e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID 
  AND e.category = 'AppearanceReconDispatched'
  AND e.FromTS >= b.FromTS 
LEFT JOIN VehicleInventoryItemStatuses g ON a.VehicleInventoryItemID = g.VehicleInventoryItemID 
  AND g.category = 'RMFlagWTF'
  AND g.FromTS >= b.FromTS      
ORDER BY a.stocknumber  


-- ok, but need to SUM the dept time separately FROM pulled
-- need to pull days separately, statuses need to be grouped AND summed
-- disproportionate mech days
-- still double counting take out pull
SELECT *
FROM (
  SELECT a.stocknumber, timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) AS [days pulled]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL) a 
LEFT JOIN (   
  SELECT stocknumber,  
    SUM(timestampdiff(sql_tsi_day, coalesce(cast(c.fromts AS sql_date), curdate()), coalesce(cast(c.thruts AS sql_date), curdate()))) AS [Mech Days],
    SUM(timestampdiff(sql_tsi_day, coalesce(cast(d.fromts AS sql_date), curdate()), coalesce(cast(d.thruts AS sql_date), curdate()))) AS [Body Days],
    SUM(timestampdiff(sql_tsi_day, coalesce(cast(e.fromts AS sql_date), curdate()), coalesce(cast(e.thruts AS sql_date), curdate()))) AS [App Days],
    SUM(timestampdiff(sql_tsi_day, coalesce(cast(g.fromts AS sql_date), curdate()), coalesce(cast(g.thruts AS sql_date), curdate()))) AS [WTF Days]  
  FROM VehicleInventoryItems a
--  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
--    AND b.category = 'RMFlagPulled'
--    AND b.ThruTS IS NULL 
  LEFT JOIN VehicleInventoryItemStatuses c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.category = 'MechanicalReconDispatched'
--    AND c.FromTS >= b.FromTS  
    AND c.fromts >= (SELECT fromts FROM VehicleInventoryItemStatuses WHERE category = 'RMFlagPulled' AND ThruTS IS NULL  AND VehicleInventoryItemID = a.VehicleInventoryItemID)
  LEFT JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.category = 'BodyReconDispatched'
--    AND d.FromTS >= b.FromTS    
  LEFT JOIN VehicleInventoryItemStatuses e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID 
    AND e.category = 'AppearanceReconDispatched'
--    AND e.FromTS >= b.FromTS 
  LEFT JOIN VehicleInventoryItemStatuses g ON a.VehicleInventoryItemID = g.VehicleInventoryItemID 
    AND g.category = 'RMFlagWTF'
--    AND g.FromTS >= b.FromTS      
  GROUP BY stocknumber) b ON a.stocknumber = b.stocknumber  
ORDER BY a.stocknumber  

-- separate joins to the VehicleInventoryItemStatuses tables
-- NOT quite
SELECT a.stocknumber, a.[Pulled Days], b.[Mech Days], c.[Body Days], 
  d.[App Days], e.[PB Days], f.[WTF Days] 
FROM (
  SELECT a.VehicleInventoryItemID, b.fromts, b.thruts, a.stocknumber, timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) AS [Pulled Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL) a 
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts, 
    timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [Mech Days]
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'MechanicalReconDispatched') b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.fromTS >= a.fromts 
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts, 
    timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [Body Days]
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'BodyReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= a.fromts   
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts, 
    timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [App Days]
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'AppearanceReconDispatched') d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID AND d.fromTS >= a.fromts   
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts, 
    timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [PB Days]
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPB') e ON a.VehicleInventoryItemID = e.VehicleInventoryItemID AND e.fromTS >= a.fromts    
LEFT JOIN (
  SELECT VehicleInventoryItemID, fromts, 
    timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [WTF Days]
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagWTF') f ON a.VehicleInventoryItemID = f.VehicleInventoryItemID AND f.fromTS >= a.fromts     
ORDER BY a.stocknumber  
      
      
-----
-- ok, this looks pretty good
-- same day disp open/CLOSE or disp today & not done yet comes out AS 0 days
-- NULL days means no dispatch for that dept (yet)
SELECT m.stocknumber, m.[Pulled Days], n.[Mech Days], o.[Body Days], p.[App Days],
  q.[PB Days], r.[WTF Days], s.[WSB Days]
FROM ( -- pulled
  SELECT a.stocknumber, timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) AS [Pulled Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL) m    
LEFT JOIN ( -- mech
  SELECT a.stocknumber, SUM(c.[Mech Days]) AS [Mech Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [Mech Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'MechanicalReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS 
  GROUP BY stocknumber) n ON m.stocknumber = n.stocknumber    
LEFT JOIN ( -- body
  SELECT a.stocknumber, SUM(c.[Body Days]) AS [Body Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [Body Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'BodyReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) o ON m.stocknumber = o.stocknumber    
LEFT JOIN ( -- app
  SELECT a.stocknumber, SUM(c.[App Days]) AS [App Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [App Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'AppearanceReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) p ON m.stocknumber = p.stocknumber
LEFT JOIN ( -- pb
  SELECT a.stocknumber, SUM(c.[PB Days]) AS [PB Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [PB Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) q ON m.stocknumber = q.stocknumber  
LEFT JOIN ( -- wtf
  SELECT a.stocknumber, SUM(c.[WTF Days]) AS [WTF Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [WTF Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) r ON m.stocknumber = r.stocknumber    
LEFT JOIN ( -- wsb
  SELECT a.stocknumber, SUM(c.[WSB Days]) AS [WSB Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [WSB Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagWSB') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) s ON m.stocknumber = s.stocknumber     
ORDER BY m.stocknumber


-----
-- ok, this looks pretty good
-- same day disp open/CLOSE or disp today & not done yet comes out AS 0 days
-- NULL days means no dispatch for that dept (yet)
-- 2/8, ADD curr recon status

SELECT m.stocknumber, m.[Pulled Days], n.[Mech Days], o.[Body Days], p.[App Days],
  q.[PB Days], r.[WTF Days], s.[WSB Days],
  CASE t.status
    WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
    WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
  END AS [Mech Status],
  CASE u.status
    WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
    WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'BodyReconProcess_NotStarted' THEN 'Not Started'
  END AS [Body Status],
  CASE v.status
    WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
    WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'AppearanceReconProcess_NotStarted' THEN 'Not Started'
  END AS [App Status]       
FROM ( -- pulled
  SELECT a.stocknumber, timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), 
    curdate()) AS [Pulled Days], a.VehicleInventoryItemID 
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL) m    
LEFT JOIN ( -- mech
  SELECT a.stocknumber, SUM(c.[Mech Days]) AS [Mech Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [Mech Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'MechanicalReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS 
  GROUP BY stocknumber) n ON m.stocknumber = n.stocknumber    
LEFT JOIN ( -- body
  SELECT a.stocknumber, SUM(c.[Body Days]) AS [Body Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [Body Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'BodyReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) o ON m.stocknumber = o.stocknumber    
LEFT JOIN ( -- app
  SELECT a.stocknumber, SUM(c.[App Days]) AS [App Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [App Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'AppearanceReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) p ON m.stocknumber = p.stocknumber
LEFT JOIN ( -- pb
  SELECT a.stocknumber, SUM(c.[PB Days]) AS [PB Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [PB Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) q ON m.stocknumber = q.stocknumber  
LEFT JOIN ( -- wtf
  SELECT a.stocknumber, SUM(c.[WTF Days]) AS [WTF Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [WTF Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) r ON m.stocknumber = r.stocknumber    
LEFT JOIN ( -- wsb
  SELECT a.stocknumber, SUM(c.[WSB Days]) AS [WSB Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [WSB Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagWSB') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) s ON m.stocknumber = s.stocknumber     
LEFT JOIN VehicleInventoryItemStatuses t ON m.VehicleInventoryItemID = t.VehicleInventoryItemID
  AND t.category = 'MechanicalReconProcess'
  AND t.ThruTS IS NULL 
LEFT JOIN VehicleInventoryItemStatuses u ON m.VehicleInventoryItemID = u.VehicleInventoryItemID
  AND u.category = 'BodyReconProcess'
  AND u.ThruTS IS NULL 
LEFT JOIN VehicleInventoryItemStatuses v ON m.VehicleInventoryItemID = v.VehicleInventoryItemID
  AND v.category = 'AppearanceReconProcess'
  AND v.ThruTS IS NULL       
ORDER BY m.stocknumber



-- 2/13, without curr recon status

SELECT m.stocknumber, m.[Pulled Days], n.[Mech Days], o.[Body Days], p.[App Days],
  q.[PB Days], r.[WTF Days], s.[WSB Days]    
FROM ( -- pulled
  SELECT a.stocknumber, timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), 
    curdate()) AS [Pulled Days], a.VehicleInventoryItemID 
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL) m    
LEFT JOIN ( -- mech
  SELECT a.stocknumber, SUM(c.[Mech Days]) AS [Mech Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [Mech Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'MechanicalReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS 
  GROUP BY stocknumber) n ON m.stocknumber = n.stocknumber    
LEFT JOIN ( -- body
  SELECT a.stocknumber, SUM(c.[Body Days]) AS [Body Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [Body Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'BodyReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) o ON m.stocknumber = o.stocknumber    
LEFT JOIN ( -- app
  SELECT a.stocknumber, SUM(c.[App Days]) AS [App Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [App Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'AppearanceReconDispatched') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) p ON m.stocknumber = p.stocknumber
LEFT JOIN ( -- pb
  SELECT a.stocknumber, SUM(c.[PB Days]) AS [PB Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [PB Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) q ON m.stocknumber = q.stocknumber  
LEFT JOIN ( -- wtf
  SELECT a.stocknumber, SUM(c.[WTF Days]) AS [WTF Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [WTF Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPB') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) r ON m.stocknumber = r.stocknumber    
LEFT JOIN ( -- wsb
  SELECT a.stocknumber, SUM(c.[WSB Days]) AS [WSB Days]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagPulled'
    AND b.ThruTS IS NULL     
  LEFT JOIN (
    SELECT VehicleInventoryItemID, fromts, 
      timestampdiff(sql_tsi_day, coalesce(cast(fromts AS sql_date), curdate()), coalesce(cast(thruts AS sql_date), curdate())) AS [WSB Days]
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagWSB') c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID AND c.fromTS >= b.fromts -- >= pulledTS    
  GROUP BY stocknumber) s ON m.stocknumber = s.stocknumber     
ORDER BY m.stocknumber



SELECT a.stocknumber, b.*
FROM vusedcarspulledvehicles a
LEFT JOIN vusedcarsreconbuffer b ON a.stocknumber = b.stocknumber