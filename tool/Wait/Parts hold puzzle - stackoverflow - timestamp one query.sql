
-- DROP TABLE #orders
CREATE TABLE #orders (
  id integer, 
  d1 timestamp,
  d2 timestamp);
  
INSERT INTO #orders values (1, CAST('08/01/2011 08:00:00' AS sql_timestamp), CAST('08/08/2011 10:00:00' AS sql_timestamp));
INSERT INTO #orders values (1, CAST('08/02/2011 13:00:00' AS sql_timestamp), CAST('08/06/2011 09:00:00' AS sql_timestamp));
INSERT INTO #orders values (1, CAST('08/12/2011 16:00:00' AS sql_timestamp), CAST('08/14/2011 12:00:00' AS sql_timestamp));
INSERT INTO #orders values (1, CAST('08/03/2011 08:00:00' AS sql_timestamp), CAST('08/10/2011 10:00:00' AS sql_timestamp));  

SELECT ID, MIN(d1) AS d1, MAX(d2) AS d2, SUM(hours) AS hours
FROM ( --#tablePeriods 
  SELECT C.ID, MIN(C.d1) AS d1, C.d2,
    (select Utilities.BusinessHoursFromInterval(MIN(c.d1), c.d2, 'Service') FROM system.iota) AS hours
  FROM (
    SELECT A.ID, A.d1, MAX(A.d2) AS d2
    FROM ( -- #tableProtoPeriods A
      SELECT A.ID, A.date AS d1, B.date AS d2
      FROM ( -- #tableStacked A
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( --#tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date        
        GROUP BY A.ID, A.date) A     
      INNER JOIN ( --#tableStacked B ON B.ID = A.ID AND B.date > A.date
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( -- #tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date      
        GROUP BY A.ID, A.date) B ON B.ID = A.ID AND B.date > A.date      
      INNER JOIN ( -- #tableStacked C ON C.ID = A.ID AND C.date > A.date AND C.date <= B.date
        SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
        FROM ( -- #tableEvents A
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) A        
        LEFT JOIN ( -- #tableEvents B ON B.ID = A.ID AND B.date < A.date
          SELECT ID, d1 AS date, 1 AS diff 
          FROM #Orders
          UNION ALL
          SELECT ID, d2 AS date, -1 AS diff
          FROM #Orders) B ON B.ID = A.ID AND B.date < A.date        
        GROUP BY A.ID, A.date)  C ON C.ID = A.ID AND C.date > A.date AND C.date <= B.date      
      WHERE A.thecount = 0
      AND B.thecount = 1
      GROUP BY A.ID, A.date, B.date
      HAVING MIN(C.thecount) > 0) A    
    GROUP BY A.ID, A.d1) C    
  GROUP BY C.ID, C.d2, hours) A
GROUP BY ID
ORDER BY d1;