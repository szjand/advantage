
-- i need a COLUMN for each category
-- fuck, i'm stuck IN thinking i need a dynamic number of columns
-- well, fuck it, DO it the dummy way, after ALL, i DO know the 7 fucking categories
-- but what i really need IS from/thru for the timeline
SELECT VehicleInventoryItemID, max(timestampdiff(sql_tsi_day, cast(pulledTS AS sql_date),cast(pbTS AS sql_date))),
  SUM(
    CASE 
      WHEN cat = 'AppearanceReconItem' THEN BusHours
    END) AS "Detail Other WIP",
  SUM(
    CASE 
      WHEN cat = 'Parts' THEN BusHours
    END) AS "Parts Hold",
  SUM(
    CASE 
      WHEN cat = 'BodyReconItem' THEN BusHours
    END) AS "Body WIP",
  SUM(
    CASE 
      WHEN cat = 'MechanicalReconItem' THEN BusHours
    END) AS "Mech WIP" ,
  SUM(
    CASE 
      WHEN cat = 'Move' THEN BusHours
    END) AS Move ,
  SUM(
    CASE 
      WHEN cat = 'PartyCollection' THEN BusHours
    END) AS "Detail WIP" ,
  SUM(
    CASE 
      WHEN cat = 'WTF' THEN BusHours
    END) AS WTF                      
-- etc etc etc      
FROM(  
  SELECT VehicleInventoryItemID, cat, MAX(pulledTS) AS pulledTS, MAX(pbTS) AS pbTS,
    SUM(
      CASE
        WHEN category = cat THEN 
          CASE 
            WHEN cat = 'BodyReconItem' THEN 
              (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'BodyShop') FROM system.iota)
            WHEN cat = 'AppearanceReconItem' OR cat = 'PartyCollection' THEN 
              (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Detail') FROM system.iota)
            ELSE 
              (SELECT Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
          END 
        ELSE 0
      END) AS BusHours
  FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
--  WHERE x.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'
  GROUP BY VehicleInventoryItemID, cat) e
GROUP BY VehicleInventoryItemID   

-- multiples
-- can be multiple IN same category with the same FromTS, so GROUP BY both TSs
SELECT COUNT(*) FROM ( -- same Grouped or not grouped 873
select VehicleInventoryItemID, category, FromTS, ThruTS
FROM #wtf
GROUP BY VehicleInventoryItemID, category, FromTS, ThruTS) s


-- need MIN(FromTS) for car -- will always be pulled ts
-- need to be able to recurse thru multiples
-- separate subtable of multiples
-- so multiples
-- AND the winner IS 6 moves ON a vehicle

-- the fucking fromts are the base TABLE for the pivot

-- nah, still the same problem, dynamic COLUMN generation, ain't going to happen
-- what DO i, 6 fucking move columns for every record, which for most cars
-- will 1-6 NULL from/thrus?
SELECT VehicleInventoryItemID, category, COUNT(*)
FROM #xxx
GROUP BY VehicleInventoryItemID, category
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC 

SELECT VehicleInventoryItemID, category, COUNT(*)
FROM #xxx
WHERE category <> 'Move'
GROUP BY VehicleInventoryItemID, category
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC 

SELECT COUNT(DISTINCT VehicleInventoryItemID) FROM (oooooooo
SELECT VehicleInventoryItemID, category, COUNT(*)
FROM #xxx
WHERE category = 'Move'
GROUP BY VehicleInventoryItemID, category
HAVING COUNT(*) > 1)w
 
SELECT COUNT(DISTINCT VehicleInventoryItemID) FROM #xxx
SELECT *
FROM #xxx
WHERE VehicleInventoryItemID = 'b74edc51-cde2-4ae3-894a-bbf6c81a2919'
ORDER BY FromTS

SELECT *
FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
WHERE x.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29'

SELECT VehicleInventoryItemID, pulledTS, 
  SUM(
    CASE 
      WHEN cat = 'AppearanceReconItem' THEN BusHours
    END) AS "Detail Other WIP",
  pbTS,
FROM (
  SELECT *
  FROM #xxx x, (SELECT DISTINCT category as cat FROM #xxx) y 
  WHERE x.VehicleInventoryItemID = '8674c724-b61e-462a-968d-bebcbf5d2a29')a   