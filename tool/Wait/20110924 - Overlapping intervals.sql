
SELECT VehicleInventoryItemID, MIN(FromTS) AS FromTS, ThruTS, Activity
FROM #OtherActivities 
GROUP BY VehicleInventoryItemID, ThruTS, Activity
UNION  
SELECT VehicleInventoryItemID, FromTS, ThruTS, 'AppOther'
FROM #WipBase
WHERE category = 'AppearanceReconItem' 
 

SELECT * -- ALL move, wtf, parts, AppOther
INTO #t
FROM (
  SELECT VehicleInventoryItemID, MIN(FromTS) AS FromTS, ThruTS, Activity
  FROM #OtherActivities 
  GROUP BY VehicleInventoryItemID, ThruTS, Activity
  UNION  
  SELECT VehicleInventoryItemID, FromTS, ThruTS, 'AppOther'
  FROM #WipBase
  WHERE category = 'AppearanceReconItem') x
WHERE ThruTS IS NOT NULL -- can't handle NULL thruts, eg move never completed, parts never recvd
ORDER BY VehicleInventoryItemID 

SELECT -- DISTINCT FromTS, VehicleInventoryItemID 
  VehicleInventoryItemID, FromTS
FROM #t
WHERE ThruTS IS NOT NULL -- can't handle NULL thruts, eg move never completed, parts never recvd
GROUP BY VehicleInventoryItemID, FromTS

SELECT * FROM #t


SELECT DISTINCT VehicleInventoryItemID, ThruTS
FROM #t t1
WHERE ( --*-- return only those values of d2 (interval end date) for each id where
  SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0)) --*- d2 NOT BETWEEN any d1/d2 interval per row
  FROM #t t2 
  WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID and t2.ThruTS <> t1.ThruTS) = 0 
  AND ThruTS <> ( --*-- and d2 IS NOT the largest date
    SELECT max(ThruTS) 
    FROM #t t3 
    WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID) 
    
-- pull                                              mFrom
--  |--------------------------------------------------|    
-- parts
--  |------------------|
--                move
--                  |-------------|
--                                     wtf
--                                      |----------|


SELECT
   d.VehicleInventoryItemID, 
   d.duration, 
   d.duration - 
   IFNULL((
     SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #t t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
     FROM (
       SELECT DISTINCT VehicleInventoryItemID, ThruTS 
       FROM #t t1 
       WHERE (
         SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
         FROM #t t2 
         WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
         AND t2.ThruTS <> t1.ThruTS ) = 0 
       AND ThruTS <> (
         SELECT  MAX(ThruTS) 
         FROM #t t3 
         WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
     WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) "parts hold"
FROM (
  SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) duration
  FROM #t GROUP BY VehicleInventoryItemID) d  
  
LEFT JOIN #ts u ON d.VehicleInventoryItemID = u.VehicleInventoryItemID   


what i want to know: for each vehicle, are there any overlapping intervals

SELECT * 
FROM #ts t1
LEFT JOIN #ts t2 ON t1.VehicleInventoryItemID = t2.VehicleInventoryItemID 

-- so WHERE i'm at IS adding the pull-pbts to this
-- hold times are NOT relevant to a particular dept, but rather to the entire interval

-- pull
SELECT * -- ALL move, wtf, parts, AppOther
INTO #ts
FROM (
  SELECT VehicleInventoryItemID, MIN(FromTS) AS FromTS, ThruTS, Activity
  FROM #OtherActivities 
  GROUP BY VehicleInventoryItemID, ThruTS, Activity
  UNION  
  SELECT VehicleInventoryItemID, FromTS, ThruTS, 'AppOther'
  FROM #WipBase
  WHERE category = 'AppearanceReconItem') x
WHERE ThruTS IS NOT NULL -- can't handle NULL thruts, eg move never completed, parts never recvd
ORDER BY VehicleInventoryItemID 

-- so going instead to:

-- pull                                                                                                          pb
--  |-------------------------------------------------------------------------------------------------------------|    
-- parts                                         mWip               
--  |------------------|                          |---------------|
--                move
--                  |----------|
--                               wtf
--                                |----------|

fuck losing focus ON DEPT (capacity) wait

-- so this IS what i really need, for each dept
-- based ON seq   
-- seq IS mb                                                                                                      pbTS
-- pull                                           mFrom       mThru                       bFrom                   bThru
--  |-----------------------------------------------|----------|----------------------------|----------------------|    
-- parts                                                      move
--  |--------|                                                 |----------|
--            move                        mWait        mWip                      bWait           bWip
--              |----------|        |--------------|-----------|          |-----------------|----------------------|
--                      wtf
--                       |----------|



select * FROM #AllWip
SELECT * FROM #OtherActivities

so i need pullts -> seq1.FROM -> seq1.Thru -> seq2.FROM ... -> pbTS

looking for the periods of no activity

SELECT * 
FROM #AllWip a
LEFT JOIN #OtherActivities o ON a.VehicleInventoryItemID = o.VehicleInventoryItemID 

-- here are ALL From/Thrus
-- this becomes the base for the Alex Query
-- DROP TABLE #ts
SELECT VehicleInventoryItemID, pulledTS AS FromTS, pbTS AS ThruTS
INTO #ts
FROM #AllWip
UNION 
SELECT VehicleInventoryItemID, FromTs, ThruTS 
FROM #AllWip
UNION SELECT VehicleInventoryItemID, FromTS, ThruTS
FROM #OtherActivities

-- thought this would WORK, but it IS returning the same value IN both columns
-- AAAHHHHHHHHHH can't include pull - > pb, the base TABLE IS activities 
-- what i want to return IS the total of inactivity IN the interval of first activity to last activity
SELECT
   d.VehicleInventoryItemID, 
   d.duration, 
   d.duration - 
   IFNULL((
     SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
     FROM (
       SELECT DISTINCT VehicleInventoryItemID, ThruTS 
       FROM #ts t1 
       WHERE (
         SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
         FROM #ts t2 
         WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
         AND t2.ThruTS <> t1.ThruTS ) = 0 
       AND ThruTS <> (
         SELECT  MAX(ThruTS) 
         FROM #ts t3 
         WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
     WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) "parts hold"
FROM (
  SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) duration
  FROM #ts GROUP BY VehicleInventoryItemID) d  

-- so let's leave out pull -> pb AND see what we get

-- here are ALL From/Thrus
-- this becomes the base for the Alex Query
-- DROP TABLE #ts
SELECT VehicleInventoryItemID, FromTS, ThruTS
INTO #ts
FROM #AllWip
UNION SELECT VehicleInventoryItemID, FromTS, ThruTS
FROM #OtherActivities

-- thought this would WORK, but it IS returning the same value IN both columns
-- AAAHHHHHHHHHH can't include pull - > pb, the base TABLE IS activities 
-- what i want to return IS the total of inactivity IN the interval of first activity to last activity

-- so this now gives me 2 columns
-- duration: the interval FROM the start of the first activity until the END of the last activity
-- parts hold: the period of time during with an activity was current - WHEN something
-- was happening: wip, hold, parts, wtf, move
SELECT
   d.VehicleInventoryItemID, 
   d.duration, 
   d.duration - 
   IFNULL((
     SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
     FROM (
       SELECT DISTINCT VehicleInventoryItemID, ThruTS 
       FROM #ts t1 
       WHERE (
         SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
         FROM #ts t2 
         WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
         AND t2.ThruTS <> t1.ThruTS ) = 0 
       AND ThruTS <> (
         SELECT  MAX(ThruTS) 
         FROM #ts t3 
         WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
     WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) "parts hold"
FROM (
  SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) duration
  FROM #ts GROUP BY VehicleInventoryItemID) d  

  
-- ADD the inactive time AS a COLUMN
SELECT
   d.VehicleInventoryItemID, 
   d.duration, 
   d.duration - 
   IFNULL((
     SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
     FROM (
       SELECT DISTINCT VehicleInventoryItemID, ThruTS 
       FROM #ts t1 
       WHERE (
         SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
         FROM #ts t2 
         WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
         AND t2.ThruTS <> t1.ThruTS ) = 0 
       AND ThruTS <> (
         SELECT  MAX(ThruTS) 
         FROM #ts t3 
         WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
     WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) AS Active, 
     
     coalesce(
       (SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
       FROM (
         SELECT DISTINCT VehicleInventoryItemID, ThruTS 
         FROM #ts t1 
         WHERE (
           SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
           FROM #ts t2 
           WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
           AND t2.ThruTS <> t1.ThruTS ) = 0 
         AND ThruTS <> (
           SELECT  MAX(ThruTS) 
           FROM #ts t3 
           WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
       WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) AS InActive     
FROM (
  SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) duration
  FROM #ts GROUP BY VehicleInventoryItemID) d   
  
-- ADD the inactive time AS a COLUMN
-- CONVERT to business hours
-- shit here's the dept problem what are the business hours over the entire interval
SELECT
   d.VehicleInventoryItemID, 
   d.duration, 
   d.duration - 
   IFNULL((
     SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
     FROM (
       SELECT DISTINCT VehicleInventoryItemID, ThruTS 
       FROM #ts t1 
       WHERE (
         SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
         FROM #ts t2 
         WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
         AND t2.ThruTS <> t1.ThruTS ) = 0 
       AND ThruTS <> (
         SELECT  MAX(ThruTS) 
         FROM #ts t3 
         WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
     WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) AS Active, 
     
     coalesce(
       (SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
       FROM (
         SELECT DISTINCT VehicleInventoryItemID, ThruTS 
         FROM #ts t1 
         WHERE (
           SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
           FROM #ts t2 
           WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
           AND t2.ThruTS <> t1.ThruTS ) = 0 
         AND ThruTS <> (
           SELECT  MAX(ThruTS) 
           FROM #ts t3 
           WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
       WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) AS InActive     
FROM (
  SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) duration
  FROM #ts GROUP BY VehicleInventoryItemID) d   
  
-- now need the inactive time over the course of pull->pb  
-- shit, just use service hours?
-- OR DO separate depts?
-- TRY service hours
SELECT p.VehicleInventoryItemID, p.pulledTS, p.pbTS, 
  (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota),
  y.ActivityDuration, y.Active, y.InActive
FROM #PullToPb p
LEFT JOIN (
  SELECT VehicleInventoryItemID, ActivityDuration, Active, ActivityDuration - Active AS Inactive
  FROM (
    SELECT
       d.VehicleInventoryItemID, 
       d.ActivityDuration, 
       d.ActivityDuration - 
       IFNULL((
         SELECT Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
         FROM (
           SELECT DISTINCT VehicleInventoryItemID, ThruTS 
           FROM #ts t1 
           WHERE (
             SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
             FROM #ts t2 
             WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
             AND t2.ThruTS <> t1.ThruTS ) = 0 
           AND ThruTS <> (
             SELECT  MAX(ThruTS) 
             FROM #ts t3 
             WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
         WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) AS Active
    FROM (
      SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) ActivityDuration
      FROM #ts GROUP BY VehicleInventoryItemID) d) w) y ON p.VehicleInventoryItemID = y.VehicleInventoryItemID 
WHERE p.VehicleInventoryItemID IS NOT NULL     
      
SELECT VehicleInventoryItemID, Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS)))
FROM (
-- oooooo IF i exclude MIN(FromTS) FROM this can i THEN use pullTS AND pbTS AS the BEGIN AND END
-- check this out after fucking figuring out the business hours shit 
  SELECT DISTINCT VehicleInventoryItemID, ThruTS 
  FROM #ts t1 
  WHERE (
   SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
   FROM #ts t2 
   WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
   AND t2.ThruTS <> t1.ThruTS ) = 0 
  AND ThruTS <> (
   SELECT  MAX(ThruTS) 
   FROM #ts t3 
   WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID)) no_hold
GROUP BY VehicleInventoryItemID    


-- the business hours shit
SELECT VehicleInventoryItemID, 
  Sum(timestampdiff(SQL_TSI_hour, no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS))) AS Hours,
  SUM((select Utilities.BusinessHoursFromInterval(no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS), 'Service') FROM system.iota)) AS BusHours
  
FROM (
  SELECT DISTINCT VehicleInventoryItemID, ThruTS 
  FROM #ts t1 
  WHERE (
   SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
   FROM #ts t2 
   WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
   AND t2.ThruTS <> t1.ThruTS ) = 0 
  AND ThruTS <> (
   SELECT  MAX(ThruTS) 
   FROM #ts t3 
   WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID)) no_hold
GROUP BY VehicleInventoryItemID  

-- bus > hours
-- fuck, DO i have to test for 0 time WIP? 
SELECT * FROM #AllWip WHERE VehicleInventoryItemID = '025010f2-d37f-4fbd-b634-f33e3f76f1b2'

SELECT p.VehicleInventoryItemID, p.pulledTS, p.pbTS, 
  (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota) AS BusHours,
  y.ActivityDuration, y.Active, y.InActive
FROM #PullToPb p
LEFT JOIN (
  SELECT VehicleInventoryItemID, ActivityDuration, Active, ActivityDuration - Active AS Inactive
  FROM (
    SELECT
       d.VehicleInventoryItemID, 
       d.ActivityDuration, 
       d.ActivityDuration - 
       IFNULL((
         SELECT SUM((select Utilities.BusinessHoursFromInterval(no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS), 'Service') FROM system.iota))
         FROM (
           SELECT DISTINCT VehicleInventoryItemID, ThruTS 
           FROM #ts t1 
           WHERE (
             SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
             FROM #ts t2 
             WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
             AND t2.ThruTS <> t1.ThruTS ) = 0 
           AND ThruTS <> (
             SELECT  MAX(ThruTS) 
             FROM #ts t3 
             WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID))/* this was the missing paren*/ no_hold
         WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) AS Active
    FROM (
      SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) ActivityDuration
      FROM #ts GROUP BY VehicleInventoryItemID) d) w) y ON p.VehicleInventoryItemID = y.VehicleInventoryItemID 
WHERE p.VehicleInventoryItemID IS NOT NULL 
    
    
--FUCKFUCKFUCK    FUCKFUCKFUCKFUCKFUCK
-- losing track

lets' start again using ALL ts, including pull & pb
DROP TABLE #ts

--SELECT VehicleInventoryItemID, PulledTS as FromTS, pbTS as ThruTS
--INTO #ts
--FROM #PullToPB
--WHERE VehicleInventoryItemID IS NOT NULL
--UNION
--SELECT VehicleInventoryItemID, FromTS, ThruTS
--FROM #AllWip
--UNION SELECT VehicleInventoryItemID, FromTS, ThruTS
--FROM #OtherActivities
DROP TABLE #ts 
SELECT VehicleInventoryItemID, FromTS, ThruTS
INTO #ts
FROM #AllWip
UNION SELECT VehicleInventoryItemID, FromTS, ThruTS
FROM #OtherActivities

 
SELECT p.VehicleInventoryItemID, p.pulledTS, p.pbTS, timestampdiff(sql_tsi_hour, pulledTS, pbTS),
  (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota) AS BusHours,
  y.ActivityDuration, y.Active, y.InActive
FROM #PullToPb p
LEFT JOIN (
  SELECT VehicleInventoryItemID, ActivityDuration, Active, ActivityDuration - Active AS Inactive
  FROM (
    SELECT
       d.VehicleInventoryItemID, 
       d.ActivityDuration, 
       d.ActivityDuration - 
       IFNULL((
         SELECT SUM((select Utilities.BusinessHoursFromInterval(no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS), 'Service') FROM system.iota))
         FROM (
           SELECT DISTINCT VehicleInventoryItemID, ThruTS 
           FROM #ts t1 
           WHERE (
             SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
             FROM #ts t2 
             WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
             AND t2.ThruTS <> t1.ThruTS ) = 0 
           AND ThruTS <> (
             SELECT  MAX(ThruTS) 
             FROM #ts t3 
             WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID)) no_hold
         WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) AS Active
    FROM (
      SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) ActivityDuration
      FROM #ts 
      GROUP BY VehicleInventoryItemID) d) w) y ON p.VehicleInventoryItemID = y.VehicleInventoryItemID 
WHERE p.VehicleInventoryItemID IS NOT NULL  

      
-- narrowing down WHERE to DO bus hours  
-- ok, here is activity duration and InActive IN Service BusHours 
    SELECT
       d.VehicleInventoryItemID, 
--       d.ActivityDuration, 
       (select Utilities.BusinessHoursFromInterval(adFrom, adThru, 'Service') FROM system.iota) AS ActivityDuration, 
       
--       d.ActivityDuration - 
--       IFNULL((
         (SELECT SUM((select Utilities.BusinessHoursFromInterval(no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS), 'Service') FROM system.iota))
         FROM (
           SELECT DISTINCT VehicleInventoryItemID, ThruTS 
           FROM #ts t1 
           WHERE (
             SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
             FROM #ts t2 
             WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
             AND t2.ThruTS <> t1.ThruTS ) = 0 
           AND ThruTS <> (
             SELECT  MAX(ThruTS) 
             FROM #ts t3 
             WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID)) no_hold
--         WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ), 0) AS Active
         WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ) AS InActive
    FROM (
--      SELECT VehicleInventoryItemID, timestampdiff( SQL_TSI_hour, min(FromTS), max(ThruTS)) ActivityDuration
      SELECT VehicleInventoryItemID, MIN(FromTS) AS adFrom, MAX(ThruTS) AS adThru
      FROM #ts 
      GROUP BY VehicleInventoryItemID) d    
-- now let's TRY to put this IN the full query        

SELECT VehicleInventoryItemID, days, BusHours, ActivityDuration, coalesce(Inactive, 0) AS InActive, BusHours - (coalesce(ActivityDuration, 0) - coalesce(Inactive, 0)) AS CapWait
INTO #jon
FROM (
SELECT p.VehicleInventoryItemID, timestampdiff(sql_tsi_day, pulledTS, pbTS) AS Days,
  (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota) AS BusHours,
  ActivityDuration, InActive  
FROM #PullToPb p
LEFT JOIN (
  SELECT
     d.VehicleInventoryItemID, 
     (select Utilities.BusinessHoursFromInterval(adFrom, adThru, 'Service') FROM system.iota) AS ActivityDuration,
     (SELECT SUM((select Utilities.BusinessHoursFromInterval(no_hold.ThruTS, (SELECT min(FromTs) FROM #ts t4 WHERE t4.VehicleInventoryItemID = no_hold.VehicleInventoryItemID and t4.FromTS > no_hold.ThruTS), 'Service') FROM system.iota))
       FROM (
         SELECT DISTINCT VehicleInventoryItemID, ThruTS 
         FROM #ts t1 
         WHERE (
           SELECT sum(IIF(t1.ThruTS between t2.FromTS and t2.ThruTS, 1, 0 )) 
           FROM #ts t2 
           WHERE t2.VehicleInventoryItemID = t1.VehicleInventoryItemID  
           AND t2.ThruTS <> t1.ThruTS ) = 0 
         AND ThruTS <> (
           SELECT  MAX(ThruTS) 
           FROM #ts t3 
           WHERE t3.VehicleInventoryItemID = t1.VehicleInventoryItemID)) no_hold
       WHERE no_hold.VehicleInventoryItemID = d.VehicleInventoryItemID ) AS InActive
  FROM (
    SELECT VehicleInventoryItemID, MIN(FromTS) AS adFrom, MAX(ThruTS) AS adThru
    FROM #ts 
    GROUP BY VehicleInventoryItemID) d) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
WHERE w.VehicleInventoryItemID IS NOT NULL) q 

DROP TABLE #jon
SELECT * FROM #jon
SELECT * FROM #counts

so what i want IS
TheWeek - FROM - Thru - COUNT - AvgDays - AvgBusHours - AvgPercCapWait

-- THIS IS IT
-- altho greg asserts that it IS useless to provide numbers outside the context of a dept
-- so will ADD ON to this
SELECT c.*, w.AvgCapWait
FROM #counts c
LEFT JOIN (
  SELECT TheWeek, round(sum(PercCapWait)/COUNT(DISTINCT j.VehicleInventoryItemID),0) AvgCapWait
  FROM #PullToPb p
  LEFT JOIN (
    SELECT VehicleInventoryItemID, (CapWait*1.0/BusHours)*100 PercCapWait
    FROM #jon) j ON p.VehicleInventoryItemID = j.VehicleInventoryItemID
  GROUP BY p.TheWeek) w ON c.TheWeek = w.TheWeek

