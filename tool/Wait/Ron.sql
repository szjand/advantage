SELECT viix.stocknumber, vix.vin, vix.yearmodel, vix.make, vix.model, 
  vix.exteriorcolor, vix.interiorcolor,
  CASE
    WHEN vrix.typ IS NULL THEN 'No Authorized Body Work'
    ELSE trim(substring(vrix.typ, position('_' IN vrix.typ) + 1, 20)) + ': ' + LEFT(cast(vrix.description AS sql_char), 100)
  END AS Description, 
  coalesce(LaborAmount, 0) AS LaborAmount,
  coalesce(
    (SELECT SUM(LaborAmount)
      FROM v.VehicleReconItems
      WHERE VehicleReconItemID = vrix.VehicleReconItemID), 0) AS TotalLabor
FROM v.VehicleInventoryItems viix
INNER JOIN v.VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID
LEFT JOIN v.VehicleReconItems vrix ON viix.VehicleInventoryItemID = vrix.VehicleInventoryItemID 
  AND vrix.typ LIKE 'Body%'
LEFT JOIN v.AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID
  AND arix.status <> 'AuthorizedReconItem_Complete'  
WHERE viix.ThruTS IS null 
ORDER BY viix.stocknumber 


