/* Base */
-- DROP TABLE _PullToPB
-- DELETE FROM _PullToPB
/*
INSERT INTO _PullToPB
SELECT p.TheWeek, TheDate, p.VehicleInventoryItemID, s.ReconSeq, p.pulledts, p.pbts,
  m.FromtS AS mWipFrom, m.ThruTS AS mWipThru, 
  coalesce(iif(timestampdiff(sql_tsi_hour, m.FromTS, m.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, m.FromTS, m.ThruTS)), 0) AS mWip, 
  b.FromTs AS bWipFrom, b.ThruTS AS bWipThru, 
  coalesce(iif(timestampdiff(sql_tsi_hour, b.FromTS, b.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, b.FromTS, b.ThruTS)), 0) AS bWip,
  a.FromTS AS aWipFrom, a.ThruTS AS aWipThru,
  coalesce(iif(timestampdiff(sql_tsi_hour, a.FromTS, a.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, a.FromTS, a.ThruTS)), 0) AS aWip,
  timestampdiff(sql_tsi_hour, pulledts, pbts) AS PullToPb,
  CASE position('m' IN ReconSeq) 
    WHEN 1 THEN pulledTS -- m**
    WHEN 2 THEN -- *m*
      CASE
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN  b.ThruTS -- bm*
        WHEN substring(ReconSeq, 1, 1) = 'a'THEN a.ThruTS -- am*
      END 
    WHEN 3 THEN -- **m
      CASE
        WHEN substring(ReconSeq, 2, 1) = 'b' THEN b.ThruTS -- *bm
        WHEN substring(ReconSeq, 2, 1) = 'a' THEN a.ThruTS -- *am
      END     
  END AS mStart,
  m.ThruTS AS mFinish,
  coalesce(
    CASE position('m' IN ReconSeq) -- m**
      WHEN 1 THEN iif(timestampdiff(sql_tsi_hour, pulledTS, m.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, pulledTS, m.ThruTS))
      WHEN 2 THEN -- *m*
        CASE
          -- bm*
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, b.ThruTS, m.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, b.ThruTS, m.ThruTS))
          -- am*
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, a.ThruTS, m.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, a.ThruTS, m.ThruTS))
        END 
      WHEN 3 THEN -- **m
        CASE
          -- *bm
          WHEN substring(ReconSeq, 2, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, b.ThruTS, m.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, b.ThruTS, m.ThruTS))
          -- *am
          WHEN substring(ReconSeq, 2, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, a.ThruTS, m.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, a.ThruTS, m.ThruTS))
        END     
    END, 0) AS mHours,
  CASE position('b' IN ReconSeq) 
    WHEN 1 THEN pulledTS -- b**
    WHEN 2 THEN -- *b*
      CASE
        WHEN substring(ReconSeq, 1, 1) = 'm' THEN m.ThruTS -- mb*
        WHEN substring(ReconSeq, 1, 1) = 'a' THEN a.ThruTS -- ab*
      END 
    WHEN 3 THEN -- **b
      CASE
        WHEN substring(ReconSeq, 2, 1) = 'm' THEN m.ThruTS -- *mb
        WHEN substring(ReconSeq, 2, 1) = 'a' THEN a.ThruTS -- *ab
      END     
  END AS bStart, 
  b.ThruTS AS bFinish,   
  coalesce(
    CASE position('b' IN ReconSeq) -- b**
      WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, b.ThruTS)
      WHEN 2 THEN -- *b*
        CASE
          -- mb*
          WHEN substring(ReconSeq, 1, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, m.ThruTS, b.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, m.ThruTS, b.ThruTS))
          -- ab*
          WHEN substring(ReconSeq, 1, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, a.ThruTS, b.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, a.ThruTS, b.ThruTS))
        END 
      WHEN 3 THEN -- **b
        CASE
          -- *mb
          WHEN substring(ReconSeq, 2, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, m.ThruTS, b.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, m.ThruTS, b.ThruTS))
          -- *ab
          WHEN substring(ReconSeq, 2, 1) = 'a' THEN iif(timestampdiff(sql_tsi_hour, a.ThruTS, b.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, a.ThruTS, b.ThruTS))
        END     
    END, 0) bHours,
  CASE position('a' IN ReconSeq) 
    WHEN 1 THEN pulledTS -- a**
    WHEN 2 THEN -- *a*
      CASE
        WHEN substring(ReconSeq, 1, 1) = 'm' THEN m.ThruTS -- ma*
        WHEN substring(ReconSeq, 1, 1) = 'b' THEN b.ThruTS -- ba*
      END 
    WHEN 3 THEN -- **a
      CASE
        WHEN substring(ReconSeq, 2, 1) = 'm' THEN m.ThruTS -- *ma
        WHEN substring(ReconSeq, 2, 1) = 'b' THEN b.ThruTS -- *ba
      END     
  END AS aStart,
  a.ThruTS AS aFinish,
  coalesce(
    CASE position('a' IN ReconSeq) -- a**
      WHEN 1 THEN timestampdiff(sql_tsi_hour, pulledTS, a.ThruTS)
      WHEN 2 THEN -- *a*
        CASE
          -- ma*
          WHEN substring(ReconSeq, 1, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, m.ThruTS, a.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, m.ThruTS, a.ThruTS)) 
          -- ba*
          WHEN substring(ReconSeq, 1, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, b.ThruTS, a.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, b.ThruTS, a.ThruTS))
        END 
      WHEN 3 THEN -- **a
        CASE
          -- *ma
          WHEN substring(ReconSeq, 2, 1) = 'm' THEN iif(timestampdiff(sql_tsi_hour, m.ThruTS, a.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, m.ThruTS, a.ThruTS))
          -- *ba
          WHEN substring(ReconSeq, 2, 1) = 'b' THEN iif(timestampdiff(sql_tsi_hour, b.ThruTS, a.ThruTS) < 1, 1, timestampdiff(sql_tsi_hour, b.ThruTS, a.ThruTS))
        END     
    END, 0) AS aHours     
FROM #PullToPB p 
--  INNER JOIN ( -- exclusions FROM multiple wip/category
--    SELECT DISTINCT VehicleInventoryItemID 
--    FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle, veh/cat multiples, AppOther
  SELECT DISTINCT(VehicleInventoryItemID)
  FROM #WipBase x
  WHERE NOT EXISTS ( -- AppOther
    SELECT 1
    FROM #wipbase
    WHERE VehicleInventoryItemID = x.VehicleInventoryItemID 
    AND category = 'AppearanceReconItem')) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
LEFT JOIN #ReconSeq s ON p.VehicleInventoryItemID = s.VehicleInventoryItemID 
LEFT JOIN #WipBase b ON p.VehicleInventoryItemID = b.VehicleInventoryItemID AND b.category = 'BodyReconItem'
LEFT JOIN #WipBase m ON p.VehicleInventoryItemID = m.VehicleInventoryItemID AND m.category = 'MechanicalReconItem'
LEFT JOIN #WipBase a ON p.VehicleInventoryItemID = a.VehicleInventoryItemID AND a.category = 'PartyCollection' 
*/

--------------------------/* Level1 */-------------------------------------BEGIN
-- GROUP BY TheWeek
-- 1 row per week 
-- Colums: The Week, Beginning (date), Ending (date), TotalHours (Pull -> PB, calendar hours)
--         Count, AvgHours/Vehicle
SELECT d.Beginning, d.Ending, a.*
FROM ( -- a: 1 row per vehicle
  SELECT x.TheWeek, 
    SUM(timestampdiff(sql_tsi_hour, x.PulledTS, x.pbTS)) AS TotalHours,
    COUNT(DISTINCT x.VehicleInventoryItemID) AS [Count],
    SUM(timestampdiff(sql_tsi_hour, cast(x.PulledTS AS sql_date), cast(x.pbTS AS sql_date)))/COUNT(DISTINCT x.VehicleInventoryItemID) AS [Avg Hours/Vehicle]
  FROM _PullToPB x
  GROUP BY x.TheWeek) a
LEFT JOIN ( -- d: 1 row per week, this IS necessary to get the beginning AND END dates for the seeks
  SELECT TheWeek, MIN(TheDate) AS Beginning, MAX(theDate) AS Ending
  FROM ( -- da: 1 row per day for 21 weeks
    SELECT d.*, 
      CASE 
        WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
        WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
        WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
        WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
        WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
        WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
        WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
        WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
        WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
        WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
        WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
        WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
        WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
        WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
        WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
        WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
        WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17 
        WHEN TheDate >= CurDate() - 125 AND TheDate <= CurDate() - 119 THEN 18 
        WHEN TheDate >= CurDate() - 132 AND TheDate <= CurDate() - 120 THEN 19 
        WHEN TheDate >= CurDate() - 139 AND TheDate <= CurDate() - 127 THEN 20 
        WHEN TheDate >= CurDate() - 146 AND TheDate <= CurDate() - 134 THEN 21 
      END AS TheWeek
    FROM dds.day d
    WHERE TheDate >= CurDate() - 146
      AND TheDate <= CurDate()) da 
  GROUP BY TheWeek) d ON a.TheWeek = d.TheWeek;  
  
-- Pull -> PB XmR chart
-- AverageHoursPerVehicle by Week
SELECT TheWeek, AvgPPBHoursPerVehicle AS "PPB per Vehicle", ppbAvg, mR, mrAvg AS "Avg mR", URL, ppbAvg + (2.66 * mrAvg) AS UCL, 
  ppbAvg - (2.66 * mrAvg) AS LCL
FROM (
  SELECT x.TheWeek, SUM(x.PullToPB)/COUNT(VehicleInventoryItemID) AS AvgPPBHoursPerVehicle,
    max(ppbXAvg) AS ppbAvg, max(mr) AS mR, MAX(mrAvg) AS mrAvg, MAX(URL) AS URL
  FROM _PullToPB x
  LEFT JOIN ( -- y: Avg Hours pull -> pb
    SELECT avg(y.AvgPPBHoursPerVehicle) AS ppbXAvg
    FROM (
      SELECT TheWeek, SUM(PullToPB)/COUNT(VehicleInventoryItemID) AS AvgPPBHoursPerVehicle
      FROM _PullToPB x
      GROUP BY theweek) y) w ON 1 = 1
  LEFT JOIN ( -- r: moving range
    SELECT q.TheWeek, r.mR
    FROM (
      SELECT TheWeek, SUM(PullToPB)/COUNT(VehicleInventoryItemID) AS AvgPPBHoursPerVehicle
      FROM _PullToPB x
      GROUP BY theweek) q
    LEFT JOIN (
      SELECT a.theweek,  abs(a.AvgPPBHoursPerVehicle - b.AvgPPBHoursPerVehicle) AS mR
      FROM ( -- cartesian product of week AND AvgHoursPerVehicle, a, with itself, b
        SELECT TheWeek, SUM(PullToPB)/COUNT(VehicleInventoryItemID) AS AvgPPBHoursPerVehicle
        FROM _PullToPB x
        GROUP BY theweek) a,
        (SELECT TheWeek, SUM(PullToPB)/COUNT(VehicleInventoryItemID) AS AvgPPBHoursPerVehicle
        FROM _PullToPB x
        GROUP BY theweek) b   
      WHERE a.TheWeek = b.TheWeek - 1) r ON r.theweek = q.theweek) d ON x.TheWeek = d.TheWeek   
  LEFT JOIN (
    SELECT avg(mr) AS mRavg, 3.27*avg(mR) AS URL 
    FROM (  
      SELECT q.TheWeek, r.mR
      FROM (
        SELECT TheWeek, SUM(PullToPB) AS ppb
        FROM _PullToPB x
        GROUP BY theweek) q
      LEFT JOIN (
        SELECT a.theweek,  abs(a.AvgPPBHoursPerVehicle - b.AvgPPBHoursPerVehicle) AS mR
        FROM ( -- cartesian product of week AND AvgHoursPerVehicle, a, with itself, b 
          SELECT TheWeek, SUM(PullToPB)/COUNT(VehicleInventoryItemID) AS AvgPPBHoursPerVehicle
          FROM _PullToPB x
          GROUP BY theweek) a,
          (SELECT TheWeek, SUM(PullToPB)/COUNT(VehicleInventoryItemID) AS AvgPPBHoursPerVehicle
          FROM _PullToPB x
          GROUP BY theweek) b   
        WHERE a.TheWeek = b.TheWeek - 1) r ON r.theweek = q.theweek) v) e ON 1 = 1  
  GROUP BY x.theweek) a   
ORDER BY TheWeek DESC   

----------------------------/* Level1 */-------------------------------------END

        

/* Level2 */


SELECT x.TheWeek, COUNT(x.VehicleInventoryItemID) AS [Count],
  SUM(timestampdiff(sql_tsi_hour, x.PulledTS, x.pbTS))/COUNT(x.VehicleInventoryItemID) AS AvgHoursPerVehicle,
  SUM(x.mWip + x.bWip + x.aWip)/COUNT(x.VehicleInventoryItemID) AS AvgWipHoursPerVehicle,
  SUM(m.MoveHours)/COUNT(x.VehicleInventoryItemID) AS AvgMoveHoursPerVehicle,
  coalesce(SUM(w.WTFHours)/COUNT(x.VehicleInventoryItemID), 0) AS AvgWTFHoursPerVehicle,
  coalesce(SUM(p.PartsHours)/COUNT(x.VehicleInventoryItemID), 0) AS AvgPartsHoursPerVehicle
FROM _PullToPB x
LEFT JOIN (
  SELECT VehicleInventoryItemID, 
    sum(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS MoveHours 
  FROM #OtherActivities
  WHERE Activity = 'Move'
  GROUP BY VehicleInventoryItemID) m ON x.VehicleInventoryItemID = m.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, 
    sum(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS WTFHours 
  FROM #OtherActivities
  WHERE Activity = 'WTF'
  GROUP BY VehicleInventoryItemID) w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID  
LEFT JOIN (
  SELECT VehicleInventoryItemID, 
    sum(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS PartsHours 
  FROM #OtherActivities
  WHERE Activity = 'Parts'
  GROUP BY VehicleInventoryItemID) p ON x.VehicleInventoryItemID = p.VehicleInventoryItemID   
GROUP BY x.TheWeek

-- Capacity Hours.xlsx 
-- Capacity Hours Chart
SELECT TheWeek, [Count], AvgHoursPerVehicle AS "PPB Hours", AvgWipHoursPerVehicle AS "WIP Hours",
  AvgMoveHoursPerVehicle AS "Move Hours", AvgWTFHoursPerVehicle AS "WTF Hours",
  AvgPartsHoursPerVehicle AS "Parts Hours",
  AvgHoursPerVehicle - AvgWipHoursPerVehicle - AvgMoveHoursPerVehicle - AvgWTFHoursPerVehicle - AvgPartsHoursPerVehicle AS "Capacity Hours"
FROM (  
  SELECT x.TheWeek, COUNT(x.VehicleInventoryItemID) AS [Count],
    SUM(timestampdiff(sql_tsi_hour, x.PulledTS, x.pbTS))/COUNT(x.VehicleInventoryItemID) AS AvgHoursPerVehicle,
    SUM(x.mWip + x.bWip + x.aWip)/COUNT(x.VehicleInventoryItemID) AS AvgWipHoursPerVehicle,
    SUM(m.MoveHours)/COUNT(x.VehicleInventoryItemID) AS AvgMoveHoursPerVehicle,
    coalesce(SUM(w.WTFHours)/COUNT(x.VehicleInventoryItemID), 0) AS AvgWTFHoursPerVehicle,
    coalesce(SUM(p.PartsHours)/COUNT(x.VehicleInventoryItemID), 0) AS AvgPartsHoursPerVehicle
  FROM _PullToPB x
  LEFT JOIN (
    SELECT VehicleInventoryItemID, 
      sum(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS MoveHours 
    FROM #OtherActivities
    WHERE Activity = 'Move'
    GROUP BY VehicleInventoryItemID) m ON x.VehicleInventoryItemID = m.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, 
      sum(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS WTFHours 
    FROM #OtherActivities
    WHERE Activity = 'WTF'
    GROUP BY VehicleInventoryItemID) w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID  
  LEFT JOIN (
    SELECT VehicleInventoryItemID, 
      sum(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS PartsHours 
    FROM #OtherActivities
    WHERE Activity = 'Parts'
    GROUP BY VehicleInventoryItemID) p ON x.VehicleInventoryItemID = p.VehicleInventoryItemID   
  GROUP BY x.TheWeek) a

-- 10/11/11
-- separate out departments
-- so the range of effect of other activities IS sequence based
-- which IS already figured IN mStart, mFinish, etc


-- Capacity Hours.xlsx  
-- body shop, looks LIKE it works
SELECT x.TheWeek, 
  SUM(bHours)/COUNT(x.VehicleInventoryItemID) AS BodyHoursPerVehicle, 
  SUM(bWip)/COUNT(x.VehicleInventoryItemID) AS BodyWipPerVehicle, 
  coalesce(SUM(mo.MoveHours), 0)/COUNT(x.VehicleInventoryItemID) AS Move,
  coalesce(SUM(w.WTFHours), 0)/COUNT(x.VehicleInventoryItemID) AS WTF,
  (SUM(bHours) - SUM(bWip) - coalesce(SUM(mo.MoveHours), 0) - coalesce(SUM(w.WTFHours), 0))/COUNT(x.VehicleInventoryItemID) AS WaitingPerVehicle,
  COUNT(x.VehicleInventoryItemID) AS TheCount
-- SELECT * 
FROM _PullToPB x
LEFT JOIN ( -- mo: VehicleInventoryItemID, 
  SELECT VehicleInventoryItemID, 
    SUM(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS MoveHours
  FROM ( -- m: VehicleInventoryItemID, FromTS, ThruTS  -- one row for VehicleInventoryItemID, FromTS    
    SELECT a.VehicleInventoryItemID, a.FromTS, MAX(a.ThruTS) AS ThruTS
    FROM #OtherActivities a
    INNER JOIN _PullToPB b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND b.bHours <> 0 -- body hours
      AND a.ThruTS > b.bStart AND a.FromTS < b.bFinish
    WHERE a.Activity = 'Move'
    GROUP BY a.VehicleInventoryItemID, a.FromTS) m 
  GROUP BY VehicleInventoryItemID) mo ON x.VehicleInventoryItemID = mo.VehicleInventoryItemID 
LEFT JOIN ( -- w: one row per vehicle, total WTF Hours
  SELECT VehicleInventoryItemID, 
    SUM(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS WTFHours
  FROM (    
    SELECT a.VehicleInventoryItemID, a.FromTS, MAX(a.ThruTS) AS ThruTS
    FROM #OtherActivities a
    INNER JOIN _PullToPB b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND b.bHours <> 0 -- body hours
      AND a.ThruTS > b.bStart AND a.FromTS < b.bFinish
    WHERE a.Activity = 'WTF'
    GROUP BY a.VehicleInventoryItemID, a.FromTS) wtf 
  GROUP BY VehicleInventoryItemID) w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID   
WHERE bHours <> 0
GROUP BY x.TheWeek  

-- Capacity Hours.xlsx  
-- appearance
SELECT x.TheWeek, 
  SUM(aHours)/COUNT(x.VehicleInventoryItemID) AS AppHours, 
  SUM(aWip)/COUNT(x.VehicleInventoryItemID) AS AppWip, 
  coalesce(SUM(mo.MoveHours), 0)/COUNT(x.VehicleInventoryItemID) AS Move,
  coalesce(SUM(w.WTFHours), 0)/COUNT(x.VehicleInventoryItemID) AS WTF,
  (SUM(aHours) - SUM(aWip) - coalesce(SUM(mo.MoveHours), 0) - coalesce(SUM(w.WTFHours), 0))/COUNT(x.VehicleInventoryItemID) AS Waiting,
  COUNT(x.VehicleInventoryItemID) AS TheCount
FROM _PullToPB x
LEFT JOIN ( -- mo: VehicleInventoryItemID, 
  SELECT VehicleInventoryItemID, 
    SUM(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS MoveHours
  FROM ( -- m: VehicleInventoryItemID, FromTS, ThruTS  -- one row for VehicleInventoryItemID, FromTS    
    SELECT a.VehicleInventoryItemID, a.FromTS, MAX(a.ThruTS) AS ThruTS
    FROM #OtherActivities a
    INNER JOIN _PullToPB b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND b.aHours <> 0 -- app hours
      AND a.ThruTS > b.aStart AND a.FromTS < b.aFinish
    WHERE a.Activity = 'Move'
    GROUP BY a.VehicleInventoryItemID, a.FromTS) m 
  GROUP BY VehicleInventoryItemID) mo ON x.VehicleInventoryItemID = mo.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, 
    SUM(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS WTFHours
  FROM (    
    SELECT a.VehicleInventoryItemID, a.FromTS, MAX(a.ThruTS) AS ThruTS
    FROM #OtherActivities a
    INNER JOIN _PullToPB b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND b.bHours <> 0 -- app hours
      AND a.ThruTS > b.aStart AND a.FromTS < b.aFinish
    WHERE a.Activity = 'WTF'
    GROUP BY a.VehicleInventoryItemID, a.FromTS) wtf 
  GROUP BY VehicleInventoryItemID) w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID   
WHERE aHours <> 0
GROUP BY x.TheWeek 
  
-- Capacity Hours.xlsx  
-- mech
-- need to ADD parts
SELECT x.TheWeek, 
  SUM(mHours)/COUNT(x.VehicleInventoryItemID) AS MechHours,
  SUM(mWIP)/COUNT(x.VehicleInventoryItemID) AS WIP,
  coalesce(SUM(mo.MoveHours), 0)/COUNT(x.VehicleInventoryItemID) AS Move,
  coalesce(SUM(w.WTFHours), 0)/COUNT(x.VehicleInventoryItemID) AS WTF,
  coalesce(SUM(p.PartsHours), 0)/COUNT(x.VehicleInventoryItemID) AS Parts,
  (SUM(mHours) - SUM(mWip) - coalesce(SUM(mo.MoveHours), 0) - coalesce(SUM(w.WTFHours), 0) - coalesce(SUM(p.PartsHours), 0))/COUNT(x.VehicleInventoryItemID) AS Waiting,
  COUNT(x.VehicleInventoryItemID) AS TheCount
-- SELECT *
FROM _PullToPB x
LEFT JOIN ( -- mo: VehicleInventoryItemID, 
  SELECT VehicleInventoryItemID, 
    SUM(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS MoveHours
  FROM ( -- m: VehicleInventoryItemID, FromTS, ThruTS  -- one row for VehicleInventoryItemID, FromTS    
    SELECT a.VehicleInventoryItemID, a.FromTS, MAX(a.ThruTS) AS ThruTS
    FROM #OtherActivities a
    INNER JOIN _PullToPB b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND b.mHours <> 0 -- mech hours
      AND a.ThruTS > b.mStart AND a.FromTS < b.mFinish
    WHERE a.Activity = 'Move'
    GROUP BY a.VehicleInventoryItemID, a.FromTS) m 
  GROUP BY VehicleInventoryItemID) mo ON x.VehicleInventoryItemID = mo.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, 
    SUM(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS WTFHours
  FROM (    
    SELECT a.VehicleInventoryItemID, a.FromTS, MAX(a.ThruTS) AS ThruTS
    FROM #OtherActivities a
    INNER JOIN _PullToPB b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      AND b.mHours <> 0 -- mech hours
      AND a.ThruTS > b.mStart AND a.FromTS < b.mFinish
    WHERE a.Activity = 'WTF'
    GROUP BY a.VehicleInventoryItemID, a.FromTS) wtf 
  GROUP BY VehicleInventoryItemID) w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID   
-- stub IN parts
LEFT JOIN (
  SELECT VehicleInventoryItemID, 
    SUM(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS PartsHours
  FROM (    
    SELECT a.VehicleInventoryItemID, a.FromTS, MAX(a.ThruTS) AS ThruTS
    FROM #OtherActivities a
    INNER JOIN _PullToPB b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID -- only those vehicles
      AND b.mHours <> 0 -- with mech hours
      AND a.ThruTS > b.mStart AND a.FromTS < b.mFinish -- AND parts ORDER recvd after mStart AND ordered before mFinish
    WHERE a.Activity = 'Parts'
    GROUP BY a.VehicleInventoryItemID, a.FromTS) parts
  GROUP BY VehicleInventoryItemID) p ON x.VehicleInventoryItemID = p.VehicleInventoryItemID   
WHERE mHours <> 0
GROUP BY x.TheWeek   



  
  
  
  
  
  
  
SELECT x.TheWeek, x.VehicleInventoryItemID, bWipFrom, bWipThru, bWip, bStart, bFinish, bHours,
  m.*
FROM _PullToPB x
LEFT JOIN (
  SELECT VehicleInventoryItemID, FromTS, max(ThruTS) AS ThruTS
  FROM #OtherActivities
  WHERE Activity = 'Move'
  GROUP BY VehicleInventoryItemID, FromTS) m ON x.VehicleInventoryItemID = m.VehicleInventoryItemID 
    AND m.ThruTS > x.bStart AND m.FromTS < x.bFinish
WHERE bHours <> 0  

-- this looks sold for bodyshop move hours  
SELECT VehicleInventoryItemID, 
  sum(iif(timestampdiff(sql_tsi_hour, FromTS, ThruTS) = 0, 1, timestampdiff(sql_tsi_hour, FromTS, ThruTS))) AS MoveHours
FROM (    
  SELECT a.VehicleInventoryItemID, a.FromTS, MAX(a.ThruTS) AS ThruTS
  FROM #OtherActivities a
  INNER JOIN _PullToPB b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.bHours <> 0
    AND a.ThruTS > b.bStart AND a.FromTS < b.bFinish
  WHERE a.Activity = 'Move'
  GROUP BY a.VehicleInventoryItemID, a.FromTS) m 
GROUP BY VehicleInventoryItemID 
  

  
SELECT COUNT(*) 
FROM #OtherActivities
WHERE Activity = 'Move'  
  
  
  
  
  
  
  
-- mech   
SELECT theweek, mHours, mHoursAvg, mR, mrAvg, URL, mHoursAvg + (2.66 * mrAvg) AS UNPL, 
  mHoursAvg - (2.66 * mrAvg) AS LNPL
FROM (
  SELECT x.TheWeek, SUM(x.mHours) AS mHours, max(mHoursXAvg) AS mHoursAvg, max(mr) AS mR, 
    MAX(mrAvg) AS mrAvg, MAX(URL) AS URL
  FROM _PullToPB x
  LEFT JOIN (
    SELECT avg(mHours) AS mHoursXAvg
    FROM (
      SELECT TheWeek, 
        SUM(mHours) AS mHours
      FROM _PullToPB x
      GROUP BY theweek) y) b ON 1 = 1
  LEFT JOIN (
    SELECT q.TheWeek, r.mR
    FROM (
      SELECT TheWeek, SUM(mHours) AS mHours
      FROM _PullToPB x
      GROUP BY theweek) q
    LEFT JOIN (
      SELECT a.theweek,  abs(a.mHours - b.mHours) AS mR
      FROM (  
        SELECT TheWeek, SUM(mHours) AS mHours
        FROM _PullToPB x
        GROUP BY theweek) a,
        (SELECT TheWeek, SUM(mHours) AS mHours
        FROM _PullToPB x
        GROUP BY theweek) b  
      WHERE a.TheWeek = b.TheWeek - 1) r ON r.theweek = q.theweek) d ON x.TheWeek = d.TheWeek   
  LEFT JOIN (
    SELECT avg(mr) AS mRavg, 3.27*avg(mR) AS URL 
    FROM (  
      SELECT q.TheWeek, r.mR
      FROM (
        SELECT TheWeek, SUM(mHours) AS mHours
        FROM _PullToPB x
        GROUP BY theweek) q
      LEFT JOIN (
        SELECT a.theweek,  abs(a.mHours - b.mHours) AS mR
        FROM (  
          SELECT TheWeek, SUM(mHours) AS mHours
          FROM _PullToPB x
          GROUP BY theweek) a,
          (SELECT TheWeek, SUM(mHours) AS mHours
          FROM _PullToPB x
          GROUP BY theweek) b  
        WHERE a.TheWeek = b.TheWeek - 1) r ON r.theweek = q.theweek) v) e ON 1 = 1    
  GROUP BY x.theweek) a     

*/



/* Level 3 */
/*
-- percentage of time total time spent IN wip
-- SELECT * FROM _PullToPB
SELECT TheWeek, 
  SUM(timestampdiff(sql_tsi_hour, pulledts, pbts)) AS ppbTotal,
  SUM(mHours) mTotal,
  SUM(mHours) - SUM(iif(timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru))) AS mNonWip,
  SUM(iif(timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru))) AS mWip,
  round(SUM(iif(timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru)))*100.0/SUM(mHours), 0) mWipPerc,
  
  SUM(bHours) bTotal,
  SUM(bHours) - SUM(iif(timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru))) AS bNonWip, 
  SUM(iif(timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru))) AS bWip,
  round(SUM(iif(timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru)))*100.0/SUM(bHours), 0) bWipPerc,
  
  SUM(aHours) aTotal,
  SUM(aHours) - SUM(iif(timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru))) AS aNonWip, 
  SUM(iif(timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru))) AS aWip,
  round(SUM(iif(timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru)))*100.0/SUM(aHours), 0) aWipPerc
-- SELECT * 
FROM _PullToPB x  --WHERE theweek = 4 AND reconseq LIKE '%m%'
GROUP BY TheWeek


-- percentage of time total time spent IN wip
-- SELECT * FROM #LevelBase
SELECT TheWeek, 
  SUM(timestampdiff(sql_tsi_hour, pulledts, pbts)) AS ppbTotal,
  SUM(
    coalesce(iif(timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru)), 0) +
    coalesce(iif(timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru)), 0) + 
    coalesce(iif(timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru)), 0)) AS TotalWip,
  round(
    SUM(
      coalesce(iif(timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru)), 0) +
      coalesce(iif(timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru)), 0) + 
      coalesce(iif(timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru)), 0))*100.0/SUM(timestampdiff(sql_tsi_hour, pulledts, pbts)), 2) AS TotalTouchTime,
          
  SUM(mHours) mTotal,
  SUM(iif(timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru))) AS mWip,
  round(
    SUM(iif(timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru)))*100.0/SUM(mHours), 2) AS "mWip/mTotal",
  SUM(bHours) bTotal,

  SUM(iif(timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru))) AS bWip,
  round(
    SUM(iif(timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.bWipFrom, x.bWipThru)))*100.0/SUM(bHours), 2) AS "bWip/bTotal",
  SUM(aHours) aTotal,

  SUM(iif(timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru))) AS aWip,
  round(
    SUM(iif(timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.aWipFrom, x.aWipThru)))*100.0/SUM(aHours), 2) AS "aWip/aTotal"
  
-- SELECT * 
FROM _PullToPB x  --WHERE theweek = 4 AND reconseq LIKE '%m%'
GROUP BY TheWeek


*/

-- SELECT DISTINCT activity FROM #OtherActivities
-- first at a higher level AS perc of dept time
-- SELECT * FROM #LevelBase 
SELECT TheWeek, 
  SUM(mHours) mTotal,
  SUM(iif(timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru))) AS mWip,
  round(SUM(iif(timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru) < 1, 1, timestampdiff(sql_tsi_hour, x.mWipFrom, x.mWipThru)))*100.0/SUM(mHours), 0) mWipPerc
FROM _PullToPB x
LEFT JOIN #OtherActivities p ON x.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND p.activity = 'Parts'
LEFT JOIN #OtherActivities m ON x.VehicleInventoryItemID = m.VehicleInventoryItemID 
  AND m.activity = 'Move'  
LEFT JOIN #OtherActivities w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID 
  AND w.activity = 'Move'   
GROUP BY TheWeek  


--       mStart                                             mFinish
--         |----------------------------------------------------|
--   parts     
--     |--------------|
--       parts
--         |----------------|
--                               parts
--                                 |---------------------|
--                                                parts
--                                                  |----------------------|

SELECT x.TheWeek, x.VehicleInventoryItemID, ReconSeq, x.mStart,x. mFinish, p.FromTS, p.ThruTS,
  timestampdiff(sql_tsi_hour, mStart, mFinish) AS mHours
  

FROM #LevelBase x 
LEFT JOIN #OtherActivities p ON x.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND p.activity = 'Parts'
--LEFT JOIN #OtherActivities m ON x.VehicleInventoryItemID = m.VehicleInventoryItemID 
--  AND m.activity = 'Move'  
--LEFT JOIN #OtherActivities w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID 
--  AND w.activity = 'Move'  
WHERE theweek > 0
AND p.VehicleInventoryItemID IS NOT NULL 

-- SELECT * FROM #LevelBase
-- using a LEFT JOIN for parts (#OtherActivities) requires an INNER grouping ON VehicleInventoryItemID 
-- thinking subselect for parts needs to be grouped
-- handle parts ORDER before mStart
-- handle NULL parts ThruTS

SELECT max(x.TheWeek) AS week , x.VehicleInventoryItemID, max(ReconSeq), 
  MAX(mHours) AS mHours, MAX(mWip) AS mWip,

  max(x.mStart) AS mStart,
  max(x. mFinish) AS mFinish, max(p.FromTS) AS pFrom, max(p.ThruTS) AS pThru



FROM #LevelBase x 
LEFT JOIN #OtherActivities p ON x.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND p.activity = 'Parts'
--LEFT JOIN #OtherActivities m ON x.VehicleInventoryItemID = m.VehicleInventoryItemID 
--  AND m.activity = 'Move'  
--LEFT JOIN #OtherActivities w ON x.VehicleInventoryItemID = w.VehicleInventoryItemID 
--  AND w.activity = 'Move'  
WHERE theweek > 0
AND p.VehicleInventoryItemID IS NOT NULL 
GROUP BY x.VehicleInventoryItemID 

SELECT *
-- SELECT COUNT(*) -- 141
FROM #OtherActivities
WHERE Activity = 'Parts'

-- DROP TABLE #parts
-- guarantees unique FromTS for each VehicleInventoryItemID 
SELECT VehicleInventoryItemID, FromTS, MAX(ThruTS) AS ThruTS
INTO #parts
FROM #OtherActivities
WHERE Activity = 'Parts'
GROUP BY VehicleInventoryItemID, FromTS
-- need to account for overlappint
--  parts
--   |--------------|
--        parts
--          |------------------|
-- total parts time IS
--   |-------------------------|
-- NOT the SUM of the 2

SELECT * FROM #pm

SELECT * 
INTO #pm
FROM #parts p
WHERE EXISTS (
  SELECT 1
  FROM #parts
  WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
  GROUP BY VehicleInventoryItemID 
  HAVING COUNT(*) > 1)
-- CLOSE
-- but excludes 0 time parts orders
SELECT p1.VehicleInventoryItemID, p1.FromTS, MAX(p2.ThruTS) AS ThruTS
FROM #pm p1 
INNER JOIN (
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #pm
  GROUP BY VehicleInventoryItemID, FromTS, ThruTS) p2 ON p1.VehicleInventoryItemID = p2.VehicleInventoryItemID 
    AND p2.ThruTS > p1.FromTS
AND NOT EXISTS (   
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #pm p3
  WHERE timestampadd(sql_tsi_second, -1, p1.FromtS) BETWEEN p3.FromTS AND p3.ThruTS
  GROUP BY VehicleInventoryItemID, FromTS, ThruTS)  
AND NOT EXISTS (
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #pm p4
  WHERE p4.ThruTS > p1.FromTS
  AND p4.ThruTS < p2.ThruTS
  AND NOT EXISTS (
    SELECT VehicleInventoryItemID, FromTS, ThruTS 
    FROM #pm p5
    WHERE timestampadd(sql_tsi_second, 1, p4.ThruTS) BETWEEN p5.FromTS AND p5.ThruTS
    GROUP BY VehicleInventoryItemID, FromTS, ThruTS)  
  GROUP BY VehicleInventoryItemID, FromTS, ThruTS)   
GROUP BY p1.VehicleInventoryItemID, p1.FromTS

-- aaaaahhhhhhhhhhaaaaaaa LEFT out the VehicleInventoryItemID IN the EXISTS clauses
-- but it does NOT pick up 0 time parts orders
SELECT * FROM #pm a
LEFT JOIN (
SELECT p1.VehicleInventoryItemID, p1.FromTS, MAX(p2.ThruTS) AS ThruTS
FROM #pm p1 
INNER JOIN (
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #pm
  GROUP BY VehicleInventoryItemID, FromTS, ThruTS) p2 ON p1.VehicleInventoryItemID = p2.VehicleInventoryItemID 
    AND p2.ThruTS > p1.FromTS
AND NOT EXISTS (   
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #pm p3
  WHERE VehicleInventoryItemID = p1.VehicleInventoryItemID 
  AND timestampadd(sql_tsi_second, -1, p1.FromtS) BETWEEN p3.FromTS AND p3.ThruTS
  GROUP BY VehicleInventoryItemID, FromTS, ThruTS)  
AND NOT EXISTS (
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #pm p4
  WHERE VehicleInventoryItemID = p1.VehicleInventoryItemID 
  AND p4.ThruTS > p1.FromTS
  AND p4.ThruTS < p2.ThruTS
  AND NOT EXISTS (
    SELECT VehicleInventoryItemID, FromTS, ThruTS 
    FROM #pm p5
    WHERE VehicleInventoryItemID = p4.VehicleInventoryItemID 
    AND timestampadd(sql_tsi_second, 1, p4.ThruTS) BETWEEN p5.FromTS AND p5.ThruTS
    GROUP BY VehicleInventoryItemID, FromTS, ThruTS)  
  GROUP BY VehicleInventoryItemID, FromTS, ThruTS)   
GROUP BY p1.VehicleInventoryItemID, p1.FromTS) b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID AND a.FromTS = b.FromTS
---------------------------------------------
-- DO i need the internal grouping?
-- looks LIKE NOT, so DO the big join
SELECT p1.VehicleInventoryItemID, p1.FromTS, MAX(p2.ThruTS) AS ThruTS
FROM #pm p1 
INNER JOIN (
  SELECT *
  FROM #pm) p2 ON p1.VehicleInventoryItemID = p2.VehicleInventoryItemID 
    AND p2.ThruTS > p1.FromTS
AND NOT EXISTS (   
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #pm p3
  WHERE VehicleInventoryItemID = p1.VehicleInventoryItemID 
  AND timestampadd(sql_tsi_second, -1, p1.FromtS) BETWEEN p3.FromTS AND p3.ThruTS)  
AND NOT EXISTS (
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #pm p4
  WHERE VehicleInventoryItemID = p1.VehicleInventoryItemID 
  AND p4.ThruTS > p1.FromTS
  AND p4.ThruTS < p2.ThruTS
  AND NOT EXISTS (
    SELECT VehicleInventoryItemID, FromTS, ThruTS 
    FROM #pm p5
    WHERE VehicleInventoryItemID = p4.VehicleInventoryItemID 
    AND timestampadd(sql_tsi_second, 1, p4.ThruTS) BETWEEN p5.FromTS AND p5.ThruTS))   
GROUP BY p1.VehicleInventoryItemID, p1.FromTS

-- 0 time wip fucks it up, for one, they get LEFT out
-- AND b74edc51-cde2-4ae3-894a-bbf6c81a2919
-- NULL ThruTS fucks it up
SELECT * FROM #parts a
LEFT JOIN (
SELECT p1.VehicleInventoryItemID, p1.FromTS, MAX(p2.ThruTS) AS ThruTS
FROM #parts p1 
INNER JOIN (
  SELECT *
  FROM #parts) p2 ON p1.VehicleInventoryItemID = p2.VehicleInventoryItemID 
    AND p2.ThruTS > p1.FromTS
AND NOT EXISTS (   
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #parts p3
  WHERE VehicleInventoryItemID = p1.VehicleInventoryItemID 
  AND timestampadd(sql_tsi_second, -1, p1.FromtS) BETWEEN p3.FromTS AND p3.ThruTS)  
AND NOT EXISTS (
  SELECT VehicleInventoryItemID, FromTS, ThruTS 
  FROM #parts p4
  WHERE VehicleInventoryItemID = p1.VehicleInventoryItemID 
  AND p4.ThruTS > p1.FromTS
  AND p4.ThruTS < p2.ThruTS
  AND NOT EXISTS (
    SELECT VehicleInventoryItemID, FromTS, ThruTS 
    FROM #parts p5
    WHERE VehicleInventoryItemID = p4.VehicleInventoryItemID 
    AND timestampadd(sql_tsi_second, 1, p4.ThruTS) BETWEEN p5.FromTS AND p5.ThruTS))   
GROUP BY p1.VehicleInventoryItemID, p1.FromTS) b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID AND a.FromTS = b.FromTS

SELECT * FROM #parts
WHERE  timestampdiff(sql_tsi_minute, FromTS, ThruTS) <1
ORDER BY FromTS

SELECT timestampdiff(sql_tsi_minute, FromTS, ThruTS), COUNT(*) 
FROM #parts
GROUP BY timestampdiff(sql_tsi_minute, FromTS, ThruTS) 

-- parts ordering process IS fucked up, OR at least undecipherable
-- 

SELECT timestampdiff(sql_tsi_minute, ORderedTS, ReceivedTS), COUNT(*) 
FROM partsorders
WHERE ReceivedTS IS NOT NULL 
GROUP BY timestampdiff(sql_tsi_minute, ORderedTS, ReceivedTS)

select * FROM partsorders

SELECT *
FROM (
SELECT COUNT(*) AS "Parts Orders", orderedby,
  SUM(CASE WHEN timestampdiff(sql_tsi_minute, ORderedTS, ReceivedTS) = 0 THEN 1 END) AS "0 Time"
FROM (
SELECT j1.VehicleInventoryItemID, 
  CASE
    WHEN p.orderedTS < PulledTS THEN PulledTS
    ELSE p.orderedTS 
  END AS OrderedTS,
  CASE
    WHEN p.receivedTS > pbTS THEN pbTS
    ELSE p.receivedTS
  END AS ReceivedTS, 
  p.OrderedBy
FROM #PullToPB j1
LEFT JOIN (
  SELECT po.*, (SELECT VehicleInventoryItemID FROM VehicleReconItems WHERE VehicleReconItemID = po.VehicleReconItemID) AS VehicleInventoryItemID 
  FROM partsorders po
  WHERE po.CancelledTS IS NULL) p ON j1.VehicleInventoryItemID = p.VehicleInventoryItemID -- exclude Cancelled Orders
    AND (p.OrderedTS < j1.pbTS AND (p.ReceivedTS > j1.pulledTS OR p.OrderedTS >= j1.pulledTS)) -- ordered before PB AND ((received after pulled) OR (ordered ON OR after Pull))
where j1.VehicleInventoryItemID IS NOT null 
AND orderedts IS NOT NULL) b
WHERE orderedby IS NOT NULL
GROUP BY orderedby) a 
LEFT JOIN people p ON a.orderedby = p.partyid


SELECT j1.VehicleInventoryItemID, 
  CASE
    WHEN p.orderedTS < PulledTS THEN PulledTS
    ELSE p.orderedTS 
  END AS OrderedTS,
  CASE
    WHEN p.receivedTS > pbTS THEN pbTS
    ELSE p.receivedTS
  END AS ReceivedTS, 
  p.OrderedBy
FROM #PullToPB j1
LEFT JOIN (
  SELECT po.*, (SELECT VehicleInventoryItemID FROM VehicleReconItems WHERE VehicleReconItemID = po.VehicleReconItemID) AS VehicleInventoryItemID 
  FROM partsorders po
  WHERE po.CancelledTS IS NULL) p ON j1.VehicleInventoryItemID = p.VehicleInventoryItemID -- exclude Cancelled Orders
    AND (p.OrderedTS < j1.pbTS AND (p.ReceivedTS > j1.pulledTS OR p.OrderedTS >= j1.pulledTS)) -- ordered before PB AND ((received after pulled) OR (ordered ON OR after Pull))
where j1.VehicleInventoryItemID IS NOT null 
AND orderedts IS NOT NULL   

