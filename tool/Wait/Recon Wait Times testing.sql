SELECT * FROM VehicleInventoryItems WHERE VehicleInventoryItemID = '5247676f-c8af-4e66-80ab-0671fa524715'

    SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
    FROM #jon j 
    LEFT JOIN (
      SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
      FROM VehicleReconItems ri
      INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
      LEFT JOIN TypCategories t ON ri.typ = t.typ
      WHERE ar.status  = 'AuthorizedReconItem_Complete'
      AND ar.startTS IS NOT NULL 
      GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
        AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))
WHERE j.VehicleInventoryItemID = 'a21529bf-0545-4967-913a-d023420e7e04'        


select *
FROM #jon j
WHERE j.VehicleInventoryItemID = 'a21529bf-0545-4967-913a-d023420e7e04'

SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
LEFT JOIN TypCategories t ON ri.typ = t.typ
WHERE ar.status  = 'AuthorizedReconItem_Complete'
AND ar.startTS IS NOT NULL 
AND VehicleInventoryItemID = '2a71e522-5b79-4a8e-aa84-2190460bb1c7'
GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category


pull:  06/20/2011 7:31:58 AM
pb:    06/29/2011 1:40:01 PM
start: 06/29/2011 1:40:01 PM
compl: 06/29/2011 1:40:01 PM


exceptions: 14319X pulled to pb, THEN recon added after pb


'5247676f-c8af-4e66-80ab-0671fa524715'

SELECT w.*,
  CASE
    WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
    WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
    WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
    WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
    WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
      CASE
        WHEN MechStart < BodyStart THEN 'mb'
        ELSE 'bm'
      END
    WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
      CASE
        WHEN MechStart < AppStart THEN 'ma'
        ELSE 'am'
      END
    WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
      CASE
        WHEN BodyStart < AppStart THEN 'ba'
        ELSE 'ab'
      END 
    WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
      CASE 
        WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
        WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
        WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
        WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
        WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
        WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
      END 
  END AS seq     
FROM #WipStartTimes w
WHERE VehicleInventoryItemID in (
'6ac97537-ac99-4ed8-80b4-f849537e009f',
'882b7392-adb3-4b23-aabc-b2a24827c633',
'e2a12a59-de90-4fe5-a623-78db246664d2',
'e9a0abf7-d2c6-4286-82de-6553e7a05b33')

