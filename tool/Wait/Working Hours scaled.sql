-- SELECT * FROM workinghours

SELECT dept,
  SUM(iif(hour(w.Closes)= 0, 24, hour(w.Closes)) - hour(w.Opens)) AS "Open Hrs/Week",
  SUM(iif(hour(w.Closes)= 0, 24, hour(w.Closes)) - hour(w.Opens))/1.68 AS "Percent of Calendar Time"
FROM workinghours w
GROUP BY dept 