/*            
      
3.  (rec1 - ord1) + (rec4 - rec1) + (rec3 - ord3)
     8/1                                8/10
     ord1                              rec1   
      |---------------------------------|  
             8/2           8/4                     8/12      8/14                  
            ord2          rec2                    ord3      rec3     
              |-------------|                      |----------|    
                       8/3                 8/11
                      ord4                 rec4
                       |---------------------|   
                       
      |-------------------------------------------------------|  = 13 days
      |--------------------------------------|  +  |----------|  = total time 12 days

*/                         

-- DROP TABLE #po;
CREATE TABLE #po (
  id integer, 
  ts1 date,
  ts2 date,
  orderid integer);
  
-- pattern 3  

INSERT INTO #po values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date), 1);
INSERT INTO #po values (1, CAST('08/02/2011' AS sql_date), CAST('08/06/2011' AS sql_date), 2);
INSERT INTO #po values (1, CAST('08/12/2011' AS sql_date), CAST('08/14/2011' AS sql_date), 3);
INSERT INTO #po values (1, CAST('08/03/2011' AS sql_date), CAST('08/10/2011' AS sql_date), 4);

SELECT * FROM #po;

-- FROM sql cookbook, p213 determining the date diff BETWEEN the curr record AND the next record
SELECT x.*
FROM (
  SELECT e.ts1, 
    (SELECT min(d.ts1) 
      FROM #po d 
      WHERE d.ts1 > e.ts1) nex_hd
  FROM #po e) x
/*            
3.  (rec1 - ord1) + (rec4 - rec1) + (rec3 - ord3)
     8/1                             8/8
     ord1                            rec1   
      |-------------------------------|  
             8/2             8/6                    8/12      8/14                  
            ord2            rec2                    ord3      rec3     
              |---------------|                      |----------|    
                       8/3                 8/10
                      ord4                 rec4
                       |---------------------|   
     8/1                                                       8/14
      |---------------------------------------------------------|  = 13 days
                                            8/10    8/12
      |--------------------------------------|  +    |----------|  = total time 11 days

*/                         

SELECT * FROM #po
-- cartesian
-- don't think i need the min/MAX here
-- maybe i DO, a starting place -- beware of multiple = orderTS with multiple recTS
-- start with ord2
-- IN actual sql, id1, id2, etc will be AND interval, WHEN ts11 <> ts12 AND ts21 <> ts22 then
-- DO i actually need to CREATE a base TABLE with pseudo ids?
SELECT x.*, mm.minOrd, mm.maxRec,
  CASE -- one interval encompasses them all 
    WHEN ((id1 = 1 AND ts11 = minOrd AND ts21 = maxRec) 
      OR (id1 = 2 AND ts11 = minOrd AND ts21 = maxRec) 
      OR (id1 = 3 AND ts11 = minOrd AND ts21 = maxRec) 
      OR (id1 = 4 AND ts11 = minOrd AND ts21 = maxRec)) THEN maxRec - minOrd
  END, 
  CASE 
    WHEN id1 = 1 and id2 <> 1 THEN 
    CASE
      WHEN ts11 = minOrd THEN 
        CASE
          WHEN ts12 = maxRec THEN maxRec - minOrd
          ELSE 999
        END 
    END 
  END
  
-- shit this IS hard
--WHEN id1 = 1 and id2 <> 1 THEN
--  CASE WHEN ts1
-- SELECT *  
FROM (
  SELECT po1.orderid AS id1, po1.ts1 AS ts11, po1.ts2 AS ts21, 
    po2.orderid AS id2, po2.ts1 AS ts12, po2.ts2 AS ts22
  FROM #po AS po1, #po AS po2) x,
  (SELECT MIN(ts1) AS minOrd, MAX(ts2) AS maxRec  
    FROM #po) mm
    
oooh, cartesian JOIN with qualifying WHERE clauses 

SELECT *  
FROM (
  SELECT po1.orderid AS id1, po1.ts1 AS ts11, po1.ts2 AS ts21, 
      po2.orderid AS id2, po2.ts1 AS ts12, po2.ts2 AS ts22
    FROM #po AS po1, #po AS po2) x,
  (SELECT MIN(ts1) AS minOrd, MAX(ts2) AS maxRec  
    FROM #po) mm
--WHERE (ts11 < ts12 AND ts21 < ts12)  
--WHERE ((ts11 BETWEEN ts12 AND ts22) AND (ts11 <> ts12 AND ts21 <> ts22))
WHERE ts21 < maxRec AND ts21 < ts12

SELECT p.*
FROM #po p
GROUP BY id
  
  
  

SELECT mm.*, p.ts1, p.ts2
FROM (
  SELECT id, MIN(ts1) AS minOrd, MAX(ts2) AS maxRec  
  FROM #po
  GROUP BY id) mm
LEFT JOIN #po p ON mm.id = p.id  

SELECT DISTINCT (ts2 - ts1)
FROM (
  SELECT id, MIN(ts1) AS minOrd, MAX(ts2) AS maxRec  
  FROM #po
  GROUP BY id) mm
LEFT JOIN #po p ON mm.id = p.id

SELECT 
  maxRec - minOrd,
--  -
  CASE -- any record that matches maxRec = minOrd - pattern 1
    WHEN ts1 = minOrd and ts2 = maxRec THEN maxRec - minOrd
    WHEN ts2
    WHEN ts1 > minOrd AND ts2 = maxRec THEN ts2 - ts1
    
  END AS PartsHoldHours
FROM (
  SELECT id, MIN(ts1) AS minOrd, MAX(ts2) AS maxRec  
  FROM #po
  GROUP BY id) mm
LEFT JOIN #po p ON mm.id = p.id  
--GROUP BY id 

DO i need to sequentially reduce the base TABLE number of records


 
SELECT *
FROM (
  SELECT mm.*, p.ts1, p.ts2
  FROM (
    SELECT id, MIN(ts1) AS minOrd, MAX(ts2) AS maxRec  
    FROM #po
    GROUP BY id) mm
  LEFT JOIN #po p ON mm.id = p.id) tb1, 
  (  
  SELECT mm.*, p.ts1, p.ts2
  FROM (
    SELECT id, MIN(ts1) AS minOrd, MAX(ts2) AS maxRec  
    FROM #po
    GROUP BY id) mm
  LEFT JOIN #po p ON mm.id = p.id) tb2  
  
/*
     8/1                             8/8
     ord1                            rec1   
      |-------------------------------|  
             8/2             8/6                    8/12      8/14                  
            ord2            rec2                    ord3      rec3     
              |---------------|                      |----------|    
                       8/3                 8/10
                      ord4                 rec4
                       |---------------------|   
     8/1                                                       8/14
      |---------------------------------------------------------|  = 13 days
                                            8/10    8/12
      |--------------------------------------|  +    |----------|  = total time 11 days
*/  
  
SELECT x.*, mm.minOrd, mm.maxRec,
  CASE -- one interval encompasses them all 
    WHEN ((id1 = 1 AND ts11 = minOrd AND ts21 = maxRec) 
      OR (id1 = 2 AND ts11 = minOrd AND ts21 = maxRec) 
      OR (id1 = 3 AND ts11 = minOrd AND ts21 = maxRec) 
      OR (id1 = 4 AND ts11 = minOrd AND ts21 = maxRec)) THEN maxRec - minOrd
  END, 
  CASE 
    WHEN id1 = 1 and id2 <> 1 THEN 
    CASE
      WHEN ts11 = minOrd THEN 
        CASE
          WHEN ts12 = maxRec THEN maxRec - minOrd
          ELSE 999
        END 
    END 
  END,
  CASE
    WHEN ts21 < maxRec AND ts21 < ts12 THEN 111
  END 
FROM (
  SELECT po1.orderid AS id1, po1.ts1 AS ts11, po1.ts2 AS ts21, 
    po2.orderid AS id2, po2.ts1 AS ts12, po2.ts2 AS ts22
  FROM #po AS po1, #po AS po2) x,
  (SELECT MIN(ts1) AS minOrd, MAX(ts2) AS maxRec  
    FROM #po) mm  

SELECT * 
FROM (    
  SELECT *
  FROM #po p,(SELECT MIN(ts1) AS minOrd, MAX(ts2) AS maxREc
    FROM #po) mm) x
WHERE ts1 = minOrd
or ts2 = maxRec         

-- how to find IF there IS a ts2 - ts1 interval that 
SELECT *
FROM po#
SELECT * 
FROM (    
  SELECT *
  FROM #po p,(SELECT MIN(ts1) AS minOrd, MAX(ts2) AS maxREc
    FROM #po) mm) x
WHERE ts1 = minOrd
or ts2 = maxRec    