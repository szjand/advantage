/*
1.  rec1 - ord1 (min/max)
     ord1                          rec1   
      |-----------------------------|
            ord2          rec2      
              |-------------|
              
2.  (rec1 - ord1) + (rec2 - ord2)                  
     ord1             rec1   ord2        rec2
      |----------------|      |------------|                   
      
3.  (rec1 - ord1) + (rec3 = ord3)
     ord1                                rec1   
      |-----------------------------------|                    
            ord2          rec2                ord3      rec3     
              |-------------|                 |----------|        
                      ord4         rec4
                       |-------------|   
                       
4. (rec1 - ord1) + (rec2 - rec1)                          
     ord1                                rec1   
      |-----------------------------------|                    
                     ord1                          rec2
                       |-----------------------------|       
                       
5. -- contiguous intervals   
     ord1                  rec1   
      |---------------------|                    
                           ord2          rec2                
                            |-------------|                      

6. -- 
     ord1                  rec1   
      |---------------------|                    
                           ord2          rec2                
                            |-------------|  
                                               ord3      rec3
                                                 |---------|                    
*/

-- DROP TABLE #orders
-- DELETE FROM #orders
CREATE TABLE #orders (
  id integer, 
  d1 date,
  d2 date);

-- pattern 1 
INSERT INTO #orders values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date)); 
-- pattern 2
INSERT INTO #orders values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date));
INSERT INTO #orders values (1, CAST('08/10/2011' AS sql_date), CAST('08/12/2011' AS sql_date));
-- pattern 3  
INSERT INTO #orders values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date));
INSERT INTO #orders values (1, CAST('08/02/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #orders values (1, CAST('08/12/2011' AS sql_date), CAST('08/14/2011' AS sql_date));
INSERT INTO #orders values (1, CAST('08/03/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
-- pattern 4  
INSERT INTO #orders values (1, CAST('08/01/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #orders values (1, CAST('08/04/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
-- pattern 5  
INSERT INTO #orders values (1, CAST('08/01/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #orders values (1, CAST('08/06/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
-- pattern 6  
INSERT INTO #orders values (1, CAST('08/01/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #orders values (1, CAST('08/06/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
INSERT INTO #orders values (1, CAST('08/12/2011' AS sql_date), CAST('08/14/2011' AS sql_date));


SELECT * FROM #orders;
-- Extract all start/end events:
-- TABLE Events
-- SELECT * FROM #tableEvents
SELECT ID, d1 AS date, 1 AS diff 
INTO #tableEvents 
FROM #Orders
UNION ALL
SELECT ID, d2 AS date, -1 AS diff
FROM #Orders;

-- Calculate the number of concurrent periods
-- Table Stacked (changed field name FROM COUNT to thecount)
-- SELECT * FROM #tableStacked
SELECT A.ID, A.date, COALESCE(SUM(B.diff),0) AS thecount 
INTO #tableStacked
FROM #tableEvents A
LEFT JOIN #tableEvents B ON B.ID = A.ID AND B.date < A.date
GROUP BY A.ID, A.date;

-- Calculate all possible intervals (including the ones we want):
-- Table ProtoPeriods
-- SELECT * FROM #tableProtoPeriods
SELECT A.ID, A.date AS d1, B.date AS d2
INTO #tableProtoPeriods
FROM #tableStacked A
INNER JOIN #tableStacked B ON B.ID = A.ID AND B.date > A.date
INNER JOIN #tableStacked C ON C.ID = A.ID AND C.date > A.date AND C.date <= B.date
WHERE A.thecount = 0
AND B.thecount = 1
GROUP BY A.ID, A.date, B.date
HAVING MIN(C.thecount) > 0;

-- Calculate the maximum periods (minimum starting point, maximum completion point)
-- Table Periods
-- SELECT * FROM #tablePeriods
SELECT
    C.ID,
    MIN(C.d1) AS d1,
    C.d2,
--    DATEDIFF(day, MIN(C.d1), C.d2) AS days
   C.d2 - MIN(C.d1) AS days
INTO #tablePeriods   
FROM (
    SELECT A.ID, A.d1, MAX(A.d2) AS d2
    FROM #tableProtoPeriods A
    GROUP BY A.ID, A.d1) C    
GROUP BY C.ID, C.d2;

-- Finally, sum up all the periods:
SELECT ID, MIN(d1) AS d1, MAX(d2) AS d2, SUM(days) AS days
FROM #tablePeriods A
GROUP BY ID
ORDER BY d1;