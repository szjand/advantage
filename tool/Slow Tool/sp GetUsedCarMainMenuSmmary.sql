                                                                                                                                                                                                                                                                                                                                                                                                                  /*
/*
//crookston
execute procedure GetUsedCarsMainMenuSummaryByLocation('1B6768C6-03B0-4195-BA3B-767C0CAC40A6')
//honda
execute procedure GetUsedCarsMainMenuSummaryByLocation('4CD8E72C-DC39-4544-B303-954C99176671')
//rydells
execute procedure GetUsedCarsMainMenuSummaryByLocation('B6183892-C79D-4489-A58C-B526DF948B06');

10/31/10
  excepted sales buffer vehicles FROM pricing buffer
*/
DECLARE @LocationID String;
@LocationID = 'B6183892-C79D-4489-A58C-B526DF948B06';

SELECT 
  ( -- TotalVehicles
    SELECT COUNT(VehicleInventoryItemID)
    FROM VehicleInventoryItems vii
    WHERE ThruTS IS NULL
    AND LocationID = @LocationID) AS TotalVehicles,        
  ( -- BookingPending
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RawMaterials_BookingPending'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
       AND LocationID = @LocationID)) AS BookingPending,  
  ( -- InTransit
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagPIT_PurchaseInTransit'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS InTransit,              
  ( --TradeBuffer
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagTNA_TradeNotAvailable'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS TradeBuffer,
  ( --InspectionPending
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagIP_InspectionPending'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND (status = 'RMFlagTNA_TradeNotAvailable' OR status = 'RMFlagPIT_PurchaseInTransit') 
      AND ThruTS IS NULL)
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS InspectionPending,
  ( --WalkPending
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagWP_WalkPending'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND status = 'RMFlagIP_InspectionPending'
      AND ThruTS IS NULL)
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS WalkPending,
  ( --RawMaterialsPool (RMP + NOT RB)
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRMP_RawMaterialsPool'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status = 'RMFlagRB_ReconBuffer'
      AND ThruTS IS null)     
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS RawMaterialsBuffer,
  ( --ReconBuffer (RB & No InProcess)
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRB_ReconBuffer'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
--    AND status LIKE '%ReconProcess_InProcess'
      AND status IN ('AppearanceReconProcess_InProcess','BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')      
      AND ThruTS IS NULL)    
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS ReconBuffer,
  ( -- RBMechanical (RB + not InProcess + Disp to Mech)
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRB_ReconBuffer'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
--      AND status LIKE '%ReconProcess_InProcess'
      AND status IN ('AppearanceReconProcess_InProcess','BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')      
      AND ThruTS IS NULL)     
      AND EXISTS (
        SELECT 1 
        FROM VehicleInventoryItemStatuses
        WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
        AND status = 'MechanicalReconDispatched_Dispatched'
        AND ThruTS IS NULL)  
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS RBMechanical,        
  ( --RBBody (RB + not InProcess + Disp to Body)
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRB_ReconBuffer'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
--      AND status LIKE '%ReconProcess_InProcess'
      AND status IN ('AppearanceReconProcess_InProcess','BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')      
      AND ThruTS IS NULL)     
    AND EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND status = 'BodyReconDispatched_Dispatched'
      AND ThruTS IS NULL)
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS RBBody,
  ( -- RBAppearance (RB + not InProcess + Disp to Body)
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagRB_ReconBuffer'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND status = 'AppearanceReconDispatched_Dispatched'
      AND ThruTS IS NULL)
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
--      AND status LIKE '%ReconProcess_InProcess'
      AND status IN ('AppearanceReconProcess_InProcess','BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')      
      AND ThruTS IS NULL)   
    AND EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
      AND status = 'AppearanceReconDispatched_Dispatched'
      AND ThruTS IS NULL)          
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS RBAppearance,
  ( --ReconWIP 
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
--    AND status LIKE '%ReconProcess_InProcess'
    AND status IN ('AppearanceReconProcess_InProcess','BodyReconProcess_InProcess','MechanicalReconProcess_InProcess')
    AND ThruTS IS NULL   
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS ReconWIP,     
  ( -- RWMechanical
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'MechanicalReconProcess_InProcess'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS RWMechanical,   
  ( --RWBody
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'BodyReconProcess_InProcess'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS RWBody,
  ( -- RWAppearance
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'AppearanceReconProcess_InProcess'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS RWAppearance,     
  ( -- PBRetail
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagPB_PricingBuffer'
    AND ThruTS IS NULL
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status = 'RMFlagSB_SalesBuffer'
      AND ThruTS IS NULL)
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS PBRetail,
  ( -- Available
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagAV_Available'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS Available,
  ( -- SalesBuffer
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagSB_SalesBuffer'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS SalesBuffer,
  ( -- WholesaleBuffer
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagWSB_WholesaleBuffer'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS WholesaleBuffer,
  ( -- WSBReconComplete
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE viis.status = 'RMFlagWSB_WholesaleBuffer'
    AND ThruTS IS NULL 
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status = 'RMFlagFLR_FrontLineReady'
      AND ThruTS IS NULL) 
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID) 
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses  
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status = 'RMFlagAtAuction_AtAuction')) AS WSBReconComplete,    
  ( -- WSBReconIncomplete
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE viis.status = 'RMFlagWSB_WholesaleBuffer'
    AND ThruTS IS NULL 
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND status = 'RMFlagFLR_FrontLineReady'
      AND ThruTS IS NULL) 
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS WSBReconIncomplete, 
  ( -- AtAuction
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE viis.status = 'RMFlagAtAuction_AtAuction'
    AND ThruTS IS NULL
    AND EXISTS (  
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS AtAuction,                   
  ( -- DefectReviewNeeded
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagAV_Available'
    AND ThruTS IS NULL
    AND TimeStampDiff(SQL_TSI_DAY, FromTS, Current_TimeStamp()) > 13
    AND NOT EXISTS ( 
      SELECT 1
      FROM VehicleInventoryReviews
      WHERE typ = 'VehicleReview_DefectReview'
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID)
   AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS DefectReviewNeeded,
  ( -- OverTheWallReviewNeeded
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagAV_Available'
    AND ThruTS IS NULL
    AND TimeStampDiff(SQL_TSI_DAY, FromTS, Current_TimeStamp()) > 29
    AND NOT EXISTS ( 
      SELECT 1
      FROM VehicleInventoryReviews
      WHERE typ = 'VehicleReview_OverTheWall'
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID)
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS OverTheWallReviewNeeded,
  ( -- TransitionOutbound
    SELECT 0 FROM system.iota) AS TransitionOutbound,
  ( -- TransitionInbound
    SELECT 0 FROM system.iota) AS TransitionInbound,
  ( -- ConsignedTo
    SELECT Count(VehicleInventoryItemID) 
      FROM VehicleInventoryItems 
      WHERE  LocationID <> OwningLocationID 
        AND LocationID = @LocationID
        AND ThruTS IS null) AS ConsignedTo,
  ( -- ConsignedFrom
        SELECT Count(VehicleInventoryItemID) 
      FROM VehicleInventoryItems 
      WHERE  LocationID <> OwningLocationID 
        AND OwningLocationID = @LocationID
        AND ThruTS IS null) AS ConsignedFrom,
  ( -- BodyWorkNeeded		
      SELECT COUNT(*) 
      FROM VehicleInventoryItemStatuses viis
      WHERE category = 'BodyReconProcess'
      AND status <> 'BodyReconProcess_NoIncompleteReconItems'
      AND ThruTS IS NULL
	  AND EXISTS (
        SELECT 1 
        FROM VehicleInventoryItems
        WHERE ThruTS IS NULL
        AND VehicleInventoryItemID = viis.VehicleInventoryItemID
        AND LocationID = @LocationID)) AS BodyWorkNeeded,
  ( -- MechWorkNeeded		
      SELECT COUNT(*) 
      FROM VehicleInventoryItemStatuses viis
      WHERE category = 'MechanicalReconProcess'
        AND status <> 'MechanicalReconProcess_NoIncompleteReconItems'
        AND ThruTS IS NULL
    	  AND EXISTS (
          SELECT 1 
          FROM VehicleInventoryItems
          WHERE ThruTS IS NULL
          AND VehicleInventoryItemID = viis.VehicleInventoryItemID
          AND LocationID = @LocationID)) AS MechWorkNeeded,
  ( -- AppWorkNeeded		
      SELECT COUNT(*) 
      FROM VehicleInventoryItemStatuses viis
      WHERE category = 'AppearanceReconProcess'
        AND status <> 'AppearanceReconProcess_NoIncompleteReconItems'
        AND ThruTS IS NULL
	      AND EXISTS (
          SELECT 1 
          FROM VehicleInventoryItems
          WHERE ThruTS IS NULL
          AND VehicleInventoryItemID = viis.VehicleInventoryItemID
          AND LocationID = @LocationID)) AS AppWorkNeeded,
  ( -- ConsignmentsOver30		
      SELECT COUNT(*) 
      FROM VehicleInventoryConsignments vic
      WHERE ConsignmentPeriodEndDate < Curdate()
      AND ThruTS IS NULL
  	  AND EXISTS (
          SELECT 1 
          FROM VehicleInventoryItems
          WHERE ThruTS IS NULL
          AND VehicleInventoryItemID = vic.VehicleInventoryItemID
          AND LocationID = @LocationID)) AS ConsignmentsOver30,
   ( -- DeliveriesComingUp
      SELECT Count(*)
      FROM VehicleInventoryItemStatuses viis
      WHERE Status = 'RawMaterials_Sold'
      AND ThruTS IS NULL
      AND EXISTS (
        SELECT 1 FROM VehicleInventoryItems vii 
        WHERE vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
        AND vii.LocationID = @LocationID
        AND vii.ThruTS IS null)) AS DeliveriesComingUp,
  ( -- SalesBufferPastDelDate
      SELECT COUNT(*)
      FROM VehicleInventoryItemSalePending sp
      WHERE sp.ThruTS IS NULL
      AND sp.ExpectedDeliveryDate < Current_Date()
      AND EXISTS (
        SELECT 1 FROM VehicleInventoryItems vii 
        WHERE vii.VehicleInventoryItemID = sp.VehicleInventoryItemID
        AND vii.LocationID = @LocationID
        AND vii.ThruTS IS null)) AS SalesBufferPastDelDate,
  ( -- DCUPastDelDate
      SELECT Count(*)
      FROM VehicleInventoryItemStatuses viis
      INNER JOIN VehicleSales vs ON vs.VehicleInventoryItemID = viis.VehicleInventoryItemID
      WHERE viis.Status = 'RawMaterials_Sold'
      AND viis.ThruTS IS NULL
      AND cast(vs.EstimatedDeliveryDate AS SQL_DATE) < Current_Date()
      AND EXISTS (
        SELECT 1 FROM VehicleInventoryItems vii 
        WHERE vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
        AND vii.LocationID = @LocationID
        AND vii.ThruTS IS null)) AS DCUPastDelDate,      
  ( --Pulled
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagPulled_Pulled'
    AND ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = viis.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS Pulled,  
  ( --VehiclesToMove
    SELECT COUNT(*)
    FROM VehiclesToMove vtm
    WHERE ThruTS IS NULL
    AND EXISTS (
      SELECT 1 
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL
      AND VehicleInventoryItemID = vtm.VehicleInventoryItemID
      AND LocationID = @LocationID)) AS VehiclesToMove,
  ( --WTF
    SELECT COUNT(status)
    FROM VehicleInventoryItemStatuses viis
    WHERE status = 'RMFlagWTF_WTF'
      AND ThruTS IS NULL
      AND EXISTS (
        SELECT 1 
        FROM VehicleInventoryItems
        WHERE ThruTS IS NULL
          AND VehicleInventoryItemID = viis.VehicleInventoryItemID
          AND LocationID = @LocationID)) AS WTF,
  ( -- No Pics
    SELECT COUNT(*)
    FROM VehicleInventoryItems viix
    LEFT JOIN (
      SELECT count(viip.VehicleInventoryItemID) AS CountOfPic, viip.VehicleInventoryItemID
      FROM viipictures viip
      GROUP BY viip.VehicleInventoryItemID) t ON viix.VehicleInventoryItemID = t.VehicleInventoryItemID  
    LEFT JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID 
    WHERE viix.ThruTS IS NULL 
    AND NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
      AND ThruTS IS NULL
      AND status IN (
        'RMFlagWSB_WholesaleBuffer',
    	'RMFlagPIT_PurchaseInTransit',
    	'RMFlagTNA_TradeNotAvailable',
    	'RMFlagIP_InspectionPending',
    	'RMFlagWP_WalkPending'))
    AND coalesce(t.CountOfPic, 0) = 0
    AND vs.VehicleInventoryItemID IS NULL
    AND LocationID = @LocationID) AS NoPics	                             		                             
FROM System.Iota;

   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
