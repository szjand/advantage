/*
what does it DO IF ReconAuthorizationID IS NULL
  what closes a ReconAuthorizations 
  looks LIKE it IS an issue - after using the sp for RemoveFromSalesBuffer generated a status of ReconBuffer
    because @MBAReconStatus IS NULL
    AND that IS the value that's uses subsequently AS mbaStatus (which IS another rant about distinguishing BETWEEN
    Recon AND VIIStatuses
  @AppReconStatus - changed to MAX(coalesce(status),1) instead of MAX(CASE ... END) , need to MAX the integer, the the string representation
  Added test for NULL ReconAuthorizationID, IF NULL ALL statuses are NoIncompleteReconItems
*/
DECLARE @VehicleInventoryItemID string;
DECLARE @MechReconStatus string;
DECLARE @BodyReconStatus string;
DECLARE @AppReconStatus string;
DECLARE @NowTS timestamp;
DECLARE @ReconAuthorizationID string;
DECLARE @MechVIIStatus string;
DECLARE @BodyVIIStatus string;
DECLARE @AppVIIStatus string;
DECLARE @UserID string;
-- UpdVIIS has @VehicleInventoryItemID, @ReconAuthorizationID, @UserID, @NowTS 
@VehicleInventoryItemID = '39bc9cab-36ea-461d-8ae3-fa5936a541cc';  -- NULL ReconAuthorizationID 
-- @VehicleInventoryItemID = '92826c6d-e1f7-4099-8a6d-ed07e2ad677c';
@NowTS = (SELECT now() FROM system.iota);
@ReconAuthorizationID = (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
    AND ThruTS IS NULL);
-- need the variable OUTPUT to feed the rest of updviis
@UserID = (
  SELECT partyid 
  FROM users
  WHERE username = 'jon');
IF @ReconAuthorizationID IS NULL THEN
  @MechReconStatus = 'MechanicalReconProcess_NoIncompleteReconItems';  
  @BodyReconStatus = 'BodyReconProcess_NoIncompleteReconItems';
  @AppReconStatus = 'AppearanceReconProcess_NoIncompleteReconItems';
ELSE 
  @MechReconStatus = (
    SELECT 
      case coalesce (y.status, 1) 
        WHEN 3 THEN 'MechanicalReconProcess_InProcess'
        WHEN 2 THEN 'MechanicalReconProcess_NotStarted'
        ELSE 'MechanicalReconProcess_NoIncompleteReconItems'
      END
    FROM (
      SELECT *
        FROM (
          SELECT DISTINCT category
          FROM typcategories
          WHERE category = 'MechanicalReconItem') a, ReconAuthorizations b
          WHERE b.VehicleInventoryItemID = @VehicleInventoryItemID
            AND b.thruts IS NULL) x
      LEFT JOIN (
        SELECT a.VehicleInventoryItemID, d.category,
          MAX(
            case 
              when b.status = 'AuthorizedReconItem_InProcess' THEN 3
              WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 2
              WHEN b.status = 'AuthorizedReconItem_Complete' THEN 1
              ELSE 1
            END) AS status
        FROM ReconAuthorizations a
        LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
        LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
        LEFT JOIN typcategories d ON c.typ = d.typ
        WHERE a.VehicleInventoryItemID = @VehicleInventoryItemID
          AND a.thruts IS NULL 
        GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category);  
  @BodyReconStatus = (
    SELECT 
      case coalesce (y.status, 1) 
        WHEN 3 THEN 'BodyReconProcess_InProcess'
        WHEN 2 THEN 'BodyReconProcess_NotStarted'
        ELSE 'BodyReconProcess_NoIncompleteReconItems'
      END
    FROM (
      SELECT *
        FROM ( -- 1 row per category/ReconAuthorizationID
          SELECT DISTINCT category
          FROM typcategories
          WHERE category = 'BodyReconItem') a, ReconAuthorizations b
          WHERE b.VehicleInventoryItemID = @VehicleInventoryItemID
            AND b.thruts IS NULL) x
      LEFT JOIN ( -- 1 row per VehicleInventoryItemID / vri.category with a numeric status representation
        SELECT a.VehicleInventoryItemID, d.category,
          MAX(
            case 
              when b.status = 'AuthorizedReconItem_InProcess' THEN 3
              WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 2
              WHEN b.status = 'AuthorizedReconItem_Complete' THEN 1
              ELSE 1
            END) AS status
        FROM ReconAuthorizations a
        LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
        LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
        LEFT JOIN typcategories d ON c.typ = d.typ
        WHERE a.VehicleInventoryItemID = @VehicleInventoryItemID
          AND a.thruts IS NULL 
        GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category);    
  @AppReconStatus = (
    SELECT 
        CASE MAX(coalesce (y.status, 1)) 
          WHEN 3 THEN 'AppearanceReconProcess_InProcess'
          WHEN 2 THEN 'AppearanceReconProcess_NotStarted'
          ELSE 'AppearanceReconProcess_NoIncompleteReconItems'
        END
    FROM (
      SELECT *
        FROM (
          SELECT DISTINCT category
          FROM typcategories
          WHERE category IN ('AppearanceReconItem','PartyCollection')) a, ReconAuthorizations b
          WHERE b.VehicleInventoryItemID = @VehicleInventoryItemID
            AND b.thruts IS NULL) x
      LEFT JOIN (
        SELECT a.VehicleInventoryItemID, d.category,
          MAX(
            case 
              when b.status = 'AuthorizedReconItem_InProcess' THEN 3
              WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 2
              WHEN b.status = 'AuthorizedReconItem_Complete' THEN 1
              ELSE 1
            END) AS status
        FROM ReconAuthorizations a
        LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
        LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
        LEFT JOIN typcategories d ON c.typ = d.typ
        WHERE a.VehicleInventoryItemID = @VehicleInventoryItemID
          AND a.thruts IS NULL 
        GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category
      WHERE status IS NOT NULL);  -- 2 categories, IF there IS only AuthorizedReconItems for one, status will be null   
END IF;      
      
      
-- put a test around this to ensure 1 row per viid/category
-- the use of Vstatuses removes the need for this query, may want to include it AS a test
SELECT *
  FROM (
  SELECT VehicleInventoryItemID, category, status, @MechReconStatus AS rStatus
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND ThruTS IS NULL
  AND category = 'MechanicalReconProcess'
  UNION
  SELECT VehicleInventoryItemID, category, status, @BodyReconStatus 
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND ThruTS IS NULL
  AND category = 'BodyReconProcess'
  UNION
  SELECT VehicleInventoryItemID, category, status, @AppReconStatus 
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND ThruTS IS NULL
  AND category = 'AppearanceReconProcess') z
WHERE status <> rStatus;

--  @MechanicalStatus = 'MechanicalReconProcess_NoIncompleteReconItems';  
--  @BodyStatus = 'BodyReconProcess_NoIncompleteReconItems';
--  @AppearanceStatus = 'AppearanceReconProcess_NoIncompleteReconItems'; 

@MechVIIStatus = coalesce((
  SELECT status 
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND ThruTS IS NULL
  AND category = 'MechanicalReconProcess'), 'MechanicalReconProcess_NoIncompleteReconItems');

@BodyVIIStatus = coalesce((
  SELECT status 
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND ThruTS IS NULL
  AND category = 'BodyReconProcess'), 'BodyReconProcess_NoIncompleteReconItems');
  
@AppVIIStatus = coalesce((
  SELECT status
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND ThruTS IS NULL
  AND category = 'AppearanceReconProcess'),'AppearanceReconProcess_NoIncompleteReconItems');


SELECT @AppVIIStatus,@AppReconStatus,@MechVIIStatus,@MechReconStatus,@BodyVIIStatus,@BodyReconStatus FROM system.iota;


IF @AppVIIStatus <> @AppReconStatus THEN
-- fuck, i don't even need this, i know they are different, AND i know what they should be
----  IF @AppReconStatus = 'AppearanceReconProcess_NoIncompleteReconItems' THEN
-- hmmm doesn't matter what the old one was, just CLOSE out the OPEN one - we already know that rStatus AND viiStatus are different
-- AND that the new status should be rStatus
  UPDATE VehicleInventoryItemStatuses 
    SET ThruTS = @NowTS
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
      AND category = 'AppearanceReconProcess'
      AND ThruTS IS NULL;  
 INSERT INTO VehicleInventoryItemStatuses (VehicleInventoryItemID, Status, Category, FromTS, BasisTable, TableKey, UserID) 
    values(@VehicleInventoryItemID, @AppReconStatus, 'AppearanceReconProcess', 
      @NowTS, 'ReconAuthorizations', @ReconAuthorizationID, @UserID);  
END IF;  
IF @MechVIIStatus <> @MechReconStatus THEN  
  UPDATE VehicleInventoryItemStatuses 
    SET ThruTS = @NowTS
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
      AND category = 'MechanicalReconProcess'
      AND ThruTS IS NULL;  
  INSERT INTO VehicleInventoryItemStatuses (VehicleInventoryItemID, Status, Category, FromTS, BasisTable, TableKey, UserID) 
    values(@VehicleInventoryItemID, @AppReconStatus, 'MechanicalReconProcess', 
      @NowTS, 'ReconAuthorizations', @ReconAuthorizationID, @UserID); 
END IF;   
IF @BodyVIIStatus <> @BodyReconStatus THEN  
  UPDATE VehicleInventoryItemStatuses 
    SET ThruTS = @NowTS
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
      AND category = 'BodyReconProcess'
      AND ThruTS IS NULL;  
  INSERT INTO VehicleInventoryItemStatuses (VehicleInventoryItemID, Status, Category, FromTS, BasisTable, TableKey, UserID) 
    values(@VehicleInventoryItemID, @AppReconStatus, 'BodyReconProcess', 
      @NowTS, 'ReconAuthorizations', @ReconAuthorizationID, @UserID); 
END IF;   
/**/