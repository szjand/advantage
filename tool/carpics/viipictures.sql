SELECT b.stocknumber, CAST(fromts AS sql_date) AS from_date, CAST(thruts AS sql_Date)  AS thru_date
FROM viipictures a
JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 


SELECT CAST(a.uploadts AS sql_date), b.stocknumber
FROM viipictureuploads a
JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 


SELECT * FROM VehicleInventoryItems WHERE VehicleInventoryItemID = '3a28c44a-4b8d-42db-912e-719a0ec79308'


SELECT top 50 * 
FROM viipictureuploads a
JOIN viipictures b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
ORDER BY a.uploadts desc


SELECT *
SELECT MIN(year(uploadts))
-- select COUNT(*)
FROM viipictureuploads a
JOIN viipictures b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
WHERE year(uploadts) = 2011

SELECT * 
FROM viipictures a
WHERE EXISTS (
  SELECT 1
	FROM viipictureuploads
	WHERE year(uploadts) = 2018
	  AND VehicleInventoryItemID = a.VehicleInventoryItemID)
		
		
CREATE TABLE VIIPictures_2018 ( 
      VehicleInventoryItemID Char( 38 ),
      SequenceNo Integer,
      Description Char( 50 ),
      Picture Blob) IN DATABASE;
			
INSERT INTO VIIPictures_2018  --49540
SELECT VehicleInventoryItemID, sequenceno, description, picture
FROM viipictures a
WHERE EXISTS (
  SELECT 1
	FROM viipictureuploads
	WHERE year(uploadts) = 2018
	  AND VehicleInventoryItemID = a.VehicleInventoryItemID)	
		
SELECT COUNT(*) 
FROM viipictures a
WHERE VehicleInventoryItemID IN (
  SELECT DISTINCT VehicleInventoryItemID
	FROM VIIPictures_2018)		


CREATE TABLE VIIPictures_2019 ( 
      VehicleInventoryItemID Char( 38 ),
      SequenceNo Integer,
      Description Char( 50 ),
      Picture Blob) IN DATABASE;
			
INSERT INTO VIIPictures_2019-- 60152
SELECT VehicleInventoryItemID, sequenceno, description, picture
FROM viipictures a
WHERE EXISTS (
  SELECT 1
	FROM viipictureuploads
	WHERE year(uploadts) = 2019
	  AND VehicleInventoryItemID = a.VehicleInventoryItemID)	
		

CREATE TABLE VIIPictures_2020 ( 
      VehicleInventoryItemID Char( 38 ),
      SequenceNo Integer,
      Description Char( 50 ),
      Picture Blob) IN DATABASE;
			
INSERT INTO VIIPictures_2020	-- 59062	
SELECT VehicleInventoryItemID, sequenceno, description, picture
FROM viipictures a
WHERE EXISTS (
  SELECT 1
	FROM viipictureuploads
	WHERE year(uploadts) = 2020
	  AND VehicleInventoryItemID = a.VehicleInventoryItemID)			
		

CREATE TABLE VIIPictures_2021 ( 
      VehicleInventoryItemID Char( 38 ),
      SequenceNo Integer,
      Description Char( 50 ),
      Picture Blob) IN DATABASE;
			
INSERT INTO VIIPictures_2021	-- 58545	
SELECT VehicleInventoryItemID, sequenceno, description, picture
FROM viipictures a
WHERE EXISTS (
  SELECT 1
	FROM viipictureuploads
	WHERE year(uploadts) = 2021
	  AND VehicleInventoryItemID = a.VehicleInventoryItemID)
		
		
CREATE TABLE VIIPictures_2022 ( 
      VehicleInventoryItemID Char( 38 ),
      SequenceNo Integer,
      Description Char( 50 ),
      Picture Blob) IN DATABASE;
			
INSERT INTO VIIPictures_2022	-- 59269	
SELECT VehicleInventoryItemID, sequenceno, description, picture
FROM viipictures a
WHERE EXISTS (
  SELECT 1
	FROM viipictureuploads
	WHERE year(uploadts) = 2022
	  AND VehicleInventoryItemID = a.VehicleInventoryItemID)
		
CREATE TABLE VIIPictures_2023 ( 
      VehicleInventoryItemID Char( 38 ),
      SequenceNo Integer,
      Description Char( 50 ),
      Picture Blob) IN DATABASE;
			
INSERT INTO VIIPictures_2023	-- 8506	
SELECT VehicleInventoryItemID, sequenceno, description, picture
FROM viipictures a
WHERE EXISTS (
  SELECT 1
	FROM viipictureuploads
	WHERE year(uploadts) = 2023
	  AND VehicleInventoryItemID = a.VehicleInventoryItemID)		
		

	
SELECT * 
FROM viipictures
WHERE VehicleInventoryItemID = 'd1e326f2-0ac6-4c57-be2e-196ccf57271d'	


SELECT * FROM VIIPictures_2018	

SELECT * FROM VehicleInventoryItems where VehicleInventoryItemID = '97325898-df9a-465b-8195-0fc4cb049729'

EXECUTE PROCEDURE sp_reindex('viipictures.adt', 512);

EXECUTE PROCEDURE sp_RenameDDObject( 'viipictures', 'viipictures_orig', 1, 0);

CREATE TABLE VIIPictures ( 
      VehicleInventoryItemID Char( 38 ),
      SequenceNo Integer,
      Description Char( 50 ),
      Picture Blob) IN DATABASE;
			
EXECUTE PROCEDURE sp_CreateIndex90( 
   'VIIPictures',
   'VIIPictures.adi',
   'VII_SEQ_NDX',
   'VehicleInventoryItemID;SequenceNo',
   '',
   2,
   512,
   '' ); 
					
INSERT INTO VIIPictures
SELECT * FROM VIIPictures_2023;

INSERT INTO VIIPictures
SELECT * FROM VIIPictures_2022;


SELECT * 
FROM viipictureuploads
ORDER BY uploadts DESC

-- successfully uploaded full SET for G45309B
SELECT * 
delete
FROM viipictureuploads
WHERE VehicleInventoryItemID = '602bb086-a0b0-4fba-8195-451c993e62bd'

SELECT * 
delete
FROM viipictures
WHERE VehicleInventoryItemID = '602bb086-a0b0-4fba-8195-451c993e62bd'


