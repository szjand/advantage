SELECT VehicleInventoryItemID, COUNT(*)
FROM ReconAuthorizations
GROUP BY VehicleInventoryItemID 
ORDER BY COUNT(*) desc

SELECT *
FROM ReconAuthorizations 
WHERE VehicleInventoryItemID = 'ebb90572-3131-4ebb-b423-7196adc2f0c2'

SELECT left(rax.BasisTable, 15) AS BasisTable, rax.ReconAuthorizationID, rax.fromts, rax.ThruTS, arix.VehicleReconItemID, 
  arix.status, arix.startts, arix.completets, LEFT(vrix.description, 25)
FROM ReconAuthorizations rax
LEFT JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID
LEFT JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
--WHERE rax.VehicleInventoryItemID = 'ebb90572-3131-4ebb-b423-7196adc2f0c2' -- turns out this was bought ws FROM honda
WHERE rax.VehicleInventoryItemID = '0f5afc5e-0102-4edb-937f-750d6c4d8b87' -- this IS the honda vehicle
ORDER BY rax.FromTS, arix.VehicleReconItemID   

-- so what i'm really looking for IS, under which ReconAuthorizations was an AuthorizedReconItems completed


