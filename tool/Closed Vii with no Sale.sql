SELECT COUNT(*)
FROM VehicleInventoryItems v
WHERE ThruTS IS NOT NULL
AND stocknumber IS NOT NULL 
AND NOT EXISTS (
  SELECT 1
  FROM vehiclesales
  WHERE VehicleInventoryItemID = v.VehicleInventoryItemID)
  
  
SELECT *
FROM VehicleInventoryItems
WHERE VehicleInventoryItemID = '2e482f44-adaf-4df4-a483-1c287379b82b'  


DECLARE @Viix CURSOR AS
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems v
  WHERE ThruTS IS NOT NULL
  AND stocknumber IS NOT NULL 
  AND NOT EXISTS (
    SELECT 1
    FROM vehiclesales
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID);
DECLARE @VehicleInventoryItemID string;
OPEN @Viix;
TRY
  WHILE FETCH @viix DO
    @VehicleInventoryItemID = @viix.VehicleInventoryItemID;
    EXECUTE PROCEDURE DeleteVehicleInventoryItem(@VehicleInventoryItemID);
  END WHILE;
FINALLY
  CLOSE @Viix;
END TRY;    