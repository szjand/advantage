
SELECT year(inspectiondate), month(inspectiondate) , COUNT(*)
FROM vehicleinspections
WHERE year(inspectiondate) >= 2013
GROUP BY year(inspectiondate), month(inspectiondate)


SELECT month(vehiclewalkdate), COUNT(*)
FROM vehiclewalks
WHERE year(vehiclewalkdate) >= 2013
GROUP BY month(vehiclewalkdate)

SELECT month(tradeevaluationdate), COUNT(*)
FROM tradeevaluations
WHERE year(tradeevaluationdate) >= 2013
GROUP BY month(tradeevaluationdate)


SELECT distinct theyear, monthofyear, coalesce(d.eval, 0) AS eval, coalesce(b.insp, 0) AS insp, coalesce(c.walk, 0) AS walk
FROM dds.day a
LEFT JOIN (
  SELECT year(inspectiondate) AS iYear, month(inspectiondate) AS iMonth, COUNT(*)AS insp
  FROM vehicleinspections
  WHERE year(inspectiondate) >= 2013
  GROUP BY year(inspectiondate), month(inspectiondate)) b on a.theyear = b.iyear AND a.monthofyear = b.imonth
LEFT JOIN (  
  SELECT year(vehiclewalkdate) as wyear, month(vehiclewalkdate) AS wmonth, COUNT(*) AS walk
  FROM vehiclewalks
  WHERE year(vehiclewalkdate) >= 2013
  GROUP BY year(vehiclewalkdate), month(vehiclewalkdate)) c on a.theyear = c.wyear AND a.monthofyear = c.wmonth
LEFT JOIN (
  SELECT year(tradeevaluationdate) AS eyear, month(tradeevaluationdate) AS emonth, COUNT(*) AS eval
  FROM tradeevaluations
  WHERE year(tradeevaluationdate) >= 2013
  GROUP BY year(tradeevaluationdate), month(tradeevaluationdate)) d on a.theyear = d.eyear AND a.monthofyear = d.emonth  
WHERE a.yearmonth between 201306 AND 201401