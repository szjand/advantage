SELECT status, COUNT(*)
FROM tradeevaluations
WHERE marketid = (select marketid FROM markets WHERE market = 'SF-Sioux Falls') 
GROUP BY status

SELECT status, tradeevaluationstatus, COUNT(*)
FROM tradeevaluations
WHERE marketid = (select marketid FROM markets WHERE market = 'SF-Sioux Falls') 
GROUP BY status, tradeevaluationstatus

CREATE TABLE #jon (
  stocknumber CIChar(20),
  salesstatus CIChar(100));
  
INSERT INTO #jon
SELECT stocknumber, salesstatus
FROM usedcars  
WHERE market = 'SF-Sioux Falls';

-- uc status ON walk pending & insp pending evals
SELECT u.salesstatus, t.status, COUNT(*)
-- SELECT COUNT(*) -- sold: 146, ws: 67
FROM tradeevaluations t
LEFT JOIN #jon u ON t.stocknumber = u.stocknumber 
WHERE marketid = (select marketid FROM markets WHERE market = 'SF-Sioux Falls')
AND status in ('Walk Pending', 'Inspection Pending')
GROUP BY u.salesstatus, t.status


-- whack sold pending walks
UPDATE tradeevaluations
SET status = 'X-Walk Pending'
WHERE marketid = (select marketid FROM markets WHERE market = 'SF-Sioux Falls')
AND status = 'Walk Pending'
AND stocknumber IN (
  SELECT stocknumber
  FROM #jon
  WHERE salesstatus IN ('Sold','Wholesaled'));
-- whack sold pending inspections  
UPDATE tradeevaluations
SET status = 'X-Inspection Pending'
WHERE marketid = (select marketid FROM markets WHERE market = 'SF-Sioux Falls')
AND status = 'Inspection Pending'
AND stocknumber IN (
  SELECT stocknumber
  FROM #jon
  WHERE salesstatus IN ('Sold','Wholesaled'));  
  
-- whack available pending walks
UPDATE tradeevaluations
SET status = 'X-Walk Pending'
WHERE marketid = (select marketid FROM markets WHERE market = 'SF-Sioux Falls')
AND status = 'Walk Pending'
AND stocknumber IN (
  SELECT stocknumber
  FROM #jon
  WHERE salesstatus = 'Available');
-- whack available pending inspections  
UPDATE tradeevaluations
SET status = 'X-Inspection Pending'
WHERE marketid = (select marketid FROM markets WHERE market = 'SF-Sioux Falls')
AND status = 'Inspection Pending'
AND stocknumber IN (
  SELECT stocknumber
  FROM #jon
  WHERE salesstatus = 'Available');