DELETE FROM minotavailable

SELECT stocknumber, vin, dateacquired, salesstatus
FROM usedcars
WHERE market = 'MT-Minot'
AND vin IN (
  SELECT vin
  FROM usedcars
  WHERE market = 'MT-Minot'
  AND dateacquired > '12/31/2010'
  GROUP BY vin
  HAVING COUNT(*) > 1)
AND dateacquired > '12/31/2010' 
ORDER BY vin


SELECT u.stocknumber, u.vin, u.salesstatus, m.stocknumber, m.vin, m.salesstatus  
FROM (
  select *
  from usedcars
  WHERE market = 'MT-Minot'
  AND dateacquired > '12/31/2010') u
LEFT JOIN MinotAvailable m ON u.stocknumber = m.stocknumber 
WHERE u.salesstatus <> m.salesstatus

SELECT stocknumber, vin, dateacquired, salesstatus
FROM Usedcars
WHERE vin IN (
  SELECT u.vin  
  FROM (
    select *
    from usedcars
    WHERE market = 'MT-Minot'
    AND dateacquired > '12/31/2010') u
LEFT JOIN MinotAvailable m ON u.stocknumber = m.stocknumber 
WHERE u.salesstatus <> m.salesstatus)
ORDER BY vin


SELECT stocknumber
FROM  usedcars
  WHERE market = 'MT-Minot'
  AND dateacquired > '12/31/2010'
GROUP BY stocknumber 
HAVING COUNT(*) > 1  

-- populate MinotAvailable -- data FROM aqt/notes (MTInventory)


SELECT stocknumber, market
FROM  usedcars
WHERE market = 'MT-Minot'
GROUP BY stocknumber, market
HAVING COUNT(*) > 1  

SELECT stocknumber, vin, salesstatus
FROM MinotAvailable m
WHERE salesstatus NOT IN ('Sold','Wholesaled')
AND EXISTS (
  SELECT 1
  FROM usedcars
  WHERE stocknumber = m.stocknumber
  AND salesstatus <> m.salesstatus)
  
  
SELECT m.stocknumber, m.vin, m.dateacquired, m.salesstatus, u.stocknumber, u.vin, u.dateacquired, u.salesstatus 
FROM MinotAvailable m
LEFT JOIN usedcars u ON m.stocknumber = u.stocknumber
  AND m.salesstatus <> u.salesstatus
WHERE m.SalesStatus NOT IN  ('Sold','Wholesaled')
AND year(m.dateacquired) = 2011
AND year(u.dateacquired) = 2011


SELECT stocknumber, vin, salesstatus
FROM usedcars
WHERE vin = '3B7HF13ZXVG758898'
  