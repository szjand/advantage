
SELECT category, subcategory, min(NotesTS), max(NotesTS), max(viix.stocknumber), COUNT(*)
FROM VehicleInventoryItemNotes vinx
LEFT JOIN VehicleInventoryItems viix ON vinx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
GROUP BY category, subcategory
ORDER BY category, subcategory


SELECT category, COUNT(*)
FROM VehicleInventoryItemNotes viinx
WHERE NOT EXISTS (
  SELECT 1
  FROM categories
  WHERE category = viinx.category)
GROUP BY category  


SELECT * FROM VehicleInventoryItemNotes WHERE category = 'TradeBuffer' ORDER BY notests desc