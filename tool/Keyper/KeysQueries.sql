/*
SELECT number, cabinet, stocknumber, status, CAST('12/28/1800' + issue_Date AS sql_date) AS IssueDate, IssuedToName
-- SELECT COUNT(*)
FROM keys k
WHERE EXISTS(
  SELECT 1
  FROM keys
  WHERE number = k.number
  AND stocknumber <> k.stocknumber)
ORDER BY number, cabinet
*/

/*
SELECT *
-- SELECT COUNT(*) --87, 92
FROM (
  SELECT DISTINCT number
  FROM keys) k
WHERE NOT EXISTS(
  SELECT 1
  FROM keys
  WHERE cabinet = 'RY1Mech'
  AND number = k.number)
OR NOT EXISTS(  
  SELECT 1
  FROM keys
  WHERE cabinet = 'RY1Body'
  AND number = k.number)
*/  
  
/******************************************************************************/
-- fobs that don't exist IN ALL cabinets
SELECT k.number AS FOB, ry1b.stocknumber AS BodyShop, ry1d.stocknumber AS Detail,
  ry1m.stocknumber AS Recon, ry1n.stocknumber AS Mech,
  ry1u.stocknumber AS Used, ry2sa.stocknumber AS "Honda Sales",
  ry2se.stocknumber AS "Honda Service",
  ry3.stocknumber AS Crookston
-- SELECT COUNT(*) 
FROM (
  SELECT DISTINCT number
  FROM keys) k
LEFT JOIN (
  SELECT number, stocknumber
  FROM keys
  WHERE cabinet = 'ry1body') ry1b ON k.number = ry1b.number
LEFT JOIN (
  SELECT number, stocknumber
  FROM keys
  WHERE cabinet = 'ry1detail') ry1d ON k.number = ry1d.number  
LEFT JOIN (
  SELECT number, stocknumber
  FROM keys
  WHERE cabinet = 'ry1mech') ry1m ON k.number = ry1m.number 
LEFT JOIN (
  SELECT number, stocknumber
  FROM keys
  WHERE cabinet = 'ry1new') ry1n ON k.number = ry1n.number   
LEFT JOIN (
  SELECT number, stocknumber
  FROM keys
  WHERE cabinet = 'ry1used') ry1u ON k.number = ry1u.number   
LEFT JOIN (
  SELECT number, stocknumber
  FROM keys
  WHERE cabinet = 'ry2sales') ry2sa ON k.number = ry2sa.number 
LEFT JOIN (
  SELECT number, stocknumber
  FROM keys
  WHERE cabinet = 'ry2service') ry2se ON k.number = ry2se.number  
LEFT JOIN (
  SELECT number, stocknumber
  FROM keys
  WHERE cabinet = 'ry3') ry3 ON k.number = ry3.number      
WHERE ry1b.stocknumber IS NULL
  OR ry1d.stocknumber IS NULL 
  OR ry1m.stocknumber IS NULL 
  OR ry1n.stocknumber IS NULL 
  OR ry1u.stocknumber IS NULL 
  OR ry2sa.stocknumber IS NULL 
  OR ry2se.stocknumber IS NULL 
  OR ry3.stocknumber IS NULL 
/******************************************************************************/  

/******************************************************************************/
-- multiple stocknumber for the same fob
SELECT distinct k.number AS FOB,
  (SELECT stocknumber FROM keys WHERE number = k.number AND cabinet = 'ry1body') AS Body,
  (SELECT stocknumber FROM keys WHERE number = k.number AND cabinet = 'ry1detail') AS Detail,
  (SELECT stocknumber FROM keys WHERE number = k.number AND cabinet = 'ry1mech') AS Mech,
  (SELECT stocknumber FROM keys WHERE number = k.number AND cabinet = 'ry1new') AS New,
  (SELECT stocknumber FROM keys WHERE number = k.number AND cabinet = 'ry1used') AS Used,
  (SELECT stocknumber FROM keys WHERE number = k.number AND cabinet = 'ry2sales') AS "Honda Sales",
  (SELECT stocknumber FROM keys WHERE number = k.number AND cabinet = 'ry2service') AS "Honda Service",
  (SELECT stocknumber FROM keys WHERE number = k.number AND cabinet = 'ry3') AS Crookston
-- SELECT COUNT(DISTINCT number)
FROM (
  select number
  from keys 
  GROUP BY number, stocknumber
  HAVING COUNT(*) < 8) k
/******************************************************************************/  

/******************************************************************************/ 
-- multiple fobs for the same stocknumber

SELECT distinct trim(k.stocknumber) AS stocknumber,
  (SELECT number FROM keys WHERE stocknumber = k.stocknumber AND cabinet = 'ry1body') AS Body,
  (SELECT number FROM keys WHERE stocknumber = k.stocknumber AND cabinet = 'ry1detail') AS Detail,
  (SELECT number FROM keys WHERE stocknumber = k.stocknumber AND cabinet = 'ry1mech') AS Mech,
  (SELECT number FROM keys WHERE stocknumber = k.stocknumber AND cabinet = 'ry1new') AS New,
  (SELECT number FROM keys WHERE stocknumber = k.stocknumber AND cabinet = 'ry1used') AS Used,
  (SELECT number FROM keys WHERE stocknumber = k.stocknumber AND cabinet = 'ry2sales') AS "Honda Sales",
  (SELECT number FROM keys WHERE stocknumber = k.stocknumber AND cabinet = 'ry2service') AS "Honda Service",
  (SELECT number FROM keys WHERE stocknumber = k.stocknumber AND cabinet = 'ry3') AS Crookston
-- SELECT COUNT(DISTINCT stocknumber)
FROM (
  select stocknumber
  from keys 
  GROUP BY number, stocknumber
  HAVING COUNT(*) < 8) k
ORDER BY trim(k.stocknumber)  
/******************************************************************************/ 
-- fobs physically located IN multiple cabinets (physical location IS the dealername field)
SELECT *
FROM keys 
ORDER BY stocknumber, dealername

SELECT trim(stocknumber), dealername, COUNT(*)
FROM keys
GROUP BY trim(stocknumber), dealername
HAVING COUNT(*) > 1


SELECT stocknumber, COUNT(*)
FROM (
SELECT DISTINCT trim(stocknumber) AS stocknumber, dealername
FROM keys) k
GROUP BY stocknumber
HAVING COUNT(*) > 1

SELECT DISTINCT trim(stocknumber) AS stocknumber FROM keys


SELECT number, COUNT(*)
FROM (
SELECT distinct number, dealername
FROM keys
WHERE status = 'In') k
GROUP BY number
HAVING COUNT(*) > 1


SELECT DISTINCT number, stocknumber, dealername, status, CAST('12/28/1800' + issue_Date AS sql_date) 
-- SELECT COUNT(*)
FROM keys
WHERE number IN (
  SELECT number
  FROM (
    SELECT distinct number, dealername
    FROM keys
    WHERE status = 'In') k
  GROUP BY number
  HAVING COUNT(*) > 1)
AND status = 'In'  
ORDER BY number, dealername  