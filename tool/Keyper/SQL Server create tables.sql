CREATE TABLE [asset](
	[asset_id] [int] IDENTITY(1,1) NOT NULL,
	[asset_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[fob_number] [int] NOT NULL CONSTRAINT [DF_asset_fob_number]  DEFAULT ((0)),
	[asset_status] [varchar](15) NOT NULL CONSTRAINT [DF_asset_asset_status]  DEFAULT ('none'),
	[asset_type] [varchar](15) NOT NULL CONSTRAINT [DF_asset_asset_type]  DEFAULT ('none'),
	[asset_removal_type] [varchar](15) NOT NULL CONSTRAINT [DF_asset_asset_removal_type]  DEFAULT ('none'),
	[cabinet_id] [int] NULL,
	[user_id] [int] NULL,
	[issue_reason_id] [int] NULL,
	[out_duration] [float] NOT NULL CONSTRAINT [DF_asset_out_duration]  DEFAULT ((0)),
	[active] [bit] NOT NULL CONSTRAINT [DF_asset_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_asset_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_asset_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_asset_updated_date]  DEFAULT (getutcdate()),
	[checkout_date] [datetime] NOT NULL CONSTRAINT [DF_asset_checkout_date]  DEFAULT (getutcdate()),
	[attribute_key_1] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_1]  DEFAULT (''),
	[attribute_value_1] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_1]  DEFAULT (''),
	[attribute_key_2] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_2]  DEFAULT (''),
	[attribute_value_2] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_2]  DEFAULT (''),
	[attribute_key_3] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_3]  DEFAULT (''),
	[attribute_value_3] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_3]  DEFAULT (''),
	[attribute_key_4] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_4]  DEFAULT (''),
	[attribute_value_4] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_4]  DEFAULT (''),
	[attribute_key_5] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_5]  DEFAULT (''),
	[attribute_value_5] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_5]  DEFAULT (''),
	[attribute_key_6] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_6]  DEFAULT (''),
	[attribute_value_6] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_6]  DEFAULT (''),
	[attribute_key_7] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_7]  DEFAULT (''),
	[attribute_value_7] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_7]  DEFAULT (''));

 CREATE TABLE [login_auth_group_users](
	[login_auth_group_id] [int] NOT NULL,
	[user_id] [int] NOT NULL);

CREATE TABLE [alert_report](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](250) NOT NULL,
	[view_name] [varchar](250) NULL);


CREATE TABLE [sms_gateway](
	[id] [int] NOT NULL,
	[carrier] [nvarchar](max) NOT NULL,
	[postfix] [nvarchar](max) NOT NULL);


CREATE TABLE [user](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[user_guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_user_user_guid]  DEFAULT (newid()),
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[keyboard_password] [nvarchar](50) NOT NULL,
	[prox_password] [nvarchar](200) NULL,
	[swipe_password] [nvarchar](max) NULL,
	[fingerprint_template] [varbinary](max) NULL,
	[user_role] [varchar](15) NOT NULL CONSTRAINT [DF_user_user_role]  DEFAULT ((0)),
	[active] [bit] NOT NULL CONSTRAINT [DF_user_active]  DEFAULT ((1)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_user_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_user_updated_date]  DEFAULT (getutcdate()),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_user_deleted]  DEFAULT ((0)),
	[login_date] [datetime] NOT NULL CONSTRAINT [DF_user_login_date]  DEFAULT (getutcdate()),
	[email] [varchar](250) NULL,
	[phone] [varchar](250) NULL,
	[gateway_id] [int] NULL,
	[allow_email] [bit] NULL,
	[allow_sms] [bit] NULL);


CREATE TABLE [asset_unit_assets](
	[asset_unit_id] [int] NOT NULL,
	[asset_id] [int] NOT NULL);


CREATE TABLE [checkout_auth_group](
	[checkout_auth_group_id] [int] IDENTITY(1,1) NOT NULL,
	[checkout_auth_group_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[deleted] [bit] NOT NULL CONSTRAINT [DF_checkout_auth_group_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_checkout_auth_group_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_checkout_auth_group_updated_date]  DEFAULT (getutcdate());


CREATE TABLE [Comdyn_Reporting_SiteMap](
	[ID] [int] NOT NULL,
	[Title] [varchar](32) NULL,
	[Description] [varchar](512) NULL,
	[Url] [varchar](512) NULL,
	[Roles] [varchar](512) NULL,
	[Parent] [int] NULL,
	[Category] [varchar](50) NULL,
	[imageUrl] [varchar](100) NULL);



CREATE TABLE [Comdyn_Reporting_tblInsertQuery](
	[queryID] [uniqueidentifier] NOT NULL,
	[query] [varchar](1000) NOT NULL,
	[queryName] [varchar](100) NOT NULL,
	[visibilityID] [int] NULL,
	[userID] [uniqueidentifier] NOT NULL,
	[dateentered] [smalldatetime] NULL,
	[datelastmodified] [smalldatetime] NULL,
	[viewname] [varchar](100) NOT NULL,
	[DateCreated] [datetime] NULL,
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL);


CREATE TABLE [Comdyn_Reporting_tblOperators](
	[operatorid] [uniqueidentifier] NOT NULL,
	[operatornames] [varchar](25) NOT NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF__Comdyn_Re__DateC__1CDC41A7]  DEFAULT (getdate()),
	[DateLastModified] [datetime] NULL CONSTRAINT [DF__Comdyn_Re__DateL__1DD065E0]  DEFAULT (getdate()),
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL);

CREATE TABLE [issue_reason_list](
	[issue_reason_list_id] [int] IDENTITY(1,1) NOT NULL,
	[issue_reason_list_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_issue_reason_list_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_issue_reason_list_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_issue_reason_list_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_issue_reason_list_updated_date]  DEFAULT (getutcdate());


CREATE TABLE [Comdyn_Reporting_tblSelectColumns](
	[id] [uniqueidentifier] NOT NULL,
	[name] [varchar](100) NOT NULL,
	[queryID] [uniqueidentifier] NULL,
	[DateCreated] [datetime] NULL,
	[DateLastModified] [datetime] NULL,
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL);


CREATE TABLE [Comdyn_Reporting_tblShowParameters](
	[parameterID] [uniqueidentifier] NOT NULL,
	[parameterColumnName] [varchar](50) NULL,
	[parameterOperator] [varchar](50) NULL,
	[parameterValue] [varchar](50) NULL,
	[queryID] [uniqueidentifier] NULL,
	[datatype] [varchar](50) NULL,
	[DateCreated] [datetime] NULL,
	[DateLastModified] [datetime] NULL,
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL);


CREATE TABLE [Comdyn_Reporting_tblSortColumns](
	[SortColumnID] [uniqueidentifier] NOT NULL,
	[SortColumnName] [varchar](50) NOT NULL,
	[SortChoice] [varchar](50) NOT NULL,
	[queryID] [uniqueidentifier] NULL,
	[DataType] [varchar](100) NULL,
	[DateCreated] [datetime] NULL,
	[DateLastModified] [datetime] NULL,
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL);

CREATE TABLE [Comdyn_Reporting_tblViews](
	[ViewID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Comdyn_Reporting_tblViews_ViewID]  DEFAULT (newid()),
	[ViewName] [varchar](100) NOT NULL);


CREATE TABLE [login_auth_group](
	[login_auth_group_id] [int] IDENTITY(1,1) NOT NULL,
	[login_auth_group_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[deleted] [bit] NOT NULL CONSTRAINT [DF_login_auth_group_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_login_auth_group_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_login_auth_group_updated_date]  DEFAULT (getutcdate()));


CREATE TABLE [Comdyn_Reporting_ReportGen](
	[ModuleID] [int] NOT NULL,
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[Content] [ntext] NOT NULL,
	[CreatedByUser] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL);


CREATE TABLE [alert_transactions](
	[mail_transactions_id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [int] NOT NULL,
	[location_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[report_name] [nvarchar](50) NULL,
	[mail_recipients] [nvarchar](max) NULL,
	[mail_body] [nvarchar](max) NULL,
	[mail_from] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
	[transaction_date] [datetime] NOT NULL CONSTRAINT [DF_mail_transactions_transaction_date]  DEFAULT (getutcdate()),
	[log_level] [nvarchar](50) NOT NULL);


CREATE TABLE [cabinet_transactions](
	[cabinet_transactions_id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [int] NOT NULL,
	[location_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[cabinet_id] [int] NULL,
	[command] [nvarchar](50) NULL,
	[packet_received] [nvarchar](max) NULL,
	[packet_sent] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[log_level] [nvarchar](50) NOT NULL,
	[transaction_date] [datetime] NOT NULL CONSTRAINT [DF_cabinet_transactions_transaction_date]  DEFAULT (getutcdate()));

CREATE TABLE [user_transactions](
	[user_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[system_id] [int] NOT NULL,
	[location_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[log_level] [nvarchar](50) NOT NULL,
	[description] [nvarchar](max) NULL,
	[device] [nvarchar](50) NULL,
	[transaction_date] [datetime] NOT NULL CONSTRAINT [DF_user_transaction_transaction_date]  DEFAULT (getutcdate()));

CREATE TABLE [attribute](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[display_order] [int] NOT NULL CONSTRAINT [DF__attribute__displ__18D6A699]  DEFAULT ((0)),
	[filter] [bit] NOT NULL CONSTRAINT [DF__attribute__filte__19CACAD2]  DEFAULT ((0)),
	[collection] [bit] NOT NULL CONSTRAINT [DF__attribute__colle__1ABEEF0B]  DEFAULT ((0)),
	[parent_id] [int] NULL,
	[visible] [bit] NOT NULL CONSTRAINT [DF__attribute__visib__1BB31344]  DEFAULT ((1));


CREATE TABLE [alert_report_users](
	[alert_report_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,);


CREATE TABLE [asset_unit](
	[asset_unit_id] [int] IDENTITY(1,1) NOT NULL,
	[asset_unit_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_asset_unit_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_asset_unit_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_asset_unit_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_asset_unit_updated_date]  DEFAULT (getutcdate()));


CREATE TABLE [issue_reason](
	[issue_reason_id] [int] IDENTITY(1,1) NOT NULL,
	[issue_reason_guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_issue_reason_issue_reason_guid]  DEFAULT (newid()),
	[name] [nvarchar](100) NOT NULL,
	[deleted] [bit] NOT NULL CONSTRAINT [DF_issue_reason_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_issue_reason_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_issue_reason_updated_date]  DEFAULT (getutcdate()));


CREATE TABLE [cabinet](
	[cabinet_id] [int] IDENTITY(1,1) NOT NULL,
	[cabinet_guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_cabinet_cabinet_guid]  DEFAULT (newid()),
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[address] [int] NOT NULL CONSTRAINT [DF_cabinet_address]  DEFAULT ((0)),
	[num_locations] [int] NOT NULL CONSTRAINT [DF_cabinet_num_locations]  DEFAULT ((0)),
	[num_full] [int] NOT NULL CONSTRAINT [DF_cabinet_num_full]  DEFAULT ((0)),
	[num_empty] [int] NOT NULL CONSTRAINT [DF_cabinet_num_empty]  DEFAULT ((0)),
	[flash_enabled] [int] NOT NULL CONSTRAINT [DF_cabinet_flash_enabled]  DEFAULT ((1)),
	[buzzer_enabled] [int] NOT NULL CONSTRAINT [DF_cabinet_buzzer_enabled]  DEFAULT ((1)),
	[system_id] [int] NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_cabinet_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_cabinet_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_cabinet_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_cabinet_updated_date]  DEFAULT (getutcdate()));


CREATE TABLE [asset_transactions](
	[asset_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[asset_id] [int] NULL,
	[cabinet_id] [int] NULL,
	[user_id] [int] NULL,
	[system_id] [int] NOT NULL,
	[location_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[issue_reason_id] [int] NULL,
	[transaction_date] [datetime] NOT NULL CONSTRAINT [DF_asset_transactions_transaction_date]  DEFAULT (getutcdate()),
	[message] [nvarchar](max) NULL,
	[asset_status] [nvarchar](50) NOT NULL,
	[asset_type] [nvarchar](50) NULL,
	[asset_removal_type] [nvarchar](50) NULL,
	[asset_unit_id] [int] NULL);


CREATE TABLE [company](
	[company_id] [int] IDENTITY(1,1) NOT NULL,
	[company_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_company_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_company_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_company_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_company_updated_date]  DEFAULT (getutcdate()));


CREATE TABLE [attribute_collection_value](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_collection_id] [int] NOT NULL,
	[object_id] [int] NOT NULL CONSTRAINT [DF__attribute__objec__253C7D7E]  DEFAULT ((0)));

CREATE TABLE [checkout_auth_group_users](
	[checkout_auth_group_id] [int] NOT NULL,
	[user_id] [int] NOT NULL);


CREATE TABLE [issue_reason_list_reasons](
	[issue_reason_list_id] [int] NOT NULL,
	[issue_reason_id] [int] NOT NULL);


CREATE TABLE [system](
	[system_id] [int] IDENTITY(1,1) NOT NULL,
	[system_guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_system_system_guid]  DEFAULT (newid()),
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[checkout_timeout] [int] NOT NULL,
	[checkin_timeout] [int] NOT NULL,
	[open_door_timeout] [int] NOT NULL,
	[close_door_timeout] [int] NOT NULL,
	[default_timeout] [int] NOT NULL,
	[login_timeout] [int] NOT NULL,
	[use_issue_reason] [bit] NOT NULL CONSTRAINT [DF_system_use_issue_reason]  DEFAULT ((0)),
	[issue_reason_list_id] [int] NULL,
	[location_id] [int] NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_system_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_system_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_system_created_time]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_system_updated_time]  DEFAULT (getutcdate());


CREATE TABLE [user_access_group](
	[user_access_group_id] [int] IDENTITY(1,1) NOT NULL,
	[user_access_group_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[issue_limit] [int] NOT NULL CONSTRAINT [DF_user_access_group_issue_limit]  DEFAULT ((0)),
	[restrict_asset_list] [bit] NOT NULL CONSTRAINT [DF_user_access_group_restrict_asset_list]  DEFAULT ((0)),
	[require_login_auth] [bit] NOT NULL CONSTRAINT [DF_user_access_group_require_login_auth]  DEFAULT ((0)),
	[allow_remote_auth] [bit] NOT NULL CONSTRAINT [DF_user_access_group_allow_remote_auth]  DEFAULT ((0)),
	[active] [bit] NOT NULL CONSTRAINT [DF_user_access_group_active]  DEFAULT ((1)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_user_access_group_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_user_access_group_updated_date]  DEFAULT (getutcdate()),
	[login_auth_group_id] [int] NULL,
	[deleted] [bit] NOT NULL CONSTRAINT [DF_user_access_group_deleted]  DEFAULT ((0)),
	[out_duration_limit] [float] NULL);


CREATE TABLE [user_access_group_users](
	[user_access_group_id] [int] NOT NULL,
	[user_id] [int] NOT NULL);

CREATE TABLE [user_access_group_systems](
	[system_id] [int] NOT NULL,
	[user_access_group_id] [int] NOT NULL);


CREATE TABLE [location](
	[location_id] [int] IDENTITY(1,1) NOT NULL,
	[location_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[company_id] [int] NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_location_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_location_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_location_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_location_updated_date]  DEFAULT (getutcdate()));


CREATE TABLE [user_access_group_cabinets](
	[cabinet_id] [int] NOT NULL CONSTRAINT [DF_user_access_group_cabinet_list_cabinet_id]  DEFAULT ((0)),
	[user_access_group_id] [int] NOT NULL CONSTRAINT [DF_user_access_group_cabinet_list_user_access_group_id]  DEFAULT ((0)));


CREATE TABLE [attribute_single_value](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_id] [int] NOT NULL,
	[value] [varchar](500) NULL,
	[object_id] [int] NOT NULL CONSTRAINT [DF__attribute__objec__2077C861]  DEFAULT ((0)));


CREATE TABLE [access_time](
	[access_time_id] [int] IDENTITY(1,1) NOT NULL,
	[asset_access_group_id] [int] NULL,
	[user_access_group_id] [int] NULL,
	[day] [nvarchar](50) NOT NULL,
	[start_time] [datetime] NOT NULL CONSTRAINT [DF_access_time_start_time]  DEFAULT (getutcdate()),
	[end_time] [datetime] NOT NULL CONSTRAINT [DF_access_time_end_time]  DEFAULT (getutcdate()));


CREATE TABLE [user_access_group_assets](
	[user_access_group_id] [int] NOT NULL,
	[asset_id] [int] NOT NULL);


CREATE TABLE [attribute_collection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_id] [int] NOT NULL,
	[value] [varchar](500) NULL,
	[parent_id] [int] NULL);




