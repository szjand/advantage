USE [KeyperData]
GO
/****** Object:  Table [dbo].[asset]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[asset](
	[asset_id] [int] IDENTITY(1,1) NOT NULL,
	[asset_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[fob_number] [int] NOT NULL CONSTRAINT [DF_asset_fob_number]  DEFAULT ((0)),
	[asset_status] [varchar](15) NOT NULL CONSTRAINT [DF_asset_asset_status]  DEFAULT ('none'),
	[asset_type] [varchar](15) NOT NULL CONSTRAINT [DF_asset_asset_type]  DEFAULT ('none'),
	[asset_removal_type] [varchar](15) NOT NULL CONSTRAINT [DF_asset_asset_removal_type]  DEFAULT ('none'),
	[cabinet_id] [int] NULL,
	[user_id] [int] NULL,
	[issue_reason_id] [int] NULL,
	[out_duration] [float] NOT NULL CONSTRAINT [DF_asset_out_duration]  DEFAULT ((0)),
	[active] [bit] NOT NULL CONSTRAINT [DF_asset_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_asset_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_asset_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_asset_updated_date]  DEFAULT (getutcdate()),
	[checkout_date] [datetime] NOT NULL CONSTRAINT [DF_asset_checkout_date]  DEFAULT (getutcdate()),
	[attribute_key_1] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_1]  DEFAULT (''),
	[attribute_value_1] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_1]  DEFAULT (''),
	[attribute_key_2] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_2]  DEFAULT (''),
	[attribute_value_2] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_2]  DEFAULT (''),
	[attribute_key_3] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_3]  DEFAULT (''),
	[attribute_value_3] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_3]  DEFAULT (''),
	[attribute_key_4] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_4]  DEFAULT (''),
	[attribute_value_4] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_4]  DEFAULT (''),
	[attribute_key_5] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_5]  DEFAULT (''),
	[attribute_value_5] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_5]  DEFAULT (''),
	[attribute_key_6] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_6]  DEFAULT (''),
	[attribute_value_6] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_6]  DEFAULT (''),
	[attribute_key_7] [varchar](30) NOT NULL CONSTRAINT [DF_asset_attribute_key_7]  DEFAULT (''),
	[attribute_value_7] [varchar](100) NOT NULL CONSTRAINT [DF_asset_attribute_value_7]  DEFAULT (''),
 CONSTRAINT [PK_asset] PRIMARY KEY CLUSTERED 
(
	[asset_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[v_asset_transactions]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_asset_transactions]
AS
SELECT        asset_transactions.asset_transaction_id AS [Transaction ID], [user].first_name + ' ' + [user].last_name AS [User Name], asset.name AS Asset, 
                         asset_transactions.asset_type AS Type, asset_transactions.asset_status AS Status, asset_transactions.asset_removal_type AS [Last Removal Type], 
                         cabinet.name AS Cabinet, system.name AS System, location.name AS Location, company.name AS Company, asset_transactions.message AS Description, 
                         issue_reason.name AS Reason, asset_transactions.transaction_date AS DATE
FROM            asset_transactions INNER JOIN
                         company ON asset_transactions.company_id = company.company_id INNER JOIN
                         system ON asset_transactions.system_id = system.system_id INNER JOIN
                         location ON asset_transactions.location_id = location.location_id LEFT OUTER JOIN
                         [user] ON asset_transactions.user_id = [user].user_id INNER JOIN
                         cabinet ON asset_transactions.cabinet_id = cabinet.cabinet_id INNER JOIN
                         asset ON asset_transactions.asset_id = asset.asset_id LEFT OUTER JOIN
                         issue_reason ON asset_transactions.issue_reason_id = issue_reason.issue_reason_id
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[66] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "asset_transactions"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "company"
            Begin Extent = 
               Top = 6
               Left = 265
               Bottom = 125
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "system"
            Begin Extent = 
               Top = 6
               Left = 463
               Bottom = 125
               Right = 648
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "location"
            Begin Extent = 
               Top = 6
               Left = 686
               Bottom = 125
               Right = 846
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "user"
            Begin Extent = 
               Top = 6
               Left = 884
               Bottom = 125
               Right = 1071
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cabinet"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 203
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "asset"
            Begin Extent = 
               Top = 126
               Left = 241
               Bottom = 245
               Right = 428
            End
         ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_asset_transactions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'   DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "issue_reason"
            Begin Extent = 
               Top = 126
               Left = 466
               Bottom = 245
               Right = 644
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneHidden = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_asset_transactions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_asset_transactions'
GO
/****** Object:  View [dbo].[v_assets]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_assets]
AS
SELECT        asset.asset_id AS [Asset ID], asset.name AS Name, asset.description AS Description, asset.asset_status AS Status, asset.asset_type AS Type, 
                         asset.asset_removal_type AS [Removal Type], ir.name AS Reason, c.name AS Cabinet, asset.checkout_date AS [Checkout Date], s.name AS [System], 
                         l.name AS Location
FROM            asset LEFT OUTER JOIN
                         issue_reason ir ON ir.issue_reason_id = asset.issue_reason_id LEFT OUTER JOIN
                         cabinet c ON c.cabinet_id = asset.cabinet_id LEFT OUTER JOIN
                         [system] s ON s.system_id = c.system_id LEFT OUTER JOIN
                         location l ON l.location_id = s.location_id
--ORDER BY Name
GO
/****** Object:  StoredProcedure [dbo].[pAssetsCurrentlyOut]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pAssetsCurrentlyOut] 

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  name, [description], [Type], [Removal Type], [Reason], Cabinet, [Checkout Date] from dbo.v_assets where Status = 2
END
GO
/****** Object:  View [dbo].[v_assett_attributes]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_assett_attributes]
AS
SELECT     dbo.asset.asset_id, dbo.attribute_collection.value, dbo.attribute.name, dbo.attribute.display_order
FROM         dbo.asset INNER JOIN
                      dbo.attribute_collection_value ON dbo.asset.asset_id = dbo.attribute_collection_value.object_id INNER JOIN
                      dbo.attribute_collection ON dbo.attribute_collection_value.attribute_collection_id = dbo.attribute_collection.id INNER JOIN
                      dbo.attribute ON dbo.attribute_collection.attribute_id = dbo.attribute.id
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[11] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "asset"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 277
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "attribute_collection"
            Begin Extent = 
               Top = 11
               Left = 696
               Bottom = 197
               Right = 856
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "attribute_collection_value"
            Begin Extent = 
               Top = 15
               Left = 385
               Bottom = 146
               Right = 580
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "attribute"
            Begin Extent = 
               Top = 10
               Left = 939
               Bottom = 211
               Right = 1098
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 2130
         Width = 1500
         Width = 1500
         Width = 3315
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_assett_attributes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_assett_attributes'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_assett_attributes'
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CountOfAssetsWithFobNumbers]    Script Date: 07/01/2011 11:23:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_CountOfAssetsWithFobNumbers]  (@fob_number INT, @Asset_ID int)
RETURNS int
AS
BEGIN

      DECLARE @Count INT

      SELECT @Count = COUNT(*) FROM ASSET
      WHERE fob_number = @fob_number AND deleted = 'false' AND asset.Asset_ID <> @Asset_ID

      RETURN @Count

END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pDeleteReportGen]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pDeleteReportGen]

	@ModuleId       int,
        @ItemId         int

as

delete
from   Comdyn_Reporting_ReportGen
where  ModuleId = @ModuleId
and    ItemId = @ItemId
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetReportGen]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetReportGen]

	@ModuleId int,
        @ItemId int

as

select ModuleId,
       ItemId,
       Content,
       CreatedByUser,
       CreatedDate
from Comdyn_Reporting_ReportGen with (nolock)
left outer join COMDYNUsers on Comdyn_Reporting_ReportGen.CreatedByUser = COMDYNUsers.UserId
where  ModuleId = @ModuleId
and ItemId = @ItemId
GO
/****** Object:  Table [dbo].[login_auth_group_users]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[login_auth_group_users](
	[login_auth_group_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK_login_auth_group_users] PRIMARY KEY CLUSTERED 
(
	[login_auth_group_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[alert_report]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[alert_report](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](250) NOT NULL,
	[view_name] [varchar](250) NULL,
 CONSTRAINT [PK__alert_re__3213E83F71BCD978] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sms_gateway]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sms_gateway](
	[id] [int] NOT NULL,
	[carrier] [nvarchar](max) NOT NULL,
	[postfix] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK__sms_gate__3213E83F4F67C174] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[user_guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_user_user_guid]  DEFAULT (newid()),
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[keyboard_password] [nvarchar](50) NOT NULL,
	[prox_password] [nvarchar](200) NULL,
	[swipe_password] [nvarchar](max) NULL,
	[fingerprint_template] [varbinary](max) NULL,
	[user_role] [varchar](15) NOT NULL CONSTRAINT [DF_user_user_role]  DEFAULT ((0)),
	[active] [bit] NOT NULL CONSTRAINT [DF_user_active]  DEFAULT ((1)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_user_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_user_updated_date]  DEFAULT (getutcdate()),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_user_deleted]  DEFAULT ((0)),
	[login_date] [datetime] NOT NULL CONSTRAINT [DF_user_login_date]  DEFAULT (getutcdate()),
	[email] [varchar](250) NULL,
	[phone] [varchar](250) NULL,
	[gateway_id] [int] NULL,
	[allow_email] [bit] NULL,
	[allow_sms] [bit] NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetReportGens]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/** Create Stored Procedures **/


CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetReportGens]

	@ModuleId int

as

select ModuleId,
       ItemId,
       Content,
       CreatedByUser,
       CreatedDate
from Comdyn_Reporting_ReportGen with (nolock)
left outer join COMDYNUsers on Comdyn_Reporting_ReportGen.CreatedByUser = COMDYNUsers.UserId
where  ModuleId = @ModuleId
GO
/****** Object:  Table [dbo].[asset_unit_assets]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[asset_unit_assets](
	[asset_unit_id] [int] NOT NULL,
	[asset_id] [int] NOT NULL,
 CONSTRAINT [PK_asset_unit_assets] PRIMARY KEY CLUSTERED 
(
	[asset_unit_id] ASC,
	[asset_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[checkout_auth_group]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[checkout_auth_group](
	[checkout_auth_group_id] [int] IDENTITY(1,1) NOT NULL,
	[checkout_auth_group_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[deleted] [bit] NOT NULL CONSTRAINT [DF_checkout_auth_group_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_checkout_auth_group_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_checkout_auth_group_updated_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_checkout_authentication_group] PRIMARY KEY CLUSTERED 
(
	[checkout_auth_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comdyn_Reporting_SiteMap]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comdyn_Reporting_SiteMap](
	[ID] [int] NOT NULL,
	[Title] [varchar](32) NULL,
	[Description] [varchar](512) NULL,
	[Url] [varchar](512) NULL,
	[Roles] [varchar](512) NULL,
	[Parent] [int] NULL,
	[Category] [varchar](50) NULL,
	[imageUrl] [varchar](100) NULL,
 CONSTRAINT [PK_SiteMap] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comdyn_Reporting_tblInsertQuery]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comdyn_Reporting_tblInsertQuery](
	[queryID] [uniqueidentifier] NOT NULL,
	[query] [varchar](1000) NOT NULL,
	[queryName] [varchar](100) NOT NULL,
	[visibilityID] [int] NULL,
	[userID] [uniqueidentifier] NOT NULL,
	[dateentered] [smalldatetime] NULL,
	[datelastmodified] [smalldatetime] NULL,
	[viewname] [varchar](100) NOT NULL,
	[DateCreated] [datetime] NULL,
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comdyn_Reporting_tblOperators]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comdyn_Reporting_tblOperators](
	[operatorid] [uniqueidentifier] NOT NULL,
	[operatornames] [varchar](25) NOT NULL,
	[DateCreated] [datetime] NULL CONSTRAINT [DF__Comdyn_Re__DateC__1CDC41A7]  DEFAULT (getdate()),
	[DateLastModified] [datetime] NULL CONSTRAINT [DF__Comdyn_Re__DateL__1DD065E0]  DEFAULT (getdate()),
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblOPerators] PRIMARY KEY CLUSTERED 
(
	[operatorid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[issue_reason_list]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[issue_reason_list](
	[issue_reason_list_id] [int] IDENTITY(1,1) NOT NULL,
	[issue_reason_list_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_issue_reason_list_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_issue_reason_list_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_issue_reason_list_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_issue_reason_list_updated_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_issue_reason_list] PRIMARY KEY CLUSTERED 
(
	[issue_reason_list_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comdyn_Reporting_tblSelectColumns]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comdyn_Reporting_tblSelectColumns](
	[id] [uniqueidentifier] NOT NULL,
	[name] [varchar](100) NOT NULL,
	[queryID] [uniqueidentifier] NULL,
	[DateCreated] [datetime] NULL,
	[DateLastModified] [datetime] NULL,
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblSelectColumns] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comdyn_Reporting_tblShowParameters]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comdyn_Reporting_tblShowParameters](
	[parameterID] [uniqueidentifier] NOT NULL,
	[parameterColumnName] [varchar](50) NULL,
	[parameterOperator] [varchar](50) NULL,
	[parameterValue] [varchar](50) NULL,
	[queryID] [uniqueidentifier] NULL,
	[datatype] [varchar](50) NULL,
	[DateCreated] [datetime] NULL,
	[DateLastModified] [datetime] NULL,
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblShowParameters] PRIMARY KEY CLUSTERED 
(
	[parameterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comdyn_Reporting_tblSortColumns]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comdyn_Reporting_tblSortColumns](
	[SortColumnID] [uniqueidentifier] NOT NULL,
	[SortColumnName] [varchar](50) NOT NULL,
	[SortChoice] [varchar](50) NOT NULL,
	[queryID] [uniqueidentifier] NULL,
	[DataType] [varchar](100) NULL,
	[DateCreated] [datetime] NULL,
	[DateLastModified] [datetime] NULL,
	[UserCreated] [uniqueidentifier] NULL,
	[UserModified] [uniqueidentifier] NULL,
 CONSTRAINT [PK_tblSortColumns] PRIMARY KEY CLUSTERED 
(
	[SortColumnID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comdyn_Reporting_tblViews]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comdyn_Reporting_tblViews](
	[ViewID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Comdyn_Reporting_tblViews_ViewID]  DEFAULT (newid()),
	[ViewName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_tblViews] PRIMARY KEY CLUSTERED 
(
	[ViewID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[login_auth_group]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[login_auth_group](
	[login_auth_group_id] [int] IDENTITY(1,1) NOT NULL,
	[login_auth_group_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[deleted] [bit] NOT NULL CONSTRAINT [DF_login_auth_group_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_login_auth_group_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_login_auth_group_updated_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_login_authentication_group] PRIMARY KEY CLUSTERED 
(
	[login_auth_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[pAssetsCurrentlyInByDate]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pAssetsCurrentlyInByDate]

 @BeginDate AS DATETIME,
 @EndDate AS DATETIME

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from dbo.v_assets where Status = 1 and [Checkout Date] between @BeginDate and @EndDate
END
GO
/****** Object:  StoredProcedure [dbo].[pAssetsCurrentlyIn]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pAssetsCurrentlyIn] 

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  name, [description], [Type], Cabinet from dbo.v_assets where Status = 1
END
GO
/****** Object:  Table [dbo].[Comdyn_Reporting_ReportGen]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comdyn_Reporting_ReportGen](
	[ModuleID] [int] NOT NULL,
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[Content] [ntext] NOT NULL,
	[CreatedByUser] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_COMDYN_ReportGen] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[pTransactionsByUserName]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[pTransactionsByUserName]

 @BeginDate AS DATETIME,
 @EndDate AS DATETIME,
 @UserName AS VARCHAR(50)

AS
-- THis addresses that the dates have times
SET @EndDate = @EndDate + 1

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  [User Name],Asset, [Description], [Status],  Cabinet, [Date] from dbo.v_asset_transactions  
	where  [Date] between @BeginDate and @EndDate 
	and [User Name] like @UserName + '%'
	Order By [Date] DESC
END
GO
/****** Object:  StoredProcedure [dbo].[pTransactionsByAssetName]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pTransactionsByAssetName]

 @BeginDate AS DATETIME,
 @EndDate AS DATETIME,
 @AssetName AS VARCHAR(50)

AS
-- THis addresses that the dates have times
SET @EndDate = @EndDate + 1

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  [User Name],Asset, [Description], [Status],  Cabinet, [Date] from dbo.v_asset_transactions  
	where  [Date] between @BeginDate and @EndDate 
	and Asset like @AssetName + '%'
	Order By [Date] DESC
END
GO
/****** Object:  StoredProcedure [dbo].[pDailyTransactionReport]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pDailyTransactionReport]

 @BeginDate AS DATETIME,
 @EndDate AS DATETIME

AS

-- THis addresses that the dates have times
SET @EndDate = @EndDate + 1

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [User Name], Asset,  Status,  Cabinet, Date from dbo.v_asset_transactions  where  [Date] between @BeginDate and @EndDate
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetAllColumns]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================



CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetAllColumns]
      @View_Name AS VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SqlCommandString VARCHAR(150)
	SET @SqlCommandString = 'SELECT TOP 1 * FROM ' + @View_Name

	-- defend against sql injections
	
	IF CHARINDEX('-', @SqlCommandString) > 0
		SET @SqlCommandString = ''
		
	IF CHARINDEX(CHAR(39), @SqlCommandString) > 0
		SET @SqlCommandString = ''

	EXEC (@SqlCommandString)

END
GO
/****** Object:  Table [dbo].[alert_transactions]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[alert_transactions](
	[mail_transactions_id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [int] NOT NULL,
	[location_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[report_name] [nvarchar](50) NULL,
	[mail_recipients] [nvarchar](max) NULL,
	[mail_body] [nvarchar](max) NULL,
	[mail_from] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
	[transaction_date] [datetime] NOT NULL CONSTRAINT [DF_mail_transactions_transaction_date]  DEFAULT (getutcdate()),
	[log_level] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_mail_transactions] PRIMARY KEY CLUSTERED 
(
	[mail_transactions_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetPctPass]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetPctPass]
AS
BEGIN

	select  convert(varchar, TestDate, 101) as Date,convert(integer,(COnvert(float,maxNumberPassed)/COnvert(float,maxNumberTested))*100) as PctPass from dbo.Comdyn_Testing_TestReportData
	order by TestDate
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetPageLoadMinMax]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetPageLoadMinMax]
as

Select top 10 [Page],Convert(float,[MinSeconds]) as 'Min' ,Convert(float,[MaxSeconds]) as 'Max' from Comdyn_Reporting_View_PageLoadTimes
where Convert(float,[MaxSeconds]) < 10 and Convert(float,[MinSeconds]) <> .00
order by Convert(float,[MaxSeconds]) desc
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetDailySummary]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetDailySummary]
AS
BEGIN

	--select  convert(varchar, TestDate, 101) as Date,convert(integer,(COnvert(float,maxNumberPassed)/COnvert(float,maxNumberTested))*100) as PctPass from dbo.TestReportData
	--order by TestDate
	Declare @LastDate varchar(12)
	Declare @TestCases int
	Declare @TestCasesRun int
	Declare @TestCasesNotRun int
	Set @TestCases =(select count(*) from dbo.Comdyn_Testing_testcase)	
	Set @LastDate = (select max(convert(varchar, datetimestarted, 101)) from Comdyn_Testing_TestCase)	
	Set @TestCasesRun =(select count(*) from dbo.Comdyn_Testing_testcase
	where convert(varchar, datetimestarted, 101) = @LastDate)	
	Set @TestCasesNotRun = (@TestCases-@TestCasesRun)

	select 'Test Cases Not Run' as Description,Convert(varchar(15),@TestCasesNotRun) as Result,@LastDate as LastDate from dbo.Comdyn_Testing_testcase
	union
	select 'Test Cases Run - Last Date' as Description,Convert(varchar(15),count(*)) as Result,@LastDate from dbo.Comdyn_Testing_testcase
	
	where convert(varchar, datetimestarted, 101) = @LastDate
	--select count(Distinct testcase.TestCaseID) from dbo.testcase
	--join testresult on testcase.TestCaseID = testresult.TestCaseID
	
END
GO
/****** Object:  Table [dbo].[cabinet_transactions]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cabinet_transactions](
	[cabinet_transactions_id] [int] IDENTITY(1,1) NOT NULL,
	[system_id] [int] NOT NULL,
	[location_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[cabinet_id] [int] NULL,
	[command] [nvarchar](50) NULL,
	[packet_received] [nvarchar](max) NULL,
	[packet_sent] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[log_level] [nvarchar](50) NOT NULL,
	[transaction_date] [datetime] NOT NULL CONSTRAINT [DF_cabinet_transactions_transaction_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_cabinet_transactions] PRIMARY KEY CLUSTERED 
(
	[cabinet_transactions_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetDailyFailSummary]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetDailyFailSummary]
AS
BEGIN
	Declare @LastDate varchar(12)
	Set @LastDate = (select max(convert(varchar, datetimestarted, 101)) from Comdyn_Testing_TestCase)	
	
select 'Test Passed' as 'Results',count(*) as '#',@LastDate as LastDate  from dbo.Comdyn_Testing_testcase 
	Join dbo.Comdyn_Testing_TestResult on dbo.Comdyn_Testing_TestResult.TestCaseID = dbo.Comdyn_Testing_TestCase.TestCaseID  
	where Result = 'P'
	and DateEntered = (Select top 1 tr.dateEntered from dbo.Comdyn_Testing_TestResult tr where tr.TestCaseID =  dbo.Comdyn_Testing_TestCase.TestCaseID 
	order by tr.dateentered desc)
	
	union
	select 'Test Failed' as 'Passed',count(*) as '#',@LastDate as LastDate  from dbo.Comdyn_Testing_testcase 
	Join dbo.Comdyn_Testing_TestResult on dbo.Comdyn_Testing_TestResult.TestCaseID = dbo.Comdyn_Testing_TestCase.TestCaseID  
	where Result = 'F'
	and DateEntered = (Select top 1 tr.dateEntered from dbo.Comdyn_Testing_TestResult tr where tr.TestCaseID =  dbo.Comdyn_Testing_TestCase.TestCaseID 
	order by tr.dateentered desc)

	
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetColumnDataType]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetColumnDataType]
@Column_Name AS VARCHAR(50),
@Table_Name AS VARCHAR(50)
AS
BEGIN

	SELECT data_type
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE table_name = @Table_Name
	AND column_name = @Column_Name
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_PGetViewColumnNames]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Comdyn_Reporting_PGetViewColumnNames]
@ViewName varchar(100)
AS
SELECT v1.name
FROM sys.columns v1
INNER JOIN sys.views v2 on V1.OBJECT_ID = V2.OBJECT_ID
and V2.NAME = @ViewName
GO
/****** Object:  Table [dbo].[user_transactions]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_transactions](
	[user_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[system_id] [int] NOT NULL,
	[location_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[log_level] [nvarchar](50) NOT NULL,
	[description] [nvarchar](max) NULL,
	[device] [nvarchar](50) NULL,
	[transaction_date] [datetime] NOT NULL CONSTRAINT [DF_user_transaction_transaction_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_user_transaction] PRIMARY KEY CLUSTERED 
(
	[user_transaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_populateReportData]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_populateReportData] as

truncate table [Comdyn_Reporting_TestReportData]

Declare @date as smalldatetime 
Declare @enddate as smalldatetime, @MaxNumberPassed as bigint, @MaxNumberTested as bigint

set @MaxNumberPassed = 215
set @MaxNumberTested = 2357
SET @date = '7/1/2010'
SET @enddate = '10/1/2010'

While @date < @enddate
Begin

set @MaxNumberPassed = @MaxNumberPassed * 1.08

if (@MaxNumberPassed > @MaxNumberTested)
begin
set @MaxNumberPassed = @MaxNumberTested
end

if (right(@MaxNumberPassed,1) = 6 )
begin
set @MaxNumberPassed = @MaxNumberPassed * .7
end

if (right(@MaxNumberPassed,1) = 0 )
begin
set @MaxNumberPassed = @MaxNumberPassed / 1.13
end

if (right(@MaxNumberPassed,1) = 9 )
begin
set @MaxNumberPassed = @MaxNumberPassed / .85
end

INSERT INTO [Comdyn_Reporting_TestReportData]
           (
           [TestApplicationName]
           ,[TestDate]
           ,[MaxNumberPassed]
           ,[LastNumberPassed]
           ,[MaxNumberTested]
           ,[LastNumberTested]
           ,[TotalTestTested])
     VALUES
           (
           'Employee Benefits Test Demo'
           ,@date
           ,@MaxNumberPassed
           ,@MaxNumberPassed
           ,@MaxNumberTested
           ,@MaxNumberTested
           ,@MaxNumberTested * (1 + right(@MaxNumberPassed,1)))

Set @date = @date + 1

End
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pInsertTemplateParameter]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pInsertTemplateParameter]
@TemplateParameterID uniqueidentifier,
@TemplateID uniqueidentifier,
@ParameterData varchar(max),
@UserCreated uniqueidentifier,
@UserModified uniqueidentifier
AS
BEGIN
	INSERT INTO Comdyn_Reporting_tblTemplateParameters
	(TemplateParameterID, TemplateID, ParameterData, Active, UserCreated, UserModified)
	VALUES
	(@TemplateParameterID, @TemplateID, @ParameterData, 1, @UserCreated, @UserModified)
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pInsertEmail]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pInsertEmail] 
	@EmailID as uniqueidentifier,
	@Subject as varchar(50),
	@SentFrom as varchar(50),
	@SentTo as varchar(50),
	@Body as varchar(100)
	
AS
BEGIN
DECLARE @UserID uniqueidentifier

SELECT @UserID = UserID FROM aspnet_Users Where UserName = @SentFrom
	
	INSERT INTO [dbo].[Comdyn_Reporting_tblEmail]
           ([EmailID]
           ,[Subject]
           ,[SentFrom]
           ,[SentTo]
           ,[Body],[Date],[UserCreated])
     VALUES
           (@EmailID,@Subject,@SentFrom,@SentTo,@Body,getdate(),@UserID)

END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pUpdateReportGen]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pUpdateReportGen]

	@ModuleId       int,
        @ItemId         int,
	@Content        ntext,
	@UserID         int

as

update Comdyn_Reporting_ReportGen
set    Content       = @Content,
       CreatedByUser = @UserID,
       CreatedDate   = getdate()
where  ModuleId = @ModuleId
and    ItemId = @ItemId
GO
/****** Object:  Table [dbo].[attribute]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[attribute](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[display_order] [int] NOT NULL CONSTRAINT [DF__attribute__displ__18D6A699]  DEFAULT ((0)),
	[filter] [bit] NOT NULL CONSTRAINT [DF__attribute__filte__19CACAD2]  DEFAULT ((0)),
	[collection] [bit] NOT NULL CONSTRAINT [DF__attribute__colle__1ABEEF0B]  DEFAULT ((0)),
	[parent_id] [int] NULL,
	[visible] [bit] NOT NULL CONSTRAINT [DF__attribute__visib__1BB31344]  DEFAULT ((1)),
	[object_type] [varchar](50) NOT NULL,
 CONSTRAINT [PK__attribut__3213E83F16EE5E27] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[alert_report_users]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[alert_report_users](
	[alert_report_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK__alert_re__6292A8DB758D6A5C] PRIMARY KEY CLUSTERED 
(
	[alert_report_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[asset_unit]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[asset_unit](
	[asset_unit_id] [int] IDENTITY(1,1) NOT NULL,
	[asset_unit_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_asset_unit_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_asset_unit_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_asset_unit_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_asset_unit_updated_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_asset_unit] PRIMARY KEY CLUSTERED 
(
	[asset_unit_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[issue_reason]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[issue_reason](
	[issue_reason_id] [int] IDENTITY(1,1) NOT NULL,
	[issue_reason_guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_issue_reason_issue_reason_guid]  DEFAULT (newid()),
	[name] [nvarchar](100) NOT NULL,
	[deleted] [bit] NOT NULL CONSTRAINT [DF_issue_reason_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_issue_reason_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_issue_reason_updated_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_issue_reason] PRIMARY KEY CLUSTERED 
(
	[issue_reason_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cabinet]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cabinet](
	[cabinet_id] [int] IDENTITY(1,1) NOT NULL,
	[cabinet_guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_cabinet_cabinet_guid]  DEFAULT (newid()),
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[address] [int] NOT NULL CONSTRAINT [DF_cabinet_address]  DEFAULT ((0)),
	[num_locations] [int] NOT NULL CONSTRAINT [DF_cabinet_num_locations]  DEFAULT ((0)),
	[num_full] [int] NOT NULL CONSTRAINT [DF_cabinet_num_full]  DEFAULT ((0)),
	[num_empty] [int] NOT NULL CONSTRAINT [DF_cabinet_num_empty]  DEFAULT ((0)),
	[flash_enabled] [int] NOT NULL CONSTRAINT [DF_cabinet_flash_enabled]  DEFAULT ((1)),
	[buzzer_enabled] [int] NOT NULL CONSTRAINT [DF_cabinet_buzzer_enabled]  DEFAULT ((1)),
	[system_id] [int] NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_cabinet_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_cabinet_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_cabinet_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_cabinet_updated_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_cabinet] PRIMARY KEY CLUSTERED 
(
	[cabinet_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[asset_transactions]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[asset_transactions](
	[asset_transaction_id] [int] IDENTITY(1,1) NOT NULL,
	[asset_id] [int] NULL,
	[cabinet_id] [int] NULL,
	[user_id] [int] NULL,
	[system_id] [int] NOT NULL,
	[location_id] [int] NOT NULL,
	[company_id] [int] NOT NULL,
	[issue_reason_id] [int] NULL,
	[transaction_date] [datetime] NOT NULL CONSTRAINT [DF_asset_transactions_transaction_date]  DEFAULT (getutcdate()),
	[message] [nvarchar](max) NULL,
	[asset_status] [nvarchar](50) NOT NULL,
	[asset_type] [nvarchar](50) NULL,
	[asset_removal_type] [nvarchar](50) NULL,
	[asset_unit_id] [int] NULL,
 CONSTRAINT [PK_asset_transactions] PRIMARY KEY CLUSTERED 
(
	[asset_transaction_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pAddReportGen]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pAddReportGen]

			@ModuleId       int,
			@Content        ntext,
			@UserID         int

		as

		insert into Comdyn_Reporting_ReportGen (
			ModuleId,
			Content,
			CreatedByUser,
			CreatedDate
		) 
		values (
			@ModuleId,
			@Content,
			@UserID,
			getdate()
		)
GO
/****** Object:  Table [dbo].[company]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[company](
	[company_id] [int] IDENTITY(1,1) NOT NULL,
	[company_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_company_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_company_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_company_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_company_updated_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_company] PRIMARY KEY CLUSTERED 
(
	[company_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[attribute_collection_value]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attribute_collection_value](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_collection_id] [int] NOT NULL,
	[object_id] [int] NOT NULL CONSTRAINT [DF__attribute__objec__253C7D7E]  DEFAULT ((0)),
 CONSTRAINT [PK__attribut__3213E83F2354350C] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[checkout_auth_group_users]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[checkout_auth_group_users](
	[checkout_auth_group_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK_checkout_auth_group_users] PRIMARY KEY CLUSTERED 
(
	[checkout_auth_group_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[issue_reason_list_reasons]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[issue_reason_list_reasons](
	[issue_reason_list_id] [int] NOT NULL,
	[issue_reason_id] [int] NOT NULL,
 CONSTRAINT [PK_issue_reason_list_reasons] PRIMARY KEY CLUSTERED 
(
	[issue_reason_list_id] ASC,
	[issue_reason_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[system]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[system](
	[system_id] [int] IDENTITY(1,1) NOT NULL,
	[system_guid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_system_system_guid]  DEFAULT (newid()),
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[checkout_timeout] [int] NOT NULL,
	[checkin_timeout] [int] NOT NULL,
	[open_door_timeout] [int] NOT NULL,
	[close_door_timeout] [int] NOT NULL,
	[default_timeout] [int] NOT NULL,
	[login_timeout] [int] NOT NULL,
	[use_issue_reason] [bit] NOT NULL CONSTRAINT [DF_system_use_issue_reason]  DEFAULT ((0)),
	[issue_reason_list_id] [int] NULL,
	[location_id] [int] NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_system_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_system_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_system_created_time]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_system_updated_time]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_system] PRIMARY KEY CLUSTERED 
(
	[system_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_access_group]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_access_group](
	[user_access_group_id] [int] IDENTITY(1,1) NOT NULL,
	[user_access_group_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[issue_limit] [int] NOT NULL CONSTRAINT [DF_user_access_group_issue_limit]  DEFAULT ((0)),
	[restrict_asset_list] [bit] NOT NULL CONSTRAINT [DF_user_access_group_restrict_asset_list]  DEFAULT ((0)),
	[require_login_auth] [bit] NOT NULL CONSTRAINT [DF_user_access_group_require_login_auth]  DEFAULT ((0)),
	[allow_remote_auth] [bit] NOT NULL CONSTRAINT [DF_user_access_group_allow_remote_auth]  DEFAULT ((0)),
	[active] [bit] NOT NULL CONSTRAINT [DF_user_access_group_active]  DEFAULT ((1)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_user_access_group_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_user_access_group_updated_date]  DEFAULT (getutcdate()),
	[login_auth_group_id] [int] NULL,
	[deleted] [bit] NOT NULL CONSTRAINT [DF_user_access_group_deleted]  DEFAULT ((0)),
	[out_duration_limit] [float] NULL,
 CONSTRAINT [PK_user_access_group] PRIMARY KEY CLUSTERED 
(
	[user_access_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_access_group_users]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_access_group_users](
	[user_access_group_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK_user_access_group_users] PRIMARY KEY CLUSTERED 
(
	[user_access_group_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_access_group_systems]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_access_group_systems](
	[system_id] [int] NOT NULL,
	[user_access_group_id] [int] NOT NULL,
 CONSTRAINT [PK_user_access_group_systems] PRIMARY KEY CLUSTERED 
(
	[system_id] ASC,
	[user_access_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[location]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[location](
	[location_id] [int] IDENTITY(1,1) NOT NULL,
	[location_guid] [uniqueidentifier] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NULL,
	[company_id] [int] NOT NULL,
	[active] [bit] NOT NULL CONSTRAINT [DF_location_active]  DEFAULT ((1)),
	[deleted] [bit] NOT NULL CONSTRAINT [DF_location_deleted]  DEFAULT ((0)),
	[created_date] [datetime] NOT NULL CONSTRAINT [DF_location_created_date]  DEFAULT (getutcdate()),
	[updated_date] [datetime] NOT NULL CONSTRAINT [DF_location_updated_date]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_location] PRIMARY KEY CLUSTERED 
(
	[location_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_access_group_cabinets]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_access_group_cabinets](
	[cabinet_id] [int] NOT NULL CONSTRAINT [DF_user_access_group_cabinet_list_cabinet_id]  DEFAULT ((0)),
	[user_access_group_id] [int] NOT NULL CONSTRAINT [DF_user_access_group_cabinet_list_user_access_group_id]  DEFAULT ((0)),
 CONSTRAINT [PK_user_access_group_cabinets] PRIMARY KEY CLUSTERED 
(
	[cabinet_id] ASC,
	[user_access_group_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[attribute_single_value]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[attribute_single_value](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_id] [int] NOT NULL,
	[value] [varchar](500) NULL,
	[object_id] [int] NOT NULL CONSTRAINT [DF__attribute__objec__2077C861]  DEFAULT ((0)),
 CONSTRAINT [PK__attribut__3213E83F1E8F7FEF] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[access_time]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[access_time](
	[access_time_id] [int] IDENTITY(1,1) NOT NULL,
	[asset_access_group_id] [int] NULL,
	[user_access_group_id] [int] NULL,
	[day] [nvarchar](50) NOT NULL,
	[start_time] [datetime] NOT NULL CONSTRAINT [DF_access_time_start_time]  DEFAULT (getutcdate()),
	[end_time] [datetime] NOT NULL CONSTRAINT [DF_access_time_end_time]  DEFAULT (getutcdate()),
 CONSTRAINT [PK_asset_access_time] PRIMARY KEY CLUSTERED 
(
	[access_time_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_access_group_assets]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_access_group_assets](
	[user_access_group_id] [int] NOT NULL,
	[asset_id] [int] NOT NULL,
 CONSTRAINT [PK_user_access_group_assets] PRIMARY KEY CLUSTERED 
(
	[user_access_group_id] ASC,
	[asset_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[attribute_collection]    Script Date: 07/01/2011 11:23:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[attribute_collection](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[attribute_id] [int] NOT NULL,
	[value] [varchar](500) NULL,
	[parent_id] [int] NULL,
 CONSTRAINT [PK_attribute_collection] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_proc_GetSiteMapLeft]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_proc_GetSiteMapLeft] AS
    SELECT [ID], [Title], [Description], [Url], [Roles], [Parent], ImageUrl
    FROM [Comdyn_Reporting_SiteMap]
    WHERE Comdyn_Reporting_SiteMap.Category = 'Left'
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetSiteMapLeft]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetSiteMapLeft] AS
    SELECT [ID], [Title], [Description], [Url], [Roles], [Parent], ImageUrl
    FROM [Comdyn_Reporting_SiteMap]
    WHERE Comdyn_Reporting_SiteMap.Category = 'Left'
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetSiteMap]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetSiteMap] AS
    SELECT [ID], [Title], [Description], [Url], [Roles], [Parent]
    FROM [Comdyn_Reporting_SiteMap] 
    WHERE Comdyn_Reporting_SiteMap.Category IS NULL
    ORDER BY [ID]
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetSharedQueries]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetSharedQueries]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- put query right here
    select queryID as ID, queryName, query as savedsql, visibilityid, viewname
	from [dbo].[Comdyn_Reporting_tblInsertQuery] where visibilityID=0

END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetQueryByID]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetQueryByID]
@queryID as uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- put query right here
    select queryID, queryName, query as savedsql, visibilityid, viewname
	from [dbo].[Comdyn_Reporting_tblInsertQuery] where queryID = @queryID

END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetPrivateQueries]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetPrivateQueries]-- 'b4bc0dd2-9aaa-4467-b4e0-65e5c9f57863'
@UserID as UniqueIdentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- put query right here
   select queryID as ID,queryName, visibilityID, query as savedsql, viewname 
	from [dbo].[Comdyn_Reporting_tblInsertQuery] where visibilityID=1 and userid = @UserID

END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pExecuteQuery]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pExecuteQuery]
  @queryID AS UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


	-- put query right here
      
		DECLARE @sqlstmt AS VARCHAR(1000)
        SELECT @sqlstmt = query 
			FROM Comdyn_Reporting_tblInsertQuery 
			WHERE queryID=@queryID
		PRINT @sqlstmt
        EXEC(@sqlstmt)		
     END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pDeleteQuery]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pDeleteQuery]
  @queryID AS UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- put query right here
    DELETE FROM dbo.Comdyn_Reporting_tblInsertQuery WHERE queryID=@queryID

END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pUpdateQuery]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pUpdateQuery]  
       @queryID as uniqueidentifier,  
       @query as varchar(1000),  
       @queryName as varchar(100),  
    @visibilityID as int,  
    @userID as uniqueidentifier,
	@viewname as varchar(100)  ,
	   @UserModified as uniqueidentifier

AS  
BEGIN  
 

Update [dbo].[Comdyn_Reporting_tblInsertQuery]
Set query=@query,
	queryname=@queryname,
	visibilityid=@visibilityid,
	userid=@userID,
	viewname=@viewname,
UserModified=@UserModified
WHERE queryID = @queryID
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pInsertQuery]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pInsertQuery]  
       @queryID as uniqueidentifier,  
       @query as varchar(1000),  
       @queryName as varchar(100),  
	   @visibilityID as int,  
	   @userID as uniqueidentifier,
	   @viewname as varchar(100) , 
	   @UserCreated as uniqueidentifier,
	   @UserModified as uniqueidentifier
  
AS
declare @Exists int 
Set @Exists = (select COUNT(*) from Comdyn_Reporting_tblInsertQuery where queryID = @queryID)
If @Exists = 0
BEGIN  

Insert [dbo].[Comdyn_Reporting_tblInsertQuery]
(queryID, query, queryname, visibilityid, userid, viewname,UserCreated,UserModified)
Values
(@queryID, @query, @queryname, @visibilityID, @userID, @viewname,@UserCreated,@UserModified)
END
ELSE
BEGIN
Update [dbo].[Comdyn_Reporting_tblInsertQuery]
Set query=@query,
	queryname=@queryname,
	visibilityid=@visibilityid,
	userid=@userID,
	viewname=@viewname,
UserModified=@UserModified
WHERE queryID = @queryID
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetAllOperators]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetAllOperators]  
AS   
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT * FROM [dbo].[Comdyn_Reporting_tblOperators]  

END
GO
/****** Object:  View [dbo].[Comdyn_Reporting_View_Operators]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Comdyn_Reporting_View_Operators]
AS
SELECT    *
FROM        dbo.Comdyn_Reporting_tblOperators
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetAllColumnsByQueryID]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetAllColumnsByQueryID]
@queryID as uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select NAME, queryID, id
FROM Comdyn_Reporting_tblSelectColumns
WHERE queryID = @queryID


END


select * from INFORMATION_SCHEMA.VIEW_COLUMN_USAGE
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pDeleteSelectColumnsbyQueryID]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pDeleteSelectColumnsbyQueryID]
  @QueryID AS UNIQUEIDENTIFIER
AS
BEGIN

	-- put query right here
    DELETE FROM [dbo].[Comdyn_Reporting_tblSelectColumns] WHERE queryID = @QueryID

END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pInsertSelectColumn]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pInsertSelectColumn]
	@ID as uniqueidentifier,
    @Name as varchar(50),
    @queryID as uniqueidentifier,
    @UserID as uniqueidentifier

AS
BEGIN   
	INSERT INTO [dbo].[Comdyn_Reporting_tblSelectColumns]
	(ID,Name,queryID,UserCreated,UserModified)
	VALUES(@ID, @Name, @queryID,@UserID,@UserID)
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pDeleteSelectColumns]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pDeleteSelectColumns]
  @ID AS UNIQUEIDENTIFIER
AS
BEGIN

	-- put query right here
    DELETE FROM [dbo].[Comdyn_Reporting_tblSelectColumns] WHERE ID=@ID

END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pInsertParameters]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pInsertParameters]  
    
	@parameterID as uniqueidentifier,  
	@parameterColumnName as varchar(50),  
	@parameterOperator as varchar(50),  
	@parameterValue as varchar(50),  
	@queryID as uniqueidentifier,
	@datatype as varchar(50) ,
	@UserCreated as uniqueidentifier ,
	@UserModified as uniqueidentifier 

AS  
BEGIN  
	INSERT INTO [dbo].[Comdyn_Reporting_tblShowParameters]
	(parameterID,parameterColumnName,parameterOperator,parameterValue,queryID,datatype,UserCreated,UserModified)
	VALUES(@parameterID,@parameterColumnName,@parameterOperator,@parameterValue,@queryID,@datatype,@UserCreated,@UserModified)  
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pDeleteParameterRows]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pDeleteParameterRows]
		  @parameterID AS UNIQUEIDENTIFIER
		AS
		BEGIN
			-- SET NOCOUNT ON added to prevent extra result sets from
			-- interfering with SELECT statements.
			SET NOCOUNT ON;

			-- put query right here
			DELETE FROM [dbo].[Comdyn_Reporting_tblShowParameters] WHERE parameterID=@parameterID

		END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetWhereParameters]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetWhereParameters] --'8753a314-1e58-400f-8f9d-a08ca7825b8d'
@queryID as uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- put query right here
    select  
    parameterID, parameterColumnName, parameterOperator, parameterValue, queryID, datatype, DateCreated, DateLastModified, UserCreated, UserModified
    from [dbo].[Comdyn_Reporting_tblShowParameters] where queryID=@queryID AND 
    datatype<>'uniqueidentifier'

END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetSortColumns]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pGetSortColumns]
@queryID as uniqueidentifier	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select * from [dbo].[Comdyn_Reporting_tblSortColumns] where queryid = @queryID
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pDeleteAllSortRows]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Comdyn_Reporting_pDeleteAllSortRows]
	@sortColumnID as uniqueidentifier

AS
BEGIN

Delete from dbo.Comdyn_Reporting_tblSortColumns
where sortColumnID = @SortColumnID
END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pInsertSort]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
	-- Author:		<Author,,Name>
	-- Create date: <Create Date,,>
	-- Description:	<Description,,>
	-- =============================================
	CREATE PROCEDURE [dbo].[Comdyn_Reporting_pInsertSort]
		@sortColumnID as uniqueidentifier,
		@sortColumnname as varchar(50),
		@sortChoice as varchar(20),
		@queryID as uniqueidentifier,
		@UserID as uniqueidentifier
	AS
	BEGIN
		-- Insert statements for procedure here
		INSERT INTO [dbo].[Comdyn_Reporting_tblSortColumns](SortColumnID,SortColumnName,SortChoice,queryID,UserCreated)
		VALUES(@sortColumnID, @sortColumnname, @sortChoice,@queryID,@UserID)
	END
GO
/****** Object:  StoredProcedure [dbo].[Comdyn_Reporting_pGetAllViews]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[Comdyn_Reporting_pGetAllViews]
AS
	SELECT viewid, viewname AS [name], viewname
	FROM Comdyn_Reporting_tblViews
	ORDER BY viewname
GO
/****** Object:  View [dbo].[v_user_transactions]    Script Date: 07/01/2011 11:23:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_user_transactions]
AS
SELECT        user_transactions.user_transaction_id AS [Transaction ID], [user].first_name + ' ' + [user].last_name AS [User Name], system.name AS System, 
                         location.name AS Location, company.name AS Company, user_transactions.log_level AS [Level], user_transactions.description AS Description, 
                         user_transactions.device AS Device, user_transactions.transaction_date AS DATE
FROM            user_transactions INNER JOIN
                         company ON user_transactions.company_id = company.company_id INNER JOIN
                         system ON user_transactions.system_id = system.system_id INNER JOIN
                         location ON user_transactions.location_id = location.location_id LEFT OUTER JOIN
                         [user] ON user_transactions.user_id = [user].user_id
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[34] 4[26] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1[42] 4[33] 3) )"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2[40] 3) )"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2) )"
      End
      ActivePaneConfig = 5
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "user_transactions"
            Begin Extent = 
               Top = 145
               Left = 103
               Bottom = 264
               Right = 287
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "company"
            Begin Extent = 
               Top = 6
               Left = 263
               Bottom = 125
               Right = 423
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "system"
            Begin Extent = 
               Top = 14
               Left = 816
               Bottom = 133
               Right = 1001
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "location"
            Begin Extent = 
               Top = 39
               Left = 556
               Bottom = 158
               Right = 716
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "user"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 225
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 11
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 3060
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      PaneH' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_user_transactions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'idden = 
      Begin ColumnWidths = 11
         Column = 4140
         Alias = 1455
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_user_transactions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_user_transactions'
GO
/****** Object:  Check [chkAssetDuplicates]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[asset]  WITH NOCHECK ADD  CONSTRAINT [chkAssetDuplicates] CHECK  (([dbo].[fn_CountOfAssetsWithFobNumbers]([asset].[fob_number],[asset].[Asset_ID])<(1)))
GO
ALTER TABLE [dbo].[asset] CHECK CONSTRAINT [chkAssetDuplicates]
GO
/****** Object:  ForeignKey [FK_access_time_user_access_group]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[access_time]  WITH CHECK ADD  CONSTRAINT [FK_access_time_user_access_group] FOREIGN KEY([user_access_group_id])
REFERENCES [dbo].[user_access_group] ([user_access_group_id])
GO
ALTER TABLE [dbo].[access_time] CHECK CONSTRAINT [FK_access_time_user_access_group]
GO
/****** Object:  ForeignKey [FK_asset_cabinet]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[asset]  WITH CHECK ADD  CONSTRAINT [FK_asset_cabinet] FOREIGN KEY([cabinet_id])
REFERENCES [dbo].[cabinet] ([cabinet_id])
GO
ALTER TABLE [dbo].[asset] CHECK CONSTRAINT [FK_asset_cabinet]
GO
/****** Object:  ForeignKey [FK_asset_issue_reason]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[asset]  WITH CHECK ADD  CONSTRAINT [FK_asset_issue_reason] FOREIGN KEY([issue_reason_id])
REFERENCES [dbo].[issue_reason] ([issue_reason_id])
GO
ALTER TABLE [dbo].[asset] CHECK CONSTRAINT [FK_asset_issue_reason]
GO
/****** Object:  ForeignKey [FK_asset_user]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[asset]  WITH CHECK ADD  CONSTRAINT [FK_asset_user] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[asset] CHECK CONSTRAINT [FK_asset_user]
GO
/****** Object:  ForeignKey [FK_attribute_collection_attribute]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[attribute_collection]  WITH CHECK ADD  CONSTRAINT [FK_attribute_collection_attribute] FOREIGN KEY([attribute_id])
REFERENCES [dbo].[attribute] ([id])
GO
ALTER TABLE [dbo].[attribute_collection] CHECK CONSTRAINT [FK_attribute_collection_attribute]
GO
/****** Object:  ForeignKey [FK_attribute_collection_value_asset]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[attribute_collection_value]  WITH CHECK ADD  CONSTRAINT [FK_attribute_collection_value_asset] FOREIGN KEY([object_id])
REFERENCES [dbo].[asset] ([asset_id])
GO
ALTER TABLE [dbo].[attribute_collection_value] CHECK CONSTRAINT [FK_attribute_collection_value_asset]
GO
/****** Object:  ForeignKey [FK_attribute_collection_value_attribute_collection]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[attribute_collection_value]  WITH CHECK ADD  CONSTRAINT [FK_attribute_collection_value_attribute_collection] FOREIGN KEY([attribute_collection_id])
REFERENCES [dbo].[attribute_collection] ([id])
GO
ALTER TABLE [dbo].[attribute_collection_value] CHECK CONSTRAINT [FK_attribute_collection_value_attribute_collection]
GO
/****** Object:  ForeignKey [FK_attribute_single_value_asset]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[attribute_single_value]  WITH CHECK ADD  CONSTRAINT [FK_attribute_single_value_asset] FOREIGN KEY([object_id])
REFERENCES [dbo].[asset] ([asset_id])
GO
ALTER TABLE [dbo].[attribute_single_value] CHECK CONSTRAINT [FK_attribute_single_value_asset]
GO
/****** Object:  ForeignKey [FK_attribute_single_value_attribute]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[attribute_single_value]  WITH CHECK ADD  CONSTRAINT [FK_attribute_single_value_attribute] FOREIGN KEY([attribute_id])
REFERENCES [dbo].[attribute] ([id])
GO
ALTER TABLE [dbo].[attribute_single_value] CHECK CONSTRAINT [FK_attribute_single_value_attribute]
GO
/****** Object:  ForeignKey [FK_checkout_auth_group_users_checkout_auth_group]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[checkout_auth_group_users]  WITH CHECK ADD  CONSTRAINT [FK_checkout_auth_group_users_checkout_auth_group] FOREIGN KEY([checkout_auth_group_id])
REFERENCES [dbo].[checkout_auth_group] ([checkout_auth_group_id])
GO
ALTER TABLE [dbo].[checkout_auth_group_users] CHECK CONSTRAINT [FK_checkout_auth_group_users_checkout_auth_group]
GO
/****** Object:  ForeignKey [FK_checkout_auth_group_users_user]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[checkout_auth_group_users]  WITH CHECK ADD  CONSTRAINT [FK_checkout_auth_group_users_user] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[checkout_auth_group_users] CHECK CONSTRAINT [FK_checkout_auth_group_users_user]
GO
/****** Object:  ForeignKey [FK_issue_reason_list_reasons_issue_reason]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[issue_reason_list_reasons]  WITH CHECK ADD  CONSTRAINT [FK_issue_reason_list_reasons_issue_reason] FOREIGN KEY([issue_reason_id])
REFERENCES [dbo].[issue_reason] ([issue_reason_id])
GO
ALTER TABLE [dbo].[issue_reason_list_reasons] CHECK CONSTRAINT [FK_issue_reason_list_reasons_issue_reason]
GO
/****** Object:  ForeignKey [FK_issue_reason_list_reasons_issue_reason_list]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[issue_reason_list_reasons]  WITH CHECK ADD  CONSTRAINT [FK_issue_reason_list_reasons_issue_reason_list] FOREIGN KEY([issue_reason_list_id])
REFERENCES [dbo].[issue_reason_list] ([issue_reason_list_id])
GO
ALTER TABLE [dbo].[issue_reason_list_reasons] CHECK CONSTRAINT [FK_issue_reason_list_reasons_issue_reason_list]
GO
/****** Object:  ForeignKey [FK_location_company]    Script Date: 07/01/2011 11:23:45 ******/
ALTER TABLE [dbo].[location]  WITH CHECK ADD  CONSTRAINT [FK_location_company] FOREIGN KEY([company_id])
REFERENCES [dbo].[company] ([company_id])
GO
ALTER TABLE [dbo].[location] CHECK CONSTRAINT [FK_location_company]
GO
/****** Object:  ForeignKey [FK_system_issue_reason_list]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[system]  WITH CHECK ADD  CONSTRAINT [FK_system_issue_reason_list] FOREIGN KEY([issue_reason_list_id])
REFERENCES [dbo].[issue_reason_list] ([issue_reason_list_id])
GO
ALTER TABLE [dbo].[system] CHECK CONSTRAINT [FK_system_issue_reason_list]
GO
/****** Object:  ForeignKey [FK_system_location]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[system]  WITH CHECK ADD  CONSTRAINT [FK_system_location] FOREIGN KEY([location_id])
REFERENCES [dbo].[location] ([location_id])
GO
ALTER TABLE [dbo].[system] CHECK CONSTRAINT [FK_system_location]
GO
/****** Object:  ForeignKey [FK_user_access_group_login_authentication_group]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[user_access_group]  WITH CHECK ADD  CONSTRAINT [FK_user_access_group_login_authentication_group] FOREIGN KEY([login_auth_group_id])
REFERENCES [dbo].[login_auth_group] ([login_auth_group_id])
GO
ALTER TABLE [dbo].[user_access_group] CHECK CONSTRAINT [FK_user_access_group_login_authentication_group]
GO
/****** Object:  ForeignKey [FK_user_access_group_assets_user_access_group]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[user_access_group_assets]  WITH CHECK ADD  CONSTRAINT [FK_user_access_group_assets_user_access_group] FOREIGN KEY([user_access_group_id])
REFERENCES [dbo].[user_access_group] ([user_access_group_id])
GO
ALTER TABLE [dbo].[user_access_group_assets] CHECK CONSTRAINT [FK_user_access_group_assets_user_access_group]
GO
/****** Object:  ForeignKey [FK_user_access_group_cabinet_list_cabinet]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[user_access_group_cabinets]  WITH CHECK ADD  CONSTRAINT [FK_user_access_group_cabinet_list_cabinet] FOREIGN KEY([cabinet_id])
REFERENCES [dbo].[cabinet] ([cabinet_id])
GO
ALTER TABLE [dbo].[user_access_group_cabinets] CHECK CONSTRAINT [FK_user_access_group_cabinet_list_cabinet]
GO
/****** Object:  ForeignKey [FK_user_access_group_cabinet_list_user_access_group]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[user_access_group_cabinets]  WITH CHECK ADD  CONSTRAINT [FK_user_access_group_cabinet_list_user_access_group] FOREIGN KEY([user_access_group_id])
REFERENCES [dbo].[user_access_group] ([user_access_group_id])
GO
ALTER TABLE [dbo].[user_access_group_cabinets] CHECK CONSTRAINT [FK_user_access_group_cabinet_list_user_access_group]
GO
/****** Object:  ForeignKey [FK_user_access_group_system_list_system]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[user_access_group_systems]  WITH CHECK ADD  CONSTRAINT [FK_user_access_group_system_list_system] FOREIGN KEY([system_id])
REFERENCES [dbo].[system] ([system_id])
GO
ALTER TABLE [dbo].[user_access_group_systems] CHECK CONSTRAINT [FK_user_access_group_system_list_system]
GO
/****** Object:  ForeignKey [FK_user_access_group_system_list_user_access_group1]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[user_access_group_systems]  WITH CHECK ADD  CONSTRAINT [FK_user_access_group_system_list_user_access_group1] FOREIGN KEY([user_access_group_id])
REFERENCES [dbo].[user_access_group] ([user_access_group_id])
GO
ALTER TABLE [dbo].[user_access_group_systems] CHECK CONSTRAINT [FK_user_access_group_system_list_user_access_group1]
GO
/****** Object:  ForeignKey [FK_user_access_group_users_user]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[user_access_group_users]  WITH CHECK ADD  CONSTRAINT [FK_user_access_group_users_user] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[user_access_group_users] CHECK CONSTRAINT [FK_user_access_group_users_user]
GO
/****** Object:  ForeignKey [FK_user_access_group_users_user_access_group]    Script Date: 07/01/2011 11:23:46 ******/
ALTER TABLE [dbo].[user_access_group_users]  WITH CHECK ADD  CONSTRAINT [FK_user_access_group_users_user_access_group] FOREIGN KEY([user_access_group_id])
REFERENCES [dbo].[user_access_group] ([user_access_group_id])
GO
ALTER TABLE [dbo].[user_access_group_users] CHECK CONSTRAINT [FK_user_access_group_users_user_access_group]
GO
