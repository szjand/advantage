-- v1
SELECT viix.stocknumber, 
  CASE
    WHEN EXISTS (
      SELECT 1
      FROM VehicleInspections
      WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID)
      THEN (
        SELECT VehicleInspectionTs
        FROM VehicleInspections
        WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID)
    ELSE NULL
  END AS Inspection, 
  CASE
    WHEN EXISTS (
      SELECT 1
      FROM VehicleWalks
      WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID)
      THEN (
        SELECT VehicleWalkTS
        FROM VehicleWalks
        WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID)
    ELSE NULL
  END AS Walk,
  CASE
    WHEN EXISTS (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
        AND Category = 'RMFlagPulled')
      THEN (
        SELECT FromTS
        FROM VehicleInventoryItemStatuses 
        WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
          AND Category = 'RMFlagPulled')
    ELSE NULL
  END AS Pulled    
FROM VehicleInventoryItems viix
WHERE viix.VehicleInventoryItemID = '2bf4acc7-3a0b-49dc-b882-588338e82b9f'

-- v2
SELECT viix.stocknumber, 
  vinsp.VehicleInspectionTS AS Inspection,
  vwx.VehicleWalkTS AS Walk,
  pull.FromTS AS Pulled
FROM VehicleInventoryItems viix
LEFT JOIN VehicleInspections vinsp ON viix.VehicleInventoryItemID = vinsp.VehicleInventoryItemID
LEFT JOIN VehicleWalks vwx ON viix.VehicleInventoryItemID = vwx.VehicleInventoryItemID 
LEFT JOIN VehicleInventoryItemStatuses pull
  ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID
  AND pull.Category = 'RMFlagPulled'
--WHERE viix.VehicleInventoryItemID = '2bf4acc7-3a0b-49dc-b882-588338e82b9f'
WHERE cast(viix.FromTS AS sql_date) > '08/01/2010'
-- SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = '2bf4acc7-3a0b-49dc-b882-588338e82b9f'