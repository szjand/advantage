/*
SELECT count(viip.VehicleInventoryItemID) AS CountOfPic, viip.VehicleInventoryItemID
FROM viipictures viip
GROUP BY viip.VehicleInventoryItemID;
*/

SELECT 
  CASE
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagSB_SalesBuffer'
      AND ThruTS IS NULL) = 1 THEN  'Sales Buffer'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagWSB_WholesaleBuffer'
      AND ThruTS IS NULL) = 1 THEN 'Wholesale Buffer' 
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagPIT_PurchaseInTransit'
      AND ThruTS IS NULL) = 1 THEN 'In Transit'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagTNA_TradeNotAvailable'
      AND ThruTS IS NULL) = 1 THEN 'Trade Buffer'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagIP_InspectionPending'
      AND ThruTS IS NULL) = 1 THEN 'Inspection Pending'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagWP_WalkPending'
      AND ThruTS IS NULL) = 1 THEN 'Walk Pending'  
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagRMP_RawMaterialsPool'
      AND ThruTS IS NULL) = 1 THEN 'Raw Materials'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagAV_Available'
      AND ThruTS IS NULL) = 1 THEN 'Available'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagPB_PricingBuffer'
      AND ThruTS IS NULL) = 1 THEN 'Pricing Buffer'
    WHEN (    
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagPulled_Pulled'
      AND ThruTS IS NULL) = 1 THEN 'Pulled'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RawMaterials_BookingPending'
      AND ThruTS IS NULL) = 1 THEN 'Booking Pending'
  END AS Status,
/*  
CASE 
  WHEN viis.status = 'RMFlagSB_SalesBuffer' THEN 'Sales Buffer'
  WHEN viis.status = 'RMFlagWSB_WholesaleBuffer' THEN 'Wholesale Buffer' 
  WHEN viis.status = 'RMFlagPIT_PurchaseInTransit' THEN 'In Transit'
  WHEN viis.status = 'RMFlagTNA_TradeNotAvailable' THEN 'Trade Buffer'
  WHEN viis.status = 'RMFlagIP_InspectionPending' THEN 'Inspection Pending'
  WHEN viis.status = 'RMFlagWP_WalkPending' THEN 'Walk Pending'
  WHEN viis.status = 'RMFlagRMP_RawMaterialsPool' THEN 'Raw Materials'
  WHEN viis.status = 'RMFlagAV_Available' THEN 'Available'
  WHEN viis.status = 'RMFlagPB_PricingBuffer' THEN 'Pricing Buffer'
  WHEN viis.status = 'RMFlagPulled_Pulled' THEN 'Pulled'
  WHEN viis.status = 'RawMaterials_BookingPending' THEN 'Booking Pending'
END AS Status2,  
*/
CASE
  WHEN sb.VehicleInventoryItemID IS NOT NULL THEN 'Sales Buffer'
  WHEN rmp.VehicleInventoryItemID IS NOT NULL THEN 'Raw Materials'
  WHEN av.VehicleInventoryItemID IS NOT NULL THEN 'Available'
  WHEN pb.VehicleInventoryItemID IS NOT NULL THEN 'Pricing Buffer'
  WHEN p.VehicleInventoryItemID IS NOT NULL THEN 'Pulled'
END AS status2,
  vii.StockNumber, vi.VIN,
  vi.YearModel, vi.Make, vi.Model, vi.BodyStyle,
  Coalesce(Trim(vi.Trim),'') + ' '  + Coalesce(Trim(vi.Engine), '') AS VehicleDescription,	   
--  CASE
--    WHEN t.CountOfPic IS NULL THEN
--      0
--    ELSE
--	  t.CountOfPic  
--    END AS CountOfPic
  coalesce(t.countofpic, 0)
FROM VehicleInventoryItems vii
INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc
          on mmc.Make = vi.Make
          AND mmc.Model = vi.Model
          AND mmc.ThruTS IS NULL
--LEFT JOIN VehicleInventoryItemStatuses viis	ON vii.VehicleInventoryItemID = viis.VehicleInventoryItemID 
--  AND viis.ThruTS IS null	  
LEFT JOIN VehicleInventoryItemStatuses sb ON vii.VehicleInventoryItemID = sb.VehicleInventoryItemID 
  AND sb.status = 'RMFlagSB_SalesBuffer'
  AND sb.thruts IS NULL 		
LEFT JOIN VehicleInventoryItemStatuses wsb ON vii.VehicleInventoryItemID = wsb.VehicleInventoryItemID 
  AND wsb.status = 'RMFlagWSB_WholesaleBuffer'
  AND wsb.thruts IS NULL  
LEFT JOIN VehicleInventoryItemStatuses it ON vii.VehicleInventoryItemID = it.VehicleInventoryItemID 
  AND it.status = 'RMFlagPIT_PurchaseInTransit'
  AND it.thruts IS NULL     
LEFT JOIN VehicleInventoryItemStatuses tna ON vii.VehicleInventoryItemID = tna.VehicleInventoryItemID 
  AND tna.status = 'RMFlagTNA_TradeNotAvailable'
  AND tna.thruts IS NULL   
LEFT JOIN VehicleInventoryItemStatuses ip ON vii.VehicleInventoryItemID = ip.VehicleInventoryItemID 
  AND ip.status = 'RMFlagIP_InspectionPending'
  AND ip.thruts IS NULL
LEFT JOIN VehicleInventoryItemStatuses wp ON vii.VehicleInventoryItemID = wp.VehicleInventoryItemID 
  AND wp.status = 'RMFlagWP_WalkPending'
  AND wp.thruts IS NULL  
LEFT JOIN VehicleInventoryItemStatuses rmp ON vii.VehicleInventoryItemID = rmp.VehicleInventoryItemID 
  AND rmp.status = 'RMFlagRMP_RawMaterialsPool'
  AND rmp.thruts IS NULL 
LEFT JOIN VehicleInventoryItemStatuses av ON vii.VehicleInventoryItemID = av.VehicleInventoryItemID 
  AND av.status = 'RMFlagAV_Available'
  AND av.thruts IS NULL   
LEFT JOIN VehicleInventoryItemStatuses pb ON vii.VehicleInventoryItemID = pb.VehicleInventoryItemID 
  AND pb.status = 'RMFlagPB_PricingBuffer'
  AND pb.thruts IS NULL  
LEFT JOIN VehicleInventoryItemStatuses p ON vii.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND p.status = 'RMFlagPulled_Pulled'
  AND p.thruts IS NULL           
LEFT JOIN (
  SELECT count(viip.VehicleInventoryItemID) AS CountOfPic, viip.VehicleInventoryItemID
  FROM viipictures viip
  GROUP BY viip.VehicleInventoryItemID)	t ON vii.VehicleInventoryItemID = t.VehicleInventoryItemID 	  
WHERE vii.ThruTS IS NULL
AND vii.LocationID IN (
  SELECT PartyID2
  FROM PartyRelationships 
  WHERE PartyID1 =  'A4847DFC-E00E-42E7-89EE-CD4368445A82'
  AND Typ = 'PartyRelationship_MarketLocations')
AND coalesce(t.countofpic, 0) < 7
/*
AND ( 
  CASE
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagSB_SalesBuffer'
      AND ThruTS IS NULL) = 1 THEN  'Sales Buffer'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagWSB_WholesaleBuffer'
      AND ThruTS IS NULL) = 1 THEN 'Wholesale Buffer' 
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagPIT_PurchaseInTransit'
      AND ThruTS IS NULL) = 1 THEN 'In Transit'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagTNA_TradeNotAvailable'
      AND ThruTS IS NULL) = 1 THEN 'Trade Buffer'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagIP_InspectionPending'
      AND ThruTS IS NULL) = 1 THEN 'Inspection Pending'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagWP_WalkPending'
      AND ThruTS IS NULL) = 1 THEN 'Walk Pending'  
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagRMP_RawMaterialsPool'
      AND ThruTS IS NULL) = 1 THEN 'Raw Materials'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagAV_Available'
      AND ThruTS IS NULL) = 1 THEN 'Available'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagPB_PricingBuffer'
      AND ThruTS IS NULL) = 1 THEN 'Pricing Buffer'
    WHEN (    
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RMFlagPulled_Pulled'
      AND ThruTS IS NULL) = 1 THEN 'Pulled'
    WHEN (
      SELECT 1
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND status = 'RawMaterials_BookingPending'
      AND ThruTS IS NULL) = 1 THEN 'Booking Pending'
  END) NOT IN ('Inspection Pending', 
  'Walk Pending', 'In Transit', 'Trade Buffer')
*/  
AND wsb.VehicleInventoryItemID IS NULL 
AND tna.VehicleInventoryItemID IS NULL 
AND ip.VehicleInventoryItemID IS NULL
AND wp.VehicleInventoryItemID IS NULL 
ORDER BY stocknumber  
/*  
AND vii.VehicleInventoryItemID NOT IN (SELECT VehicleINventoryITemID FROM #PicCount)
  OR (vii.VehicleInventoryItemID IN (SELECT VehicleINventoryITemID FROM #PicCount)
    AND
	 (t.CountOfPic < @WithLessThan));
*/	 