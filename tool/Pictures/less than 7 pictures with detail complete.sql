SELECT *
FROM  VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID  = '8d3c628d-e6cc-4532-9fbb-c5e0ca6d4459'


SELECT b.stocknumber, a.*
FROM VehicleInventoryItemStatuses a
LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE a.category = 'RMFlagRMP'
  AND a.ThruTS IS NULL 
  AND LEFT(b.stocknumber, 1) = '1'

  -- less than 7 pics, RawMaterials
SELECT a.pics, a.VehicleInventoryItemID, b.stocknumber
INTO #wtf
FROM (  
  SELECT VehicleInventoryItemID, COUNT(*) AS pics
  FROM viiPictures  
  GROUP BY VehicleInventoryItemID 
  HAVING COUNT(*) < 7) a
LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN VehicleInventoryItemStatuses c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE b.ThruTS IS NULL
  AND left(b.stocknumber, 1) = '1'
  AND c.category = 'RMFlagRMP'
  AND c.ThruTS IS NULL   
  
  
SELECT a.*, b.typ, b.description, c.status, c.startts, c.completets
FROM #wtf a  
LEFT JOIN VehicleReconItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN AuthorizedReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
WHERE b.typ = 'PartyCollection_AppearanceReconItem'
  AND c.completets IS NOT NULL 
  
SELECT a.stocknumber, a.VehicleInventoryItemID, a.frontlineready, coalesce(b.pics, 0)
FROM vUsedCarsRawMaterials a 
LEFT JOIN (
  SELECT VehicleInventoryItemID, COUNT(*) AS pics
  FROM viiPictures  
  GROUP BY VehicleInventoryItemID) b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE LEFT(a.stocknumber, 1) = '1'


SELECT *
FROM vusedcarsopenrecon

SELECT a.VehicleInventoryItemID, a.stocknumber, a.status, a.piccount, 
  b.typ, b.description, c.status, c.startts, c.completets 
FROM (EXECUTE PROCEDURE GetVehiclePictureCounts('A4847DFC-E00E-42E7-89EE-CD4368445A82','',7)) a
LEFT JOIN VehicleReconItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN AuthorizedReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
WHERE b.typ = 'PartyCollection_AppearanceReconItem'
--  AND c.completets IS NOT NULL 
  AND LEFT(a.stocknumber, 1) = '1'
  AND a.status <> 'Available'