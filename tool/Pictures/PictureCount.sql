
SELECT vii.stocknumber, vii.Fromts, wtf.HowMany, o.name
FROM VehicleInventoryItems vii
LEFT JOIN (
  SELECT VehicleInventoryItemID,COUNT(*) AS howmany
  FROM viipictures
  GROUP BY VehicleInventoryItemID) AS wtf ON wtf.VehicleInventoryItemID = vii.VehicleInventoryItemID
LEFT JOIN Organizations o ON o.PartyID = vii.LocationID
WHERE vii.ThruTS IS NULL   
ORDER BY vii.fromts desc