SELECT stocknumber, wtf.pix, 
  Utilities.CurrentViiSalesStatus(viix.VehicleInventoryItemID)
FROM VehicleInventoryItems  viix
LEFT JOIN (
  SELECT VehicleInventoryItemID, COUNT(*) AS pix
  FROM viiPictures p
  WHERE EXISTS (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItems
    WHERE VehicleInventoryItemID = p.VehicleInventoryItemID
    AND ThruTS IS NULL)
  GROUP BY VehicleInventoryItemID) wtf ON viix.VehicleInventoryItemID = wtf.VehicleInventoryItemID 
WHERE viix.ThruTS IS NULL   
AND viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
ORDER BY Utilities.CurrentViiSalesStatus(viix.VehicleInventoryItemID), wtf.pix, stocknumber