SELECT *
FROM (
SELECT a.stocknumber,
  left(Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID),24) AS status,
  LEFT(TRIM(c.description),12) AS detail, CAST(d.completeTS AS sql_date) AS completed,
  coalesce(e.pics, 0) AS pics,
  left(Utilities.CurrentViiLocation(a.VehicleInventoryItemID), 50) AS location
FROM VehicleInventoryItems a
--LEFT JOIN ReconAuthorizations b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN VehicleReconItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID
  AND c.description IN ('Car Buff','Full Detail','Truck Buff')
LEFT JOIN AuthorizedReconItems d on c.VehicleReconItemID = d.VehicleReconItemID   
LEFT JOIN (
  SELECT VehicleInventoryItemID, COUNT(*) AS pics
  FROM viiPictures
  GROUP BY VehicleInventoryItemID) e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.thruts IS NULL 
  AND c.VehicleInventoryItemID IS NOT NULL 
  AND d.status = 'AuthorizedReconItem_Complete'
--ORDER BY pics  
) m
WHERE LEFT(stocknumber,1) <> 'h'
  AND pics < 10
  AND status NOT IN ('at auction','ws buffer','Sold and Not Delivered')
ORDER BY stocknumber   

