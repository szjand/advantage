SELECT *
FROM dps.viipictures
WHERE VehicleInventoryItemID = 'ba35c28d-1778-4caa-b1d3-e5708c5ea9d9'
  AND picture IS NOT null


UPDATE dps.viipictures
SET picture = x.picture
FROM (
SELECT *
FROM viipictures a
WHERE a.VehicleInventoryItemID = 'ba35c28d-1778-4caa-b1d3-e5708c5ea9d9')x
WHERE viipictures.VehicleInventoryItemID = 'ba35c28d-1778-4caa-b1d3-e5708c5ea9d9'
  AND viipictures.sequenceno = x.sequenceno

  
  
select COUNT(*) --147193
FROM dps.viipictures
WHERE picture IS null  


SELECT COUNT(*) --25915
FROM viipictures

SELECT * FROM x_dps.viipictures

SELECT *
FROM viipictures
WHERE picture IS NULL 


SELECT *
INTO #bad_pics
FROM viipictures
WHERE picture IS NULL;

CREATE INDEX empndx ON emp ( emp_addr, emp_name )
CREATE index test_ndx on #bad_pics(VehicleInventoryItemID, sequenceno)

select b.*
-- INTO #good_pics
FROM #bad_pics a
INNER JOIN x_dps.viipictures b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND a.sequenceno = b.sequenceno
  
  
SELECT * FROM VehicleInventoryItems WHERE VehicleInventoryItemID = 'db3f6592-7246-4179-bd38-53bb794cda31'  

select *
FROM #bad_pics a
LEFT JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 


DELETE FROM #bad_pics WHERE VehicleInventoryItemID = '773eb0be-7551-4da5-97a1-9d4eeb46527c'


SELECT VehicleInventoryItemID, sequenceno
FROM viipictures
WHERE sequenceno = 1
GROUP BY VehicleInventoryItemID, sequenceno
HAVING COUNT(*) > 1
ORDER BY VehicleInventoryItemID


SELECT *
FROM x_dps.viipictures
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID 
  FROM viipictures)
  
    
select * 
FROM #bad_pics a
LEFT JOIN viipictures b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND a.sequenceno = b.sequenceno
  
CREATE index vii_seq_ndx on viipictures(VehicleInventoryItemID, sequenceno)  
  
  
DELETE FROM viipictures
WHERE VehicleInventoryItemID IN (SELECT VehicleInventoryItemID FROM #bad_pics GROUP BY VehicleInventoryItemID)  
AND picture IS NULL  

INSERT INTO viipictures
SELECT * 
FROM x_dps.viipictures
WHERE VehicleInventoryItemID IN (
SELECT VehicleInventoryItemID 
FROM VehicleInventoryItems a
WHERE a.thruts IS NULL
  AND NOT EXISTS (
    SELECT 1
    FROM viipictures
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID ))
    
    
-------------------------------------------------------------------------------

-- no remaining pictures IN x_dps that don't EXISTS IN dps
SELECT VehicleInventoryItemID, stocknumber
-- SELECT COUNT(*)
FROM VehicleInventoryItems a
WHERE a.thruts IS NULL  
  AND NOT EXISTS (
    SELECT 1
    FROM viipictures
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID )   
  AND NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
      AND category IN ('RMFlagAtAuction','RMFlagWSB','RMFlagIP')
      AND thruts IS NULL)
  AND EXISTS (
    SELECT 1
    FROM x_dps.viipictures
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID )
        
        
SELECT * 
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '0785b811-0526-43b4-aaaa-dc951610c111'
  AND thruts IS null    