
SELECT vii.stocknumber, vii.VehicleInventoryItemID, vi.vin
FROM VehicleInventoryItems vii
INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
WHERE vii.ThruTS IS NULL
AND vii.LocationID IN (
  SELECT PartyID2
  FROM PartyRelationships 
  WHERE PartyID1 =  'A4847DFC-E00E-42E7-89EE-CD4368445A82'
  AND Typ = 'PartyRelationship_MarketLocations')
--AND NOT EXISTS (
--  SELECT 1
--  FROM VehicleInventoryItemStatuses
--  WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID 
--  AND category IN ('RMFlagIP', 'RMFlagWP', 'RMFlagPIT', 'RMFlagTNA')  
--  AND ThruTS IS NULL)
AND (
  SELECT COUNT(*)
  FROM viiPictures viip
  WHERE viip.VehicleInventoryItemID = vii.VehicleInventoryItemID) = 0 
AND (SELECT COUNT(*)FROM legacy.VehiclePictures WHERE picvin = vi.vin GROUP BY picvin) <> 0  
AND vii.stocknumber IS NOT NULL

DECLARE @StockNumber string;
@Stocknumber = 'H2270B';
INSERT INTO viiPictures (VehicleInventoryItemID, SequenceNo, Description, Picture)
SELECT vii.VehicleInventoryItemID, 
  CASE vp.Description
    WHEN 'Dash View' THEN 9
    WHEN 'Engine View' THEN 7
    WHEN 'Front 3/4 View' THEN 1
    WHEN 'Front View' THEN 5
    WHEN 'Interior View' THEN 4
    WHEN 'Rear 3/4 View' THEN 2
    WHEN 'Rear View' THEN 6
    WHEN 'Side View' THEN 3
    WHEN 'Wheel View' THEN 8
  END AS seq,
  vp.Description collate ADS_DEFAULT_CS, vp.LargePic
FROM VehicleInventoryItems vii
INNER JOIN vehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN legacy.VehiclePictures vp ON vp.picvin = vi.vin
WHERE vii.stocknumber = @Stocknumber
AND vp.description <> 'Raw Materials Pic';

INSERT INTO viiPictureUploads (VehicleInventoryItemID, SetType, UploadTS)
SELECT * FROM (
  SELECT DISTINCT vii.VehicleInventoryItemID, 'CartivaTransfer', cast('07/11/2010 06:06:06' AS SQL_TIMESTAMP) AS UploadTS
  FROM VehicleInventoryItems vii
  INNER JOIN vehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
  INNER JOIN legacy.VehiclePictures vp ON vp.picvin = vi.vin
  WHERE vii.stocknumber = @Stocknumber
  AND vp.description <> 'Raw Materials Pic') wtf;
/*
DELETE FROM  viiPictures WHERE VehicleInventoryItemID = 'dffadc13-248e-3f4f-8963-fb9474915343';
DELETE FROM  viiPictureUploads WHERE VehicleInventoryItemID = 'dffadc13-248e-3f4f-8963-fb9474915343';
*/