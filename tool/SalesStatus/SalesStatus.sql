/*
using fx CurrentViiSalesStatus AS a guide
issue has to DO with sold vehicles, dcu's

limiting statuses to NULL ThruTS AND category of RM

maybe a UNION 

SELECT stocknumber, 
  Utilities.CurrentViiSalesStatus(viix.VehicleInventoryItemID) AS status
FROM VehicleInventoryItems viix
ORDER BY status
WHERE viix.ThruTS IS NULL 

-- whhat are the OPEN statuses ON a closed VehicleInventoryItem 
-- fuck, only 2 vehicles
SELECT viix.VehicleInventoryItemID, COUNT(*)
FROM VehicleInventoryItems viix
INNER JOIN VehicleInventoryItemStatuses viisx ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND viisx.ThruTS IS NULL 
WHERE viix.ThruTS IS NOT NULL 
GROUP BY viix.VehicleInventoryItemID 
ORDER BY COUNT(*) DESC  

ok, so IF ALL i am interested IN IS non delivered vehicles,
just need to ADD dcuu

-- sold vehicles with OPEN VehicleInventoryItemStatuses 
-- ALL are dcu 
-- except 12305A AND H2840A which are delived (WS intra market

SELECT viix.stocknumber, viisx.status, viisx.fromts, vs.SoldTS 
FROM vehiclesales vs
INNER JOIN VehicleInventoryItemStatuses viisx ON vs.VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND viisx.ThruTS IS NULL
INNER JOIN VehicleInventoryItems viix ON vs.VehicleInventoryItemID = viix.VehicleInventoryItemID   
WHERE vs.status = 'VehicleSale_Sold'  
ORDER BY viix.stocknumber , viisx.fromts
*/

SELECT viisx.VehicleInventoryItemID,
  MAX(viix.stocknumbeR) AS stocknumber,
  max(
    CASE 
	  WHEN viisx.category = 'RMFlagAtAuction' THEN 'At Auction'
	  WHEN viisx.category = 'RMFlagSB' THEN 'Sales Buffer'
	  WHEN viisx.category = 'RMFlagWSB' THEN 'WS Buffer'
	  WHEN viisx.category = 'RMFlagPIT' THEN 'In Transit'
	  WHEN viisx.category = 'RMFlagTNA' THEN 'Trade Not Available'
	  WHEN viisx.category = 'RMFlagIP' THEN 'Inspection Pending'
	  WHEN viisx.category = 'RMFlagWP' THEN 'Walk Pending'
	  WHEN viisx.category = 'RMFlagRMP' THEN 'Raw Materials'
	  WHEN viisx.category = 'RMFlagAV' THEN 'Available'
	  WHEN viisx.category = 'RMFlagPB' THEN 'Pricing Buffer'
	  WHEN viisx.category = 'RMFlagPulled' THEN 'Pulled' 
	  WHEN viisx.status = 'RawMaterials_BookingPending' THEN 'Booking Pending'
	  ELSE ''
--	    CASE -- ehh, NOT quite
--		  WHEN viix.ThruTS IS NULL THEN 'Delivered'
--		  WHEN EXISTS (
--		    SELECT 1
--			FROM vehiclesales  
--			WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID 
--		  ELSE ''
--		END 
	END) AS SalesStatus
FROM VehicleInventoryItemStatuses viisx
LEFT JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE viisx.ThruTS IS NULL
--AND category LIKE 'RM%'
--AND viisx.VehicleInventoryItemID = 'fbab57ac-571d-47d1-96ae-ee1f3e282e5d'
GROUP BY viisx.VehicleInventoryItemID 
ORDER BY stocknumber

-- SELECT DISTINCT category FROM VehicleInventoryItemStatuses WHERE category LIKE 'RM%'

SELECT * -- 12853a DCU
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '3f8d4fc7-9cfc-4307-893b-6d2b1065b090'


SELECT * -- 12305A Delivered
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '703de030-a0ac-4185-81c7-55e5e3ccaf5b'




