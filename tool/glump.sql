SELECT salesstatus, max(curdate() - dateacquired), min(curdate() - dateacquired), COUNT(*)
FROM usedcars
WHERE market = 'SP-St. Paul'
AND location = 'SP-Saturn MV'
GROUP BY salesstatus

SELECT salesstatus, COUNT(*)
FROM usedcars
WHERE market = 'SP-St. Paul'
AND location = 'SP-Saturn MV'
GROUP BY salesstatus

SELECT curdate() - dateacquired
FROM usedcars
WHERE market = 'SP-St. Paul'
AND location = 'SP-Saturn MV'
AND salesstatus NOT IN ('Sold','Wholesaled')

SELECT *
FROM usedcars
WHERE market = 'SP-St. Paul'
AND location = 'SP-Saturn MV'
AND salesstatus = 'Available'
AND curdate() - dateacquired < 7

SELECT *
FROM usedcars
WHERE stocknumber LIKE '62755%'h

SELECT DISTINCT location FROM usedcars WHERE market = 'SP-St. Paul'

SELECT segment, vehicletype, glump
FROM usedcars
WHERE market = 'SP-St. Paul'
AND location = 'SP-Saturn MV'
AND salesstatus NOT IN ('Sold','Wholesaled')
GROUP BY segment, vehicletype

SELECT glump, COUNT(*)
FROM usedcars
WHERE market = 'SP-St. Paul'
AND location = 'SP-Saturn MV'
AND salesstatus NOT IN ('Sold','Wholesaled')
GROUP BY glump

SELECT glump, COUNT(*)
FROM usedcars
WHERE market = 'SP-St. Paul'
AND location = 'SP-Saturn WBL'
AND salesstatus NOT IN ('Sold','Wholesaled')
GROUP BY glump