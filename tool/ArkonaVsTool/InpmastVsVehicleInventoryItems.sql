SELECT v.stocknumber, t.vin, i.imvin --v.stocknumber, 
FROM dps.VehicleInventoryItems v
LEFT JOIN dps.VehicleItems t ON v.VehicleItemID = t.VehicleItemID 
LEFT JOIN stgArkonaINPMAST i ON v.stocknumber = i.imstk#
WHERE v.ThruTS IS NULL
  AND t.vin <> i.imvin
  
SELECT v.stocknumber, i.imstk#, t.vin, i.imvin --v.stocknumber, 
FROM dps.VehicleInventoryItems v
LEFT JOIN dps.VehicleItems t ON v.VehicleItemID = t.VehicleItemID 
LEFT JOIN stgArkonaINPMAST i ON t.vin = i.imvin
WHERE v.ThruTS IS NULL
  AND v.stocknumber <> i.imstk#
  AND v.stocknumber NOT LIKE '%*%'