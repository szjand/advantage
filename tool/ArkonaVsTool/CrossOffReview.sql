SELECT *
FROM stgArkonabopmast
WHERE bmstk# = '16469'

SELECT imstk#, imvin, imdinv, v.stocknumber, CAST(v.fromts AS sql_Date)
FROM stgArkonaINPMAST i
LEFT JOIN dps.VehicleInventoryItems v ON (i.imstk# = v.stocknumber)
WHERE imdinv BETWEEN '04/01/2012' AND '04/30/2012'
AND imtype = 'U'


SELECT imstk#, imvin, imdinv, v.vin
FROM stgArkonaINPMAST i
LEFT JOIN dps.VehicleItems  v ON (i.imvin = v.vin)
WHERE imdinv BETWEEN '04/01/2012' AND '04/30/2012'
  AND i.imtype = 'U'

-- inpmast.vin NOT IN VehicleItems 
SELECT imstk#, imvin, imdinv, imtype, imstat
FROM dds.stgArkonaINPMAST i
WHERE NOT EXISTS (
  SELECT 1
  FROM VehicleItems
  WHERE vin = imvin)
AND imtype = 'U'  
AND length(TRIM(imvin)) = 17
AND imstk# <> ''
AND imdinv BETWEEN curdate() - 400 AND curdate()

-- inpmast.vin NOT IN VehicleItems 
-- but inpmast.stocknumber IN VehicleInventoryItems  
-- bad vins   
SELECT x.*, v.stocknumber,
  (SELECT vin FROM VehicleItems WHERE VehicleItemID = v.VehicleItemID)
FROM (
  SELECT imstk#, imvin, imdinv, imtype, imstat
  FROM dds.stgArkonaINPMAST i
  WHERE NOT EXISTS (
    SELECT 1
    FROM VehicleItems
    WHERE vin = imvin)
  AND imtype = 'U'  
  AND length(TRIM(imvin)) = 17
  AND imstk# <> ''
  AND imdinv BETWEEN curdate() - 400 AND curdate()) x
LEFT JOIN VehicleInventoryItems v ON x.imstk# = v.stocknumber    
  
  
SELECT a.stocknumber, 
  CASE 
    WHEN a.thruts IS NULL THEN 'Inv'
    ELSE e.description
  END AS status,
  c.vin, f.*
FROM VehicleInventoryItems a
LEFT JOIN vehicleSales b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND status = 'VehicleSale_Sold'
LEFT JOIN TypDescriptions e ON b.typ = e.typ  
LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID 
LEFT JOIN dds.stgArkonaInpmast f ON a.stocknumber = f.imstk#
WHERE CAST(a.fromts AS sql_date) BETWEEN curdate() - 365 AND curdate()
    
-- stocknumber discrepancies    
    
SELECT *
FROM dds.stgArkonaBopmast
WHERE bmstk# = '16047b'

SELECT a.stocknumber, 
  CASE 
    WHEN a.thruts IS NULL THEN 'Inv'
    ELSE e.description
  END AS status,
  c.vin, f.*
FROM VehicleInventoryItems a
LEFT JOIN vehicleSales b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND status = 'VehicleSale_Sold'
LEFT JOIN TypDescriptions e ON b.typ = e.typ  
LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID 
LEFT JOIN dds.stgArkonaBopmast f ON a.stocknumber = f.bmstk#
WHERE CAST(a.fromts AS sql_date) BETWEEN curdate() - 365 AND curdate()

-- have to change my approach
1. trades
inpmast imtype = 'U' that are NOT IN VehicleInventoryItems (VehicleEvaluation status)

-- NULL VehicleInventoryItems BY stocknumber
SELECT a.imstk#, a.imvin, a.imdinv, b.*
FROM dds.stgArkonaInpmast a
LEFT JOIN VehicleInventoryItems b ON a.imstk# = b.stocknumber
WHERE a.imtype = 'u'
  AND a.imstat = 'i'
  AND b.VehicleInventoryItemID IS NULL 

-- NULL VehicleInventoryItems , non-null VehicleItems, with eval status
-- IF status IS booked: potential wrong stocknumber
-- ELSE IN eval, NOT booked  
    
-- vi.VehicleItemID NULL AND vii.stocknumber NOT NULL:  bad vin
-- vi.VehicleItemID NOT NULL AND vii.stocknumber NULL AND status = booked:  bad stocknumber
-- vi.VehicleItemID NOT NULL AND vii.stocknumber NULL AND status <> booked: eval NOT done

SELECT *
FROM (
  SELECT a.imstk#, a.imvin, a.imdinv, b.VehicleItemID, c.stocknumber, c.fromts
  FROM dds.stgArkonaInpmast a
  full OUTER JOIN VehicleItems b ON a.imvin = b.vin 
  full OUTER JOIN VehicleInventoryItems c on a.imstk# = c.stocknumber
  WHERE a.imtype = 'u'
    AND a.imstat = 'i') d
LEFT JOIN (
  SELECT a.VehicleItemID, a.VehicleEvaluationTS, a.currentstatus
  FROM VehicleEvaluations a
  INNER JOIN (
    SELECT VehicleItemID, max(VehicleEvaluationTS) AS veTS
    FROM VehicleEvaluations
    GROUP BY VehicleItemID) b ON a.VehicleItemID = b.VehicleItemID 
      AND a.VehicleEvaluationTS = b.veTS) e ON d.VehicleItemID = e.VehicleItemID     
WHERE (
  d.imstk# IS NULL 
  OR d.VehicleItemID IS NULL 
  OR d.stocknumber IS NULL)
  
-- shit added a bogus vehicle IN tool, why doesn't it show up AS imstk# null  
--   AND the reason why it was being LEFT out was the WHERE clause
--   limiting the inpmast records     
--   getting rid of the WHERE clause exposes some evals FROM today
  SELECT a.imstk#, a.imvin, a.imdinv, b.VehicleItemID, c.stocknumber, c.fromts
  FROM dds.stgArkonaInpmast a
  full OUTER JOIN VehicleItems b ON a.imvin = b.vin 
  full OUTER JOIN VehicleInventoryItems c on a.imstk# = c.stocknumber
--  WHERE a.imtype = 'u'
--    AND a.imstat = 'i'
  ORDER BY fromts desc    
-- this IS kind of going nowhere trying to limit the output
SELECT *
FROM (
  SELECT a.imstk#, a.imvin, a.imdinv, a.imtype, a.imstat, b.VehicleItemID, c.stocknumber, c.fromts
  FROM dds.stgArkonaInpmast a
  full OUTER JOIN VehicleItems b ON a.imvin = b.vin 
  full OUTER JOIN VehicleInventoryItems c on a.imstk# = c.stocknumber) d
WHERE (
    ((d.imstk# IS NULL OR imstk# = '') AND imvin IS NOT NULL)
    OR d.VehicleItemID IS NULL 
    OR d.stocknumber IS NULL) 
  AND imdinv <> '12/31/9999'  
  AND imtype = 'u'
  AND imstat = 'i'

-- decode different possible combinations  
SELECT *
FROM (
  SELECT a.imstk#, a.imvin, a.imdinv, a.imtype, a.imstat, b.VehicleItemID, c.stocknumber, c.fromts
  FROM stgArkonaInpmast a
  full OUTER JOIN dps.VehicleItems b ON a.imvin = b.vin 
  full OUTER JOIN dps.VehicleInventoryItems c on a.imstk# = c.stocknumber) d  
WHERE (imstk# IS NULL AND vehicleitemid IS NOT NULL AND stocknumber IS NULL) 

  
-- simulate a full OUTER JOIN with a UNION
SELECT *
FROM (
SELECT imstk#, imvin, imdinv, 'x' AS VehicleItemID 
FROM dds.stgArkonaInpmast a
WHERE a.imtype = 'u'
  AND a.imstat = 'i'  
UNION   
SELECT stocknumber, 'X', CAST(fromts AS sql_date), VehicleItemID
FROM VehicleInventoryItems  
UNION 
SELECT 'X', vin, cast('12/31/9999' AS sql_date), VehicleItemID
FROM VehicleItems)a
WHERE imdinv = curdate()     

-- ark stock# NOT IN tool
SELECT a.imstk#, a.imvin, a.imdinv
FROM dds.stgArkonaInpmast a
LEFT JOIN VehicleInventoryItems b ON a.imstk# = b.stocknumber 
WHERE a.imtype = 'u'
  AND a.imstat = 'i' 
  AND b.stocknumber IS NULL 
-- tool stock# NOT IN arkona  
SELECT a.stocknumber, a.fromts, c.vin
FROM VehicleInventoryItems a
LEFT JOIN dds.stgArkonaInpmast b ON a.stocknumber = b.imstk#
LEFT JOIN VehicleItems c ON a.VehicleItemID = c.VehicleItemID 
WHERE CAST(a.fromts AS sql_date) > curdate() - 90
  AND b.imstk# IS NULL    

2. sales

SELECT a.bmstk#, a.bmvin, a.bmwhsl, a.bmdtaprv, b.ThruTS
FROM dds.stgArkonabopmast a
LEFT JOIN VehicleInventoryItems b ON a.bmstk# = b.stocknumber
WHERE a.bmstat in ('a', 'u')
  AND a.bmdtaprv BETWEEN curdate() - 30 AND curdate()
  AND a.bmvtyp = 'u'


-- 9/4
-- deliveries for a day
SELECT imstk#, imvin, imstat, imdinv, imddlv
FROM stgarkonainpmast
WHERE imstk# IN ('16733a','17156xb','17303xxa','17401xa')

-- days deliveries FROM inpmast
SELECT a.imtype, a.imstk#, a.imvin, a.imstat, a.imdinv, a.imddlv, 
  b.bmstat, b.bmtype, b.bmwhsl
FROM stgarkonainpmast a
LEFT JOIN stgArkonaBOPMAST b ON a.imstk# = b.bmstk#
WHERE imddlv = '08/27/2012'
ORDER BY imstk#

SELECT *
FROM stgarkonabopmast
WHERE bmvin = '1GCNCPEX5BZ383007'


/*  BOPMAST */
FIELD   Field Def      VALUE      
BMSTAT  Record Status             In Process Deal (Unaccepted)
BMSTAT  Record Status   A         Accepted Deal
BMSTAT  Record Status   U         Capped Deal
BMTYPE  Record Type     C         Cash, Wholesale, Fleet, or Dealer Xfer
BMTYPE  Record Type     F         Financed Retail Deal
BMTYPE  Record Type     L         Financed Lease Deal
BMTYPE  Record Type     O         Cash Deal w/ Owner Financing
BMVTYP  Vehicle Type    N         New  
BMVTYP  Vehicle Type    U         Used  
BMWHSL  Sale Type       F         Fleet Deal
BMWHSL  Sale Type       L         Lease Deal
BMWHSL  Sale Type       R         Retail Deal
BMWHSL  Sale Type       W         Wholesale Deal
BMWHSL  Sale Type       X         Dealer Xfer

-- trade ins BY day
SELECT a.imtype, a.imstk#, a.imvin, a.imstat, a.imdinv, a.imddlv, immodl
-- SELECT *
FROM stgarkonainpmast a
WHERE imdinv = '09/13/2012'
  AND imtype = 'u'
ORDER BY imstk#

SELECT *
FROM dps.VehicleInventoryItems 
WHERE stocknumber = '16179a'

ok, let''s TRY to get a views of multiple tables for a day
base it ON inpmast.imddlv

SELECT a.imtype, a.imstk#, a.imvin, a.imstat, a.imdinv, a.imddlv, 
  b.bmstat, b.bmtype, b.bmwhsl, c.fromts, c.thruts
FROM stgarkonainpmast a
LEFT JOIN stgArkonaBOPMAST b ON a.imstk# = b.bmstk#
full OUTER JOIN dps.VehicleInventoryItems c ON a.imstk# = c.stocknumber
WHERE imddlv = '08/28/2012'
  AND imtype = 'u'
ORDER BY imstk#

-- 9/14, additions on C/O show more than inpmast 
SELECT a.imtype, a.imstk#, a.imvin, a.imstat, a.imdinv, a.imddlv, immodl
-- SELECT *
FROM stgarkonainpmast a
WHERE imstk# = '17021b'


different grains: arkona/tool
trades: insp pending vs trade buffer
sales: del vs sold AND NOT del

daily IS going to be challenging, but maybe possible
need to address the backlog first

SELECT *
FROM stgarkonainpmast a
LEFT JOIN dps.VehicleInventoryItems b ON a.imstk# = b.stocknumber
WHERE imstat = 'i'
  AND imtype = 'u'
  AND b.VehicleInventoryItemID IS NULL 

  -- 9/15
-- trying to see 17359A FROM this morning, it was NOT yet booked  
SELECT *
FROM stgarkonainpmast a
LEFT JOIN dps.VehicleInventoryItems b ON a.imstk# = b.stocknumber
  AND CAST(b.fromts AS sql_date) < curdate() 
WHERE imstat = 'i'
  AND imtype = 'u'
  AND b.VehicleInventoryItemID IS NULL 
  
-- 9/17  
cross off review:
16314A NOT booked got booked 9/17 8:06 AM
17436A eval NOT finished

SELECT *
FROM stgarkonainpmast a
LEFT JOIN dps.VehicleInventoryItems b ON a.imstk# = b.stocknumber
WHERE imstat = 'i'
  AND imtype = 'u'
  AND b.VehicleInventoryItemID IS NULL 
  
SELECT *
FROM stgarkonainpmast a
WHERE imstk# = '16314a'

SELECT *
FROM dps.VehicleInventoryItems 
WHERE stocknumber = '16314a'

SELECT *
FROM stgarkonabopmast
WHERE bmvin = '2CNFLGEY4A6312822'
  
  
   
  
