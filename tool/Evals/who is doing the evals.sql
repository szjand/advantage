SELECT b.thedate, c.fullname, a.* 
-- SELECT DISTINCT currentstatus
FROM VehicleEvaluations a
INNER JOIN dds.day b on CAST(a.VehicleEvaluationTS AS sql_date) = b.thedate
  AND b.thedate BETWEEN '01/01/2015' AND curdate()
INNER JOIN people c on a.vehicleEvaluatorID = c.partyid
WHERE currentstatus IN ('VehicleEvaluation_Booked','VehicleEvaluation_Finished')
  AND locationid = 'B6183892-C79D-4489-A58C-B526DF948B06' 

  
  
SELECT left(c.fullname, 20) as evaluator, COUNT(*) AS [evals Jan 2015]
-- SELECT DISTINCT currentstatus
FROM VehicleEvaluations a
INNER JOIN dds.day b on CAST(a.VehicleEvaluationTS AS sql_date) = b.thedate
  AND b.thedate BETWEEN '01/01/2015' AND curdate()
INNER JOIN people c on a.vehicleEvaluatorID = c.partyid
WHERE currentstatus IN ('VehicleEvaluation_Booked','VehicleEvaluation_Finished')
  AND locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  AND yearmonth = 201501 
GROUP BY c.fullname  

SELECT x.*, y.[evals Jan 2015] 
FROM (
  SELECT left(c.fullname, 20) as evaluator, COUNT(*) AS [evals Jun 2015]
  -- SELECT DISTINCT currentstatus
  FROM VehicleEvaluations a
  INNER JOIN dds.day b on CAST(a.VehicleEvaluationTS AS sql_date) = b.thedate
    AND b.thedate BETWEEN '01/01/2015' AND curdate()
  INNER JOIN people c on a.vehicleEvaluatorID = c.partyid
  WHERE currentstatus IN ('VehicleEvaluation_Booked','VehicleEvaluation_Finished')
    AND locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
    AND yearmonth = 201506
  GROUP BY c.fullname) x
LEFT JOIN (
  SELECT left(c.fullname, 20) as evaluator, COUNT(*) AS [evals Jan 2015]
  -- SELECT DISTINCT currentstatus
  FROM VehicleEvaluations a
  INNER JOIN dds.day b on CAST(a.VehicleEvaluationTS AS sql_date) = b.thedate
    AND b.thedate BETWEEN '01/01/2015' AND curdate()
  INNER JOIN people c on a.vehicleEvaluatorID = c.partyid
  WHERE currentstatus IN ('VehicleEvaluation_Booked','VehicleEvaluation_Finished')
    AND locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
    AND yearmonth = 201501 
  GROUP BY c.fullname) y on x.evaluator = y.evaluator  