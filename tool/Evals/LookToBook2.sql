/* tables of interest */
VehicleEvaluations

-- Typ:
SELECT Typ, COUNT(*)
FROM VehicleEvaluations
GROUP BY typ

-- CurrentStatus:
SELECT CurrentStatus, COUNT(*)
FROM VehicleEvaluations
GROUP BY CurrentStatus

VehicleInventoryItemID IS NULL until booked

-- evaluator
SELECT b.fullname, COUNT(*)
FROM VehicleEvaluations a
LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid 
GROUP BY b.fullname

-- sc
SELECT b.fullname, COUNT(*)
FROM VehicleEvaluations a
LEFT JOIN people b ON a.SalesConsultantID = b.partyid 
GROUP BY b.fullname

-- store
SELECT b.fullname, COUNT(*)
FROM VehicleEvaluations a
LEFT JOIN organizations b ON a.locationid = b.partyid 
GROUP BY b.fullname

-- look to book
SELECT  b.fullname, COUNT(*) AS Looked,
  SUM(CASE WHEN currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Booked
FROM VehicleEvaluations a
LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid 
WHERE a.typ <> 'VehicleEvaluation_Auction' 
  AND CAST(a.VehicleEvaluationTS AS sql_date) BETWEEN curdate() - 90 AND curdate()
  AND LocationID = (
    SELECT partyid
    FROM organizations
    WHERE name = 'rydells')
GROUP BY b.fullname 

-- booked BY
SELECT b.fullname, COUNT(*)
FROM VehicleInventoryItems a
LEFT JOIN people b ON a.BookerID = b.partyid
GROUP BY b.fullname  