-- each month since last october
SELECT COUNT(*) -- 874
FROM (
SELECT DISTINCT VEhicleItemID
FROM VehicleEvaluations
WHERE cast(VehicleEvaluationTS AS SQL_DATE) > '12/31/2009')wtf


SELECT COUNT(*) -- 477
FROM (
SELECT VehicleInventoryItemID 
FROM VehicleEvaluations
WHERE cast(VehicleEvaluationTS AS SQL_DATE) > '09/30/2009'
AND VehicleInventoryItemID IS NOT null )wtf


SELECT month(VehicleEvaluationTS), year(VehicleEvaluationTS), COUNT(*)
FROM VehicleEvaluations ve
WHERE cast(VehicleEvaluationTS AS SQL_DATE) > '09/30/2009'
GROUP BY month(VehicleEvaluationTS), year(VehicleEvaluationTS)
ORDER BY year(VehicleEvaluationTS), month(VehicleEvaluationTS)

-- Looked
-- DISTINCT vehicles Evaluated
SELECT month(wtf.veTS) AS Month, COUNT(*) AS Looked
FROM (
  SELECT VehicleItemID, MAX(VehicleEvaluationTS) AS veTS
  FROM VehicleEvaluations
  WHERE cast(VehicleEvaluationTS AS SQL_DATE) > '12/31/2010'
  GROUP BY VehicleItemID) wtf
GROUP BY month(wtf.veTS)

-- Booked
-- those vehicles evaluated since 10/1/09 that were booked
SELECT month(wtf.veTS) AS Month, COUNT(*) AS Booked
FROM (
  SELECT VehicleItemID, MAX(VehicleEvaluationTS) AS veTS
  FROM VehicleEvaluations
  WHERE cast(VehicleEvaluationTS AS SQL_DATE) > '12/31/2010'
  AND VehicleInventoryItemID IS NOT NULL 
  GROUP BY VehicleItemID) wtf
GROUP BY month(wtf.veTS)

-- 4/14/2001
-- non booked
SELECT status, COUNT(*)
FROM VEhicleItemSTatuses
WHERE ThruTS IS NULL
AND status LIKE 'VehicleEvaluation%'
AND timestampdiff(sql_tsi_Day, FromTS, now()) < 90
GROUP BY status

-- including booked:  fromts=ThruTS
SELECT status, COUNT(*)
FROM VEhicleItemSTatuses
WHERE ((ThruTS IS NULL) OR (FromTS = ThruTS))
AND status LIKE 'VehicleEvaluation%'
AND timestampdiff(sql_tsi_Day, FromTS, now()) < 90
GROUP BY status

-- including booked:  ThruTS IS NULL OR Booked - this one IS a little faster
SELECT status, COUNT(*)
FROM VEhicleItemSTatuses
WHERE ((ThruTS IS NULL) OR (status = 'VehicleEvaluation_Booked'))
AND status LIKE 'VehicleEvaluation%'
AND timestampdiff(sql_tsi_Day, FromTS, now()) < 90
GROUP BY status

/**** 
  these evaluations may NOT have been finished, but are there subsequent
  evaluations ON these vehicles 
  the question becomes what IS the eval history of a vehicle
  - use VehicleEvaluationID AS bt/tk to derive status info 
***/   

SELECT VehicleItemID, COUNT(*)
FROM VehicleEvaluations
WHERE CAST(vehicleEvaluationTS AS sql_date) > '12/31/2010'
GROUP BY VehicleItemID 
HAVING COUNT(*) > 1
ORDER BY COUNT(*) desc

SELECT *
FROM VehicleEvaluations
WHERE VehicleItemID = '0f9ce334-fd17-ce43-af16-53c35fe53832'

SELECT DISTINCT status FROM vehicleitemstatuses

selecvt

SELECT * FROM VehicleItemSTatuses WHERE status = 'VehicleEvaluation_Booked' ORDER BY fromts DESC

SELECT vexF

SELECT 761.0/(761 + 508 + 96 + 19) FROM system.iota


-- an evaluation started within the last x days
-- looking at EvalTS vs statusTS
-- looks LIKE EvalTS = WHEN DataCollection IS done
--  find the MIN status ts for each VehicleItems that has status of VE_DataDone
SELECT vex.VehicleITemID, vex.vehicleEvaluationTS, vix.status, vix.fromts, vix.thruts
FROM VehicleEvaluations vex
LEFT JOIN VehicleItemStatuses vix ON vex.VehicleItemID = vix.VehicleItemID 
WHERE vex.typ = 'VehicleEvaluation_Trade'
AND vex.VehicleItemID IN ('67f9dbc0-2f24-b641-9446-00142b966233',
  '5547E3CF-1B7D-4CB3-9D99-881F65522CDB','4b512d82-2d19-1e40-a735-1e2206a1d550')
AND vix.status LIKE 'VehicleEvaluation%'
ORDER BY vex.VehicleItemID, vix.fromts

-- so, IS the evalTS initially the DataDoneFromTS, THEN gets updated?
-- yep
SELECT vex.VehicleITemID, vex.vehicleEvaluationTS, vix.status, vix.fromts, vix.thruts
FROM VehicleEvaluations vex
INNER JOIN VehicleItemStatuses vix ON vex.VehicleItemID = vix.VehicleItemID 
WHERE vex.typ = 'VehicleEvaluation_Trade'
AND vix.status = 'VehicleEvaluation_DataDone'
AND vix.ThruTS IS null
ORDER BY vex.VehicleItemID, vix.fromts

