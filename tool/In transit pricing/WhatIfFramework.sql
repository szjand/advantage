-- stay focused, this IS SPECIFICALLY about what iffing intransit pricing
-- what would the intransit price have been, how does that compare to price
-- generated at walk

/*
at this point, we're good with 1 row per vehicle
SELECT vex.VehicleInventoryItemID 
FROM VehicleEvaluations vex
INNER JOIN VehicleInventoryItems viix -- IN fact, only 1 eval per VehicleInventoryItems
  ON vex.VehicleInventoryItemID = viix.VehicleInventoryItemID 
INNER JOIN VehicleInspections insp -- only inspected vehicles
  ON vex.VehicleInventoryItemID = insp.VehicleInventoryItemID  
INNER JOIN VehicleWalks vw -- only walked vehicles
  ON vex.VehicleInventoryItemID = vw.VehicleInventoryItemID   
WHERE vex.VehicleInventoryItemID IS NOT NULL
GROUP BY vex.VehicleInventoryItemID 
HAVING COUNT(*) > 1
*/

/*
-- base query
FROM VehicleEvaluations vex
INNER JOIN VehicleInventoryItems viix -- IN fact, only 1 eval per VehicleInventoryItems
  ON vex.VehicleInventoryItemID = viix.VehicleInventoryItemID 
INNER JOIN VehicleInspections insp -- only inspected vehicles
  ON vex.VehicleInventoryItemID = insp.VehicleInventoryItemID  
INNER JOIN VehicleWalks vw -- only walked vehicles
  ON vex.VehicleInventoryItemID = vw.VehicleInventoryItemID   
WHERE vex.VehicleInventoryItemID IS NOT NULL

*/
-- so, what columns DO we need
-- package
-- eval blackbook
-- walk blackbook

-- days to sell,

SELECT vex.VehicleEvaluationTS, vex.VehicleInventoryItemID, viix.stocknumber, 
  vix.yearmodel, left(vix.make, 12) AS Make, 
  left(vix.model,12) AS Model, left(vix.trim, 12) AS TrimLevel, m.Value AS Miles,
  CASE
    WHEN (select TimeStampDiff(SQL_TSI_MONTH, cast(Trim(CAST(YearModel as SQL_CHAR)) + '-01-01' as SQL_DATE), CurDate()) * 12 from system.iota) = 0 THEN
      value
    ELSE
      Truncate(value / TimeStampDiff(SQL_TSI_MONTH, CAST(Trim(CAST(yearmodel AS SQL_CHAR)) + '-01-01' AS SQL_DATE), CurDate()) * 12, 0)
  END AS MilesPerYear,   
  td.Description AS Source,
  CASE 
    WHEN viix.ThruTS IS NULL THEN 'Inventory'
    ELSE
      CASE vs.typ
        WHEN 'VehicleSale_Retail' THEN 'Retail'
        ELSE 'WholeSale'
      END 
  END AS Disposition,
  (SELECT typ
    FROM SelectedReconPackages 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND TableKey = vex.VehicleEvaluationID) AS EvalPackage,
  ed.DemandOverride as EvalDemand, 
  (SELECT value
    FROM VehicleAppeals
    WHERE VehicleInventoryItemID = vex.VehicleInventoryItemID 
    AND BasisTable = 'VehicleEvaluations') AS EvalAppeal,
  (SELECT Amount
    FROM VehicleThirdPartyValues
    WHERE VehicleInventoryItemID = vex.VehicleInventoryItemID
    AND TableKey = vex.VehicleEvaluationID
    AND Typ = 'VehicleThirdPartyValue_Manheim') AS EvalManheim,    
  ea.SelectedACV AS EvalACV,
  coalesce(vex.ReconMechanicalAmount, 0) + coalesce(ReconAppearanceAmount, 0) + coalesce(ReconTireAmount, 0) 
  + coalesce(ReconGlassAmount, 0) + coalesce(ReconInspectionAmount, 0) AS TotalEvalRecon,
  AmountShown, 
  (SELECT typ
    FROM SelectedReconPackages 
    WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND TableKey = vw.VehicleWalkID) AS WalkPackage,  
  wd.DemandOverride as WalkDemand,  
  (SELECT value
    FROM VehicleAppeals
    WHERE VehicleInventoryItemID = vw.VehicleInventoryItemID 
    AND BasisTable = 'VehicleWalks') AS WalkAppeal,
  (SELECT Amount
    FROM VehicleThirdPartyValues
    WHERE VehicleInventoryItemID = vw.VehicleInventoryItemID
    AND TableKey = vw.VehicleWalkID 
    AND Typ = 'VehicleThirdPartyValue_Manheim') AS WalkManheim,      
  wa.SelectedACV AS WalkACV, va.TotalWalkRecon,
  (SELECT TOP 1 vpd.Amount
    FROM VehiclePricings vp
    INNER JOIN VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
    WHERE vp.VehicleInventoryItemID = viix.VehicleInventoryItemID
    ORDER BY VehiclePricingTS ASC) as WalkPrice,  
  CAST(vw.VehicleWalkTS AS sql_date) AS WalkDate,
  (SELECT TOP 1 vpd.Amount
    FROM VehiclePricings vp
    INNER JOIN VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
    WHERE vp.VehicleInventoryItemID = viix.VehicleInventoryItemID
    ORDER BY VehiclePricingTS DESC) as LastPrice,  
  (SELECT TOP 1 CAST(VehiclePricingTS AS sql_date)
    FROM VehiclePricings vp
    INNER JOIN VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
    WHERE vp.VehicleInventoryItemID = viix.VehicleInventoryItemID
    ORDER BY VehiclePricingTS DESC) as LastPriceDate,
  vs.SoldAmount      
--SELECT vex.VehicleInventoryItemID 
--SELECT *
-- DROP TABLE #jon
INTO #jon
-- SELECT COUNT(*)
FROM VehicleEvaluations vex
INNER JOIN VehicleInventoryItems viix ON vex.VehicleInventoryItemID = viix.VehicleInventoryItemID -- IN fact, only 1 eval per VehicleInventoryItems
INNER JOIN VehicleInspections insp ON vex.VehicleInventoryItemID = insp.VehicleInventoryItemID  -- only inspected vehicles
INNER JOIN VehicleWalks vw ON vex.VehicleInventoryItemID = vw.VehicleInventoryItemID  -- only walked vehicles
LEFT JOIN acvs ea ON vex.VehicleEvaluationID = ea.tablekey -- single row per vehicle
LEFT JOIN acvs wa ON vw.VehicleWalkID = wa.tablekey   -- single row per vehicle
LEFT JOIN TypDescriptions td ON vex.typ = td.typ -- Source 
LEFT JOIN VehicleItemMileages m ON vex.VehicleEvaluationID = m.TableKey-- single row per vehicle
  AND m.BasisTable = 'VehicleEvaluations' 
LEFT JOIN VehicleItems vix ON vex.VehicleItemID = vix.VehicleItemID   
LEFT JOIN ReconAuthorizations rax ON vw.VehicleWalkID = rax.TableKey -- single row per vehicle
LEFT JOIN ( -- SUM of author recon items FROM walk authorization
  SELECT a.ReconAuthorizationID, SUM(coalesce(TotalPartsAmount, 0) + coalesce(LaborAmount, 0)) AS TotalWalkRecon
  FROM AuthorizedReconItems a
  INNER JOIN VehicleReconItems v ON a.VehicleReconItemID = v.VehicleReconItemID 
  GROUP BY a.ReconAuthorizationID) va ON rax.ReconAuthorizationID = va.ReconAuthorizationID  
LEFT JOIN VehicleSales vs ON vex.VehicleInventoryItemID = vs.VehicleInventoryItemID -- still good with single row per vehicle
  AND vs.status <> 'VehicleSale_SaleCanceled'
LEFT JOIN DemandScores ed ON vex.VehicleEvaluationID = ed.tablekey -- still good with single row per vehicle
LEFT JOIN DemandScores wd ON vw.VehicleWalkID = wd.tablekey -- still good with single row per vehicle
WHERE vex.VehicleInventoryItemID IS NOT NULL
AND CAST(vex.VehicleEvaluationTS AS sql_date) > CAST(now() AS sql_Date) - 300
AND vix.VinResolved = True
--GROUP BY vex.VehicleInventoryItemID HAVING COUNT(*) > 1


/* 
see sql\intramarket wholesale\pricing migration.sql
-- here's a problem  
SELECT tablekey FROM VehiclePricings GROUP BY tablekey HAVING COUNT(*) > 1
fucking intramarket ws
brings over a pricing
14486A 6/30 -> 7/28
H3867X bough imws, so it was never walked 
but the migrated pricing IS identical to the Rydell pricing, but for the VehicleInventoryItemID AND VehiclePricingID 
brings over the fucking rydell walk AS basistable AND tablekey
*/  



/* fuck it, dick with this later, mayber 
a subquery to show the first pricing for each vehicle

SELECT vpx.VehiclePricingID, vpx.VehicleInventoryItemID, vpx.VehiclePricingTS, vpdx.amount 
FROM VehiclePricings vpx
LEFT JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
  AND typ = 'VehiclePricingDetail_BestPrice'
ORDER BY vpx.VehicleInventoryItemID, vpx.VehiclePricingTS

SELECT * FROM VehiclePricingDetails 

-- same issue AS pricings
SELECT * FROM VehicleAppeals WHERE tablekey IN (SELECT tablekey FROM VehicleAppeals where basistable = 'VehicleWalks' GROUP BY tablekey HAVING COUNT(*) > 1)
ORDER BY tablekey
*/
  
  (SELECT Amount
    FROM VehicleThirdPartyValues
    WHERE VehicleInventoryItemID = vex.VehicleInventoryItemID
    AND TableKey = vex.VehicleEvaluationID
    AND Typ = 'VehicleThirdPartyValue_Manheim') AS EvalManheim,
  wd.DemandOverride as WalkDemand,   
  
SELECT * FROM VehicleThirdPartyValues
  
SELECT VehicleInventoryItemID, tablekey
FROM VehicleThirdPartyValues
WHERE VehicleInventoryItemID IS NOT NULL 
GROUP BY VehicleInventoryItemID, tablekey
HAVING COUNT(*) > 1  
ORDER BY VehicleInventoryItemID 

SELECT * FROM blackbookvalues

select stocknumber, VehicleEvaluationTS, VehicleWalkTS, b.*
FROM #jon j
LEFT JOIN BlackBookValues b ON j.VehicleEvaluationID = b.tablekey

select stocknumber
FROM #jon j
LEFT JOIN BlackBookValues b ON j.VehicleWalkID = b.tablekey
GROUP BY stocknumber HAVING COUNT(*) > 1

SELECT * FROM SelectedReconPackages 

-- hmm, only 2
SELECT VehicleInventoryItemID, tablekey
FROM SelectedReconPackages
WHERE VehicleInventoryItemID IS NOT NULL
GROUP BY VehicleInventoryItemID, tablekey
HAVING COUNT(*) > 1

SELECT * FROM #jon WHERE VehicleInventoryItemID IN ('034641f7-d6e1-6444-9963-93d2184892c7','79a5a88e-0dce-456f-b36c-f10dafc8a862')

SELECT * FROM SelectedReconPackages WHERE VehicleInventoryItemID IN ('034641f7-d6e1-6444-9963-93d2184892c7','79a5a88e-0dce-456f-b36c-f10dafc8a862')

SELECT * FROM VehicleInventoryItems WHERE VehicleInventoryItemID IN ('034641f7-d6e1-6444-9963-93d2184892c7','79a5a88e-0dce-456f-b36c-f10dafc8a862')

DELETE 
FROM SelectedReconPackages
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID  FROM SelectedReconPackages s
  WHERE VehicleInventoryItemID IS NOT NULL
  AND NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItems
    WHERE VehicleInventoryItemID = s.VehicleInventoryItemID))

SELECT * FROM VehicleInventoryItems WHERE VehicleInventoryItemID = '69af2416-217b-d145-9dd1-4214ee6ece2c'

SELECT j.stocknumber
FROM #jon j
LEFT JOIN SelectedReconPackages ep ON j.VehicleInventoryItemID = ep.VehicleInventoryItemID
  AND j.VehicleEvaluationID = ep.tablekey
GROUP BY j.stocknumber HAVING COUNT(*) > 1
