SELECT *
FROM (
select b.fullname, b.partyid
FROM partyprivileges a
INNER JOIN  people b ON a.partyid = b.partyid
LEFT JOIN dds.edwEmployeeDim c on b.lastname = c.lastname
  AND c.currentrow = true 
  AND c.active = 'active'
  AND c.distcode = 'sale'
WHERE a.privilege = 'Privilege_SC'
  AND a.thruts IS NULL 
  AND c.storecode IS NULL 
  AND b.fullname <> 'Mike Delohery') a
LEFT JOIN partyprivileges b on a.partyid = b.partyid  





UPDATE partyprivileges
SET thruts = now()
WHERE partyid IN (
  select b.partyid
  FROM partyprivileges a
  INNER JOIN  people b ON a.partyid = b.partyid
  LEFT JOIN dds.edwEmployeeDim c on b.lastname = c.lastname
    AND c.currentrow = true 
    AND c.active = 'active'
    AND c.distcode = 'sale'
  WHERE a.privilege = 'Privilege_SC'
    AND a.thruts IS NULL 
    AND c.storecode IS NULL 
    AND b.fullname <> 'Mike Delohery')
AND thruts IS NULL     




  select b.fullname, a.*
  FROM partyprivileges a
  INNER JOIN  people b ON a.partyid = b.partyid
  WHERE a.privilege = 'Privilege_SC'
    AND a.thruts IS NULL 
    AND b.fullname NOT IN ( 'Mike Delohery', 'Larry Stadstad')
ORDER BY fullname    
  

SELECT * FROM users WHERE username = 'jgarceau'

select * FROM partyprivileges WHERE partyid = '66B69E34-6924-4CE2-9409-45D1BF50A49B'


-- has ry1 NOT but NOT ry2
ry1: 'B6183892-C79D-4489-A58C-B526DF948B06'
ry2: '4CD8E72C-DC39-4544-B303-954C99176671'

select b.fullname, a.*
FROM partyprivileges a
INNER JOIN  people b ON a.partyid = b.partyid
WHERE a.privilege = 'Privilege_SC'
  AND a.thruts IS NULL 
  AND b.fullname NOT IN ( 'Mike Delohery', 'Larry Stadstad')
  AND locationid = '4CD8E72C-DC39-4544-B303-954C99176671'  
  AND NOT EXISTS (
    select 1
    FROM partyprivileges 
    WHERE partyid = a.partyid
      AND a.privilege = 'Privilege_SC'
      AND thruts IS NULL 
      AND locationid = 'B6183892-C79D-4489-A58C-B526DF948B06')  
  
  
-- give ry1 to ry2 sc
INSERT INTO partyprivileges
SELECT partyid, 'B6183892-C79D-4489-A58C-B526DF948B06', privilege, now(), cast(NULL AS sql_timestamp)
FROM (
-- select * FROM (
  select b.fullname, a.*
  FROM partyprivileges a
  INNER JOIN  people b ON a.partyid = b.partyid
  WHERE a.privilege = 'Privilege_SC'
    AND a.thruts IS NULL 
    AND b.fullname NOT IN ( 'Mike Delohery', 'Larry Stadstad')
    AND locationid = '4CD8E72C-DC39-4544-B303-954C99176671'  
    AND NOT EXISTS (
      select 1
      FROM partyprivileges 
      WHERE partyid = a.partyid
        AND a.privilege = 'Privilege_SC'
        AND thruts IS NULL 
        AND locationid = 'B6183892-C79D-4489-A58C-B526DF948B06')) x

        
-- give ry2 to ry1 sc
INSERT INTO partyprivileges
SELECT partyid, '4CD8E72C-DC39-4544-B303-954C99176671', privilege, now(), cast(NULL AS sql_timestamp)
FROM (
-- select * FROM (
  select b.fullname, a.*
  FROM partyprivileges a
  INNER JOIN  people b ON a.partyid = b.partyid
  WHERE a.privilege = 'Privilege_SC'
    AND a.thruts IS NULL 
    AND b.fullname NOT IN ( 'Mike Delohery', 'Larry Stadstad')
    AND locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'  
    AND NOT EXISTS (
      select 1
      FROM partyprivileges 
      WHERE partyid = a.partyid
        AND a.privilege = 'Privilege_SC'
        AND thruts IS NULL 
        AND locationid = '4CD8E72C-DC39-4544-B303-954C99176671')) x        