Larrry moving FROM sc-rydells to gm-crookston
-- Al Berry moving FROM sales mgr at ry1 to gm at ry3
SELECT * FROM users WHERE username = 'jwarmack'
Larry PartyID: '283F4131-4660-42AA-BEE5-C4EF193C799C'
warmack PartyID:  '8E998EAC-EFAD-4AFB-8998-2B67E4894AFD'
al berry: '903E7479-B6B9-4ACF-9D61-CE551A87EA86'

Locations
SELECT * FROM organizations WHERE fullname LIKE 'GF-%'
ry1: 'B6183892-C79D-4489-A58C-B526DF948B06'
ry2: '4CD8E72C-DC39-4544-B303-954C99176671'
ry3: '1B6768C6-03B0-4195-BA3B-767C0CAC40A6'

SELECT *
FROM PartyPrivileges
WHERE partyid = '8E998EAC-EFAD-4AFB-8998-2B67E4894AFD'

SELECT *
FROM PartyPrivileges
WHERE partyid = '903E7479-B6B9-4ACF-9D61-CE551A87EA86'

-- Parties
-- no changes
-- People
-- no changes

-- ApplicationUsers
-- ADD app carpic
larry
INSERT INTO applicationusers 
values('283F4131-4660-42AA-BEE5-C4EF193C799C', 'carpic', 2, 120, NULL, NULL, now(), NULL)
al
INSERT INTO applicationusers 
values('903E7479-B6B9-4ACF-9D61-CE551A87EA86', 'carpic', 2, 120, NULL, NULL, now(), NULL)

-- ContactMechanisms
larry
INSERT INTO ContactMechanisms
values('ContactMechanism_WorkEmail', '283F4131-4660-42AA-BEE5-C4EF193C799C', 'lhumble@crookstonmotors.com', now(), NULL)
al - no crookston email yet

-- Users
-- no changes

-- PartyRelationships
-- ADD MarketBuyers
larry
INSERT INTO PartyRelationships
values('A4847DFC-E00E-42E7-89EE-CD4368445A82', '283F4131-4660-42AA-BEE5-C4EF193C799C', 'PartyRelationship_MarketBuyers', now(), NULL) 
al
INSERT INTO PartyRelationships
values('A4847DFC-E00E-42E7-89EE-CD4368445A82', '903E7479-B6B9-4ACF-9D61-CE551A87EA86', 'PartyRelationship_MarketBuyers', now(), NULL) 

-- PartyPrivileges
-- CLOSE rydell SC
larry
UPDATE PartyPrivileges
SET ThruTS = now()
WHERE partyID = '283F4131-4660-42AA-BEE5-C4EF193C799C'
al
UPDATE PartyPrivileges
SET ThruTS = now()
WHERE partyID = '903E7479-B6B9-4ACF-9D61-CE551A87EA86'

-- ADD Crookston - AdjustRecon, WholesaleWithinMarket, SalesManage, InvMgr
larry
INSERT INTO PartyPrivileges values('283F4131-4660-42AA-BEE5-C4EF193C799C','1B6768C6-03B0-4195-BA3B-767C0CAC40A6','Privilege_AdjustRecon', now(), NULL);
INSERT INTO PartyPrivileges values('283F4131-4660-42AA-BEE5-C4EF193C799C','1B6768C6-03B0-4195-BA3B-767C0CAC40A6','Privilege_WholesaleWithinMarket', now(), NULL);
INSERT INTO PartyPrivileges values('283F4131-4660-42AA-BEE5-C4EF193C799C','1B6768C6-03B0-4195-BA3B-767C0CAC40A6','Privilege_SalesManager', now(), NULL);
INSERT INTO PartyPrivileges values('283F4131-4660-42AA-BEE5-C4EF193C799C','1B6768C6-03B0-4195-BA3B-767C0CAC40A6','Privilege_InvMgr', now(), NULL);

al
INSERT INTO PartyPrivileges values('903E7479-B6B9-4ACF-9D61-CE551A87EA86','1B6768C6-03B0-4195-BA3B-767C0CAC40A6','Privilege_AdjustRecon', now(), NULL);
INSERT INTO PartyPrivileges values('903E7479-B6B9-4ACF-9D61-CE551A87EA86','1B6768C6-03B0-4195-BA3B-767C0CAC40A6','Privilege_WholesaleWithinMarket', now(), NULL);
INSERT INTO PartyPrivileges values('903E7479-B6B9-4ACF-9D61-CE551A87EA86','1B6768C6-03B0-4195-BA3B-767C0CAC40A6','Privilege_SalesManager', now(), NULL);
INSERT INTO PartyPrivileges values('903E7479-B6B9-4ACF-9D61-CE551A87EA86','1B6768C6-03B0-4195-BA3B-767C0CAC40A6','Privilege_InvMgr', now(), NULL);

-- larry
-- back to ry1 AS a sc 
-- already added ry1 sc
-- remove ALL crookston privileges
SELECT * 
FROM partyprivileges
WHERE partyid = '283F4131-4660-42AA-BEE5-C4EF193C799C'

UPDATE partyprivileges
SET ThruTS = now()
WHERE partyid = '283F4131-4660-42AA-BEE5-C4EF193C799C'
AND locationid = '1B6768C6-03B0-4195-BA3B-767C0CAC40A6'

-- 1/2/13
al berry IS back at ry1
-- CLOSE out crookston
UPDATE PartyPrivileges
SET ThruTS = now()
WHERE partyID = '903E7479-B6B9-4ACF-9D61-CE551A87EA86';
-- ADD ry1 'Privilege_AdjustRecon'  'Privilege_SalesManager' 'Privilege_WholesaleWithin'
INSERT INTO PartyPrivileges values('903E7479-B6B9-4ACF-9D61-CE551A87EA86','B6183892-C79D-4489-A58C-B526DF948B06','Privilege_AdjustRecon', now(), NULL);
INSERT INTO PartyPrivileges values('903E7479-B6B9-4ACF-9D61-CE551A87EA86','B6183892-C79D-4489-A58C-B526DF948B06','Privilege_WholesaleWithinMarket', now(), NULL);
INSERT INTO PartyPrivileges values('903E7479-B6B9-4ACF-9D61-CE551A87EA86','B6183892-C79D-4489-A58C-B526DF948B06','Privilege_SalesManager', now(), NULL);


