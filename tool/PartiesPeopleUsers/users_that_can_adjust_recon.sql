8/23/22
cahalan wanted a list of people that can ADD recon

SELECT distinct b.fullname, d.term_date
FROM partyprivileges a
JOIN people b on a.partyid = b.partyid
JOIN organizations c on a.locationid = c.partyid
JOIN ukg.employees d on b.firstname = d.first_name AND b.lastname = d.last_name
  AND d.term_date > curdate()
WHERE a.privilege IN ('Privilege_AdjustRecon','Privilege_InvMgr')
  AND a.thruts IS null
