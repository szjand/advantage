/*
SELECT * FROM organizations ORDER BY fromts desc
SELECT * FROM parties
DELETE FROM parties WHERE partyid = 'd37fe3b4-0bbb-3543-a1f4-ff0818f777b8'

SELECT * FROM partyrelationships WHERE partyid2 = 'C3C3478E-A06B-4C2B-ABEF-5002A055EF2B'
*/

DECLARE @PartyID string;
DECLARE @FullName string;
DECLARE @NowTS timestamp;
DECLARE @Typ string;
DECLARE @Market string;
DECLARE @MarketID string;
DECLARE @PartyRelationshipTyp string;
// this IS the only data to be entered
@FullName = 'Adesa Seattle';
//
@Market = 'GF-Grand Forks';
@PartyID = (SELECT newidstring(d) FROM system.iota);
@NowTS = Now();
@Typ = 'Party_Organization';
@MarketID = (SELECT partyid FROM organizations WHERE fullname = @Market);
@PartyRelationshipTyp = 'PartyRelationship_MarketAuctions'; 

IF @FullName IS NULL OR @FullName = '' THEN
  RAISE PartyException( 101, 'Missing FullName');
END IF;
IF (SELECT COUNT(*) 
     FROM Organizations 
	 WHERE FullName = @FullName) <> 0 THEN
  RAISE PartyException(102, 'FullName already EXISTS IN Organizations');
END IF;
IF (SELECT COUNT(*)
     FROM TypDescriptions
     WHERE typ = @PartyRelationshipTyp) = 0 THEN 
  RAISE PartyException(103, 'PartyRelationshipTyp does NOT exist IN TypDescriptions');
END IF;      
BEGIN TRANSACTION;
TRY
  INSERT INTO parties VALUES(@PartyID, @Typ);
  INSERT INTO Organizations(PartyID, FullName, FromTS) 
    VALUES(@PartyID, @FullName, @NowTS);
  INSERT INTO PartyRelationships(PartyID1, PartyID2, Typ, FromTS)
    VALUES(@MarketID, @PartyID, @PartyRelationshipTyp, @NowTS);
COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END;
  


