SELECT p.fullname, o.name, pp.privilege, x.username, x.active
FROM partyprivileges pp
LEFT JOIN people p ON p.partyid = pp.partyid
LEFT JOIN organizations o ON o.PartyID = pp.LocationID
LEFT JOIN users x ON p.partyid = x.partyid
WHERE pp.ThruTS IS NULL
AND pp.Privilege = 'Privilege_SC'
ORDER BY o.name, p.fullname