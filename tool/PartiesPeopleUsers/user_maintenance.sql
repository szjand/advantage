python project tool/user_clean_up.py calls stored proc PROCEDURE user_cleanup(username)


-- never used
SELECT a.username
FROM users a
WHERE active = true
  AND a.lastaccess IS NULL
  
-- no access IN over a year  
SELECT a.username, 
  a.lastaccess, TIMESTAMPDIFF( SQL_TSI_MONTH, lastaccess, now() )
FROM users a
WHERE a.active = true  
  AND TIMESTAMPDIFF( SQL_TSI_MONTH, lastaccess, now() ) >= 12
  
-- NOT reliable  
SELECT a.username, b.lastname, b.firstname, c.termdate
FROM users a
LEFT JOIN people b on a.partyid = b.partyid
LEFT JOIN dds.edwEmployeeDim c on b.lastname = c.lastname
  AND b.firstname = c.firstname
  AND c.currentrow = true
WHERE a.active = true  
  AND coalesce(c.termdate, cast('12/31/9999' AS sql_date)) < curdate()
ORDER BY username  

-- down to 118
SELECT COUNT(*) FROM users WHERE active = true

-- bevs main concern was techs
SELECT a.username, b.lastname, b.firstname, c.distcode, c.storecode
FROM users a
inner JOIN people b on a.partyid = b.partyid
inner JOIN dds.edwEmployeeDim c on b.lastname = c.lastname
  AND b.firstname = c.firstname
  AND c.currentrow = true
  AND c.distcode = 'STEC'
WHERE a.active = true  
ORDER BY b.lastname  

SELECT trim(b.lastname) + ', ' + trim(b.firstname)
FROM users a
inner JOIN people b on a.partyid = b.partyid
inner JOIN dds.edwEmployeeDim c on b.lastname = c.lastname
  AND b.firstname = c.firstname
  AND c.currentrow = true
  AND c.distcode = 'STEC'
WHERE a.active = true  
ORDER BY b.lastname  