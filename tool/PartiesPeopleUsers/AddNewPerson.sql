-- Parties, People, ContactMechanisms
DECLARE @NewpartyID string;
DECLARE @Firstname string;
DECLARE @Lastname string;
DECLARE @Fullname string;
DECLARE @email string;
DECLARE @Now timestamp;
DECLARE @COUNT integer;

@NewpartyID = newidstring(D);
@Now = Now();
@Firstname = 'Kevin';
@Lastname = 'Hanson';
@Fullname = TRIM(@FirstName) + ' ' + TRIM(@LastName);
@email = '';

BEGIN TRANSACTION; 
TRY
  IF @Firstname IS NULL OR @Firstname = '' THEN
    RAISE PartyException( 101, 'Missing first name');
  END IF;
  IF @Lastname IS NULL OR @Lastname = '' THEN
    RAISE PartyException( 102, 'Missing last name');
  END IF;  
  @COUNT = (SELECT COUNT(*) FROM People WHERE fullname = @Fullname);
  IF @COUNT <> 0 then
    RAISE PartyException(103, 'This user fullname already exists');
  END IF; 
  INSERT INTO Parties 
    VALUES(@NewpartyID, 'Party_Person');
  INSERT INTO People(PartyID, Fullname, Firstname, Lastname, FromTS)
    VALUES(@NewPartyID, @Fullname, @Firstname, @Lastname, @Now);     
  IF @email IS NOT NULL AND @email <> '' THEN
    INSERT INTO ContactMechanisms (Typ, PartyID, Description, FromTS)
	  VALUES('ContactMechanism_WorkEmail', @NewpartyID, 'ContactMechanism_WorkEmail', @Now);
  END IF;
COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END;
