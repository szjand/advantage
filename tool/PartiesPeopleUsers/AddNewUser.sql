-- 'GF-Crookston'
-- 'GF-Honda Cartiva'
-- 'GF-Rydells'
-- Users, ApplicationUsers
DECLARE @Username string;
DECLARE @Marketname string;
DECLARE @MarketID string;
DECLARE @PartyID string;
DECLARE @Fullname string;
DECLARE @NowTS timestamp;

@Fullname = 'Kevin Hanson';
  IF @Fullname IS NULL OR @Fullname = '' THEN
    RAISE UserException( 101, 'Missing full name');
  END IF;
@Username = 'khanson';
  IF @Username IS NULL OR @Username = '' THEN
    RAISE UserException( 102, 'Missing user name');
  END IF;
  IF (SELECT PartyID FROM users WHERE username = @Username) IS NOT NULL THEN 
    RAISE UserException( 103, 'Username already exists'); 
  END IF;    
@Marketname = 'GF-Rydells';
  IF @Marketname IS NULL OR @Marketname = '' THEN
    RAISE UserException( 103, 'Missing market name');
  END IF; 
@PartyID = (SELECT PartyID FROM People WHERE fullname = @Fullname);
  IF @PartyID IS NULL THEN
    RAISE UserException( 104, @Fullname + ' does not exist in People');
  END IF; 
@MarketID = (SELECT PartyId FROM Organizations WHERE fullname = @Marketname);
  IF @MarketID IS NULL THEN
    RAISE UserException( 105, 'Market name does not decode to an existing organization');
  END IF;   
@NowTS = Now();

BEGIN TRANSACTION; 
TRY
  INSERT INTO Users (PartyID, UserName, Password, DefaultMarketID, TimeOutMinutes, Active)
    VALUES(@PartyID, @Username, 'password', @MarketID, 120, True);
  INSERT INTO ApplicationUsers(PartyID, AppName, AppSecurityLevel, AppTimeOutMinutes, FromTS)
    VALUES(@PartyID, 'portalvseries', 2, 120, @NowTS);
  INSERT INTO ApplicationUsers(PartyID, AppName, AppSecurityLevel, AppTimeOutMinutes, FromTS)
    VALUES(@PartyID, 'inventory', 2, 120, @NowTS);	
COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END;
