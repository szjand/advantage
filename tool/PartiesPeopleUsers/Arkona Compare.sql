create TABLE peopletest (
  [ymco#] cichar(3),
  ymname cichar(45), 
  ymempn cichar(12),
  ymactv cichar(1))
  
SELECT left(p.fullname, 30), u.username, u.password, u.active, pt.*
FROM people p
LEFT JOIN users u ON p.partyid = u.partyid
LEFT JOIN peopletest pt ON p.lastname = LEFT(ymname, position(',' IN ymname)-1)
WHERE (
  (u.active = true AND ymactv <> 'A')
  OR
  (u.active = false AND ymactv = 'A'))


SELECT LEFT(ymname, position(',' IN ymname)-1), pt.*
INTO #test
FROM peopletest pt


select *
FROM peopletest
WHERE position(',' IN ymname) = 0

DELETE 
FROM peopletest
WHERE length(ymname) < 5