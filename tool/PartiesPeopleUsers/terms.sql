-- 7/13/15, mike tweten
-- don't need to DO anything with the user TABLE, just remove ALL the below
-- AND we are good to go
-- SELECT * FROM people WHERE lastname = 'karaba'

SELECT * FROM users WHERE username LIKE '%taubol%'  -- 48bc88ff-5687-3340-999d-47ddf2d7e037

-- 2/9/23, instead of deleting FROM users, SET active = false

DECLARE @PartyID string;
@PartyID = (SELECT partyid FROM users WHERE username = 'taubol');

UPDATE partyRelationships
SET thruts = now()
WHERE partyid2 = @PartyID
  AND thruts IS null;
  
UPDATE partyprivileges 
SET thruts = now()
WHERE partyid = @PartyID
  AND thruts IS NULL;
  
UPDATE applicationusers
SET thruts = now()
WHERE partyid = @PartyID
  AND thruts IS NULL;
	
-- DELETE FROM users where partyid = @PartyID;
UPDATE users
SET active = false
WHERE partyid = @PartyID;



-- the DELETE everything approach
DECLARE @PartyID string;
@PartyID = (SELECT partyid FROM users WHERE username = 'klabelle');
delete FROM partyprivileges WHERE partyid = @PartyID;
DELETE FROM partyRelationships WHERE partyid2 = @PartyID;
DELETE FROM applicationusers WHERE partyid = @PartyID;
DELETE FROM users WHERE  partyid = @PartyID;
DELETE FROM people WHERE partyid = @PartyID;
DELETE FROM parties WHERE partyid = @PartyID;