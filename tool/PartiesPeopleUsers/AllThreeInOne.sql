/*
'GF-Crookston'
'GF-Honda Cartiva'
'GF-Rydells'
'GF-Toyota'
'GF-Rydell Auto Outlet'
'Privilege_Internet'
'Privilege_InvMgr'
'Privilege_Recon'
'Privilege_SC'
'Privilege_SalesManager'
'Privilege_AdjustRecon'
'Privilege_Biller'
'Privilege_WholesaleWithinMarket'
Market: 'GF-Grand Forks' 
*/


DECLARE @NewpartyID string;
DECLARE @Firstname string;
DECLARE @Lastname string;
DECLARE @Fullname string;
DECLARE @email string;
DECLARE @NowTS timestamp;
DECLARE @COUNT integer;
DECLARE @Username string;
DECLARE @Marketname string;
DECLARE @MarketID string;
DECLARE @Location string;
DECLARE @LocationID string;
DECLARE @Privilege string;

@NewpartyID = newidstring(D);
@Firstname = 'Kristi';
@Lastname = 'Goulet';
@Fullname = 'Kristi Goulet';
@email = ''; 
@Username = 'kgoulet';
@Marketname = 'GF-Grand Forks';
@Location = 'GF-Honda Cartiva';
@Privilege = 'Privilege_Biller';
@MarketID = (SELECT PartyId FROM Organizations WHERE fullname = @Marketname);
@LocationID = (SELECT PartyID FROM Organizations WHERE fullname = @Location);
@NowTS = Now();

  IF @Firstname IS NULL OR @Firstname = '' THEN
    RAISE PartyException( 101, 'Missing first name');
  END IF;
  IF @Lastname IS NULL OR @Lastname = '' THEN
    RAISE PartyException( 102, 'Missing last name');
  END IF;  
  IF @Fullname IS NULL OR @Fullname = '' THEN
    RAISE UserException( 103, 'Missing full name');
  END IF;
  IF @Fullname <> TRIM(@FirstName) + ' ' + TRIM(@LastName) THEN
    RAISE UserException(104, 'FullName does not match First & Last');
  END IF;
  IF UPPER(@UserName) <> UPPER(LEFT(TRIM(@FirstName),1) + TRIM(@LastName)) THEN 
    RAISE UserException(115, 'UserName NOT first initial AND last name');
  END IF;
  IF @Username IS NULL OR @Username = '' THEN
    RAISE UserException( 105, 'Missing user name');
  END IF; 
  IF @Marketname IS NULL OR @Marketname = '' THEN
    RAISE UserException( 106, 'Missing market name');
  END IF;
  IF @Location IS NULL OR @Location = '' THEN
    RAISE UserException(107, 'Missing Location');
  END IF;  
  IF (SELECT fullname FROM People WHERE fullname = @Fullname) IS NOT NULL THEN
    RAISE PartyException(109, 'This person''s fullname already exists');
  END IF;    
  IF (SELECT username FROM users WHERE username = @Username) IS NOT NULL THEN 
    RAISE UserException( 110, 'Username already exists'); 
  END IF; 
  IF (SELECT Description FROM ContactMechanisms WHERE Description = @email) IS NOT NULL THEN
    RAISE UserException(112, 'This email is already in use');
  END IF;
     
  IF @MarketID IS NULL THEN
    RAISE UserException( 113, 'Market name does not decode to an existing organization');
  END IF;  
  IF @LocationID IS NULL THEN
    RAISE UserException( 114, 'Location name does not decode to an existing organization');
  END IF; 
  
BEGIN TRANSACTION; 
TRY
  INSERT INTO Parties 
    VALUES(@NewpartyID, 'Party_Person');
    
  INSERT INTO People(PartyID, Fullname, Firstname, Lastname, FromTS)
    VALUES(@NewPartyID, @Fullname, @Firstname, @Lastname, @NowTS);  
       
  IF @email IS NOT NULL AND @email <> '' THEN
    INSERT INTO ContactMechanisms (Typ, PartyID, Description, FromTS)
	  VALUES('ContactMechanism_WorkEmail', @NewpartyID, @email, @NowTS);
  END IF;
  
  INSERT INTO Users (PartyID, UserName, Password, DefaultMarketID, TimeOutMinutes, Active)
    VALUES(@NewPartyID, @Username, 'password', @MarketID, 120, True);
    
  INSERT INTO ApplicationUsers(PartyID, AppName, AppSecurityLevel, AppTimeOutMinutes, FromTS)
    VALUES(@NewPartyID, 'portalvseries', 2, 120, @NowTS);
  INSERT INTO ApplicationUsers(PartyID, AppName, AppSecurityLevel, AppTimeOutMinutes, FromTS)
    VALUES(@NewPartyID, 'inventory', 2, 120, @NowTS);	
    
  INSERT INTO PartyRelationShips (PartyID1, PartyID2, Typ, FromTS)
    VALUES(@MarketID, @NewPartyID, 'PartyRelationship_MarketToolUsers', @NowTS); 
    
  INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
    VALUES (@NewpartyID, @LocationID, @Privilege, @NowTS);
  COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END;  
