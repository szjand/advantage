-- without the repeats FROM the 1 to many
-- partyrelationship AND market
-- location: privileges
/*
SELECT * FROM partyrelationships
SELECT * FROM partyprivileges
SELECT * FROM applicationusers
select * FROM TypDescriptions WHERE typ = 'PartyRelationship_MarketToolUsers'
thinking nested cursors for location-privilege
for each location, what are the privileges

need applications FROM applicationusers
*/
DECLARE @lastname string;
DECLARE @PrivilegeList string;
DECLARE @PartyRelationshipList string;
DECLARE @PartyID string;
DECLARE @PRMarket string;
DECLARE @PRListCur CURSOR AS
  SELECT typ
  FROM PartyRelationships
  WHERE PartyID2 = @PartyID;
  
DECLARE @LocationID string;  
DECLARE @LocationCur CURSOR AS
  SELECT name
  FROM Organizations
  WHERE partyid IN (
    SELECT locationid
    FROM partyprivileges
    WHERE PartyID = @PartyID);
    
DECLARE @AppList string;
DECLARE @AppListCur CURSOR AS
  SELECT AppName
  FROM ApplicationUsers
  WHERE PartyID = @PartyID;    

@lastname = 'wilkening';
-- lastname could be multiples, so this needs to be fixed
@PartyID = (SELECT partyid FROM people WHERE lastname = @lastname);
@PRMarket = (
  SELECT name
  FROM Organizations
  WHERE PartyID in (
    SELECT PartyID1
    FROM PartyRelationships
    WHERE PartyID2 = @PartyID));

OPEN @AppListCur;
TRY
  @AppList = '';
  WHILE FETCH @AppListCur DO
    @AppList = @AppList + TRIM(@AppListCur.AppName) + ' - ' ;
  END WHILE;
FINALLY
  CLOSE @AppListCur;
END;    
OPEN @PRListCur;
TRY
  @PartyRelationshipList = '';
  WHILE FETCH @PRListCur DO 
    @PartyRelationshipList = TRIM(@PartyRelationshipList) + (
      select trim(Description)
      from TypDescriptions
      where typ = @PRListCur.typ) + ' - ';
  END WHILE;
FINALLY
  CLOSE @PRListCur;
END;      
  
SELECT distinct p.PartyID, left(pe.fullname, 25) AS name, left(cm.Description, 25) AS email, 
  u.username, u.password, 
  LEFT(@PRMarket, 25) AS Market,
  @PartyRelationshipList AS MarketPartyRel,
  @AppList AS AppUserApps
/*
  left(au.AppName, 25) AS AppName,
  left(pp.Privilege, 25) AS Privilege, left(pr.Typ, 35) AS PartyRel,
  (SELECT name FROM organizations WHERE partyid = pp.locationid) AS location
*/  
FROM Parties p
LEFT JOIN People pe ON pe.PartyID = p.PartyID
LEFT JOIN ContactMechanisms cm ON cm.PartyID = p.PartyID AND cm.typ = 'ContactMechanism_WorkEmail'
LEFT JOIN users u ON u.partyid = p.partyid
LEFT JOIN applicationusers au ON au.partyid = u.partyid
LEFT JOIN PartyPrivileges pp ON pp.partyid = p.partyid
LEFT JOIN PartyRelationships pr ON pr.partyid2 = p.partyid
WHERE pe.lastname = @lastname

