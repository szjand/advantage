-- 7/10 -- generalize the move sp ---------------------------------------------
-- first, basic GET
ALTER PROCEDURE dwbGetCustomerPayJobs(
  Misc cichar (25) output,
  Customer cichar(40) output,
  Vehicle cichar(80) output,
  Job memo output)
BEGIN
/*
EXECUTE PROCEDURE dwbGetCustomerPayJobs();
*/
INSERT INTO __output
SELECT *
FROM (
  SELECT left(b.dayname, 3) + ' ' + b.mmdd + ' ' + substring(cast(startTS as sql_char), 12,5) collate ads_default_ci AS Appointment, 
    customer, 
    CASE 
      WHEN color = '' THEN TRIM(make) + ' ' + TRIM(model)
      ELSE TRIM(color) + ' ' + TRIM(model)
    END AS vehicle,
    JobDescription
  FROM dds.SchedulerDetailAppointments a
  LEFT JOIN dds.day b ON CAST(startts AS sql_date) = b.thedate) c
WHERE NOT EXISTS (
  SELECT 1
  FROM DetailWhiteboard
  WHERE customer = c.customer
    AND vehicle = c.vehicle);
END;

ALTER PROCEDURE dwbGetReconBufferJobs(  
  Misc cichar (25) output,
  Customer cichar(40) output,
  Vehicle cichar(80) output,
  Job memo output)
BEGIN
/*
EXECUTE PROCEDURE dwbGetReconBufferJobs();
*/
INSERT INTO __output
SELECT *
FROM (
  SELECT cast(CurrentPriority AS sql_char), stocknumber AS Customer, 
    CASE
      WHEN color IS NULL THEN left(TRIM(make) + ' ' + TRIM(model), 60)
      ELSE left(TRIM(color) + ' ' + TRIM(model), 60) 
    END AS Vehicle,
    app AS Job
  FROM vusedcarsreconbuffer
  WHERE status = 'AppearanceReconDispatched_Dispatched') c
WHERE NOT EXISTS (
  SELECT 1
  FROM DetailWhiteboard
  WHERE customer = c.customer
    AND vehicle = c.vehicle);
END;

alter PROCEDURE dwbGetRawMaterialsJobs(  
  Misc cichar (25) output,
  Customer cichar(40) output,
  Vehicle cichar(80) output,
  Job memo output)
BEGIN
/*
EXECUTE PROCEDURE dwbGetRawMaterialsJobs();
*/
INSERT INTO __output
SELECT *
FROM (
  SELECT cast(Priority AS sql_char), stocknumber AS Customer, 
    CASE
      WHEN color IS NULL THEN left(TRIM(make) + ' ' + TRIM(model), 60)
      ELSE left(TRIM(color) + ' ' + TRIM(model), 60) 
    END AS Vehicle,
    detail AS Job
  FROM vusedcarsAppearanceG2G a
  WHERE NOT EXISTS (
    SELECT 1
    FROM vusedcarsreconbuffer
    WHERE status = 'AppearanceReconDispatched_Dispatched'
      AND stocknumber = a.stocknumber)) c
WHERE NOT EXISTS (
  SELECT 1
  FROM DetailWhiteboard
  WHERE customer = c.customer
    AND vehicle = c.vehicle);
END;





DROP TABLE DetailWhiteboard;
CREATE TABLE DetailWhiteboard ( 
      Customer CIChar( 40 ),
      Vehicle CIChar( 80 ),
      Job CIChar( 250 ),
      JobSource CIChar( 40 ),
      DateComplete Date,
      Seq Integer) IN DATABASE;
  
ALTER PROCEDURE dwbGetWhiteBoard(  
  Customer cichar(40) output,
  Vehicle cichar(80) output,
  Job memo output)
BEGIN
/*
EXECUTE PROCEDURE dwbGetWhiteBoard();
*/
INSERT INTO __output
SELECT Customer,Vehicle,Job
FROM DetailWhiteboard
WHERE DateComplete = '12/31/9999';
END;  

alter PROCEDURE dwbGetComplete(  
  Customer cichar(40) output,
  Vehicle cichar(80) output,
  Job memo output)
BEGIN
/*
EXECUTE PROCEDURE dwbGetComplete();
*/
INSERT INTO __output
SELECT Customer,Vehicle,Job
FROM DetailWhiteboard
WHERE DateComplete <> '12/31/9999';
END;  
--DROP PROCEDURE dwbMoveJobnew;
alter PROCEDURE dwbMoveJob(Customer CICHAR (40),Vehicle CICHAR (80),MoveFrom CICHAR (40),ForwardFlag CICHAR (5)) 
BEGIN 
/*
EXECUTE PROCEDURE dwbgetwip()
EXECUTE PROCEDURE dwbgetrawmaterialsjobs()
EXECUTE PROCEDURE dwbgetreconbufferjobs()
EXECUTE PROCEDURE dwbgetcustomerpayjobs()

-- to whiteboard
EXECUTE PROCEDURE dwbMoveJob('20180XX','silver Yukon','rawmaterialsjobs', 'False');
-- back resource
EXECUTE PROCEDURE dwbMoveJob('20180XX','silver Yukon','whiteboard', 'False');
-- to wip
EXECUTE PROCEDURE dwbMoveJob('20180XX','silver Yukon','whiteboard', 'True');
-- back to whiteboard
EXECUTE PROCEDURE dwbMoveJob('20180XX','silver Yukon','wip', 'false');
-- to complete
EXECUTE PROCEDURE dwbMoveJob('20180XX','silver Yukon','wip', 'True');
-- back to wip
EXECUTE PROCEDURE dwbMoveJob('20180XX','silver Yukon','complete', 'True');

initially the resourses were CamelCased, AND even though every fucking thing IS
defined AS cichar, php was sending lower CASE resources AND the match IN the
sp was failing.
*/
DECLARE @Customer cichar(40);
DECLARE @Vehicle cichar(80);
DECLARE @MoveFrom cichar(40);
DECLARE @ForwardFlag cichar(5);
@Customer = (SELECT Customer FROM __input);
@Vehicle = (SELECT Vehicle FROM __input);
@MoveFrom = (SELECT lcase(MoveFrom) FROM __input);
@ForwardFlag = (SELECT lcase(ForwardFlag) FROM __input);
IF @MoveFrom = 'reconbufferjobs' THEN 
  INSERT INTO DetailWhiteBoard (customer,vehicle,job,jobsource,location) 
  SELECT *
  FROM (
    SELECT stocknumber AS Customer, 
      CASE
        WHEN color IS NULL THEN left(TRIM(make) + ' ' + TRIM(model), 60)
        ELSE left(TRIM(color) + ' ' + TRIM(model), 60) 
      END AS Vehicle,
      app AS Job, @MoveFrom,PhysLocation
    FROM vusedcarsreconbuffer
    WHERE status = 'AppearanceReconDispatched_Dispatched') a
  WHERE Customer = @Customer
    AND Vehicle = @Vehicle;       
ELSEIF @MoveFrom = 'rawmaterialsjobs' THEN     
  INSERT INTO DetailWhiteBoard (customer,vehicle,job,jobsource,location)  
  SELECT *
  FROM (
    SELECT stocknumber AS Customer, 
      CASE
        WHEN color IS NULL THEN left(TRIM(make) + ' ' + TRIM(model), 60)
        ELSE left(TRIM(color) + ' ' + TRIM(model), 60) 
      END AS Vehicle,
      detail AS Job, @MoveFrom,PhysLocation 
    FROM vusedcarsAppearanceG2G a
    WHERE NOT EXISTS (
      SELECT 1
      FROM vusedcarsreconbuffer
      WHERE status = 'AppearanceReconDispatched_Dispatched'
        AND stocknumber = a.stocknumber)) b
  WHERE Customer = @Customer
    AND Vehicle = @Vehicle;         
ELSEIF @MoveFrom = 'customerpayjobs' THEN   
  INSERT INTO DetailWhiteBoard (customer,vehicle,job,jobsource,location) 
  SELECT *
  FROM (  
    SELECT customer, 
      CASE 
        WHEN color = '' THEN TRIM(make) + ' ' + TRIM(model)
        ELSE TRIM(color) + ' ' + TRIM(model)
      END AS vehicle,
      JobDescription, @MoveFrom, 'Customer Pay' 
    FROM dds.SchedulerDetailAppointments) a  
  WHERE Customer = @Customer
    AND Vehicle = @Vehicle; 
ELSEIF @MoveFrom = 'whiteboard' THEN 
  IF @ForwardFlag = 'true' THEN -- move to complete
    UPDATE DetailWhiteboard
    SET DateWip = curdate()
    WHERE Customer = @Customer
      AND Vehicle = @Vehicle;
  ELSE 
    DELETE FROM DetailWhiteboard -- make visible IN whiteboard
    WHERE Customer = @Customer
      AND Vehicle = @Vehicle;
  ENDIF; 
ELSEIF @MoveFrom = 'wip' THEN 
  IF @ForwardFlag = 'true' THEN -- move to complete
    UPDATE DetailWhiteboard
    SET DateComplete = curdate(),
        DateWip = '12/31/9999'
    WHERE Customer = @Customer
      AND Vehicle = @Vehicle;
  ELSE -- make visible IN whiteboard
    UPDATE DetailWhiteboard
    SET DateWip = '12/31/9999'
    WHERE Customer = @Customer
      AND Vehicle = @Vehicle;
  ENDIF;   
ELSEIF @MoveFrom = 'complete' THEN 
  UPDATE DetailWhiteboard -- make visible IN wip
  SET DateComplete = '12/31/9999',
      DateWip = curdate()
  WHERE Customer = @Customer
    AND Vehicle = @Vehicle;   
ENDIF;    



END;

-- 7/15
-- using the view IS way too fucking slow
-- DO the query
-- first buffer


EXECUTE PROCEDURE dwbGetReconBufferJobs();
-- great
SELECT *
FROM (
  SELECT cast(b.CurrentPriority AS sql_char), b.stocknumber AS Customer, 
    CASE
      WHEN c.exteriorcolor IS NULL THEN left(TRIM(c.make) + ' ' + TRIM(c.model), 60)
      ELSE left(TRIM(exteriorcolor) + ' ' + TRIM(c.model), 60) 
    END AS Vehicle,
    CASE
      WHEN f.typ = 'PartyCollection_AppearanceReconItem' THEN 
        CASE
          WHEN upper(f.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
          WHEN upper(f.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
          WHEN (upper(f.description) LIKE 'DELUXE%' OR upper(f.description) LIKE 'AUCTION%') THEN 'Deluxe'
          WHEN upper(f.Description) = 'PRICING RINSE' THEN 'PR' 
          ELSE 'WTF' 
        END
      ELSE 'Other'
    END AS Detail    
  FROM VehicleInventoryItemStatuses a
  LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  LEFT JOIN VehicleItems c ON b.VehicleItemID = c.VehicleItemID 
  LEFT JOIN reconauthorizations d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.thruts IS NULL 
  LEFT JOIN AuthorizedReconItems e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
    AND e.startts IS NULL 
  LEFT JOIN VehicleReconItems f ON e.VehicleReconItemID = f.VehicleReconItemID   
  WHERE a.status = 'AppearanceReconDispatched_Dispatched'
    AND a.thruts IS NULL 
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
      AND status = 'AppearanceReconProcess_InProcess'
      AND ThruTS IS NULL)) g
WHERE NOT EXISTS (
  SELECT 1
  FROM DetailWhiteboard
  WHERE customer = g.customer
    AND vehicle = g.vehicle); 
    
-- now the G2G
SELECT *
FROM (
  SELECT cast(a.CurrentPriority AS sql_char), a.stocknumber AS Customer, 
    CASE
      WHEN e.exteriorcolor IS NULL THEN left(TRIM(e.make) + ' ' + TRIM(e.model), 60)
      ELSE left(TRIM(e.exteriorcolor) + ' ' + TRIM(e.model), 60) 
    END AS Vehicle,
    CASE
      WHEN h.typ = 'PartyCollection_AppearanceReconItem' THEN 
        CASE
          WHEN upper(h.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
          WHEN upper(h.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
          WHEN (upper(h.description) LIKE 'DELUXE%' OR upper(h.description) LIKE 'AUCTION%') THEN 'Deluxe'
          WHEN upper(h.Description) = 'PRICING RINSE' THEN 'PR' 
          ELSE 'WTF' 
        END
      ELSE 'Other'
    END AS Detail  
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.status = 'BodyReconProcess_NoIncompleteReconItems'
    AND b.thruts IS NULL 
  INNER JOIN VehicleInventoryItemStatuses c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.status = 'MechanicalReconProcess_NoIncompleteReconItems'
    AND c.thruts IS NULL   
  INNER JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.status = 'AppearanceReconProcess_NotStarted'
      AND d.thruts IS NULL  
  LEFT JOIN VehicleItems e ON a.VehicleItemID = e.VehicleItemID 
  LEFT JOIN reconauthorizations f ON a.VehicleInventoryItemID = f.VehicleInventoryItemID 
    AND f.thruts IS NULL 
  LEFT JOIN AuthorizedReconItems g ON f.ReconAuthorizationID = g.ReconAuthorizationID   
    AND g.startts IS NULL 
  LEFT JOIN VehicleReconItems h ON g.VehicleReconItemID = h.VehicleReconItemID         
  WHERE a.thruts IS NULL    
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
        AND status = 'AppearanceReconDispatched_Dispatched'
        AND ThruTS IS NULL)) i  
WHERE NOT EXISTS (
  SELECT 1
  FROM DetailWhiteboard
  WHERE customer = i.customer
    AND vehicle = i.vehicle);       

-- rawmaterials, tried using VehicleInventoryItemStatuses AS base rather than VehicleInventoryItems 
-- performs identically    
SELECT *
FROM (    
  SELECT cast(d.CurrentPriority AS sql_char), d.stocknumber AS Customer, 
    CASE
      WHEN e.exteriorcolor IS NULL THEN left(TRIM(e.make) + ' ' + TRIM(e.model), 60)
      ELSE left(TRIM(e.exteriorcolor) + ' ' + TRIM(e.model), 60) 
    END AS Vehicle,
    CASE
      WHEN h.typ = 'PartyCollection_AppearanceReconItem' THEN 
        CASE
          WHEN upper(h.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
          WHEN upper(h.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
          WHEN (upper(h.description) LIKE 'DELUXE%' OR upper(h.description) LIKE 'AUCTION%') THEN 'Deluxe'
          WHEN upper(h.Description) = 'PRICING RINSE' THEN 'PR' 
          ELSE 'WTF' 
        END
      ELSE 'Other'
    END AS Detail 
  FROM VehicleInventoryItemStatuses a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.status = 'BodyReconProcess_NoIncompleteReconItems'
    AND b.thruts IS NULL 
  INNER JOIN VehicleInventoryItemStatuses c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.status = 'MechanicalReconProcess_NoIncompleteReconItems'
    AND c.thruts IS NULL  
  LEFT JOIN VehicleInventoryItems d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  LEFT JOIN VehicleItems e ON d.VehicleItemID = e.VehicleItemID 
  LEFT JOIN reconauthorizations f ON a.VehicleInventoryItemID = f.VehicleInventoryItemID 
    AND f.thruts IS NULL 
  LEFT JOIN AuthorizedReconItems g ON f.ReconAuthorizationID = g.ReconAuthorizationID   
    AND g.startts IS NULL 
  LEFT JOIN VehicleReconItems h ON g.VehicleReconItemID = h.VehicleReconItemID     
  WHERE a.status = 'AppearanceReconProcess_NotStarted'
    AND a.ThruTS IS NULL  
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
        AND status = 'AppearanceReconDispatched_Dispatched'
        AND ThruTS IS NULL)) i   
WHERE NOT EXISTS (
  SELECT 1
  FROM DetailWhiteboard
  WHERE customer = i.customer
    AND vehicle = i.vehicle);          