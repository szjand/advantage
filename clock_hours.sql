-- DROP TABLE clock_hours;
CREATE TABLE clock_hours ( 
      employee_number CIChar( 7 ),
      the_date Date,
      clock_hours Numeric( 6 ,2 ),
      reg_hours Numeric( 6 ,2 ),
      ot_hours Numeric( 6 ,2 ),
      vac_hours Numeric( 6 ,2 ),
      pto_hours Numeric( 6 ,2 ),
      hol_hours Numeric( 6 ,2 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'clock_hours',
   'clock_hours.adi',
   'PK',
   'the_date;employee_number',
   '',
   2051,
   512,
   '' ); 

INSERT INTO clock_hours
SELECT b.employeenumber, c.thedate, a.clockhours, a.regularhours,a.overtimehours,
  a.vacationhours, a.ptohours, a.holidayhours
FROM dds.edwClockHoursFact a
JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
JOIN dds.day c on a.datekey =  c.datekey 
  WHERE c.thedate > '12/31/2020'
	
SELECT * FROM clock_hours
WHERE the_date = '09/13/2021'	


