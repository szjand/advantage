/*
07/16/21
david gonzalez (DAG), eric thompson (ERT), were wrong IN drivecentric, couple of 
deals with DGO AND ETH got thru

08/08/22
  excluded christie johnson with a bad RY1 sales id for deal 73067, record IN bopmast IS
	incomplete, but the customer name on 73067 IS the same for H15680, DARREL STALHEIM
*/
SELECT *
FROM (
  SELECT bqco#, bqspid
  FROM tmpBOPSLSS
  WHERE bqspid NOT IN ( 'HAL', 'ENT', 'WWW', '000', 'KUE', 'KTK', 'HAR', 'KLA','JLS','BSB',
 'NDO','FDC', 'EJO', '124', 'BYO', 'NLA', 'NKO', 'JUB', 'TRO','BHI','RTU','BDA',
 'SAR','BED','KBU','DDU','EGU','CTH','JLO','EHA','ZRO','AOW','FPA','SCA','BAL','MMC','KCH',
 'DGO','ETH','GBE')
  -- *a*
    AND bqkey NOT IN (50887, 50866, 15984, 15990, 50968, 50955, 73067) 
  GROUP BY bqco#, bqspid) a
LEFT JOIN dimSalesPerson b on a.bqco# = b.storecode
  AND a.bqspid = b.salesPersonID
WHERE b.storecode IS NULL;     


-- sc
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)
VALUES (
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'KSC', '198373', 
  'Kody Schlenvogt', 'Kody', 'Schlenvogt', 'Kody Schlenvogt', 'S',
  'Consultant', True); 

SELECT * FROM dimsalesperson WHERE firstname = 'nicholas' AND lastname = 'michael'

-- FI
INSERT INTO dimSalesPerson (salesPersonKey, storecode, salespersonID, employeenumber, 
  description, firstname, lastname, fullname, salespersontypecode,
  salespersontype, active)    
SELECT 
  (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY1', 'SMG', trim(employee_Number), trim(trim(last_name) + ', ' + first_name),  first_name, last_name, 
  TRIM(first_name) + ' ' + last_name, 'F','F/I Mgr', true
FROM ukg.employees
WHERE employee_number = '189768'

SELECT * FROM dimsalesperson WHERE lastname = 'trottier'
SELECT * FROM ukg.employees WHERE last_name = 'trottier'
SELECT * FROM tmpbopslss WHERE bqspid = 'SMG'
 
SELECT * FROM stgArkonaBOPSLSS WHERE bqspid in('wha') AND bqco# = 'ry1'

SELECT * FROM stgArkonaBOPSLSS WHERE bqspid = 'gbe' AND bqco# = 'RY2'

SELECT * FROM stgArkonaBOPMAST WHERE bmkey IN (73566)
SELECT * FROM stgArkonaBOPMAST WHERE bmsnam = 'STALHEIM, DARREL'
SELECT * FROM dimsalesperson WHERE lastname = 'callahan'

SELECT * FROM dimsalesperson WHERE employeenumber = '1147810'

SELECT * FROM dimsalesperson WHERE salespersonid = 'JW'

SELECT * FROM dimsalesperson ORDER BY lastname

UPDATE dimsalesperson
SET salespersontypecode = 'F', salespersontype = 'F/I Mgr'
WHERE storecode = 'RY2'
  AND salespersonid = 'JW'

UPDATE dimsalesperson
SET employeenumber = '115883'
WHERE salespersonkey = 392

select * FROM dimsalesperson WHERE lastname = 'halldorson'

SELECT * FROM dimsalesperson ORDER BY employeenumber

-- james "alex" waldeck needs an RY2 id
INSERT INTO dimsalesperson(salespersonkey,storecode,salespersonid,
  employeenumber,description, firstname,lastname,fullname,salespersontypecode,
  salespersontype,active)
SELECT (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2',salespersonid,employeenumber,
  description, firstname,lastname,fullname,salespersontypecode,
  salespersontype,active 
FROM dimsalesperson WHERE employeenumber = '165989';

-- dylan haley needs an RY2 FI id
INSERT INTO dimsalesperson(salespersonkey,storecode,salespersonid,
  employeenumber,description, firstname,lastname,fullname,salespersontypecode,
  salespersontype,active)
SELECT (SELECT MAX(salespersonkey) + 1 FROM dimsalesperson),
  'RY2','DYH',employeenumber,
  description, firstname,lastname,fullname,'F',
  'F/I Mgr',active 
FROM dimsalesperson WHERE employeenumber = '163700'
  AND storecode = 'RY2';