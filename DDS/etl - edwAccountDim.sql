SELECT account, description, gldescription, accounttype, gldepartmentcode, gmfsaccount

FROM edwAccountDim
WHERE gmfsaccount IN ('225','660a','667','669','242','247a','324','460a','467','469')

SELECT account, description, gldescription, accounttype, gldepartmentcode, gmfsaccount
FROM edwAccountDim
WHERE gmfsaccount in('460a', '660a')

SELECT account, description, gldescription, accounttype, gldepartmentcode, gmfsaccount
FROM edwAccountDim
WHERE description LIKE '%Labor%'
  AND accounttype = '4'
ORDER BY gldepartmentcode, account  

SELECT account, description, gldescription, accounttype, gldepartmentcode, gmfsaccount
FROM edwAccountDim
WHERE gldescription LIKE 'WIP%'
WHERE gmfsaccount = '247a'

SELECT gldescription
FROM edwaccountdim
ORDER BY gldescription

SELECT thedate, j.code, j.description, SUM(amount)
FROM day d
INNER JOIN edwAccountingFact f ON d.datekey = f.posteddatekey
INNER JOIN edwJournalDim j ON f.JournalKey = j.JournalKey
WHERE d.thedate BETWEEN '03/01/2012' AND '03/31/2012'
GROUP BY thedate, j.code, j.description

SELECT account, description, gldescription, accounttype, gldepartmentcode, gmfsaccount
FROM edwAccountDim
WHERE (gmfsaccount LIKE '460%' OR 
    gmfsaccount LIKE '461%' OR
    gmfsaccount IN ('462','463','464','466','469'))
  AND accounttype = '4'    
  
SELECT *
FROM stgArkonaGLPTRNS  
WHERE gtpost = 'Y'
AND gtdoc# = ''

SELECT *
FROM stgArkonaGLPTRNS
WHERE gtacct = '166504'
ORDER BY gtdate DESC


SELECT * FROM zdimro


-- ALL accounts of type Sales/COG tied to an RO --------------------------------
SELECT gtjrnl, gtacct, MAX(ro), MIN(ro), COUNT(*)
INTO #wtf
FROM (  
  SELECT ro
  FROM zdimro) a  
INNER JOIN (  
  SELECT gtjrnl, gtacct, gtdoc#
  FROM stgArkonaGLPTRNS g
  INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
    AND m.gmtype = '4') b ON a.ro = b.gtdoc#
GROUP BY gtjrnl, gtacct

SELECT w.*, a.description, a.gldescription, accounttype, gldepartment, 
  gmfsaccount, gmfsdepartment, gmfssection
FROM #wtf w
LEFT JOIN edwAccountDim a ON w.gtacct = a.account
  
SELECT * FROM #wtf  

CREATE TABLE zAccountsFromROs (
  gtjrnl cichar(3),
  gtacct cichar(10),
  ro1 cichar(9),
  ro2 cichar(9),
  howmany integer) IN database;
  
ALTER TABLE zAccountsFromROs
ADD COLUMN AccountType cichar(12);  
  
UPDATE zAccountsFromROs
SET AccountType = 'Sales'
  
INSERT INTO zAccountsFromRos
SELECT *
FROM #wtf  
  
DROP TABLE #wtf  
SELECT gtjrnl, gtacct, MAX(ro), MIN(ro), COUNT(*)
INTO #wtf
FROM (  
  SELECT ro
  FROM zdimro) a  
INNER JOIN (  
  SELECT gtjrnl, gtacct, gtdoc#
  FROM stgArkonaGLPTRNS g
  INNER JOIN stgArkonaGLPMAST m ON g.gtacct = m.gmacct
    AND m.gmtype = '5') b ON a.ro = b.gtdoc#
GROUP BY gtjrnl, gtacct

INSERT INTO zAccountsFromRos  
SELECT gtjrnl, gtacct, expr, expr_1, expr_2, 'COGS' FROM #wtf  

-- ALL accounts of type Sales/COGS tied to an RO --------------------------------

-- types ok
SELECT w.*, a.description, a.gldescription, a.accounttype, a.gldepartmentcode, g.gmtype, g.gmdept
--SELECT COUNT(*)
FROM zAccountsFromRos w
LEFT JOIN edwAccountDim a ON w.gtacct = a.account
LEFT JOIN stgArkonaGLPMAST g ON w.gtacct = g.gmacct
  AND g.gmyear = 2012
WHERE a.gldepartmentcode <> g.gmdept
ORDER BY w.gtacct  

-- change list for Jeri to approve
SELECT distinct w.gtacct AS Account, a.gldescription AS Description, gmdept AS "Current Dept", gldepartmentcode AS "New Dept"
--SELECT COUNT(*)
FROM zAccountsFromRos w
LEFT JOIN edwAccountDim a ON w.gtacct = a.account
LEFT JOIN stgArkonaGLPMAST g ON w.gtacct = g.gmacct
  AND g.gmyear = 2012
WHERE a.gldepartmentcode <> g.gmdept
ORDER BY w.gtacct 

SELECT *
FROM zaccountsfromros
where gtacct = '246031'



