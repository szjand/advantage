--< DDL -----------------------------------------------------------------------
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimPaymentType-factRepairOrder');      
 
-- TABLE
DROP TABLE dimPaymentType;
CREATE TABLE dimPaymentType (
  PaymentTypeKey autoinc,
  PaymentTypeCode cichar(1),
  PaymentType cichar(24))IN database;
  
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimPaymentType','dimPaymentType.adi','PK',
   'PaymentTypeKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimPaymentType','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'dimPaymentTypeObjectsfail');   
-- index: NK 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimPaymentType','dimPaymentType.adi','NK',
   'PaymentTypeCode', '',2051,512,'' );   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimPaymentType','PaymentTypeCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimPaymentType','PaymentType', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
--/> DDL -----------------------------------------------------------------------  
--< keyMap --------------------------------------------------------------------
--this IS a trivial, hand crafted dimension, no need for a keymap
--/> keyMap --------------------------------------------------------------------  
--< Dependencies & zProc ------------------------------------------------------
--no nightly updates, no dependencies outside the notion of an unknow dimension row
--/> Dependencies & zProc ------------------------------------------------------  

--< initial load --------------------------------------------------------------
-- unknown row
INSERT INTO dimPaymentType (PaymentTypeCode, PaymentType)
values ('U','UNKNOWN');
INSERT INTO dimPaymentType (PaymentTypeCode, PaymentType) values ('C','Customer Pay');
INSERT INTO dimPaymentType (PaymentTypeCode, PaymentType) values ('I','Internal');
INSERT INTO dimPaymentType (PaymentTypeCode, PaymentType) values ('S','Service Contract');
INSERT INTO dimPaymentType (PaymentTypeCode, PaymentType) values ('W','Warranty');
--/> initial load --------------------------------------------------------------

-- RI
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimPaymentType-factRepairOrder',
     'dimPaymentType', 'factRepairOrder', 'PaymentTypeKey', 
     2, 2, NULL, '', '');  