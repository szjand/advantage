/*

*/

-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	  WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
  	WHEN data_type = 'numeric' THEN 'integer,'
  	WHEN data_type = 'timestmp' THEN 'timestamp,'
  	WHEN data_type = 'smallint' THEN 'integer,'
  	WHEN data_type = 'integer' THEN 'double,'
  	WHEN data_type = 'decimal' THEN 'double,'
  	WHEN data_type = 'date' THEN 'date,'
  	WHEN data_type = 'time' THEN 'cichar(8),'
  	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'SDPLOPC' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'SDPLOPC'  
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'SDPLOPC'  
AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaSDPLOPC
-- 3. revise DDL based ON step 2, CREATE table
CREATE TABLE stgArkonaSDPLOPC (
soco#      cichar(3),          
solopc     cichar(10),         
sodes1     cichar(50),
sodes2     cichar(50),
sodes3     cichar(50),
sodes4     cichar(50),        
somajc     cichar(3),          
sominc     cichar(3),          
somanf     cichar(3),          
somake     cichar(25),         
somodl     cichar(25),         
soengn     cichar(3),          
soyear     cichar(1),          
sodlpm     cichar(2),          
socorr     cichar(1),          
sotype     cichar(1),          
soxdis     cichar(1),          
soskil     cichar(1),          
sotaxb     cichar(1),          
sohmchg    money,             
sosupc     cichar(1),          
sormth     cichar(1),          
soramt     money,             
socmth     cichar(1),          
socamt     money,             
sopmth     cichar(1),          
sopamt     money,             
soestm     cichar(1),          
soehrs     double,             
sowaitf    cichar(1),          
socds1     cichar(50),         
sodes5     cichar(50),         
sodes6     cichar(50),         
somds1     cichar(50),         
sodftsvct  cichar(2)) IN database;     

CREATE TABLE tmpSDPLOPC (
) IN database;        

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'SDPLOPC';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'SDPLOPC';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaSDPLOPC';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
/************ 8/6/12 *************/
fuck, the entire TABLE IS only 22633 records
just DO the whole thing
concatenating the description columns FROM ads did NOT appear to WORK
AND, i am worried about trimming those columns, the leading space
IN sodes2-4 are relevant

-- 8/7

-- base TABLE
-- exclude columns (no data): somake, somodl, soengn, soyear, sotype, sodes5, sodes6, sowaitf, socds1, somds1
SELECT soco#, solopc, sodes1
FROM (
select SOCO#, SOLOPC, SODES1, SODES2, SODES3, SODES4, SOMAJC, SOMINC, SOMANF, 
  SODLPM, SOCORR, SOXDIS, SOSKIL, SOTAXB, SOHMCHG, SOSUPC, SORMTH, SORAMT, 
  SOCMTH, SOCAMT, SOPMTH, SOPAMT, SOESTM, SOEHRS, SODFTSVCT
FROM stgArkonaSDPLOPC
GROUP BY SOCO#, SOLOPC, SODES1, SODES2, SODES3, SODES4, SOMAJC, SOMINC, SOMANF, 
  SODLPM, SOCORR, SOXDIS, SOSKIL, SOTAXB, SOHMCHG, SOSUPC, SORMTH, SORAMT, 
  SOCMTH, SOCAMT, SOPMTH, SOPAMT, SOESTM, SOEHRS, SODFTSVCT) x
GROUP BY soco#, solopc, sodes1
HAVING COUNT(*) > 1  
-- NOT good enuf, will be linking to ro BY co/lopc, so there can NOT be dup opcodes
SELECT SOCO#, SOLOPC, SODES1, SODES2, SODES3, SODES4, SOMAJC, SOMINC, SOMANF, 
  SODLPM, SOCORR, SOXDIS, SOSKIL, SOTAXB, SOHMCHG, SOSUPC, SORMTH, SORAMT, 
  SOCMTH, SOCAMT, SOPMTH, SOPAMT, SOESTM, SOEHRS, SODFTSVCT
FROM stgArkonaSDPLOPC a
WHERE EXISTS (
  SELECT 1
  FROM stgArkonaSDPLOPC
  WHERE soco# = a.soco#
    AND solopc = a.solopc
  GROUP BY soco#, solopc
  HAVING COUNT(*) > 1)
ORDER BY a.solopc  
