
DROP TABLE dimRoStatus;
CREATE TABLE dimRoStatus (
  RoStatusKey autoinc,
  RoStatusCode cichar(1),
  RoStatus cichar(24)) IN database;
  
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimRoStatus','dimRoStatus.adi','PK',
   'RoStatusKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimRoStatus','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'dimRoStatusObjectsfail');   
-- index: NK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimRoStatus','dimRoStatus.adi','NK',
   'RoStatusCode','',2051,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimRoStatus','dimRoStatus.adi','RoStatus',
   'RoStatus','',2051,512,'' );     
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimRoStatus','RoStatusKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimRoStatus','RoStatusCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimRoStatus','RoStatus', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
  
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('0','Closed');  
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('1','Open');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('2','In Process');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('3','Appr-PD');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('4','Cashier');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('5','Cashier/Delay');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('6','Pre-Invoice');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('7','Odom Rq');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('8','Parts On Order');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('9','Parts Arrived');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('L','G/L Error');
INSERT INTO dimRoStatus (RoStatusCode, RoStatus) values ('P','PO Required');

--- keyMap --------------------------------------------------------------------

--DROP TABLE keyMapDimRoStatus;     
CREATE TABLE keyMapDimRoStatus (
  RoStatusKey integer,
  RoStatusCode cichar(1)) IN database;         
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keyMapDimRoStatus','keyMapDimRoStatus.adi','PK',
   'RoStatusKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('keyMapDimRoStatus','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'keyMapDimRoStatusfail');   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimRoStatus','RoStatusKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimRoStatus','RoStatusCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
--RI  
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimRoStatus-KeyMap');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimRoStatus-KeyMap','dimRoStatus','keyMapdimRoStatus','PK',2,2,NULL,'','');   
      
INSERT INTO keyMapDimRoStatus
SELECT RoStatusKey, RoStatusCode FROM dimRoStatus;  

--/ keyMap --------------------------------------------------------------------


--- Dependencies & zProc ------------------------------------------------------
INSERT INTO zProcExecutables values('dimRoStatus','daily',CAST('02:30:15' AS sql_time),6);
INSERT INTO zProcProcedures values('dimRoStatus','checkRoStatus',1,'Daily');
INSERT INTO DependencyObjects values('dimRoStatus','checkRoStatus','dimRoStatus');
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimRoStatus'
  AND b.dObject = 'tmpPDPPHDR';
  
  
    
--/ Dependencies & zProc -----------------------------------------------------

nightly stored proc needs to ensure that there are no rows IN pdppdet with a 
status that does NOT decode, hmmm, i can either DO a separate scrape FROM PDPPHDR
OR just use tmpPDPPHDR AND factor IN the dependency
need to decide, it matters for WHEN this IS run 
fuck it, it IS a trivial query, DO a fresh pdpphdr scrape
but querying tmppdphdr IS clean for the test


      WHEN '1' THEN 'Open'
      WHEN '2' THEN 'In-Process'
      WHEN '3' THEN 'Appr-PD'
      WHEN '4' THEN 'Cashier'
      WHEN '5' THEN 'Cashier/Delay'
      WHEN '6' THEN 'Pre-Invoice'
      WHEN '7' THEN 'Odom Rq'
      WHEN '9' THEN 'Parts Arrived'
      WHEN 'L' THEN 'G/L Error'
      WHEN 'P' THEN 'PO Required'

CREATE PROCEDURE checkRoStatus (
  PassFail cichar(4) OUTPUT)
BEGIN
/*
  verify that there are no new OR previously undisclosed ro status IN PDPPHDR
*/   
DECLARE @PassFail integer; 
@PassFail = (    
  SELECT COUNT(*)
  FROM (
    SELECT ptrsts
    FROM tmpPDPPHDR a
    WHERE ptdtyp = 'RO'
      AND ptdoc# <> ''
      AND NOT EXISTS (
        SELECT 1
        FROM dimRoStatus
        WHERE RoStatusCode = ptrsts)
    GROUP BY ptrsts) b);
INSERT INTO __output
SELECT 
  CASE
    WHEN @PassFail = 0 THEN 'Pass' 
    ELSE 'Fail'
  END AS PassFail     
FROM system.iota;    
END;  

SELECT *
FROM nightlybatch