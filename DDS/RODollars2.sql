thinking dollars IS a different deal than operational
to tie p/l sales/cos to a line

SELECT * FROM factroline

SELECT storecode, ro, line
FROM factroline
GROUP BY storecode, ro, line
HAVING COUNT(*) > 1


SELECT b.gtdoc#, b.gtdtyp, b.gtdate, b.gttamt, b.gtacct, c.gmyear, c.gmtype, c.gmdept    
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
--  AND c.gmdept <> 'PD'
WHERE b.gtdtyp = 's'
  AND b.gtdoc# = '19098080'
ORDER BY gmdept, gmtype  

-- why this IS fucking with my head
-- ro 19098080
-- CW labor sales $13, no cos against cs
-- SD cos $3.96, no labor sales

DECLARE @ro string;
DROP TABLE #ro;
DROP TABLE #gl;
@ro = '19098080';
SELECT ro, servicetype
INTO #ro
FROM factroline
WHERE ro = @ro
GROUP BY ro, servicetype;
SELECT b.gtdoc#, c.gmtype, c.gmdept, SUM(gttamt) 
INTO #gl   
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
--  AND c.gmdept <> 'PD'
WHERE b.gtdtyp = 's'
  AND b.gtdoc# = @ro
GROUP BY b.gtdoc#, gmdept, gmtype;
SELECT *
FROM #ro a
full JOIN #gl  b ON a.ro = b.gtdoc#
  AND a.servicetype = b.gmdept
ORDER BY gmdept, gmtype    

SELECT *
FROM factro
WHERE ro = '16090849'
  
  
SELECT ptinv#, ptline, ptcode, ptdate, ptpart, ptqty, ptcost, ptnet, ptqty*ptnet
FROM stgarkonapdptdet
WHERE trim(ptinv#) = '16066772'  

SELECT ptinv#, ptline, sum(ptqty*ptcost) AS cost, sum(ptqty*ptnet) AS sales
FROM stgarkonapdptdet
WHERE trim(ptinv#) = '16066772'  
GROUP BY ptinv#, ptline

SELECT ptco#, ptinv#, sum(ptqty*ptcost) AS cost, sum(ptqty*ptnet) AS sales
FROM stgarkonapdptdet
WHERE trim(ptinv#) = '16066772'  
GROUP BY ptco#, ptinv#
-- ok parts IS looking good, compare it to gl
-- well, there are discrepancies
SELECT *
--INTO #disc
--SELECT COUNT(*)
FROM (
  SELECT ptco#, ptinv#, sum(ptqty*ptcost) AS pdCost, sum(ptqty*ptnet) AS pdSales
  FROM stgarkonapdptdet
  WHERE trim(ptinv#) in (SELECT ro FROM factro WHERE opendatekey BETWEEN 3044 AND 3074) -- 05/2012
    AND ptline < 800
  GROUP BY ptco#, ptinv#) a
LEFT JOIN (
  SELECT b.gtdoc#, c.gmdept,
    SUM(CASE when c.gmtype = '5' THEN gttamt END) AS glCost,
    sum(CASE when c.gmtype = '4' THEN gttamt END) AS glSales
  FROM stgArkonaGLPTRNS b
  INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
    AND year(b.gtdate) = c.gmyear
    AND c.gmtype in ('4', '5')
    AND c.gmdept = 'PD'
  WHERE b.gtdtyp = 's'
    AND b.gtdoc# in (SELECT ro FROM factro WHERE opendatekey BETWEEN 3044 AND 3074)
  GROUP BY b.gtdoc#, gmdept) b ON trim(a.ptinv#) = b.gtdoc#
WHERE pdcost - glcost <> 0 
  OR pdsales + glsales <> 0 
 
SELECT * FROM #disc 
SELECT COUNT(*) FROM #disc -- 1237 of 4853, a 1/4 don't match
questions
   16087519 why no glsales
  
SELECT ptinv#, ptline, ptcode, ptdate, ptpart, ptqty, ptcost, ptnet, ptqty*ptnet
FROM stgarkonapdptdet
WHERE trim(ptinv#) = '16087483'   

SELECT b.gtdoc#, gtseq#, b.gtdtyp, b.gtdate, b.gttamt, b.gtacct, c.gmyear, c.gmtype, c.gmdept   
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
  AND c.gmdept = 'PD'
WHERE b.gtdtyp = 's'
  AND b.gtdoc# = '16087483' 
ORDER BY gmdept, gmtype  

-- i'm ok going with gl AS parts sales AND cost, but THEN i'm stuck ON allocating
-- the correct amounts to the correct depts

SELECT b.gtdoc#, gtseq#, b.gtdtyp, b.gtdate, b.gttamt, b.gtacct, b.gtjrnl, c.gmyear, c.gmtype, c.gmdept   
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
--  AND c.gmdept = 'PD'
WHERE b.gtdtyp = 's'
  AND b.gtdoc# = '16090849' 
ORDER BY gmdept, gmtype  

-- 7/13/12
maybe a bad assumbption, CW (146207: SLS CAR WASH CUSTOMER) IS NOT labor sales
now wait just a goddamned minute, 
SELECT *
FROM stgarkonaglpmast
WHERE gmdesc like '%WASH%'
  AND gmacct LIKE '1%'
  AND gmyear = 2012
  
SELECT *
FROM stgarkonaglptrns
WHERE gtacct = '11301b'
  AND year(gtdate) = 2012
  AND gtjrnl = 'svi'
ORDER BY gtdate DESC



SELECT ro, COUNT(*)
FROM factroline a
WHERE servicetype = 'mr'
and EXISTS (

SELECT 1
FROM factroline
WHERE ro = a.ro
and servicetype = 'cw')
GROUP BY ro
ORDER BY COUNT(*)

-- 7/13
-- gl FROM factro
-- FROM SDPRHDR
SELECT a.ro, b.ptdate, a.laborsales, b.ptltot as lSales,
  a.partssales, b.ptptot AS pSales
FROM factro a
LEFT JOIN stgArkonaSDPRHDR b ON a.ro = b.ptro#
WHERE a.opendatekey > 3000

-- stuck ON categorization
-- go back to statues, productivity


DECLARE @ro string;
--DROP TABLE #ro;
--DROP TABLE #gl;
@ro = '16091952';
SELECT ro, servicetype
INTO #ro
FROM factroline
WHERE ro = @ro
GROUP BY ro, servicetype;
SELECT b.gtdoc#, c.gmtype, c.gmdept, SUM(gttamt) 
INTO #gl   
FROM stgArkonaGLPTRNS b
INNER JOIN stgArkonaGLPMAST c ON b.gtacct = c.gmacct
  AND year(b.gtdate) = c.gmyear
  AND c.gmtype in ('4', '5')
  AND c.gmdept <> 'PD'
WHERE b.gtdtyp = 's'
  AND b.gtdoc# = @ro
GROUP BY b.gtdoc#, gmdept, gmtype;
SELECT *
FROM #ro a
full JOIN #gl  b ON a.ro = b.gtdoc#
  AND a.servicetype = b.gmdept
  
  
SELECT servicetype, opcode, COUNT(*)
FROM factroline 
WHERE storecode = 'ry1'
  AND servicetype = 're'
group by servicetype, opcode 
ORDER BY COUNT(*) desc
  
