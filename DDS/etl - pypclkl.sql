-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	  WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
  	WHEN data_type = 'date' THEN 'date,'
  	WHEN data_type = 'time' THEN 'time,'
  	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'PYPCLKL' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'PYPCLKL'  
  AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'PYPCLKL'  
  AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaPYPCLOCKIN
-- 3. revise DDL based ON step 2, CREATE table
CREATE TABLE stgArkonaPYPCLKL (
ylcco#     cichar(3),          
ylemp#     cichar(7),          
ylclkind   date,               
ylclkint   time,               
ylclkoutd  date,               
ylclkoutt  time,               
ylcode     cichar(3),          
yluser     cichar(10),         
ylwsid     cichar(10),         
ylchgc     cichar(3),          
ylchgd     date,               
ylchgt     time) IN database;       

SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text, ordinal_position
INTO #pypclkl
from syscolumns
WHERE table_name = 'pypclkl'  
 
-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
@i = (
  SELECT MAX(ordinal_position)
  FROM #pypclkl);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = coalesce((SELECT '[' + TRIM(column_name) + '], '  FROM #pymast WHERE ordinal_position = @j), '');
-- just COLUMN names
--  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM #pymast WHERE ordinal_position = @j), '');
-- parameter names  
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM #pymast WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;     

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
@i = (
  SELECT MAX(ordinal_position)
  FROM #pypclkl);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM #pypclkl WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@i = (
  SELECT MAX(ordinal_position)
  FROM #pypclkl);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM #pypclkl WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPYPCLKL';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	  WHEN 11 THEN 'AsInteger'
  	WHEN 3 THEN 'AsDateTime'
  	WHEN 18 THEN 'AsCurrency'
  	WHEN 10 THEN 'AsFloat'
    WHEN 13 THEN 'AsDateTime'
  	ELSE 'AsXXX'
    END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
  	WHEN 11 THEN 'AsInteger'
  	WHEN 3 THEN 'AsDateTime'
  	WHEN 18 THEN 'AsCurrency'
  	WHEN 10 THEN 'AsFloat'
    WHEN 13 THEN 'AsDateTime'
  	ELSE 'AsXXX' 
  END + ';' 
-- SELECT *  
FROM system.columns 
WHERE parent = @table_name;
 
-------------------------------------------------------------------------------- 
 
select * 
FROM stgArkonaPYPCLKL
WHERE TRIM(ylemp#) = '96580'