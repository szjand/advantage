Roger Stewart showing up on PDQ Writer Stats with no ROS

SELECT *
FROM factrepairorder
where ro = '19360550'

SELECT *
FROM dimservicewriter
WHERE servicewriterkey = 726

SELECT *
FROM dimservicewriter
WHERE name LIKE 'stew%'

-- no history IN edwEmployeeDim just one row
SELECT * FROM edwEmployeeDim WHERE employeenumber = '154893'

-- there are no ros for writerkey 960 - the inactive one
-- the last ro for writerkey 952 IS on 11/17/19
SELECT servicewriterkey, MAX(b.thedate)
FROM factrepairorder a
JOIN day b on a.opendatekey > b.datekey
  and b.thedate > '12/01/2018'
WHERE servicewriterkey IN (952,960)
GROUP BY servicewriterkey 

-- there are none, so whack it
SELECT *
FROM factrepairorder
WHERE servicewriterkey = 960

DELETE FROM dimservicewriter WHERE servicewriterkey = 960;

-- AND fix key 952
UPDATE dimservicewriter
SET currentrow = True, rowchangedate = NULL, rowchangedatekey = NULL, rowthruts = NULL,
  rowchangereason = NULL, servicewriterkeythrudate = '12/31/9999',
  servicewriterkeythrudatekey = 7306
where servicewriterkey = 952 

-- need to figure out which ros were his
SELECT a.ptro#, ptdate, ptcdat, ptfcdt, ptcreate, ptcnam, b.*
-- SELECT COUNT(*)
FROM stgarkonasdprhdr a
LEFT JOIN (
  SELECT ro, servicewriterkey
  FROM factrepairorder
  GROUP BY ro, servicewriterkey) b on a.ptro# = b.ro
WHERE  a.ptswid = '437'
  AND a.ptdate > '11/01/2019'
  AND b.servicewriterkey = 726
  
--AND fix factrepairorder  
UPDATE factrepairorder
SET servicewriterkey = 952
WHERE ro IN (
  SELECT b.ro
  FROM stgarkonasdprhdr a
  LEFT JOIN (
    SELECT ro, servicewriterkey
    FROM factrepairorder
    GROUP BY ro, servicewriterkey) b on a.ptro# = b.ro
  WHERE  a.ptswid = '437'
    AND a.ptdate > '11/01/2019'
    AND b.servicewriterkey = 726)  