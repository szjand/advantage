
/*
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'double,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'money,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'pymast' 
  AND column_name NOT IN ('ymext', 'ymsecg', 'ymcncd', 'ymmucd', 'ymnkey',
    'ymfp','ymexss','ymexfd','ymvtyp','ymsala','ymrata','ymretp','ymrfix',
    'ymramx','ymwkcd','ymcwfl','ymstad','ymstap','ymstfx','ymexcn','ymcnad',
    'ymcnap','ymexmu','ymmuad','ymmuap','ymdvex', 'ymlsup','ymmutx','ymlsck',
	'ymdece','ymexst','ymdvac','ymdhol','ymdsck','ymdvyr','ymdhyr','ymdsyr',
	'ymvrat','ymhrat','ymsrat')
  AND ordinal_position < 81
--ORDER BY data_type
ORDER BY ordinal_position



         

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text, ordinal_position
from syscolumns
WHERE table_name = 'pymast'  
--  AND data_type <> 'CHAR'
  AND column_name NOT IN ('ymext', 'ymsecg', 'ymcncd', 'ymmucd', 'ymnkey',
    'ymfp','ymexss','ymexfd','ymvtyp','ymsala','ymrata','ymretp','ymrfix',
    'ymramx','ymwkcd','ymcwfl','ymstad','ymstap','ymstfx','ymexcn','ymcnad',
    'ymcnap','ymexmu','ymmuad','ymmuap','ymdvex', 'ymlsup','ymmutx','ymlsck',
	'ymdece','ymexst','ymdvac','ymdhol','ymdsck','ymdvyr','ymdhyr','ymdsyr',
	'ymvrat','ymhrat','ymsrat')
  AND ordinal_position < 81
ORDER BY ordinal_position

-- non char field list for spreadsheet
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'pymast'  
  AND data_type <> 'CHAR'
  AND column_name NOT IN ('ymext', 'ymsecg', 'ymcncd', 'ymmucd', 'ymnkey',
    'ymfp','ymexss','ymexfd','ymvtyp','ymsala','ymrata','ymretp','ymrfix',
    'ymramx','ymwkcd','ymcwfl','ymstad','ymstap','ymstfx','ymexcn','ymcnad',
    'ymcnap','ymexmu','ymmuad','ymmuap','ymdvex', 'ymlsup','ymmutx','ymlsck',
	'ymdece','ymexst','ymdvac','ymdhol','ymdsck','ymdvyr','ymdhyr','ymdsyr',
	'ymvrat','ymhrat','ymsrat')
  AND ordinal_position < 81
ORDER BY ordinal_position



*/
/*      
DROP TABLE #pymast

SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text, ordinal_position
INTO #Pymast  
from syscolumns
WHERE table_name = 'pymast'  
  AND column_name NOT IN ('ymext', 'ymsecg', 'ymcncd', 'ymmucd', 'ymnkey',
    'ymfp','ymexss','ymexfd','ymvtyp','ymsala','ymrata','ymretp','ymrfix',
    'ymramx','ymwkcd','ymcwfl','ymstad','ymstap','ymstfx','ymexcn','ymcnad',
    'ymcnap','ymexmu','ymmuad','ymmuap','ymdvex', 'ymlsup','ymmutx','ymlsck',
	'ymdece','ymexst','ymdvac','ymdhol','ymdsck','ymdvyr','ymdhyr','ymdsyr',
	'ymvrat','ymhrat','ymsrat')
  AND ordinal_position < 81;
  
SELECT * FROM #pymast
*/
/*
-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
@i = (
  SELECT MAX(ordinal_position)
  FROM #pymast);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = coalesce((SELECT '[' + TRIM(column_name) + '], '  FROM #pymast WHERE ordinal_position = @j), '');
-- just COLUMN names
--  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM #pymast WHERE ordinal_position = @j), '');
-- parameter names  
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM #pymast WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPYMAST';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;


select cast('9999-12-31' as sql_date) FROM system.iota

CREATE TABLE stgArkonaPYMAST (
ymco#      cichar(3),          
ymempn     cichar(7),          
ymactv     cichar(1),          
ymdept     cichar(2),          
ymstcd     cichar(5),          
ymname     cichar(25),         
ymstre     cichar(30),         
ymcity     cichar(20),         
ymstat     cichar(2),          
ymzip      cichar(9),              
ymarea     cichar(3),              
ymphon     cichar(7),              
ymss#      cichar(9),              
ymdriv     cichar(15),         
ymbdte     date,              
ymhdte     date,              
ymhdto     date,              
ymrdte     date,              
ymtdte     date,              
ymsex      cichar(1),          
ymmari     cichar(1),          
ymfdex     integer,              
ymfdad     money,              
ymfdap     double,              
ymclas     cichar(1),          
ymeeic     cichar(1),          
ymecls     cichar(1),          
ympper     cichar(1),          
ymsaly     money,              
ymrate     money,              
ymdist     cichar(4),          
ymrete     cichar(1),          
ymsttx     cichar(5),          
ymcntx     cichar(5),          
ymstex     integer,              
ymsbki     cichar(30),         
ymar#      cichar(10),         
ymdvst     cichar(2),          
ymfdst     cichar(1),          
ymstst     cichar(1)) IN database     

*/

-------------------------------------------------------------------
ok, so what DO i DO with a nightly pymast scrape
populate the Employee Dimension
currenly:
      EmployeeKey AutoInc,
      Store CIChar( 30 ),
      EmployeeNumber CIChar( 12 ),
      Name CIChar( 30 ),
      Manager CIChar( 30 ),
      Department CIChar( 30 ),
      BirthDate Date
	  
ALTER TABLE edwEmployeeDim
ADD COLUMN MostRecentRow logical
ADD COLUMN RowEffectiveTS timestamp
ADD COLUMN RowExpirationTS timestamp
ADD COLUMN RowLoadedTS timestamp
ADD COLUMN RowUpdatedTS timestamp
--ADD COLUMN BirthDateKey integer
ADD COLUMN Active cichar(1)
ADD COLUMN HireDateKey integer
ADD COLUMN TermDateKey integer


select * FROM stgArkonaPYMAST ORDER BY ymdept

SELECT *
FROM edwEmployeeDim
WHERE Department = ''
	  
-- check for new record	  
DELETE FROM edwEmployeeDim

SELECT * FROM edwEmployeeDim ORDER BY length(name)
DELETE FROM edwEmployeeDim WHERE length(name) < 6

SELECT *
FROM stgArkonaPYMAST a
LEFT JOIN edwEmployeeDim e ON a.ymempn = e.employeenumber
WHERE e.employeenumber IS NULL 


INSERT INTO edwEmployeeDim (Store, EmployeeNumber, Name, Department,
  BirthDateKey, Active, HireDateKey, TermDateKey)
select s.ymco#, s.ymempn, s.ymname, s.ymdept, 
  (SELECT datekey FROM day WHERE theDate = s.ymbdte),
  s.ymactv, 
  (SELECT datekey FROM day WHERE theDate = s.ymhdte),
  (SELECT datekey FROM day WHERE theDate = s.ymtdte)
FROM stgArkonaPYMAST s
LEFT JOIN edwEmployeeDim e ON s.ymempn = e.employeenumber
  AND 
    case
	  when s.ymco# = 'RY1' THEN 'Rydell'
	  WHEN s.ymco# = 'RY2' then 'Honda'
	  when s.ymco# = 'RY3' then 'Crookston'
	end = e.store
WHERE e.employeenumber IS NULL 
   
execute procedure stgTrimCharacterFields('PYMAST')    
SELECT DISTINCT store FROM edwEmployeeDim


-- check for type 1 changes
SELECT *
FROM stgArkonaPYMAST a
LEFT JOIN edwEmployeeDim e ON a.ymempn = e.employeenumber
  AND 
    case
	  when a.ymco# = 'RY1' THEN 'Rydell'
	  WHEN a.ymco# = 'RY2' then 'Honda'
	  when a.ymco# = 'RY3' then 'Crookston'
	end = e.store
WHERE (
  a.ymco# <> e.Store OR
  a.ymempn <> e.EmployeeNumber) OR 
  (SELECT datekey FROM day WHERE TheDate = a.ymbdte) <> e.BirthDateKey OR 
  (SELECT datekey FROM day WHERE TheDate = a.ymhdte) <> e.HireDateKey OR
  CASE 
    WHEN (SELECT datetype FROM day WHERE datekey = e.TermDateKey) <> 'NA' THEN 
	  (SELECT datekey FROM day WHERE theDate = a.ymtdte) <> e.TermDateKey
	ELSE 1 = 1
  END)
-- shit, so i'm thinking abt updating based ON the NK  (emp/store), but IF those fields
-- are type 1, i'll never find them based ON the above JOIN
-- so, are these fields NOT updateable?  
-- duh, #1, store IS rydell, hond ..., ymco# IS RY1 ...
-- thinking ADD the edw fields to stgArkonaPYMAST ?
-- OR IS there an interim TABLE etlEmplDim, that IS the subset of fields FROM stgArkonaPYMAST
-- that are needed for edwEmployeeDim AS well AS conversions needed (store, datekeys, etc)
-- something LIKE xfmEmployeeDim

SELECT ymname, name, ymempn, employeeNumber, ymco#, store
FROM stgArkonaPYMAST a, edwEmployeeDim e
WHERE a.ymempn = e.EmployeeNumber
  AND 
    case
	  when a.ymco# = 'RY1' THEN 'Rydell'
	  WHEN a.ymco# = 'RY2' then 'Honda'
	  when a.ymco# = 'RY3' then 'Crookston'
	end <> e.store

SELECT *
FROM stgArkonaPYMAST a
LEFT JOIN edwEmployeeDim e ON a.ymempn = e.employeenumber
  AND a.ymco# = e.store
  
  


UPDATE e1
  SET e1.BirthDateKey = (SELECT datekey FROM day WHERE thedate = a.ymbdte),
      e1.HireDateKey = (SELECT datekey FROM day WHERE thedate = a.ymhdte)
	  
SELECT name, ymname, employeenumber, ymempn, active, BirthDateKey, ymbdte, (SELECT datekey FROM day WHERE thedate = a.ymbdte), HireDateKey, ymhdte, (SELECT datekey FROM day WHERE thedate = a.ymhdte) 
  FROM stgArkonaPYMAST a
  LEFT JOIN edwEmployeeDim e1 ON a.ymempn = e1.EmployeeNumber 
    AND e1.Store = a.ymco#
    AND (
	  (SELECT datekey FROM day WHERE TheDate = a.ymbdte) <> coalesce(e1.BirthDateKey, 0) OR 
	  (SELECT datekey FROM day WHERE TheDate = a.ymhdte) <> coalesce(e1.HireDateKey, 0))
WHERE e1.EmployeeNumber = a.ymempn 

select *
FROM stgArkonaPYMAST
WHERE ymempn IN (  
SELECT EmployeeNumber FROM edwEmployeeDim WHERE HireDateKey IS NULL)	  

SELECT * FROM day WHERE datetype = 'NA'
UPDATE day SET datetype = 'NA' WHERE datetype = 'N/A'
	  
UPDATE edwEmployeeDim
SET BirthDate = (
  SELECT ymbdte
  FROM stgArkonaPYMAST
  WHERE ymempn = edwEmployeeDim.EmployeeNumber)


---- interim TABLE idea
-- data IN this TABLE will have ALL fields necessary to 
-- populate the dimension
-- this TABLE IS populated BY stgArkonaPYMAST
-- with joins to Day & stgArkonaPYPCLKCTL
-- data IN this TABLE IS expunged after edwDim IS populated
-- drop TABLE xfmEmployeeDim
CREATE TABLE xfmEmployeeDim ( 
      StoreCode CIChar( 3 ), //ymco#
      StoreName CIChar( 30 ),
      EmployeeNumber CIChar( 7 ),
      ActiveCode CIChar( 1 ), //ymactv
      ActiveName CIChar( 4 ),
      DeptCode CIChar( 2 ), //ymdept
      DeptName CIChar( 30 ),
      Name CIChar( 25 ),
      BirthDate Date, //ymbdte
      BirthDateKey Integer,
      HireDate Date, //ymhdte
      HireDateKey Integer,
      TermDate Date, //ymtdte
      TermDateKey Integer) IN DATABASE;	 
ALTER TABLE xfmEmployeeDim
ALTER COLUMN StoreName StoreCodeDesc cichar(30)
ALTER COLUMN ActiveName ActiveCodeDesc cichar(4)
ALTER COLUMN DeptName DeptCodeDesc cichar(30)	  
-- SELECT * FROM stgArkonaPYMAST	  
-- DROP drivers license
-- DELETE FROM xfmEmployeeDim
INSERT INTO xfmEmployeeDim (StoreCode, StoreCodeDesc, EmployeeNumber, ActiveCode, 
  ActiveCodeDesc, DeptCode, DeptCodeDesc, Name, BirthDate, BirthDateKey, HireDate, 
  HireDateKey, TermDate, TermDateKey)
-- using CodesAndTypes TABLE
-- SELECT storecode, employeenumber
-- FROM (  
SELECT ymco# AS StoreCode,
  store.codeDesc AS StoreCodeDesc,
  ymempn AS EmployeeNumber, 
  trim(active.code) as ActiveCode, trim(active.CodeDesc) AS ActiveCodeDesc,
  trim(dept.code) AS DeptCode,
  trim(dept.codeDesc) AS DeptCodeDesc,
  ymname AS Name, 
  ymbdte AS BirthDate, bd.DateKey AS BirthDateKey,
  ymhdte AS HireDate, hd.DateKey AS HireDateKey,
  ymtdte AS TermDate, td.DateKey AS TermDateKey
FROM stgArkonaPYMAST p
LEFT JOIN day bd ON p.ymbdte = bd.thedate
LEFT JOIN day hd ON p.ymhdte = hd.thedate
LEFT JOIN day td ON p.ymtdte = td.thedate
LEFT JOIN resArkonaCodes store ON store.stgTableName = 'stgArkonaPYMAST'
  AND store.stgColumnName = 'ymco#'
  AND store.storeCode = 'All'
  AND p.ymco# = store.code
LEFT JOIN resArkonaCodes active ON active.stgTableName = 'stgArkonaPYMAST'
  AND active.stgColumnName = 'ymactv'
  AND active.storeCode = 'ALL'
  AND p.ymactv = active.code 
LEFT JOIN resArkonaCodes dept ON dept.stgTableName = 'stgArkonaPYPCLKCTL'
  AND dept.stgColumnName = 'yicdept'
  AND dept.storeCode = p.ymco#
  AND p.ymdept = dept.code --) x group BY storecode, employeenumber HAVING COUNT(*) > 1  

ORDER BY employeenumber




-- verify integrity of data
-- constraint, clean, conform tests

SELECT employeenumber, storecode
FROM xfmEmployeeDim
GROUP BY employeenumber, storecode
  HAVING COUNT(*) > 1;

SELECT *
FROM xfmEmployeeDim
WHERE (deptcode IS NULL OR deptcode = '')
  AND ActiveCode <> 'T';
  
SELECT *
FROM xfmEmployeeDim
WHERE ActiveCode = 'T'
AND TermDate = '12/31/9999';  

SELECT *
FROM xfmEmployeeDim
WHERE (
  EmployeeNumber IS NULL OR 
  ActiveCode IS NULL OR
  ActiveName IS NULL OR
  BirthDateKey IS NULL OR
  HireDateKey IS NULL)
  
  
-- THEN look for changes 
-- changes are detectable using the natural key,
-- for a given NK, are ALL the fields the same?
-- what IF the dept name for dept 06 IN pypclkctl changes
-- 1. handle the change, IN what other tables IS that value perpetrated?
--    checking for changes IN zap/reload
-- type 1 changes
-- type 2 changes
 
-- DELETE FROM edwEmployeeDim
INSERT INTO edwEmployeeDim (StoreCode, StoreName, EmployeeNumber, ActiveCode, 
  ActiveName, DeptCode, DeptName, Name, BirthDate, BirthDateKey, HireDate, 
  HireDateKey, TermDate, TermDateKey, 
  RowLoadedTS, RowUpdatedTS, RowEffectiveTS, RowExpirationTS, MostRecentRow)
SELECT StoreCode, StoreName, EmployeeNumber, ActiveCode, ActiveName, DeptCode, DeptName,
  Name, BirthDate, BirthDateKey, HireDate, HireDateKey,
  TermDate, TermDateKey,
  now(), cast(NULL AS sql_timestamp), now(), cast(NULL AS sql_timestamp), true
FROM xfmEmployeeDim

