/* dimEmp *********************************************************************/
SELECT storecode, technumber
FROM dimtech
GROUP BY storecode, technumber
HAVING COUNT(*) > 1

SELECT *
FROM edwEmployeeDim 

SELECT storecode, employeenumber, fullparttimecode, pydeptcode, pydept, distcode, name, 
  payrollclasscode, salary, hourlyrate, coalesce(cast(rowfromts AS sql_date), cast('12/31/9999' AS sql_date))
FROM edwEmployeeDim
GROUP BY storecode, employeenumber, fullparttimecode, pydeptcode, pydept, distcode, name,
  payrollclasscode, salary, hourlyrate, coalesce(cast(rowfromts AS sql_date), cast('12/31/9999' AS sql_date)) -- necessary emp# 1134835 was IN same dist for 2 different periods of time
HAVING COUNT(*) > 1    

SELECT employeekey
FROM (
SELECT employeekey, storecode, employeenumber, fullparttimecode, pydeptcode, pydept, distcode, name
  payrollclasscode, salary, hourlyrate, coalesce(cast(rowfromts AS sql_date), cast('12/31/9999' AS sql_date))
FROM edwEmployeeDim) a
GROUP BY employeekey
HAVING COUNT(*) > 1

DROP TABLE keymapDimEmp;
CREATE TABLE keyMapDimEmp ( 
      EmployeeKey Integer,
      StoreCode CIChar( 3 ),
      EmployeeNumber CIChar( 7 ),
      FullPartTimeCode CIChar( 1 ),
      PyDeptCode CIChar( 2 ),
      PyDept CIChar( 30 ),
      DistCode CIChar( 4 ),
      Name CIChar( 25 ),
      RowFromTS TimeStamp,
      PayrollClassCode CIChar( 1 ),
      Salary Money,
      HourlyRate Money,
      PayPeriodCode CIChar( 1 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keyMapDimEmp',
   'keyMapDimEmp.adi',
   'EMPLOYEEKEY',
   'EmployeeKey',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'keyMapDimEmp',
   'keyMapDimEmp.adi',
   'NK',
   'StoreCode;EmployeeNumber;FullPartTimeCode;PyDeptCode;PyDept;DistCode;Name;RowFromTS;PayrollClassCode;Salary;HourlyRate;PayPeriodCode',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'keyMapDimEmp',
   'keyMapDimEmp.adi',
   'ALL',
   'EmployeeKey;StoreCode;EmployeeNumber;FullPartTimeCode;PyDeptCode;PyDept;DistCode;Name;RowFromTS;PayrollClassCode;Salary;HourlyRate;PayPeriodCode',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapDimEmp', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'keyMapDimEmpfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapDimEmp', 
   'Table_Primary_Key', 
   'EMPLOYEEKEY', 'APPEND_FAIL', 'keyMapDimEmpfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapDimEmp', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'keyMapDimEmpfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapDimEmp', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'keyMapDimEmpfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapDimEmp', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'keyMapDimEmpfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'EmployeeKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'StoreCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'EmployeeNumber', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'FullPartTimeCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'PyDeptCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'PyDept', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'DistCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'Name', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'RowFromTS', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'PayrollClassCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'Salary', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'HourlyRate', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimEmp', 
      'PayPeriodCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapDimEmpfail' ); 
      
INSERT INTO keyMapDimEmp      
SELECT employeekey, storecode, employeenumber, coalesce(fullparttimecode, 'X'), 
  coalesce(pydeptcode, 'N/A'), coalesce(pydept, 'N/A'), 
  distcode, name, coalesce(RowFromTS, CAST('12/31/9999 00:00:01' AS sql_timestamp)), 
  payrollclasscode, salary, hourlyrate, payperiodcode
FROM edwEmployeeDim;  

ALTER TABLE edwEmployeeDim
ALTER COLUMN employeekey employeekey integer;

SELECT *
FROM edwEmployeeDim
WHERE rowfromts IS NULL 
ORDER BY employeekey DESC 

SELECT *
FROM edwEmployeeDim
WHERE employeenumber IN ('1117901','193795')

DELETE 
FROM edwEmployeeDim
WHERE employeekey IN (1611,1612);

DELETE 
FROM keyMapDimEmp
WHERE employeekey IN (1611,1612);

DROP TABLE tally;
CREATE TABLE tally (
  n integer) IN database;
  
DECLARE @i integer;
@i = 0;
WHILE @i < 100001 DO
  INSERT INTO tally values(@i);
  @i = @i + 1;
END WHILE;

select * FROM tally;

/* 6/10
what follows is an unsuccesful attempt to generate 1 row for each record to be inserted
that includes a COLUMN with an integer FROM tally
instead i keep getting

n       emp#   
1       193795
1       1117901
2       193795
2       1117901

so, i'm giving up for now
*/
-- i want a number for each row      

SELECT 
--        (SELECT MAX(EmployeeKey) + 1 FROM keyMapDimEmp), 
  x.StoreCode, x.Store, x.EmployeeNumber, 
  x.ActiveCode, x.Active, x.FullPartTimeCode, x.FullPartTime,
  x.PyDeptCode, x.PyDept, x.DistCode, x.Distribution, x.TechNumber,x.Name, x.BirthDate, 
  x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, true,
  x.HireDate AS EKFrom, x.HireDateKey AS EKFromKey,
  (SELECT thedate FROM day WHERE datetype = 'NA') AS EKThru,
  (SELECT datekey FROM day WHERE datetype = 'NA') as EKThruKey, 
  x.PayrollClassCode, x.PayrollClass, x.Salary, x.HourlyRate,
  x.PayPeriodCode, x.PayPeriod
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
  AND x.employeeNumber = edwED.EmployeeNumber
AND edwED.CurrentRow = true
WHERE edwED.StoreCode IS NULL; 
 
SELECT *
FROM (     
        SELECT n
        FROM tally
        WHERE n <= (
              SELECT COUNT(x.storecode)
              FROM xfmEmployeeDim x
              LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
                AND x.employeeNumber = edwED.EmployeeNumber
          		  AND edwED.CurrentRow = true
              WHERE edwED.StoreCode IS NULL)) a
full OUTER JOIN (
SELECT 
--        (SELECT MAX(EmployeeKey) + 1 FROM keyMapDimEmp), 
  x.StoreCode, x.Store, x.EmployeeNumber, 
  x.ActiveCode, x.Active, x.FullPartTimeCode, x.FullPartTime,
  x.PyDeptCode, x.PyDept, x.DistCode, x.Distribution, x.TechNumber,x.Name, x.BirthDate, 
  x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, true,
  x.HireDate AS EKFrom, x.HireDateKey AS EKFromKey,
  (SELECT thedate FROM day WHERE datetype = 'NA') AS EKThru,
  (SELECT datekey FROM day WHERE datetype = 'NA') as EKThruKey, 
  x.PayrollClassCode, x.PayrollClass, x.Salary, x.HourlyRate,
  x.PayPeriodCode, x.PayPeriod
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
  AND x.employeeNumber = edwED.EmployeeNumber
AND edwED.CurrentRow = true
WHERE edwED.StoreCode IS NULL) b ON 1 =1       

SELECT b.*, a.*
FROM (
  SELECT 
  --        (SELECT MAX(EmployeeKey) + 1 FROM keyMapDimEmp), 
    x.StoreCode, x.Store, x.EmployeeNumber, 
    x.ActiveCode, x.Active, x.FullPartTimeCode, x.FullPartTime,
    x.PyDeptCode, x.PyDept, x.DistCode, x.Distribution, x.TechNumber,x.Name, x.BirthDate, 
    x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, true,
    x.HireDate AS EKFrom, x.HireDateKey AS EKFromKey,
    (SELECT thedate FROM day WHERE datetype = 'NA') AS EKThru,
    (SELECT datekey FROM day WHERE datetype = 'NA') as EKThruKey, 
    x.PayrollClassCode, x.PayrollClass, x.Salary, x.HourlyRate,
    x.PayPeriodCode, x.PayPeriod
  FROM xfmEmployeeDim x
  LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
    AND x.employeeNumber = edwED.EmployeeNumber
  AND edwED.CurrentRow = true
  WHERE edwED.StoreCode IS NULL) a  
full outer JOIN  tally b ON 1 = 1
WHERE b.n <= (
      SELECT COUNT(x.storecode)
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
  		  AND edwED.CurrentRow = true
      WHERE edwED.StoreCode IS NULL)   
      
SELECT b.*, a.*
FROM (
  SELECT 
  --        (SELECT MAX(EmployeeKey) + 1 FROM keyMapDimEmp), 
    x.StoreCode, x.Store, x.EmployeeNumber, 
    x.ActiveCode, x.Active, x.FullPartTimeCode, x.FullPartTime,
    x.PyDeptCode, x.PyDept, x.DistCode, x.Distribution, x.TechNumber,x.Name, x.BirthDate, 
    x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, true,
    x.HireDate AS EKFrom, x.HireDateKey AS EKFromKey,
    (SELECT thedate FROM day WHERE datetype = 'NA') AS EKThru,
    (SELECT datekey FROM day WHERE datetype = 'NA') as EKThruKey, 
    x.PayrollClassCode, x.PayrollClass, x.Salary, x.HourlyRate,
    x.PayPeriodCode, x.PayPeriod
  FROM xfmEmployeeDim x
  LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
    AND x.employeeNumber = edwED.EmployeeNumber
  AND edwED.CurrentRow = true
  WHERE edwED.StoreCode IS NULL) a,  tally b 
WHERE b.n <= (
      SELECT COUNT(x.storecode)
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
  		  AND edwED.CurrentRow = true
      WHERE edwED.StoreCode IS NULL)       

/* dimEmp *********************************************************************/              
              
/* BridgeTechGroup *********************************************************************/               
DROP TABLE keymapBridgeTechGroup;
CREATE TABLE keyMapBridgeTechGroup ( 
      TechGroupKey integer,
      TechKey Integer,
      WeightFactor double) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keymapBridgeTechGroup',
   'keymapBridgeTechGroup.adi',
   'TechGroupKey',
   'TechGroupKey',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'keymapBridgeTechGroup',
   'keymapBridgeTechGroup.adi',
   'NK',
   'TechKey;WeightFactor',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'keymapBridgeTechGroup',
   'keymapBridgeTechGroup.adi',
   'ALL',
   'TechGroupKey;TechKey;WeightFactor',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapBridgeTechGroup', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'keyMapBridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapBridgeTechGroup', 
   'Table_Primary_Key', 
   'TechGroupKey', 'APPEND_FAIL', 'keyMapBridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapBridgeTechGroup', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'keyMapBridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapBridgeTechGroup', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'keyMapBridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keyMapBridgeTechGroup', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'keyMapBridgeTechGroupfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapBridgeTechGroup', 
      'TechGroupKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapBridgeTechGroupfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapBridgeTechGroup', 
      'TechKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapBridgeTechGroupfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapBridgeTechGroup', 
      'WeightFactor', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keyMapBridgeTechGroupfail' ); 


      
INSERT INTO keyMapBridgeTechGroup     
SELECT TechGroupKey, TechKey, WeightFactor
FROM bridgeTechGroup;  

/* BridgeTechGroup *********************************************************************/

/* dimTech *********************************************************************/
DROP TABLE keyMapDimTech;
CREATE TABLE keyMapDimTech ( 
      TechKey integer,
      StoreCode CIChar( 3 ),
      TechNumber CIChar( 3 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keymapDimTech',
   'keymapDimTech.adi',
   'TECHKEY',
   'TechKey',
   '',
   2051,
   512,
   '' ); 

EXECUTE PROCEDURE sp_CreateIndex90( 
   'keymapDimTech',
   'keymapDimTech.adi',
   'NK',
   'StoreCode;TechNumber',
   '',
   2051,
   512,
   '' ); 
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keymapDimTech',
   'keymapDimTech.adi',
   'All',
   'TechKey;StoreCode;TechNumber',
   '',
   2051,
   512,
   '' );    

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keymapDimTech', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'keymapDimTechfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keymapDimTech', 
   'Table_Primary_Key', 
   'TECHKEY', 'APPEND_FAIL', 'keymapDimTechfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keymapDimTech', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'keymapDimTechfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keymapDimTech', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'keymapDimTechfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'keymapDimTech', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'keymapDimTechfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keymapDimTech', 
      'TechKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keymapDimTechfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keymapDimTech', 
      'StoreCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keymapDimTechfail' ); 


EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keymapDimTech', 
      'TechNumber', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'keymapDimTechfail' ); 

INSERT INTO keyMapDimTech    
SELECT TechKey, StoreCode, TechNumber
FROM dimTech;  
/* dimTech *********************************************************************/
