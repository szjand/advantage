-- 2/13/14
-- yay, FINALLY get to DO a junk dimension
-- for factVehicleSale
DROP TABLE dimCarDealInfo;
CREATE TABLE dimCarDealInfo (
  carDealInfoKey autoinc,
  dealStatusCode cichar(1) constraint NOT NULL,
  dealStatus cichar(12) constraint NOT NULL,
  dealTypeCode cichar(1) constraint NOT NULL,
  dealType cichar(24) constraint NOT NULL,
  vehicleTypeCode cichar(1),
  vehicleType cichar(4) constraint NOT NULL,
  saleTypeCode cichar(1) constraint NOT NULL,
  saleType cichar(12) constraint NOT NULL,
  constraint PK Primary Key (carDealInfoKey)) IN database;
-- unique index  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCarDealInfo','dimCarDealInfo.adi','NK',
   'dealStatusCode;dealTypeCode;vehicleTypeCode;saleTypeCode','',2051,512,'' );    
   
-- nightly DO test for a new combination

INSERT INTO dimCarDealInfo (dealStatusCode, dealStatus, dealTypeCode, dealType,
  vehicleTypeCode, vehicleType, saleTypeCode, saleType) 
SELECT bmstat,
  CASE bmstat 
    WHEN 'A' THEN 'Accepted' 
    WHEN 'U' THEN 'Capped'
    ELSE NULL 
  END, 
  bmtype,
  CASE bmtype
    WHEN 'C' THEN 'Cash, Wholesale, Fleet'
    WHEN 'F' THEN 'Financed Retail'
    WHEN 'L' THEN 'Financed Lease'
    WHEN 'O' THEN 'Cash Owner Finance'
    ELSE NULL 
  END,
  bmvtyp,
  CASE bmvtyp
    WHEN 'N' THEN 'New'
    WHEN 'U' THEN 'Used'
    ELSE NULL 
  END,
  bmwhsl,
  CASE bmwhsl
    WHEN 'F' THEN 'Fleet'
    WHEN 'L' THEN 'Lease'
    WHEN 'R' THEN 'Retail'
    WHEN 'W' THEN 'Wholesale'
    WHEN 'X' THEN 'Dealer Xfer'
    ELSE NULL 
  END
FROM (
  SELECT bmstat, bmtype, bmvtyp, bmwhsl
  FROM stgArkonaBOPMAST
  WHERE bmco# IN ('ry1','ry2') 
    AND bmstat <> ''
  GROUP BY bmstat, bmtype, bmvtyp, bmwhsl) a;

-- new rows  
INSERT INTO dimCarDealInfo (dealStatusCode, dealStatus, dealTypeCode, dealType,
  vehicleTypeCode, vehicleType, saleTypeCode, saleType) 
  
INSERT INTO dimCarDealInfo (dealStatusCode, dealStatus, dealTypeCode, dealType,
  vehicleTypeCode, vehicleType, saleTypeCode, saleType)   
SELECT bmstat,
  CASE bmstat 
    WHEN 'A' THEN 'Accepted' 
    WHEN 'U' THEN 'Capped'
    ELSE NULL 
  END, 
  bmtype,
  CASE bmtype
    WHEN 'C' THEN 'Cash, Wholesale, Fleet'
    WHEN 'F' THEN 'Financed Retail'
    WHEN 'L' THEN 'Financed Lease'
    WHEN 'O' THEN 'Cash Owner Finance'
    ELSE NULL 
  END,
  bmvtyp,
  CASE bmvtyp
    WHEN 'N' THEN 'New'
    WHEN 'U' THEN 'Used'
    ELSE NULL 
  END,
  bmwhsl,
  CASE bmwhsl
    WHEN 'F' THEN 'Fleet'
    WHEN 'L' THEN 'Lease'
    WHEN 'R' THEN 'Retail'
    WHEN 'W' THEN 'Wholesale'
    WHEN 'X' THEN 'Dealer Xfer'
    ELSE NULL 
  END
FROM (  
  SELECT *
  FROM (   
    SELECT bmstat, bmtype, bmvtyp, bmwhsl
    FROM stgArkonaBOPMAST
    WHERE bmco# IN ('ry1','ry2') 
      AND bmstat <> ''
    GROUP BY bmstat, bmtype, bmvtyp, bmwhsl) a   
  WHERE NOT EXISTS (
    SELECT 1
    FROM dimCarDealInfo
    WHERE dealstatuscode = a.bmstat
      AND dealtypecode = a.bmtype
      AND vehicletypecode = a.bmvtyp
      AND saletypecode = a.bmwhsl)) c ; 
  

-- 3/22/14
/* 
  fuck missing values, cartesian of all possible combinations is only 64 rows which
  is only 43 more than current "real" world combinations
  go ahead AND flesh out dimCarDealInfo with ALL the cartesian possibilities
  (minus saletype X: DealerXfer which has never been used)
*/

INSERT INTO dimCarDealInfo (dealStatusCode, dealStatus, dealTypeCode, 
  dealType, vehicleTypeCode, vehicleType, saleTypeCode, saleType)
SELECT dealstatuscode,
  CASE dealstatuscode 
  WHEN 'A' THEN 'Accepted' 
  WHEN 'U' THEN 'Capped'
  ELSE NULL 
END, 
  dealtypecode,
  CASE dealtypecode
    WHEN 'C' THEN 'Cash, Wholesale, Fleet'
    WHEN 'F' THEN 'Financed Retail'
    WHEN 'L' THEN 'Financed Lease'
    WHEN 'O' THEN 'Cash Owner Finance'
    ELSE NULL 
  END, 
  vehicletypecode,
  CASE vehicletypecode
    WHEN 'N' THEN 'New'
    WHEN 'U' THEN 'Used'
    ELSE NULL 
  END,  
  saletypecode,
  CASE saletypecode
    WHEN 'F' THEN 'Fleet'
    WHEN 'L' THEN 'Lease'
    WHEN 'R' THEN 'Retail'
    WHEN 'W' THEN 'Wholesale'
    WHEN 'X' THEN 'Dealer Xfer'
    ELSE NULL 
  END    
-- SELECT COUNT(*)  
FROM (
  SELECT DISTINCT *
  FROM (
    SELECT DISTINCT dealstatuscode
    FROM dimcardealinfo) a,
    (SELECT DISTINCT dealtypecode
    FROM dimcardealinfo) b,
    (SELECT DISTINCT vehicletypecode
    FROM dimcardealinfo) c,
    (SELECT DISTINCT saletypecode
    FROM dimcardealinfo) d) x  
  WHERE NOT EXISTS (
    SELECT 1
    FROM dimcardealinfo
    WHERE dealstatuscode = x.dealstatuscode
      AND dealtypecode = x.dealtypecode
      AND vehicletypecode = x.vehicletypecode
      AND saletypecode = x.saletypecode)  
      
-- 3/1/15: ADD a row to identify Intra Market Wholesale
-- always capped, used, AND Cash-Ws-Fleet
-- new saleTypeCode (I) AND saleType (Intra Market Wholesale)
-- shit, saleType IS only cichar(12)
ALTER TABLE dimCarDealInfo
ALTER COLUMN saleType saleType cichar(24);
INSERT INTO dimCarDealInfo (dealStatusCode,dealStatus,dealTypeCode,dealType,
  vehicleTypeCode,vehicleType,saleTypeCode,saleType)
SELECT dealStatusCode,dealStatus,dealTypeCode,dealType,vehicleTypeCode,
  vehicleType, 'I', 'Intra Market Wholesale'
FROM dimCarDealInfo
WHERE vehicleType = 'used'  
  AND dealStatus = 'Capped' 
  AND dealType = 'Cash, Wholesale, Fleet'   
  AND saleType = 'Wholesale';