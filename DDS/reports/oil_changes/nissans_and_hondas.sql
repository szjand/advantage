-- ry1 opcode LIKE pdq  lof
-- ry2 opcode LIKE lof OR pdqcat1 = lof
select b.thedate, c.modelyear, c.model, d.*, a.*
-- SELECT DISTINCT opcode, description, pdqcat1
FROM factrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
  AND b.thedate BETWEEN curdate() - 366 AND curdate()
INNER JOIN dimVehicle c on a.vehiclekey = c.vehiclekey
  AND c.make = 'nissan'  
  AND c.modelyear IN ('2014','2015','2016')
INNER JOIN dimopcode d on a.opcodekey = d.opcodekey  
WHERE a.storecode = 'ry2'
  AND d.description LIKE '%oil%'
  
  
select a.storecode, b.thedate, c.modelyear, c.model, d.opcode
SELECT storecode, COUNT(*)
FROM (
SELECT a.storecode, ro
FROM factrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
  AND b.thedate BETWEEN curdate() - 366 AND curdate()
INNER JOIN dimVehicle c on a.vehiclekey = c.vehiclekey
  AND c.make = 'nissan'  
  AND c.modelyear IN ('2014','2015','2016')
INNER JOIN dimopcode d on a.opcodekey = d.opcodekey  
WHERE d.opcode LIKE '%pdq%'
  OR d.opcode LIKE '%lof%'
  OR d.pdqcat1 = 'lof'
GROUP BY a.storecode, ro) x
GROUP BY storecode 


SELECT storecode, COUNT(*)
FROM (
SELECT a.storecode, ro
FROM factrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
  AND b.thedate BETWEEN curdate() - 366 AND curdate()
INNER JOIN dimVehicle c on a.vehiclekey = c.vehiclekey
  AND c.make = 'honda'  
  AND c.modelyear IN ('2014','2015','2016')
INNER JOIN dimopcode d on a.opcodekey = d.opcodekey  
WHERE d.opcode LIKE '%pdq%'
  OR d.opcode LIKE '%lof%'
  OR d.pdqcat1 = 'lof'
GROUP BY a.storecode, ro) x
GROUP BY storecode 