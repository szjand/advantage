SELECT b.stocknumber, c.vin, CAST(a.soldts AS sql_date) AS saleDate, d.*
FROM dds.vehicleSales a
INNER JOIN dds.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN dds.VehicleItems c on b.VehicleItemID = c.VehicleItemID  
LEFT JOIN dds.selectedReconPAckages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.selectedReconPackageTS = (
    SELECT MAX(selectedReconPackageTS)
    FROM dds.selectedReconPAckages
    WHERE VehicleInventoryItemID = d.VehicleInventoryItemID)
WHERE year(a.soldts) = 2014
  AND a.typ = 'VehicleSale_Retail'
  AND a.status = 'VehicleSale_Sold'
  
  
SELECT *
FROM (  
  SELECT b.stocknumber, c.vin, CAST(a.soldts AS sql_date) AS saleDate, d.*
  FROM vehicleSales a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN VehicleItems c on b.VehicleItemID = c.VehicleItemID  
  LEFT JOIN selectedReconPAckages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.selectedReconPackageTS = (
      SELECT MAX(selectedReconPackageTS)
      FROM selectedReconPAckages
      WHERE VehicleInventoryItemID = d.VehicleInventoryItemID)
  WHERE year(a.soldts) = 2014
    AND a.typ = 'VehicleSale_Retail'
    AND a.status = 'VehicleSale_Sold'
    AND vin NOT IN ( -- exclude multiple sales of same vehicle
      SELECT vin FROM (
      SELECT b.stocknumber, c.vin, CAST(a.soldts AS sql_date) AS saleDate, d.*
      FROM vehicleSales a
      INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      INNER JOIN VehicleItems c on b.VehicleItemID = c.VehicleItemID  
      LEFT JOIN selectedReconPAckages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
        AND d.selectedReconPackageTS = (
          SELECT MAX(selectedReconPackageTS)
          FROM selectedReconPAckages
          WHERE VehicleInventoryItemID = d.VehicleInventoryItemID)
      WHERE year(a.soldts) = 2014
        AND a.typ = 'VehicleSale_Retail'
        AND a.status = 'VehicleSale_Sold') x GROUP BY vin HAVING COUNT(*) > 1)
    AND d.typ = 'ReconPackage_Nice' ) z  
    

-- this IS probably good enuf 
-- nice care sales, 1/1/2014 to date
-- DROP TABLE #nice;
SELECT stocknumber, vin, saledate
INTO #nice
FROM (
  SELECT b.stocknumber, c.vin, CAST(a.soldts AS sql_date) AS saleDate, d.*
  FROM dps.vehicleSales a
  INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID  
  LEFT JOIN dps.selectedReconPAckages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.selectedReconPackageTS = (
      SELECT MAX(selectedReconPackageTS)
      FROM dps.selectedReconPAckages
      WHERE VehicleInventoryItemID = d.VehicleInventoryItemID)
  WHERE year(a.soldts) = 2014
    AND a.typ = 'VehicleSale_Retail'
    AND a.status = 'VehicleSale_Sold') e     
WHERE e.typ = 'ReconPackage_Nice'   

-- accounting: nice care labor sales: 146305, 246305

SELECT distinct a.*, c.ro, c.line, d.opcode, coalesce(c.laborsales, 0) AS laborsales
FROM #nice a
INNER JOIN dimVehicle b on a.vin = b.vin
left JOIN factRepairOrder c on b.vehicleKey = c.vehicleKey
left JOIN dimOpcode d on c.opcodekey = d.opcodekey
  AND d.description LIKE '%nice care%'
  
SELECT *
FROM (
  SELECT distinct a.*, c.ro, d.opcode
  FROM #nice a
  INNER JOIN dimVehicle b on a.vin = b.vin
  INNER JOIN factRepairOrder c on b.vehicleKey = c.vehicleKey
  INNER JOIN dimOpcode d on c.opcodekey = d.opcodekey
    AND d.description LIKE '%nice care%') e  
LEFT JOIN stgArkonaGLPTRNS f on e.ro = f.gtctl#
  AND f.gtacct IN ('146305','246305')  