SELECT name, pydeptcode, pydept, FullPartTime
FROM edwEmployeeDim
WHERE storecode = 'ry1'
AND Active = 'Active'
AND CurrentRow = true
ORDER BY name


1)	All employees for both Honda and GM store with employee name and department
2)	All new employees hired from Jan 2012 to June 2012 
      with the department and whether full time or part time
3)	All terminations from Jan 2012 to June 

1)
SELECT store, name, pydept
FROM edwEmployeeDim
WHERE storecode in ('ry1', 'ry2')
AND Active = 'Active'
AND CurrentRow = true
ORDER BY name
-- need to check
SELECT name, COUNT(*)
FROM edwEmployeeDim
WHERE storecode in ('ry1', 'ry2')
AND Active = 'Active'
AND CurrentRow = true
GROUP BY name
HAVING COUNT(*) > 1
