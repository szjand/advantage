/*
11/30/2016
Could I please have you pull two censuses (sp?) with SSN, First Name, MI, Last Name, Suffix, Address, 
City, State, Zip, Hire Date and Term Date for everyone that was employed here at any time during 2016? 
I need that separately for each store when you can for ACA reporting.  
*/

select b.ssn AS SSN, a.firstname AS FIRST_NAME, left(a.middlename, 1) AS MI, a.lastname AS LAST_NAME, 
  c.ymadd1 AS ADDRESS, c.ymcity AS CITY, c.ymstat AS STATE, c.ymzip AS ZIP, a.hiredate AS HIRE_DATE, 
  case when a.termdate > curdate() THEN CAST(NULL AS sql_date) else termdate END AS TERM_DATE
FROM edwEmployeeDim a
LEFT JOIN tmpssn b on a.employeenumber = b.employeenumber
LEFT JOIN stgArkonaPYMAST c on a.employeenumber = c.ymempn
WHERE currentrow = true
  AND storecode = 'RY1'
  AND hiredate <= curdate()
  AND termdate > '12/31/2015'
ORDER BY lastname;

select b.ssn AS SSN, a.firstname AS FIRST_NAME, left(a.middlename, 1) AS MI, a.lastname AS LAST_NAME, 
  c.ymadd1 AS ADDRESS, c.ymcity AS CITY, c.ymstat AS STATE, c.ymzip AS ZIP, a.hiredate AS HIRE_DATE, 
  case when a.termdate > curdate() THEN CAST(NULL AS sql_date) else termdate END AS TERM_DATE
FROM edwEmployeeDim a
LEFT JOIN tmpssn b on a.employeenumber = b.employeenumber
LEFT JOIN stgArkonaPYMAST c on a.employeenumber = c.ymempn
WHERE currentrow = true
  AND storecode = 'RY2'
  AND hiredate <= curdate()
  AND termdate > '12/31/2015'
ORDER BY lastname;

/*
Could you also please tell me the total number of employees (including both full-time and part-time) 
for each month of the 2016 year for each store?  
*/

SELECT 'RY1' AS Store ,
  SUM(CASE WHEN hiredate <= '01/31/2016' AND termdate >= '01/01/2016' THEN 1 END) AS Jan,
  SUM(CASE WHEN hiredate <= '02/29/2016' AND termdate >= '02/01/2016' THEN 1 END) AS Feb,
  SUM(CASE WHEN hiredate <= '03/31/2016' AND termdate >= '03/01/2016' THEN 1 END) AS Mar,
  SUM(CASE WHEN hiredate <= '04/30/2016' AND termdate >= '04/01/2016' THEN 1 END) AS Apr,
  SUM(CASE WHEN hiredate <= '05/31/2016' AND termdate >= '05/01/2016' THEN 1 END) AS May,
  SUM(CASE WHEN hiredate <= '06/30/2016' AND termdate >= '06/01/2016' THEN 1 END) AS Jun,
  SUM(CASE WHEN hiredate <= '07/31/2016' AND termdate >= '07/01/2016' THEN 1 END) AS Jul,
  SUM(CASE WHEN hiredate <= '08/31/2016' AND termdate >= '08/01/2016' THEN 1 END) AS Aug,
  SUM(CASE WHEN hiredate <= '09/30/2016' AND termdate >= '09/01/2016' THEN 1 END) AS Sep,
  SUM(CASE WHEN hiredate <= '10/31/2016' AND termdate >= '10/01/2016' THEN 1 END) AS Oct,
  SUM(CASE WHEN hiredate <= '11/30/2016' AND termdate >= '11/01/2016' THEN 1 END) AS Nov,
  SUM(CASE WHEN hiredate <= '12/31/2016' AND termdate >= '01/01/2017' THEN 1 END) AS Dec
FROM edwEmployeeDim
WHERE currentrow = true
  AND storecode = 'ry1'
UNION   	
SELECT 'RY2' AS Store ,
  SUM(CASE WHEN hiredate <= '01/31/2016' AND termdate >= '01/01/2016' THEN 1 END) AS Jan,
  SUM(CASE WHEN hiredate <= '02/29/2016' AND termdate >= '02/01/2016' THEN 1 END) AS Feb,
  SUM(CASE WHEN hiredate <= '03/31/2016' AND termdate >= '03/01/2016' THEN 1 END) AS Mar,
  SUM(CASE WHEN hiredate <= '04/30/2016' AND termdate >= '04/01/2016' THEN 1 END) AS Apr,
  SUM(CASE WHEN hiredate <= '05/31/2016' AND termdate >= '05/01/2016' THEN 1 END) AS May,
  SUM(CASE WHEN hiredate <= '06/30/2016' AND termdate >= '06/01/2016' THEN 1 END) AS Jun,
  SUM(CASE WHEN hiredate <= '07/31/2016' AND termdate >= '07/01/2016' THEN 1 END) AS Jul,
  SUM(CASE WHEN hiredate <= '08/31/2016' AND termdate >= '08/01/2016' THEN 1 END) AS Aug,
  SUM(CASE WHEN hiredate <= '09/30/2016' AND termdate >= '09/01/2016' THEN 1 END) AS Sep,
  SUM(CASE WHEN hiredate <= '10/31/2016' AND termdate >= '10/01/2016' THEN 1 END) AS Oct,
  SUM(CASE WHEN hiredate <= '11/30/2016' AND termdate >= '11/01/2016' THEN 1 END) AS Nov,
  SUM(CASE WHEN hiredate <= '12/31/2016' AND termdate >= '01/01/2017' THEN 1 END) AS Dec
FROM edwEmployeeDim
WHERE currentrow = true
  AND storecode = 'ry2'
  
/*
for each store for any transition BETWEEN full AND part time,
name, date AND transition
*/

select storecode as store, employeenumber AS "emp#", name, rowchangedate AS date,
  case fullparttime
    WHEN 'part' THEN 'Part to Full Time'
	WHEN 'full' THEN 'Full to Part Time'
  END AS transition 
FROM edwEmployeeDim
WHERE rowchangereason LIKE '%full%'
  AND rowchangedate BETWEEN '01/01/2016' AND curdate()
ORDER BY store, name

  