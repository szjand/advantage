SELECT a.employeenumber AS [Emp #], a.lastname AS [Last Name], 
  a.firstname AS [First Name], b.ymsex AS Gender,b.ymhdte AS [Hire Date], 
  CASE b.ymtdte
    WHEN '12/31/9999' THEN CAST(NULL AS sql_date)
    ELSE b.ymtdte
  END AS [Term Date], 
  b.ymstre AS Address, b.ymcity AS City, b.ymstat AS State, b.ymzip AS Zip, 
  left(d.yrtext, 50) AS Job,
  trim(b.ymarea) + b.ymphon AS [Home Phone]
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYMAST b on a.storecode = b.ymco#
  AND a.employeenumber = b.ymempn 
LEFT JOIN stgArkonaPYPRHEAD c on a.employeenumber = c.yrempn
  AND c.yrjobd <> ''
  AND a.storecode = c.yrco#
LEFT JOIN stgArkonaPYPRJOBD d on c.yrjobd = d.yrjobd
  AND d.yrtext NOT IN ('')--('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
  AND c.yrco# = d.yrco#    
WHERE a.currentrow = true
  AND a.termdate > '12/31/2012'
  AND a.storecode = 'RY1'
ORDER BY a.lastname, a.firstname



SELECT [Emp #]
FROM (
SELECT a.employeenumber AS [Emp #], a.lastname AS [Last Name], 
  a.firstname AS [First Name], b.ymsex AS Gender,b.ymhdte AS [Hire Date], 
  CASE b.ymtdte
    WHEN '12/31/9999' THEN CAST(NULL AS sql_date)
    ELSE b.ymtdte
  END AS [Term Date], 
  b.ymstre AS Address, b.ymcity AS City, b.ymstat AS State, b.ymzip AS Zip, 
  left(d.yrtext, 50) AS Job,
  trim(b.ymarea) + b.ymphon AS [Home Phone]
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYMAST b on a.storecode = b.ymco#
  AND a.employeenumber = b.ymempn 
LEFT JOIN stgArkonaPYPRHEAD c on a.employeenumber = c.yrempn
  AND c.yrjobd <> ''
  AND a.storecode = c.yrco#
LEFT JOIN stgArkonaPYPRJOBD d on c.yrjobd = d.yrjobd
  AND d.yrtext NOT IN ('')--('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
  AND c.yrco# = d.yrco#    
WHERE a.currentrow = true
  AND a.termdate > '12/31/2012'
  AND a.storecode = 'RY1') x GROUP BY [Emp #] HAVING COUNT(*) > 1
  