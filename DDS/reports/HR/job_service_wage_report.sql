﻿select a.employee_name, a.department_code, d.description, a.active_code, c.data, a.payroll_class, a.pay_period, a.base_salary, a.base_hrly_rate, a.alt_salary, a.alt_hrly_rate
-- select a.* 
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
left join (
  select distinct company_number, job_Description, data
  from arkona.ext_pyprjobd) c on b.job_description = c.job_description
    and b.company_number = c.company_number
    and c.data not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') 
left join arkona.ext_pypclkctl d on a.department_code = d.department_code 
  and a.pymast_company_number = d.company_number
where a.current_row = true
  and a.active_code <> 'T'
  and a.payroll_class = 'H'
  and a.pymast_company_number = 'ry1'
order by employee_name  


select a.employee_name, a.department_code, d.description, a.active_code, c.data, a.payroll_class, a.pay_period, (26 * a.base_salary)::integer as annual_salary, a.base_hrly_rate, a.alt_salary, a.alt_hrly_rate
-- select a.* 
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
left join (
  select distinct company_number, job_Description, data
  from arkona.ext_pyprjobd) c on b.job_description = c.job_description
    and b.company_number = c.company_number
    and c.data not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') 
left join arkona.ext_pypclkctl d on a.department_code = d.department_code 
  and a.pymast_company_number = d.company_number
where a.current_row = true
  and a.active_code <> 'T'
  and a.payroll_class = 'S'
  and a.pymast_company_number = 'ry1'
order by employee_name  

drop table if exists js_wage;
create temp table js_wage as
select a.employee_name, c.data as "job title", null::numeric as "hourly wage", (26 * sum(total_gross_pay)/count(*))::integer as "annual wage", d.description as department
-- select a.* 
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
left join (
  select distinct company_number, job_Description, data
  from arkona.ext_pyprjobd) c on b.job_description = c.job_description
    and b.company_number = c.company_number
    and c.data not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') 
left join arkona.ext_pypclkctl d on a.department_code = d.department_code 
  and a.pymast_company_number = d.company_number
left join arkona.ext_pyhshdta f on a.pymast_employee_number = f.employee_
  and f.payroll_cen_year = 118
where a.current_row = true
  and a.active_code <> 'T'
  and a.payroll_class not in ('H','S')
  and a.pymast_company_number = 'ry1'
group by a.employee_name, c.data, d.description
union 
select a.employee_name, c.data, null::numeric as "hourly wage", (26 * a.base_salary)::integer as annual_salary, d.description as department
-- select a.* 
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
left join (
  select distinct company_number, job_Description, data
  from arkona.ext_pyprjobd) c on b.job_description = c.job_description
    and b.company_number = c.company_number
    and c.data not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') 
left join arkona.ext_pypclkctl d on a.department_code = d.department_code 
  and a.pymast_company_number = d.company_number
where a.current_row = true
  and a.active_code <> 'T'
  and a.payroll_class = 'S'
  and a.pymast_company_number = 'ry1'
union  
select a.employee_name, c.data, a.base_hrly_rate, null as "annual wage", d.description
-- select a.* 
from arkona.xfm_pymast a
left join arkona.ext_pyprhead b on a.pymast_employee_number = b.employee_number
left join (
  select distinct company_number, job_Description, data
  from arkona.ext_pyprjobd) c on b.job_description = c.job_description
    and b.company_number = c.company_number
    and c.data not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE') 
left join arkona.ext_pypclkctl d on a.department_code = d.department_code 
  and a.pymast_company_number = d.company_number
where a.current_row = true
  and a.active_code <> 'T'
  and a.payroll_class = 'H'
  and a.pymast_company_number = 'ry1'
order by employee_name    

select employee_name
from js_wage
group by employee_name
having count(*) > 1


select *
from js_wage





