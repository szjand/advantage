﻿
/*
I have to complete an Equal Employment Opportunity survey using one month’s worth of data from last quarter.  

Would you be able to pull a report for me that shows everyone that worked here in December along with their gender, 
date of birth and their job title?  You can use Compli to get their job title and I will also need them 
separated out by GM and Honda stores.  We do have employees fill out an EEO Voluntary Self-Identification 
form in Compli, but I am not sure if you can pull that information from each employee’s file?  
That would be great if you could, but I think Taylor said we had issues with pulling that in the past.

*/

********************
POSTGRESQL
********************

select *
from ads.ext_edw_employee_dim
where hiredate < '01/01/2018'
  and termdate > '11/30/2017' 
  and currentrow = true
order by name

select a.pymast_company_number, a.employee_name, a.sex, (select dds.db2_integer_to_date(a.birth_date)), 
  case
    when a.employee_name = 'SHIREK, NICHOLAS' then 'General Sales Manager'
    when a.employee_name = 'MOORE, LANNY' then 'Hobby Shop Technician'
    else b.title
  end as job_title
from arkona.ext_pymast a
left join ads.ext_pto_compli_users b on a.pymast_employee_number = b.userid
where (select dds.db2_integer_to_date(hire_date)) < '01/01/2018'
  and (select dds.db2_integer_to_date(termination_date)) > '11/30/2017'
  and  b.userid <> '1141642'
order by pymast_company_number, a.employee_name  