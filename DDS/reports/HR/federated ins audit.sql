/*
I need an employee list  for  the federated ins audit on Thursday.  
If I could get it today that would be great so I can add any lic # we are missing.        

What I need on the report is name , hire date, term date, job title, 
part-time/full time, drivers lic#, and state  .  
the audit period is 9/1/13 to 09/01/14

10/6/15, need this again, the time range IN this query IS wrong, AND 
IS returning folks it should NOT, eg Zoellick, hired 4/1/04, still employed
need to redefine the data parameters

SELECT storecode, name, hiredate, termdate, fullparttime 
FROM edwEmployeeDim
WHERE hiredate <= '09/01/2014'
  AND termdate > '08/31/2013'
  AND currentrow = true

--SELECT ymempn FROM (  
select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdto AS [Hire Date], 
  CASE
    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
    ELSE a.ymtdte
  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN ''
  END AS [Full/Part Time],
  a.ymdriv AS [DL #], a.ymdvst AS [DL State] 
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
WHERE ymhdto <= '09/01/2014'
  AND ymtdte > '08/31/2013'
--) x GROUP BY ymempn HAVING COUNT(*) > 1
ORDER BY name
*/
/*
ymhdte: hire date
ymhdto: orig hire date
ymtdte: term date
*/
DECLARE @from_date date;
DECLARE @thru_date date;
@from_date = '09/01/2014';
@thru_date = '08/31/2015';

select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdte AS [Hire Date],
  a.ymhdto AS [Orig Hire Date], 
--  a.ymtdte AS [Term Date],
  CASE
    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
    ELSE a.ymtdte
  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time]
--  a.ymdriv AS [DL #], a.ymdvst AS [DL State] 
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
WHERE ymhdte BETWEEN @from_date AND @thru_date
  OR ymhdto BETWEEN @from_date AND @thru_date
  OR ymhdte BETWEEN @from_date AND @thru_date  
ORDER BY name;

-- AND current employees
select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdte AS [Hire Date],
--  a.ymhdto AS [Orig Hire Date], 
--  a.ymtdte AS [Term Date],
--  CASE
--    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
--    ELSE a.ymtdte
--  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time],
  a.ymdriv AS [DL #], a.ymdvst AS [DL State]
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
WHERE a.ymactv <> 'T'
ORDER BY name;


I also need a list of all employees who worked during the audit period�probably like the one you did last year.

I am looking for a list like this.  You pulled this last year for me for the Federated audit.  
Employee list would be 9/1/16 to 8/31/17.

Thanks,

Jeri Schmiess Penas, CPA

DECLARE @from_date date;
DECLARE @thru_date date;
@from_date = '09/01/2016';
@thru_date = '08/31/2017';

select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdte AS [Hire Date],
  a.ymhdto AS [Orig Hire Date], 
--  a.ymtdte AS [Term Date],
  CASE
    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
    ELSE a.ymtdte
  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time]
--  a.ymdriv AS [DL #], a.ymdvst AS [DL State] 
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
WHERE ymhdto <= @thru_date
  AND ymtdte >= @from_date
ORDER BY name;


-------------------------------------------------------------------------------
Can you pull this list for me again?  9/1/17-8/31/18.

Thanks,

Jeri

9/30/2019
Will you please run this list for me again?  9/1/18 � 8/31/19. 

Thanks.

/*
    DECLARE @from_date date;
    DECLARE @thru_date date;
    @from_date = '09/01/2018';
    @thru_date = '08/31/2019';
    
    select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
      a.ymhdte AS [Hire Date],
      a.ymhdto AS [Orig Hire Date], 
    --  a.ymtdte AS [Term Date],
      CASE
        WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
        ELSE a.ymtdte
      END AS [Term Date],
      c.yrtext AS Job,
      CASE a.ymactv
        WHEN 'A' THEN 'Full'
        WHEN 'P' THEN 'Part'
        WHEN 'T' THEN d.fullparttime
      END AS [Full/Part Time]
    --  a.ymdriv AS [DL #], a.ymdvst AS [DL State] 
    FROM stgArkonaPYMAST a
    LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
    LEFT JOIN (
      select distinct yrco#, yrjobd, yrtext
      FROM stgArkonaPYPRJOBD
      WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
    LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
      AND d.currentrow = true  
    WHERE ymhdto <= @thru_date
      AND ymtdte >= @from_date
    ORDER BY name;
*/

-- AND current employees w/DL info
-- fucking drivers licenses are no longer IN the data
-- run a report LIKE for ssn, TABLE dds.tmp_dl

9/30/19 no need for the above, only interested IN currently employed folks
DO NOT need the date range


CREATE TABLE tmp_dl (
  employee_number cichar(12),
  name cichar(40),
  dl cichar(20));
  
delete FROM tmp_dl

SELECT COUNT(*) FROM tmp_dl
  
select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
--  a.ymhdte AS [Hire Date],
--  a.ymhdto AS [Orig Hire Date], 
--  a.ymtdte AS [Term Date],
--  CASE
--    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
--    ELSE a.ymtdte
--  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time],
--  a.ymdriv AS [DL #], 
  e.dl [DL #], a.ymdvst AS [DL State]
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
LEFT JOIN tmp_dl e on a.ymempn = e.employee_number
WHERE a.ymactv <> 'T'
ORDER BY name;




-------------------------------------------------------
12/4/18
Is there anyway that you can run this same list with total hours worked from 9/1/17-8/31/18?

9/30/19
Will you please run this report for 9/1/18 � 8/31/19?


DECLARE @from_date date;
DECLARE @thru_date date;
@from_date = '09/01/2018';
@thru_date = '08/31/2019';

-- DROP TABLE #wtf
select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdte AS [Hire Date],
  a.ymhdto AS [Orig Hire Date], 
--  a.ymtdte AS [Term Date],
  CASE
    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
    ELSE a.ymtdte
  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time]
--  a.ymdriv AS [DL #], a.ymdvst AS [DL State] 
INTO #wtf
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
WHERE ymhdto <= @thru_date
  AND ymtdte >= @from_date
ORDER BY name;

-- DROP TABLE #wtf_1;
SELECT employee_number, name, cast(SUM(clockhours) AS sql_integer) AS clock_hours
INTO #wtf_1
FROM (
  SELECT  b.employeekey, a.[emp #] AS employee_number, a.name, c.*
  FROM #wtf a
  left JOIN edwEmployeeDim b on [emp #] = b.employeenumber 
    AND a.store = b.storecode -- added this for employees at both stores during interval
  LEFT JOIN edwClockHoursFact c on b.employeekey = c.employeekey
  INNER JOIN day d on c.datekey = d.datekey
    AND d.thedate BETWEEN '09/01/2018' AND '08/31/2019') e
group by employee_number, name  
ORDER BY name;

SELECT a.*, b.clock_hours
FROM #wtf a
LEFT JOIN #wtf_1 b on a.name = b.name 
  AND a.[emp #] = b.employee_number  -- added this for employees at both stores during interval


-------------------------------------------------------------------------------
09/16/20
jeri requesting both reports again
1. ALL employees employed during the period with clock hours
2. current employees with dl
period: 09/01/2019 thru 08/31/2020
drivers license number no longer IN pymast, get it FROM employee status report
was going to move this ALL to pg, but, fuck it, i am tired

-- report 1 -------------------------------------------------------------------
DECLARE @from_date date;
DECLARE @thru_date date;
@from_date = '09/01/2019';
@thru_date = '08/31/2020';

-- DROP TABLE #wtf
select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  a.ymhdte AS [Hire Date],
  a.ymhdto AS [Orig Hire Date], 
--  a.ymtdte AS [Term Date],
  CASE
    WHEN a.ymtdte > curdate() THEN CAST(NULL AS sql_date)
    ELSE a.ymtdte
  END AS [Term Date],
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time]
--  a.ymdriv AS [DL #], a.ymdvst AS [DL State] 
INTO #wtf
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
WHERE ymhdto <= @thru_date
  AND ymtdte >= @from_date
ORDER BY name;

-- DROP TABLE #wtf_1;
SELECT employee_number, name, cast(SUM(clockhours) AS sql_integer) AS clock_hours
INTO #wtf_1
FROM (
  SELECT  b.employeekey, a.[emp #] AS employee_number, a.name, c.*
  FROM #wtf a
  left JOIN edwEmployeeDim b on [emp #] = b.employeenumber 
    AND a.store = b.storecode -- added this for employees at both stores during interval
  LEFT JOIN edwClockHoursFact c on b.employeekey = c.employeekey
  INNER JOIN day d on c.datekey = d.datekey
    AND d.thedate BETWEEN @from_date AND @thru_date) e
group by employee_number, name  
ORDER BY name;

-- DROP TABLE #wtf_2
SELECT a.*, b.clock_hours
INTO #wtf_2
FROM #wtf a
LEFT JOIN #wtf_1 b on a.name = b.name 
  AND a.[emp #] = b.employee_number;  -- added this for employees at both stores during interval
  
select * FROM #wtf_2;  
SELECT * FROM #wtf
-- report 2 -------------------------------------------------------------------

-- TABLE IS already created, just need to empty it AND repopulate it with current
-- data
-- fuck i didn't document it but the status file IS a fucking mess AND there are hardly
-- any dls IN it
-- told jeri, she IS looking INTO it
-- of 1912 rows, 41 have a dl
-- don't panic about the file format, just sort BY COLUMN a, that give ALL
--  the employee info including DL on a single line
so i added a unique index on employee number IN tmp_dl 
AND THEN inserted what we have FROM the latest employee status report


CREATE TABLE tmp_dl (
  employee_number cichar(12),
  name cichar(40),
  dl cichar(20));
  
delete FROM tmp_dl

SELECT COUNT(*) FROM tmp_dl

select * FROM tmp_dl WHERE employee_number = '134120'

SELECT * FROM tmp_dl ORDER BY name
  
select a.ymco# AS Store, a.ymempn AS [Emp #], a.ymname AS Name, 
  c.yrtext AS Job,
  CASE a.ymactv
    WHEN 'A' THEN 'Full'
    WHEN 'P' THEN 'Part'
    WHEN 'T' THEN d.fullparttime
  END AS [Full/Part Time],
--  a.ymdriv AS [DL #], 
  e.dl [DL #], a.ymdvst AS [DL State]
FROM stgArkonaPYMAST a
LEFT JOIN stgArkonaPYPRHEAD b on a.ymco# = b.yrco# AND a.ymempn = b.yrempn
LEFT JOIN (
  select distinct yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD
  WHERE yrtext <> '') c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
LEFT JOIN edwEmployeeDim d on a.ymempn = d.employeenumber
  AND d.currentrow = true  
LEFT JOIN tmp_dl e on a.ymempn = e.employee_number
WHERE a.ymactv <> 'T'
ORDER BY name;