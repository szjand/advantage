-- employees currently classified AS part time, clock hours for 2014
SELECT storecode, employeenumber, firstname, lastname, c.sundayToSaturdayWeek, 
  c.thedate, b.clockHours,
  (SELECT DISTINCT sundaytosaturdayweekselectformat FROM day WHERE thedate = c.thedate) AS weekOf
FROM edwEmployeeDim a
INNER JOIN edwClockHoursFact b on a.employeekey = b.employeekey
  AND b.datekey IN (SELECT datekey FROM day WHERE theyear = 2014)
  AND clockhours > 0
INNER JOIN day c on b.datekey = c.datekey  
WHERE currentrow = true
  AND active = 'active'
  AND fullparttime = 'part';

-- DROP TABLE #wtf
SELECT storecode, employeenumber, firstname, lastname, weekOf, SUM(clockHours) AS clockhours
INTO #wtf
FROM (    
  SELECT storecode, employeenumber, firstname, lastname, c.sundayToSaturdayWeek, 
    c.thedate, b.clockHours,
    (SELECT DISTINCT sundaytosaturdayweekselectformat FROM day WHERE thedate = c.thedate) AS weekOf
  FROM edwEmployeeDim a
  inner JOIN edwClockHoursFact b on a.employeekey = b.employeekey
    AND b.datekey IN (SELECT datekey FROM day WHERE theyear = 2014)
    AND clockhours > 0
  inner JOIN day c on b.datekey = c.datekey  
  WHERE currentrow = true
    AND active = 'active'
  AND fullparttime = 'part') e    
GROUP BY storecode, employeenumber, firstname, lastname, weekOf
ORDER BY lastname, firstname

SELECT * FROM #wtf

SELECT b.storecode, b.employeenumber, b.firstname, b.lastname, b.minHoursPerWeek,
  (SELECT max(weekOF) FROM #wtf WHERE employeenumber = b.employeenumber and clockhours = b.minHoursPerWeek) minHoursWeek,
  b.maxHoursPerWeek,
  (SELECT max(weekOF) FROM #wtf WHERE employeenumber = b.employeenumber and clockhours = b.maxHoursPerWeek) maxHoursWeek,
  avgHoursPerWeek,
  (SELECT COUNT(*) FROM #wtf WHERE employeenumber = b.employeenumber AND clockhours > 30) AS weeksOver30Hours
FROM (
  SELECT DISTINCT storecode, employeenumber, firstname, lastname,
    (SELECT MIN(clockHours) FROM #wtf WHERE employeenumber = a.employeenumber) AS minHoursPerWeek,
    (SELECT MAX(clockHours) FROM #wtf WHERE employeenumber = a.employeenumber) AS maxHoursPerWeek,
    round((SELECT avg(clockHours) FROM #wtf WHERE employeenumber = a.employeenumber), 2) AS avgHoursPerWeek
  FROM #wtf a) b
ORDER BY lastname  

-- include hiredate, orighiredate

SELECT b.storecode, b.employeenumber, b.firstname, b.lastname, 
  c.ymhdte as hireDate, c.ymhdto as origHireDate, b.minHoursPerWeek,
  (SELECT max(weekOF) FROM #wtf WHERE employeenumber = b.employeenumber and clockhours = b.minHoursPerWeek) minHoursWeek,
  b.maxHoursPerWeek,
  (SELECT max(weekOF) FROM #wtf WHERE employeenumber = b.employeenumber and clockhours = b.maxHoursPerWeek) maxHoursWeek,
  avgHoursPerWeek,
  (SELECT COUNT(*) FROM #wtf WHERE employeenumber = b.employeenumber AND clockhours > 30) AS weeksOver30Hours
FROM (
  SELECT DISTINCT storecode, employeenumber, firstname, lastname,
    (SELECT MIN(clockHours) FROM #wtf WHERE employeenumber = a.employeenumber) AS minHoursPerWeek,
    (SELECT MAX(clockHours) FROM #wtf WHERE employeenumber = a.employeenumber) AS maxHoursPerWeek,
    round((SELECT avg(clockHours) FROM #wtf WHERE employeenumber = a.employeenumber), 2) AS avgHoursPerWeek
  FROM #wtf a) b
LEFT JOIN stgArkonaPYMAST c on b.employeenumber = c.ymempn  
ORDER BY lastname  


-- ronnie verzosa
SELECT storecode, employeenumber, firstname, lastname, weekOf, SUM(clockHours) AS clockhours
FROM (    
  SELECT storecode, employeenumber, firstname, lastname, c.sundayToSaturdayWeek, 
    c.thedate, b.clockHours,
    (SELECT DISTINCT sundaytosaturdayweekselectformat FROM day WHERE thedate = c.thedate) AS weekOf
  FROM edwEmployeeDim a
  inner JOIN edwClockHoursFact b on a.employeekey = b.employeekey
    AND b.datekey IN (SELECT datekey FROM day WHERE theyear >2013)
    AND clockhours > 0
  inner JOIN day c on b.datekey = c.datekey  
  WHERE a.employeenumber = '1145560') e    
GROUP BY storecode, employeenumber, firstname, lastname, weekOf
ORDER BY clockhours DESC

-- 2/10/15  jacob berry 113560 & hunter nelson 1100800

SELECT storecode, employeenumber, firstname, lastname, weekOf, SUM(clockHours) AS clockhours
FROM (    
  SELECT storecode, employeenumber, firstname, lastname, c.sundayToSaturdayWeek, 
    c.thedate, b.clockHours,
    (SELECT DISTINCT sundaytosaturdayweekselectformat FROM day WHERE thedate = c.thedate) AS weekOf
  FROM edwEmployeeDim a
  inner JOIN edwClockHoursFact b on a.employeekey = b.employeekey
    AND b.datekey IN (SELECT datekey FROM day WHERE theyear >2013)
    AND clockhours > 0
  inner JOIN day c on b.datekey = c.datekey  
  WHERE a.employeenumber = '1100800') e    
GROUP BY storecode, employeenumber, firstname, lastname, weekOf
ORDER BY clockhours DESC