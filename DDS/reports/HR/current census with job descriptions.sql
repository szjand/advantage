SELECT distinct storecode, lastname, firstname, employeenumber, pydeptcode, 
  pydept AS [dept descr], distcode, 
  distribution AS [distcode desc], payrollclass,
  fullparttime, b.yrjobd AS jobcode, yrtext AS jobdesc
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
  AND b.yrjobd <> ''
LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
  AND c.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
WHERE currentrow = true
  AND active = 'Active' 
 
SELECT COUNT(*) FROM (  
SELECT distinct storecode, lastname, firstname, employeenumber, pydept, distribution, payrollclass,
  fullparttime, b.yrjobd AS jobcode, yrtext AS jobdesc
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
  AND b.yrjobd <> ''
LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
  AND c.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
WHERE currentrow = true
  AND active = 'Active'  
) x

SELECT employeenumber FROM (  
SELECT distinct storecode, lastname, firstname, employeenumber, pydept, distribution, payrollclass,
  fullparttime, b.yrjobd AS jobcode, yrtext AS jobdesc
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
  AND b.yrjobd <> ''
LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
  AND c.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
WHERE currentrow = true
  AND active = 'Active'  
) x GROUP BY employeenumber HAVING COUNT(*) > 1



SELECT *
FROM (
  SELECT distinct storecode, lastname, firstname, employeenumber, pydept, distribution, payrollclass,
    fullparttime, b.yrjobd AS jobcode, yrtext AS jobdesc
  FROM edwEmployeeDim a
  LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
    AND b.yrjobd <> ''
  LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
    AND c.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
  WHERE currentrow = true
    AND active = 'Active' ) x
WHERE employeenumber IN (    
  SELECT employeenumber FROM (  
  SELECT distinct storecode, lastname, firstname, employeenumber, pydept, distribution, payrollclass,
    fullparttime, b.yrjobd AS jobcode, yrtext AS jobdesc
  FROM edwEmployeeDim a
  LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
    AND b.yrjobd <> ''
  LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
    AND c.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE')
  WHERE currentrow = true
    AND active = 'Active'  
  ) x GROUP BY employeenumber HAVING COUNT(*) > 1)
  
-- for ben, 6/11/14
-- added storecode to JOIN edwEmployeeDim to pyprhead AND pyprhead to pyprjobd
-- need to ADD manager/supervisor
SELECT employeenumber AS [employee id], firstname, lastname, 
  (SELECT trim(lastname) + ', ' + firstname FROM edwEmployeeDim WHERE employeenumber = yrmgrn AND currentrow = true) AS [supervisor name], 
  storecode AS [location id], hiredate AS [date of hire], yrtext AS [job title]
--SELECT COUNT(*)  
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
  AND b.yrjobd <> ''
  AND a.storecode = b.yrco#
LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
  AND c.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
  AND b.yrco# = c.yrco#
WHERE currentrow = true
  AND active = 'Active'
ORDER BY storecode, lastname


-- 7/14 current census, 
-- include emp#, last/first name, orig/last hiredates, full/part, pay rate, 
--   payroll class, dept, job, distcode

SELECT a.storeCode, employeenumber, lastname, firstname, 
  d.ymhdto AS origHireDate, d.ymhdte AS lastHireDate, 
  fullPartTime AS status, 
  payrollClass, iif(salary = 0, hourlyRate, salary) AS pay,
  yrtext AS job, 
  pyDept AS deparment, distCode, distribution as distCodeDescription
--SELECT COUNT(*)  
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
  AND b.yrjobd <> ''
  AND a.storecode = b.yrco#
LEFT JOIN stgArkonaPYPRJOBD c on b.yrjobd = c.yrjobd
  AND c.yrtext NOT IN ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE', '')
  AND b.yrco# = c.yrco#
LEFT JOIN stgArkonaPYMAST d on a.employeeNumber = d.ymempn  
WHERE currentrow = true
  AND active = 'Active'
ORDER BY storecode, lastname

