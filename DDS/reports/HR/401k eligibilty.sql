select *
FROM edwEmployeeDim
WHERE currentrow = true
  AND active = 'active'
  
  
SELECT ymco# as Store, ymname AS Name, 
  CASE ymactv
    WHEN 'A' THEN 'Full Time'
    WHEN 'P' THEN 'Part Time'
  END AS Status,
  ymhdto AS [Start Date],
  ymstre AS Street, ymcity AS City, ymstat AS State, ymzip AS Zip,
  case 
    when timestampdiff(sql_tsi_day, ymhdto, '01/01/2015') >= 365 AND ymactv = 'A' THEN 'Eligible'
    WHEN timestampdiff(sql_tsi_day, ymhdto, '01/01/2015') < 365 OR ymactv = 'P' THEN 'Not Eligible'
  END AS Eligibity
FROM stgArkonaPYMAST  
WHERE ymactv <> 'T'
ORDER BY timestampdiff(sql_tsi_day, ymhdto, '01/01/2015')