Can I get three reports.  
If they can be combines I am fine with that.  
But here is the information I need for each. 

1)	All employees for both Honda and GM store with employee name and department
2)	All new employees hired from Jan 2012 to June 2012 
      with the department and whether full time or part time
3)	All terminations from Jan 2012 to June 

3) 

SELECT store, termdate, name, pydept
FROM edwEmployeeDim
WHERE termdate BETWEEN '01/01/2012' AND curdate()
  AND currentrow = true
ORDER BY name 


SELECT name
FROM edwEmployeeDim
WHERE termdate BETWEEN '01/01/2012' AND curdate()
  AND currentrow = true
GROUP BY name
HAVING COUNT(*) > 1

1/2/13
Can you get me a termination report with employee name termination date and location.  
If you could run the report for the last quarter that would be great.  Thank you!

SELECT store, name, termdate
FROM edwEmployeeDim
WHERE termdate BETWEEN '10/01/2012' AND '12/31/2012'
  AND currentrow = true
ORDER BY name 

2/8/13
Hello can you get me a termination report for both stores from November to now.  Thank you!

SELECT store, name, termdate
FROM edwEmployeeDim
WHERE termdate BETWEEN '11/01/2012' AND curdate()
  AND currentrow = true
  AND storecode <> 'RY3'
ORDER BY name 



SELECT store, name, termdate
FROM edwEmployeeDim
WHERE termdate BETWEEN '02/01/2013' AND curdate()
  AND currentrow = true
  AND storecode <> 'RY3'
ORDER BY name 

--7/3/13
Anyway I can get a list of all terminations in both stores for the month of June.  
Just need the termination date and name of employee.  Thank you!
SELECT name, termdate
FROM edwEmployeeDim
WHERE termdate BETWEEN '07/01/2013' AND '07/31/2013'
  AND currentrow = true
  AND storecode <> 'RY3'
ORDER BY name 

// 8/11/13
Can you run me a termination report year to date for both stores.  Just need to know the following:

Employee name
Department
Date of hire
Date of termination

SELECT * FROM edwEmployeeDim 

SELECT name, pydept, hiredate, termdate
FROM edwEmployeeDim
WHERE termdate BETWEEN '01/01/2013' AND curdate()
  AND currentrow = true
  AND storecode <> 'RY3'
ORDER BY name 