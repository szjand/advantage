SELECT employeenumber FROM (

SELECT distinct storecode, employeenumber, name, firstname, lastname, pydept, hiredate, 
  case when termdate = '12/31/9999' THEN CAST(NULL AS sql_date) ELSE termdate END AS termDate,
  c.yrtext
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.employeenumber = b.yrempn
LEFT JOIN stgArkonaPYPRJOBD c on b.yrco# = c.yrco# AND b.yrjobd = c.yrjobd
  AND c.yrtext <> ''
WHERE currentrow = true
  AND year(hiredate) = 2014 

  
) x GROUP BY employeenumber HAVING COUNT(*) > 1