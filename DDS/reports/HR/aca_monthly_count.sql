﻿/*
1/10/18

Hi Jon-

Taylor mentioned that you may have pulled a report for her last year, for the ACA reporting that we do.  
This report would show how many active, full and part time, employees we had employed here each month.  
Here is the wording from the actual report itself:

Enter the total number of all of the employees, including full-time employees, non-full-time employees, 
and employees in a Limited Non-Assessment Period, for each calendar month. An ALE Member must choose to 
use one of the following days of the month to determine the number of employees per month and must use
that day for all months of the year: (1) the first day of each month; (2) the last day of each month; 
(3) the 12th day of each month; (4) the first day of the first payroll period that starts during each month; 
or (5) the last day of the first payroll period that starts during each month 
(provided that for each month that last day falls within the calendar month in which the payroll period starts). 
f the total number of employees was the same for every month of the entire calendar year, 
enter that number in cell for “All 12 Months” or in the boxes for each month of the calendar year. 
If the number of employees for any month is zero, enter “0.”

Would you be able to pull that information for me for each location?


--------------------------

That’s exactly what I want!  Let’s use parameter #2.

Thanks Jon!  We can also save this for next year under ACA reporting.
*/

select *
from ads.ext_edw_employee_dim
where employeekeyfromdate < '01/01/2018'
  and employeekeythrudate < '12/31/2017'
  and active = 'Active'
order by name  


select storecode, name, hiredate, termdate
from ads.ext_edw_employee_dim
where hiredate < '01/01/2018'
  and termdate > '12/31/2017'
group by storecode, name, hiredate, termdate  
order by name


select storecode, 
  count(1) filter (where employeekeyfromdate <'02/01/2017' and employeekeythrudate > '01/31/2017' and hiredate <'02/01/2017' and termdate > '01/31/2017'  ) as jan
from ads.ext_edw_employee_dim  
group by storecode

drop table if exists dds;
create temp table dds as 
select employeenumber
from ads.ext_edw_employee_dim  
where storecode = 'ry1'
  and employeekeyfromdate <'02/01/2017' and employeekeythrudate > '01/31/2017' and hiredate <'02/01/2017' and termdate > '01/31/2017'

select storecode,
from ads.ext_edw_employee_dim  
where storecode = 'ry1'
  and employeekeyfromdate <'02/01/2017' and employeekeythrudate > '01/31/2017' and hiredate <'02/01/2017' and termdate > '01/31/2017'
  
select name, active,hiredate,termdate, employeekeyfromdate,employeekeythrudate
from ads.ext_edw_employee_dim
where employeenumber = '16250'


select pymast_company_number, employee_name, hire_date, org_hire_date, termination_date, active_code
-- select *
from arkona.ext_pymast
limit 10



drop table if exists arkona;
create temp table arkona as 
select pymast_employee_number
from arkona.ext_pymast
where pymast_company_number = 'ry1'
  and (select arkona.db2_integer_to_date(hire_date)) < '02/01/2017'
  and ((select arkona.db2_integer_to_date(termination_date)) > '01/31/2017' or termination_date = 0)


select storecode, name, active,hiredate,termdate, employeekeyfromdate,employeekeythrudate
from ads.ext_edw_employee_dim
where employeenumber in (
select a.employeenumber
from dds a
full outer join arkona b on a.employeenumber = b.pymast_employee_number
where b.pymast_employee_number is null)
order by name


select name, active,hiredate,termdate, employeekeyfromdate,employeekeythrudate
from ads.ext_edw_employee_dim  
where storecode = 'ry1'
  and employeenumber = '125028'
  and employeekeyfromdate <'02/01/2017' and employeekeythrudate > '01/31/2017' and hiredate <'02/01/2017' and termdate > '01/31/2017'

select employee_name, hire_date, (select arkona.db2_integer_to_date(hire_date)), org_hire_date, termination_date, active_code
from arkona.ext_pymast
where pymast_employee_number = '125028'

select * from arkona where pymast_employee_number in('125028','138385','159100','185801','195413')
-- looks like dim employee is just plain wrong for 125028, need to look into that, but for now just use pymast
-- all i really need are hire date and term date
-- EXCEPT for those that came and went and came during the year
select name, active,hiredate,termdate, employeekeyfromdate,employeekeythrudate
-- select *
from ads.ext_edw_employee_dim
where employeenumber in('125028','138385','159100','185801','195413')
order by name,employeekeyfromdate

-- probably good enough
-- and again on 1/2/2019: good enuf
select pymast_company_number as store,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '02/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '01/31/2018' or termination_date = 0)) as jan,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '03/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '02/28/2018' or termination_date = 0)) as feb,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '04/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '03/31/2018' or termination_date = 0)) as mar,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '05/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '04/30/2018' or termination_date = 0)) as apr,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '06/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '05/31/2018' or termination_date = 0)) as may,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '07/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '06/30/2018' or termination_date = 0)) as jun,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '08/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '07/31/2018' or termination_date = 0)) as jul,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '09/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '08/31/2018' or termination_date = 0)) as aug,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '10/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '09/30/2018' or termination_date = 0)) as sep,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '11/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '10/31/2018' or termination_date = 0)) as oct,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '12/01/2018' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '11/30/2018' or termination_date = 0)) as nov,
  count(1) filter (where (select arkona.db2_integer_to_date(hire_date)) < '01/01/2019' 
    and ((select arkona.db2_integer_to_date(termination_date)) > '12/31/2018' or termination_date = 0)) as dec                                  
from arkona.xfm_pymast 
where pymast_company_number in ('RY1','RY2')
  and current_row
group by pymast_company_number


-- ok

select count(*) from arkona.ext_pymast where active_code <> 'T'

