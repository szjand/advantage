﻿-- copy csv file generated by AQT export to production server:
-- jon@ubuntu:/mnt/hgfs/E/xfr between vms$ scp enrollment_census_format_with_location_health.csv rydell@10.130.196.173:/home/rydell/files

-- create server hr
-- foreign data wrapper file_fdw;

from Enrollment Census Format with locaiton.xlsx
-- Health tab
-- need to add employee
drop table if exists jon.enrollment_census_health;
create table jon.enrollment_census_health (
  employee_number citext,
  store_location citext,
  last_name citext,
  first_name citext,
  middle_name citext,
  ssn citext,
  relationship citext,
  dob integer,
  gender citext,
  address1 citext,
  address2 citext,
  city citext,
  state citext,
  zip citext,
  telephone citext);

select a.store_location, a.last_name, a.first_name, b.ssn, a.relationship, 
  (select * from arkona.db2_integer_to_date(dob)),
  gender, address1, address2, city, state, zip, telephone
  
from jon.enrollment_census_health  a
left join ads.ext_ssn b on a.employee_number = b.employee_number


-- Flex Tab
-- from laura in response to question: 

-- Have you had any luck correlating the columns in red on the Flex tab to deduction codes in payroll?
-- 
-- Hi Jon-
-- 
-- Yes…
-- 
-- Medical FSA – 119
-- Dependent Care FSA – 115

-- who has 115/119 deductions

select company_number, employee_number, code_id, amount, count(*) as the_count
-- select *
from arkona.ext_pyhscdta
where payroll_cen_year = 117
  and code_id in ('115','119')
group by company_number, employee_number, code_id, amount  
having count(*) > 1

-- hmm, no one from honda
select code_id, count(*)
from arkona.ext_pyhscdta
where payroll_cen_year = 117
  and company_number = 'ry2'
group by code_id
order by code_id

-- ok, just making sure ry2 codes are the same
select *
from arkona.ext_pypcodes
where company_number = 'ry2'
order by ded_pay_code


-- duh, this is probably the better way of doing this, use the pymast extract
select e.employee_last_name, e.employee_first_name, f.ssn, 
  (select * from arkona.db2_integer_to_date(hire_date)),
  e.marital_status, 
  (select * from arkona.db2_integer_to_date(e.birth_date)), e.sex,
  e.address_1, e.address_2, e.city_name, e.state_code_address_, e.zip_code, 
  e.tel_area_code || '-' || e.telephone_number as telephone,
  d.employee_number,
  "Medical FSA Account","Per Pay Period Amount","Dep Care FSA Account","Per Pay Period Amount_2" 
-- select *
from (
  select company_number, employee_number, 
    max(case when code_id = '119' then account_number end) as "Medical FSA Account",
    max(case when code_id = '119' then amount end) as "Per Pay Period Amount",
    '' as annual_1,
    max(case when code_id = '115' then account_number end) as "Dep Care FSA Account",
    max(case when code_id = '115' then amount end) as "Per Pay Period Amount_2",
    '' as annual_s
  from (  
    select a.company_number, a.employee_number, a.code_id, a.amount, b.account_number, count(*) as the_count
    -- select *
    from arkona.ext_pyhscdta a
    left join arkona.ext_pypcodes b on a.company_number = b.company_number
      and a.code_id = b.ded_pay_code
    where a.payroll_cen_year = 117
      and a.code_id in ('115','119')
    group by a.company_number, a.employee_number, a.code_id, a.amount, b.account_number  
    having count(*) > 1) c
  group by company_number, employee_number) d 
inner join arkona.ext_pymast e on d.company_number = e.pymast_company_number
  and d.employee_number = e.pymast_employee_number
  and e.active_code <> 'T'
left join ads.ext_ssn f on d.employee_number = f.employee_number


/*
Hi Jon-

A month ago I had you run the attached report for me.  I am wondering if you would be able to add a few columns to this report for me?  The company that we get all of our disability insurances from needs this information but then also needs the below information along with it for each person.

-	Status – FT or PT
-	Occupation
-	Monthly Salary
-	Average Monthly hours

I can stop over and we can talk about this too if you are available?

Thanks!

Laura Roth 
*/

ok, i have decided do this all in pg
1.ssn
-- 1/14/19: use ads.ext_ssn which was just recently generated for aca reports
from E:\DDS2\Scripts\SSN
follow instructions to get ssn from dealertrack
save to a csv and import it here in pg
python projects/ext_arkona/x_import_csv.py

drop table if exists jon.ssn;
create table jon.ssn (
  employee_number citext not null,
  full_name citext not null,
  ssn citext);
  
select employee_number from jon.ssn group by employee_number having count(*) > 1

select employee_number from ads.ext_ssn group by employee_number having count(*) > 1

delete
from ads.ext_ssn
where employee_number in ('28580','124910')


alter table jon.ssn
add primary key (employee_number)

create unique index on ads.ext_ssn(ssn,employee_number) where ssn is not null;

2. run ext_arkona/hr_report_enrollment_health.py

select *
from jon.enrollment_census_health a
order by last_name

update jon.enrollment_census_health a
set ssn = b.ssn
from ads.ext_ssn b
-- from jon.ssn b
where a.employee_number = b.employee_number


select a.store_location, a.last_name, a.first_name, a.ssn, a.relationship, 
  (select * from arkona.db2_integer_to_date(dob)),
  gender, address1, address2, city, state, zip, telephone
from jon.enrollment_census_health  a
order by last_name


3. need to add
   pt/ft - from pymast
   occupation from pypread/pyprjobd
   monthly salary for hourly, paid for year/12
   avg monthly hours, 0


drop table if exists job;
create temp table job as
select a.company_number, a.employee_number, j.data as job
from arkona.ext_pyprhead a
left join (
  select distinct company_number, job_description, data
  from arkona.ext_pyprjobd) j on j.job_description = a.job_description 
    and a.company_number = j.company_number
    and j.data not in ('VEICHLE SALES', 'Owner', 'GENERAL SALES MGR', 'SERVICE RUNNERS', 'BUILDING MAINTENANCE'); -- job descriptions
-- little house cleaning
select * 
from job
where employee_number in (
select employee_number from job group by employee_number having count(*) > 1)
order by employee_number

delete from job where job is null

delete from job where employee_number = '28580';


drop table if exists employees;
create temp table employees as
select a.employee_name, a.pymast_employee_number as employee_number, 
  (select * from arkona.db2_integer_to_date(a.hire_date)) as hire_date,
  (select * from arkona.db2_integer_to_date(a.termination_date)) as termination_date,
  a.payroll_class, a.base_salary, a.base_hrly_rate,
  case a.active_code
    when 'P' then 'Part Time'
    when 'A' then 'Full Time'
  end as "Full/Part"
from arkona.ext_pymast a
where pymast_company_number in ('RY1','RY2');

-- ok, got all the rows
select a.* from employees a inner join jon.enrollment_census_health b on a.employee_number = b.employee_number where termination_date > '12/31/2017'

-- need ads for timeclock
drop table if exists clock_hours;
create temp table clock_hours as
select c.employeenumber as employee_number, sum(clockhours)::integer as clock_hours
from ads.ext_edw_clock_hours_fact a
inner join dds.dim_date b on a.datekey = b.date_key
inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
where b.the_year = 2018
group by c.employeenumber
having sum(clockhours) > 0;

select * from ads.ext_edw_employee_dim where lastname = 'nefs'  4146

select * from ads.ext_edw_clock_hours_fact where employeekey = 2568

-- this covers everybody
drop table if exists months_employed;
create temp table months_employed as
select a.employee_number,
  case 
    when termination_date > '12/31/2018' and hire_date < '01/01/2018' then 12::integer
    when termination_date > '12/31/2018' and hire_date > '01/01/2018' then round((365 - c.day_of_year)/30.4, 2)
  end as months_employed
from employees a
inner join jon.enrollment_census_health b on a.employee_number = b.employee_number
left join dds.dim_date c on a.hire_date = c.the_date;
-- order by 
--   case 
--     when termination_date > '12/31/2017' and hire_date < '01/01/2017' then 12
--     when termination_date > '12/31/2017' and hire_date > '01/01/2017' then 13 - extract(month from hire_date)
--   end 

-- now gross pay for 2018
drop table if exists gross_pay;
create temp table gross_pay as
select employee_ as employee_number, sum(total_gross_pay) as total_gross_pay
from arkona.ext_pyhshdta
where payroll_cen_year = 118
  and check_year = 18
group by employee_

-- go with gross / months employed for monthly salary
-- 1/4/18
-- saved this as unum_census_complete
-- v2, added hire_date
-- 1/14/19 guess she wants payroll class this time
select a.store_location, a.last_name, a.first_name, a.ssn, a.relationship, 
  (select * from arkona.db2_integer_to_date(dob)) as birth_date, hire_date,
  gender, address1, address2, city, state, zip, telephone, 
  -- "Full/Part", 
  c.job, 
  case
    when b.payroll_class = 'H' then 'hourly'
    when b.payroll_class = 'S' then 'salary'
    when b.payroll_class = 'C' then 'commission'
  end as payroll_class,
  (total_gross_pay/months_employed)::integer as monthly_salary,
  round(coalesce(
    case 
      when b.payroll_class = 'H' then e.clock_hours/months_employed
      else 0
    end, 0), 2) as avg_monthly_hours
from jon.enrollment_census_health a
left join employees b on a.employee_number = b.employee_number
left join job c on a.employee_number = c.employee_number
left join months_employed d on a.employee_number = d.employee_number
left join clock_hours e on a.employee_number = e.employee_number
left join gross_pay f on a.employee_number = f.employee_number
where "Full/Part" = 'Full Time'
-- where a.last_name = 'roth' 

select *
from clock_hours
where employee_number = '159100'





------------------------------------------------------------------

-- create temp table months_employed as
select a.employee_number, hire_date, (365 - c.day_of_year)/365.0, -- has worked .112 years and earned 5869.22
  (5869.22/.1123287)/ 12,  -- 4354.2, close but should be 4615.38
  5869.22/4615.38, -- which implies that she has worked 1.27 months
  42/365.0, -- hired 11/20, worked for 42 days in the year, .115 of a year
  case 
    when termination_date > '12/31/2017' and hire_date < '01/01/2017' then 12::integer
    when termination_date > '12/31/2017' and hire_date > '01/01/2017' then round((365 - c.day_of_year)/30.0, 2)
  end as months_employed, 13 - extract(month from hire_date)
from employees a
inner join jon.enrollment_census_health b on a.employee_number = b.employee_number
left join dds.dim_date c on a.hire_date = c.the_date
where a.employee_number = '12236'

select * from employees
select *
from dds.dim_date
limit 10

select * from months_employed where employee_number = 

select *
from gross_pay
where employee_number = '12236'

select 5869.22/1.37

select 2307.69 + 2307.69 + 1153.84

select * from ads.ext_edw_employee_dim where employeenumber = '12236'

select 365.25/12

---------------------------------------------------------------------------------------------------
-- 1/10/19
Hey Jon-

Last year you pulled census information for me for Unum, similar to that ACA report you just pulled, 
but I can’t find the old report you sent me.  Basically I just need that same information that was 
on the ACA report but then also the job title, monthly income and 
how they are paid (hourly, salary, semi monthly, etc) for all Full Time employees.  
Could you pull that information for me?  If it would be easier to just take that ACA 
report you sent me and then add in job title, monthly salary and pay frequency to all 
employees, I can weed out the part time ones.

Thanks!

-- 1/11/19
Hi Jon-

I know you are out sick today but I did just realize that along with the below information, 
I also need each employees birthdate and gender.  I know there was a report with this 
information that you pulled for me last year so hopefully you are able to find it once 
you are feeling better.

Thanks!


header from unum spreadsheet sent last year (1/5/18) which is what this script was all about
/*
included full and part time
did not include how they are paid
*/
store_location	
last_name	
first_name	
ssn	
relationship	-- empty
birth_date	
hire_date	
gender	
address1	
address2	
city	
state	
zip	
telephone	
Full/Part	
job	
monthly_salary	
avg_monthly_hours


ACA report sent on 1/3/19
-- includes anyone employed at anytime during the year
location	
lastname	
suffix	
firstname	
middlename	
ssn	
address	
city	
state	
zip	
hiredate	
termdate	
fullparttime

-- 1/14/19
/*
This is what we did last year.

Last year included part time, I removed them for this year and also removed the Full/Part column

You asked for “...how they are paid (hourly, salary, semi monthly, etc)” which is a combination of payroll class and frequency, I assumed you meant payroll class and that has been included.
*/

-- 1/14/19 guess she wants payroll class this time
-- saved spreadsheet as unum_census_2019
select a.store_location, a.last_name, a.first_name, a.ssn, a.relationship, 
  (select * from arkona.db2_integer_to_date(dob)) as birth_date, hire_date,
  gender, address1, address2, city, state, zip, telephone, 
  -- "Full/Part", 
  c.job, 
  case
    when b.payroll_class = 'H' then 'hourly'
    when b.payroll_class = 'S' then 'salary'
    when b.payroll_class = 'C' then 'commission'
  end as payroll_class,
  (total_gross_pay/months_employed)::integer as monthly_salary,
  round(coalesce(
    case 
      when b.payroll_class = 'H' then e.clock_hours/months_employed
      else 0
    end, 0), 2) as avg_monthly_hours
from jon.enrollment_census_health a
left join employees b on a.employee_number = b.employee_number
left join job c on a.employee_number = c.employee_number
left join months_employed d on a.employee_number = d.employee_number
left join clock_hours e on a.employee_number = e.employee_number
left join gross_pay f on a.employee_number = f.employee_number
where "Full/Part" = 'Full Time'
