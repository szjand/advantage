/*
1/26/15
ben foster
lastest hire date, orig hire date,
tenure for each AND diff 
*/
SELECT b.storecode, b.lastname, b.firstname, iif(a.ymactv = 'A', 'Full','Part') AS status,
  a.ymhdte AS [latest hire date], 
  round(timestampdiff(sql_tsi_day, a.ymhdte, curdate())/365.0,1) AS [latest tenure],
  a.ymhdto as [orig hire date],
  round(timestampdiff(sql_tsi_day, a.ymhdto, curdate())/365.0,1) AS [orig tenure],
  round(timestampdiff(sql_tsi_day, a.ymhdto, curdate())/365.0,1)
    - round(timestampdiff(sql_tsi_day, a.ymhdte, curdate())/365.0,1) AS [tenure diff]
-- select *
FROM stgArkonaPYMAST a
INNER JOIN edwEmployeeDim b on a.ymempn = b.employeenumber
  AND b.currentrow = true
WHERE a.ymactv <> 'T'
ORDER BY b.storecode, b.lastname