/*
Hello,  Can you get me a report for any employee hired April, May, and June of 2011 in all three stores.  
And if you can tell if they are part time or full time.  
If they are part time can we show how many hours they worked last year.  
Alerus needs the information.  Thank you for your help.
*/

-- employees with multiple hire dates IN date range
SELECT employeenumber, name
FROM (
  SELECT employeenumber, name, hiredate
  FROM (
    SELECT employeenumber, name, hiredate,
      CASE 
        WHEN EXISTS (
          SELECT 1 
          FROM edwEmployeeDim
          WHERE employeekey = e.employeekey
          AND FullPartTime = 'Part') THEN 'Part'
        ELSE 'Full'
      end
    FROM edwEmployeeDim e
    WHERE hiredate BETWEEN '04/01/2011' AND '06/30/2011') x
  GROUP BY employeenumber, name, hiredate) y
GROUP BY employeenumber, name
HAVING COUNT(*) > 1  

-- yes, they EXISTS, probably corrections, changes for some reason
SELECT *
FROM edwEmployeeDim
WHERE employeenumber IN ('113880','1145560','181410')
ORDER BY name

-- employees with multiple fullparttime status within the range
SELECT employeenumber, name
FROM (
  SELECT employeenumber, name, coalesce(fullparttime, 'n/a')
  FROM edwEmployeeDim e
  WHERE hiredate BETWEEN '04/01/2011' AND '06/30/2011'
  GROUP BY employeenumber, name,coalesce(fullparttime, 'n/a')) x
GROUP BY employeenumber, name
HAVING COUNT(*) > 1 

-- only one,, Nancy Liebsch
-- but she was NOT part time IN 2011, so should NOT be included
SELECT *
FROM edwEmployeeDim
WHERE employeenumber = '1102090'

---------
-- only nancy shows up twice
-- so the real challenge IS to extract the period of time for which a FP status EXISTS
-- AND THEN determine IF there IS overlap
SELECT name
FROM (
SELECT employeenumber, name, min(hiredate) AS hiredate, 
  coalesce(fullparttime, 'Unknown')
FROM edwEmployeeDim e
WHERE hiredate BETWEEN '04/01/2011' AND '06/30/2011'
GROUP BY employeenumber, name, coalesce(fullparttime, 'Unknown')) x
GROUP BY name
HAVING COUNT(*) >1 
ORDER BY name


SELECT employeenumber, employeekey, name, min(hiredate) AS hiredate, 
  coalesce(fullparttime, 'Unknown') AS "Full/Part"
FROM edwEmployeeDim e
WHERE hiredate BETWEEN '04/01/2011' AND '06/30/2011'
GROUP BY employeenumber, employeekey, name, coalesce(fullparttime, 'Unknown')

-- to get hours, need the full compliment of possible emplkeys
      
SELECT employeekey, SUM(ClockHours) AS Hours
FROM edwClockHoursFact f
INNER JOIN day d ON f.datekey = d.datekey 
  AND d.thedate BETWEEN '01/01/2011' AND '12/31/2011'   
WHERE employeekey IN (
  SELECT employeekey 
  FROM edwEmployeeDim
  WHERE employeenumber IN (
    SELECT employeenumber
    FROM edwEmployeeDim e
    WHERE hiredate BETWEEN '04/01/2011' AND '06/30/2011'))   
GROUP BY employeekey   

SELECT 
  CASE  
    WHEN LEFT(employeenumber, 1) = '1' THEN 'Rydell'
    WHEN LEFT(employeenumber, 1) = '2' THEN 'Honda'
    WHEN LEFT(employeenumber, 1) = '3' THEN 'Crookston'
  END AS store,
  name, employeenumber, hiredate, "Full/Part", round(SUM(hours), 0) AS "2011 Hours"
INTO #wtf  
FROM (
  SELECT employeenumber, employeekey, name, min(hiredate) AS hiredate, 
    coalesce(fullparttime, 'Unknown') AS "Full/Part"
  FROM edwEmployeeDim e
  WHERE hiredate BETWEEN '04/01/2011' AND '06/30/2011'
  GROUP BY employeenumber, employeekey, name, coalesce(fullparttime, 'Unknown')) a  
LEFT JOIN (
  SELECT employeekey, SUM(ClockHours) AS Hours
  FROM edwClockHoursFact f
  INNER JOIN day d ON f.datekey = d.datekey 
    AND d.thedate BETWEEN '01/01/2011' AND '12/31/2011'   
  WHERE employeekey IN (
    SELECT employeekey 
    FROM edwEmployeeDim
    WHERE employeenumber IN (
      SELECT employeenumber
      FROM edwEmployeeDim e
      WHERE hiredate BETWEEN '04/01/2011' AND '06/30/2011'))   
  GROUP BY employeekey) b on a.employeekey = b.employeekey
GROUP BY name, employeenumber, hiredate, "Full/Part"  

-- ALL but one of the Unknown are termed, AND that one IS an emp# reuse
SELECT * 
FROM edwEmployeeDim
WHERE currentrow = true
AND employeenumber IN (
  SELECT employeenumber
  FROM #wtf
  WHERE "Full/Part" = 'Unknown')
  
SELECT *
FROM edwEmployeeDim
WHERE employeenumber = '169850'  

-- this IS ok, but will still need to remove some dups FROM the spread sheet
SELECT store, name, employeenumber, hiredate, "Full/Part",
  CASE
    WHEN "Full/Part" = 'Full' THEN 'N/A'
    ELSE CAST([2011 Hours] AS sql_char) 
  END AS [2011 Hours]
FROM #wtf

-- fuck, edwEmployeeDim needs reg maintenance
-- why 2 hiredates for 1133880?
SELECT *
FROM edwEmployeeDim
WHERE employeenumber = '1133880'

-- 5/18 ------
-- start with empkey AND THEN WORK out
-- this may prove to be a common pattern, how to handle 1:M

SELECT * FROM edwEmployeeDim 

-- the employee number of peeps hired IN range
SELECT *
FROM edwEmployeeDim
WHERE employeekey IN (
  SELECT employeekey
  FROM edwEmployeeDim 
  WHERE hiredate  BETWEEN '04/01/2011' AND '06/30/2011')
  
-- keep wanting to know shit LIKE, for what duration of time was an emp full AND what duration part

SELECT *
FROM edwEmployeeDim
WHERE employeenumber IN (
  SELECT employeenumber
  FROM edwEmployeeDim
  WHERE rowchangereason = 'FullPart')
ORDER BY name   

