﻿select *
from ads.ext_vehicle_evaluations
limit 10


select *
from ads.ext_vehicle_sales
where status = 'VehicleSale_Sold'
limit 100


select *
from ads.ext_recon_author


select *
from 


select sale_type, count(*)
from sls.ext_bopmast_partial
group by sale_type

select a.stock_number, b.fullname, a.delivery_date
-- select *
from sls.ext_bopmast_partial a
left join ads.ext_dim_customer b on a.buyer_bopname_id = b.bnkey
where sale_type = 'F'
  and delivery_date between current_Date - 45 and current_date


SELECT string_agg(column_name, ',')
FROM information_schema.columns
WHERE table_schema = 'ads'
  AND table_name   = 'ext_vehicle_duebill_authorized_items'
group by table_name  

select *
from ads.ext_vehicle_duebill_authorized_items

drop table if exists step_1;
create temp table step_1 as
select a.vehicleinventoryitemid, a.vehiclesaleid, a.soldts::date as sold_date, 
  a.soldamount, a.typ, b.stocknumber, b.fromts::date as from_Date, b.thruts::date  as thru_date, 
  c.appraiser, c.model_year || ' ' || c.make || ' ' || c.model || ' ' || coalesce(c.series, '') as vehicle,
  appraisal, recon_costs, cert_costs, appraisal_created::date
-- select *  
from ads.ext_vehicle_sales a
inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join vauto.finalized_appraisal_report c on b.stocknumber = c.stock_number
  and c.entity = 'Rydell Auto Center'
where a.status = 'VehicleSale_Sold'
  and a.soldts::date < '05/01/2018'
  and c.stock_number not in ('30011A','32272XA','30726C','30323B','32563XXA','31363A');
create unique index on step_1(stocknumber) ;
create unique index on step_1(vehicleinventoryitemid);  

drop table if exists est_price;
create temp table est_price as
select vehicleinventoryitemid, vehiclepricingts::date as initial_price_date, initial_price
from (
  select a.vehiclepricingid, a.vehicleinventoryitemid, vehiclepricingts, b.amount as initial_price,
    row_number () over (partition by a.vehicleinventoryitemid order by vehiclepricingts)
  from ads.ext_vehicle_pricings a
  inner join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
    and b.typ = 'VehiclePricingDetail_BestPrice'
  inner join step_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) x
where x.row_number = 1;
create unique index on est_price(vehicleinventoryitemid);

drop table if exists recon;
create temp table recon as
select control, sum(amount) as recon
from fin.fact_gl a
inner join step_1 aa on a.control = aa.stocknumber
inner join dds.dim_date b on a.date_key = b.date_key
inner join fin.dim_Account c on a.account_key = c.account_key
  and c.account in ('124000', '124100',  '224000', '224100')
inner join fin.dim_journal d on a.journal_key = d.journal_key 
  and d.journal_code in ('SVI', 'SWA', 'SCA')
group by control;
create unique index on recon(control);

drop table if exists factory;
create temp table factory as
select vehicleinventoryitemid 
from (
  select a.vehicleinventoryitemid, a.typ, a.selectedreconpackagets,
    row_number () over (partition by a.vehicleinventoryitemid order by a.selectedreconpackagets desc)
  from ads.ext_selected_Recon_packages a
  inner join step_1 b on a.vehicleinventoryitemid = b.vehicleinventoryitemid) c
where row_number = 1
  and typ = 'ReconPackage_Factory';
create unique index on factory(vehicleinventoryitemid);  

drop table if exists duebills;
create temp table duebills as
select b.vehicleinventoryitemid, sum(d.totalpartsamount + d.laboramount) as due_bill
from ads.ext_vehicle_duebill_authorizations a
inner join step_1 b on a.vehiclesaleid = b.vehiclesaleid
inner join ads.ext_vehicle_duebill_authorized_items c on a.vehicleduebillauthorizationid = c.vehicleduebillauthorizationid
inner join ads.ext_vehicle_Recon_items d on c.vehiclereconitemid = d.vehiclereconitemid
group by b.vehicleinventoryitemid;
create unique index on duebills(vehicleinventoryitemid);

drop table if exists on_ground;
create temp table on_ground as
select a.vehicleinventoryitemid, b.thruts::date as ground_date
from step_1 a
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.category = 'RMFlagTNA'
group by  a.vehicleinventoryitemid, b.thruts  
union  
select a.vehicleinventoryitemid, b.fromts::date as ground_date
from step_1 a
inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and not exists (
    select 1
    from ads.ext_vehicle_inventory_item_statuses 
    where vehicleinventoryitemid = b.vehicleinventoryitemid
      and category = 'RMFlagTNA')
group by a.vehicleinventoryitemid, b.fromts;
create unique index on on_ground(vehicleinventoryitemid); 

drop table if exists accounting;
create temp table accounting as
select a.stocknumber, 
  sum(amount) filter (where b.account_type = 'Sale') as sale,
  sum(amount) filter (where b.account_type = 'cogs' and gl_account not in ('164700','164701','164702','165100','165101','165102')) as cost,
  coalesce(sum(amount) filter (where b.account_type = 'cogs' and gl_account in ('164700','164701','164702','165100','165101','165102')), 0) as recon
from step_1 a
inner join sls.deals_accounting_detail b on a.stocknumber = b.control
  and b.page = 16
  and left(b.gl_account, 1) = '1'
group by a.stocknumber;
create unique index on accounting(stocknumber);

drop table if exists step_2;
create temp table step_2 as
select a.appraisal_created, a.appraiser as evaluator, a.vehicle, a.stocknumber, 
  case a.typ
    when 'VehicleSale_Wholesale' then 'wholesale'
    when  'VehicleSale_Retail' then 'retail'
  end as sale_type,
  b.initial_price::integer as est_sale_price,
  coalesce(a.recon_costs::integer, 0) as est_recon, a.appraisal::integer as appraisal, 
  a.cert_costs::integer as cert_costs, case when d.vehicleinventoryitemid is not null then 'factory' else null end as certified,
  coalesce(case when a.typ = 'VehicleSale_Wholesale' then c.recon::integer end, 0) as ws_recon,
  coalesce(e.due_bill::integer, 0) as due_bill, f.ground_date, a.sold_date, a.sold_date - f.ground_date as days_in_inventory,
  coalesce(g.sale::integer, 0) as sale, coalesce(g.cost::integer, 0) as cost, coalesce(g.recon::integer, 0) as recon
-- select *
from step_1 a
left join est_price b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
left join recon c on a.stocknumber = c.control
left join factory d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
left join duebills e on a.vehicleinventoryitemid = e.vehicleinventoryitemid
left join on_ground f on a.vehicleinventoryitemid = f.vehicleinventoryitemid
left join accounting g on a.stocknumber = g.stocknumber;
create unique index on step_2(stocknumber);

-- 5/11/18 sent out as appraisal_report_V1
select appraisal_created as eval_date, evaluator, vehicle, stocknumber, sale_type,
  est_sale_price, -sale as actual_sale_price, 
  -sale - est_sale_price as difference,
  est_recon, due_bill + ws_recon + recon as actual_recon, (due_bill + ws_recon + recon) - est_recon as recon_diff,
  est_sale_price - (appraisal + est_recon) as est_gross,
  -sale - cost - (due_bill + ws_recon + recon) as actual_gross,
--   (est_sale_price - (appraisal + est_recon)) - (-sale - cost - (due_bill + ws_recon + recon)) as gross_diff
  (-sale - cost - (due_bill + ws_recon + recon)) - (est_sale_price - (appraisal + est_recon)) as gross_diff,
  certified, cert_costs as cert_fee_collected, days_in_inventory
-- select * 
from step_2 a
order by vehicle



--------------------------------------------------------------------------------------
-- colors
--------------------------------------------------------------------------------------
select color, min(year), max(year), count(*)
from arkona.xfm_inpmast 
where current_row = true
  and model like 'silverado 1500%'
  and color is not null
group by color
having count(*) > 1
  and length(color) > 2

--------------------------------------------------------------------------------------
select color
from arkona.xfm_inpmast 
where current_row = true
  and model like 'silverado 1500%'
  and color is not null
group by color
having count(*) > 1
  and length(color) > 2
  
--------------------------------------------------------------------------------------
-- colors
--------------------------------------------------------------------------------------

-- ry1 evaluations of type trade that do not exist in vauto
drop table if exists tool_1;
create temp table tool_1 as
select a.vehicleinventoryitemid, b.vehicleitemid, d.vehiclesaleid, d.soldts::date as sold_date, 
  case d.typ
    when 'VehicleSale_Retail' then 'Retail'
    when 'VehicleSale_Wholesale' then 'Wholesale'
  end as sale_type, a.vehicleevaluationts::date as eval_date, 
  c.lastname || ', ' || c.firstname as evaluator, b.stocknumber, bb.vin,
  bb.yearmodel || ' ' || bb.make || ' ' || bb.model || ' ' || coalesce(bb.trim, '') as vehicle,
  (reconmechanicalamount + reconbodyamount + reconappearanceamount + recontireamount + reconglassamount + reconinspectionamount)::integer as eval_recon,
  a.amountshown::integer as amountshown, e.selectedacv as eval_acv
from ads.ext_vehicle_evaluations a
inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join ads.ext_vehicle_items bb on b.vehicleitemid = bb.vehicleitemid
inner join ads.ext_vehicle_sales d on a.vehicleinventoryitemid = d.vehicleinventoryitemid
  and d.status = 'VehicleSale_Sold'
inner join ads.ext_acvs e on a.vehicleevaluationid = e.tablekey
left join ads.ext_people c on a.vehicleevaluatorid = c.partyid
where a.vehicleinventoryitemid is not null
  and a.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  and extract(year from vehicleevaluationts) > 2014
  and a.currentstatus = 'VehicleEvaluation_Booked'
  and not exists (
    select 1
    from vauto.finalized_appraisal_report
    where stock_number = b.stocknumber
      or vin = bb.vin)
  and a.typ = 'VehicleEvaluation_Trade';    
create unique index on tool_1(stocknumber);
create unique index on tool_1(vehicleinventoryitemid);

drop table if exists tool_first_price;
create temp table tool_first_price as
select vehicleinventoryitemid, vehiclepricingts::date as initial_price_date, initial_price:: integer
from (
  select a.vehiclepricingid, a.vehicleinventoryitemid, vehiclepricingts, b.amount as initial_price,
    row_number () over (partition by a.vehicleinventoryitemid order by vehiclepricingts)
  from ads.ext_vehicle_pricings a
  inner join ads.ext_vehicle_pricing_details b on a.vehiclepricingid = b.vehiclepricingid
    and b.typ = 'VehiclePricingDetail_BestPrice'
  inner join tool_1 c on a.vehicleinventoryitemid = c.vehicleinventoryitemid) x
where x.row_number = 1;
create unique index on tool_first_price(vehicleinventoryitemid);


drop table if exists tool_duebills;
create temp table tool_duebills as
select b.vehicleinventoryitemid, sum(d.totalpartsamount + d.laboramount)::integer as due_bill
from ads.ext_vehicle_duebill_authorizations a
inner join tool_1 b on a.vehiclesaleid = b.vehiclesaleid
inner join ads.ext_vehicle_duebill_authorized_items c on a.vehicleduebillauthorizationid = c.vehicleduebillauthorizationid
inner join ads.ext_vehicle_Recon_items d on c.vehiclereconitemid = d.vehiclereconitemid
group by b.vehicleinventoryitemid;
create unique index on tool_duebills(vehicleinventoryitemid);

drop table if exists tool_on_ground;
create temp table tool_on_ground as
select a.vehicleinventoryitemid, b.thruts::date as ground_date
from tool_1 a
inner join ads.ext_vehicle_inventory_item_statuses b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.category = 'RMFlagTNA'
group by  a.vehicleinventoryitemid, b.thruts  
union  
select a.vehicleinventoryitemid, b.fromts::date as ground_date
from tool_1 a
inner join ads.ext_vehicle_inventory_items b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and not exists (
    select 1
    from ads.ext_vehicle_inventory_item_statuses 
    where vehicleinventoryitemid = b.vehicleinventoryitemid
      and category = 'RMFlagTNA')
group by a.vehicleinventoryitemid, b.fromts;
create unique index on tool_on_ground(vehicleinventoryitemid); 

drop table if exists tool_accounting;
create temp table tool_accounting as
select  a.control,
  sum(amount) filter (where b.account_type = 'Sale')::integer as sale,
  sum(amount) filter (where b.account_type = 'cogs' and gl_account not in ('164700','164701','164702','165100','165101','165102'))::integer as cost,
  coalesce(sum(amount) filter (where b.account_type = 'cogs' and gl_account in ('164700','164701','164702','165100','165101','165102')), 0)::integer as recon
from fin.fact_gl a
inner join tool_1 aaa on a.control = aaa.stocknumber
inner join fin.dim_account b on a.account_key = b.account_key
inner join sls.deals_accounts_routes d on b.account = d.gl_account
where post_status = 'Y'
  and d.page = 16
group by a.control;
create unique index on tool_accounting(control);

drop table if exists tool_ws_recon;
create temp table tool_ws_recon as
select control, sum(amount)::integer as ws_recon
from fin.fact_gl a
inner join tool_1 aa on a.control = aa.stocknumber
inner join fin.dim_Account c on a.account_key = c.account_key
  and c.account in ('124000', '124100',  '224000', '224100')
inner join fin.dim_journal d on a.journal_key = d.journal_key 
  and d.journal_code in ('SVI', 'SWA', 'SCA')
group by control;
create unique index on tool_ws_recon(control);


drop table if exists tool_2;
create temp table tool_2 as
select a.eval_Date, a.evaluator, a.vehicle, a.stocknumber, a.sale_type, 
  b.initial_price as est_sale_price, a.eval_recon, coalesce(c.due_bill, 0) as duebill, 
  a.eval_acv, e.sale, e.cost, e.recon, 
  coalesce(case when a.sale_type = 'Wholesale' then coalesce(f.ws_recon, 0)  end, 0) as ws_recon,
  initial_price - (eval_acv + eval_recon) as est_gross,
  -sale - cost - (coalesce(due_bill, 0) + ws_recon + recon) as actual_gross,
  a.sold_date - d.ground_date as days_in_inventory
from tool_1 a
inner join tool_first_price b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
inner join tool_on_ground d on  a.vehicleinventoryitemid = d.vehicleinventoryitemid
inner join tool_accounting e on a.stocknumber = e.control
left join tool_duebills c on a.vehicleinventoryitemid = c.vehicleinventoryitemid
left join tool_ws_recon f on a.stocknumber = f.control
where sale <> 0;
create unique index on tool_2(stocknumber);


select eval_date,evaluator, vehicle, stocknumber, sale_type, est_sale_price,
  -sale as actual_sale_price, (-sale - est_sale_price) as difference,
  eval_recon as est_recon, duebill + ws_recon + recon as actual_recon,
  (duebill + ws_recon + recon) - eval_recon as recon_diff,
  est_gross, actual_gross,  actual_gross - est_gross as gross_diff,
  days_in_inventory
-- select *  
from tool_2
where actual_Gross is not null


----------------------------------------------------------------------------------------------------------
-- 5/14
compare walk acv to eval acv

select a.*, b.selectedacv as walk_acv
from tool_1 a
inner join ads.ext_acvs b on a.vehicleinventoryitemid = b.vehicleinventoryitemid
  and b.basistable = 'vehiclewalks'
where eval_acv <> b.selectedacv


select * from ads.ext_acvs limit 100