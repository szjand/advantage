SELECT year, series, retailaverage, blackbookpubdate
INTO #t1
FROM bb.blackbookadt
WHERE retailaverage > 1
  AND model = 'impala'
GROUP BY year, series, retailaverage, blackbookpubdate  


SELECT blackbookpubdate, series, retailaverage, b.yearmonth
FROM #t1 a
LEFT JOIN dds.day b ON a.blackbookpubdate = b.thedate
WHERE year = '2009'
  AND retailaverage > 0
  AND series = 'ls'
  
SELECT b.yearmonth, series, max(retailaverage) AS retailaverage 
FROM #t1 a
LEFT JOIN dds.day b ON a.blackbookpubdate = b.thedate
WHERE year = '2009'
  AND retailaverage > 0
--  AND series = 'ls'  
GROUP BY b.yearmonth, series  
ORDER BY series, b.yearmonth
  
  
SELECT a.groupnumber, a.uvc, b.make, b.model, b.yearmodel, COUNT(*)
FROM blackbookresolver a
LEFT JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID 
GROUP BY a.groupnumber, a.uvc, b.make, b.model, b.yearmodel
ORDER BY COUNT(*) DESC

-- DROP TABLE #t1
SELECT year, series, retailaverage, blackbookpubdate
INTO #t1
-- SELECT *
FROM bb.blackbookadt
WHERE retailaverage > 1
  AND model = 'Malibu'
--  AND year = '1999'
GROUP BY year, series, retailaverage, blackbookpubdate

SELECT *
FROM #t1
WHERE year = '2009'
ORDER BY series, blackbookpubdate

SELECT b.yearmonth, series, max(retailaverage) AS retailaverage 
FROM #t1 a
LEFT JOIN dds.day b ON a.blackbookpubdate = b.thedate
WHERE series <> 'Hybrid'
  AND year = '2009'
GROUP BY b.yearmonth, series  
ORDER BY series, b.yearmonth


-- DROP TABLE #t1
SELECT year, series, retailaverage, blackbookpubdate
INTO #t1
-- SELECT *
FROM bb.blackbookadt
WHERE retailaverage > 1
  AND model = 'Silverado 1500'
--  AND year = '1999'
GROUP BY year, series, retailaverage, blackbookpubdate

SELECT *
FROM #t1
WHERE year = '2009'
ORDER BY series, blackbookpubdate

SELECT b.yearmonth, series, max(retailaverage) AS retailaverage 
FROM #t1 a
LEFT JOIN dds.day b ON a.blackbookpubdate = b.thedate
WHERE series <> 'Hybrid'
  AND year = '2009'
GROUP BY b.yearmonth, series  
ORDER BY series, b.yearmonth