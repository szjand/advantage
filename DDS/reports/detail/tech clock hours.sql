Yes to WTEC.  Clock hours per pay period YTD please?

From: Jon Andrews [mailto:jandrews@cartiva.com] 
Sent: Friday, October 16, 2015 10:01 AM
To: 'Taylor Monson'
Subject: RE: Clock Hours

And how do you define “Detail Technicians”?
Distribution Code WTEC?

From: Taylor Monson [mailto:tmonson@rydellcars.com] 
Sent: Friday, October 16, 2015 9:46 AM
To: 'Jon Andrews'
Subject: Clock Hours

Hi Jon,

Could you please pull all the Detail Technicians’ clock hours from DealerTrack for me? 


 
    
    

SELECT firstname, lastname, x.employeenumber, biweeklypayperiodstartdate, 
  biweeklypayperiodenddate, SUM(clockhours) AS clockhours, SUM(holidayhours) AS holidayhours,
  SUM(vacationhours + ptohours) AS ptohours
FROM (
  SELECT a.firstname, a.lastname, a.employeenumber, a.employeekey
  FROM edwEmployeeDim a
  WHERE a.distcode = 'WTEC'
    AND a.currentrow = true
    AND a.active = 'active') x
LEFT JOIN (  
  SELECT a.*, b.biweeklypayperiodstartdate, b.biweeklypayperiodenddate, d.employeenumber
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
    AND b.thedate BETWEEN '12/28/2014' AND curdate()
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  INNER JOIN (
    SELECT employeekey, employeenumber
    FROM edwEmployeeDim
    WHERE distcode = 'WTEC'
      AND currentrow = true
      AND activecode = 'a') d on c.employeenumber = d.employeenumber) y on x.employeenumber = y.employeenumber
GROUP BY firstname, lastname, x.employeenumber, biweeklypayperiodstartdate, 
  biweeklypayperiodenddate    
ORDER BY firstname, lastname, biweeklypayperiodstartdate    