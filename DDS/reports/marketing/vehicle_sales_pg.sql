﻿select 
  case bopmast_company_number
    when 'RY1' then 'GM'
    when 'RY2' then 'Honda/Nissan'
  end as store, date_capped as date, bopmast_stock_number, b.fullname as name, 
  CASE 
    WHEN b.homephone <> '0' THEN homephone
    ELSE 
      CASE 
	    WHEN cellphone <> '0' THEN cellphone
        ELSE 
          CASE 
		    WHEN businessphone <> '0' THEN businessphone
    	    ELSE 'None'
    	  END
      END
  END AS phone,
  CASE
    WHEN emailvalid = true THEN email
	ELSE
	  CASE 
	    WHEN email2valid = true THEN email2
		ELSE 'None'
	  END
  END AS email,
  case sale_type
    when 'R' then retail_price::integer 
    when 'L' then lease_price::integer
  end as price, house_gross::integer as gross
-- select *
from dds.ext_bopmast a
left join ads.ext_dds_dimcustomer b on a.buyer_number = b.bnkey
where date_capped between '10/01/2016' and '11/30/2016'  
  and sale_type <> 'W'
order by bopmast_company_number, date_capped, bopmast_stock_number  


