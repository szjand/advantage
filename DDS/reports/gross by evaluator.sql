SELECT *
FROM dps.vehicleevaluations a
LEFT JOIN dps.people b ON a.vehicleevaluatorid = b.partyid


SELECT *
FROM (
  SELECT a.stocknumber, c.fullname, CAST(b.VehicleEvaluationTS AS sql_date)
  FROM dps.VehicleInventoryItems a
  INNER JOIN dps.VehicleEvaluations b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  LEFT JOIN dps.people c ON b.VehicleEvaluatorID = c.partyid
  WHERE cast(a.fromts AS sql_date) > '01/01/2010') a
LEFT JOIN (
  SELECT al1, b.gtctl#, SUM(gttamt)
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND gtdate > '01/01/2010'
  WHERE a.name = 'gmfs gross'
    AND dl5 = 'used'
  GROUP BY al1, b.gtctl#) b ON a.stocknumber = b.gtctl#
WHERE fullname = 'ron wilkening'

SELECT fullname, year(evaldate), month(evaldate), COUNT(*), SUM(gross), round(SUM(gross)/COUNT(*),0)
FROM (
  SELECT a.stocknumber, c.fullname, CAST(b.VehicleEvaluationTS AS sql_date) evaldate
  FROM dps.VehicleInventoryItems a
  INNER JOIN dps.VehicleEvaluations b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  LEFT JOIN dps.people c ON b.VehicleEvaluatorID = c.partyid
  WHERE cast(a.fromts AS sql_date) > '01/01/2010') a
LEFT JOIN (
  SELECT al1, b.gtctl#, SUM(gttamt) AS gross
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND gtdate > '01/01/2010'
  WHERE a.name = 'gmfs gross'
    AND dl5 = 'used'
  GROUP BY al1, b.gtctl#) b ON a.stocknumber = b.gtctl#
GROUP BY fullname, year(evaldate), month(evaldate)


-- look at a particular vehicle
SELECT a.*, b.*, c.gmdesc
FROM (
  SELECT a.stocknumber, c.fullname, CAST(b.VehicleEvaluationTS AS sql_date)
  FROM dps.VehicleInventoryItems a
  INNER JOIN dps.VehicleEvaluations b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  LEFT JOIN dps.people c ON b.VehicleEvaluatorID = c.partyid
  WHERE cast(a.fromts AS sql_date) > '01/01/2010') a
LEFT JOIN (
  SELECT al2, glaccount, b.gtctl#, al3, gmcoa, SUM(gttamt)
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND gtdate > '01/01/2010'
  WHERE a.name = 'gmfs gross'
    AND dl5 = 'used'
  GROUP BY al2, glaccount, b.gtctl#, al3, gmcoa) b ON a.stocknumber = b.gtctl#
LEFT JOIN stgArkonaGLPMAST c ON b.glaccount = c.gmacct
  AND c.gmyear = 2012  
WHERE a.stocknumber = '17192a'


