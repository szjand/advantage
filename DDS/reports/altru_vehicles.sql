﻿
select *
from (
select a.stock_number, a.vin, a.capped_date, a.delivery_date, b.fullname
-- select *
from sls.ext_bopmast_partial a 
inner join ads.ext_dim_customer b on a.buyer_bopname_id = b.bnkey
  and b.fullname like '%ALTRU%'
where a.vin is not null) a  

full outer join (
select c.vin, b.fullname, max(d.the_date) as last_visit
from ads.ext_fact_repair_order a
inner join ads.ext_dim_customer b on a.customerkey = b.customerkey
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
inner join dds.dim_date d on a.opendatekey = d.date_key
where b.fullname like '%ALTRU%'
group by c.vin, b.fullname) b on a.vin = b.vin


  select 'purchase', a.stock_number, a.vin, a.delivery_date as the_date, b.fullname
  -- select *
  from sls.ext_bopmast_partial a 
  inner join ads.ext_dim_customer b on a.buyer_bopname_id = b.bnkey
    and b.fullname like '%ALTRU%'
  where a.vin is not null
order by stock_number  


-- i think i like the union better
-- all instances of either a sale or service visit where the cust like ALTRU
drop table if exists altru_1;
create temp table altru_1 as
select x.*, y.year, y.make, y.model, y.color
from (
  select 'purchase', a.stock_number, a.delivery_date as the_date, a.vin, b.fullname
  from sls.ext_bopmast_partial a 
  inner join ads.ext_dim_customer b on a.buyer_bopname_id = b.bnkey
    and b.fullname like '%ALTRU%'
  where a.vin is not null
  union all
  select 'service', '', max(d.the_date) as last_visit, c.vin,  b.fullname
  from ads.ext_fact_repair_order a
  inner join ads.ext_dim_customer b on a.customerkey = b.customerkey
  inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
  inner join dds.dim_date d on a.opendatekey = d.date_key
  where b.fullname like '%ALTRU%'
  group by c.vin, b.fullname) x
left join arkona.xfm_inpmast y on x.vin = y.inpmast_vin
  and y.current_row = true
order by vin, the_date;







-- using window functions to expose ownership history
-- a small sample where i know there are multiple rows/vin in bopvref
select a.*, b.fullname, 
  row_number() over (w),
  rank() over (w),
  dense_rank() over(w)
from arkona.ext_bopvref a
left join ads.ext_dim_customer b on a.customer_key = b.bnkey
where vin in ('1N4AL2AP9AN550794','1HGCP2F38AA062698','1N4AL2AP9AN565313','1N4AL2AP3AN480774','1N4AL2AP3CN561051','1N4AL21E79N542056','1N4AL3AP0EN261055')
window w as (order by vin)

-- where the last known owner is altru
select *
from (
  select vin, start_date, b.fullname, 
    row_number() over (partition by vin order by start_date desc) as first_row,
    count(*) over (partition by vin) as the_count
  from arkona.ext_bopvref a
  left join ads.ext_dim_customer b on a.customer_key = b.bnkey
  where vin not like '0%'
    and length(vin) = 17) x
where first_row = 1  
  and fullname like '%ALTRU%'


-- 152 distinct vins
select x.*
from  altru_1 x
inner join (
  select *
  from (-- the most recent where the last known owner is altru
    select vin, start_date, b.fullname, 
      row_number() over (partition by vin order by start_date desc) as first_row,
      count(*) over (partition by vin) as the_count
    from arkona.ext_bopvref a
    left join ads.ext_dim_customer b on a.customer_key = b.bnkey
    where vin not like '0%'
      and length(vin) = 17) x
  where first_row = 1  
    and fullname like '%ALTRU%') y on x.vin = y.vin  
order by x.vin, x.the_date


.o0O