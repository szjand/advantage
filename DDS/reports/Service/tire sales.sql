
-- DROP TABLE #wtf;
SELECT year(b.thedate) AS theYear, month(b.thedate) as theMonth, c.opcode,
  CAST(LEFT(TRIM(c.opcode), 1) AS sql_integer) AS multiplier
INTO #wtf
FROM factRepairORder a
INNER JOIN day b on a.closedatekey = b.datekey
INNER JOIN dimOpcode c on a.opcodekey = c.opcodekey
WHERE year(b.thedate) IN (2013,2014)
  AND c.opcode IN ('1nt','2nt','3nt','4nt','5nt','6nt')  
  AND a.storecode = 'ry1'
  
SELECT theyear, SUM(multiplier) AS sold
FROM #wtf
GROUP BY theyear  

-- DROP TABLE z_temp_tire_sales;
CREATE TABLE z_temp_tire_sales (
  theYear integer,
  theMonth integer,
  ro cichar(9),
  opcode cichar(10),
  sold integer) IN database;
INSERT INTO z_temp_tire_sales
-- ruh roh, dup lines, GROUP first
SELECT theYear, theMonth, ro, opcode, CAST(LEFT(TRIM(e.opcode), 1) AS sql_integer)
FROM (
  SELECT year(b.thedate) AS theYear, month(b.thedate) as theMonth, a.ro, c.opcode
  FROM factRepairORder a
  INNER JOIN day b on a.closedatekey = b.datekey
  INNER JOIN dimOpcode c on a.corcodekey = c.opcodekey
  WHERE year(b.thedate) IN (2013,2014)
    AND c.opcode IN ('1nt','2nt','3nt','4nt','5nt','6nt')  
    AND a.storecode = 'ry1'
  group by year(b.thedate), month(b.thedate), a.ro, c.opcode) e;
  
select theyear,SUM(sold) AS sold
FROM z_temp_tire_Sales
GROUP BY theyear  
  
SELECT theyear, opcode, SUM(sold) AS sold
FROM z_temp_tire_Sales
GROUP BY theyear, opcode

-- ros with multiple xNT lines
SELECT *
FROM z_temp_tire_sales
  WHERE ro IN (
  SELECT ro
  FROM z_Temp_tire_sales
  GROUP BY ro HAVING COUNT(*) > 1)
ORDER BY ro


SELECT *
FROM factRepairOrder
WHERE ro = '16177493'

