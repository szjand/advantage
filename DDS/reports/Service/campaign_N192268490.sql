﻿

-- 1166
select -- a.ro, b.the_date, 
  c.fullname, c.homephone, c.email, d.vin, d.modelyear, d.make, d.model
from ads.ext_fact_repair_order a
join dds.dim_date b on a.opendatekey = b.date_key
join ads.ext_dim_customer c on a.customerkey = c.customerkey
  and c.fullname <> 'inventory'
join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey  
where a.opcodekey = 28987
order by c.fullname


select * from ads.ext_dim_vehicle limit 100