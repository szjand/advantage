﻿drop table if exists ro_vehicles;
create temp table ro_vehicles as
select a.ro, b.the_date, c.fullname, d.vin, a.miles, a.roflaghours, e.comment
-- select a.*
from ads.ext_fact_repair_order a
inner join dds.dim_date b on a.opendatekey = b.date_key
  and b.the_year = 2018
inner join ads.ext_dim_customer c on a.customerkey = c.customerkey  
  and c.fullname not in ( 'inventory','CARRY IN, ANY','COURTESY DELIVERY','DEALER TRADE 2018','RYDELL AUTO CENTER','RYDELL, ROBERT','RYDELL, WESLEY DAVID')
inner join ads.ext_dim_vehicle d on a.vehiclekey = d.vehiclekey
  and d.vin not like '111%'
  and d.vin not like '000%'
  and length(d.vin) = 17
inner join ads.ext_dim_ro_comment e on a.rocommentkey = e.rocommentkey
where left(a.ro,2) = '16'
group by a.ro, b.the_date, c.fullname, d.vin, a.miles, a.roflaghours, e.comment

-- need the ccc

select a.*
from ro_vehicles a
inner join (
  select vin
  from ro_vehicles
  group by vin
  having count(*) > 1) b on a.vin = b.vin
order by a.fullname