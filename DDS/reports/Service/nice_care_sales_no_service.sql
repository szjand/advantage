/*
for justin kilmer
*/


/*
-- 1/5
Hey Jon
Can we get a new one that involves the cars sold in August?
Justin
-- 2/6/18: september
-- 4/2/18: november
-- 5/2/18: december
-- 6/1/18: january
-- 7/1/18: february
-- 8/1/18: march

BETWEEN '08/01/2022' AND '08/31/2022' sales in aug 22 for report on 1/1/23
BETWEEN '09/01/2022' AND '09/30/2022' sales IN sep 22 for report 2/1/23
*/
-- select * FROM organization


/*











    IN postresql now 
    E:\sql\postgresql\marketing_bdc\justin nice care sale no service










*/
DROP TABLE #deals;
SELECT *
INTO #deals
FROM (
  SELECT a.stocknumber, c.vin, cast(b.soldts AS sql_date) AS sale_date, e.name
  FROM VehicleInventoryItems a 
  INNER JOIN vehicleSales b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN VehicleItems c on a.VehicleItemID = c.VehicleItemID 
  INNER JOIN selectedreconpackages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
    AND d.typ = 'ReconPackage_Nice'
  LEFT JOIN organizations e on a.locationid = e.partyid
  WHERE cast(b.soldts AS sql_date) BETWEEN '03/01/2023' AND '03/31/2023' ----------------- sales in mar 23 for report on 9/1/23
    AND b.status = 'VehicleSale_Sold'
--    AND a.locationid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  GROUP BY a.stocknumber, c.vin, cast(b.soldts AS sql_date), e.name) d
LEFT JOIN dds.dimvehicle e on d.vin = e.vin    
LEFT JOIN (
  select b.thedate, a.ro, c.vehiclekey
  from dds.factrepairorder a
  inner join dds.day b on a.opendatekey = b.datekey
  inner join dds.dimvehicle c on a.vehiclekey = c.vehiclekey
  INNER JOIN dds.dimservicetype d on a.servicetypekey = d.servicetypekey
    AND d.servicetypecode IN ('AM','MR','QL','QS','MN')
  where b.yearmonth > 202301
  GROUP BY b.thedate, a.ro, c.vehiclekey) f on e.vehiclekey = f.vehiclekey AND f.thedate > d.sale_date
WHERE f.ro IS NULL;  


-- this IS the spreadsheet query for justin  
SELECT a.name, c.fullname, a.sale_date, c.homephone, c.businessphone, c.cellphone, c.city, c.state,
  a.stocknumber, a.vin, a.modelyear, a.make, a.model
-- SELECT COUNT(*)
-- SELECT *
FROM #deals a
LEFT JOIN dds.factvehiclesale b on a.vehiclekey = b.vehiclekey
  AND a.stocknumber = b.stocknumber
LEFT JOIN dds.dimcustomer c on b.buyerkey = c.customerkey  
--WHERE c.customerkey IS NOT NULL
ORDER BY a.name, sale_date

SELECT * FROM dds.factvehiclesale a 
join dds.dimcustomer b on a.buyerkey = b.customerkey
WHERE vehiclekey IN (233389,232129,199178,207281,216451)

SELECT name, COUNT(*) from #deals a
JOIN dds.factvehiclesale b on a.vehiclekey = b.vehiclekey
join dds.dimcustomer c on b.buyerkey = c.customerkey where c.customerkey is not null GROUP BY name

SELECT a.stocknumber
FROM #deals a
LEFT JOIN dds.factvehiclesale b on a.vehiclekey = b.vehiclekey
  AND a.stocknumber = b.stocknumber
LEFT JOIN dds.dimcustomer c on b.buyerkey = c.customerkey  
WHERE c.customerkey IS NOT NULL

-- mar 23 no toyotas?!?
-- the issue turns out to be be dimcustomer
select * FROM #deals
DROP TABLE #sales;
SELECT *
INTO #sales
from (
  SELECT a.stocknumber, c.vin, cast(b.soldts AS sql_date) AS sale_date, e.name
  FROM VehicleInventoryItems a 
  INNER JOIN vehicleSales b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN VehicleItems c on a.VehicleItemID = c.VehicleItemID 
  INNER JOIN selectedreconpackages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID
    AND d.typ = 'ReconPackage_Nice'
  LEFT JOIN organizations e on a.locationid = e.partyid
  WHERE cast(b.soldts AS sql_date) BETWEEN '03/01/2023' AND '03/31/2023' ----------------- sales in mar 23 for report on 9/1/23
    AND b.status = 'VehicleSale_Sold'
    AND stocknumber LIKE 'T%'
  GROUP BY a.stocknumber, c.vin, cast(b.soldts AS sql_date), e.name)aa
	
SELECT COUNT(*) FROM #sales;  23

	
  select b.thedate, a.ro, c.vehiclekey
  from dds.factrepairorder a
  inner join dds.day b on a.opendatekey = b.datekey
  inner join dds.dimvehicle c on a.vehiclekey = c.vehiclekey
	--  AND c.vin IN (SELECT vin FROM #sales)
  INNER JOIN dds.dimservicetype d on a.servicetypekey = d.servicetypekey
    AND d.servicetypecode IN ('AM','MR','QL','QS','MN')
  where b.yearmonth > 202301
  GROUP BY b.thedate, a.ro, c.vehiclekey	