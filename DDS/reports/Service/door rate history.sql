SELECT *
FROM factroline a
INNER JOIN (
  SELECT ro
  FROM factroline
  GROUP BY ro
  HAVING COUNT(*) = 1) b ON a.ro = b.ro
LEFT JOIN factro c ON a.ro = c.ro  
WHERE lineflaghours = 1  
AND NOT EXISTS (
  SELECT 1
  FROM stgarkonasdplopc
  WHERE soco# = a.storecode
    AND solopc = a.corcode
    AND soramt <> 0)
      
      
SELECT servicetype, paytype, laborsales, COUNT(*), MIN(thedate), MAX(thedate), MIN(a.ro), MAX(a.ro)
FROM factroline a
INNER JOIN (
  SELECT ro
  FROM factroline
  GROUP BY ro
  HAVING COUNT(*) = 1) b ON a.ro = b.ro
LEFT JOIN factro c ON a.ro = c.ro  
LEFT JOIN day d ON c.closedatekey = d.datekey
WHERE lineflaghours = 1  
AND a.storecode = 'ry1'
AND NOT EXISTS (
  SELECT 1
  FROM stgarkonasdplopc
  WHERE soco# = a.storecode
    AND solopc = a.corcode
    AND soramt <> 0)
GROUP BY servicetype, paytype, laborsales   
HAVING timestampdiff(sql_tsi_day, MIN(thedate), MAX(thedate)) > 2 
ORDER BY servicetype, paytype, MIN(thedate)