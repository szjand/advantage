-- GM Drivetrain
DROP TABLE #andrew_1;
-- select c.thedate, a.miles, a.customerkey, b.vin, b.modelyear, b.make, b.model, b.body
SELECT a.ro
INTO #andrew_1
FROM factrepairorder a
INNER JOIN dimvehicle b on a.vehiclekey = b.vehiclekey
  AND modelyear IN ('2008','2009','2010','2011','2012','2013','2014','2015')
INNER JOIN day c on a.finalclosedatekey = c.datekey  
WHERE a.miles BETWEEN 45000 AND 110000
  AND ((b.make = 'chevrolet' AND b.model IN ('silverado','tahoe','suburban'))
    OR b.make = 'gmc' AND b.model IN ('sierra','yukon','yukon xl','denali'))
    OR (b.make = 'cadillac' AND b.model IN ('escalade','escalade esv','ext')))
  AND NOT EXISTS ( -- vehicle has NOT had one of the opcode list
    SELECT 1 
	FROM factrepairorder aa
	INNER JOIN dimvehicle bb on aa.vehiclekey = bb.vehiclekey
	INNER JOIN dimopcode cc on aa.opcodekey = cc.opcodekey
	  AND cc.opcode IN ('wtff','wtffd','wtffdod','fdiff','rdiff','stc')
    WHERE aa.vehiclekey = b.vehiclekey)	  
  AND c.thedate = ( -- most recent service visit
    SELECT MAX(thedate)
	FROM factrepairorder aaa
	INNER JOIN day bbb on aaa.finalclosedatekey = bbb.datekey
	WHERE aaa.vehiclekey = a.vehiclekey)	
--group by c.thedate, a.miles, a.customerkey, b.vin, b.modelyear, b.make, b.model, b.body
GROUP BY a.ro

DROP TABLE #andrew_2;
SELECT a.ro, c.thedate, b.vehiclekey, b.customerkey
INTO #andrew_2
FROM #andrew_1 a
INNER JOIN factrepairorder b on a.ro = b.ro
INNER JOIN day c on b.finalclosedatekey = c.datekey
GROUP BY a.ro, c.thedate, b.vehiclekey, b.customerkey

-- duplicate vins due to multiple ros on same day
SELECT a.*, b.vin
FROM #andrew_2 a
INNER JOIN dimvehicle b on a.vehiclekey = b.vehiclekey
WHERE a.vehiclekey IN (
select vehiclekey
FROM #andrew_2
GROUP BY vehiclekey 
HAVING COUNT(*) > 1)
ORDER BY b.vin

SELECT COUNT(*) FROM (
select MAX(ro), thedate,vehiclekey,customerkey
FROM #andrew_2
WHERE thedate <= curdate()
GROUP BY thedate, vehiclekey,customerkey
)x

-- wanted most recent ro to get most relevant mileage
SELECT fullname, homephone, businessphone, cellphone, email
FROM (
  select fullname, homephone, businessphone, cellphone,
    CASE 
      WHEN emailvalid THEN email
  	WHEN email2valid THEN email2
  	ELSE 'None'
    END AS email
  FROM (
    select MAX(ro) AS ro, thedate, vehiclekey,customerkey
    FROM #andrew_2
    WHERE thedate <= curdate()
      AND customerkey <> 2 -- inventory
    GROUP BY thedate, vehiclekey,customerkey) a
  LEFT JOIN dimvehicle b on a.vehiclekey = b.vehiclekey
  LEFT JOIN dimcustomer c on a.customerkey = c.customerkey) x
WHERE homephone <> '0'
  OR businessphone <> '0'
  OR cellphone <> '0'
  OR email <> 'None'  
GROUP BY fullname, homephone, businessphone, cellphone, email  
ORDER BY fullname


--------------------------------------------------------------------------------
-- 2000 - 2012 V6 Honda Vehicles: Timing Belt
select a.*
INTO #honda_v6
FROM dimvehicle a
LEFT JOIN chrome.vinpattern b on LEFT(b.vinpattern, 8) = LEFT(a.vin, 8)
  AND a.vin LIKE replace(b.vinpattern, '*','_')
INNER JOIN chrome.category c on b.enginetypecategoryid = c.categoryid 
  AND c.description = 'V6 Cylinder Engine'
WHERE a.make = 'honda'
  AND a.modelyear BETWEEN '2000' AND '2012'
  AND A.MODEL in ('Odyssey','pilot','accord')

select fullname, homephone, businessphone, cellphone,
  CASE 
    WHEN emailvalid THEN email
	WHEN email2valid THEN email2
	ELSE 'None'
  END AS email
FROM (
  SELECT customerkey
  FROM factRepairOrder a
  INNER JOIN #honda_v6 b on a.vehiclekey = b.vehiclekey
  WHERE a.storecode = 'RY2'
    AND NOT EXISTS (
      SELECT 1
  	FROM factrepairorder aa
  	INNER JOIN dimopcode bb on aa.opcodekey = bb.opcodekey
  	  AND opcode = 'RTB'
  	WHERE ro = a.ro)
  GROUP BY customerkey) c
INNER JOIN dimcustomer d on c.customerkey = d.customerkey  
WHERE d.customerkey NOT IN (1,2)
  AND (homephone <> '0'
  OR businessphone <> '0'
  OR cellphone <> '0'
  OR email <> 'None')  
ORDER BY d.fullname

--------------------------------------------------------------------------------
-- Honda AC System

select fullname, homephone, businessphone, cellphone,
  CASE 
    WHEN emailvalid THEN email
	WHEN email2valid THEN email2
	ELSE 'None'
  END AS email
FROM (
  SELECT customerkey
  FROM factrepairorder a
  INNER JOIN  dimvehicle b on a.vehiclekey = b.vehiclekey
    AND b.modelyear <> 'UNKN'
    AND b.modelyear BETWEEN '1998' AND '2010'
  WHERE a.storecode = 'RY2'  
    AND NOT EXISTS (
      SELECT 1
  	FROM factrepairorder aa
  	INNER JOIN dimopcode bb on aa.opcodekey = bb.opcodekey
  	  AND bb.opcode = '620020'
  	WHERE ro = a.ro)
  GROUP BY customerkey) c	
INNER JOIN dimcustomer d on c.customerkey = d.customerkey  
WHERE d.customerkey NOT IN (1,2)
  AND (
    d.homephone <> '0'
    OR d.businessphone <> '0'
    OR d.cellphone <> '0'
    OR d.email <> 'None')  
	
	
---------------------------------------------------------------------------------

-- honda nissan drivetrain	
select a.*
INTO #honda_dt
FROM dimvehicle a
LEFT JOIN chrome.vinpattern b on LEFT(b.vinpattern, 8) = LEFT(a.vin, 8)
  AND a.vin LIKE replace(b.vinpattern, '*','_')
--INNER JOIN chrome.category c on b.enginetypecategoryid = c.categoryid 
--  AND c.description = 'V6 Cylinder Engine'
WHERE (
  (a.make = 'honda' AND a.model IN ('ridgeline','pilot'))
  OR
  (a.make = 'nissan' AND a.model IN ('titan','xterra','frontier')))
  AND a.modelyear BETWEEN '2008' AND '2015'
  AND vinstylename LIKE '%4WD%'

select fullname, homephone, businessphone, cellphone,
  CASE 
    WHEN emailvalid THEN email
	WHEN email2valid THEN email2
	ELSE 'None'
  END AS email
FROM (  
  select customerkey
  FROM factrepairorder a
  INNER JOIN #honda_dt b on a.vehiclekey = b.vehiclekey
  WHERE a.storecode = 'RY2'
    AND a.miles BETWEEN 45000 AND 110000  
    AND NOT EXISTS (
      SELECT 1
  	FROM factrepairorder aa
  	INNER JOIN dimopcode bb on aa.opcodekey = bb.opcodekey
  	WHERE ro = a.ro
  	  AND bb.opcode = 'HWTS')
  GROUP BY customerkey) c	  
INNER JOIN dimcustomer d on c.customerkey = d.customerkey  
WHERE d.customerkey NOT IN (1,2)
  AND (
    d.homephone <> '0'
    OR d.businessphone <> '0'
    OR d.cellphone <> '0'
    OR d.email <> 'None')  
	