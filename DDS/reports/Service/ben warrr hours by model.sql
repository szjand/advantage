SELECT ro, flaghours, 
  (SELECT thedate FROM day WHERE datekey = a.openDateKey),
  b.model,
  CASE 
    WHEN EXISTS (
      SELECT 1
      FROM dimccc
      WHERE ro = a.ro 
        AND (lower(cast(complaint AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(complaint AS sql_longvarchar)) LIKE '%campaign%' OR 
          lower(cast(correction AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(correction AS sql_longvarchar)) LIKE '%campaign%'))  THEN 'yes'
    ELSE 'no'
  end
FROM factrepairorder a
LEFT JOIN dimVehicle b on a.vehicleKey = b.vehicleKey
--INNER JOIN day b on a.flagdatekey = b.datekey
--  AND b.thedate BETWEEN curdate() - 90 AND curdate()
--INNER JOIN dimPaymentType c on a.paymentTypeKey = c.paymentTypeKey
--  AND c.paymentTypeCode = 'w'
--WHERE b.thedate BETWEEN curdatE() - 90 AND curdate()
WHERE a.paymentTypeKey = (
    SELECT paymentTypeKey
    FROM dimPaymentType
    WHERE paymentTypeCode = 'w')
AND a.flagdatekey IN (
  SELECT datekey
  FROM day 
  WHERE thedate BETWEEN curdate() - 90 AND curdate())
AND a.storecode = 'ry1'
AND a.servicetypeKey = ( -- eliminate PDQ
  SELECT serviceTypeKey
  FROM dimSErviceType
  WHERE serviceTypeCode = 'MR')
AND round(a.flaghours, 2) > 0

include a flag for pdi

-- 9/15
what i think i want IS something LIKE

month   total warr flag hours    recall flag hours   pdi flag hours  other war flag hours
shit LEFT out model

SELECT ro, line FROM (
SELECT *
FROM factrepairorder a
INNER JOIN day aa on a.flagdatekey = aa.datekey
  AND year(aa.thedate) = 2014
INNER JOIN dimVehicle b on a.vehicleKey = b.vehicleKey
INNER JOIN dimOpcode c on a.opcodeKey = c.opcodekey
INNER JOIN dimCCC d on a.ccckey = d.ccckey
WHERE a.paymentTypeKey = (
    SELECT paymentTypeKey
    FROM dimPaymentType
    WHERE paymentTypeCode = 'w')
--AND a.flagdatekey IN (
----  SELECT datekey
--  FROM day 
--  WHERE thedate BETWEEN curdate() - 90 AND curdate())
AND a.storecode = 'ry1'
AND a.servicetypeKey = ( -- eliminate PDQ
  SELECT serviceTypeKey
  FROM dimSErviceType
  WHERE serviceTypeCode = 'MR')
AND round(a.flaghours, 2) > 0
AND a.ro = '16139771' AND a.line = 1
) x GROUP BY ro,line HAVING COUNT(*) > 1
-- ok, getting multiples rows for a line, GROUP AND SUM flag hours on ro, line

-- DROP TABLE #war;
SELECT a.ro, a.line, aa.thedate, SUM(a.flaghours) AS flaghours, b.vehicleKey, c.opcodeKey,
  d.ccckey
INTO #war  
FROM factrepairorder a
INNER JOIN day aa on a.opendatekey = aa.datekey
  AND year(aa.thedate) = 2014
INNER JOIN dimVehicle b on a.vehicleKey = b.vehicleKey
INNER JOIN dimOpcode c on a.opcodeKey = c.opcodekey
INNER JOIN dimCCC d on a.ccckey = d.ccckey
WHERE a.paymentTypeKey = (
    SELECT paymentTypeKey
    FROM dimPaymentType
    WHERE paymentTypeCode = 'w')
AND a.storecode = 'ry1'
AND a.servicetypeKey = ( -- eliminate PDQ
  SELECT serviceTypeKey
  FROM dimSErviceType
  WHERE serviceTypeCode = 'MR')
AND round(a.flaghours, 2) > 0
GROUP BY a.ro, a.line, aa.thedate, b.vehicleKey, c.opcodeKey, d.ccckey

SELECT month(a.thedate),
  round(SUM(flaghours), 0) AS "Total War Flag Hours",
  round(SUM(CASE WHEN c.opcode LIKE '%pdi%' THEN flaghours ELSE 0 END), 0) AS "PDI Flag Hours",
  round(SUM(CASE WHEN (lower(cast(d.complaint AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.complaint AS sql_longvarchar)) LIKE '%campaign%' OR 
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%campaign%') THEN flaghours ELSE 0 END), 0) AS "Recall Flag Hours",
  round(SUM(CASE WHEN c.opcode NOT LIKE '%pdi%' AND (    
      (lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%recall%' AND
        lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%campaign%' AND
        lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%recall%' AND
        lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%campaign%')) THEN flaghours ELSE 0 END), 0) AS "Other War Flag Hours"        
FROM #war a
INNER JOIN dimVehicle b on a.vehicleKey = b.vehicleKey
INNER JOIN dimOpcode c on a.opcodekey = c.opcodekey
INNER JOIN dimCCC d on a.ccckey = d.ccckey
GROUP BY month(a.thedate)

SELECT b.make, b.model, COUNT(*), MIN(ro), MAX(ro)
FROM #war a
INNER JOIN dimvehicle b on a.vehiclekey = b.vehiclekey
GROUP BY b.make, b.model
ORDER BY COUNT(*) DESC

-- include just specific makes
SELECT make, month(a.thedate),
  round(SUM(flaghours), 0) AS "Total War Flag Hours",
  round(SUM(CASE WHEN c.opcode LIKE '%pdi%' THEN flaghours ELSE 0 END), 0) AS "PDI Flag Hours",
  round(SUM(CASE WHEN (lower(cast(d.complaint AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.complaint AS sql_longvarchar)) LIKE '%campaign%' OR 
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%campaign%') THEN flaghours ELSE 0 END), 0) AS "Recall Flag Hours",
  round(SUM(CASE WHEN c.opcode NOT LIKE '%pdi%' AND (    
      (lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%recall%' AND
        lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%campaign%' AND
        lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%recall%' AND
        lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%campaign%')) THEN flaghours ELSE 0 END), 0) AS "Other War Flag Hours"
-- SELECT COUNT(*)                
FROM #war a
INNER JOIN dimVehicle b on a.vehicleKey = b.vehicleKey
INNER JOIN dimOpcode c on a.opcodekey = c.opcodekey
INNER JOIN dimCCC d on a.ccckey = d.ccckey
WHERE make IN ('buick','cadillac','chevrolet', 'saturn', 'gmc')
GROUP BY make, month(a.thedate)

-- just talked to ben
the actual situation he IS trying to address IS, according to ken/andrew:
  WHEN a particular model comes IN for a recall, they know, thru past experience
  that there IS a great likelihood that there will be additional warranty WORK
  that will need to be done on that model
  therefor they should only sched X number of that model, because although the
  recall itself represents a fixed amount of WORK, they know that a significant
  amount of additional warranty WORK will need to be done
  
so, the question becomes, of those models that come IN for warranty WORK, 
how much additional warranty WORK IS done

the base TABLE IS only those vehicles on with recalls are performed
SELECT * 
-- SELECT COUNT(*)
FROM #war a
WHERE EXISTS (
  SELECT 1
  FROM dimCCC
  WHERE ro = a.ro
    AND (lower(cast(complaint AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(complaint AS sql_longvarchar)) LIKE '%campaign%' OR 
          lower(cast(correction AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(correction AS sql_longvarchar)) LIKE '%campaign%'))
          

-- redefine #war, limit to repair orders with recalls, AND april 14 - curdate()
-- take flag hours out of #war, just want the ros IN the time period that had recall work
-- DROP TABLE #war;
SELECT a.ro, a.line, aa.thedate, b.vehicleKey, c.opcodeKey,
  d.ccckey
INTO #war  
FROM factrepairorder a
INNER JOIN day aa on a.opendatekey = aa.datekey
  AND aa.thedate BETWEEN '04/01/2014' AND curdate()
INNER JOIN dimVehicle b on a.vehicleKey = b.vehicleKey
INNER JOIN dimOpcode c on a.opcodeKey = c.opcodekey
INNER JOIN dimCCC d on a.ccckey = d.ccckey
  AND (lower(cast(d.complaint AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.complaint AS sql_longvarchar)) LIKE '%campaign%' OR 
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%campaign%')
WHERE a.paymentTypeKey = (
    SELECT paymentTypeKey
    FROM dimPaymentType
    WHERE paymentTypeCode = 'w')
AND a.storecode = 'ry1'
AND a.servicetypeKey = ( -- eliminate PDQ
  SELECT serviceTypeKey
  FROM dimSErviceType
  WHERE serviceTypeCode = 'MR')
AND round(a.flaghours, 2) > 0
GROUP BY a.ro, a.line, aa.thedate, b.vehicleKey, c.opcodeKey, d.ccckey;

-- now, break out the different warranty spends, on an ro BY ro basis
-- yuck this IS NOT workin 16153915: line 1 other war WORK, lines 2,3 recall WORK
-- ok, getting better, very CLOSE, that ro looks ok now
SELECT aa.ro, aa.line,  b.model, 
  CASE WHEN c.opcode LIKE '%pdi%' THEN flaghours ELSE 0 END AS "PDI Flag Hours",
  CASE 
    WHEN (lower(cast(d.complaint AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.complaint AS sql_longvarchar)) LIKE '%campaign%' OR 
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%campaign%') THEN flaghours ELSE 0 
  END AS "Recall Flag Hours",
  CASE 
    WHEN c.opcode NOT LIKE '%pdi%' AND    
       lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%recall%' AND
       lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%campaign%' AND
       lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%recall%' AND
       lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%campaign%' THEN flaghours ELSE 0 
  END AS "Other War Flag Hours"
-- FROM #war a    
FROM factRepairOrder aa 
INNER JOIN dimVehicle b on aa.vehicleKey = b.vehicleKey
INNER JOIN dimOpcode c on aa.opcodekey = c.opcodekey
INNER JOIN dimCCC d on aa.ccckey = d.ccckey
WHERE aa.ro IN (SELECT ro FROM #war)
  AND aa.ro = '16153915'
  
-- lets DO some grouping AND summing: 1 row per ro  
SELECT aa.ro, b.model, 
  sum(CASE WHEN c.opcode LIKE '%pdi%' THEN flaghours ELSE 0 END) AS "PDI Flag Hours",
  sum(CASE 
    WHEN (lower(cast(d.complaint AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.complaint AS sql_longvarchar)) LIKE '%campaign%' OR 
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%recall%' OR
          lower(cast(d.correction AS sql_longvarchar)) LIKE '%campaign%') THEN flaghours ELSE 0 
  END) AS "Recall Flag Hours",
  sum(CASE 
    WHEN c.opcode NOT LIKE '%pdi%' AND    
       lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%recall%' AND
       lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%campaign%' AND
       lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%recall%' AND
       lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%campaign%' THEN flaghours ELSE 0 
  END) AS "Other War Flag Hours"
-- FROM #war a    
FROM factRepairOrder aa 
INNER JOIN dimVehicle b on aa.vehicleKey = b.vehicleKey
INNER JOIN dimOpcode c on aa.opcodekey = c.opcodekey
INNER JOIN dimCCC d on aa.ccckey = d.ccckey
WHERE aa.ro IN (SELECT ro FROM #war)
--  AND aa.ro = '16153915'
GROUP BY aa.ro, b.model  

-- lets DO some grouping AND summing: 1 row per ro  
-- AND there IS other warranty WORK done
SELECT model, round(avg("Other War Flag Hours"), 2) AS "Avg Other War Flag Hours", MIN(ro), MAX(ro), COUNT(*)
FROM (
  SELECT aa.ro, b.model, 
    sum(CASE WHEN c.opcode LIKE '%pdi%' THEN flaghours ELSE 0 END) AS "PDI Flag Hours",
    sum(CASE 
      WHEN (lower(cast(d.complaint AS sql_longvarchar)) LIKE '%recall%' OR
            lower(cast(d.complaint AS sql_longvarchar)) LIKE '%campaign%' OR 
            lower(cast(d.correction AS sql_longvarchar)) LIKE '%recall%' OR
            lower(cast(d.correction AS sql_longvarchar)) LIKE '%campaign%') THEN flaghours ELSE 0 
    END) AS "Recall Flag Hours",
    sum(CASE 
      WHEN c.opcode NOT LIKE '%pdi%' AND    
         lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%recall%' AND
         lower(cast(d.complaint AS sql_longvarchar)) NOT LIKE '%campaign%' AND
         lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%recall%' AND
         lower(cast(d.correction AS sql_longvarchar)) NOT LIKE '%campaign%' THEN flaghours ELSE 0 
    END) AS "Other War Flag Hours"
  -- FROM #war a    
  FROM factRepairOrder aa 
  INNER JOIN dimVehicle b on aa.vehicleKey = b.vehicleKey
  INNER JOIN dimOpcode c on aa.opcodekey = c.opcodekey
  INNER JOIN dimCCC d on aa.ccckey = d.ccckey
  WHERE aa.ro IN (SELECT ro FROM #war)
  --  AND aa.ro = '16153915'
  GROUP BY aa.ro, b.model) k
WHERE "Other War Flag Hours" > 0 
GROUP BY model 
ORDER BY COUNT(*) desc