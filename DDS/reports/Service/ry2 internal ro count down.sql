-- raw ry2 internal ros
SELECT *
FROM factro a
INNER JOIN day b ON a.finalclosedatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND c.paytype = 'i'
WHERE b.yearmonth IN (201301,201302)
  AND a.storecode = 'ry2'
  AND a.void = false
  
-- internal ro counts BY month

SELECT *
FROM factro a
INNER JOIN day b ON a.finalclosedatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND c.paytype = 'i'
WHERE b.yearmonth IN (201301,201302)
  AND a.storecode = 'ry2'
  AND a.void = false

-- DROP TABLE #wtf
SELECT * 
INTO #wtf
FROM zcb a
INNER JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
INNER JOIN day c ON b.gtdate = c.thedate
WHERE a.name = 'cb2'
  AND a.dl2 = 'RY2'
  AND a.dl4 = 'parts' 
  AND a.al4 = 'sales'
  AND c.yearmonth IN (201301,201302)
  AND b.gtdtyp = 'S'
--  AND b.gtjrnl = 'SVI'

SELECT yearmonth, gtjrnl, SUM(gttamt)
FROM #wtf
GROUP BY yearmonth, gtjrnl

SELECT gtdoc#, gtjrnl, yearmonth, SUM(gttamt)
FROM #wtf
GROUP BY gtdoc#, gtjrnl, yearmonth

SELECT gtdoc#
FROM (
  SELECT gtdoc#, gtjrnl, yearmonth, SUM(gttamt)
  FROM #wtf
  GROUP BY gtdoc#, gtjrnl, yearmonth) a
GROUP BY gtdoc# 
HAVING COUNT(*) > 1  


SELECT *
FROM (
  SELECT gtdoc#, gtjrnl, yearmonth, SUM(gttamt)
  FROM #wtf
  GROUP BY gtdoc#, gtjrnl, yearmonth) a