mike wants to determine IF flag hours for aftermarket team opcodes make sense

SELECT *
FROM scotest.tptechs ORDER BY lastname

alex: 608
lupe: 612
kyle: 613

SELECT * FROM dimtech

SELECT * FROM dimccc

SELECT COUNT(*) FROM (
SELECT b.technumber, b.name, e.thedate, a.ro, a.flaghours, 
  c.opcode AS opcode, left(c.description, 50)
  d.opcode AS corcode, left(c.description, 50) 
  left(f.complaint, 50) AS complaint, left(f.correction, 50) AS correction
FROM factRepairOrder a
INNER JOIN dimTech b on a.techKey = b.techKey
INNER JOIN dimOpcode c on a.opcodekey = c.opcodekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN day e on a.flagdatekey = e.datekey
LEFT JOIN dimCCC f on a.ccckey = f.ccckey
WHERE a.flagdatekey IN (
  SELECT datekey
  FROM day
  WHERE thedate BETWEEN curdate() - 60 AND curdate())
AND b.technumber IN ('608','612','613')  
) x

SELECT b.technumber, b.name, e.thedate, a.ro, a.flaghours, 
  c.opcode AS opcode, left(c.description, 50) AS "opcode desc", 
  d.opcode AS corcode, 
  left(d.description, 50)  AS "corcode desc",  
  left(f.complaint, 50) AS complaint, left(f.correction, 50) AS correction
FROM factRepairOrder a
INNER JOIN dimTech b on a.techKey = b.techKey
INNER JOIN dimOpcode c on a.opcodekey = c.opcodekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN day e on a.flagdatekey = e.datekey
LEFT JOIN dimCCC f on a.ccckey = f.ccckey
WHERE a.flagdatekey IN (
  SELECT datekey
  FROM day
  WHERE thedate BETWEEN curdate() - 60 AND curdate())
AND b.technumber IN ('608','612','613')
ORDER BY c.opcode, d.opcode, left(f.complaint, 50), left(f.correction, 50)