﻿
-- 5/2/18
ok, base tables (deals, service) for unioning: minimum info, can flesh out more details
once we have the relevant data set
this comes to mind as i consider verifying most recent owner using bopvref


-- deals
drop table if exists deals;
create temp table deals as
select 'purchase'::citext as the_type, a.delivery_date as the_date, a.vin, b.fullname, b.bnkey -- year, c.make, c.model, c.inpmast_vin, c.odometer
from sls.ext_bopmast_partial a 
inner join ads.ext_dim_customer b on a.buyer_bopname_id = b.bnkey
inner join arkona.xfm_inpmast c on a.vin = c.inpmast_vin
where a.delivery_date > current_date - interval '18 months'
  and c.current_row = true
  and (
    c.model like '%SIERRA%'
    or c.model like '%COLORADO%'
    or c.model like '%SILVERADO%'
    or c.model like '%CANYON%'
    or c.model like '%SUBURBAN%'  
    or c.model like '%TAHOE%'
    or c.model like '%YUKON%'
    or c.model like '%ESCALADE%')
  and c.year between 2010 and 2014    
  and length(c.inpmast_vin) = 17  
  and a.sale_type <> 'W';

-- service
drop table if exists service;
create temp table service as
select 'service'::text as the_type, max(d.the_date) as last_visit, c.vin, b.fullname, b.bnkey
from ads.ext_fact_repair_order a
inner join ads.ext_dim_customer b on a.customerkey = b.customerkey
inner join ads.ext_dim_vehicle c on a.vehiclekey = c.vehiclekey
inner join arkona.xfm_inpmast cc on c.vin = cc.inpmast_vin
inner join dds.dim_date d on a.opendatekey = d.date_key
inner join ads.ext_dim_opcode e on a.opcodekey = e.opcodekey
  and e.opcode not in ('fdiff','rdiff','stc')
inner join ads.ext_dim_opcode f on a.corcodekey = f.opcodekey
  and f.opcode not in ('fdiff','rdiff','stc')   
where d.the_date > current_date - interval '18 months'
  and cc.current_row = true
  and (
    c.model like '%SIERRA%'
    or cc.model like '%COLORADO%'
    or cc.model like '%SILVERADO%'
    or cc.model like '%CANYON%'
    or cc.model like '%SUBURBAN%'  
    or cc.model like '%TAHOE%'
    or cc.model like '%YUKON%'
    or cc.model like '%ESCALADE%')
  and cc.year between 2010 and 2014    
  and length(cc.inpmast_vin) = 17  
  and b.fullname not in ('inventory')
group by c.vin, b.fullname, b.bnkey


drop table if exists all_vins;
create table all_vins as -- 3883 rows
select *
from deals 
union 
select *
from service
order by vin;


select vin, max(the_date) as the_date -- 3204 rows
from all_vins
group by vin


select * -- 3228 rows
from all_vins a
inner join (
  select vin, max(the_date) as the_date -- 3204 rows
  from all_vins
  group by vin) b on a.vin = b.vin 
  and a.the_date = b.the_date



select vin -- 24 rows, makes sense
from (
  select a.* -- 3228 rows
  from all_vins a
  inner join (
    select vin, max(the_date) as the_date -- 3204 rows
    from all_vins
    group by vin) b on a.vin = b.vin 
    and a.the_date = b.the_date) c
group by vin having count(*) > 1    

these are all the vins for which there is more than one row on the max date
in each case it is 1 purch & 1 serv, both to the same party
except lacoursiere who has 2 bnkey values
select * -- 48 rows, makes sense
from all_vins a
inner join (
  select vin, max(the_date) as the_date -- 3204 rows
  from all_vins
  group by vin) b on a.vin = b.vin 
  and a.the_date = b.the_date
where a.vin in (
  select vin -- 24 rows
  from (
    select a.* -- 3228 rows
    from all_vins a
    inner join (
      select vin, max(the_date) as the_date -- 3204 rows
      from all_vins
      group by vin) b on a.vin = b.vin 
      and a.the_date = b.the_date) c
  group by vin having count(*) > 1 )  


drop table if exists the_data;
create temp table the_data as
select a.the_date, a.vin, a.fullname, max(bnkey) as bnkey
from all_vins a
inner join (
  select vin, max(the_date) as the_date -- 3204 rows
  from all_vins
  group by vin) b on a.vin = b.vin  and a.the_date = b.the_date
group by a.the_date, a.vin, a.fullname;
create unique index on the_data(vin);



-- i think this is it
select a.*, b.customer_key, b.start_date, b.end_date,
    row_number() over (partition by a.vin order by b.start_date desc) as first_row,
    count(*) over (partition by a.vin) as the_count
from the_data a
left join arkona.ext_bopvref b on a.vin = b.vin
order by a.vin, b.start_date desc

select * from (
select *
from (
  select a.*, b.customer_key, b.start_date, b.end_date,
      row_number() over (partition by a.vin order by b.start_date desc) as first_row,
      count(*) over (partition by a.vin) as the_count
  from the_data a
  left join arkona.ext_bopvref b on a.vin = b.vin) c
--   where first_row = 1
    where bnkey <> customer_key--  order by vin
) x where vin = '1GT121CG0EF102125'
                 1GT121CG0EF102125
  
-- well at least, expected
select vin
from (
select a.*, b.customer_key, b.start_date, b.end_date,
    row_number() over (partition by a.vin order by b.start_date desc) as first_row,
    count(*) over (partition by a.vin) as the_count
from the_data a
left join arkona.ext_bopvref b on a.vin = b.vin) c
where first_row = 1
group by vin having count(*) > 1


select a.*, b.customer_key, b.start_date, b.end_date,
    row_number() over (partition by a.vin order by b.start_date desc) as first_row,
    count(*) over (partition by a.vin) as the_count
from the_data a
left join arkona.ext_bopvref b on a.vin = b.vin
where a.vin in ('1GNSKKE78DR273653','1GT121CG0EF102125','3GCPKSE70DG197800','3GCUKSECXEG484417')
order by a.vin, b.start_date desc  

-- this still feels a smidge goofy, returning all rows in bopvref for each row in all_vins
-- looks like 1GT121CG0EF102125 should be included
-- maybe somethin like start_date needs to be greater than the_Date
select *
from all_vins a
left join arkona.ext_bopvref b on a.vin = b.vin
where a.vin in ('1GNSKKE78DR273653','1GT121CG0EF102125','3GCPKSE70DG197800','3GCUKSECXEG484417')
order by a.vin, a.the_date desc, b.start_date desc


select *
from arkona.ext_bopvref
where vin = '1GT121CG0EF102125'

select *
from arkona.ext_bopname 
where bopname_record_key in (1069324,10196740)


drop table if exists bopvref;
create temp table bopvref as
select vin, customer_key, start_date, end_date
from (
  select customer_key, vin, (select dds.db2_integer_to_date(start_date)) as start_date,
    (select dds.db2_integer_to_date(end_date)) as end_date,
    row_number() over (partition by vin order by start_date desc) as last_row,
      count(*) over (partition by vin) as the_count
  from arkona.ext_bopvref
  where customer_key <> 0
    and length(vin) = 17
    and vin not like '0%'
    and vin not like '111%'
  order by vin) a
where last_row = 1;
create unique index on bopvref(vin);


select aa.fullname, cc.phone_number, cc.cell_phone, cc.email_address, bb.year, bb.make, bb.model, aa.vin
from (
  select a.* 
  from the_data a
  left join bopvref b on a.vin = b.vin
  where bnkey = customer_key) aa
left join arkona.xfm_inpmast bb on aa.vin = bb.inpmast_vin  
  and bb.current_row = true
left join arkona.ext_bopname cc on aa.bnkey = cc.bopname_record_key  
where bb.odometer between 45000 and 85000





select aa.fullname, cc.phone_number, cc.cell_phone, cc.email_address, bb.year, bb.make, bb.model, aa.vin
from (
  select a.* 
  from the_data a
  left join bopvref b on a.vin = b.vin
  where bnkey = customer_key) aa
left join arkona.xfm_inpmast bb on aa.vin = bb.inpmast_vin  
  and bb.current_row = true
left join arkona.ext_bopname cc on aa.bnkey = cc.bopname_record_key  
where bb.odometer between 45000 and 85000
  and vin in (
select vin from (
select aa.fullname, cc.phone_number, cc.cell_phone, cc.email_address, bb.year, bb.make, bb.model, aa.vin
from (
  select a.* 
  from the_data a
  left join bopvref b on a.vin = b.vin
  where bnkey = customer_key) aa
left join arkona.xfm_inpmast bb on aa.vin = bb.inpmast_vin  
  and bb.current_row = true
left join arkona.ext_bopname cc on aa.bnkey = cc.bopname_record_key  
where bb.odometer between 45000 and 85000
) x group by vin having count(*) > 1)   


drop table if exists gm_diff_campaign;
create temp table gm_diff_campaign as
select aa.fullname, cc.phone_number, cc.cell_phone, cc.email_address, bb.year, bb.make, bb.model, aa.vin
from (
  select a.* 
  from the_data a
  left join bopvref b on a.vin = b.vin
  where bnkey = customer_key) aa
left join arkona.xfm_inpmast bb on aa.vin = bb.inpmast_vin  
  and bb.current_row = true
left join arkona.ext_bopname cc on aa.bnkey = cc.bopname_record_key  
where bb.odometer between 45000 and 85000
  and vin not in ('1GC2KXCG9CZ120822','1GCNKPE00DZ206650','1GKS2KE7XCR217421','1GT120CG1CF174202','1GYS4BEF8ER228590','3GCPCREAXBG165189','3GCPKSE76DG237975','3GCUKSEC0EG523063','3GCUKSEC1EG252448','3GTP2VE39BG237416');



select * from gm_diff_campaign



  