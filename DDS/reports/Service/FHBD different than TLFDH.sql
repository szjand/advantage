--      INSERT INTO factTechFlagHoursByDay -- INSERT 2048 
DROP TABLE #wtf1;
      SELECT c.techkey, coalesce(d.EmployeeKey, -1) AS employeekey, b.datekey AS flagdatekey, round(sum(ptlhrs), 2) AS ptlhrs
INTO #wtf1      
      FROM xfmRO a
      LEFT JOIN day b ON a.flagdate = b.thedate
      LEFT JOIN dimTech c ON a.ptco# = c.storecode
        AND a.pttech = c.technumber
        AND b.thedate BETWEEN c.techkeyfromdate AND c.techkeythrudate
      LEFT JOIN edwEmployeeDim d ON c.employeenumber = d.employeenumber
        AND b.thedate BETWEEN d.EmployeeKeyFromDate AND d.EmployeeKeyThruDate  
      WHERE a.ptltyp = 'L'
        AND a.ptcode = 'TT'
        AND a.ptlhrs > 0
        AND a.flagdate IS NOT NULL -- some conversion stragglers
        AND a.status <> 'VOID'  
        AND c.techkey IS NOT NULL  
      GROUP BY c.techkey, coalesce(d.EmployeeKey, -1), b.datekey;

--      INSERT INTO factTechLineFlagDateHours 
DROP TABLE #wtf2;
      SELECT ptco#, ptro#, ptline,
        (SELECT datekey FROM day WHERE thedate = a.flagdate) AS flagdatekey,
        TechGroupKey, TechLineFlagDateHours
INTO #wtf2        
      FROM ( -- this gives me line/tech/flagdate w/hours
        SELECT ptco#, ptro#, ptline, pttech, flagdate, SUM(ptlhrs) AS TechLineFlagDateHours
        FROM xfmro
          WHERE ptltyp = 'l'
          AND ptcode = 'tt'
          AND ptlhrs > 0 -- 8/13: exclude negative hours --AND ptlhrs <> 0
          AND flagdate IS NOT NULL -- some conversion stragglers
          AND status <> 'VOID'
        GROUP BY ptco#, ptro#, ptline, pttech, flagdate) a
      LEFT JOIN FactROLine b ON a.ptco# = b.storecode
        AND a.ptro# = b.ro
        AND a.ptline = b.line
        AND b.LineFlagHours > 0
      LEFT JOIN dimtech c ON a.ptco# = c.storecode
        and a.pttech = c.technumber
        AND flagdate BETWEEN c.techkeyfromdate AND c.techkeythrudate
      LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
        AND d.weightfactor = round(a.TechLineFlagDateHours/b.LineFlagHours, 2) 
      WHERE a.TechLineFlagDateHours > 0
        AND b.LineFlagHours > 0
        AND c.techkey IS NOT NULL;
        
SELECT * FROM #wtf1   

SELECT * FROM #wtf2     

so, what IS common BETWEEN the 2
1.datekey = 2.datekey
1.techkey = 2.techkey FROM techgroupkey
DROP TABLE #FHD;
SELECT a.thedate, coalesce(b.techkey, -1) AS FHDTech, coalesce(round(SUM(ptlhrs), 2), 0) AS FHDHours
INTO #FHD
FROM day a
LEFT JOIN #wtf1 b ON a.datekey = b.flagdatekey
WHERE a.thedate BETWEEN curdate() - 90 AND curdate()
GROUP BY a.thedate, b.techkey

SELECT a.thedate, coalesce(c.techkey, -1) AS TLTech, coalesce(round(SUM(TechLineFlagDateHours), 2), 0) AS TLHours 
INTO #TL
FROM day a
LEFT JOIN #wtf2 b ON a.datekey = b.flagdatekey
LEFT JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey        
WHERE a.thedate BETWEEN curdatE() - 90 AND curdate()
GROUP BY a.thedate, c.techkey

-- holy shit, pretty CLOSE, ONly one outlier FROM ALL of xfmro
SELECT *
FROM #FHD a
full OUTER JOIN #TL b ON a.thedate = b.thedate
  AND a.fhdtech = b.tltech
WHERE fhdhours <> tlhours

SELECT ptro#, SUM(techlineflagdatehours)
FROM #wtf2
WHERE flagdatekey = (SELECT datekey FROM day WHERE thedate = '02/23/2013')
AND techgroupkey in (SELECT techgroupkey FROM bridgetechgroup WHERE techkey = 109)
GROUP BY ptro#

missing FROM #wtf2
2675985 ro shows 2/13 with flag hours to 999 IS missing

SELECT *
FROM xfmro
WHERE ptro# = '2675985'

SELECT *
FROM (
        SELECT ptco#, ptro#, ptline, pttech, flagdate, SUM(ptlhrs) AS TechLineFlagDateHours
        FROM xfmro
          WHERE ptltyp = 'l'
          AND ptcode = 'tt'
          AND ptlhrs > 0 -- 8/13: exclude negative hours --AND ptlhrs <> 0
          AND flagdate IS NOT NULL -- some conversion stragglers
          AND status <> 'VOID'
          AND ptro# = '2675985'
        GROUP BY ptco#, ptro#, ptline, pttech, flagdate) a
      LEFT JOIN FactROLine b ON a.ptco# = b.storecode
        AND a.ptro# = b.ro
        AND a.ptline = b.line
        AND b.LineFlagHours > 0     
/******************************************************************************/
/******************************************************************************/  
/******************************************************************************/  
/******************************************************************************/         
!!! line 1 missing FROM factroline    
SELECT * FROM factroline WHERE ro  = '2675985'    
/******************************************************************************/
/******************************************************************************/  
/******************************************************************************/  
/******************************************************************************/     
 
      SELECT ptco#, ptro#, ptline,
        (SELECT datekey FROM day WHERE thedate = linedate) AS linedatekey,
        ServType, PayType, OpCode, coalesce(CorCode, 'N/A') AS CorCode, LineHours
      FROM (
        SELECT ptco#, ptro#, ptline,
          max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
          MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
          MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
          MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
          MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
          SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours
        FROM xfmRO
        WHERE status <> 'VOID'
        GROUP BY ptco#, ptro#, ptline) a
        
      WHERE ptro# = '2675985'

need to revisit what i mean BY linedate 
      
SELECT * FROM xfmro WHERE ptro# = '2675985'

SELECT * FROM xfmro WHERE linedate = '12/31/9999' AND flagdate <> '12/31/9999'

    