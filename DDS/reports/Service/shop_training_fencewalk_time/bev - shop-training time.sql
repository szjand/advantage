
SELECT MIN(thedate), MAX(thedate)
FROM day 
WHERE biweeklypayperiodstartdate > '01/01/2016'
  AND biweeklypayperiodenddate < curdate()
  

SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate,
    CAST((SELECT mmdd FROM day WHERE thedate = a.biweeklypayperiodstartdate) AS sql_char)
	+ ' -> '
	+ CAST((SELECT mmdd FROM day WHERE thedate = a.biweeklypayperiodenddate) AS sql_char)
FROM (
  SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  WHERE biweeklypayperiodstartdate > '01/01/2016'
    AND biweeklypayperiodenddate < curdate() + 7
  GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate) a	
  
SELECT
	'y.' + '"' + CAST((SELECT mmdd FROM day WHERE thedate = a.biweeklypayperiodstartdate) AS sql_char)
	+ ' -> '
	+ CAST((SELECT mmdd FROM day WHERE thedate = a.biweeklypayperiodenddate) AS sql_char) + '"' + ', '
	+
	'z.' + '"' + CAST((SELECT mmdd FROM day WHERE thedate = a.biweeklypayperiodstartdate) AS sql_char)
	+ ' -> '
	+ CAST((SELECT mmdd FROM day WHERE thedate = a.biweeklypayperiodenddate) AS sql_char) + '",'
FROM (
  SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day a
  WHERE biweeklypayperiodstartdate > '01/01/2016'
    AND biweeklypayperiodenddate < curdate() + 7
  GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate) a
  
   

SELECT d.name, d.technumber, c.opcode, 
  biweeklypayperiodstartdate, biweeklypayperiodenddate,
  SUM(flaghours) AS flaghours
FROM factrepairorder a
INNER JOIN (  
  SELECT thedate, datekey, biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM day 
  WHERE biweeklypayperiodstartdate > '01/01/2016'
    AND biweeklypayperiodenddate < curdate())  b on a.closedatekey =  b.datekey
INNER JOIN dimopcode c on (a.opcodekey = c.opcodekey OR a.corcodekey = c.opcodekey)
  AND c.opcode IN ('shop', 'train')	
INNER JOIN dimtech d on a.techkey = d.techkey  
  AND d.flagdeptcode IN ('MR','AM')
WHERE flaghours <> 0
GROUP BY d.name, d.technumber, c.opcode, 
  biweeklypayperiodstartdate, biweeklypayperiodenddate

SELECT x.lastname, x.firstname, x.technumber,
y."01-10 -> 01-23", z."01-10 -> 01-23",
y."01-24 -> 02-06", z."01-24 -> 02-06",
y."02-07 -> 02-20", z."02-07 -> 02-20",
y."02-21 -> 03-05", z."02-21 -> 03-05",
y."03-06 -> 03-19", z."03-06 -> 03-19",
y."03-20 -> 04-02", z."03-20 -> 04-02",
y."04-03 -> 04-16", z."04-03 -> 04-16",
y."04-17 -> 04-30", z."04-17 -> 04-30",
y."05-01 -> 05-14", z."05-01 -> 05-14",
y."05-15 -> 05-28", z."05-15 -> 05-28",
y."05-29 -> 06-11", z."05-29 -> 06-11",
y."06-12 -> 06-25", z."06-12 -> 06-25",
y."06-26 -> 07-09", z."06-26 -> 07-09",
y."07-10 -> 07-23", z."07-10 -> 07-23",
y."07-24 -> 08-06", z."07-24 -> 08-06",
y."08-07 -> 08-20", z."08-07 -> 08-20",
y."08-21 -> 09-03", z."08-21 -> 09-03",
y."09-04 -> 09-17", z."09-04 -> 09-17",
y."09-18 -> 10-01", z."09-18 -> 10-01",
y."10-02 -> 10-15", z."10-02 -> 10-15",
y."10-16 -> 10-29", z."10-16 -> 10-29",
y."10-30 -> 11-12", z."10-30 -> 11-12"
FROM ( 
  SELECT lastname, firstname, technumber
  -- SELECT * 
  FROM scotest.tmpDeptTechCensus 
  WHERE thedate BETWEEN '01/01/2016' AND curdate()
    AND flagdeptcode IN ('mr','am')
    AND lastname IS NOT NULL  
	AND storecode= 'ry1' 
  GROUP BY lastname, firstname, technumber) x  
LEFT JOIN (
  SELECT name, technumber,
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '01/10/2016' THEN flaghours else 0 END) AS "01-10 -> 01-23",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '01/24/2016' THEN flaghours else 0 END) AS "01-24 -> 02-06",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '02/07/2016' THEN flaghours else 0 END) AS "02-07 -> 02-20",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '02/21/2016' THEN flaghours else 0 END) AS "02-21 -> 03-05",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '03/06/2016' THEN flaghours else 0 END) AS "03-06 -> 03-19",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '03/20/2016' THEN flaghours else 0 END) AS "03-20 -> 04-02",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '04/03/2016' THEN flaghours else 0 END) AS "04-03 -> 04-16",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '04/17/2016' THEN flaghours else 0 END) AS "04-17 -> 04-30",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '05/01/2016' THEN flaghours else 0 END) AS "05-01 -> 05-14",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '05/15/2016' THEN flaghours else 0 END) AS "05-15 -> 05-28",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '05/29/2016' THEN flaghours else 0 END) AS "05-29 -> 06-11",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '06/12/2016' THEN flaghours else 0 END) AS "06-12 -> 06-25",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '06/26/2016' THEN flaghours else 0 END) AS "06-26 -> 07-09",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '07/10/2016' THEN flaghours else 0 END) AS "07-10 -> 07-23",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '07/24/2016' THEN flaghours else 0 END) AS "07-24 -> 08-06",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '08/07/2016' THEN flaghours else 0 END) AS "08-07 -> 08-20",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '08/21/2016' THEN flaghours else 0 END) AS "08-21 -> 09-03",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '09/04/2016' THEN flaghours else 0 END) AS "09-04 -> 09-17",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '09/18/2016' THEN flaghours else 0 END) AS "09-18 -> 10-01",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '10/02/2016' THEN flaghours else 0 END) AS "10-02 -> 10-15",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '10/16/2016' THEN flaghours else 0 END) AS "10-16 -> 10-29",
    SUM(CASE WHEN opcode = 'SHOP' AND from_date = '10/30/2016' THEN flaghours else 0 END) AS "10-30 -> 11-12"
  FROM ( 
    SELECT d.name, d.technumber, c.opcode, 
      from_date, thru_date,
      SUM(flaghours) AS flaghours
    FROM factrepairorder a
    INNER JOIN (  
      SELECT thedate, datekey, biweeklypayperiodstartdate AS from_date, biweeklypayperiodenddate AS thru_date
      FROM day 
      WHERE biweeklypayperiodstartdate > '01/01/2016'
        AND biweeklypayperiodenddate < curdate() + 7)  b on a.flagdatekey =  b.datekey
    INNER JOIN dimopcode c on (a.opcodekey = c.opcodekey OR a.corcodekey = c.opcodekey)
      AND c.opcode IN ('shop', 'train')	
    INNER JOIN dimtech d on a.techkey = d.techkey  
      AND d.flagdeptcode IN ('MR','AM')
    WHERE flaghours <> 0
    GROUP BY d.name, d.technumber, c.opcode, 
      from_date, thru_date) d  
  GROUP BY  name, technumber) y on x.technumber = y.technumber
LEFT JOIN (
  SELECT name, technumber,
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '01/10/2016' THEN flaghours else 0 END) AS "01-10 -> 01-23",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '01/10/2016' THEN flaghours else 0 END) AS "01-10 -> 01-23",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '01/24/2016' THEN flaghours else 0 END) AS "01-24 -> 02-06",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '02/07/2016' THEN flaghours else 0 END) AS "02-07 -> 02-20",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '02/21/2016' THEN flaghours else 0 END) AS "02-21 -> 03-05",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '03/06/2016' THEN flaghours else 0 END) AS "03-06 -> 03-19",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '03/20/2016' THEN flaghours else 0 END) AS "03-20 -> 04-02",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '04/03/2016' THEN flaghours else 0 END) AS "04-03 -> 04-16",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '04/17/2016' THEN flaghours else 0 END) AS "04-17 -> 04-30",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '05/01/2016' THEN flaghours else 0 END) AS "05-01 -> 05-14",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '05/15/2016' THEN flaghours else 0 END) AS "05-15 -> 05-28",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '05/29/2016' THEN flaghours else 0 END) AS "05-29 -> 06-11",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '06/12/2016' THEN flaghours else 0 END) AS "06-12 -> 06-25",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '06/26/2016' THEN flaghours else 0 END) AS "06-26 -> 07-09",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '07/10/2016' THEN flaghours else 0 END) AS "07-10 -> 07-23",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '07/24/2016' THEN flaghours else 0 END) AS "07-24 -> 08-06",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '08/07/2016' THEN flaghours else 0 END) AS "08-07 -> 08-20",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '08/21/2016' THEN flaghours else 0 END) AS "08-21 -> 09-03",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '09/04/2016' THEN flaghours else 0 END) AS "09-04 -> 09-17",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '09/18/2016' THEN flaghours else 0 END) AS "09-18 -> 10-01",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '10/02/2016' THEN flaghours else 0 END) AS "10-02 -> 10-15",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '10/16/2016' THEN flaghours else 0 END) AS "10-16 -> 10-29",
    SUM(CASE WHEN opcode = 'TRAIN' AND from_date = '10/30/2016' THEN flaghours else 0 END) AS "10-30 -> 11-12"
  FROM ( 
    SELECT d.name, d.technumber, c.opcode, 
      from_date, thru_date,
      SUM(flaghours) AS flaghours
    FROM factrepairorder a
    INNER JOIN (  
      SELECT thedate, datekey, biweeklypayperiodstartdate AS from_date, biweeklypayperiodenddate AS thru_date
      FROM day 
      WHERE biweeklypayperiodstartdate > '01/01/2016'
        AND biweeklypayperiodenddate < curdate() + 7)  b on a.flagdatekey =  b.datekey
    INNER JOIN dimopcode c on (a.opcodekey = c.opcodekey OR a.corcodekey = c.opcodekey)
      AND c.opcode IN ('shop','train')	
    INNER JOIN dimtech d on a.techkey = d.techkey  
      AND d.flagdeptcode IN ('MR','AM')
    WHERE flaghours <> 0
    GROUP BY d.name, d.technumber, c.opcode, 
      from_date, thru_date) d  
  GROUP BY  name, technumber) z on x.technumber = z.technumber
  
  
-- FENCE WALK
SELECT x.lastname, x.firstname, x.technumber,
  y."01-10 -> 01-23", ' ',
  y."01-24 -> 02-06", ' ', 
  y."02-07 -> 02-20", ' ', 
  y."02-21 -> 03-05", ' ', 
  y."03-06 -> 03-19", ' ', 
  y."03-20 -> 04-02", ' ', 
  y."04-03 -> 04-16", ' ', 
  y."04-17 -> 04-30", ' ', 
  y."05-01 -> 05-14", ' ', 
  y."05-15 -> 05-28", ' ', 
  y."05-29 -> 06-11", ' ', 
  y."06-12 -> 06-25", ' ',
  y."06-26 -> 07-09", ' ', 
  y."07-10 -> 07-23", ' ', 
  y."07-24 -> 08-06", ' ', 
  y."08-07 -> 08-20", ' ', 
  y."08-21 -> 09-03", ' ', 
  y."09-04 -> 09-17", ' ', 
  y."09-18 -> 10-01", ' ', 
  y."10-02 -> 10-15", ' ', 
  y."10-16 -> 10-29", ' ', 
  y."10-30 -> 11-12", ' '
FROM (
  SELECT lastname, firstname, technumber
  -- SELECT * 
  FROM scotest.tmpDeptTechCensus 
  WHERE thedate BETWEEN '01/01/2016' AND curdate()
    AND flagdeptcode IN ('mr','am')
    AND lastname IS NOT NULL  
	AND storecode= 'ry1' 
  GROUP BY lastname, firstname, technumber) x  
LEFT JOIN (  
  SELECT name, technumber,
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '01/10/2016' THEN flaghours else 0 END) AS "01-10 -> 01-23",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '01/10/2016' THEN flaghours else 0 END) AS "01-10 -> 01-23",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '01/24/2016' THEN flaghours else 0 END) AS "01-24 -> 02-06",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '02/07/2016' THEN flaghours else 0 END) AS "02-07 -> 02-20",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '02/21/2016' THEN flaghours else 0 END) AS "02-21 -> 03-05",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '03/06/2016' THEN flaghours else 0 END) AS "03-06 -> 03-19",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '03/20/2016' THEN flaghours else 0 END) AS "03-20 -> 04-02",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '04/03/2016' THEN flaghours else 0 END) AS "04-03 -> 04-16",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '04/17/2016' THEN flaghours else 0 END) AS "04-17 -> 04-30",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '05/01/2016' THEN flaghours else 0 END) AS "05-01 -> 05-14",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '05/15/2016' THEN flaghours else 0 END) AS "05-15 -> 05-28",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '05/29/2016' THEN flaghours else 0 END) AS "05-29 -> 06-11",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '06/12/2016' THEN flaghours else 0 END) AS "06-12 -> 06-25",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '06/26/2016' THEN flaghours else 0 END) AS "06-26 -> 07-09",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '07/10/2016' THEN flaghours else 0 END) AS "07-10 -> 07-23",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '07/24/2016' THEN flaghours else 0 END) AS "07-24 -> 08-06",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '08/07/2016' THEN flaghours else 0 END) AS "08-07 -> 08-20",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '08/21/2016' THEN flaghours else 0 END) AS "08-21 -> 09-03",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '09/04/2016' THEN flaghours else 0 END) AS "09-04 -> 09-17",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '09/18/2016' THEN flaghours else 0 END) AS "09-18 -> 10-01",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '10/02/2016' THEN flaghours else 0 END) AS "10-02 -> 10-15",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '10/16/2016' THEN flaghours else 0 END) AS "10-16 -> 10-29",
    SUM(CASE WHEN opcode = 'FENCE' AND from_date = '10/30/2016' THEN flaghours else 0 END) AS "10-30 -> 11-12"
  FROM ( 
    SELECT d.name, d.technumber, c.opcode, 
      from_date, thru_date,
      SUM(flaghours) AS flaghours
    FROM factrepairorder a
    INNER JOIN (  
      SELECT thedate, datekey, biweeklypayperiodstartdate AS from_date, biweeklypayperiodenddate AS thru_date
      FROM day 
      WHERE biweeklypayperiodstartdate > '01/01/2016'
        AND biweeklypayperiodenddate < curdate() + 7)  b on a.flagdatekey =  b.datekey
    INNER JOIN dimopcode c on (a.opcodekey = c.opcodekey OR a.corcodekey = c.opcodekey)
      AND c.opcode IN ('fence')	
    INNER JOIN dimtech d on a.techkey = d.techkey  
      AND d.flagdeptcode IN ('MR','AM')
    WHERE flaghours <> 0
    GROUP BY d.name, d.technumber, c.opcode, 
      from_date, thru_date) d  
  GROUP BY  name, technumber) y on x.technumber = y.technumber
WHERE y.technumber IS NOT null    