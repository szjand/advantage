SELECT DISTINCT name
FROM zcb

select DISTINCT al3, gmcoa
FROM zcb
WHERE name = 'gmfs gross'
  AND storecode = 'ry1'
  AND dl4 = 'parts'

looks LIKE wholesale sales accounts are 148305 & 148304  
SELECT * 
FROM zcb
WHERE name = 'gmfs gross'
  AND storecode = 'ry1'
  AND dl4 = 'parts'
  AND gmcoa IN ('483','683')  
  
  
select gtacct, gtjrnl, SUM(gttamt)
FROM stgarkonaglptrns  
WHERE gtdate BETWEEN '01/01/2014' AND '06/30/2014'
  AND gtacct IN ('148305','148304')
GROUP BY gtacct, gtjrnl 

select * FROM stgArkonaBOPNAME

SELECT a.gttrn#, a.gtco#, a.gtjrnl, a.gtacct, a.gttamt, a.gtctl#, a.gtdesc-- , b.* 
-- SELECT *
FROM stgarkonaglptrns a 
--LEFT JOIN stgArkonaBOPNAME b on a.gtco# = b.bnco# AND cast(a.gtctl# AS sql_integer) = b.bnkey
WHERE gtdate BETWEEN '06/01/2014' AND '06/30/2014'
  AND gtacct IN ('148305','148304')
  
SELECT a.gtdate AS date, a.gttamt AS amount, a.gtctl# AS invoice, a.gtdesc as customer-- , b.* 
-- SELECT *
FROM stgarkonaglptrns a 
--LEFT JOIN stgArkonaBOPNAME b on a.gtco# = b.bnco# AND cast(a.gtctl# AS sql_integer) = b.bnkey
WHERE gtdate BETWEEN '01/01/2014' AND '06/30/2014'
  AND gtacct IN ('148305','148304')
  

SELECT month(gtdate), monthName, a.gtdesc as customer, sum(a.gttamt)
FROM stgarkonaglptrns a 
LEFT JOIN day b on a.gtdate = b.thedate
WHERE gtdate BETWEEN '01/01/2014' AND '06/30/2014'
  AND gtacct IN ('148305','148304')  
GROUP BY month(gtdate), monthName, a.gtdesc  
ORDER BY gtdesc, month(gtdate)

-- year to date
SELECT m.*, n.jan, n.feb, n.mar, n.apr, n.may, n.jun, n.jul, n.aug, n.sep,
  n.oct, n.nov
FROM (
  SELECT a.gtdesc as customer, cast(round(sum(a.gttamt) * -1, 0) AS sql_integer) AS YTD, 
    cast(round((sum(a.gttamt) * -1/11), 0) as sql_integer) AS AvgPerMo
  FROM stgarkonaglptrns a 
  WHERE gtdate BETWEEN '01/01/2014' AND '11/30/2014'
    AND gtacct IN ('148305','148304')
  GROUP BY a.gtdesc) m  
LEFT JOIN (
SELECT a.gtdesc as customer, 
  round(SUM(CASE WHEN month(gtdate) = 1 THEN gttamt*-1 ELSE 0 END), 0) AS Jan,
  round(SUM(CASE WHEN month(gtdate) = 2 THEN gttamt*-1 ELSE 0 END), 0) AS Feb,
  round(SUM(CASE WHEN month(gtdate) = 3 THEN gttamt*-1 ELSE 0 END), 0) AS Mar,
  round(SUM(CASE WHEN month(gtdate) = 4 THEN gttamt*-1 ELSE 0 END), 0) AS Apr,
  round(SUM(CASE WHEN month(gtdate) = 5 THEN gttamt*-1 ELSE 0 END), 0) AS May,
  round(SUM(CASE WHEN month(gtdate) = 6 THEN gttamt*-1 ELSE 0 END), 0) AS Jun,
  round(SUM(CASE WHEN month(gtdate) = 7 THEN gttamt*-1 ELSE 0 END), 0) AS Jul,
  round(SUM(CASE WHEN month(gtdate) = 8 THEN gttamt*-1 ELSE 0 END), 0) AS Aug,
  round(SUM(CASE WHEN month(gtdate) = 9 THEN gttamt*-1 ELSE 0 END), 0) AS Sep,
  round(SUM(CASE WHEN month(gtdate) = 10 THEN gttamt*-1 ELSE 0 END), 0) AS Oct,
  round(SUM(CASE WHEN month(gtdate) = 11 THEN gttamt*-1 ELSE 0 END), 0) AS Nov
FROM stgarkonaglptrns a 
WHERE gtdate BETWEEN '01/01/2014' AND '11/30/2014'
  AND gtacct IN ('148305','148304')  
GROUP BY a.gtdesc) n on m.customer = n.customer  
ORDER BY m.ytd DESC

-- jul - sep
SELECT m.*, n.jul, n.aug, n.sep
FROM (
  SELECT a.gtdesc as customer, sum(a.gttamt) * -1 AS total
  FROM stgarkonaglptrns a 
  WHERE gtdate BETWEEN '07/31/2014' AND '09/30/2014'
    AND gtacct IN ('148305','148304')
  GROUP BY a.gtdesc) m  
LEFT JOIN (
SELECT a.gtdesc as customer, 
  SUM(CASE WHEN month(gtdate) = 7 THEN gttamt*-1 ELSE 0 END) AS Jul,
  SUM(CASE WHEN month(gtdate) = 8 THEN gttamt*-1 ELSE 0 END) AS Aug,
  SUM(CASE WHEN month(gtdate) = 9 THEN gttamt*-1 ELSE 0 END) AS Sep
FROM stgarkonaglptrns a 
WHERE gtdate BETWEEN '07/31/2014' AND '09/30/2014'
  AND gtacct IN ('148305','148304')  
GROUP BY a.gtdesc) n on m.customer = n.customer  
ORDER BY m.total desc


-- dec 14, jan 15
SELECT m.*, n.dec, n.jan
FROM (
  SELECT a.gtdesc as customer, sum(a.gttamt) * -1 AS total
  FROM stgarkonaglptrns a 
  WHERE gtdate BETWEEN '12/01/2014' AND '01/31/2015'
    AND gtacct IN ('148305','148304')
  GROUP BY a.gtdesc) m  
LEFT JOIN (
SELECT a.gtdesc as customer, 
  SUM(CASE WHEN month(gtdate) = 12 THEN gttamt*-1 ELSE 0 END) AS Dec,
  SUM(CASE WHEN month(gtdate) = 1 THEN gttamt*-1 ELSE 0 END) AS Jan
FROM stgarkonaglptrns a 
WHERE gtdate BETWEEN '12/01/2014' AND '01/31/2015'
  AND gtacct IN ('148305','148304')  
GROUP BY a.gtdesc) n on m.customer = n.customer  
ORDER BY m.total desc