SELECT b.thedate, c.name, a.overtimehours
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
WHERE a.overtimehours <> 0
  AND b.thedate BETWEEN '01/01/2014' AND '11/30/2014'
  AND c.pydeptcode = '06'
  
SELECT b.monthofyear, c.name, sum(a.overtimehours) AS hours
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
WHERE a.overtimehours <> 0
  AND b.thedate BETWEEN '01/01/2014' AND '11/30/2014'
  AND c.pydeptcode = '06'  
GROUP BY b.monthofyear, c.name  

SELECT name,
  SUM(CASE WHEN monthofyear = 1 THEN hours ELSE 0 END) AS Jan,
  SUM(CASE WHEN monthofyear = 2 THEN hours ELSE 0 END) AS Feb,
  SUM(CASE WHEN monthofyear = 3 THEN hours ELSE 0 END) AS Mar,
  SUM(CASE WHEN monthofyear = 4 THEN hours ELSE 0 END) AS Apr,
  SUM(CASE WHEN monthofyear = 5 THEN hours ELSE 0 END) AS May,
  SUM(CASE WHEN monthofyear = 6 THEN hours ELSE 0 END) AS Jun,
  SUM(CASE WHEN monthofyear = 7 THEN hours ELSE 0 END) AS Jul,
  SUM(CASE WHEN monthofyear = 8 THEN hours ELSE 0 END) AS Aug,
  SUM(CASE WHEN monthofyear = 9 THEN hours ELSE 0 END) AS Sep,
  SUM(CASE WHEN monthofyear = 10 THEN hours ELSE 0 END) AS Oct,
  SUM(CASE WHEN monthofyear = 11 THEN hours ELSE 0 END) AS Nov
FROM (  
  SELECT b.monthofyear, c.name, sum(a.overtimehours) AS hours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  WHERE a.overtimehours <> 0
    AND b.thedate BETWEEN '01/01/2014' AND '11/30/2014'
    AND c.pydeptcode = '06'  
  GROUP BY b.monthofyear, c.name) x
GROUP BY name  
ORDER BY name

