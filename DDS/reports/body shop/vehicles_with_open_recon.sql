﻿select *
from ads.ext_recon_authorizations
limit 100


select *
from ads.ext_vehicle_recon_items
where extract(year from ts) = 2018
limit 100


select *
from ads.ext_recon_authorizations aa
left join ads.ext_authorized_recon_items a on aa.reconauthorizationid = a.reconauthorizationid
left join ads.ext_Vehicle_recon_items b on a.vehiclereconitemid = b.vehiclereconitemid
-- where a.status in ('AuthorizedReconItem_InProcess','AuthorizedReconItem_NotStarted')
limit 10


select * from dds.dim_Date where the_date = current_date

select distinct status
from ads.ext_authorized_recon_items

select * from ads.ext_Vehicle_inventory_items where vehicleinventoryitemid = '946735a8-8bda-4a39-9968-439ccde9a0f0'


select a.the_date, b.fromts, b.thruts, c.typ, c.status, c.startts, c.completets, d.*
-- select *
from dds.dim_date a
left join ads.ext_recon_authorizations b on a.the_date between b.fromts::date and b.thruts::date
left join ads.ext_authorized_recon_items c on b.reconauthorizationid = c.reconauthorizationid
left join ads.ext_vehicle_recon_items d on c.vehiclereconitemid = d.vehiclereconitemid
where a.year_month between 201803 and 201804
--   and a.day_of_week = 4 -- wednesday
  and d.typ like 'body%'
  and d.vehicleinventoryitemid = '946735a8-8bda-4a39-9968-439ccde9a0f0'
order by a.the_date  


select '03/01/2018'::timestamp + interval '12 hours'

select *
from ads.ext_vehicle_inventory_item_statuses
where category in ('BodyReconProcess','BodyReconDispatched')
  and status <> 'BodyReconProcess_NoIncompleteReconItems'
limit 200




select a.the_date, b.*
from dds.dim_date a
left join ads.ext_vehicle_inventory_item_statuses b on a.the_date::timestamp + interval '12 hours' between b.fromts and b.thruts
  and b.category in ('BodyReconProcess','BodyReconDispatched')
  and b.status <> 'BodyReconProcess_NoIncompleteReconItems'
where a.year_month = 201805 -- between 201803 and 201804
  and a.day_of_week = 4
order by a.the_date  
limit 100


-- turns out this was WAY simpler
select a.the_date, count(distinct b.vehicleinventoryitemid)
from dds.dim_date a
left join ads.ext_vehicle_inventory_item_statuses b on a.the_date::timestamp + interval '12 hours' between b.fromts and b.thruts
  and b.category in ('BodyReconProcess','BodyReconDispatched')
  and b.status <> 'BodyReconProcess_NoIncompleteReconItems'
-- where a.year_month between 201801 and 201804
where a.the_date between '01/03/2018' and current_date
  and a.day_of_week = 4
group by a.the_date
order by a.the_date



  