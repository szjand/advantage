
SELECT COUNT(DISTINCT ro)
FROM factrepairorder a
WHERE a.serviceWriterKey IN (
  SELECT serviceWriterKey
  FROM dimServiceWriter
  WHERE censusDept = 'BS')
AND closedatekey = 7306
AND flaghours <> 0  


SELECT COUNT(DISTINCT ro)
FROM factrepairorder a
WHERE a.serviceWriterKey IN (
  SELECT serviceWriterKey
  FROM dimServiceWriter
  WHERE censusDept = 'BS')
AND closedateKey = 7306  
AND flaghours <> 0

SELECT cast(ronumber AS sql_char),framehours+repairhours+painthours+glasshours+pdrhours+chipsawayhours AS estHours
FROM bs.roestimates
WHERE thruTS IS NULL 
AND ronumber IS NOT NULL 


SELECT COUNT(DISTINCT ro)
FROM factrepairorder a
WHERE a.serviceWriterKey IN (
  SELECT serviceWriterKey
  FROM dimServiceWriter
  WHERE censusDept = 'BS')
AND closedateKey = 7306  
AND flaghours <> 0


SELECT distinct a.ro, b.*
FROM factrepairorder a
LEFT JOIN (
  SELECT ronumber AS ro,framehours+repairhours+painthours+glasshours+pdrhours+chipsawayhours AS estHours
  FROM bs.roestimates
  WHERE thruTS IS NULL 
  AND ronumber IS NOT NULL) b on cast(a.ro AS sql_integer) = b.ro
WHERE a.serviceWriterKey IN (
  SELECT serviceWriterKey
  FROM dimServiceWriter
  WHERE censusDept = 'BS')
AND closedateKey = 7306  
AND flaghours <> 0

SELECT SUM(diff)
FROM (
SELECT a.ro, a.flaghours, round(coalesce(b.estHours, 0), 2), round(coalesce(b.estHours) - flaghours, 2)  AS diff
FROM (
  SELECT ro, SUM(flaghours) AS flagHours
  FROM factRepairORder x
  WHERE closedateKey = 7306
    AND flaghours  <> 0
    AND x.serviceWriterKey IN (
      SELECT serviceWriterKey
      FROM dimServiceWriter
      WHERE censusDept = 'BS')
  GROUP BY ro) a  
LEFT JOIN (   
  SELECT ronumber AS ro,framehours+repairhours+painthours+glasshours+pdrhours+chipsawayhours AS estHours
  FROM bs.roestimates
  WHERE thruTS IS NULL 
  AND ronumber IS NOT NULL) b on cast(a.ro AS sql_integer) = b.ro  
) x  
ORDER BY  coalesce(b.estHours) - flaghours  DESC   