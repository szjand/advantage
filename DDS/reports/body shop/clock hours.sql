SELECT b.sundaytoSaturdayWeek, b.thedate, a.*, c.lastname, c.firstname
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  AND c.distcode = 'BTEC'
WHERE b.theDate BETWEEN '02/02/2014' AND '03/15/2014'
  AND a.clockhours <> 0
  
SELECT * FROM day WHERE theyear = 2014 AND monthname = 'march'



SELECT MIN(b.thedate) AS fromDate, max(b.thedate) AS thruDate, 
  sum(a.clockHours) AS clockHours, c.lastname, c.firstname
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  AND c.distcode = 'BTEC'
WHERE b.theDate BETWEEN '02/02/2014' AND '03/15/2014'
  AND a.clockhours <> 0
GROUP BY sundayToSaturdayWeek, c.lastname, c.firstname  


SELECT TRIM(n.firstname) + ' ' + TRIM(lastname) AS name, m.fromDate, m.thruDate, clockhours, CAST(NULL AS sql_integer)
FROM (
  SELECT sundayToSaturdayWeek, MIN(thedate) AS fromDate, MAX(theDate) AS thruDate
  FROM day
  WHERE thedate BETWEEN '02/02/2014' AND '03/20/2014'
  GROUP BY sundayToSaturdayWeek) m
LEFT JOIN (
  SELECT sundayToSaturdayWeek, 
    sum(a.clockHours) AS clockHours, c.lastname, c.firstname
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
    AND c.distcode = 'BTEC'
  WHERE b.theDate BETWEEN '02/02/2014' AND '03/20/2014'
    AND a.clockhours <> 0
  GROUP BY sundayToSaturdayWeek, c.lastname, c.firstname) n on m.sundayToSaturdayWeek = n.sundaytoSaturdayWeek
union

SELECT '_Total', m.fromDate, m.thruDate, sum(clockhours) AS clockhours, COUNT(*)
FROM (
  SELECT sundayToSaturdayWeek, MIN(thedate) AS fromDate, MAX(theDate) AS thruDate
  FROM day
  WHERE thedate BETWEEN '02/02/2014' AND '03/20/2014'
  GROUP BY sundayToSaturdayWeek) m
LEFT JOIN (
  SELECT sundayToSaturdayWeek, 
    sum(a.clockHours) AS clockHours, c.lastname, c.firstname
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
    AND c.distcode = 'BTEC'
  WHERE b.theDate BETWEEN '02/02/2014' AND '03/20/2014'
    AND a.clockhours <> 0
  GROUP BY sundayToSaturdayWeek, c.lastname, c.firstname) n on m.sundayToSaturdayWeek = n.sundaytoSaturdayWeek
GROUP BY m.fromDate, m.thruDate  
ORDER BY fromdate, name


-- feb 2014
SELECT '_Total', sum(clockhours) AS clockhours, COUNT(*)
FROM (
  SELECT sundayToSaturdayWeek, MIN(thedate) AS fromDate, MAX(theDate) AS thruDate
  FROM day
  WHERE thedate BETWEEN '02/01/2014' AND '02/28/2014'
  GROUP BY sundayToSaturdayWeek) m
LEFT JOIN (
  SELECT sundayToSaturdayWeek, 
    sum(a.clockHours) AS clockHours, c.lastname, c.firstname
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
    AND c.distcode = 'BTEC'
  WHERE b.theDate BETWEEN '02/01/2014' AND '02/28/2014'
    AND a.clockhours <> 0
  GROUP BY sundayToSaturdayWeek, c.lastname, c.firstname) n on m.sundayToSaturdayWeek = n.sundaytoSaturdayWeek
  
  
-- want clock hours/day
SELECT mmdd + ' ' + left(trim(dayname), 3), COUNT(*) AS census, 
  sum(a.clockHours) AS totalClockHours, round(SUM(clockHours)/COUNT(*), 2) AS hoursPerTech
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  AND c.distcode = 'BTEC'
WHERE b.theDate BETWEEN '02/01/2014' AND '03/19/2014'
  AND a.clockhours <> 0
  AND dayname <> 'Saturday'
GROUP BY mmdd + ' ' + left(trim(dayname), 3)
