SELECT distinct a.ro, roflaghours
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
WHERE c.servicetypecode = 'bs'
AND b.thedate > '11/30/2013'
AND a.ro = '18'


SELECT a.ro, a.line, roflaghours, flaghours, e.opcode, e.description, f.complaint, f.correction
FROM factrepairorder a
LEFT JOIN day b on a.finalclosedatekey = b.datekey
LEFT JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
LEFT JOIN dimopcode d on a.corcodekey = d.opcodekey
LEFT JOIN dimopcode e on a.opcodekey = e.opcodekey
LEFT JOIN dimccc f on a.ccckey = f.ccckey
WHERE c.servicetypecode = 'bs' AND a.ro = '18026556'
AND b.thedate > '11/30/2013'

-- INNER JOIN faster
SELECT a.ro, a.line, roflaghours, flaghours, e.opcode, e.description, f.complaint, f.correction
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
WHERE c.servicetypecode = 'bs' 
AND b.thedate > '11/30/2013'

-- opcodes & corcodes are rarely used
SELECT a.ro, a.line, roflaghours, flaghours, e.opcode, 
  left(cast(e.description AS sql_char),50) AS opCodeDesc, 
  left(cast(f.complaint AS sql_char), 50) AS complaint, 
  left(cast(f.correction AS sql_char), 50) AS correction,
  d.opcode AS corCode, 
  left(cast(d.description AS sql_char),50) AS corCodeDesc 
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
WHERE c.servicetypecode = 'bs' --AND a.ro = '18026556'
AND b.thedate between '11/30/2013' AND curdate()

-- only ros with > 8 flag hours
SELECT a.ro, a.line, roflaghours, flaghours, e.opcode, 
  left(cast(e.description AS sql_char),50) AS opCodeDesc, 
  left(cast(f.complaint AS sql_char), 50) AS complaint, 
  left(cast(f.correction AS sql_char), 50) AS correction,
  d.opcode AS corCode, 
  left(cast(d.description AS sql_char),50) AS corCodeDesc 
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
WHERE c.servicetypecode = 'bs' --AND a.ro = '18026556'
  AND b.thedate between '11/30/2013' AND curdate()
  AND a.roflaghours > 8
  
SELECT opcode, opcodedesc, COUNT(*)
FROM (
-- only ros with > 4 flag hours
SELECT a.ro, a.line, roflaghours, flaghours, e.opcode, 
  left(cast(e.description AS sql_char),50) AS opCodeDesc, 
  left(cast(f.complaint AS sql_char), 50) AS complaint, 
  left(cast(f.correction AS sql_char), 50) AS correction,
  d.opcode AS corCode, 
  left(cast(d.description AS sql_char),50) AS corCodeDesc 
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
WHERE c.servicetypecode = 'bs' --AND a.ro = '18026556'
  AND b.thedate between '09/30/2013' AND curdate()
  AND a.roflaghours > 4
) x GROUP BY opcode, opcodedesc   

-- mostly, what we are looking at are opcode: EST for metal, , 025 for metal, 024 for Paint  
-- what about FRAME, yep that too
SELECT a.ro, a.line, roflaghours, flaghours, e.opcode, 
  left(cast(e.description AS sql_char),50) AS opCodeDesc, 
  left(cast(f.complaint AS sql_char), 50) AS complaint, 
  left(cast(f.correction AS sql_char), 50) AS correction,
  d.opcode AS corCode, 
  left(cast(d.description AS sql_char),50) AS corCodeDesc 
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
WHERE e.opcode = 'frame'
  AND b.thedate between '11/30/2012' AND curdate()
  AND a.roflaghours > 8  
  
-- lets limit to those 4 opcodes , AND flaghours > 8
SELECT a.ro, a.line, roflaghours, flaghours, e.opcode, 
  left(cast(e.description AS sql_char),50) AS opCodeDesc, 
  left(cast(f.complaint AS sql_char), 50) AS complaint, 
  left(cast(f.correction AS sql_char), 50) AS correction,
  d.opcode AS corCode, 
  left(cast(d.description AS sql_char),50) AS corCodeDesc 
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
INNER JOIN dimopcode d on a.corcodekey = d.opcodekey
INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
WHERE c.servicetypecode = 'bs' AND a.ro = '18025802'
--  AND b.thedate between '11/30/2013' AND curdate()
  AND a.roflaghours > 8
  AND e.opcode IN ('EST','024','025','FRAME')
  
-- AND a "finalish" query
-- lets limit to those 4 opcodes , AND flaghours > 4
SELECT a.ro, b.thedate AS CloseDate, 
  SUM(CASE WHEN e.opcode = 'EST' OR e.opcode = 'FRAME' THEN flaghours END) AS MetalHours,
  SUM(CASE WHEN e.opcode = '024' THEN flaghours END) AS PaintHours,  
  roflaghours
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimservicetype c on a.servicetypekey = c.servicetypekey
INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
WHERE c.servicetypecode = 'bs' --AND a.ro = '18026556'
  AND b.thedate between '06/30/2013' AND curdate()
  AND a.roflaghours > 4
  AND e.opcode IN ('EST','024','025','FRAME')  
GROUP BY a.ro, b.thedate, a.roflaghours  

-- sent to cary 1/17
SELECT a.ro, b.thedate AS CloseDate, 
  coalesce(SUM(CASE WHEN e.opcode IN ('EST','FRAME','025') THEN a.flaghours END), 0) AS MetalHours,
  coalesce(SUM(CASE WHEN e.opcode = '024' THEN a.flaghours END), 0) AS PaintHours,  
  max(a.roflaghours) AS TotalFlagHours
FROM factRepairOrder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimOpcode e on a.opcodekey = e.opcodekey
WHERE a.roflaghours > 4
  AND b.thedate BETWEEN '06/30/2013' AND curdate()
  AND e.opcode IN ('EST','024','FRAME', '025')
  AND a.serviceTypeKey = (
    SELECT serviceTypeKey
	FROM dimServiceType
	WHERE serviceTypeCode = 'BS')
GROUP BY a.ro, b.thedate

-- 1/28/14
-- why are so many internal ros written up without an opcode
-- who IS writing internals AND USING N/A
-- obviously Jama IS doing the most
SELECT c.name, c.writernumber, 
  SUM(CASE WHEN e.opcode = 'N/A' THEN 1 ELSE 0 END) AS opNA, 
  SUM(CASE WHEN e.opcode <> 'N/A' THEN 1 ELSE 0 END) AS opReal,
  COUNT(*) AS howMany, MIN(ro), MAX(ro)
FROM factRepairOrder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
INNER JOIN dimOpcode e on a.opcodekey = e.opcodekey
WHERE b.thedate BETWEEN '01/01/2013' AND curdate()
  AND a.serviceTypeKey = (
    SELECT serviceTypeKey
	  FROM dimServiceType
	  WHERE serviceTypeCode = 'BS') 
  AND a.paymentTypeKey = (
    SELECT paymentTypeKey
    FROM dimPaymentType
    WHERE paymentTypeCode = 'I')    
GROUP BY c.name, c.writernumber

-- ALL Jama history
SELECT c.name, c.writernumber, e.opcode, COUNT(*), MIN(ro), MAX(ro), MIN(b.thedate), MAX(b.thedate)
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimServiceWriter c on a.servicewriterkey = c.servicewriterkey
INNER JOIN dimOpcode e on a.opcodekey = e.opcodekey
WHERE c.writernumber = '314' 
GROUP BY c.name, c.writernumber, e.opcode
  

    
-- 2/10: ok, randy IS ready to use opcodes
-- opcode use
select b.opcode, left(b.description, 50) AS description, COUNT(*)
FROM factrepairorder a
INNER JOIN dimopcode b on a.opcodekey = b.opcodekey
INNER JOIN day c on a.opendatekey = c.datekey
WHERE flaghours > 0
  AND c.yearmonth > 201212
  AND a.serviceTypeKey = (
    SELECT serviceTypeKey
    FROM dimServiceType
    WHERE serviceTypeCode = 'BS')
GROUP BY b.opcode, left(b.description, 50)   
ORDER BY COUNT(*) desc   

-- corcode use
select b.opcode, left(b.description, 50), COUNT(*)
FROM factrepairorder a
INNER JOIN dimopcode b on a.corcodekey = b.opcodekey
WHERE flaghours > 0
  AND a.serviceTypeKey = (
    SELECT serviceTypeKey
    FROM dimServiceType
    WHERE serviceTypeCode = 'BS')
GROUP BY b.opcode, left(b.description, 50)   
ORDER BY COUNT(*) desc   
    
SELECT *
FROM dimopcode  
WHERE (
  opcode LIKE '%MOP%'
  OR opcode LIKE '%PDR%'
  OR opcode LIKE '%C/A%')   
  
 DROP TABLE #wtf; 
SELECT opcode, LEFT(cast(description AS sql_char), 75) AS description
INTO #wtf
FROM dimopcode  
    
select *
FROM #wtf    
WHERE  description LIKE '%PDR%'

SELECT *
FROM dimopcode
WHERE (
  CAST(description AS sql_char) like '%nov%'
  OR CAST(description AS sql_char) like '%dent%'
  OR CAST(description AS sql_char) like '%Chip%'
  OR CAST(description AS sql_char) like '%mop%')
AND CAST(description AS sql_char) not like '%ident%'  
AND CAST(description AS sql_char) not like '%endent%' 
      
    