
SELECT c.thedate, a.ro, a.flaghours, 
  CASE LEFT(trim(complaint), 3) 
    WHEN 'PDR' THEN 'PDR'
    WHEN 'C/A' THEN 'Chips Away'
    WHEN 'NOV' THEN 'Novus'
    ELSE 
      CASE
        WHEN LEFT(correction, 3) = 'PDR' THEN 'PDR'
        WHEN correction LIKE '%NDSH%' THEN 'Windshield'
        ELSE 'Other'
      END 
  END, 
  d.opcode AS opcode, d.description AS opcodedesc, 
  e.opcode AS corcode, e.description AS corcodedesc, 
  f.complaint, f.correction
-- SELECT COUNT(*) 
FROM factrepairorder a
INNER JOIN dimtech b on a.techkey = b.techkey
INNER JOIN day c on a.finalclosedatekey = c.datekey
INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
INNER JOIN dimopcode e on a.corcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
WHERE b.technumber = '213'
  AND c.thedate BETWEEN '09/01/2013' AND curdate()
ORDER BY 
  CASE LEFT(complaint, 3) 
    WHEN 'PDR' THEN 'PDR'
    WHEN 'C/A' THEN 'Chips Away'
    WHEN 'NOV' THEN 'Novus'
    ELSE 
      CASE
        WHEN LEFT(correction, 3) = 'PDR' THEN 'PDR'
        WHEN correction LIKE '%NDSH%' THEN 'Windshield'
        ELSE 'Other'
      END 
  END
  

SELECT yearmonth, job, SUM(flaghours) AS flaghours, COUNT(*) AS howMany
FROM ( 
SELECT c.thedate, c.yearmonth, a.ro, a.flaghours, 
  CASE LEFT(complaint, 3) 
    WHEN 'PDR' THEN 'PDR'
    WHEN 'C/A' THEN 'Chips Away'
    WHEN 'NOV' THEN 'Novus'
    ELSE 
      CASE
        WHEN LEFT(correction, 3) = 'PDR' THEN 'PDR'
        WHEN correction LIKE '%NDSH%' THEN 'Windshield'
        ELSE 'Other'
      END 
  END AS job, 
  d.opcode AS opcode, d.description AS opcodedesc, 
  e.opcode AS corcode, e.description AS corcodedesc, 
  f.complaint, f.correction
-- SELECT COUNT(*) 
FROM factrepairorder a
INNER JOIN dimtech b on a.techkey = b.techkey
INNER JOIN day c on a.finalclosedatekey = c.datekey
INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
INNER JOIN dimopcode e on a.corcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
WHERE b.technumber = '213'
  AND c.thedate BETWEEN '09/01/2013' AND curdate()) x
GROUP BY yearmonth, job


-- who the fuck did pdr before tech 213
-- aha BS1: Quick Lane Team
SELECT thedate, a.ro, technumber, b.description
FROM factrepairorder a
INNER JOIN dimtech b on a.techkey = b.techkey
INNER JOIN day c on a.finalclosedatekey = c.datekey
INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
INNER JOIN dimopcode e on a.corcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
WHERE c.thedate BETWEEN '09/01/2013' AND curdate()
  AND LEFT(complaint, 3) = 'PDR'
  
  
-- pdr lines regardless of tech since 01/2013
SELECT c.thedate, a.ro, a.line, a.flaghours, technumber, 
  CASE LEFT(complaint, 3) 
    WHEN 'PDR' THEN 'PDR'
    WHEN 'C/A' THEN 'Chips Away'
    WHEN 'NOV' THEN 'Novus'
    ELSE 
      CASE
        WHEN LEFT(correction, 3) = 'PDR' THEN 'PDR'
        WHEN correction LIKE '%NDSH%' THEN 'Windshield'
        ELSE 'Other'
      END 
  END, 
  d.opcode AS opcode, d.description AS opcodedesc, 
  e.opcode AS corcode, e.description AS corcodedesc, 
  f.complaint, f.correction
-- SELECT COUNT(*) 
FROM factrepairorder a
INNER JOIN dimtech b on a.techkey = b.techkey
INNER JOIN day c on a.finalclosedatekey = c.datekey
INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
INNER JOIN dimopcode e on a.corcodekey = e.opcodekey
INNER JOIN dimccc f on a.ccckey = f.ccckey
--WHERE b.technumber = '213'
WHERE c.thedate BETWEEN '01/01/2013' AND curdate()
  AND (
    LEFT(complaint, 3) = 'PDR'
    OR
    LEFT(correction, 3) = 'PDR')

DROP TABLE #wtf;
SELECT m.*, n.ptlamt
INTO #wtf
FROM (  
  SELECT c.thedate, monthname, monthofyear, a.ro, a.line, a.flaghours, technumber
  -- SELECT COUNT(*) 
  FROM factrepairorder a
  INNER JOIN dimtech b on a.techkey = b.techkey
  INNER JOIN day c on a.finalclosedatekey = c.datekey
  INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
  INNER JOIN dimopcode e on a.corcodekey = e.opcodekey
  INNER JOIN dimccc f on a.ccckey = f.ccckey
  --WHERE b.technumber = '213'
  WHERE c.theyear = 2013
    AND flaghours <> 0
    AND (
      LEFT(complaint, 3) = 'PDR'
      OR
      LEFT(correction, 3) = 'PDR')) m    
LEFT JOIN stgArkonaSDPRDET n on m.ro = n.ptro# AND m.line = n.ptline AND m.technumber = n.pttech
WHERE n.ptltyp = 'L'
  AND n.ptcode = 'TT'
  AND n.ptdate > '01/01/2013';     

SELECT monthname AS month, SUM(flaghours) AS flaghours, SUM(ptlamt) AS laborsales,
  SUM(ptlamt)/SUM(flaghours) AS "$$/Hr"
FROM #wtf a
GROUP BY monthname, monthofyear  
ORDER BY monthofyear
   
