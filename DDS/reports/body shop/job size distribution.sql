-- DROP TABLE #wtf;
SELECT b.thedate, b.yearmonth, a.ro, a.flaghours,
  CASE 
    WHEN flaghours < 6 THEN 'small'
    WHEN flaghours BETWEEN 6 AND 20 THEN 'medium'
    WHEN flaghours > 20 THEN 'large'
  END AS jobSize
INTO #wtf  
FROM factRepairOrder a
INNER JOIN day b on a.finalCloseDateKey = b.datekey
WHERE b.yearmonth > 201212
  AND a.flaghours > 0
  AND a.serviceTypeKey = (
    SELECT serviceTypeKey
    FROM dimServiceType
    WHERE serviceTypeCode = 'bs')
GROUP BY b.thedate, b.yearmonth, a.ro, a.flaghours

SELECT *
FROM #wtf

SELECT yearmonth, count(*) as totalJobs, round(SUM(flagHours), 0) AS totalFlagHours,
  SUM(CASE WHEN jobSize = 'small' THEN 1 ELSE 0 END) AS smallJobs,
  round(SUM(CASE WHEN jobSize = 'small' THEN flagHours ELSE 0 END), 0) AS smallFlagHours,
  SUM(CASE WHEN jobSize = 'medium' THEN 1 ELSE 0 END) AS mediumJobs,
  round(SUM(CASE WHEN jobSize = 'medium' THEN flagHours ELSE 0 END), 0) AS mediumFlagHours,
  SUM(CASE WHEN jobSize = 'large' THEN 1 ELSE 0 END) AS largeJobs,
  round(SUM(CASE WHEN jobSize = 'large' THEN flagHours ELSE 0 END), 0) AS largeFlagHours
FROM #wtf
GROUP BY yearmonth  

-- same thing, no temp table
SELECT yearmonth, count(*) as totalJobs, round(SUM(flagHours), 0) AS totalFlagHours,
  SUM(CASE WHEN jobSize = 'small' THEN 1 ELSE 0 END) AS smallJobs,
  round(SUM(CASE WHEN jobSize = 'small' THEN flagHours ELSE 0 END), 0) AS smallFlagHours,
  SUM(CASE WHEN jobSize = 'medium' THEN 1 ELSE 0 END) AS mediumJobs,
  round(SUM(CASE WHEN jobSize = 'medium' THEN flagHours ELSE 0 END), 0) AS mediumFlagHours,
  SUM(CASE WHEN jobSize = 'large' THEN 1 ELSE 0 END) AS largeJobs,
  round(SUM(CASE WHEN jobSize = 'large' THEN flagHours ELSE 0 END), 0) AS largeFlagHours
FROM (  
  SELECT b.thedate, b.yearmonth, a.ro, a.flaghours,
    CASE 
      WHEN flaghours < 6 THEN 'small'
      WHEN flaghours BETWEEN 6 AND 20 THEN 'medium'
      WHEN flaghours > 20 THEN 'large'
    END AS jobSize
  FROM factRepairOrder a
  INNER JOIN day b on a.finalCloseDateKey = b.datekey
  WHERE b.yearmonth > 201212
    AND a.flaghours > 0
    AND a.serviceTypeKey = (
      SELECT serviceTypeKey
      FROM dimServiceType
      WHERE serviceTypeCode = 'bs')
  GROUP BY b.thedate, b.yearmonth, a.ro, a.flaghours) x
GROUP BY yearmonth