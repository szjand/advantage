﻿select *
from ads.bs_ro_details
where ro in ('18038674','18038864')


  select a.ro, a.open_date, a.close_date, a.flag_hours, round(a.ro_labor_sales, 0) as labor_sales, a.labor_cogs, a.labor_gross, 
    b.sales as parts_sales, b.cogs as parts_cogs, b.gross as parts_gross, 
    round(a.ro_shop_supplies, 0) as shop_supplies, round(ro_hazardous_materials, 0) as hazardous_materials, 
    round(ro_paint_materials, 0) as paint_materials,
    a.labor_gross + b.gross as total_gross, payment_type,
    round(a.ro_labor_sales + b.sales) as total_sales,
    multiple_ros, a.vin
  from ( -- ro level
    select min(a.ro) || '*' as ro, 
      min(open_date) as open_date, max(close_date) as close_date, sum(flag_hours) as flag_hours, 
      ro_labor_sales,
      round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
      round(ro_labor_sales - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
      ro_shop_supplies as ro_shop_supplies, 
      ro_hazardous_materials as ro_hazardous_materials, 
      ro_paint_materials, 
      string_agg(distinct payment_type, ',') as payment_type,
      string_agg(distinct a.ro, '|') as multiple_ros, a.vin   
    from ads.bs_ro_details a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
      and aa.ro in ('18038674','18038684')
    group by a.vin, ro_labor_sales, ro_shop_supplies, ro_hazardous_materials, ro_paint_materials) a
  left join ( -- parts
    select aa.vin, sum(a.sales) as sales, sum(a.cogs) as cogs, sum(a.gross) as gross
    from ads.bs_ro_parts a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    group by aa.vin) b on a.vin = b.vin;  


    select a.ro, 
      open_date, close_date, 
      flag_hours,
      tech_labor_cost,
      flag_hours * tech_labor_cost as labor_cogs,
      ro_labor_sales,
      ro_shop_supplies, 
      ro_hazardous_materials, 
      ro_paint_materials, 
      payment_type,
      a.vin   
    from ads.bs_ro_details a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
      and aa.ro in ('18043584','18043585', '18043704', '18043715') 

  select a.ro, a.open_date, a.close_date, a.flag_hours, round(a.ro_labor_sales, 0) as labor_sales, 
    round(a.labor_cogs, 0) as labor_cogs, round(a.labor_gross, 0) as labor_gross,
    b.sales as parts_sales, b.cogs as parts_cogs, b.gross as parts_gross, 
    round(a.ro_shop_supplies, 0) as shop_supplies, 
    round(ro_hazardous_materials, 0) as hazardous_materials, 
    round(ro_paint_materials, 0) as paint_materials,
    a.labor_gross + b.gross as total_gross, payment_type,
    round(a.ro_labor_sales + b.sales) as total_sales,
    multiple_ros, a.vin
  from (  
    select min(ro) || '*' as ro, min(open_date) as open_date, max(close_date) as close_date,
      sum(flag_hours) as flag_hours, 
      sum(ro_labor_sales) as ro_labor_sales, 
      round(sum(labor_cogs), 2) as labor_cogs,
      round(sum(ro_labor_sales + labor_cogs), 2) as labor_gross,
      sum(ro_shop_supplies) as ro_shop_supplies,
      sum(ro_hazardous_materials) as ro_hazardous_materials, 
      sum(ro_paint_materials) as ro_paint_materials, 
      payment_type, 
      string_agg(distinct ro, '|') as multiple_ros,
      vin
    from ( -- the individual ros, 1 row per ro
      select a.ro, 
        open_date, close_date, 
        sum(flag_hours) as flag_hours,
        sum(tech_labor_cost * flag_hours) as labor_cogs,
        ro_labor_sales,
        ro_shop_supplies, 
        ro_hazardous_materials, 
        ro_paint_materials, 
        string_agg(distinct payment_type, ',') as payment_type,
        a.vin   
      from ads.bs_ro_details a
      inner join ads.bs_multiple_ros aa on a.ro = aa.ro
        and aa.ro in ('18043584','18043585', '18043704', '18043715') 
      group by a.ro, open_date, close_date, ro_labor_sales, ro_shop_supplies,
        ro_hazardous_materials, ro_paint_materials, a.vin) bb
    group by payment_type, vin) a
  left join ( -- parts
    select aa.vin, sum(a.sales) as sales, sum(a.cogs) as cogs, sum(a.gross) as gross
    from ads.bs_ro_parts a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    group by aa.vin) b on a.vin = b.vin;  

      