﻿select *
from fin.dim_account
where account in ('124703')


select *
from fin.dim_account
where (
  (description like any(array['%SALAR%','%SLRY%','%COMP%']))
  or
  (account like '1247%'))
  and store_code = 'RY1'
order by account  

select a.*, b.the_date, c.account, c.description, d.*
from fin.fact_gl a 
inner join dds.dim_date b on a.date_key = b.date_key
  and b.year_month = '201701'
inner join (
  select *
  from fin.dim_account
  where (
    (description like any(array['%SALAR%','%SLRY%','%COMPEN%']))
    or
    (account like '1247%'))
    and store_code = 'RY1') c  on a.account_key = c.account_key
inner join fin.dim_journal d on a.journal_key = d.journal_key  
  and d.journal_code in ('PAY','GLI')
limit 100

-- select * from fin.dim_Account where account like '125%'

  select a.*, c.account, c.description, d.*
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = '201701'
  inner join (
    select * 
    from fin.dim_Account
    where account like '125%') c  on a.account_key = c.account_key
  inner join fin.dim_journal d on a.journal_key = d.journal_key  
    and d.journal_code in ('PAY','GLI')
  group by a.control
  

select d.*, e.tax, round(e.tax/d.comp, 2) as tax_perc, f.name
from (
  select a.control, sum(a.amount) as comp--, b.the_date, c.account, c.description, d.*
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = '201701'
  inner join (
    select *
    from fin.dim_account
    where (
      (description like any(array['%SALAR%','%SLRY%','%COMPEN%']))
      or
      (account like '1247%'))
      and store_code = 'RY1') c  on a.account_key = c.account_key
  inner join fin.dim_journal d on a.journal_key = d.journal_key  
    and d.journal_code in ('PAY','GLI')
  group by a.control) d
left join (
  select a.control, sum(a.amount) as tax--, b.the_date, c.account, c.description, d.*
  from fin.fact_gl a 
  inner join dds.dim_date b on a.date_key = b.date_key
    and b.year_month = '201701'
  inner join (
    select * 
    from fin.dim_Account
    where account like '125%') c  on a.account_key = c.account_key
  inner join fin.dim_journal d on a.journal_key = d.journal_key  
    and d.journal_code in ('PAY','GLI')
  group by a.control) e on d.control = e.control
left join ads.ext_dds_edwemployeedim f on e.control = f.employeenumber
  and f.currentrow = true
