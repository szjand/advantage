﻿I’m looking for a report that lists parts sales and gross on repair orders for the following service types by month from Oct 2015 through Sept 2016:

MN Customer Pay
EM  Customer Pay
SV Customer Pay
MR Customer Pay

MN Internal
MR Internal

MR Warranty
MR Service Contract

I’m also looking for the following info by technician for Oct 2015 to Sept 2016 by month:

Tech Name
Hourly or Commission
Tech Cost/Flagged Hour as set up in DT
Tech Pay Rate/Flagged Hour
PTO Pay per Flagged Hour
Average Clock Hours/Day
Proficiency

Can you help?




create table ads.repair_order(
  store_code citext,
  the_date date,
  ro citext,
  line integer,
  service_type citext, 
  payment_type citext);
create index on ads.repair_order(ro);  
create index on ads.repair_order(line);  
create index on ads.repair_order(service_type);  
create index on ads.repair_order(payment_type); 

delete from ads.repair_order

select *
from dds.ext_pdptdet
where ptdate > 20161000
limit 500  
create index on dds.ext_pdptdet (ptinv_);
create index on dds.ext_pdptdet(ptline);

select a.*, b.ptqty, b.ptqty * b.ptcost as cogs, b.ptqty * b.ptnet as sales, b.ptqty * ptnet - b.ptqty * ptcost as gross
from ads.repair_order a
left join dds.ext_pdptdet b on a.ro = b.ptinv_
  and a.line = b.ptline
where a.ro = '16194613'  
  and b.ptinv_ is not null 
order by line  

select * from ads.repair_order where ro = '16194613' order by line
  
select line, sum(ptcost), sum(ptlist), sum(ptnet)
from ads.repair_order a
left join dds.ext_pdptdet b on a.ro = b.ptinv_
  and a.line = b.ptline
where a.ro = '16194613'  
  and b.ptinv_ is not null 
group by line  
limit 100  


select a.*, b.account, b.account_type, b.department, b.description
from fin.fact_gl a
inner join fin.dim_Account b on a.account_key = b.account_key
  and b.department_code = 'PD'
where a.control = '16194613'




select c.yearmonth, a.service_type, a.payment_type, 
  round(sum(b.ptqty * b.ptnet), 0) as parts_sales,
  round(sum(b.ptqty * b.ptcost), 0) as parts_cogs,  
  round(sum(b.ptqty * ptnet - b.ptqty * ptcost), 0) as parts_gross,
  count(distinct a.ro) as ro_count
from ads.repair_order a
left join dds.ext_pdptdet b on a.ro = b.ptinv_
  and a.line = b.ptline
left join dds.day c on a.the_date = c.thedate  
where b.ptinv_ is not null 
  and (
    (service_type = 'MN' and payment_type = 'customer pay')
    or
    (service_type = 'EM' and payment_type = 'customer pay')
    or
    (service_type = 'SV' and payment_type = 'customer pay')
    or
    (service_type = 'MR' and payment_type = 'customer pay')
    or
    (service_type = 'MN' and payment_type = 'internal')
    or
    (service_type = 'MR' and payment_type = 'internal')
    or
    (service_type = 'MR' and payment_type = 'warranty')
    or
    (service_type = 'MR' and payment_type = 'service contract'))                                
group by c.yearmonth, a.service_type, a.payment_type
-- order by a.payment_type, service_type
) x group by yearmonth
--