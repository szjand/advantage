1. Acct 146002:  8/1/14 � 8/31/14; Op codes used with counts for each
2. Acct 146203:  8/1/14 � 8/31/14; number of debits vs. number of credits; op codes used with counts for each

146002: SLS PDQ LBR CAR/LD TK: should be just non oil change labor ops
146203: SLS-WARR LBR PDQ


--< 1. ------------------------------------------------------------------------<
-- 1917647, pdq labor: some to 146002, some to 146032
-- joining on gttamt = laborsales limits the opcodes to acct 146002
SELECT a.gtctl#, a.gtjrnl, a.gtacct, a.gttamt, b.line, b.flaghours, 
  b.laborsales, c.*
-- SELECT * 
FROM stgArkonaGLPTRNS a
LEFT JOIN factRepairORder b on a.gtctl# = b.ro
LEFT JOIN dimOpcode c on b.opcodekey = c.opcodekey
WHERE a.gtacct = '146002'
  AND a.gtdate BETWEEN '08/01/2014' AND '08/31/2014'
--  AND ABS(a.gttamt) = ABS(b.laborsales)
ORDER BY gtctl#  

SELECT opcode, COUNT(*)
FROM (
  SELECT a.gtctl#, a.gtjrnl, a.gtacct, a.gttamt, b.line, b.flaghours, 
    b.laborsales, c.*
  -- SELECT * 
  FROM stgArkonaGLPTRNS a
  LEFT JOIN factRepairORder b on a.gtctl# = b.ro
  LEFT JOIN dimOpcode c on b.opcodekey = c.opcodekey
  WHERE a.gtacct = '146002'
    AND a.gtdate BETWEEN '08/01/2014' AND '08/31/2014') e
--    AND ABS(a.gttamt) = ABS(b.laborsales)) e
GROUP BY opcode  

SELECT COUNT(DISTINCT b.ro)
FROM stgArkonaGLPTRNS a
LEFT JOIN factRepairORder b on a.gtctl# = b.ro
LEFT JOIN dimOpcode c on b.opcodekey = c.opcodekey
WHERE a.gtacct = '146002'
  AND a.gtdate BETWEEN '08/01/2014' AND '08/31/2014'
  AND c.opcode = 'ROT'

  

--/> 1. -----------------------------------------------------------------------/>

--< 2. ------------------------------------------------------------------------<
-- ok, this IS good the ~400 to 302 difference
SELECT gtacct,
  SUM(CASE WHEN gttamt > 0 THEN 1 ELSE 0 END) AS posEntries,
  SUM(CASE WHEN gttamt < 0 THEN 1 ELSE 0 END) AS negEntries
FROM stgArkonaGLPTRNS
WHERE gtacct = '146203'
  AND gtdate BETWEEN '08/01/2014' AND '08/31/2014'  
GROUP BY gtacct  

-- multiple opcode that SUM to the accting amount
-- eg, RO:.01 + PDQDLE1:49.72 = 49.73
-- limit to laborsales <> 0, can't match acct amt with labor sale amt
SELECT a.gtctl#, a.gtjrnl, a.gtacct, a.gttamt, b.line, b.flaghours, 
  b.laborsales, c.*
-- SELECT * 
FROM stgArkonaGLPTRNS a
LEFT JOIN factRepairORder b on a.gtctl# = b.ro
  AND b.serviceTypeKey = (
    SELECT serviceTypeKey
    FROM dimServiceType
    WHERE serviceTypeCode = 'QL')
LEFT JOIN dimOpcode c on b.opcodekey = c.opcodekey
WHERE a.gtacct = '146203'
  AND a.gtdate BETWEEN '08/01/2014' AND '08/31/2014'
  AND laborsales <> 0

SELECT opcode, COUNT(*)
FROM (
  SELECT a.gtctl#, a.gtjrnl, a.gtacct, a.gttamt, b.line, b.flaghours, 
    b.laborsales, c.*
  -- SELECT * 
  FROM stgArkonaGLPTRNS a
  LEFT JOIN factRepairORder b on a.gtctl# = b.ro
    AND b.serviceTypeKey = (
      SELECT serviceTypeKey
      FROM dimServiceType
      WHERE serviceTypeCode = 'QL')
  LEFT JOIN dimOpcode c on b.opcodekey = c.opcodekey
  WHERE a.gtacct = '146203'
    AND a.gtdate BETWEEN '08/01/2014' AND '08/31/2014'
    AND laborsales <> 0) e
group BY opcode  
ORDER BY COUNT(*) DESC   
--/> 2. -----------------------------------------------------------------------/>






SELECT opcode, COUNT(*)
FROM (
SELECT a.gtctl#, a.gtjrnl, a.gtacct, a.gttamt, b.line, b.flaghours, 
  b.laborsales, c.*
-- SELECT * 
FROM stgArkonaGLPTRNS a
LEFT JOIN factRepairORder b on a.gtctl# = b.ro
LEFT JOIN dimOpcode c on b.opcodekey = c.opcodekey
WHERE a.gtacct = '146002'
  AND a.gtdate BETWEEN '08/01/2014' AND '08/31/2014') x
GROUP BY opcode