
Tech Name
Hourly or Commission
Tech Cost/Flagged Hour as set up in DT
Tech Pay Rate/Flagged Hour
PTO Pay per Flagged Hour
Average Clock Hours/Day
Proficiency

Can you help?

Jeri Schmiess Penas, CPA

-- response to
I am not sure on the tech part
Main shop only?
GM Store only?
What exactly you mean by Tech Pay Rate/Flagged Hour
Define what you mean by proficiency in this case

For tech part:

Main shop only for GM store
Pay Rate/Flagged hour is the tech costing rate.  I can look this up too, but I thought if it was easy to add it would save a bunch of time
Proficiency = flagged hours/clock hours

--QUESTIONS
-- so what's the difference betweeen 
Tech Cost/Flagged Hour as set up in DT
AND 
Tech Pay Rate/Flagged Hour

PTO Pay per Flagged Hour: include holiday pay

-- what about techs with multiple laborcosts per month

Average Clock Hours/Day: calendar days, WORK days, days worked?

--QUESTIONS
select *
FROM tpdata


SELECT b.thedate, a.ro, a.line, a.flaghours, c.*
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.closedatekey = b.datekey
INNER JOIN dds.dimtech c on a.techkey = c.techkey
WHERE b.thedate BETWEEN '10/01/2015' AND '09/30/2016'
  AND a.storecode = 'ry1'
  AND a.flaghours <> 0
  AND c.employeenumber <> 'NA'

-- flaghours/techkey  
SELECT a.techkey, round(SUM(flaghours), 0)
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.closedatekey = b.datekey
INNER JOIN dds.dimtech c on a.techkey = c.techkey
WHERE b.thedate BETWEEN '10/01/2015' AND '09/30/2016'
  AND a.storecode = 'ry1'
  AND a.flaghours <> 0
  AND c.employeenumber <> 'NA'
GROUP BY a.techkey   
 
-- DROP TABLE #techs_1; 
select b.yearmonth, a.techkey, a.employeenumber, a.name, a.technumber, a.laborcost,
  a.techkeyfromdate, a.techkeythrudate, b.flag_hours,
  CASE
    WHEN c.techkey IS NOT NULL THEN True
	ELSE False
  END AS team_pay
INTO #techs_1  
FROM dds.dimtech a
LEFT JOIN (
  SELECT b.yearmonth, a.techkey, round(SUM(flaghours), 0) AS flag_hours
  FROM dds.factrepairorder a
  INNER JOIN dds.day b on a.closedatekey = b.datekey
  INNER JOIN dds.dimtech c on a.techkey = c.techkey
  WHERE b.thedate BETWEEN '10/01/2015' AND '09/30/2016'
    AND a.storecode = 'ry1'
    AND a.flaghours <> 0
    AND c.employeenumber <> 'NA'
  GROUP BY b.yearmonth, a.techkey ) b on a.techkey = b.techkey
LEFT JOIN ( -- team pay techs
  SELECT *
  FROM tptechs a
  LEFT JOIN tpteamtechs b on a.techkey = b.techkey
    AND b.fromdate < '09/30/2016'
    AND b.thrudate > '10/01/2015'
  LEFT JOIN tpteams c on b.teamkey = c.teamkey
  WHERE a.departmentkey = 18
    AND b.techkey IS NOT NULL) c on a.employeenumber = c.employeenumber 
WHERE a.techkeyfromdate < '09/30/2016' 
  AND a.techkeythrudate >  '10/01/2015'
  AND a.employeenumber <> 'NA'
  AND a.storecode = 'ry1'
  AND a.flagdept = 'service'
  AND b.flag_hours > 0
ORDER BY a.technumber, a.techkeyfromdate, b.yearmonth  

SELECT yearmonth, technumber
FROM #techs_1
GROUP BY yearmonth, technumber
HAVING COUNT(*) > 1

SELECT *
FROM #techs_1
ORDER BY technumber, yearmonth

select *
FROM dds.dimtech
ORDER BY description


SELECT *
FROM dds.factrepairorder
WHERE techkey IN (333,334)

-- DROP TABLE #clock_hours;
SELECT b.yearmonth, aa.employeenumber, round(SUM(clockhours), 0) AS clock_hours, 
  SUM(vacationhours + ptohours) AS pto_hours, SUM(holidayhours) AS holiday_hours
INTO #clock_hours  
FROM dds.edwClockHoursFact a
INNER JOIN dds.edwEmployeeDim aa on a.employeekey = aa.employeekey
INNER JOIN dds.day b on a.datekey = b.datekey
  AND b.thedate BETWEEN '10/01/2015' AND '09/30/2016'
GROUP BY b.yearmonth, aa.employeenumber
HAVING SUM(clockhours + vacationhours + ptohours + holidayhours) > 0


SELECT *
FROM #techs_1 a
LEFT JOIN #clock_hours b on a.employeenumber = b.employeenumber
  AND a.yearmonth = b.yearmonth
  
SELECT yearmonth, employeenumber, COUNT(*)
FROM #techs_1
GROUP BY yearmonth, employeenumber
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC 

SELECT * FROM #techs_1 WHERE employeenumber = '18005' ORDER BY yearmonth, techkeyfromdate

select * FROM dds.dimtech

start over
1 row per tech/yearmonth/laborcost
-- don't care about individual ros OR line, doesn't matter, just flaghours/day
-- 1 row date/ro/line/tech
DROP TABLE z_techs_1;
CREATE TABLE z_techs_1 (
  the_date date constraint NOT NULL,
  tech_key integer constraint NOT NULL,
  flag_hours numeric (8,2) constraint NOT NULL,
  constraint pk primary key (the_date, tech_key)) IN database;
INSERT INTO z_techs_1
select a.thedate, coalesce(b.techkey, -1), coalesce(sum(b.flaghours), 0) AS flaghours
FROM dds.day a
left join dds.factrepairorder b on a.datekey = b.flagdatekey
  AND b.storecode = 'ry1'
WHERE a.thedate BETWEEN '10/01/2015' AND '09/30/2016'
--  AND b.flaghours <> 0
GROUP BY a.thedate, b.techkey;



-- narrow it down to ry1 main shop
-- ADD employee_number, tech_number, labor_cost
DROP TABLE z_techs_2;
CREATE TABLE z_techs_2 (
  the_date date constraint NOT NULL,
  tech_key integer constraint NOT NULL,
  flag_hours numeric (8,2) constraint NOT NULL,
  employee_number cichar(7) constraint NOT NULL,
  tech_number cichar(3) constraint NOT NULL,
  labor_cost numeric(6,2) constraint NOT NULL,
  constraint pk primary key (the_date, tech_key)) IN database;
-- ADD technumber, laborcost FROM arkons
INSERT INTO z_techs_2
SELECT a.*, coalesce(b.employeenumber, 'none'), coalesce(b.technumber, 'non'), coalesce(b.laborcost, 0)
-- SELECT COUNT(*)
FROM z_techs_1 a
LEFT JOIN dds.dimtech b on a.tech_key = b.techkey
  AND b.employeenumber <> 'NA'
  AND b.storecode = 'ry1'
  AND b.flagdept = 'service';
  
select * FROM z_techs_1 WHERE tech_key = 590 AND the_date BETWEEN '09/01/2016' AND '09/30/2016'

/* 
-- what about techs with multiple laborcosts per month

SELECT yearmonth, technumber, COUNT(*)
FROM (
  SELECT b.yearmonth, a.technumber, a.laborcost
  FROM #techs_2 a
  INNER JOIN dds.day b on a.thedate = b.thedate  
  GROUP BY b.yearmonth, a.technumber, a.laborcost) x
GROUP BY yearmonth, technumber
HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC 
*/
/*

-- wait a fucking minute
-- how can a tech have multiple laborcosts on a single day!?!?!
-- OR multiple techkeys on a single day
-- i fucked something up IN the base queries
-- think the issue IS USING finalclosedate rather than flag date
-- yep, ro 16224477
solution is to  use flagdate
select * 
FROM #techs_2
WHERE thedate BETWEEN '02/01/2016' AND '02/28/2016'
  AND technumber = '595'
ORDER BY thedate  

*/
-- ADD year_month, techname, hourlyrate, payrollclass, clockhours
DROP TABLE z_techs_3;
CREATE TABLE z_techs_3 (
  year_month integer constraint NOT NULL,
  the_date date constraint NOT NULL,
  tech_key integer constraint NOT NULL,
  flag_hours numeric (8,2) constraint NOT NULL,
  employee_number cichar(7) constraint NOT NULL,
  tech_number cichar(3) constraint NOT NULL,
  labor_cost numeric(6,2) constraint NOT NULL,
  first_name cichar(25) constraint NOT NULL,
  last_name cichar(25) constraint NOT NULL,
  hourly_rate numeric(8,2) constraint NOT NULL,
  payroll_class cichar(12) constraint NOT NULL,
  clock_hours integer constraint NOT NULL ,
  pto_hours integer constraint NOT NULL,
  holiday_hours integer constraint NOT NULL,
  constraint pk primary key (the_date, tech_key)) IN database;
INSERT INTO z_techs_3  
SELECT c.yearmonth, a.*, coalesce(b.firstname, 'none'), 
  coalesce(b.lastname, 'none'), coalesce(b.hourlyrate, 0), 
  coalesce(b.payrollclass, 'none'),
  coalesce(round(SUM(d.clockhours), 0), 0) AS clock_hours, 
  coalesce(SUM(d.vacationhours + d.ptohours), 0) AS pto_hours, 
  coalesce(SUM(d.holidayhours), 0) AS holiday_hours
FROM z_techs_2 a 
LEFT JOIN dds.edwEmployeeDim b on a.employee_number = b.employeenumber
  AND a.the_date BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
LEFT JOIN dds.day c on a.the_date = c.thedate
LEFT JOIN dds.edwClockHoursFact d on c.datekey = d.datekey
  AND b.employeekey = d.employeekey
GROUP BY c.yearmonth, a.the_Date, a.tech_key, a.flag_hours, a.employee_number, a.tech_number, 
  a.labor_cost, b.firstname, b.lastname, b.hourlyrate, b.payrollclass;  

SELECT * from z_techs_3 WHERE last_name = 'bear' and year_month = 201609

get the rates AND proficiency FROM tp for commission techs

SELECT a.*, c.techteampercentage, round(c.previoushourlyrate, 2) AS pto_rate, 
  e.budget, 
  f.payperiodstart, f.payperiodend, f.techTFRRate, f.teamprofpptd
FROM z_techs_3 a
LEFT JOIN tptechs b on a.employee_number = b.employeenumber
LEFT JOIN tptechvalues c on b.techkey = c.techkey
  AND a.the_date BETWEEN c.fromdate AND c.thrudate
LEFT JOIN tpteamtechs d on b.techkey = d.techkey
  AND a.the_Date BETWEEN d.fromdate AND d.thrudate    
LEFT JOIN tpteamvalues e on d.teamkey = e.teamkey
  AND a.the_date BETWEEN e.fromdate AND e.thrudate  
LEFT JOIN tpdata f on a.the_date BETWEEN f.payperiodstart AND f.payperiodend
  AND f.dayofpayperiod = 14
  AND b.techkey = f.techkey 
WHERE tech_number = '587'
ORDER BY the_date  

SELECT * FROM z_techs_3 WHERE year_month = 201609 AND last_name = 'bear'
  
SELECT a.*, c.techteampercentage, round(c.previoushourlyrate, 2) AS pto_rate, 
  e.budget, 
  f.payperiodstart, f.payperiodend, f.techTFRRate, f.teamprofpptd
-- DROP TABLE #techs_4; 
SELECT year_month, first_name, last_name, tech_number, payroll_class,
  MAX(hourly_rate) AS hourly_rate, MAX(round(c.previoushourlyrate, 2)) AS pto_rate, 
  SUM(clock_hours) AS clock_hours, SUM(pto_hours) AS pto_hours, 
  SUM(holiday_hours) AS holiday_hours, round(SUM(flag_hours), 0) AS flag_hours,
  CASE SUM(clock_hours)
    WHEN 0 THEN 0
    else 100 * round(SUM(flag_hours)/SUM(clock_hours), 2) 
  end AS prof
INTO #techs_4  
FROM z_techs_3 a
LEFT JOIN tptechs b on a.employee_number = b.employeenumber
LEFT JOIN tptechvalues c on b.techkey = c.techkey
  AND a.the_date BETWEEN c.fromdate AND c.thrudate
LEFT JOIN tpteamtechs d on b.techkey = d.techkey
  AND a.the_Date BETWEEN d.fromdate AND d.thrudate    
LEFT JOIN tpteamvalues e on d.teamkey = e.teamkey
  AND a.the_date BETWEEN e.fromdate AND e.thrudate  
LEFT JOIN tpdata f on a.the_date BETWEEN f.payperiodstart AND f.payperiodend
  AND f.dayofpayperiod = 14
  AND b.techkey = f.techkey 
-- WHERE tech_number = '587'
GROUP BY year_month, first_name, last_name, tech_number, payroll_class  

select *
FROM #techs_4
ORDER BY year_month, last_name

-- wtf bear only has 77 clock hours?
SELECT * FROM z_techs_3 WHERE year_month = 201609 AND last_name = 'bear'
no rows for 9/3 - 9/12
old problem of base needs to be 1 row per day, NOT 1 row per day with flag hous

SELECT SUM(clockhours)
SELECT b.thedate, a.*
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
  AND b.yearmonth = 201609
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
  AND c.lastname = 'bear'
  
--------------------------------------------------------------------------------  
-- 10/17 so fucking start over again -------------------------------------------
--------------------------------------------------------------------------------
-- 1 row for each day
-- include workday designator
DROP TABLE #days;
SELECT yearmonth,thedate,datekey,
  CASE 
    WHEN weekday = true AND holiday = false THEN true
	ELSE false
  END AS workday
INTO #days
-- SELECT COUNT(*) -- 366
FROM dds.day
WHERE yearmonth BETWEEN 201510 AND 201609;

-- 1 row for each techkey on an ro for the time period
DROP TABLE #techs;
SELECT techkey, technumber, employeenumber, techkeyfromdate, techkeythrudate, 
  cast(laborcost AS sql_numeric(6,2)) AS labor_cost
INTO #techs
-- SELECT COUNT(*) -- 145
FROM dds.dimtech a
WHERE employeenumber <> 'NA'
  AND storecode = 'ry1'
  AND flagdept = 'service'
  AND EXISTS (
    SELECT 1
	FROM dds.factrepairorder m
	INNER JOIN dds.day n on m.flagdatekey = n.datekey
	  AND n.yearmonth BETWEEN 201510 AND 201609
	WHERE storecode = 'ry1'
	  AND techkey = a.techkey);

-- 1 row for each day/techkey for which that techkey was active
-- ADD edwEmployeeDim stuff: employeekey, payrollclass, hourlyrate, name
-- AND tp stuff: previoushourlyrate
DROP TABLE #tech_days;
SELECT a.*, b.*, c.lastname, c.firstname, c.employeekey, c.payrollclass, 
  CASE c.payrollclass
    WHEN 'Hourly' THEN cast(c.hourlyrate as sql_numeric (6,2))
    ELSE round(coalesce(e.previoushourlyrate, 0), 2) 
  end AS pto_rate
INTO #tech_days
-- SELECT COUNT(*) -- 13120
FROM #days a
full OUTER JOIN #techs b on a.thedate BETWEEN b.techkeyfromdate AND b.techkeythrudate
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  and a.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
LEFT JOIN tptechs d on b.employeenumber = d.employeenumber
  AND a.thedate BETWEEN d.fromdate AND d.thrudate
LEFT JOIN tptechvalues e on d.techkey = e.techkey
  AND a.thedate BETWEEN e.fromdate AND e.thrudate;
  

-- flaghours
-- 1 row per techkey/day which have flaghours
-- DROP TABLE #flag_hours;
/*
SELECT a.techkey, a.flagdatekey, sum(a.flaghours) AS flag_hours
INTO #flag_hours
FROM dds.factrepairorder a
INNER JOIN #days b on a.flagdatekey = b.datekey
WHERE a.flaghours <> 0
group by a.techkey, a.flagdatekey;  
*/
-- oops, forgot adjustments
DROP TABLE #flag_hours;
SELECT x.techkey, x.flagdatekey, x.flag_hours + coalesce(y.ptlhrs, 0) AS flag_hours
INTO #flag_hours
FROM (
  SELECT a.techkey, a.flagdatekey, sum(a.flaghours) AS flag_hours
  -- INTO #flag_hours
  FROM dds.factrepairorder a
  INNER JOIN #days b on a.flagdatekey = b.datekey
  WHERE a.flaghours <> 0
  group by a.techkey, a.flagdatekey) x
LEFT JOIN (
  SELECT c.datekey, b.techkey, round(sum(a.ptlhrs), 2) AS ptlhrs
  FROM dds.stgarkonasdpxtim a
  INNER JOIN dds.dimtech b on a.pttech = b.technumber
  INNER JOIN dds.day c on a.ptdate = c.thedate
  GROUP BY c.datekey, b.techkey) y on x.techkey = y.techkey AND x.flagdatekey = y.datekey;

  
  SELECT COUNT(*) FROM dds.edwClockHoursFact 
  select *
  FROM #flag_hours
-- clockhours
-- get hourly/comm AND hourly_rate FROM edwEmployeeDim 
-- DROP TABLE #clock_hours;
SELECT a.datekey, c.employeenumber, c.hourlyrate, c.payrollclass,
  SUM(a.clockhours) AS clock_hours, 
  SUM(a.vacationhours + a.ptohours) AS pto_hours, 
  SUM(a.holidayhours) AS holiday_hours
INTO #clock_hours  
FROM dds.edwClockHoursFact a
INNER JOIN #days b on a.datekey = b.datekey
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
GROUP BY a.datekey, c.employeenumber, c.hourlyrate, c.payrollclass
HAVING SUM(a.clockhours + a.vacationhours + a.ptohours) > 0

select * FROM #tech_Days

SELECT a.*, coalesce(flag_hours, 0) AS flag_hours, 
  coalesce(c.clock_hours, 0) AS clock_hours, coalesce(c.pto_hours, 0) AS pto_hours,
  coalesce(c.holiday_hours, 0) AS holiday_hours

-- this looks LIKE the spreadsheet that was sent to jeri IN Oct 2016
SELECT a.yearmonth, a.lastname, a.firstname, a.payrollclass, max(a.labor_cost) AS cost,   
  SUM(flag_hours) AS flag_hours, SUM(clock_hours) AS clock_hours,
  SUM(pto_hours + holiday_hours) AS pto_hours,
  SUM(pto_hours * pto_rate) AS pto_pay,
  round(SUM(pto_hours * pto_rate)/SUM(flag_hours), 2) AS pto_per_flag,
  round(sum(clock_hours)/SUM(CASE WHEN workday = true then 1 ELSE 0 END), 2) AS avg_clock_day,
  CASE 
    WHEN SUM(clock_hours) = 0 THEN 0
    ELSE round(100 * SUM(flag_hours)/SUM(clock_hours), 2) 
  END AS proficiency
FROM #tech_days a 
LEFT JOIN #flag_hours b on a.techkey = b.techkey
  AND a.datekey = b.flagdatekey
LEFT JOIN #clock_hours c on a.employeenumber = c.employeenumber 
  AND a.datekey = c.datekey
GROUP BY a.yearmonth, a.lastname, a.firstname, a.payrollclass
ORDER BY a.lastname, a.yearmonth

SELECT yearmonth, SUM(flag_hours) AS flag_hours, SUM(clock_hours) AS clock_hours,
  CASE SUM(clock_hours)
    WHEN 0 THEN 0
	ELSE round(100*SUM(flag_hours)/SUM(clock_hours),2)
  END
FROM (  
SELECT a.yearmonth, a.lastname, a.firstname, a.payrollclass, max(a.labor_cost) AS cost,   
  SUM(flag_hours) AS flag_hours, SUM(clock_hours) AS clock_hours,
  SUM(pto_hours + holiday_hours) AS pto_hours,
  SUM(pto_hours * pto_rate) AS pto_pay,
  round(SUM(pto_hours * pto_rate)/SUM(flag_hours), 2) AS pto_per_flag,
  round(sum(clock_hours)/SUM(CASE WHEN workday = true then 1 ELSE 0 END), 2) AS avg_clock_day,
  CASE 
    WHEN SUM(clock_hours) = 0 THEN 0
    ELSE round(100 * SUM(flag_hours)/SUM(clock_hours), 2) 
  END AS proficiency
FROM #tech_days a 
LEFT JOIN #flag_hours b on a.techkey = b.techkey
  AND a.datekey = b.flagdatekey
LEFT JOIN #clock_hours c on a.employeenumber = c.employeenumber 
  AND a.datekey = c.datekey
GROUP BY a.yearmonth, a.lastname, a.firstname, a.payrollclass
) x GROUP BY yearmonth



SELECT x.firstname, x.lastname, x.flaghours, y.flag_hours, round(x.flaghours - y.flag_hours, 2)
FROM (
  SELECT firstname, lastname, SUM(flaghours + shoptime) AS flaghours, SUM(clockhours) AS clockhours,
    case sum(clockhours)
      when 0 then 0
  	else round(100*SUM(flaghours + shoptime)/SUM(clockhours), 2)
    END AS prof
  -- select SUM(flaghours) AS flaghours, sum(shoptime) AS shoptime, SUM(clockhours) AS clockhours
  FROM tmpben
  WHERE flagdeptcode = 'mr'
    AND firstname IS NOT NULL 
    AND yearmonth = 201605
    AND storecode = 'ry1'
  GROUP BY firstname, lastname) x  
left JOIN (
  SELECT a.lastname, a.firstname,  
    SUM(flag_hours) AS flag_hours, SUM(clock_hours) AS clock_hours,
    CASE 
      WHEN SUM(clock_hours) = 0 THEN 0
      ELSE round(100 * SUM(flag_hours)/SUM(clock_hours), 2) 
    END AS proficiency
  FROM #tech_days a 
  LEFT JOIN #flag_hours b on a.techkey = b.techkey
    AND a.datekey = b.flagdatekey
  LEFT JOIN #clock_hours c on a.employeenumber = c.employeenumber 
    AND a.datekey = c.datekey
  WHERE a.yearmonth = 201605  
  GROUP BY a.lastname, a.firstname) y on x.firstname = y.firstname AND x.lastname = y.lastname


SELECT *
FROM (  
  SELECT thedate, a.lastname, a.firstname,  
    SUM(flag_hours) AS flag_hours, SUM(clock_hours) AS clock_hours,
    CASE 
      WHEN SUM(clock_hours) = 0 THEN 0
      ELSE round(100 * SUM(flag_hours)/SUM(clock_hours), 2) 
    END AS proficiency
  FROM #tech_days a 
  LEFT JOIN #flag_hours b on a.techkey = b.techkey
    AND a.datekey = b.flagdatekey
  LEFT JOIN #clock_hours c on a.employeenumber = c.employeenumber 
    AND a.datekey = c.datekey
  WHERE a.yearmonth = 201605  
    and firstname = 'clayton'
  GROUP BY thedate, a.lastname, a.firstname) x 
LEFT JOIN (
  select thedate, flaghours
  FROM tmpben
  WHERE firstname = 'clayton'
    AND yearmonth = 201605) y on x.thedate = y.thedate  
	
SELECT * FROM dds.dimtech	
	
SELECT *
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
  AND b.thedate = '05/31/2016'
INNER JOIN dds.dimtech c on a.techkey = c.techkey	
  AND c.description LIKE '%clayton%'
  





	
--------------------------------------------------------------------------------
-- 10/19  payrate
--------------------------------------------------------------------------------

of the techs currently employed, 
payrollclass, 
past year
-- teampay
SELECT employeenumber, lastname, firstname, commission,
  round(max(techhourlyrate), 2) AS pto_rate,
  round(avg(techtfrrate), 2) AS avg_comm_rate, 
  round(MIN(techtfrrate), 2) AS min_comm_rate,
  round(MAX(techtfrrate), 2) AS max_comm_rate  
FROM tpdata
WHERE departmentkey = 18
  AND thedate = payperiodend
  AND techkey IN (
    SELECT techkey
	FROM tpdata
	WHERE departmentkey = 18
	  AND thedate = curdate() -1)
  AND thedate BETWEEN  '10/01/2015' AND '10/01/2016'
GROUP BY lastname, firstname, employeenumber

-- start with techs
SELECT * FROM #techs 

SELECT * FROM dds.edwEmployeeDim 

SELECT COUNT(distinct employeenumber) FROM #techs -- 38

SELECT employeenumber, lastname, firstname, 'commission', 0 AS hourly_rate,
  round(max(techhourlyrate), 2) AS pto_rate,
  round(avg(techtfrrate), 2) AS avg_comm_rate, 
  round(MIN(techtfrrate), 2) AS min_comm_rate,
  round(MAX(techtfrrate), 2) AS max_comm_rate  
FROM tpdata
WHERE departmentkey = 18
  AND thedate = payperiodend
  AND techkey IN (
    SELECT techkey
	FROM tpdata
	WHERE departmentkey = 18
	  AND thedate = curdate() -1)
  AND thedate BETWEEN  '10/01/2015' AND '10/01/2016'
GROUP BY lastname, firstname, employeenumber
union
SELECT a.employeenumber, b.lastname, b.firstname, b.payrollclass,
  cast(hourlyrate AS sql_numeric(6,2)) AS hourly_rate, 0,0,0,0
FROM (
  SELECT employeenumber
  FROM #techs
  GROUP BY employeenumber) a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'Active'
WHERE b.payrollclass = 'hourly'  
