one row per tech/pay period:  gross pay, flag hours, gross pay/flaghours


-- pay periods
biweeklypayperiodstartdate
biweeklypayperiodenddate

select MIN(biweeklypayperiodstartdate) -- 9/20/15
FROM dds.day 
WHERE thedate > '09/30/2015'


select max(biweeklypayperiodenddate) -- 10/01/2016
FROM dds.day 
WHERE thedate < '09/30/2016'

SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate, COUNT(*)
FROM dds.day
WHERE thedate BETWEEN '09/20/2015' AND '10/01/2016'
GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate;

DROP TABLE #days;
SELECT biweeklypayperiodstartdate AS from_date, biweeklypayperiodenddate AS thru_date,
	(SELECT datekey FROM dds.day WHERE thedate = a.biweeklypayperiodstartdate) AS from_date_key,
	(SELECT datekey FROM dds.day WHERE thedate = a.biweeklypayperiodenddate) AS thru_date_key,
    CAST((SELECT mmdd FROM dds.day WHERE thedate = a.biweeklypayperiodstartdate) AS sql_char)
	+ ' -> '
	+ CAST((SELECT mmdd FROM dds.day WHERE thedate = a.biweeklypayperiodenddate) AS sql_char)
INTO #days	
FROM (
  SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate
  FROM dds.day a4
  WHERE thedate BETWEEN '09/20/2015' AND '10/01/2016'
  GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate) a;	

SELECT * FROM #days
  
-- techs  
DROP TABLE #techs;
SELECT a.storecode, a.flagdeptcode, a.techkey, a.technumber, a.employeenumber, 
  b.lastname, b.firstname
INTO #techs
FROM dds.dimtech a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.employeenumber <> 'NA'
--  AND storecode = 'ry1'
  AND a.flagdeptcode IN ('am','bs','mr')
  AND EXISTS (
    SELECT 1
	FROM dds.factrepairorder m
	INNER JOIN dds.day n on m.flagdatekey = n.datekey
	  AND n.thedate BETWEEN '09/20/2015' AND '10/01/2016'
	WHERE techkey = a.techkey);
	

SELECT d.from_date, d.thru_date, SUM(c.flaghours) AS flaghours,
  c.storecode, c.employeenumber, c.flagdeptcode, c.firstname, c.lastname
INTO #flag_hours  
FROM (	
  SELECT b.thedate, aa.employeenumber, a.flaghours,
    aa.storecode, aa.flagdeptcode, 
    aa.firstname, aa.lastname
  FROM dds.factrepairorder a
  INNER JOIN #techs aa on a.techkey = aa.techkey
  INNER JOIN dds.day b on a.flagdatekey = b.datekey
    AND b.thedate BETWEEN (SELECT MIN(from_date) FROM #days) 
      AND (SELECT MAX(thru_date) FROM #days)) c
LEFT JOIN #days d on c.thedate BETWEEN d.from_date AND d.thru_date	  
GROUP BY d.from_date, d.thru_date, c.storecode, c.employeenumber, 
  c.flagdeptcode, c.firstname, c.lastname;
  
SELECT COUNT(*) FROM #flag_hours  
  
-- this IS gross FROM payroll  
CREATE TABLE z_tech_gross (
  gross numeric(8,2),
  employee_number cichar(9),
  from_date date);  
  
SELECT a.from_date, a.thru_date, a.storecode, a.flagdeptcode, a.lastname, a.firstname, 
  round(a.flaghours, 0) AS flag_hours, round(b.gross, 0) AS gross,
  case a.flaghours
    when 0 then 0
	else round(b.gross/a.flaghours, 0) 
  END AS gross_per_flag_hour
FROM #flag_hours a
LEFT JOIN z_tech_Gross b on a.employeenumber = b.employee_number
  AND a.from_Date = b.from_date
ORDER BY a.storecode, a.flagdeptcode, a.from_date, a.lastname