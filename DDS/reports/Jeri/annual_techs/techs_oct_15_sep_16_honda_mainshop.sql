--------------------------------------------------------------------------------  
-- 10/17 so fucking start over again -------------------------------------------
--------------------------------------------------------------------------------
-- 1 row for each day
-- include workday designator
DROP TABLE #days;
SELECT yearmonth,thedate,datekey,
  CASE 
    WHEN weekday = true AND holiday = false THEN true
	ELSE false
  END AS workday
INTO #days
-- SELECT COUNT(*) -- 366
FROM dds.day
WHERE yearmonth BETWEEN 201510 AND 201609;

-- 1 row for each techkey on an ro for the time period
DROP TABLE #techs;
SELECT techkey, technumber, employeenumber, techkeyfromdate, techkeythrudate, 
  cast(laborcost AS sql_numeric(6,2)) AS labor_cost
INTO #techs
-- SELECT COUNT(*) -- 145
FROM dds.dimtech a
WHERE employeenumber <> 'NA'
  AND storecode = 'ry2'
  AND flagdept = 'service'
  AND EXISTS (
    SELECT 1
	FROM dds.factrepairorder m
	INNER JOIN dds.day n on m.flagdatekey = n.datekey
	  AND n.yearmonth BETWEEN 201510 AND 201609
	WHERE storecode = 'ry2'
	  AND techkey = a.techkey);

-- 1 row for each day/techkey for which that techkey was active
-- ADD edwEmployeeDim stuff: employeekey, payrollclass, hourlyrate, name
-- AND tp stuff: previoushourlyrate
DROP TABLE #tech_days;
SELECT a.*, b.*, c.lastname, c.firstname, c.employeekey, c.payrollclass, 
  CASE c.payrollclass
    WHEN 'Hourly' THEN cast(c.hourlyrate as sql_numeric (6,2))
    ELSE round(coalesce(e.previoushourlyrate, 0), 2) 
  end AS pto_rate
INTO #tech_days
-- SELECT COUNT(*) -- 13120
FROM #days a
full OUTER JOIN #techs b on a.thedate BETWEEN b.techkeyfromdate AND b.techkeythrudate
LEFT JOIN dds.edwEmployeeDim c on b.employeenumber = c.employeenumber
  and a.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
LEFT JOIN tptechs d on b.employeenumber = d.employeenumber
  AND a.thedate BETWEEN d.fromdate AND d.thrudate
LEFT JOIN tptechvalues e on d.techkey = e.techkey
  AND a.thedate BETWEEN e.fromdate AND e.thrudate;
  

-- flaghours
-- 1 row per techkey/day which have flaghours
-- DROP TABLE #flag_hours;
/*
SELECT a.techkey, a.flagdatekey, sum(a.flaghours) AS flag_hours
INTO #flag_hours
FROM dds.factrepairorder a
INNER JOIN #days b on a.flagdatekey = b.datekey
WHERE a.flaghours <> 0
group by a.techkey, a.flagdatekey;  
*/
-- oops, forgot adjustments
DROP TABLE #flag_hours;
SELECT x.techkey, x.flagdatekey, x.flag_hours + coalesce(y.ptlhrs, 0) AS flag_hours
INTO #flag_hours
FROM (
  SELECT a.techkey, a.flagdatekey, sum(a.flaghours) AS flag_hours
  -- INTO #flag_hours
  FROM dds.factrepairorder a
  INNER JOIN #days b on a.flagdatekey = b.datekey
  WHERE a.flaghours <> 0
  group by a.techkey, a.flagdatekey) x
LEFT JOIN (
  SELECT c.datekey, b.techkey, round(sum(a.ptlhrs), 2) AS ptlhrs
  FROM dds.stgarkonasdpxtim a
  INNER JOIN dds.dimtech b on a.pttech = b.technumber
  INNER JOIN dds.day c on a.ptdate = c.thedate
  GROUP BY c.datekey, b.techkey) y on x.techkey = y.techkey AND x.flagdatekey = y.datekey;


-- clockhours
-- get hourly/comm AND hourly_rate FROM edwEmployeeDim 
-- DROP TABLE #clock_hours;
SELECT a.datekey, c.employeenumber, c.hourlyrate, c.payrollclass,
  SUM(a.clockhours) AS clock_hours, 
  SUM(a.vacationhours + a.ptohours) AS pto_hours, 
  SUM(a.holidayhours) AS holiday_hours
INTO #clock_hours  
FROM dds.edwClockHoursFact a
INNER JOIN #days b on a.datekey = b.datekey
INNER JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
GROUP BY a.datekey, c.employeenumber, c.hourlyrate, c.payrollclass
HAVING SUM(a.clockhours + a.vacationhours + a.ptohours) > 0

-- spreadsheet for jeri
SELECT a.yearmonth, a.lastname, a.firstname, a.payrollclass, max(a.labor_cost) AS cost,   
  SUM(flag_hours) AS flag_hours, SUM(clock_hours) AS clock_hours,
  SUM(pto_hours + holiday_hours) AS pto_hours,
  SUM(pto_hours * pto_rate) AS pto_pay,
  round(SUM(pto_hours * pto_rate)/SUM(flag_hours), 2) AS pto_per_flag,
  round(sum(clock_hours)/SUM(CASE WHEN workday = true then 1 ELSE 0 END), 2) AS avg_clock_day,
  CASE 
    WHEN SUM(clock_hours) = 0 THEN 0
    ELSE round(100 * SUM(flag_hours)/SUM(clock_hours), 2) 
  END AS proficiency
FROM #tech_days a 
LEFT JOIN #flag_hours b on a.techkey = b.techkey
  AND a.datekey = b.flagdatekey
LEFT JOIN #clock_hours c on a.employeenumber = c.employeenumber 
  AND a.datekey = c.datekey
-- WHERE a.yearmonth = 201609  
GROUP BY a.yearmonth, a.lastname, a.firstname, a.payrollclass


-- monthly totals
SELECT yearmonth, SUM(flag_hours) AS flag_hours, SUM(clock_hours) AS clock_hours,
  CASE SUM(clock_hours)
    WHEN 0 THEN 0
	ELSE round(100*SUM(flag_hours)/SUM(clock_hours),2)
  END
FROM (  
SELECT a.yearmonth, a.lastname, a.firstname, a.payrollclass, max(a.labor_cost) AS cost,   
  SUM(flag_hours) AS flag_hours, SUM(clock_hours) AS clock_hours,
  SUM(pto_hours + holiday_hours) AS pto_hours,
  SUM(pto_hours * pto_rate) AS pto_pay,
  round(SUM(pto_hours * pto_rate)/SUM(flag_hours), 2) AS pto_per_flag,
  round(sum(clock_hours)/SUM(CASE WHEN workday = true then 1 ELSE 0 END), 2) AS avg_clock_day,
  CASE 
    WHEN SUM(clock_hours) = 0 THEN 0
    ELSE round(100 * SUM(flag_hours)/SUM(clock_hours), 2) 
  END AS proficiency
FROM #tech_days a 
LEFT JOIN #flag_hours b on a.techkey = b.techkey
  AND a.datekey = b.flagdatekey
LEFT JOIN #clock_hours c on a.employeenumber = c.employeenumber 
  AND a.datekey = c.datekey
GROUP BY a.yearmonth, a.lastname, a.firstname, a.payrollclass
) x GROUP BY yearmonth


-- diff BETWEEN this (y) AND tmpBen (x)(Production Summary)
SELECT x.firstname, x.lastname, x.flaghours, y.flag_hours, round(x.flaghours - y.flag_hours, 2)
FROM (
  SELECT firstname, lastname, SUM(flaghours + shoptime) AS flaghours, SUM(clockhours) AS clockhours,
    case sum(clockhours)
      when 0 then 0
  	else round(100*SUM(flaghours + shoptime)/SUM(clockhours), 2)
    END AS prof
  -- select SUM(flaghours) AS flaghours, sum(shoptime) AS shoptime, SUM(clockhours) AS clockhours
  FROM tmpben
  WHERE flagdeptcode = 'bs'
    AND firstname IS NOT NULL 
    AND yearmonth = 201609
    AND storecode = 'ry1'
  GROUP BY firstname, lastname) x  
left JOIN (
  SELECT a.lastname, a.firstname,  
    SUM(flag_hours) AS flag_hours, SUM(clock_hours) AS clock_hours,
    CASE 
      WHEN SUM(clock_hours) = 0 THEN 0
      ELSE round(100 * SUM(flag_hours)/SUM(clock_hours), 2) 
    END AS proficiency
  FROM #tech_days a 
  LEFT JOIN #flag_hours b on a.techkey = b.techkey
    AND a.datekey = b.flagdatekey
  LEFT JOIN #clock_hours c on a.employeenumber = c.employeenumber 
    AND a.datekey = c.datekey
  WHERE a.yearmonth = 201609  
  GROUP BY a.lastname, a.firstname) y on x.firstname = y.firstname AND x.lastname = y.lastname



