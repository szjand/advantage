SELECT a.thedate, a.yearmonth, b.gtjrnl, gtacct, gtctl#, gtdesc, gttamt 
FROM day a
INNER JOIN  stgarkonaglptrns b ON a.thedate = b.gtdate
WHERE a.thedate BETWEEN '01/31/2013' AND '03/31/2013'
  AND gtacct IN ('12214','12304','12305','12306')
  AND gttamt > 0
  
  
SELECT yearmonth, gtjrnl, gtacct, gtctl#, COUNT(*), SUM(gttamt)
FROM (
SELECT a.thedate, a.yearmonth, b.gtjrnl, gtacct, gtctl#, gtdesc, gttamt 
FROM day a
INNER JOIN  stgarkonaglptrns b ON a.thedate = b.gtdate
WHERE a.thedate BETWEEN '01/31/2013' AND '03/31/2013'
  AND gtacct IN ('12214','12304','12305','12306')
  AND gttamt > 0) a 
GROUP BY yearmonth, gtjrnl, gtacct, gtctl#
ORDER by gtctl#


SELECT yearmonth, gtctl#, COUNT(*), SUM(gttamt)
FROM (
SELECT a.thedate, a.yearmonth, b.gtjrnl, gtacct, gtctl#, gtdesc, gttamt 
FROM day a
INNER JOIN  stgarkonaglptrns b ON a.thedate = b.gtdate
WHERE a.thedate BETWEEN '01/31/2013' AND '03/31/2013'
  AND gtacct IN ('12214','12304','12305','12306')
  AND gttamt > 0) a 
GROUP BY yearmonth, gtctl#
ORDER by gtctl#


SELECT a.gtacct, gtctl#, b.ymname, b.ymdept, a.january, a.february, a.march
FROM (
  SELECT gtacct, gtctl#, 
    sum(CASE WHEN yearmonth = 201301 THEN gttamt END) AS January,
    sum(CASE WHEN yearmonth = 201302 THEN gttamt END) AS February,
    sum(CASE WHEN yearmonth = 201303 THEN gttamt END) AS March
  FROM (
    SELECT a.thedate, a.yearmonth, b.gtjrnl, gtacct, gtctl#, gtdesc, gttamt 
    FROM day a
    INNER JOIN  stgarkonaglptrns b ON a.thedate = b.gtdate
    WHERE a.thedate BETWEEN '01/31/2013' AND '03/31/2013'
      AND gtacct IN ('12214','12304','12305','12306')
      /*AND gttamt > 0*/) a 
    GROUP BY gtacct, gtctl#) a
LEFT JOIN (
  select ymco#, ymempn, ymname, ymdept 
  FROM stgarkonapymast) b ON a.gtctl# = b.ymempn
WHERE ymdept IS NOT NULL   
ORDER BY ymdept, ymname  