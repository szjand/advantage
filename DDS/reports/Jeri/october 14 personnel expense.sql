Hi Jon,

Will you run a report by employee # and name that details the personnel expense for October for the following accounts:

Main Shop
12104
12304
12404

Carwash
12127
12327
12427


SELECT c.lastname, c.firstname, b.*, d.gmdesc AS [Account Desc]
FROM (
  SELECT a.gtctl# AS control, a.gtacct AS account, SUM(a.gttamt) AS amount
  FROM stgArkonaGLPTRNS a
  WHERE a.gtdate BETWEEN '10/01/2014' AND '10/31/2014'
    AND a.gtacct IN ('12104','12304','12404','12127','12327','12427')
  GROUP BY a.gtctl#, a.gtacct) b
LEFT JOIN edwEmployeeDim c on b.control = c.employeenumber
  AND c.currentrow = true  
LEFT JOIN stgArkonaGLPMAST d on b.account = d.gmacct
  AND d.gmyear = 2014
ORDER by account, lastname  
  