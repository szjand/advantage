Will you please run a detailed by employee name for November vs. October for the following accounts?

12303
12403

12304
12404

12306
12406

SELECT c.lastname, c.firstname, b.*, d.gmdesc AS [Account Desc]
FROM (
  SELECT a.gtctl# AS control, a.gtacct AS account, 
  coalesce(sum(CASE WHEN month(gtdate) = 10 THEN a.gttamt END), 0) AS Oct,
  coalesce(SUM(CASE WHEN month(gtdate) = 11 THEN a.gttamt END), 0) AS Nov
--   SUM(a.gttamt) AS amount
  FROM stgArkonaGLPTRNS a
  WHERE a.gtdate BETWEEN '10/01/2014' AND '11/30/2014'
    AND a.gtacct IN ('12303','12403','12304','12404','12306','12406')
  GROUP BY a.gtctl#, a.gtacct) b
LEFT JOIN edwEmployeeDim c on b.control = c.employeenumber
  AND c.currentrow = true  
LEFT JOIN stgArkonaGLPMAST d on b.account = d.gmacct
  AND d.gmyear = 2014
ORDER by account, lastname