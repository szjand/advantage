assumptions
1. service type: only BS

observations
1. most of the repair info IS IN ccc.correction

only
service typ = bs
ro - 18...
exclude shop/training ros

ok, get parts cost,
actually probably need line level parts sales AND cost, DO it IN pg FROM pdptdet

proficiency: for the pay period IN which the ro closed

fuck, DO i need to figure out what a tech was actually paid for an ro
i believe there IS no fucking way to DO that for team pay techs
techs are paid based on clock hours, NOT flag hours
but, it can be reverse engineeered

team prof * tech rate * tech clock hours = tech pay

tech pay/tech flag hours = tech rate per flag hour
so i need pay period info for the techs

so, since pay periods are going to be the granularity for figuring constraint hours,
  let the date window be defined be a complete SET of pay periods
-- the date range
SELECT min(biweeklypayperiodstartdate)
FROM dds.day
WHERE yearmonth = 201509
union
SELECT max(biweeklypayperiodenddate)
FROM dds.day
WHERE yearmonth = 201608 


1 row per ro/line/corcode/tech/flag_date
select ro, line, technumber, corcode, flag_date, COUNT(*) FROM #bs_ro_details GROUP BY ro, line, technumber, corcode, flag_date HAVING COUNT(*) > 1 ORDER BY COUNT(*) desc
service type: BS
opcode: no shop/training time

DROP TABLE #bs_ro_details;
SELECT b.thedate as close_date, a.ro , a.line, a.flaghours, bb.thedate AS flag_date,
  d.complaint, d.cause, d.correction, e.opcode, left(trim(e.description), 100) AS opcode_desc, 
  f.opcode as corcode, left(trim(f.description), 100) AS corcode_desc,
  g.comment,a.ropartssales, a.rolaborsales,
  a.roshopsupplies, a.rohazardousmaterials, a.ropaintmaterials,
  h.technumber, h.employeenumber, h.laborcost,
  i.lastname, i.firstname, 
  b.biweeklypayperiodstartdate, b.biweeklypayperiodenddate 
INTO #bs_ro_details
-- select a.*
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
--  AND b.yearmonth BETWEEN 201508 AND 201607
  AND b.thedate BETWEEN (
      SELECT min(biweeklypayperiodstartdate)
      FROM dds.day
      WHERE yearmonth = 201509)
	AND (
      SELECT max(biweeklypayperiodenddate)
      FROM dds.day
      WHERE yearmonth = 201608)
LEFT JOIN dds.day bb on a.flagdatekey = bb.datekey  
INNER JOIN dds.dimservicetype c on a.servicetypekey =  c.servicetypekey
  AND c.servicetypecode = 'BS'
INNER JOIN dds.dimccc d on a.ccckey = d.ccckey
INNER JOIN dds.dimopcode e on a.opcodekey = e.opcodekey
  AND e.opcode NOT IN ('SHOP','TRAIN') 
INNER JOIN dds.dimopcode f on a.corcodekey = f.opcodekey
INNER JOIN dds.dimrocomment g on a.rocommentkey = g.rocommentkey
INNER JOIN dds.dimtech h on a.techkey = h.techkey
  AND h.flagdeptcode = 'BS'  
  AND h.employeenumber <> 'NA'
LEFT JOIN dds.edwEmployeeDim i on h.employeenumber = i.employeenumber  
  AND i.currentrow = true
WHERE roflaghours <> 0;

select * FROM #bs_ro_details

-- bs techs
SELECT lastname, firstname, employeenumber, technumber,
  CASE team
    WHEN 'hourly' THEN 'hourly'
	ELSE 'commission'
  END AS pay_type 
-- SELECT *
FROM tmpDeptTechCensus
WHERE flagdeptcode = 'bs'
  AND employeenumber IN (SELECT employeenumber FROM #bs_ro_details GROUP BY employeenumber)
  AND thedate = curdate()
ORDER BY lastname  

-- proficiency
-- clockhours
-- 1 row per pay period/tech

SELECT a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, a.technumber, 
  a.employeenumber, a.lastname, a.firstname, SUM(d.clockhours) AS clock_hours 
INTO #bs_clock_hours   
FROM (  
  SELECT biweeklypayperiodstartdate, biweeklypayperiodenddate,
    technumber, employeenumber, lastname, firstname 
  FROM #bs_ro_details  
  GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate,
    technumber, employeenumber, lastname, firstname) a 
INNER JOIN dds.day b on b.thedate BETWEEN a.biweeklypayperiodstartdate and a.biweeklypayperiodenddate
INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
INNER JOIN dds.edwClockHoursFact d on b.datekey = d.datekey
  AND c.employeekey = d.employeekey
GROUP BY a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, a.technumber,
  a.employeenumber, a.lastname, a.firstname;

-- flag hours  
-- these are going to be different FROM the team pay OR productivity numbers
-- due to the limits of the ros being analyzed IN bs_ro_details
-- 1 row per pay period/tech
SELECT a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  a.technumber, a.employeenumber, a.lastname, a.firstname, 
  SUM(a.flaghours) AS flag_hours
INTO #bs_flag_hours  
FROM #bs_ro_details a
GROUP BY a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  a.technumber, a.employeenumber, a.lastname, a.firstname

  
SELECT a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  a.technumber, a.employeenumber, a.lastname, a.firstname,
  a.flag_hours, b.clock_hours,
  CASE b.clock_hours
    WHEN 0 THEN 0
	ELSE round(100 * a.flag_hours/b.clock_hours, 2)
  END AS prof
INTO #bs_prof  
FROM (
  SELECT a.c, a.biweeklypayperiodenddate, 
    a.technumber, a.employeenumber, a.lastname, a.firstname, 
    SUM(a.flaghours) AS flag_hours
  FROM #bs_ro_details a
  GROUP BY a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
    a.technumber, a.employeenumber, a.lastname, a.firstname) a
LEFT JOIN (
  SELECT a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, a.technumber, 
    a.employeenumber, a.lastname, a.firstname, SUM(d.clockhours) AS clock_hours   
  FROM (  
    SELECT v, biweeklypayperiodenddate,
      technumber, employeenumber, lastname, firstname 
    FROM #bs_ro_details  
    GROUP BY biweeklypayperiodstartdate, biweeklypayperiodenddate,
      technumber, employeenumber, lastname, firstname) a 
  INNER JOIN dds.day b on b.thedate BETWEEN a.biweeklypayperiodstartdate and a.biweeklypayperiodenddate
  INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  INNER JOIN dds.edwClockHoursFact d on b.datekey = d.datekey
    AND c.employeekey = d.employeekey
  GROUP BY a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, a.technumber,
    a.employeenumber, a.lastname, a.firstname) b on a.technumber = b.technumber
      AND a.biweeklypayperiodstartdate = b.biweeklypayperiodstartdate
 
select * FROM #bs_prof ORDER by biweeklypayperiodstartdate, technumber

select * FROM dds.dimtech
-- 10/26, don't exclude parts only ros
DROP TABLE #bs_ro_details_1;
SELECT b.thedate as close_date, a.ro , a.line, a.flaghours, bb.thedate AS flag_date,
  d.complaint, d.cause, d.correction, e.opcode, left(trim(e.description), 100) AS opcode_desc,
  f.opcode as corcode, left(trim(f.description), 100) AS corcode_desc,
  g.comment,a.ropartssales, a.rolaborsales,
  a.roshopsupplies, a.rohazardousmaterials, a.ropaintmaterials,
  h.technumber, coalesce(h.employeenumber, 'NA'), coalesce(h.laborcost, 0), 
  h.flagDeptCode, 
  coalesce(i.lastname, 'N/A'), coalesce(i.firstname, 'N/A'),
  b.biweeklypayperiodstartdate, b.biweeklypayperiodenddate
INTO #bs_ro_details_1
-- select a.*
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
--  AND b.yearmonth BETWEEN 201508 AND 201607
  AND b.thedate BETWEEN (
      SELECT min(biweeklypayperiodstartdate)
      FROM dds.day
      WHERE yearmonth = 201509)
	AND (
      SELECT max(biweeklypayperiodenddate)
      FROM dds.day
      WHERE yearmonth = 201608)
LEFT JOIN dds.day bb on a.flagdatekey = bb.datekey  
INNER JOIN dds.dimservicetype c on a.servicetypekey =  c.servicetypekey
  AND c.servicetypecode = 'BS'
INNER JOIN dds.dimccc d on a.ccckey = d.ccckey
INNER JOIN dds.dimopcode e on a.opcodekey = e.opcodekey
  AND e.opcode NOT IN ('SHOP','TRAIN') 
INNER JOIN dds.dimopcode f on a.corcodekey = f.opcodekey
INNER JOIN dds.dimrocomment g on a.rocommentkey = g.rocommentkey
left JOIN dds.dimtech h on a.techkey = h.techkey
--  AND h.flagdeptcode = 'BS'  
--  AND h.employeenumber <> 'NA'
LEFT JOIN dds.edwEmployeeDim i on h.employeenumber = i.employeenumber  
  AND i.currentrow = true
-- WHERE roflaghours <> 0;
WHERE LEFT(a.ro,2) = '18'

SELECT COUNT(*) FROM #bs_ro_details; --15805

SELECT COUNT(*) FROM #bs_ro_details_1; --17265

SELECT employeenumber, COUNT(*) FROM #bs_ro_details_1 GROUP BY employeenumber

SELECT *
FROM #bs_Ro_details_1
WHERE employeenumber IS NULL

SELECT *
FROM #bs_Ro_details_1
WHERE ro = '18038665'

SELECT *
FROM #bs_Ro_details_1
WHERE ro = '18045812'

select *
FROM #bs_ro_details_1
WHERE ro = '18037796'