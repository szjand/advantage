﻿-- 10/27/16 add payment type, vin
-- 10/27/16 add payment type, vin

drop table ads.bs_ro_details;
create table ads.bs_ro_details (
  open_date date not null,
  close_date date not null,
  ro citext not null,
  line integer not null,
  flag_hours numeric(6,2),
  flag_date date not null,
  complaint citext not null,
  cause citext not null,
  correction citext not null,
  op_code citext not null,
  op_code_description citext not null,
  cor_code citext not null,
  cor_code_description citext not null,
  ro_comment citext not null,
  ro_parts_sales numeric(8,2) not null,
  ro_labor_sales numeric(8,2) not null,
  ro_shop_supplies numeric(8,2) not null,
  ro_hazardous_materials numeric(8,2) not null,
  ro_paint_materials numeric(8,2) not null,
  tech_number citext not null,
  employee_number citext not null,
  tech_labor_cost numeric(8,2) not null,
  flag_dept_code citext not null,
  last_name citext not null,
  first_name citext not null,
  pay_period_start date not null,
  pay_period_end date not null,
  payment_type citext not null,
  vin citext not null,
  constraint pk unique(ro,line,cor_code,tech_number,flag_date));
  
drop table ads.bs_tech_proficiency; 
create table ads.bs_tech_proficiency (
  pay_period_start date not null,
  pay_period_end date not null,
  tech_number citext not null,
  employee_number citext not null,
  last_name citext not null,
  first_name citext not null,
  flag_hours numeric(6,2) not null,
  clock_hours numeric(6,2) not null,
  proficiency numeric(6,2) not null,
  constraint bs_tech_proficiency_pk unique(pay_period_start,tech_number));

truncate ads.bs_tech_proficiency;
insert into ads.bs_tech_proficiency
SELECT a.pay_period_start, a.pay_period_end, 
  a.tech_number, a.employee_number, a.last_name, a.first_name,
  a.flag_hours, b.clock_hours,
  CASE b.clock_hours
    WHEN 0 THEN 0
	ELSE round(100 * a.flag_hours/b.clock_hours, 2)
  END AS prof
FROM (
  SELECT a.pay_period_start, a.pay_period_end, 
    a.tech_number, a.employee_number, a.last_name, a.first_name, 
    SUM(a.flag_hours) AS flag_hours
  FROM ads.bs_ro_details a
  where a.flag_dept_code = 'BS'
    and a.employee_number <> 'NA'
  GROUP BY a.pay_period_start, a.pay_period_end, 
    a.tech_number, a.employee_number, a.last_name, a.first_name) a
LEFT JOIN (
  SELECT a.pay_period_start, a.pay_period_end, a.tech_number, 
    a.employee_number, a.last_name, a.first_name, SUM(d.clockhours) AS clock_hours   
  FROM (  
    SELECT pay_period_start, pay_period_end,
      tech_number, employee_number, last_name, first_name 
    FROM ads.bs_ro_details 
    where flag_dept_code = 'BS' 
      and employee_number <> 'NA'
    GROUP BY pay_period_start, pay_period_end,
      tech_number, employee_number, last_name, first_name) a 
  INNER JOIN dds.day b on b.thedate BETWEEN a.pay_period_start and a.pay_period_end
  INNER JOIN ads.ext_dds_edwEmployeeDim c on a.employee_number = c.employeenumber
  INNER JOIN ads.ext_dds_edwClockHoursFact d on b.datekey = d.datekey
    AND c.employeekey = d.employeekey
  GROUP BY a.pay_period_start, a.pay_period_end, a.tech_number,
    a.employee_number, a.last_name, a.first_name) b on a.tech_number = b.tech_number
      AND a.pay_period_start = b.pay_period_start;

select pay_period_end from ads.bs_Tech_proficiency group by pay_period_end order by pay_period_end

-- parts
-- !!! ro_parts_sales from ro is not a reliable source of parts info
drop table ads.bs_ro_parts;
create table ads.bs_ro_parts (
  ro citext not null,
  line integer not null,
  sales integer not null,
  cogs integer not null,
  gross integer not null,
  constraint bs_ro_parts_pk unique(ro,line));
insert into ads.bs_ro_parts  
select ro, line, 
  round(sum(ptqty * ptnet), 0) as parts_sales,
  round(sum(ptqty * ptcost), 0) as parts_cogs,
  round(sum((ptqty * ptnet) - (ptqty * ptcost)), 0) as parts_gross
from (  
  select a.ro, a.line, coalesce(b.ptqty, 0) as ptqty,
    coalesce(b.ptnet, 0) as ptnet, 
    coalesce(b.ptcost, 0) as ptcost
  from (
    select ro, line
    from ads.bs_ro_details
    group by ro, line) a
  left join dds.ext_pdptdet b on a.ro = b.ptinv_
    and a.line = b.ptline) x
group by ro, line;   

select *
from (
  select ro, ro_parts_sales
  from ads.bs_ro_details
  where ro_parts_sales <> 0
  group by ro, ro_parts_sales) a
full outer join (
  select ro, sum(sales) as sales
  from ads.bs_ro_parts
  where sales <> 0
  group by ro) b on a.ro = b.ro 
where a.ro is null or b.ro is null  

select *
from dds.ext_pdptdet
where ptinv_ = '18039616'

select * from ads.bs_ro_parts where ro = '18039616'


select *
from (
  select ro, ro_labor_sales, sum(flag_hours) as flag_hours
  from ads.bs_ro_details
  group by ro, ro_labor_sales) a
left join (
  select ro, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross
  from ads.bs_ro_parts
  group by ro) b on a.ro = b.ro  
order by flag_hours desc  

select *
from ads.bs_ro_details

select flag_hours, tech_number,employee_number,tech_labor_cost,
  flag_dept_code,last_name,first_name,
  ro_labor_sales, 
  flag_hours * tech_labor_cost as cogs,
  ro_labor_sales/(select sum(flag_hours) from ads.bs_ro_Details where ro = a.ro)
-- select *  
from ads.bs_ro_details a
where ro = '18045558'  


select ro, sum(flag_hours) as flag_hours, ro_labor_sales, round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
  round(ro_labor_sales - sum(flag_hours*tech_labor_cost), 0) as labor_gross
from ads.bs_ro_details a
where ro = '18045558'  
group by ro, ro_labor_sales

-- date range: 8/23/15 thru 9/3/2016
-- gross
select a.ro, a.open_date, a.close_date, a.flag_hours, round(a.ro_labor_sales, 0) as labor_sales, a.labor_cogs, a.labor_gross, 
  b.sales as parts_sales, b.cogs as parts_cogs, b.gross as parts_gross, 
  round(a.ro_shop_supplies, 0) as shop_supplies, round(ro_hazardous_materials, 0) as hazardous_materials, 
  round(ro_paint_materials, 0) as paint_materials,
  a.labor_gross + b.gross as total_gross
from (
  select ro, open_date, close_date, sum(flag_hours) as flag_hours, ro_labor_sales,
    round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
    round(ro_labor_sales - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
    ro_shop_supplies, ro_hazardous_materials, ro_paint_materials
  from ads.bs_ro_details
  group by ro, open_date, close_date, ro_labor_sales, ro_shop_supplies, ro_hazardous_materials, ro_paint_materials) a
left join (
  select ro, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross
  from ads.bs_ro_parts
  group by ro) b on a.ro = b.ro  
where a.labor_gross + b.gross > 0  
order by a.labor_gross + b.gross desc 


tech classifications per randy

paint/prep
metal
detail
paint
metal/paint
metal/glass
pdr/metal
disassembly

NAME	            TECH ROLL	    TECHNICIAN RATING
Adam, Patrick	    Paint Prep	  A
Driscoll, Terry	  Metal	        High B
Eberly, Marcus	  Metal	        Low C
Gardner, Chad	    Metal	        A
Hicks, Emily	    Detailer	    Not a Tech
Iverson, Arnold	  Metal	        A
Jacobson, Peter	  Paint	        A
Johnson, Cody	    Metal & Paint	B / A
Lamont, Brandon	  Metal	        Low B
Lene, Ryan	      Metal	        A
Lindom, Aaron	    Metal/Glass	  C Metal / A Glass
Mavity, Robert	  Prep/Paint	  A Prep / B Paint
Olson, Justin	    Metal	        B 
Olson, Cody	      Metal	        Low C
Peterson, Mavrik	Paint	        A
Peterson, Brian	  PDR/Metal	    A PDR / C Metal
Reuter, Tim	      Disassembly 	Not a Tech
Rose, Cory	      Metal	        A
Sevigny, Scott	  Metal	        Low A
Shereck, Loren	  Metal	        A
Swanson, Kristen	Metal	        C
Walden, Chris	    Paint	        A
Walton, Josh	    Metal	        A

drop table ads.bs_techs;
create table ads.bs_techs (
  tech_number citext not null,
  employee_number citext not null,
  last_name citext not null,
  first_name citext not null,
  classification citext not null);

-- 10/31/2016 manually updated to include tech rating (from randy's spreadsheet)
-- added primary key (tech_number,employee_number)
insert into ads.bs_techs  
select tech_number,employee_number,last_name,first_name,
  case
    when last_name in ('adam', 'mavity') then 'paint/prep'
    when last_name in ('driscoll','eberle','gardner','iverson','lamont','lene','olson','rose','sevigny','shereck','swanson','walton','perry','ramirez') then 'metal'
    when last_name = 'hicks' then 'detailer'
    when last_name in ('jacobson', 'walden','trosen') then 'paint'
    when last_name = 'johnson' then 'metal/paint'
    when last_name = 'lindom' then 'metal/glass'
    when last_name = 'peterson' and first_name = 'brian' then 'pdr/metal'
    when last_name = 'peterson' and first_name = 'mavrik' then 'paint'
    when last_name = 'reuter' then 'disassembly'
  end as classification
from ads.bs_ro_details
where flag_dept_code = 'bs'
  and employee_number <> 'na'
group by tech_number,employee_number,flag_dept_code,last_name,first_name

select op_code, count(*) from ads.bs_ro_details group by op_code order by count(*) desc 

select a.open_date, a.close_Date, a.ro, a.line, a.flag_hours, 
  a.op_code, a.cor_code, a.tech_number, a.last_name, a.first_name, b.classification, 
  c.proficiency, d.ro_flag_hours,
  case c.proficiency
    when 0 then 0
    else round(100 * a.flag_hours/c.proficiency, 2)
  end as touch_time
from ads.bs_ro_details a
left join ads.bs_techs b on a.tech_number = b.tech_number
left join ads.bs_tech_proficiency c on a.tech_number = c.tech_number
  and a.pay_period_start = c.pay_period_start
left join (
  select ro, sum(flag_hours) as ro_flag_hours
  from ads.bs_ro_details
  group by ro) d on a.ro = d.ro  
order by   case c.proficiency
    when 0 then 0
    else round(100 * a.flag_hours/c.proficiency, 2)
  end desc   

select a.ro, string_agg(a.tech_number || ':' || flag_hours::citext, '|')
from ads.bs_ro_details a
-- inner join ads.bs_Techs b on a.tech_number = b.tech_number
group by ro

select *
from ads.bs_ro_details
limit 1000

-- compare ro_labor_sales to accounting
-- looks ok
select a.amount, b.*--b.account, description, account_type
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
where a.control = '18045558'
order by account

select a.ro, a.ro_labor_sales, b.gl_amount, a.ro_labor_sales + b.gl_amount
from (
  select ro, ro_labor_sales
  from ads.bs_ro_details
  group by ro, ro_labor_sales) a
left join (
  select control, account, sum(amount) as gl_amount
  from fin.fact_gl a
  inner join fin.dim_account b on a.account_key = b.account_key
  inner join dds.day c on a.date_key = c.datekey
    and c.thedate between '08/23/2015' and '09/03/2016'
  where b.account in ('147300','147100','147000','147200')
  group by control, account) b on a.ro = b.control
where a.ro_labor_Sales > 0
order by a.ro
limit 1000


-- group byopcode, tech classification to get touchtime in a string_agg

select a.ro, a.line, a.op_code, a.tech_number, b.classification, c.proficiency, a.flag_hours,
  case c.proficiency
    when 0 then 0
    else round(100 * a.flag_hours/c.proficiency, 2) 
  end as touch_time
from ads.bs_ro_details a
inner join ads.bs_techs b on a.tech_number = b.tech_number
left join ads.bs_tech_proficiency c on a.tech_number = c.tech_number
  and a.pay_period_start = c.pay_period_start
limit 100

select ro, string_agg(op_code || ':' || classification || ':' || touch_time::citext, ',')
-- select *
from (
  select a.ro, a.line, a.op_code, a.tech_number, b.classification, c.proficiency, a.flag_hours,
    case c.proficiency
      when 0 then 0
      else round(100 * a.flag_hours/c.proficiency, 2) 
    end as touch_time
  from ads.bs_ro_details a
  inner join ads.bs_techs b on a.tech_number = b.tech_number
  left join ads.bs_tech_proficiency c on a.tech_number = c.tech_number
    and a.pay_period_start = c.pay_period_start  
  limit 100) x
group by ro   
order by ro

select

-- leave out op_code
select ro, string_agg(classification || ':' || touch_time::citext, ','),
  (select sum(flag_hours) from ads.bs_ro_details where ro = y.ro) as ro_flag_hours
from (
  select ro, classification, sum(touch_time) as touch_time
  -- select *
  from (
    select a.ro, a.line, a.op_code, a.tech_number, b.classification, c.proficiency, a.flag_hours,
      case c.proficiency
        when 0 then 0
        else round(100 * a.flag_hours/c.proficiency, 2) 
      end as touch_time
    from ads.bs_ro_details a
    inner join ads.bs_techs b on a.tech_number = b.tech_number
    left join ads.bs_tech_proficiency c on a.tech_number = c.tech_number
      and a.pay_period_start = c.pay_period_start
    limit 100) x
  group by ro, classification) y 
group by ro  
order by ro  

-- just metal touch time
select ro, sum(touch_time) as metal_touch_time,
(select sum(flag_hours) from ads.bs_ro_details where ro = x.ro) as ro_flag_hours
from (
  select a.ro, a.line, a.op_code, a.tech_number, b.classification, c.proficiency, a.flag_hours,
    case c.proficiency
      when 0 then 0
      else round(100 * a.flag_hours/c.proficiency, 2) 
    end as touch_time
  from ads.bs_ro_details a
  inner join ads.bs_techs b on a.tech_number = b.tech_number
  left join ads.bs_tech_proficiency c on a.tech_number = c.tech_number
    and a.pay_period_start = c.pay_period_start
  where b.classification = 'metal') x  
group by ro  

-- tpcu throughput per constraint unit (gross per metal touch time)
-- 10/27 added payment type, total sales
-- 10/31 added multiple_ros
drop table if exists tpcu;
create temp table tpcu as
select m.*, labor_sales + parts_sales as total_sales, n.metal_touch_time, 
  case metal_touch_time
    when 0 then 0
    else round(total_gross/metal_touch_time, 2)
  end as "tp/cu", ''::citext as multiple_ros
from (-- gross
  select a.ro, a.open_date, a.close_date, a.flag_hours, round(a.ro_labor_sales, 0) as labor_sales, a.labor_cogs, a.labor_gross, 
    b.sales as parts_sales, b.cogs as parts_cogs, b.gross as parts_gross, 
    round(a.ro_shop_supplies, 0) as shop_supplies, round(ro_hazardous_materials, 0) as hazardous_materials, 
    round(ro_paint_materials, 0) as paint_materials,
    a.labor_gross + b.gross as total_gross, payment_type
  from (
    select ro, open_date, close_date, sum(flag_hours) as flag_hours, ro_labor_sales,
      round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
      round(ro_labor_sales - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
      ro_shop_supplies, ro_hazardous_materials, ro_paint_materials, 
      string_agg(distinct payment_type, ',') as payment_type   
    from ads.bs_ro_details a
    where not exists (
      select 1
      from ads.bs_multiple_ros
      where ro = a.ro)
    group by ro, open_date, close_date, ro_labor_sales, ro_shop_supplies, ro_hazardous_materials, ro_paint_materials) a
  left join (
    select ro, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross
    from ads.bs_ro_parts
    group by ro) b on a.ro = b.ro  
  where a.labor_gross + b.gross > 0) m
inner join (-- just metal touch time
  select ro, sum(touch_time) as metal_touch_time
  from (
    select a.ro, a.line, a.op_code, a.tech_number, b.classification, c.proficiency, a.flag_hours,
      case c.proficiency
        when 0 then 0
        else round(100 * a.flag_hours/c.proficiency, 2) 
      end as touch_time
    from ads.bs_ro_details a
    inner join ads.bs_techs b on a.tech_number = b.tech_number
    left join ads.bs_tech_proficiency c on a.tech_number = c.tech_number
      and a.pay_period_start = c.pay_period_start
    where b.classification = 'metal') x  
  group by ro) n on m.ro = n.ro
union
select m.ro, m.open_date, m.close_date, m.flag_hours, m.labor_sales, m.labor_cogs, m.labor_gross,
  m.parts_Sales, m.parts_cogs, m.parts_gross, m.shop_supplies, m.hazardous_materials, 
  m.paint_materials, m.total_gross, m.payment_type, 
  labor_sales + parts_sales as total_sales, n.metal_touch_time, 
  case metal_touch_time
    when 0 then 0
    else round(total_gross/metal_touch_time, 2)
  end as "tp/cu", m.multiple_ros
from (-- gross
  select a.ro, a.open_date, a.close_date, a.flag_hours, round(a.ro_labor_sales, 0) as labor_sales, a.labor_cogs, a.labor_gross, 
    b.sales as parts_sales, b.cogs as parts_cogs, b.gross as parts_gross, 
    round(a.ro_shop_supplies, 0) as shop_supplies, round(ro_hazardous_materials, 0) as hazardous_materials, 
    round(ro_paint_materials, 0) as paint_materials,
    a.labor_gross + b.gross as total_gross, payment_type, a.vin, a.multiple_ros
  from (
    select min(ro) || '*' as ro, 
      min(open_date) as open_date, max(close_date) as close_date, sum(flag_hours) as flag_hours, 
      sum(ro_labor_sales) as ro_labor_sales,
      round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
      round(sum(ro_labor_sales) - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
      sum(ro_shop_supplies) as ro_shop_supplies, 
      sum(ro_hazardous_materials) as ro_hazardous_materials, 
      sum(ro_paint_materials) as ro_paint_materials, 
      string_agg(distinct payment_type, ',') as payment_type,
      string_agg(distinct ro, '|') as multiple_ros, vin 
    from ads.bs_ro_details a
    where exists (
      select 1
      from ads.bs_multiple_ros
      where ro = a.ro)
    group by vin) a
  left join ( -- parts
    select aa.vin, sum(a.sales) as sales, sum(a.cogs) as cogs, sum(a.gross) as gross
    from ads.bs_ro_parts a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    group by aa.vin) b on a.vin = b.vin 
  where a.labor_gross + b.gross > 0) m
inner join (-- just metal touch time
  select vin, min(ro) as min_ro, max(ro) as max_ro, sum(touch_time) as metal_touch_time, string_agg(distinct ro, ',') as ro_agg
  from (
    select a.ro, a.line, a.op_code, a.tech_number, b.classification, c.proficiency, a.flag_hours, a.vin,
      case c.proficiency
        when 0 then 0
        else round(100 * a.flag_hours/c.proficiency, 2) 
      end as touch_time
    from ads.bs_ro_details a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    inner join ads.bs_techs b on a.tech_number = b.tech_number
    left join ads.bs_tech_proficiency c on a.tech_number = c.tech_number
      and a.pay_period_start = c.pay_period_start
    where b.classification = 'metal' order by vin) x  
  group by vin) n on m.vin = n.vin; 

select * from tpcu

select * from dds.day where yearmonth between 201610 and 201611

-- the dups to be unioned
    select min(ro) || '*' as ro, 
      min(open_date) as open_date, max(close_date) as close_date, sum(flag_hours) as flag_hours, 
      sum(ro_labor_sales) as ro_labor_sales,
      round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
      round(sum(ro_labor_sales) - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
      sum(ro_shop_supplies) as ro_shop_supplies, 
      sum(ro_hazardous_materials) as ro_hazardous_materials, 
      sum(ro_paint_materials) as ro_paint_materials, 
      string_agg(distinct payment_type, ',') as payment_type,
      string_agg(distinct ro, ',') as multiple_ros, vin 
    from ads.bs_ro_details a
    where exists (
      select 1
      from ads.bs_multiple_ros
      where ro = a.ro)
    group by vin

-- parts
    select aa.vin, sum(a.sales) as sales, sum(a.cogs) as cogs, sum(a.gross) as gross
    from ads.bs_ro_parts a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    group by aa.vin   
    
-- metal touch time
  select vin, min(ro) as min_ro, max(ro) as max_ro, sum(touch_time) as metal_touch_time, string_agg(distinct ro, ',') as ro_agg
  from (
    select a.ro, a.line, a.op_code, a.tech_number, b.classification, c.proficiency, a.flag_hours, a.vin,
      case c.proficiency
        when 0 then 0
        else round(100 * a.flag_hours/c.proficiency, 2) 
      end as touch_time
    from ads.bs_ro_details a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    inner join ads.bs_techs b on a.tech_number = b.tech_number
    left join ads.bs_tech_proficiency c on a.tech_number = c.tech_number
      and a.pay_period_start = c.pay_period_start
    where b.classification = 'metal' order by vin) x  
  group by vin

select * from ads.bs_multiple_ros where vin = '19XFB2F93DE087900'


select *
from ads.bs_multiple_ros a
left join (
    select a.ro, a.line, a.op_code, a.tech_number, b.classification, c.proficiency, a.flag_hours, a.vin,
      case c.proficiency
        when 0 then 0
        else round(100 * a.flag_hours/c.proficiency, 2) 
      end as touch_time
    from ads.bs_ro_details a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    inner join ads.bs_techs b on a.tech_number = b.tech_number
    left join ads.bs_tech_proficiency c on a.tech_number = c.tech_number
      and a.pay_period_start = c.pay_period_start
    where b.classification = 'metal') b on a.ro = b.ro
where b.ro is null      

-- the 18044559/18044060 issue, ro split in two to facilitate paying tech on a big
-- job before the job is done
-- turns out there are a lot of these, not always for the same reason
-- need to deal with it
-- 1. exclude the ros from the base query, add them to the end results separately
-- 10/31 think i want to go for defining the date range that encompasses all the ros, 
--    then group all the shit from the different ros, maybe add an * to the ro number to signify
--    multiple ros
not limited to 2 ros

-- first cut, crude based on proximity of closing dates
select *
from (
  select ro, vin, open_date, close_date, sum(flag_hours)
  from ads.bs_ro_details
  where vin not like '1111%'
    and length(vin) = 17
  group by ro, vin, open_date, close_date) a
inner join (  
  select ro, vin, open_date, close_date, sum(flag_hours)
  from ads.bs_ro_details
  where vin not like '1111%'
    and length(vin) = 17  
  group by ro, vin, open_date, close_date) b on a.vin = b.vin
    and a.ro <> b.ro
    and abs(a.close_date - b.close_date) < 14
order by a.vin

1. find those ros to exclude from base query
any ro for which there are any ros for the same vin with overlapping open/close date range
-- this may be it
select *
from (
  select ro, vin, daterange(open_date,close_date,'[]') as date_range, sum(flag_hours)
  from ads.bs_ro_details
  where vin not like '1111%' and length(vin) = 17
  group by ro, vin, daterange(open_date,close_date,'[]')) a
where exists (
  select 1
  from ads.bs_ro_details
  where vin = a.vin
    and ro <> a.ro
    -- hell, all i need is any overlap
    and a.date_range && daterange(open_date,close_date,'[]'))
order by vin    


-- now i want to group by vin
-- hell don't need to worry about defining an exhaustive, inclusive range, i have the fucking ros
-- wait a minute, i do need the range, final output (tpcu) includes open/close dates,
-- maybe not, may be able to do it by grouping on vin
create table ads.bs_multiple_ros (
  ro citext not null,
  vin citext not null,
  constraint bs_multiple_ros_pk unique(ro, vin));
-- drop table if exists ro_multiples;
-- create temp table ro_multiples as
-- any ro for which there are any ros for the same vin with overlapping open/close date range
insert into ads.bs_multiple_ros
select ro, vin
-- select * 
from (
  select ro, vin, daterange(open_date,close_date,'[]') as date_range, sum(flag_hours),
  from ads.bs_ro_details
  where vin not like '1111%' and length(vin) = 17
  group by ro, vin, daterange(open_date,close_date,'[]')) a
where exists (
  select 1
  from ads.bs_ro_details
  where vin = a.vin
    and ro <> a.ro
    and a.date_range && daterange(open_date,close_date,'[]')) 
group by ro, vin
order by vin

select a.vin, max(b.close_date) 
from ads.bs_multiple_ros a 
inner join ads.bs_ro_details b on a.ro = b.ro
group by a.vin
order by a.vin

select a.ro, a.vin
from ads.bs_ro_details a    
inner join ads.bs_multiple_ros b on a.ro = b.ro
group by a.ro, a.vin
order by count(*) desc






select open_date, close_date
from ads.bs_ro_details
limit 100

select open_date, close_date, daterange(open_date,close_date,'[]')
from ads.bs_ro_details
limit 100

need to process = ranges separately from contained ranges

select *
from (
  select ro, vin, daterange(open_date,close_date,'[]') as date_range, sum(flag_hours)
  from ads.bs_ro_details
  where vin not like '1111%' and length(vin) = 17
  group by ro, vin, daterange(open_date,close_date,'[]')) a
inner join (  
  select ro, vin, daterange(open_date,close_date,'[]') as date_range, sum(flag_hours)
  from ads.bs_ro_details
  group by ro, vin, daterange(open_date,close_date,'[]')) b on a.vin = b.vin
    and a.ro <> b.ro
--     and a.date_range @> b.date_range  and a.date_range <> b.date_range  -- contains but not equal
--     and a.date_range = b.date_range
    and a.date_range && b.date_range
--     and lower(a.date_range) <= lower(b.date_range) -- insures one way
    and a.date_range <> b.date_range
    and not (a.date_range @> b.date_range)
    and not (b.date_range @> a.date_range)
order by a.vin, a.ro    

select * from (
select a.ro, a.vin
from (
  select ro, vin, daterange(open_date,close_date,'[]') as date_range, sum(flag_hours)
  from ads.bs_ro_details
  where vin not like '1111%' and length(vin) = 17
  group by ro, vin, daterange(open_date,close_date,'[]')) a
inner join (  
  select ro, vin, daterange(open_date,close_date,'[]') as date_range, sum(flag_hours)
  from ads.bs_ro_details
  group by ro, vin, daterange(open_date,close_date,'[]')) b on a.vin = b.vin
    and a.ro <> b.ro
    and a.date_range @> b.date_range
union
select b.ro, b.vin
from (
  select ro, vin, daterange(open_date,close_date,'[]') as date_range, sum(flag_hours)
  from ads.bs_ro_details
  where vin not like '1111%' and length(vin) = 17
  group by ro, vin, daterange(open_date,close_date,'[]')) a
inner join (  
  select ro, vin, daterange(open_date,close_date,'[]') as date_range, sum(flag_hours)
  from ads.bs_ro_details
  group by ro, vin, daterange(open_date,close_date,'[]')) b on a.vin = b.vin
    and a.ro <> b.ro
    and a.date_range @> b.date_range
) C order by ro 