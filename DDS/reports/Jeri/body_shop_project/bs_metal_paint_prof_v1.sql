﻿drop table if exists gross;
create temp table gross as
  select a.ro, a.open_date, a.close_date, a.flag_hours, round(a.ro_labor_sales, 0) as labor_sales, a.labor_cogs, a.labor_gross, 
    b.sales as parts_sales, b.cogs as parts_cogs, b.gross as parts_gross, 
    round(a.ro_shop_supplies, 0) as shop_supplies, round(ro_hazardous_materials, 0) as hazardous_materials, 
    round(ro_paint_materials, 0) as paint_materials,
    a.labor_gross + b.gross as total_gross, payment_type,
    round(a.ro_labor_sales + b.sales) as total_sales, ''::citext as multiple_ros,
    a.vin
  from ( -- ro level
    select ro, open_date, close_date, sum(flag_hours) as flag_hours, ro_labor_sales,
      round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
      round(ro_labor_sales - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
      ro_shop_supplies, ro_hazardous_materials, ro_paint_materials, 
      string_agg(distinct payment_type, ',') as payment_type, a.vin   
    from ads.bs_ro_details a
    where not exists (
      select 1
      from ads.bs_multiple_ros
      where ro = a.ro)
    group by ro, open_date, close_date, ro_labor_sales, ro_shop_supplies, ro_hazardous_materials, ro_paint_materials, a.vin) a
  left join ( -- parts
    select ro, sum(sales) as sales, sum(cogs) as cogs, sum(gross) as gross
    from ads.bs_ro_parts
    group by ro) b on a.ro = b.ro  
union
  select a.ro, a.open_date, a.close_date, a.flag_hours, round(a.ro_labor_sales, 0) as labor_sales, a.labor_cogs, a.labor_gross, 
    b.sales as parts_sales, b.cogs as parts_cogs, b.gross as parts_gross, 
    round(a.ro_shop_supplies, 0) as shop_supplies, round(ro_hazardous_materials, 0) as hazardous_materials, 
    round(ro_paint_materials, 0) as paint_materials,
    a.labor_gross + b.gross as total_gross, payment_type,
    round(a.ro_labor_sales + b.sales) as total_sales,
    multiple_ros, a.vin
  from ( -- ro level
    select min(a.ro) || '*' as ro, 
      min(open_date) as open_date, max(close_date) as close_date, sum(flag_hours) as flag_hours, 
      sum(ro_labor_sales) as ro_labor_sales,
      round(sum(flag_hours*tech_labor_cost), 0) as labor_cogs,
      round(sum(ro_labor_sales) - sum(flag_hours*tech_labor_cost), 0) as labor_gross,
      sum(ro_shop_supplies) as ro_shop_supplies, 
      sum(ro_hazardous_materials) as ro_hazardous_materials, 
      sum(ro_paint_materials) as ro_paint_materials, 
      string_agg(distinct payment_type, ',') as payment_type,
      string_agg(distinct a.ro, '|') as multiple_ros, a.vin   
    from ads.bs_ro_details a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    group by a.vin) a
  left join ( -- parts
    select aa.vin, sum(a.sales) as sales, sum(a.cogs) as cogs, sum(a.gross) as gross
    from ads.bs_ro_parts a
    inner join ads.bs_multiple_ros aa on a.ro = aa.ro
    group by aa.vin) b on a.vin = b.vin;  

select * from gross 

select *
from gross h
left join (
  select ro, metal_A_hours, metal_A_prof, metal_B_hours, metal_B_prof,
      metal_C_hours, metal_C_prof, 
      total_metal_hours,
      round(case 
        when total_metal_hours = 0 then 0
        else metal_a_prof * metal_a_hours/total_metal_hours
          + metal_b_prof * metal_b_hours/total_metal_hours
          + metal_c_prof * metal_c_hours/total_metal_hours
      end, 2) as total_metal_prof, 
      paint_A_hours, paint_A_prof,
      paint_B_hours, paint_B_prof,
      total_paint_hours,
      round(case 
        when total_paint_hours = 0 then 0
        else paint_a_prof * paint_a_hours/total_paint_hours
          + paint_b_prof * paint_b_hours/total_paint_hours
      end, 2) as total_paint_prof      
  from (
    select ro, sum(metal_A_hours) as metal_A_hours, sum(metal_A_prof) as metal_A_prof,
      sum(metal_B_hours) as metal_B_hours, sum(metal_B_prof) as metal_B_prof,
      sum(metal_C_hours) as metal_C_hours, sum(metal_C_prof) as metal_C_prof,
      sum(paint_A_hours) paint_A_hours, sum(paint_A_prof) as paint_A_prof,
      sum(paint_B_hours) as paint_B_hours, sum(paint_B_prof) as paint_B_prof,
      sum(metal_a_hours + metal_b_hours + metal_c_hours) as total_metal_hours,
      sum(paint_a_hours + paint_b_hours) as total_paint_hours
    from (
      select c.ro, c.classification,
        case when c.classification = 'metal:A' then c.flag_hours else 0 end as metal_A_hours,
        case when c.classification = 'metal:A' then d.proficiency else 0 end as metal_A_prof,
        case when c.classification = 'metal:B' then c.flag_hours else 0 end as metal_B_hours,
        case when c.classification = 'metal:B' then d.proficiency else 0 end as metal_B_prof,
        case when c.classification = 'metal:C' then c.flag_hours else 0 end as metal_C_hours,
        case when c.classification = 'metal:C' then d.proficiency else 0 end as metal_C_prof,
        case when c.classification like '%paint:A%' then c.flag_hours else 0 end as paint_A_hours,
        case when c.classification like '%paint:A%' then d.proficiency else 0 end as paint_A_prof,
        case when c.classification like '%paint:B%' then c.flag_hours else 0 end as paint_B_hours,
        case when c.classification like '%paint:B%' then d.proficiency else 0 end as paint_B_prof                                
      from (
        select a.pay_period_start, a.ro, b.classification, sum(flag_hours) as flag_hours
        from ads.bs_ro_details a
        inner join ads.bs_techs b on a.tech_number = b.tech_number
        -- without multiples
        left join ads.bs_multiple_ros c on a.ro = c.ro
        where c.ro is null
        group by a.pay_period_start, a.ro, b.classification) c
      left join ads.bs_class_proficiency d on c.pay_period_start = d.pay_period_start
        and c.classification = d.classification 
        and d.proficiency <> 0
      left join gross e on c.ro = e.ro      
      where d.proficiency is not null) f
  group by ro) g) i on h.ro = i.ro
where h.ro not like '%*'

union 

select *
from gross h
left join (  
  select vin, metal_A_hours, metal_A_prof, metal_B_hours, metal_B_prof,
      metal_C_hours, metal_C_prof, 
      total_metal_hours,
      round(case 
        when total_metal_hours = 0 then 0
        else metal_a_prof * metal_a_hours/total_metal_hours
          + metal_b_prof * metal_b_hours/total_metal_hours
          + metal_c_prof * metal_c_hours/total_metal_hours
      end, 2) as total_metal_prof, 
      paint_A_hours, paint_A_prof,
      paint_B_hours, paint_B_prof,
      total_paint_hours,
      round(case 
        when total_paint_hours = 0 then 0
        else paint_a_prof * paint_a_hours/total_paint_hours
          + paint_b_prof * paint_b_hours/total_paint_hours
      end, 2) as total_paint_prof  
  from ( 
    select vin, sum(metal_A_hours) as metal_A_hours, sum(metal_A_prof) as metal_A_prof,
      sum(metal_B_hours) as metal_B_hours, sum(metal_B_prof) as metal_B_prof,
      sum(metal_C_hours) as metal_C_hours, sum(metal_C_prof) as metal_C_prof,
      sum(paint_A_hours) paint_A_hours, sum(paint_A_prof) as paint_A_prof,
      sum(paint_B_hours) as paint_B_hours, sum(paint_B_prof) as paint_B_prof,
      sum(metal_a_hours + metal_b_hours + metal_c_hours) as total_metal_hours,
      sum(paint_a_hours + paint_b_hours) as total_paint_hours
    from (
      select c.vin, c.classification, 
        case when c.classification = 'metal:A' then c.flag_hours else 0 end as metal_A_hours,
        case when c.classification = 'metal:A' then d.proficiency else 0 end as metal_A_prof,
        case when c.classification = 'metal:B' then c.flag_hours else 0 end as metal_B_hours,
        case when c.classification = 'metal:B' then d.proficiency else 0 end as metal_B_prof,
        case when c.classification = 'metal:C' then c.flag_hours else 0 end as metal_C_hours,
        case when c.classification = 'metal:C' then d.proficiency else 0 end as metal_C_prof,
        case when c.classification like '%paint:A%' then c.flag_hours else 0 end as paint_A_hours,
        case when c.classification like '%paint:A%' then d.proficiency else 0 end as paint_A_prof,
        case when c.classification like '%paint:B%' then c.flag_hours else 0 end as paint_B_hours,
        case when c.classification like '%paint:B%' then d.proficiency else 0 end as paint_B_prof 
      from ( -- 1 row per ro/classification
        select a.vin, b.classification, 
          sum(flag_hours) as flag_hours,
          max(close_date) as close_date
        from ads.bs_ro_details a
        inner join ads.bs_techs b on a.tech_number = b.tech_number
        -- multiples
        inner join ads.bs_multiple_ros c on a.ro = c.ro
        group by a.vin, b.classification) c
      left join ads.bs_class_proficiency d on c.close_date between d.pay_period_start and d.pay_period_end
        and c.classification = d.classification 
        and d.proficiency <> 0 
      left join gross e on c.vin = e.vin
      -- VIN alone join is too general, it is including other visits of the same vin
      where e.ro like '%*') f
    group by vin) g) i on h.vin = i.vin
where h.ro like '%*'
