-- Honda:  22303, 22304
SELECT 
  CASE month(gtdate)
    WHEN 3 THEN 'March'
    WHEN 4 THEN 'April'
    WHEN 5 THEN 'May'
  END AS Month,
  gtacct as Account, gtctl# as Control, b.name,
  SUM(gttamt) AS [Total], COUNT(gtacct) AS [How Many]
FROM stgArkonaGLPTRNS a
LEFT JOIN (
  SELECT name, employeenumber
  FROM edwEmployeeDim
  WHERE currentrow = true) b ON a.gtctl# = b.employeenumber
WHERE year(gtdate) = 2012
  AND month(gtdate) BETWEEN 3 AND 5
  AND gtacct IN ('12301','12302')
GROUP BY month(gtdate), gtacct, gtctl#, name


SELECT month, account, name
FROM (
  SELECT 
    CASE month(gtdate)
      WHEN 3 THEN 'March'
      WHEN 4 THEN 'April'
      WHEN 5 THEN 'May'
    END AS Month,
    gtacct as Account, gtctl# as Control, b.name,
    SUM(gttamt) AS [Total], COUNT(gtacct) AS [How Many]
  FROM stgArkonaGLPTRNS a
  LEFT JOIN (
    SELECT name, employeenumber
    FROM edwEmployeeDim
    WHERE currentrow = true) b ON a.gtctl# = b.employeenumber
  WHERE year(gtdate) = 2012
    AND month(gtdate) BETWEEN 3 AND 5
    AND gtacct IN ('12301','12302')
  GROUP BY month(gtdate), gtacct, gtctl#, name) a
WHERE name <> ''  
GROUP BY month, account, name
HAVING COUNT(*) > 1