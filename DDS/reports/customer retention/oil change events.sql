/*
    sum(case when OpCode in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDEL1') then 1 else 0 end) as OilChange,
    sum(case when OpCode in ('PDQD', 'PDQDS', 'PDQM1', 'PDQ020') then 1 else 0 end) as BOC,
    sum(case when OpCode in ('13A') then 1 else 0 end) as AirFilter,
    sum(case when OpCode in ('ROT', 'FREEROT', 'ROTEMP') then 1 else 0 end) as Rotate,
    sum(case when OpCode in ('NT2', 'NT4') then 1 else 0 end) as Tires 
*/


DROP TABLE #wtf;
SELECT a.storecode, a.vin, b.thedate, b.yearmonth
INTO #wtf
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%')  
GROUP BY a.storecode, a.vin, b.thedate, b.yearmonth

at the END of any month (trending) 
how many:
  new
  first post sale
  lost
  recap
  retained

DROP TABLE #wtf1;  
SELECT a.*, b.howmany, b.first, b.last  
INTO #wtf1
FROM #wtf a
LEFT JOIN (
  SELECT vin, COUNT(*) AS howmany, MIN(thedate) AS first, MAX(thedate) AS last
  FROM #wtf
  GROUP BY vin) b ON a.vin = b.vin

SELECT * 
FROM #wtf1 a
ORDER BY vin

marketwide? yes, per greg
1B3EJ46X0YN213996
  
something LIKE

yearmonth first`  LOST              retained        recapture
                  IN prev 6-12 but  IN prev 0-6     IN prev 0-6
                  NOT IN prev 0-6   AND prev 6-12   AND NOT IN prev 6-12 AND IN more than 12 
201108      8          15            
  
SELECT * FROM #wtf1 ORDER BY vin 
 
SELECT yearmonth, 
  SUM(CASE WHEN thedate = first THEN 1 ELSE 0 END) AS first ,
  COUNT(*) AS howmany
FROM #wtf1 a
GROUP BY yearmonth


SELECT yearmonth,
  SUM(
    CASE 
      WHEN howmany <> 1 THEN
        CASE 
          WHEN EXISTS (
            SELECT 1
            FROM #wtf1
            WHERE vin = a.vin
              AND yearmonth BETWEEN a.yearmonth - 6 AND a.yearmonth) THEN 1 ELSE 0
        
        END 
    END) AS prev6
FROM #wtf1 a
GROUP BY yearmonth    
  
  
calculate ALL the relevant dates IN the vin row
ie
vin, thedate, yearmonth, prev6from, prev6-12from, prev6-12thru,morethan12

SELECT vin, curdate() as today, thedate, yearmonth, first, howmany, 
  timestampadd(sql_tsi_month, -6, thedate) AS SixMosBeforeTheDateFrom,
  thedate - 1AS SixMosAgoThru 
FROM #wtf1
ORDER BY vin    

SELECT yearmonth, 
  SUM(CASE WHEN thedate = first THEN 1 ELSE 0 END) AS first,
  SUM(CASE WHEN the date <> first AND the
  
  
so ON any given thedate, what IS 0-6 mos before, what, 6-12 mos before \
AND IS there an oil change event for that vehicle within those date ranges

maybe previous event date ON the vin row

SELECT * from #wtf1 ORDER BY howmany desc

LEFT JOIN WHERE thedate < thedate

SELECT *
FROM #wtf1 a
inner JOIN #wtf1 b ON a.thedate < b.thedate
  AND a.vin = b.vin
  
SELECT * FROM #wtf1 WHERE vin = '19XFA1F56AE029205'  

SELECT *
FROM #wtf1 a
WHERE EXISTS (
  SELECT 1
  FROM #wtf1
  WHERE vin = a.vin
    AND thedate BETWEEN a.thedate - 181 AND a.thedate -1) 
AND vin = '19XFA1F56AE029205'      

SELECT a.*, 
  CASE WHEN thedate = first THEN 1 ELSE 0 END
FROM #wtf1 a

2 subsets single AND multiple occurences
-- new vins
SELECT yearmonth, SUM(howmany)
FROM #wtf1
WHERE howmany = 1
GROUP BY yearmonth

SELECT *
FROM #wtf1
WHERE vin = '19XFA1F56AE029205'

SELECT yearmonth,
  SUM(
    CASE 
      WHEN EXISTS (
        SELECT 1
        FROM #wtf
        WHERE vin = a.vin
          AND thedate BETWEEN a.thedate - 181 AND a.thedate -1)
    THEN 1 ELSE 0 END) AS prev6mos
FROM #wtf1 a
WHERE a.vin = '19XFA1F56AE029205'
GROUP BY yearmonth


SELECT a.yearmonth,
  SUM(CASE WHEN b.vin IS NOT NULL THEN 1 ELSE 0 END) AS prev6mos
SELECT * 
FROM #wtf1 a
LEFT JOIN #wtf1 b ON a.vin = b.vin 
  AND b.thedate BETWEEN a.thedate - 181 AND a.thedate -1
WHERE a.vin = '19XFA1F56AE029205'
GROUP BY a.yearmonth

-- this IS looking close
SELECT a.*,
  CASE WHEN b.vin IS NOT NULL THEN 1 ELSE 0 END AS prev6mos  
FROM #wtf1 a
LEFT JOIN #wtf1 b ON a.vin = b.vin 
  AND b.thedate BETWEEN a.thedate - 181 AND a.thedate -1
WHERE a.vin = '19XFA1F56AE029205'

-- these are vins w/OC AND whether that vin had an OC IN the prev 6 mos
SELECT vin, b.thedate, yearmonth, first, prev6mos
FROM (
  SELECT a.*,
    CASE WHEN b.vin IS NOT NULL THEN 1 ELSE 0 END AS prev6mos  
  FROM #wtf1 a
  LEFT JOIN #wtf1 b ON a.vin = b.vin 
    AND b.thedate BETWEEN a.thedate - 181 AND a.thedate -1
  WHERE a.howmany > 1 ) b
-- WHERE a.vin = '19XFA1F56AE029205') b
GROUP BY vin, b.thedate, yearmonth, first, prev6mos


SELECT a.*,
  CASE WHEN b.vin IS NOT NULL THEN 1 ELSE 0 END AS prev6to12mos  
FROM #wtf1 a
LEFT JOIN #wtf1 b ON a.vin = b.vin 
  AND b.thedate BETWEEN a.thedate - 365 AND a.thedate - 182
WHERE a.vin = '19XFA1F56AE029205'

-- these are vins w/OC AND whether that vin had an OC IN the prev 6-12 mos
SELECT vin, b.thedate, yearmonth, first, prev6to12mos
FROM (
  SELECT a.*,
    CASE WHEN b.vin IS NOT NULL THEN 1 ELSE 0 END AS prev6to12mos  
  FROM #wtf1 a
  LEFT JOIN #wtf1 b ON a.vin = b.vin 
    AND b.thedate BETWEEN a.thedate - 365 AND a.thedate - 182
  WHERE a.howmany > 1 ) b
GROUP BY vin, b.thedate, yearmonth, first, prev6to12mos

-- these are vins w/OC AND whether that vin had an OC more than a year ago
SELECT vin, b.thedate, yearmonth, first, gtYear
FROM (
  SELECT a.*,
    CASE WHEN b.vin IS NOT NULL THEN 1 ELSE 0 END AS gtYear
  FROM #wtf1 a
  LEFT JOIN #wtf1 b ON a.vin = b.vin 
    AND b.thedate < a.thedate - 365
  WHERE a.howmany > 1 AND a.vin = '19XFA1F56AE029205') b
GROUP BY vin, b.thedate, yearmonth, first, gtYear

for each OC event vin, yearmonth, date, 
flags: prev6, prev6-12, > 1 year, first, most recent,

DROP TABLE #wtf2;
SELECT c.storecode, c.howmany, c.vin, c.thedate, c.yearmonth, 
  coalesce(d.gtYear,0) AS gtYear, 
  coalesce(e.prev6to12mos,0) AS prev6to12, coalesce(prev6mos,0) AS prev6,
  CASE WHEN c.thedate = c.first THEN 1 ELSE 0 END AS first
INTO #wtf2  
--SELECT c.*, d.gtYear
FROM #wtf1 c
LEFT JOIN (
  -- these are vins w/OC AND whether that vin had an OC more than a year ago
  SELECT vin, b.thedate, yearmonth, first, gtYear
  FROM (
    SELECT a.*,
      CASE WHEN b.vin IS NOT NULL THEN 1 ELSE 0 END AS gtYear
    FROM #wtf1 a
    LEFT JOIN #wtf1 b ON a.vin = b.vin 
      AND b.thedate < a.thedate - 365
    WHERE a.howmany > 1) b
  GROUP BY vin, b.thedate, yearmonth, first, gtYear) d ON c.vin = d.vin AND c.thedate = d.thedate
LEFT JOIN (
  -- these are vins w/OC AND whether that vin had an OC IN the prev 6-12 mos
  SELECT vin, b.thedate, yearmonth, first, prev6to12mos
  FROM (
    SELECT a.*,
      CASE WHEN b.vin IS NOT NULL THEN 1 ELSE 0 END AS prev6to12mos  
    FROM #wtf1 a
    LEFT JOIN #wtf1 b ON a.vin = b.vin 
      AND b.thedate BETWEEN a.thedate - 365 AND a.thedate - 182
    WHERE a.howmany > 1 ) b
  GROUP BY vin, b.thedate, yearmonth, first, prev6to12mos) e ON c.vin = e.vin AND c.thedate = e.thedate  
LEFT JOIN (
  -- these are vins w/OC AND whether that vin had an OC IN the prev 6 mos
  SELECT vin, b.thedate, yearmonth, first, prev6mos
  FROM (
    SELECT a.*,
      CASE WHEN b.vin IS NOT NULL THEN 1 ELSE 0 END AS prev6mos  
    FROM #wtf1 a
    LEFT JOIN #wtf1 b ON a.vin = b.vin 
      AND b.thedate BETWEEN a.thedate - 181 AND a.thedate -1
    WHERE a.howmany > 1 ) b
  -- WHERE a.vin = '19XFA1F56AE029205') b
  GROUP BY vin, b.thedate, yearmonth, first, prev6mos) f ON c.vin = f.vin AND c.thedate = f.thedate 
--WHERE c.vin = '19XFA1F56AE029205'  

CREATE TABLE zOilChanges (
  storecode cichar(3), 
  vin cichar(17), 
  thedate date,
  yearmonth integer,
  howmany integer,
  first integer,
  prev6 integer,
  prev6to12 integer,
  gtYear integer) IN database;
INSERT INTO zOilChanges
SELECT storecode, vin, thedate, yearmonth, howmany, first, prev6,prev6to12,gtYear
FROM #wtf2;  

SELECT * FROM zOilChanges;

SELECT yearmonth, COUNT(*) AS Total, 
  SUM(first) AS first, 
  SUM(CASE WHEN prev6 = 1 AND prev6to12 = 1 THEN 1 ELSE 0 END) AS Retained,
  SUM(CASE WHEN prev6 = 1 AND prev6to12 = 0 AND gtYear = 1 THEN 1 ELSE 0 END) AS Recap,
  SUM(CASE WHEN prev6 = 0 AND prev6to12 = 1 THEN 1 ELSE 0 END) AS Lost
FROM zOilChanges
GROUP BY yearmonth;

-- 2/14/13
-- ned wants any oil change CUSTOMER (regardless of dept OR service type OR store) FROM more than a
-- year ago but NOT within the past year


SELECT b.thedate, a.storecode, a.ro, a.writerid, a.customername, a.customerkey, 
  a.vin, c.*
-- SELECT a.*
-- SELECT COUNT(*)
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%')  
WHERE b.thedate BETWEEN curdate() - 730 AND curdate() - 366
  AND a.void = false
  AND a.customerkey > 0
ORDER by customerkey  


SELECT b.thedate, a.storecode, a.ro, a.writerid, a.customername, a.customerkey, 
  a.vin, c.*
-- SELECT a.*
-- SELECT COUNT(*)
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%')  
WHERE b.thedate BETWEEN curdate() - 730 AND curdate() - 366
  AND a.void = false
  AND a.customerkey > 0
AND NOT EXISTS (  
  SELECT 1
  FROM factro c
  INNER JOIN day d ON c.opendatekey = d.datekey
  INNER JOIN factroline e ON c.ro = e.ro
    AND (e.opcode LIKE '%lof%' OR e.opcode LIKE '%pdq%')  
  WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
    AND c.void = false
    AND c.customerkey > 0
    AND c.customerkey = a.customerkey)
ORDER BY customerkey    

DROP TABLE #wtf;
SELECT a.customername, a.customerkey
INTO #wtf
-- SELECT a.*
-- SELECT COUNT(*)
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN factroline c ON a.ro = c.ro
  AND (c.opcode LIKE '%lof%' OR c.opcode LIKE '%pdq%')  
WHERE b.thedate BETWEEN curdate() - 730 AND curdate() - 366
  AND a.void = false
  AND a.customerkey > 0
AND NOT EXISTS (  
  SELECT 1
  FROM factro c
  INNER JOIN day d ON c.opendatekey = d.datekey
  INNER JOIN factroline e ON c.ro = e.ro
    AND (e.opcode LIKE '%lof%' OR e.opcode LIKE '%pdq%')  
  WHERE d.thedate BETWEEN curdate() - 365 AND curdate()
    AND c.void = false
    AND c.customerkey > 0
    AND c.customerkey = a.customerkey)
GROUP BY a.customername, a.customerkey  

SELECT COUNT(*) FROM #wtf

SELECT * FROM stgarkonabopname WHERE bnsnam LIKE '%SORUM%' ORDER BY bnsnam

SELECT a.customername, a.customerkey, b.bnadr1, b.bncity, b.bnstcd, b.bnzip, b.bnphon
--SELECT COUNT(*)
INTO #wtf1
from #wtf a
INNER JOIN stgArkonaBOPNAME b ON a.customerkey = b.bnkey
  AND a.customername = b.bnsnam
WHERE a.customername NOT LIKE '%INVENTO%'  
  AND b.bnadr1 <> ''
  AND b.bnphon <> '0'
  
SELECT a.customername, MIN(thedate), MAX(thedate)
FROM #wtf1 a
LEFT JOIN factro b ON a.customerkey = b.customerkey   
LEFT JOIN day c ON b.opendatekey = c.datekey
GROUP BY a.customername
ORDER BY MAX(thedate) desc


SELECT * FROM factro a left join factroline b on a.ro = b.ro WHERE customername = 'AARNES, WAYNE'

SELECT * FROM stgarkonabopname WHERE bnsnam LIKE '%chwingl%'

SELECT b.thedate, a.*, c.* FROM factro a left join day b on a.opendatekey = b.datekey left join factroline c on a.ro = c.ro WHERE customerkey = 233357

-- discrepancy: does NOT include honda labor ops, eg MSS

SELECT * FROM stgarkonasdplopc WHERE sodes1 LIKE '%oil%' AND sodes1 NOT LIKE '%spoiler%' AND sodes1 NOT LIKE '%coil%' AND sodes1 LIKE '%hang%' ORDER BY solopc
SELECT * FROM stgarkonasdplopc WHERE solopc LIKE 'MS%'

SELECT opcode, corcode, COUNT(*) FROM factroline WHERE opcode LIKE 'pdq%' OR corcode LIKE 'pdq%' GROUP BY opcode, corcode

SELECT COUNT(*) FROM factroline WHERE opcode LIKE 'MS%'