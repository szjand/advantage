SELECT 'Gross', EmployeeNumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * TotalPay AS Amount
FROM (
  SELECT a.storecode, a.amount AS TotalPay, c.*
  FROM AccrualCommissions a
  INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
  LEFT JOIN (
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualhourly1
    UNION
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
  WHERE a.amount <> 0) a
