SELECT b.thedate, a.stocknumber, c.vin, dd.fullname, ddd.fullname, d.*, f.thedate, e.ro, f.thedate, ee.fullname
FROM factvehiclesale a
INNER JOIN day b on a.cappeddatekey = b.datekey
  AND b.thedate < curdate()
INNER JOIN dimvehicle c on a.vehiclekey = c.vehiclekey  
INNER JOIN dimcardealinfo d on a.cardealinfokey = d.cardealinfokey
left JOIN dimcustomer dd on a.buyerkey = dd.customerkey
LEFT JOIN dimcustomer ddd on a.cobuyerkey = ddd.customerkey
LEFT JOIN (
  select ro, vehiclekey, opendatekey, customerkey
  from factrepairorder
  GROUP BY ro,vehiclekey,opendatekey, customerkey) e on c.vehiclekey = e.vehiclekey
LEFT JOIN dimcustomer ee on e.customerkey = ee.customerkey  
LEFT JOIN day f on e.opendatekey = f.datekey

-- 7/9/15 IS last acquisition i have
SELECT b.thedate, a.stocknumber, c.vin, d.acquisitiontype, d.acquisitiontypecategory
FROM factvehicleacquisition a 
INNER JOIN day b on a.datekey = b.datekey 
INNER JOIN dimvehicle c on a.vehiclekey = c.vehiclekey
INNER JOIN dimvehicleAcquisitioninfo d on a.vehicleAcquisitioninfokey = d.vehicleAcquisitioninfokey


DO a UNION


SELECT 'Sale', b.thedate, a.stocknumber, c.vin, trim(dd.fullname) +'/'+ TRIM(ddd.fullname), d.saletype
INTO #retention
FROM factvehiclesale a
INNER JOIN day b on a.cappeddatekey = b.datekey
  AND thedate < '07/31/2015'
INNER JOIN dimvehicle c on a.vehiclekey = c.vehiclekey  
  AND length(TRIM(vin)) = 17
INNER JOIN dimcardealinfo d on a.cardealinfokey = d.cardealinfokey
left JOIN dimcustomer dd on a.buyerkey = dd.customerkey
LEFT JOIN dimcustomer ddd on a.cobuyerkey = ddd.customerkey
UNION 
SELECT 'Acquisition', b.thedate, a.stocknumber, c.vin, d.acquisitiontype, d.acquisitiontypecategory
FROM factvehicleacquisition a 
INNER JOIN day b on a.datekey = b.datekey 
  AND thedate < '07/31/2015'
INNER JOIN dimvehicle c on a.vehiclekey = c.vehiclekey
  AND length(TRIM(vin)) = 17
INNER JOIN dimvehicleAcquisitioninfo d on a.vehicleAcquisitioninfokey = d.vehicleAcquisitioninfokey
UNION
SELECT 'RO', b.thedate,'',c.vin, d.fullname, ''
FROM (
  select ro, opendatekey, vehiclekey, customerkey
  from factrepairorder
  GROUP BY ro, opendatekey, vehiclekey, customerkey) a
INNER JOIN day b on a.opendatekey = b.datekey
  AND thedate < '07/31/2015'
INNER JOIN dimvehicle c on a.vehiclekey = c.vehiclekey
  AND length(TRIM(vin)) = 17
INNER JOIN dimcustomer d on a.customerkey = d.customerkey
  AND d.fullname <> 'INVENTORY'
  
SELECT *
-- SELECT COUNT(*)
FROM #retention  
WHERE LEFT(vin,1) <> '0'
  AND vin IN (
    SELECT vin
	FROM #retention
	GROUP BY vin
	HAVING COUNT(*) > 6)
ORDER BY vin, thedate

DO it with just sales AND ro without the date limitation

SELECT 'Sale', b.thedate, a.stocknumber, c.vin, trim(dd.fullname) +'/'+ TRIM(ddd.fullname), d.saletype
INTO #retention_1
FROM factvehiclesale a
INNER JOIN day b on a.cappeddatekey = b.datekey
  AND thedate < curdate()
INNER JOIN dimvehicle c on a.vehiclekey = c.vehiclekey  
  AND length(TRIM(vin)) = 17
  AND LEFT(vin, 1) <> '0'
INNER JOIN dimcardealinfo d on a.cardealinfokey = d.cardealinfokey
left JOIN dimcustomer dd on a.buyerkey = dd.customerkey
LEFT JOIN dimcustomer ddd on a.cobuyerkey = ddd.customerkey
UNION
SELECT 'RO', b.thedate,'',c.vin, d.fullname, ''
FROM (
  select ro, opendatekey, vehiclekey, customerkey
  from factrepairorder
  GROUP BY ro, opendatekey, vehiclekey, customerkey) a
INNER JOIN day b on a.opendatekey = b.datekey
  AND thedate < curdate()
INNER JOIN dimvehicle c on a.vehiclekey = c.vehiclekey
  AND length(TRIM(vin)) = 17
  AND LEFT(vin, 1) <> '0'
INNER JOIN dimcustomer d on a.customerkey = d.customerkey
  AND d.fullname <> 'INVENTORY'
  
SELECT *
FROM #retention_1
ORDER BY vin,thedate  