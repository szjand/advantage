redefined:
  any ro BETWEEN 12 AND 24 months ago AND no one IN the household has subsequently
  had a pdq ro IN the last 365 days

-- ALL customers (dimCustomer, bopname) FROM 12 to 24 mos ago 
-- limit to ry1 
-- limit to non business customers
-- limit to a valid address AND a homephone, non blank firstname
-- exclude po box
-- DROP TABLE #12to24;
DROP TABLE z12to24;
CREATE TABLE z12to24 (
  storeCode cichar(3) constraint NOT NULL,
  bnkey integer constraint NOT NULL,
--  household cichar(12) constraint NOT NULL,
  constraint pk primary key (storecode, bnkey)) IN database;
--EXECUTE PROCEDURE sp_CreateIndex90( 'z12to24', 'z12to24.adi', 'household', 'household', '', 2, 512, NULL );
EXECUTE PROCEDURE sp_CreateIndex90( 'z12to24', 'z12to24.adi', 'bnkey', 'bnkey', '', 2, 512, NULL );

INSERT INTO z12to24
SELECT c.storecode, c.bnkey
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimCustomer c on a.customerkey = c.customerkey
INNER JOIN stgArkonaBOPNAME d on c.bnkey = d.bnkey
  AND c.storecode = d.bnco#
WHERE thedate BETWEEN curdate() - 760 AND curdate() - 365  
  AND c.customerTypeCode = 'I'
  AND a.storecode = 'RY1'
  AND c.lastname <> 'CARWASH'
  AND c.homephone <> '0'
  AND c.firstname <> ''
  AND d.bnadr1 <> ''
  AND d.bnadr1 NOT LIKE 'PO%'
--GROUP BY c.customerkey, c.bnkey
GROUP BY c.storecode, c.bnkey;

ALTER TABLE z12to24
ADD COLUMN household cichar(20) constraint NOT NULL;
EXECUTE PROCEDURE sp_CreateIndex90( 'z12to24', 'z12to24.adi', 'household', 'household', '', 2, 512, NULL );

UPDATE z12to24
SET household = storecode + CAST(bnkey AS sql_char) collate ads_default_ci;


-- customers with the same homephone AS z12to24
DROP TABLE #phone;
SELECT a.household, c.storecode, c.bnkey, c.fullname, c.homephone
INTO #phone
FROM z12to24 a
INNER JOIN dimCustomer b on a.bnkey = b.bnkey
INNER JOIN dimcustomer c on b.homephone = c.homephone;

--INSERT INTO z12to24 (storecode, bnkey, household)
-- DROP TABLE #wtf;

SELECT a.storecode, a.bnkey, a.household 
INTO #wtf
FROM #phone a
LEFT JOIN z12to24 b on a.storecode = b.storecode AND a.bnkey = b.bnkey
WHERE NOT EXISTS (
  SELECT 1
  FROM z12to24
  WHERE storecode = a.storecode
    AND bnkey = a.bnkey)
-- aha, belongs to multiple households
SELECT *
FROM #wtf a
INNER JOIN (    
SELECT storecode, bnkey 
FROM #wtf   
GROUP BY storecode, bnkey 
HAVING COUNT(*) > 1) b on a.storecode = b.storecode AND a.bnkey = b.bnkey
ORDER BY a.storecode, a.bnkey

SELECT *
FROM #phone
WHERE bnkey = 200129
 
select *
FROM dimcustomer
WHERE homephone = '7017461822' 

SELECT * FROM dimcustomer WHERE bnkey IN (247653,249375,256983,321388)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
-- ADD these bnkey values to z12to24, 9462 rows
INSERT INTO z12to24
SELECT bnkey, household
FROM #phone 
WHERE bnkey NOT IN (
  SELECT bnkey
  FROM z12to24);
  
SELECT *
FROM z12to24 a
INNER JOIN #phone b on a.bnkey = b.bnkey  
/*
-- ADD these bnkey values to z12to24, 9462 rows
INSERT INTO z12to24
SELECT d.bnkey
FROM (
  SELECT c.bnkey, a.household
  FROM z12to24 a
  INNER JOIN dimCustomer b on a.bnkey = b.bnkey
  INNER JOIN dimcustomer c on b.homephone = c.homephone
  GROUP BY c.bnkey) d
LEFT JOIN z12to24 e on d.bnkey = e.bnkey
WHERE e.bnkey IS NULL;  
*/

-- customers with the same address AS z12to24
SELECT a.bnkey, b.bnsnam, b.bnadr1, b.bncity, b.bnzip, b.bnphon, c.bnkey, c.bnsnam,c.bnadr1, c.bncity, c.bnzip, c.bnphon
FROM z12to24 a
INNER JOIN stgArkonaBOPNAME b on a.bnkey = b.bnkey
INNER JOIN stgArkonaBOPNAME c on b.bnadr1 = c.bnadr1;

-- ADD these bnkey values to z12to24, 
INSERT INTO z12to24
SELECT d.bnkey
FROM (
  SELECT c.bnkey
  FROM z12to24 a
  INNER JOIN stgArkonaBOPNAME b on a.bnkey = b.bnkey
  INNER JOIN stgArkonaBOPNAME c on b.bnadr1 = c.bnadr1
  GROUP BY c.bnkey) d
WHERE NOT EXISTS (
  SELECT 1
  FROM z12to24
  WHERE bnkey = d.bnkey);


-- which of these have had a pdq event IN the past year
SELECT a.ro, a.customerkey
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
LEFT JOIN #12to24 c on a.customerkey = c.customerkey
WHERE thedate BETWEEN curdate() - 365 AND curdate()
  AND LEFT(a.ro, 2) = '19'
  AND c.customerkey IS NULL 
GROUP BY a.ro, a.customerkey  
 
SELECT a.*
FROM #12to24 a
LEFT JOIN factRepairOrder b on a.customerkey
  AND LEFT(b.ro, 2) = '19'
  