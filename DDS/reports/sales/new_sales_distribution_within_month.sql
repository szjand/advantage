/*
Good morning!

I want to analyze new vehicle sales and the GM store and the Honda store (separately).

I want to see the percentage of total new vehicles sold in three time buckets:
  1-10th of the month
 11-20th of the month
 21st to the end of the month.

So if we sold 100 cars for the month of January 2015:

   1-10th of the month sales = 20 so 20/100 = 20%
   11th-20th of the month sales = 25 so 25/100 = 25%
   21st to the end of month = 55 so 55/100 = 55%.

I�d like to see it for each store for 2014 through July 2016:
  Each month
  Each calendar year.

Thanks!

I�ll be back in the store Thursday instead of Friday.

Greg
*/

SELECT b.thedate, a.stocknumber, c.*
-- SELECT COUNT(*)
-- SELECT dealtype, COUNT(*)
-- SELECT saleType, COUNT(*)
FROM factVehicleSale a
INNER JOIN day b on a.cappeddatekey = b.datekey
INNER JOIN dimCarDealInfo c on a.cardealinfokey = c.cardealinfokey
WHERE c.dealstatus = 'Capped'
  AND c.vehicletype = 'New'
  AND c.saletypecode <> 'W'
  AND b.thedate BETWEEN '01/01/2014' AND curdate()
  AND LEFT(a.stocknumber, 1) <> 'H'
-- GROUP BY dealtype  
--GROUP BY saletype
ORDER BY saletype DESC 


-- wholesale deals
SELECT d.fullname, COUNT(*)
-- SELECT COUNT(*)
-- SELECT dealtype, COUNT(*)
-- SELECT saleType, COUNT(*)
FROM factVehicleSale a
INNER JOIN day b on a.cappeddatekey = b.datekey
INNER JOIN dimCarDealInfo c on a.cardealinfokey = c.cardealinfokey
INNER JOIN dimCustomer d on a.buyerkey = d.customerkey
WHERE c.dealstatus = 'Capped'
  AND c.vehicletype = 'New'
  AND c.saletypecode = 'W'
  AND b.thedate BETWEEN '01/01/2014' AND curdate()
GROUP BY d.fullname



-- ry1
SELECT b.thedate, a.stocknumber, c.*
FROM factVehicleSale a
INNER JOIN day b on a.cappeddatekey = b.datekey
INNER JOIN dimCarDealInfo c on a.cardealinfokey = c.cardealinfokey
WHERE c.dealstatus = 'Capped'
  AND c.vehicletype = 'New'
  AND c.saletypecode <> 'W'
  AND b.thedate BETWEEN '01/01/2014' AND curdate()
  AND LEFT(a.stocknumber, 1) <> 'H'

select *
FROM (
  SELECT 
    CASE LEFT(a.stocknumber, 1)
      WHEN 'H' THEN 'RY2'
  	ELSE 'RY1'
    END AS store, 
    b.yearmonth, 
    round(100.0 * SUM(CASE WHEN b.dayofmonth BETWEEN 1 AND 10 THEN 1 ELSE 0 END)/COUNT(*), 0) AS "1-10 %",
    round(100.0 * SUM(CASE WHEN b.dayofmonth BETWEEN 11 AND 20 THEN 1 ELSE 0 END)/COUNT(*), 0) AS "11-20 %",
    round(100.0 * cast(SUM(CASE WHEN b.dayofmonth > 20 THEN 1 ELSE 0 END) AS sql_double)/COUNT(*), 0) AS "21-EOM %"
  FROM factVehicleSale a
  INNER JOIN day b on a.cappeddatekey = b.datekey
  INNER JOIN dimCarDealInfo c on a.cardealinfokey = c.cardealinfokey
  WHERE c.dealstatus = 'Capped'
    AND c.vehicletype = 'New'
    AND c.saletypecode <> 'W'
    AND b.yearmonth BETWEEN 201401 AND 201607
  --  AND LEFT(a.stocknumber, 1) <> 'H'  
  GROUP BY yearmonth,
    CASE LEFT(a.stocknumber, 1)
      WHEN 'H' THEN 'RY2'
  	ELSE 'RY1'
    END) x
ORDER BY store, yearmonth	
  