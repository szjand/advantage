﻿
select *
from ( -- pyhshdta
--     select a.employee_, check_month, sum(a.total_gross_pay) as arkona_total_gross
  select 'November' as month,
  (select last_name from scpp.sales_consultants where employee_number = a.employee_) as last_name,
  (select first_name from scpp.sales_consultants where employee_number = a.employee_),
  a.employee_, 
  sum(case when check_month = 11  then a.total_gross_pay::integer else 0 end) as total_gross_pay
  from dds.ext_pyhshdta a
  where trim(a.distrib_code) = 'SALE'
    and a.payroll_cen_year = 116
    and a.employee_ in ('123425','123600','124120','128530','133017','137220','145840','148080',
      '150040','163700','161325','167600','171150','181665','189100','1106225','1106223','1109815',
      '1112410','1130690','1132420','1135518','1141642','1147250','1151450')
    and a.company_number = 'RY1'
  group by a.employee_) m
left join ( -- pyhscdta
--      select employee_, check_month, 
--        sum(case when q.code_id in ('79','78') then amount end) as arkona_comm_draw,
--        string_agg(q.description || ':' || q.amount::citext, ' | ' order by q.description) as cat
   select q.employee_,
     sum(case when check_month = 11 and code_id = '79' then amount::integer else 0 end) as commission,
     sum(case when check_month = 11 and code_id = '78' then amount::integer else 0 end) as draw,
     sum(case when check_month = 11 and code_id = '82' then amount::integer else 0 end) as spiff,
     sum(case when check_month = 11 and code_id = '500' then amount::integer else 0 end) as lease,
     sum(case when check_month = 11 and code_id = '74' then amount::integer else 0 end) as vacation,
     sum(case when check_month = 11 and code_id = '400' then amount::integer else 0 end) as pto_payout
  from ( 
    select a.employee_name, a.employee_, a.check_month, b.description, sum(b.amount) as amount, b.code_id
    --select distinct c.description
    from dds.ext_pyhshdta a
    inner join dds.ext_pyhscdta b on a.payroll_run_number = b.payroll_run_number
      and a.company_number = b.company_number
      and a.employee_ = b.employee_number
      and b.code_type in ('0','1') -- 0,1: income, 2: deduction
    where trim(a.distrib_code) = 'SALE'
      and a.payroll_cen_year = 116
    and a.employee_ in ('123425','123600','124120','128530','133017','137220','145840','148080',
      '150040','163700','161325','167600','171150','181665','189100','1106225','1106223','1109815',
      '1112410','1130690','1132420','1135518','1141642','1147250','1151450')
      and a.company_number = 'RY1'
    group by a.employee_name, a.employee_, a.check_month, b.description, b.code_id) q
  group by  employee_) n on m.employee_ = n.employee_
order by last_name    