SELECT a.*, b.thedate, c.make, d.vehicleType, d.saleType, e.zip, e.*
FROM factVehicleSale a
INNER JOIN day b on a.cappedDateKey = b.datekey
INNER JOIN dimVehicle c on a.vehicleKey = c.vehicleKey
INNER JOIN dimCarDealInfo d on a.carDealInfoKey = d.CarDealInfoKey
INNER JOIN dimCustomer e on a.buyerKey = e.customerKey
WHERE d.saleType <> 'Wholesale'
  AND thedate BETWEEN curdate() - 180 AND curdate()
  AND e.zip = '0'
  
  
SELECT c.make,left(e.zip,5) AS zip, d.vehicleType AS NewUsed, COUNT(*) AS howMany
FROM factVehicleSale a
INNER JOIN day b on a.cappedDateKey = b.datekey
INNER JOIN dimVehicle c on a.vehicleKey = c.vehicleKey
INNER JOIN dimCarDealInfo d on a.carDealInfoKey = d.CarDealInfoKey
INNER JOIN dimCustomer e on a.buyerKey = e.customerKey
WHERE d.saleType IN ('Retail','Lease')
  AND thedate BETWEEN curdate() - 180 AND curdate()  
  AND d.vehicleType = 'New'
GROUP BY c.make, left(e.zip, 5), d.vehicleType  
ORDER BY COUNT(*) DESC 

SELECT * FROM dimcardealinfo