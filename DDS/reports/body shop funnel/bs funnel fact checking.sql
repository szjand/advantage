-- select COUNT (DISTINCT ro)
SELECT a.ro, a.line, b.thedate AS open_date, bb.thedate AS close_date,
  c.servicetype, d.fullname, e.paymentType, rolaborsales, ropartssales,
  flaghours, roflaghours
-- SELECT *  
INTO #wtf
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN day bb on a.closedatekey = bb.datekey
INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey
INNER JOIN dimCustomer d on a.customerkey = d.customerkey
INNER JOIN dimPaymentType e on a.paymentTypeKey = e.paymentTypeKey
WHERE b.yearmonth IN (201412,201501,201502)
  AND c.servicetypekey = (
    select servicetypekey
    from dimServiceType
    WHERE servicetypecode = 'bs')
    
select * from #wtf    

select ro    
FROM (
select ro, paymenttype 
FROM #wtf 
GROUP BY ro, paymenttype) a
GROUP BY ro HAVING COUNT(*) > 1  

SELECT COUNT(DISTINCT ro) FROM #wtf

select *
FROM #wtf
WHERE ro IN (
select ro    
FROM (
select ro, paymenttype 
FROM #wtf 
GROUP BY ro, paymenttype) a
GROUP BY ro HAVING COUNT(*) > 1)

SELECT * FROM zcb WHERE name = 'gmfs gross'

SELECT dl2, dl5, line, al3,
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt, 0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2015
  AND month(gtdate) = 2
WHERE a.name = 'gmfs gross' 
  AND dl3 = 'fixed'
  AND line BETWEEN  35 AND 43
GROUP BY dl2, dl5, line, al3
ORDER BY dl2, line


-- bs sales (NOT parts) lines 35 - 43
DROP TABLE #acct;
SELECT a.glaccount, a.line, b.*
INTO #acct
FROM zcb a
inner JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2015
  AND month(gtdate) = 2
WHERE a.name = 'gmfs gross' 
  AND dl3 = 'fixed'
  AND al2 = 'sales'
  AND line BETWEEN  35 AND 43
  
-- still matching the FS
SELECT line, SUM(gttamt)
FROM #acct
GROUP BY line  

-- DROP TABLE #fs;
SELECT a.glaccount, a.line, al3, gtctl#, gtdate, gtdesc, gttamt 
INTO #fs
FROM zcb a
inner JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2015
  AND month(gtdate) = 2
WHERE a.name = 'gmfs gross' 
  AND dl3 = 'fixed'
  AND al2 = 'sales'
  AND line BETWEEN  35 AND 43
  
-- how many ros -- 442 
SELECT COUNT(DISTINCT gtctl#)
FROM #fs

SELECT * FROM #fs WHERE gtctl# IN (
SELECT gtctl# FROM #fs GROUP BY gtctl# HAVING COUNT(*) > 1)
ORDER BY gtctl#

SELECT line, SUM(gttamt)
FROM #fs
GROUP BY line

SELECT DISTINCT(gtctl#) into #ro FROM #fs

select gtctl# AS [RO IN FS], b.ro AS [RO IN BS QUERY]
INTO #diff
FROM #ro a
full OUTER JOIN z_ron_ro b on a.gtctl# = b.ro
WHERE a.gtctl# IS NULL OR b.ro IS NULL 

SELECT -- 210 IN fs NOT IN query, 96 IN query NOT IN bs
  SUM(CASE WHEN [RO IN FS] IS NOT NULL THEN 1 ELSE 0 END) AS FS,
  SUM(CASE WHEN [RO IN BS QUERY] IS NOT NULL THEN 1 ELSE 0 END) AS BSQ
FROM #diff

select * FROM #diff ORDER BY [RO IN BS QUERY], [ro IN fs]

SELECT ro FROM z_ron_ro GROUP BY ro HAVING COUNT(*) > 1

-- rons query
select bsj.JobID, bsji.RoNumber 
from bs.BSJobs bsj
left outer join bs.BSJobInsurances bsji on bsj.jobid = bsji.jobid
Where Cast(JobInsertedTS as SQL_DATE) >= '2/1/2015' and Cast(JobInsertedTS as SQL_DATE) <= '2/28/2015'
  and bsj.JobID in (
    SELECT JobID 
    FROM JobProgressStatuses 
    where Status = 'Delivered_Delivered' 
      and JobID = BSJ.JobID)
group by bsj.JobID, bsji.RoNumber
order by bsj.JobID

-- arrived status
-- only RepairVehicleArrived_RepairVehicleArrived IS relevant
SELECT status, COUNT(*), MIN(Fromts), MAX(fromts) 
FROM bs.jobprogressstatuses
WHERE lower(status) LIKE '%arriv%'
GROUP BY status

SELECT jobid FROM (
SELECT a.jobid, CAST(fromTS AS sql_date) AS arrived_date, b.ronumber, 
  left(b.customer, 30) AS customer, 
  b.createdby, b.filehandler, substring(jobtype, position('_' in jobtype) + 1, 12) as jobtype, c.license, c.vin, c.usedcarstocknumber
FROM bs.JobProgressStatuses a
LEFT JOIN bs.bsjobs b on a.jobid = b.jobid
LEFT JOIN bs.bsvehicles c on b.jobid = c.jobid
WHERE CAST(a.fromTS AS sql_date) BETWEEN '10/01/2014' AND curdate() ORDER BY a.jobid
) x GROUP BY jobid HAVING COUNT(*) > 1

-- bsjobs.jobInsertedTS the date the record was created, ie, customers level of funnel

SELECT jobid FROM (
SELECT a.jobid, cast(a.jobInsertedTS as sql_date) as date_inserted, 
  a.ronumber, LEFT(a.customer,30) AS customer, a.createdby, a.filehandler,
  substring(a.jobtype, position('_' in jobtype) + 1, 12) as jobtype,
  b.license, b.vin, b.usedcarstocknumber 
FROM bs.bsjobs a 
LEFT JOIN bs.bsvehicles b on a.jobid = b.jobid
WHERE cast(a.jobInsertedTS as sql_date) BETWEEN '10/01/2014' AND curdate() 
) x GROUP BY jobid HAVING COUNT(*) > 1


SELECT jobid FROM (
  SELECT *
  FROM bs.jobprogressstatuses
  WHERE status = 'RepairVehicleArrived_RepairVehicleArrived'
    AND CAST(fromts AS sql_date) BETWEEN '10/01/2014' AND curdate()) x 
GROUP BY jobid HAVING COUNT(*) > 1


-- jobids with multiple arrived ts  
SELECT *
FROM bs.jobProgressStatuses
WHERE status = 'RepairVehicleArrived_RepairVehicleArrived'
  AND CAST(fromts AS sql_date) BETWEEN '10/01/2014' AND curdate() 
AND jobid IN (
  SELECT jobid FROM (
    SELECT *
    FROM bs.jobProgressStatuses
    WHERE status = 'RepairVehicleArrived_RepairVehicleArrived'
      AND CAST(fromts AS sql_date) BETWEEN '10/01/2014' AND curdate()) x 
  GROUP BY jobid HAVING COUNT(*) > 1)
ORDER BY jobid   

-- narrow it down, GROUP BY jobid, arrived date
SELECT jobid, date_arrived
FROM (
  SELECT jobid, CAST(fromts AS sql_date) AS date_arrived
  FROM bs.jobProgressStatuses
  WHERE status = 'RepairVehicleArrived_RepairVehicleArrived'
    AND CAST(fromts AS sql_date) BETWEEN '10/01/2014' AND curdate()) z
GROUP BY jobid, date_arrived

-- now which of these has multiple arr dates
SELECT jobid 
FROM (
  SELECT jobid, date_arrived
  FROM (
    SELECT jobid, CAST(fromts AS sql_date) AS date_arrived
    FROM bs.jobProgressStatuses
    WHERE status = 'RepairVehicleArrived_RepairVehicleArrived'
      AND CAST(fromts AS sql_date) BETWEEN '10/01/2014' AND curdate()) z
  GROUP BY jobid, date_arrived) x
GROUP BY jobid HAVING COUNT(*) > 1  

-- AND lets look at those 
SELECT *
-- SELECT COUNT(DISTINCT jobid) -- only 90 jobids
FROM bs.jobProgressStatuses
WHERE jobid IN (
  SELECT jobid 
  FROM (
    SELECT jobid, date_arrived
    FROM (
      SELECT jobid, CAST(fromts AS sql_date) AS date_arrived
      FROM bs.jobProgressStatuses
      WHERE status = 'RepairVehicleArrived_RepairVehicleArrived'
        AND CAST(fromts AS sql_date) BETWEEN '10/01/2014' AND curdate()) z
    GROUP BY jobid, date_arrived) x
  GROUP BY jobid HAVING COUNT(*) > 1)
AND status = 'RepairVehicleArrived_RepairVehicleArrived'  
ORDER BY jobid

-- ok, executive decision, use the first arrived fromts
SELECT jobid, min(CAST(fromts AS sql_date)) AS date_arrived
FROM bs.jobProgressStatuses
WHERE status = 'RepairVehicleArrived_RepairVehicleArrived' 
  AND CAST(fromts AS sql_date) BETWEEN '10/01/2014' AND curdate()
GROUP BY jobid 

-- now integrate with the rest 
-- 1 row per jobid
-- SELECT jobid FROM (
-- DROP TABLE #bs;
SELECT a.jobid, cast(a.jobInsertedTS as sql_date) as date_inserted, 
  cast(a.ronumber AS sql_char) AS ro, LEFT(a.customer,30) AS customer, a.createdby, a.filehandler,
  substring(a.jobtype, position('_' in jobtype) + 1, 12) as jobtype,
  b.license, b.vin, b.usedcarstocknumber, c.date_arrived
INTO #bs  
FROM bs.bsjobs a 
LEFT JOIN bs.bsvehicles b on a.jobid = b.jobid
LEFT JOIN (
  SELECT jobid, min(CAST(fromts AS sql_date)) AS date_arrived
  FROM bs.jobProgressStatuses
  WHERE status = 'RepairVehicleArrived_RepairVehicleArrived' 
    AND CAST(fromts AS sql_date) BETWEEN '10/01/2014' AND curdate()
  GROUP BY jobid) c on a.jobid = c.jobid
WHERE cast(a.jobInsertedTS as sql_date) BETWEEN '10/01/2014' AND curdate() 
-- ) x GROUP BY jobid HAVING COUNT(*) > 1

SELECT * FROM #fs

SELECT *
FROM (
  SELECT gtctl#
  FROM #fs
  GROUP BY gtctl#) a
LEFT JOIN #bs b on a.gtctl# = b.ro collate ads_default_ci



SELECT *
FROM stgArkonaGLPTRNS
WHERE gtctl# = '18033024'

SELECT *
FROM #fs
WHERE gtdesc LIKE 'ADJ%'

-- exclude gl adjustments
SELECT gtctl#, sum(gttamt) AS amt
FROM #fs
WHERE gtdesc NOT LIKE 'ADJ%'
GROUP BY gtctl#

SELECT COUNT(*) AS total_ros, SUM(amt) AS total_sales,
  SUM(CASE WHEN b.jobid IS NOT NULL THEN 1 ELSE 0 END) AS cap_ros,
  SUM(CASE WHEN b.jobid IS NOT NULL THEN amt ELSE 0 END) AS cap_sales
FROM (
  SELECT gtctl#, sum(gttamt) AS amt
  FROM #fs
  WHERE gtdesc NOT LIKE 'ADJ%'
  GROUP BY gtctl#) a
LEFT JOIN #bs b on a.gtctl# = b.ro collate ads_default_ci

-- details of the ros NOT IN bs
DROP TABLE #wtf;
SELECT ro, opendatekey, closedatekey, finalclosedatekey, rolaborsales
  roflaghours, customerkey, vehiclekey
INTO #wtf   
FROM factRepairOrder
WHERE ro IN (
  SELECT distinct gtctl#
  FROM (
    SELECT gtctl#, sum(gttamt) AS amt
    FROM #fs
    WHERE gtdesc NOT LIKE 'ADJ%'
    GROUP BY gtctl#) a
  LEFT JOIN #bs b on a.gtctl# = b.ro collate ads_default_ci
  WHERE b.ro IS NULL) 
GROUP BY ro, opendatekey, closedatekey, finalclosedatekey, rolaborsales,
  roflaghours, customerkey, vehiclekey
  
SELECT a.ro, b.thedate AS open_date, c.thedate AS close_date, d.thedate AS final_close_date, e.fullname, f.vin
FROM #wtf a
LEFT JOIN day b on a.opendatekey = b.datekey
LEFT JOIN day c on a.closedatekey = c.datekey
LEFT JOIN day d on a.finalclosedatekey = d.datekey
LEFT JOIN dimCustomer e on a.customerkey = e.customerkey  
LEFT JOIN dimVehicle f on a.vehiclekey = f.vehiclekey

-- ok, but DO any of the vins exist IN bs?
SELECT a.ro, b.thedate AS open_date, c.thedate AS close_date, 
  d.thedate AS final_close_date, e.fullname, f.vin, g.*
FROM #wtf a
LEFT JOIN day b on a.opendatekey = b.datekey
LEFT JOIN day c on a.closedatekey = c.datekey
LEFT JOIN day d on a.finalclosedatekey = d.datekey
LEFT JOIN dimCustomer e on a.customerkey = e.customerkey  
LEFT JOIN dimVehicle f on a.vehiclekey = f.vehiclekey
LEFT JOIN #bs g on f.vin = g.vin

select *
FROM bs.bsjobs
WHERE jobid IN (125101,125098)