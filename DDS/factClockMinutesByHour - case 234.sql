
-- assuming the problem IS a conflict BETWEEN future clockins, 


/*
Andrew

The time clock shows brandon johnson with with clock hours all this week.

I am guessing it is vacation time, but shows up as regular clockin-clockout time.

Could you fix that please
Thanks
jon

He is training all week, should I do it a different way?
*/

TRY 
-- so this part only deletes thru curdatE()
  DELETE 
  FROM factClockMinutesByHour
  WHERE datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate());
    
SELECT *
INTO #wtf
  FROM factClockMinutesByHour
  WHERE datekey NOT IN (
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate())
AND datekey IN (SELECT datekey FROM day WHERE thedate BETWEEN '05/01/2013' AND '05/31/2013')    

SELECT MIN(b.thedate), MAX(b.thedate)
FROM #wtf a
INNER JOIN day b ON a.datekey = b.datekey
        
--  INSERT INTO factClockMinutesByHour
  SELECT b.employeekey, c.datekey, d.TimeOfDayKey, sum(a.minutes)
INTO #wtf1  
  FROM tmpClockMinutes a
  LEFT JOIN edwEmployeeDim b ON  a.yico = b.storecode
    AND a.yiemp = b.employeenumber
    AND a.yiclkind BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  LEFT JOIN day c ON a.yiclkind = c.thedate 
  LEFT JOIN TimeOfDay d ON a.thetime = d.thetime
  WHERE a.minutes < 61
    AND a.yico IN ('ry1','ry2','ry3')
    AND b.storecode IS NOT null  -- first fix clockhours outside of empl duration
    AND (a.yiemp <> '164920' AND a.yiclkind <> '02/08/2013')
  GROUP BY b.employeekey, c.datekey, d.TimeOfDayKey;  
  
-- 
SELECT a.employeekey, a.datekey, a.timeofdaykey
FROM #wtf a
INNER JOIN #wtf1 b ON a.employeekey = b.employeekey AND a.datekey = b.datekey AND  a.timeofdaykey = b.timeofdaykey
GROUP BY a.employeekey, a.datekey, a.timeofdaykey

SELECT c.thedate, d.name, COUNT(*)
FROM #wtf a
INNER JOIN #wtf1 b ON a.employeekey = b.employeekey AND a.datekey = b.datekey AND  a.timeofdaykey = b.timeofdaykey
LEFT JOIN day c ON a.datekey = c.datekey
LEFT JOIN edwEmployeeDim d ON a.employeekey = d.employeekey
GROUP BY c.thedate, d.name
-- looks LIKE that proves the hypothesis
-- now to test it
-- short term: IF i leave brandon johnson out will the sp WORK
-- long term: only populate these tables thru the curdate()
--    OR DELETE FROM factClockMinutesByHour any future dates

-- testing against ddsDailyCut.monday brought down to local
-- bad data exists

BEGIN TRANSACTION;
TRY 
  DELETE 
--  SELECT *
  FROM factClockMinutesByHour
  WHERE datekey IN (
    SELECT datekey
    FROM day
--    WHERE thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate());
-- this works, simply DELETE FROM factClockMinutesByHour ALL rows with a date > -28,,,
    WHERE thedate >= (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()));
  INSERT INTO factClockMinutesByHour
  SELECT b.employeekey, c.datekey, d.TimeOfDayKey, sum(a.minutes)
  FROM tmpClockMinutes a
  LEFT JOIN edwEmployeeDim b ON  a.yico = b.storecode
    AND a.yiemp = b.employeenumber
    AND a.yiclkind BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  LEFT JOIN day c ON a.yiclkind = c.thedate 
  LEFT JOIN TimeOfDay d ON a.thetime = d.thetime
  WHERE a.minutes < 61
    AND a.yico IN ('ry1','ry2','ry3')
    AND b.storecode IS NOT null  -- first fix clockhours outside of empl duration
    AND (a.yiemp <> '164920' AND a.yiclkind <> '02/08/2013')
  GROUP BY b.employeekey, c.datekey, d.TimeOfDayKey;  
COMMIT WORK;
CATCH ALL
  ROLLBACK; 
  RAISE SPfctClockMinutesByHour(200, __errText); 
END TRY; // TRANSACTION   

