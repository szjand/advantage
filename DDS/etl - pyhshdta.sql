-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'integer,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'double,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'PYHSHDTA' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'PYHSHDTA'  
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'PYHSHDTA'  
AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaPYHSHDTA
-- 3. revise DDL based ON step 2, CREATE TABLE
-- this one IS too fucking much for the spreadsheet, just go thru it here
-- 2/20/12 greg says yhdref needs to be a character field
-- 
CREATE TABLE stgArkonaPYHSHDTA (
yhdco#     cichar(3),          
ypbcyy     integer,             
ypbnum     integer,             
yhdemp     cichar(7),          
yhdseq     cichar(2),          
--yhdref     integer,   
yhdref     cichar(12),          
yhdecc     integer,            
yhdeyy     integer,            
yhdemm     integer,            
yhdedd     integer,            
yhdccc     integer,            
yhdcyy     integer,            
yhdcmm     integer,            
yhdcdd     integer,            
yhdbsp     money,             
yhdtgp     money,             
yhdtga     money,             
yhcfed     money,             
yhcsse     money,             
yhcssm     money,             
yhcmde     money,             
yhcmdm     money,             
yhceic     money,             
yhcst      money,             
yhcsde     money,             
yhcsdm     money,             
yhccnt     money,             
yhccty     money,             
yhcfuc     money,             
yhcsuc     money,             
yhccmp     money,             
yhcrte     money,             
yhcrtm     money,             
yhdota     money,             
yhdded     money,             
yhcopy     money,             
yhcnet     money,             
yhctax     money,             
yhafed     money,             
yhass      money,             
yhafuc     money,             
yhast      money,             
yhacn      money,             
yhamu      money,             
yhasuc     money,             
yhacm      money,             
yhdvac     double,             
yhdhol     double,             
yhdsck     double,             
yhavac     double,             
yhahol     double,             
yhasck     double,             
yhdhrs     double,             
yhdoth     double,             
yhdahr     double,             
yhvacd     double,             
yhhold     double,             
yhsckd     double,             
yhxvac     cichar(1),          
yhxsck     cichar(1),          
yhxhol     cichar(1),          
yhxfed     cichar(1),          
yhxss      cichar(1),          
yhxsit     cichar(1),          
yhxcnt     cichar(1),          
yhxmu      cichar(1),          
yhhdte     integer,             
yhmari     cichar(1),          
yhpper     cichar(1),          
yhclas     cichar(1),          
yhexss     cichar(1),          
yhfdex     integer, --            
yhfdad     money,--             
yheeic     cichar(1),          
yhecls     cichar(1),          
yhstex     integer,--             
yhstfx     money,             
yhstad     money,             
yhwkcd     money,             
yhcwfl     cichar(1),          
yhsttx     cichar(5),          
yhcntx     cichar(5),          
yhmutx     cichar(5),          
yhstcd     cichar(5),          
yhcncd     cichar(5),          
yhmucd     cichar(5), --         
yhsaly     money,             
yhrate     money,             
yhsala     money,             
yhrata     money,--             
yhrete     cichar(1),          
yhretp     money,--             
yhrfix     money,             
ymdvyr     integer,             
ymdhyr     integer,             
ymdsyr     integer,             
ymname     cichar(25),         
ymdept     cichar(2),  --        
ymsecg     integer,--             
ymss#      integer,             
ymdist     cichar(4),          
ymar#      cichar(10),         
yhvoid     cichar(1), --         
yhvcyy     integer,             
yhvnum     integer,             
yhvseq     cichar(2),          
yhtype     cichar(1), --          
yhacty     money,             
yhasdiw    money,             
yhaotr     money,             
yhdird     cichar(1), --        
yhftap     double,             
yholcs     cichar(1),          
yhstap     double,             
yhxfut     cichar(1),          
yhxovr     cichar(1),          
yhxrte     cichar(1),          
yhxrtm     cichar(1),          
yhxsde     cichar(1),          
yhxsdm     cichar(1),          
yhxsut     cichar(1),  --        
yhcsue     double,             
yhcst2     double,             
yhccn2     double,             
yhcct2     double,             
yhfdst     cichar(1),          
yhstst     cichar(1),          
yhcnst     cichar(1),          
yhmust     cichar(1),          
yhsts2     cichar(1),          
yhcns2     cichar(1),          
yhmus2     cichar(1),          
yhstt2     cichar(5),          
yhcnt2     cichar(5),          
yhmut2     cichar(5),          
yhsdst     cichar(5),          
yhwkst     cichar(5),          
yhrpt1     cichar(5),          
yhrpt2     cichar(5),          
yheeoc     cichar(2),          
yhotex     cichar(1),          
yhhghc     cichar(1),          
yhtmp01    cichar(1),          
yhtmp02    cichar(1),          
yhtmp03    cichar(1),          
yhtmp04    cichar(1),          
yhtmp05    cichar(1),          
yhtmp06    cichar(10),         
yhtmp07    cichar(10),         
yhtmp08    double,             
yhtmp09    double) IN database;            

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PYHSHDTA';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PYHSHDTA';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPYHSHDTA';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;

ALTER TABLE stgArkonaPYHSHDTA
ADD COLUMN PayrollEndingDate date
ADD COLUMN CheckDate date

UPDATE stgArkonaPYHSHDTA
SET PayrollEndingDate = 
      CAST (
        case
          when yhdemm between 1 and 9 then '0' + trim(cast(yhdemm AS sql_char))
          else trim(cast(yhdemm AS sql_char))
        end 
        + '/' +
        case
          when yhdedd between 1 and 9 then '0' + trim(cast(yhdedd AS sql_char))
          else trim(cast(yhdedd AS sql_char))
        end 
        + '/' +
        case
          when yhdeyy between 1 and 9 then '200' + trim(cast(yhdeyy AS sql_char))
          else '20' + trim(cast(yhdeyy AS sql_char))
        END AS sql_date),
    CheckDate = 
      CAST (
        case
          when yhdcmm between 1 and 9 then '0' + trim(cast(yhdcmm AS sql_char))
          else trim(cast(yhdcmm AS sql_char))
        end 
        + '/' +
        case
          when yhdcdd between 1 and 9 then '0' + trim(cast(yhdcdd AS sql_char))
          else trim(cast(yhdedd AS sql_char))
        end 
        + '/' +
        case
          when yhdcyy between 1 and 9 then '200' + trim(cast(yhdcyy AS sql_char))
          else '20' + trim(cast(yhdcyy AS sql_char))
        END AS sql_date); 

/******************************************************************************/
/******************************************************************************/ 

/************* granularity ****************/
SELECT yhdco#, ypbcyy, ypbnum, yhdemp
FROM stgArkonaPyhshdta
GROUP BY yhdco#, ypbcyy, ypbnum, yhdemp
  HAVING COUNT(*) > 1 
  
/********* dates *****************/  
  
SELECT LEFT(column_name, 12), column_text
FROM syscolumns
WHERE table_name = 'pyhshdta'
  AND (
    column_text LIKE '%CEN%' OR
    column_text LIKE '%YEAR%' OR
    column_text LIKE '%MONTH%' OR
    column_text LIKE '%DAY%' OR
    column_text LIKE '%DATE%')
  AND column_text NOT LIKE '%HOLIDAY%'
ORDER BY ordinal_position  
    
  /**** get some normal christian dates IN this TABLE ******************/
  SELECT LEFT(column_name, 12), column_text
  FROM syscolumns
  WHERE table_name = 'PYHSHDTA'
    AND (
      column_text LIKE '%YEAR%' OR
      column_text LIKE '%MONTH%' OR
      column_text LIKE '%DAY%' OR
      column_text LIKE '%DATE%')
    AND column_text NOT LIKE '%hol%'
  
  payroll ending year/month/day:  yhdeyy/yhdemm/yhdedd
  check year/month/day: yhdcyy/yhdcmm/yhdcdd
  hire date: yhhdte don't need hiredate FROM here
  
  ALTER TABLE stgArkonaPYHSHDTA
  ADD COLUMN PayrollEndingDate date
  ADD COLUMN CheckDate date
  
  UPDATE stgArkonaPYHSHDTA
  SET PayrollEndingDate = 
        CAST (
          case
            when yhdemm between 1 and 9 then '0' + trim(cast(yhdemm AS sql_char))
            else trim(cast(yhdemm AS sql_char))
          end 
          + '/' +
          case
            when yhdedd between 1 and 9 then '0' + trim(cast(yhdedd AS sql_char))
            else trim(cast(yhdedd AS sql_char))
          end 
          + '/' +
          case
            when yhdeyy between 1 and 9 then '200' + trim(cast(yhdeyy AS sql_char))
            else '20' + trim(cast(yhdeyy AS sql_char))
          END AS sql_date),
      CheckDate = 
        CAST (
          case
            when yhdcmm between 1 and 9 then '0' + trim(cast(yhdcmm AS sql_char))
            else trim(cast(yhdcmm AS sql_char))
          end 
          + '/' +
          case
            when yhdcdd between 1 and 9 then '0' + trim(cast(yhdcdd AS sql_char))
            else trim(cast(yhdedd AS sql_char))
          end 
          + '/' +
          case
            when yhdcyy between 1 and 9 then '200' + trim(cast(yhdcyy AS sql_char))
            else '20' + trim(cast(yhdcyy AS sql_char))
          END AS sql_date); 
  /**** get some normal christian dates IN this TABLE ******************/
/*********  END dates *****************/ 

SELECT MAX(payrollendingdate)
FROM stgArkonaPYHSHDTA

/******* dist codes over time ********************/
-- can this be used for history of dist code?

select yhdco#, trim(yhdemp), trim(ymdist)
from stgArkonaPYHSHDTA
group by yhdco#, trim(yhdemp), trim(ymdist)
  having count(*) > 1
order by trim(yhdemp)


select yhdemm, yhdedd, yhdeyy,
  CAST (
    case
      when yhdemm between 1 and 9 then '0' + trim(cast(yhdemm AS sql_char))
      else trim(cast(yhdemm AS sql_char))
    end 
    + '/' +
    case
      when yhdedd between 1 and 9 then '0' + trim(cast(yhdedd AS sql_char))
      else trim(cast(yhdedd AS sql_char))
    end 
    + '/' +
    case
      when yhdeyy between 1 and 9 then '200' + trim(cast(yhdeyy AS sql_char))
      else '20' + trim(cast(yhdeyy AS sql_char))
    END AS sql_date)
from stgArkonapyhshdta
where yhdeyy = 12

-- employees with multiple distcodes
select yhdemp
from (
  select distinct yhdco#, trim(yhdemp) as yhdemp, trim(ymdist) as ymdist
  from stgArkonapyhshdta) x
  group by yhdemp
    having count(*) > 1

-- employees with mult distcodes
select distinct yhdco#, trim(yhdemp), trim(ymdist) 
from stgArkonapyhshdta
where trim(yhdemp) in (
  select yhdemp
  from (
    select distinct trim(yhdemp) as yhdemp, trim(ymdist) as ymdist
    from stgArkonapyhshdta) x
    group by yhdemp
      having count(*) > 1)
order by trim(yhdemp)

-- all data, with PayrollEndDate
select 
  cast (
    case
      when yhdemm between 1 and 9 then '0' + trim(cast(yhdemm AS sql_char))
      else trim(cast(yhdemm AS sql_char))
    end 
    + '/' +
    case
      when yhdedd between 1 and 9 then '0' + trim(cast(yhdedd AS sql_char))
      else trim(cast(yhdedd AS sql_char))
    end 
    + '/' +
    case
      when yhdeyy between 1 and 9 then '200' + trim(cast(yhdeyy AS sql_char))
      else '20' + trim(cast(yhdeyy AS sql_char))
    END AS sql_date) as PayrollEndDate,
  p.*
from stgArkonaPYHSHDTA p

-- so, i want min/max payroll end date for each store/emp#/dist where an emp# has mult dist codes

select yhdco#, trim(yhdemp) as employeenumber, trim(ymdist) as DistCode, 
  min(PayrollEndDate) as minDate, max(PayrollEndDate) as maxDate
from (
  select 
    cast (
      case
        when yhdemm between 1 and 9 then '0' + trim(cast(yhdemm AS sql_char))
        else trim(cast(yhdemm AS sql_char))
      end 
      + '/' +
      case
        when yhdedd between 1 and 9 then '0' + trim(cast(yhdedd AS sql_char))
        else trim(cast(yhdedd AS sql_char))
      end 
      + '/' +
      case
        when yhdeyy between 1 and 9 then '200' + trim(cast(yhdeyy AS sql_char))
        else '20' + trim(cast(yhdeyy AS sql_char))
      END AS sql_date) as PayrollEndDate,
    p.* 
  from stgArkonaPYHSHDTA p) x
where trim(yhdemp) in ( -- mult dist codes/empl
    select yhdemp
    from (
      select distinct yhdco#, trim(yhdemp) as yhdemp, trim(ymdist) as ymdist
      from stgArkonapyhshdta) x
      group by yhdemp
        having count(*) > 1)
group by yhdco#, trim(yhdemp), trim(ymdist)
order by employeenumber

-- populate ads.__MultipleDistCodes
select *
from (
  select yhdco#, trim(yhdemp) as employeenumber, trim(ymdist) as DistCode, 
    min(PayrollEndDate) as minDate, max(PayrollEndDate) as maxDate
  from (
    select 
      cast (
        case
          when yhdemm between 1 and 9 then '0'||trim(char(yhdemm))
          else trim(char(yhdemm))
        end 
        ||'/'||
        case
          when yhdedd between 1 and 9 then '0'||trim(char(yhdedd))
          else trim(char(yhdedd))
        end 
        ||'/'||
        case
          when yhdeyy between 1 and 9 then '200'||trim(char(yhdeyy))
          else '20'||trim(char(yhdeyy))
        end as date) as PayrollEndDate,
      p.* 
    from RYDEDATA.PYHSHDTA p
    where yhdeyy*10000 + yhdemm*100 + yhdedd > 90731) x
where trim(yhdemp) in ( -- mult dist codes/empl
    select yhdemp
    from (
      select distinct yhdco#, trim(yhdemp) as yhdemp, trim(ymdist) as ymdist
      from rydedata.pyhshdta
      where yhdeyy*10000 + yhdemm*100 + yhdedd > 90731) x
      group by yhdemp
        having count(*) > 1)
group by yhdco#, trim(yhdemp), trim(ymdist)) x
order by employeenumber, mindate 

-- but the problem is overlapping periods of min/max
select yhdco#, ypbnum, yhdemp, yhdeyy*10000 + yhdemm*100 + yhdedd, ymdist, ymdept, yhrate
from stgArkonapyhshdta
where trim (yhdemp) = '116550'
order by yhdeyy*10000 + yhdemm*100 + yhdedd

-- fact
-- 1 row per store/payrollrun, emp#/year
select yhdco#, ypbnum, yhdemp, yhdeyy
from rydedata.pyhshdta
group by yhdco#, ypbnum, yhdemp, yhdeyy
  having count(*) > 1


SELECT DISTINCT yhfdad FROM stgArkonapyhshdta

