SELECT *
FROM edwClockHoursFact


SELECT employeekey, datekey FROM (
SELECT a.storecode, a.technumber, a.employeenumber, a.name, c.*
FROM (
  SELECT storecode, technumber, employeenumber, name
  FROM dimtech 
  WHERE flagdeptcode = 'MR'
    AND storecode = 'ry1'
    AND employeenumber <> 'na'
  GROUP BY storecode, technumber, employeenumber, name) a
LEFT JOIN edwEmployeeDim b ON a.storecode = b.storecode
  AND a.employeenumber = b.employeenumber  
LEFT JOIN edwClockHoursFact c ON b.employeekey = c.employeekey
) x GROUP BY employeekey, datekey HAVING COUNT(*) > 1

-- this actually looks pretty fucking good
-- Jason Hunter (tech 610 shows up (AS capacity) AND should not
SELECT a.storecode, a.technumber, a.name, 
  SUM(regularhours) AS REG, SUM(overtimehours) AS ot, SUM(clockhours) AS TOT
FROM (
  SELECT storecode, technumber, employeenumber, name
  FROM dimtech 
  WHERE flagdeptcode = 'MR'
    AND storecode = 'ry1'
    AND employeenumber <> 'na'
  GROUP BY storecode, technumber, employeenumber, name) a
LEFT JOIN edwEmployeeDim b ON a.storecode = b.storecode
  AND a.employeenumber = b.employeenumber  
INNER JOIN edwClockHoursFact c ON b.employeekey = c.employeekey
  AND c.datekey IN (SELECT datekey FROM day WHERE thedate BETWEEN '04/01/2013' AND '04/30/2013')
GROUP BY a.storecode, a.technumber, a.name
ORDER BY a.name 


-- this actually looks pretty fucking good
-- Jason Hunter (tech 610 shows up (AS capacity) AND should NOT
-- ok, this should take care of some of it (EXISTS)
-- sitch: part of the month a person clocks time AS a tech, part of the time the person
-- clocks time AS a nontech, how DO i distinguish the tech type clockhours form the nontech type clock hours
-- jason hunter IS a good example 
--   dimtech active thru 3/16
--   payroll active tech thru 4/10 (changed distcode)
DECLARE @d1 date;
DECLARE @d2 date;
@d1 = '02/01/2013';
@d2 = '02/28/2013';
SELECT d.*, e.other, e.training
FROM ( -- clock hours
  SELECT a.storecode, a.technumber, a.name, 
    SUM(regularhours) AS REG, SUM(overtimehours) AS OT, SUM(clockhours) AS TOT
  FROM (
    SELECT storecode, technumber, employeenumber, name
    FROM dimtech a
    WHERE flagdeptcode = 'MR'
      AND storecode = 'ry1'
      AND employeenumber <> 'na'
      AND EXISTS (
        SELECT 1
        FROM dimtech
        WHERE techkey = a.techkey
          AND techkeyfromdate < @d2
          AND techkeythrudate > @d1
          AND active = true)
    GROUP BY storecode, technumber, employeenumber, name) a
  LEFT JOIN edwEmployeeDim b ON a.storecode = b.storecode
    AND a.employeenumber = b.employeenumber  
  INNER JOIN edwClockHoursFact c ON b.employeekey = c.employeekey
    AND c.datekey IN (SELECT datekey FROM day WHERE thedate BETWEEN @d1 AND @d2)
  GROUP BY a.storecode, a.technumber, a.name) d
LEFT JOIN ( -- other/training
  SELECT storecode, technumber, 
    SUM(CASE WHEN reason <> 'Training' THEN hours END) AS other,
    SUM(CASE WHEN reason = 'Training' THEN hours END) AS training
  FROM stgTechTrainingOther
  WHERE thedate BETWEEN @d1 AND @d2
  GROUP BY storecode, technumber) e ON d.storecode = e.storecode AND d.technumber = e.technumber
ORDER BY d.name 

/*
monthly totals are fucked up, even though the pay period portions are right ON
obviously the issue IS IN pay periods that span 2 months, the time IS NOT
always being apportioned to the correct month




SELECT technumber, name,
  sum(CASE WHEN yearmonth = 201212 THEN other END) AS DecO,
  sum(CASE WHEN yearmonth = 201212 THEN training END) AS DecT,
  sum(CASE WHEN yearmonth = 201301 THEN other END) AS JanO,
  sum(CASE WHEN yearmonth = 201301 THEN training END) AS JanT,
  sum(CASE WHEN yearmonth = 201302 THEN other END) AS FebO,
  sum(CASE WHEN yearmonth = 201302 THEN training END) AS FebT,
  sum(CASE WHEN yearmonth = 201303 THEN other END) AS MarO,
  sum(CASE WHEN yearmonth = 201303 THEN training END) AS MarT,
  sum(CASE WHEN yearmonth = 201304 THEN other END) AS AprO,
  sum(CASE WHEN yearmonth = 2012304 THEN training END) AS AprT      
FROM (
  SELECT z.yearmonth, b.technumber, b.name, 
    coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other,
    coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training
  FROM day z 
  LEFT JOIN  (
    SELECT technumber, employeenumber, name, min(techkeyfromdate) AS fromdate, max(techkeythrudate) AS thrudate
    FROM dimtech a
    WHERE a.flagdeptcode = 'MR'
      AND a.storecode = 'ry1'
      AND a.employeenumber <> 'na'
      AND a.active = true 
      
    GROUP BY a.technumber, a.employeenumber, a.name) b on z.thedate between b.fromdate  AND b.thrudate
  LEFT JOIN stgTechTrainingOther c ON b.technumber = c.technumber  
    AND c.thedate = z.thedate
  WHERE z.thedate BETWEEN '11/01/2012' AND curdate()  
  GROUP BY z.yearmonth, b.technumber, b.name) x    
WHERE technumber = '526'   
GROUP BY technumber, name




SELECT z.yearmonth, b.technumber, b.name, 
  coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other,
  coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training
FROM day z 
LEFT JOIN  (
  SELECT technumber, employeenumber, name, min(techkeyfromdate) AS fromdate, max(techkeythrudate) AS thrudate
  FROM dimtech a
  WHERE a.flagdeptcode = 'MR'
    AND a.storecode = 'ry1'
    AND a.employeenumber <> 'na'
    AND a.active = true 
    
  GROUP BY a.technumber, a.employeenumber, a.name) b on z.thedate between b.fromdate  AND b.thrudate
LEFT JOIN stgTechTrainingOther c ON b.technumber = c.technumber  
  AND c.thedate = z.thedate
WHERE z.thedate BETWEEN '06/01/2012' AND curdate() 
  AND b.technumber = '526' 
  
*/  