SELECT *
FROM xfmro

SELECT *
FROM (
  SELECT ptro#, ptckey, ptcnam, opendate
  FROM xfmro
  GROUP BY ptro#, ptckey, ptcnam, opendate) a
LEFT JOIN dimCustomer b ON a.ptckey = b.bnkey  
WHERE b.bnkey IS NULL 

-- VOID IS determined IN stgRO transforming SDPRHDR => xfmRO
-- SELECT LEFT(ptcnam,10), COUNT(*)
SELECT *
FROM (
  SELECT ptro#, ptckey, ptcnam, opendate
  FROM xfmro
  WHERE status <> 'VOID'
  GROUP BY ptro#, ptckey, ptcnam, opendate) a
LEFT JOIN dimCustomer b ON a.ptckey = b.bnkey  
WHERE b.bnkey IS NULL 
-- GROUP BY LEFT(ptcnam,10)

-- 2 issues:
--   1. inventory who the fuck IS the customer, what IS the row IN dimCustomer
--   2. no match IN dimCustomer

SELECT *
FROM xfmro 
WHERE LEFT(ptcnam,10) IN ('CARIGNAN,','KITSCH, ET')

SELECT * FROM stgArkonaBOPNAME WHERE bnkey IN (1034723,1055092)

SELECT bnco#,bnkey,bnsnam
FROM stgArkonaBOPNAME
GROUP BY bnco#,bnkey,bnsnam
HAVING COUNT(*) > 1

SELECT MIN(bnkey), MAX(bnkey) FROM dimCustomer
-- 2.1
--DELETE FROM dimcustomer WHERE bnkey = 1
INSERT INTO dimCustomer (StoreCode,bnkey,customertypecode,customertype,fullname,
  currentrow,customerkeyfromdate,customerkeyfromdatekey,
  customerkeythrudate,customerkeythrudatekey)
values('RY1',0,'C','Company','Inventory',true,'11/22/2011',2883,'12/31/9999',7306);
-- 2.2
INSERT INTO dimCustomer (StoreCode,bnkey,customertypecode,customertype,fullname,
  currentrow,customerkeyfromdate,customerkeyfromdatekey,
  customerkeythrudate,customerkeythrudatekey)
values('RY1',2,'X','Unknown','Unknown',true,'11/22/2011',2883,'12/31/9999',7306);


SELECT ptro#, ptck
SELECT *
FROM (
  SELECT ptro#, ptckey, ptcnam, opendate
  FROM xfmro
  WHERE status <> 'VOID'
  GROUP BY ptro#, ptckey, ptcnam, opendate) a
LEFT JOIN dimCustomer b ON a.ptckey = b.bnkey  
WHERE b.bnkey IS NULL 

-- fuck, bnkey = 0 IS a reasonable inventory indicator
SELECT LEFT(ptcnam,8), COUNT(*)
FROM stgArkonaSDPRHDR
WHERE ptckey = 0
GROUP BY LEFT(ptcnam,8)

SELECT * 
FROM stgArkonaSDPRHDR
WHERE ptckey = 0
  AND length(TRIM(ptcnam)) = 0
  
SELECT month(createdts), year(createdts), COUNT(*)
FROM factro a
LEFT JOIN dimCustomer b ON a.customerkey = b.bnkey
WHERE a.void = false
  AND b.bnkey IS NULL 
GROUP BY month(createdts), year(createdts)
  
SELECT * FROM stgArkonaBOPNAME WHERE bnkey = 1047618
  
SELECT bnkey
FROM dimCustomer 
GROUP BY bnkey HAVING COUNT(*) > 1 
         
SELECT * FROM xfmro    

SELECT *
FROM (
  SELECT ptro#, ptckey, ptcnam, opendate
  FROM xfmro
  GROUP BY ptro#, ptckey, ptcnam, opendate) a
LEFT JOIN dimCustomer b ON a.ptckey = b.bnkey  
WHERE b.bnkey IS NULL 



DROP TABLE zTest;   
CREATE TABLE zTest (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  CustomerKey integer) IN database;  
DELETE FROM ztest;  
INSERT INTO ztest
SELECT a.ptco#, a.ptro#, CAST(NULL AS sql_integer),
  coalesce(b.customerkey, (SELECT customerkey FROM dimcustomer WHERE bnkey = 2))
FROM (
  SELECT  ptco#, ptro#, ptckey
  FROM xfmro
  WHERE status <> 'VOID'
  GROUP BY ptco#, ptro#, ptckey) a
LEFT JOIN dimCustomer b ON a.ptckey = b.bnkey   
-- unknown customers
SELECT *
FROM ztest 
WHERE customerkey = (SELECT customerkey from dimcustomer WHERE bnkey = 0)
         