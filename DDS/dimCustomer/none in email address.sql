5/17 tyrrell bitching about email address XXHOLENONEXX@YAHOO.COM decoding
AS no valid email

so, refine the filter
/*
        WHEN length(TRIM(coalesce(bnemail, ''))) = 0 THEN False
    	  WHEN position('@' IN bnemail) = 0 THEN False
        WHEN position ('.' IN bnemail) = 0 THEN False
        WHEN position('wng' IN bnemail) > 0 THEN False
      	WHEN position('dnh' IN bnemail) > 0 THEN False
      	WHEN position('noemail' IN bnemail) > 0 THEN False
      	WHEN position('none@' IN bnemail) > 0 THEN False
      	WHEN position('dng' IN bnemail) > 0 THEN False
      	ELSE True
*/        
-- valid email addresses that contain 'none'

-- hmm, looks LIKE 3 false negatives
SELECT bnemail, COUNT(*), MAX(bnupdts), 'email'
FROM stgArkonaBOPNAME
WHERE position('none' IN bnemail) > 0
GROUP BY bnemail
UNION 
SELECT bneml2, COUNT(*), MAX(bnupdts), 'email2'
FROM stgArkonaBOPNAME
WHERE position('none' IN bneml2) > 0
GROUP BY bneml2
ORDER BY COUNT(*) DESC

select bnkey, bnsnam, bnemail, bneml2
FROM stgArkonaBOPNAME
WHERE position(' ' IN trim(bnemail)) > 0

so, exlude none@,instead of none, should be good enough for now 