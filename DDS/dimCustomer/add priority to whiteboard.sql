-- ADD COLUMN to TABLE --------------------------------------------------------

ALTER TABLE DetailWhiteboard 
ADD COLUMN Priority cichar(25);

--modify dwbMoveJob
IF @MoveFrom = 'reconbufferjobs' THEN 
  INSERT INTO DetailWhiteBoard (customer,vehicle,job,jobsource,location) 
  SELECT *
  FROM (
    SELECT stocknumber AS Customer, 
      CASE
        WHEN color IS NULL THEN left(TRIM(make) + ' ' + TRIM(model), 60)
        ELSE left(TRIM(color) + ' ' + TRIM(model), 60) 
      END AS Vehicle,
      coalesce(app, 'Unknown') AS Job, @MoveFrom,PhysLocation
    FROM vusedcarsreconbuffer
    WHERE status = 'AppearanceReconDispatched_Dispatched') a
  WHERE Customer = @Customer
    AND Vehicle = @Vehicle;  

EXECUTE PROCEDURE dwbGetReconBufferJobs();
-- INSERT INTO DetailWhiteboard(customer,priority,vehicle,job,jobsource,location)   
DECLARE @movefrom string;
@movefrom = 'reconbufferjobs';  
SELECT *
FROM (
  SELECT b.stocknumber AS Customer, cast(b.CurrentPriority AS sql_char) AS Priority,  
    CASE
      WHEN c.exteriorcolor IS NULL THEN left(TRIM(c.make) + ' ' + TRIM(c.model), 60)
      ELSE left(TRIM(exteriorcolor) + ' ' + TRIM(c.model), 60) 
    END AS Vehicle,
    CASE
      WHEN f.typ = 'PartyCollection_AppearanceReconItem' THEN 
        CASE
          WHEN upper(f.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
          WHEN upper(f.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
          WHEN (upper(f.description) LIKE 'DELUXE%' OR upper(f.description) LIKE 'AUCTION%') THEN 'Deluxe'
          WHEN upper(f.Description) = 'PRICING RINSE' THEN 'PR' 
          ELSE 'WTF' 
        END
      ELSE 'Other'
    END AS Detail, @MoveFrom,
    Utilities.CurrentViiLocationWithoutStore(a.VehicleInventoryItemID)    
  FROM VehicleInventoryItemStatuses a
  LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  LEFT JOIN VehicleItems c ON b.VehicleItemID = c.VehicleItemID 
  LEFT JOIN reconauthorizations d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.thruts IS NULL 
  LEFT JOIN AuthorizedReconItems e ON d.ReconAuthorizationID = e.ReconAuthorizationID   
    AND e.startts IS NULL 
  LEFT JOIN VehicleReconItems f ON e.VehicleReconItemID = f.VehicleReconItemID   
  WHERE a.status = 'AppearanceReconDispatched_Dispatched'
    AND a.thruts IS NULL 
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
      AND status = 'AppearanceReconProcess_InProcess'
      AND ThruTS IS NULL)) x
WHERE customer = '19966A'
  AND vehicle = 'Green/Blue Sonata';
  
  
ELSEIF @MoveFrom = 'rawmaterialsjobs' THEN     
  INSERT INTO DetailWhiteBoard (customer,vehicle,job,jobsource,location)  
  SELECT *
  FROM (
    SELECT stocknumber AS Customer, 
      CASE
        WHEN color IS NULL THEN left(TRIM(make) + ' ' + TRIM(model), 60)
        ELSE left(TRIM(color) + ' ' + TRIM(model), 60) 
      END AS Vehicle,
      coalesce(detail, 'Unknown') AS Job, @MoveFrom,PhysLocation 
    FROM vusedcarsAppearanceG2G a
    WHERE NOT EXISTS (
      SELECT 1
      FROM vusedcarsreconbuffer
      WHERE status = 'AppearanceReconDispatched_Dispatched'
        AND stocknumber = a.stocknumber)) b
  WHERE Customer = @Customer
    AND Vehicle = @Vehicle;     
    
EXECUTE PROCEDURE dwbGetRawMaterialsJobs();       
DECLARE @movefrom string;
@movefrom = 'rawmaterialsjobs';  
-- INSERT INTO DetailWhiteboard(customer,priority,vehicle,job,jobsource,location)     
SELECT *
FROM (
  SELECT a.stocknumber AS Customer, cast(a.CurrentPriority AS sql_char), 
    CASE
      WHEN e.exteriorcolor IS NULL THEN left(TRIM(e.make) + ' ' + TRIM(e.model), 60)
      ELSE left(TRIM(e.exteriorcolor) + ' ' + TRIM(e.model), 60) 
    END AS Vehicle,
    CASE
      WHEN h.typ = 'PartyCollection_AppearanceReconItem' THEN 
        CASE
          WHEN upper(h.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
          WHEN upper(h.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
          WHEN (upper(h.description) LIKE 'DELUXE%' OR upper(h.description) LIKE 'AUCTION%') THEN 'Deluxe'
          WHEN upper(h.Description) = 'PRICING RINSE' THEN 'PR' 
          ELSE 'WTF' 
        END
      ELSE 'Other'
    END AS Detail, @MoveFrom,
    Utilities.CurrentViiLocationWithoutStore(a.VehicleInventoryItemID)    
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND b.status = 'BodyReconProcess_NoIncompleteReconItems'
    AND b.thruts IS NULL 
  INNER JOIN VehicleInventoryItemStatuses c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.status = 'MechanicalReconProcess_NoIncompleteReconItems'
    AND c.thruts IS NULL   
  INNER JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.status = 'AppearanceReconProcess_NotStarted'
      AND d.thruts IS NULL  
  LEFT JOIN VehicleItems e ON a.VehicleItemID = e.VehicleItemID 
  LEFT JOIN reconauthorizations f ON a.VehicleInventoryItemID = f.VehicleInventoryItemID 
    AND f.thruts IS NULL 
  LEFT JOIN AuthorizedReconItems g ON f.ReconAuthorizationID = g.ReconAuthorizationID   
    AND g.startts IS NULL 
  LEFT JOIN VehicleReconItems h ON g.VehicleReconItemID = h.VehicleReconItemID         
  WHERE a.thruts IS NULL    
    AND NOT EXISTS (
      SELECT 1 
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
        AND status = 'AppearanceReconDispatched_Dispatched'
        AND ThruTS IS NULL)) i  
WHERE Customer = @Customer
  AND Vehicle = @Vehicle;          
    
ELSEIF @MoveFrom = 'customerpayjobs' THEN   
  INSERT INTO DetailWhiteBoard (customer,vehicle,job,jobsource,location) 
  SELECT *
  FROM (  
    SELECT customer, 
      CASE 
        WHEN color = '' THEN TRIM(make) + ' ' + TRIM(model)
        ELSE TRIM(color) + ' ' + TRIM(model)
      END AS vehicle,
      coalesce(JobDescription,'Unknown'), @MoveFrom, 'Customer Pay' 
    FROM dds.SchedulerDetailAppointments) a  
  WHERE Customer = @Customer
    AND Vehicle = @Vehicle;     
    
EXECUTE PROCEDURE dwbGetCustomerPayJobs();       
DECLARE @movefrom string;
@movefrom = 'rawmaterialsjobs';  
-- INSERT INTO DetailWhiteboard(customer,priority,vehicle,job,jobsource,location)     
  SELECT *
  FROM (  
    SELECT customer, 'CP', 
      CASE 
        WHEN color = '' THEN TRIM(make) + ' ' + TRIM(model)
        ELSE TRIM(color) + ' ' + TRIM(model)
      END AS vehicle,
      coalesce(JobDescription,'Unknown'), @MoveFrom, 'Customer Pay' 
    FROM dds.SchedulerDetailAppointments) a  
WHERE Customer = 'RICH, BOB'
  AND Vehicle = 'LIGHT BROWN MUSTANG';   
    
    
EXECUTE PROCEDURE dwbGetOtherJobs();       
DECLARE @movefrom string;
@movefrom = 'otherjobs';  
-- INSERT INTO DetailWhiteboard(customer,priority,vehicle,job,jobsource,location)       
SELECT *
FROM (
  SELECT a.stocknumber AS Customer, cast(a.CurrentPriority AS sql_char), 
    CASE
      WHEN e.exteriorcolor IS NULL THEN left(TRIM(e.make) + ' ' + TRIM(e.model), 60)
      ELSE left(TRIM(e.exteriorcolor) + ' ' + TRIM(e.model), 60) 
    END AS Vehicle,
    CASE
      WHEN h.typ = 'PartyCollection_AppearanceReconItem' THEN 
        CASE
          WHEN upper(h.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
          WHEN upper(h.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
          WHEN (upper(h.description) LIKE 'DELUXE%' OR upper(h.description) LIKE 'AUCTION%') THEN 'Deluxe'
          WHEN upper(h.Description) = 'PRICING RINSE' THEN 'PR' 
          ELSE 'WTF' 
        END
      ELSE 'Other'
    END AS Detail, @MoveFrom, 
    Utilities.CurrentViiLocationWithoutStore(a.VehicleInventoryItemID)    
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.status = 'AppearanceReconProcess_NotStarted'
      AND d.thruts IS NULL  
  INNER JOIN VehicleItems e ON a.VehicleItemID = e.VehicleItemID 
  INNER JOIN reconauthorizations f ON a.VehicleInventoryItemID = f.VehicleInventoryItemID 
    AND f.thruts IS NULL 
  INNER JOIN AuthorizedReconItems g ON f.ReconAuthorizationID = g.ReconAuthorizationID   
    AND g.startts IS NULL 
  INNER JOIN VehicleReconItems h ON g.VehicleReconItemID = h.VehicleReconItemID   
    AND h.typ = 'PartyCollection_AppearanceReconItem'     
  WHERE a.thruts IS NULL 
    AND currentpriority = 0) i  
WHERE Customer = '19749A'
  AND Vehicle = 'white G3500 Vans';    