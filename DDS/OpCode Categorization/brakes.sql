
-- what are the opcodes
SELECT * FROM dimopcode WHERE description LIKE '%rak%'    
-- what are the CorCodeGroupKeys for these opcodes
SELECT c.*
FROM dimopcode a 
INNER JOIN brCorCodeGroup b on a.opcodekey = b.opcodekey
INNER JOIN dimCorCodeGroup c on b.corCodeGroupKey = c.CorCodeGroupKey
WHERE description LIKE '%rak%'

-- ro lines on which the above CorCodeGroupKeys exist
SELECT *
FROM factRepairOrder a
WHERE opendatekey IN (SELECT datekey FROM day WHERE theyear = 2013)
  AND corCodeGroupKey IN (
    SELECT c.corcodeGroupKey
    FROM dimopcode a 
    INNER JOIN brCorCodeGroup b on a.opcodekey = b.opcodekey
    INNER JOIN dimCorCodeGroup c on b.corCodeGroupKey = c.CorCodeGroupKey
    WHERE description LIKE '%rak%')  
ORDER BY ro, line  

-- how many times was each opcode performed since the beginning of 2013
SELECT count(*), max(e.thedate) AS LastPerformed, /*min(a.ro), max(a.ro),*/ a.storecode, d.opcode, d.description
FROM factRepairOrder a
INNER JOIN dimCorCodeGroup b on a.CorCodeGroupKey = b.CorCodeGroupKey
INNER JOIN brCorCodeGroup c on b.CorCodeGroupKey = c.CorCodeGroupKey
INNER JOIN dimOpcode d on c.OpcodeKey = d.OpcodeKey
  AND d.description LIKE '%rak%'
INNER JOIN day e on a.opendatekey = e.datekey  
WHERE opendatekey IN (SELECT datekey FROM day WHERE theyear = 2013)
GROUP BY a.storecode, d.opcode, d.description
ORDER BY COUNT(*) desc


-- AND combine the 2
SELECT a.storecode, coalesce(b.howmany, 0), a.opcode, a.description 
FROM dimopcode a
LEFT JOIN (
  SELECT count(*) AS howmany, max(e.thedate) AS LastPerformed, /*min(a.ro), max(a.ro),*/ a.storecode, d.opcode, d.description
  FROM factRepairOrder a
  INNER JOIN dimCorCodeGroup b on a.CorCodeGroupKey = b.CorCodeGroupKey
  INNER JOIN brCorCodeGroup c on b.CorCodeGroupKey = c.CorCodeGroupKey
  INNER JOIN dimOpcode d on c.OpcodeKey = d.OpcodeKey
    AND d.description LIKE '%rak%'
  INNER JOIN day e on a.opendatekey = e.datekey  
  WHERE opendatekey IN (SELECT datekey FROM day WHERE theyear = 2013)
  GROUP BY a.storecode, d.opcode, d.description) b on a.storecode = b.storecode AND a.opcode = b.opcode
WHERE a.description LIKE '%rak%' 
ORDER BY b.howmany desc
