-- general ledger
SELECT company_number AS "GL CO", department_code AS "GL DEPT CODE", dept_description AS "GL DESC"
FROM stgArkonaGLPDEPT
WHERE company_number IN ('ry1','ry2')
ORDER BY company_number, dept_description


SELECT yicco# AS "PY CO", yicdept AS "PY DEPT CODE" , yicdesc AS "PY DESC"
FROM stgArkonaPYPCLKCTL
WHERE yicco# IN ('ry1','ry2')
ORDER BY yicco#, yicdept


-- spreadsheets for ben foster
SELECT a.storecode, a.name, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, a.payrollclass, a.fullparttime, b.yrjobd, c.yrtext
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
  AND b.yrjobd <> '' -- takes care of dups
LEFT JOIN ( -- takes care of dups
  select yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD 
  WHERE yrtext <> ''
  group by yrco#, yrjobd, yrtext) c on a.storecode = c.yrco# AND b.yrjobd = c.yrjobd
WHERE a.currentrow = true
  AND a.Active = 'Active'
ORDER BY storecode, pydeptcode, b.yrjobd  
ORDER BY storecode, name  


SELECT a.storecode, a.pydeptcode, a.pydept, b.yrjobd, c.yrtext, COUNT(*)
FROM edwEmployeeDim a
LEFT JOIN stgArkonaPYPRHEAD b on a.storecode = b.yrco# AND a.employeenumber = b.yrempn
  AND b.yrjobd <> '' -- takes care of dups
LEFT JOIN ( -- takes care of dups
  select yrco#, yrjobd, yrtext
  FROM stgArkonaPYPRJOBD 
  WHERE yrtext <> ''
  group by yrco#, yrjobd, yrtext) c on a.storecode = c.yrco# AND b.yrjobd = c.yrjobd
WHERE a.currentrow = true
  AND a.Active = 'Active'
  AND a.storecode = 'ry1' 
GROUP BY a.storecode, a.pydeptcode, a.pydept, b.yrjobd, c.yrtext  
ORDER BY pydeptcode, yrtext



