SELECT COUNT(*) FROM xfmro

EXECUTE PROCEDURE sp_zaptable('xfmro')
INSERT INTO xfmro
SELECT sh.ptco#, sh.ptro#, sh.ptfran, ptcnam, ptckey, ptvin, sd.ptline, sd.ptseq#, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptsvctyp, sd.ptlpym,
  CASE WHEN ptltyp = 'A' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS LineDate, 
  CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN sd.ptdate ELSE cast('12/31/9999' AS sql_date) END AS FlagDate,
  sh.ptdate AS OpenDate, sh.ptcdat AS CloseDate, sh.ptfcdt AS FinalCloseDate,
  sd.ptlamt, sd.ptlopc, sd.ptcrlo, 'SDET' AS source, sh.ptswid, 
  CASE  
    when ptcnam = '*VOIDED REPAIR ORDER*' then 'VOID'
    when ptchk# like 'v%' then 'VOID'
    else 'Closed'
  END AS status
FROM stgArkonaSDPRHDR sh  
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
  AND sd.ptlhrs >= 0 -- 8/13
WHERE length(trim(sh.ptro#)) > 6  -- exclude conversion data
  AND sh.ptdate > '07/25/2009'; -- exclude conversion data    
  
INSERT INTO xfmRO
SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, ptcnam, ptckey, ptvin, pd.ptline, pd.ptseq#, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, 
  min(CASE WHEN ptltyp = 'A' THEN pd.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS LineDate, 
  min(CASE WHEN ptltyp = 'L' AND ptcode = 'TT' THEN pd.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS FlagDate,
  max(ph.ptdate) AS OpenDate, '12/31/9999' AS CloseDate, '12/31/9999' AS FinalCloseDate,
  0 as ptlamt, pd.ptlopc,
  pd.ptcrlo, 'PDET' AS source, ph.ptswid, ph.ptrsts
FROM stgArkonaPDPPHDR ph
LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
      WHERE ph.ptdtyp = 'RO' 
        AND ph.ptdoc# <> ''
        AND ph.ptco# IN ('RY1','RY2','RY3')
        AND pd.ptdate <> '12/31/9999' 
        AND pd.ptlhrs >= 0 --8/13
  AND NOT EXISTS (
    SELECT 1
    FROM xfmRO
    WHERE ptco# = ph.ptco#
      AND ptro# = ph.ptdoc#
      AND ptline = pd.ptline)
GROUP BY ph.ptco#, ph.ptdoc#, ph.ptfran,  ptcnam, ptckey, ptvin,pd. ptline, pd.ptseq#, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptlopc, pd.ptcrlo, ph.ptswid, ph.ptrsts;  

    
UPDATE xfmro
SET pttech = '506'
WHERE pttech = 'm17'; 
UPDATE xfmro
SET pttech = '22'
WHERE pttech = '21';
UPDATE xfmro
SET pttech = '251'
WHERE pttech = '521';
UPDATE xfmro
SET pttech = '17'
WHERE pttech = '28';  
UPDATE xfmro
SET pttech = '599'
WHERE pttech = '628'; 

-- koehlers 536 hr line
UPDATE xfmro
SET ptlhrs = 0
WHERE ptro# = '16083960'
  AND ptline = 3;     


EXECUTE PROCEDURE sp_zaptable('factTechFlagHoursByDay');
INSERT INTO factTechFlagHoursByDay -- INSERT 2048 
SELECT c.techkey, coalesce(d.EmployeeKey, -1), b.datekey, round(sum(ptlhrs), 2) AS ptlhrs
FROM xfmRO a
LEFT JOIN day b ON a.flagdate = b.thedate
LEFT JOIN dimTech c ON a.ptco# = c.storecode
  AND a.pttech = c.technumber
LEFT JOIN edwEmployeeDim d ON c.employeenumber = d.employeenumber
  AND b.thedate BETWEEN d.EmployeeKeyFromDate AND d.EmployeeKeyThruDate  
WHERE a.ptltyp = 'L'
  AND a.ptcode = 'TT'
  AND a.ptlhrs > 0
  AND a.flagdate IS NOT NULL -- some conversion stragglers
  AND a.status <> 'VOID'   
GROUP BY c.techkey, coalesce(d.EmployeeKey, -1), b.datekey;


EXECUTE PROCEDURE sp_zaptable('factTechLineFlagDateHours');
INSERT INTO factTechLineFlagDateHours 

SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = a.flagdate),
  TechGroupKey, TechLineFlagDateHours
FROM ( -- this gives me line/tech/flagdate w/hours
  SELECT ptco#, ptro#, ptline, pttech, flagdate, SUM(ptlhrs) AS TechLineFlagDateHours
  FROM xfmro
    WHERE ptltyp = 'l'
    AND ptcode = 'tt'
    AND ptlhrs <> 0
    AND flagdate IS NOT NULL -- some conversion stragglers
    AND status <> 'VOID'
  GROUP BY ptco#, ptro#, ptline, pttech, flagdate) a
LEFT JOIN FactROLine b ON a.ptco# = b.storecode
  AND a.ptro# = b.ro
  AND a.ptline = b.line
  AND b.LineFlagHours > 0
LEFT JOIN dimtech c ON a.ptco# = c.storecode
  and a.pttech = c.technumber
LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
  AND d.weightfactor = round(a.TechLineFlagDateHours/b.LineFlagHours, 2) 
WHERE a.TechLineFlagDateHours > 0
  AND b.LineFlagHours > 0
  AND ptro# NOT IN ('16040650','16084596','18014546')
  
  
EXECUTE PROCEDURE sp_zaptable('factROLine');
INSERT INTO factROLine
SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = linedate) AS linedatekey,
  ServType, PayType, OpCode, coalesce(CorCode, 'N/A') AS CorCode, LineHours
FROM (
  SELECT ptco#, ptro#, ptline,
    max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
    MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
    MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
    SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours
  FROM xfmRO
  WHERE status <> 'VOID'
  GROUP BY ptco#, ptro#, ptline) a
WHERE linedate IS NOT NULL
  AND servtype IS NOT NULL 
  AND paytype IS NOT NULL
  AND opcode IS NOT NULL;   
 

-- DROP TABLE #flagday
SELECT a.thedate, b.TechKey, SUM(b.flaghours) AS flagday
INTO #FlagDay
FROM day a
INNER JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey  
GROUP BY thedate, b.TechKey;
-- DROP TABLE #flagline
SELECT d.thedate, c.techkey, SUM(a.flaghours) AS flagline
INTO #FlagLine
FROM factTechLineFlagDateHours a
LEFT JOIN bridgeTechGroup b ON a.techgroupkey = b.techgroupkey
LEFT JOIN dimtech c ON b.techkey = c.techkey
LEFT JOIN day d ON a.flagdatekey = d.datekey
GROUP BY d.thedate, c.techkey;

-- fuck, why don't these agree
-- a chunk of it IS negative flag hours
-- 8/13, took care of the neg flag hours, but still beacoup conflicts
-- ON mcveigh, 6/29, it just looks LIKE the data IN factTechFlagHoursByDay IS wrong, 
-- DO another reconstruct
-- done, there are still discrepancies but NOT AS many
-- some of the discrepancies come FROM missing lines IN factroline
-- repopulated factroline
-- how's it look now
-- got rid of a few
SELECT a.*, b.*, round(round(flagday, 2) - round(flagline,2), 2)
FROM #flagday a
full JOIN #flagline b ON a.thedate = b.thedate
  AND a.techkey = b.techkey
WHERE round(flagday, 2) <> round(flagline, 2)  
ORDER BY round(round(flagday, 2) - round(flagline,2), 2) desc

SELECT * -- <> 262, = 46529
SELECT COUNT(*)
FROM #flagday a
full JOIN #flagline b ON a.thedate = b.thedate
  AND a.techkey = b.techkey
WHERE round(flagday, 2) <> round(flagline, 2)  

-- line correct?
-- 10.2
-- SELECT sum(ptlhrs)/*ptro#, ptline, ptdate, ptlhrs, pttech*/ FROM stgArkonaSDPRDET WHERE ptdate = '06/21/2012' AND pttech = '545' ORDER BY ptro#, ptline
tech 503 fitzsimonds, techkey 78 ON 11/08/2010
flag: 159.8  day: 29.3
SELECT * FROM xfmro WHERE flagdate = '11/08/2010' AND pttech = '503'
SELECT * FROM factroline WHERE ro = '16040650' -- , excl from line, 130 hours for pdi, fixed to 1.3

tech 234 rose, techkey 40 ON 02/14/2012
flag: 26.3  day: 2.2
SELECT * FROM xfmro WHERE flagdate = '02/14/2012' AND pttech = '234'
SELECT * FROM factroline WHERE ro = '18014546' -- this ro excluded FROM line

tech 646 mavity, techkey 102 ON 02/14/2012
flag: 19.7  day: 8.5
SELECT * FROM xfmro WHERE flagdate = '02/14/2012' AND pttech = '646'
SELECT * FROM factroline WHERE ro = '18014546' -- this ro excluded FROM line


the rest (~200) are ALL old AND < 1

GOOD ENOUGH




SELECT SUM(ptlhrs) FROM xfmro WHERE flagdate = '08/09/2012' AND pttech = '545'
-- .37
SELECT * FROM #flagday WHERE thedate = '06/29/2012' AND techkey = 105
-- 4.98
SELECT * FROM #flagline WHERE thedate = '06/29/2012' AND techkey = 105

SELECT * FROM factTechFlagHoursByDay WHERE techkey = 105 AND flagdatekey = (SELECT datekey FROM day WHERE thedate = '06/29/2012')

SELECT * FROM factroline
  
-- 4.98 , this generates TechFlagHoursByDay
      SELECT c.techkey, coalesce(d.EmployeeKey, -1), b.datekey, round(sum(ptlhrs), 2) AS ptlhrs
      SELECT SUM(ptlhrs)
      SELECT *
      FROM stgArkonaSDPRDET a
      LEFT JOIN day b ON a.ptdate = b.thedate
      LEFT JOIN dimTech c ON a.ptco# = c.storecode
        AND a.pttech = c.technumber
      LEFT JOIN edwEmployeeDim d ON c.employeenumber = d.employeenumber
        AND b.thedate BETWEEN d.EmployeeKeyFromDate AND d.EmployeeKeyThruDate  
      WHERE a.ptltyp = 'L'
        AND a.ptcode = 'TT'
        AND a.ptlhrs > 0
--        AND a.ptdate IS NOT NULL -- some conversion stragglers
--        AND a.status <> 'VOID' 
        AND a.ptdate = '06/21/2012'
        AND a.pttech = '545'  
      GROUP BY c.techkey, coalesce(d.EmployeeKey, -1), b.datekey;
      
--  factTechLineFlagDateHours   
      SELECT ptco#, ptro#, ptline,
        (SELECT datekey FROM day WHERE thedate = a.ptdate),
        TechGroupKey, TechLineFlagDateHours
--
      SELECT SUM(techlineflagdatehours)
--      SELECT *
      FROM ( -- this gives me line/tech/flagdate w/hours
        SELECT ptco#, ptro#, ptline, pttech, ptdate, SUM(ptlhrs) AS TechLineFlagDateHours
        FROM stgArkonaSDPRDET
          WHERE ptltyp = 'l'
          AND ptcode = 'tt'
--          AND ptlhrs <> 0
--          AND flagdate IS NOT NULL -- some conversion stragglers
--          AND status <> 'VOID'
          AND ptdate = '06/21/2012'
          AND pttech = '545'
        GROUP BY ptco#, ptro#, ptline, pttech, ptdate) a
      LEFT JOIN FactROLine b ON a.ptco# = b.storecode
        AND a.ptro# = b.ro
        AND a.ptline = b.line
        AND b.LineFlagHours > 0
      LEFT JOIN dimtech c ON a.ptco# = c.storecode
        and a.pttech = c.technumber
      LEFT JOIN bridgeTechGroup d ON c.techkey = d.techkey
        AND d.weightfactor = round(a.TechLineFlagDateHours/b.LineFlagHours, 2) 
      WHERE a.TechLineFlagDateHours > 0
        AND b.LineFlagHours > 0;       
      
SELECT *
FROM factroline 
WHERE ro = '16090851'   

select *
FROM xfmro
WHERE ptro# =  '16095009'   

select *
FROM xfmro
WHERE ptlhrs < 0
ORDER BY flagdate DESC

SELECT *
FROM stgArkonaSDPRDET
WHERE ptlhrs < 0
ORDER BY ptdate desc