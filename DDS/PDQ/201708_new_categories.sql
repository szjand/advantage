SELECT storecode, opcode, LEFT(description, 50), pdqcat1,pdqcat2,pdqcat3 
FROM dimopcode 
WHERE (pdqCat1 <> 'N/A' OR pdqCat2 <> 'N/A' OR pdqCat3 <> 'N/A')
  AND storecode = 'ry1'
ORDER BY opcode

UPDATE dimopcode
SET pdqcat1 = 'N/A',
    pdqcat2 = 'N/A',
	pdqcat3 = 'N/A'
WHERE (pdqCat1 <> 'N/A' OR pdqCat2 <> 'N/A' OR pdqCat3 <> 'N/A')
  AND storecode = 'ry1'	
  
SELECT *
FROM dimopcode
WHERE storecode = 'ry1'
  AND pdqcat1 <> 'N/A'  
  
 
SELECT b.thedate AS final_close_date, a.ro, d.name as writer, c.opcode, LEFT(c.description, 50) AS description, 
  c.pdqcat1 AS opcode_category, rocreatedts AS ro_created
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
  AND pdqcat1 <> 'N/A'
INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey  
WHERE a.storecode = 'ry1'
  AND b.thedate = '08/01/2017'
--  AND ro IN ('19280992','19281005','19281008','19281013','19281033')
group by b.thedate, a.ro, d.name, c.opcode, LEFT(c.description, 50), 
  c.pdqcat1, rocreatedts 
ORDER BY opcode


-- 8/3 
spec: running 30 days, COUNT of opcodes, limit service types to QS, QL

SELECT b.thedate, c.opcode, COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
  AND pdqcat1 <> 'N/A'
INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey  
INNER JOIN dimservicetype e on a.servicetypekey = e.servicetypekey
  AND e.servicetypecode IN ('QS','QL')
WHERE a.storecode = 'ry1'
  AND b.thedate = '08/01/2017'
GROUP BY b.thedate, c.opcode

SELECT a.opcode, x.*
FROM dimopcode a
LEFT JOIN (
  SELECT b.thedate, c.opcode, COUNT(*)
  FROM factrepairorder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
    AND pdqcat1 <> 'N/A'
  INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey  
  INNER JOIN dimservicetype e on a.servicetypekey = e.servicetypekey
    AND e.servicetypecode IN ('QS','QL')
  WHERE a.storecode = 'ry1'
    AND b.thedate BETWEEN curdate() - 4 AND curdate() - 1
  GROUP BY b.thedate, c.opcode) x on a.opcode = x.opcode
WHERE a.pdqcat1 <> 'N/A'  
ORDER BY a.opcode

SELECT * FROM day

SELECT a.opcode, x.*
FROM dimopcode a
LEFT JOIN (
  SELECT c.opcode, 
    SUM(CASE WHEN thedate = curdate() - 4 THEN 1 ELSE 0 END) AS "-4",
    SUM(CASE WHEN thedate = curdate() - 3 THEN 1 ELSE 0 END) AS "-3",
    SUM(CASE WHEN thedate = curdate() - 2 THEN 1 ELSE 0 END) AS "-2",
    SUM(CASE WHEN thedate = curdate() - 1 THEN 1 ELSE 0 END) AS "-1"    
  FROM factrepairorder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
    AND b.thedate BETWEEN curdate() - 4 AND curdate() - 1
  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
    AND pdqcat1 <> 'N/A'
  INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey  
  INNER JOIN dimservicetype e on a.servicetypekey = e.servicetypekey
  WHERE a.storecode = 'ry1'
    AND e.servicetypecode IN ('QS','QL')
  GROUP BY c.opcode) x on a.opcode = x.opcode
WHERE a.pdqcat1 <> 'N/A'
ORDER BY a.opcode
  
-- looks good, send to andrew AND nick for feedback  
SELECT *
FROM (
SELECT '0pcode' as opcode FROM system.iota) z
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 7) b on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 6) c on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 5) d on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 4) e on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 3) f on 1 = 1
LEFT JOIN ( 
SELECT mmmdd FROM day WHERE thedate = curdate() - 2) g on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 1) h on 1 = 1  
UNION 
SELECT a.opcode, "g","f","e","d","c","b","a"
FROM dimopcode a
LEFT JOIN (
  SELECT c.opcode,
    cast(SUM(CASE WHEN thedate = curdate() - 7 THEN 1 ELSE 0 END) AS sql_char) AS "g",
    cast(SUM(CASE WHEN thedate = curdate() - 6 THEN 1 ELSE 0 END) AS sql_char) AS "f",
    cast(SUM(CASE WHEN thedate = curdate() - 5 THEN 1 ELSE 0 END) AS sql_char) AS "e",  
    cast(SUM(CASE WHEN thedate = curdate() - 4 THEN 1 ELSE 0 END) AS sql_char) AS "d",
    cast(SUM(CASE WHEN thedate = curdate() - 3 THEN 1 ELSE 0 END) AS sql_char) AS "c",
    cast(SUM(CASE WHEN thedate = curdate() - 2 THEN 1 ELSE 0 END) AS sql_char) AS "b",
    cast(SUM(CASE WHEN thedate = curdate() - 1 THEN 1 ELSE 0 END) AS sql_char) AS "a"    
  FROM factrepairorder a
  INNER JOIN day b on a.closedatekey = b.datekey
    AND b.thedate BETWEEN curdate() - 7 AND curdate() - 1
  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
    AND pdqcat1 <> 'N/A'
  INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey  
  INNER JOIN dimservicetype e on a.servicetypekey = e.servicetypekey
  WHERE a.storecode = 'ry1'
    AND e.servicetypecode IN ('QS','QL')
  GROUP BY c.opcode) x on a.opcode = x.opcode
WHERE a.pdqcat1 <> 'N/A'
ORDER BY opcode


-- Nick: ADD month to date

  SELECT c.opcode, COUNT(*) AS mtd
  FROM factrepairorder a
  INNER JOIN day b on a.closedatekey = b.datekey
    AND b.MonthOfYear = month(curdate()) 
    AND b.TheYear = year(curdate())
  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
    AND pdqcat1 <> 'N/A'
  INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey  
  INNER JOIN dimservicetype e on a.servicetypekey = e.servicetypekey
  WHERE a.storecode = 'ry1'
    AND e.servicetypecode IN ('QS','QL')
  GROUP BY c.opcode

SELECT *
FROM (
SELECT '0pcode' as opcode FROM system.iota) z
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 7) b on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 6) c on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 5) d on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 4) e on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 3) f on 1 = 1
LEFT JOIN ( 
SELECT mmmdd FROM day WHERE thedate = curdate() - 2) g on 1 = 1
LEFT JOIN (
SELECT mmmdd FROM day WHERE thedate = curdate() - 1) h on 1 = 1  
LEFT JOIN (
  SELECT 'MTD' FROM system.iota) i on 1 = 1
UNION 
SELECT y.*, z.mtd
FROM (
  SELECT a.opcode, "g","f","e","d","c","b","a"
  FROM dimopcode a
  LEFT JOIN (
    SELECT c.opcode,
      cast(SUM(CASE WHEN thedate = curdate() - 7 THEN 1 ELSE 0 END) AS sql_char) AS "g",
      cast(SUM(CASE WHEN thedate = curdate() - 6 THEN 1 ELSE 0 END) AS sql_char) AS "f",
      cast(SUM(CASE WHEN thedate = curdate() - 5 THEN 1 ELSE 0 END) AS sql_char) AS "e",  
      cast(SUM(CASE WHEN thedate = curdate() - 4 THEN 1 ELSE 0 END) AS sql_char) AS "d",
      cast(SUM(CASE WHEN thedate = curdate() - 3 THEN 1 ELSE 0 END) AS sql_char) AS "c",
      cast(SUM(CASE WHEN thedate = curdate() - 2 THEN 1 ELSE 0 END) AS sql_char) AS "b",
      cast(SUM(CASE WHEN thedate = curdate() - 1 THEN 1 ELSE 0 END) AS sql_char) AS "a"    
    FROM factrepairorder a
    INNER JOIN day b on a.closedatekey = b.datekey
      AND b.thedate BETWEEN curdate() - 7 AND curdate() - 1
    INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
      AND pdqcat1 <> 'N/A'
    INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey  
    INNER JOIN dimservicetype e on a.servicetypekey = e.servicetypekey
    WHERE a.storecode = 'ry1'
      AND e.servicetypecode IN ('QS','QL')
    GROUP BY c.opcode) x on a.opcode = x.opcode
  WHERE a.pdqcat1 <> 'N/A') y
LEFT JOIN (
  SELECT c.opcode, cast(COUNT(*) AS sql_char) AS mtd
  FROM factrepairorder a
  INNER JOIN day b on a.closedatekey = b.datekey
    AND b.MonthOfYear = month(curdate()) 
    AND b.TheYear = year(curdate())
  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
    AND pdqcat1 <> 'N/A'
  INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey  
  INNER JOIN dimservicetype e on a.servicetypekey = e.servicetypekey
  WHERE a.storecode = 'ry1'
    AND e.servicetypecode IN ('QS','QL')
  GROUP BY c.opcode) z on y.opcode = z.opcode
ORDER BY opcode

-----------------------------------------------------------------------------------------------------------------
SELECT b.thedate AS final_close_date, a.ro, d.name as writer, c.opcode, LEFT(c.description, 50) AS description, 
  c.pdqcat1 AS opcode_category, rocreatedts AS ro_created
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
  AND pdqcat1 <> 'N/A'
INNER JOIN dimservicewriter d on a.servicewriterkey = d.servicewriterkey  
WHERE a.storecode = 'ry1'
  AND b.thedate = '07/31/2017'
  AND opcode = 'pdqd0208'
ORDER BY opcode


SELECT * FROM tmpsdprhdr WHERE ptro# = '19280648'