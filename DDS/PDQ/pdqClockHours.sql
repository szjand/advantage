SELECT storecode, distcode, distribution, COUNT(*)
FROM edwEmployeeDim 
WHERE pydept = 'pdq'
GROUP BY storecode, distcode, distribution



SELECT storecode, name, distcode, distribution
FROM edwEmployeeDim 
WHERE pydept = 'pdq'
  AND active = 'active'
  AND currentrow = true
ORDER BY storecode, distcode, name 


SELECT c.storecode, c.name, c.distcode, c.distribution,
  b.thedate, a.regularhours, a.overtimehours, a.vacationhours, a.ptohours, a.holidayhours
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
WHERE b.theyear IN (2012, 2013)
  AND a.clockhours + a.vacationhours + a.ptohours + a.holidayhours > 0
  AND c.pydept = 'pdq'
  
SELECT c.storecode, c.name, c.distcode, c.distribution,
  b.thedate, a.regularhours, a.overtimehours, a.vacationhours, a.ptohours, a.holidayhours
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
WHERE b.theyear IN (2012, 2013)
  AND a.clockhours + a.vacationhours + a.ptohours + a.holidayhours > 0
  AND c.pydept = 'pdq'  
  
  
SELECT c.storecode, b.thedate, c.distcode, 
  sum(a.regularhours) AS regHours, sum(a.overtimehours) AS otHours, 
  sum(a.vacationhours) AS vacHours, sum(a.ptohours) AS ptoHours
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
WHERE b.theyear IN (2012, 2013)
  AND a.clockhours + a.vacationhours + a.ptohours + a.holidayhours > 0
  AND c.pydept = 'pdq'    
GROUP BY c.storecode, b.thedate, c.distcode  