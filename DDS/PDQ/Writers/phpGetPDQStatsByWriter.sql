ALTER PROCEDURE phpGetPDQStatsByWriter
   ( 
--      StoreCode CICHAR ( 4 ),
      TheType CICHAR ( 10 ),
      SortCol DATE OUTPUT,
      TheDate CICHAR ( 20 ) OUTPUT,
      WriterID CICHAR ( 30 ) OUTPUT,
      OilChanges Integer OUTPUT,
      BOC Integer OUTPUT,
      AirFilterPenetration Integer OUTPUT,
      RotatePenetration Integer OUTPUT,
      TirePenetration Integer OUTPUT
   ) 
/*
EXECUTE PROCEDURE phpGetPDQStatsByWriter('RY1','DAILY');
execute procedure phpGetPDQStatsByWriter('Daily')
select * from(execute procedure phpGetPDQStatsByWriter('Daily')) a order by TheDate desc
*/   
BEGIN 
  declare #StoreCode string;
  declare #TheType cichar(15);
  
  #StoreCode = 'RY1';--upper((select StoreCode from __Input));
  #TheType = upper((select TheType from __Input));
  
  try
    drop table #phpGetPDQStatsByWriter;
    catch ADS_SCRIPT_EXCEPTION
    if __ErrCode = 7112 then
    end if;
  end try;
  
  select TheDate, Name, a.RO, 
      sum(case when #StoreCode = 'RY1' and OpCode in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDEL1', 'PDQDP') then 1
        when #StoreCode = 'RY2' and OpCode in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S') then 1
        else 0 end) as OilChange,
    sum(case when #StoreCode = 'RY1' and OpCode in ('PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQDP') then 1
      when #StoreCode = 'RY2' and OpCode in ('LOFD', 'LOF0205', 'MSS', 'M1S') then 1
      else 0 end) as BOC,
    sum(case when #StoreCode = 'RY1' and OpCode in ('13A') then 1
      when #StoreCode = 'RY2' and OpCode in ('RAF') then 1
      else 0 end) as AirFilter,
    sum(case when #StoreCode = 'RY1' and OpCode in ('ROT', 'FREEROT', 'ROTEMP') then 1
      when #StoreCode = 'RY2' and OpCode in ('FROT', 'ROT') then 1
      else 0 end) as Rotate,
    sum(case when OpCode in ('NT2', 'NT4') then 1 else 0 end) as Tires into #phpGetPDQStatsByWriter
  from factro a
  left join factroline b on a.RO = b.RO
  left join day c on a.CloseDateKey = c.DateKey
  left join dimServiceWriter d on a.WriterID = WriterNumber and d.StoreCode = a.StoreCode
  where ((#StoreCode = 'RY1' and WriterID in ('401', '410', '415', '419', '423', '425', '427', '430', '431', '432', '740', '644', '728')) or (#StoreCode = 'RY2' and WriterID in ('102', '424', '428', '644', '728'))) and
      (((#StoreCode = 'RY1') and (OpCode in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((#StoreCode = 'RY2') and (OpCode in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
       ))
  group by TheDate, Name, a.RO
  order by TheDate desc;
  
  if #TheType = 'DAILY' then
    insert into __Output
      select TheDate, left(trim(monthname(TheDate)), 3) + '-' + trim(cast(dayofmonth(TheDate) as SQL_Char)), Name, sum(OilChange) as OilChanges, round(sum(BOC) / (sum(OilChange) * 1.0) * 100, 0) as BOC, round(sum(AirFilter) / (sum(OilChange) * 1.0) * 100, 0) as AirPen,
        round(sum(Rotate) / (sum(OilChange) * 1.0) * 100, 0) as Rotate, round(sum(Tires) / (sum(OilChange) * 1.0) * 100, 0) as Tires
      from #phpGetPDQStatsByWriter
      where TheDate >= curdate() - 10 and TheDate <> '12/31/9999'
      group by TheDate, Name
	  having sum(OilChange) > 0;
  end if;

  if #TheType = 'WTD' then
    insert into __Output
      select curdate(), 'This Week', Name, sum(OilChange) as OilChanges, round(sum(BOC) / (sum(OilChange) * 1.0) * 100, 0) as BOC, round(sum(AirFilter) / (sum(OilChange) * 1.0) * 100, 0) as AirPen,
        round(sum(Rotate) / (sum(OilChange) * 1.0) * 100, 0) as Rotate, round(sum(Tires) / (sum(OilChange) * 1.0) * 100, 0) as Tires
      from #phpGetPDQStatsByWriter
      where (year(TheDate) = year(curdate())) and (week(TheDate) = week(curdate())) and (TheDate <> '12/31/9999')
      group by curdate(), Name
	  having sum(OilChange) > 0;
  end if;

  if #TheType = 'MTD' then
    insert into __Output
      select curdate(), MonthName(curdate()), Name, sum(OilChange) as OilChanges, round(sum(BOC) / (sum(OilChange) * 1.0) * 100, 0) as BOC, round(sum(AirFilter) / (sum(OilChange) * 1.0) * 100, 0) as AirPen,
        round(sum(Rotate) / (sum(OilChange) * 1.0) * 100, 0) as Rotate, round(sum(Tires) / (sum(OilChange) * 1.0) * 100, 0) as Tires
      from #phpGetPDQStatsByWriter
      where year(TheDate) = year(curdate()) and month(TheDate) = month(curdate()) and TheDate <> '12/31/9999'
      group by curdate(), Name
	  having sum(OilChange) > 0;
  end if;

  if #TheType = 'LASTMONTH' then
    insert into __Output
      select curdate(), MonthName(timestampadd(SQL_TSI_MONTH, - 1, curdate())), Name, sum(OilChange) as OilChanges, round(sum(BOC) / (sum(OilChange) * 1.0) * 100, 0) as BOC, round(sum(AirFilter) / (sum(OilChange) * 1.0) * 100, 0) as AirPen,
        round(sum(Rotate) / (sum(OilChange) * 1.0) * 100, 0) as Rotate, round(sum(Tires) / (sum(OilChange) * 1.0) * 100, 0) as Tires
      from #phpGetPDQStatsByWriter
      where year(TheDate) = year(timestampadd(SQL_TSI_MONTH, - 1, curdate())) and month(TheDate) = month(timestampadd(SQL_TSI_MONTH, - 1, curdate())) and TheDate <> '12/31/9999'
      group by curdate(), Name
	  having sum(OilChange) > 0;
  end if;

  if #TheType = 'YTD' then
    insert into __Output
      select curdate(), trim(cast(year(curdate()) as SQL_Char)), Name, sum(OilChange) as OilChanges, round(sum(BOC) / (sum(OilChange) * 1.0) * 100, 0) as BOC, round(sum(AirFilter) / (sum(OilChange) * 1.0) * 100, 0) as AirPen,
        round(sum(Rotate) / (sum(OilChange) * 1.0) * 100, 0) as Rotate, round(sum(Tires) / (sum(OilChange) * 1.0) * 100, 0) as Tires
      from #phpGetPDQStatsByWriter
      where year(TheDate) = year(curdate()) and TheDate <> '12/31/9999'
      group by curdate(), Name
	  having sum(OilChange) > 0;
  end if;

  if #TheType = 'WEEKLY' then
    insert into __Output
      select TheDate - (dayofweek(TheDate) - 1), 'Week of ' + trim(cast(month(TheDate - (dayofweek(TheDate) - 1)) as SQL_Char)) + '-' + trim(cast(dayofmonth(TheDate - (dayofweek(TheDate) - 1)) as SQL_Char)), Name, sum(OilChange) as OilChanges, round(sum(BOC) / (sum(OilChange) * 1.0) * 100, 0) as BOC, round(sum(AirFilter) / (sum(OilChange) * 1.0) * 100, 0) as AirPen,
        round(sum(Rotate) / (sum(OilChange) * 1.0) * 100, 0) as Rotate, round(sum(Tires) / (sum(OilChange) * 1.0) * 100, 0) as Tires
      from #phpGetPDQStatsByWriter
      where TheDate >= curdate() - 183 and TheDate <> '12/31/9999'
      group by TheDate - (dayofweek(TheDate) - 1), Name
	  having sum(OilChange) > 0;
  end if;


END;



/* 
what IS a pdq ro
depends on who IS asking
for the purpose of pdq writer stats
a pdq ro IS any ro written BY a pdq writer
so, how DO i know that an ro was written BY a pdq writer?
*/
-- ALL ros IN 2013 written BY a PDQ writer
SELECT *
FROM factRepairOrder a
INNER JOIN dimOpcode b on a.opcodekey = b.opcodekey
INNER JOIN dimServiceWriter c on a.ServiceWriterKey = c.ServiceWriterKey
  AND c.CensusDept = 'QL'
LEFT JOIN day d on a.finalclosedatekey = d.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)


declare #StoreCode string;
#StoreCode = 'RY1';
  select TheDate, Name, a.RO, 
      sum(case when #StoreCode = 'RY1' and OpCode in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', 'PDQDEL1', 'PDQDP') then 1
        when #StoreCode = 'RY2' and OpCode in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S') then 1
        else 0 end) as OilChange,
    sum(case when #StoreCode = 'RY1' and OpCode in ('PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQDP') then 1
      when #StoreCode = 'RY2' and OpCode in ('LOFD', 'LOF0205', 'MSS', 'M1S') then 1
      else 0 end) as BOC,
    sum(case when #StoreCode = 'RY1' and OpCode in ('13A') then 1
      when #StoreCode = 'RY2' and OpCode in ('RAF') then 1
      else 0 end) as AirFilter,
    sum(case when #StoreCode = 'RY1' and OpCode in ('ROT', 'FREEROT', 'ROTEMP') then 1
      when #StoreCode = 'RY2' and OpCode in ('FROT', 'ROT') then 1
      else 0 end) as Rotate,
    sum(case when OpCode in ('NT2', 'NT4') then 1 else 0 end) as Tires 
  from factro a
  left join factroline b on a.RO = b.RO
  left join day c on a.CloseDateKey = c.DateKey
  left join dimServiceWriter d on a.WriterID = WriterNumber and d.StoreCode = a.StoreCode
  where ((#StoreCode = 'RY1' and WriterID in ('401', '410', '415', '419', '423', '425', '427', '430', '431', '432', '740', '644', '728')) or (#StoreCode = 'RY2' and WriterID in ('102', '424', '428', '644', '728'))) and
      (((#StoreCode = 'RY1') and (OpCode in ('PDQ', 'PDQD', 'PDQDS', 'PDQM1', 'PDQ020', 'PDQ520', 'PDQED', 'PDQE', 'PDQEM1', '13A', 'ROT', 'FREEROT', 'ROTEMP', 'NT2', 'NT4'))
        )
       or
       ((#StoreCode = 'RY2') and (OpCode in ('LOF', 'LOFD', 'LOF0204', 'LOF0205', 'MS', 'MSHM', 'MSS', 'M1S', 'LOFD', 'LOF0205', 'MSS', 'M1S', 'FROT', 'ROT', 'RAF', 'NT2', 'NT4'))
       ))
  group by TheDate, Name, a.RO
  order by TheDate desc;


