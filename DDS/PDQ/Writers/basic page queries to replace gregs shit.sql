-- are there ros with more than one line with lof
-- yep, 4 IN 2013, 9 IN 2012/2013 NOT enuf to worry about
SELECT ro
FROM factrepairorder a
INNER JOIN dimopcode b on a.opcodekey = b.opcodekey
  AND b.pdqcat1 = 'lof'
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear IN (2012, 2013))
GROUP BY ro
HAVING COUNT(*) > 1

-- what about the other sw metrics
SELECT pdqcat3, COUNT(*) FROM dimopcode GROUP BY pdqcat3
-- 5
SELECT ro
FROM factrepairorder a
INNER JOIN dimopcode b on a.opcodekey = b.opcodekey
  AND b.pdqcat3 = 'Air Filters'
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear IN (2012, 2013))
GROUP BY ro
HAVING COUNT(*) > 1
-- 8
SELECT ro
FROM factrepairorder a
INNER JOIN dimopcode b on a.opcodekey = b.opcodekey
  AND b.pdqcat3 = 'Rotate'
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear IN (2012, 2013))
GROUP BY ro
HAVING COUNT(*) > 1


-- start with writers TABLE ?
-- 2013 - daily
-- 5 sec
SELECT d.thedate, a.storecode, a.description, a.writernumber,
  SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
  SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
  SUM(CASE WHEN pdqcat3 = 'Air Filter' THEN 1 ELSE 0 END) AS AirFilter,
  SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
FROM dimServiceWriter a
INNER JOIN factRepairOrder b on a.ServiceWriterKey = b.ServiceWriterKey
  AND b.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)
INNER JOIN dimopcode c on b.opcodekey = c.opcodekey
  AND c.pdqcat1 <> 'N/A'
LEFT JOIN day d on b.finalclosedatekey = d.datekey  
WHERE a.CensusDept = 'QL' 
GROUP BY d.thedate, a.storecode, a.description, a.writernumber

-- start with factRepairOrder 2013 - daily
-- 4 sec
SELECT d.thedate, a.storecode, a.name, a.description, a.writernumber,
  SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
  SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
  SUM(CASE WHEN pdqcat3 = 'Air Filter' THEN 1 ELSE 0 END) AS AirFilter,
  SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
FROM factRepairOrder b
--INNER JOIN dimServiceType bb on b.ServiceTypeKey = bb.ServiceTypeKey -- this makes it workse
--  AND bb.ServiceTypeCode = 'QL'
INNER JOIN dimOpcode c on b.opcodekey = c.opcodekey
--  AND c.pdqcat1 <> 'N/A'
  AND c.pdqcat1 IN ('LOF','Other') -- no change
INNER JOIN dimServiceWriter a on b.serviceWriterKey = a.ServiceWriterKey
  AND a.CensusDept = 'QL' 
LEFT JOIN day d on b.finalclosedatekey = d.datekey  
WHERE b.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)  
--  AND b.ServiceTypeKey = (SELECT ServiceTypeKey FROM dimServiceType WHERE ServiceTypeCode = 'QL') -- this makes it worse
GROUP BY d.thedate, a.storecode, a.name, a.description, a.writernumber

-- fucking 3.5 sec
SELECT d.thedate, a.storecode, a.name, a.description, a.writernumber,
  SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
  SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
  SUM(CASE WHEN pdqcat3 = 'Air Filter' THEN 1 ELSE 0 END) AS AirFilter,
  SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
-- SELECT COUNT(*)  
FROM factRepairOrder b
INNER JOIN dimServiceWriter a on b.serviceWriterKey = a.ServiceWriterKey
  AND a.CensusDept = 'QL' 
LEFT JOIN day d on b.finalclosedatekey = d.datekey  
LEFT JOIN dimopcode e on b.opcodekey = e.opcodekey
WHERE b.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)  
  AND b.opcodekey IN (SELECT opcodekey FROM dimopcode WHERE pdqcat1 <> 'N/A')
GROUP BY d.thedate, a.storecode, a.name, a.description, a.writernumber

-- fucking 3.2 sec
-- this IS the fastest so far
SELECT d.thedate, b.ro, a.storecode, a.name, a.description, a.writernumber,
  SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
  SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
  SUM(CASE WHEN pdqcat3 = 'Air Filter' THEN 1 ELSE 0 END) AS AirFilter,
  SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
-- SELECT COUNT(*)  
FROM factRepairOrder b
INNER JOIN day d on b.finalclosedatekey = d.datekey  
INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
WHERE b.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)  
  AND b.opcodekey IN (SELECT opcodekey FROM dimopcode WHERE pdqcat1 <> 'N/A')
  AND b.ServiceWriterKey IN (SELECT ServiceWriterKey FROM dimServiceWriter WHERE CensusDept = 'QL')
GROUP BY d.thedate, b.ro, a.storecode, a.name, a.description, a.writernumber

-- CREATE a TABLE
DROP TABLE tmpPDQWriterStats;
CREATE TABLE tmpPDQWriterStats (
  theDate date constraint NOT NULL,
  mmdd cichar(5) constraint NOT NULL,
  storecode cichar(3) constraint NOT NULL,
  ro cichar(9) constraint NOT NULL,
  name cichar(25) constraint NOT NULL,
  writernumber cichar(3) constraint NOT NULL,
  lof integer constraint NOT NULL,
  boc integer constraint NOT NULL,
  airfilters integer constraint NOT NULL,
  rotate integer constraint NOT NULL,
CONSTRAINT PK PRIMARY KEY (ro)) IN database; 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpPDQWriterStats','tmpPDQWriterStats.adi','theDate','theDate',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpPDQWriterStats','tmpPDQWriterStats.adi','StoreCode','StoreCode',
   '',2,512,'' );    

DELETE FROM tmpPDQWriterStats;  
INSERT INTO tmpPDQWriterStats  
SELECT d.thedate, d.mmdd, a.storecode, b.ro, max(coalesce(a.name, a.description)), a.writernumber,
  SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
  SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
  SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
  SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
FROM factRepairOrder b
INNER JOIN day d on b.closedatekey = d.datekey  -- ** closedate NOT finalclosedate
INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
WHERE b.closedatekey IN (SELECT datekey FROM day WHERE theyear IN (2011, 2012, 2013))-- ** closedate NOT finalclosedate  
  AND b.opcodekey IN (SELECT opcodekey FROM dimopcode WHERE pdqcat1 <> 'N/A')
  AND b.ServiceWriterKey IN (SELECT ServiceWriterKey FROM dimServiceWriter WHERE CensusDept = 'QL')
GROUP BY d.thedate, d.mmdd, b.ro, a.storecode, a.writernumber;

-- include todays data
SELECT *
FROM tmpPDQWriterStats
UNION 
SELECT d.thedate, d.mmdd, a.storecode, b.ro, max(coalesce(a.name, a.description)), a.writernumber,
  SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
  SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
  SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilter,
  SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
FROM todayFactRepairOrder b
INNER JOIN day d on b.finalclosedatekey = d.datekey  
INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
WHERE b.finalclosedatekey IN (SELECT datekey FROM day WHERE theyear = 2013)  
  AND b.opcodekey IN (SELECT opcodekey FROM dimopcode WHERE pdqcat1 <> 'N/A')
  AND b.ServiceWriterKey IN (SELECT ServiceWriterKey FROM dimServiceWriter WHERE CensusDept = 'QL')
GROUP BY d.thedate, d.mmdd, b.ro, a.storecode, a.writernumber;

-- daily, last 10 days
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilter = 0 THEN 0
    ELSE round(100.0 * airfilter/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM (  
  SELECT mmdd AS "Last 10 Days", storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
    SUM(airfilters) AS airfilter, SUM(rotate) AS rotate
  FROM tmpPDQWriterStats
  WHERE thedate BETWEEN curdate() - 9 AND curdate()
  GROUP BY mmdd, storecode, name
  UNION 
  SELECT d.mmdd, a.storecode, coalesce(a.name, a.description), 
    SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
    SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
    SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
    SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate  
  FROM todayFactRepairOrder b
  INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
    AND d.thedate = curdate()
  INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
    AND e.pdqcat2 <> 'N/A'
  INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
    AND a.CensusDept = 'QL'   
  GROUP BY d.mmdd, a.storecode, coalesce(a.name, a.description)) x
ORDER BY "Last 10 Days" DESC, storecode, name  

-- week to date
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilters = 0 THEN 0
    ELSE round(100.0 * airfilters/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM ( 
  SELECT *
  FROM (
    SELECT MIN(mmdd) + ' thru ' +  MAX(mmdd) AS "Week to Date"
    FROM day
    WHERE year(thedate) = year(curdate())
      AND week(thedate) = week(curdate())) m
    LEFT JOIN ( 
      -- n :: ALL data grouped BY store/writer   
      SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
      FROM ( 
        -- c :: historical data for the desired date interval   
        SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
        FROM tmpPDQWriterStats a
        WHERE year(thedate) = year(curdate())
          AND week(thedate) = week(curdate())
        GROUP BY storecode, name
        UNION 
        -- today data
        SELECT a.storecode, coalesce(a.name, a.description), 
          SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
          SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
          SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
          SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
        FROM todayFactRepairOrder b
        INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
          AND d.thedate = curdate()
        INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
          AND e.pdqcat2 <> 'N/A'
        INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
          AND a.CensusDept = 'QL'     
        GROUP BY a.storecode, coalesce(a.name, a.description)) c  
      GROUP BY storecode, name) n on 1 = 1) x
ORDER BY storecode, name;      
      
-- month to date
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilters = 0 THEN 0
    ELSE round(100.0 * airfilters/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM ( 
  SELECT *
  FROM (
    SELECT distinct YearMonthShort AS "Month to Date"
    FROM day
    WHERE year(thedate) = year(curdate())
      AND month(thedate) = month(curdate())) m
    LEFT JOIN (   
      -- n :: ALL data grouped BY store/writer 
      SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
      FROM ( --
        -- c :: historical data for the desired date interval  
        SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
        FROM tmpPDQWriterStats a
        WHERE year(thedate) = year(curdate()) 
          AND month(thedate) = month(curdate())
        GROUP BY storecode, name
        UNION 
        -- today data
        SELECT a.storecode, coalesce(a.name, a.description), 
          SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
          SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
          SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
          SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
        FROM todayFactRepairOrder b
        INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
          AND d.thedate = curdate()
        INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
          AND e.pdqcat2 <> 'N/A'
        INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
          AND a.CensusDept = 'QL'   
        GROUP BY a.storecode, coalesce(a.name, a.description)) c  
      GROUP BY storecode, name) n on 1 = 1) x
ORDER BY storecode, name;  

-- last month          
SELECT year(curdate()) - 1 AS "Year to Date Last Year", storecode, name, 
  SUM(lof) AS lof, SUM(boc) AS boc, 
  SUM(airfilters) AS airfilters, SUM(rotate) as rotate,
  CASE 
    WHEN SUM(lof) = 0 OR SUM(boc) = 0 THEN 0
    ELSE round(100.0 * SUM(boc)/SUM(lof), 0) 
  END AS "BOC PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(airfilters) = 0 THEN 0
    ELSE round(100.0 * SUM(airfilters)/SUM(lof), 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(rotate) = 0 THEN 0
    ELSE round(100.0 * SUM(rotate)/SUM(lof), 0) 
  END AS "ROTATE PEN"    
FROM tmpPDQWriterStats a
WHERE year(thedate) = year(timestampadd(SQL_TSI_MONTH, - 1, curdate()))
  AND month(thedate) = month(timestampadd(SQL_TSI_MONTH, - 1, curdate()))
GROUP BY storecode, name;      
      
-- year to date  
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilters = 0 THEN 0
    ELSE round(100.0 * airfilters/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM ( 
  SELECT *
  FROM (
    SELECT year(curdate()) AS "Year to Date"
    FROM system.iota) m
    LEFT JOIN (   
      -- n :: ALL data grouped BY store/writer 
      SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
      FROM ( --
        -- c :: historical data for the desired date interval  
        SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
        FROM tmpPDQWriterStats a
        WHERE year(thedate) = year(curdate())
        GROUP BY storecode, name
        UNION 
        -- today data
        SELECT a.storecode, coalesce(a.name, a.description), 
          SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
          SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
          SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
          SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
        FROM todayFactRepairOrder b
        INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
          AND d.thedate = curdate()
        INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
          AND e.pdqcat2 <> 'N/A'
        INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
          AND a.CensusDept = 'QL'   
        GROUP BY a.storecode, coalesce(a.name, a.description)) c  
      GROUP BY storecode, name) n on 1 = 1) x
ORDER BY storecode, name;  

-- year to date last year
SELECT year(curdate()) - 1 AS "Year to Date Last Year", storecode, name, 
  SUM(lof) AS lof, SUM(boc) AS boc, 
  SUM(airfilters) AS airfilters, SUM(rotate) as rotate,
  CASE 
    WHEN SUM(lof) = 0 OR SUM(boc) = 0 THEN 0
    ELSE round(100.0 * SUM(boc)/SUM(lof), 0) 
  END AS "BOC PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(airfilters) = 0 THEN 0
    ELSE round(100.0 * SUM(airfilters)/SUM(lof), 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(rotate) = 0 THEN 0
    ELSE round(100.0 * SUM(rotate)/SUM(lof), 0) 
  END AS "ROTATE PEN"    
FROM tmpPDQWriterStats a
WHERE year(thedate) = year(curdate()) - 1
  AND thedate <= cast(timestampadd(sql_tsi_year, -1, curdate()) AS sql_date)
GROUP BY storecode, name  

---------- same queries BY store ----------------------------------------------
hmm store IS different, IN that i have QL lines on ros (18025335, 16133683) WHERE 
the writer IS nick sattler AND cashier
? a COLUMN for pdq vs nonpdq writer?
ADD INNER JOIN to dimServiceType
shit, which means a different base TABLE, tmptmpPDQWriterStats IS pdq writers only
too far, at least the extra columns are too far
maybe a tmpPDQStoreStats TABLE

--
unrelated, the notion of storing the pivoted data, both for store AND writers
-- 
-- daily, last 10 days
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilter = 0 THEN 0
    ELSE round(100.0 * airfilter/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM (  
  SELECT mmdd AS "Last 10 Days", storecode, SUM(lof) AS lof, SUM(boc) AS boc, 
    SUM(airfilters) AS airfilter, SUM(rotate) AS rotate
  FROM tmpPDQWriterStats
  WHERE thedate BETWEEN curdate() - 9 AND curdate()
  GROUP BY mmdd, storecode
  UNION 
  SELECT d.mmdd, a.storecode, 
    SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
    SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
    SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
    SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate  
  FROM todayFactRepairOrder b
  INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
    AND d.thedate = curdate()
  INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
    AND e.pdqcat2 <> 'N/A'
  INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
    AND a.CensusDept = 'QL'   
  GROUP BY d.mmdd, a.storecode) x
ORDER BY "Last 10 Days" DESC, storecode ;

-- week to date
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilters = 0 THEN 0
    ELSE round(100.0 * airfilters/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM ( 
  SELECT *
  FROM (
    SELECT MIN(mmdd) + ' thru ' +  MAX(mmdd) AS "Week to Date"
    FROM day
    WHERE year(thedate) = year(curdate())
      AND week(thedate) = week(curdate())) m
    LEFT JOIN ( 
      -- n :: ALL data grouped BY store/writer   
      SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
      FROM ( 
        -- c :: historical data for the desired date interval   
        SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
        FROM tmpPDQWriterStats a
        WHERE year(thedate) = year(curdate())
          AND week(thedate) = week(curdate())
        GROUP BY storecode, name
        UNION 
        -- today data
        SELECT a.storecode, coalesce(a.name, a.description), 
          SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
          SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
          SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
          SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
        FROM todayFactRepairOrder b
        INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
          AND d.thedate = curdate()
        INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
          AND e.pdqcat2 <> 'N/A'
        INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
          AND a.CensusDept = 'QL'     
        GROUP BY a.storecode, coalesce(a.name, a.description)) c  
      GROUP BY storecode, name) n on 1 = 1) x
ORDER BY storecode, name;      
      
-- month to date
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilters = 0 THEN 0
    ELSE round(100.0 * airfilters/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM ( 
  SELECT *
  FROM (
    SELECT distinct YearMonthShort AS "Month to Date"
    FROM day
    WHERE year(thedate) = year(curdate())
      AND month(thedate) = month(curdate())) m
    LEFT JOIN (   
      -- n :: ALL data grouped BY store/writer 
      SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
      FROM ( --
        -- c :: historical data for the desired date interval  
        SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
        FROM tmpPDQWriterStats a
        WHERE year(thedate) = year(curdate()) 
          AND month(thedate) = month(curdate())
        GROUP BY storecode, name
        UNION 
        -- today data
        SELECT a.storecode, coalesce(a.name, a.description), 
          SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
          SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
          SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
          SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
        FROM todayFactRepairOrder b
        INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
          AND d.thedate = curdate()
        INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
          AND e.pdqcat2 <> 'N/A'
        INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
          AND a.CensusDept = 'QL'   
        GROUP BY a.storecode, coalesce(a.name, a.description)) c  
      GROUP BY storecode, name) n on 1 = 1) x
ORDER BY storecode, name;  

-- last month          
SELECT year(curdate()) - 1 AS "Year to Date Last Year", storecode, name, 
  SUM(lof) AS lof, SUM(boc) AS boc, 
  SUM(airfilters) AS airfilters, SUM(rotate) as rotate,
  CASE 
    WHEN SUM(lof) = 0 OR SUM(boc) = 0 THEN 0
    ELSE round(100.0 * SUM(boc)/SUM(lof), 0) 
  END AS "BOC PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(airfilters) = 0 THEN 0
    ELSE round(100.0 * SUM(airfilters)/SUM(lof), 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(rotate) = 0 THEN 0
    ELSE round(100.0 * SUM(rotate)/SUM(lof), 0) 
  END AS "ROTATE PEN"    
FROM tmpPDQWriterStats a
WHERE year(thedate) = year(timestampadd(SQL_TSI_MONTH, - 1, curdate()))
  AND month(thedate) = month(timestampadd(SQL_TSI_MONTH, - 1, curdate()))
GROUP BY storecode, name;      
      
-- year to date  
SELECT x.*,
  CASE 
    WHEN lof = 0 OR boc = 0 THEN 0
    ELSE round(100.0 * boc/lof, 0) 
  END AS "BOC PEN",
  CASE 
    WHEN lof = 0 OR airfilters = 0 THEN 0
    ELSE round(100.0 * airfilters/lof, 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN lof = 0 OR rotate = 0 THEN 0
    ELSE round(100.0 * rotate/lof, 0) 
  END AS "ROTATE PEN"    
FROM ( 
  SELECT *
  FROM (
    SELECT year(curdate()) AS "Year to Date"
    FROM system.iota) m
    LEFT JOIN (   
      -- n :: ALL data grouped BY store/writer 
      SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
      FROM ( --
        -- c :: historical data for the desired date interval  
        SELECT storecode, name, SUM(lof) AS lof, SUM(boc) AS boc, 
          SUM(airfilters) AS airfilters, SUM(rotate) as rotate
        FROM tmpPDQWriterStats a
        WHERE year(thedate) = year(curdate())
        GROUP BY storecode, name
        UNION 
        -- today data
        SELECT a.storecode, coalesce(a.name, a.description), 
          SUM(CASE WHEN pdqcat1 = 'LOF' THEN 1 ELSE 0 END) AS LOF,
          SUM(CASE WHEN pdqcat3 = 'BOC' THEN 1 ELSE 0 END) AS BOC,
          SUM(CASE WHEN pdqcat3 = 'Air Filters' THEN 1 ELSE 0 END) AS AirFilters,
          SUM(CASE WHEN pdqcat3 = 'Rotate' THEN 1 ELSE 0 END) AS Rotate
        FROM todayFactRepairOrder b
        INNER JOIN day d on b.closedatekey = d.datekey -- ** closedate NOT finalclosedate 
          AND d.thedate = curdate()
        INNER JOIN dimopcode e on b.opcodekey = e.opcodekey
          AND e.pdqcat2 <> 'N/A'
        INNER JOIN dimServiceWriter a on b.ServiceWriterKEy = a.ServiceWriterKey
          AND a.CensusDept = 'QL'   
        GROUP BY a.storecode, coalesce(a.name, a.description)) c  
      GROUP BY storecode, name) n on 1 = 1) x
ORDER BY storecode, name;  

-- year to date last year
SELECT year(curdate()) - 1 AS "Year to Date Last Year", storecode, name, 
  SUM(lof) AS lof, SUM(boc) AS boc, 
  SUM(airfilters) AS airfilters, SUM(rotate) as rotate,
  CASE 
    WHEN SUM(lof) = 0 OR SUM(boc) = 0 THEN 0
    ELSE round(100.0 * SUM(boc)/SUM(lof), 0) 
  END AS "BOC PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(airfilters) = 0 THEN 0
    ELSE round(100.0 * SUM(airfilters)/SUM(lof), 0) 
  END AS "AIR FILTER PEN",
  CASE 
    WHEN SUM(lof) = 0 OR SUM(rotate) = 0 THEN 0
    ELSE round(100.0 * SUM(rotate)/SUM(lof), 0) 
  END AS "ROTATE PEN"    
FROM tmpPDQWriterStats a
WHERE year(thedate) = year(curdate()) - 1
  AND thedate <= cast(timestampadd(sql_tsi_year, -1, curdate()) AS sql_date)
GROUP BY storecode, name  