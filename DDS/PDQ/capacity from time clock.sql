SELECT storecode, pydeptcode, pydept, distcode, distribution, COUNT(*)
-- SELECT *
FROM edwEmployeeDim 
WHERE currentrow = true
  AND active = 'active'
GROUP BY storecode, pydeptcode, pydept, distcode, distribution

distcode: 
  RY1:PDQW, PTEC
  RY2:PDQW, TPDQ
  
  
SELECT b.thedate, b.dayname, c.storecode,c.distcode, e.thehour
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
INNER JOIN factClockMinutesByHour d on b.datekey = d.datekey 
  AND c.employeekey = d.employeekey
INNER JOIN timeOfDay e on d.timeofDayKey = e.timeOfDayKey  
WHERE b.theYEar = 2014  
  AND c.distcode IN ('PDQW','PTEC','TPDQ')
  AND a.clockhours > 0

DROP TABLE #wtf;  
SELECT storecode, thedate, distcode, thehour, COUNT(*)
--INTO #wtf
FROM (  
  SELECT b.thedate, b.dayname, c.storecode,c.distcode, e.thehour
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  INNER JOIN factClockMinutesByHour d on b.datekey = d.datekey 
    AND c.employeekey = d.employeekey
  INNER JOIN timeOfDay e on d.timeofDayKey = e.timeOfDayKey  
  WHERE b.theYEar = 2014  
    AND c.distcode IN ('PDQW','PTEC','TPDQ')
    AND a.clockhours > 0) x  
GROUP BY storecode, thedate, distcode, thehour    

SELECT a.*, b.dayname
FROM #wtf a
INNER JOIN day b on a.thedate = b.thedate
WHERE a.storecode = 'ry2'
  AND b.thedate > '08/31/2014'
  AND a.distcode = 'tpdq'
  
  

SELECT a.*, b.dayname
FROM #wtf a
INNER JOIN day b on a.thedate = b.thedate
WHERE a.storecode = 'ry2'
  AND b.thedate > '08/31/2014'
  AND a.distcode = 'tpdq'
  AND thehour = 13
  

  
go more abstract 
tech/writer capacity hours BY day/week/month  

DROP TABLE #wtf;

SELECT b.thedate, b.dayname, c.storecode,c.distcode, sum(a.clockhours) AS clockHours
INTO #wtf
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey 
WHERE b.theYEar = 2014  
  AND c.distcode IN ('PDQW','PTEC','TPDQ')
  AND a.clockhours > 0
GROUP BY b.thedate, b.dayname, c.storecode,c.distcode  

SELECT month(thedate), distcode, SUM(clockHours) AS capacity
FROM #wtf
WHERE storecode = 'ry2'
GROUP BY month(thedate), distcode

-- why the big jump IN writer hours IN october

SELECT month(thedate), name, SUM(clockhours)
FROM (
SELECT b.thedate, b.dayname, c.storecode,c.distcode, a.clockhours, c.name
-- INTO #wtf
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey 
WHERE b.theYEar = 2014  
  AND c.distcode = 'PDQW'
  AND a.clockhours > 0
  AND storecode = 'ry2'
  AND month(thedate) IN (8,9,10)

) x GROUP BY month(thedate), name  

-- 11/19/14
for each hour of the day IN 2014, how much pdq capacity
date     hour   tech mins   writ mins   total mins
6/1/14   8 AM   1120
6/1/14   9 AM

-- ok here's the detail
-- oops forgot the store
SELECT b.thedate, a.*, c.*, d.distcode
FROM factClockMinutesByHour a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN timeOfDay c on a.timeofdaykey = c.timeofdaykey
INNER JOIN edwEmployeeDim d on a.employeekey = d.employeekey
WHERE b.theyear = 2014
  AND d.distcode IN ('PDQW','PTEC','TPDQ')

DROP TABLE #wtf
SELECT d.storecode, thedate, thehour,
  SUM(CASE WHEN distcode IN ('PTEC','TPDQ') THEN minutes else 0 END) AS techMinutes,
  SUM(CASE WHEN distcode = 'PDQW' THEN minutes else 0 END) AS writerMinutes,
  SUM(minutes) AS totalMinutes
INTO #wtf  
FROM factClockMinutesByHour a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN timeOfDay c on a.timeofdaykey = c.timeofdaykey
INNER JOIN edwEmployeeDim d on a.employeekey = d.employeekey
WHERE b.theyear = 2014
  AND d.distcode IN ('PDQW','PTEC','TPDQ')
GROUP BY d.storecode, theDate, theHour   

SELECT * FROM #wtf;

