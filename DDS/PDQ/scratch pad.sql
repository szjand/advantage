SELECT shapeAndSize, SUM(units)
FROM ucinv_glump_ranking
WHERE thedate = curdate()
  AND saletype = 'retail'
GROUP BY shapeAndSize
ORDER BY SUM(units) DESC 