-- this IS going to need lots of work
SELECT a.stocknumber, b.vin, cast(a.FromTS as sql_date) as viiFrom, 
  cast(a.ThruTS as sql_date) as viiThru, c.ro,
  c.customername, d.thedate AS OpenDate, e.*
FROM dps.VehicleInventoryItems a
LEFT JOIN dps.VehicleItems b ON a.VehicleItemID = b.VehicleItemID 
LEFT JOIN factro c ON b.vin = c.vin 
  AND c.void = false
LEFT JOIN day d ON c.opendatekey = d.datekey  
LEFT JOIN stgRydellServiceAppointments e ON c.ro = e.ro
WHERE ThruTS IS NULL 
  AND owninglocationid = (
    select partyid
    FROM dps.organizations
    WHERE name = 'rydells')
    
  
-- ok, this IS a descent extract FROM factRO
-- ro's opened within a daterange
SELECT b.thedate as OpenDate, c.thedate AS CloseDate, a.ro, a.writerid, 
  a.customername, a.vin
FROM factro a 
INNER JOIN day b ON a.opendatekey = b.datekey 
  AND b.thedate BETWEEN '07/02/2012' AND '07/03/2012'
LEFT JOIN day c ON a.closedatekey = c.datekey  
WHERE a.void = false

-- what i want IS OPEN ros
-- think this IS it
SELECT b.thedate as OpenDate, c.thedate AS CloseDate, a.ro, a.writerid, 
  a.customername, a.vin
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey 
LEFT JOIN day c ON a.closedatekey = c.datekey  
WHERE a.storecode = 'ry1'
  AND closedatekey = (SELECT datekey FROM day WHERE datetype = 'NA')
  AND void = false
  
-- holy shit multiple rows per ro
SELECT *
FROM (  
  SELECT b.thedate as OpenDate, c.thedate AS CloseDate, a.ro, a.writerid, 
    a.customername, a.vin
  FROM factro a
  INNER JOIN day b ON a.opendatekey = b.datekey 
  LEFT JOIN day c ON a.closedatekey = c.datekey  
  WHERE a.storecode = 'ry1'
    AND closedatekey = (SELECT datekey FROM day WHERE datetype = 'NA')
    AND void = false) d
LEFT JOIN stgRydellServiceAppointments e ON d.ro = e.ro   
  
/******************************************************************************/  
multiple appointments for the same ro  
/******************************************************************************/  
-- apptstatuses might help sort out which IS the relevant appt
-- it appears that changing some statuses creates a new appointment

SELECT ro, advisorstatus, COUNT(*)
FROM stgRydellServiceAppointments
GROUP BY ro, advisorstatus
HAVING COUNT(*) > 1

-- 2 months of carry overs
SELECT *
FROM stgRydellServiceAppointments
WHERE ro = '16086001'

-- it appears that changing some statuses creates a new appointment
-- IS it just carryovers
-- nope it's a whole bunch of transitions
SELECT *
FROM stgRydellServiceAppointments
WHERE length(TRIM(ro)) = 8
  AND ro IN (
    SELECT ro 
    FROM stgRydellServiceAppointments
    GROUP BY ro, advisorstatus
    HAVING COUNT(*) > 1)
ORDER BY ro, createdts    

/* most recent advisor status per row */
-- don't care about the array of statuses, just the current status
-- one row per ro
SELECT ro, MAX(createdTS) AS TS
FROM stgRydellServiceAppointments
WHERE length(trim(ro)) = 8 
GROUP BY ro

SELECT *
FROM stgRydellServiceAppointments a
INNER JOIN (
    SELECT ro, MAX(createdTS) AS TS
    FROM stgRydellServiceAppointments
    WHERE length(trim(ro)) = 8 
    -- could have an AND date range here ?
    GROUP BY ro) b ON a.ro = b.ro
  AND a.CreatedTS = b.TS
WHERE length(trim(a.ro)) = 8

-- bingo 1 row per ro
SELECT ro
FROM (
SELECT *
FROM stgRydellServiceAppointments a
INNER JOIN (
    SELECT ro, MAX(createdTS) AS TS
    FROM stgRydellServiceAppointments
    WHERE length(trim(ro)) = 8 
    GROUP BY ro) b ON a.ro = b.ro
  AND a.CreatedTS = b.TS
WHERE length(trim(a.ro)) = 8 ) x
GROUP BY ro HAVING COUNT(*) > 1
/* most recent advisor status per ro */

-- this looks pretty good for arkona + scheduler.appointments
-- OPEN ROs, based ON factRO.CloseDate
SELECT b.thedate as OpenDate, a.ro, a.writerid, 
  a.customername, a.vin, d.advisorstatus, 
  case g.ptrsts
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Proc'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier /D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Errpr'
    ELSE g.ptrsts
  END AS pdpStatus
FROM factro a
INNER JOIN day b ON a.opendatekey = b.datekey 
LEFT JOIN ( -- stgRydellServiceAppointments - most recent advisor status per ro
  SELECT *
  FROM stgRydellServiceAppointments a
  INNER JOIN (
      SELECT ro, MAX(createdTS) AS TS
      FROM stgRydellServiceAppointments
      WHERE length(trim(ro)) = 8 
      -- could have an AND date range here ?
      GROUP BY ro) b ON a.ro = b.ro
    AND a.CreatedTS = b.TS
  WHERE length(trim(a.ro)) = 8) d ON a.ro = d.ro 
LEFT JOIN stgArkonaPDPPHDR g ON a.ro = g.ptdoc#  
WHERE a.storecode = 'ry1'
  AND closedatekey = (SELECT datekey FROM day WHERE datetype = 'NA')
  AND void = false
ORDER BY a.ro

-- appointments arrived, but no ro OR fucked up ro number
SELECT *
FROM stgRydellServiceAppointments
WHERE CAST(arrivalts AS sql_date) > '12/31/1900'
  AND length(trim(ro)) <> 8 
  
-- ro IN rs.appointments but no IN arkona
-- damned few
SELECT a.ro, a.vin, a.advisorstatus, a.arrivalts, b.* 
FROM stgRydellServiceAppointments a
LEFT JOIN factro b ON a.ro = b.ro
WHERE CAST(arrivalts AS sql_date) > '12/31/1900'
  AND length(trim(a.ro)) = 8 
  AND b.ro IS NULL 

SELECT b.thedate, a.*
FROM factro a
LEFT JOIN day b ON a.opendatekey = b.datekey
WHERE vin = '1GNDT13S462284166'  
-- need tool data

  