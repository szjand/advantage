SELECT *
FROM stgRydellServiceAppointments

SELECT appointmentid
FROM stgRydellServiceAppointments
GROUP BY appointmentid
HAVING COUNT(*) > 1

SELECT advisorstatus, COUNT(*)
FROM stgRydellServiceAppointments
GROUP BY advisorstatus

SELECT ro, vin
FROM stgRydellServiceAppointments
GROUP BY ro, vin
HAVING COUNT(*) > 1

SELECT advisorstatus, COUNT(*)
FROM stgRydellServiceAppointments
WHERE ro = ''
GROUP BY advisorstatus

SELECT a.ro, a.vin, advisorstatus, appointmentts, b.performedby, b.comments,
  b.ts, b.status, c.*
FROM stgRydellServiceAppointments a
LEFT JOIN stgRydellServiceStatuses b ON a.appointmentid = b.appointmentid
LEFT JOIN stgRydellServiceAppointmentStatuses c ON b.appointmentid = c.appointmentid
WHERE b.appointmentid IS NOT NULL 
ORDER BY ro

SELECT a.storecode, a.ro, a.vin, c.thedate AS OpenDate, b.thedate AS CloseDate
FROM factro a
LEFT JOIN day b ON a.closedatekey = b.datekey
LEFT JOIN day c ON a.opendatekey = c.datekey
LEFT JOIN day d ON a.finalclosedatekey = d.datekey
WHERE a.void = false
  AND d.thedate = '12/31/9999'
ORDER BY opendate desc  

SELECT * FROM xfmro WHERE ptro# = '19106782'


SELECT *
FROM factroline

SELECT *
FROM factro


SELECT a.*, b.thedate
FROM factro a
inner JOIN day b ON a.opendatekey = b.datekey
WHERE b.thedate = '07/02/2012'
  AND a.storecode = 'ry1'
  
SELECT *
FROM stgRydellServiceAppointments a
LEFT JOIN factro b ON a.ro = b.ro
WHERE a.vin = '1FMFU18L23LC38319'

SELECT *
from factro a
LEFT JOIN factroline b ON a.ro = b.ro
WHERE vin = '1FMFU18L23LC38319'
ORDER BY a.ro, b.line

-- using AS a base
-- ro's with multiple statuses
SELECT a.createdts, a.advisorstatus, a.ro, a.appointmentts, 
  b.status, b.ts
FROM stgRydellServiceAppointments a
LEFT JOIN stgRydellServiceStatuses b ON a.appointmentid = b.appointmentid
WHERE length(TRIM(a.ro)) = 8
  AND a.ro IN (
    SELECT ro 
    FROM stgRydellServiceAppointments
    GROUP BY ro, advisorstatus
    HAVING COUNT(*) > 1)
ORDER BY a.ro, a.createdts, ts    
 
SELECT a.createdts, a.advisorstatus, a.ro, a.appointmentts, 
  b.status, b.fromts, b.thruts
FROM stgRydellServiceAppointments a
LEFT JOIN stgRydellServiceAppointmentStatuses b ON a.appointmentid = b.appointmentid
WHERE length(TRIM(a.ro)) = 8
  AND a.ro IN (
    SELECT ro 
    FROM stgRydellServiceAppointments
    GROUP BY ro, advisorstatus
    HAVING COUNT(*) > 1)
ORDER BY a.ro, a.createdts, b.fromts   

SELECT appointmentid
FROM (
SELECT appointmentid, status
-- SELECT *
from stgRydellServiceAppointmentStatuses  
WHERE CAST(thruts AS sql_date) = '12/30/1899'
GROUP BY appointmentid, status) a
GROUP BY appointmentid
HAVING COUNT(*) > 1


SELECT *
FROM stgRydellServiceAppointmentStatuses
WHERE appointmentid IN (
    SELECT appointmentid
    FROM (
      SELECT appointmentid, status
      -- SELECT *
      from stgRydellServiceAppointmentStatuses  
      WHERE CAST(thruts AS sql_date) = '12/30/1899'
      GROUP BY appointmentid, status) a
    GROUP BY appointmentid
    HAVING COUNT(*) > 1)
  AND CAST(thruts AS sql_date) = '12/30/1899'

-- 2 status categories: Advisor_ AND Technician_  
SELECT status, COUNT(*)
FROM stgRydellServiceAppointmentStatuses

SELECT appointmentid
FROM stgRydellServiceAppointmentStatuses
GROUP BY appointmentid
HAVING COUNT(*) > 1

-- 1 current Adv Status
SELECT appointmentid
FROM stgRydellServiceAppointmentStatuses
WHERE status LIKE 'A%'
  AND thruts IS NULL 
GROUP BY appointmentid
HAVING COUNT(*) > 1

-- 1 current Adv Status
SELECT appointmentid
FROM stgRydellServiceAppointmentStatuses
WHERE status LIKE 'T%'
  AND thruts IS NULL 
GROUP BY appointmentid

-- compare those to advisorstatus

-- 7/10
-- statuses
-- Appointments:
SELECT advisorstatus, COUNT(*)
FROM stgRydellServiceAppointments
GROUP BY advisorstatus
--                    Appointment
--                    Blueprint
--                    Carry Over
--                    Done
--                    Kitted
--                    No Show
--                    Pend Auth
--                    Pend PTs

-- currentstatus
SELECT *
FROM stgRydellServiceAppointments a
INNER JOIN (
    SELECT ro, MAX(createdTS) AS TS
    FROM stgRydellServiceAppointments
    WHERE length(trim(ro)) = 8 
    -- could have an AND date range here ?
    GROUP BY ro) b ON a.ro = b.ro
  AND a.CreatedTS = b.TS
WHERE length(trim(a.ro)) = 8

-- Statuses: 
SELECT status, COUNT(*) 
-- SELECT *                                     
FROM stgRydellServiceStatuses  
GROUP BY status        
-- Statuses:           Advisor_StatusCallMade
--                     Technician_BluePrintDone  
--                     Technician_BlueprintStart      
-- currentstatus
-- current AdvisorStatus  
-- these are just call made statuses, DO i even care
SELECT a.* 
FROM stgRydellServiceStatuses a
INNER JOIN (
  SELECT appointmentID, Status, MAX(TS) AS ts
  FROM stgRydellServiceStatuses
  WHERE status LIKE 'A%'
  GROUP BY appointmentID, Status) b ON a.AppointmentID  = b.AppointmentID
    AND a.status = b.status
    AND a.ts = b.ts 
-- current TechStatus  
-- take status out of grouping, many appts have T_BluePrintStart AND T_BluePrintDone
-- just go with the most recent
SELECT a.* 
FROM stgRydellServiceStatuses a
INNER JOIN (
  SELECT appointmentID, MAX(TS) AS ts
  FROM stgRydellServiceStatuses
  WHERE status LIKE 'T%'
  GROUP BY appointmentID) b ON a.AppointmentID  = b.AppointmentID
    AND a.ts = b.ts     
 
-- AppointmentStatuseses
SELECT status, COUNT(*)
-- SELECT *
FROM stgRydellServiceAppointmentStatuses
GROUP BY status
-- Statuses:           Advisor_Booked
--                     Technician_AuthorizationHold   
--                     Technician_Available
--                     Technician_CarryOver
--                     Technician_Done
--                     Technician_PartsHold
--                     Technician_PreDiagnosis
--                     Technician_TechHold
--                     Technician_Working
-- current status
SELECT appointmentid
FROM stgRydellServiceAppointmentStatuses
WHERE status LIKE 'A%'
GROUP BY appointmentid
HAVING COUNT(*) > 1

SELECT appointmentid
FROM stgRydellServiceAppointmentStatuses
WHERE status LIKE 'A%'
  AND thruts <> cast('12/30/1899 00:00:00' AS sql_timestamp)
GROUP BY appointmentid
HAVING COUNT(*) > 1
-- 1 current Adv Status
-- of course, the only Astatus IS Advisor_Booked

-- 1 current Tech status
SELECT appointmentid
-- SELECT *
FROM stgRydellServiceAppointmentStatuses
WHERE status LIKE 'T%'
  AND thruts = cast('12/30/1899 00:00:00' AS sql_timestamp)
GROUP BY appointmentid
HAVING COUNT(*) > 1                      

-- current status
SELECT appointmentid, status
FROM stgRydellServiceAppointmentStatuses
WHERE status LIKE 'T%'
  AND thruts = cast('12/30/1899 00:00:00' AS sql_timestamp)

-- current Appointments AS base
SELECT a.CreatedTS, a.AdvisorStatus, a.ro, a.vin, 
  c.Status, c.TS, d.Status AS asStatus, d.FromTS
FROM stgRydellServiceAppointments a
INNER JOIN (
    SELECT ro, MAX(createdTS) AS TS
    FROM stgRydellServiceAppointments
    WHERE length(trim(ro)) = 8 
    -- could have an AND date range here ?
    GROUP BY ro) b ON a.ro = b.ro
  AND a.CreatedTS = b.TS
LEFT JOIN ( -- stgRydellServiceStatuses
  SELECT a.* 
  FROM stgRydellServiceStatuses a
  INNER JOIN (
    SELECT appointmentID, MAX(TS) AS ts
    FROM stgRydellServiceStatuses
    WHERE status LIKE 'T%'
    GROUP BY appointmentID) b ON a.AppointmentID  = b.AppointmentID
      AND a.ts = b.ts) c ON a.AppointmentID = c.appointmentid
LEFT JOIN (
  SELECT appointmentid, status, FromTS
  FROM stgRydellServiceAppointmentStatuses
  WHERE status LIKE 'T%'
    AND thruts = cast('12/30/1899 00:00:00' AS sql_timestamp)) d ON a.AppointmentID = d.appointmentid    
WHERE length(trim(a.ro)) = 8  

-- OPEN ros
SELECT r.ro, s.thedate as OpenDate, r.writerid, 
  r.customername, r.vin,a.CreatedTS, a.AdvisorStatus, a.ro, a.vin, 
  c.Status, c.TS, d.Status AS asStatus, d.FromTS,
  case u.status
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Proc'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier /D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Errpr'
    ELSE u.status
  END AS pdpStatus  
FROM factro r
LEFT JOIN (
  SELECT ptro#, status
  FROM xfmro
  WHERE source = 'PDET'  
  GROUP BY ptro#, status) u ON r.ro = u.ptro#
INNER JOIN day s ON r.opendatekey = s.datekey 
LEFT JOIN day t ON r.closedatekey = t.datekey  
LEFT JOIN stgRydellServiceAppointments a ON r.ro = a.ro
INNER JOIN (
    SELECT ro, MAX(createdTS) AS TS
    FROM stgRydellServiceAppointments
    WHERE length(trim(ro)) = 8 
    -- could have an AND date range here ?
    GROUP BY ro) b ON a.ro = b.ro
  AND a.CreatedTS = b.TS
LEFT JOIN ( -- stgRydellServiceStatuses
  SELECT a.* 
  FROM stgRydellServiceStatuses a
  INNER JOIN (
    SELECT appointmentID, MAX(TS) AS ts
    FROM stgRydellServiceStatuses
    WHERE status LIKE 'T%'
    GROUP BY appointmentID) b ON a.AppointmentID  = b.AppointmentID
      AND a.ts = b.ts) c ON a.AppointmentID = c.appointmentid
LEFT JOIN (
  SELECT appointmentid, status, FromTS
  FROM stgRydellServiceAppointmentStatuses
  WHERE status LIKE 'T%'
    AND thruts = cast('12/30/1899 00:00:00' AS sql_timestamp)) d ON a.AppointmentID = d.appointmentid    
WHERE r.storecode = 'ry1'
  AND r.closedatekey = (SELECT datekey FROM day WHERE datetype = 'NA')
  AND r.void = false
  AND u.status NOT IN ('4','5')
ORDER BY pdpstatus  
  
-- ok, so the snapshot of previous day IS NOT good enuf, of course
-- IS this an accumulating snapshot?
-- OR just a periodic
-- have to get there, 
-- what about adding tool data

-- OPEN ros
SELECT r.ro, s.thedate as OpenDate, r.writerid, 
  r.customername, r.vin,
  case u.status
    WHEN '1' THEN 'Open'
    WHEN '2' THEN 'In-Proc'
    WHEN '3' THEN 'Appr-PD'
    WHEN '4' THEN 'Cashier'
    WHEN '5' THEN 'Cashier /D'
    WHEN '6' THEN 'Pre-Inv'
    WHEN '7' THEN 'Odom Rq'
    WHEN '9' THEN 'Parts-A'
    WHEN 'L' THEN 'G/L Errpr'
    ELSE u.status
  END AS pdpStatus, advisorstatus,
  w.stocknumber 
FROM factro r
LEFT JOIN (
  SELECT ptro#, status
  FROM xfmro
  WHERE source = 'PDET'  
  GROUP BY ptro#, status) u ON r.ro = u.ptro#
INNER JOIN day s ON r.opendatekey = s.datekey 
LEFT JOIN day t ON r.closedatekey = t.datekey  
LEFT JOIN dps.VehicleItems v ON r.vin = v.vin
LEFT JOIN dps.VehicleInventoryItems w ON v.VehicleItemID = w.VehicleItemID 
  AND w.ThruTS IS NULL 
LEFT JOIN stgRydellServiceAppointments a ON r.ro = a.ro
INNER JOIN (
    SELECT ro, MAX(createdTS) AS TS
    FROM stgRydellServiceAppointments
    WHERE length(trim(ro)) = 8 
    -- could have an AND date range here ?
    GROUP BY ro) b ON a.ro = b.ro
  AND a.CreatedTS = b.TS
WHERE r.storecode = 'ry1'
  AND r.closedatekey = (SELECT datekey FROM day WHERE datetype = 'NA')
  AND r.void = false
  AND u.status NOT IN ('4','5')
  AND LEFT(r.ro, 2) IN ('16','19')
-- this IS WHERE i'm thinking i need to be generating current recon status IN 
-- dpsvseries, updates anytime a status changes
-- hmmm could DO IN updateviis
-- OR a periodic scrape

