/* 5/18/15

Jon;

The database change has been made on the Honda ADS server. The IP is 10.130.190.63. The username is Administrator and pwd is of the LS variety. 

The database path is D:\Advantage\NewHondaService\NewHondaService.add

The ADS connection path is \\10.130.190.63:6363\Advantage\NewHondaService\NewHondaService.add

Users should get the new executables on restart of the app.

Thank you,
Ron

*/

-- 5/14/15 ADD cust address AND vehicle info
DROP TABLE ext_xtime_appointments;
CREATE TABLE ext_xtime_appointments (
  company_number cichar(3) constraint NOT NULL,
  pending_key integer constraint NOT NULL,
  doc_create_timestamp cichar(30) constraint NOT NULL,
  cust_name cichar(30) constraint NOT NULL,
  cust_phone_no numeric(20,0),
  vin cichar(17),
  promised_date_time numeric(20,0) constraint NOT NULL,
  ptline integer constraint NOT NULL,
  ptltyp cichar(1) constraint NOT NULL,
  ptseq integer constraint NOT NULL,
  ptcmnt cichar(50),
  advisorID cichar(40),
  address_1 cichar(60),
  city cichar(20),
  state_code cichar(2),
  zip_code integer,
  year integer,
  make cichar(25),
  model cichar(25)) IN database;
  
select * FROM ext_xtime_appointments 

-- this converts the date & timestamp 
select company_number, pending_key, 
  cast(LEFT(cast(open_tran_date AS sql_char), 4) + '-' 
    + substring(cast(open_tran_date AS sql_char), 5,2)
    + '-' + substring(cast(open_tran_date AS sql_char),7,2) AS sql_date),
  cust_name, cust_phone_no, vin,  
  CAST(LEFT(cast(promised_date_time AS sql_char), 4) + '-' 
    + substring(cast(promised_date_time AS sql_char),5,2) + '-' 
    + substring(cast(promised_date_time AS sql_char),7,2) 
    + ' ' + substring(cast(promised_date_time AS sql_char),9,2) 
    + ':'+ substring(cast(promised_date_time AS sql_char),11,2) + ':' + '00' AS sql_timestamp), 
  ptline, ptltyp, ptseq, ptcmnt
FROM ext_xtime_appointments 
ORDER BY promised_date_time DESC, pending_key, ptline, ptseq# 


-- need to concatenate ptcmnt INTO a single row

SELECT pending_key, ptline, ptseq#, ptltyp, ptcmnt
FROM ext_xtime_appointments 
WHERE ptcmnt <> ''
ORDER BY pending_key DESC, ptline, ptseq# 

DROP TABLE tmp_comment;
CREATE TABLE tmp_xtime_comment (
  pending_key integer,
  comment memo) IN database;

-- DELETE FROM tmp_xtime_comment;
DECLARE @key integer;
DECLARE @key_cur CURSOR AS 
  SELECT distinct pending_key
  FROM ext_xtime_appointments 
  WHERE ptcmnt <> '';
DECLARE @line integer;  
DECLARE @line_cur CURSOR AS 
  SELECT distinct ptline
  FROM ext_xtime_appointments
  WHERE pending_key = @key
  ORDER BY ptline;  
DECLARE @cur CURSOR AS 
  SELECT ptcmnt
  FROM ext_xtime_appointments
  WHERE pending_key = @key
    AND ptline = @line
  ORDER BY ptseq;
DECLARE @string string; 

@string = '';
OPEN @key_cur;
TRY
  WHILE FETCH @key_cur DO
    @key = @key_cur.pending_key;
    OPEN @line_cur;
    TRY
      WHILE FETCH @line_cur DO
--        @string = TRIM(@string) + TRIM(@cur.ptcmnt);
        @line = @line_cur.ptline;
        OPEN @cur;
        TRY
          WHILE FETCH @cur DO
            @string = TRIM(@string) + TRIM(@cur.ptcmnt);
          END WHILE;
        FINALLY 
          CLOSE @cur;
        END TRY;
        @string = @string + CHAR(13) + char(10);
      END WHILE;
    FINALLY
      CLOSE @line_cur;
    END TRY;
    TRY
      INSERT INTO tmp_xtime_comment values(@key, @string);
      @string = '';
    CATCH ALL
      RAISE OMFG(100, CAST(@key AS sql_char) + ' ' + @string + ' ' + __errtext);
    END TRY;
  END WHILE;
FINALLY
  CLOSE @key_cur;
END TRY; 
  
select * FROM ext_xtime_appointments
  
select * FROM tmp_comment;

SELECT pending_key FROM tmp_comment GROUP BY pending_key HAVING COUNT(*) > 1

select b.*, c.comment
FROM (
  SELECT company_number, pending_key, date_time_created, cust_name, cust_phone_no, vin, 
    appt_date_time
  FROM (  
    select company_number, pending_key, 
--      cast(LEFT(cast(open_tran_date AS sql_char), 4) + '-' 
--        + substring(cast(open_tran_date AS sql_char), 5,2)
--        + '-' + substring(cast(open_tran_date AS sql_char),7,2) AS sql_date) AS date_time_created,
      CAST(doc_create_timestamp AS sql_timestamp) AS date_time_created,
      cust_name, cust_phone_no, vin,  
      CAST(LEFT(cast(promised_date_time AS sql_char), 4) + '-' 
        + substring(cast(promised_date_time AS sql_char),5,2) + '-' 
        + substring(cast(promised_date_time AS sql_char),7,2) 
        + ' ' + substring(cast(promised_date_time AS sql_char),9,2) 
        + ':'+ substring(cast(promised_date_time AS sql_char),11,2) 
        + ':' + '00' AS sql_timestamp) AS appt_date_time
    FROM ext_xtime_appointments) a
  GROUP BY company_number, pending_key, date_time_created, cust_name, cust_phone_no, vin, 
    appt_date_time) b  
LEFT JOIN tmp_xtime_comment c on b.pending_key = c.pending_key
ORDER BY b.pending_key


select cust_phone_no FROM ext_xtime_appointments

select distinct length(cast(cust_phone_no as sql_varchar)) FROM ext_xtime_appointments
 

select b.*, c.comment
FROM (
  SELECT company_number, pending_key, date_time_created, cust_name, 
    cast(cust_phone_no AS sql_char), vin, 
    appt_date_time
  FROM (  
    select company_number, pending_key, 
      CAST(doc_create_timestamp AS sql_timestamp) AS date_time_created,
      cust_name, cust_phone_no, vin,  
      CAST(LEFT(cast(promised_date_time AS sql_char), 4) + '-' 
        + substring(cast(promised_date_time AS sql_char),5,2) + '-' 
        + substring(cast(promised_date_time AS sql_char),7,2) 
        + ' ' + substring(cast(promised_date_time AS sql_char),9,2) 
        + ':'+ substring(cast(promised_date_time AS sql_char),11,2) 
        + ':' + '00' AS sql_timestamp) AS appt_date_time
    FROM ext_xtime_appointments) a
  GROUP BY company_number, pending_key, date_time_created, cust_name, cust_phone_no, vin, 
    appt_date_time) b  
LEFT JOIN tmp_xtime_comment c on b.pending_key = c.pending_key
WHERE CAST(appt_date_time AS sql_date) >= curdate();

-- 5/14/15 ADD cust address AND vehicle info
DROP TABLE xtime_appointments;
CREATE TABLE xtime_appointments (
  store_code cichar(3) constraint NOT NULL,
  pending_key integer constraint NOT NULL,
  created timestamp constraint NOT NULL,
  customer cichar(40) constraint NOT NULL,
  customer_phone cichar(30),
  vin char(17),
  startTime timestamp constraint NOT NULL,
  notes memo constraint NOT NULL,
  advisorID cichar(40),
  streetAddress cichar(45),
  city cichar(35),
  state cichar(2),
  zip cichar(10),
  modelYear integer,
  make cichar(25),
  model cichar(25),
  CONSTRAINT PK PRIMARY KEY (pending_key)) IN DATABASE;

ALTER PROCEDURE xfm_xtime_appointments()
BEGIN  
/* 
stored proc for real time updating
only current AND future appointments are needed

EXECUTE PROCEDURE xfm_xtime_appointments();

*/
DECLARE @key integer;
DECLARE @key_cur CURSOR AS 
  SELECT distinct pending_key
  FROM ext_xtime_appointments 
  WHERE ptcmnt <> '';
DECLARE @line integer;  
DECLARE @line_cur CURSOR AS 
  SELECT distinct ptline
  FROM ext_xtime_appointments
  WHERE pending_key = @key
  ORDER BY ptline;  
DECLARE @cur CURSOR AS 
  SELECT ptcmnt
  FROM ext_xtime_appointments
  WHERE pending_key = @key
    AND ptline = @line
  ORDER BY ptseq;
DECLARE @string string;  
BEGIN TRANSACTION;
TRY 
  DELETE FROM tmp_xtime_comment;
  @string = '';
  OPEN @key_cur;
  TRY
    WHILE FETCH @key_cur DO
      @key = @key_cur.pending_key;
      OPEN @line_cur;
      TRY
        WHILE FETCH @line_cur DO
          @line = @line_cur.ptline;
          OPEN @cur;
          TRY
            WHILE FETCH @cur DO
              @string = TRIM(@string) + TRIM(@cur.ptcmnt);
            END WHILE;
          FINALLY 
            CLOSE @cur;
          END TRY;
          @string = @string + CHAR(13) + char(10);
        END WHILE;
      FINALLY
        CLOSE @line_cur;
      END TRY;
      TRY
        INSERT INTO tmp_xtime_comment values(@key, @string);
        @string = '';
      CATCH ALL
        RAISE OMFG(100, CAST(@key AS sql_char) + ' ' + @string + ' ' + __errtext);
      END TRY;
    END WHILE;
  FINALLY
    CLOSE @key_cur;
  END TRY; 
  DELETE FROM xtime_appointments;  
  INSERT INTO xtime_appointments (store_code,pending_key,created,customer,
    customer_phone,vin,startTime,notes,advisorID)
  select b.company_number, b.pending_key, b.date_time_created, b.cust_name,
  customer_phone, vin, appt_date_time, c.comment, b.advisorID
  FROM (
    SELECT company_number, pending_key, date_time_created, cust_name, 
      cast(cust_phone_no AS sql_char) AS customer_phone, vin, 
      appt_date_time, advisorID
    FROM (  
      select company_number, pending_key, 
        CAST(doc_create_timestamp AS sql_timestamp) AS date_time_created,
        cust_name, cust_phone_no, vin, advisorID, 
        CAST(LEFT(cast(promised_date_time AS sql_char), 4) + '-' 
          + substring(cast(promised_date_time AS sql_char),5,2) + '-' 
          + substring(cast(promised_date_time AS sql_char),7,2) 
          + ' ' + substring(cast(promised_date_time AS sql_char),9,2) 
          + ':'+ substring(cast(promised_date_time AS sql_char),11,2) 
          + ':' + '00' AS sql_timestamp) AS appt_date_time
      FROM ext_xtime_appointments) a
    GROUP BY company_number, pending_key, date_time_created, cust_name, cust_phone_no, vin, 
      appt_date_time, advisorID) b  
  LEFT JOIN tmp_xtime_comment c on b.pending_key = c.pending_key
  WHERE CAST(appt_date_time AS sql_date) >= curdate();
COMMIT WORK;  	  
CATCH ALL
  ROLLBACK;
  RAISE xtime_appts(1, __errtext);   
END TRY;
  
END;
  
  