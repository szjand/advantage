/*
Creates a new Advantage Data Dictionary

Syntax

CREATE DATABASE <database name> [ PASSWORD <adssys password> ] [ DESCRIPTION <database description> ]     [ ENCRYPT <TRUE | FALSE> ] [ ENCRYPTIONTYPE AES128 | AES256 | RC4 ] 

[ DDPASSWORD <dictionary password> ]

 

database name ::= The path of the new data dictionary

adssys password ::= A password for the adssys administrator user

database description ::= A description of the data dictionary

dictionary password ::= Password for table and dictionary encryption when using AES128 or AES256 encryption

Remarks

CREATE DATABASE creates a new Advantage Data Dictionary and sets its description and password. By including the ENCRYPT keyword, the data dictionary (the .ADD file) will be encrypted if TRUE is specified. If FALSE is specified, it will not be encrypted (the default is FALSE).

The default encryption type is RC4. If one of the AES encryption types is used, then the DDPassword option (dictionary password) must also be provided. The dictionary password is used for encrypting the dictionary files (if the ENCRYPT TRUE option is provided) and any tables in the dictionary that are encrypted.

The encryption type of an existing data dictionary (and whether or not the dictionary is encrypted) can be changed via the sp_SetDDEncryptionType system procedure.

When called on a dictionary connection, CREATE DATABASE can only be executed by the ADSSYS user or a member of the DB:admin group.

Example(s)

CREATE DATABASE "n:\MyData\myData.ADD" PASSWORD 'admin' DESCRIPTION 'MyData Data Dictionary' ENCRYPT TRUE                        EncryptionType AES256 DDPassword 'ddpass';

CREATE DATABASE "c:\program files\my data\new.add";

CREATE DATABASE newDB;
*/
CREATE DATABASE "\\10.130.197.81\Advantage\RydellService\RydellService.add" ENCRYPT False;
EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Version_Major',
        '0'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Version_Minor',
        '0'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Default_Table_Path',
        '.\'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Temp_Table_Path',
        '.\'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Log_In_Required',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Verify_Access_Rights',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Encrypt_Table_Password',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Encrypt_New_Table',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Enable_Internet',
        'True'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Internet_Security_Level',
        '2'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Max_Failed_Attempts',
        '5'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Logins_Disabled',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Logins_Disabled_Msg',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Comment',
        '
'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'User_Defined_Prop',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'FTS_Delimiters',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'FTS_Noise_Words',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'FTS_Drop_Chars',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'FTS_Conditional_Chars',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Encrypt_Indexes',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Query_Log_Table',
        ''
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Encrypt_Communication',
        'False'
      );

EXECUTE PROCEDURE
   sp_ModifyDatabase
      (
        'Disable_DLL_Caching',
        'False'
      );

CREATE TABLE Users ( 
      UserID Char( 40 ),
      UserFirstName Char( 25 ),
      UserLastName Char( 25 ),
      Password Char( 40 ),
      LastSignOn TimeStamp,
      Status Char( 1 ),
      Rights Char( 20 ),
      ShortName Char( 20 ),
      Active Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Users',
   'Users.adi',
   'ACTIVE',
   'Active',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Users',
   'Users.adi',
   'USERID',
   'UserID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Users',
   'Users.adi',
   'SHORTNAME',
   'ShortName',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Users',
   'Users.adi',
   'STATUS',
   'Status',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Users',
   'Users.adi',
   'RIGHTS',
   'Rights',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Users',
   'Users.adi',
   'PASSWORD',
   'Password',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Users', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Usersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Users', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Usersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Users', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Usersfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Users', 
      'Active', 'Field_Default_Value', 
      'True', 'APPEND_FAIL', 'Usersfail' ); 

CREATE TABLE Customers ( 
      CustomerID Char( 40 ),
      LastName Char( 40 ),
      FirstName Char( 40 ),
      PhoneNumber Char( 15 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Customers', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Customersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Customers', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Customersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Customers', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Customersfail');

CREATE TABLE Rights ( 
      RightID Char( 40 ),
      [Right] Char( 20 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'Rights', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Rightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Rights', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Rightsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Rights', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Rightsfail');

CREATE TABLE Teams ( 
      TeamID Char( 40 ),
      Name Char( 40 ),
      Writer Char( 40 ),
      Status Char( 1 ),
      DepartmentID Char( 40 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Teams',
   'Teams.adi',
   'TEAMID',
   'TeamID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Teams',
   'Teams.adi',
   'NAME',
   'Name',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Teams',
   'Teams.adi',
   'DEPARTMENTID',
   'DepartmentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Teams', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Teamsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Teams', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Teamsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Teams', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Teamsfail');

CREATE TABLE Advisors ( 
      AdvisorID Char( 40 ),
      LastName Char( 40 ),
      FirstName Char( 40 ),
      ShortName Char( 20 ),
      Status Char( 1 ),
      AdvisorNumber Char( 4 ),
      TeamID Char( 40 ),
      Active Logical,
      Displayed Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Advisors',
   'Advisors.adi',
   'SHORTNAME',
   'ShortName',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Advisors',
   'Advisors.adi',
   'ADVISORID',
   'AdvisorID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Advisors',
   'Advisors.adi',
   'STATUS',
   'Status',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Advisors',
   'Advisors.adi',
   'ACTIVE',
   'Active',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Advisors',
   'Advisors.adi',
   'DISPLAYED',
   'Displayed',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Advisors',
   'Advisors.adi',
   'TEAMID',
   'TeamID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Advisors', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Advisorsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Advisors', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Advisorsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Advisors', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Advisorsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Advisors', 
      'Active', 'Field_Default_Value', 
      'True', 'APPEND_FAIL', 'Advisorsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Advisors', 
      'Displayed', 'Field_Default_Value', 
      'True', 'APPEND_FAIL', 'Advisorsfail' ); 

CREATE TABLE Blocks ( 
      TeamID Char( 40 ),
      SevenAM Logical,
      SevenFifteenAM Logical,
      SevenThirtyAM Logical,
      SevenFourtyFiveAM Logical,
      EightAM Logical,
      EightFifteenAM Logical,
      EightThirtyAM Logical,
      EightFourtyFiveAM Logical,
      NineAM Logical,
      NineFifteenAM Logical,
      NineThirtyAM Logical,
      NineFourtyFiveAM Logical,
      TenAM Logical,
      TenFifteenAM Logical,
      TenThirtyAM Logical,
      TenFourtyFiveAM Logical,
      ElevenAM Logical,
      ElevenFifteenAM Logical,
      ElevenThirtyAM Logical,
      ElevenFourtyFiveAM Logical,
      TwelveNoon Logical,
      TwelveFifteenPM Logical,
      TwelveThirtyPM Logical,
      TwelveFourtyFivePM Logical,
      OnePM Logical,
      OneFifteenPM Logical,
      OneThirtyPM Logical,
      OneFourtyFivePM Logical,
      TwoPM Logical,
      TwoFifteenPM Logical,
      TwoThirtyPM Logical,
      TwoFourtyFivePM Logical,
      ThreePM Logical,
      ThreeFifteenPM Logical,
      ThreeThirtyPM Logical,
      ThreeFourtyFivePM Logical,
      FourPM Logical,
      FourFifteenPM Logical,
      FourThirtyPM Logical,
      FourFourtyFivePM Logical,
      FivePM Logical,
      FiveFifteenPM Logical,
      FiveThirtyPM Logical,
      FiveFourtyFivePM Logical,
      SixPM Logical,
      SixFifteenPM Logical,
      SixThirtyPM Logical,
      SixFourtyFivePM Logical,
      SevenPM Logical,
      SevenFifteenPM Logical,
      SevenThirtyPM Logical,
      SevenFourtyFivePM Logical,
      EightPM Logical,
      EightFifteenPM Logical,
      EightThirtyPM Logical,
      EightFourtyFivePM Logical,
      NinePM Logical,
      NineFifteenPM Logical,
      NineThirtyPM Logical,
      NineFourtyFivePM Logical,
      TenPM Logical,
      TenFifteenPM Logical,
      TenThirtyPM Logical,
      TenFourtyFivePM Logical,
      ElevenPM Logical,
      ElevenFifteenPM Logical,
      ElevenThirtyPM Logical,
      ElevenFourtyFivePM Logical,
      TwelveMidnight Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Blocks',
   'Blocks.adi',
   'TEAMID',
   'TeamID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Blocks', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Blocksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Blocks', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Blocksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Blocks', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Blocksfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TeamID', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SevenAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SevenFifteenAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SevenThirtyAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SevenFourtyFiveAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'EightAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'EightFifteenAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'EightThirtyAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'EightFourtyFiveAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'NineAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'NineFifteenAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'NineThirtyAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'NineFourtyFiveAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TenAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TenFifteenAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TenThirtyAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TenFourtyFiveAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ElevenAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ElevenFifteenAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ElevenThirtyAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ElevenFourtyFiveAM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TwelveNoon', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TwelveFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TwelveThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TwelveFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'OnePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'OneFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'OneThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'OneFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TwoPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TwoFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TwoThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TwoFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ThreePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ThreeFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ThreeThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ThreeFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'FourPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'FourFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'FourThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'FourFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'FivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'FiveFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'FiveThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'FiveFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SixPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SixFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SixThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SixFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SevenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SevenFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SevenThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'SevenFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'EightPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'EightFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'EightThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'EightFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'NinePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'NineFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'NineThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'NineFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TenFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TenThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TenFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ElevenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ElevenFifteenPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ElevenThirtyPM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'ElevenFourtyFivePM', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Blocks', 
      'TwelveMidnight', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Blocksfail' ); 

CREATE TABLE Technicians ( 
      TechnicianID Char( 40 ),
      LastName Char( 40 ),
      FirstName Char( 40 ),
      SkillLevel Char( 1 ),
      ShortName Char( 20 ),
      TechNumber Char( 4 ),
      Mon Logical,
      Tue Logical,
      Wed Logical,
      Thr Logical,
      Fri Logical,
      Sat Logical,
      MonStartTime1 Time,
      MonStopTime1 Time,
      MonStartTime2 Time,
      MonStopTime2 Time,
      TueStartTime1 Time,
      TueStopTime1 Time,
      TueStartTime2 Time,
      TueStopTime2 Time,
      WedStartTime1 Time,
      WedStopTime1 Time,
      WedStartTIme2 Time,
      WedStopTime2 Time,
      ThrStartTime1 Time,
      ThrStopTime1 Time,
      ThrStartTime2 Time,
      ThrStopTime2 Time,
      FriStartTime1 Time,
      FriStopTime1 Time,
      FriStartTime2 Time,
      FriStopTime2 Time,
      SatStartTime1 Time,
      SatStopTime1 Time,
      SatStartTime2 Time,
      SatStopTime2 Time,
      TeamID Char( 40 ),
      SecondarySkillLevel1 Char( 1 ),
      SecondarySkillLevel2 Char( 1 ),
      CorrectionFactor Double( 0 ),
      Active Logical,
      Displayed Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Technicians',
   'Technicians.adi',
   'SHORTNAME',
   'ShortName',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Technicians',
   'Technicians.adi',
   'ACTIVE',
   'Active',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Technicians',
   'Technicians.adi',
   'DISPLAYED',
   'Displayed',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Technicians',
   'Technicians.adi',
   'TEAMID',
   'TeamID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Technicians',
   'Technicians.adi',
   'SKILLLEVEL',
   'SkillLevel',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Technicians',
   'Technicians.adi',
   'TECHNUMBER',
   'TechNumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Technicians', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Techniciansfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Technicians', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Techniciansfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Technicians', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Techniciansfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Technicians', 
      'Active', 'Field_Default_Value', 
      'True', 'APPEND_FAIL', 'Techniciansfail' ); 

CREATE TABLE TechnicianByDate ( 
      TechnicianID Char( 40 ),
      Date Date,
      StartTime Time,
      EndTime Time) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'TechnicianByDate', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'TechnicianByDatefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TechnicianByDate', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'TechnicianByDatefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TechnicianByDate', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'TechnicianByDatefail');

CREATE TABLE CommitmentsByDate ( 
      AppointmentID Char( 40 ),
      Date Date,
      SkillLevel Char( 1 ),
      TechnicianID Char( 40 ),
      AnticipatedDuration Integer,
      ActualDuration Integer,
      Comments Memo) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommitmentsByDate', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CommitmentsByDatefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommitmentsByDate', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CommitmentsByDatefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommitmentsByDate', 
   'Table_Memo_Block_Size', 
   '512', 'APPEND_FAIL', 'CommitmentsByDatefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CommitmentsByDate', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CommitmentsByDatefail');

CREATE TABLE CapturedInfo ( 
      DateCreated TimeStamp,
      CreatedByID Char( 40 ),
      Customer Char( 40 ),
      StreetAddress Char( 45 ),
      City Char( 35 ),
      State Char( 2 ),
      Zip Char( 10 ),
      PromiseTime TimeStamp,
      Notes Memo,
      Model Char( 25 ),
      Mileage Integer,
      EMail Char( 35 ),
      ExtendedWarranty Logical,
      Shuttle Char( 5 ),
      HomePhone Char( 30 ),
      HomeExt Char( 8 ),
      BestPhoneToday Char( 30 ),
      BestExt Char( 8 ),
      Make Char( 25 ),
      VIN Char( 17 ),
      ModelYear Integer,
      PurchaseOrder Char( 20 ),
      ModelColor Char( 25 ),
      LicenseNumber Char( 12 ),
      Comments Memo,
      CapturedInfoID Char( 40 ),
      PickupDropoff Logical,
      CellPhone Char( 30 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CapturedInfo',
   'CapturedInfo.adi',
   'CUSTOMER',
   'Upper(Customer)',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CapturedInfo',
   'CapturedInfo.adi',
   'CELLPHONE',
   'CellPhone',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CapturedInfo', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CapturedInfofail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CapturedInfo', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CapturedInfofail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CapturedInfo', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CapturedInfofail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'CapturedInfo', 
      'PickupDropoff', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'CapturedInfofail' ); 

CREATE TABLE ChangeLog ( 
      ChangeLogID Char( 40 ),
      AppointmentID Char( 40 ),
      Customer Char( 40 ),
      CurrentDateTime TimeStamp,
      UserID Char( 40 ),
      AdvisorID Char( 40 ),
      Created TimeStamp,
      Action Char( 40 ),
      NewPromiseTime TimeStamp,
      OldPromiseTime TimeStamp,
      Reason Char( 35 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ChangeLog',
   'ChangeLog.adi',
   'CUSTOMER',
   'Customer',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ChangeLog',
   'ChangeLog.adi',
   'APPOINTMENTID',
   'AppointmentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ChangeLog',
   'ChangeLog.adi',
   'REASON',
   'Reason',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ChangeLog',
   'ChangeLog.adi',
   'CURRENTDATETIME',
   'CurrentDateTime',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ChangeLog',
   'ChangeLog.adi',
   'USERID',
   'UserID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ChangeLog',
   'ChangeLog.adi',
   'ADVISORID',
   'AdvisorID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLog', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ChangeLogfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLog', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ChangeLogfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLog', 
   'Table_Memo_Block_Size', 
   '512', 'APPEND_FAIL', 'ChangeLogfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLog', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ChangeLogfail');

CREATE TABLE Appointments ( 
      AppointmentID Char( 40 ),
      AdvisorID Char( 40 ),
      TechnicianID Char( 40 ),
      Created TimeStamp,
      CreatedByID Char( 40 ),
      Modified TimeStamp,
      ModifiedByID Char( 40 ),
      TagNumber Char( 10 ),
      Status Char( 25 ),
      RONumber Char( 10 ),
      Customer Char( 40 ),
      PromiseTime TimeStamp,
      ResourceID Integer,
      Notes Memo,
      StartTime TimeStamp,
      EndTime TimeStamp,
      HeavyDuration Double( 0 ),
      MaintenanceDuration Double( 0 ),
      DriveabilityDuration Double( 0 ),
      TeamID Char( 40 ),
      Model Char( 25 ),
      Mileage Integer,
      EMail Char( 100 ),
      ExtendedWarranty Logical,
      Shuttle Char( 5 ),
      HomePhone Char( 30 ),
      HomeExt Char( 8 ),
      BestPhoneToday Char( 30 ),
      BestExt Char( 8 ),
      Make Char( 25 ),
      VIN Char( 17 ),
      ModelYear Integer,
      StreetAddress Char( 45 ),
      City Char( 35 ),
      State Char( 2 ),
      Zip Char( 10 ),
      PurchaseOrder Char( 20 ),
      ModelColor Char( 25 ),
      LicenseNumber Char( 12 ),
      Comments Memo,
      TechStatus Char( 25 ),
      Done TimeStamp,
      TechDone TimeStamp,
      PartsStatus Char( 25 ),
      TechComments Memo,
      ArrivalTime TimeStamp,
      DepartureTime TimeStamp,
      PickupDropoff Logical,
      DisableFlashing Logical,
      TOCReleaseTime TimeStamp,
      TOCFinishTime TimeStamp,
      TOCComments Memo,
      TechnicianAssignedID Char( 40 ),
      TOCBufferSize Double( 2 ),
      TOCFinishColor Char( 12 ),
      Requisite Char( 14 ),
      TOCApproval TimeStamp,
      TOCPartsHold TimeStamp,
      TOCInspectDiag TimeStamp,
      TOCReason Char( 50 ),
      CellPhone Char( 30 ),
      OKToEMail Logical,
      EMailSent Logical,
      Alignment Logical,
      Priority Logical) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'ADVISORID',
   'AdvisorID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TECHNICIANID',
   'TechnicianID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'STATUS',
   'Status',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'STARTTIME',
   'StartTime',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'ENDTIME',
   'EndTime',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'RONUMBER',
   'Upper(RONumber)',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TAGNUMBER',
   'Upper(TagNumber)',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'LICENSENUMBER',
   'LicenseNumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'HOMEPHONE',
   'HomePhone',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'BESTPHONETODAY',
   'BestPhoneToday',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'APPOINTMENTID',
   'AppointmentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'STARTDATE',
   'TSTOD(StartTime)',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TECHNICIANASSIGNEDID',
   'TechnicianAssignedID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TOCRELEASETIME',
   'TOCReleaseTime',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TECHSTATUS',
   'TechStatus',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'PROMISETIME',
   'PromiseTime',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'DONE',
   'Done',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TOCBUFFERSIZE',
   'TOCBufferSize',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'REQUISITE',
   'Requisite',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TOCINSPECTDIAG',
   'TOCInspectDiag',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TOCAPPROVAL',
   'TOCApproval',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TOCPARTSHOLD',
   'TOCPartsHold',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TECHDONE',
   'TechDone',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TOCREASON',
   'TOCReason',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'CELLPHONE',
   'CellPhone',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'OKTOEMAIL',
   'OKToEMail',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'EMAILSENT',
   'EMailSent',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'UPPER(CUSTOMER)',
   'Upper(Customer)',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'CUSTOMER',
   'Customer',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'TEAMID',
   'TeamID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'STARTTIMEADVISORID',
   'StartTime;AdvisorID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'ALIGNMENT',
   'Alignment',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Appointments',
   'Appointments.adi',
   'PRIORITY',
   'Priority',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Appointments', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Appointmentsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Appointments', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Appointmentsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Appointments', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Appointmentsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Appointments', 
      'ExtendedWarranty', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Appointmentsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Appointments', 
      'PickupDropoff', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Appointmentsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Appointments', 
      'DisableFlashing', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Appointmentsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Appointments', 
      'Requisite', 'Field_Default_Value', 
      'None', 'APPEND_FAIL', 'Appointmentsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Appointments', 
      'OKToEMail', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Appointmentsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Appointments', 
      'EMailSent', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Appointmentsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Appointments', 
      'Alignment', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Appointmentsfail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'Appointments', 
      'Priority', 'Field_Default_Value', 
      'False', 'APPEND_FAIL', 'Appointmentsfail' ); 

CREATE TRIGGER AfterAppointmentUpdate
   ON Appointments
   AFTER 
   UPDATE 
   BEGIN 
      execute procedure RydellServiceAppointmentUpdated(
  (SELECT CAST(Trim(AppointmentID) AS SQL_CHAR(38)) FROM __new),
  (SELECT Status FROM __New),
  (SELECT TechStatus FROM __NEW)
);
   END 
   NO TRANSACTION 
   PRIORITY 1;

CREATE TABLE Exceptions ( 
      ExceptionID Char( 40 ),
      TechNumber Char( 4 ),
      Hours Double( 0 ),
      Date Date) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Exceptions',
   'Exceptions.adi',
   'TECHNUMBER',
   'TechNumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Exceptions',
   'Exceptions.adi',
   'DATE',
   'Date',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Exceptions',
   'Exceptions.adi',
   'HOURS',
   'Hours',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Exceptions', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Exceptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Exceptions', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Exceptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Exceptions', 
   'Table_Memo_Block_Size', 
   '512', 'APPEND_FAIL', 'Exceptionsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Exceptions', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Exceptionsfail');

CREATE TABLE Alerts ( 
      AlertID Char( 40 ),
      AlertType Char( 25 ),
      Active Logical,
      Flashing Logical,
      Make Char( 25 ),
      Model Char( 25 ),
      ModelYear Integer,
      Icon Char( 25 ),
      Customer Char( 40 ),
      ImageID Integer,
      Miles Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'ACTIVE',
   'Active',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'MAKE',
   'Make',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'MODELYEAR',
   'ModelYear',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Alerts',
   'Alerts.adi',
   'FLASHING',
   'Flashing',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Alerts', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Alerts', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Alertsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Alerts', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Alertsfail');

CREATE TABLE Categories ( 
      CategoryID Char( 40 ),
      Category CIChar( 25 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Categories',
   'Categories.adi',
   'CATEGORYID',
   'CategoryID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Categories',
   'Categories.adi',
   'CATEGORY',
   'Category',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Categories', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Categoriesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Categories', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Categoriesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Categories', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Categoriesfail');

CREATE TABLE CategoryStatuses ( 
      CategoryStatusID Char( 40 ),
      CategoryID Char( 40 ),
      CategoryStatus CIChar( 60 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CategoryStatuses',
   'CategoryStatuses.adi',
   'CATEGORYID',
   'CategoryID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CategoryStatuses',
   'CategoryStatuses.adi',
   'CATEGORYSTATUSID',
   'CategoryStatusID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CategoryStatuses',
   'CategoryStatuses.adi',
   'CATEGORYSTATUS',
   'CategoryStatus',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CategoryStatuses', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CategoryStatusesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CategoryStatuses', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CategoryStatusesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CategoryStatuses', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CategoryStatusesfail');

CREATE TABLE AppointmentStatuses ( 
      AppointmentID Char( 40 ),
      CategoryID Char( 40 ),
      CategoryStatusID Char( 40 ),
      FromTimeStamp TimeStamp,
      ThruTimeStamp TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AppointmentStatuses',
   'AppointmentStatuses.adi',
   'APPOINTMENTIDCATEGORYTHRU',
   'AppointmentID;CategoryID;ThruTimeStamp',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AppointmentStatuses',
   'AppointmentStatuses.adi',
   'CATEGORYID',
   'CategoryID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AppointmentStatuses',
   'AppointmentStatuses.adi',
   'CATEGORYSTATUSID',
   'CategoryStatusID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AppointmentStatuses',
   'AppointmentStatuses.adi',
   'FROMTIMESTAMP',
   'FromTimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AppointmentStatuses',
   'AppointmentStatuses.adi',
   'THRUTIMESTAMP',
   'ThruTimeStamp',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AppointmentStatuses',
   'AppointmentStatuses.adi',
   'APPOINTMENTID',
   'AppointmentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentStatuses', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'AppointmentStatusesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentStatuses', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AppointmentStatusesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentStatuses', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AppointmentStatusesfail');

CREATE TABLE AdvisorBlocks ( 
      ShortName Char( 20 ),
      TimeBlock Time) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AdvisorBlocks',
   'AdvisorBlocks.adi',
   'ADVISORIDTIMEBLOCK',
   'ShortName;TimeBlock',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AdvisorBlocks',
   'AdvisorBlocks.adi',
   'SHORTNAME',
   'ShortName',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AdvisorBlocks',
   'AdvisorBlocks.adi',
   'TIMEBLOCK',
   'TimeBlock',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'AdvisorBlocks', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'AdvisorBlocksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AdvisorBlocks', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AdvisorBlocksfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AdvisorBlocks', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AdvisorBlocksfail');

CREATE TABLE AppointmentFieldsToCheck ( 
      FieldName Char( 40 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AppointmentFieldsToCheck',
   'AppointmentFieldsToCheck.adi',
   'FIELDNAME',
   'FieldName',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentFieldsToCheck', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'AppointmentFieldsToCheckfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentFieldsToCheck', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AppointmentFieldsToCheckfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentFieldsToCheck', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AppointmentFieldsToCheckfail');

CREATE TABLE Departments ( 
      DepartmentID Char( 40 ),
      Description Char( 40 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Departments',
   'Departments.adi',
   'DEPARTMENTID',
   'DepartmentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Departments',
   'Departments.adi',
   'DESCRIPTION',
   'Description',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Departments', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Departmentsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Departments', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Departmentsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Departments', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'Departmentsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Departments', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Departmentsfail');

CREATE TABLE AppointmentsArchived ( 
      AppointmentID Char( 40 ),
      AdvisorID Char( 40 ),
      TechnicianID Char( 40 ),
      Created TimeStamp,
      CreatedByID Char( 40 ),
      Modified TimeStamp,
      ModifiedByID Char( 40 ),
      TagNumber Char( 10 ),
      Status Char( 25 ),
      RONumber Char( 10 ),
      Customer Char( 40 ),
      PromiseTime TimeStamp,
      ResourceID Integer,
      Notes Memo,
      StartTime TimeStamp,
      EndTime TimeStamp,
      HeavyDuration Double( 0 ),
      MaintenanceDuration Double( 0 ),
      DriveabilityDuration Double( 0 ),
      TeamID Char( 40 ),
      Model Char( 25 ),
      Mileage Integer,
      EMail Char( 100 ),
      ExtendedWarranty Logical,
      Shuttle Char( 5 ),
      HomePhone Char( 30 ),
      HomeExt Char( 8 ),
      BestPhoneToday Char( 30 ),
      BestExt Char( 8 ),
      Make Char( 25 ),
      VIN Char( 17 ),
      ModelYear Integer,
      StreetAddress Char( 45 ),
      City Char( 35 ),
      State Char( 2 ),
      Zip Char( 10 ),
      PurchaseOrder Char( 20 ),
      ModelColor Char( 25 ),
      LicenseNumber Char( 12 ),
      Comments Memo,
      TechStatus Char( 25 ),
      Done TimeStamp,
      TechDone TimeStamp,
      PartsStatus Char( 25 ),
      TechComments Memo,
      ArrivalTime TimeStamp,
      DepartureTime TimeStamp,
      PickupDropoff Logical,
      DisableFlashing Logical,
      TOCReleaseTime TimeStamp,
      TOCFinishTime TimeStamp,
      TOCComments Memo,
      TechnicianAssignedID Char( 40 ),
      TOCBufferSize Double( 2 ),
      TOCFinishColor Char( 12 ),
      Requisite Char( 14 ),
      TOCApproval TimeStamp,
      TOCPartsHold TimeStamp,
      TOCInspectDiag TimeStamp,
      TOCReason Char( 50 ),
      CellPhone Char( 30 ),
      OKToEMail Logical,
      EMailSent Logical,
      Alignment Logical,
      Priority Logical) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentsArchived', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'AppointmentsArchivedfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentsArchived', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AppointmentsArchivedfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentsArchived', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AppointmentsArchivedfail');

CREATE TABLE VehicleStatuses ( 
      VehicleStatusID Char( 40 ),
      VehicleStatus Char( 30 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'VehicleStatuses',
   'VehicleStatuses.adi',
   'VEHICLESTATUSID',
   'VehicleStatusID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'VehicleStatuses',
   'VehicleStatuses.adi',
   'VEHICLESTATUS',
   'VehicleStatus',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'VehicleStatuses', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'VehicleStatusesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'VehicleStatuses', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'VehicleStatusesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'VehicleStatuses', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'VehicleStatusesfail');

CREATE TABLE CorrectionFactors ( 
      Heavy Double( 15 ),
      Driveability Double( 15 ),
      Maintenance Double( 15 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'CorrectionFactors',
   'CorrectionFactors.adi',
   'HEAVY',
   'Heavy',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CorrectionFactors',
   'CorrectionFactors.adi',
   'DRIVEABILITY',
   'Driveability',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'CorrectionFactors',
   'CorrectionFactors.adi',
   'MAINTENANCE',
   'Maintenance',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'CorrectionFactors', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'CorrectionFactorsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CorrectionFactors', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'CorrectionFactorsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'CorrectionFactors', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'CorrectionFactorsfail');

CREATE TABLE HoursAvailableForTheDay ( 
      DriveabilityHours Double( 15 ),
      MaintenanceHours Double( 15 ),
      HeavyHours Double( 15 ),
      LastViewed TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'HoursAvailableForTheDay', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'HoursAvailableForTheDayfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'HoursAvailableForTheDay', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'HoursAvailableForTheDayfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'HoursAvailableForTheDay', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'HoursAvailableForTheDayfail');

CREATE TABLE AppointmentsBackup ( 
      AppointmentID Char( 40 ),
      AdvisorID Char( 40 ),
      TechnicianID Char( 40 ),
      Created TimeStamp,
      CreatedByID Char( 40 ),
      Modified TimeStamp,
      ModifiedByID Char( 40 ),
      TagNumber Char( 10 ),
      Status Char( 25 ),
      RONumber Char( 10 ),
      Customer Char( 40 ),
      PromiseTime TimeStamp,
      ResourceID Integer,
      Notes Memo,
      StartTime TimeStamp,
      EndTime TimeStamp,
      HeavyDuration Double( 0 ),
      MaintenanceDuration Double( 0 ),
      DriveabilityDuration Double( 0 ),
      TeamID Char( 40 ),
      Model Char( 25 ),
      Mileage Integer,
      EMail Char( 35 ),
      ExtendedWarranty Logical,
      Shuttle Char( 5 ),
      HomePhone Char( 30 ),
      HomeExt Char( 8 ),
      BestPhoneToday Char( 30 ),
      BestExt Char( 8 ),
      Make Char( 25 ),
      VIN Char( 17 ),
      ModelYear Integer,
      StreetAddress Char( 45 ),
      City Char( 35 ),
      State Char( 2 ),
      Zip Char( 10 ),
      PurchaseOrder Char( 20 ),
      ModelColor Char( 25 ),
      LicenseNumber Char( 12 ),
      Comments Memo,
      TechStatus Char( 25 ),
      Done TimeStamp,
      TechDone TimeStamp,
      PartsStatus Char( 25 ),
      TechComments Memo,
      ArrivalTime TimeStamp,
      DepartureTime TimeStamp,
      PickupDropoff Logical,
      DisableFlashing Logical,
      TOCReleaseTime TimeStamp,
      TOCFinishTime TimeStamp,
      TOCComments Memo,
      TechnicianAssignedID Char( 40 ),
      TOCBufferSize Double( 2 ),
      TOCFinishColor Char( 12 ),
      Requisite Char( 14 ),
      TOCApproval TimeStamp,
      TOCPartsHold TimeStamp,
      TOCInspectDiag TimeStamp,
      TOCReason Char( 50 ),
      CellPhone Char( 30 ),
      OKToEMail Logical,
      EMailSent Logical) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentsBackup', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'AppointmentsBackupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentsBackup', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AppointmentsBackupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentsBackup', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AppointmentsBackupfail');

CREATE TABLE ChangeLogArchive ( 
      ChangeLogID Char( 40 ),
      AppointmentID Char( 40 ),
      Customer Char( 40 ),
      CurrentDateTime TimeStamp,
      UserID Char( 40 ),
      AdvisorID Char( 40 ),
      Created TimeStamp,
      Action Char( 40 ),
      NewPromiseTime TimeStamp,
      OldPromiseTime TimeStamp,
      Reason Char( 35 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLogArchive', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ChangeLogArchivefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLogArchive', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ChangeLogArchivefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLogArchive', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ChangeLogArchivefail');

CREATE TABLE ChangeLogBackup ( 
      ChangeLogID Char( 40 ),
      AppointmentID Char( 40 ),
      Customer Char( 40 ),
      CurrentDateTime TimeStamp,
      UserID Char( 40 ),
      AdvisorID Char( 40 ),
      Created TimeStamp,
      Action Char( 40 ),
      NewPromiseTime TimeStamp,
      OldPromiseTime TimeStamp,
      Reason Char( 35 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLogBackup', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ChangeLogBackupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLogBackup', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ChangeLogBackupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ChangeLogBackup', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ChangeLogBackupfail');

CREATE TABLE AppointmentStatusesArchive ( 
      AppointmentID Char( 40 ),
      CategoryID Char( 40 ),
      CategoryStatusID Char( 40 ),
      FromTimeStamp TimeStamp,
      ThruTimeStamp TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentStatusesArchive', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'AppointmentStatusesArchivefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentStatusesArchive', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AppointmentStatusesArchivefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentStatusesArchive', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AppointmentStatusesArchivefail');

CREATE TABLE AppointmentStatusesBackup ( 
      AppointmentID Char( 40 ),
      CategoryID Char( 40 ),
      CategoryStatusID Char( 40 ),
      FromTimeStamp TimeStamp,
      ThruTimeStamp TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentStatusesBackup', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'AppointmentStatusesBackupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentStatusesBackup', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AppointmentStatusesBackupfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AppointmentStatusesBackup', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AppointmentStatusesBackupfail');

CREATE TABLE InitialGuess ( 
      Heavy Double( 15 ),
      Driveability Double( 15 ),
      Maintenance Double( 15 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'InitialGuess',
   'InitialGuess.adi',
   'HEAVY',
   'Heavy',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'InitialGuess',
   'InitialGuess.adi',
   'DRIVEABILITY',
   'Driveability',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'InitialGuess',
   'InitialGuess.adi',
   'MAINTENANCE',
   'Maintenance',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'InitialGuess', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'InitialGuessfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'InitialGuess', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'InitialGuessfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'InitialGuess', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'InitialGuessfail');

CREATE TABLE Weekends ( 
      EntryID Char( 40 ),
      Name Char( 40 ),
      Date Date) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Weekends',
   'Weekends.adi',
   'DATE',
   'Date',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Weekends',
   'Weekends.adi',
   'NAME',
   'Name',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Weekends', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Weekendsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Weekends', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Weekendsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Weekends', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Weekendsfail');

CREATE TABLE WeekendsAftermarket ( 
      EntryID Char( 40 ),
      Name Char( 40 ),
      Date Date,
      TechHours Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'WeekendsAftermarket',
   'WeekendsAftermarket.adi',
   'DATE',
   'Date',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'WeekendsAftermarket',
   'WeekendsAftermarket.adi',
   'NAME',
   'Name',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'WeekendsAftermarket', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'WeekendsAftermarketfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'WeekendsAftermarket', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'WeekendsAftermarketfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'WeekendsAftermarket', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'WeekendsAftermarketfail');

CREATE TABLE ExceptionsAftermarket ( 
      ExceptionID Char( 40 ),
      TechNumber Char( 4 ),
      Hours Double( 0 ),
      Date Date) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ExceptionsAftermarket',
   'ExceptionsAftermarket.adi',
   'TECHNUMBER',
   'TechNumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ExceptionsAftermarket',
   'ExceptionsAftermarket.adi',
   'DATE',
   'Date',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ExceptionsAftermarket',
   'ExceptionsAftermarket.adi',
   'HOURS',
   'Hours',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ExceptionsAftermarket', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ExceptionsAftermarketfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ExceptionsAftermarket', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ExceptionsAftermarketfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ExceptionsAftermarket', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ExceptionsAftermarketfail');

CREATE TABLE ExceptionsNight ( 
      ExceptionID Char( 40 ),
      TechNumber Char( 4 ),
      Hours Double( 0 ),
      Date Date) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ExceptionsNight',
   'ExceptionsNight.adi',
   'TECHNUMBER',
   'TechNumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ExceptionsNight',
   'ExceptionsNight.adi',
   'DATE',
   'Date',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ExceptionsNight', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ExceptionsNightfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ExceptionsNight', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ExceptionsNightfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ExceptionsNight', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ExceptionsNightfail');

CREATE TABLE ExceptionsQuickService ( 
      ExceptionID Char( 40 ),
      TechNumber Char( 4 ),
      Hours Double( 0 ),
      Date Date) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ExceptionsQuickService',
   'ExceptionsQuickService.adi',
   'TECHNUMBER',
   'TechNumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ExceptionsQuickService',
   'ExceptionsQuickService.adi',
   'DATE',
   'Date',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ExceptionsQuickService',
   'ExceptionsQuickService.adi',
   'HOURS',
   'Hours',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ExceptionsQuickService', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ExceptionsQuickServicefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ExceptionsQuickService', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ExceptionsQuickServicefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ExceptionsQuickService', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ExceptionsQuickServicefail');

CREATE TABLE Notes ( 
      Date Date,
      Notes Memo) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Notes',
   'Notes.adi',
   'DATE',
   'Date',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Notes', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Notesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Notes', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Notesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Notes', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Notesfail');

CREATE TABLE PreAuthorizationAmount ( 
      AppointmentID Char( 40 ),
      PreAuthorizationCreatedByID Char( 40 ),
      PreAuthorizationAmount Numeric( 12 ,2 ),
      PreAuthorizationLocation Char( 40 ),
      PreAuthorizationNotes Memo,
      FromTS TimeStamp,
      ThruTS TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'PreAuthorizationAmount',
   'PreAuthorizationAmount.adi',
   'APPOINTMENTID',
   'AppointmentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'PreAuthorizationAmount',
   'PreAuthorizationAmount.adi',
   'PREAUTHORIZATIONCREATEDBYID',
   'PreAuthorizationCreatedByID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'PreAuthorizationAmount',
   'PreAuthorizationAmount.adi',
   'PREAUTHORIZATIONAMOUNT',
   'PreAuthorizationAmount',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'PreAuthorizationAmount',
   'PreAuthorizationAmount.adi',
   'PREAUTHORIZATIONLOCATION',
   'PreAuthorizationLocation',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'PreAuthorizationAmount',
   'PreAuthorizationAmount.adi',
   'THRUTS',
   'ThruTS',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'PreAuthorizationAmount',
   'PreAuthorizationAmount.adi',
   'FROMTS',
   'FromTS',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'PreAuthorizationAmount', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'PreAuthorizationAmountfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PreAuthorizationAmount', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'PreAuthorizationAmountfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PreAuthorizationAmount', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'PreAuthorizationAmountfail');

CREATE TABLE Statuses ( 
      AppointmentID Char( 40 ),
      CategoryStatusID Char( 40 ),
      PerformedBy Char( 40 ),
      Comments Memo,
      DateTime TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Statuses',
   'Statuses.adi',
   'APPOINTMENTID',
   'AppointmentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Statuses',
   'Statuses.adi',
   'CATEGORYSTATUSID',
   'CategoryStatusID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Statuses',
   'Statuses.adi',
   'PERFORMEDBY',
   'PerformedBy',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Statuses',
   'Statuses.adi',
   'DATETIME',
   'DateTime',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Statuses', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Statusesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Statuses', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Statusesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Statuses', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Statusesfail');

CREATE TABLE Goals ( 
      GoalID Char( 40 ),
      GoalDate Date,
      Goal Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Goals',
   'Goals.adi',
   'GOALID',
   'GoalID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Goals',
   'Goals.adi',
   'GOALDATE',
   'GoalDate',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Goals',
   'Goals.adi',
   'GOAL',
   'Goal',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Goals', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Goalsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Goals', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Goalsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Goals', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Goalsfail');

CREATE TABLE AssignedJobs ( 
      AppointmentID Char( 40 ),
      TechnicianID Char( 40 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AssignedJobs',
   'AssignedJobs.adi',
   'APPOINTMENTID',
   'AppointmentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AssignedJobs',
   'AssignedJobs.adi',
   'TECHNICIANID',
   'TechnicianID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'AssignedJobs',
   'AssignedJobs.adi',
   'APPOINTMENTIDTECHNICIANID',
   'AppointmentID;TechnicianID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignedJobs', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'AssignedJobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignedJobs', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'AssignedJobsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'AssignedJobs', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'AssignedJobsfail');

CREATE TABLE TechniciansAvailable ( 
      TechnicianID Char( 40 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'TechniciansAvailable',
   'TechniciansAvailable.adi',
   'TECHNICIANID',
   'TechnicianID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'TechniciansAvailable', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'TechniciansAvailablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TechniciansAvailable', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'TechniciansAvailablefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'TechniciansAvailable', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'TechniciansAvailablefail');

CREATE TABLE RepairOrders ( 
      [RO#] Char( 16 ),
      Date Date,
      Tech Char( 10 ),
      Hours Double( 2 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'RepairOrders', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'RepairOrdersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'RepairOrders', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'RepairOrdersfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'RepairOrders', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'RepairOrdersfail');

CREATE TABLE [__ads] ( 
      UpdateTime TimeStamp,
      EntryID AutoInc,
      TxnID Integer,
      TxnReady Integer,
      ThisDBID Integer,
      AllDBID Blob,
      TableName VarChar( 30 ),
      CRC Integer,
      CompatLock Logical,
      IdentColumns Blob,
      RecordNumber Integer,
      UpdateType Char( 1 ),
      RecordData Blob,
      BlobData Blob) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   '__ads',
   'ads.adi',
   'TXNREADY',
   'TxnReady',
   'TxnReady <> NULL',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   '__ads',
   'ads.adi',
   'TXNENTRY',
   'TxnID;EntryID',
   'TxnID <> NULL',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( '__ads', 
   'Table_Auto_Create', 
   'True', 'APPEND_FAIL', '__adsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( '__ads', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', '__adsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( '__ads', 
   'Table_Memo_Block_Size', 
   '64', 'APPEND_FAIL', '__adsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( '__ads', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', '__adsfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( '__ads', 
      'UpdateTime', 'Field_Default_Value', 
      'now()', 'APPEND_FAIL', '__adsfail' ); 

CREATE TABLE EMailNotifications ( 
      PartyID Char( 127 ),
      PartyIDList Memo,
      EmailAddresses Memo,
      Subject CIChar( 200 ),
      MessageBody Memo,
      Attachment Blob,
      FromEMailAddress CIChar( 127 ),
      CreationTS TimeStamp,
      DoneTS TimeStamp,
      ErrorMessage Memo) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'EMailNotifications',
   'EMailNotifications.adi',
   'PARTYID',
   'PartyID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'EMailNotifications',
   'EMailNotifications.adi',
   'SUBJECT',
   'Subject',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'EMailNotifications',
   'EMailNotifications.adi',
   'CREATIONTS',
   'CreationTS',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'EMailNotifications',
   'EMailNotifications.adi',
   'DONETS',
   'DoneTS',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'EMailNotifications', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'EMailNotificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'EMailNotifications', 
   'Table_Default_Index', 
   'CreationTS', 'APPEND_FAIL', 'EMailNotificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'EMailNotifications', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'EMailNotificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'EMailNotifications', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'EMailNotificationsfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'EMailNotifications', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'EMailNotificationsfail');

CREATE TRIGGER AfterEMailNotificationsInsert
   ON EMailNotifications
   AFTER 
   INSERT 
   BEGIN 
      EXECUTE PROCEDURE sp_SignalEvent('EMailNotification', False, 0);
   
   
   END 
   NO TRANSACTION 
   PRIORITY 1;

CREATE TABLE ServiceAppointmentsOfInterest ( 
      AppointmentID Char( 40 ),
      InterestedPartyID Char( 38 ),
      AppointmentDispositionStatus Char( 60 ),
      FromTS TimeStamp,
      ThruTS TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ServiceAppointmentsOfInterest',
   'ServiceAppointmentsOfInterest.adi',
   'APPOINTMENTID',
   'AppointmentID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ServiceAppointmentsOfInterest',
   'ServiceAppointmentsOfInterest.adi',
   'CREATEDTS',
   'FromTS',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ServiceAppointmentsOfInterest',
   'ServiceAppointmentsOfInterest.adi',
   'THRUTS',
   'ThruTS',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ServiceAppointmentsOfInterest',
   'ServiceAppointmentsOfInterest.adi',
   'PK_SERVICEAPPOINTMENTSOFINTEREST',
   'AppointmentID;ThruTS',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ServiceAppointmentsOfInterest', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ServiceAppointmentsOfInterestfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ServiceAppointmentsOfInterest', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ServiceAppointmentsOfInterestfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ServiceAppointmentsOfInterest', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ServiceAppointmentsOfInterestfail');

CREATE TABLE text ( 
      Text CIChar( 50 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'text', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'textfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'text', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'textfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'text', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'textfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'text', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'textfail');

CREATE TRIGGER testewstesetswte
   ON text
   AFTER 
   UPDATE 
   BEGIN 
      insert into test values('hi');
   END 
   PRIORITY 1;

CREATE TABLE test ( 
      text CIChar( 100 )) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'test', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'testfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'test', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'testfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'test', 
   'Table_Memo_Block_Size', 
   '8', 'APPEND_FAIL', 'testfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'test', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'testfail');

CREATE TABLE Anomalies ( 
      IncidentTS TimeStamp,
      Category Char( 60 ),
      CodeSource Memo,
      Description Memo) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'Anomalies',
   'Anomalies.adi',
   'INCIDENTTS',
   'IncidentTS',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'Anomalies',
   'Anomalies.adi',
   'CATEGORY',
   'Category',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'Anomalies', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'Anomaliesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Anomalies', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'Anomaliesfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'Anomalies', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'Anomaliesfail');

CREATE TABLE WeekendsSunday ( 
      EntryID Char( 40 ),
      Name Char( 40 ),
      Date Date) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'WeekendsSunday',
   'WeekendsSunday.adi',
   'DATE',
   'Date',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'WeekendsSunday',
   'WeekendsSunday.adi',
   'NAME',
   'Name',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'WeekendsSunday',
   'WeekendsSunday.adi',
   'ENTRYID',
   'EntryID',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'WeekendsSunday', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'WeekendsSundayfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'WeekendsSunday', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'WeekendsSundayfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'WeekendsSunday', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'WeekendsSundayfail');

CREATE TABLE PreAuthorizationAmountArchive ( 
      AppointmentID Char( 40 ),
      PreAuthorizationCreatedByID Char( 40 ),
      PreAuthorizationAmount Numeric( 12 ,2 ),
      PreAuthorizationLocation Char( 40 ),
      PreAuthorizationNotes Memo,
      FromTS TimeStamp,
      ThruTS TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'PreAuthorizationAmountArchive', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'PreAuthorizationAmountArchivefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PreAuthorizationAmountArchive', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'PreAuthorizationAmountArchivefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'PreAuthorizationAmountArchive', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'PreAuthorizationAmountArchivefail');

CREATE TABLE StatusesArchive ( 
      AppointmentID Char( 40 ),
      CategoryStatusID Char( 40 ),
      PerformedBy Char( 40 ),
      Comments Memo,
      DateTime TimeStamp) IN DATABASE;
EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusesArchive', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'StatusesArchivefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusesArchive', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'StatusesArchivefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'StatusesArchive', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'StatusesArchivefail');

CREATE PROCEDURE DeleteCapturedInfo
   ( 
   ) 
   BEGIN 
                 
  delete from CapturedInfo where [DateCreated] < TimeStampAdd(SQL_TSI_DAY, -1, now()); 
 
   
   END;

CREATE PROCEDURE GetAppointmentsByDate
   ( 
      BeginDate Date,
      EndDate Date,
      StartTime TimeStamp OUTPUT,
      Customer Char ( 40 ) OUTPUT,
      VIN Char ( 17 ) OUTPUT,
      Year Integer OUTPUT,
      Make Char ( 25 ) OUTPUT,
      Model Char ( 25 ) OUTPUT,
      AdvisorID Char ( 40 ) OUTPUT
   ) 
   BEGIN 
       
      
  -- Drop Procedure GetAppointmentsByDate;
  -- Execute Procedure GetAppointmentsByDate('4/5/2008', '4/5/2008');
  declare @Input Cursor as select * from __Input;
  declare @BeginDate Date;
  declare @EndDate Date;

  open @Input;
  try
    fetch @Input;
    @BeginDate = @Input.BeginDate;
	@EndDate = @Input.EndDate;
  finally
    close @Input;
  end;
  
  
--  try
--    drop table #OrderByTable;
--  catch ADS_SCRIPT_EXCEPTION
--    if __ErrCode = 5132 then
--    end if;
--  end try;  

  SELECT Utilities.DropTablesIfExist('#OrderByTable') FROM system.iota;

  Select StartTime, Customer, VIN, ModelYear as Year, Make, Model, AdvisorID 
  into #OrderByTable
  from [Appointments]
  where Convert(StartTime, SQL_DATE) >= @BeginDate
  and Convert(StartTime, SQL_DATE) <= @EndDate
  and Customer <> '' and Upper(Customer) <> 'BUFFER' and Upper(Customer) <> 'CARRY OVER'
  and Upper(Customer) <> 'OFF' and Upper(Customer) <> 'LUNCH' and Upper(Customer) <> 'CLEAN UP'
  and Upper(Customer) <> 'IDL' 
  Order by Convert(StartTime, SQL_DATE), AdvisorID, Make;
  
  insert into [__OUTPUT] Select StartTime, Customer, VIN, Year, Make, Model, AdvisorID from [#OrderByTable];
  
  

   
   END;

CREATE PROCEDURE ChangeStatus
   ( 
      AppointmentID Char ( 40 ),
      CategoryStatus Char ( 40 ),
      ReturnValue Logical OUTPUT
   ) 
   BEGIN 
      

  declare @Input Cursor as select * from __Input;
  declare @AppointmentID String;
  declare @CategoryStatus String;  
  declare @Stmt String;
  declare @ReturnValue String;
  declare @Now String;
  declare @CategoryID String;
  declare @CategoryStatusID String;
  declare @RecordCount Integer;
  declare @RecordCount2 Integer;

  open @Input;
  try
    fetch @Input;
    @AppointmentID = @Input.AppointmentID;
	@CategoryStatus = @Input.CategoryStatus;
  finally
    close @Input;
  end;
  
  -- Get the Current Time/Date
  @Now = Convert(Now(), SQL_CHAR);
  
  -- Get the CategoryID and CategoryStatusID
  @CategoryID = (Select CategoryID from CategoryStatuses where CategoryStatus = @CategoryStatus);
  @CategoryStatusID = (Select CategoryStatusID from CategoryStatuses where CategoryStatus = @CategoryStatus);
  
  -- See if Null Category Thru Stamp exists
  @RecordCount = (Select Count(*) from AppointmentStatuses where AppointmentID = @AppointmentID and CategoryID = @CategoryID
    and ThruTimeStamp is Null);
  @RecordCount2 = (Select Count(*) from AppointmentStatuses where AppointmentID = @AppointmentID and CategoryStatusID = @CategoryStatusID
    and ThruTimeStamp is Null);
	

  try
    -- If @RecordCount > 0 then record exists to check then modify, else create one.
    if @RecordCount > 0 then
      --First if there is an existing CategoryStatusID with a thru stamp of null do nothing.
	  if @RecordCount2 = 0 then
  	    -- First Update the existing TimeStamp with a Thru DateTime
        @Stmt = 'Update AppointmentStatuses set ThruTimeStamp = ''' + @Now + ''' where AppointmentID = ''' +
  	      @AppointmentID + ''' and CategoryID = ''' + @CategoryID + ''' and ThruTimeStamp is Null';	
        execute Immediate @Stmt;	
	
	    -- Now set the Status
        @Stmt = 'Insert Into AppointmentStatuses (AppointmentID, CategoryID, CategoryStatusID, FromTimeStamp, ThruTimeStamp) Values (''' + 
	      @AppointmentID + ''', ''' + @CategoryID + ''', ''' + @CategoryStatusID + ''', ''' + @Now + ''', Null)';
	    execute Immediate @Stmt;
	  endif;
    else
      -- Just start a new record
      @Stmt = 'Insert Into AppointmentStatuses (AppointmentID, CategoryID, CategoryStatusID, FromTimeStamp, ThruTimeStamp) Values (''' + 
	    @AppointmentID + ''', ''' + @CategoryID + ''', ''' + @CategoryStatusID + ''', ''' + @Now + ''', Null)';
	  execute Immediate @Stmt;
    endif;
    insert into [__Output] Values (True);	
  catch all
    insert into [__Output] Values (False);  
  end try; 
 

   END;

CREATE PROCEDURE CreateAppointmentTimeStamp
   ( 
      AppointmentID Char ( 40 ),
      ReturnValue Logical OUTPUT
   ) 
   BEGIN 
       

  declare @Input Cursor as select * from __Input;
  declare @AppointmentID String;
  declare @Stmt String;
  declare @ReturnValue String;
  declare @CategoryID String;
  declare @CategoryStatusID String;
  declare @Now String;
  declare @Count Integer;

  open @Input;
  try
    fetch @Input;
    @AppointmentID = @Input.AppointmentID;
  finally
    close @Input;
  end;
  
  @Now = Convert(Now(), SQL_CHAR);
  
  -- Now Get the Guids
  @CategoryID = (Select CategoryID from Categories where Category = 'Advisor');
  @CategoryStatusID = (Select CategoryStatusID from CategoryStatuses where CategoryStatus = 'Advisor_LeadTime');
  @Count = (Select Count(*) from AppointmentStatuses where AppointmentID = @AppointmentID and CategoryID = @CategoryID
    and CategoryStatusID = @CategoryStatusID and ThruTimestamp is Null);

  -- Insert the Record
  if @Count = 0 then

    @Stmt = 'Insert Into AppointmentStatuses (AppointmentID, CategoryID, CategoryStatusID, FromTimeStamp, ThruTimeStamp) Values (''' + 
	@AppointmentID + ''', ''' + @CategoryID + ''', ''' + @CategoryStatusID + ''', ''' + @Now + ''', Null)';
    execute Immediate @Stmt;
	
    insert into [__Output] Values (True);
  else 
    insert into [__Output] Values (False);   
  endif;


   END;

CREATE PROCEDURE SetStatus
   ( 
      AppointmentID Char ( 40 ),
      DateTimeStr Char ( 40 ),
      CategoryStatus Char ( 40 ),
      ReturnValue Logical OUTPUT
   ) 
   BEGIN 
       
      

  declare @Input Cursor as select * from __Input;
  declare @AppointmentID String;
  declare @DateTimeStr String;
  declare @CategoryStatus String;  
  declare @Stmt String;
  declare @ReturnValue String;
  declare @Now String;
  declare @CategoryID String;
  declare @CategoryStatusID String;
  declare @RecordCount Integer;
  declare @RecordCount2 Integer;

  open @Input;
  try
    fetch @Input;
    @AppointmentID = @Input.AppointmentID;
	@DateTimeStr = @Input.DateTimeStr;
	@CategoryStatus = @Input.CategoryStatus;
  finally
    close @Input;
  end;
  
  -- Get the Current Time/Date
  @Now = Convert(Now(), SQL_CHAR);
  
  -- Get the CategoryID and CategoryStatusID
  @CategoryID = (Select CategoryID from CategoryStatuses where CategoryStatus = @CategoryStatus);
  @CategoryStatusID = (Select CategoryStatusID from CategoryStatuses where CategoryStatus = @CategoryStatus);
  
  -- See if Null Category Thru Stamp exists
  @RecordCount = (Select Count(*) from AppointmentStatuses where AppointmentID = @AppointmentID and CategoryID = @CategoryID
    and ThruTimeStamp is Null);
  @RecordCount2 = (Select Count(*) from AppointmentStatuses where AppointmentID = @AppointmentID and CategoryStatusID = @CategoryStatusID
    and ThruTimeStamp is Null);
	

  try
    -- If @RecordCount > 0 then record exists to check then modify, else create one.
    if @RecordCount > 0 then
      --First if there is an existing CategoryStatusID with a thru stamp of null do nothing.
	  if @RecordCount2 = 0 then
  	    -- First Update the existing TimeStamp with a Thru DateTime
        @Stmt = 'Update AppointmentStatuses set ThruTimeStamp = ''' + @DateTimeStr + ''' where AppointmentID = ''' +
  	      @AppointmentID + ''' and CategoryID = ''' + @CategoryID + ''' and ThruTimeStamp is Null';	
        execute Immediate @Stmt;	
	
	    -- Now set the Status
        @Stmt = 'Insert Into AppointmentStatuses (AppointmentID, CategoryID, CategoryStatusID, FromTimeStamp) Values (''' + 
	      @AppointmentID + ''', ''' + @CategoryID + ''', ''' + @CategoryStatusID + ''', ''' + @DateTimeStr + ''')';
	    execute Immediate @Stmt;
	  else
  	    -- Just Update the existing TimeStamp with a new From DateTime
        @Stmt = 'Update AppointmentStatuses set FromTimeStamp = ''' + @DateTimeStr + ''' where AppointmentID = ''' +
  	      @AppointmentID + ''' and CategoryID = ''' + @CategoryID + ''' and ThruTimeStamp is Null';	
        execute Immediate @Stmt;	  
	  
	  endif;
    else
      -- Just start a new record
      @Stmt = 'Insert Into AppointmentStatuses (AppointmentID, CategoryID, CategoryStatusID, FromTimeStamp) Values (''' + 
	    @AppointmentID + ''', ''' + @CategoryID + ''', ''' + @CategoryStatusID + ''', ''' + @DateTimeStr + ''')';
	  execute Immediate @Stmt;
    endif;
    insert into [__Output] Values (True);	
  catch all
    insert into [__Output] Values (False);  
  end try; 
 


   END;

CREATE PROCEDURE GetTechJobsByDate
   ( 
      DisplayedDate DATE,
      C CHAR ( 1 ) OUTPUT,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      TechStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      TechWrk CHAR ( 40 ) OUTPUT,
      ReleaseTime TIMESTAMP OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      ArrivalTime TIMESTAMP OUTPUT,
      M CHAR ( 1 ) OUTPUT,
      D CHAR ( 1 ) OUTPUT,
      H CHAR ( 1 ) OUTPUT,
      TOCBufferSize DOUBLE ( 15 ) OUTPUT,
      C1 CHAR ( 1 ) OUTPUT
   ) 
   BEGIN 
             
       
       
       
-- drop procedure GetTechJobsByDate;       
-- execute procedure GetTechJobsByDate('2/19/2008');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
--  try
--    drop table #TechJobsAll;
--  catch ADS_SCRIPT_EXCEPTION
--    if __ErrCode = 5132 then
--    end if;
--  end try;
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
  
--  try
--    drop table #TechJobsHeavy;
--  catch ADS_SCRIPT_EXCEPTION
--    if __ErrCode = 5132 then
--    end if;
--  end try; 
  SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select ' ' as C,
  Case
   When (A.TechnicianID = 'Jared D.') and (A.TeamID is Null) then '1'
   When (A.TechnicianID = 'Doug B') and (A.TeamID is Null) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  Case When A.MaintenanceDuration = 0 then '' else 'X' end as "M",  
  Case When A.DriveabilityDuration = 0 then '' else 'X' end as "D",
  Case When A.HeavyDuration = 0 then '' else 'X' end as "H", TOCBufferSize, ' ' as C1
  into #TechJobsAll 
  from [Appointments] A
  left outer join Technicians Tec on A.TechnicianID = Tec.ShortName
  left outer join Teams Tea on Tec.TeamID = Tea.TeamID  
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.TechnicianID in (Select B.ShortName from Technicians B where B.TeamID =
    (Select C.TeamID from Teams C where C.Name = '1'))
  or A.TechnicianID in (Select B.ShortName from Technicians B where B.TeamID =
    (Select C.TeamID from Teams C where C.Name = '2'))
  or A.TechnicianID in (Select B.ShortName from Technicians B where B.TeamID =
    (Select C.TeamID from Teams C where C.Name = '3'))
  or A.TechnicianID in (Select B.ShortName from Technicians B where B.TeamID =
    (Select C.TeamID from Teams C where C.Name = '4')))
  and (A.TeamID = '1' or A.TeamID = '2' or A.TeamID = '3' or A.TeamID = '4' or ((A.TeamID = '' or A.TeamID is Null) and Tec.SkillLevel <> 'H'))	 
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off'
  and A.Status <> 'Carry Over')
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and Tea.Name <> ''
  order by
  Team,
  M Desc,
  D Desc,
  Convert(A.PromiseTime, SQL_TIME);
  
  
  Select ' ' as C, 'H' as Team, A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  Case When A.MaintenanceDuration = 0 then '' else 'X' end as "M",  
  Case When A.DriveabilityDuration = 0 then '' else 'X' end as "D",
  Case When A.HeavyDuration = 0 then '' else 'X' end as "H", TOCBufferSize, ' ' as C1
  into #TechJobsHeavy 
  from [Appointments] A
  left outer join Technicians Tec on A.TechnicianID = Tec.ShortName
  left outer join Teams Tea on Tec.TeamID = Tea.TeamID  
  where  Convert(A.StartTime, SQL_DATE) = @DisplayedDate  
  and(A.TechnicianID in (Select B.ShortName from Technicians B where B.TeamID =
    (Select C.TeamID from Teams C where C.Name = '1'))
  or A.TechnicianID in (Select B.ShortName from Technicians B where B.TeamID =
    (Select C.TeamID from Teams C where C.Name = '2'))
  or A.TechnicianID in (Select B.ShortName from Technicians B where B.TeamID =
    (Select C.TeamID from Teams C where C.Name = '3'))
  or A.TechnicianID in (Select B.ShortName from Technicians B where B.TeamID =
    (Select C.TeamID from Teams C where C.Name = '4')))
  and (A.TeamID = 'H' or (A.TeamID = '' and Tec.SkillLevel = 'H') or (A.TeamID is Null and Tec.SkillLevel = 'H')) 
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off'
  and A.Status <> 'Carry Over')
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET' 
  and Tea.Name <> ''
  order by Convert(A.PromiseTime, SQL_TIME);
  insert into [__OUTPUT] Select C, Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, C1 from [#TechJobsAll] 
  union all
  Select C, Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, C1 from [#TechJobsHeavy];
  
  --insert into [__OUTPUT] Select C, Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  --TagNumber, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  --Requisite, M, D, H, TOCBufferSize, C1 from [#TechJobsAll];
  

   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetAvailableQuickServiceJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      TechStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      TechWrk CHAR ( 40 ) OUTPUT,
      ReleaseTime TIMESTAMP OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      ArrivalTime TIMESTAMP OUTPUT,
      M CHAR ( 1 ) OUTPUT,
      D CHAR ( 1 ) OUTPUT,
      H CHAR ( 1 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                
-- Drop Procedure GetAvailableQuickServiceJobsByDate;       
-- execute procedure GetAvailableQuickServiceJobsByDate('3/5/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  Case When A.MaintenanceDuration = 0 then '' else 'X' end as "M",  
  Case When A.DriveabilityDuration = 0 then '' else 'X' end as "D",
  Case When A.HeavyDuration = 0 then '' else 'X' end as "H",
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID    
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off')  
  //Exclude All Day Jobs
  and Tea.Name = '6'
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team,
  M Desc,
  D Desc,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll]; 
    
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetFinishedTechDayJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      TechStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      TechWrk CHAR ( 40 ) OUTPUT,
      ReleaseTime TIMESTAMP OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      ArrivalTime TIMESTAMP OUTPUT,
      M DOUBLE ( 15 ) OUTPUT,
      D DOUBLE ( 15 ) OUTPUT,
      H DOUBLE ( 15 ) OUTPUT,
      TOCBufferSize DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                                  
-- Drop Procedure GetFinishedTechDayJobsByDate;       
-- execute procedure GetFinishedTechDayJobsByDate('3/05/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
  SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H, 
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID   
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off' and A.Status <> 'Carry Over')  
  //Exclude Heavy Include Assigned Day Jobs
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night')))
  and (A.TeamID = '1' or A.TeamID = '2' or A.TeamID = '3' or A.TeamID = '4' or A.TeamID = '5' or ((A.TeamID = '' or A.TeamID is Null) 
       and ((A.HeavyDuration is Null) or (A.HeavyDuration <= 0))))
  //Exclude other stuff	   
  and ((Convert(A.TOCFinishTime, SQL_DATE) <> '12/30/1899') or (A.TOCFinishTime is Not Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team,
  case when A.MaintenanceDuration > 0 then 2 when A.DriveabilityDuration > 0 then 1 else 0 end DESC,
  Convert(A.PromiseTime, SQL_TIME);
  
  
  Select 'H' as Team, A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H, 
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsHeavy 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID      
  where  Convert(A.StartTime, SQL_DATE) = @DisplayedDate  
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off' and A.Status <> 'Carry Over')
  //Include Heavy Only
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID = 'H')))
  and (A.TeamID = 'H' or ((A.TeamID = '' or A.TeamID is Null) and (A.HeavyDuration > 0)))
  //Exclude other stuff
  and ((Convert(A.TOCFinishTime, SQL_DATE) <> '12/30/1899') or (A.TOCFinishTime is Not Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET' 
  and Tea.Name <> ''
  and PA.ThruTS is Null
  order by Convert(A.PromiseTime, SQL_TIME);
  insert into [__OUTPUT] Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll] 
  union all
  Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsHeavy];
    
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetCarryOverTechDayJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      TechStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      TechWrk CHAR ( 40 ) OUTPUT,
      ReleaseTime TIMESTAMP OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      ArrivalTime TIMESTAMP OUTPUT,
      M DOUBLE ( 15 ) OUTPUT,
      D DOUBLE ( 15 ) OUTPUT,
      H DOUBLE ( 15 ) OUTPUT,
      TOCBufferSize DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                                              
-- Drop Procedure GetCarryOverTechDayJobsByDate;       
-- execute procedure GetCarryOverTechDayJobsByDate('3/05/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
  SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H,
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID   
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status = 'Carry Over')
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))    
  //Exclude Heavy Include Assigned Day Jobs
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night')))
  and (A.TeamID = '1' or A.TeamID = '2' or A.TeamID = '3' or A.TeamID = '4' or A.TeamID = '5' or ((A.TeamID = '' or A.TeamID is Null) 
       and ((A.HeavyDuration is Null) or (A.HeavyDuration <= 0))))
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team,
  case when A.MaintenanceDuration > 0 then 2 when A.DriveabilityDuration > 0 then 1 else 0 end DESC,
  A.PromiseTime;
  
  
  Select 'H' as Team, A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H, 
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsHeavy 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID      
  where  Convert(A.StartTime, SQL_DATE) = @DisplayedDate  
  and (A.Status = 'Carry Over')
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))  
  //Include Heavy Only
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID = 'H')))
  and (A.TeamID = 'H' or ((A.TeamID = '' or A.TeamID is Null) and (A.HeavyDuration > 0)))
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET' 
  and Tea.Name <> ''
  and PA.ThruTS is Null
  order by A.PromiseTime;
  insert into [__OUTPUT] Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll] 
  union all
  Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsHeavy];
    
   
   
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE EditAppointment
   ( 
      AppointmentID CHAR ( 40 ),
      Customer CHAR ( 40 ),
      StreetAddress CHAR ( 45 ),
      City CHAR ( 35 ),
      State CHAR ( 2 ),
      Zip CHAR ( 10 ),
      HomePhone CHAR ( 30 ),
      HomeExt CHAR ( 8 ),
      BestPhoneToday CHAR ( 30 ),
      BestExt CHAR ( 8 ),
      CellPhone CHAR ( 30 ),
      EMail CHAR ( 100 ),
      PromiseTime TIMESTAMP,
      Comments Memo,
      RONumber CHAR ( 10 ),
      TagNumber CHAR ( 10 ),
      Status CHAR ( 25 ),
      VIN CHAR ( 17 ),
      ModelYear Integer,
      Make CHAR ( 25 ),
      Model CHAR ( 25 ),
      Mileage Integer,
      ModelColor CHAR ( 25 ),
      LicenseNumber CHAR ( 12 ),
      PurchaseOrder CHAR ( 20 ),
      MaintenanceDuration DOUBLE ( 15 ),
      HeavyDuration DOUBLE ( 15 ),
      DriveabilityDuration DOUBLE ( 15 ),
      Notes Memo,
      ExtendedWarranty LOGICAL,
      Requisite CHAR ( 14 ),
      Shuttle CHAR ( 5 ),
      TeamID CHAR ( 40 ),
      TOCBufferSize DOUBLE ( 15 )
   ) 
   BEGIN 
                                        
  -- Drop Procedure EditAppointment;
  -- Execute Procedure EditAppointment('2008-07-29 07:15:00', 'Dan E.', 'Joe Blow', '', '', '', '', '', '', '', '', '', '', '2008-07-28 07:15:00', '', '', '', '')             

  declare @Input Cursor as select * from __Input;
  declare @AppointmentID String;
  declare @Customer String;
  declare @StreetAddress String;
  declare @City String;
  declare @State String;
  declare @Zip String;
  declare @HomePhone String;
  declare @HomeExt String;
  declare @BestPhoneToday String;
  declare @BestExt String;
  declare @CellPhone String;
  declare @EMail String;
  declare @PromiseTime TimeStamp;
  declare @Comments MEMO;
  declare @RONumber String;
  declare @TagNumber String;  
  declare @Status String;
  declare @VIN String;
  declare @ModelYear Integer;
  declare @Make String;
  declare @Model String;  
  declare @Mileage Integer;
  declare @ModelColor String;
  declare @LicenseNumber String;
  declare @PurchaseOrder String;
  declare @MaintenanceDuration Double;  
  declare @HeavyDuration Double;
  declare @DriveabilityDuration Double;
  declare @Notes MEMO;
  declare @ExtendedWarranty Logical;
  declare @Requisite String;
  declare @Shuttle String;
  declare @TeamID String;
  declare @TOCBufferSize Double;
  declare @CurrentStatus String;
  declare @ArrivalTime TimeStamp;
  declare @TOCFinishTime TimeStamp;

  open @Input;
  try
    fetch @Input;
	--@EndTime = @Input.EndTime;
	@AppointmentID = @Input.AppointmentID;
	@Customer = @Input.Customer;
	@StreetAddress = @Input.StreetAddress;
	@City = @Input.City;
	@State = @Input.State;
	@Zip = @Input.Zip;
	@HomePhone = @Input.HomePhone;
	@HomeExt = @Input.HomeExt;
	@BestPhoneToday = @Input.BestPhoneToday;
	@BestExt = @Input.BestExt;
	@CellPhone = @Input.CellPhone;
	@EMail = @Input.EMail;
	@PromiseTime = @Input.PromiseTime;
	@Comments = @Input.Comments;
	@RONumber = @Input.RONumber;
	@TagNumber = @Input.TagNumber;
	@Status = @Input.Status;
	@VIN = @Input.VIN;
	@ModelYear = @Input.ModelYear;
	@Make = @Input.Make;
	@Model = @Input.Model;
	@Mileage = @Input.Mileage;
	@ModelColor = @Input.ModelColor;
	@LicenseNumber = @Input.LicenseNumber;
	@PurchaseOrder = @Input.PurchaseOrder;
	@MaintenanceDuration = @Input.MaintenanceDuration;
	@HeavyDuration = @Input.HeavyDuration;
	@DriveabilityDuration = @Input.DriveabilityDuration;
	@Notes = @Input.Notes;
	@ExtendedWarranty = @Input.ExtendedWarranty;
	@Requisite = @Input.Requisite;		
	@Shuttle = @Input.Shuttle;
	@TeamID = @Input.TeamID;
	@TOCBufferSize = @Input.TOCBufferSize;
  finally
    close @Input;
  end;
  @CurrentStatus = (Select Status from Appointments where AppointmentID = @AppointmentID);
  @ArrivalTime = (Select ArrivalTime from Appointments where AppointmentID = @AppointmentID);
  @TOCFinishTime = (Select TOCFinishTime from Appointments where AppointmentID = @AppointmentID); 
  -- Update the Record
	
    Update Appointments Set Customer = @Customer, StreetAddress = @StreetAddress, City = @City, State = @State,
	Zip = @Zip, HomePhone = @HomePhone, HomeExt = @HomeExt, BestPhoneToday = @BestPhoneToday, BestExt = @BestExt,
	CellPhone = @CellPhone, EMail = @EMail, PromiseTime = @PromiseTime, Comments = @Comments, RONumber = @RONumber,
	TagNumber = @TagNumber, Status = @Status, VIN = @VIN, ModelYear = @ModelYear, Make = @Make, Model = @Model,
	Mileage = @Mileage, ModelColor = @ModelColor, LicenseNumber = @LicenseNumber, PurchaseOrder = @PurchaseOrder,
	MaintenanceDuration = @MaintenanceDuration, HeavyDuration = @HeavyDuration, 
	DriveabilityDuration = @DriveabilityDuration, Notes = @Notes, ExtendedWarranty = @ExtendedWarranty, 
	Requisite = @Requisite, Shuttle = @Shuttle, TeamID = @TeamID, TOCBufferSize = @TOCBufferSize
	where AppointmentID = @AppointmentID; 
    
    if (@Status <> @CurrentStatus) and (@Status = 'Wait' or @Status = 'Dispatched' or @Status = 'Blueprint' or @Status = 'Kitted') then
	  if @ArrivalTime is Null then
	    Update Appointments set ArrivalTime = Now() where AppointmentID = @AppointmentID;
	  endif;
	elseif (@Status <> @CurrentStatus) and (@Status = 'Done') then
	  if @TOCFinishTime is Null then
	    Update Appointments set TOCFinishTime = Now() where AppointmentID = @AppointmentID;
	  endif;	   
	endif; 
   
   
   
   
   
   END;

CREATE PROCEDURE GetAppointmentTechDayJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      CreatedBy CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      ApptTime TIME OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      M DOUBLE ( 15 ) OUTPUT,
      D DOUBLE ( 15 ) OUTPUT,
      H DOUBLE ( 15 ) OUTPUT,
      TOCBufferSize DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                                              
-- Drop Procedure GetAppointmentTechDayJobsByDate;       
-- execute procedure GetAppointmentTechDayJobsByDate('3/05/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
  SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H, 
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID    
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status = 'Appointment' or A.Status = 'No Show')  
  //Exclude Heavy Include Assigned Day Jobs
  //and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID <> '5')))
  and ((D.Description = 'Main Shop - Day'))
  and (A.TeamID = '1' or A.TeamID = '2' or A.TeamID = '3' or A.TeamID = '4' or A.TeamID = '5' or ((A.TeamID = '' or A.TeamID is Null) 
  and ((A.HeavyDuration is Null) or (A.HeavyDuration <= 0))))
  //Exclude other stuff	   
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team,
  case when A.MaintenanceDuration > 0 then 2 when A.DriveabilityDuration > 0 then 1 else 0 end DESC,
  Convert(A.PromiseTime, SQL_TIME);
  
  
  Select 'H' as Team, 
  A.Customer, A.AdvisorID, A.CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H, 
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsHeavy 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID     
  where  Convert(A.StartTime, SQL_DATE) = @DisplayedDate  
  and (A.Status = 'Appointment' or A.Status = 'No Show')
  //Include Heavy Only
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID = 'H')))
  and (A.TeamID = 'H' or ((A.TeamID = '' or A.TeamID is Null) and (A.HeavyDuration > 0)))
  //Exclude other stuff
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET' 
  and Tea.Name <> ''
  and PA.ThruTS is Null
  order by Convert(A.PromiseTime, SQL_TIME);
  insert into [__OUTPUT] 
  Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll] 
  union all
  Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsHeavy];
    
   
   
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetAppointmentAftermarketJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      CreatedBy CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      ApptTime TIME OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      M CHAR ( 1 ) OUTPUT,
      D CHAR ( 1 ) OUTPUT,
      H CHAR ( 1 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                                              
-- Drop Procedure GetAppointmentAftermarketJobsByDate;       
-- execute procedure GetAppointmentAftermarketJobsByDate('03/05/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  Case When A.MaintenanceDuration = 0 then '' else 'X' end as "M",  
  Case When A.DriveabilityDuration = 0 then '' else 'X' end as "D",
  Case When A.HeavyDuration = 0 then '' else 'X' end as "H", TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID    
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and ((A.Status = 'Appointment') or (A.Status = 'No Show'))  
  //Exclude All Day Jobs
  and Tea.Name = '9'
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team,
  M Desc,
  D Desc,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll]; 
    
   
   
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE CreateAppointment
   ( 
      StartTime TIMESTAMP,
      AdvisorID CHAR ( 40 ),
      Customer CHAR ( 40 ),
      StreetAddress CHAR ( 45 ),
      City CHAR ( 35 ),
      State CHAR ( 2 ),
      Zip CHAR ( 10 ),
      HomePhone CHAR ( 30 ),
      HomeExt CHAR ( 8 ),
      BestPhone CHAR ( 30 ),
      BestExt CHAR ( 8 ),
      CellPhone CHAR ( 30 ),
      EMail CHAR ( 100 ),
      PromiseTime TIMESTAMP,
      Comments Memo,
      RONumber CHAR ( 10 ),
      TagNumber CHAR ( 10 ),
      Status CHAR ( 25 ),
      VIN CHAR ( 17 ),
      ModelYear Integer,
      Make CHAR ( 25 ),
      Model CHAR ( 25 ),
      Mileage Integer,
      ModelColor CHAR ( 25 ),
      LicenseNumber CHAR ( 12 ),
      PurchaseOrder CHAR ( 20 ),
      MaintenanceDuration DOUBLE ( 15 ),
      HeavyDuration DOUBLE ( 15 ),
      DriveabilityDuration DOUBLE ( 15 ),
      CreatedByID CHAR ( 40 ),
      Notes Memo,
      ExtendedWarranty LOGICAL,
      Requisite CHAR ( 14 ),
      Shuttle CHAR ( 5 ),
      TeamID CHAR ( 40 ),
      TOCBufferSize DOUBLE ( 15 ),
      PreAuthorizationAmount DOUBLE ( 15 ),
      PreAuthorizationCreatedByID CHAR ( 40 ),
      PreAuthorizationLocation CHAR ( 40 ),
      PreAuthorizationNotes Memo,
      Alignment LOGICAL,
      ReturnValue CHAR ( 40 ) OUTPUT
   ) 
   BEGIN 
                                                              
  -- Drop Procedure CreateAppointment;
  -- Execute Procedure CreateAppointment('2008-07-29 07:15:00', 'Dan E.', 'Joe Blow', '', '', '', '', '', '', '', '', '', '', '2008-07-28 07:15:00', '', '', '', '', True)             

  declare @Input Cursor as select * from __Input;
  declare @AdvisorID String;
  declare @StartTime TimeStamp;
  declare @ReturnValue String;
  declare @Customer String;
  declare @StreetAddress String;
  declare @City String;
  declare @State String;
  declare @Zip String;
  declare @HomePhone String;
  declare @HomeExt String;
  declare @BestPhone String;
  declare @BestExt String;
  declare @CellPhone String;
  declare @EMail String;
  declare @PromiseTime TimeStamp;
  declare @Comments MEMO;
  declare @RONumber String;
  declare @TagNumber String;  
  declare @Status String;
  declare @VIN String;
  declare @ModelYear Integer;
  declare @Make String;
  declare @Model String;  
  declare @Mileage Integer;
  declare @ModelColor String;
  declare @LicenseNumber String;
  declare @PurchaseOrder String;
  declare @MaintenanceDuration Double;  
  declare @HeavyDuration Double;
  declare @DriveabilityDuration Double;
  declare @CreatedByID String;
  declare @Notes MEMO;
  declare @NewIDString String;
  declare @Count Integer;
  declare @ExtendedWarranty Logical;
  declare @Requisite String;
  declare @Shuttle String;
  declare @TeamID String;
  declare @TOCBufferSize Double;
  declare @PreAuthorizationAmount Double;
  declare @PreAuthorizationCreatedByID String;
  declare @PreAuthorizationLocation String;
  declare @PreAuthorizationNotes Memo;
  declare @Alignment Logical;

  open @Input;
  try
    fetch @Input;
    @AdvisorID = @Input.AdvisorID;
	@StartTime = @Input.StartTime;
	--@EndTime = @Input.EndTime;
	@Customer = @Input.Customer;
	@StreetAddress = @Input.StreetAddress;
	@City = @Input.City;
	@State = @Input.State;
	@Zip = @Input.Zip;
	@HomePhone = @Input.HomePhone;
	@HomeExt = @Input.HomeExt;
	@BestPhone = @Input.BestPhone;
	@BestExt = @Input.BestExt;
	@CellPhone = @Input.CellPhone;
	@EMail = @Input.EMail;
	@PromiseTime = @Input.PromiseTime;
	@Comments = @Input.Comments;
	@RONumber = @Input.RONumber;
	@TagNumber = @Input.TagNumber;
	@Status = @Input.Status;
	@VIN = @Input.VIN;
	@ModelYear = @Input.ModelYear;
	@Make = @Input.Make;
	@Model = @Input.Model;
	@Mileage = @Input.Mileage;
	@ModelColor = @Input.ModelColor;
	@LicenseNumber = @Input.LicenseNumber;
	@PurchaseOrder = @Input.PurchaseOrder;
	@MaintenanceDuration = @Input.MaintenanceDuration;
	@HeavyDuration = @Input.HeavyDuration;
	@DriveabilityDuration = @Input.DriveabilityDuration;
	@CreatedByID = @Input.CreatedByID;
	@Notes = @Input.Notes;
	@ExtendedWarranty = @Input.ExtendedWarranty;
	@Requisite = @Input.Requisite;		
	@Shuttle = @Input.Shuttle;
	@TeamID = @Input.TeamID;
	@TOCBufferSize = @Input.TOCBufferSize;
	@PreAuthorizationAmount = @Input.PreAuthorizationAmount;
	@PreAuthorizationCreatedByID = @Input.PreAuthorizationCreatedByID;
	@PreAuthorizationLocation = @Input.PreAuthorizationLocation;
	@PreAuthorizationNotes = @Input.PreAuthorizationNotes;
	@Alignment = @Input.Alignment;
  finally
    close @Input;
  end;
   
  --Get the appointment count for this Advisor/Timeslot.
  @Count = (Select Count(*) from Appointments where AdvisorID = @AdvisorID and StartTime = @StartTime);
  @NewIDString = NEWIDSTRING(B);
  -- Insert the Record
  if @Count = 0 then

    Insert Into Appointments (AppointmentID, AdvisorID, Customer, StartTime, Created, StreetAddress, City,
	State, Zip, HomePhone, HomeExt, BestPhoneToday, BestExt, CellPhone, EMail, PromiseTime, Comments, RONumber,
	TagNumber, Status, VIN, ModelYear, Make, Model, Mileage, ModelColor, LicenseNumber, PurchaseOrder, MaintenanceDuration, 
	HeavyDuration, DriveabilityDuration, CreatedByID, Notes, ExtendedWarranty, Requisite, Shuttle, TeamID, TOCBufferSize, Alignment) 
	Values (@NewIDString, @AdvisorID, @Customer, @StartTime, Now(), @StreetAddress, @City, @State, @Zip, @HomePhone, 
	@HomeExt, @BestPhone, @BestExt, @CellPhone, @EMail, @PromiseTime, @Comments, @RONumber, @TagNumber, @Status, @VIN, 
	@ModelYear,	@Make, @Model, @Mileage, @ModelColor, @LicenseNumber, @PurchaseOrder, @MaintenanceDuration, @HeavyDuration, 
	@DriveabilityDuration, @CreatedByID, @Notes, @ExtendedWarranty, @Requisite, @Shuttle, @TeamID, @TOCBufferSize, @Alignment);
    
	if @PreAuthorizationAmount = 0 or @PreAuthorizationAmount is Null then
	-- Do nothing
	else
	  Insert into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS)
	  values (@NewIDString, @PreAuthorizationAmount, @PreAuthorizationCreatedByID, @PreAuthorizationLocation, @PreAuthorizationNotes, Now(), Null);
	endif;
	--insert into [__Output] Values (@AdvisorID);
	//Timestamp here if status <> Appointment
	if @Status = 'Dispatched' or @Status = 'Wait' or @Status = 'Blueprint' or @Status = 'Kitted' then
	  Update Appointments set ArrivalTime = Now() where AppointmentID = @NewIDString;
	endif;
    insert into [__Output] Values (@NewIDString);
  else 
    insert into [__Output] Values ('Rejected');   
  endif;

   
   
   
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetAvailableTechNightJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      TechStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      TechWrk CHAR ( 40 ) OUTPUT,
      ReleaseTime TIMESTAMP OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      ArrivalTime TIMESTAMP OUTPUT,
      M DOUBLE ( 15 ) OUTPUT,
      D DOUBLE ( 15 ) OUTPUT,
      H DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                                  
-- Drop Procedure GetAvailableTechNightJobsByDate;       
-- execute procedure GetAvailableTechNightJobsByDate('3/5/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes  
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID     
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off')  
  //Exclude All Day Jobs
  and ((D.Description = 'Main Shop - Night') and ((A.TeamID = '5') or (A.TeamID is Null) or (A.TeamID = '')))
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  --and Tea.Name <> ''
  and PA.ThruTS is Null  
  order by
  Team,
--  case when A.MaintenanceDuration > 0 then 2 when A.DriveabilityDuration > 0 then 1 else 0 end DESC,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll]; 
    
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetAvailableTechDayJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      TechStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      PA DOUBLE ( 15 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      TechWrk CHAR ( 40 ) OUTPUT,
      ReleaseTime TIMESTAMP OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      ArrivalTime TIMESTAMP OUTPUT,
      M DOUBLE ( 15 ) OUTPUT,
      D DOUBLE ( 15 ) OUTPUT,
      H DOUBLE ( 15 ) OUTPUT,
      TOCBufferSize DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT,
      Priority LOGICAL OUTPUT
   ) 
   BEGIN 
                                                                                                                                                                  
-- Drop Procedure GetAvailableTechDayJobsByDate;       
-- execute procedure GetAvailableTechDayJobsByDate('02/18/2010');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
  SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  PA.PreAuthorizationAmount as PA,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H,
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes,
  A.Priority
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID  
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off' and A.Status <> 'Carry Over')  
  //Exclude Heavy Include Assigned Day Jobs
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID <> '')))
  and (A.TeamID = '1' or A.TeamID = '2' or A.TeamID = '3' or A.TeamID = '4' or A.TeamID = '5' or A.TeamID = '9' or A.TeamID = '6' or ((A.TeamID = '' or A.TeamID is Null) 
       and ((A.HeavyDuration is Null) or (A.HeavyDuration <= 0))))
  //Exclude other stuff	   
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  and (A.AdvisorID <> 'Matt P.')
  --and Tea.Name <> ''
  order by
  Team, Priority Desc,
  -- case when A.MaintenanceDuration > 0 then 2 when A.DriveabilityDuration > 0 then 1 else 0 end DESC,
  Convert(A.ArrivalTime, SQL_DATE),
  case when A.StartTime > A.ArrivalTime then A.StartTime else A.ArrivalTime end;
  
  Select 'H' as Team, A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  PA.PreAuthorizationAmount as PA,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H,
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes,
  A.Priority
  into #TechJobsHeavy 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID      
  where  Convert(A.StartTime, SQL_DATE) = @DisplayedDate  
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off' and A.Status <> 'Carry Over')
  //Include Heavy Only
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID = 'H')))
  and (A.TeamID = 'H' or ((A.TeamID = '' or A.TeamID is Null) and (A.HeavyDuration > 0)))
  //Exclude other stuff
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET' 
  and Tea.Name <> ''
  and PA.ThruTS is Null
  and (A.AdvisorID <> 'Matt P.')
  order by Priority Desc,  
  Convert(A.ArrivalTime, SQL_DATE),
  case when A.StartTime > A.ArrivalTime then A.StartTime else A.ArrivalTime end;
  
  insert into [__OUTPUT] Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus, PA,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes, Priority from [#TechJobsAll] 
  union all
  Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus, PA,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes, Priority from [#TechJobsHeavy]; 
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetAvailableAftermarketJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      TechStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      TechWrk CHAR ( 40 ) OUTPUT,
      ReleaseTime TIMESTAMP OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      ArrivalTime TIMESTAMP OUTPUT,
      M CHAR ( 1 ) OUTPUT,
      D CHAR ( 1 ) OUTPUT,
      H CHAR ( 1 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                            
-- Drop Procedure GetAvailableAftermarketJobsByDate;       
-- execute procedure GetAvailableAftermarketJobsByDate('3/5/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  Case When A.MaintenanceDuration = 0 then '' else 'X' end as "M",  
  Case When A.DriveabilityDuration = 0 then '' else 'X' end as "D",
  Case When A.HeavyDuration = 0 then '' else 'X' end as "H", TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID     
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off')  
  //Exclude All Day Jobs
  and Tea.Name = '9'
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team,
  M Desc,
  D Desc,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll]; 

   
   
   
   
   END;

CREATE PROCEDURE GetAppointmentQuickServiceJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      CreatedBy CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      ApptTime TIME OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      M CHAR ( 1 ) OUTPUT,
      D CHAR ( 1 ) OUTPUT,
      H CHAR ( 1 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                      
-- Drop Procedure GetAppointmentQuickServiceJobsByDate;       
-- execute procedure GetAppointmentQuickServiceJobsByDate('03/05/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  Case When A.MaintenanceDuration = 0 then '' else 'X' end as "M",  
  Case When A.DriveabilityDuration = 0 then '' else 'X' end as "D",
  Case When A.HeavyDuration = 0 then '' else 'X' end as "H",
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID     
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status = 'Appointment' or A.Status = 'No Show')  
  //Exclude All Day Jobs
  and Tea.Name = '6'
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team,
  M Desc,
  D Desc,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll]; 
    
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetMainShopDayCapacity
   ( 
      InDate DATE,
      DriveabilityHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceHours DOUBLE ( 15 ) OUTPUT,
      HeavyHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityDuration DOUBLE ( 15 ) OUTPUT,
      MaintenanceDuration DOUBLE ( 15 ) OUTPUT,
      HeavyDuration DOUBLE ( 15 ) OUTPUT
   ) 
   BEGIN 
                                      -- Drop Procedure GetMainShopDayCapacity;
  -- Execute Procedure GetMainShopDayCapacity('1/3/2009');
  declare @Input Cursor as select * from __Input;  
  declare @InDate Date;
  
  declare @DHours Double;
  declare @MHours Double;
  declare @HHours Double;
  declare @DDuration Double;
  declare @MDuration Double;
  declare @HDuration Double;
  declare @SaturdayTeamWorking String;
  
  open @Input;
  try
    fetch @Input;
    @InDate = @Input.InDate;
  finally
    close @Input;
  end;
  
  
  @SaturdayTeamWorking = '';
  --SELECT Utilities.DropTablesIfExist('#TempCapacityTable') FROM system.iota;  
  --if Now() > TimeStampAdd(SQL_TSI_MINUTE, 15, (Select LastViewed From HoursAvailableForTheDay)) then
  if (DayOfWeek(@InDate) = 7) then
    @SaturdayTeamWorking = (Select Name from Weekends where Date = Convert(@InDate, SQL_DATE));
	if @SaturdayTeamWorking <> '' then
	  @HHours = (Select Count(*) from Technicians T
	         where TeamID = 
			 (Select TeamID from Teams where Name = @SaturdayTeamWorking and DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day'))
             and T.SkillLevel = 'H'
             and T.Active = True) * 9.5 -
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'H'
			 and D.Description = 'Main Shop - Day');
			 
      @DHours = (Select Count(*) from Technicians T
	         where TeamID = 
			 (Select TeamID from Teams where Name = @SaturdayTeamWorking and DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day'))
             and T.SkillLevel = 'D'
             and T.Active = True) * 9.5 - 
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'D'
			 and D.Description = 'Main Shop - Day');
      @MHours = (Select Count(*) from Technicians T
	         where TeamID = 
			 (Select TeamID from Teams where Name = @SaturdayTeamWorking and DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day'))
             and T.SkillLevel = 'M'
             and T.Active = True) * 9.5 - 
			 (Select Coalesce(Sum(E.Hours), 0) As Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'M'
			 and D.Description = 'Main Shop - Day');			 
	
	else
      @HHours = 0;
	  @MHours = 0;
	  @DHours = 0;
    endif;			 
			  			   
  else
    @HHours = (Select Count(*) from Technicians T
             left outer join Teams Tea on T.TeamID = Tea.TeamID
             left outer join Departments D on Tea.DepartmentID = D.DepartmentID
             where T.SkillLevel = 'H'
             and T.Active = True
             and D.Description = 'Main Shop - Day') * 9.5 -
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'H'
			 and D.Description = 'Main Shop - Day');
    @DHours = (select Count(*) from Technicians T
             left outer join Teams Tea on T.TeamID = Tea.TeamID
             left outer join Departments D on Tea.DepartmentID = D.DepartmentID
             where T.SkillLevel = 'D'
             and T.Active = True
             and D.Description = 'Main Shop - Day') * 9.5 - 
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'D'
			 and D.Description = 'Main Shop - Day');
    @MHours = (select Count(*) from Technicians T
             left outer join Teams Tea on T.TeamID = Tea.TeamID
             left outer join Departments D on Tea.DepartmentID = D.DepartmentID
             where T.SkillLevel = 'M'
             and T.Active = True
             and D.Description = 'Main Shop - Day') * 9.5 - 
			 (Select Coalesce(Sum(E.Hours), 0) As Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'M'
			 and D.Description = 'Main Shop - Day');				 			 
  endif;
  @HHours = @HHours * (Select Heavy from CorrectionFactors);			 			 
  @DHours = @DHours * (Select Driveability from CorrectionFactors);			 
  @MHours = @MHours * (Select Maintenance from CorrectionFactors);			      
  @DDuration = (Select Coalesce(Sum(DriveabilityDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'No Show'
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day')))); 
  
  @MDuration = (Select Coalesce(Sum(MaintenanceDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'No Show'
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day')))); 
			 		 	 
  @HDuration = (Select Coalesce(Sum(HeavyDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'No Show'
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID In 
			 (Select DepartmentID from Departments where (Description = 'Main Shop - Day') or (Description = 'Main Shop - Night')))));
			             
  Insert Into [__Output] (DriveabilityHours, MaintenanceHours, HeavyHours, DriveabilityDuration, MaintenanceDuration, HeavyDuration) 
  values(@DHours, @MHours, @HHours, @DDuration, @MDuration, @HDuration);

   
   
   
   
   
   
   END;

CREATE PROCEDURE GetAppointmentTechNightJobsByDate
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      CreatedBy CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      ApptTime TIME OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      M DOUBLE ( 15 ) OUTPUT,
      D DOUBLE ( 15 ) OUTPUT,
      H DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                                        
-- Drop Procedure GetAppointmentTechNightJobsByDate;       
-- execute procedure GetAppointmentTechNightJobsByDate('3/05/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID     
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status = 'Appointment' or A.Status = 'No Show')  
  //Exclude All Day Jobs
  and ((D.Description = 'Main Shop - Night') and ((A.TeamID = '5') or (A.TeamID is Null) or (A.TeamID = '')))
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team,
  case when A.MaintenanceDuration > 0 then 2 when A.DriveabilityDuration > 0 then 1 else 0 end DESC,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll]; 
    

   
   
   
   
   
   END;

CREATE PROCEDURE GetAftermarketCapacity
   ( 
      InDate DATE,
      MaintenanceHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceDuration DOUBLE ( 15 ) OUTPUT
   ) 
   BEGIN 
                                      -- Drop Procedure GetAftermarketCapacity;
  -- Execute Procedure GetAftermarketCapacity('10/5/2008');
  declare @Input Cursor as select * from __Input;  
  declare @InDate Date;
  
  declare @MHours Double;
  declare @MDuration Double;
  declare @FSaturday Logical;
  
  open @Input;
  try
    fetch @Input;
    @InDate = @Input.InDate;
  finally
    close @Input;
  end;
  
  
  
  --SELECT Utilities.DropTablesIfExist('#TempCapacityTable') FROM system.iota;  
  --if Now() > TimeStampAdd(SQL_TSI_MINUTE, 15, (Select LastViewed From HoursAvailableForTheDay)) then

  if (DayOfWeek(@InDate) = 7) then
    if (Select Count(*) from WeekendsAftermarket where Date = Convert(@InDate, SQL_DATE)) > 0 then
      @FSaturday = True;  
      @MHours = 1
		    	  * 9 - 
			   (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsAftermarket E 
			   left outer join Technicians T on E.TechNumber = T.TechNumber
			   left outer join Teams Tea on T.TeamID = Tea.TeamID
			   left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			   where E.Date = Convert(@InDate, SQL_DATE)
			   and T.SkillLevel = 'R'
			   and D.Description = 'Aftermarket');
	else
	  @MHours = 0;
	endif;
  else	 
    @MHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.SkillLevel = 'R'
              and T.Active = True
              and D.Description = 'Aftermarket')
	    	  * 9 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsAftermarket E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and T.SkillLevel = 'R'
			  and D.Description = 'Aftermarket');
  endif;			  
  --@MHours = @MHours * (Select Maintenance from CorrectionFactors);			      
  
  @MDuration = (Select Coalesce(Sum(MaintenanceDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'No Show'
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Aftermarket')))); 
			             
  Insert Into [__Output] (MaintenanceHours, MaintenanceDuration) 
  values(@MHours, @MDuration);

  
   
   
   
   
   END;

CREATE PROCEDURE GetNightCapacity
   ( 
      InDate DATE,
      MaintenanceHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceDuration DOUBLE ( 15 ) OUTPUT,
      DriveabilityDuration DOUBLE ( 15 ) OUTPUT
   ) 
   BEGIN 
                                            -- Drop Procedure GetNightCapacity;
  -- Execute Procedure GetNightCapacity('9/24/2009');
  declare @Input Cursor as select * from __Input;  
  declare @InDate Date;
  
  declare @MHours Double;
  declare @DHours Double;
  declare @MDuration Double;
  declare @DDuration Double;
  declare @FSaturday Logical;
  
  open @Input;
  try
    fetch @Input;
    @InDate = @Input.InDate;
  finally
    close @Input;
  end;
  
  
  
  --SELECT Utilities.DropTablesIfExist('#TempCapacityTable') FROM system.iota;  
  --if Now() > TimeStampAdd(SQL_TSI_MINUTE, 15, (Select LastViewed From HoursAvailableForTheDay)) then

  if (DayOfWeek(@InDate) = 7) then
      @FSaturday = True;  
      @MHours = 0;
	  @DHours = 0;
  else	 
    @MHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.SkillLevel = 'M'			  
              and T.Active = True
              and D.Description = 'Main Shop - Night')
	    	  * 9 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsNight E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and T.SkillLevel = 'M'			  
			  and D.Description = 'Main Shop - Night');
    @DHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.SkillLevel = 'D'			  
              and T.Active = True
              and D.Description = 'Main Shop - Night')
	    	  * 9 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsNight E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and T.SkillLevel = 'D'			  
			  and D.Description = 'Main Shop - Night');			  
  endif;			  
  --@MHours = @MHours * (Select Maintenance from CorrectionFactors);			      
  
  @MDuration = (Select Coalesce(Sum(MaintenanceDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'No Show'
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Night'))));
  @DDuration = (Select Coalesce(Sum(DriveabilityDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'No Show'
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Night'))));			 
			  
			             
  Insert Into [__Output] (MaintenanceHours, DriveabilityHours, MaintenanceDuration, DriveabilityDuration) 
  values(@MHours, @DHours, @MDuration, @DDuration);

  
   
   
   
   
   
   END;

CREATE PROCEDURE GetQuickServiceCapacity
   ( 
      InDate DATE,
      MaintenanceHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceDuration DOUBLE ( 15 ) OUTPUT
   ) 
   BEGIN 
                                            -- Drop Procedure GetQuickServiceCapacity;
  -- Execute Procedure GetQuickServiceCapacity('10/5/2008');
  declare @Input Cursor as select * from __Input;  
  declare @InDate Date;
  
  declare @MHours Double;
  declare @MDuration Double;
  declare @FSaturday Logical;
  
  open @Input;
  try
    fetch @Input;
    @InDate = @Input.InDate;
  finally
    close @Input;
  end;
  
  
  
  --SELECT Utilities.DropTablesIfExist('#TempCapacityTable') FROM system.iota;  
  --if Now() > TimeStampAdd(SQL_TSI_MINUTE, 15, (Select LastViewed From HoursAvailableForTheDay)) then

  if (DayOfWeek(@InDate) = 7) then
 
      @MHours = 1
		    	  * 9 - 
			   (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsAftermarket E 
			   left outer join Technicians T on E.TechNumber = T.TechNumber
			   left outer join Teams Tea on T.TeamID = Tea.TeamID
			   left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			   where E.Date = Convert(@InDate, SQL_DATE)
			   and D.Description = 'Quick Service');
  else	 
    @MHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.Active = True
              and D.Description = 'Quick Service')
	    	  * 9 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsQuickService E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and D.Description = 'Quick Service');
  endif;			  
  --@MHours = @MHours * (Select Maintenance from CorrectionFactors);			      
  
  @MDuration = (Select Coalesce(Sum(MaintenanceDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'No Show'
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Quick Service')))); 
			             
  Insert Into [__Output] (MaintenanceHours, MaintenanceDuration) 
  values(@MHours, @MDuration);

  
   
   
   
   
   
   END;

CREATE PROCEDURE CreateAppointmentFromPaste
   ( 
      StartTime TIMESTAMP,
      AdvisorID CHAR ( 40 ),
      Customer CHAR ( 40 ),
      StreetAddress CHAR ( 45 ),
      City CHAR ( 35 ),
      State CHAR ( 2 ),
      Zip CHAR ( 10 ),
      HomePhone CHAR ( 30 ),
      HomeExt CHAR ( 8 ),
      BestPhone CHAR ( 30 ),
      BestExt CHAR ( 8 ),
      CellPhone CHAR ( 30 ),
      EMail CHAR ( 100 ),
      PromiseTime TIMESTAMP,
      ArrivalTime TIMESTAMP,
      Comments Memo,
      RONumber CHAR ( 10 ),
      TagNumber CHAR ( 10 ),
      Status CHAR ( 25 ),
      VIN CHAR ( 17 ),
      ModelYear Integer,
      Make CHAR ( 25 ),
      Model CHAR ( 25 ),
      Mileage Integer,
      ModelColor CHAR ( 25 ),
      LicenseNumber CHAR ( 12 ),
      PurchaseOrder CHAR ( 20 ),
      MaintenanceDuration DOUBLE ( 15 ),
      HeavyDuration DOUBLE ( 15 ),
      DriveabilityDuration DOUBLE ( 15 ),
      CreatedByID CHAR ( 40 ),
      Notes Memo,
      ExtendedWarranty LOGICAL,
      Requisite CHAR ( 14 ),
      Shuttle CHAR ( 5 ),
      TeamID CHAR ( 40 ),
      TOCBufferSize DOUBLE ( 15 ),
      TechComments Memo,
      PreAuthorizationAmount DOUBLE ( 15 ),
      PreAuthorizationCreatedByID CHAR ( 40 ),
      PreAuthorizationLocation CHAR ( 40 ),
      PreAuthorizationNotes Memo,
      ReturnValue CHAR ( 40 ) OUTPUT
   ) 
   BEGIN 
                                                                                
  -- Drop Procedure CreateAppointmentFromPaste;
  -- Execute Procedure CreateAppointmentFromPaste('2009-03-05 11:30:00', 'Brad S.', 'Joe Blow', '', '', '', '', '', '', '', '', '', '', '2008-07-28 07:15:00', '', '', '', '', 0, 'rnokelby', 'Consultant', 'Just a Note');             

  declare @Input Cursor as select * from __Input;
  declare @AdvisorID String;
  declare @StartTime TimeStamp;
  declare @ReturnValue String;
  declare @Customer String;
  declare @StreetAddress String;
  declare @City String;
  declare @State String;
  declare @Zip String;
  declare @HomePhone String;
  declare @HomeExt String;
  declare @BestPhone String;
  declare @BestExt String;
  declare @CellPhone String;
  declare @EMail String;
  declare @PromiseTime TimeStamp;
  declare @ArrivalTime TimeStamp;
  declare @Comments MEMO;
  declare @RONumber String;
  declare @TagNumber String;  
  declare @Status String;
  declare @VIN String;
  declare @ModelYear Integer;
  declare @Make String;
  declare @Model String;  
  declare @Mileage Integer;
  declare @ModelColor String;
  declare @LicenseNumber String;
  declare @PurchaseOrder String;
  declare @MaintenanceDuration Double;  
  declare @HeavyDuration Double;
  declare @DriveabilityDuration Double;
  declare @CreatedByID String;
  declare @Notes MEMO;
  declare @NewIDString String;
  declare @Count Integer;
  declare @ExtendedWarranty Logical;
  declare @Requisite String;
  declare @Shuttle String;
  declare @TeamID String;
  declare @TOCBufferSize Double;
  declare @TechComments MEMO;
  declare @PreAuthorizationAmount Double;
  declare @PreAuthorizationCreatedByID String;
  declare @PreAuthorizationLocation String;
  declare @PreAuthorizationNotes MEMO;

  open @Input;
  try
    fetch @Input;
    @AdvisorID = @Input.AdvisorID;
	@StartTime = @Input.StartTime;
	--@EndTime = @Input.EndTime;
	@Customer = @Input.Customer;
	@StreetAddress = @Input.StreetAddress;
	@City = @Input.City;
	@State = @Input.State;
	@Zip = @Input.Zip;
	@HomePhone = @Input.HomePhone;
	@HomeExt = @Input.HomeExt;
	@BestPhone = @Input.BestPhone;
	@BestExt = @Input.BestExt;
	@CellPhone = @Input.CellPhone;
	@EMail = @Input.EMail;
	@PromiseTime = @Input.PromiseTime;
	@ArrivalTime = @Input.ArrivalTime;
	@Comments = @Input.Comments;
	@RONumber = @Input.RONumber;
	@TagNumber = @Input.TagNumber;
	@Status = @Input.Status;
	@VIN = @Input.VIN;
	@ModelYear = @Input.ModelYear;
	@Make = @Input.Make;
	@Model = @Input.Model;
	@Mileage = @Input.Mileage;
	@ModelColor = @Input.ModelColor;
	@LicenseNumber = @Input.LicenseNumber;
	@PurchaseOrder = @Input.PurchaseOrder;
	@MaintenanceDuration = @Input.MaintenanceDuration;
	@HeavyDuration = @Input.HeavyDuration;
	@DriveabilityDuration = @Input.DriveabilityDuration;
	@CreatedByID = @Input.CreatedByID;
	@Notes = @Input.Notes;
	@ExtendedWarranty = @Input.ExtendedWarranty;
	@Requisite = @Input.Requisite;		
	@Shuttle = @Input.Shuttle;
	@TeamID = @Input.TeamID;
	@TOCBufferSize = @Input.TOCBufferSize;
	@TechComments = @Input.TechComments;
	@PreAuthorizationAmount = @Input.PreAuthorizationAmount;
	@PreAuthorizationCreatedByID = @Input.PreAuthorizationCreatedByID;
	@PreAuthorizationLocation = @Input.PreAuthorizationLocation;
	@PreAuthorizationNotes = @Input.PreAuthorizationNotes;
  finally
    close @Input;
  end;
   
  --Get the appointment count for this Advisor/Timeslot.
  @Count = (Select Count(*) from Appointments where AdvisorID = @AdvisorID and StartTime = @StartTime);
  @NewIDString = NEWIDSTRING(B);
  -- Insert the Record
  if @Count = 0 then

    Insert Into Appointments (AppointmentID, AdvisorID, Customer, StartTime, Created, StreetAddress, City,
	State, Zip, HomePhone, HomeExt, BestPhoneToday, BestExt, CellPhone, EMail, PromiseTime, Comments, RONumber,
	TagNumber, Status, VIN, ModelYear, Make, Model, Mileage, ModelColor, LicenseNumber, PurchaseOrder, MaintenanceDuration, 
	HeavyDuration, DriveabilityDuration, CreatedByID, Notes, ExtendedWarranty, Requisite, Shuttle, TeamID, TOCBufferSize, TechComments) 
	Values ( @NewIDString, @AdvisorID, @Customer, @StartTime, Now(), @StreetAddress, @City, @State, @Zip, @HomePhone, 
	@HomeExt, @BestPhone, @BestExt, @CellPhone, @EMail, @PromiseTime, @Comments, @RONumber, @TagNumber, @Status, @VIN, 
	@ModelYear,	@Make, @Model, @Mileage, @ModelColor, @LicenseNumber, @PurchaseOrder, @MaintenanceDuration, @HeavyDuration, 
	@DriveabilityDuration, @CreatedByID, @Notes, @ExtendedWarranty, @Requisite, @Shuttle, @TeamID, @TOCBufferSize, @TechComments);

	--insert into [__Output] Values (@AdvisorID);
	//Timestamp here if status <> Appointment
	if @Status = 'Dispatched' then
	  Update Appointments set ArrivalTime = Now() where AppointmentID = @NewIDString;
	elseif @Status = 'Wait' then
	  Update Appointments set ArrivalTime = Now(), Status = 'Dispatched' where AppointmentID = @NewIDString;
--      Update Appointments set Status = 'Dispatched' where AppointmentID = @NewIDString;
	endif;


    --Execute procedure EditPreAuthorizationAmount
    if @PreAuthorizationAmount <> 0 then
	  Execute procedure EditPreAuthorizationAmount(@NewIDString, Convert(@PreAuthorizationAmount, SQL_CHAR), @PreAuthorizationCreatedByID, @PreAuthorizationLocation, @PreAuthorizationNotes);
	endif;  
		
		
    insert into [__Output] Values (@NewIDString);
  else 
    insert into [__Output] Values ('Rejected');   
  endif; 
   

   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetAppointmentTechDayJobsByDate2
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      CreatedBy CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      ApptTime TIME OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      M DOUBLE ( 15 ) OUTPUT,
      D DOUBLE ( 15 ) OUTPUT,
      H DOUBLE ( 15 ) OUTPUT,
      TOCBufferSize DOUBLE ( 15 ) OUTPUT
   ) 
   BEGIN 
                                                            
-- Drop Procedure GetAppointmentTechDayJobsByDate2;       
-- execute procedure GetAppointmentTechDayJobsByDate2('12/15/2008');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
  SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H, 
  TOCBufferSize
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID  
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status = 'Appointment' or A.Status = 'No Show')  
  //Exclude Heavy Include Assigned Day Jobs
  //and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID <> '5')))
  and ((D.Description = 'Main Shop - Day'))
  and (A.TeamID = '1' or A.TeamID = '2' or A.TeamID = '3' or A.TeamID = '4' or ((A.TeamID = '' or A.TeamID is Null) 
  and ((A.HeavyDuration is Null) or (A.HeavyDuration <= 0))))
  //Exclude other stuff	   
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  --and Tea.Name <> ''
  order by
  Team,
  case when A.MaintenanceDuration > 0 then 2 when A.DriveabilityDuration > 0 then 1 else 0 end DESC,
  Convert(A.PromiseTime, SQL_TIME);
  
  
  Select 'H' as Team, 
  A.Customer, A.AdvisorID, A.CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H, 
  TOCBufferSize
  into #TechJobsHeavy 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID   
  where  Convert(A.StartTime, SQL_DATE) = @DisplayedDate  
  and (A.Status = 'Appointment' or A.Status = 'No Show')
  //Include Heavy Only
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID = 'H')))
  and (A.TeamID = 'H' or ((A.TeamID = '' or A.TeamID is Null) and (A.HeavyDuration > 0)))
  //Exclude other stuff
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET' 
  and Tea.Name <> ''
  order by Convert(A.PromiseTime, SQL_TIME);
  insert into [__OUTPUT] 
  Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H, TOCBufferSize from [#TechJobsAll] 
  union all
  Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H, TOCBufferSize from [#TechJobsHeavy];
    
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetAppointmentTechNightJobsByDate2
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      CreatedBy CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      ApptTime TIME OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      M DOUBLE ( 15 ) OUTPUT,
      D DOUBLE ( 15 ) OUTPUT,
      H DOUBLE ( 15 ) OUTPUT
   ) 
   BEGIN 
                                                            
-- Drop Procedure GetAppointmentTechNightJobsByDate2;       
-- execute procedure GetAppointmentTechNightJobsByDate2('12/15/2008');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID   
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status = 'Appointment' or A.Status = 'No Show')  
  //Exclude All Day Jobs
  and ((D.Description = 'Main Shop - Night') and ((A.TeamID = '5') or (A.TeamID is Null) or (A.TeamID = '')))
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  --and Tea.Name <> ''
  order by
  Team,
  case when A.MaintenanceDuration > 0 then 2 when A.DriveabilityDuration > 0 then 1 else 0 end DESC,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H from [#TechJobsAll]; 
    

   
   
   
   END;

CREATE PROCEDURE GetAppointmentAftermarketJobsByDate2
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      CreatedBy CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      ApptTime TIME OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      M CHAR ( 1 ) OUTPUT,
      D CHAR ( 1 ) OUTPUT,
      H CHAR ( 1 ) OUTPUT
   ) 
   BEGIN 
                                                      
-- Drop Procedure GetAppointmentAftermarketJobsByDate2;       
-- execute procedure GetAppointmentAftermarketJobsByDate2('12/16/2008');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  Case When A.MaintenanceDuration = 0 then '' else 'X' end as "M",  
  Case When A.DriveabilityDuration = 0 then '' else 'X' end as "D",
  Case When A.HeavyDuration = 0 then '' else 'X' end as "H", TOCBufferSize
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID  
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and ((A.Status = 'Appointment') or (A.Status = 'No Show'))  
  //Exclude All Day Jobs
  and D.Description = 'Aftermarket'
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  --and Tea.Name <> ''
  order by
  Team,
  M Desc,
  D Desc,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H from [#TechJobsAll]; 
    
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetAppointmentQuickServiceJobsByDate2
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      CreatedBy CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      ApptTime TIME OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      M CHAR ( 1 ) OUTPUT,
      D CHAR ( 1 ) OUTPUT,
      H CHAR ( 1 ) OUTPUT
   ) 
   BEGIN 
                                          
-- Drop Procedure GetAppointmentQuickServiceJobsByDate2;       
-- execute procedure GetAppointmentQuickServiceJobsByDate2('12/16/2008');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, CreatedByID as CreatedBy, A.PromiseTime, Convert(A.StartTime, SQL_TIME) as ApptTime, A.Status as AdvisorStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model,
  A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite, 
  Case When A.MaintenanceDuration = 0 then '' else 'X' end as "M",  
  Case When A.DriveabilityDuration = 0 then '' else 'X' end as "D",
  Case When A.HeavyDuration = 0 then '' else 'X' end as "H"
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID   
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status = 'Appointment' or A.Status = 'No Show')  
  //Exclude All Day Jobs
  and D.Description = 'Quick Service'
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  --and Tea.Name <> ''
  order by
  Team,
  M Desc,
  D Desc,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, CreatedBy, PromiseTime, ApptTime, AdvisorStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, Notes, AppointmentID, TechComments, 
  Requisite, M, D, H from [#TechJobsAll]; 
    
   
   
   
   
   
   
   END;

CREATE PROCEDURE CreateAppointmentFromPasteDelete
   ( 
      StartTime TIMESTAMP,
      AdvisorID CHAR ( 40 ),
      Customer CHAR ( 40 ),
      StreetAddress CHAR ( 45 ),
      City CHAR ( 35 ),
      State CHAR ( 2 ),
      Zip CHAR ( 10 ),
      HomePhone CHAR ( 30 ),
      HomeExt CHAR ( 8 ),
      BestPhone CHAR ( 30 ),
      BestExt CHAR ( 8 ),
      CellPhone CHAR ( 30 ),
      EMail CHAR ( 100 ),
      PromiseTime TIMESTAMP,
      ArrivalTime TIMESTAMP,
      Comments Memo,
      RONumber CHAR ( 10 ),
      TagNumber CHAR ( 10 ),
      Status CHAR ( 25 ),
      VIN CHAR ( 17 ),
      ModelYear Integer,
      Make CHAR ( 25 ),
      Model CHAR ( 25 ),
      Mileage Integer,
      ModelColor CHAR ( 25 ),
      LicenseNumber CHAR ( 12 ),
      PurchaseOrder CHAR ( 20 ),
      MaintenanceDuration DOUBLE ( 15 ),
      HeavyDuration DOUBLE ( 15 ),
      DriveabilityDuration DOUBLE ( 15 ),
      CreatedByID CHAR ( 40 ),
      Notes Memo,
      ExtendedWarranty LOGICAL,
      Requisite CHAR ( 14 ),
      Shuttle CHAR ( 5 ),
      TeamID CHAR ( 40 ),
      TOCBufferSize DOUBLE ( 15 ),
      TechComments Memo,
      ReturnValue CHAR ( 40 ) OUTPUT
   ) 
   BEGIN 
                                                              
  -- Drop Procedure CreateAppointmentFromPasteDelete;
  -- Execute Procedure CreateAppointmentFromPaste2('2008-07-29 07:15:00', 'Dan E.', 'Joe Blow', '', '', '', '', '', '', '', '', '', '', '2008-07-28 07:15:00', '', '', '', '')             

  declare @Input Cursor as select * from __Input;
  declare @AdvisorID String;
  declare @StartTime TimeStamp;
  declare @ReturnValue String;
  declare @Customer String;
  declare @StreetAddress String;
  declare @City String;
  declare @State String;
  declare @Zip String;
  declare @HomePhone String;
  declare @HomeExt String;
  declare @BestPhone String;
  declare @BestExt String;
  declare @CellPhone String;
  declare @EMail String;
  declare @PromiseTime TimeStamp;
  declare @ArrivalTime TimeStamp;
  declare @Comments MEMO;
  declare @RONumber String;
  declare @TagNumber String;  
  declare @Status String;
  declare @VIN String;
  declare @ModelYear Integer;
  declare @Make String;
  declare @Model String;  
  declare @Mileage Integer;
  declare @ModelColor String;
  declare @LicenseNumber String;
  declare @PurchaseOrder String;
  declare @MaintenanceDuration Double;  
  declare @HeavyDuration Double;
  declare @DriveabilityDuration Double;
  declare @CreatedByID String;
  declare @Notes MEMO;
  declare @NewIDString String;
  declare @Count Integer;
  declare @ExtendedWarranty Logical;
  declare @Requisite String;
  declare @Shuttle String;
  declare @TeamID String;
  declare @TOCBufferSize Double;
  declare @TechComments MEMO;

  open @Input;
  try
    fetch @Input;
    @AdvisorID = @Input.AdvisorID;
	@StartTime = @Input.StartTime;
	--@EndTime = @Input.EndTime;
	@Customer = @Input.Customer;
	@StreetAddress = @Input.StreetAddress;
	@City = @Input.City;
	@State = @Input.State;
	@Zip = @Input.Zip;
	@HomePhone = @Input.HomePhone;
	@HomeExt = @Input.HomeExt;
	@BestPhone = @Input.BestPhone;
	@BestExt = @Input.BestExt;
	@CellPhone = @Input.CellPhone;
	@EMail = @Input.EMail;
	@PromiseTime = @Input.PromiseTime;
	@ArrivalTime = @Input.ArrivalTime;
	@Comments = @Input.Comments;
	@RONumber = @Input.RONumber;
	@TagNumber = @Input.TagNumber;
	@Status = @Input.Status;
	@VIN = @Input.VIN;
	@ModelYear = @Input.ModelYear;
	@Make = @Input.Make;
	@Model = @Input.Model;
	@Mileage = @Input.Mileage;
	@ModelColor = @Input.ModelColor;
	@LicenseNumber = @Input.LicenseNumber;
	@PurchaseOrder = @Input.PurchaseOrder;
	@MaintenanceDuration = @Input.MaintenanceDuration;
	@HeavyDuration = @Input.HeavyDuration;
	@DriveabilityDuration = @Input.DriveabilityDuration;
	@CreatedByID = @Input.CreatedByID;
	@Notes = @Input.Notes;
	@ExtendedWarranty = @Input.ExtendedWarranty;
	@Requisite = @Input.Requisite;		
	@Shuttle = @Input.Shuttle;
	@TeamID = @Input.TeamID;
	@TOCBufferSize = @Input.TOCBufferSize;
	@TechComments = @Input.TechComments;
  finally
    close @Input;
  end;
   
  --Get the appointment count for this Advisor/Timeslot.
  @Count = (Select Count(*) from Appointments where AdvisorID = @AdvisorID and StartTime = @StartTime);
  @NewIDString = NEWIDSTRING(B);
  -- Insert the Record
  if @Count = 0 then

    Insert Into Appointments (AppointmentID, AdvisorID, Customer, StartTime, Created, StreetAddress, City,
	State, Zip, HomePhone, HomeExt, BestPhoneToday, BestExt, CellPhone, EMail, PromiseTime, Comments, RONumber,
	TagNumber, Status, VIN, ModelYear, Make, Model, Mileage, ModelColor, LicenseNumber, PurchaseOrder, MaintenanceDuration, 
	HeavyDuration, DriveabilityDuration, CreatedByID, Notes, ExtendedWarranty, Requisite, Shuttle, TeamID, TOCBufferSize, TechComments) 
	Values ( @NewIDString, @AdvisorID, @Customer, @StartTime, Now(), @StreetAddress, @City, @State, @Zip, @HomePhone, 
	@HomeExt, @BestPhone, @BestExt, @CellPhone, @EMail, @PromiseTime, @Comments, @RONumber, @TagNumber, @Status, @VIN, 
	@ModelYear,	@Make, @Model, @Mileage, @ModelColor, @LicenseNumber, @PurchaseOrder, @MaintenanceDuration, @HeavyDuration, 
	@DriveabilityDuration, @CreatedByID, @Notes, @ExtendedWarranty, @Requisite, @Shuttle, @TeamID, @TOCBufferSize, @TechComments);

	--insert into [__Output] Values (@AdvisorID);
	//Timestamp here if status <> Appointment
	if @Status = 'Dispatched' then
--	  Update Appointments set ArrivalTime = @ArrivalTime where AppointmentID = @NewIDString;
	elseif @Status = 'Wait' then
--	  Update Appointments set ArrivalTime = @ArrivalTime, Status = 'Dispatched' where AppointmentID = @NewIDString;
      Update Appointments set Status = 'Dispatched' where AppointmentID = @NewIDString;
	endif;
    insert into [__Output] Values (@NewIDString);
  else 
    insert into [__Output] Values ('Rejected');   
  endif; 
   

   
   
   
   
   END;

CREATE PROCEDURE CreateAppointment2
   ( 
      StartTime TIMESTAMP,
      AdvisorID CHAR ( 40 ),
      Customer CHAR ( 40 ),
      StreetAddress CHAR ( 45 ),
      City CHAR ( 35 ),
      State CHAR ( 2 ),
      Zip CHAR ( 10 ),
      HomePhone CHAR ( 30 ),
      HomeExt CHAR ( 8 ),
      BestPhone CHAR ( 30 ),
      BestExt CHAR ( 8 ),
      CellPhone CHAR ( 30 ),
      EMail CHAR ( 100 ),
      PromiseTime TIMESTAMP,
      Comments Memo,
      RONumber CHAR ( 10 ),
      TagNumber CHAR ( 10 ),
      Status CHAR ( 25 ),
      VIN CHAR ( 17 ),
      ModelYear Integer,
      Make CHAR ( 25 ),
      Model CHAR ( 25 ),
      Mileage Integer,
      ModelColor CHAR ( 25 ),
      LicenseNumber CHAR ( 12 ),
      PurchaseOrder CHAR ( 20 ),
      MaintenanceDuration DOUBLE ( 15 ),
      HeavyDuration DOUBLE ( 15 ),
      DriveabilityDuration DOUBLE ( 15 ),
      CreatedByID CHAR ( 40 ),
      Notes Memo,
      ExtendedWarranty LOGICAL,
      Requisite CHAR ( 14 ),
      Shuttle CHAR ( 5 ),
      TeamID CHAR ( 40 ),
      TOCBufferSize DOUBLE ( 15 ),
      PreAuthorizationAmount DOUBLE ( 15 ),
      PreAuthorizationCreatedByID CHAR ( 40 ),
      PreAuthorizationLocation CHAR ( 40 ),
      PreAuthorizationNotes Memo,
      ReturnValue CHAR ( 40 ) OUTPUT
   ) 
   BEGIN 
                                                        
  -- Drop Procedure CreateAppointment2;
  -- Execute Procedure CreateAppointment2('2008-07-29 07:15:00', 'Dan E.', 'Joe Blow', '', '', '', '', '', '', '', '', '', '', '2008-07-28 07:15:00', '', '', '', '')             

  declare @Input Cursor as select * from __Input;
  declare @AdvisorID String;
  declare @StartTime TimeStamp;
  declare @ReturnValue String;
  declare @Customer String;
  declare @StreetAddress String;
  declare @City String;
  declare @State String;
  declare @Zip String;
  declare @HomePhone String;
  declare @HomeExt String;
  declare @BestPhone String;
  declare @BestExt String;
  declare @CellPhone String;
  declare @EMail String;
  declare @PromiseTime TimeStamp;
  declare @Comments MEMO;
  declare @RONumber String;
  declare @TagNumber String;  
  declare @Status String;
  declare @VIN String;
  declare @ModelYear Integer;
  declare @Make String;
  declare @Model String;  
  declare @Mileage Integer;
  declare @ModelColor String;
  declare @LicenseNumber String;
  declare @PurchaseOrder String;
  declare @MaintenanceDuration Double;  
  declare @HeavyDuration Double;
  declare @DriveabilityDuration Double;
  declare @CreatedByID String;
  declare @Notes MEMO;
  declare @NewIDString String;
  declare @Count Integer;
  declare @ExtendedWarranty Logical;
  declare @Requisite String;
  declare @Shuttle String;
  declare @TeamID String;
  declare @TOCBufferSize Double;
  declare @PreAuthorizationAmount Double;
  declare @PreAuthorizationCreatedByID String;
  declare @PreAuthorizationLocation String;
  declare @PreAuthorizationNotes Memo;

  open @Input;
  try
    fetch @Input;
    @AdvisorID = @Input.AdvisorID;
	@StartTime = @Input.StartTime;
	--@EndTime = @Input.EndTime;
	@Customer = @Input.Customer;
	@StreetAddress = @Input.StreetAddress;
	@City = @Input.City;
	@State = @Input.State;
	@Zip = @Input.Zip;
	@HomePhone = @Input.HomePhone;
	@HomeExt = @Input.HomeExt;
	@BestPhone = @Input.BestPhone;
	@BestExt = @Input.BestExt;
	@CellPhone = @Input.CellPhone;
	@EMail = @Input.EMail;
	@PromiseTime = @Input.PromiseTime;
	@Comments = @Input.Comments;
	@RONumber = @Input.RONumber;
	@TagNumber = @Input.TagNumber;
	@Status = @Input.Status;
	@VIN = @Input.VIN;
	@ModelYear = @Input.ModelYear;
	@Make = @Input.Make;
	@Model = @Input.Model;
	@Mileage = @Input.Mileage;
	@ModelColor = @Input.ModelColor;
	@LicenseNumber = @Input.LicenseNumber;
	@PurchaseOrder = @Input.PurchaseOrder;
	@MaintenanceDuration = @Input.MaintenanceDuration;
	@HeavyDuration = @Input.HeavyDuration;
	@DriveabilityDuration = @Input.DriveabilityDuration;
	@CreatedByID = @Input.CreatedByID;
	@Notes = @Input.Notes;
	@ExtendedWarranty = @Input.ExtendedWarranty;
	@Requisite = @Input.Requisite;		
	@Shuttle = @Input.Shuttle;
	@TeamID = @Input.TeamID;
	@TOCBufferSize = @Input.TOCBufferSize;
	@PreAuthorizationAmount = @Input.PreAuthorizationAmount;
	@PreAuthorizationCreatedByID = @Input.PreAuthorizationCreatedByID;
	@PreAuthorizationLocation = @Input.PreAuthorizationLocation;
	@PreAuthorizationNotes = @Input.PreAuthorizationNotes;
  finally
    close @Input;
  end;
   
  --Get the appointment count for this Advisor/Timeslot.
  @Count = (Select Count(*) from Appointments where AdvisorID = @AdvisorID and StartTime = @StartTime);
  @NewIDString = NEWIDSTRING(B);
  -- Insert the Record
  if @Count = 0 then

    Insert Into Appointments (AppointmentID, AdvisorID, Customer, StartTime, Created, StreetAddress, City,
	State, Zip, HomePhone, HomeExt, BestPhoneToday, BestExt, CellPhone, EMail, PromiseTime, Comments, RONumber,
	TagNumber, Status, VIN, ModelYear, Make, Model, Mileage, ModelColor, LicenseNumber, PurchaseOrder, MaintenanceDuration, 
	HeavyDuration, DriveabilityDuration, CreatedByID, Notes, ExtendedWarranty, Requisite, Shuttle, TeamID, TOCBufferSize) 
	Values (@NewIDString, @AdvisorID, @Customer, @StartTime, Now(), @StreetAddress, @City, @State, @Zip, @HomePhone, 
	@HomeExt, @BestPhone, @BestExt, @CellPhone, @EMail, @PromiseTime, @Comments, @RONumber, @TagNumber, @Status, @VIN, 
	@ModelYear,	@Make, @Model, @Mileage, @ModelColor, @LicenseNumber, @PurchaseOrder, @MaintenanceDuration, @HeavyDuration, 
	@DriveabilityDuration, @CreatedByID, @Notes, @ExtendedWarranty, @Requisite, @Shuttle, @TeamID, @TOCBufferSize);
    
	if @PreAuthorizationAmount = 0 or @PreAuthorizationAmount is Null then
	-- Do nothing
	else
	  Insert into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS)
	  values (@NewIDString, @PreAuthorizationAmount, @PreAuthorizationCreatedByID, @PreAuthorizationLocation, @PreAuthorizationNotes, Now(), Null);
	endif;
	--insert into [__Output] Values (@AdvisorID);
	//Timestamp here if status <> Appointment
	if @Status = 'Dispatched' or @Status = 'Wait' or @Status = 'Blueprint' or @Status = 'Kitted' then
	  Update Appointments set ArrivalTime = Now() where AppointmentID = @NewIDString;
	endif;
    insert into [__Output] Values (@NewIDString);
  else 
    insert into [__Output] Values ('Rejected');   
  endif;

   
   
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE EditPreAuthorizationAmount
   ( 
      AppointmentID CHAR ( 40 ),
      PreAuthorizationAmount CHAR ( 40 ),
      PreAuthorizationCreatedByID CHAR ( 40 ),
      PreAuthorizationLocation CHAR ( 40 ),
      PreAuthorizationNotes Memo,
      ReturnValue LOGICAL OUTPUT
   ) 
   BEGIN 
                                          
-- Drop procedure EditPreAuthorizationAmount;
-- Execute Procedure EditPreAuthorizationAmount('[bfc13062-6718-6c4e-909e-a2301885e265]', '12.40', 'jschneider', 'Consultant', 'Second Note23');
  declare @Input Cursor as select * from __Input;
  declare @Now String;
  declare @Stmt String;
  declare @AppointmentID String;
  declare @PreAuthorizationCreatedByID String;
  declare @PreAuthorizationAmount String;  
  declare @PreAuthorizationLocation String;
  declare @PreAuthorizationNotes String;
  declare @ExistingCount Integer;
  declare @ExistingAmountNote Integer;

  open @Input;
  try
    fetch @Input;
    @AppointmentID = @Input.AppointmentID;
	@PreAuthorizationCreatedByID = @Input.PreAuthorizationCreatedByID;
	@PreAuthorizationAmount = @Input.PreAuthorizationAmount;
	@PreAuthorizationLocation = @Input.PreAuthorizationLocation;
	@PreAuthorizationNotes = @Input.PreAuthorizationNotes;
  finally
    close @Input;
  end;
  
  -- Get the Current Time/Date
  @Now = Convert(Now(), SQL_CHAR);
  
  -- Find out if null thru timestamp exists
  @ExistingCount = (Select count(*) from PreAuthorizationAmount where AppointmentID = @AppointmentID and ThruTS is Null);
  try
    if @ExistingCount > 0 then
	  --First find out if PreAuthorizationAmount is null
	  if @PreAuthorizationAmount = '' then
	    @PreAuthorizationAmount = '0';
	  endif;  
	  --Find out if it exists first with same note
	  @ExistingAmountNote = (Select Count(*) from PreAuthorizationAmount where AppointmentID = @AppointmentID 
	  and ((PreAuthorizationNotes = @PreAuthorizationNotes) or ((PreAuthorizationNotes = '') and (@PreAuthorizationNotes is Null))) and Convert(PreAuthorizationAmount, SQL_DOUBLE) = Convert(@PreAuthorizationAmount, SQL_DOUBLE)
	  and ThruTS is Null);
	  if @ExistingAmountNote = 0 then
	    Begin Transaction;
	      --First close out existing
          @Stmt = 'Update PreAuthorizationAmount set ThruTS = ''' + @Now + ''' where AppointmentID = ''' +
  	      @AppointmentID + ''' and ThruTS is Null';
		
          execute Immediate @Stmt;
		
		  --Now insert new record
		  --Make sure PreAuthorizationCreatedByID is not empty string
		  if @PreAuthorizationCreatedByID = '' then
		
            @Stmt = 'Insert Into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, ' +  
	          'PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS) Values (''' + @AppointmentID + ''', Convert(' + @PreAuthorizationAmount +
	          ', SQL_DOUBLE), Null, ''' + @PreAuthorizationLocation + ''', ''' +  Coalesce(@PreAuthorizationNotes, '') + ''', ''' +
	          @Now + ''', Null)';
	      else
            @Stmt = 'Insert Into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, ' +  
	          'PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS) Values (''' + @AppointmentID + ''', Convert(' + @PreAuthorizationAmount +
	          ', SQL_DOUBLE), ''' + @PreAuthorizationCreatedByID + ''', ''' + @PreAuthorizationLocation + ''', ''' +  Coalesce(@PreAuthorizationNotes, '') + ''', ''' +
	          @Now + ''', Null)';		
		  endif;
	      execute Immediate @Stmt;
	    Commit Work;
	  endif;  	
    else
      --Just treat as new
      if @PreAuthorizationAmount <> '' then
	    if @PreAuthorizationCreatedByID = '' then
          @Stmt = 'Insert Into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, ' +  
	        'PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS) Values (''' + @AppointmentID + ''', Convert(''' + @PreAuthorizationAmount +
	        ''', SQL_DOUBLE), Null, ''' + @PreAuthorizationLocation + ''', ''' + Coalesce(@PreAuthorizationNotes, '') + ''', ''' +
	        @Now + ''', Null)';
		else
          @Stmt = 'Insert Into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, ' +  
	        'PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS) Values (''' + @AppointmentID + ''', Convert(''' + @PreAuthorizationAmount +
	        ''', SQL_DOUBLE), ''' + @PreAuthorizationCreatedByID + ''', ''' + @PreAuthorizationLocation + ''', ''' + Coalesce(@PreAuthorizationNotes, '') + ''', ''' +
	        @Now + ''', Null)';
		endif;
	    execute Immediate @Stmt;
	  endif;
    endif;
    insert into [__Output] Values (True);
  catch all
    insert into [__Output] Values (False);  
  end try;



   
   
   
   
   
   
   END;

CREATE PROCEDURE CreateAppointmentFromPaste2
   ( 
      StartTime TIMESTAMP,
      AdvisorID CHAR ( 40 ),
      Customer CHAR ( 40 ),
      StreetAddress CHAR ( 45 ),
      City CHAR ( 35 ),
      State CHAR ( 2 ),
      Zip CHAR ( 10 ),
      HomePhone CHAR ( 30 ),
      HomeExt CHAR ( 8 ),
      BestPhone CHAR ( 30 ),
      BestExt CHAR ( 8 ),
      CellPhone CHAR ( 30 ),
      EMail CHAR ( 100 ),
      PromiseTime TIMESTAMP,
      ArrivalTime TIMESTAMP,
      Comments Memo,
      RONumber CHAR ( 10 ),
      TagNumber CHAR ( 10 ),
      Status CHAR ( 25 ),
      VIN CHAR ( 17 ),
      ModelYear Integer,
      Make CHAR ( 25 ),
      Model CHAR ( 25 ),
      Mileage Integer,
      ModelColor CHAR ( 25 ),
      LicenseNumber CHAR ( 12 ),
      PurchaseOrder CHAR ( 20 ),
      MaintenanceDuration DOUBLE ( 15 ),
      HeavyDuration DOUBLE ( 15 ),
      DriveabilityDuration DOUBLE ( 15 ),
      CreatedByID CHAR ( 40 ),
      Notes Memo,
      ExtendedWarranty LOGICAL,
      Requisite CHAR ( 14 ),
      Shuttle CHAR ( 5 ),
      TeamID CHAR ( 40 ),
      TOCBufferSize DOUBLE ( 15 ),
      TechComments Memo,
      PreAuthorizationAmount DOUBLE ( 15 ),
      PreAuthorizationCreatedByID CHAR ( 40 ),
      PreAuthorizationLocation CHAR ( 40 ),
      PreAuthorizationNotes Memo,
      Alignment LOGICAL,
      ReturnValue CHAR ( 40 ) OUTPUT
   ) 
   BEGIN 
                                                                                      
  -- Drop Procedure CreateAppointmentFromPaste2;
  -- Execute Procedure CreateAppointmentFromPaste2('2009-03-05 11:30:00', 'Brad S.', 'Joe Blow', '', '', '', '', '', '', '', '', '', '', '2008-07-28 07:15:00', '', '', '', '', 0, 'rnokelby', 'Consultant', 'Just a Note', False);             

  declare @Input Cursor as select * from __Input;
  declare @AdvisorID String;
  declare @StartTime TimeStamp;
  declare @ReturnValue String;
  declare @Customer String;
  declare @StreetAddress String;
  declare @City String;
  declare @State String;
  declare @Zip String;
  declare @HomePhone String;
  declare @HomeExt String;
  declare @BestPhone String;
  declare @BestExt String;
  declare @CellPhone String;
  declare @EMail String;
  declare @PromiseTime TimeStamp;
  declare @ArrivalTime TimeStamp;
  declare @Comments MEMO;
  declare @RONumber String;
  declare @TagNumber String;  
  declare @Status String;
  declare @VIN String;
  declare @ModelYear Integer;
  declare @Make String;
  declare @Model String;  
  declare @Mileage Integer;
  declare @ModelColor String;
  declare @LicenseNumber String;
  declare @PurchaseOrder String;
  declare @MaintenanceDuration Double;  
  declare @HeavyDuration Double;
  declare @DriveabilityDuration Double;
  declare @CreatedByID String;
  declare @Notes MEMO;
  declare @NewIDString String;
  declare @Count Integer;
  declare @ExtendedWarranty Logical;
  declare @Requisite String;
  declare @Shuttle String;
  declare @TeamID String;
  declare @TOCBufferSize Double;
  declare @TechComments MEMO;
  declare @PreAuthorizationAmount Double;
  declare @PreAuthorizationCreatedByID String;
  declare @PreAuthorizationLocation String;
  declare @PreAuthorizationNotes MEMO;
  declare @Alignment Logical;

  open @Input;
  try
    fetch @Input;
    @AdvisorID = @Input.AdvisorID;
	@StartTime = @Input.StartTime;
	--@EndTime = @Input.EndTime;
	@Customer = @Input.Customer;
	@StreetAddress = @Input.StreetAddress;
	@City = @Input.City;
	@State = @Input.State;
	@Zip = @Input.Zip;
	@HomePhone = @Input.HomePhone;
	@HomeExt = @Input.HomeExt;
	@BestPhone = @Input.BestPhone;
	@BestExt = @Input.BestExt;
	@CellPhone = @Input.CellPhone;
	@EMail = @Input.EMail;
	@PromiseTime = @Input.PromiseTime;
	@ArrivalTime = @Input.ArrivalTime;
	@Comments = @Input.Comments;
	@RONumber = @Input.RONumber;
	@TagNumber = @Input.TagNumber;
	@Status = @Input.Status;
	@VIN = @Input.VIN;
	@ModelYear = @Input.ModelYear;
	@Make = @Input.Make;
	@Model = @Input.Model;
	@Mileage = @Input.Mileage;
	@ModelColor = @Input.ModelColor;
	@LicenseNumber = @Input.LicenseNumber;
	@PurchaseOrder = @Input.PurchaseOrder;
	@MaintenanceDuration = @Input.MaintenanceDuration;
	@HeavyDuration = @Input.HeavyDuration;
	@DriveabilityDuration = @Input.DriveabilityDuration;
	@CreatedByID = @Input.CreatedByID;
	@Notes = @Input.Notes;
	@ExtendedWarranty = @Input.ExtendedWarranty;
	@Requisite = @Input.Requisite;		
	@Shuttle = @Input.Shuttle;
	@TeamID = @Input.TeamID;
	@TOCBufferSize = @Input.TOCBufferSize;
	@TechComments = @Input.TechComments;
	@PreAuthorizationAmount = @Input.PreAuthorizationAmount;
	@PreAuthorizationCreatedByID = @Input.PreAuthorizationCreatedByID;
	@PreAuthorizationLocation = @Input.PreAuthorizationLocation;
	@PreAuthorizationNotes = @Input.PreAuthorizationNotes;
	@Alignment = @Input.Alignment;
  finally
    close @Input;
  end;
   
  --Get the appointment count for this Advisor/Timeslot.
  @Count = (Select Count(*) from Appointments where AdvisorID = @AdvisorID and StartTime = @StartTime);
  @NewIDString = NEWIDSTRING(B);
  -- Insert the Record
  if @Count = 0 then

    Insert Into Appointments (AppointmentID, AdvisorID, Customer, StartTime, Created, StreetAddress, City,
	State, Zip, HomePhone, HomeExt, BestPhoneToday, BestExt, CellPhone, EMail, PromiseTime, Comments, RONumber,
	TagNumber, Status, VIN, ModelYear, Make, Model, Mileage, ModelColor, LicenseNumber, PurchaseOrder, MaintenanceDuration, 
	HeavyDuration, DriveabilityDuration, CreatedByID, Notes, ExtendedWarranty, Requisite, Shuttle, TeamID, TOCBufferSize, TechComments, Alignment) 
	Values ( @NewIDString, @AdvisorID, @Customer, @StartTime, Now(), @StreetAddress, @City, @State, @Zip, @HomePhone, 
	@HomeExt, @BestPhone, @BestExt, @CellPhone, @EMail, @PromiseTime, @Comments, @RONumber, @TagNumber, @Status, @VIN, 
	@ModelYear,	@Make, @Model, @Mileage, @ModelColor, @LicenseNumber, @PurchaseOrder, @MaintenanceDuration, @HeavyDuration, 
	@DriveabilityDuration, @CreatedByID, @Notes, @ExtendedWarranty, @Requisite, @Shuttle, @TeamID, @TOCBufferSize, @TechComments, @Alignment);

	--insert into [__Output] Values (@AdvisorID);
	//Timestamp here if status <> Appointment
	if @Status = 'Dispatched' then
	  Update Appointments set ArrivalTime = Now() where AppointmentID = @NewIDString;
	elseif @Status = 'Wait' then
	  Update Appointments set ArrivalTime = Now(), Status = 'Dispatched' where AppointmentID = @NewIDString;
--      Update Appointments set Status = 'Dispatched' where AppointmentID = @NewIDString;
	endif;


    --Execute procedure EditPreAuthorizationAmount
    if @PreAuthorizationAmount <> 0 then
	  Execute procedure EditPreAuthorizationAmount(@NewIDString, Convert(@PreAuthorizationAmount, SQL_CHAR), @PreAuthorizationCreatedByID, @PreAuthorizationLocation, @PreAuthorizationNotes);
	endif;  
		
		
    insert into [__Output] Values (@NewIDString);
  else 
    insert into [__Output] Values ('Rejected');   
  endif; 
   

   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE EditAppointment2
   ( 
      AppointmentID CHAR ( 40 ),
      Customer CHAR ( 40 ),
      StreetAddress CHAR ( 45 ),
      City CHAR ( 35 ),
      State CHAR ( 2 ),
      Zip CHAR ( 10 ),
      HomePhone CHAR ( 30 ),
      HomeExt CHAR ( 8 ),
      BestPhoneToday CHAR ( 30 ),
      BestExt CHAR ( 8 ),
      CellPhone CHAR ( 30 ),
      EMail CHAR ( 100 ),
      PromiseTime TIMESTAMP,
      Comments Memo,
      RONumber CHAR ( 10 ),
      TagNumber CHAR ( 10 ),
      Status CHAR ( 25 ),
      VIN CHAR ( 17 ),
      ModelYear Integer,
      Make CHAR ( 25 ),
      Model CHAR ( 25 ),
      Mileage Integer,
      ModelColor CHAR ( 25 ),
      LicenseNumber CHAR ( 12 ),
      PurchaseOrder CHAR ( 20 ),
      MaintenanceDuration DOUBLE ( 15 ),
      HeavyDuration DOUBLE ( 15 ),
      DriveabilityDuration DOUBLE ( 15 ),
      Notes Memo,
      ExtendedWarranty LOGICAL,
      Requisite CHAR ( 14 ),
      Shuttle CHAR ( 5 ),
      TeamID CHAR ( 40 ),
      TOCBufferSize DOUBLE ( 15 ),
      Alignment LOGICAL
   ) 
   BEGIN 
                                              
  -- Drop Procedure EditAppointment2;
  -- Execute Procedure EditAppointment2('2008-07-29 07:15:00', 'Dan E.', 'Joe Blow', '', '', '', '', '', '', '', '', '', '', '2008-07-28 07:15:00', '', '', '', '', False)             

  declare @Input Cursor as select * from __Input;
  declare @AppointmentID String;
  declare @Customer String;
  declare @StreetAddress String;
  declare @City String;
  declare @State String;
  declare @Zip String;
  declare @HomePhone String;
  declare @HomeExt String;
  declare @BestPhoneToday String;
  declare @BestExt String;
  declare @CellPhone String;
  declare @EMail String;
  declare @PromiseTime TimeStamp;
  declare @Comments MEMO;
  declare @RONumber String;
  declare @TagNumber String;  
  declare @Status String;
  declare @VIN String;
  declare @ModelYear Integer;
  declare @Make String;
  declare @Model String;  
  declare @Mileage Integer;
  declare @ModelColor String;
  declare @LicenseNumber String;
  declare @PurchaseOrder String;
  declare @MaintenanceDuration Double;  
  declare @HeavyDuration Double;
  declare @DriveabilityDuration Double;
  declare @Notes MEMO;
  declare @ExtendedWarranty Logical;
  declare @Requisite String;
  declare @Shuttle String;
  declare @TeamID String;
  declare @TOCBufferSize Double;
  declare @CurrentStatus String;
  declare @ArrivalTime TimeStamp;
  declare @TOCFinishTime TimeStamp;
  declare @Alignment Logical;

  open @Input;
  try
    fetch @Input;
	--@EndTime = @Input.EndTime;
	@AppointmentID = @Input.AppointmentID;
	@Customer = @Input.Customer;
	@StreetAddress = @Input.StreetAddress;
	@City = @Input.City;
	@State = @Input.State;
	@Zip = @Input.Zip;
	@HomePhone = @Input.HomePhone;
	@HomeExt = @Input.HomeExt;
	@BestPhoneToday = @Input.BestPhoneToday;
	@BestExt = @Input.BestExt;
	@CellPhone = @Input.CellPhone;
	@EMail = @Input.EMail;
	@PromiseTime = @Input.PromiseTime;
	@Comments = @Input.Comments;
	@RONumber = @Input.RONumber;
	@TagNumber = @Input.TagNumber;
	@Status = @Input.Status;
	@VIN = @Input.VIN;
	@ModelYear = @Input.ModelYear;
	@Make = @Input.Make;
	@Model = @Input.Model;
	@Mileage = @Input.Mileage;
	@ModelColor = @Input.ModelColor;
	@LicenseNumber = @Input.LicenseNumber;
	@PurchaseOrder = @Input.PurchaseOrder;
	@MaintenanceDuration = @Input.MaintenanceDuration;
	@HeavyDuration = @Input.HeavyDuration;
	@DriveabilityDuration = @Input.DriveabilityDuration;
	@Notes = @Input.Notes;
	@ExtendedWarranty = @Input.ExtendedWarranty;
	@Requisite = @Input.Requisite;		
	@Shuttle = @Input.Shuttle;
	@TeamID = @Input.TeamID;
	@TOCBufferSize = @Input.TOCBufferSize;
	@Alignment = @Input.Alignment;
  finally
    close @Input;
  end;
  @CurrentStatus = (Select Status from Appointments where AppointmentID = @AppointmentID);
  @ArrivalTime = (Select ArrivalTime from Appointments where AppointmentID = @AppointmentID);
  @TOCFinishTime = (Select TOCFinishTime from Appointments where AppointmentID = @AppointmentID); 
  -- Update the Record
	
    Update Appointments Set Customer = @Customer, StreetAddress = @StreetAddress, City = @City, State = @State,
	Zip = @Zip, HomePhone = @HomePhone, HomeExt = @HomeExt, BestPhoneToday = @BestPhoneToday, BestExt = @BestExt,
	CellPhone = @CellPhone, EMail = @EMail, PromiseTime = @PromiseTime, Comments = @Comments, RONumber = @RONumber,
	TagNumber = @TagNumber, Status = @Status, VIN = @VIN, ModelYear = @ModelYear, Make = @Make, Model = @Model,
	Mileage = @Mileage, ModelColor = @ModelColor, LicenseNumber = @LicenseNumber, PurchaseOrder = @PurchaseOrder,
	MaintenanceDuration = @MaintenanceDuration, HeavyDuration = @HeavyDuration, 
	DriveabilityDuration = @DriveabilityDuration, Notes = @Notes, ExtendedWarranty = @ExtendedWarranty, 
	Requisite = @Requisite, Shuttle = @Shuttle, TeamID = @TeamID, TOCBufferSize = @TOCBufferSize, Alignment = @Alignment
	where AppointmentID = @AppointmentID; 
    
    if (@Status <> @CurrentStatus) and (@Status = 'Wait' or @Status = 'Dispatched' or @Status = 'Blueprint' or @Status = 'Kitted') then
	  if @ArrivalTime is Null then
	    Update Appointments set ArrivalTime = Now() where AppointmentID = @AppointmentID;
	  endif;
	elseif (@Status <> @CurrentStatus) and (@Status = 'Done') then
	  if @TOCFinishTime is Null then
	    Update Appointments set TOCFinishTime = Now() where AppointmentID = @AppointmentID;
	  endif;	   
	endif; 
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetGlobalCapacity
   ( 
      InDate DATE,
      DriveabilityHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceHours DOUBLE ( 15 ) OUTPUT,
      HeavyHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityDuration DOUBLE ( 15 ) OUTPUT,
      MaintenanceDuration DOUBLE ( 15 ) OUTPUT,
      HeavyDuration DOUBLE ( 15 ) OUTPUT,
      Goal Integer OUTPUT
   ) 
   BEGIN 
                          -- Drop Procedure GetGlobalCapacity;
  -- Execute Procedure GetGlobalCapacity('6/11/2010');
  declare @Input Cursor as select * from __Input;  
  declare @InDate Date;
  
  declare @DHours Double;
  declare @MHours Double;
  declare @HHours Double;
  declare @DDuration Double;
  declare @MDuration Double;
  declare @HDuration Double;
  declare @SaturdayTeamWorking String; 
  
  declare @NMHours Double;
  declare @NDHours Double;
  declare @NMDuration Double;
  declare @NDDuration Double;
  declare @NFSaturday Logical;

  
  declare @QSMHours Double;

  declare @AMHours Double;
  declare @AFSaturday Logical;
  declare @Goal Integer;   
  
  open @Input;
  try
    fetch @Input;
    @InDate = @Input.InDate;
  finally
    close @Input;
  end;
  
  
  --Initialize
  @DHours = 0;
  @MHours = 0;
  @HHours = 0;
  @DDuration = 0;
  @MDuration = 0;
  @HDuration = 0;

  @NDHours = 0;
  @NMHours = 0;
  
  @QSMHours = 0;
  
  @AMHours = 0;  
  @Goal = 0;

  
  @SaturdayTeamWorking = '';

  if (DayOfWeek(@InDate) = 7) then
    @SaturdayTeamWorking = (Select Name from Weekends where Date = Convert(@InDate, SQL_DATE));
	if @SaturdayTeamWorking <> '' then
	  @HHours = (Select Count(*) from Technicians T
	         where TeamID = 
			 (Select TeamID from Teams where Name = @SaturdayTeamWorking and DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day'))
             and T.SkillLevel = 'H'
             and T.Active = True
			 and T.ShortName <> 'Duane R.') * 8 -
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'H'
			 and D.Description = 'Main Shop - Day');
			 
      @DHours = (Select Count(*) from Technicians T
	         where TeamID = 
			 (Select TeamID from Teams where Name = @SaturdayTeamWorking and DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day'))
             and T.SkillLevel = 'D'
             and T.Active = True) * 8 - 
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'D'
			 and D.Description = 'Main Shop - Day');
      @MHours = (Select Count(*) from Technicians T
	         where TeamID = 
			 (Select TeamID from Teams where Name = @SaturdayTeamWorking and DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day'))
             and T.SkillLevel = 'M'
             and T.Active = True) * 8 - 
			 (Select Coalesce(Sum(E.Hours), 0) As Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'M'
			 and D.Description = 'Main Shop - Day');			 
	
	else
      @HHours = 0;
	  @MHours = 0;
	  @DHours = 0;
    endif;			 
			  			   
  else
    @HHours = (Select Count(*) from Technicians T
             left outer join Teams Tea on T.TeamID = Tea.TeamID
             left outer join Departments D on Tea.DepartmentID = D.DepartmentID
             where T.SkillLevel = 'H'
             and T.Active = True
             and D.Description = 'Main Shop - Day') * 8 -
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'H'
			 and D.Description = 'Main Shop - Day');
    @DHours = (select Count(*) from Technicians T
             left outer join Teams Tea on T.TeamID = Tea.TeamID
             left outer join Departments D on Tea.DepartmentID = D.DepartmentID
             where T.SkillLevel = 'D'
             and T.Active = True
             and D.Description = 'Main Shop - Day') * 8 - 
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'D'
			 and D.Description = 'Main Shop - Day');
    @MHours = (select Count(*) from Technicians T
             left outer join Teams Tea on T.TeamID = Tea.TeamID
             left outer join Departments D on Tea.DepartmentID = D.DepartmentID
             where T.SkillLevel = 'M'
             and T.Active = True
             and D.Description = 'Main Shop - Day') * 8 - 
			 (Select Coalesce(Sum(E.Hours), 0) As Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'M'
			 and D.Description = 'Main Shop - Day');				 			 
  endif;
		      
  @DDuration = (Select Coalesce(Sum(DriveabilityDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'Hold'
			   and Status <> 'No Show'
			   and Status <> 'Pend Pts'
			   and Status <> 'Pend Auth'
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID In 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Main Shop - Night')))); 
  
  @MDuration = (Select Coalesce(Sum(MaintenanceDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'Hold'
			   and Status <> 'No Show'
			   and Status <> 'Pend Pts'
			   and Status <> 'Pend Auth'			   
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID In 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Main Shop - Night'
			  or Description = 'Quick Service' or Description = 'Aftermarket')))); 
			 		 	 
  @HDuration = (Select Coalesce(Sum(HeavyDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'Hold'
			   and Status <> 'No Show'
			   and Status <> 'Pend Pts'
			   and Status <> 'Pend Auth'			   
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID In 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Main Shop - Night'))));
			 
			 
  if (DayOfWeek(@InDate) = 7) then
      @NFSaturday = True;  
      @NMHours = 0;
	  @NDHours = 0;
  else	 
    @NMHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.SkillLevel = 'M'			  
              and T.Active = True
              and D.Description = 'Main Shop - Night')
	    	  * 10.5 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsNight E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and T.SkillLevel = 'M'			  
			  and D.Description = 'Main Shop - Night');
    @NDHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.SkillLevel = 'D'			  
              and T.Active = True
              and D.Description = 'Main Shop - Night')
	    	  * 10.5 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsNight E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and T.SkillLevel = 'D'			  
			  and D.Description = 'Main Shop - Night');			  
  endif; 
				 

  if (DayOfWeek(@InDate) = 7) then
 
    @QSMHours = 8 - 
			   (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsAftermarket E 
			   left outer join Technicians T on E.TechNumber = T.TechNumber
			   left outer join Teams Tea on T.TeamID = Tea.TeamID
			   left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			   where E.Date = Convert(@InDate, SQL_DATE)
			   and D.Description = 'Quick Service');
  else	 
    @QSMHours = (select Count(*) from Technicians T
               left outer join Teams Tea on T.TeamID = Tea.TeamID
               left outer join Departments D on Tea.DepartmentID = D.DepartmentID
               where T.Active = True
               and D.Description = 'Quick Service')
	    	   * 10.5 - 
			   (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsQuickService E 
		 	   left outer join Technicians T on E.TechNumber = T.TechNumber
			   left outer join Teams Tea on T.TeamID = Tea.TeamID
			   left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			   where E.Date = Convert(@InDate, SQL_DATE)
			   and D.Description = 'Quick Service');
  endif;



  if (DayOfWeek(@InDate) = 7) then
    if (Select Count(*) from WeekendsAftermarket where Date = Convert(@InDate, SQL_DATE)) > 0 then
      @AFSaturday = True;  
      @AMHours = 8 - 
			   (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsAftermarket E 
			   left outer join Technicians T on E.TechNumber = T.TechNumber
			   left outer join Teams Tea on T.TeamID = Tea.TeamID
			   left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			   where E.Date = Convert(@InDate, SQL_DATE)
			   and T.SkillLevel = 'R'
			   and D.Description = 'Aftermarket');
	else
	  @AMHours = 0;
	endif;
  else	 
    @AMHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.SkillLevel = 'R'
              and T.Active = True
              and D.Description = 'Aftermarket')
	    	  * 10.5 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsAftermarket E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and T.SkillLevel = 'R'
			  and D.Description = 'Aftermarket');
  endif;

  @MHours = @MHours + @NMHours + @QSMHours + @AMHours;
  @DHours = @DHours + @NDHours;

  @HHours = @HHours * (Select Heavy from CorrectionFactors);			 			 
  @DHours = @DHours * (Select Driveability from CorrectionFactors);			 
  @MHours = @MHours * (Select Maintenance from CorrectionFactors);
  @Goal = (Select Goal from Goals where GoalDate = Convert(@InDate, SQL_DATE));
  if @Goal is Null then 
    @Goal = 0;
  endif;
  
		             
  Insert Into [__Output] (DriveabilityHours, MaintenanceHours, HeavyHours, DriveabilityDuration, MaintenanceDuration, HeavyDuration, Goal) 
  values(@DHours, @MHours, @HHours, @DDuration, @MDuration, @HDuration, @Goal);

   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetBlueprintTechDayJobsByDate
   ( 
      DisplayedDate DATE,
      C CHAR ( 1 ) OUTPUT,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      TechStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      PA DOUBLE ( 15 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      TechWrk CHAR ( 40 ) OUTPUT,
      ReleaseTime TIMESTAMP OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      ArrivalTime TIMESTAMP OUTPUT,
      M DOUBLE ( 15 ) OUTPUT,
      D DOUBLE ( 15 ) OUTPUT,
      H DOUBLE ( 15 ) OUTPUT,
      TOCBufferSize DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT,
      Priority LOGICAL OUTPUT
   ) 
   BEGIN 
                                                                                                                                                                        
-- Drop Procedure GetBlueprintTechDayJobsByDate;       
-- execute procedure GetBlueprintTechDayJobsByDate('02/18/2010');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
  SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
    When (NOW() > TimestampAdd(SQL_TSI_HOUR, 3, ArrivalTime)) and (NOW() > TimestampAdd(SQL_TSI_HOUR, 3, StartTime)
	and Requisite <> 'WalkIn/TowIn') then 'B'
	When (NOW() > TimestampAdd(SQL_TSI_HOUR, 2, ArrivalTime)) and (NOW() > TimestampAdd(SQL_TSI_HOUR, 2, StartTime)
	and Requisite <> 'WalkIn/TowIn') then 'R'
	When (NOW() > TimestampAdd(SQL_TSI_HOUR, 1, ArrivalTime)) and (NOW() > TimestampAdd(SQL_TSI_HOUR, 1, StartTime)
	and Requisite <> 'WalkIn/TowIn') then 'Y'
	When (NOW() > TimestampAdd(SQL_TSI_HOUR, 0, ArrivalTime)) and (NOW() > TimestampAdd(SQL_TSI_HOUR, 0, StartTime)
	and Requisite <> 'WalkIn/TowIn') then 'G'	
	Else '' 
  End As C,
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  PA.PreAuthorizationAmount as PA,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H,
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes,
  A.Priority
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID  
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status = 'Blueprint')  
  //Exclude Heavy Include Assigned Day Jobs
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID <> '')))
  and (A.TeamID = '1' or A.TeamID = '2' or A.TeamID = '3' or A.TeamID = '4' or A.TeamID = '5' or ((A.TeamID = '' or A.TeamID is Null) 
       and ((A.HeavyDuration is Null) or (A.HeavyDuration <= 0))))
  //Exclude other stuff	   
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team, Priority Desc,
  -- case when A.MaintenanceDuration > 0 then 2 when A.DriveabilityDuration > 0 then 1 else 0 end DESC,
  Convert(A.ArrivalTime, SQL_DATE),
  case when A.StartTime > A.ArrivalTime then A.StartTime else A.ArrivalTime end;
  
  
  Select
  Case
    When (NOW() > TimestampAdd(SQL_TSI_HOUR, 3, ArrivalTime)) and (NOW() > TimestampAdd(SQL_TSI_HOUR, 3, StartTime)
	and Requisite <> 'WalkIn/TowIn') then 'B'
	When (NOW() > TimestampAdd(SQL_TSI_HOUR, 2, ArrivalTime)) and (NOW() > TimestampAdd(SQL_TSI_HOUR, 2, StartTime)
	and Requisite <> 'WalkIn/TowIn') then 'R'
	When (NOW() > TimestampAdd(SQL_TSI_HOUR, 1, ArrivalTime)) and (NOW() > TimestampAdd(SQL_TSI_HOUR, 1, StartTime)
	and Requisite <> 'WalkIn/TowIn') then 'Y'
	When (NOW() > TimestampAdd(SQL_TSI_HOUR, 0, ArrivalTime)) and (NOW() > TimestampAdd(SQL_TSI_HOUR, 0, StartTime)
	and Requisite <> 'WalkIn/TowIn') then 'G'	
	Else '' 
  End As C, 
  'H' as Team, A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  PA.PreAuthorizationAmount as PA,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  A.MaintenanceDuration as M,
  A.DriveabilityDuration as D,
  A.HeavyDuration as H,
  TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes,
  A.Priority
  into #TechJobsHeavy 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID      
  where  Convert(A.StartTime, SQL_DATE) = @DisplayedDate  
  and (A.Status = 'Blueprint')
  //Include Heavy Only
  and ((D.Description = 'Main Shop - Day') or ((D.Description = 'Main Shop - Night') and (A.TeamID = 'H')))
  and (A.TeamID = 'H' or ((A.TeamID = '' or A.TeamID is Null) and (A.HeavyDuration > 0)))
  //Exclude other stuff
  and ((Convert(A.TOCFinishTime, SQL_DATE) = '12/30/1899') or (A.TOCFinishTime is Null))
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET' 
  and Tea.Name <> ''
  and PA.ThruTS is Null
  order by Priority Desc, 
  Convert(A.ArrivalTime, SQL_DATE),
  case when A.StartTime > A.ArrivalTime then A.StartTime else A.ArrivalTime end;
  
  insert into [__OUTPUT] Select C, Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus, PA,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes, Priority from [#TechJobsAll] 
  union all
  Select C, Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus, PA,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, TOCBufferSize, PreAuthorizationAmount, PreAuthorizationNotes, Priority from [#TechJobsHeavy]; 
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE ChangeStatus2
   ( 
      AppointmentID CHAR ( 40 ),
      CategoryStatus CHAR ( 40 ),
      PerformedBy CHAR ( 40 ),
      Comments Memo,
      ReturnValue LOGICAL OUTPUT
   ) 
   BEGIN 
        -- drop procedure ChangeStatus2;
  -- execute procedure ChangeStatus2('ID1', 'Technician_BlueprintDone', 'rnokelby', '');          
  declare @Input Cursor as select * from __Input;
  declare @AppointmentID String;
  declare @CategoryStatus String;
  declare @PerformedBy String;
  declare @Comments String;  
  declare @Stmt String;
  declare @ReturnValue String;
  declare @Now String;
  declare @CategoryID String;
  declare @CategoryStatusID String;

  open @Input;
  try
    fetch @Input;
    @AppointmentID = Trim(@Input.AppointmentID);
	@CategoryStatus = Trim(@Input.CategoryStatus);
	@PerformedBy = Trim(@Input.PerformedBy);
	@Comments = Trim(@Input.Comments);
  finally
    close @Input;
  end;
  
  -- Get the Current Time/Date
  @Now = Convert(Now(), SQL_CHAR);
  
  -- Get the CategoryID and CategoryStatusID
  @CategoryStatusID = (Select CategoryStatusID from CategoryStatuses where CategoryStatus = @CategoryStatus);
	

  try
      -- Just start a new record
     Insert Into Statuses (AppointmentID, CategoryStatusID, PerformedBy, Comments, DateTime) 
	 Values (@AppointmentID, Trim(@CategoryStatusID), @PerformedBy, @Comments, Convert(@Now, SQL_TIMESTAMP));

    insert into [__Output] Values (True);	
  catch all
    insert into [__Output] Values (False);  
  end try;
   
   
   END;

CREATE PROCEDURE GetAlignmentCount
   ( 
      InDate DATE,
      AlignmentsAvailable Integer OUTPUT,
      AlignmentsFinished Integer OUTPUT
   ) 
   BEGIN 
        --Drop procedure GetAlignmentCount;
  declare @Input Cursor as select * from __Input;  
  declare @InDate Date;
  declare @AlignmentsAvailable Integer;
  declare @AlignmentsFinished Integer;  


  open @Input;
  try
    fetch @Input;
    @InDate = @Input.InDate;
  finally
    close @Input;
  end;   
 

 
 
    @AlignmentsAvailable = (Select Count(Customer) as AlignmentsAvailable from (Select Distinct Customer, VIN from Appointments Ap
    left outer join Advisors Ad on Ad.ShortName = Ap.AdvisorID
    left outer join Teams Te on Ad.TeamID = Te.TeamID
    left outer join Departments De on Te.DepartmentID = De.DepartmentID
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and ((De.Description = 'Main Shop - Day') or (De.Description = 'Quick Service') or (De.Description = 'Aftermarket'))
    and Ap.Alignment = True
    and Ap.Status <> 'Carry Over'
    and Ap.Status <> 'Done'
    and Ap.Status <> 'No Show') as Available);		  
			      
  
  @AlignmentsFinished = (Select Count(Customer) as AlignmentsDone from (Select Distinct Customer, VIN from Appointments Ap
    left outer join Advisors Ad on Ad.ShortName = Ap.AdvisorID
    left outer join Teams Te on Ad.TeamID = Te.TeamID
    left outer join Departments De on Te.DepartmentID = De.DepartmentID
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and ((De.Description = 'Main Shop - Day') or (De.Description = 'Quick Service') or (De.Description = 'Aftermarket'))
    and Ap.Alignment = True
    and Ap.Status = 'Done') as Done); 
			             
  Insert Into [__Output] (AlignmentsAvailable, AlignmentsFinished) 
  values(@AlignmentsAvailable, @AlignmentsFinished);
  
  
   
   END;

CREATE PROCEDURE EditPreAuthorizationAmount2
   ( 
      AppointmentID CHAR ( 40 ),
      PreAuthorizationAmount CHAR ( 40 ),
      PreAuthorizationCreatedByID CHAR ( 40 ),
      PreAuthorizationLocation CHAR ( 40 ),
      PreAuthorizationNotes Memo,
      ReturnValue LOGICAL OUTPUT
   ) 
   BEGIN 
                                          
-- Drop procedure EditPreAuthorizationAmount2;
-- Execute Procedure EditPreAuthorizationAmount2('[8c9786a1-85f5-2f45-8085-3b01b842da7a]', '0', 'jschneider', 'Consultant', 'Second Note2');
  declare @Input Cursor as select * from __Input;
  declare @Now String;
  declare @Stmt String;
  declare @AppointmentID String;
  declare @PreAuthorizationCreatedByID String;
  declare @PreAuthorizationAmount String;  
  declare @PreAuthorizationLocation String;
  declare @PreAuthorizationNotes String;
  declare @ExistingCount Integer;
  declare @ExistingAmountNote Integer;

  open @Input;
  try
    fetch @Input;
    @AppointmentID = @Input.AppointmentID;
	@PreAuthorizationCreatedByID = @Input.PreAuthorizationCreatedByID;
	@PreAuthorizationAmount = @Input.PreAuthorizationAmount;
	@PreAuthorizationLocation = @Input.PreAuthorizationLocation;
	@PreAuthorizationNotes = @Input.PreAuthorizationNotes;
  finally
    close @Input;
  end;
  
  -- Get the Current Time/Date
  @Now = Convert(Now(), SQL_CHAR);
  
  -- Find out if null thru timestamp exists
  @ExistingCount = (Select count(*) from PreAuthorizationAmount where AppointmentID = @AppointmentID and ThruTS is Null);
  try
    if @ExistingCount > 0 then
	  --First find out if PreAuthorizationAmount is null
	  if @PreAuthorizationAmount = '' then
	    @PreAuthorizationAmount = '0';
	  endif;  
	  --Find out if it exists first with same note
	  @ExistingAmountNote = (Select Count(*) from PreAuthorizationAmount where AppointmentID = @AppointmentID 
	  and ((PreAuthorizationNotes = @PreAuthorizationNotes) or ((PreAuthorizationNotes = '') and (@PreAuthorizationNotes is Null))) and Convert(PreAuthorizationAmount, SQL_DOUBLE) = Convert(@PreAuthorizationAmount, SQL_DOUBLE)
	  and ThruTS is Null);
	  if @ExistingAmountNote = 0 then
	  Begin Transaction;
	    --First close out existing
        @Stmt = 'Update PreAuthorizationAmount set ThruTS = ''' + @Now + ''' where AppointmentID = ''' +
  	    @AppointmentID + ''' and ThruTS is Null';
		
        execute Immediate @Stmt;
		
		--Now insert new record
		--Make sure PreAuthorizationCreatedByID is not empty string
		if @PreAuthorizationCreatedByID = '' then
		
          @Stmt = 'Insert Into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, ' +  
	        'PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS) Values (''' + @AppointmentID + ''', Convert(' + @PreAuthorizationAmount +
	        ', SQL_DOUBLE), Null, ''' + @PreAuthorizationLocation + ''', ''' +  Coalesce(@PreAuthorizationNotes, '') + ''', ''' +
	        @Now + ''', Null)';
	    else
          @Stmt = 'Insert Into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, ' +  
	        'PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS) Values (''' + @AppointmentID + ''', Convert(' + @PreAuthorizationAmount +
	        ', SQL_DOUBLE), ''' + @PreAuthorizationCreatedByID + ''', ''' + @PreAuthorizationLocation + ''', ''' +  Coalesce(@PreAuthorizationNotes, '') + ''', ''' +
	        @Now + ''', Null)';		
		endif;
	    execute Immediate @Stmt;
	  Commit Work;
	  endif;  	
    else
      --Just treat as new
      if @PreAuthorizationAmount <> '' then
	    if @PreAuthorizationCreatedByID = '' then
          @Stmt = 'Insert Into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, ' +  
	        'PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS) Values (''' + @AppointmentID + ''', Convert(''' + @PreAuthorizationAmount +
	        ''', SQL_DOUBLE), Null, ''' + @PreAuthorizationLocation + ''', ''' + Coalesce(@PreAuthorizationNotes, '') + ''', ''' +
	        @Now + ''', Null)';
		else
          @Stmt = 'Insert Into PreAuthorizationAmount (AppointmentID, PreAuthorizationAmount, PreAuthorizationCreatedByID, ' +  
	        'PreAuthorizationLocation, PreAuthorizationNotes, FromTS, ThruTS) Values (''' + @AppointmentID + ''', Convert(''' + @PreAuthorizationAmount +
	        ''', SQL_DOUBLE), ''' + @PreAuthorizationCreatedByID + ''', ''' + @PreAuthorizationLocation + ''', ''' + Coalesce(@PreAuthorizationNotes, '') + ''', ''' +
	        @Now + ''', Null)';
		endif;
	    execute Immediate @Stmt;
	  endif;
    endif;
    insert into [__Output] Values (True);
  catch all
    insert into [__Output] Values (False);  
  end try;



   
   
   
   
   
   
   END;

CREATE PROCEDURE GetGlobalCapacity2
   ( 
      InDate DATE,
      DriveabilityHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceHours DOUBLE ( 15 ) OUTPUT,
      HeavyHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityDuration DOUBLE ( 15 ) OUTPUT,
      MaintenanceDuration DOUBLE ( 15 ) OUTPUT,
      HeavyDuration DOUBLE ( 15 ) OUTPUT,
      Goal Integer OUTPUT
   ) 
   BEGIN 
              -- Drop Procedure GetGlobalCapacity2;
  -- Execute Procedure GetGlobalCapacity2('6/21/2010');
  declare @Input Cursor as select * from __Input;  
  declare @InDate Date;
  
  declare @DHours Double;
  declare @MHours Double;
  declare @HHours Double;
  declare @DDuration Double;
  declare @MDuration Double;
  declare @HDuration Double;
  declare @SaturdayTeamWorking String; 
  
  declare @NMHours Double;
  declare @NDHours Double;
  declare @NMDuration Double;
  declare @NDDuration Double;
  declare @NFSaturday Logical;

  
  declare @QSMHours Double;

  declare @AMHours Double;
  declare @AFSaturday Logical;
  declare @Goal Integer;   
  
  open @Input;
  try
    fetch @Input;
    @InDate = @Input.InDate;
  finally
    close @Input;
  end;
  
  
  --Initialize
  @DHours = 0;
  @MHours = 0;
  @HHours = 0;
  @DDuration = 0;
  @MDuration = 0;
  @HDuration = 0;

  @NDHours = 0;
  @NMHours = 0;
  
  @QSMHours = 0;
  
  @AMHours = 0;  
  @Goal = 0;

  
  @SaturdayTeamWorking = '';

  if (DayOfWeek(@InDate) = 7) then
    @SaturdayTeamWorking = (Select Name from Weekends where Date = Convert(@InDate, SQL_DATE));
	if @SaturdayTeamWorking <> '' then
	  @HHours = (Select Count(*) from Technicians T
	         where TeamID = 
			 (Select TeamID from Teams where Name = @SaturdayTeamWorking and DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day'))
             and T.SkillLevel = 'H'
             and T.Active = True
			 and T.ShortName <> 'Duane R.') * 8 -
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'H'
			 and D.Description = 'Main Shop - Day');
			 
      @DHours = (Select Count(*) from Technicians T
	         where TeamID = 
			 (Select TeamID from Teams where Name = @SaturdayTeamWorking and DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day'))
             and T.SkillLevel = 'D'
             and T.Active = True) * 10.5 - 
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'D'
			 and D.Description = 'Main Shop - Day');
      @MHours = (Select Count(*) from Technicians T
	         where TeamID = 
			 (Select TeamID from Teams where Name = @SaturdayTeamWorking and DepartmentID = 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day'))
             and T.SkillLevel = 'M'
             and T.Active = True) * 10.5 - 
			 (Select Coalesce(Sum(E.Hours), 0) As Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'M'
			 and D.Description = 'Main Shop - Day');			 
	
	else
      @HHours = 0;
	  @MHours = 0;
	  @DHours = 0;
    endif;			 
			  			   
  else
    @HHours = (Select Count(*) from Technicians T
             left outer join Teams Tea on T.TeamID = Tea.TeamID
             left outer join Departments D on Tea.DepartmentID = D.DepartmentID
             where T.SkillLevel = 'H'
             and T.Active = True
             and D.Description = 'Main Shop - Day') * 10.5 -
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'H'
			 and D.Description = 'Main Shop - Day');
    @DHours = (select Count(*) from Technicians T
             left outer join Teams Tea on T.TeamID = Tea.TeamID
             left outer join Departments D on Tea.DepartmentID = D.DepartmentID
             where T.SkillLevel = 'D'
             and T.Active = True
             and D.Description = 'Main Shop - Day') * 10.5 - 
			 (Select Coalesce(Sum(E.Hours), 0) as Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'D'
			 and D.Description = 'Main Shop - Day');
    @MHours = (select Count(*) from Technicians T
             left outer join Teams Tea on T.TeamID = Tea.TeamID
             left outer join Departments D on Tea.DepartmentID = D.DepartmentID
             where T.SkillLevel = 'M'
             and T.Active = True
             and D.Description = 'Main Shop - Day') * 10.5 - 
			 (Select Coalesce(Sum(E.Hours), 0) As Hours from Exceptions E 
			 left outer join Technicians T on E.TechNumber = T.TechNumber
			 left outer join Teams Tea on T.TeamID = Tea.TeamID
			 left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			 where E.Date = Convert(@InDate, SQL_DATE)
			 and T.SkillLevel = 'M'
			 and D.Description = 'Main Shop - Day');				 			 
  endif;
		      
  @DDuration = (Select Coalesce(Sum(DriveabilityDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'Hold'
			   and Status <> 'No Show'
			   and Status <> 'Pend Pts'
			   and Status <> 'Pend Auth'
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID In 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Main Shop - Night')))); 
  
  @MDuration = (Select Coalesce(Sum(MaintenanceDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'Hold'
			   and Status <> 'No Show'
			   and Status <> 'Pend Pts'
			   and Status <> 'Pend Auth'			   
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID In 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Main Shop - Night'
			  or Description = 'Quick Service' or Description = 'Aftermarket')))); 
			 		 	 
  @HDuration = (Select Coalesce(Sum(HeavyDuration), 0) from Appointments
			   where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
			   and Status <> 'Carry Over'
			   and Status <> 'Hold'
			   and Status <> 'No Show'
			   and Status <> 'Pend Pts'
			   and Status <> 'Pend Auth'			   
			 and AdvisorID In (Select ShortName from Advisors where TeamID In (Select TeamID from Teams where DepartmentID In 
			 (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Main Shop - Night'))));
			 
			 
  if (DayOfWeek(@InDate) = 7) then
      @NFSaturday = True;  
      @NMHours = 0;
	  @NDHours = 0;
  else	 
    @NMHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.SkillLevel = 'M'			  
              and T.Active = True
              and D.Description = 'Main Shop - Night')
	    	  * 10.5 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsNight E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and T.SkillLevel = 'M'			  
			  and D.Description = 'Main Shop - Night');
    @NDHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.SkillLevel = 'D'			  
              and T.Active = True
              and D.Description = 'Main Shop - Night')
	    	  * 10.5 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsNight E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and T.SkillLevel = 'D'			  
			  and D.Description = 'Main Shop - Night');			  
  endif; 
				 

  if (DayOfWeek(@InDate) = 7) then
 
    @QSMHours = 8 - 
			   (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsAftermarket E 
			   left outer join Technicians T on E.TechNumber = T.TechNumber
			   left outer join Teams Tea on T.TeamID = Tea.TeamID
			   left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			   where E.Date = Convert(@InDate, SQL_DATE)
			   and D.Description = 'Quick Service');
  else	 
    @QSMHours = (select Count(*) from Technicians T
               left outer join Teams Tea on T.TeamID = Tea.TeamID
               left outer join Departments D on Tea.DepartmentID = D.DepartmentID
               where T.Active = True
               and D.Description = 'Quick Service')
	    	   * 10.5 - 
			   (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsQuickService E 
		 	   left outer join Technicians T on E.TechNumber = T.TechNumber
			   left outer join Teams Tea on T.TeamID = Tea.TeamID
			   left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			   where E.Date = Convert(@InDate, SQL_DATE)
			   and D.Description = 'Quick Service');
  endif;



  if (DayOfWeek(@InDate) = 7) then
    if (Select Count(*) from WeekendsAftermarket where Date = Convert(@InDate, SQL_DATE)) > 0 then
      @AFSaturday = True;  
      @AMHours = 8 - 
			   (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsAftermarket E 
			   left outer join Technicians T on E.TechNumber = T.TechNumber
			   left outer join Teams Tea on T.TeamID = Tea.TeamID
			   left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			   where E.Date = Convert(@InDate, SQL_DATE)
			   and T.SkillLevel = 'R'
			   and D.Description = 'Aftermarket');
	else
	  @AMHours = 0;
	endif;
  else	 
    @AMHours = (select Count(*) from Technicians T
              left outer join Teams Tea on T.TeamID = Tea.TeamID
              left outer join Departments D on Tea.DepartmentID = D.DepartmentID
              where T.SkillLevel = 'R'
              and T.Active = True
              and D.Description = 'Aftermarket')
	    	  * 10.5 - 
			  (Select Coalesce(Sum(E.Hours), 0) As Hours from ExceptionsAftermarket E 
		 	  left outer join Technicians T on E.TechNumber = T.TechNumber
			  left outer join Teams Tea on T.TeamID = Tea.TeamID
			  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
			  where E.Date = Convert(@InDate, SQL_DATE)
			  and T.SkillLevel = 'R'
			  and D.Description = 'Aftermarket');
  endif;

  @MHours = @MHours + @NMHours + @QSMHours + @AMHours;
  @DHours = @DHours + @NDHours;

  @HHours = @HHours * (Select Heavy from CorrectionFactors);			 			 
  @DHours = @DHours * (Select Driveability from CorrectionFactors);			 
  @MHours = @MHours * (Select Maintenance from CorrectionFactors);
  @Goal = (Select Goal from Goals where GoalDate = Convert(@InDate, SQL_DATE));
  if @Goal is Null then 
    @Goal = 0;
  endif;
  
		             
  Insert Into [__Output] (DriveabilityHours, MaintenanceHours, HeavyHours, DriveabilityDuration, MaintenanceDuration, HeavyDuration, Goal) 
  values(@DHours, @MHours, @HHours, @DDuration, @MDuration, @HDuration, @Goal);

   
   
   
   
   
   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetBevReport
   ( 
      InDate DATE,
      Department CHAR ( 40 ),
      TotalItems Integer OUTPUT,
      Appointments Integer OUTPUT,
      HeavyAppHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityAppHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceAppHours DOUBLE ( 15 ) OUTPUT,
      TotalAppHours DOUBLE ( 15 ) OUTPUT,
      CarryOvers Integer OUTPUT,
      HeavyCOHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityCOHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceCOHours DOUBLE ( 15 ) OUTPUT,
      TotalCOHours DOUBLE ( 15 ) OUTPUT,
      Done Integer OUTPUT,
      HeavyDoneHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityDoneHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceDoneHours DOUBLE ( 15 ) OUTPUT,
      TotalDoneHours DOUBLE ( 15 ) OUTPUT,
      PendingAuth Integer OUTPUT,
      HeavyAuthHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityAuthHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceAuthHours DOUBLE ( 15 ) OUTPUT,
      TotalAuthHours DOUBLE ( 15 ) OUTPUT,
      PendPts Integer OUTPUT,
      HeavyPendPtsHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityPendPtsHours DOUBLE ( 15 ) OUTPUT,
      MaintenancePendPtsHours DOUBLE ( 15 ) OUTPUT,
      TotalPendPtsHours DOUBLE ( 15 ) OUTPUT,
      Kitted Integer OUTPUT,
      HeavyKittedHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityKittedHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceKittedHours DOUBLE ( 15 ) OUTPUT,
      TotalKittedHours DOUBLE ( 15 ) OUTPUT,
      Blueprint Integer OUTPUT,
      HeavyBlueprintHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityBlueprintHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceBlueprintHours DOUBLE ( 15 ) OUTPUT,
      TotalBlueprintHours DOUBLE ( 15 ) OUTPUT,
      NoShow Integer OUTPUT,
      HeavyNoShowHours DOUBLE ( 15 ) OUTPUT,
      DriveabilityNoShowHours DOUBLE ( 15 ) OUTPUT,
      MaintenanceNoShowHours DOUBLE ( 15 ) OUTPUT,
      TotalNoShowHours DOUBLE ( 15 ) OUTPUT,
      PreAuth Integer OUTPUT,
      AvgPreAuth DOUBLE ( 15 ) OUTPUT,
      PreAuthPct DOUBLE ( 15 ) OUTPUT,
      TotalHours DOUBLE ( 15 ) OUTPUT
   ) 
   BEGIN 
                                            -- Drop Procedure GetBevReport;
  -- Execute Procedure GetBevReport('6/28/2010', 'Main Shop - Day, Quick Service');
  declare @Input Cursor as select * from __Input;  
  declare @InDate Date;
  declare @Department String;
  declare @TotalItems Integer;
  declare @Appointments Integer;
  declare @HeavyAppHours Double;    
  declare @DriveabilityAppHours Double;
  declare @MaintenanceAppHours Double;
  declare @TotalAppHours Double;
  declare @CarryOvers Integer;
  declare @HeavyCOHours Double;
  declare @DriveabilityCOHours Double;
  declare @MaintenanceCOHours Double;
  declare @TotalCOHours Double;
  declare @Done Integer;
  declare @HeavyDoneHours Double;
  declare @DriveabilityDoneHours Double;
  declare @MaintenanceDoneHours Double;
  declare @TotalDoneHours Double; 
  declare @PendingAuth Integer;
  declare @HeavyAuthHours Double;
  declare @DriveabilityAuthHours Double;
  declare @MaintenanceAuthHours Double;
  declare @TotalAuthHours Double;
  declare @PendPts Integer;
  declare @HeavyPendPtsHours Double;
  declare @DriveabilityPendPtsHours Double;
  declare @MaintenancePendPtsHours Double;
  declare @TotalPendPtsHours Double;
  declare @Kitted Integer;
  declare @HeavyKittedHours Double;
  declare @DriveabilityKittedHours Double;
  declare @MaintenanceKittedHours Double;
  declare @TotalKittedHours Double;
  declare @Blueprint Integer;
  declare @HeavyBlueprintHours Double;
  declare @DriveabilityBlueprintHours Double;
  declare @MaintenanceBlueprintHours Double;
  declare @TotalBlueprintHours Double;
  declare @NoShow Integer;
  declare @HeavyNoShowHours Double;
  declare @DriveabilityNoShowHours Double;
  declare @MaintenanceNoShowHours Double;
  declare @TotalNoShowHours Double;
  declare @PreAuth Integer;
  declare @AvgPreAuth Double;
  declare @PreAuthCt Integer;
  declare @PreAuthPct Double;
  declare @TotalHours Double; 

  open @Input;
  try
    fetch @Input;
    @InDate = @Input.InDate;
	@Department = @Input.Department;
  finally
    close @Input;
  end;
  --Initialize

if @Department = 'Main Shop - Day, Quick Service' then
  
  @TotalItems = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));  

  @Appointments = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @HeavyAppHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @DriveabilityAppHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @MaintenanceAppHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @TotalAppHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @CarryOvers = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @HeavyCOHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));	
	
  @DriveabilityCOHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));		
	
  @MaintenanceCOHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @TotalCOHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @Done = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @HeavyDoneHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @DriveabilityDoneHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @MaintenanceDoneHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));			
	
  @TotalDoneHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));	
  
  @PendingAuth = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @HeavyAuthHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @DriveabilityAuthHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @MaintenanceAuthHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @TotalAuthHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @PendPts = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service')))); 
  
  @HeavyPendPtsHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @DriveabilityPendPtsHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @MaintenancePendPtsHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @TotalPendPtsHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service')))); 	

  @Kitted = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @HeavyKittedHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @DriveabilityKittedHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @MaintenanceKittedHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @TotalKittedHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
	
  @Blueprint = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @HeavyBlueprintHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @DriveabilityBlueprintHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @MaintenanceBlueprintHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @TotalBlueprintHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @NoShow = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @HeavyNoShowHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @DriveabilityNoShowHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @MaintenanceNoShowHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @TotalNoShowHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))));
  
  @PreAuth = (Select Count(*) from PreAuthorizationAmount
    where AppointmentID in
    (Select AppointmentID from Appointments 
    where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))))
	and PreAuthorizationAmount > 0
    and ThruTS is Null);
  
  @AvgPreAuth = (Select Coalesce(Avg(PreAuthorizationAmount), 0) from PreAuthorizationAmount
    where AppointmentID in
    (Select AppointmentID from Appointments 
    where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service'))))
    and ThruTS is Null);
  
  @PreAuthCt = (Select Count(*) from Appointments
    where AppointmentID in
    (Select AppointmentID from Appointments 
    where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Status in ('Carry Over', 'Done', 'Pend Auth', 'Pend Pts', 'Kitted', 'Blueprint')
    and AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in
    (Select DepartmentID from Departments where Description = 'Main Shop - Day' or Description = 'Quick Service')))));
	
else
  @TotalItems = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));  

  @Appointments = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @HeavyAppHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @DriveabilityAppHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @MaintenanceAppHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @TotalAppHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Appointment'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @CarryOvers = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @HeavyCOHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));	
	
  @DriveabilityCOHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));		
	
  @MaintenanceCOHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @TotalCOHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Carry Over'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @Done = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @HeavyDoneHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @DriveabilityDoneHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @MaintenanceDoneHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));			
	
  @TotalDoneHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Done'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));	
  
  @PendingAuth = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @HeavyAuthHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @DriveabilityAuthHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @MaintenanceAuthHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @TotalAuthHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Auth'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @PendPts = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department)))); 
  
  @HeavyPendPtsHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @DriveabilityPendPtsHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @MaintenancePendPtsHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @TotalPendPtsHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Pend Pts'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department)))); 	

  @Kitted = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @HeavyKittedHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @DriveabilityKittedHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @MaintenanceKittedHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @TotalKittedHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Kitted'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
	
  @Blueprint = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @HeavyBlueprintHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @DriveabilityBlueprintHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @MaintenanceBlueprintHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @TotalBlueprintHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'Blueprint'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @NoShow = (Select Count(*) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @HeavyNoShowHours = (Select Coalesce(Sum(HeavyDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @DriveabilityNoShowHours = (Select Coalesce(Sum(DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @MaintenanceNoShowHours = (Select Coalesce(Sum(MaintenanceDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in 
    (Select DepartmentID from Departments where Description = @Department))));
  
  @TotalNoShowHours = (Select Coalesce(Sum(HeavyDuration + MaintenanceDuration + DriveabilityDuration), 0) from appointments Ap
    where Convert(Ap.StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Ap.Status = 'No Show'
    and Ap.AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in
    (Select DepartmentID from Departments where Description = @Department))));
  
  @PreAuth = (Select Count(*) from PreAuthorizationAmount
    where AppointmentID in
    (Select AppointmentID from Appointments 
    where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in
    (Select DepartmentID from Departments where Description = @Department))))
	and PreAuthorizationAmount > 0
    and ThruTS is Null);
  
  @AvgPreAuth = (Select Coalesce(Avg(PreAuthorizationAmount), 0) from PreAuthorizationAmount
    where AppointmentID in
    (Select AppointmentID from Appointments 
    where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in
    (Select DepartmentID from Departments where Description = @Department))))
    and ThruTS is Null);
  
  @PreAuthCt = (Select Count(*) from Appointments
    where AppointmentID in
    (Select AppointmentID from Appointments 
    where Convert(StartTime, SQL_DATE) = Convert(@InDate, SQL_DATE)
    and Status in ('Carry Over', 'Done', 'Pend Auth', 'Pend Pts', 'Kitted', 'Blueprint')
    and AdvisorID in (Select ShortName from Advisors where TeamID in 
    (Select TeamID from Teams where DepartmentID in
    (Select DepartmentID from Departments where Description = @Department)))));

endif;	
  
  if @PreAuthCt > 0 then
    @PreAuthPct = ROUND((@PreAuth*1.00/@PreAuthCt)*100, 0);
	--@PreAuthPct = @PreAuthCt;
  else
    @PreAuthPct = 0;
  endif;
  
  @TotalHours = @TotalBlueprintHours + @TotalKittedHours + @TotalPendPtsHours + @TotalAuthHours 
    + @TotalDoneHours + @TotalCOHours;
		             
  Insert Into [__Output] (TotalItems, Appointments, HeavyAppHours, DriveabilityAppHours, 
    MaintenanceAppHours, TotalAppHours, CarryOvers, HeavyCOHours, DriveabilityCOHours, 
	MaintenanceCOHours, TotalCOHours, Done, HeavyDoneHours, DriveabilityDoneHours, 
	MaintenanceDoneHours, TotalDoneHours, PendingAuth, HeavyAuthHours, DriveabilityAuthHours,
	MaintenanceAuthHours, TotalAuthHours, PendPts, HeavyPendPtsHours, DriveabilityPendPtsHours,
	MaintenancePendPtsHours, TotalPendPtsHours, Kitted, HeavyKittedHours, DriveabilityKittedHours,
	MaintenanceKittedHours, TotalKittedHours, Blueprint, HeavyBlueprintHours, DriveabilityBlueprintHours,
	MaintenanceBlueprintHours, TotalBlueprintHours, NoShow, HeavyNoShowHours, DriveabilityNoShowHours,
	MaintenanceNoShowHours, TotalNoShowHours, PreAuth, AvgPreAuth, PreAuthPct, TotalHours) 
  values(@TotalItems, @Appointments, @HeavyAppHours, @DriveabilityAppHours, 
    @MaintenanceAppHours, @TotalAppHours, @CarryOvers, @HeavyCOHours, @DriveabilityCOHours,
	@MaintenanceCOHours, @TotalCOHours, @Done, @HeavyDoneHours, @DriveabilityDoneHours,
	@MaintenanceDoneHours, @TotalDoneHours, @PendingAuth, @HeavyAuthHours, @DriveabilityAuthHours,
	@MaintenanceAuthHours, @TotalAuthHours, @PendPts, @HeavyPendPtsHours, @DriveabilityPendPtsHours,
	@MaintenancePendPtsHours, @TotalPendPtsHours, @Kitted, @HeavyKittedHours, @DriveabilityKittedHours,
	@MaintenanceKittedHours, @TotalKittedHours, @Blueprint, @HeavyBlueprintHours, @DriveabilityBlueprintHours,
	@MaintenanceBlueprintHours, @TotalBlueprintHours, @NoShow, @HeavyNoShowHours, @DriveabilityNoShowHours,
	@MaintenanceNoShowHours, @TotalNoShowHours, @PreAuth, @AvgPreAuth, @PreAuthPct, @TotalHours);

   
   
   
   
   
   
   
   
   END;

CREATE PROCEDURE GetPAInfo
   ( 
      InDate DATE,
      PAPercentage DOUBLE ( 15 ) OUTPUT,
      PAAverage DOUBLE ( 15 ) OUTPUT
   ) 
   BEGIN 
                                -- Drop Procedure GetPAInfo;
  -- Execute Procedure GetPAInfo('6/15/2011');
  declare @Input Cursor as select * from __Input;  
  declare @InDate Date;
  
  declare @PAPercentage Double;
  declare @PAAverage Double;
  declare @ROCount Integer;
  declare @PAGreaterThanZero Integer;
  
  open @Input;
  try
    fetch @Input;
    @InDate = @Input.InDate;
  finally
    close @Input;
  end;
  
  
  --Initialize
  @PAPercentage = 0;
  @PAAverage = 0;
  @ROCount = 0;
  @PAGreaterThanZero = 0;
  
  @ROCount = (Select Count(*) from Appointments A where Convert(A.StartTime, SQL_DATE) = @InDate);
  @PAGreaterThanZero = (Select Count(*) from Appointments A
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID
  where Convert(StartTime, SQL_DATE) = @InDate 
  and PA.PreAuthorizationAmount > 0
  and PA.ThruTS is null);
  
  
  if @ROCount > 0 then
    
    @PAPercentage = Round((@PAGreaterThanZero*1.0)/(@ROCount*1.0) * 100, 0);
	
    @PAAverage = (Select Avg(PA.PreAuthorizationAmount) from Appointments A
    left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID
    where Convert(StartTime, SQL_DATE) = @InDate 
    and PA.PreAuthorizationAmount > 0
    and PA.ThruTS is null);	
  end if;
   
		             
  Insert Into [__Output] (PAPercentage, PAAverage) 
  values(@PAPercentage, @PAAverage);
    
   
   END;

CREATE PROCEDURE WriteToAnomalies
   ( 
      Category CHAR ( 60 ),
      CodeSource Memo,
      Description Memo
   ) 
   BEGIN 
       
      
/*
DROP PROCEDURE WriteToAnomalies;

*/
INSERT INTO Anomalies VALUES ( (SELECT Current_Timestamp() FROM system.iota),
                               (SELECT Coalesce(Category, '') FROM __INPUT),
							   (SELECT CodeSource FROM __INPUT),
							   (SELECT Description FROM __INPUT));

   

   END;

CREATE PROCEDURE SendEmailNotification
   ( 
      PartyID CHAR ( 127 ),
      PartyIDList Memo,
      EmailAddresses Memo,
      Subject CICHAR ( 200 ),
      MessageBody Memo,
      Attachment BLOB,
      FromEMailAddress CICHAR ( 127 )
   ) 
   BEGIN 
                        /*
      PartyID CHAR ( 127 ),           //A partyID, AND optional ContactMechanism
                                      //IF ContactMechanism IS omitted, an email will be send to the
                                      //ContactMechanism_WorkEmail description,
                                      //WHEN ContactMechanism IS included, separate PartyID AND
                                      //ContactMechanism with a semicolon: 52397361-c9d6-3e47-91a9-7fbe64aa2eff;ContactMechanism_TextEmail

      PartyIDList Memo,               //A colon separated list of PartyID;ContactMechanism entries.
                                      //ContactMechanism IS optional.
                                      //Can include a single partyID AND optional ContactMechanism
                                      
      EmailAddresses Memo,            //A colon separated list of email addresses.
                                      //Can include just one email address

      Subject CICHAR ( 200 ),         //the subject line       
      MessageBody Memo,               //The message body
      
      Attachment BLOB,                //reserved for future use
      
      FromEMailAddress CICHAR ( 127 ) //NOT sure IF this IS used anywhere


EXECUTE PROCEDURE SendEmailNotification(NULL, NULL, 'cary@cartiva.com', 'Subject', 'Message Body', NULL, null);      

EXECUTE PROCEDURE SendEmailNotification(NULL, '48bc88ff-5687-3340-999d-47ddf2d7e037;ContactMechanism_TextEmail', null, 'Service test message 1', 'Hi, Jon', NULL, 'TheTool@Cartiva.com');      

*/ 
DECLARE @Count Integer;

  @Count = 0;
  IF (SELECT PartyID FROM __INPUT) IS NOT NULL 
    THEN @Count = @Count + 1;
  END;
  IF (SELECT PartyIDList FROM __INPUT) IS NOT NULL 
    THEN @Count = @Count + 1;
  END;
  IF (SELECT EmailAddresses FROM __INPUT) IS NOT NULL 
    THEN @Count = @Count + 1;
  END;
  
  IF @Count = 0 THEN
    Raise InvalidParameters(2740, 'No email parameters provided. Cannot send');
  END;
  
  IF @Count > 1 THEN
    Raise InvalidParameters(2740, 'Only 1 email parameter expected. Cannot send');
  END;

  INSERT INTO EMailNotifications (PartyID, PartyIDList, EmailAddresses, Subject, MessageBody, Attachment, FromEMailAddress, CreationTS)
  SELECT
    (SELECT PartyID FROM __INPUT),
    (SELECT PartyIDList FROM __INPUT),
    (SELECT EmailAddresses FROM __INPUT),
    (SELECT Subject FROM __INPUT),
    (SELECT MessageBody FROM __INPUT),
    (SELECT Attachment FROM __INPUT),
    (SELECT FromEMailAddress FROM __INPUT),
	current_timestamp()
    FROM system.iota;
 
   
   
   END;

CREATE PROCEDURE InsertIntoTest
   ( 
      SomeText CICHAR ( 50 )
   ) 
   BEGIN 
      /*
  execute procedure InsertIntoTest('some tex');
*/
insert into test select Sometext from __input;

   END;

CREATE PROCEDURE RydellServiceAppointmentUpdated
   ( 
      AppointmentID CHAR ( 38 ),
      Status CHAR ( 25 ),
      TechStatus CHAR ( 25 )
   ) 
   BEGIN 
      /*
EXECUTE procedure RydellServiceAppointmentUpdated('[feb77558-3927-2146-be57-d3662b705132]');
*/
DECLARE @Status String;
DECLARE @TechStatus String;
DECLARE @AppointmentID String;
DECLARE @NotificationScheduled Logical;
DECLARE @Cur CURSOR AS SELECT * FROM Appointments WHERE AppointmentID = @AppointmentID;
DECLARE @Message String;
declare @interestedPartyID string;    


  @AppointmentID = (SELECT AppointmentID FROM __input);
  @NotificationScheduled = (SELECT CASE
                                    WHEN (SELECT 1 
                                            FROM ServiceAppointmentsOfInterest 
                                            WHERE AppointmentID = @AppointmentID 
                                            AND ThruTS IS null) IS NULL THEN False ELSE True END
							FROM system.iota);
						
  IF @NotificationScheduled = True THEN    							
    @Status = (SELECT Status FROM __input);  
    IF @Status = 'Done' THEN
      //REMOVE 
	  execute procedure RemoveAppointmentFromServiceAppointmentsOfInterest(@AppointmentID, 'AppointmentOfInterest_Missed', Current_Timestamp());
	  return;
    ELSEIF  @Status = 'No Show' THEN
      //Remove the appointment FROM
	  execute procedure RemoveAppointmentFromServiceAppointmentsOfInterest(@AppointmentID, 'AppointmentOfInterested_NoShow', Current_Timestamp());
	  return;
	END;
	
	@TechStatus = (SELECT TechStatus FROM __input);
    if @TechStatus = 'Pre Diagnosis' OR @TechStatus = 'Working' THEN
//      @interestedPartyID = 'cjensen@jensendatasystems.com:cary@cartiva.com';
//Jon, RJ, AND Cary	  
	  @interestedPartyID = '48bc88ff-5687-3340-999d-47ddf2d7e037;ContactMechanism_TextEmail:52397361-c9d6-3e47-91a9-7fbe64aa2eff;ContactMechanism_TextEmail:zzzz;ContactMechanism_TextEmail';    
//Jon AND Cary
	  //@interestedPartyID = '52397361-c9d6-3e47-91a9-7fbe64aa2eff;ContactMechanism_TextEmail:zzzz;ContactMechanism_TextEmail';    
      OPEN @Cur;
      TRY
        IF FETCH @Cur THEN
          @Message = Coalesce(CAST(@Cur.ModelYear AS SQL_CHAR(4)), '') + 
                          ' ' + 
                          Coalesce(Trim(@Cur.Make),'') + 
                          ' ' + 
                          Coalesce(Trim(@Cur.Model),'') + 
                          ' : ' + 
                          Trim(Coalesce(CAST(@Cur.Mileage AS SQL_CHAR),'')) + ' Tech: ' + 
                          Coalesce(Trim(@Cur.TechnicianAssignedID),'') + ', Name: ' + 
                          Coalesce(Trim(@Cur.Customer),'') + ' Sent: ' + CAST(Current_TimeStamp() AS SQL_CHAR); 
          EXECUTE PROCEDURE SendEmailNotification(null, @interestedPartyID, null, 'Service notification', @Message, NULL, 'TheTool@Cartiva.com');
          EXECUTE PROCEDURE RemoveAppointmentFromServiceAppointmentsOfInterest(CAST(@AppointmentID AS SQL_CHAR(38)), 'AppointmentOfInterest_Notified', Current_Timestamp());
        ELSE
          EXECUTE PROCEDURE WriteToAnomalies('AppointmentNotification', 'stored proc RydellServiceAppointmentStatusUpdated', 'Cannot find appointment record IN Appointments table');
        END;
	  CATCH ALL
        EXECUTE PROCEDURE WriteToAnomalies('AppointmentNotification', 'failure to call SendEMailNotification and/or RemoveAppointmentsFrom...', 'error text : ' + __errtext);	  
      FINALLY
        CLOSE @Cur;
      END;
	  RETURN;
    END; //	
  END;	

   END;

CREATE PROCEDURE RemoveAppointmentFromServiceAppointmentsOfInterest
   ( 
      AppointmentID CHAR ( 38 ),
      Disposition CHAR ( 60 ),
      RemoveTime TIMESTAMP
   ) 
   BEGIN 
                  /*
EXECUTE PROCEDURE RemoveAppointmentFromServiceAppointmentsOfInterest('[444facbe-75cb-5d4b-a8a8-e782c075072b]', 'AppointmentOfInterest_Canceled', Current_Timestamp());
*/

UPDATE ServiceAppointmentsOfInterest
  SET [AppointmentDispositionStatus] = (SELECT Disposition FROM __INPUT),
      [THRUTS] = (SELECT RemoveTime FROM __INPUT)
  WHERE [AppointmentID] = (SELECT AppointmentID FROM __INPUT)
    AND [ThruTS] IS null;
  

   
   
   END;

CREATE PROCEDURE zzz
   ( 
   ) 
   BEGIN 
      EXECUTE PROCEDURE sp_SignalEvent('EMailNotification', False, 0);

   END;

CREATE PROCEDURE IssueEMailNotificationTerminateEvent
   ( 
   ) 
   BEGIN 
                    EXECUTE PROCEDURE sp_SignalEvent('EMailNotificationTerminateEvent', False, 0);  

   
   
   END;

CREATE PROCEDURE GetAvailableAftermarketJobsByDate2
   ( 
      DisplayedDate DATE,
      Team CHAR ( 40 ) OUTPUT,
      Customer CHAR ( 40 ) OUTPUT,
      AdvisorID CHAR ( 40 ) OUTPUT,
      PromiseTime TIMESTAMP OUTPUT,
      AdvisorStatus CHAR ( 25 ) OUTPUT,
      TechStatus CHAR ( 25 ) OUTPUT,
      PartsStatus CHAR ( 25 ) OUTPUT,
      Tag# CHAR ( 10 ) OUTPUT,
      RONumber CHAR ( 10 ) OUTPUT,
      Year Integer OUTPUT,
      Make CHAR ( 25 ) OUTPUT,
      Model CHAR ( 25 ) OUTPUT,
      TechWrk CHAR ( 40 ) OUTPUT,
      ReleaseTime TIMESTAMP OUTPUT,
      Notes Memo OUTPUT,
      AppointmentID CHAR ( 40 ) OUTPUT,
      TechComments Memo OUTPUT,
      Requisite CHAR ( 14 ) OUTPUT,
      ArrivalTime TIMESTAMP OUTPUT,
      M CHAR ( 1 ) OUTPUT,
      D CHAR ( 1 ) OUTPUT,
      H CHAR ( 1 ) OUTPUT,
      PreAuthorizationAmount DOUBLE ( 15 ) OUTPUT,
      PreAuthorizationNotes Memo OUTPUT
   ) 
   BEGIN 
                                                            
-- Drop Procedure GetAvailableAftermarketJobsByDate;       
-- execute procedure GetAvailableAftermarketJobsByDate('3/5/2009');      

  declare @Input Cursor as select * from __Input;
  declare @DisplayedDate Date;

  open @Input;
  try
    fetch @Input;
    @DisplayedDate = @Input.DisplayedDate;
  finally
    close @Input;
  end;
   
  SELECT Utilities.DropTablesIfExist('#TechJobsAll') FROM system.iota; 
   
--SELECT Utilities.DropTablesIfExist('#TechJobsHeavy') FROM system.iota; 
   

  Select
  Case
   When ((A.TechnicianID = '') or (A.TechnicianID is Null)) and ((A.TeamID is Null) or (A.TeamID = '')) then ''
   When (A.TechnicianID = 'Jared D.') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   When (A.TechnicianID = 'Doug B') and ((A.TeamID is Null) or (A.TeamID = '')) then '1'
   //When (A.TechnicianID = 'Kevin R.') and (A.TeamID is Null) then '4'
   Else Coalesce(A.TeamID, Tea.Name) 
  End As Team,
  A.Customer, A.AdvisorID, A.PromiseTime, A.Status as AdvisorStatus, A.TechStatus, A.PartsStatus,
  A.TagNumber as Tag#, A.RONumber, A.ModelYear as Year, A.Make, A.Model, A.TechnicianAssignedID as TechWrk,
  A.TOCReleaseTIme as ReleaseTime, A.Notes,
  A.AppointmentID, A.TechComments, A.Requisite,
  A.ArrivalTime, 
  Case When A.MaintenanceDuration = 0 then '' else 'X' end as "M",  
  Case When A.DriveabilityDuration = 0 then '' else 'X' end as "D",
  Case When A.HeavyDuration = 0 then '' else 'X' end as "H", TOCBufferSize,
  PA.PreAuthorizationAmount,
  PA.PreAuthorizationNotes
  into #TechJobsAll 
  from [Appointments] A
  left outer join Advisors Adv on A.AdvisorID = Adv.ShortName
  left outer join Teams Tea on Adv.TeamID = Tea.TeamID
  left outer join Departments D on Tea.DepartmentID = D.DepartmentID
  left outer join PreAuthorizationAmount PA on A.AppointmentID = PA.AppointmentID     
  where Convert(A.StartTime, SQL_DATE) = @DisplayedDate   
  and (A.Status <> 'Appointment' and A.Status <> 'No Show' and A.Status <> 'Day Off')  
  //Exclude All Day Jobs
  and D.Description = 'Aftermarket'
  //Exclude other stuff
  and Upper(A.Customer) <> 'DAY OFF' and Upper(A.Customer) <> 'BUFFER' and A.Customer <> ''
  and Upper(A.Customer) <> 'NOT HERE YET'
  and PA.ThruTS is Null
  --and Tea.Name <> ''
  order by
  Team,
  M Desc,
  D Desc,
  Convert(A.PromiseTime, SQL_TIME);

  insert into [__OUTPUT] Select Team, Customer, AdvisorID, PromiseTime, AdvisorStatus, TechStatus, PartsStatus,
  Tag#, RONumber, Year, Make, Model, TechWrk, ReleaseTime, Notes, AppointmentID, TechComments, 
  Requisite, ArrivalTime, M, D, H, PreAuthorizationAmount, PreAuthorizationNotes from [#TechJobsAll]; 

   
   
   
   
   END;

EXECUTE PROCEDURE 
  sp_CreateUser ( 
     'ServiceScheduler',
     ''/* YOUR PASSWORD GOES HERE */,
     '' );
EXECUTE PROCEDURE 
  sp_ModifyUserProperty ( 
     'ServiceScheduler', 
     'ENABLE_INTERNET', 
     'TRUE' );
GRANT Inherit ON  [Users]  TO  [ServiceScheduler];
GRANT Inherit ON  [Customers]  TO  [ServiceScheduler];
GRANT Inherit ON  [Rights]  TO  [ServiceScheduler];
GRANT Inherit ON  [Teams]  TO  [ServiceScheduler];
GRANT Inherit ON  [Advisors]  TO  [ServiceScheduler];
GRANT Inherit ON  [Blocks]  TO  [ServiceScheduler];
GRANT Inherit ON  [Technicians]  TO  [ServiceScheduler];
GRANT Inherit ON  [TechnicianByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [CommitmentsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [CapturedInfo]  TO  [ServiceScheduler];
GRANT Inherit ON  [ChangeLog]  TO  [ServiceScheduler];
GRANT Inherit ON  [Appointments]  TO  [ServiceScheduler];
GRANT Inherit ON  [Exceptions]  TO  [ServiceScheduler];
GRANT Inherit ON  [Alerts]  TO  [ServiceScheduler];
GRANT Inherit ON  [Categories]  TO  [ServiceScheduler];
GRANT Inherit ON  [CategoryStatuses]  TO  [ServiceScheduler];
GRANT Inherit ON  [AppointmentStatuses]  TO  [ServiceScheduler];
GRANT Inherit ON  [AdvisorBlocks]  TO  [ServiceScheduler];
GRANT Inherit ON  [AppointmentFieldsToCheck]  TO  [ServiceScheduler];
GRANT Inherit ON  [Departments]  TO  [ServiceScheduler];
GRANT Inherit ON  [AppointmentsArchived]  TO  [ServiceScheduler];
GRANT Inherit ON  [VehicleStatuses]  TO  [ServiceScheduler];
GRANT Inherit ON  [CorrectionFactors]  TO  [ServiceScheduler];
GRANT Inherit ON  [HoursAvailableForTheDay]  TO  [ServiceScheduler];
GRANT Inherit ON  [AppointmentsBackup]  TO  [ServiceScheduler];
GRANT Inherit ON  [ChangeLogArchive]  TO  [ServiceScheduler];
GRANT Inherit ON  [ChangeLogBackup]  TO  [ServiceScheduler];
GRANT Inherit ON  [AppointmentStatusesArchive]  TO  [ServiceScheduler];
GRANT Inherit ON  [AppointmentStatusesBackup]  TO  [ServiceScheduler];
GRANT Inherit ON  [InitialGuess]  TO  [ServiceScheduler];
GRANT Inherit ON  [Weekends]  TO  [ServiceScheduler];
GRANT Inherit ON  [WeekendsAftermarket]  TO  [ServiceScheduler];
GRANT Inherit ON  [ExceptionsAftermarket]  TO  [ServiceScheduler];
GRANT Inherit ON  [ExceptionsNight]  TO  [ServiceScheduler];
GRANT Inherit ON  [ExceptionsQuickService]  TO  [ServiceScheduler];
GRANT Inherit ON  [Notes]  TO  [ServiceScheduler];
GRANT Inherit ON  [PreAuthorizationAmount]  TO  [ServiceScheduler];
GRANT Inherit ON  [Statuses]  TO  [ServiceScheduler];
GRANT Inherit ON  [Goals]  TO  [ServiceScheduler];
GRANT Inherit ON  [AssignedJobs]  TO  [ServiceScheduler];
GRANT Inherit ON  [TechniciansAvailable]  TO  [ServiceScheduler];
GRANT Inherit ON  [RepairOrders]  TO  [ServiceScheduler];
GRANT Inherit ON  [__ads]  TO  [ServiceScheduler];
GRANT Inherit ON  [EMailNotifications]  TO  [ServiceScheduler];
GRANT Inherit ON  [ServiceAppointmentsOfInterest]  TO  [ServiceScheduler];
GRANT Inherit ON  [text]  TO  [ServiceScheduler];
GRANT Inherit ON  [test]  TO  [ServiceScheduler];
GRANT Inherit ON  [Anomalies]  TO  [ServiceScheduler];
GRANT Inherit ON  [WeekendsSunday]  TO  [ServiceScheduler];
GRANT Inherit ON  [PreAuthorizationAmountArchive]  TO  [ServiceScheduler];
GRANT Inherit ON  [StatusesArchive]  TO  [ServiceScheduler];
GRANT Inherit ON  [DeleteCapturedInfo]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAppointmentsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [ChangeStatus]  TO  [ServiceScheduler];
GRANT Inherit ON  [CreateAppointmentTimeStamp]  TO  [ServiceScheduler];
GRANT Inherit ON  [SetStatus]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetTechJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAvailableQuickServiceJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetFinishedTechDayJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetCarryOverTechDayJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [EditAppointment]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAppointmentTechDayJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAppointmentAftermarketJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [CreateAppointment]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAvailableTechNightJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAvailableTechDayJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAvailableAftermarketJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAppointmentQuickServiceJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetMainShopDayCapacity]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAppointmentTechNightJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAftermarketCapacity]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetNightCapacity]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetQuickServiceCapacity]  TO  [ServiceScheduler];
GRANT Inherit ON  [CreateAppointmentFromPaste]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAppointmentTechDayJobsByDate2]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAppointmentTechNightJobsByDate2]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAppointmentAftermarketJobsByDate2]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAppointmentQuickServiceJobsByDate2]  TO  [ServiceScheduler];
GRANT Inherit ON  [CreateAppointmentFromPasteDelete]  TO  [ServiceScheduler];
GRANT Inherit ON  [CreateAppointment2]  TO  [ServiceScheduler];
GRANT Inherit ON  [EditPreAuthorizationAmount]  TO  [ServiceScheduler];
GRANT Inherit ON  [CreateAppointmentFromPaste2]  TO  [ServiceScheduler];
GRANT Inherit ON  [EditAppointment2]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetGlobalCapacity]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetBlueprintTechDayJobsByDate]  TO  [ServiceScheduler];
GRANT Inherit ON  [ChangeStatus2]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAlignmentCount]  TO  [ServiceScheduler];
GRANT Inherit ON  [EditPreAuthorizationAmount2]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetGlobalCapacity2]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetBevReport]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetPAInfo]  TO  [ServiceScheduler];
GRANT Inherit ON  [WriteToAnomalies]  TO  [ServiceScheduler];
GRANT Inherit ON  [SendEmailNotification]  TO  [ServiceScheduler];
GRANT Inherit ON  [InsertIntoTest]  TO  [ServiceScheduler];
GRANT Inherit ON  [RydellServiceAppointmentUpdated]  TO  [ServiceScheduler];
GRANT Inherit ON  [RemoveAppointmentFromServiceAppointmentsOfInterest]  TO  [ServiceScheduler];
GRANT Inherit ON  [zzz]  TO  [ServiceScheduler];
GRANT Inherit ON  [IssueEMailNotificationTerminateEvent]  TO  [ServiceScheduler];
GRANT Inherit ON  [GetAvailableAftermarketJobsByDate2]  TO  [ServiceScheduler];
GRANT Inherit ON  [Utilities]  TO  [ServiceScheduler];
GRANT Inherit ON  [RydellService]  TO  [ServiceScheduler];
GRANT Inherit ON  [ads]  TO  [ServiceScheduler];

EXECUTE PROCEDURE sp_ModifyPermission( 11, NULL, NULL, 'ServiceScheduler', 8, TRUE );
EXECUTE PROCEDURE sp_ModifyPermission( 11, NULL, NULL, 'ServiceScheduler', 2147483904, FALSE );

GRANT  Inherit Create TABLE TO  [ServiceScheduler];
GRANT  Inherit Create VIEW TO  [ServiceScheduler];
GRANT  Inherit Create USER TO  [ServiceScheduler];
GRANT  Inherit Create USER GROUP TO  [ServiceScheduler];
GRANT  Inherit Create PROCEDURE TO  [ServiceScheduler];
GRANT  Inherit Create FUNCTION TO  [ServiceScheduler];
GRANT  Inherit Create PACKAGE TO  [ServiceScheduler];
GRANT  Inherit Create LINK TO  [ServiceScheduler];
GRANT  Inherit Create PUBLICATION TO  [ServiceScheduler];
GRANT  Inherit Create SUBSCRIPTION TO  [ServiceScheduler];
EXECUTE PROCEDURE sp_ModifyPermission( 11, NULL, NULL, 'DB:Public', 2147483904, FALSE );

