SELECT * 
FROM dimTech
WHERE flagDeptCode = 'RE'
AND currentrow = true
ORDER BY technumber

SELECT m.*,n.*, iif(clockhours <> 0, round(flaghours/clockhours, 2), 0)
FROM (
  SELECT b.thedate, e.Description, e.employeenumber, round(sum(d.weightfactor * a.flaghours), 2) AS flaghours
  -- SELECT COUNT(*)
  FROM factRepairOrder a
  INNER JOIN day b on a.opendatekey = b.datekey
    AND b.thedate > curdate() - 60
  INNER JOIN dimTechGroup c on a.techGroupKey = c.TechGroupKey
  INNER JOIN brTechGroup d on c.techGroupKey = d.TechGroupKey  
  INNER JOIN dimTech e on d.TechKey = e.TechKey
    AND e.FlagDeptCode = 'RE'
    AND employeenumber <> 'NA'
  GROUP BY  b.thedate, e.Description, e.employeenumber) m  
LEFT JOIN ( 
  SELECT b.thedate, c.employeenumber, clockhours, regularhours, overtimehours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
     AND b.thedate > curdate() - 60
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey) n on m.thedate = n.thedate
AND m.employeenumber = n.employeenumber  
 

-- 9/18 ben conversation
SELECT * FROM dimservicetype

SELECT a.ro, b.thedate, flaghours
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
--  AND b.thedate > curdate() - 30
INNER JOIN dimtechgroup c on a.techgroupkey = c.techgroupkey
INNER JOIN brtechgroup d on c.techgroupkey = d.techgroupkey
INNER JOIN dimtech e on d.techkey = e.techkey 
  AND e.technumber = '526' 
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 're'

-- opcodes on ros w/servicetype = RE  
SELECT g.opcode, left(g.description, 25), min(ro), max(ro),min(b.thedate), max(b.thedate), COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 120
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 're' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey   
WHERE flaghours > 0
GROUP BY g.opcode, left(g.description, 25)
ORDER BY opcode

-- GROUP the opcode with the tech
SELECT g.opcode, left(g.description, 25), 
  e.technumber, coalesce(e.name, coalesce(e.description, 'xxxxxxxxxxxxxxxxx')), 
  min(ro), max(ro),min(b.thedate), max(b.thedate), COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 90
INNER JOIN dimtechgroup c on a.techgroupkey = c.techgroupkey
INNER JOIN brtechgroup d on c.techgroupkey = d.techgroupkey
INNER JOIN dimtech e on d.techkey = e.techkey   
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 're' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey   
WHERE flaghours > 0
GROUP BY g.opcode, e.technumber, left(g.description, 25), 
  coalesce(e.name, coalesce(e.description, 'xxxxxxxxxxxxxxxxx'))
ORDER BY opcode

-- 9/18, these have now been fixed
-- what IS interesting here are the xxxxxxxxxx techs, wy dat
SELECT *
FROM factrepairorder
WHERE ro = '16126182'

SELECT * 
FROM brTechGroup
WHERE techgroupkey = 1

SELECT * FROM dimtech WHERE techkey = 335
hmmm, tech N/A, implying no flag hours

-- this IS a factRepairOrder issue
SELECT ro, line, b.thedate
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE techgroupkey = 1
AND flaghours <> 0
GROUP BY ro, line, b.thedate
ORDER BY b.thedate DESC 

--9/19
so how the fuck DO we know whose clockhours we track AND attribute to detail

SELECT a.storecode, a.name, a.firstname, a.lastname, a.employeenumber, a.employeekey, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, b.technumber
FROM edwEmployeeDim a
LEFT JOIN dimTech b on a.employeenumber = b.employeenumber
WHERE a.currentrow = true
  AND a.Activecode = 'A'
  AND a.pydeptcode = '24'
  AND a.PayrollClass = 'Hourly'
  
    
SELECT DISTINCT storecode, pydeptcode, pydept, distcode, distribution
FROM edwEmployeeDim a  
WHERE a.currentrow = true
  AND a.Activecode = 'A'
  AND a.PayrollClass = 'Hourly'
  
  
-- 9/22 pivoted weekly opcode numbers for ben's friday meetings
SELECT * FROM day

SELECT a.thruDate,
  SUM(CASE WHEN b.opcode = 'CBW' THEN 1 ELSE 0 END) AS CBW,
  SUM(CASE WHEN b.opcode = 'CFS' THEN 1 ELSE 0 END) AS CFS,
  SUM(CASE WHEN b.opcode = 'NDEL' THEN 1 ELSE 0 END) AS NDEL,
  SUM(CASE WHEN b.opcode = 'PS1' THEN 1 ELSE 0 END) AS PS1,
  SUM(CASE WHEN b.opcode = 'STG' THEN 1 ELSE 0 END) AS STG,
  SUM(CASE WHEN b.opcode = 'TBW' THEN 1 ELSE 0 END) AS TBW,
  SUM(CASE WHEN b.opcode = 'TDW' THEN 1 ELSE 0 END) AS TDW,
  SUM(CASE WHEN b.opcode = 'TSP' THEN 1 ELSE 0 END) AS TSP,  
  SUM(CASE WHEN b.opcode = 'BUFF' THEN 1 ELSE 0 END) AS BUFF,
  SUM(CASE WHEN b.opcode = 'DLX' THEN 1 ELSE 0 END) AS DLX,
  SUM(CASE WHEN b.opcode = 'EXP' THEN 1 ELSE 0 END) AS EXP,
  SUM(CASE WHEN b.opcode = 'EXT' THEN 1 ELSE 0 END) AS EXT,
  SUM(CASE WHEN b.opcode = 'HLB' THEN 1 ELSE 0 END) AS HLB,
  SUM(CASE WHEN b.opcode = 'INT' THEN 1 ELSE 0 END) AS INT,
  SUM(CASE WHEN b.opcode = 'SHO' THEN 1 ELSE 0 END) AS SHO,
  SUM(CASE WHEN b.opcode = 'UDEL' THEN 1 ELSE 0 END) AS UDEL
--SELECT *  
FROM (
  SELECT min(thedate) as fromDate, max(thedate) AS thruDate  
  FROM day  
  WHERE yearmonth = 201309
  GROUP BY SundayToSaturdayWeek) a
LEFT JOIN (  
  SELECT g.opcode, left(g.description, 25), b.thedate
  FROM factrepairorder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
    AND b.yearmonth = 201309
  INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
    AND f.servicetypecode = 're' 
  INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey   
  WHERE flaghours > 0
    AND a.StoreCode = 'RY1') b  on b.thedate BETWEEN a.fromdate AND a.thrudate
GROUP BY a.thruDate  
  
-- opcodes on ros w/servicetype = RE  
SELECT g.opcode, left(g.description, 25), min(ro), max(ro),min(b.thedate), max(b.thedate), COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 120
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 're' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey   
WHERE flaghours > 0
GROUP BY g.opcode, left(g.description, 25)
ORDER BY opcode



-- 9/25
SELECT a.storecode, a.name, a.firstname, a.lastname, a.employeenumber, a.employeekey, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution, b.technumber
FROM edwEmployeeDim a
LEFT JOIN dimTech b on a.employeenumber = b.employeenumber
WHERE a.currentrow = true
  AND a.Activecode = 'A'
  AND a.pydeptcode = '24'
  AND a.PayrollClass = 'Hourly'
  
  
SELECT a.storecode, a.name, a.firstname, a.lastname, a.employeenumber, a.employeekey, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution
FROM edwEmployeeDim a
WHERE a.currentrow = true
  AND a.Activecode = 'A'
  AND a.pydeptcode = '24'
  AND a.PayrollClass = 'Hourly'  
ORDER BY lastname  
  
SELECT b.thedate, a.clockhours, a.regularhours, a.overtimehours, c.firstname, 
  c.lastname, d.technumber,
  b.datekey, d.techkey
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
  AND b.thedate = '09/24/2013'
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  AND c.pydeptcode = '24'  
LEFT JOIN dimTech d on c.employeenumber = d.employeenumber
  AND b.thedate BETWEEN d.techkeyfromdate AND d.techkeythrudate  
WHERE a.clockhours > 0  

-- hmm, actually need full list of employees, THEN clockhours for those employees, 
--   THEN flaghours for those employees
i keep mindfucking on ekfrom/thru
so let us look at it this way
head COUNT for detail for each day of the last month
-- hourly emp headcount for dept 24
SELECT a.thedate, COUNT(*) AS headcount
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE a.yearmonth in (201308,201309)
GROUP BY a.thedate
-- clockhours
here IS the mindfuck territory
i have clockhours for an employeekey
how DO i link to edwEmployeeDim based on ekFrom/Thru
IN the sense of 
i want ALL the emplo

SELECT a.thedate
FROM day a
LEFT JOIN edwClockHoursFact b on a.datekey = b.datekey
WHERE a.yearmonth in (201308,201309)

-- current census
SELECT a.storecode, a.name, a.firstname, a.lastname, a.employeenumber, a.employeekey, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution
FROM edwEmployeeDim a
WHERE a.currentrow = true
  AND a.Activecode = 'A'
  AND a.pydeptcode = '24'
  AND a.PayrollClass = 'Hourly'  
ORDER BY lastname  
-- did NOT pick up Darrick Richardson because active code = 'P'
-- it IS a bug IN sp.xfmEmpDIM
-- so here IS the current census
SELECT a.storecode, a.name, a.firstname, a.lastname, a.employeenumber, a.employeekey, a.pydeptcode, a.pydept, a.distcode, 
  a.distribution
FROM edwEmployeeDim a
WHERE a.currentrow = true
  AND a.Active = 'Active'
  AND a.pydeptcode = '24'
  AND a.PayrollClass = 'Hourly'  
ORDER BY lastname  

-- so, starting with census
what IS the census for each day
subset of employeekeys

-- hourly emp headcount for dept 24
-- ok, how the fuck DO i get 42
SELECT a.thedate, COUNT(*) AS headcount
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE a.thedate = curdate()
GROUP BY a.thedate
-- fuck yes, the issue IS that termed folks have OPEN ended empkeyThruDates
-- so does active work? no, that IS type 1, but currentrow might no it fucking
-- won't, because the currentrow IS the fucking one with the OPEN ended ekThruDate
SELECT a.thedate, b.name
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.currentrow = true
WHERE a.thedate = curdate()
ORDER BY name

-- so what IS the fucking general algorith for determining census on a give day
-- that's what i have failed to define
-- thedate BETWEEN ekfromdate AND termdate ?
-- this looks good for the census on a given date
SELECT a.thedate, b.name
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND b.termdate >= a.thedate
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE a.thedate = curdate()
ORDER BY name
-- so, now, head COUNT last 2 months
SELECT a.thedate, COUNT(*)
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND b.termdate >= a.thedate
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE a.yearmonth in (201308,201309)
GROUP BY a.thedate
/*
-- so, back to mindfuck territory, clock hours that map to a census empkey
-- aha 198215 farah mohamed 8/4/13
-- hired 8/4 ekfromdate 8/5
SELECT *
FROM stgArkonaPYPCLOCKIN a
INNER JOIN stgArkonaPYMAST b on a.yiemp# = b.ymempn
  AND b.ymdept = '24'
WHERE a.yiclkind BETWEEN curdate() - 90 AND curdate()
  AND NOT EXISTS (
    SELECT 1
    FROM edwEmployeeDim b
    WHERE employeenumber = a.yiemp#
      AND termdate >= a.yiclkind
      AND a.yiclkind BETWEEN employeekeyfromdate AND employeekeythrudate) 
-- hmm any others?
SELECT name, employeenumber, employeekey, hiredate, termdate, employeekeyfromdate, employeekeythrudate
FROM edwEmployeeDim a
WHERE NOT EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = a.employeenumber
  AND hiredate = employeekeyfromdate)    
AND active = 'Active'
  AND currentrow = true    
-- mostly old history AND goofy shit LIKE jay sorum, maybe some rehire/reuse that
-- didn't get processed correctly
SELECT x.*, ABS(hiredate - minEK)
FROM ( 
  SELECT employeenumber, name, hiredate, MIN(employeekeyfromdate) AS minEK
  FROM edwEmployeeDim  
  GROUP BY employeenumber, name, hiredate) x
WHERE hiredate <> minEK
ORDER BY hiredate DESC

SELECT  name, employeenumber, employeekey, hiredate, termdate, employeekeyfromdate, employeekeythrudate,
  b.ymhdte, b.ymhdto, left(a.rowchangereason, 20)
FROM edwEmployeeDim a
INNER JOIN stgArkonaPYMAST b on a.employeenumber = b.ymempn
  AND b.ymhdte <> b.ymhdto
WHERE a.hiredate <> ymhdte
ORDER BY a.name  

-- fuck it good enuf
*/
-- so, now, head COUNT last 2 months
SELECT a.thedate, COUNT(*)
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND b.termdate >= a.thedate
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE a.yearmonth in (201308,201309)
GROUP BY a.thedate

-- 9/27 flirting with thinking i got with the notion of IF the clock hours exist
-- so any employeekey for which that employee was ever IN dept 24 AS an hourly employee
-- clock hours
SELECT *
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
WHERE b.yearmonth in (201308,201309)
  AND EXISTS (
    SELECT 1
    FROM edwEmployeeDim 
    WHERE employeekey = a.employeekey
      AND pydeptcode = '24'
      AND payrollclasscode = 'h')
      
-- census AND flag hours 
SELECT a.thedate, b.Employeenumber, b.Employeekey, b.name
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND b.termdate >= a.thedate
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE a.yearmonth in (201308,201309)
ORDER BY a.thedate, b.name
/*
-- ok, back to mindfuckville IF i DO a LEFT JOIN to clockhours FROM census
-- AND there are clockhours that don't show up 
so, let me DO temp tables for census AND clockhours going back a year, THEN a full OUTER JOIN
-- DROP TABLE #census;
SELECT a.thedate, b.Employeenumber, b.Employeekey, b.name
INTO #census
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND b.termdate >= a.thedate
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE a.theyear = 2013
  AND a.thedate < curdate();
-- DROP TABLE #clock;
SELECT b.thedate, a.employeekey, SUM(RegularHours) AS Reg, SUM(OvertimeHours) AS OT
INTO #clock
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
WHERE b.theyear = 2013
  AND b.thedate < curdate()
  AND a.clockhours + a.vacationhours + a.ptohours + a.holidayhours <> 0
  AND EXISTS (
    SELECT 1
    FROM edwEmployeeDim 
    WHERE employeekey = a.employeekey
      AND pydeptcode = '24'
      AND payrollclasscode = 'h')
GROUP BY b.thedate, a.employeekey;  

SELECT a.*, b.*, c.dayname 
FROM #census a
full OUTER JOIN #clock b on a.thedate = b.thedate AND a.employeekey = b.employeekey 
LEFT JOIN day c on a.thedate = c.thedate
WHERE a.employeekey IS NULL OR b.employeekey IS NULL 

-- this looks pretty good, the only NULL census rows are for
-- ek 2026 on 5/24/13 & 5/28/13 
-- Jonathan Hawkins: clock hours after term date (5/20)
GOOD ENUF
*/

-- ok that rabbit hole IS closed for now
-- census
SELECT a.thedate, b.employeenumber, b.employeekey, b.name
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND b.termdate >= a.thedate
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE a.yearmonth in (201308,201309)
-- clock hours
SELECT *
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
WHERE b.yearmonth in (201308,201309)
  AND a.clockhours + a.vacationhours + a.ptohours + a.holidayhours <> 0
  AND EXISTS (
    SELECT 1
    FROM edwEmployeeDim 
    WHERE employeekey = a.employeekey
      AND pydeptcode = '24'
      AND payrollclasscode = 'h')
-- USING census AS the base, i think this IS ok
SELECT a.thedate, a.dayname, b.employeenumber, b.employeekey, b.name, c.clockhours
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND b.termdate >= a.thedate
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
LEFT JOIN edwClockHoursFact c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
--WHERE a.yearmonth in (201308,201309)
WHERE a.thedate BETWEEN curdate() -14 AND curdate()
UNION 
SELECT a.thedate, a.dayname, 'All', 999, 'Detail', sum(c.clockhours)
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND b.termdate >= a.thedate
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
LEFT JOIN edwClockHoursFact c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
--WHERE a.yearmonth in (201308,201309)
WHERE a.thedate BETWEEN curdate() -14 AND curdate()
GROUP BY a.thedate, a.dayname

-- now flag hours
-- 
SELECT *
FROM dimtech
WHERE flagDeptCode = 'RE'
ORDER BY technumber

SELECT b.thedate, b.dayname, a.ro, a.line, a.techgroupkey, d.technumber, d.description
FROM factRepairOrder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
INNER JOIN dimTech d on c.TechKey = d.TechKey
--  AND d.FlagDeptCode = 'RE'
WHERE b.thedate = curdate() - 2 //BETWEEN curdate() -14 AND curdate()
  AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
  AND d.FlagDeptCode = 'RE'
  
SELECT b.thedate, b.dayname, d.technumber, d.description, SUM(flaghours)
FROM factRepairOrder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
INNER JOIN dimTech d on c.TechKey = d.TechKey
  AND d.FlagDeptCode = 'RE'
WHERE b.thedate = curdate() - 2 //BETWEEN curdate() -14 AND curdate()
  AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
GROUP BY b.thedate, b.dayname, d.technumber, d.description  

-- hmmm, ADD IN opcode
  
SELECT b.thedate, b.dayname, d.technumber, d.description, SUM(flaghours), e.opcode, COUNT(*)
FROM factRepairOrder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
INNER JOIN dimTech d on c.TechKey = d.TechKey
--  AND d.FlagDeptCode = 'RE'
INNER JOIN dimOpcode e on a.opcodekey = e.opcodekey
WHERE b.thedate = curdate() - 2 //BETWEEN curdate() -14 AND curdate()
  AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
  AND d.FlagDeptCode = 'RE'  
GROUP BY b.thedate, b.dayname, d.technumber, d.description, e.opcode

-- finest grain: day/opcode/tech
SELECT b.thedate, b.dayname, a.ro, a.line, d.technumber, d.description, SUM(flaghours), e.opcode, COUNT(*)
FROM factRepairOrder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
INNER JOIN dimTech d on c.TechKey = d.TechKey
--  AND d.FlagDeptCode = 'RE'
INNER JOIN dimOpcode e on a.opcodekey = e.opcodekey
WHERE b.thedate = curdate() - 2 //BETWEEN curdate() -14 AND curdate()
  AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
  AND d.FlagDeptCode = 'RE'  
GROUP BY b.thedate, b.dayname, d.technumber, a.ro, a.line, d.description, e.opcode

-- ok, great, now what the fuck DO we want to show
-- day,tech,clockhours,flaghours

-- census/clockhours for dept 24
SELECT a.thedate, a.dayname, b.employeenumber, b.employeekey, b.name, c.clockhours
FROM day a
LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
  AND b.PayrollClassCode = 'H'
  AND b.termdate >= a.thedate
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
LEFT JOIN edwClockHoursFact c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
WHERE a.thedate BETWEEN curdate() -14 AND curdate()
GROUP BY a.thedate, a.dayname

-- flag hours for recon tech numbers
SELECT b.thedate, b.dayname, d.technumber, d.description, SUM(flaghours)
FROM factRepairOrder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
INNER JOIN dimTech d on c.TechKey = d.TechKey
  AND d.FlagDeptCode = 'RE'
WHERE b.thedate = curdate() - 2 //BETWEEN curdate() -14 AND curdate()
  AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
GROUP BY b.thedate, b.dayname, d.technumber, d.description  


-- Daily Productivity, last 2 weeks
-- ? leave out Saturday & Sunday ?
-- 300 mSec, good enuf
-- basis IS Day (one row for each day IN period) -> census -> clockhours
--                                              \-> ro's final closed (on day) with Recon tech flag hours
SELECT a.theDate, a.dayname, round(a.ClockHours, 0) AS ClockHours, 
  coalesce(round(b.flaghours, 0), 0) AS FlagHours,
  CASE 
    WHEN a.Clockhours = 0 OR coalesce(b.flaghours,0) = 0 THEN 0
    WHEN a.dayname = 'Sunday' THEN 0
    ELSE round((b.Flaghours/a.Clockhours)*100,0) 
  END AS Proficiency
FROM (
  SELECT a.thedate, a.dayname, sum(c.clockhours) AS ClockHours
  FROM day a
  LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
    AND b.PayrollClassCode = 'H'
    AND b.termdate >= a.thedate
    AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  LEFT JOIN edwClockHoursFact c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
  WHERE a.thedate BETWEEN curdate() -14 AND curdate() - 1
  GROUP BY a.thedate, a.dayname
  UNION
  SELECT a.thedate, a.dayname, round(sum(c.clockhours), 0) AS ClockHours
  FROM day a
  LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
    AND b.PayrollClassCode = 'H'
    AND b.termdate >= a.thedate
    AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  LEFT JOIN factClockHoursToday c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
  WHERE a.thedate = curdate()
  GROUP BY a.thedate, a.dayname) a
LEFT JOIN (
  SELECT b.thedate, SUM(flaghours) AS flaghours
  FROM factRepairOrder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
  INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
  INNER JOIN dimTech d on c.TechKey = d.TechKey
    AND d.FlagDeptCode = 'RE'
  WHERE b.thedate BETWEEN curdate() -14 AND curdate() - 1
    AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
  GROUP BY b.thedate
  UNION 
  SELECT b.thedate, SUM(flaghours)
  FROM todayFactRepairOrder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
  INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
  INNER JOIN dimTech d on c.TechKey = d.TechKey
    AND d.FlagDeptCode = 'RE'
  WHERE b.thedate = curdate()
    AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
  GROUP BY b.thedate) b on a.thedate = b.thedate
--WHERE a.dayname NOT IN ('Saturday','Sunday')
WHERE round(a.ClockHours, 0) + coalesce(round(b.flaghours, 0), 0) > 0 
ORDER BY a.thedate DESC 
-- just talked to ben
-- my thoughts are forget the rollups (this vs last week/payperiod/month)
-- just DO the last 14 days BY dept/bytech  

-- the mindfucking i'm doing on  dept techs
-- just DO the detail level at technumber level
-- since the bases IS day, it does NOT matter IF there are no clock hours
-- so the clock hours part remains the same...
-- the issue IS the JOIN FROM clock to flag

-- ok, for dept techs, the emp# IS NA
-- 300 mSec, good enuf
-- basis IS Day (one row for each day IN period)/employeenumber -> census -> clockhours
--                                              \-> ro's final closed (on day) with Recon tech flag hours
SELECT a.theDate, a.dayname, name, round(a.ClockHours, 0) AS ClockHours, 
  coalesce(round(b.flaghours, 0), 0) AS FlagHours,
  CASE 
    WHEN a.Clockhours = 0 OR coalesce(b.flaghours,0) = 0 THEN 0
    WHEN a.dayname = 'Sunday' THEN 0
    ELSE round((b.Flaghours/a.Clockhours)*100,0) 
  END AS Proficiency
--INTO #wtf  
FROM (
  SELECT a.thedate, a.dayname, b.name, b.employeenumber, sum(c.clockhours) AS ClockHours
  FROM day a
  LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
    AND b.PayrollClassCode = 'H'
    AND b.termdate >= a.thedate
    AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  LEFT JOIN edwClockHoursFact c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
  WHERE a.thedate BETWEEN curdate() -14 AND curdate() - 1
  GROUP BY a.thedate, a.dayname, b.name, b.employeenumber
  UNION
  SELECT a.thedate, a.dayname, b.name, b.employeenumber, round(sum(c.clockhours), 0) AS ClockHours
  FROM day a
  LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
    AND b.PayrollClassCode = 'H'
    AND b.termdate >= a.thedate
    AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  LEFT JOIN factClockHoursToday c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
  WHERE a.thedate = curdate()
  GROUP BY a.thedate, a.dayname, b.name, b.employeenumber
  UNION
  SELECT a.thedate, a.dayname, 'Other' AS Name, 'NA' AS EmployeeNumber, 0 AS ClockHours
  FROM day a
  WHERE a.thedate BETWEEN curdate() - 14 AND curdate()) a
LEFT JOIN (
  SELECT b.thedate, d.employeenumber, SUM(flaghours) AS flaghours
  FROM factRepairOrder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
  INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
  INNER JOIN dimTech d on c.TechKey = d.TechKey
    AND d.FlagDeptCode = 'RE'
  WHERE b.thedate BETWEEN curdate() -14 AND curdate() - 1
    AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
  GROUP BY b.thedate, d.employeenumber
  UNION 
  SELECT b.thedate, d.employeenumber, SUM(flaghours)
  FROM todayFactRepairOrder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
  INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
  INNER JOIN dimTech d on c.TechKey = d.TechKey
    AND d.FlagDeptCode = 'RE'
  WHERE b.thedate = curdate()
    AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
  GROUP BY b.thedate,d.employeenumber) b on a.thedate = b.thedate AND a.employeenumber = b.employeenumber
WHERE round(a.ClockHours, 0) +  coalesce(round(b.flaghours, 0), 0) > 0
    
    
    
    
    
    
    
SELECT thedate, COUNT(*) FROM #wtf GROUP BY thedate   

SELECT a.*
FROM (
 SELECT thedate, name FROM #wtf WHERE thedate = '09/25/2013') a
LEFT JOIN (
  SELECT thedate, name FROM #wtf WHERE thedate = '09/22/2013') b on a.name = b.name
WHERE b.name IS NULL   

SELECT *
FROM #wtf
ORDER BY name, thedate  


-- 10/3
ALTER PROCEDURE detail2WeekProficiency (
  Level cichar(12) output,
  TheDate cichar(5) output,
  DayName cichar(12) output,
  Who cichar(25) output,
  ClockHours cichar(6) output,
  FlagHours cichar(6) output,
  Proficiency cichar(6) output)
BEGIN 
/*
EXECUTE PROCEDURE detail2WeekProficiency();
SELECT * FROM (EXECUTE PROCEDURE detail2WeekProficiency()) a ORDER BY thedate, who
*/  
INSERT INTO __output
SELECT top 200 *
FROM (
  SELECT 'Department' AS Level, a.mmdd, a.dayname, 'AAAAALL' AS Who, 
    trim(cast(round(a.ClockHours, 0) as sql_char)) AS ClockHours, 
    trim(cast(coalesce(round(b.flaghours, 0), 0) AS sql_char)) AS FlagHours,
    trim(cast(CASE 
      WHEN a.Clockhours = 0 OR coalesce(b.flaghours,0) = 0 THEN 0
  --    WHEN a.dayname = 'Sunday' THEN 0
      ELSE round((b.Flaghours/a.Clockhours)*100,0) 
    END AS sql_char)) AS Proficiency
  FROM (
    SELECT a.thedate, a.dayname, a.mmdd, sum(c.clockhours) AS ClockHours
    FROM day a
    LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
      AND b.PayrollClassCode = 'H'
      AND b.termdate >= a.thedate
      AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
    LEFT JOIN edwClockHoursFact c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
    WHERE a.thedate BETWEEN curdate() -14 AND curdate() - 1
    GROUP BY a.thedate, a.dayname, a.mmdd
    UNION
    SELECT a.thedate, a.dayname, a.mmdd, round(sum(c.clockhours), 0) AS ClockHours
    FROM day a
    LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
      AND b.PayrollClassCode = 'H'
      AND b.termdate >= a.thedate
      AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
    LEFT JOIN factClockHoursToday c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
    WHERE a.thedate = curdate()
    GROUP BY a.thedate, a.dayname, a.mmdd) a
  LEFT JOIN (
    SELECT b.thedate, SUM(flaghours) AS flaghours
    FROM factRepairOrder a
    INNER JOIN day b on a.finalclosedatekey = b.datekey
    INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
    INNER JOIN dimTech d on c.TechKey = d.TechKey
      AND d.FlagDeptCode = 'RE'
    WHERE b.thedate BETWEEN curdate() -14 AND curdate() - 1
      AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
    GROUP BY b.thedate
    UNION 
    SELECT b.thedate, SUM(flaghours)
    FROM todayFactRepairOrder a
    INNER JOIN day b on a.finalclosedatekey = b.datekey
    INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
    INNER JOIN dimTech d on c.TechKey = d.TechKey
      AND d.FlagDeptCode = 'RE'
    WHERE b.thedate = curdate()
      AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
    GROUP BY b.thedate) b on a.thedate = b.thedate
  --WHERE a.dayname NOT IN ('Saturday','Sunday')
  WHERE round(a.ClockHours, 0) + coalesce(round(b.flaghours, 0), 0) > 0 
  UNION 
  SELECT 'Person', a.mmdd, a.dayname, name, 
    trim(cast(round(a.ClockHours, 0) as sql_char)) AS ClockHours, 
    trim(cast(coalesce(round(b.flaghours, 0), 0) as sql_char)) AS FlagHours,
    trim(cast(CASE 
      WHEN a.Clockhours = 0 OR coalesce(b.flaghours,0) = 0 THEN 0
  --    WHEN a.dayname = 'Sunday' THEN 0
      ELSE round((b.Flaghours/a.Clockhours)*100,0) 
    END AS sql_char)) AS Proficiency
  --INTO #wtf  
  FROM (
    SELECT a.thedate, a.dayname, a.mmdd, b.name, b.employeenumber, sum(c.clockhours) AS ClockHours
    FROM day a
    LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
      AND b.PayrollClassCode = 'H'
      AND b.termdate >= a.thedate
      AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
    LEFT JOIN edwClockHoursFact c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
    WHERE a.thedate BETWEEN curdate() -14 AND curdate() - 1
    GROUP BY a.thedate, a.dayname, a.mmdd, b.name, b.employeenumber
    UNION
    SELECT a.thedate, a.dayname, a.mmdd, b.name, b.employeenumber, round(sum(c.clockhours), 0) AS ClockHours
    FROM day a
    LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
      AND b.PayrollClassCode = 'H'
      AND b.termdate >= a.thedate
      AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
    LEFT JOIN factClockHoursToday c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
    WHERE a.thedate = curdate()
    GROUP BY a.thedate, a.dayname, a.mmdd, b.name, b.employeenumber
    UNION
    SELECT a.thedate, a.dayname, a.mmdd, 'Other' AS Name, 'NA' AS EmployeeNumber, 0 AS ClockHours
    FROM day a
    WHERE a.thedate BETWEEN curdate() - 14 AND curdate()) a
  LEFT JOIN (
    SELECT b.thedate, d.employeenumber, SUM(flaghours) AS flaghours
    FROM factRepairOrder a
    INNER JOIN day b on a.finalclosedatekey = b.datekey
    INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
    INNER JOIN dimTech d on c.TechKey = d.TechKey
      AND d.FlagDeptCode = 'RE'
    WHERE b.thedate BETWEEN curdate() -14 AND curdate() - 1
      AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
    GROUP BY b.thedate, d.employeenumber
    UNION 
    SELECT b.thedate, d.employeenumber, SUM(flaghours)
    FROM todayFactRepairOrder a
    INNER JOIN day b on a.finalclosedatekey = b.datekey
    INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
    INNER JOIN dimTech d on c.TechKey = d.TechKey
      AND d.FlagDeptCode = 'RE'
    WHERE b.thedate = curdate()
      AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
    GROUP BY b.thedate,d.employeenumber) b on a.thedate = b.thedate AND a.employeenumber = b.employeenumber
  WHERE round(a.ClockHours, 0) +  coalesce(round(b.flaghours, 0), 0) > 0) x
ORDER BY mmdd DESC, who ASC;
END;
    



