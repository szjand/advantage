/* list of techs
SELECT b.technumber, a.employeekey, a.name, a.employeekeyfromdate,
  a.employeekeythrudate
FROM vempdim a
INNER JOIN dimtech b ON a.employeenumber = b.employeenumber
WHERE a.storecode = 'ry1'
  AND a.pydept = 'service'
  AND a.distcode = 'stec'
  AND termdate > '01/01/2012'
--ORDER BY b.technumber
--ORDER BY employeekey
ORDER BY a.name  

-- better 
SELECT b.technumber, a.employeekey, a.name, a.employeekeyfromdate,
  a.employeekeythrudate
FROM vempdim a
INNER JOIN dimtech b ON a.employeenumber = b.employeenumber
WHERE a.storecode = 'ry1'
  AND a.pydept = 'service'
  AND a.distcode = 'stec'
  AND '04/08/2012' between a.employeekeyfromdate AND a.employeekeythrudate
ORDER BY b.technumber  
-- best
SELECT b.technumber, a.employeekey, a.name, a.employeekeyfromdate,
  a.employeekeythrudate
FROM vempdim a
INNER JOIN dimtech b ON a.employeenumber = b.employeenumber
WHERE a.storecode = 'ry1'
  AND a.pydept = 'service'
  AND a.distcode = 'stec'
  AND (
    '08/12/2012' between a.employeekeyfromdate AND a.employeekeythrudate
    OR
    '08/25/2012' between a.employeekeyfromdate AND a.employeekeythrudate)
ORDER BY a.name    
--ORDER BY employeekey
ORDER BY b.technumber 
*/

-- bad data:
SELECT a.thedate, b.name, a.hours, a.reason
FROM stgTechClockTimeAdjustments a
LEFT JOIN edwEmployeeDim b ON a.employeekey = b.employeekey
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
WHERE b.name IS NULL   

-- expose more detail about bad data  
SELECT a.*, c.technumber
FROM stgTechClockTimeAdjustments a
LEFT JOIN edwEmployeeDim b ON a.employeekey = b.employeekey
  AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
LEFT JOIN dimtech c ON b.employeenumber = c.employeenumber  
WHERE a.thedate = '03/10/2012'
  AND hours = 7

-- summary 8/8
DECLARE @d1 date;
DECLARE @d2 date;
@d1 = '07/29/2012';
@d2 = '08/11/2012';
SELECT b.name, c.technumber, a.employeekey, 
  coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other,
  coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training
FROM stgTechClockTimeAdjustments a
INNER join edwEmployeeDim b ON a.employeekey = b.employeekey
LEFT JOIN dimtech c ON b.employeenumber = c.employeenumber
WHERE a.thedate BETWEEN @d1 AND @d2
GROUP BY b.name, c.technumber, a.employeekey
UNION
SELECT 'ALL', 'ALL', 0,
  coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other,
  coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training
FROM stgTechClockTimeAdjustments a
WHERE a.thedate BETWEEN @d1 AND @d2



-- 8/4
-- FROM rptClockHours (sp.fctClockHours)
--  SELECT b.thedate, b.datekey, c.storecode, a.employeekey, c.employeenumber, c.name, c.pydept, 
--    c.pydeptcode, c.distcode, distribution, coalesce(d.technumber, 'NA') AS technumber, clockhours, 
--    regularhours, overtimehours, vacationhours, ptohours, holidayhours
SELECT b.thedate,c.name, c.pydept, coalesce(d.technumber, 'NA') AS technumber, clockhours, 
  regularhours, overtimehours, vacationhours, ptohours, holidayhours, e.*    
FROM edwClockHoursFact a
INNER JOIN day b ON a.datekey = b.datekey
LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
LEFT JOIN dimtech d ON c.storecode = d.storecode
  AND c.employeenumber = d.employeenumber  
LEFT JOIN stgTechClockTimeAdjustments e ON a.employeekey = e.employeekey
  AND b.thedate = e.thedate    
WHERE b.thedate BETWEEN '11/06/2011' AND curdate() -1
  AND d.technumber <> 'NA'
  AND e.thedate IS NOT NULL 
  
  
SELECT b.thedate,c.name, c.pydept, coalesce(d.technumber, 'NA') AS technumber, clockhours, 
  regularhours, overtimehours, vacationhours, ptohours, holidayhours, e.* 
  
-- pp start/END dates  
select a.thedate, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate   
FROM day a
WHERE a.thedate BETWEEN '11/06/2011' AND curdate() -1 
 
-- one row per tech/date
SELECT thedate, Employeekey, 
  coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
  coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
FROM stgTechClockTimeAdjustments 
GROUP BY thedate, Employeekey
-- one row per tech/date with techadj
select a.thedate, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*   
FROM day a
LEFT JOIN (
  SELECT thedate, Employeekey, 
    coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
    coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
  FROM stgTechClockTimeAdjustments 
  GROUP BY thedate, Employeekey) b ON a.thedate = b.thedate
WHERE a.thedate BETWEEN '08/01/2012' AND curdate() -1 


select a.thedate, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.*
FROM day a
LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
LEFT JOIN (
  SELECT thedate, Employeekey, 
    coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
    coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
  FROM stgTechClockTimeAdjustments 
  GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
  AND b.employeekey = c.employeekey
WHERE a.thedate BETWEEN '11/06/2011' AND curdate() -1 


SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
  f.employeekeythrudate, e.technumber
FROM edwEmployeeDim f
LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
WHERE f.pydept = 'service'
  AND f.distcode = 'stec'
  AND e.technumber IS NOT NULL 

  
SELECT m.BiWeeklyPayPeriodEndDate, n.name, n.technumber, SUM(clockhours) AS clockhours,
  SUM(regularhours) AS reghours, SUM(overtimehours) AS othours, 
  SUM(vacationhours) AS vachours, SUM(ptohours) AS ptohours, SUM(holidayhours) AS holidayhours,
  SUM(coalesce(Training, 0)) AS traininghours, SUM(coalesce(other, 0)) AS otherhours
FROM (
  select a.thedate, a.dayname, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.Training, c.Other
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  LEFT JOIN (
    SELECT thedate, Employeekey, 
      coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
      coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
    FROM stgTechClockTimeAdjustments 
    GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
    AND b.employeekey = c.employeekey
  WHERE a.thedate BETWEEN '11/06/2011' AND curdate() -1) m  
LEFT JOIN (
  SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
    f.employeekeythrudate, e.technumber
  FROM edwEmployeeDim f
  LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
  WHERE f.pydept = 'service'
    AND f.distcode = 'stec'
    AND e.technumber IS NOT NULL) n ON m.thedate BETWEEN n.employeekeyfromdate AND n.employeekeythrudate
  AND m.employeekey = n.employeekey
WHERE n.name IS NOT NULL  
GROUP BY m.BiWeeklyPayPeriodEndDate, n.name, n.technumber 

-- clock hours BY payperiod
SELECT m.BiWeeklyPayPeriodEndDate, SUM(clockhours) AS clockhours,
  SUM(regularhours) AS reghours, SUM(overtimehours) AS othours, 
  SUM(vacationhours) AS vachours, SUM(ptohours) AS ptohours, SUM(holidayhours) AS holidayhours,
  SUM(coalesce(Training, 0)) AS traininghours, SUM(coalesce(other, 0)) AS otherhours
FROM (
  select a.thedate, a.dayname, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.Training, c.Other
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  LEFT JOIN (
    SELECT thedate, Employeekey, 
      coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
      coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
    FROM stgTechClockTimeAdjustments 
    GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
    AND b.employeekey = c.employeekey
  WHERE a.thedate BETWEEN '08/01/2012' AND curdate() -1) m  
LEFT JOIN (
  SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
    f.employeekeythrudate, e.technumber
  FROM edwEmployeeDim f
  LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
  WHERE f.pydept = 'service'
    AND f.distcode = 'stec'
    AND e.technumber IS NOT NULL) n ON m.thedate BETWEEN n.employeekeyfromdate AND n.employeekeythrudate
  AND m.employeekey = n.employeekey
WHERE n.name IS NOT NULL  
GROUP BY m.BiWeeklyPayPeriodEndDate


-- clock hours BY month
SELECT m.yearmonth, round(SUM(clockhours), 2) AS clockhours,
  round(SUM(regularhours), 2) AS reghours, SUM(overtimehours) AS othours, 
  SUM(vacationhours) AS vachours, SUM(ptohours) AS ptohours, SUM(holidayhours) AS holidayhours,
  SUM(coalesce(Training, 0)) AS traininghours, SUM(coalesce(other, 0)) AS otherhours
FROM (
  select a.thedate, a.yearmonth, a.dayname, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.Training, c.Other
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  LEFT JOIN (
    SELECT thedate, Employeekey, 
      coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
      coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
    FROM stgTechClockTimeAdjustments 
    GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
    AND b.employeekey = c.employeekey
  WHERE a.thedate BETWEEN '11/06/2011' AND curdate() -1) m  
LEFT JOIN (
  SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
    f.employeekeythrudate, e.technumber
  FROM edwEmployeeDim f
  LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
  WHERE f.pydept = 'service'
    AND f.distcode = 'stec'
    AND e.technumber IS NOT NULL) n ON m.thedate BETWEEN n.employeekeyfromdate AND n.employeekeythrudate
  AND m.employeekey = n.employeekey
WHERE n.name IS NOT NULL  
GROUP BY m.yearmonth

-- 8/5 getting hinky about what hours are what
-- does capacity include vacation hours ?
-- capacity AS 8 * # of techs currently employed
-- # of techs employed each day
SELECT a.thedate, COUNT(*)
FROM day a
LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.pydept = 'service'
  AND b.distcode = 'stec'
WHERE a.thedate BETWEEN '11/06/2011' AND curdate() - 1
GROUP BY a.thedate
-- techs per day, min/MAX dates
SELECT techs, MIN(thedate), MAX(thedate)
FROM (
  SELECT a.thedate, COUNT(*) AS techs
  FROM day a
  LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
    AND b.pydept = 'service'
    AND b.distcode = 'stec'
  WHERE a.thedate BETWEEN '11/06/2011' AND curdate() - 1
  GROUP BY a.thedate) x
GROUP BY techs

/*
-- ahh fuck, this turns INTO another etl - empDimBackToHell 8-6-12 
-- this gives some rows with NULL edwClockHoursFact data
SELECT a.thedate, b.employeekey, b.name, d.technumber, c.clockhours, c.vacationhours, c.ptohours, c.holidayhours
FROM day a
LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.pydept = 'service'
  AND b.distcode = 'stec'
LEFT JOIN edwClockHoursFact c ON a.datekey = c.datekey
  AND b.employeekey = c.employeekey  
LEFT JOIN dimTech d ON b.employeenumber = d.employeenumber  
WHERE a.thedate BETWEEN '11/06/2011' AND curdate() - 1
  AND clockhours IS NULL 
*/
-- 8/6
-- for comparison to holy graille
DECLARE @d1 date;
DECLARE @d2 date;
DECLARE @yearmonth integer;
@d1 = '07/15/2012';
@d2 = '07/28/2012';
@yearmonth = 201207;
SELECT n.name, n.technumber, round(SUM(clockhours), 2) AS clockhours,
  round(SUM(regularhours), 2) AS reghours, SUM(overtimehours) AS othours, 
  SUM(vacationhours) AS vachours, SUM(ptohours) AS ptohours, SUM(holidayhours) AS holidayhours,
  SUM(coalesce(Training, 0)) AS traininghours, SUM(coalesce(other, 0)) AS otherhours
FROM (
  select a.thedate, a.yearmonth, a.dayname, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.Training, c.Other
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  LEFT JOIN (
    SELECT thedate, Employeekey, 
      coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
      coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
    FROM stgTechClockTimeAdjustments 
    GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
    AND b.employeekey = c.employeekey
  WHERE a.thedate BETWEEN @d1 AND @d2) m  
LEFT JOIN (
  SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
    f.employeekeythrudate, e.technumber
  FROM edwEmployeeDim f
  LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
  WHERE f.pydept = 'service'
    AND f.distcode = 'stec'
    AND e.technumber IS NOT NULL) n ON m.thedate BETWEEN n.employeekeyfromdate AND n.employeekeythrudate
  AND m.employeekey = n.employeekey
WHERE n.name IS NOT NULL  
  AND m.yearmonth = @yearmonth
GROUP BY n.name, n.technumber     
UNION
SELECT 'zMONTH' AS name, 'ALL' AS technumber, round(SUM(clockhours), 2) AS clockhours,
  round(SUM(regularhours), 2) AS reghours, SUM(overtimehours) AS othours, 
  SUM(vacationhours) AS vachours, SUM(ptohours) AS ptohours, SUM(holidayhours) AS holidayhours,
  SUM(coalesce(Training, 0)) AS traininghours, SUM(coalesce(other, 0)) AS otherhours
FROM (
  select a.thedate, a.yearmonth, a.dayname, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.Training, c.Other
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  LEFT JOIN (
    SELECT thedate, Employeekey, 
      coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
      coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
    FROM stgTechClockTimeAdjustments 
    GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
    AND b.employeekey = c.employeekey
  WHERE a.thedate BETWEEN @d1 AND @d2) m  
LEFT JOIN (
  SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
    f.employeekeythrudate, e.technumber
  FROM edwEmployeeDim f
  LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
  WHERE f.pydept = 'service'
    AND f.distcode = 'stec'
    AND e.technumber IS NOT NULL) n ON m.thedate BETWEEN n.employeekeyfromdate AND n.employeekeythrudate
  AND m.employeekey = n.employeekey
WHERE n.name IS NOT NULL  
  AND m.yearmonth = @yearmonth
  
-- holy graille BY month
SELECT n.name, n.technumber, round(SUM(regularhours), 2) AS reghours, 
  SUM(overtimehours) AS othours, round(SUM(clockhours), 2) AS Total,
  SUM(coalesce(other, 0)) AS otherhours,
  SUM(coalesce(Training, 0)) AS traininghours, 
  round(SUM(clockhours), 2) - SUM(coalesce(other, 0)) - SUM(coalesce(Training, 0)) AS AdjHours
FROM (
  select a.thedate, a.yearmonth, a.dayname, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.Training, c.Other
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  LEFT JOIN (
    SELECT thedate, Employeekey, 
      coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
      coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
    FROM stgTechClockTimeAdjustments 
    GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
    AND b.employeekey = c.employeekey
  WHERE a.thedate BETWEEN '11/06/2011' AND curdate() -1) m  
LEFT JOIN (
  SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
    f.employeekeythrudate, e.technumber
  FROM edwEmployeeDim f
  LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
  WHERE f.pydept = 'service'
    AND f.distcode = 'stec'
    AND e.technumber IS NOT NULL) n ON m.thedate BETWEEN n.employeekeyfromdate AND n.employeekeythrudate
  AND m.employeekey = n.employeekey
WHERE n.name IS NOT NULL  
  AND m.yearmonth = 201205
GROUP BY n.name, n.technumber     
UNION -- totals
SELECT 'zMONTH' AS name, 'ALL' AS technumber, round(SUM(regularhours), 2) AS reghours, 
  SUM(overtimehours) AS othours, round(SUM(clockhours), 2) AS Total,
  SUM(coalesce(other, 0)) AS otherhours,
  SUM(coalesce(Training, 0)) AS traininghours, 
  round(SUM(clockhours), 2) - SUM(coalesce(other, 0)) - SUM(coalesce(Training, 0))
FROM (
  select a.thedate, a.yearmonth, a.dayname, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.Training, c.Other
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  LEFT JOIN (
    SELECT thedate, Employeekey, 
      coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
      coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
    FROM stgTechClockTimeAdjustments 
    GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
    AND b.employeekey = c.employeekey
  WHERE a.thedate BETWEEN '11/06/2011' AND curdate() -1) m  
LEFT JOIN (
  SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
    f.employeekeythrudate, e.technumber
  FROM edwEmployeeDim f
  LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
  WHERE f.pydept = 'service'
    AND f.distcode = 'stec'
    AND e.technumber IS NOT NULL) n ON m.thedate BETWEEN n.employeekeyfromdate AND n.employeekeythrudate
  AND m.employeekey = n.employeekey
WHERE n.name IS NOT NULL  
  AND m.yearmonth = 201205
ORDER BY technumber
  
  
-- 8/13
so i got fucked up ON THE notion that payroll IS NOT a good current indicator
eg for part of the perod of 7/1 - 7/14 holwerda IS IN PTEC AND IN STEC
but has flag hours for the entire period, the ptec hours were being LEFT out
which led to needing to find an inclusive way to make sure ALL records are included
which led to a huge diversion reconciling factTechFlagHoursByDay with factTechLineFlagDateHours
such that they each return the same hours by tech/day which led to restoreFactTechFlagHoursByDay.sql  

back to the inclusive look
the other part of the "inclusive" look IS to be sure to include ALL days for a tech
some days will have clock AND no flag hours, AND vice versa

that led to this:
simulate an OUTER JOIN FROM a UNION

SELECT y.*, z.storecode, z.distcode, z.name
FROM (
  SELECT employeekey, min(technumber) AS technumber, sum(clock) AS clock, 
    sum(flag) AS flag, sum(other) AS other, sum(training) AS training
  FROM (
      SELECT a.thedate, b.employeekey, technumber, coalesce(b.flaghours, 0) AS flag, 0 AS clock, 0 AS Training, 0 AS Other
      FROM day a
      LEFT JOIN rptFlaghours b ON a.thedate = b.thedate
      WHERE a.thedate BETWEEN '07/01/2012' AND '07/14/2012'
      UNION ALL -- adj hours
      SELECT a.thedate, b.employeekey, 'NA', 0 as flag, 0 as clock, 
        coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training, 
        coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other
      FROM day a
      LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
      WHERE a.thedate BETWEEN '07/01/2012' AND '07/14/2012'
      GROUP BY a.thedate, b.employeekey
      UNION ALL -- clock hours
      SELECT a.thedate, b.employeekey, 'NA', 0 as flag, coalesce(b.clockhours, 0) AS clock, 0 AS training, 0 AS other
      FROM day a
      LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
      WHERE a.thedate BETWEEN '07/01/2012' AND '07/14/2012') x
  GROUP BY employeekey) y  
LEFT JOIN edwEmployeeDim z ON y.employeekey = z.employeekey 
WHERE y.technumber <> 'NA'
ORDER BY y.technumber  
-- so now DO this for monthly totals AND check against holygraille.xls
-- wait a minute, why no rows FROM technumber in ('619', '620'--, '135')
-- 8/14, GROUPing BY employeekey, dept techs have a -1 for empkey
that''s the flaw, flaghours
so IF i GROUP BY empkey, that leaves out the dept techs
IF i GROUP BY tech# i leave out clock/adj hours
so, replace edwClockHoursFact with rptClockHours
ADD a JOIN to stgTechClockTimeAdj
can''tt GROUP BY technumber
ry1 135 gil
ry3 135 b stinar
so need storecode
think this IS it
SELECT * FROM (
      SELECT b.storecode, a.thedate, b.employeekey, technumber, 
        coalesce(b.flaghours, 0) AS flag, 0 AS clock, 0 AS Training, 0 AS Other
      FROM day a
      LEFT JOIN rptFlaghours b ON a.thedate = b.thedate
      WHERE a.thedate BETWEEN '07/01/2012' AND '07/14/2012'
      UNION ALL -- adj hours
      SELECT d.storecode, a.thedate, b.employeekey, d.technumber, 0 as flag, 0 as clock, 
        coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training, 
        coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other
      FROM day a
      LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
      LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
      LEFT JOIN dimtech d ON c.employeenumber = d.employeenumber
      WHERE a.thedate BETWEEN '07/01/2012' AND '07/14/2012'
      GROUP BY d.storecode, a.thedate, b.employeekey, d.technumber
      UNION ALL -- clock hours
      SELECT b.storecode, a.thedate, b.employeekey, technumber, 0 as flag, 
        coalesce(b.clockhours, 0) AS clock, 0 AS training, 0 AS other
      FROM day a
      LEFT JOIN rptClockHours b ON a.datekey = b.datekey
      WHERE a.thedate BETWEEN '07/01/2012' AND '07/14/2012') X
WHERE technumber = '135'
  AND storecode = 'ry1'

-- wow, even with ALL dates, this runs LIKE instantly, 386709 rows 
-- shit, i sure hope it's right
-- bunch of training/other hours are off, for at least march
-- fuck, lemer, month of march totals shows 32.5 training
-- 26 of it was IN the pay period 3/25 - 4/7
-- i put that 26 hours ON the last day of the payperiod (apr 7)
-- bev shows it IN the march totals
-- so i have to go back thru ALL the pay periods that cross months AND sort the shit out
-- i don't even know what that fucking looks like
SELECT y.yearmonth, x.technumber, SUM(clock) AS clock, SUM(flag) AS flag, 
  SUM(other) AS other, SUM(training) AS training
FROM (
      SELECT b.storecode, a.thedate, b.employeekey, technumber, 
        coalesce(b.flaghours, 0) AS flag, 0 AS clock, 0 AS Training, 0 AS Other
      FROM day a
      LEFT JOIN rptFlaghours b ON a.thedate = b.thedate
      WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
      UNION ALL -- adj hours
      SELECT d.storecode, a.thedate, b.employeekey, d.technumber, 0 as flag, 0 as clock, 
        coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training, 
        coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other
      FROM day a
      LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
      LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
      LEFT JOIN dimtech d ON c.employeenumber = d.employeenumber
      WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1
      GROUP BY d.storecode, a.thedate, b.employeekey, d.technumber
      UNION ALL -- clock hours
      SELECT b.storecode, a.thedate, b.employeekey, technumber, 0 as flag, 
        coalesce(b.clockhours, 0) AS clock, 0 AS training, 0 AS other
      FROM day a
      LEFT JOIN rptClockHours b ON a.datekey = b.datekey
      WHERE a.thedate BETWEEN '11/01/2011' AND curdate() - 1) x
LEFT JOIN day y ON x.thedate = y.thedate      
WHERE x.technumber <> 'NA'
  AND x.technumber > '500' 
  AND x.storecode = 'ry1' 
  AND y.yearmonth >= 201203
GROUP BY y.yearmonth, x.technumber  
ORDER BY x.technumber, y.yearmonth 

-- working out the fucking overlap distribution
SELECT a.yearmonth, c.employeekey, d.technumber, 
  coalesce(sum(CASE WHEN reason <> 'Training' THEN b.hours END), 0) AS Other,
  coalesce(sum(CASE WHEN reason = 'Training' THEN b.hours END), 0) AS Training 
FROM day a
LEFT JOIN stgTechClockTimeAdjustments b ON a.thedate = b.thedate
LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
LEFT JOIN dimtech d ON c.employeenumber = d.employeenumber
WHERE a.yearmonth = 201206
  AND d.technumber <> 'NA'
  AND d.technumber > '501'
GROUP BY a.yearmonth, c.employeekey, d.technumber
ORDER BY d.technumber

SELECT *
FROM stgTechClockTimeAdjustments
WHERE employeekey = 990 -- 4/10  
  AND reason = 'Training'
ORDER BY thedate

INSERT INTO stgTechClockTimeAdjustments values(
  '06/30/2012', 1249, 1, 'Training')
INSERT INTO stgTechClockTimeAdjustments values(
  '04/08/2012', 1494, 7.5, 'Training')  

SELECT *        
SELECT *
FROM stgTechClockTimeAdjustments
WHERE employeekey = 1494
  AND reason = 'Training'
  AND thedate BETWEEN '03/01/2012' AND '03/31/2012'
UNION all  
SELECT *
FROM stgTechClockTimeAdjustments
WHERE employeekey = 1494
  AND reason = 'Training'
  AND thedate BETWEEN '03/25/2012' AND '04/07/2012'


/*
so, now, WHERE i "want" to go IS to also make sure edwClockHoursFact AND rptClockHours reconcile
done 8/13
-- good
SELECT a.employeekey, a.datekey, a.clockhours
FROM edwClockHoursFact a
LEFT JOIN rptClockHours b ON a.employeekey = b.employeekey
  AND a.datekey = b.datekey
WHERE a.clockhours <> b.clockhours  
-- NOT good
what a suprise, these are ALL tim paige
they are the wrong employeekey for a period of time
SELECT a.employeekey, a.datekey, a.clockhours
FROM edwClockHoursFact a
LEFT JOIN rptClockHours b ON a.employeekey = b.employeekey
  AND a.datekey = b.datekey  
WHERE b.datekey IS NULL 
  AND a.clockhours <> 0  
-- this fixes them
UPDATE edwClockHoursFact 
SET employeekey = 1575
--SELECT * FROM edwClockHoursFact 
WHERE employeekey = 1665
AND datekey IN (SELECT datekey FROM day WHERE thedate BETWEEN '04/26/2012' AND '07/02/2012')
-- but now need to ADD the fixed records to rptClockHours

SELECT MIN(c.thedate), MAX(c.thedate)
FROM edwClockHoursFact a
LEFT JOIN rptClockHours b ON a.employeekey = b.employeekey
  AND a.datekey = b.datekey  
LEFT JOIN day c ON a.datekey = c.datekey  
WHERE b.datekey IS NULL 
  AND a.clockhours <> 0  
 
INSERT INTO rptClockHours
SELECT b.thedate, b.datekey, c.storecode, a.employeekey, c.employeenumber, c.name, c.pydept, 
  c.pydeptcode, c.distcode, distribution, coalesce(d.technumber, 'NA') AS technumber, clockhours
FROM edwClockHoursFact a
INNER JOIN day b ON a.datekey = b.datekey
LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
LEFT JOIN dimtech d ON c.storecode = d.storecode
  AND c.employeenumber = d.employeenumber  
WHERE a.employeekey = 1575
  AND thedate BETWEEN '06/04/2012' AND '06/30/2012'
-- ok    
SELECT employeekey, datekey FROM rptclockhours GROUP BY employeekey, datekey HAVING COUNT(*) > 1   
-- AND ok
SELECT a.employeekey, a.datekey, a.clockhours
FROM edwClockHoursFact a
LEFT JOIN rptClockHours b ON a.employeekey = b.employeekey
  AND a.datekey = b.datekey  
WHERE b.datekey IS NULL 
  AND a.clockhours <> 0 
-- AND ok  
SELECT a.employeekey, a.datekey, a.clockhours
FROM rptClockhours a
LEFT JOIN edwClockHoursFact b ON a.employeekey = b.employeekey
  AND a.datekey = b.datekey  
WHERE b.datekey IS NULL  
*/ 
  
 
/*
-- best
SELECT b.technumber, a.employeekey, a.name, a.employeekeyfromdate,
  a.employeekeythrudate
FROM vempdim a
INNER JOIN dimtech b ON a.employeenumber = b.employeenumber
WHERE a.storecode = 'ry1'
  AND a.pydept = 'service'
  AND a.distcode = 'stec'
  AND (
    '08/12/2012' between a.employeekeyfromdate AND a.employeekeythrudate
    OR
    '08/25/2012' between a.employeekeyfromdate AND a.employeekeythrudate)
ORDER BY a.name    
--ORDER BY employeekey
ORDER BY b.technumber 
*/ 
 
-- 8/16
-- for comparison to holy graille
DECLARE @d1 date;
DECLARE @d2 date;
DECLARE @yearmonth integer;
@d1 = '04/07/2013';
@d2 = '04/20/2013';
--@yearmonth = 201208;
SELECT n.employeekey, n.name, n.technumber, round(SUM(clockhours), 2) AS clockhours,
  round(SUM(regularhours), 2) AS reghours, SUM(overtimehours) AS othours, 
--  SUM(vacationhours) AS vachours, SUM(ptohours) AS ptohours, SUM(holidayhours) AS holidayhours,
  SUM(coalesce(Training, 0)) AS traininghours, SUM(coalesce(other, 0)) AS otherhours
FROM (
  select a.thedate, a.yearmonth, a.dayname, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.Training, c.Other
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  LEFT JOIN (
    SELECT thedate, Employeekey, 
      coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
      coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
    FROM stgTechClockTimeAdjustments 
    GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
    AND b.employeekey = c.employeekey
  WHERE a.thedate BETWEEN @d1 AND @d2) m  
LEFT JOIN (
  SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
    f.employeekeythrudate, e.technumber
  FROM edwEmployeeDim f
  LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
  WHERE f.pydept = 'service'
    AND f.distcode = 'stec'
    AND e.technumber IS NOT NULL) n ON m.thedate BETWEEN n.employeekeyfromdate AND n.employeekeythrudate
  AND m.employeekey = n.employeekey
WHERE n.name IS NOT NULL  
--  AND m.yearmonth = @yearmonth
GROUP BY n.employeekey, n.name, n.technumber     
UNION
SELECT 666, 'zMONTH' AS name, 'ALL' AS technumber, round(SUM(clockhours), 2) AS clockhours,
  round(SUM(regularhours), 2) AS reghours, SUM(overtimehours) AS othours, 
--  SUM(vacationhours) AS vachours, SUM(ptohours) AS ptohours, SUM(holidayhours) AS holidayhours,
  SUM(coalesce(Training, 0)) AS traininghours, SUM(coalesce(other, 0)) AS otherhours
FROM (
  select a.thedate, a.yearmonth, a.dayname, a.BiWeeklyPayPeriodStartDate, a.BiWeeklyPayPeriodEndDate, b.*, c.Training, c.Other
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  LEFT JOIN (
    SELECT thedate, Employeekey, 
      coalesce(SUM(CASE WHEN reason = 'Training' THEN coalesce(hours, 0) END), 0) AS Training,
      coalesce(SUM(CASE WHEN reason <> 'Training' THEN coalesce(hours, 0) END), 0) AS Other
    FROM stgTechClockTimeAdjustments 
    GROUP BY thedate, Employeekey) c ON a.thedate = c.thedate
    AND b.employeekey = c.employeekey
  WHERE a.thedate BETWEEN @d1 AND @d2) m  
LEFT JOIN (
  SELECT f.name, f.employeenumber, f.employeekey, f.employeekeyfromdate, 
    f.employeekeythrudate, e.technumber
  FROM edwEmployeeDim f
  LEFT JOIN dimtech e ON f.employeenumber = e.employeenumber
  WHERE f.pydept = 'service'
    AND f.distcode = 'stec'
    AND e.technumber IS NOT NULL) n ON m.thedate BETWEEN n.employeekeyfromdate AND n.employeekeythrudate
  AND m.employeekey = n.employeekey
WHERE n.name IS NOT NULL  
--  AND m.yearmonth = @yearmonth
ORDER BY name    
  
-- 9/17, fair approximation of holy graille
/*

SELECT a.*, prodmtd, prodpptd, prodprev14 
FROM (
SELECT '9/9 - 9/22', a.name, SUM(a.clock) AS clock, SUM(a.flag) AS flag, SUM(a.other) AS other,
  SUM(a.training) AS training
FROM rptry1techprod a
WHERE a.thedate BETWEEN '09/09/2012' AND curdate()
GROUP BY a.name) a
LEFT JOIN (
  SELECT name, prodmtd, prodpptd, prodprev14
  FROM rptry1techprod
  WHERE thedate = '09/16/2012') b ON a.name = b.name  
*/    

