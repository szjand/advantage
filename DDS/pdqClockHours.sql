-- too slow
SELECT a.thedate, b.name, b.pydept, b.distcode, c.*
FROM day a
LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
LEFT JOIN edwClockHoursFact c ON b.employeekey = c.employeekey
  AND (SELECT thedate FROM day WHERE datekey = c.datekey) = a.thedate
WHERE a.thedate between curdate() - 3 AND curdate()
  AND b.Active = 'Active'
  AND b.pydept = 'PDQ'
  AND b.storecode = 'ry1'
  AND b.distcode IN ('PTEC','PDQW')
  
SELECT *
FROM edwClockHoursFact
WHERE datekey IN (
  SELECT datekey
  FROM day
  WHERE thedate between curdate() - 3 AND curdate())

-- much better 
SELECT a.thedate, b.name, b.pydept, b.distcode, c.clockhours 
FROM day a
LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
LEFT JOIN (
    SELECT *
    FROM edwClockHoursFact
    WHERE datekey IN (
      SELECT datekey
      FROM day
      WHERE thedate between curdate() - 3 AND curdate())) c ON b.employeekey = c.employeekey
  AND a.datekey = c.datekey
WHERE a.thedate between curdate() - 3 AND curdate() - 1
  AND b.Active = 'Active'
  AND b.pydept = 'PDQ'
  AND b.storecode = 'ry1'
  AND b.distcode IN ('PTEC','PDQW')  
  

SELECT thedate, distcode, SUM(clockhours)
FROM (  
  SELECT a.thedate, b.name, b.pydept, b.distcode, c.clockhours 
  FROM day a
  LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  LEFT JOIN (
    SELECT employeekey, datekey, clockhours
    FROM edwClockHoursFact
    WHERE datekey IN (
      SELECT datekey
      FROM day
      WHERE thedate between curdate() - 3 AND curdate() - 1)
    UNION
    SELECT *
    FROM factClockHoursToday) c ON b.employeekey = c.employeekey
      AND a.datekey = c.datekey
  WHERE a.thedate between curdate() - 3 AND curdate() 
    AND b.Active = 'Active'
    AND b.pydept = 'PDQ'
    AND b.storecode = 'ry1'
    AND b.distcode IN ('PTEC','PDQW')    
    AND clockhours <> 0) x
GROUP BY thedate, distcode    
  
  
SELECT a.thedate, c.name, c.pydept, c.distcode, b.clockhours
FROM day a
LEFT JOIN (
  SELECT employeekey, datekey, clockhours
  FROM edwClockHoursFact
  WHERE datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate between curdate() - 3 AND curdate() - 1)
  UNION ALL
  SELECT *
  FROM factClockHoursToday) b ON a.datekey = b.datekey
LEFT JOIN edwEmployeeDim c ON b.employeekey = c.employeekey  
WHERE a.thedate between curdate() - 3 AND curdate()   
  AND EXISTS (
    SELECT 1
    FROM edwEmployeeDim
    WHERE employeekey = b.employeekey
      AND pydept = 'PDQ'
      AND storecode = 'ry1'
      AND distcode IN ('PTEC','PDQW'))   
  AND clockhours <> 0      
ORDER BY thedate, name  