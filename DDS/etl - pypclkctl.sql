SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'double,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'double,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'pypclkctl' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'pypclkctl'  
AND data_type <> 'CHAR'


CREATE TABLE stgArkonaPYPCLKCTL (
yicco#     cichar(3),          
yicdept    cichar(2),          
yicdesc    cichar(30),         
yicdsw     cichar(1),          
yicsort    cichar(1),          
yicapp1    cichar(2),          
yicapp2    cichar(2),          
yicapp3    cichar(2),          
yicapp4    cichar(2)) IN database;            

-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PYPCLKCTL';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = coalesce((SELECT '[' + TRIM(column_name) + '], '  FROM #pymast WHERE ordinal_position = @j), '');
-- just COLUMN names
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
-- parameter names  
--  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM #pymast WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PYPCLKCTL';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPYPCLKCTL';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;

/*************** 2/15 *********************************************************/
-- modify to accomodate type2 changes
ALTER TABLE stgArkonaPYPCLKCTL
ADD COLUMN CurrentRow logical
ADD COLUMN RowChangeDate date
ADD COLUMN RowChangeDateKey integer
ADD COLUMN RowFromTS timestamp
ADD COLUMN RowThruTS timestamp
ADD COLUMN RowChangeReason cichar(100);

UPDATE stgArkonaPYPCLKCTL
SET CurrentRow = true;

UPDATE stgPYPCLKCTL with current values
UPDATE edwEmployeeDim AS requd
clean up resArkonaCodes, no longer need dept info IN that table
DELETE FROM resarkonaCodes WHERE arkColumnName = 'YICDEPT';
DROP PROCEDURE ChangesInPYPCLKCTL
/*** 2/18 ON local ***/

run exe

-- current state of Dept/Desc IN edwEmployeeDim 
  SELECT storecode, pyDeptCode, PyDeptCodeDesc
  FROM edwEmployeeDim 
  WHERE pyDeptCode <> ''
  GROUP BY storecode, pyDeptCode, PyDeptCodeDesc

-- any store/dept IN stg NOT IN edw   
SELECT *
FROM stgArkonaPYPCLKCTL p
LEFT JOIN (
    SELECT storecode, pyDeptCode, PyDeptCodeDesc
    FROM edwEmployeeDim 
    WHERE pyDeptCode <> ''
    GROUP BY storecode, pyDeptCode, PyDeptCodeDesc) e ON p.yicco# = e.storecode
  AND p.yicdept = e.pydeptcode
WHERE e.storecode IS NULL  

-- AND vice versa
SELECT *
FROM (
  SELECT storecode, pyDeptCode, PyDeptCodeDesc
  FROM edwEmployeeDim 
  WHERE pyDeptCode <> ''
  GROUP BY storecode, pyDeptCode, PyDeptCodeDesc) e 
LEFT JOIN stgArkonaPYPCLKCTL p ON e.storecode = p.yicco#
  AND e.pydeptcode = p.yicdept
WHERE p.yicco# IS NULL     
  
-- mismatched descriptions  
select *
FROM (
  SELECT storecode, pyDeptCode, PyDeptCodeDesc
  FROM edwEmployeeDim 
  WHERE pyDeptCode <> ''
  GROUP BY storecode, pyDeptCode, PyDeptCodeDesc) e
LEFT JOIN tmpPYPCLKCTL p ON e.storecode = p.yicco#
  AND e.pyDeptCode = p.yicdept
WHERE e.pyDeptCodeDesc <> p.yicdesc

UPDATE edwEmployeeDim
SET pyDeptCodeDesc = x.yicdesc
FROM stgArkonaPYPCLKCTL x
WHERE storecode = x.yicco#
  AND pyDeptCode = x.yicdept 

-- unique store, depcode
SELECT storecode, pydeptcode
FROM (
  SELECT storecode, pydeptcode, pydeptcodedesc
  FROM edwEmployeeDim
  WHERE pydeptcode <> ''
  GROUP BY storecode, pydeptcode, pydeptcodedesc) x 
GROUP BY storecode, pydeptcode
HAVING COUNT(*) > 1
  


/*************** 2/15 *********************************************************/

-- replace sp.ChangesInPYPCLKCTL with sp.stgArkonaPYPCLKCTL

-- 1. Check Constraint ON tmpPYPCLCKCTL
IF ( -- 1 row per yicco#, yicdesc
  SELECT COUNT(*)
  FROM (
    SELECT yicco#, yicdept
    FROM tmpPYPCLKCTL
    GROUP BY yicco#, yicdept
    HAVING COUNT(*) > 1) x) > 0 THEN
  RAISE SPstgArkPYPCLKCTL(100, 'Constraint Fail');
END IF;

BEGIN TRANSACTION;
TRY
-- 2. new records
  TRY 
    INSERT INTO stgArkonaPYPCLKCTL (yicco#, yicdept, yicdesc, yicdsw, yicsort, yicapp1,
      yicapp2, yicapp3, yicapp4, CurrentRow)
    SELECT t.yicco#, t.yicdept, t.yicdesc, t.yicdsw, t.yicsort, t.yicapp1, 
      t.yicapp2, t.yicapp3, t.yicapp4, true
    FROM tmpPYPCLKCTL t
    LEFT JOIN stgArkonaPYPCLKCTL p ON t.yicco# = p.yicco#
      AND t.yicdept = p.yicdept
    WHERE p.yicco# IS NULL;
  CATCH ALL
    RAISE SPstgArkPYPCLKCTL(200, 'New Records');
  END TRY;
-- 3. type 2 changes: yicdesc  
  TRY
    SELECT Utilities.DropTablesIfExist('#Type2') FROM system.iota;
    SELECT p.yicco#, p.yicdept, 'Description' AS RowChangeReason, t.*
    INTO #type2
    FROM tmpPYPCLKCTL t
    LEFT JOIN stgArkonaPYPCLKCTL p ON t.yicco# = p.yicco#
      AND t.yicdept = p.yicdept
      AND p.CurrentRow = true 
    WHERE t.yicdesc <> p.yicdesc;
  CATCH ALL
    RAISE SPstgArkPYPCLKCTL(300, 'generate #Type2');
  END TRY;
  TRY
    IF (SELECT COUNT(*) FROM #Type2) > 0 THEN
  -- UPDATE the old record
    UPDATE stgArkonaPYPCLKCTL 
      SET RowChangeDate = CAST(now()/*@NowTS*/ AS sql_date),
          RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = CAST(now()/*@NowTS*/ AS sql_date)),
          RowThruTS = now()/*@NowTS*/,
          CurrentRow = false,
          RowChangeReason = x.RowChangeReason
      FROM #Type2 x
      LEFT JOIN stgArkonaPYPCLKCTL p ON x.yicco# = p.yicco#
        AND x.yicdept = p.yicdept
        AND p.CurrentRow = true;
  -- INSERT new record
      INSERT INTO stgArkonaPYPCLKCTL (yicco#, yicdept, yicdesc, yicdsw, yicsort, yicapp1,
        yicapp2, yicapp3, yicapp4, RowFromTS, CurrentRow)
      SELECT yicco#, yicdept, yicdesc, yicdsw, yicsort, yicapp1,
        yicapp2, yicapp3, yicapp4, now()/*@NowTS*/ , True
      FROM #Type2;
    END IF;
  CATCH ALL
    RAISE SPstgArkPYPCLKCTL(400, 'Type 2 updates');
  END TRY;
  IF ( -- 1 Current row per yicco#, yicdept
    SELECT COUNT(*)
    FROM (
      SELECT yicco#, yicdept
      FROM stgArkonaPYPCLKCTL
      WHERE CurrentRow = true
      GROUP BY yicco#, yicdept
        HAVING COUNT(*) > 1) x) > 0 THEN
    RAISE SPstgArkPYPCLKCTL(500, 'stgArkonaPYPCLKCTL constraint check');
COMMIT WORK;
CATCH ALL
  RAISE SPstgArkonaPYPCLKCTL(666, __errtext);
END TRY;

  
/************ 2/18 looks ok, TRY it out ***********************************/