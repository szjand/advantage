-- Table Type of DependencyObjects  is ADT
drop Table DependencyObjects (
   dExecutable CIChar( 50 ),
   dProcedure CIChar( 50 ),
   dObject CIChar( 50 ) );

EXECUTE PROCEDURE sp_CreateIndex90( 'DependencyObjects ', 'DependencyObjects .adi', 'PK', 'dExecutable;dProcedure;dObject', '', 2051, 512, NULL );

INSERT INTO "DependencyObjects " VALUES( 'ServiceLine', 'SDPRHDR', 'tmpSDPRHDR' );
INSERT INTO "DependencyObjects " VALUES( 'ServiceLine', 'SDPRDET', 'tmpSDPRDET' );
INSERT INTO "DependencyObjects " VALUES( 'ServiceLine', 'PDPPHDR', 'tmpPDPPHDR' );
INSERT INTO "DependencyObjects " VALUES( 'ServiceLine', 'PDPPDET', 'tmpPDPPDET' );
INSERT INTO "DependencyObjects " VALUES( 'ServiceLine', 'stgRO', 'xfmRO' );

-- Table Type of Dependencies is ADT
drop Table Dependencies(
   dObjExecutable CIChar( 50 ),
   dObjProcedure CIChar( 50 ),
   dObjObject CIChar( 50 ),
   DependsOnExecutable CIChar( 50 ),
   DependsOnProcedure CIChar( 50 ),
   DependsOnObject CIChar( 50 ) );

EXECUTE PROCEDURE sp_CreateIndex90( 'Dependencies', 'Dependencies.adi', 'PK', 'dObjExecutable;dObjProcedure;dObjObject;DependsOnExecutable;DependsOnProcedure;DependsOnObject', '', 2051, 1024, NULL );
EXECUTE PROCEDURE sp_CreateIndex90( 'Dependencies', 'Dependencies.adi', 'FK1', 'dObjExecutable;dObjProcedure;dObjObject', '', 2, 1024, NULL );
EXECUTE PROCEDURE sp_CreateIndex90( 'Dependencies', 'Dependencies.adi', 'FK2', 'DependsOnExecutable;DependsOnProcedure;DependsOnObject', '', 2, 1024, NULL );

INSERT INTO "Dependencies" VALUES( 'ServiceLine', 'stgRO', 'xfmRO', 'ServiceLine', 'SDPRHDR', 'tmpSDPRHDR' );
INSERT INTO "Dependencies" VALUES( 'ServiceLine', 'stgRO', 'xfmRO', 'ServiceLine', 'SDPRDET', 'tmpSDPRDET' );
INSERT INTO "Dependencies" VALUES( 'ServiceLine', 'stgRO', 'xfmRO', 'ServiceLine', 'PDPPHDR', 'tmpPDPPHDR' );
INSERT INTO "Dependencies" VALUES( 'ServiceLine', 'stgRO', 'xfmRO', 'ServiceLine', 'PDPPDET', 'tmpPDPPDET' );
