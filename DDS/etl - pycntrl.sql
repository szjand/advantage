-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'integer,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'double,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'PYCNTRL' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'PYCNTRL'  
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'PYCNTRL'  
AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaPYCNTRL
-- 3. revise DDL based ON step 2, CREATE table
CREATE TABLE stgArkonaPYCNTRL (
yco#       cichar(3),          
ycyy       integer,            
ypdte      integer,            
yckdte     integer,            
ychk#      integer,            
yfed#      cichar(10),         
yficl      integer,             
yficep     double,             
yficmp     double,             
yficea     cichar(10),         
yficma     cichar(10),         
ymedl      integer,             
ymedep     double,             
ymedmp     double,             
ymedea     cichar(10),         
ymedma     cichar(10),         
yfucl      integer,             
yfucp      double,             
yfucma     cichar(10),         
ybank      cichar(10),         
ypayac     cichar(10),         
yfxact     cichar(10),         
yretac     cichar(10),         
yarac      cichar(10),         
yavac      cichar(10),         
yahol      cichar(10),         
yasck      cichar(10),         
yvacr      integer,             
yholr      integer,             
ysckr      integer,             
yvacop     cichar(1),          
yholop     cichar(1),          
ysckop     cichar(1),          
yvacac     cichar(1),          
yholac     cichar(1),          
ysckac     cichar(1),          
yvacsh     cichar(1),          
yholsh     cichar(1),          
yscksh     cichar(1),          
yvaccd     cichar(3),          
yholcd     cichar(3),          
ysckcd     cichar(3),          
yvacdm     integer,             
yholdm     integer,             
ysckdm     integer,             
yvacmx     integer,             
yholmx     integer,             
ysckmx     integer,             
yretp      double,             
yretap     double,             
y401k      cichar(3),          
ycafcd     cichar(3),          
ypayyr     integer,             
yglyr      integer,             
yglmo      integer,             
yqtrup     integer,             
ypnck      cichar(1),          
yhrday     double,             
ydweek     integer,             
yasrc      cichar(1),          
yretl      integer,             
ymsid      cichar(1),          
yretmp     double,             
yretmy     cichar(1),          
yhldsw     cichar(1),          
yretag     cichar(1),          
yholda     cichar(10),         
ygllib     cichar(10),         
yglpgm     cichar(10),         
yglfil     cichar(10),         
yaltpo     cichar(1),          
ypnum      integer,             
yflag0     cichar(1),          
yflag1     cichar(1),          
yflag2     cichar(1),          
yflag3     cichar(1),          
yflag4     cichar(1),          
yflag5     cichar(1),          
yflag6     cichar(1),          
yflag7     cichar(1),          
yflag8     cichar(1),          
yflag9     cichar(1),          
yglddc     cichar(10),         
yretmp1    double,             
yretmp2    double,             
yretp1     double,             
yretp2     double,             
ysckcs     cichar(1),          
yvaccs     cichar(1),          
y401l      integer,             
y401s      integer) IN database;     

CREATE TABLE tmpPYCNTRL (
yco#       cichar(3),          
ycyy       integer,            
ypdte      integer,            
yckdte     integer,            
ychk#      integer,            
yfed#      cichar(10),         
yficl      integer,             
yficep     double,             
yficmp     double,             
yficea     cichar(10),         
yficma     cichar(10),         
ymedl      integer,             
ymedep     double,             
ymedmp     double,             
ymedea     cichar(10),         
ymedma     cichar(10),         
yfucl      integer,             
yfucp      double,             
yfucma     cichar(10),         
ybank      cichar(10),         
ypayac     cichar(10),         
yfxact     cichar(10),         
yretac     cichar(10),         
yarac      cichar(10),         
yavac      cichar(10),         
yahol      cichar(10),         
yasck      cichar(10),         
yvacr      integer,             
yholr      integer,             
ysckr      integer,             
yvacop     cichar(1),          
yholop     cichar(1),          
ysckop     cichar(1),          
yvacac     cichar(1),          
yholac     cichar(1),          
ysckac     cichar(1),          
yvacsh     cichar(1),          
yholsh     cichar(1),          
yscksh     cichar(1),          
yvaccd     cichar(3),          
yholcd     cichar(3),          
ysckcd     cichar(3),          
yvacdm     integer,             
yholdm     integer,             
ysckdm     integer,             
yvacmx     integer,             
yholmx     integer,             
ysckmx     integer,             
yretp      double,             
yretap     double,             
y401k      cichar(3),          
ycafcd     cichar(3),          
ypayyr     integer,             
yglyr      integer,             
yglmo      integer,             
yqtrup     integer,             
ypnck      cichar(1),          
yhrday     double,             
ydweek     integer,             
yasrc      cichar(1),          
yretl      integer,             
ymsid      cichar(1),          
yretmp     double,             
yretmy     cichar(1),          
yhldsw     cichar(1),          
yretag     cichar(1),          
yholda     cichar(10),         
ygllib     cichar(10),         
yglpgm     cichar(10),         
yglfil     cichar(10),         
yaltpo     cichar(1),          
ypnum      integer,             
yflag0     cichar(1),          
yflag1     cichar(1),          
yflag2     cichar(1),          
yflag3     cichar(1),          
yflag4     cichar(1),          
yflag5     cichar(1),          
yflag6     cichar(1),          
yflag7     cichar(1),          
yflag8     cichar(1),          
yflag9     cichar(1),          
yglddc     cichar(10),         
yretmp1    double,             
yretmp2    double,             
yretp1     double,             
yretp2     double,             
ysckcs     cichar(1),          
yvaccs     cichar(1),          
y401l      integer,             
y401s      integer) IN database;        

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PYCNTRL';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PYCNTRL';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPYCNTRL';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
