SELECT sum(hourlyrate * 1.5 * otHours)
FROM (
SELECT c.storecode, c.pydeptcode, c.pydept, c.name, c.hourlyrate, sum(a.regularhours) AS regHours, sum(a.overtimehours) AS otHours
FROM edwClockHoursFact a
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
WHERE b.thedate BETWEEN '02/09/2014' AND '02/22/2014'
  AND a.clockhours > 0
  AND payrollclasscode = 'H'
GROUP BY c.storecode, c.pydeptcode, c.pydept, c.name, c.hourlyrate) x
ORDER BY otHours desc