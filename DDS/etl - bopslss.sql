-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	  WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
  	WHEN data_type = 'numeric' THEN 'integer,'
  	WHEN data_type = 'timestmp' THEN 'timestamp,'
  	WHEN data_type = 'smallint' THEN 'integer,'
  	WHEN data_type = 'integer' THEN 'double,'
  	WHEN data_type = 'decimal' THEN 'double,'
  	WHEN data_type = 'date' THEN 'date,'
  	WHEN data_type = 'time' THEN 'cichar(8),'
  	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'BOPSLSS' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'BOPSLSS'  
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'BOPSLSS'  
AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaBOPSLSS
-- 3. revise DDL based ON step 2, CREATE table
CREATE TABLE stgArkonaBOPSLSS (
bqco#      cichar(3),          
bqrtyp     cichar(1),          
bqkey      integer,//             
bqspid     cichar(3),          
bqstat     cichar(1),          
bqdate     date,//             
bqname     cichar(20),         
bqtype     cichar(1),          
bqstyp     cichar(1),          
bqprnt     cichar(1),          
bqsplt     cichar(1),          
bqunit     cichar(1),          
bqucnt     integer,//             
bqcgrs     money,//             
bqngrs     money,//             
bqgcrt     double,             
bqpcrt     double,             
bqtcrt     double,             
bqhcrt     double,             
bqdcrt     double,             
bqrcrt     double,             
bqicrt     double,             
bqscrt     double,             
bqacrt     double,             
bqncrt     double, //            
bqgbon     double,             
bqpbon     double,             
bqtbon     double,             
bqhbon     double,             
bqdbon     double,             
bqrbon     double,             
bqibon     double,             
bqsbon     double,             
bqabon     double,             
bqnbon     double,             
bqgmin     double,             
bqpmin     double,             
bqtmin     double,             
bqhmin     double,             
bqdmin     double,             
bqrmin     double,             
bqimin     double,             
bqsmin     double,             
bqamin     double,             
bqnmin     double,             
bqgmino    double,             
bqpmino    double,             
bqtmino    double,             
bqhmino    double,             
bqdmino    double,             
bqrmino    double,             
bqimino    double,             
bqsmino    double,             
bqamino    double,             
bqnmino    double,             
bqgcom     double,             
bqpcom     double,             
bqtcom     double,             
bqhcom     double,             
bqdcom     double,             
bqrcom     double,             
bqicom     double,             
bqscom     double,             
bqacom     double,             
bqncom     double,             
bqtotcom   double,             
bqaccrt    double,             
bqacbon    double,             
bqacmin    double,             
bqacmino   double) IN database;     

CREATE TABLE tmpBOPSLSS (
) IN database;        

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'BOPSLSS';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'BOPSLSS';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaBOPSLSS';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
 
