
/*
-- reqd tests: 
  zBGTTotal 1 ro per line
 s MAX techs per line
*/
-- get the base data FROM tmpPHDR/SHDR/PDET/SDET
-- AND stick it IN zBTG1
DELETE FROM zbtg1;  
INSERT INTO zBTG1 
-- thinking, DO pdet, the sdet WHERE NOT EXISTS
SELECT a.ptco#, a.ptdoc#, a.ptline, e.totalhours, a.seq, a.hrs, 
  CASE 
    WHEN a.ptco# = 'RY1' THEN
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
    WHEN a.ptco# = 'RY2' THEN 
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
  END AS TechKey 
FROM (
  SELECT b.ptco#, a.ptdoc#, b.ptline, b.pttech, MAX(b.ptseq#) AS seq, SUM(b.ptlhrs) AS hrs, MIN(b.ptdate) AS mindate
  FROM tmpPDPPHDR a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND a.ptchk# NOT LIKE 'v%'
    AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
    AND b.ptline < 900
    AND b.pttech <> ''
    AND b.ptlhrs > 0
  GROUP BY b.ptco#, a.ptdoc#, b.ptline, b.pttech) a
LEFT JOIN dimtech b ON a.pttech = b.technumber
  AND a.ptco# = b.storecode
  AND a.mindate BETWEEN b.techkeyfromdate AND b.techkeythrudate  
LEFT JOIN (
  SELECT storecode, TechKey 
  FROM dimTech
  WHERE technumber = 'UNK') c ON a.ptco# = c.storecode  
LEFT JOIN (    
  SELECT b.ptco#, a.ptdoc#, b.ptline, SUM(b.ptlhrs) AS totalhours
  FROM tmpPDPPHDR a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND a.ptchk# NOT LIKE 'v%'
    AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
    AND b.ptline < 900
    AND b.pttech <> ''
    AND b.ptlhrs > 0
  GROUP BY b.ptco#, a.ptdoc#, b.ptline) e ON a.ptco# = e.ptco# AND a.ptdoc# = e.ptdoc# AND a.ptline = e.ptline;
INSERT INTO zBTG1   
SELECT a.ptco#, a.ptro#, a.ptline, e.totalhours, a.seq, a.hrs, 
  CASE 
    WHEN a.ptco# = 'RY1' THEN
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
    WHEN a.ptco# = 'RY2' THEN 
      CASE WHEN b.TechKey IS NOT NULL THEN b.TechKey ELSE c.TechKey END 
  END AS TechKey 
FROM (
  SELECT ptco#, ptro#, ptline, pttech, max(ptseq#) AS seq, SUM(ptlhrs) AS hrs, MIN(ptdate) AS mindate
  FROM tmpSDPRDET a
  WHERE a.pttech <> ''
    AND a.ptlhrs > 0
    AND a.ptco# IN ('RY1','RY2')
  GROUP BY ptco#, ptro#, ptline, pttech) a
LEFT JOIN dimtech b ON a.pttech = b.technumber
  AND a.ptco# = b.storecode
  AND a.mindate BETWEEN b.techkeyfromdate AND b.techkeythrudate  
LEFT JOIN (
  SELECT storecode, TechKey 
  FROM dimTech
  WHERE technumber = 'UNK') c ON a.ptco# = c.storecode    
LEFT JOIN (  
  SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS totalhours
  FROM tmpSDPRDET a
  WHERE a.pttech <> ''
    AND a.ptlhrs > 0
    AND a.ptco# IN ('RY1','RY2')
  GROUP BY ptco#, ptro#, ptline) e ON a.ptco# = e.ptco# AND a.ptro# = e.ptro# AND a.ptline = e.ptline
WHERE NOT EXISTS (
  SELECT 1
  FROM zbtg1 
  WHERE ro = a.ptro#
    AND line = a.ptline);            
-- SELECT ro, line, techkey from zbtg1 GROUP BY ro, line, techkey HAVING COUNT(*) > 1

-- this IS a necessary test, current script only handles up to 4 techs per line
-- MAX IS 4 techs per line     
 
SELECT a.ro, a.line, COUNT(*) 
FROM zbtg1 a 
GROUP BY a.ro, a.line HAVING COUNT(*) > 4
ORDER BY COUNT(*) DESC 

-- zBGTTotal IS the holder of ALL data IN a single row per line
DROP TABLE zBTGTotal;
create TABLE zBTGTotal (
  TechGroupKey integer,
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  TotalHours double(4),
  tech1 integer,
  hours1 double(4),
  wf1 double(4),
  tech2 integer, 
  hours2 double(4),
  wf2 double(4),
  tech3 integer,
  hours3 double(4),
  wf3 double(4),
  tech4 integer,
  hours4 double(4),
  wf4 double(4)) IN database;

DROP TABLE zBTG2Techs;
CREATE TABLE zBTG2Techs (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  totalhours double(4),
  seq integer,
  hours double(4),
  techkey integer) IN database;
-- DELETE FROM zbtg2Techs;  
INSERT INTO zbtg2Techs
SELECT *
FROM zbtg1 a
WHERE EXISTS (
  SELECT 1
    FROM (
      SELECT storecode, ro, line
      FROM zbtg1 
      GROUP BY storecode, ro, line
      HAVING COUNT(*) = 2) z
    WHERE z.ro = a.ro
      AND z.line = a.line);  
INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1,tech2,hours2,wf2)
SELECT d.storecode, d.ro, d.line, d.totalhours, d.tech1, d.tech1hrs, 
  round(d.tech1hrs/d.totalhours,4) as wf1, e.tech2, e.tech2hrs, 
--  round(e.tech2hrs/d.totalhours, 4) AS wf2
-- since i am freaking about wf NOT adding up to 1
  1 - round(d.tech1hrs/d.totalhours,4) as wf2
FROM (
  SELECT *
  FROM (
    SELECT storecode, ro, line, totalhours,
      CASE 
        WHEN seq = (SELECT MIN(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN techkey 
      END AS tech1,
      CASE 
        WHEN seq = (SELECT MIN(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN hours
      END AS tech1hrs
    FROM zbtg2techs a) b
  WHERE tech1 IS NOT NULL) d    
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN seq = (SELECT max(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN techkey 
    END AS tech2,
    CASE 
      WHEN seq = (SELECT max(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN hours
    END AS tech2hrs  
  FROM zbtg2techs a) c
  WHERE tech2 IS NOT NULL) e ON d.storecode = e.storecode AND d.ro = e.ro AND d.line = e.line;   
/*  
--< goofy failure, this one has the same seq for each tech ON the same line -----    

SELECT ro, line FROM zbtgtotal GROUP BY ro,line HAVING COUNT(*) > 1

SELECT * FROM zbtgtotal WHERE ro = '16092180'

-- another way to detect
SELECT * FROM zbtgtotal WHERE tech1 = tech2
DELETE FROM zbtgtotal WHERE tech1 = tech2;
DELETE FROM zbtgtotal WHERE ro = '16092180' AND line = 3 AND tech1 = 51

SELECT * FROM zbtg1 WHERE ro = '16092180'
SELECT * FROM zbtg2techs WHERE ro = '16092180'

SELECT ro,line,techkey FROM zbtg2techs GROUP BY ro,line,techkey HAVING COUNT(*) > 1

SELECT * FROM stgarkonasdprdet WHERE ptro# = '16092180' AND ptline = 3
--/> goofy failure, this one has the same seq for each tech ON the same line ----- 

SELECT * FROM zbtgtotal    
SELECT ro, line, COUNT(*) FROM zbtgtotal GROUP BY ro,line HAVING COUNT(*) > 1
-- this should give me the unique techgroup groupings for each 2 tech combination
-- but of course, AS i have learned, does NOT, have to deal with the tech1=tech2 AND tech2=tech1 deal
SELECT tech1, tech2, wf1,wf2 FROM zbtgtotal GROUP by tech1, tech2, wf1,wf2
*/
-- 3 techs
-- delete FROM zbtgtotal WHERE tech3 IS NOT NULL AND tech4 is NULL  
-- 8/14 1/3 IS fucking with me, grouping on round 4 is returning separate rows for .3333 and .3334 (of course)
-- extract AS double(5) GROUP BY round 4 AND see how it goes
DROP TABLE zbtg3Techs;
CREATE TABLE zBTG3Techs (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  totalhours double(5),
  seq integer,
  hours double(4),
  techkey integer) IN database;
DELETE FROM zbtg3techs;  
INSERT INTO zbtg3Techs
SELECT *
FROM zbtg1 a
WHERE EXISTS (
  SELECT 1
    FROM (
      SELECT storecode, ro, line
      FROM zbtg1 
      GROUP BY storecode, ro, line
      HAVING COUNT(*) = 3) z
    WHERE z.ro = a.ro
      AND z.line = a.line);   
      
INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1,
  tech2,hours2,wf2,tech3,hours3,wf3) 
SELECT d.storecode, d.ro, d.line, d.totalhours, d.tech1, d.tech1hrs, 
  round(d.tech1hrs/d.totalhours,5) as wf1, e.tech2, e.tech2hrs, 
  round(e.tech2hrs/d.totalhours,5) AS wf2, f.tech3, f.tech3hrs,
-- since i am freaking about wf NOT adding up to 1  
--  round(f.tech3hrs/d.totalhours, 4) AS wf3
  round(1 - (round(d.tech1hrs/d.totalhours,5) + round(e.tech2hrs/d.totalhours, 5)),5) 
FROM (
  SELECT *
  FROM (
    SELECT storecode, ro, line, totalhours,
      CASE 
        WHEN seq = (SELECT MIN(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN techkey 
      END AS tech1,
      CASE 
        WHEN seq = (SELECT MIN(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN hours
      END AS tech1hrs
    FROM zbtg3techs a) b
  WHERE tech1 IS NOT NULL) d    
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN seq = (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN techkey 
    END AS tech2,
    CASE 
      WHEN seq = (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN hours
    END AS tech2hrs  
  FROM zbtg3techs a) c
  WHERE tech2 IS NOT NULL) e ON d.storecode = e.storecode AND d.ro = e.ro AND d.line = e.line
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN seq <> (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN 
        CASE 
          WHEN seq <> (SELECT min(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN techkey 
        END
    END AS tech3,
    CASE 
      WHEN seq <> (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN
        CASE
          WHEN seq <> (SELECT min(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN hours
        END
    END AS tech3hrs  
  FROM zbtg3techs a) d
  WHERE tech3 IS NOT NULL) f ON d.storecode = f.storecode AND d.ro = f.ro AND d.line = f.line;   
/*  
SELECT * FROM zbtgtotal    
SELECT a.* FROM zbtgtotal a
INNER JOIN (
SELECT ro, line FROM zbtgtotal GROUP BY ro,line HAVING COUNT(*) > 1) b ON a.ro = b.ro AND a.line = b.line
*/  


-- 4 techs, DO 2 groups of 2 GROUP 1: tech1, tech2, GROUP 2: tech3, tech4 
-- nope, assign a new seq via a CURSOR based script 
DROP TABLE zbtg4Techs;
CREATE TABLE zBTG4Techs (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  totalhours double(4),
  seq integer,
  hours double(4),
  techkey integer,
  newseq integer) IN database;
-- DELETE FROM zbtg4Techs; 
INSERT INTO zbtg4Techs
SELECT a.*, 0
FROM zbtg1 a
WHERE EXISTS (
  SELECT 1
    FROM (
      SELECT storecode, ro, line
      FROM zbtg1 
      GROUP BY storecode, ro, line
      HAVING COUNT(*) = 4) z
    WHERE z.ro = a.ro
      AND z.line = a.line);   

DECLARE @cur CURSOR as      
SELECT * from zbtg4techs WHERE ro = @ro AND line = @line;
DECLARE @rolinecur CURSOR AS
SELECT DISTINCT ro,line
FROM zbtg4techs;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @i integer;
DECLARE @seq integer;

OPEN @rolinecur;
TRY
  WHILE FETCH @rolinecur DO
    @ro = @rolinecur.ro;
    @line = @rolinecur.line;
    @i = 1;    
    OPEN @cur;
    TRY 
      WHILE FETCH @cur DO
        @seq = @cur.seq;
        UPDATE zbtg4techs
        SET newseq = @i
        WHERE seq = @seq;
        @i = @i + 1;
      END WHILE;
    FINALLY
      CLOSE @cur;
    END TRY;   
  END WHILE;
FINALLY 
  CLOSE @rolinecur;
END TRY;   

INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1,
  tech2,hours2,wf2,tech3,hours3,wf3, tech4,hours4,wf4) 
SELECT d.storecode, d.ro, d.line, d.totalhours, d.tech1, d.tech1hrs, 
  round(d.tech1hrs/d.totalhours,4) as wf1, e.tech2, e.tech2hrs, 
  round(e.tech2hrs/d.totalhours, 4) AS wf2, g.tech3, g.tech3hrs,
  round(g.tech3hrs/d.totalhours, 4) AS wf3, tech4, tech4hrs,
  1 - (round(tech3hrs/d.totalhours, 4) + round(tech2hrs/d.totalhours, 4) + round(tech1hrs/d.totalhours, 4)) AS wf4
FROM (
  SELECT *
  FROM (
    SELECT storecode, ro, line, totalhours,
      CASE 
        WHEN newseq = 1 THEN techkey 
      END AS tech1,
      CASE 
        WHEN newseq = 1 THEN hours
      END AS tech1hrs
    FROM zbtg4techs a) b
  WHERE tech1 IS NOT NULL) d    
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN newseq = 2 THEN techkey 
    END AS tech2,
    CASE 
      WHEN newseq = 2 THEN hours
    END AS tech2hrs  
  FROM zbtg4techs a) c
  WHERE tech2 IS NOT NULL) e ON d.storecode = e.storecode AND d.ro = e.ro AND d.line = e.line  
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN newseq = 3 THEN techkey 
    END AS tech3,
    CASE 
      WHEN newseq = 3 THEN hours
    END AS tech3hrs  
  FROM zbtg4techs a) f
  WHERE tech3 IS NOT NULL) g ON d.storecode = g.storecode AND d.ro = g.ro AND d.line = g.line 
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN newseq = 4 THEN techkey 
    END AS tech4,
    CASE 
      WHEN newseq = 4 THEN hours
    END AS tech4hrs  
  FROM zbtg4techs a) h
  WHERE tech4 IS NOT NULL) i ON d.storecode = i.storecode AND d.ro = i.ro AND d.line = i.line;   
  
/*  
SELECT * FROM zbtgtotal    
SELECT ro, line FROM zbtgtotal GROUP BY ro,line HAVING COUNT(*) > 1

*/  
    
-- AND single techs
DELETE FROM zbtgtotal WHERE tech2 IS NULL;
INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1)
SELECT storecode,ro,line,totalhours, techkey, hours,1.0
FROM zbtg1 a
WHERE EXISTS (
  SELECT 1
    FROM (
      SELECT storecode, ro, line
      FROM zbtg1 
      GROUP BY storecode, ro, line
      HAVING COUNT(*) = 1) z
    WHERE z.ro = a.ro
      AND z.line = a.line);  

DROP TABLE zBTG;
CREATE TABLE zBTG (
  brdgTechGroupKey autoinc,
  tech1 integer,
  tech2 integer,
  tech3 integer,
  tech4 integer,
  wf1 double(4),
  wf2 double(4),
  wf3 double(4),
  wf4 double(4)) IN database;
-- generate telmporary techgroupkeys  


DECLARE @cur CURSOR AS 
  SELECT * FROM (
    SELECT tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4
    FROM zbtgtotal 
    WHERE tech4 IS NOT NULL
    UNION
    -- 1 tech
    SELECT tech1, cast(NULL as sql_integer) AS tech2, cast(NULL as sql_integer) AS tech3, 
      cast(NULL as sql_integer) AS tech4, 
      cast(1 as sql_double) AS wf1, cast(NULL as sql_double) AS wf2, 
      cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
    FROM (     
    SELECT tech1, 1
    FROM zbtgtotal
    WHERE tech2 IS NULL 
    GROUP BY tech1) x
    UNION
    -- 2 techs
    SELECT DISTINCT tech1,tech2,
      cast(NULL as sql_integer) AS tech3, 
      cast(NULL as sql_integer) AS tech4,
      round(wf1,4) AS wf1, round(wf2,4) AS wf2, 
      cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
    FROM (
      SELECT 
        CASE WHEN tech1 <= tech2 THEN tech1 ELSE tech2 END AS tech1,
        CASE WHEN tech1 <= tech2 THEN tech2 ELSE tech1 END AS tech2,
        CASE WHEN tech1 <= tech2 THEN wf1 ELSE wf2 END AS wf1,
        CASE WHEN tech1 <= tech2 THEN wf2 ELSE wf1 END AS wf2
      FROM zbtgtotal 
      WHERE tech2 IS NOT NULL 
        AND tech3 IS NULL  
      GROUP BY tech1,tech2,wf1,wf2) x
    UNION
    -- 3 techs
    SELECT DISTINCT tech1,tech2,tech3,cast(NULL as sql_integer) AS tech4,
    round(wf1,4),round(wf2,4),round(wf3,4),cast(NULL as sql_double) AS wf4
    FROM (  
      SELECT
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech1, iif(tech1 <= tech3, tech1, tech3)) 
          ELSE iif(tech1 <= tech3, tech2, iif(tech2 <= tech3, tech2, tech3)) 
        END AS tech1,
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech2, iif(tech1 <= tech3, tech3 ,tech1)) 
          ELSE iif(tech1 <= tech3, tech1, iif(tech2 <= tech3, tech3, tech2)) 
        END AS tech2,
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech3, iif(tech1 <= tech3, tech2 ,tech2)) 
          ELSE iif(tech1 <= tech3, tech3, iif(tech2 <= tech3, tech1, tech1)) 
        END AS tech3,   
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf1, iif(tech1 <= tech3, wf1, wf3)) 
          ELSE iif(tech1 <= tech3, wf2, iif(tech2 <= tech3, wf2, wf3)) 
        END AS wf1,
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf2, iif(tech1 <= tech3, wf3 ,wf1)) 
          ELSE iif(tech1 <= tech3, wf1, iif(tech2 <= tech3, wf3, wf2)) 
        END AS wf2,
        CASE 
          WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf3, iif(tech1 <= tech3, wf2, wf2)) 
          ELSE iif(tech1 <= tech3, wf3, iif(tech2 <= tech3, wf1, wf1)) 
        END AS wf3    
      FROM zbtgtotal
      WHERE tech3 IS NOT NULL
        AND tech4 IS NULL 
      GROUP BY tech1,tech2,tech3,wf1,wf2,wf3) x) z;
DECLARE @i integer;
-- @i = (SELECT MAX(techgroupkey) + 1 FROM dimTechGroup);
  @i = 1;
OPEN @cur;
TRY
  WHILE FETCH @cur DO
    INSERT INTO zBTG values(@i, @cur.tech1,@cur.tech2,@cur.tech3,@cur.tech4,@cur.wf1,@cur.wf2,@cur.wf3,@cur.wf4);
    @i = @i + 1;
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY;      

 
-- 8/15 2 things to consider, which of the tech groups already EXIST, 
-- of those that don't, need to ADD new brtechgroup/dimtechgroup rows
-- need to popluate the NEW/tmpSDET/PDET rows the correct techgroupkeys
-- 8/16
-- go the route of creating a tmpBrTechGroup TABLE, the fuck with comparing that
-- with brTechGroup
CREATE TABLE tmpBrTechGroup ( 
      TechGroupKey Integer,
      TechKey Integer,
      WeightFactor Double( 2 )) IN DATABASE;
DELETE FROM tmpBrTechGroup;
INSERT INTO tmpBrTechGroup   
SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech2 IS NULL;

INSERT INTO tmpBrTechGroup   
SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NULL;

INSERT INTO tmpBrTechGroup   
SELECT brdgTechGroupKey,tech2,wf2 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NULL;

INSERT INTO tmpBrTechGroup  
SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NOT NULL AND tech4 IS NULL;

INSERT INTO tmpBrTechGroup   
SELECT brdgTechGroupKey,tech2,wf2 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NOT NULL AND tech4 IS NULL;

INSERT INTO tmpBrTechGroup   
SELECT brdgTechGroupKey,tech3,wf3 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NOT NULL AND tech4 IS NULL;

INSERT INTO tmpBrTechGroup  
SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech4 IS NOT NULL;

INSERT INTO tmpBrTechGroup  
SELECT brdgTechGroupKey,tech2,wf2 FROM zbtg WHERE tech4 IS NOT NULL;

INSERT INTO tmpBrTechGroup   
SELECT brdgTechGroupKey,tech3,wf3 FROM zbtg WHERE tech4 IS NOT NULL;

INSERT INTO tmpBrTechGroup  
SELECT brdgTechGroupKey,tech4,wf4 FROM zbtg WHERE tech4 IS NOT NULL; 

-- tmp there IS a techgroup (126)  that consists of 303/.3235 & 304/.6765
-- does that tech GROUP  exist IN brTechGroup ?

-- ok, this gives me the individual tech/wf combinations that don't exist
-- but i have no fucking idea IF it gives me the missing groups
-- i can NOT even grok IF it IS the same thing
-- feeling cautiously optimistic about this
-- AS the basis for new rows IN brTechGroup
SELECT *
FROM tmpBrTechGroup
WHERE TechGroupKey IN (
  SELECT a.techGroupKey
  FROM tmpBrTechGroup a
  WHERE NOT EXISTS (
    SELECT 1
    FROM brTechGroup
    WHERE techkey = a.techkey
      AND weightfactor = a.weightfactor))
ORDER BY techgroupkey    
-- this works good enough
-- those rows IN tmpBrTechGroup WHERE the techkey/weightfactor does NOT exist IN brTechGroup
DECLARE @curGroup CURSOR AS
  SELECT DISTINCT a.techGroupKey
  FROM tmpBrTechGroup a
  WHERE NOT EXISTS (
    SELECT 1
    FROM brTechGroup
    WHERE techkey = a.techkey
      AND weightfactor = a.weightfactor)
  ORDER BY TechGroupKey, TechKey;
DECLARE @cur CURSOR AS 
  SELECT techkey, weightfactor
  FROM tmpBrTechGroup
  WHERE TechGroupKey = @TechGroupKey;
DECLARE @i integer;
DECLARE @techkey integer;
DECLARE @weightfactor double(4);
DECLARE @TechGroupKey integer;
@i = (SELECT MAX(TechGroupKey) + 1 FROM dimTechGroup);  -- 2912
OPEN @curGroup;
TRY
  WHILE FETCH @curGroup DO 
    @TechGroupKey = @curGroup.TechGroupKey;
    OPEN @cur;
    TRY 
      WHILE FETCH @cur DO
        @TechKey = @cur.TechKey;
        @WeightFactor = @cur.WeightFactor;
        INSERT INTO brTechGroup values(@i, @TechKey, @WeightFactor);
      END WHILE;
    FINALLY
      CLOSE @cur;
    END TRY;
    @i = @i + 1;
  END WHILE;
FINALLY
  CLOSE @curGroup;
END TRY;        

INSERT INTO DimTechGroup
SELECT DISTINCT TechGroupKey
FROM brTechGroup a
WHERE NOT EXISTS (
  SELECT 1
  FROM dimtechgroup
  WHERE techgroupkey = a.techgroupkey);























































