    
-- this is 1 row for every tech/wf combination   

-- 4 techs   
-- DROP TABLE #wtf1
-- SELECT * INTO #wtf1 FROM (
SELECT tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4
FROM zbtgtotal 
WHERE tech4 IS NOT NULL
UNION
-- 1 tech
SELECT tech1, cast(NULL as sql_integer) AS tech2, cast(NULL as sql_integer) AS tech3, 
  cast(NULL as sql_integer) AS tech4, 
  cast(1 as sql_double) AS wf1, cast(NULL as sql_double) AS wf2, 
  cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
FROM (     
SELECT tech1, 1
FROM zbtgtotal
WHERE tech2 IS NULL 
GROUP BY tech1) x
UNION
-- 2 techs
SELECT DISTINCT tech1,tech2,
  cast(NULL as sql_integer) AS tech3, 
  cast(NULL as sql_integer) AS tech4,
  round(wf1,4) AS wf1, round(wf2,4) AS wf2, 
  cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
FROM (
  SELECT 
    CASE WHEN tech1 <= tech2 THEN tech1 ELSE tech2 END AS tech1,
    CASE WHEN tech1 <= tech2 THEN tech2 ELSE tech1 END AS tech2,
    CASE WHEN tech1 <= tech2 THEN wf1 ELSE wf2 END AS wf1,
    CASE WHEN tech1 <= tech2 THEN wf2 ELSE wf1 END AS wf2
  FROM zbtgtotal 
  WHERE tech2 IS NOT NULL 
    AND tech3 IS NULL  
  GROUP BY tech1,tech2,wf1,wf2) x
UNION
-- 3 techs
SELECT DISTINCT tech1,tech2,tech3,cast(NULL as sql_integer) AS tech4,
round(wf1,4),round(wf2,4),round(wf3,4),cast(NULL as sql_double) AS wf4
FROM (  
  SELECT
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech1, iif(tech1 <= tech3, tech1, tech3)) 
      ELSE iif(tech1 <= tech3, tech2, iif(tech2 <= tech3, tech2, tech3)) 
    END AS tech1,
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech2, iif(tech1 <= tech3, tech3 ,tech1)) 
      ELSE iif(tech1 <= tech3, tech1, iif(tech2 <= tech3, tech3, tech2)) 
    END AS tech2,
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech3, iif(tech1 <= tech3, tech2 ,tech2)) 
      ELSE iif(tech1 <= tech3, tech3, iif(tech2 <= tech3, tech1, tech1)) 
    END AS tech3,   
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf1, iif(tech1 <= tech3, wf1, wf3)) 
      ELSE iif(tech1 <= tech3, wf2, iif(tech2 <= tech3, wf2, wf3)) 
    END AS wf1,
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf2, iif(tech1 <= tech3, wf3 ,wf1)) 
      ELSE iif(tech1 <= tech3, wf1, iif(tech2 <= tech3, wf3, wf2)) 
    END AS wf2,
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf3, iif(tech1 <= tech3, wf2, wf2)) 
      ELSE iif(tech1 <= tech3, wf3, iif(tech2 <= tech3, wf1, wf1)) 
    END AS wf3    
  FROM zbtgtotal
  WHERE tech3 IS NOT NULL
    AND tech4 IS NULL 
  GROUP BY tech1,tech2,tech3,wf1,wf2,wf3) x
--) z
  

DROP TABLE zBTG;
CREATE TABLE zBTG (
  brdgTechGroupKey autoinc,
  tech1 integer,
  tech2 integer,
  tech3 integer,
  tech4 integer,
  wf1 double(4),
  wf2 double(4),
  wf3 double(4),
  wf4 double(4)) IN database;
  
INSERT INTO zBTG (tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4)
SELECT * FROM (
SELECT tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4
FROM zbtgtotal 
WHERE tech4 IS NOT NULL
UNION
-- 1 tech
SELECT tech1, cast(NULL as sql_integer) AS tech2, cast(NULL as sql_integer) AS tech3, 
  cast(NULL as sql_integer) AS tech4, 
  cast(1 as sql_double) AS wf1, cast(NULL as sql_double) AS wf2, 
  cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
FROM (     
SELECT tech1, 1
FROM zbtgtotal
WHERE tech2 IS NULL 
GROUP BY tech1) x
UNION
-- 2 techs
SELECT DISTINCT tech1,tech2,
  cast(NULL as sql_integer) AS tech3, 
  cast(NULL as sql_integer) AS tech4,
  round(wf1,4) AS wf1, round(wf2,4) AS wf2, 
  cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
FROM (
  SELECT 
    CASE WHEN tech1 <= tech2 THEN tech1 ELSE tech2 END AS tech1,
    CASE WHEN tech1 <= tech2 THEN tech2 ELSE tech1 END AS tech2,
    CASE WHEN tech1 <= tech2 THEN wf1 ELSE wf2 END AS wf1,
    CASE WHEN tech1 <= tech2 THEN wf2 ELSE wf1 END AS wf2
  FROM zbtgtotal 
  WHERE tech2 IS NOT NULL 
    AND tech3 IS NULL  
  GROUP BY tech1,tech2,wf1,wf2) x
UNION
-- 3 techs
SELECT DISTINCT tech1,tech2,tech3,cast(NULL as sql_integer) AS tech4,
round(wf1,4),round(wf2,4),round(wf3,4),cast(NULL as sql_double) AS wf4
FROM (  
  SELECT
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech1, iif(tech1 <= tech3, tech1, tech3)) 
      ELSE iif(tech1 <= tech3, tech2, iif(tech2 <= tech3, tech2, tech3)) 
    END AS tech1,
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech2, iif(tech1 <= tech3, tech3 ,tech1)) 
      ELSE iif(tech1 <= tech3, tech1, iif(tech2 <= tech3, tech3, tech2)) 
    END AS tech2,
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech3, iif(tech1 <= tech3, tech2 ,tech2)) 
      ELSE iif(tech1 <= tech3, tech3, iif(tech2 <= tech3, tech1, tech1)) 
    END AS tech3,   
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf1, iif(tech1 <= tech3, wf1, wf3)) 
      ELSE iif(tech1 <= tech3, wf2, iif(tech2 <= tech3, wf2, wf3)) 
    END AS wf1,
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf2, iif(tech1 <= tech3, wf3 ,wf1)) 
      ELSE iif(tech1 <= tech3, wf1, iif(tech2 <= tech3, wf3, wf2)) 
    END AS wf2,
    CASE 
      WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf3, iif(tech1 <= tech3, wf2, wf2)) 
      ELSE iif(tech1 <= tech3, wf3, iif(tech2 <= tech3, wf1, wf1)) 
    END AS wf3    
  FROM zbtgtotal
  WHERE tech3 IS NOT NULL
    AND tech4 IS NULL 
  GROUP BY tech1,tech2,tech3,wf1,wf2,wf3) x) z  
 
-- this becomes the resource for assigning the techgroupkey to the ro line
-- zbtg IS the list of actual techgroupkeys 
SELECT * 
FROM (
  SELECT storecode, ro, line, tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4 
  FROM zbtgtotal -- 1/4 techs
  WHERE (tech2 IS NULL OR tech4 IS NOT NULL) 
  UNION -- 2 techs
  SELECT storecode, ro, line, tech1, tech2,
    cast(NULL as sql_integer) AS tech3, 
    cast(NULL as sql_integer) AS tech4,
    round(wf1,4) AS wf1, round(wf2,4) AS wf2, 
    cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
  FROM (
    SELECT storecode, ro, line,
      CASE WHEN tech1 <= tech2 THEN tech1 ELSE tech2 END AS tech1,
      CASE WHEN tech1 <= tech2 THEN tech2 ELSE tech1 END AS tech2,
      CASE WHEN tech1 <= tech2 THEN wf1 ELSE wf2 END AS wf1,
      CASE WHEN tech1 <= tech2 THEN wf2 ELSE wf1 END AS wf2
    FROM zbtgtotal 
    WHERE tech2 IS NOT NULL 
      AND tech3 IS NULL) x  
  UNION -- 3 techs
  SELECT storecode, ro, line, tech1,tech2,tech3,cast(NULL as sql_integer) AS tech4,
    round(wf1,4),round(wf2,4),round(wf3,4),cast(NULL as sql_double) AS wf4
  FROM (  
    SELECT storecode, ro, line,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech1, iif(tech1 <= tech3, tech1, tech3)) 
        ELSE iif(tech1 <= tech3, tech2, iif(tech2 <= tech3, tech2, tech3)) 
      END AS tech1,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech2, iif(tech1 <= tech3, tech3 ,tech1)) 
        ELSE iif(tech1 <= tech3, tech1, iif(tech2 <= tech3, tech3, tech2)) 
      END AS tech2,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech3, iif(tech1 <= tech3, tech2 ,tech2)) 
        ELSE iif(tech1 <= tech3, tech3, iif(tech2 <= tech3, tech1, tech1)) 
      END AS tech3,   
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf1, iif(tech1 <= tech3, wf1, wf3)) 
        ELSE iif(tech1 <= tech3, wf2, iif(tech2 <= tech3, wf2, wf3)) 
      END AS wf1,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf2, iif(tech1 <= tech3, wf3 ,wf1)) 
        ELSE iif(tech1 <= tech3, wf1, iif(tech2 <= tech3, wf3, wf2)) 
      END AS wf2,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf3, iif(tech1 <= tech3, wf2, wf2)) 
        ELSE iif(tech1 <= tech3, wf3, iif(tech2 <= tech3, wf1, wf1)) 
      END AS wf3    
    FROM zbtgtotal
    WHERE tech3 IS NOT NULL
      AND tech4 IS NULL) x) a
LEFT JOIN zbtg b ON a.tech1 = b.tech1 AND coalesce(a.tech2,0) = coalesce(b.tech2,0)
  AND coalesce(a.tech3,0) = coalesce(b.tech3,0) AND coalesce(a.tech4,0) = coalesce(b.tech4,0)
  AND coalesce(round(a.wf1,4),0) = coalesce(round(b.wf1,4),0)
  AND coalesce(round(a.wf2,4),0) = coalesce(round(b.wf2,4),0)
  AND coalesce(round(a.wf3,4),0) = coalesce(round(b.wf3,4),0)
  AND coalesce(round(a.wf4,4),0) = coalesce(round(b.wf4,4),0)           
--WHERE b.brdgtechgroupkey IS NULL 