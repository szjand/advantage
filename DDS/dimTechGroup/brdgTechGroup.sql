
--< need the unknown techkey, one for each store ------------------------------
-- DELETE FROM dimtech WHERE technumber = 'UNK';
INSERT INTO dimtech (storecode, employeenumber, description, technumber, active, 
  flagdeptcode, flagdept, currentrow, 
  techkeyfromdate, techkeyfromdatekey, techkeythrudate, techkeythrudatekey)
SELECT 'RY1', 'NA', 'UNKNOWN', 'UNK',true, 'UN', 'UNKNOWN', true, 
  '07/29/2009', (SELECT datekey FROM day WHERE thedate = '07/29/2009'),
  '12/31/9999', (SELECT datekey FROM day WHERE datetype <> 'date')
FROM system.iota;

INSERT INTO dimtech (storecode, employeenumber, description, technumber, active, 
  flagdeptcode, flagdept, currentrow, 
  techkeyfromdate, techkeyfromdatekey, techkeythrudate, techkeythrudatekey)
SELECT 'RY2', 'NA', 'UNKNOWN', 'UNK',true, 'UN', 'UNKNOWN', true, 
  '07/29/2009', (SELECT datekey FROM day WHERE thedate = '07/29/2009'),
  '12/31/9999', (SELECT datekey FROM day WHERE datetype <> 'date')
FROM system.iota;

INSERT INTO keymapdimtech
SELECT techkey, storecode, technumber, active, flagdeptcode, laborcost, employeenumber
FROM dimtech
WHERE technumber = 'UNK';  
--/> need the unknown techkey, one for each store ------------------------------

--< need the NA techkey, one for each store ------------------------------
hmmm i this a CASE WHERE separate unknown AND n/a dim rows IS justified?

IN AS much AS there can be no NULL dimkeys IN the fact TABLE, seems LIKE, yep,
every row with 0 total flag hours gets this one

INSERT INTO dimTech (storecode,technumber,active,flagdeptcode,flagdept,currentrow,
  techkeyfromdate, techkeyfromdatekey, techkeythrudate,techkeythrudatekey)
SELECT storecode,'N/A',true,'NA','N/A',true,
  techkeyfromdate, techkeyfromdatekey, techkeythrudate,techkeythrudatekey
FROM dimtech
WHERE storecode = 'RY1'
  AND technumber = 'UNK';  
  
INSERT INTO dimTech (storecode,technumber,active,flagdeptcode,flagdept,currentrow,
  techkeyfromdate, techkeyfromdatekey, techkeythrudate,techkeythrudatekey)
SELECT storecode,'N/A',true,'NA','N/A',true,
  techkeyfromdate, techkeyfromdatekey, techkeythrudate,techkeythrudatekey
FROM dimtech
WHERE storecode = 'RY2'
  AND technumber = 'UNK';  
  
INSERT INTO keymapdimtech
SELECT techkey, storecode, technumber, active, flagdeptcode, laborcost, employeenumber
FROM dimtech
WHERE technumber = 'N/A';   
--/> need the NA techkey, one for each store ------------------------------  

bridge tech GROUP becomes every combination of techs/weightfactor per an ro line
the basic grain of bridgeTechGroupKey IS an ro line
-- flag hours > 0 only
SELECT COUNT(*) FROM zbtg1
DROP TABLE zBTG1;
CREATE TABLE zBTG1 (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  totalhours double(2),
  seq integer,
  hours double(2),
  techkey integer) IN database;
INSERT INTO zBTG1 
SELECT a.storecode, a.ro, a.line, a.lineflaghours AS TotalHours, 
  b.seq, b.hrs, 
  CASE 
    WHEN a.storecode = 'RY1' THEN
      CASE WHEN c.TechKey IS NOT NULL THEN c.TechKey ELSE d.TechKey END 
    WHEN a.storecode = 'RY2' THEN 
      CASE WHEN c.TechKey IS NOT NULL THEN c.TechKey ELSE d.TechKey END 
  END AS TechKey  
FROM factroline a
LEFT JOIN (
  SELECT ptco#, ptro#, ptline,pttech, max(ptseq#) as seq, SUM(ptlhrs) AS hrs, MIN(ptdate) AS minDate
  FROM stgArkonaSDPRDET
  WHERE pttech <> ''
    AND ptlhrs > 0
    AND ptco# IN ('RY1','RY2')
  GROUP BY ptco#, ptro#, ptline, pttech) b ON a.storecode = b.ptco# AND a.ro = b.ptro# AND a.line = b.ptline
LEFT JOIN dimtech c ON b.pttech = c.technumber
  AND a.storecode = c.storecode
  AND b.mindate BETWEEN c.techkeyfromdate AND c.techkeythrudate
  AND c.active = true
LEFT JOIN (
  SELECT storecode, TechKey 
  FROM dimTech
  WHERE technumber = 'UNK') d ON a.storecode = d.storecode 
WHERE a.lineflaghours > 0
  AND EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.ro
      AND line = a.line);

-- MAX IS 4 techs per line      
SELECT a.ro, a.line, COUNT(*) 
FROM zbtg1 a 
GROUP BY a.ro, a.line HAVING COUNT(*) > 1
ORDER BY COUNT(*) DESC 

DROP TABLE zBTGTotal;
create TABLE zBTGTotal (
  TechGroupKey integer,
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  TotalHours double(4),
  tech1 integer,
  hours1 double(4),
  wf1 double(4),
  tech2 integer, 
  hours2 double(4),
  wf2 double(4),
  tech3 integer,
  hours3 double(4),
  wf3 double(4),
  tech4 integer,
  hours4 double(4),
  wf4 double(4)) IN database;

DROP TABLE zBTG2Techs;
CREATE TABLE zBTG2Techs (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  totalhours double(4),
  seq integer,
  hours double(4),
  techkey integer) IN database;
-- DELETE FROM zbtg2Techs;  
INSERT INTO zbtg2Techs
SELECT *
FROM zbtg1 a
WHERE EXISTS (
  SELECT 1
    FROM (
      SELECT storecode, ro, line
      FROM zbtg1 
      GROUP BY storecode, ro, line
      HAVING COUNT(*) = 2) z
    WHERE z.ro = a.ro
      AND z.line = a.line);  
INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1,tech2,hours2,wf2)
SELECT d.storecode, d.ro, d.line, d.totalhours, d.tech1, d.tech1hrs, 
  round(d.tech1hrs/d.totalhours,4) as wf1, e.tech2, e.tech2hrs, 
--  round(e.tech2hrs/d.totalhours, 4) AS wf2
-- since i am freaking about wf NOT adding up to 1
  1 - round(d.tech1hrs/d.totalhours,4) as wf2
FROM (
  SELECT *
  FROM (
    SELECT storecode, ro, line, totalhours,
      CASE 
        WHEN seq = (SELECT MIN(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN techkey 
      END AS tech1,
      CASE 
        WHEN seq = (SELECT MIN(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN hours
      END AS tech1hrs
    FROM zbtg2techs a) b
  WHERE tech1 IS NOT NULL) d    
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN seq = (SELECT max(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN techkey 
    END AS tech2,
    CASE 
      WHEN seq = (SELECT max(seq) FROM zbtg2techs WHERE ro = a.ro AND line = a.line) THEN hours
    END AS tech2hrs  
  FROM zbtg2techs a) c
  WHERE tech2 IS NOT NULL) e ON d.storecode = e.storecode AND d.ro = e.ro AND d.line = e.line;   
/*  
--< goofy failure, this one has the same seq for each tech ON the same line -----    

SELECT ro, line FROM zbtgtotal GROUP BY ro,line HAVING COUNT(*) > 1

SELECT * FROM zbtgtotal WHERE ro = '16092180'

-- another way to detect
SELECT * FROM zbtgtotal WHERE tech1 = tech2
DELETE FROM zbtgtotal WHERE tech1 = tech2;
DELETE FROM zbtgtotal WHERE ro = '16092180' AND line = 3 AND tech1 = 51

SELECT * FROM zbtg1 WHERE ro = '16092180'
SELECT * FROM zbtg2techs WHERE ro = '16092180'

SELECT ro,line,techkey FROM zbtg2techs GROUP BY ro,line,techkey HAVING COUNT(*) > 1

SELECT * FROM stgarkonasdprdet WHERE ptro# = '16092180' AND ptline = 3
--/> goofy failure, this one has the same seq for each tech ON the same line ----- 

SELECT * FROM zbtgtotal    
SELECT ro, line, COUNT(*) FROM zbtgtotal GROUP BY ro,line HAVING COUNT(*) > 1
-- this should give me the unique techgroup groupings for each 2 tech combination
-- but of course, AS i have learned, does NOT, have to deal with the tech1=tech2 AND tech2=tech1 deal
SELECT tech1, tech2, wf1,wf2 FROM zbtgtotal GROUP by tech1, tech2, wf1,wf2
*/
-- 3 techs
-- delete FROM zbtgtotal WHERE tech3 IS NOT NULL AND tech4 is NULL  
-- 8/14 1/3 IS fucking with me, grouping on round 4 is returning separate rows for .3333 and .3334 (of course)
-- extract AS double(5) GROUP BY round 4 AND see how it goes
DROP TABLE zbtg3Techs;
CREATE TABLE zBTG3Techs (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  totalhours double(5),
  seq integer,
  hours double(4),
  techkey integer) IN database;
INSERT INTO zbtg3Techs
SELECT *
FROM zbtg1 a
WHERE EXISTS (
  SELECT 1
    FROM (
      SELECT storecode, ro, line
      FROM zbtg1 
      GROUP BY storecode, ro, line
      HAVING COUNT(*) = 3) z
    WHERE z.ro = a.ro
      AND z.line = a.line);   
      
INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1,
  tech2,hours2,wf2,tech3,hours3,wf3) 
SELECT d.storecode, d.ro, d.line, d.totalhours, d.tech1, d.tech1hrs, 
  round(d.tech1hrs/d.totalhours,5) as wf1, e.tech2, e.tech2hrs, 
  round(e.tech2hrs/d.totalhours,5) AS wf2, f.tech3, f.tech3hrs,
-- since i am freaking about wf NOT adding up to 1  
--  round(f.tech3hrs/d.totalhours, 4) AS wf3
  round(1 - (round(d.tech1hrs/d.totalhours,5) + round(e.tech2hrs/d.totalhours, 5)),5) 
FROM (
  SELECT *
  FROM (
    SELECT storecode, ro, line, totalhours,
      CASE 
        WHEN seq = (SELECT MIN(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN techkey 
      END AS tech1,
      CASE 
        WHEN seq = (SELECT MIN(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN hours
      END AS tech1hrs
    FROM zbtg3techs a) b
  WHERE tech1 IS NOT NULL) d    
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN seq = (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN techkey 
    END AS tech2,
    CASE 
      WHEN seq = (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN hours
    END AS tech2hrs  
  FROM zbtg3techs a) c
  WHERE tech2 IS NOT NULL) e ON d.storecode = e.storecode AND d.ro = e.ro AND d.line = e.line
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN seq <> (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN 
        CASE 
          WHEN seq <> (SELECT min(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN techkey 
        END
    END AS tech3,
    CASE 
      WHEN seq <> (SELECT max(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN
        CASE
          WHEN seq <> (SELECT min(seq) FROM zbtg3techs WHERE ro = a.ro AND line = a.line) THEN hours
        END
    END AS tech3hrs  
  FROM zbtg3techs a) d
  WHERE tech3 IS NOT NULL) f ON d.storecode = f.storecode AND d.ro = f.ro AND d.line = f.line;   
/*  
SELECT * FROM zbtgtotal    
SELECT a.* FROM zbtgtotal a
INNER JOIN (
SELECT ro, line FROM zbtgtotal GROUP BY ro,line HAVING COUNT(*) > 1) b ON a.ro = b.ro AND a.line = b.line
*/  


-- 4 techs, DO 2 groups of 2 GROUP 1: tech1, tech2, GROUP 2: tech3, tech4 
-- nope, assign a new seq via a CURSOR based script 
DROP TABLE zbtg4Techs;
CREATE TABLE zBTG4Techs (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  totalhours double(4),
  seq integer,
  hours double(4),
  techkey integer,
  newseq integer) IN database;
-- DELETE FROM zbtg4Techs; 
INSERT INTO zbtg4Techs
SELECT a.*, 0
FROM zbtg1 a
WHERE EXISTS (
  SELECT 1
    FROM (
      SELECT storecode, ro, line
      FROM zbtg1 
      GROUP BY storecode, ro, line
      HAVING COUNT(*) = 4) z
    WHERE z.ro = a.ro
      AND z.line = a.line);   

DECLARE @cur CURSOR as      
SELECT * from zbtg4techs WHERE ro = @ro AND line = @line;
DECLARE @rolinecur CURSOR AS
SELECT DISTINCT ro,line
FROM zbtg4techs;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @i integer;
DECLARE @seq integer;

OPEN @rolinecur;
TRY
  WHILE FETCH @rolinecur DO
    @ro = @rolinecur.ro;
    @line = @rolinecur.line;
    @i = 1;    
    OPEN @cur;
    TRY 
      WHILE FETCH @cur DO
        @seq = @cur.seq;
        UPDATE zbtg4techs
        SET newseq = @i
        WHERE seq = @seq;
        @i = @i + 1;
      END WHILE;
    FINALLY
      CLOSE @cur;
    END TRY;   
  END WHILE;
FINALLY 
  CLOSE @rolinecur;
END TRY;   

INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1,
  tech2,hours2,wf2,tech3,hours3,wf3, tech4,hours4,wf4) 
SELECT d.storecode, d.ro, d.line, d.totalhours, d.tech1, d.tech1hrs, 
  round(d.tech1hrs/d.totalhours,4) as wf1, e.tech2, e.tech2hrs, 
  round(e.tech2hrs/d.totalhours, 4) AS wf2, g.tech3, g.tech3hrs,
  round(g.tech3hrs/d.totalhours, 4) AS wf3, tech4, tech4hrs,
  1 - (round(tech3hrs/d.totalhours, 4) + round(tech2hrs/d.totalhours, 4) + round(tech1hrs/d.totalhours, 4)) AS wf4
FROM (
  SELECT *
  FROM (
    SELECT storecode, ro, line, totalhours,
      CASE 
        WHEN newseq = 1 THEN techkey 
      END AS tech1,
      CASE 
        WHEN newseq = 1 THEN hours
      END AS tech1hrs
    FROM zbtg4techs a) b
  WHERE tech1 IS NOT NULL) d    
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN newseq = 2 THEN techkey 
    END AS tech2,
    CASE 
      WHEN newseq = 2 THEN hours
    END AS tech2hrs  
  FROM zbtg4techs a) c
  WHERE tech2 IS NOT NULL) e ON d.storecode = e.storecode AND d.ro = e.ro AND d.line = e.line  
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN newseq = 3 THEN techkey 
    END AS tech3,
    CASE 
      WHEN newseq = 3 THEN hours
    END AS tech3hrs  
  FROM zbtg4techs a) f
  WHERE tech3 IS NOT NULL) g ON d.storecode = g.storecode AND d.ro = g.ro AND d.line = g.line 
LEFT JOIN (       
  SELECT *
  FROM (        
  SELECT storecode, ro, line,
    CASE 
      WHEN newseq = 4 THEN techkey 
    END AS tech4,
    CASE 
      WHEN newseq = 4 THEN hours
    END AS tech4hrs  
  FROM zbtg4techs a) h
  WHERE tech4 IS NOT NULL) i ON d.storecode = i.storecode AND d.ro = i.ro AND d.line = i.line;   
  
/*  
SELECT * FROM zbtgtotal    
SELECT ro, line FROM zbtgtotal GROUP BY ro,line HAVING COUNT(*) > 1

*/  
    
-- AND single techs
DELETE FROM zbtgtotal WHERE tech2 IS NULL;
INSERT INTO zBTGTotal (storecode,ro,line,totalhours,tech1,hours1,wf1)
SELECT storecode,ro,line,totalhours, techkey, hours,1.0
FROM zbtg1 a
WHERE EXISTS (
  SELECT 1
    FROM (
      SELECT storecode, ro, line
      FROM zbtg1 
      GROUP BY storecode, ro, line
      HAVING COUNT(*) = 1) z
    WHERE z.ro = a.ro
      AND z.line = a.line);  
-- 8/13 good to this point
-- now i need to generate a bridge TABLE with a bridgekey    
/*
-- 2 tech lines
DROP TABLE zwtf;
CREATE TABLE zwtf (
  wtf integer,
  tech integer,
  wf double(4)) IN database;
DECLARE @cur cursor AS   
--SELECT tech1,tech2,wf1,wf2
--FROM zbtgtotal 
--WHERE tech2 IS NOT NULL 
--  AND tech3 IS NULL  
--GROUP BY tech1,tech2,wf1,wf2;
  SELECT DISTINCT tech1,tech2,round(wf1,4) AS wf1,round(wf2,4) AS wf2 FROM (
  SELECT 
    CASE WHEN tech1 <= tech2 THEN tech1 ELSE tech2 END AS tech1,
    CASE WHEN tech1 <= tech2 THEN tech2 ELSE tech1 END AS tech2,
    CASE WHEN tech1 <= tech2 THEN wf1 ELSE wf2 END AS wf1,
    CASE WHEN tech1 <= tech2 THEN wf2 ELSE wf1 END AS wf2
  FROM zbtgtotal 
  WHERE tech2 IS NOT NULL 
    AND tech3 IS NULL  
  GROUP BY tech1,tech2,wf1,wf2
  ) x;

DECLARE @tech1 integer;
DECLARE @tech2 integer;
DECLARE @wf1 double(4);
DECLARE @wf2 double(4);
DECLARE @i integer;
@i = 1;
--@i = (SELECT MAX(TechGroupKey) FROM brTechGroup) + 1;
OPEN @cur;
TRY
  WHILE FETCH @cur DO
    @tech1 = @cur.tech1;
    @tech2 = @cur.tech2;
    @wf1 = @cur.wf1;
    @wf2 = @cur.wf2;
    INSERT INTO zwtf (wtf, tech, wf)
    values (@i, @tech1, @wf1);
    INSERT INTO zwtf (wtf, tech,wf)
    values (@i, @tech2, @wf2);
    @i = @i + 1;
  END WHILE;
FINALLY
  CLOSE @cur;
END TRY;   
-- 8/14 got this far
*/
-- 8/15 DO NOT think i need the zwtf table
/*******************************************************************************/

DROP TABLE zBTG;
CREATE TABLE zBTG (
  brdgTechGroupKey autoinc,
  tech1 integer,
  tech2 integer,
  tech3 integer,
  tech4 integer,
  wf1 double(4),
  wf2 double(4),
  wf3 double(4),
  wf4 double(4)) IN database;
-- generate the techgroupkeys  
INSERT INTO zBTG (tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4)
SELECT * FROM (
  SELECT tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4
  FROM zbtgtotal 
  WHERE tech4 IS NOT NULL
  UNION
  -- 1 tech
  SELECT tech1, cast(NULL as sql_integer) AS tech2, cast(NULL as sql_integer) AS tech3, 
    cast(NULL as sql_integer) AS tech4, 
    cast(1 as sql_double) AS wf1, cast(NULL as sql_double) AS wf2, 
    cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
  FROM (     
  SELECT tech1, 1
  FROM zbtgtotal
  WHERE tech2 IS NULL 
  GROUP BY tech1) x
  UNION
  -- 2 techs
  SELECT DISTINCT tech1,tech2,
    cast(NULL as sql_integer) AS tech3, 
    cast(NULL as sql_integer) AS tech4,
    round(wf1,4) AS wf1, round(wf2,4) AS wf2, 
    cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
  FROM (
    SELECT 
      CASE WHEN tech1 <= tech2 THEN tech1 ELSE tech2 END AS tech1,
      CASE WHEN tech1 <= tech2 THEN tech2 ELSE tech1 END AS tech2,
      CASE WHEN tech1 <= tech2 THEN wf1 ELSE wf2 END AS wf1,
      CASE WHEN tech1 <= tech2 THEN wf2 ELSE wf1 END AS wf2
    FROM zbtgtotal 
    WHERE tech2 IS NOT NULL 
      AND tech3 IS NULL  
    GROUP BY tech1,tech2,wf1,wf2) x
  UNION
  -- 3 techs
  SELECT DISTINCT tech1,tech2,tech3,cast(NULL as sql_integer) AS tech4,
  round(wf1,4),round(wf2,4),round(wf3,4),cast(NULL as sql_double) AS wf4
  FROM (  
    SELECT
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech1, iif(tech1 <= tech3, tech1, tech3)) 
        ELSE iif(tech1 <= tech3, tech2, iif(tech2 <= tech3, tech2, tech3)) 
      END AS tech1,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech2, iif(tech1 <= tech3, tech3 ,tech1)) 
        ELSE iif(tech1 <= tech3, tech1, iif(tech2 <= tech3, tech3, tech2)) 
      END AS tech2,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech3, iif(tech1 <= tech3, tech2 ,tech2)) 
        ELSE iif(tech1 <= tech3, tech3, iif(tech2 <= tech3, tech1, tech1)) 
      END AS tech3,   
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf1, iif(tech1 <= tech3, wf1, wf3)) 
        ELSE iif(tech1 <= tech3, wf2, iif(tech2 <= tech3, wf2, wf3)) 
      END AS wf1,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf2, iif(tech1 <= tech3, wf3 ,wf1)) 
        ELSE iif(tech1 <= tech3, wf1, iif(tech2 <= tech3, wf3, wf2)) 
      END AS wf2,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf3, iif(tech1 <= tech3, wf2, wf2)) 
        ELSE iif(tech1 <= tech3, wf3, iif(tech2 <= tech3, wf1, wf1)) 
      END AS wf3    
    FROM zbtgtotal
    WHERE tech3 IS NOT NULL
      AND tech4 IS NULL 
    GROUP BY tech1,tech2,tech3,wf1,wf2,wf3) x) z;
  
-- SET the techgroupokey IN zbtgtotal  
UPDATE zbtgTotal
SET TechGroupKey = brdgTechGroupKey
FROM (-- brGechGroup scratch pad 4.sql
  SELECT storecode, ro, line, tech1,tech2,tech3,tech4,wf1,wf2,wf3,wf4 
  FROM zbtgtotal -- 1/4 techs
  WHERE (tech2 IS NULL OR tech4 IS NOT NULL) 
  UNION -- 2 techs
  SELECT storecode, ro, line, tech1, tech2,
    cast(NULL as sql_integer) AS tech3, 
    cast(NULL as sql_integer) AS tech4,
    round(wf1,4) AS wf1, round(wf2,4) AS wf2, 
    cast(NULL as sql_double) as wf3, cast(NULL as sql_double) AS wf4
  FROM (
    SELECT storecode, ro, line,
      CASE WHEN tech1 <= tech2 THEN tech1 ELSE tech2 END AS tech1,
      CASE WHEN tech1 <= tech2 THEN tech2 ELSE tech1 END AS tech2,
      CASE WHEN tech1 <= tech2 THEN wf1 ELSE wf2 END AS wf1,
      CASE WHEN tech1 <= tech2 THEN wf2 ELSE wf1 END AS wf2
    FROM zbtgtotal 
    WHERE tech2 IS NOT NULL 
      AND tech3 IS NULL) x  
  UNION -- 3 techs
  SELECT storecode, ro, line, tech1,tech2,tech3,cast(NULL as sql_integer) AS tech4,
    round(wf1,4),round(wf2,4),round(wf3,4),cast(NULL as sql_double) AS wf4
  FROM (  
    SELECT storecode, ro, line,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech1, iif(tech1 <= tech3, tech1, tech3)) 
        ELSE iif(tech1 <= tech3, tech2, iif(tech2 <= tech3, tech2, tech3)) 
      END AS tech1,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech2, iif(tech1 <= tech3, tech3 ,tech1)) 
        ELSE iif(tech1 <= tech3, tech1, iif(tech2 <= tech3, tech3, tech2)) 
      END AS tech2,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, tech3, iif(tech1 <= tech3, tech2 ,tech2)) 
        ELSE iif(tech1 <= tech3, tech3, iif(tech2 <= tech3, tech1, tech1)) 
      END AS tech3,   
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf1, iif(tech1 <= tech3, wf1, wf3)) 
        ELSE iif(tech1 <= tech3, wf2, iif(tech2 <= tech3, wf2, wf3)) 
      END AS wf1,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf2, iif(tech1 <= tech3, wf3 ,wf1)) 
        ELSE iif(tech1 <= tech3, wf1, iif(tech2 <= tech3, wf3, wf2)) 
      END AS wf2,
      CASE 
        WHEN tech1 <= tech2 THEN iif(tech2 <= tech3, wf3, iif(tech1 <= tech3, wf2, wf2)) 
        ELSE iif(tech1 <= tech3, wf3, iif(tech2 <= tech3, wf1, wf1)) 
      END AS wf3    
    FROM zbtgtotal
    WHERE tech3 IS NOT NULL
      AND tech4 IS NULL) x) a
LEFT JOIN zbtg b ON a.tech1 = b.tech1 AND coalesce(a.tech2,0) = coalesce(b.tech2,0)
  AND coalesce(a.tech3,0) = coalesce(b.tech3,0) AND coalesce(a.tech4,0) = coalesce(b.tech4,0)
  AND coalesce(round(a.wf1,4),0) = coalesce(round(b.wf1,4),0)
  AND coalesce(round(a.wf2,4),0) = coalesce(round(b.wf2,4),0)
  AND coalesce(round(a.wf3,4),0) = coalesce(round(b.wf3,4),0)
  AND coalesce(round(a.wf4,4),0) = coalesce(round(b.wf4,4),0) 
WHERE zbtgTotal.ro = a.ro
  AND zbtgTotal.line = a.line
  
-- 8/15
what DO i fucking DO AND IN what ORDER
1. remove RI dimTechGroup-factRepairOrder
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimTechGroup-factRepairOrder'); 
DROP index factRepairOrder.TechGroupKey;

UPDATE factRepairOrder
SET TechGroupKey = 0;

-- 8/15 trying to CONTINUE

DROP TABLE brTechGroup;
CREATE TABLE brTechGroup (
  TechGroupKey integer,
  TechKey integer,
  WeightFactor double(4)) IN database;

-- this does a good job of creating the actual bridge table
INSERT INTO brTechGroup   
SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech2 IS NULL;

INSERT INTO brTechGroup   
SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NULL;

INSERT INTO brTechGroup   
SELECT brdgTechGroupKey,tech2,wf2 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NULL;

INSERT INTO brTechGroup  
SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NOT NULL AND tech4 IS NULL;

INSERT INTO brTechGroup   
SELECT brdgTechGroupKey,tech2,wf2 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NOT NULL AND tech4 IS NULL;

INSERT INTO brTechGroup   
SELECT brdgTechGroupKey,tech3,wf3 FROM zbtg WHERE tech2 IS NOT NULL AND tech3 IS NOT NULL AND tech4 IS NULL;

INSERT INTO brTechGroup  
SELECT brdgTechGroupKey,tech1,wf1 FROM zbtg WHERE tech4 IS NOT NULL;

INSERT INTO brTechGroup  
SELECT brdgTechGroupKey,tech2,wf2 FROM zbtg WHERE tech4 IS NOT NULL;

INSERT INTO brTechGroup   
SELECT brdgTechGroupKey,tech3,wf3 FROM zbtg WHERE tech4 IS NOT NULL;

INSERT INTO brTechGroup  
SELECT brdgTechGroupKey,tech4,wf4 FROM zbtg WHERE tech4 IS NOT NULL;  

-- PK
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brTechGroup','brTechGroup.adi','PK','TechGroupKey;TechKey;WeightFactor',
   '',2051,512, '' ); 
-- TABLE Primary Key
EXECUTE PROCEDURE sp_ModifyTableProperty( 'brTechGroup', 
   'Table_Primary_Key', 
   'PK', 'APPEND_FAIL', 'factRepairOrderfail');   
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brTechGroup','brTechGroup.adi','TechGroupKey','TechGroupKey',
   '',2,512,'' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brTechGroup','brTechGroup.adi','TechKey','TechKey',
   '',2,512,'' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brTechGroup','brTechGroup.adi','WeightFactor','WeightFactor',
   '',2,512,'' );       
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'brTechGroup', 
      'TechGroupKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'brTechGroup', 
      'TechKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'brTechGroup', 
      'WeightFactor', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );   
      
INSERT INTO brTechGroup
SELECT 
  (SELECT max(techgroupkey) + 1 FROM brTechGroup),
  (SELECT techkey FROM dimtech WHERE technumber = 'N/A' AND storecode = 'RY1'), 1
FROM system.iota;   

INSERT INTO brTechGroup
SELECT 
  (SELECT max(techgroupkey) + 1 FROM brTechGroup),
  (SELECT techkey FROM dimtech WHERE technumber = 'N/A' AND storecode = 'RY2'), 1
FROM system.iota;     

-- now finish updating factRepairOrder
UPDATE factRepairOrder
SET TechGroupKey = x.TechGroupKey
FROM (
SELECT a.techgroupkey, a.techkey, a.weightfactor
  FROM brTechGroup a
  INNER JOIN dimtech b ON a.techkey = b.techkey
    AND storecode = 'RY1'
    AND TechNumber = 'N/A') x
WHERE factRepairOrder.TechGroupKey = 0
  AND factRepairOrder.StoreCode = 'RY1'
  AND factRepairOrder.FlagHours = 0;
  
UPDATE factRepairOrder
SET TechGroupKey = x.TechGroupKey
FROM (
SELECT a.techgroupkey, a.techkey, a.weightfactor
  FROM brTechGroup a
  INNER JOIN dimtech b ON a.techkey = b.techkey
    AND storecode = 'RY2'
    AND TechNumber = 'N/A') x
WHERE factRepairOrder.TechGroupKey = 0
  AND factRepairOrder.StoreCode = 'RY2'
  AND factRepairOrder.FlagHours = 0;    
  
  
SELECT * FROM factRepairOrder WHERE techgroupkey = 0  

looks LIKE they are voids
AND AS such become part of the nightly void test/update  

DELETE FROM dimTechGroup;

INSERT INTO dimTechGroup
SELECT distinct TechGroupKey
FROM brTechGroup;

  
   
-- AND FINALLY, put everything back
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','TechGroupKey','TechGroupKey',
   '',2,512,'' );     
  
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimTechGroup-factRepairOrder',
     'dimTechGroup', 'factRepairOrder', 'TechGroupKey', 
     2, 2, NULL, '', '');

  
  








  
-- KOOOOOOOOL the only thing LEFT IS 0 flag hours
SELECT * FROM factRepairOrder WHERE techgroupkey is NULL AND lineflaghours <> 0

need a row IN brTechGroup that accomodates NA  

INSERT INTO brTechGroup
SELECT 
  (SELECT max(btgkey) + 1 FROM zjon),
  (SELECT techkey FROM dimtech WHERE technumber = 'N/A' AND storecode = 'RY1'), 1
FROM system.iota;   

INSERT INTO brTechGroup
SELECT 
  (SELECT max(btgkey) + 1 FROM zjon),
  (SELECT techkey FROM dimtech WHERE technumber = 'N/A' AND storecode = 'RY2'), 1
FROM system.iota;     

-- now finish updating factRepairOrder
UPDATE factRepairOrder
SET TechGroupKey = x.TechGroupKey
FROM (
SELECT a.techgroupkey, a.techkey, a.weightfactor
  FROM brTechGroup a
  INNER JOIN dimtech b ON a.techkey = b.techkey
    AND storecode = 'RY1'
    AND TechNumber = 'N/A') x
WHERE factRepairOrder.TechGroupKey IS NULL 
  AND factRepairOrder.StoreCode = 'RY1'
  AND factRepairOrder.LineFlagHours = 0;
  
UPDATE factRepairOrder
SET TechGroupKey = x.TechGroupKey
FROM (
SELECT a.techgroupkey, a.techkey, a.weightfactor
  FROM brTechGroup a
  INNER JOIN dimtech b ON a.techkey = b.techkey
    AND storecode = 'RY2'
    AND TechNumber = 'N/A') x
WHERE factRepairOrder.TechGroupKey IS NULL 
  AND factRepairOrder.StoreCode = 'RY2'
  AND factRepairOrder.LineFlagHours = 0;    
-- boom  
-- SELECT * FROM factRepairOrder WHERE techgroupkey IS NULL     
DELETE FROM dimTechGroup;
INSERT INTO dimTechGroup
SELECT DISTINCT TechGroupKey FROM brTechGroup;   

------------- what about ros with NO lines -----------------------------------
-- no problem, see CONVERT FROM existing fact tables 


SELECT * FROM dimtech WHERE techkey = 1

SELECT * FROM zbtgtotal WHERE tech1 = 0

SELECT * FROM zbtg1 WHERE 
      


SELECT COUNT(*) FROM zbtgtotal
SELECT COUNT(*) FROM (
SELECT tech1, wf1, tech2, wf2, tech3, wf3
FROM zbtgtotal
GROUP BY tech1, wf1, tech2, wf2, tech3, wf3) x

-- freaking about getting correct flag hours for a specific tech
the notion of storing the actual tech flag hours IN the bridge TABLE
which will greatly expand the size of the bridge TABLE
but would NOT need it for single tech lines

SELECT a.*, wf1 + coalesce(wf2,0) + coalesce(wf3,0) + coalesce(wf4,0) FROM zbtgtotal a

SELECT COUNT(*)
FROM zbtg1
-- dist of techs/line
SELECT techlines, COUNT(*)
FROM (
  SELECT ro, line, COUNT(*) AS techlines
  FROM zbtg1
  GROUP BY ro, line) a
GROUP BY techlines  


-- this does a good job of creating the actual bridge table
DROP TABLE zjon;
CREATE TABLE zjon (
  btgKey integer,
  techKey integer,
  weightFactor double(2)) IN database;

INSERT INTO zjon   
--SELECT * FROM (
SELECT brdgTechGroupKey,tech1,wf1
-- SELECT COUNT(*)
FROM zbtg
WHERE tech2 IS NULL;
--) x WHERE tech1 IS NULL  

INSERT INTO zjon   
SELECT brdgTechGroupKey,tech1,wf1
FROM zbtg
WHERE tech2 IS NOT NULL 
  AND tech3 IS NULL;

INSERT INTO zjon   
SELECT brdgTechGroupKey,tech2,wf2
FROM zbtg
WHERE tech2 IS NOT NULL 
  AND tech3 IS NULL;

INSERT INTO zjon   
SELECT brdgTechGroupKey,tech1,wf1
FROM zbtg
WHERE tech2 IS NOT NULL
  AND tech3 IS NOT NULL
  AND tech4 IS NULL;

INSERT INTO zjon   
SELECT brdgTechGroupKey,tech2,wf2
FROM zbtg
WHERE tech2 IS NOT NULL
  AND tech3 IS NOT NULL
  AND tech4 IS NULL;

INSERT INTO zjon   
SELECT brdgTechGroupKey,tech3,wf3
FROM zbtg
WHERE tech2 IS NOT NULL
  AND tech3 IS NOT NULL
  AND tech4 IS NULL;

INSERT INTO zjon   
SELECT brdgTechGroupKey,tech1,wf1
FROM zbtg
WHERE tech4 IS NOT NULL;

INSERT INTO zjon   
SELECT brdgTechGroupKey,tech2,wf2
FROM zbtg
WHERE tech4 IS NOT NULL;

INSERT INTO zjon   
SELECT brdgTechGroupKey,tech3,wf3
FROM zbtg
WHERE tech4 IS NOT NULL;

INSERT INTO zjon   
SELECT brdgTechGroupKey,tech4,wf4
FROM zbtg
WHERE tech4 IS NOT NULL;

DROP TABLE zbtg;
DROP TABLE zbtg1;
DROP TABLE zbtg2techs;
DROP TABLE zbtg3techs;
DROP TABLE zbtg4techs;
DROP TABLE zbtgtotal;
DROP TABLE zbtgwhatever;

-- 8/11, ok, nightly
ok, i am mind fucking ON which IS the correct techkey A-FUCKING-GAIN
Active IS NOT the answer, an active technumber can have multiple techkeys
ok, use techkeyfrom/thru, but this will highlight issues
eg Travis Strandell tech 636, SET up with a fromdate of 7/1, dude has been an employee for 
much longer, recently became a tech, first flagdate IS 6/24/13, did NOT know that
at the time he was SET up AS a tech
So that means that his has to be a test requiring more fucking manual intervention,
IN this CASE, changing the techkeyfromdate for travis

UPDATE dimTech
SET techkeyfromdate = '06/21/2013',
    techkeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '06/21/2013')
WHERE techkey = 315

SELECT a.ptco#, a.ptro#, a.ptline, a.pttech, a.TechLineHours, b.linehours, 
  c.ptdate, d.techkey, round(techlinehours/linehours, 4) AS wf
FROM (
  SELECT ptco#, ptro#, ptline, pttech, SUM(ptlhrs) AS techlinehours
  FROM tmpsdprdet 
  WHERE ptltyp = 'L'
    AND ptcode = 'TT'
    AND ptlhrs <> 0
  GROUP BY ptco#, ptro#, ptline, pttech) a  
LEFT JOIN (
  SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS LineHours
  FROM tmpsdprdet a
  WHERE ptltyp = 'L'
    AND ptcode = 'TT'
  GROUP BY ptco#, ptro#, ptline) b ON a.ptco# = b.ptco# AND a.ptro# = b.ptro# AND a.ptline = b.ptline
LEFT JOIN tmpsdprhdr c ON a.ptco# = c.ptco# AND a.ptro# = c.ptro#
LEFT JOIN dimtech d ON a.ptco# = d.storecode AND a.pttech = d.technumber
  AND c.ptdate BETWEEN d.techkeyfromdate AND d.techkeythrudate
WHERE linehours <> 0

-- missing techs
SELECT *
FROM (
  SELECT a.ptco#, a.ptro#, a.ptline, a.pttech, a.TechLineHours, b.linehours, 
    c.ptdate, d.techkey, round(techlinehours/linehours, 4) AS wf
  FROM (
    SELECT ptco#, ptro#, ptline, pttech, SUM(ptlhrs) AS techlinehours
    FROM tmpsdprdet 
    WHERE ptltyp = 'L'
      AND ptcode = 'TT'
      AND ptlhrs <> 0
    GROUP BY ptco#, ptro#, ptline, pttech) a  
  LEFT JOIN (
    SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS LineHours
    FROM tmpsdprdet a
    WHERE ptltyp = 'L'
      AND ptcode = 'TT'
    GROUP BY ptco#, ptro#, ptline) b ON a.ptco# = b.ptco# AND a.ptro# = b.ptro# AND a.ptline = b.ptline
  LEFT JOIN tmpsdprhdr c ON a.ptco# = c.ptco# AND a.ptro# = c.ptro#
  LEFT JOIN dimtech d ON a.ptco# = d.storecode AND a.pttech = d.technumber
    AND c.ptdate BETWEEN d.techkeyfromdate AND d.techkeythrudate
  WHERE linehours <> 0) x
WHERE techkey IS NULL 

-- missing techs
SELECT *
FROM (
  SELECT a.ptco#, a.ptro#, a.ptline, a.pttech, a.TechLineHours, b.linehours, 
    c.ptdate, d.techkey, round(techlinehours/linehours, 4) AS wf
  FROM (
    SELECT ptco#, ptro#, ptline, pttech, SUM(ptlhrs) AS techlinehours
    FROM tmpsdprdet 
    WHERE ptltyp = 'L'
      AND ptcode = 'TT'
      AND ptlhrs <> 0
    GROUP BY ptco#, ptro#, ptline, pttech) a  
  LEFT JOIN (
    SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS LineHours
    FROM tmpsdprdet a
    WHERE ptltyp = 'L'
      AND ptcode = 'TT'
    GROUP BY ptco#, ptro#, ptline) b ON a.ptco# = b.ptco# AND a.ptro# = b.ptro# AND a.ptline = b.ptline
  LEFT JOIN tmpsdprhdr c ON a.ptco# = c.ptco# AND a.ptro# = c.ptro#
  LEFT JOIN dimtech d ON a.ptco# = d.storecode AND a.pttech = d.technumber
    AND c.ptdate BETWEEN d.techkeyfromdate AND d.techkeythrudate
  WHERE linehours <> 0) x
WHERE techkey IS NULL 

SELECT TechGroupKey, COUNT(*)
FROM brTechGroup
GROUP BY TechGroupKey
ORDER BY COUNT(*) DESC

SELECT * FROM brTechGroup ORDER BY techgroupkey


-- wf = 1
fuck fuck fuck, this exposes techkey 1/wf 1 AS NOT existing IN brTechGroup
techkey 1 - ry2 ql tech 999
ro 2680367 (ry2 tech 999)
factRepairOrder 2680367 IS fucked up, shows 2 techgroupkeys: 2323 & 3120
1. brTechGroup tgKey 3120: 2 techkeys each with a weight factor of 1: 335::RY1/N/A & 336::RY2/N/A
  there should never be more than one techkey IN a techgroupkey with a weightfactor = 1
1.
-- guessing i goofed A-FUCKING-GAIN AND neglected the storecode JOIN
technumber N/A IS valid for lines for which there are no flag hours
SELECT techgroupkey
FROM brtechgroup
WHERE weightfactor = 1
GROUP BY techgroupkey
HAVING COUNT(*) > 1
SELECT * FROM factrepairorder WHERE techgroupkey = 3120
ok, detail out this fix
brTechGroup: 
             ADD a new row
             UPDATE factRepairOrder techGroupKey WHERE tgkey = 3120 AND store = ry2
             DELETE row 3120/336/1 (ry2 N/A)
INSERT INTO brTechGroup 
SELECT 
  (SELECT MAX(techgroupkey) + 1 FROM brTechGroup), 336,1
FROM system.iota; 

INSERT INTO dimtechgroup 
SELECT techgroupkey 
FROM brtechgroup a 
WHERE NOT EXISTS (
  SELECT 1 
  FROM dimtechgroup 
  WHERE techgroupkey = a.techgroupkey)

UPDATE factRepairOrder
  SET techGroupKey = 3124
WHERE Storecode = 'RY2' AND techgroupkey = 3120;               
             
DELETE FROM brTechGroup
WHERE TechGroupKey = 3120
  AND techkey = 336;       

there will be ros with unk tech keys, eg old ros with technumbers that have
subsequently been reused, eg 511
SELECT * FROM factRepairOrder WHERE techgroupkey IN (3039,3094,3095,3096)

-- only 2 that violate this
-- there should ever only be 1 instance of a techkey IN a techgroup
-- fix these, THEN ADD unique index AND RI to brTechGroup
SELECT techgroupkey, techkey
FROM brtechgroup
GROUP BY techgroupkey, techkey
HAVING COUNT(*) > 1
-- no ros w/tgkey 774, only one with 935
SELECT * FROM factrepairorder WHERE techgroupkey IN (774,935)
fix: 
DELETE FROM brTechGroup & dimTechGroup WHERE tgkey = 774
DELETE FROM dimtechgroup WHERE techgroupkey = 774;
DELETE FROM brTechGroup WHERE techgroupkey = 774;
935: 2 rows for techkey 57, repl techtroupkey IN ro 16092180 with 924
UPDATE factRepairOrder SET techgroupkey = 924 WHERE ro = '16092180' AND line = 3;
DELETE FROM dimtechgroup WHERE techgroupkey = 935;
DELETE FROM brTechGroup WHERE techgroupkey = 935;

-- unique index   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brTechGroup','brTechGroup.adi','TechGroupTechKey','TechGroupKey;TechKey',
   '',2048,512,'' ); 
   
2. no brTechGroup row for techkey 1 - which made me wake up to the fact that
   i had excluded the store FROM the JOIN condition for dimtech
      
SELECT * FROM (
  SELECT techkey FROM (
    SELECT a.ptco#, a.ptro#, a.ptline, a.pttech, a.TechLineHours, b.linehours, 
      c.ptdate, d.techkey, round(techlinehours/linehours, 4) AS wf
    FROM (
      SELECT ptco#, ptro#, ptline, pttech, SUM(ptlhrs) AS techlinehours
      FROM tmpsdprdet 
      WHERE ptltyp = 'L'
        AND ptcode = 'TT'
        AND ptlhrs <> 0
      GROUP BY ptco#, ptro#, ptline, pttech) a  
    LEFT JOIN (
      SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS LineHours
      FROM tmpsdprdet a
      WHERE ptltyp = 'L'
        AND ptcode = 'TT'
      GROUP BY ptco#, ptro#, ptline) b ON a.ptco# = b.ptco# AND a.ptro# = b.ptro# AND a.ptline = b.ptline
    LEFT JOIN tmpsdprhdr c ON a.ptco# = c.ptco# AND a.ptro# = c.ptro#
    LEFT JOIN dimtech d ON a.ptco# = d.storecode AND a.pttech = d.technumber
      AND c.ptdate BETWEEN d.techkeyfromdate AND d.techkeythrudate
    WHERE linehours <> 0) x 
  WHERE wf = 1 GROUP BY techkey) y   
WHERE NOT EXISTS (
  SELECT 1
  FROM brTechGroup
  WHERE techkey = y.techkey
    AND weightfactor = 1)  
    
SELECT ptco#, ptro#, ptline, SUM(ptlhrs) AS linehours 
FROM tmpsdprdet
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
GROUP BY ptco#, ptro#, ptline  

SELECT ptco#, ptro#, ptline, COUNT(*)
FROM (
  SELECT ptco#, ptro#, ptline, pttech
  FROM tmpsdprdet
  WHERE ptltyp = 'L'
    AND ptcode = 'TT'
  GROUP BY ptco#, ptro#, ptline, pttech) a
GROUP BY ptco#, ptro#, ptline
ORDER BY COUNT(*) DESC   
 

-- ok, this may be interesting
-- but it also highlights a possible problem
-- without the problem, IF the GROUP COUNT ON this query (ro/line/techgroup) = the number of techs, it's a match ?
SELECT b.ptdate,a.ptco#, a.ptro#, a.ptline, c.techkey, 
  round(techlinehours/linehours, 4) AS wf, d.*
FROM (    
  SELECT ptco#, ptro#, ptline, pttech, sum(ptlhrs) AS techlinehours
  FROM tmpsdprdet
  WHERE ptltyp = 'L'
    AND ptcode = 'TT' 
    AND ptro# = '18024255' AND ptline = 1
  GROUP BY ptco#, ptro#, ptline, pttech) a  
LEFT JOIN (
  SELECT min(ptdate) as ptdate, ptco#, ptro#, ptline, sum(ptlhrs) AS linehours
  FROM tmpsdprdet
  WHERE ptltyp = 'L'
    AND ptcode = 'TT' 
    AND ptro# = '18024255' AND ptline = 1
  GROUP BY ptco#, ptro#, ptline) b ON a.ptco# = b.ptco# AND a.ptro# = b.ptro# AND a.ptline = b.ptline
LEFT JOIN dimtech c ON a.ptco# = c.storecode AND a.pttech = c.technumber
  AND b.ptdate BETWEEN c.techkeyfromdate AND c.techkeythrudate  
INNER JOIN brtechgroup d ON c.techkey = d.techkey
  AND round(techlinehours/linehours, 4) = d.weightfactor
  
-- but it also highlights a possible problem
tg 3044 & 3033 both consist of 302/.5 & 305/.5
which should NOT be, there should only be one combination
feels LIKE WHEN i did the populating, i did NOT enforce rigrorous enough uniqueness
IN this CASE the 2 tech rows
   
so how DO fix it?

SELECT techgroupkey
FROM brtechgroup a
INNER JOIN (
  SELECT techkey, weightfactor
  FROM brtechgroup
  GROUP BY techkey, weightfactor) b ON a.techkey = b.techkey AND a.weightfactor = b.weightfactor
GROUP BY techgroupkey 
HAVING COUNT(*) > 1   

SELECT *
FROM brTechGroup a
INNER JOIN (
  SELECT techkey, weightfactor, COUNT(*)
  FROM brtechgroup
  GROUP BY techkey, weightfactor
  HAVING COUNT(*) > 1) b ON a.techkey = b.techkey AND a.weightfactor = b.weightfactor

SELECT a.techgroupkey, b.techgroupkey, COUNT(*)
FROM (  
  SELECT *
  FROM brTechGroup a
  WHERE a.weightfactor = .5) a
LEFT JOIN (
  SELECT *
  FROM brTechGroup
  WHERE weightfactor = .5) b ON  a.techkey <> b.techkey
GROUP BY a.techgroupkey, b.techgroupkey 
HAVING COUNT(*) > 1  

SELECT *
FROM brtechgroup
WHERE techkey = 22
  AND weightfactor = .5

Just fucking can NOT see it


-- without the problem, IF the GROUP COUNT ON this query (ro/line/techgroup) = the number of techs, it's a match ?
SELECT b.ptdate,a.ptco#, a.ptro#, a.ptline, c.techkey, 
  round(techlinehours/linehours, 4) AS wf, d.*
FROM (    
  SELECT ptco#, ptro#, ptline, pttech, sum(ptlhrs) AS techlinehours
  FROM tmpsdprdet
  WHERE ptltyp = 'L'
    AND ptcode = 'TT' 
--    AND ptro# = '18024255' AND ptline = 1
  GROUP BY ptco#, ptro#, ptline, pttech) a  
LEFT JOIN (
  SELECT min(ptdate) as ptdate, ptco#, ptro#, ptline, sum(ptlhrs) AS linehours
  FROM tmpsdprdet
  WHERE ptltyp = 'L'
    AND ptcode = 'TT' 
--    AND ptro# = '18024255' AND ptline = 1
  GROUP BY ptco#, ptro#, ptline) b ON a.ptco# = b.ptco# AND a.ptro# = b.ptro# AND a.ptline = b.ptline and linehours <> 0
LEFT JOIN dimtech c ON a.ptco# = c.storecode AND a.pttech = c.technumber
  AND b.ptdate BETWEEN c.techkeyfromdate AND c.techkeythrudate  
INNER JOIN brtechgroup d ON c.techkey = d.techkey
  AND round(techlinehours/linehours, 4) = d.weightfactor
where linehours <> 0  

-- way the fuck too many weightfactor 1
select techkey, count(*)
from brTechGroup
where weightfactor = 1
group by techkey having count(*) > 1
order by count(*) desc 

-- 
select *
from brTechGroup
where weightfactor = 1
  and techkey in (
    select techkey
    from brTechGroup
    where weightfactor = 1
    group by techkey having count(*) > 1)
order by techkey