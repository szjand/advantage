
--< what the fuck IS up with slow queries --------------------------------------------------------------------------------------------------------------------

-- this IS really fucking goofy

-- 30 mSec
SELECT b.thedate, a.ro, line
FROM factrepairorder a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN dimServiceType c ON a.servicetypekey = c.servicetypekey
WHERE b.thedate BETWEEN curdate() - 50 AND curdate()
-- 30 FUCKING SECONDS (IF 50 days OR more)
SELECT b.thedate, a.ro, line
FROM factrepairorder a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN dimpaymenttype c ON a.paymenttypekey = c.paymenttypekey
WHERE b.thedate BETWEEN curdate() - 50 AND curdate()

-- 300 mSec
SELECT b.thedate, a.ro, line
FROM factrepairorder a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN dimServiceType c ON a.servicetypekey = c.servicetypekey
WHERE b.theyear = 2013
-- 16 FUCKING SECONDS
SELECT b.thedate, a.ro, line
FROM factrepairorder a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN dimpaymenttype c ON a.paymenttypekey = c.paymenttypekey
WHERE b.theyear = 2013

-- this ONe IS ok
SELECT b.thedate, a.ro, line
FROM factrepairorder a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN dimlinestatus c ON a.statuskey = c.linestatuskey
WHERE b.thedate BETWEEN curdate() - 50 AND curdate()
-- this one IS ok
SELECT b.thedate, a.ro, line
FROM factrepairorder a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN dimrostatus c ON a.rostatuskey = c.rostatuskey
WHERE b.thedate BETWEEN curdate() - 50 AND curdate()

-- WTF, this one IS OK
SELECT b.thedate, a.ro, line
FROM factrepairorder a
INNER JOIN day b ON a.opendatekey = b.datekey
INNER JOIN dimServiceType c ON a.servicetypekey = c.servicetypekey
INNER JOIN dimlinestatus d ON a.statuskey = d.linestatuskey
INNER JOIN dimrostatus e ON a.rostatuskey = e.rostatuskey
INNER JOIN dimpaymenttype f ON a.paymenttypekey = f.paymenttypekey
WHERE b.thedate BETWEEN curdate() - 90 AND curdate()


--/> what the fuck IS up with slow queries -------------------------------------------------------------------------------------------------------------------

-- 9/23

-- 10 mSec
SELECT a.storecode, a.ro, a.line, g.opcode, left(g.description, 50), 'QL', b.thedate
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey
-- ADD the date limit AND BOOM
-- 25 sec
SELECT a.storecode, a.ro, a.line, g.opcode, left(g.description, 50), 'QL', b.thedate
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 30
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey
-- this IS no better
SELECT *
FROM (
  SELECT a.storecode, a.ro, a.line, g.opcode, left(g.description, 50), 'QL', b.thedate
  FROM factrepairorder a
  INNER JOIN day b on a.opendatekey = b.datekey
  --  AND b.thedate > curdate() - 30
  INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
    AND f.servicetypecode = 'ql' 
  INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey)  x
WHERE thedate > curdate() - 30   
-- 10 mSec
SELECT a.storecode, a.ro
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 30
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey  
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey    
-- 25 Sec
SELECT a.storecode, a.ro
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 30
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey  
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
-- 25 sec 
SELECT a.storecode, a.ro
FROM factrepairorder a
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey  
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 30    
  

-- 10 mSec
SELECT a.storecode, b.*, c.*
FROM factrepairorder a
INNER JOIN dimServiceType b on a.servicetypekey = b.servicetypekey  
  AND b.servicetypecode = 'ql' 
INNER JOIN dimPaymentType c on a.PaymentTypeKEy = c.PaymentTypeKey   
  AND c.PaymentTypeCode = 'I'
INNER JOIN day d on a.finalclosedatekey = d.datekey

-- 20 Sec
SELECT a.storecode, b.*, c.*
FROM factrepairorder a
INNER JOIN dimServiceType b on a.servicetypekey = b.servicetypekey  
  AND b.servicetypecode = 'ql' 
INNER JOIN dimPaymentType c on a.PaymentTypeKEy = c.PaymentTypeKey   
  AND c.PaymentTypeCode = 'I'
INNER JOIN day d on a.finalclosedatekey = d.datekey
WHERE d.thedate BETWEEN curdate() - 90 AND curdate()

-- LEFT JOIN AND WHERE clause
-- 10 msec
SELECT a.storecode, b.*, c.*
FROM factrepairorder a
LEFT JOIN dimServiceType b on a.servicetypekey = b.servicetypekey  
LEFT JOIN dimPaymentType c on a.PaymentTypeKEy = c.PaymentTypeKey   
LEFT JOIN day d on a.finalclosedatekey = d.datekey
-- 100 msec
SELECT a.storecode, b.*, c.*
FROM factrepairorder a
LEFT JOIN dimServiceType b on a.servicetypekey = b.servicetypekey  
LEFT JOIN dimPaymentType c on a.PaymentTypeKey = c.PaymentTypeKey   
LEFT JOIN day d on a.finalclosedatekey = d.datekey
WHERE b.servicetypecode = 'ql'
  AND c.PaymentTypeCode = 'I' 
-- add the date range BOOM 40 fucking seconds
SELECT a.storecode, b.*, c.*
FROM factrepairorder a
LEFT JOIN dimServiceType b on a.servicetypekey = b.servicetypekey  
LEFT JOIN dimPaymentType c on a.PaymentTypeKey = c.PaymentTypeKey   
LEFT JOIN day d on a.finalclosedatekey = d.datekey
WHERE b.servicetypecode = 'ql'
  AND c.PaymentTypeCode = 'I'
  AND d.thedate > curdate() - 60
  
-- this actually cuts it down to 4 sec, even though the plan shows red
SELECT *
FROM (  
  SELECT *
  FROM factRepairOrder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
  AND b.thedate BETWEEN curdate() - 90 AND curdate()) a
INNER JOIN dimServiceType b on a.servicetypekey = b.servicetypekey  
  AND b.servicetypecode = 'ql' 
INNER JOIN dimPaymentType c on a.PaymentTypeKEy = c.PaymentTypeKey   
  AND c.PaymentTypeCode = 'I'  
  
SELECT *
FROM (  
  SELECT *
  FROM factRepairOrder a
  INNER JOIN day b on a.finalclosedatekey = b.datekey
  AND b.yearmonth BETWEEN 201307 and 201309) a
INNER JOIN dimServiceType b on a.servicetypekey = b.servicetypekey  
--  AND b.servicetypecode = 'ql' 
INNER JOIN dimPaymentType c on a.PaymentTypeKEy = c.PaymentTypeKey   
--  AND c.PaymentTypeCode = 'I'   


SELECT a.storecode, a.ro
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 30
INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey  
--  AND b.servicetypecode = 'ql' 
INNER JOIN dimPaymentType d on a.PaymentTypeKEy = d.PaymentTypeKey   
--  AND c.PaymentTypeCode = 'I'  

factRepairOrder: 1,054,677
day 35,794

BigTable: 1 million rows
MediumTable: 40K rows
SmallTable1: 10 rows
SmallTable2: 5 rows

-- < 10 mSec
SELECT a.storecode, a.ro
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 30
  
SELECT a.field1, a.field2
FROM BigTable a
INNER JOIN MediumTable b on a.Field3 = b.Field3
  AND b.Field4 = 'wtf' -- Limiting MediumTable to Field4 = 'wtf' results IN 90 rows 

SELECT a.storecode, a.ro
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.thedate > curdate() - 30
INNER JOIN dimServiceType c on a.servicetypekey = c.servicetypekey  
--  AND c.servicetypecode = 'ql'
INNER JOIN dimPaymentType d on a.PaymentTypeKey = d.PaymentTypeKey     

-- hmm it appears IF there are multiple INNER joins, the slow down happens
-- WHEN there are "AND" clauses to more than one of the joins   

-- 9/24
-- TRY another mid size TABLE besides day
-- dimOpcode does NOT have the same effect
-- 10 mSec
SELECT a.storecode, a.ro, a.line, g.*
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
--  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN dimOpcode g on a.opcodekey = g.opcodekey  
  AND (g.opcode LIKE 'pdq%' OR g.opcode LIKE '%nt%')
  
-- pick another
-- works just fine, 10 mSec
SELECT a.storecode, a.ro, a.line, g.*
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
--  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
INNER JOIN dimCCC g on a.ccckey = g.ccckey  
  AND g.complaint = 'N/A'
  
-- ok, make the advantage post for help  
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
  
-- 10 mSec
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.yearmonth = 201309  
-- 10 mSec  
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql'   
-- 10 mSec  
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
--  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql'  
   
-- 12 Sec  
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql'  

 -- holy shit, looks LIKE multiple ANDs IS the issue
-- 10 mSec 
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
--  AND f.servicetypecode = 'ql'  

-- 17 Sec
-- use this one
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql'  

Tables: 
Fact: 1,050,000 rows
dimDay: 40,000 rows
dim2: 10 rows

SELECT a.field1, a.field2
FROM Fact a
INNER JOIN dimDay b on a.dateKey = b.dateKey
  AND b.yearMonth = 201309
INNER JOIN dim2 c on a.dim2Key = c.dim2Key    


SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
  
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
--  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql' 
  
-- gregs suggestions

SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
  AND f.servicetypecode = 'ql'   

-- reverse ORDER of JOIN keys 
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on b.datekey = a.opendatekey
  AND b.yearmonth = 201309
INNER JOIN dimServiceType f on f.servicetypekey = a.servicetypekey
  AND f.servicetypecode = 'ql'    

-- constants first IN joins  
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on b.yearmonth = 201309 
  AND a.opendatekey = b.datekey
INNER JOIN dimServiceType f on f.servicetypecode = 'ql'
  AND a.servicetypekey = f.servicetypekey   
  
  
SELECT a.storecode, a.ro, a.line
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimServiceType f on a.servicetypekey = f.servicetypekey  
WHERE b.yearmonth = 201309
  AND f.servicetypecode = 'ql'   
  
-- 9/25 sent to advantage
These are snappy:
SELECT a.storeCode, a.ro, a.line
FROM factRepairOrder a
INNER JOIN Day b on a.openDateKey = b.datekey
INNER JOIN dimServiceType c on a.serviceTypeKey = c.serviceTypeKey;

SELECT a.storeCode, a.ro, a.line
FROM factRepairOrder a
INNER JOIN Day b on a.openDateKey = b.datekey
INNER JOIN dimServiceType c on a.serviceTypeKey = c.serviceTypeKey 
WHERE b.yearMonth = 201309; 

SELECT a.storeCode, a.ro, a.line
FROM factRepairOrder a
INNER JOIN Day b on a.openDateKey = b.datekey
INNER JOIN dimServiceType c on a.serviceTypeKey = c.serviceTypeKey  
WHERE c.serviceTypeCode = 'ql';   
This is not:   
SELECT a.storeCode, a.ro, a.line
FROM factRepairOrder a
INNER JOIN Day b on a.openDateKey = b.datekey
INNER JOIN dimServiceType c on a.serviceTypeKey = c.serviceTypeKey  
WHERE b.yearMonth = 201309
  AND c.serviceTypeCode = 'ql';   

dimServiceType can be replaced with dimPaymentType and get the same type of results

But, here is a twist, these are snappy !?!?:
SELECT a.storeCode, a.ro, a.line
FROM factRepairOrder a
INNER JOIN dimDay b on a.openDateKey = b.datekey
INNER JOIN dimRoStatus c on a.roStatusKey = c.roStatusKey
WHERE c.roStatusCode = '2'
  AND b.yearMonth = 201309;

SELECT a.storeCode, a.ro, a.line
FROM factRepairOrder a
INNER JOIN dimDay b on a.openDateKey = b.datekey
INNER JOIN dimOpCode c on a.opcodeKey = c.opcodeKey
WHERE c.opcode LIKE 'PDQ%'
  AND b.yearMonth = 201309;

I am so confused and pretty much convinced that I am missing something obvious, I just don�t know what.

-- 10/7/13
-- found a reasonable workaround


-- this IS a typically killer slow query: > 20 seconds
SELECT a.ro, b.thedate, c.paymenttype, d.servicetype 
FROM factrepairorder a
INNER JOIN day b on a.finalclosedatekey = b.datekey
INNER JOIN dimpaymenttype c on a.paymenttypekey = c.paymenttypekey
INNER JOIN dimServiceType d on a.ServiceTypeKey = d.ServiceTypeKey
WHERE b.yearmonth IN (201309,201308,201307)

-- here's what works: 200 mSec
SELECT a.ro, e.thedate, c.paymenttype, d.servicetype 
FROM factrepairorder a
INNER JOIN dimpaymenttype c on a.paymenttypekey = c.paymenttypekey
INNER JOIN dimServiceType d on a.ServiceTypeKey = d.ServiceTypeKey
LEFT JOIN day e on a.finalclosedatekey = e.datekey
WHERE a.finalclosedatekey IN (SELECT datekey FROM day WHERE yearmonth IN (201309,201308,201307))
  