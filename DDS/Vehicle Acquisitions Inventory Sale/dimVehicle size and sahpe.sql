SELECT a.*
FROM dimVehicle a
LEFT JOIN dps.MakeModelClassifications b on a.make = b.make 
  AND a.model = b.model
  
  
-- shape AND size for dimVehicle  
SELECT * FROM (
SELECT aaa.stocknumber, aa.vehiclekey, aa.make, aa.model, aa.modelyear, aa.vin,
  coalesce(b.description, 'unknown') AS size, 
  coalesce(c.description, 'unknown') AS shape,
  left(d.vindivisionname,2) AS chrMake, left(d.vinmodelname, 20) as chrModel,
  h.description AS dpsSize, i.description AS dpsShape, g.model AS dpsModel
FROM factVehicleInventory aaa  
INNER JOIN dimVehicle aa on aaa.vehiclekey = aa.vehiclekey
LEFT JOIN dps.MakeModelClassifications a on aa.make = a.make AND aa.model = a.model
LEFT JOIN dps.typDescriptions b on a.vehiclesegment = b.typ
LEFT JOIN dps.typdescriptions c on a.vehicletype = c.typ  
LEFT JOIN chrome.vinpattern d on LEFT(aa.vin,8) = LEFT(d.vinpattern,8)
  AND aa.vin LIKE replace(d.vinpattern, '*', '_')
LEFT JOIN dps.VehicleInventoryItems e on aaa.stocknumber = e.stocknumber
LEFT JOIN dps.VehicleItems f on e.VehicleItemID = f.VehicleItemID 
LEFT JOIN dps.MakeModelClassifications g on f.make = g.make AND f.model = g.model
LEFT JOIN dps.typDescriptions h on g.vehiclesegment = h.typ
LEFT JOIN dps.typdescriptions i on g.vehicletype = i.typ  
) x WHERE size = 'unknown'



SELECT *
FROM factvehiclesale
WHERE stocknumber = 'H6023B'

 