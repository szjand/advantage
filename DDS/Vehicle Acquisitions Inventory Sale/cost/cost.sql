

SELECT gtjrnl, gtdate, gtacct, gttamt
FROM stgArkonaGLPTRNS
WHERE gtctl# = '22798XX'
  AND gtjrnl = 'pvu'
  
-- current cost  
SELECT SUM(gttamt)
FROM stgArkonaGLPTRNS
WHERE gtctl# = '22798XX'
  AND gttamt > 0
  AND gtacct = '124100'



SELECT a.*, b.gmdesc
FROM (  
  SELECT gtacct, COUNT(*) AS theCount
  FROM stgArkonaGLPTRNS  
  WHERE year(gtdate) = 2015
    AND gtjrnl IN ('pvi','pvu')
  GROUP BY gtacct) a   
LEFT JOIN stgArkonaGLPMAST b on a.gtacct = b.gmacct
  AND b.gmyear = 2015
ORDER BY theCount DESC 
