/*
2-2-15
at least FROM the standpoint of generating past history, this may be the easiest
the whole exercise of trying to extract historical acquisition data FROM arkona
was driving me nuts

particularly the derived acquisition date for sales
 
may want to verify/UPDATE dps info with arkona data, at least for current stuff
because the dps data IS NOT always entered IN realtime, eg the weekly cross off review fixes


*/
-- 2/26 eliminate crookston sales
-- DROP TABLE #dpsSales;
select b.stocknumber, f.vin, CAST(a.soldts AS sql_date) AS saleDate, 
  left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20) AS saleType,
  left(trim(substring(a.status, position('_' IN a.status) + 1, length(TRIM(a.status)) - position('_' IN a.status))), 20) AS saleStatus, 
  left(coalesce(e.fullname, left(e.fullname, 16), a.soldto), 30) AS soldTo, 
  left(trim(substring(c.typ, position('_' IN c.typ) + 1, length(TRIM(c.typ)) - position('_' IN c.typ))), 20) AS acqTyp,
  CAST(b.fromts AS sql_date) AS acqDate, replace(b.stocknumber, 'y','') AS noy
INTO #dpsSales  
FROM dps.vehicleSales a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dps.organizations bb on b.owninglocationid = bb.partyid
  AND bb.name IN ('Rydells','Honda Cartiva')
INNER JOIN dps.vehicleAcquisitions c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN dps.typDescriptions d on a.typ = d.typ
LEFT JOIN dps.organizations e on a.soldto = e.partyid collate ads_default_ci
LEFT JOIN dps.VehicleItems f on b.VehicleItemID = f.VehicleItemID 
LEFT JOIN dps.typDescriptions g on c.typ = g.typ
--ORDER BY a.soldts DESC 


-- DROP TABLE #factSales;
SELECT b.thedate AS saleDate, a.storecode, a.stocknumber, c.vin, e.fullname as buyer, 
  d.vehicleType, d.saleType, replace(a.stocknumber, 'y','') AS noy
INTO #factSales  
FROM factVehicleSale a
INNER JOIN day b on a.approvedDateKey = b.datekey
INNER JOIN dimVehicle c on a.vehicleKey = c.vehicleKey
INNER JOIN dimCarDealInfo d on a.carDealInfoKey = d.carDealInfoKey
INNER JOIN dimCustomer e on a.buyerkey = e.customerkey


-- IN dps, NOT fact
SELECT year(a.saledate), COUNT(*) 
FROM #dpsSales a
LEFT JOIN #factSales b on a.noy = b.noy
WHERE b.noy IS NULL  
  AND year(a.saledate) > 2009
GROUP BY year(a.saledate)

-- BY far, the largest portion of IN dps NOT IN fact are the intra market wholesales
SELECT year(a.saledate) AS thYear, a.saletype, 
  CASE WHEN trim(a.saletype) = 'Wholesale' THEN a.soldto
  ELSE 'retail'
  END AS soldto, COUNT(*)
FROM #dpsSales a
LEFT JOIN #factSales b on a.noy = b.noy
WHERE b.noy IS NULL  
  AND year(a.saledate) > 2009
GROUP BY year(a.saledate), a.saletype, 
  CASE WHEN trim(a.saletype) = 'Wholesale' THEN a.soldto
  ELSE 'retail'
  END  

 
SELECT *
FROM (  
  SELECT year(a.saledate) AS theYear, COUNT(*) 
  FROM #dpsSales a
  LEFT JOIN #factSales b on a.noy = b.noy
  WHERE b.noy IS NULL  
    AND year(a.saledate) > 2009
  GROUP BY year(a.saledate)) c
LEFT JOIN (
  SELECT year(a.saledate) AS theYear, a.saletype, COUNT(*)
  FROM #dpsSales a
  LEFT JOIN #factSales b on a.noy = b.noy
  WHERE b.noy IS NULL
    AND year(a.saledate) > 2009
  GROUP BY year(a.saledate), a.saletype) d on c.theyear = d.theyear
LEFT JOIN (
  SELECT year(a.saledate) AS theYear, a.saletype, 
    CASE WHEN trim(a.saletype) = 'Wholesale' THEN a.soldto
    ELSE 'retail'
    END AS soldto, COUNT(*)
  FROM #dpsSales a
  LEFT JOIN #factSales b on a.noy = b.noy
  WHERE b.noy IS NULL  
    AND year(a.saledate) > 2009
  GROUP BY year(a.saledate), a.saletype, 
  CASE WHEN trim(a.saletype) = 'Wholesale' THEN a.soldto
  ELSE 'retail'
  END) e on d.theyear = e.theyear
    AND d.saletype = e.saletype
    
-- fact VehicleSale IS RY1 & RY2 only

-- IN fact, NOT IN dps  
-- NOT too bad
SELECT year(a.saledate) , COUNT(*),
  SUM(CASE WHEN a.stocknumber = '' THEN 1 ELSE 0 END) AS noStock
FROM #factSales a
LEFT JOIN #dpsSales b on a.noy = b.noy
WHERE a.vehicleType = 'used'
  AND b.noy IS NULL 
GROUP BY year(a.saledate)  

SELECT a.*, c.*
FROM #factSales a
LEFT JOIN #dpsSales b on a.noy = b.noy
LEFT JOIN dps.VehicleInventoryItems  c on a.stocknumber = c.stocknumber
WHERE a.vehicleType = 'used'
  AND b.noy IS NULL 
  AND year(a.saledate) = 2015

  
SELECT * FROM stgarkonabopmast WHERE bmvin = '1GTR2WE73CZ225135'  
select * FROM stgArkonaINPMAST WHERE imvin = '1GTR2WE73CZ225135'

-- #factSales rows with empty stocknumber
select *
-- SELECT COUNT(*) -- 37 of the 82 are used
FROM #factSales 
WHERE stocknumber = ''
  AND vehicletype = 'used'
ORDER BY saledate  

SELECT * FROM #dpsSales

SELECT acqTyp, suffix, COUNT(*)
FROM (
SELECT a.acqTyp,
replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix
FROM #dpsSales a  
) x GROUP BY acqtyp, suffix

SELECT acqTyp, COUNT(*)
FROM #dpsSales
GROUP BY acqTyp

SELECT a.typ, COUNT(*)
-- SELECT *
FROM dps.vehicleAcquisitions a
GROUP BY typ

-- vehicleAcquisitions with no corresponding row IN VehicleInventoryItems 
-- this IS of no concern, vehicleAcquisitions are used based on an INNER JOIN to vehicleSales
--
select COUNT(*) -- 241 of 27258
-- SELECT *
FROM dps.VehicleAcquisitions a
LEFT JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE b.VehicleInventoryItemID IS NULL 


-- only one vehicle sale without an a vehicle acquisition record
SELECT * 
FROM dps.vehiclesales a
LEFT JOIN dps.vehicleAcquisitions b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE b.VehicleInventoryItemID IS NULL 

-- only 2 vehicle sales with no VehicleInventoryItems row
SELECT * 
FROM dps.vehiclesales a
LEFT JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE b.VehicleInventoryItemID IS NULL  

-- there are no VehicleInventoryItems without an acquisition record
SELECT COUNT(*)
-- SELECT *
FROM dps.VehicleInventoryItems a
LEFT JOIN dps.VehicleAcquisitions b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE b.VehicleInventoryItemID IS NULL 

-- dps sales with no VehicleInventoryItems record
SELECT COUNT(*) -- 2 of 26992
-- SELECT *
FROM dps.vehicleSales a
LEFT JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
WHERE b.VehicleInventoryItemID IS NULL 

-- 2/28/15
-- ADD intra market wholesales to factVehicleSales

SELECT * FROM #factSales
select * FROM #dpsSales

so, how DO i know a TRANSACTION was an intramarket ws?
-- vehicleSales.typ only shows retail/wholesale
select * FROM dps.vehiclesales ORDER BY soldts desc
SELECT typ, COUNT(*) FROM dps.vehiclesales GROUP BY typ
-- vehicleSales.SoldTo IS one way
SELECT d.fullname as seller, b.fullname AS buyer, year(cast(a.soldts AS sql_date)), COUNT(*)
FROM dps.vehicleSales a
INNER JOIN dps.organizations b on a.soldto = b.partyid collate ads_Default_ci
INNER JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
INNER JOIN dps.organizations d on c.owninglocationid = d.partyid
WHERE b.fullname IS NOT NULL 
group by d.fullname, b.fullname, year(cast(a.soldts AS sql_date))

-- vehicleAcquisitions.typ IS another
select * FROM dps.vehicleAcquisitions
/*
-- a little clean up
UPDATE dps.VehicleAcquisitions
SET typ = 'VehicleAcquisition_DealerPurchase'
WHERE typ = 'VehicleAcquisition_DealerPurch'

UPDATE dps.VehicleAcquisitions
SET typ = 'VehicleAcquisition_IntraMarketPurchase'
WHERE typ = 'VehicleAcquisition_IntraMarket'
*/
select a.typ, min(cast(b.fromts AS sql_date)), max(cast(b.fromts AS sql_date)), COUNT(*) 
FROM dps.vehicleAcquisitions a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
GROUP BY a.typ

IN my normal OCD manner, i want to see how intra mkt ws match up, compare 
those derived FROM vehicleSales vs those derived FROM vehicleAcquisitions
let us take 2014 AS a test CASE

--first vehicleSales
SELECT c.stocknumber, cc.vin, left(d.fullname, 20) as seller, left(b.fullname, 20) AS buyer, 
  cast(a.soldts AS sql_date) AS saleDate
INTO #vs  
-- SELECT COUNT(*)
FROM dps.vehicleSales a
INNER JOIN dps.organizations b on a.soldto = b.partyid collate ads_Default_ci
  AND b.fullname IN ('GF-Honda Cartiva','GF-Rydells')
INNER JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
INNER JOIN dps.VehicleItems cc on c.VehicleItemID = cc.VehicleItemID 
INNER JOIN dps.organizations d on c.owninglocationid = d.partyid
WHERE b.fullname IS NOT NULL 
  AND year(cast(a.soldts AS sql_date)) = 2014;
  
-- AND vehicleAcquisitions
-- DROP TABLE #va;
SELECT b.stocknumber, c.vin, left(d.fullname, 20) as seller, left(e.fullname, 20) AS buyer,
  CAST(b.fromts AS sql_date) AS acqDate
-- SELECT COUNT(*)
INTO #va
FROM dps.vehicleAcquisitions a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND year(CAST(b.fromts AS sql_date)) = 2014
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID  
INNER JOIN dps.organizations d on a.acquiredfrompartyid = d.partyid
INNER JOIN dps.organizations e on b.owninglocationid = e.partyid
WHERE a.typ = 'VehicleAcquisition_IntraMarketPurchase';

-- shit looks pretty good, ALL seem to match 
SELECT *
-- SELECT COUNT(*)
FROM #va a
full OUTER JOIN #vs b on a.vin = b.vin
  AND abs(saledate-acqdate) < 3
  
so, what IS the reasoning to be used
start with the sale  

ok, the OCD IS passified
need to ADD intra mkt ws to factVehicleSales

      storeCode CIChar( 3 ),
      bmkey Integer, ------------
      stockNumber CIChar( 9 ),
      carDealInfoKey Integer,---- need new identifier for imws
      vehicleKey Integer,
      originationDateKey Integer, --- ALL 3 of these will be the sale date
      approvedDateKey Integer,    ---
      cappedDateKey Integer,      ---
      buyerKey Integer, --- figure out what to use for dealer customer key
      coBuyerKey Integer, -- na
      consultantKey Integer, -- ?? 
      managerKey Integer, -- vehiclesales.managerpartyid ??
      vehicleCost Numeric( 12 ,2 ), -- ??
      vehiclePrice Numeric( 12 ,2 ))  -- soldAmount

dps.VehicleSales started recording intra market wholesales IN 07/2010
      
SELECT * FROM dps.vehiclesales     

SELECT c.stocknumber, cc.vin, left(d.fullname, 20) as seller, left(b.fullname, 20) AS buyer, 
  cast(a.soldts AS sql_date) AS saleDate
-- SELECT *
FROM dps.vehicleSales a
INNER JOIN dps.organizations b on a.soldto = b.partyid collate ads_Default_ci
  AND b.fullname IN ('GF-Honda Cartiva','GF-Rydells')
INNER JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
INNER JOIN dps.VehicleItems cc on c.VehicleItemID = cc.VehicleItemID 
INNER JOIN dps.organizations d on c.owninglocationid = d.partyid
WHERE b.fullname IS NOT NULL 
ORDER BY soldts DESC 

-- ALL real world used car combinations FROM dimCarDealInfo
SELECT b.dealStatus,b.dealType,b.vehicleType,b.saleType, COUNT(*)
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey
  AND b.vehicleType = 'Used'
GROUP BY b.dealStatus,b.dealType,b.vehicleType,b.saleType

so, IS intra mkt ws a deal type OR a sale type?
sale type because deal type has to DO with funding, which IS NOT variable IN
intra market wholesales.
ADD the appropriate record to dimCarDealInfo IN Z:\E\DDS2\Scripts\dimCarDealInfo\dimCarDealInfro.sql

Customer Keys for (these are somewhat arbitrary, multiple customerkeys IN dimCustomer
    for each store, the arbitrary choice makes me nervous)
  buyerKey : RY1: 9019
             RY2: 12
             RY3: 175
  coBuyerKey : 180432 (N/A)
  
salesConsultant keys for
  consultantKey : RY1: 246 (UNK), RY2: 245 (UNK)
  managerKey
need to link vehicleSales.managerpartyid -> people -> dimSalesPerson
that IS NOT working out very well at all 
fuck it: ALL consultant/managerKey = 246
  
  
bmkey: -1, since there IS no record IN BOPMAST for this deal  
fuck, that will NOT WORK
factVehicleSale PK: storecode,bmkey
changed to storecode,bmkey,stocknumber

SELECT storecode, bmkey, stocknumber 
FROM factVehicleSale
GROUP BY storecode, bmkey, stocknumber  HAVING COUNT(*) > 1

-- III. A) INSERT IMWS INTO factVehicleSale      
-- SELECT stocknumber FROM ( 
-- SELECT * FROM (     
-- 3/1/15 - this may be good enuf for now, at least for back fill
-- going to accept this AS good enuf for factVehicleSale
INSERT INTO factVehicleSale (storecode,bmkey,stockNumber,carDealInfoKey,
    vehicleKey,originationDateKey,approvedDateKey,cappedDateKey,buyerKey,
    coBuyerKey,consultantKey,managerKey,vehicleCost,vehiclePrice,noY)
SELECT * FROM (
  SELECT 
    case d.fullname
      WHEN 'GF-Crookston' THEN 'RY3'
      WHEN 'GF-Honda Cartiva' THEN 'RY2'
      WHEN 'GF-Rydells' THEN 'RY1'
    END AS storeCode, 
    -1 AS bmkey, left(c.stocknumber, 9) AS stocknumber, 
    (select carDealInfoKey from dimCarDealInfo where saleTypeCode = 'I') as carDealInfoKey,
    e.vehicleKey, 
    f.datekey AS originationDateKey, f.datekey AS approvedDateKey, f.datekey AS cappedDateKey,
    case b.fullname
      WHEN 'GF-Crookston' THEN 175
      WHEN 'GF-Honda Cartiva' THEN 12
      WHEN 'GF-Rydells' THEN 9019
    END AS buyerKey,
    1084332 AS coBuyerKey,
    246 AS consultantKey, 246 AS managerKey, 
    0 AS vehicleCost,
    a.soldAmount AS vehiclePrice,
    replace(left(c.stocknumber, 9),'Y','') AS noY
  -- SELECT COUNT(*)
  -- DROP TABLE #wtf;
  -- INTO #wtf
  FROM dps.vehicleSales a
  INNER JOIN dps.organizations b on a.soldto = b.partyid collate ads_Default_ci
    AND b.fullname IN ('GF-Honda Cartiva','GF-Rydells')
  INNER JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  INNER JOIN dps.VehicleItems cc on c.VehicleItemID = cc.VehicleItemID 
  INNER JOIN dps.organizations d on c.owninglocationid = d.partyid
  LEFT JOIN dimVehicle e on cc.vin = e.vin
  LEFT JOIN day f on CAST(a.soldts AS sql_date) = f.thedate
  WHERE b.fullname IS NOT NULL   
    AND a.status = 'VehicleSale_Sold') x -- eliminate canceled sales   
WHERE vehicleKey IS NOT NULL 
  AND NOT EXISTS (
    SELECT 1
    FROM factVehicleSale
    WHERE stocknumber = x.stocknumber)     
--) x GROUP BY stocknumber HAVING COUNT(*) > 1
--) x WHERE stocknumber IN ('h5985a','h7564a')


-- 3/2/15
-- the different types of used car sales IN factVehicleSales
SELECT *
FROM (
  SELECT carDealInfoKey, COUNT(*) AS howMany
  FROM factVehicleSale
  GROUP BY carDealInfoKey) a
LEFT JOIN dimCarDealInfo b on a.carDealInfoKEy = b.carDealInfoKey
WHERE b.vehicleTypeCode = 'u'

ok, now, from the sales first approach,
need an acquisition record for each sale IN factVehicleSale
-- DROP TABLE #wtf;
SELECT c.thedate, a.*, b.*
INTO #wtf
FROM factVehicleSale a
INNER JOIN extArkonaBOPTRAD b on a.stocknumber = b.stocknumber
INNER JOIN day c on a.approvedDateKey = c.datekey
WHERE a.cardealInfoKey IN (
  SELECT carDealInfoKey
  FROM dimCarDealInfo
  WHERE vehicleTypeCode = 'U')
AND c.thedate BETWEEN '01/01/2012' AND curdate()  ;

am i updating BOPTRAD (extArkonaBOPTRAD) daily?  nope 

SELECT *
FROM #wtf
WHERE vin IS NULL 


SELECT * FROM factVehicleSale

SELECT acqCategory, acqCategoryClassification, datasource, COUNT(*)
FROM (
SELECT *  FROM tmpAisArkAcq
union
SELECT * FROM tmpAisDpsAcq) a
GROUP BY acqCategory, acqCategoryClassification,datasource
  
SELECT acqCategory, acqCategoryClassification, COUNT(*)
FROM (
SELECT *  FROM tmpAisArkAcq
union
SELECT * FROM tmpAisDpsAcq) a
GROUP BY acqCategory, acqCategoryClassification
    
  


-- see starting over again 2-1-15.sql

-- 3/3
what IS the ORDER of precedence for determining acquisition attributes for the
vehicles IN factVehicleSales
  BOPTRAD
  
but more fundamentally, what does the factVehicleAcquisition TABLE look like?  
hanging up on acq TABLE being the source for wash sheet, which probably does
NOT make sense IN its entirety, yes wash for WHERE it came FROM, but NOT wash
for what it eventually generates
except, maybe an attribute sourceVehicleKey, which becomes the next hierarchy
puzzle to solve (how many self joins to exhaust the wash)

storecode
stocknumber
?? acquisitionInfoKey: acqCategory, acqCategoryClassification,  
vehicleKey
dateKey
sourceVehicleKey
cost


wanting to make acquisitionInfoKey a junk dimension, LIKE dimCarDealInfo for factVehicleSale

vehType acqCat
new     factory
new     dealer trade -- how DO i know the source of a new car, fuck IF i know
used    purchase
used    trade
-- take new vehicles out of the mix, shit what about rentals, i don't fucking know

vehType acqCat     acqCatClass
new     NA         NA
used    purchase   Auction  
used    purchase   Dealer Purchase
used    purchase   IN AND Out
used    purchase   Intra Market WS
used    purchase   Lease Purchase
used    purchase   Service Loaner
used    purchase   Driver Training  
used    purchase   Repossession
used    purchase   Return
used    purchase   Street
used    purchase   Unknown
used    trade      new
used    trade      used


THEN, i am thinking, IN the fact TABLE have a sourceVehicleKey, which would mean
that dimVehicle would have to have an NA row, since this value would only
have meaning for used cars acquired through a trade

-- DROP TABLE dimVehicleAcquisitionInfo;
CREATE TABLE dimVehicleAcquisitionInfo (
  vehicleAcquisitionInfoKey AutoInc,
  vehicleType cichar(4) constraint NOT NULL, -- new/used
  acquisitionType cichar(10) constraint NOT NULL, -- purchase/trade - for now used only
  acquisitionTypeCategory cichar(24) constraint NOT NULL, -- used car purchases
  constraint PK primary key (vehicleAcquisitionInfoKey)) IN database;
-- compound unique index for non key fields (NK)
EXECUTE PROCEDURE sp_CreateIndex90( 'dimVehicleAcquisitionInfo', 
  'dimVehicleAcquisitionInfo.adi', 'NK', 'vehicleType;acquisitionType;acquisitionTypeCategory', 
  '', 2051, 512, NULL );
-- individual indices  
EXECUTE PROCEDURE sp_CreateIndex90( 'dimVehicleAcquisitionInfo', 
  'dimVehicleAcquisitionInfo.adi', 'vehicleType', 'vehicleType', 
  '', 2, 512, NULL );
EXECUTE PROCEDURE sp_CreateIndex90( 'dimVehicleAcquisitionInfo', 
  'dimVehicleAcquisitionInfo.adi', 'acquisitionType', 'acquisitionType', 
  '', 2, 512, NULL );  
EXECUTE PROCEDURE sp_CreateIndex90( 'dimVehicleAcquisitionInfo', 
  'dimVehicleAcquisitionInfo.adi', 'acquisitionTypeCategory', 'acquisitionTypeCategory', 
  '', 2, 512, NULL );  
  
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('New','NA','NA'); 
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Auction');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Dealer Purchase');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','In And Out');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Intra Market Wholesale');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Lease Purchase');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Service Loaner');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Driver Training');  
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Repossession');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Return');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Street');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Purchase','Unknown');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Trade','New');
INSERT INTO dimVehicleAcquisitionInfo (vehicleType,acquisitionType,acquisitionTypeCategory)
values('Used','Trade','Used');

-- NA vehicleKey = 151901
INSERT INTO dimVehicle (vin,modelyear,make,model,modelcode,body,color)
values ('NA','NA','NA','NA','NA','NA','NA');

-- ADD the stocknumber without the Y to factVehicleSale
-- the y IS used to flag a damaged title, but IS used inconsistently BETWEEN
-- the tool AND arkona
ALTER TABLE factVehicleSale
ADD COLUMN noY cichar(9);

UPDATE factVehicleSale
SET noY = replace(stocknumber,'Y','');

EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleSale', 
  'factVehicleSale.adi', 'noY', 'noY', 
  '', 2, 512, NULL ); 
  
-- for the initial load (at least) put off NOT NULL constraints
-- DROP TABLE factVehicleAcquisition;
CREATE TABLE factVehicleAcquisition(
storecode cichar(3),-- constraint NOT null,
stocknumber cichar(9),-- constraint NOT null,
noY cichar(9), -- constraint NOT null,
vehicleAcquisitionInfoKey integer,-- constraint NOT null,  
vehicleKey integer,-- constraint NOT null,
dateKey integer,-- constraint NOT null,
sourceVehicleKey integer,-- constraint NOT null,
cost double(2) default '0') in database;--  constraint NOT null,
-- individual indices  
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleAcquisition', 
  'factVehicleAcquisition.adi', 'stocknumber', 'stocknumber', 
  '', 2, 512, NULL );
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleAcquisition', 
  'factVehicleAcquisition.adi', 'noY', 'noY', 
  '', 2, 512,NULL );  
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleAcquisition', 
  'factVehicleAcquisition.adi', 'vehicleAcquisitionInfoKey', 'vehicleAcquisitionInfoKey', 
  '', 2, 512, NULL );    
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleAcquisition', 
  'factVehicleAcquisition.adi', 'vehicleKey', 'vehicleKey', 
  '', 2, 512, NULL );    
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleAcquisition', 
  'factVehicleAcquisition.adi', 'dateKey', 'dateKey', 
  '', 2, 512, NULL ); 
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleAcquisition', 
  'factVehicleAcquisition.adi', 'sourceVehicleKey', 'sourceVehicleKey', 
  '', 2, 512, NULL ); 

-- III. B)    
-- base rows for ALL sales
-- with the attributes for new car sales,
--  see below for the angst on acq date for new cars, IF NULL THEN 12/31/9999
--  leaving cost out for now, i envision this field to be the original cost,
--  AND i am NOT sure about WHERE OR how to FETCH that
-- EXECUTE PROCEDURE sp_packtable('factVehicleAcquisition');
DELETE FROM factVehicleAcquisition;
INSERT INTO factVehicleAcquisition (storecode,stocknumber,noY,vehicleKey,
  sourceVehicleKey, vehicleAcquisitionInfoKey, datekey)
SELECT f.storecode, f.stocknumber, f.noy, f.vehiclekey, 
  f.sourceVehicleKey, f.acqInfoKey, 
  CASE 
    WHEN f.vehicleType = 'New' THEN (
      SELECT datekey
      FROM day
      WHERE thedate = coalesce(f.acqDate, CAST('12/31/9999' AS sql_date)))
  END AS datekey
-- SELECT COUNT(*)
FROM (   
  SELECT b.vehicleType, a.storecode, a.stocknumber, a.noy, a.vehicleKey,
    CASE 
      WHEN b.vehicleType = 'New' THEN (
        SELECT vehicleKey
        FROM dimVehicle
        WHERE VIN = 'NA')
    END AS sourceVehicleKey,   
    CASE WHEN b.vehicleType = 'New' THEN (
      SELECT vehicleAcquisitionInfoKey
      FROM dimVehicleAcquisitionInfo
      WHERE vehicleType = 'New')
    END AS acqInfoKey,
    CASE WHEN b.vehicleType = 'New' THEN (
/* 
  5/17/14
  TROUBLE TROUBLE - THIS FUCKING QUERY MAKES NO SENCE, NO IMDINV ATTRIBUTE
  FUCKER FAILS SILENTLY
*/      
      SELECT imdinv
      FROM dimVehicleAcquisitionInfo
      WHERE vehicleType = 'New')
    END AS acqDate  
  FROM factVehicleSale a
  INNER JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey
  LEFT JOIN stgArkonaINPMAST c on a.stocknumber = c.imstk#
  WHERE a.stocknumber <> '') f

SELECT * FROM factVehicleAcquisition a INNER JOIN day b on a.datekey = b.datekey ORDER BY thedate
select * FROM factVehicleSale

/* new vehicle acq date angst -------------------------------------------------
-- so, what IS the acquisition date for new vehicles?
select a.*, b.imdinv, d.thedate AS saledate, e.vin
FROM factVehicleAcquisition a
LEFT JOIN stgArkonaINPMAST b on a.stocknumber = b.imstk#
LEFT JOIN factVehicleSale c on a.stocknumber = c.stocknumber
LEFT JOIN day d on c.approveddatekey = d.datekey
LEFT JOIN dimVehicle e on a.vehicleKEy = e.vehicleKey
WHERE a.vehicleAcquisitionInfoKey IS NOT NULL 
  AND a.stocknumber <> ''
  
--  AND coalesce(imdinv, cast('12/31/1980' AS sql_Date)) > d.thedate
  
  AND imdinv IS NULL 
  
  ORDER BY d.thedate DESC 

AND a.stocknumber = '11732'

-- fucking new car acquisition date, can't find it anywhere besides inpmast
-- AND it only EXISTS there IF the record IS still of the orig stock number
-- leaning toward fuck it, this IS the problem with deriving history FROM
-- arkona, DO the best i can, THEN keep the shit up
SELECT 
  sum(CASE WHEN imdinv IS NOT NULL THEN 1 ELSE 0 END) AS hasDate, -- 10813
  sum(CASE WHEN imdinv IS NULL THEN 1 ELSE 0 END) AS noHasDate -- 1983
FROM (
  select a.*, b.imdinv, d.thedate AS saledate, e.vin
  FROM factVehicleAcquisition a
  LEFT JOIN stgArkonaINPMAST b on a.stocknumber = b.imstk#
  LEFT JOIN factVehicleSale c on a.stocknumber = c.stocknumber
  LEFT JOIN day d on c.approveddatekey = d.datekey
  LEFT JOIN dimVehicle e on a.vehicleKEy = e.vehicleKey
  WHERE a.vehicleAcquisitionInfoKey IS NOT NULL -- new vehicles
    AND a.stocknumber <> '') x

-- sale date before acq date -- 162
SELECT 
  sum(CASE WHEN imdinv > saledate THEN 1 ELSE 0 END) AS soldBeforeAcq, 
  COUNT(*)
FROM (
  select a.*, b.imdinv, d.thedate AS saledate, e.vin
  FROM factVehicleAcquisition a
  LEFT JOIN stgArkonaINPMAST b on a.stocknumber = b.imstk#
  LEFT JOIN factVehicleSale c on a.stocknumber = c.stocknumber
  LEFT JOIN day d on c.approveddatekey = d.datekey
  LEFT JOIN dimVehicle e on a.vehicleKEy = e.vehicleKey
  WHERE a.vehicleAcquisitionInfoKey IS NOT NULL -- new vehicles
    AND a.stocknumber <> '') x
WHERE imdinv IS NOT NULL;
----------------------------------------------- new vehicle acq date angst --*/

used cars
need: 
  acqInfoKey
  date
  source vehicle
  
arkona data has precedence WHEN applicable (arkona knows nothing of purchase 
  categoryClassifications, hmm, outside of stocknumber decoding)
  AND available
    
SELECT a.*, b.imdinv
FROM factVehicleAcquisition a
LEFT JOIN stgArkonaINPMAST b on a.stocknumber = b.imstk#
WHERE a.vehicleAcquisitionInfoKey IS NULL 


-- see starting over again 2-1-15.sql for generationg tmpAis...Acq
SELECT acqCategory, acqCategoryClassification, datasource, COUNT(*)
FROM (
SELECT *  FROM tmpAisArkAcq
union
SELECT * FROM tmpAisDpsAcq) a
GROUP BY acqCategory, acqCategoryClassification,datasource

SELECT *  FROM tmpAisArkAcq
union
SELECT * FROM tmpAisDpsAcq
ORDER BY dateacq

-- III. C)
--SELECT stocknumber FROM ( -- shit only 17 dups out of 19377 rows
-- SELECT COUNT(*) FROM (
-- #wtf: ALL used car factVehicleAcquisition rows with acq info FROM tmpAisArkAcq & tmpAisDpsAcq
-- DROP TABLE #wtf;
SELECT a.*, b.datasource, b.dateAcq, b.acqCategory, b.acqCategoryClassification, 
   c.datasource, c.dateAcq, c.acqCategory, c.acqCategoryClassification
INTO #wtf   
FROM factVehicleAcquisition a
LEFT JOIN tmpAisArkAcq b on a.noy = b.stocknumbernoy
LEFT JOIN tmpAisDpsAcq c on a.noy = c.stocknumbernoy 
WHERE a.vehicleAcquisitionInfoKey IS NULL -- all used cars
--) x GROUP BY stocknumber HAVING COUNT(*) > 1

-- *** Anomalies **8
select count(*) from (SELECT stocknumbernoy FROM tmpAisArkAcq GROUP BY stocknumbernoy HAVING COUNT(*) > 1) x -- 8
select count(*) from (SELECT stocknumbernoy FROM tmpAisDpsAcq GROUP BY stocknumbernoy HAVING COUNT(*) > 1) x -- 1

-- no acq info FROM either tmpAis: the vast majority of these are FROM 
-- pre 2011 sales
-- one goofy embedded space stocknumber C4726X
SELECT c.thedate, a.*
-- SELECT COUNT(*)
FROM #wtf a
LEFT JOIN factVehicleSale b on a.stocknumber = b.stocknumber
LEFT JOIN day c on b.approveddatekey = c.datekey
WHERE a.dateacq IS NULL 
  AND a.dateacq_1 IS NULL 
ORDER BY thedate DESC 


-- eliminate those rows with no acq info: 4014 of 19406
SELECT c.thedate, a.*
-- SELECT COUNT(*)
FROM #wtf a
LEFT JOIN factVehicleSale b on a.noy = b.noy
LEFT JOIN day c on b.approveddatekey = c.datekey
WHERE NOT(a.dateacq IS NULL AND a.dateacq_1 IS NULL) -- eliminate those rows with no acq info
ORDER BY thedate DESC 


-- total agreement BETWEEN dps AND ark 9000 of 15400
-- modify this so that acqCat IS equal, 
-- AND some slack in acqDate, 10 days: 12228, 20 days: 12593, 30 days: 12816
SELECT c.thedate, a.*
-- SELECT COUNT(*)
FROM #wtf a
LEFT JOIN factVehicleSale b on a.noy = b.noy --a.stocknumber = b.stocknumber
LEFT JOIN day c on b.approveddatekey = c.datekey
WHERE NOT(a.dateacq IS NULL AND a.dateacq_1 IS NULL) -- eliminate those rows with no acq info
  AND ABS(dateAcq - dateAcq_1) < 30
--  AND dateAcq = dateAcq_1
  AND acqCategory = acqCategory_1
ORDER BY thedate DESC   

SELECT acqCategoryClassification, COUNT(*)
FROM tmpAisDpsAcq
GROUP BY acqCategoryClassification

SELECT *
FROM factVehicleAcquisition
WHERE stocknumber IN (
  SELECT stocknumber
  FROM factVehicleAcquisition
  GROUP BY stocknumber
  HAVING COUNT(*) > 1)
ORDER BY stocknumber

SELECT b.thedate, c.vin, a.*
FROM factVehicleSale a
LEFT JOIN day b on a.approveddatekey = b.datekey
LEFT JOIN dimVehicle c on a.vehicleKey = c.vehicleKey
WHERE stocknumber IN (
  SELECT stocknumber
  FROM factVehicleAcquisition
  GROUP BY stocknumber
  HAVING COUNT(*) > 1)
ORDER BY stocknumber

-- thinking just whack the dups, so few, also it establishes the pattern
-- of what needs to be checked IN real time updates

-- 3/5/15: thinking beware of premature optimization, focusing on the anomalies
--           too early, just go ahead AND backfill AND see what the data looks LIKE
--           THEN deal with shit LIKE get rid of anything before 2012 OR whatever
--         the horrible challenge IS going to be IN real time (daily) updates, 
--           so fucking many possible discrepancies AND anomalies

so WHERE am i, need to UPDATE factVehAcq with used car attributes
WHEN acqCat agree AND acqDate within 30 days, use ark info

yikes, missing source vehicle

-- 3//5/15, forgot about checking acqCategoryClassification (which IS necessary 
--              to determine the vehicleAcquisitioninfoKey
--            which brings the matching records down to 10105, but that IS ok
--            proceed, many of the anomalies will be addressed BY the eventaul
--            paring down of the data BY limiting the time frame (eg sales > 2011)

-- ark knows nothing of acqCatClass for purchases
-- this UPDATE 12689 rows
-- III. D) -- used car, acqCat:dps=ark, acqDate:dps-ark w/IN 30 days,
--              acqCatClass: IF trade: ark=dps 
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (    
  SELECT g.storecode, g.stocknumber, g.noy, 
    h.vehicleAcquisitionInfoKey, g.vehicleKey, g.datekey, 
    g.sourceVehicleKey
  FROM (  
    SELECT a.storecode, a.stocknumber, a.noY, 
      a.vehiclekey,
      a.acqCategory, 
      CASE acqCategory
        WHEN 'Trade' THEN acqCategoryClassification
        WHEN 'Purchase' THEN acqCategoryClassification_1
        ELSE 'XXX'
      END AS acqCategoryClassification,
      (select datekey from day where thedate = a.dateAcq) AS datekey, 
      CASE acqCategory
        WHEN 'Trade' THEN d.vehicleKey
        WHEN 'Purchase' THEN e.vehicleKey
        ELSE -666
      END AS sourceVehicleKey
    -- SELECT COUNT(*)
    -- SELECT *
    FROM #wtf a -- #wtf IS only used cars
    LEFT JOIN extArkonaBOPTRAD b on a.stocknumber = b.stocknumber
    LEFT JOIN factVehicleSale c on b.bopmastkey = c.bmkey AND b.storecode = c.storecode
    LEFT JOIN dimVehicle d on c.vehicleKey = d.vehicleKey -- decode source vehicle
    LEFT JOIN dimVehicle e on e.vin = 'NA'
    WHERE NOT(a.dateacq IS NULL AND a.dateacq_1 IS NULL) -- eliminate those rows with no acq info
      AND a.acqCategory = a.acqCategory_1 -- arkona = dps
      AND CASE WHEN acqCategory = 'Trade' THEN a.acqCategoryClassification = a.acqCategoryClassification_1 ELSE 1 = 1 END -- arkona = dps for trades only
      AND ABS(a.dateAcq - a.dateAcq_1) < 30) g -- arkona - dps < 30
  LEFT JOIN dimVehicleAcquisitionInfo h on g.acqCategory = h.acquisitionType
    AND g.acqCategoryClassification = h.acquisitionTypeCategory) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;

-- 3/7/15
-- remaining incomplete (no vehicleAcquisitionInfoKey) factVehicleAcquisition rows
SELECT a.*, b.*
-- SELECT COUNT(*)
FROM factVehicleAcquisition a
INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
LEFT JOIN #wtf b on a.stocknumber = b.stocknumber
WHERE a.vehicleAcquisitionInfoKey IS NULL  
  AND aaa.theyear > 2011  
  AND b.dateAcq > curdate()
--  AND acqCategoryClassification_1 = 'Intra market wholesale'
  AND acqCategory = 'trade'
  
struggling with DO ALL remaining updates IN one shot OR pick thru them
IN AS much AS this is/will be a one time back fill, go ahead AND
pick thru, the issue of course, IS WHEN i DO daily updates, exposing AND
analyzing anomalies

-- basis for remaining factVehicleAcquisition rows to be updated
-- sales > 2011
-- after III.E) 1104
SELECT a.*, b.*
-- SELECT COUNT(*)
FROM factVehicleAcquisition a
INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
INNER JOIN #wtf b on a.stocknumber = b.stocknumber
WHERE a.vehicleAcquisitionInfoKey IS NULL  
  AND aaa.theyear > 2011 



-- III. E)
-- ok, i will pick thru, ie find substantial groups that can be updated at once
-- sale > 2011 and dpsAcq =  IMWS
-- updates 543
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (    
  SELECT a.stocknumber, e.vehicleAcquisitionInfoKey, c.datekey, d.vehicleKey AS sourceVehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  LEFT JOIN #wtf b on a.stocknumber = b.stocknumber  -- #wtf IS only used cars
  LEFT JOIN day c on b.dateAcq_1 = c.thedate
  LEFT JOIN dimVehicle d on d.vin = 'NA'
  LEFT JOIN dimVehicleAcquisitionInfo e on e.acquisitionTypeCategory = 'Intra market wholesale'
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND aaa.theyear > 2011  
    AND acqCategoryClassification_1 = 'Intra market wholesale') x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;


-- ALL remaining factVehAcq rows with dateAcq = 12/31/9999 AND sale > 2011 AND acqCat = Trade
-- have no matching row IN BOPTRAD  -- 219
-- so the issue becomes one of determining the sourceVehicle
SELECT b.*, c.*
-- SELECT COUNT(*)
FROM factVehicleAcquisition a
INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
INNER JOIN #wtf b on a.stocknumber = b.stocknumber
LEFT JOIN extArkonaBOPTRAD c on a.stocknumber = c.stocknumber
WHERE a.vehicleAcquisitionInfoKey IS NULL  
  AND aaa.theyear > 2011       
  AND b.dateAcq = '12/31/9999'
  AND b.acqCategory = 'trade' ORDER BY dateAcq_1 DESC 
  AND c.storecode IS NOT NULL 

-- possible strategy for getting source vehicle for some, at least, of these  
--SELECT COUNT(*) FROM ( -- 159 of them
SELECT c.*, c.stocknumber, 
  case 
    when length(trim(c.suffix)) = 1 and c.suffix = 'A' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) 
    when length(trim(c.suffix)) > 1 and right(trim(c.suffix), 1) = 'A' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) 
    when length(trim(c.suffix)) = 1 and c.suffix = 'B' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) + 'A'
    when length(trim(c.suffix)) = 1 and c.suffix = 'C' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) + 'B'
  END,
  (SELECT stocknumber FROM factVehicleSale WHERE trim(stocknumber) = 
    case 
      when length(trim(c.suffix)) = 1 and c.suffix = 'A' then trim(left(c.stocknumber, length(trim(c.stocknumber)) - 1)) 
      when length(trim(c.suffix)) > 1 and right(trim(c.suffix), 1) = 'A' then trim(left(c.stocknumber, length(trim(c.stocknumber)) - 1)) 
      when length(trim(c.suffix)) = 1 and c.suffix = 'B' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) + 'A'
      when length(trim(c.suffix)) = 1 and c.suffix = 'C' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) + 'B'
    END) AS factVS
FROM (
  SELECT b.*,
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix  
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  INNER JOIN #wtf b on a.stocknumber = b.stocknumber
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND aaa.theyear > 2011  
    AND b.dateAcq > curdate() -- no ark acqDate
    AND b.acqCategory = b.acqCategory_1  
    AND b.acqCategory = 'trade') c 
--) x       
--WHERE x.factVS IS NOT NULL 

  
-- this might be the next step
SELECT b.*, c.*
-- SELECT COUNT(*)
FROM factVehicleAcquisition a
INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
INNER JOIN #wtf b on a.stocknumber = b.stocknumber
LEFT JOIN extArkonaBOPTRAD c on a.stocknumber = c.stocknumber
WHERE a.vehicleAcquisitionInfoKey IS NULL  
  AND aaa.theyear > 2011   
--  AND b.acqCategory = 'trade'
  AND b.acqCategory = b.acqCategory_1
  AND c.storecode IS NOT NULL 


-- stupid quibbling of the moment: DO trade/purchase together OR separately
-- fuck it: separate 
-- sale > 2011, acqCategory: ark=dps, acqCategory = purchase  
-- III. F) -- 149
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (  
  SELECT b.stocknumber, c.vehicleAcquisitionInfoKey, d.datekey, 
    e.vehicleKey AS sourceVehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  INNER JOIN #wtf b on a.stocknumber = b.stocknumber
  LEFT JOIN dimVehicleAcquisitionInfo c on b.acqCategory_1 = c.acquisitionType
    AND b.acqCategoryClassification_1 = c.acquisitionTypeCategory
  LEFT JOIN day d on b.dateAcq_1 = d.thedate  
  LEFT JOIN dimVehicle e on vin = 'NA'
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND aaa.theyear > 2011   
    AND b.acqCategory = 'Purchase'
    AND b.acqCategory = b.acqCategory_1) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;    


-- sale > 2011, acqCategory: ark=dps, acqCategory = trade, stk EXISTS IN BOPTRAD
-- III. G) -- 144
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.VehicleKey
FROM (  
  SELECT b.stocknumber, f.vehicleAcquisitionInfoKey, g.datekey, e.vehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  INNER JOIN #wtf b on a.stocknumber = b.stocknumber
  LEFT JOIN extArkonaBOPTRAD c on b.stocknumber = c.stocknumber
  LEFT JOIN factVehicleSale d on c.storecode = d.storecode AND c.bopmastkey = d.bmkey
  LEFT JOIN dimVehicle e on d.vehicleKey = e.vehicleKey
  LEFT JOIN dimVehicleAcquisitionInfo f on b.acqCategory = f.acquisitionType
    AND b.acqCategoryClassification = f.acquisitionTypeCategory
  LEFT JOIN day g on b.dateAcq = g.thedate  
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND aaa.theyear > 2011   
    AND b.acqCategory = 'Trade'
    AND b.acqCategory = b.acqCategory_1
    AND e.vehicleKey IS NOT NULL 
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;    

  
SELECT b.*
-- SELECT COUNT(*) -- after III.G) 814 LEFT
-- shit awful lot of recent stuff (20 IN 2015)
FROM factVehicleAcquisition a
INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
INNER JOIN #wtf b on a.stocknumber = b.stocknumber
WHERE a.vehicleAcquisitionInfoKey IS NULL  
  AND aaa.theyear > 2011  
ORDER BY dateAcq_1 DESC   

-- GROUP the remaining incomplete acquisitions
SELECT acqCategory, acqCategory_1, acqCategoryClassification, acqCategoryClassification_1, COUNT(*)
FROM (
  SELECT b.*
  -- SELECT COUNT(*) -- after III.G) 814 LEFT
  -- shit awful lot of recent stuff (20 IN 2015)
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  INNER JOIN #wtf b on a.stocknumber = b.stocknumber
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND aaa.theyear > 2011) x
GROUP BY acqCategory, acqCategory_1, acqCategoryClassification, acqCategoryClassification_1   
ORDER BY COUNT(*) DESC  

-- sale > 2011, acqCategory: ark=purchase dps=trade
--- III. H) -- 317
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.VehicleKey
FROM (    
  SELECT d.stocknumber, f.vehicleAcquisitionInfoKey, e.datekey, g.vehiclekey 
  FROM (  
    SELECT c.*,
      CASE -- use dps dateAcq
        WHEN dateAcq_1 > saleDate THEN saleDate
        ELSE dateAcq_1
      END AS acqDate,
      CASE right(TRIM(stockNumber),1)
        WHEN 'R' THEN 'Service Loaner'
        WHEN 'X' THEN 'Auction'
        WHEN 'L' THEN 'Lease Purchase'
        WHEN 'P' THEN 'Street'
        ELSE 'Unknown'
      END AS acqTypeCat   
    -- SELECT *  
    -- SELECT COUNT(*) -- 317
    -- this IS the single largest GROUP FROM above, mostly purchases that are
    -- categorized IN dps AS trades although the stocknumbers are X, P, R, L
    FROM (
      SELECT b.*, aaa.thedate AS saledate
      -- SELECT COUNT(*) 
      -- after III.G) 814 LEFT
      -- shit awful lot of recent stuff (20 IN 2015)
      FROM factVehicleAcquisition a
      INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
      INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
      INNER JOIN #wtf b on a.stocknumber = b.stocknumber
      WHERE a.vehicleAcquisitionInfoKey IS NULL  
        AND aaa.theyear > 2011) c
    WHERE c.acqCategory = 'purchase'
    AND c.acqCategory_1 = 'trade'
    AND c.acqCategoryClassification = '' 
    AND c.acqCategoryClassification_1 = 'used') d
  LEFT JOIN day e on d.acqDate = e.thedate 
  LEFT JOIN dimVehicleAcquisitionInfo f on f.acquisitionType = 'Purchase'
    AND d.acqTypeCat = f.acquisitionTypeCategory collate ads_default_ci
  LEFT JOIN dimVehicle g on g.vin = 'NA'
 ) x  
 WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;
  
  
-- sale > 2011, acqCategory/_1 = trade, acqCatClass/_1 = new, 
--- III. I) -- 97 updated
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (   
  SELECT stocknumber, e.vehicleAcquisitionInfoKey, d.datekey, c.sourceVehicleKey
  -- SELECT COUNT(*) -- this picks up 97 of 121
  FROM (  
    SELECT b.stocknumber, dateAcq_1 AS acqDate,
      (SELECT vehicleKey 
        FROM factVehicleSale 
        WHERE stocknumber = replace(b.stocknumber,'a','')) AS sourceVehicleKey
    -- 121 trades on new car sales with no record IN boptrad
    -- SELECT COUNT(*)
    FROM factVehicleAcquisition a
    INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
    INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
    INNER JOIN #wtf b on a.stocknumber = b.stocknumber
    WHERE a.vehicleAcquisitionInfoKey IS NULL  
      AND aaa.theyear > 2011  
    AND acqCategory = 'trade'
    AND acqCategory_1 = 'trade'
    AND acqCategoryClassification = 'new' 
    AND acqCategoryClassification_1 = 'new') c
  LEFT JOIN day d on c.acqDate = d.thedate  
  LEFT JOIN dimVehicleAcquisitionInfo e on e.acquisitionType = 'Trade'
    AND e.acquisitionTypeCategory = 'New'
  WHERE sourceVehicleKey IS NOT NULL
) x    
 WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;  
  

-- used car trades with no row IN BOPTRAD (dateAcq = 12/31/9999)  
SELECT COUNT(*)
FROM (
  SELECT c.*, c.stocknumber,
    case 
      when length(trim(c.suffix)) = 1 and c.suffix = 'A' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) 
      when length(trim(c.suffix)) > 1 and right(trim(c.suffix), 1) = 'A' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) 
      when length(trim(c.suffix)) = 1 and c.suffix = 'B' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) + 'A'
      when length(trim(c.suffix)) = 1 and c.suffix = 'C' then left(c.stocknumber, length(trim(c.stocknumber)) - 1) + 'B'
      WHEN suffix = 'XXB' THEN 'XXA'
      WHEN suffix = 'XXC' THEN 'XXB'
    END AS sourceStk
  -- SELECT COUNT(*)
  FROM (
    SELECT b.*,
        replace(replace(replace(replace(replace(replace(replace(replace(replace
          (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
            '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix  
    FROM factVehicleAcquisition a
    INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
    INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
    INNER JOIN #wtf b on a.stocknumber = b.stocknumber
    WHERE a.vehicleAcquisitionInfoKey IS NULL  
      AND aaa.theyear > 2011 
      AND b.acqCategory = 'trade'
      AND b.acqCategory_1 = 'trade'
      AND b.acqCategoryClassification = 'used' 
      AND b.acqCategoryClassification_1 = 'used') c) d   
LEFT JOIN factVehicleSale e on d.sourceStk = e.stocknumber    
WHERE e.storecode IS NOT null  
  
  
-- 3/8/15 fuck it, enuf for now, let's start fucking with inventory
-- again, leave out the NOT NULL constraints for now for the backfill
-- DROP TABLE factVehicleInventory;
CREATE TABLE factVehicleInventory (
  storeCode cichar(3),
  stockNumber cichar(9),  
  vehicleKey integer,
  fromDateKey integer,
  thruDateKey integer) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'storeCode', 'storeCode', 
  '', 2, 512, NULL );  
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'stockNumber', 'stockNumber', 
  '', 2, 512, NULL );  
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'vehicleKey', 'vehicleKey', 
  '', 2, 512, NULL );   
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'fromDateKey', 'fromDateKey', 
  '', 2, 512, NULL ); 
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'thruDateKey', 'thruDateKey', 
  '', 2, 512, NULL );   

-- this IS inventory based on sales    
DELETE FROM factVehicleInventory;    
INSERT INTO factVehicleInventory
SELECT n.storecode, n.stocknumber, n.vehiclekey,
  o.datekey, p.datekey
FROM (  
  SELECT m.storecode, m.stocknumber, m.vehicleKey, 
    CASE 
      WHEN m.acqDate > m.saleDate THEN m.saleDate
      ELSE m.acqDate
    END AS fromDate, 
    m.saleDate AS thruDate
  -- SELECT COUNT(*) -- only 112 of 11424
  FROM (  
    SELECT a.storecode, a.stocknumber, a.vehicleKey, b.thedate AS saleDate, 
      a.carDealInfoKey, d.thedate as acqDate,
      c.vehicleAcquisitionInfoKey
    -- SELECT COUNT(*)
    FROM factVehicleSale a
    INNER JOIN dimCarDealInfo aa on a.carDealInfoKey = aa.carDealInfoKey
    INNER JOIN day b on a.approveddatekey = b.datekey
    INNER JOIN factVehicleAcquisition c on a.stocknumber = c.stocknumber
      AND a.vehicleKey = c.vehicleKey
      AND c.vehicleAcquisitionInfoKey IS NOT NULL 
    INNER JOIN day d on c.datekey = d.datekey  
    WHERE b.theyear > 2011
      AND aa.vehicleTypeCode = 'U') m) n -- used only
LEFT JOIN day o on n.fromdate = o.thedate
LEFT JOIN day p on n.thrudate = p.thedate      
      
SELECT a.*, e.description, f.description
-- SELECT COUNT(*) -- shit only 62 with no match IN VehicleItems 
FROM factVehicleInventory a
LEFT JOIN dimVehicle b on a.vehicleKey = b.vehicleKey   
LEFT JOIN dps.VehicleItems c on b.vin = c.vin
LEFT JOIN dps.MakeModelClassifications d on c.make = d.make AND c.model = d.model
LEFT JOIN dps.typDescriptions e on d.vehicleType = e.typ
LEFT JOIN dps.typDescriptions f on d.vehicleSegment = f.typ

  
-- type & segment FROM dps.MakeModelClassifications  
SELECT a.*, c.description AS segment, d.description AS vehicleType
FROM dimVehicle a
LEFT JOIN dps.MakeModelClassifications b on a.make = b.make AND a.model = b.model
LEFT JOIN dps.typdescriptions c on b.vehiclesegment = c.typ
LEFT JOIN dps.TypDescriptions d on b.vehicleType = d.typ


SELECT a.thedate, b.storecode, COUNT(*)
FROM day a
LEFT JOIN factVehicleInventory b on a.thedate >= b.fromdate
  AND a.thedate <  b.thrudate
WHERE a.theyear = 2014
GROUP BY a.thedate, b.storecode
  
-- inventory windows 
-- thru date IS the driver
-- each window defined BY thrudate IS for the previous (inclusive) 30 days
SELECT a.thedate as windowFrom, a.thedate+29 AS windowThru, b.thedate, b.datekey
FROM day a
LEFT JOIN day b on b.thedate BETWEEN a.thedate AND a.thedate+29
WHERE year(a.thedate+29) = 2013

/* premature attempt at speeding shit up
DROP TABLE zTmpSales;
CREATE TABLE zTmpSales (
  windowFrom date,
  windowThru date,
  thedate date,
  datekey integer,
  storeCode cichar(3),
  stocknumber cichar(9),
  vehicleKey integer,
  saleType cichar(24)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 'zTmpSales', 
  'zTmpSales.adi', 'windowFrom', 'windowFrom', 
  '', 2, 512, NULL ); 
EXECUTE PROCEDURE sp_CreateIndex90( 'zTmpSales', 
  'zTmpSales.adi', 'windowThru', 'windowThru',  
  '', 2, 512, NULL ); 
EXECUTE PROCEDURE sp_CreateIndex90( 'zTmpSales', 
  'zTmpSales.adi', 'storeCode', 'storeCode', 
  '', 2, 512, NULL ); 
EXECUTE PROCEDURE sp_CreateIndex90( 'zTmpSales', 
  'zTmpSales.adi', 'saleType', 'saleType', 
  '', 2, 512, NULL );  
EXECUTE PROCEDURE sp_CreateIndex90( 'zTmpSales', 
  'zTmpSales.adi', 'zTmpSales_1', 'windowFrom;windowThru;storecode;saleType', 
  '', 2, 512, NULL );    
  
INSERT INTO zTmpSales    
SELECT a.thedate as windowFrom, a.thedate+29 AS windowThru, b.thedate, b.datekey,
  c.storecode, c.stocknumber, c.vehicleKey, d.saleType
FROM day a
LEFT JOIN day b on b.thedate BETWEEN a.thedate AND a.thedate+29
LEFT JOIN factVehiclesale c on b.datekey = c.approvedDateKey
INNER JOIN dimCarDealInfo d on c.carDealInfoKey = d.carDealInfoKey
  AND d.vehicleType = 'Used'
WHERE year(a.thedate+29) = 2013;

SELECT windowFrom, windowThru, storecode, saleType, COUNT(*)
FROM zTmpSales
GROUP BY windowFrom, windowThru, storecode, saleType
*/


-- gregs whiteboard
SELECT a.thedate as windowFrom, a.thedate+29 AS windowThru, b.thedate, b.datekey,
  c.storecode, c.stocknumber, c.vehicleKey, d.saleType
FROM day a
INNER JOIN day b on b.thedate BETWEEN a.thedate AND a.thedate+29
LEFT JOIN factVehiclesale c on b.datekey = c.approvedDateKey
INNER JOIN dimCarDealInfo d on c.carDealInfoKey = d.carDealInfoKey
  AND d.vehicleType = 'Used'
WHERE year(a.thedate+29) = 2013
  
SELECT windowFrom, windowThru, storecode, saleType, COUNT(*)
FROM (
  SELECT a.thedate as windowFrom, a.thedate+29 AS windowThru, b.thedate, b.datekey,
    c.storecode, c.stocknumber, c.vehicleKey, d.saleType
  FROM day a
  INNER JOIN day b on b.thedate BETWEEN a.thedate AND a.thedate+29
  LEFT JOIN factVehiclesale c on b.datekey = c.approvedDateKey
  INNER JOIN dimCarDealInfo d on c.carDealInfoKey = d.carDealInfoKey
    AND d.vehicleType = 'Used'
  WHERE year(a.thedate+29) = 2013) f
GROUP BY windowFrom, windowThru, storecode, saleType  

-- DO a version to JOIN with owned (#vo), stre,from/thru, sold
SELECT windowFrom, windowThru, storecode, 
  SUM(CASE WHEN saleType = 'Intra Market Wholesale' THEN 1 ELSE 0 END) AS IMWS,
  SUM(CASE WHEN saleType = 'Wholesale' THEN 1 ELSE 0 END) AS WS,
  SUM(CASE WHEN saleType = 'Retail' THEN 1 ELSE 0 END) AS Retail,
  COUNT(*) AS sold
INTO #vs  
FROM (
  SELECT a.thedate as windowFrom, a.thedate+29 AS windowThru, b.thedate, b.datekey,
    c.storecode, c.stocknumber, c.vehicleKey, d.saleType
  FROM day a
  INNER JOIN day b on b.thedate BETWEEN a.thedate AND a.thedate+29
  LEFT JOIN factVehiclesale c on b.datekey = c.approvedDateKey
  INNER JOIN dimCarDealInfo d on c.carDealInfoKey = d.carDealInfoKey
    AND d.vehicleType = 'Used'
  WHERE year(a.thedate+29) = 2013) f
GROUP BY windowFrom, windowThru, storecode

-- how many vehicles owned IN each window
-- one row for each stk/day IN inv
SELECT d.stocknumber, d.fromdate, d.thrudate, e.thedate
FROM (
  SELECT a.*, b.thedate AS fromDate, c.thedate AS thruDate
  FROM factVehicleInventory a 
  LEFT JOIN day b on a.fromdatekey = b.datekey
  LEFT JOIN day c on a.thrudatekey = c.datekey) d
LEFT JOIN day e on e.thedate BETWEEN d.fromDate AND d.thrudate  

SELECT h.storecode, g.windowfrom, g.windowthru, COUNT(DISTINCT h.stocknumber) AS owned
-- SELECT *
INTO #vo
FROM (
  SELECT a.thedate as windowFrom, a.thedate+29 AS windowThru, b.thedate, 
    b.datekey
  FROM day a
  LEFT JOIN day b on b.thedate BETWEEN a.thedate AND a.thedate+29
  WHERE year(a.thedate+29) = 2013) g
LEFT JOIN (
  SELECT d.storecode, d.stocknumber, d.fromdate, d.thrudate, e.thedate
  FROM (
    SELECT a.*, b.thedate AS fromDate, c.thedate AS thruDate
    FROM factVehicleInventory a 
    LEFT JOIN day b on a.fromdatekey = b.datekey
    LEFT JOIN day c on a.thrudatekey = c.datekey) d
  LEFT JOIN day e on e.thedate BETWEEN d.fromDate AND d.thrudate) h on g.thedate = h.thedate     
GROUP BY h.storecode, g.windowfrom, g.windowthru  

select a.*, b.imws, b.ws, b.retail, b.sold 
FROM #vo a
LEFT JOIN #vs b on a.windowfrom = b.windowfrom
  AND a.storecode = b.storecode
  
  
----------------------------------------------------------------------------------
-- 3/10/15 current inventory
SELECT imco#, imvin, imstk#, imtype, imdinv, imsact, imiact, impric, imcost
-- SELECT COUNT(*) 
-- SELECT *
FROM stgArkonaINPMAST a
WHERE a.imstat = 'i'
ORDER BY imdinv DESC 

-- used cars iN arkona
DROP TABLE #ark;
SELECT imvin, imstk#, replace(imstk#,'y','') AS noY,imtype, imdinv, imsact, imiact, impric, imcost
INTO #ark
FROM stgArkonaINPMAST a
WHERE a.imstat = 'i'
  AND a.imtype = 'u'

DROP TABLE #dps; 
SELECT a.stocknumber, replace(a.stocknumber,'y','') as noY,
  b.vin, CAST(a.fromts AS sql_date) AS fromDate, e.amount AS price, 
  left(f.name, 12) AS store
INTO #dps  
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID 
LEFT JOIN dimVehicle c on b.vin = c.vin
LEFT JOIN dps.vehiclePricings d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.vehiclePricingTS = (
    SELECT MAX(vehiclePricingTS)
    FROM dps.VehiclePricings 
    WHERE VehicleInventoryItemID = d.VehicleInventoryItemID)
LEFT JOIN dps.VehiclePricingDetails e on d.VehiclePricingID = e.VehiclePricingID   
  AND e.typ = 'VehiclePricingDetail_BestPrice' 
LEFT JOIN dps.organizations f on a.owningLocationID = f.partyid
WHERE a.thruts IS NULL   

SELECT *
FROM #ark a
full OUTER JOIN #dps b on a.noy = b.noy
WHERE a.imvin IS NULL OR b.vin IS NULL 


SELECT * FROM stgArkonaGLPTRNS WHERE gtacct = '224100' AND gtdate = curdate() -1

SELECT * FROM stgArkonaGLPMAST WHERE gmacct = '224100'


---------------------------------------------------------------------------------
-- 3/11
/* 
talked to greg, convinced myself that what IS needed IS a date driven status look
on each day for a period:
  how many sold
  how many acq
  how many IN each status
  
categorize vehicles:
shit some of these are time dependent, some are not
  type  -- not
  segment  -- not
  price  -- are
  age  -- are
  miles -- are
  
maybe a junk dimension: typ  segment  
  
new greg status: showable: on the ground/priced/detail complete  

*/

-- this IS pretty snappy for 1 row for each day IN 2014/vehicles IN inventory
-- the problem IS there IS no guarantee that there will be a row for each day
--   IN this query it IS NOT a problem, but WHEN we narrow it down to different
--   much smaller subsets of vehicles, no guarantees
SELECT *
-- SELECT COUNT(*)
-- INTO #wtf
FROM (
  SELECT a.thedate AS invDate
  FROM day a
  WHERE a.theyear = 2014) a,
(
  SELECT a.*, b.thedate AS fromDate, 
    case 
      when b.thedate >= c.thedate THEN b.thedate
      ELSE c.thedate - 1
    END AS thruDate
  FROM factVehicleInventory a
  LEFT JOIN day b on a.fromdatekey = b.datekey
  LEFT JOIN day c on a.thrudatekey = c.datekey) b
WHERE a.invDate BETWEEN b.fromdate AND b.thrudate

SELECT *
FROM dimVehicle

SELECT *
FROM dps.typDescriptions
WHERE lower(typ) LIKE '%suv%'
  OR lower(typ) LIKE '%large%'
  
SELECT a.description

SELECT *
FROM dps.typCategories a
LEFT JOIN dps.typDescriptions b on a.typ = b.typ
WHERE a.category = 'VehicleSegment'
  OR a.category = 'VehicleType'
ORDER BY a.category  

SELECT a.*, b.thedate, c.thedate, timestampdiff(sql_tsi_day, b.thedate, c.thedate)
FROM factVehicleInventory a
LEFT JOIN day b on a.fromdatekey = b.datekey
LEFT JOIN day c on a.thrudatekey = c.datekey
WHERE b.thedate < '01/01/2015'
  AND year(c.thedate) = 2014
ORDER BY timestampdiff(sql_tsi_day, b.thedate, c.thedate)  
 
-- type/segment FROM dps.MakeModelClassifications   
-- DROP TABLE #wtf;  
SELECT a.*, b.thedate, c.thedate, 
  d.modelyear, d.make, d.model, 
  substring(e.vehicleType, position('_' IN e.vehicleType) + 1, 12) AS arkType,
  substring(e.vehicleSegment, position('_' IN e.vehicleSegment) + 1, 12) AS arkSegment,
  f.make, f.model, 
  substring(g.vehicleType, position('_' IN g.vehicleType) + 1, 12) AS dpsType,
  substring(g.vehicleSegment, position('_' IN g.vehicleSegment) + 1, 12) AS dpsSegment
-- SELECT COUNT(*) -- 21 w/no VehicleItems record 
INTO #wtf
FROM factVehicleInventory a
LEFT JOIN day b on a.fromdatekey = b.datekey
LEFT JOIN day c on a.thrudatekey = c.datekey
LEFT JOIN dimVehicle d on a.vehicleKey = d.vehicleKey
LEFT JOIN dps.MakeModelClassifications e on d.make = e.make
  AND d.model = e.model
LEFT JOIN dps.VehicleItems f on d.vin = f.vin  
LEFT JOIN dps.MakeModelClassifications g on f.make = g.make
  AND f.model = g.model
WHERE b.thedate < '01/01/2015'
  AND year(c.thedate) = 2014
--  AND f.vin IS NULL 

SELECT * 
FROM #wtf
WHERE (make IS NOT NULL AND make_1 IS NOT NULL)
  AND (arkType IS NOT NULL AND arkSegment IS NOT NULL)
  AND (dpsType IS NOT NULL AND dpsSegment IS NOT NULL)
  AND (arkType <> dpsType OR arkSegment <> dpsSegment)
  