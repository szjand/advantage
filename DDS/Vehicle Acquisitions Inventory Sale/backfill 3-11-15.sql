/* 
  to be run after updateing extArkonaBOPTRAD
  consolidate ALL the back fill acq-inv-sale scripts INTO one, for easy regeneration
  of starting place
  includes:
    Z:\E\DDS2\Scripts\Vehicle Acquisitions Inventory Sale\Acquisitions\starting over again 2-1-15.sql
    Z:\E\DDS2\Scripts\Vehicle Acquisitions Inventory Sale\come at it FROM dps 2-2-15.sql
    
3/11/15: 
  34 sec
  there are still unresolved anomalies, this only goes thru III. I)    
*/

/* current state of generating data -------------------------------------------*/
/* "fine" tuning of scripts arkona acquisitions.sql, dps acquisitions.sql
/*
  4/28/15 no rows IN factVehicleAcquistion for current inventory
          eg, ALL factVehicleAcquistion rows
*/  
--< arkona --------------------------------------------------------------------<
/*
Z:\E\DDS2\Scripts\Vehicle Acquisitions Inventory Sale\Acquisitions\starting over again 2-1-15.sql
*/
-- trades
-- 3/5/15 why am i USING orig date AS acq date? let us DO a little snooping 
-- should be USING apprv date

-- *a*
-- 6/5/15 get rid of ALL the fucking date restrictions

DELETE FROM tmpAisArkAcq;
INSERT INTO tmpAisArkAcq
SELECT a.storeCode, a.stockNumber, a.vin, b.bmdtaprv, 'Trade',
  CASE b.bmvtyp
    WHEN 'N' THEN 'New'
    WHEN 'U' THEN 'Used'
    ELSE 'Unknown'
  END,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
  replace(a.stocknumber, 'y', '') AS stockNumberNoY, 'ark-trade'
FROM extArkonaBOPTRAD a
LEFT JOIN stgArkonaBOPMAST b on a.storeCode = b.bmco#
  AND a.bopmastKey = b.bmkey
-- *a*  
--WHERE year(b.bmdtor) > 2010
--  AND b.bmstat <> '' -- eliminates pending deals
WHERE b.bmstat <> '' -- eliminates pending deals
  AND right(TRIM(a.stocknumber), 6) <> right(TRIM(a.vin), 6); -- eliminates temp stocknumbers
  
-- trades with bad stocknumbers  
INSERT INTO tmpAisArkAcq
SELECT storecode, imstk, vin, bmdtor, 'Trade', 
  CASE bmvtyp
    WHEN 'N' THEN 'New'
    WHEN 'U' THEN 'Used'
    ELSE 'Unknown'
  END,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(imstk, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
  replace(imstk, 'y', '') AS stockNumberNoY, 'ark-trade'
FROM (  
  SELECT a.storeCode, a.stocknumber, c.imstk#, b.bmvtyp,
    CASE
      WHEN right(TRIM(a.stocknumber), 6) <> right(TRIM(a.vin), 6) THEN a.stocknumber
      WHEN c.imstk# <> '' AND c.imstk# <> a.stocknumber AND c.imdinv = bmdtor THEN imstk#
      ELSE 'NO'
    END AS imstk, 
    a.stockNumber, a.vin, b.bmdtor, c.imdinv
  FROM extArkonaBOPTRAD a
  LEFT JOIN stgArkonaBOPMAST b on a.storeCode = b.bmco#
    AND a.bopmastKey = b.bmkey
  LEFT JOIN stgArkonaINPMAST c on a.vin = c.imvin  
-- *a*  
--  WHERE year(bmdtor) > 2010
--    AND right(TRIM(a.stocknumber), 6) = right(TRIM(a.vin), 6)
  WHERE right(TRIM(a.stocknumber), 6) = right(TRIM(a.vin), 6)
    AND
      CASE
        WHEN right(TRIM(a.stocknumber), 6) <> right(TRIM(a.vin), 6) THEN a.stocknumber
        WHEN c.imstk# <> '' AND c.imstk# <> a.stocknumber AND c.imdinv = bmdtor THEN imstk#
        ELSE 'NO'
      END <> 'NO') x
WHERE NOT EXISTS (
  SELECT 1
  FROM tmpAisArkAcq
  WHERE vin = x.vin);  
  
-- sales  
-- stk# NOT already IN tmpAisArkAcq, acqDate FROM INPMAST
-- 6/5/15 how DO i know that these are purchases? because they did already exist
--   FROM processing boptrad, i guess
INSERT INTO tmpAisArkAcq
SELECT bmco#, bmstk#, bmvin, acqDate,
  CASE 
    WHEN right(TRIM(bmstk#), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
    ELSE 'Purchase'
  END AS category,
  CASE 
    WHEN right(TRIM(bmstk#), 1) IN ('a','b','c','d','e','f') THEN
      CASE 
        WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
        ELSE 'Used'
      END
    ELSE ''
  END, 
  suffix, stockNumberNoY, 'ark-sale'
FROM (  
  SELECT distinct a.bmco#, a.bmstk#, a.bmvin, 
--    coalesce(c.imdinv, d.gtdate) AS acqDate, 'Purchase', '',
    coalesce(c.imdinv, CAST('12/31/9999' AS sql_date)) AS acqDate, 'Purchase', '',    
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.bmstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
    replace(a.bmstk#, 'y', '') AS stockNumberNoY    
  FROM stgArkonaBOPMAST a
  LEFT JOIN tmpAisArkAcq b on a.bmstk# = b.stocknumber 
  LEFT JOIN stgArkonaINPMAST c on a.bmstk# = c.imstk#
  WHERE a.bmstk# <> ''
    AND a.bmstat <> '' -- finished deals
    AND a.bmvtyp = 'u' -- used car sale
-- *a*    
--    AND year(a.bmdtor) > 2010
    AND b.storecode IS NULL) x;
    
-- 6/5/15 at this point, there should be a row IN tmpAisArkAcq for every used
--   car sold    
/* these can be fixed
SELECT a.*, c.*
-- 6/6 after eliminating date restriction it IS now 266 of 20336
-- SELECT COUNT(*) -- 42 missing out of 18939, NOT too bad
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.cardealinfokey = b.cardealinfokey
  AND b.vehicletypecode = 'u'
LEFT JOIN tmpAisArkAcq c on a.stocknumber = c.stocknumber
WHERE c.storecode IS NULL 

-- what are they - ALL sales with no stocknumber, NOT true of the 266
SELECT a.stocknumber, b.*, c.thedate, d.*
-- SELECT COUNT(*) -- 42 missing out of 18939, NOT too bad
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.cardealinfokey = b.cardealinfokey
  AND b.vehicletypecode = 'u'
INNER JOIN day c on a.approveddatekey = c.datekey
INNER JOIN dimVehicle d on a.vehiclekey = d.vehiclekey  
WHERE NOT EXISTS (
  SELECT 1
  FROM tmpAisArkAcq
  WHERE stocknumber = a.stocknumber)
  
-- every frikkin one of them IS IN VehicleItems & VehicleInventoryItems 
SELECT c.thedate AS arkonaSaleDate, a.stocknumber, 
  d.vin, f.stocknumber, CAST(f.fromts AS sql_Date) AS fromDate,
  CAST(f.thruts AS sql_date) AS thruDate
-- SELECT COUNT(*) -- 42 missing out of 18939, NOT too bad
--INTO #wtf
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.cardealinfokey = b.cardealinfokey
  AND b.vehicletypecode = 'u'
INNER JOIN day c on a.approveddatekey = c.datekey
INNER JOIN dimVehicle d on a.vehiclekey = d.vehiclekey  
LEFT JOIN dps.VehicleItems e on d.vin = e.vin
LEFT JOIN dps.VehicleInventoryItems f on e.VehicleItemID = f.VehicleItemID 
WHERE NOT EXISTS (
  SELECT 1
  FROM tmpAisArkAcq
  WHERE stocknumber = a.stocknumber)   
ORDER BY c.thedate
*/    

-- current inventory --------------------------------------------
INSERT INTO tmpAisArkAcq
SELECT imco#, imstk#, imvin, imdinv,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
    ELSE 'Purchase'
  END AS category,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN
      CASE 
        WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
        ELSE 'Used'
      END
    ELSE ''
  END, 
  suffix, stockNumberNoY, 'ark-inv'
FROM (  
  SELECT imco#, imstk#, imvin, imdinv, 'Purchase', '',
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.imstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix, 
    replace(a.imstk#, 'y', '') AS stockNumberNoY
  FROM stgArkonaINPMAST a
  WHERE imstk# <> ''
    AND imstat = 'i'
    AND imtype = 'u'
    AND NOT EXISTS (
      SELECT 1
      FROM tmpAisArkAcq
      WHERE stocknumber = a.imstk#)) x;
      
--/> arkona -------------------------------------------------------------------/>      

--< dps -----------------------------------------------------------------------<
-- *x* goofy exc, mazda ws to honda, sitting IN book intra market for 9 days
DELETE FROM tmpAisDpsAcq;
INSERT INTO tmpAisDpsAcq
select 
  CASE c.name 
    WHEN 'rydells' THEN 'RY1'
    WHEN 'honda cartiva' THEN 'RY2'
    ELSE 'XXX'
  END,
  left(trim(a.stocknumber), 10), left(trim(b.vin), 17), cast(a.fromts as sql_date)AS dateAcq, 
  CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
    WHEN 'Trade' THEN 'Trade'
    ELSE 'Purchase'
  END AS acqCategory,
  CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
    WHEN 'Trade' THEN ''
    ELSE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
  END AS acqCategoryClassification,
--  left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20) AS acqCategory,
  left(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9',''), 'Y',''), 9) AS suffix,
  trim(replace(a.stocknumber, 'y', '')) AS stockNumberNoY, 'dps'
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID 
INNER JOIN dps.organizations c on a.owningLocationID = c.partyid
LEFT JOIN dps.vehicleAcquisitions d on a.VehicleInventoryItemID = d.VehicleInventoryItemID  
LEFT JOIN dps.organizations e on d.acquiredFromPartyID = e.partyid
-- *a*
--WHERE year(a.fromts) > 2010
--  AND d.typ IS NOT NULL
WHERE d.typ IS NOT NULL  
  AND a.stocknumber IS NOT NULL;  --*x* 

UPDATE tmpAisDpsAcq
  SET acqCategoryClassification = 
    CASE 
      WHEN stocknumberSuffix IN ('a','aa','aaa','aaaa','aaaaa') THEN 'New'
      ELSE 'Used'
    END    
WHERE acqCategory = 'trade';

-- 3/5/15 to comply with dds.dimVehicleAcquisitionInfo (come at it FROM dps 2-2-15.sql
UPDATE tmpAisDpsAcq
SET acqCategoryClassification = 
  CASE acqCategoryClassification
    WHEN 'DealerPurchase' THEN 'Dealer Purchase'
    WHEN 'InAndOut' THEN 'In And Out'
    WHEN 'IntraMarketPurchase' THEN 'Intra Market Wholesale'
    WHEN 'LeasePurcha' THEN 'Lease Purchase'
    WHEN 'ServiceLoan' THEN 'Service Loaner'
    ELSE acqCategoryClassification
  END;
--/> dps ----------------------------------------------------------------------/>  

/*
Z:\E\DDS2\Scripts\Vehicle Acquisitions Inventory Sale\come at it FROM dps 2-2-15.sql
*/
-- III. A) INSERT IMWS INTO factVehicleSale      
-- 3/1/15 - this may be good enuf for now, at least for back fill
-- going to accept this AS good enuf for factVehicleSale
INSERT INTO factVehicleSale (storecode,bmkey,stockNumber,carDealInfoKey,
    vehicleKey,originationDateKey,approvedDateKey,cappedDateKey,buyerKey,
    coBuyerKey,consultantKey,managerKey,vehicleCost,vehiclePrice,noY)
SELECT * 
-- SELECT COUNT(*)
FROM (
  SELECT 
    case d.fullname
      WHEN 'GF-Crookston' THEN 'RY3'
      WHEN 'GF-Honda Cartiva' THEN 'RY2'
      WHEN 'GF-Rydells' THEN 'RY1'
    END AS storeCode, 
    -1 AS bmkey, -- there IS no arkona car deal for IMWS
    left(c.stocknumber, 9) AS stocknumber, 
    (select carDealInfoKey from dimCarDealInfo where saleTypeCode = 'I') as carDealInfoKey,
    e.vehicleKey, 
    f.datekey AS originationDateKey, f.datekey AS approvedDateKey, f.datekey AS cappedDateKey,
    case b.fullname
      WHEN 'GF-Crookston' THEN 175
      WHEN 'GF-Honda Cartiva' THEN 12
      WHEN 'GF-Rydells' THEN 9019
    END AS buyerKey,
    1084332 AS coBuyerKey,
    246 AS consultantKey, 246 AS managerKey, 
    0 AS vehicleCost,
    a.soldAmount AS vehiclePrice,
    replace(left(c.stocknumber, 9),'Y','') AS noY
  FROM dps.vehicleSales a
  INNER JOIN dps.organizations b on a.soldto = b.partyid collate ads_Default_ci
    AND b.fullname IN ('GF-Honda Cartiva','GF-Rydells')
  INNER JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  INNER JOIN dps.VehicleItems cc on c.VehicleItemID = cc.VehicleItemID 
  INNER JOIN dps.organizations d on c.owninglocationid = d.partyid
  LEFT JOIN dimVehicle e on cc.vin = e.vin
  LEFT JOIN day f on CAST(a.soldts AS sql_date) = f.thedate
  WHERE b.fullname IS NOT NULL   
    AND a.status = 'VehicleSale_Sold') x -- eliminate canceled sales   
WHERE vehicleKey IS NOT NULL 
  AND NOT EXISTS (
    SELECT 1
    FROM factVehicleSale
    WHERE stocknumber = x.stocknumber);
/*
-- III. B)    
-- base rows for ALL sales
-- 6/6/15 executive decision: used cars only *c*
DELETE FROM factVehicleAcquisition;
INSERT INTO factVehicleAcquisition (storecode,stocknumber,noY,vehicleKey,
  sourceVehicleKey, vehicleAcquisitionInfoKey, datekey)
SELECT f.storecode, f.stocknumber, f.noy, f.vehiclekey, 
  f.sourceVehicleKey, f.acqInfoKey, 
  CASE 
    WHEN f.vehicleType = 'New' THEN (
      SELECT datekey
      FROM day
      WHERE thedate = coalesce(f.acqDate, CAST('12/31/9999' AS sql_date)))
  END AS datekey
FROM (   
  SELECT b.vehicleType, a.storecode, a.stocknumber, a.noy, a.vehicleKey,
    CASE 
      WHEN b.vehicleType = 'New' THEN (
        SELECT vehicleKey
        FROM dimVehicle
        WHERE VIN = 'NA')
    END AS sourceVehicleKey,   
    CASE WHEN b.vehicleType = 'New' THEN (
      SELECT vehicleAcquisitionInfoKey
      FROM dimVehicleAcquisitionInfo
      WHERE vehicleType = 'New')
    END AS acqInfoKey,
    CASE WHEN b.vehicleType = 'New' THEN (
 
--  5/17/14
--  TROUBLE TROUBLE - THIS FUCKING QUERY MAKES NO SENSE, NO IMDINV ATTRIBUTE
--  FUCKER FAILS SILENTLY
      
      SELECT imdinv
      FROM dimVehicleAcquisitionInfo
      WHERE vehicleType = 'New')
    END AS acqDate  
  FROM factVehicleSale a
  INNER JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey
  LEFT JOIN stgArkonaINPMAST c on a.stocknumber = c.imstk#
  WHERE a.stocknumber <> '') f;   
*/

-- III. B)    
-- base rows for ALL sales
-- no sourceVehicle, acqInfoKey, acqDate info, yet
-- 6/6/15 executive decision: used cars only

DELETE FROM factVehicleAcquisition;
INSERT INTO factVehicleAcquisition (storecode,stocknumber,noY,vehicleKey)
SELECT a.storecode, a.stocknumber, a.noy, a.vehicleKey
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey
  AND b.vehicleTypeCode = 'U'
LEFT JOIN stgArkonaINPMAST c on a.stocknumber = c.imstk#
WHERE a.stocknumber <> '' 
 
-- III. C)
-- #wtf: ALL used car factVehicleAcquisition rows with acq info FROM tmpAisArkAcq & tmpAisDpsAcq
-- DROP TABLE #wtf;
SELECT a.*, b.datasource, b.dateAcq, b.acqCategory, b.acqCategoryClassification, 
   c.datasource, c.dateAcq, c.acqCategory, c.acqCategoryClassification
INTO #wtf   
FROM factVehicleAcquisition a
LEFT JOIN tmpAisArkAcq b on a.noy = b.stocknumbernoy
LEFT JOIN tmpAisDpsAcq c on a.noy = c.stocknumbernoy;
-- WHERE a.vehicleAcquisitionInfoKey IS NULL; -- all used cars 6/6 NOT needed only uc IN factVehAcq

-- III. D) -- used car, acqCat:dps=ark, acqDate:dps-ark w/IN 30 days,
--              acqCatClass: IF trade: ark=dps 
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (    
  SELECT g.storecode, g.stocknumber, g.noy, 
    h.vehicleAcquisitionInfoKey, g.vehicleKey, g.datekey, 
    g.sourceVehicleKey
  FROM (  
    SELECT a.storecode, a.stocknumber, a.noY, 
      a.vehiclekey,
      a.acqCategory, 
      CASE acqCategory
        WHEN 'Trade' THEN acqCategoryClassification
        WHEN 'Purchase' THEN acqCategoryClassification_1
        ELSE 'XXX'
      END AS acqCategoryClassification,
      (select datekey from day where thedate = a.dateAcq) AS datekey, 
      CASE acqCategory
        WHEN 'Trade' THEN d.vehicleKey
        WHEN 'Purchase' THEN e.vehicleKey
        ELSE -666
      END AS sourceVehicleKey
    FROM #wtf a -- #wtf IS only used cars
    LEFT JOIN extArkonaBOPTRAD b on a.stocknumber = b.stocknumber
    LEFT JOIN factVehicleSale c on b.bopmastkey = c.bmkey AND b.storecode = c.storecode
    LEFT JOIN dimVehicle d on c.vehicleKey = d.vehicleKey -- decode source vehicle
    LEFT JOIN dimVehicle e on e.vin = 'NA'
    WHERE NOT(a.dateacq IS NULL AND a.dateacq_1 IS NULL) -- eliminate those rows with no acq info
      AND a.acqCategory = a.acqCategory_1 -- arkona = dps
      AND CASE WHEN acqCategory = 'Trade' THEN a.acqCategoryClassification = a.acqCategoryClassification_1 ELSE 1 = 1 END -- arkona = dps for trades only
      AND ABS(a.dateAcq - a.dateAcq_1) < 30) g -- arkona - dps < 30
  LEFT JOIN dimVehicleAcquisitionInfo h on g.acqCategory = h.acquisitionType
    AND g.acqCategoryClassification = h.acquisitionTypeCategory) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;
    
-- III. E)
-- ok, i will pick thru, ie find substantial groups that can be updated at once
-- sale > 2011 and dpsAcq =  IMWS
-- updates 543
-- 6/6/15 remove date limit, updates 839 rows
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (    
  SELECT a.stocknumber, e.vehicleAcquisitionInfoKey, c.datekey, d.vehicleKey AS sourceVehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  LEFT JOIN #wtf b on a.stocknumber = b.stocknumber  -- #wtf IS only used cars
  LEFT JOIN day c on b.dateAcq_1 = c.thedate
  LEFT JOIN dimVehicle d on d.vin = 'NA'
  LEFT JOIN dimVehicleAcquisitionInfo e on e.acquisitionTypeCategory = 'Intra market wholesale'
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
--    AND aaa.theyear > 2011  
    AND acqCategoryClassification_1 = 'Intra market wholesale') x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;    
  
-- sale > 2011, acqCategory: ark=dps, acqCategory = purchase  
-- III. F) -- 149
-- 6/6/15 remove date limit, updates 495 rows
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (  
  SELECT b.stocknumber, c.vehicleAcquisitionInfoKey, d.datekey, 
    e.vehicleKey AS sourceVehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  INNER JOIN #wtf b on a.stocknumber = b.stocknumber
  LEFT JOIN dimVehicleAcquisitionInfo c on b.acqCategory_1 = c.acquisitionType
    AND b.acqCategoryClassification_1 = c.acquisitionTypeCategory
  LEFT JOIN day d on b.dateAcq_1 = d.thedate  
  LEFT JOIN dimVehicle e on vin = 'NA'
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
--    AND aaa.theyear > 2011   
    AND b.acqCategory = 'Purchase'
    AND b.acqCategory = b.acqCategory_1) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;    
  
-- sale > 2011, acqCategory: ark=dps, acqCategory = trade, stk EXISTS IN BOPTRAD
-- III. G) -- 144
-- 6/6/15 remove date limit, updates 206 rows
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.VehicleKey
FROM (  
  SELECT b.stocknumber, f.vehicleAcquisitionInfoKey, g.datekey, e.vehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
  INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
  INNER JOIN #wtf b on a.stocknumber = b.stocknumber
  LEFT JOIN extArkonaBOPTRAD c on b.stocknumber = c.stocknumber
  LEFT JOIN factVehicleSale d on c.storecode = d.storecode AND c.bopmastkey = d.bmkey
  LEFT JOIN dimVehicle e on d.vehicleKey = e.vehicleKey
  LEFT JOIN dimVehicleAcquisitionInfo f on b.acqCategory = f.acquisitionType
    AND b.acqCategoryClassification = f.acquisitionTypeCategory
  LEFT JOIN day g on b.dateAcq = g.thedate  
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
--    AND aaa.theyear > 2011   
    AND b.acqCategory = 'Trade'
    AND b.acqCategory = b.acqCategory_1
    AND e.vehicleKey IS NOT NULL 
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;  
  
-- sale > 2011, acqCategory: ark=purchase dps=trade
--- III. H) -- 317
-- 6/6/15 remove date limit, updates 451 rows
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.VehicleKey
FROM (    
  SELECT d.stocknumber, f.vehicleAcquisitionInfoKey, e.datekey, g.vehiclekey 
  FROM (  
    SELECT c.*,
      CASE -- use dps dateAcq
        WHEN dateAcq_1 > saleDate THEN saleDate
        ELSE dateAcq_1
      END AS acqDate,
      CASE right(TRIM(stockNumber),1)
        WHEN 'R' THEN 'Service Loaner'
        WHEN 'X' THEN 'Auction'
        WHEN 'L' THEN 'Lease Purchase'
        WHEN 'P' THEN 'Street'
        ELSE 'Unknown'
      END AS acqTypeCat   
    -- SELECT *  
    -- SELECT COUNT(*) -- 317
    -- this IS the single largest GROUP FROM above, mostly purchases that are
    -- categorized IN dps AS trades although the stocknumbers are X, P, R, L
    FROM (
      SELECT b.*, aaa.thedate AS saledate
      -- SELECT COUNT(*) 
      -- after III.G) 814 LEFT
      -- shit awful lot of recent stuff (20 IN 2015)
      FROM factVehicleAcquisition a
      INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
      INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
      INNER JOIN #wtf b on a.stocknumber = b.stocknumber
      WHERE a.vehicleAcquisitionInfoKey IS NULL) c 
--        AND aaa.theyear > 2011) c
    WHERE c.acqCategory = 'purchase'
    AND c.acqCategory_1 = 'trade'
    AND c.acqCategoryClassification = '' 
    AND c.acqCategoryClassification_1 = 'used') d
  LEFT JOIN day e on d.acqDate = e.thedate 
  LEFT JOIN dimVehicleAcquisitionInfo f on f.acquisitionType = 'Purchase'
    AND d.acqTypeCat = f.acquisitionTypeCategory collate ads_default_ci
  LEFT JOIN dimVehicle g on g.vin = 'NA'
 ) x  
 WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;    
  
-- sale > 2011, acqCategory/_1 = trade, acqCatClass/_1 = new, 
--- III. I) -- 97 updated
-- 6/6/15 remove date limit, updates 253 rows
UPDATE factVehicleAcquisition
SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey,
    datekey = x.datekey,
    sourceVehicleKey = x.sourceVehicleKey
FROM (   
  SELECT stocknumber, e.vehicleAcquisitionInfoKey, d.datekey, c.sourceVehicleKey
  -- SELECT COUNT(*) -- this picks up 97 of 121
  FROM (  
    SELECT b.stocknumber, dateAcq_1 AS acqDate,
      (SELECT vehicleKey 
        FROM factVehicleSale 
        WHERE stocknumber = replace(b.stocknumber,'a','')) AS sourceVehicleKey
    -- 121 trades on new car sales with no record IN boptrad
    -- SELECT COUNT(*)
    FROM factVehicleAcquisition a
    INNER JOIN factVehicleSale aa on a.stocknumber = aa.stocknumber
    INNER JOIN day aaa on aa.approveddatekey = aaa.datekey
    INNER JOIN #wtf b on a.stocknumber = b.stocknumber
    WHERE a.vehicleAcquisitionInfoKey IS NULL  
--      AND aaa.theyear > 2011  
    AND acqCategory = 'trade'
    AND acqCategory_1 = 'trade'
    AND acqCategoryClassification = 'new' 
    AND acqCategoryClassification_1 = 'new') c
  LEFT JOIN day d on c.acqDate = d.thedate  
  LEFT JOIN dimVehicleAcquisitionInfo e on e.acquisitionType = 'Trade'
    AND e.acquisitionTypeCategory = 'New'
  WHERE sourceVehicleKey IS NOT NULL
) x    
 WHERE factVehicleAcquisition.stocknumber = x.stocknumber
  AND factVehicleAcquisition.vehicleAcquisitionInfoKey IS NULL;    
  

-------------------------------------------------------------------------------
-- 6/6/16
-------------------------------------------------------------------------------

--- III. J)
-- 1450 of 20294 with no acqInfoKey
SELECT COUNT(*) 
-- SELECT *
FROM factVehicleAcquisition
WHERE vehicleAcquisitionInfoKey IS NULL 

-- III. J-1
-- new car trades, derive acqDate FROM new car sale WHEN the nc stocknumber
-- EXISTS IN factVehicleSale
-- updated 243 rows
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 13, -- used-trade-new
      datekey = x.datekey, -- date of new car sale
      sourceVehicleKey =  x.vehiclekey -- new car sale on which this was traded
FROM (     
  SELECT a.stocknumber, c.datekey, b.vehiclekey 
  FROM (
    SELECT a.*, replace(replace(replace(replace(replace(replace(replace(replace(replace
          (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
            '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
          replace(a.stocknumber, 'a', '') AS ncStock
      FROM factVehicleAcquisition a
      WHERE vehicleAcquisitionInfoKey IS NULL) a
    LEFT JOIN factVehicleSale b on replace(a.stocknumber, 'a', '') = b.stocknumber
    LEFT JOIN day c on b.approveddatekey = c.datekey
    WHERE a.suffix IN ('a','aa')  
      AND b.stocknumber IS NOT NULL 
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;

-- III. J-2
-- x & xx vehicles, use dps for values, 219 rows updated  
--   first time mixing IN the notion of old stocknumbers, 1988266XX -> 988266X
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = x.vehicleAcquisitionInfoKey, -- used-purchase ...
      datekey = x.datekey, -- dps.VehicleInventoryItems.fromts
      sourceVehicleKey =  x.vehiclekey -- n/a these are ALL purchases
FROM (  
SELECT d.stocknumber, 
  CASE f.typ
    WHEN 'VehicleAcquisition_Auction' THEN 2
    WHEN 'VehicleAcquisition_DealerPurchase' THEN 3
    WHEN 'VehicleAcquisition_IntraMarketPurchase' THEN 5
    WHEN 'VehicleAcquisition_LeasePurcha' THEN 6
    WHEN 'VehicleAcquisition_Street' THEN 11
    WHEN 'VehicleAcquisition_Trade' THEN 13
  END AS vehicleAcquisitionInfoKey,
  (SELECT datekey FROM day WHERE thedate = CAST(e.fromts AS sql_date)) AS dateKey, 
  (SELECT vehicleKey FROM dimVehicle WHERE vin = 'NA') AS vehicleKey
FROM (
  SELECT a.*, 
    substring(a.stocknumber, 2,9) AS oldStock          
  FROM factVehicleAcquisition a
  WHERE a.vehicleAcquisitionInfoKey IS NULL 
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
          (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
            '4',''),'5',''),'6',''),'7',''),'8',''),'9','') IN ('x','xx')) d
LEFT JOIN dps.VehicleInventoryItems e on d.stocknumber = e.stocknumber
  OR d.oldstock = e.stocknumber   
LEFT JOIN dps.vehicleAcquisitions f on e.VehicleInventoryItemID = f.VehicleInventoryItemID  
WHERE e.stocknumber IS NOT NULL 
) x  
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;

 
-- III. J-3
-- x vehicles that were intra market purchases
-- 141 rows updated
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 5, -- intra market purchase
      datekey = x.datekey, -- factVehicleSale sale date
      sourceVehicleKey =  x.vehiclekey -- n/a these are ALL purchases
FROM (  
  SELECT a.stocknumber, aaaa.datekey,
    (SELECT vehicleKey FROM dimVehicle WHERE vin = 'NA') AS vehicleKey    
  -- SELECT a.*, aaaa.thedate, b.vin, b.make, b.model, c.stocknumber, c.fromts, c.thruts
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  LEFT JOIN dimVehicle aa on a.vehicleKey = aa.vehicleKey
  LEFT JOIN factVehicleSale aaa on a.stocknumber = aaa.stocknumber
  LEFT JOIN day aaaa on aaa.approveddatekey = aaaa.datekey
  LEFT JOIN dps.VehicleItems b on aa.vin = b.vin
  LEFT JOIN dps.VehicleInventoryItems c on b.VehicleItemID = c.VehicleItemID 
  WHERE vehicleAcquisitionInfoKey IS NULL 
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
          (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
            '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'x'
    AND abs(aaaa.thedate - CAST(c.thruts AS sql_date)) < 10    
    AND LEFT(a.stocknumber,1) <> LEFT(c.stocknumber, 1)
) x        
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;
     
-- III. J-4          
-- b vehicles - trade IN on "a" used cars 103 rows updated
      
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 14, -- used trades
      datekey = x.datekey, -- factVehicleSale sale date for "A" vehicle
      sourceVehicleKey =  x.vehiclekey -- "A" vehicle vehicleKey
FROM (         
  SELECT a.stocknumber, c.datekey, b.vehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  LEFT JOIN factVehicleSale b  on replace(a.stocknumber, 'b','a') = b.stocknumber
  LEFT JOIN day c on b.approveddatekey = c.datekey
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'b'  
    AND b.storecode IS NOT NULL                
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;
  
  
-- III. J-5         
-- "A" vehicles - deleted 140 rows
-- ** there are no factVehicleSale records for these new car stocknumbers
SELECT a.*, c.thedate,  replace(a.stocknumber, 'a', ''), b.*
FROM factVehicleAcquisition a
LEFT JOIN factVehicleSale b  on replace(a.stocknumber, 'a', '') = b.stocknumber
LEFT JOIN day c on b.approveddatekey = c.datekey
WHERE vehicleAcquisitionInfoKey IS NULL   
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'a'
-- so TRY the "old" stock # for the new car deals - nope, no good                          
SELECT a.*, c.thedate,  substring(replace(a.stocknumber, 'a', ''), 2,9) AS oldStock , b.*
FROM factVehicleAcquisition a
LEFT JOIN factVehicleSale b  on substring(replace(a.stocknumber, 'a', ''), 2,9) = b.stocknumber
LEFT JOIN day c on b.approveddatekey = c.datekey
WHERE vehicleAcquisitionInfoKey IS NULL   
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'a'
-- so just whack them FROM factVehicleAcquisition
DELETE 
FROM factVehicleAcquisition
WHERE stocknumber IN (
  SELECT stocknumber
  -- SELECT count(*)
  FROM factVehicleAcquisition 
  WHERE vehicleAcquisitionInfoKey IS NULL   
      AND replace(replace(replace(replace(replace(replace(replace(replace(replace
              (replace(substring(stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
                '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'a');
             
-- III. J-6          
-- XXA vehicles - trade IN on "XX" used cars ,-- 81 rows updated     
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 14, -- used trades
      datekey = x.datekey, -- factVehicleSale sale date for "A" vehicle
      sourceVehicleKey =  x.vehiclekey -- "A" vehicle vehicleKey
FROM (
  SELECT a.stocknumber, c.datekey, b.vehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  LEFT JOIN factVehicleSale b  on replace(a.stocknumber, 'a','') = b.stocknumber
  LEFT JOIN day c on b.approveddatekey = c.datekey
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'xxa'  
    AND b.storecode IS NOT NULL 
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;


-- III. J-7          
-- X vehicles - these are old AND unresolveable
-- whack 'em -- 89
DELETE 
FROM factVehicleAcquisition
WHERE stocknumber IN (
  SELECT a.stocknumber
  FROM factVehicleAcquisition a
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'x');  


-- III. J-8
-- XA vehicles - trade IN on "X" used cars, 49 rows updated
UPDATE factVehicleAcquisition
  SET vehicleAcquisitionInfoKey = 14, -- used trades
      datekey = x.datekey, -- factVehicleSale sale date for "X" vehicle
      sourceVehicleKey =  x.vehiclekey -- "A" vehicle vehicleKey
FROM (
  SELECT a.stocknumber, c.datekey, b.vehicleKey
  -- SELECT COUNT(*)
  FROM factVehicleAcquisition a
  LEFT JOIN factVehicleSale b  on replace(a.stocknumber, 'a','') = b.stocknumber
  LEFT JOIN day c on b.approveddatekey = c.datekey
  WHERE a.vehicleAcquisitionInfoKey IS NULL  
    AND replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') = 'xa'  
    AND b.storecode IS NOT NULL
) x
WHERE factVehicleAcquisition.stocknumber = x.stocknumber;     

    
--- IV. 
-- current inventory, need to fashion factVehicleAcquisition rows
-- here is current inventory FROM arkona   

DROP TABLE tmpAisArkInventory;
CREATE TABLE tmpAisArkInventory (
  stocknumber cichar(9),
  vin cichar(17),
  theDate date,
  acquisitionType cichar(10),
  acquisitionTypeCategory cichar(24),
  suffix cichar(9),
  noY cichar(9),
  sourceStock cichar(9)) IN database;
  
DELETE FROM tmpAisArkInventory;
INSERT INTO  tmpAisArkInventory 
SELECT imstk#, imvin, imdinv,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
    ELSE 'Purchase'
  END AS category,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN
      CASE 
        WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
        ELSE 'Used'
      END
    ELSE ''
  END, 
  suffix, stockNumberNoY,
  CASE
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN
-- need to handle BB, CC DD, AC, AD etc
      CASE
        WHEN suffix IN ('A','AA','AAA','AAAA') THEN replace(imstk#, 'A', '')
        WHEN suffix IN ('AB','B') THEN replace(imstk#, 'B', 'A')
        WHEN suffix = 'C' THEN replace(imstk#, 'C', 'B')
        WHEN suffix = 'D' THEN replace(imstk#, 'D', 'C')
        WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'A' THEN replace(imstk#, 'A', '')
        WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'B' THEN replace(imstk#, 'B', 'A')
        WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'C' THEN replace(imstk#, 'C', 'B')
        WHEN length(suffix) = 2 AND right(TRIM(suffix), 1) = 'D' THEN replace(imstk#, 'D', 'C')
        WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'A' THEN replace(imstk#, 'A', '')
        WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'B' THEN replace(imstk#, 'B', 'A')
        WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(imstk#, 'C', 'B')
        WHEN LEFT(suffix, 2) = 'xx' AND right(TRIM(suffix),1) = 'C' THEN replace(imstk#, 'D', 'C')
      END 
    ELSE 'NA'
  END AS sourceStock
-- SELECT COUNT(*)
FROM (  
  SELECT imco#, imstk#, imvin, imdinv, 'Purchase', '',
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.imstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix, 
    replace(a.imstk#, 'y', '') AS stockNumberNoY
  FROM stgArkonaINPMAST a
  WHERE imstk# <> ''
    AND imstat = 'i'
    AND imtype = 'u') x;


-- IV.A
-- ALL trades
INSERT INTO factVehicleAcquisition (storecode,stocknumber,noY,
  vehicleAcquisitionInfoKey, vehicleKey, dateKey, sourceVehicleKey)
SELECT b.storecode, a.stocknumber, a.noy, c.vehicleAcquisitionInfoKey, 
  d.vehiclekey, b.approvedDateKey, b.vehicleKey
-- SELECT *
-- SELECT COUNT(*)
FROM tmpAisArkInventory a
INNER JOIN factVehicleSale b on a.sourceStock = b.stocknumber
LEFT JOIN dimVehicleAcquisitionInfo c on a.acquisitionType = c.acquisitionType
  AND a.acquisitionTypeCategory = c.acquisitionTypeCategory
LEFT JOIN dimVehicle d on a.vin = d.vin  
WHERE a.acquisitionType = 'Trade';

-- IV.B
-- purchases

/* Working out purchases for current inventory ********************************
-- wow, fucking purchases are categorized ALL over the fucking place
-- get a COUNT, TRY to narrow it down to some form of consistency
-- ADD acqInfoKey to this
DROP TABLE #purch;
SELECT suffix, max(right(trim(suffix), 1)) AS suffix_1, COUNT(*) AS theCount,
  SUM(CASE WHEN typ = 'VehicleAcquisition_IntraMarketPurchase' THEN 1 ELSE 0 END) AS IMWS,
  vehicleAcquisitionInfoKey, acquisitionTypeCategory, typ
INTO #purch  
FROM (
  SELECT a.*,
    replace(replace(replace(replace(replace(replace(replace(replace(replace
            (replace(substring(a.noy, 5, 15),'0',''),'1',''),'2',''),'3',''),
              '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
    c.typ, aa.vehicleAcquisitionInfoKey, aa.acquisitionTypeCategory              
  FROM factVehicleAcquisition a
  INNER JOIN dimVehicleAcquisitionInfo aa on a.vehicleAcquisitionINfoKey = aa.vehicleAcquisitionInfoKey
  LEFT JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
  LEFT JOIN dps.vehicleAcquisitions c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
--  WHERE a.vehicleAcquisitionInfoKey < 13
  WHERE aa.acquisitionType = 'Purchase'
) x
GROUP BY suffix, vehicleAcquisitionInfoKey, acquisitionTypeCategory, typ



-- trying to come up with a useable scheme for categorizing purchase types
SELECT * FROM #purch 
--ORDER BY suffix_1, suffix
ORDER BY thecount DESC 

SELECT * FROM #purch 
ORDER BY suffix_1, suffix

based on suffix_1
A thru F: error, should NOT be a purchase
G: IMWS (acq BY RY2)
K: Unknown - no way to definitively know
L: Lease Purchase 
   IN AND Out 
   check dps.inandouts
N: National 
P: use dps.vehicleAcquisistions
R: Rental
S: Unknown
T: T: Unknown
   DT: Driver Training 
X: X: auction
      IMWS (acq BY RY1)
   XX: auction
Z: unknown 
-- Working out purchases for current inventory *******************************/

INSERT INTO factVehicleAcquisition (storecode,stocknumber,noY,
  vehicleAcquisitionInfoKey, vehicleKey, dateKey, sourceVehicleKey)
-- SELECT *
SELECT CASE LEFT(a.stocknumber,1) WHEN 'H' THEN 'RY2' ELSE 'RY1' END AS storecode,
  a.stocknumber, a.noy, d.vehicleAcquisitionInfoKey, aa.vehicleKey, 
  (SELECT datekey FROM day WHERE thedate = a.thedate),
  (SELECT vehiclekey FROM dimVehicle WHERE vin = 'NA')
from tmpAisArkInventory a
INNER JOIN dimVehicle aa on a.vin = aa.vin
LEFT JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
LEFT JOIN dps.vehicleAcquisitions c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN dimVehicleAcquisitionInfo d on a.acquisitionType = d.acquisitionType
  AND d.acquisitionTypeCategory = 
    CASE
      WHEN right(trim(suffix), 1) IN ('a','b','c','d','e','f','s') THEN 'Unknown'
      WHEN suffix = 'G' THEN 'Intra Market Wholesale'
      WHEN suffix = 'K' THEN 'Unknown'
      WHEN suffix = 'L' THEN 
        CASE
          WHEN c.typ IN ('VehicleAcquisition_LeasePurcha', 'VehicleAcquisition_Trade') THEN 'Lease Purchase'
          WHEN c.typ = 'VehicleAcquisition_InAndOut' THEN 'In And Out'
          ELSE 'Unknown'
        END
      WHEN right(trim(suffix), 1) = 'N' THEN 'National' 
      WHEN suffix = 'P' THEN 'Street'
      WHEN right(trim(suffix), 1) = 'R' THEN 'Rental'
      WHEN right(TRIM(suffix), 1) = 'T' THEN
        CASE
          WHEN right(TRIM(suffix), 2) = 'DT' THEN 'Driver Training'
          ELSE 'Unknown'
        END 
      WHEN right(TRIM(suffix), 1) = 'X' THEN
        CASE
          WHEN length(TRIM(suffix)) = 1 THEN
            CASE
              WHEN c.typ = 'VehicleAcquisition_Auction' THEN 'Auction'
              WHEN c.typ = 'VehicleAcquisition_IntraMarketPurchase' THEN 'Intra Market Wholesale'
              ELSE 'Unknown'
            END 
          WHEN suffix = 'XX' THEN 'Auction'
          ELSE 'Unknown'
        END 
      
      WHEN right(TRIM(suffix), 1) = 'Z' THEN 'Unknown'
    END
WHERE a.acquisitionType = 'Purchase';

-- V.
-- factVehicleInventory
-- some WORK done IN come at it FROM dps 2-2-15.sql
-- 6/9/15
IS a vehicle IN inventory on 6/5/15 IF it IS sold on 6/5/15, i am going to say
  yes, makes no sense to sell a vehicle that IS NOT IN inventory, whatever
  talk to greg
this might have some relevance
IN postgres with interval types, open-closed intervals

-- DROP TABLE factVehicleInventory;
CREATE TABLE factVehicleInventory (
-- added noY
  storeCode cichar(3),
  stockNumber cichar(9),  
  noY cichar(9),
  vehicleKey integer,
  fromDateKey integer,
  thruDateKey integer) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'storeCode', 'storeCode', 
  '', 2, 512, NULL );  
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'stockNumber', 'stockNumber', 
  '', 2, 512, NULL );  
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'vehicleKey', 'vehicleKey', 
  '', 2, 512, NULL );   
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'fromDateKey', 'fromDateKey', 
  '', 2, 512, NULL ); 
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'thruDateKey', 'thruDateKey', 
  '', 2, 512, NULL );   
EXECUTE PROCEDURE sp_CreateIndex90( 'factVehicleInventory', 
  'factVehicleInventory.adi', 'noY', 'noY', 
  '', 2, 512, NULL );   


DELETE FROM factVehicleInventory;
INSERT INTO factVehicleInventory
SELECT a.storecode, a.stocknumber, a.noy, a.vehiclekey, b.datekey,
  coalesce(d.datekey, (SELECT datekey FROM day WHERE datetype <> 'DATE'))
FROM factVehicleAcquisition a
INNER JOIN day b on a.datekey = b.datekey
LEFT JOIN factVehicleSale c on a.noy = c.noy
LEFT JOIN day d on c.approveddatekey = d.datekey;


/*   test for anomalies  ******************************************************/
???????????
instead of just a source vehicle key, should i be storing a source stocknumber AS well
because a vehiclekey does NOT point definitively to an invenory instance of a vehicle
???????????
-- dps.inandouts useful for categorizing sales only

-- current inventory with no acq row ------------------------------------------
select *
FROM tmpAisArkInventory a
LEFT JOIN factVehicleAcquisition b on a.stocknumber = b.stocknumber
WHERE b.stocknumber IS NULL 
-------------------------------------------------------------------------------

-- NULL source vehiclekey on trade --------------------------------------------
SELECT *
-- SELECT COUNT(*)
FROM factVehicleAcquisition
WHERE datekey IS NOT NULL
AND sourcevehiclekey IS NULL 

-- vehicle sale with no stocknumber -------------------------------------------
select *
-- SELECT COUNT(*)
FROM factVehicleSale
WHERE stocknumber = ''

select a.storecode, b.vin, c.vehicletype, d.thedate, e.*
-- SELECT COUNT(*)
FROM factVehicleSale a
INNER JOIN dimVehicle b on a.vehicleKey = b.vehicleKey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  AND c.vehicleTypeCode = 'U'
INNER JOIN day d on a.approvedDateKey = d.datekey
LEFT JOIN stgArkonaINPMAST e on b.vin = e.imvin
WHERE a.stocknumber = ''

-- don't know how OR IF, i still don't understand bopvref, but may be of some help
SELECT a.storecode, c.thedate, a.buyerkey, d.fullname, f.*
FROM factVehicleSale a
INNER JOIN day c on a.approveddatekey = c.datekey
INNER JOIN dimcustomer d on a.buyerkey = d.customerkey
INNER JOIN dimVehicle e on a.vehicleKEy = e.vehicleKey
LEFT JOIN stgArkonaBOPVREF f on e.vin = f.bvvin 
--  AND a.storecode = f.bvco#
  AND d.bnkey = f.bvkey
WHERE stocknumber = ''

-------------------------------------------------------------------------------
-- veh acq with no acqInfoKey
SELECT *
-- SELECT COUNT(*)           
FROM factVehicleAcquisition a
WHERE vehicleAcquisitionInfoKey IS NULL  

-- used car sale with no acquisition record -----------------------------------
select *
-- SELECT COUNT(*)
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey  
  AND b.vehicleTypeCode = 'U'
LEFT JOIN factVehicleAcquisition c on a.stocknumber = c.stocknumber
WHERE c.stocknumber IS NULL 
-- a bunch of these are no stocknumber fctVehSale rows
select aa.thedate, a.*
-- SELECT COUNT(*)
FROM factVehicleSale a
INNER JOIN day aa on a.approveddatekey = aa.datekey
INNER JOIN dimCarDealInfo b on a.carDealInfoKey = b.carDealInfoKey  
  AND b.vehicleTypeCode = 'U'
LEFT JOIN factVehicleAcquisition c on a.stocknumber = c.stocknumber
WHERE c.stocknumber IS NULL 
ORDER BY thedate DESC 

/*
select e.stocknumber, CAST(a.soldts AS sql_Date) AS soldDate, b.stocknumber, d.vin,
  d.modelyear, d.make, d.model 
-- SELECT COUNT(*)
FROM dps.vehiclesales a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID
INNER JOIN dimVehicle d on c.vin = d.vin 
INNER JOIN (
  SELECT a.stocknumber, a.vehiclekey
  FROM factVehicleAcquisition a
  LEFT JOIN factVehicleSale b on a.stocknumber = b.stocknumber
  WHERE vehicleAcquisitionInfoKey IS NULL) e on d.vehiclekey = e.vehiclekey
ORDER BY d.vin 
*/
-------------------------------------------------------------------------------

-- dup stocknumbers IN factVehicleAcquisition ---------------------------------
SELECT b.thedate, a.*
FROM factVehicleAcquisition a
LEFT JOIN day b on a.datekey = b.datekey
WHERE stocknumber IN (    
  SELECT stocknumber
  FROM factVehicleAcquisition
  GROUP BY stocknumber 
  HAVING COUNT(*) > 1)   
ORDER BY stocknumber
-------------------------------------------------------------------------------

-- IN ark inventory NOT IN dps ------------------------------------------------
SELECT *
FROM tmpAisArkInventory a 
LEFT JOIN dps.VehicleInventoryItems b on a.stocknumber = b.stocknumber
WHERE b.stocknumber IS NULL 
-------------------------------------------------------------------------------

-- decoding source stock number -----------------------------------------------
need to handle BB, CC DD, AC, AD etc
source decoding needs to be on noY
here are ALL existing suffixes:
SELECT COUNT(*),
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix
FROM factVehicleSale
WHERE     replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') <> ''        
GROUP BY     replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','')
ORDER BY COUNT(*) DESC  
-------------------------------------------------------------------------------

-- existing "N" vehicles ------------------------------------------------------
are NOT national, but a variety of goofy shit
-------------------------------------------------------------------------------

