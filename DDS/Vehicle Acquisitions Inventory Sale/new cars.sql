SELECT COUNT(*)
-- SELECT a.*, b.thedate, c.*
FROM factVehicleSale a
INNER JOIN day b on a.approvedDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
WHERE c.vehicletypecode = 'n'
  AND year(b.thedate) > 2013
  
  
  
SELECT c.dealtype, c.saletype, COUNT(*)
FROM factVehicleSale a
INNER JOIN day b on a.approvedDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
WHERE c.vehicletypecode = 'n'
  AND year(b.thedate) > 2013  
  AND storecode = 'ry1'
GROUP BY c.dealtype, c.saletype  


Inventory Date - The date the vehicle was received in inventory. 
Its used to calculate the age of the vehicle and should be entered in 
mmddyy format, without dashes or slashes. The following also apply to this 
date: It is the date the GL entries post when stocking in the vehicle.
  It should be left blank when entering vehicles that were ordered and have not 
    yet arrived. If left blank, you WILL NOT be prompted to make journal entries.
  The accounting month for this date must be open. 
  It can be changed after stock-in if you would like to �re-age� the vehicles. 
  It is an important sort criteria for Vehicle Inventory Analysis reports. 
  For Trade Vehicles, this will be the date the deal was Accepted. 


DROP TABLE #wtf;
SELECT a.storecode, a.stocknumber, b.thedate AS sale_date, vehicleCost, vehiclePrice,
  d.imdinv, d.imdsvc, d.imddlv, e.ro_open_date, e.ro_close_date
--INTO #wtf  
FROM factVehicleSale a
INNER JOIN day b on a.approvedDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
LEFT JOIN stgArkonaINPMAST d on a.stocknumber = d.imstk#
  AND d.imstk# <> ''
LEFT JOIN ( -- pdi ro 
  SELECT ro, vehiclekey, b.thedate AS ro_open_date, c.thedate AS ro_close_date 
  FROM factRepairOrder a
  INNER JOIN day b on a.opendatekey = b.datekey
  INNER JOIN day c on a.closedatekey = c.datekey
  INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
    AND d.opcode = 'PDI'  
  GROUP BY ro, vehiclekey, b.thedate, c.thedate) e on a.vehicleKey = e.vehicleKey
WHERE c.vehicletypecode = 'n'
  AND year(b.thedate) > 2013
  AND a.stocknumber <> ''
 
-- no dup stocknumbers  
SELECT stocknumber FROM #wtf GROUP BY stocknumber HAVING COUNT(*) > 1

select * FROM #wtf

-- 5/17/15
get the keyper fob dates
delphi: ext_keyper_assets.exe, part of EXE GROUP
run nightly

DROP TABLE ext_keyper_assets;
CREATE TABLE ext_keyper_assets (
  stocknumber cichar(9) constraint NOT NULL,
  created_date date constraint NOT NULL) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 'ext_keyper_assets', 
  'ext_keyper_assets.adi', 'stocknumber', 'stocknumber', 
  '', 2, 512, NULL );
EXECUTE PROCEDURE sp_CreateIndex90( 'ext_keyper_assets', 
  'ext_keyper_assets.adi', 'created_date', 'created_date', 
  '', 2, 512, NULL );   
  
select *
FROM #wtf a
LEFT JOIN (
  select stocknumber, min(created_date), max(created_date)
  FROM ext_keyper_assets
  GROUP BY stocknumber) b on a.stocknumber = b.stocknumber

  
-- re-arrange, ALL dates & ALL dimKeys  
-- ADD transport, total care ros - use opendate only for ros
DROP TABLE #wtf;
SELECT a.storecode, a.stocknumber, b.thedate AS sale_date, 
  d.imdinv, e.ro_open_date, e.ro_close_date, f.minKeyperDate, f.maxKeyperDate,
  a.vehicleCost, a.vehiclePrice, a.carDealInfoKey, a.vehicleKey, a.buyerKey, 
  a.coBuyerKey, a.consultantKey, a.managerKey
INTO #wtf  
FROM factVehicleSale a
INNER JOIN day b on a.approvedDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
LEFT JOIN stgArkonaINPMAST d on a.stocknumber = d.imstk#
  AND d.imstk# <> ''
LEFT JOIN ( -- pdi ro 
  SELECT ro, vehiclekey, b.thedate AS ro_open_date, c.thedate AS ro_close_date 
  FROM factRepairOrder a
  INNER JOIN day b on a.opendatekey = b.datekey
  INNER JOIN day c on a.closedatekey = c.datekey
  INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
    AND d.opcode = 'PDI'  
  GROUP BY ro, vehiclekey, b.thedate, c.thedate) e on a.vehicleKey = e.vehicleKey
LEFT JOIN ( -- keyper fob date
  select stocknumber, min(created_date) AS minKeyperDate, 
    max(created_date) AS maxKeyperDate
  FROM ext_keyper_assets
  GROUP BY stocknumber) f on a.stocknumber = f.stocknumber  
WHERE c.vehicletypecode = 'n'
  AND year(b.thedate) > 2013
  AND a.stocknumber <> ''
  AND a.storecode = 'ry1'
 

-- just ro OPEN dates: pdi & transport  
DROP TABLE #wtf;
SELECT a.storecode, a.stocknumber, b.thedate AS sale_date, 
  coalesce(d.imdinv, cast('12/31/9999' AS sql_date)) AS invoice_date, 
  coalesce(e.ro_open_date, cast('12/31/9999' AS sql_date)) AS pdi_date, 
  coalesce(ee.ro_open_date, cast('12/31/9999' AS sql_date)) AS tsp_date, 
  coalesce(f.minKeyperDate, cast('12/31/9999' AS sql_date)) AS minKeyperDate,
  coalesce(f.maxKeyperDate, cast('12/31/9999' AS sql_date)) AS maxKeyperDate,
  a.vehicleCost, a.vehiclePrice, a.carDealInfoKey, a.vehicleKey, a.buyerKey, 
  a.coBuyerKey, a.consultantKey, a.managerKey
INTO #wtf  
FROM factVehicleSale a
INNER JOIN day b on a.approvedDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
LEFT JOIN stgArkonaINPMAST d on a.stocknumber = d.imstk#
  AND d.imstk# <> ''
LEFT JOIN ( -- pdi ro 
  SELECT ro, vehiclekey, b.thedate AS ro_open_date, c.thedate AS ro_close_date 
  FROM factRepairOrder a
  INNER JOIN day b on a.opendatekey = b.datekey
  INNER JOIN day c on a.closedatekey = c.datekey
  INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
    AND d.opcode = 'PDI'  
  GROUP BY ro, vehiclekey, b.thedate, c.thedate) e on a.vehicleKey = e.vehicleKey
LEFT JOIN ( -- transport ro 
  SELECT ro, vehiclekey, b.thedate AS ro_open_date, c.thedate AS ro_close_date 
  FROM factRepairOrder a
  INNER JOIN day b on a.opendatekey = b.datekey
  INNER JOIN day c on a.closedatekey = c.datekey
  INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
    AND d.opcode = 'TSP'  
  GROUP BY ro, vehiclekey, b.thedate, c.thedate) ee on a.vehicleKey = ee.vehicleKey 
LEFT JOIN ( -- keyper fob date
  select stocknumber, min(created_date) AS minKeyperDate, 
    max(created_date) AS maxKeyperDate
  FROM ext_keyper_assets
  GROUP BY stocknumber) f on a.stocknumber = f.stocknumber  
WHERE c.vehicletypecode = 'n'
  AND year(b.thedate) > 2013
  AND a.stocknumber <> ''
--  AND a.storecode = 'ry1'

/*
5/22/15
instead of looking for an individual type of ro, use the first ro created for
the vehicle key, that will cover the situation WHERE the first ro may be 
accessories OR total care OR detail, NOT just pdi OR transport
*/ 


DROP TABLE #wtf;
SELECT a.storecode, a.stocknumber, b.thedate AS sale_date, 
  coalesce(d.imdinv, cast('12/31/9999' AS sql_date)) AS invoice_date, 
  coalesce(e.first_ro_date, cast('12/31/9999' AS sql_date)) AS first_ro_date, 
  coalesce(f.minKeyperDate, cast('12/31/9999' AS sql_date)) AS minKeyperDate,
  coalesce(f.maxKeyperDate, cast('12/31/9999' AS sql_date)) AS maxKeyperDate,
  a.vehicleCost, a.vehiclePrice, a.carDealInfoKey, a.vehicleKey, a.buyerKey, 
  a.coBuyerKey, a.consultantKey, a.managerKey
INTO #wtf  
FROM factVehicleSale a
INNER JOIN day b on a.approvedDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
LEFT JOIN stgArkonaINPMAST d on a.stocknumber = d.imstk#
  AND d.imstk# <> ''
LEFT JOIN (  
  SELECT vehiclekey, MIN(b.thedate) AS first_ro_date
  FROM factRepairORder a
  INNER JOIN day b on a.opendatekey = b.datekey
  GROUP BY vehiclekey) e on a.vehiclekey = e.vehiclekey  
LEFT JOIN ( -- keyper fob date
  select stocknumber, min(created_date) AS minKeyperDate, 
    max(created_date) AS maxKeyperDate
  FROM ext_keyper_assets
  GROUP BY stocknumber) f on a.stocknumber = f.stocknumber  
WHERE c.vehicletypecode = 'n'
  AND year(b.thedate) > 2013
  AND a.stocknumber <> ''
--  AND a.storecode = 'ry1'

-- no dups
SELECT stocknumber
FROM #wtf
GROUP BY stocknumber 
HAVING COUNT(*) > 1

/*
5/22/15
honda does NOT DELETE a fob FROM keyper before reassigning it to a different vehicle
resulting IN fob creation dates for new vehicles that are months before
the invoice date
therefore, fob creation date IS NOT a good indicator of honda vehicles HAVING
arrived
*/
-- DROP TABLE #wtf1; 
SELECT a.storecode, a.stocknumber, a.sale_date, a.vehiclecost, a.vehicleprice,
  a.cardealinfokey, a.vehiclekey, a.buyerkey, a.cobuyerkey, a.consultantkey,
  a.managerkey,
  CASE
    -- Rental
    WHEN right(TRIM(stocknumber), 1) = 'R' THEN
      CASE
        -- 1st choice invoice_date
        WHEN invoice_date <= sale_date THEN invoice_date
        ELSE 
          CASE 
            -- 2nd choice keyper_date
            WHEN minKeyperDate <= sale_date THEN minKeyperDate
            -- last choice sale_Date
            ELSE sale_date
          END           
      END 
    -- 1st choice: first_ro_date
    WHEN first_ro_date <= sale_date THEN first_ro_date
--    -- 2nd choice tsp_date
--    WHEN tsp_date <= sale_date THEN tsp_date
    -- 3rd choice keyper, only for RY1
    WHEN storecode = 'RY1' AND minKeyperDate <= sale_date THEN minKeyperDate
    -- 4th choice invoice_date
    WHEN invoice_date <= sale_date THEN invoice_date
    -- last choice sale_date
    ELSE sale_date
  END AS acq_date
INTO #wtf1  
FROM #wtf a  

select * FROM #wtf
select * FROM #wtf1

-- non rentals WHERE acq_date = keyper_date, only 2
select * 
FROM #wtf1 a
INNER JOIN #wtf b on a.stocknumber = b.stocknumber
  AND a.acq_date = b.minKeyperDate
  AND b.first_ro_date <> b.minKeyperDate
WHERE right(trim(a.stocknumber),1) <> 'R'

-- ok, for now at least
DROP TABLE ncinv_vehicle_sale;
CREATE TABLE ncinv_vehicle_sale (
  storecode cichar(3) constraint NOT NULL,
  stocknumber cichar(9) constraint NOT NULL,
  acquisitiondatekey integer constraint NOT NULL,
  saledatekey integer constraint NOT NULL,
  cardealinfokey integer constraint NOT NULL,
  vehiclekey integer constraint NOT NULL,
  buyerkey integer constraint NOT NULL,
  cobuyerkey integer constraint NOT NULL,
  consultantkey integer constraint NOT NULL,
  managerkey integer constraint NOT NULL,
  vehiclecost numeric(12,2) constraint NOT NULL,
  vehicleprice numeric(12,2) constraint NOT NULL) IN database;
  
INSERT INTO ncinv_vehicle_sale  
SELECT storecode, stocknumber, 
  (SELECT datekey FROM day WHERE thedate = a.acq_date),
  (SELECT datekey FROM day WHERE thedate = a.sale_date),
  cardealinfokey, vehiclekey, buyerkey, cobuyerkey, consultantkey, 
  managerkey, vehiclecost, vehicleprice
FROM #wtf1 a;

execute procedure sp_createindex90('ncinv_vehicle_sale','ncinv_vehicle_sale.adi','StoreCode','StoreCode','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_sale','ncinv_vehicle_sale.adi','stocknumber','stocknumber','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_sale','ncinv_vehicle_sale.adi','acquisitiondatekey','acquisitiondatekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_sale','ncinv_vehicle_sale.adi','saledatekey','saledatekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_sale','ncinv_vehicle_sale.adi','cardealinfokey','cardealinfokey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_sale','ncinv_vehicle_sale.adi','vehiclekey','vehiclekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_sale','ncinv_vehicle_sale.adi','consultantkey','consultantkey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_sale','ncinv_vehicle_sale.adi','managerkey','managerkey','',2,512,'');


/*
-- current inventory
-- DROP TABLE #inv;
SELECT a.imstk#, a.imdinv AS invoice_date, b.vehiclekey, e.ro_open_date AS pdi_date, 
  ee.ro_open_date AS transport_date, f.minKeyperDate AS keyper_date,
  g.ionval AS best_price, a.impric AS invoice, cast(round(a.imcost, 0) AS sql_integer) AS imcost
-- SELECT *
INTO #inv  
-- SELECT *
FROM stgArkonaINPMAST a
INNER JOIN dimVehicle b on a.imvin = b.vin 
LEFT JOIN ( -- pdi ro 
  SELECT ro, vehiclekey, b.thedate AS ro_open_date, c.thedate AS ro_close_date 
  FROM factRepairOrder a
  INNER JOIN day b on a.opendatekey = b.datekey
  INNER JOIN day c on a.closedatekey = c.datekey
  INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
    AND d.opcode = 'PDI'  
  GROUP BY ro, vehiclekey, b.thedate, c.thedate) e on b.vehicleKey = e.vehicleKey 
LEFT JOIN ( -- transport ro this one generates some dups, take out ro, use MIN date
--  SELECT ro, vehiclekey, b.thedate AS ro_open_date, c.thedate AS ro_close_date
  SELECT vehiclekey, min(b.thedate) AS ro_open_date, min(c.thedate) AS ro_close_date 
  FROM factRepairOrder a
  INNER JOIN day b on a.opendatekey = b.datekey
  INNER JOIN day c on a.closedatekey = c.datekey
  INNER JOIN dimopcode d on a.opcodekey = d.opcodekey
    AND d.opcode = 'TSP'  
--  GROUP BY ro, vehiclekey, b.thedate, c.thedate) ee on b.vehicleKey = ee.vehicleKey
  GROUP BY vehiclekey) ee on b.vehicleKey = ee.vehicleKey 
LEFT JOIN ( -- keyper fob date
  select stocknumber, min(created_date) AS minKeyperDate, 
    max(created_date) AS maxKeyperDate
  FROM ext_keyper_assets
  GROUP BY stocknumber) f on a.imstk# = f.stocknumber  
LEFT JOIN stgArkonaINPOPTF g on a.imvin = g.iovin
  AND g.ioseq# = 9  
WHERE a.imstk# <> ''
  AND a.imstat = 'i'
  AND a.imtype = 'n'
*/

-- current inventory
-- redo it with first_ro_date
-- DROP TABLE #inv;
SELECT a.imstk#, a.imdinv AS invoice_date, b.vehiclekey, e.first_ro_date, 
  f.minKeyperDate AS keyper_date, g.ionval AS best_price, 
  cast(round(a.impric, 0) AS sql_integer) AS invoice, 
  cast(round(a.imcost, 0) AS sql_integer) AS imcost
-- SELECT *
INTO #inv  
-- SELECT *
FROM stgArkonaINPMAST a
INNER JOIN dimVehicle b on a.imvin = b.vin 
LEFT JOIN (  
  SELECT vehiclekey, MIN(b.thedate) AS first_ro_date
  FROM factRepairORder a
  INNER JOIN day b on a.opendatekey = b.datekey
  GROUP BY vehiclekey) e on b.vehiclekey = e.vehiclekey  
LEFT JOIN ( -- keyper fob date
  select stocknumber, min(created_date) AS minKeyperDate, 
    max(created_date) AS maxKeyperDate
  FROM ext_keyper_assets
  GROUP BY stocknumber) f on a.imstk# = f.stocknumber  
LEFT JOIN stgArkonaINPOPTF g on a.imvin = g.iovin
  AND g.ioseq# = 9  
WHERE a.imstk# <> ''
  AND a.imstat = 'i'
  AND a.imtype = 'n'
  
-- no dups  
select imstk#
FROM #inv
GROUP BY imstk# HAVING COUNT(*) > 1  

  
-- for each day on which there IS a sale: date,make,model,#sold
SELECT m.thedate, n.make, n.model, coalesce(sales,0) AS sales
INTO #sales
FROM (
  SELECT a.thedate
  FROM day a
  WHERE a.thedate BETWEEN '01/01/2014' AND curdate()) m
LEFT JOIN (
  SELECT b.thedate, d.make, d.model, COUNT(*) AS sales
  FROM ncinv_vehicle_sale a
  INNER JOIN day b on a.saledatekey = b.datekey
  INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  INNER JOIN dimVehicle d on a.vehicleKey = d.vehicleKey
  WHERE a.storecode = 'ry1'
  GROUP BY b.thedate, d.make, d.model) n on m.thedate = n.thedate
WHERE sales IS NOT NULL   -- eliminates sundays, holidays AND 0 sales days
   
SELECT * FROM #sales
    
-- derived historical inventory based on ncinv_vehicle_sale for each day 
--   since 1/1/2014
--   inventory defined AS interval acq_date -> sale_date - 1
--   for each day: date,make,model,#IN inventory
SELECT a.thedate, e.make, e.model, COUNT(*) AS inv
INTO #inven
FROM day a
LEFT JOIN (
  SELECT b.thedate AS acq_date, c.thedate AS sale_date, a.vehicleKey
  FROM ncinv_vehicle_sale a
  INNER JOIN day b on a.acquisitiondatekey = b.datekey
  INNER JOIN day c on a.saledatekey = c.datekey
  WHERE a.storecode = 'ry1') d on a.thedate BETWEEN d.acq_date AND d.sale_Date - 1
LEFT JOIN dimVehicle e on d.vehicleKey = e.vehiclekey  
WHERE a.thedate BETWEEN '01/01/2014' AND curdate()  
GROUP BY a.thedate, e.make, e.model  

SELECT * FROM #inven
-- one row for each date,make,model
SELECT *
INTO #base
FROM (
  SELECT thedate
  FROM day
  WHERE thedate BETWEEN '01/01/2014' AND curdate()) a,  
(
  SELECT b.make, b.model
  FROM ncinv_vehicle_sale a
  INNER JOIN dimVehicle b on a.vehiclekey = b.vehiclekey
  WHERE storecode = 'ry1'
  GROUP BY b.make, b.model
  UNION 
  SELECT b.make, b.model
  FROM #inv a
  INNER JOIN dimVehicle b on a.vehiclekey = b.vehiclekey
  WHERE LEFT(imstk#,1) <> 'h'
  GROUP BY b.make, b.model) b

SELECT * FROM #base  
SELECT make, count(*) FROM #base GROUP BY make
SELECT DISTINCT immake FROM stgArkonaINPMAST
SELECT * FROM #sales

-- for each day on which there IS a sale
-- date,make,model,#sold,#inventory
select a.*, b.sales, c.inv
FROM #base a
LEFT JOIN #sales b on a.thedate = b.thedate
  AND a.make = b.make
  AND a.model = b.model
LEFT JOIN #inven c on a.thedate = c.thedate
  AND a.make = c.make
  AND a.model = c.model
WHERE b.sales IS NOT NULL AND c.inv IS NOT NULL   

-- 5/26/15
-- hmm, only diff BETWEEN acq & inv IS inv.thrudate
-- ? pricing BY day, no way to construct history
DROP TABLE ncinv_vehicle_inventory;
CREATE TABLE ncinv_vehicle_inventory (
  storecode cichar(3) constraint NOT NULL,
  stocknumber cichar(9) constraint NOT NULL,
  invoicedatekey integer constraint NOT NULL default '7306',
  acquisitiondatekey integer constraint NOT NULL default '7306',
  vehiclekey integer constraint NOT NULL,
  thrudatekey integer constraint NOT NULL default '7306',
  vehiclecost integer constraint NOT NULL default '0' ) IN database;
execute procedure sp_createindex90('ncinv_vehicle_inventory','ncinv_vehicle_inventory.adi','StoreCode','StoreCode','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_inventory','ncinv_vehicle_inventory.adi','stocknumber','stocknumber','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_inventory','ncinv_vehicle_inventory.adi','invoicedatekey','invoicedatekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_inventory','ncinv_vehicle_inventory.adi','acquisitiondatekey','acquisitiondatekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_inventory','ncinv_vehicle_inventory.adi','thrudatekey','thrudatekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_inventory','ncinv_vehicle_inventory.adi','vehiclekey','vehiclekey','',2,512,'');  

-- current inventory
INSERT INTO ncinv_vehicle_inventory
SELECT i.storecode, i.imstk#, j.datekey, k.datekey, i.vehiclekey, 
  l.datekey, i.imcost
FROM (  
  SELECT storecode, imstk#, invoice_date, 
    CASE 
      WHEN right(TRIM(imstk#),1) = 'R' THEN invoice_date
      WHEN first_ro_date <= curdate() THEN first_ro_date
      WHEN storecode = 'ry1' AND keyper_date <= curdate() THEN keyper_date
      ELSE CAST('12/31/9999' AS sql_date)
    END AS acq_date, vehiclekey, 
    CAST('12/31/9999' AS sql_date) AS thrudate, imcost
  FROM (
    SELECT 
      CASE WHEN LEFT(imstk#, 1) = 'H' THEN 'RY2' ELSE 'RY1' END AS storecode, 
      a.imstk#, a.imdinv AS invoice_date, b.vehiclekey, 
      coalesce(e.first_ro_date, CAST('12/31/9999' AS sql_date)) AS first_ro_date,
      coalesce(f.minKeyperDate, CAST('12/31/9999' AS sql_date)) AS keyper_date, 
      cast(round(a.imcost, 0) AS sql_integer) AS imcost
    -- SELECT *
    --INTO #inv  
    -- SELECT *
    FROM stgArkonaINPMAST a
    INNER JOIN dimVehicle b on a.imvin = b.vin 
    LEFT JOIN (  
      SELECT vehiclekey, MIN(b.thedate) AS first_ro_date
      FROM factRepairORder a
      INNER JOIN day b on a.opendatekey = b.datekey
      GROUP BY vehiclekey) e on b.vehiclekey = e.vehiclekey  
    LEFT JOIN ( -- keyper fob date
      select stocknumber, min(created_date) AS minKeyperDate, 
        max(created_date) AS maxKeyperDate
      FROM ext_keyper_assets
      GROUP BY stocknumber) f on a.imstk# = f.stocknumber  
    WHERE a.imstk# <> ''
      AND a.imstat = 'i'
      AND a.imtype = 'n') h) i
INNER JOIN day j on i.invoice_date = j.thedate
INNER JOIN day k on i.acq_date = k.thedate
INNER JOIN day l on i.thrudate = l.thedate;     
  
  
-- historical data derived FROM ncinv_vehicle_sale
INSERT INTO ncinv_vehicle_inventory
SELECT h.storecode, h.stocknumber, i.datekey, j.datekey, h.vehiclekey,
  k.datekey, h.vehiclecost
FROM (
  SELECT storecode, stocknumber, sale_date AS thrudate, 
    cast(round(vehiclecost, 0) AS sql_integer) AS vehiclecost, vehiclekey, 
    CASE
      -- Rental
      WHEN right(TRIM(stocknumber), 1) = 'R' THEN
        CASE
          -- 1st choice invoice_date
          WHEN invoice_date <= sale_date THEN invoice_date
          ELSE 
            CASE 
              -- 2nd choice keyper_date
              WHEN minKeyperDate <= sale_date THEN minKeyperDate
              -- last choice sale_Date
              ELSE sale_date
            END           
        END 
      -- 1st choice: first_ro_date
      WHEN first_ro_date <= sale_date THEN first_ro_date
  --    -- 2nd choice tsp_date
  --    WHEN tsp_date <= sale_date THEN tsp_date
      -- 3rd choice keyper, only for RY1
      WHEN storecode = 'RY1' AND minKeyperDate <= sale_date THEN minKeyperDate
      -- 4th choice invoice_date
      WHEN invoice_date <= sale_date THEN invoice_date
      -- last choice sale_date
      ELSE sale_date
    END AS acq_date, invoice_date
  FROM (  
    SELECT a.storecode, a.stocknumber, b.thedate AS sale_date, 
      coalesce(d.imdinv, cast('12/31/9999' AS sql_date)) AS invoice_date, 
      coalesce(e.first_ro_date, cast('12/31/9999' AS sql_date)) AS first_ro_date, 
      coalesce(f.minKeyperDate, cast('12/31/9999' AS sql_date)) AS minKeyperDate,
      a.vehicleCost, a.vehicleKey
    -- INTO #wtf  
    FROM factVehicleSale a
    INNER JOIN day b on a.approvedDateKey = b.datekey
    INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
    LEFT JOIN stgArkonaINPMAST d on a.stocknumber = d.imstk#
      AND d.imstk# <> ''
    LEFT JOIN (  
      SELECT vehiclekey, MIN(b.thedate) AS first_ro_date
      FROM factRepairORder a
      INNER JOIN day b on a.opendatekey = b.datekey
      GROUP BY vehiclekey) e on a.vehiclekey = e.vehiclekey  
    LEFT JOIN ( -- keyper fob date
      select stocknumber, min(created_date) AS minKeyperDate, 
        max(created_date) AS maxKeyperDate
      FROM ext_keyper_assets
      GROUP BY stocknumber) f on a.stocknumber = f.stocknumber  
    WHERE c.vehicletypecode = 'n'
      AND year(b.thedate) > 2013
      AND a.stocknumber <> '') g) h
INNER JOIN day i on h.invoice_date = i.thedate
INNER JOIN day j on h.acq_date = j.thedate
INNER JOIN day k on h.thrudate = k.thedate;      
  
  
select * 
FROM ncinv_vehicle_inventory a
INNER JOIN day b on a.thrudatekey = b.datekey
  AND b.thedate >= curdate()

-- on any given date, what IS nc inv?  
SELECT b.thedate, f.make, f.model, COUNT(*), avg(b.thedate - inv_date)
FROM (  
  SELECT a.thedate
  FROM day a
  WHERE a.thedate BETWEEN '01/01/2014' AND curdate()) b  
LEFT JOIN (
  SELECT a.vehiclekey, b.thedate AS inv_date, c.thedate AS acq_date, 
    d.thedate AS thru_date
  FROM ncinv_vehicle_inventory a
  INNER JOIN day b on a.invoicedatekey = b.datekey
  INNER JOIN day c on a.acquisitiondatekey = c.datekey
  INNER JOIN day d on a.thrudatekey = d.datekey
  WHERE a.storecode = 'RY1') e on b.thedate BETWEEN e.inv_date AND e.thru_date
LEFT JOIN dimVehicle f on e.vehiclekey = f.vehiclekey  
GROUP BY b.thedate, f.make, f.model
    
    
  
select a.*, b.thedate AS invoice_date, c.thedate AS acq_date, d.thedate AS thru_date
FROM ncinv_vehicle_inventory a
INNER JOIN day b on a.invoicedatekey = b.datekey
INNER JOIN day c on a.acquisitiondatekey = c.datekey
INNER JOIN day d on a.thrudatekey = d.datekey

-- current inventory
SELECT d.make, d.model, COUNT(*)
FROM ncinv_vehicle_inventory a
INNER JOIN day b on a.invoicedatekey = b.datekey
INNER JOIN day c on a.thrudatekey = c.datekey
INNER JOIN dimVehicle d on a.vehiclekey = d.vehiclekey
WHERE curdate() BETWEEN b.thedate AND c.thedate
  AND a.storecode = 'ry1'
GROUP BY d.make, d.model  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

-- 5/26/15
DROP TABLE ncinv_vehicle_acquisition;
CREATE TABLE ncinv_vehicle_acquisition (
  storecode cichar(3) constraint NOT NULL,
  stocknumber cichar(9) constraint NOT NULL,
  invoicedatekey
  acquisitiondatekey integer constraint NOT NULL,
  vehiclekey integer constraint NOT NULL,
  vehiclecost numeric(12,2) constraint NOT NULL default ) IN database;
  
INSERT INTO ncinv_vehicle_acquisition  
SELECT storecode, stocknumber, 
  (SELECT datekey FROM day WHERE thedate = a.acq_date),
  (SELECT datekey FROM day WHERE thedate = a.sale_date),
  cardealinfokey, vehiclekey, buyerkey, cobuyerkey, consultantkey, 
  managerkey, vehiclecost, vehicleprice
FROM #wtf1 a;

execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','StoreCode','StoreCode','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','stocknumber','stocknumber','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','acquisitiondatekey','acquisition_datekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','invoice_datekey','invoicedatekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','vehiclekey','vehiclekey','',2,512,'');




-- 5/26/15
DROP TABLE ncinv_vehicle_acquisition;
CREATE TABLE ncinv_vehicle_acquisition (
  storecode cichar(3) constraint NOT NULL,
  stocknumber cichar(9) constraint NOT NULL,
  invoicedatekey
  acquisitiondatekey integer constraint NOT NULL,
  vehiclekey integer constraint NOT NULL,
  vehiclecost numeric(12,2) constraint NOT NULL) IN database;
  
INSERT INTO ncinv_vehicle_acquisition  
SELECT storecode, stocknumber, 
  (SELECT datekey FROM day WHERE thedate = a.acq_date),
  (SELECT datekey FROM day WHERE thedate = a.sale_date),
  cardealinfokey, vehiclekey, buyerkey, cobuyerkey, consultantkey, 
  managerkey, vehiclecost, vehicleprice
FROM #wtf1 a;

execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','StoreCode','StoreCode','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','stocknumber','stocknumber','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','acquisitiondatekey','acquisition_datekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','invoice_datekey','invoicedatekey','',2,512,'');
execute procedure sp_createindex90('ncinv_vehicle_acquisition','ncinv_vehicle_acquisition.adi','vehiclekey','vehiclekey','',2,512,'');

