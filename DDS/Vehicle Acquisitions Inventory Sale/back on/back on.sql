--< dps ------------------------------------------------------------------ dps <
-- this IS NOT a back on: ws to national on 7/2012 AS 17232XX
-- back INTO inventory 3/12/14 AS 17232XXN & retailed 7/12/14
SELECT b.thedate, a.*
FROM factVehicleSale a
LEFT JOIN day b on a.originationDateKey = b.datekey
WHERE vehicleKey = (
  SELECT vehicleKey
  FROM dimVehicle
  WHERE vin = '1G4PR5SK9C4118752')

-- identify a back on: "back on" IN notes  
select *
FROM dps.VehicleInventoryItemNotes   
WHERE CAST(notes AS sql_longvarchar) LIKE '%back on%'
ORDER BY notests DESC 

-- identify a back on: multiple delivered statuses
SELECT *
FROM dps.VehicleInventoryItems 
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID -- multiple delivered statuses
  FROM dps.VehicleInventoryItemStatuses 
  WHERE status = 'RawMaterials_Delivered'
  GROUP BY VehicleInventoryItemID
  HAVING COUNT(*) > 1)
ORDER BY thruts DESC 

--/> dps ----------------------------------------------------------------- dps />


--< arkona ------------------------------------------------------------ arkona <
beats the shit out of me how to identify, let alone model

--/> arkona ----------------------------------------------------------- arkona />
