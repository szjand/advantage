DROP TABLE #2014;
SELECT d.*, f.*, g.*, h.gmdesc
INTO #2014
FROM ( 
  SELECT a.storecode, a.stocknumber, b.thedate AS arkSaleDate, c.saleType, b.yearMonth
  FROM factVehicleSale a
  INNER JOIN day b on a.originationDateKey = b.datekey
  INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  WHERE vehicleTypeCode = 'U'  
    AND b.yearmonth between 201401 AND 201407) d
LEFT JOIN (
  SELECT gtctl#, gtacct, gttamt, gtjrnl
  FROM stgArkonaGLPTRNS e
  WHERE e.gtdate BETWEEN '01/01/2014' AND '07/31/2014') f on d.stocknumber = f.gtctl#
INNER JOIN (
  SELECT dl4, al2, al3, gmcoa, glAccount, page, line
  FROM zcb
  WHERE name = 'gmfs gross'
--    AND storecode = 'ry2'
    AND dl3 = 'variable'
    AND dl5 = 'used') g on f.gtacct = g.glAccount 
LEFT JOIN stgArkonaGLPMAST h on f.gtacct = h.gmacct
  AND h.gmyear = 2014  
 
SELECT * FROM #2014
 
SELECT gtacct, al2, al3, gmdesc, SUM(gttamt)
FROM #2014
WHERE storecode = 'ry2'
--  AND gtacct = '245400'
  AND yearmonth = 201407 
  AND page = 7
GROUP BY gtacct, al2, al3, gmdesc  

  
select al2, al3, SUM(gttamt)
FROM #2014
WHERE storecode = 'ry2'
  AND gtacct = '245400'
  AND yearmonth = 201407 
GROUP BY al2, al3  

SELECT gtacct, al2, al3, gmcoa, glaccount, page, line, gmdesc
FROM #2014
WHERE storecode = 'ry2'
--  AND gtacct = '245400'
  AND yearmonth = 201407 
  AND page = 7
GROUP BY gtacct, al2, al3, gmcoa, glaccount, page, line, gmdesc  
ORDER BY line


SELECT dl4, dl5, al3, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2014
  AND month(gtdate) = 7
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'F&I' 
  AND dl2 = 'RY2'
GROUP BY dl4, dl5, al3, line
ORDER BY line asc

-- this matches fs line 10,20,21
SELECT 'xTotal', SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2014
  AND month(gtdate) = 7
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'F&I' 
  AND dl2 = 'RY2'
UNION 
SELECT dl5, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2014
  AND month(gtdate) = 7
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'F&I' 
  AND dl2 = 'RY2'
GROUP BY dl5

SELECT x.*, -(sales + cogs) AS gross
FROM (
  SELECT dl5, line,
    SUM(CASE WHEN al2 = 'sales' THEN coalesce(gttamt, 0) ELSE 0 END) AS sales,
    SUM(CASE WHEN al2 = 'cogs' THEN coalesce(gttamt, 0) ELSE 0 END) AS cogs
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND year(gtdate) = 2014
    AND month(gtdate) = 7
  WHERE a.name = 'gmfs gross' 
    AND dl4 = 'F&I' 
    AND dl2 = 'RY2'
  GROUP BY dl5, line) x

-- this does not  
SELECT 'xTotal', SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2014
  AND month(gtdate) = 7
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'F&I' 
  AND dl2 = 'RY1'
UNION 
SELECT dl5, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2014
  AND month(gtdate) = 7
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'F&I' 
  AND dl2 = 'RY1'
GROUP BY dl5
-- the problem IS with cogs, sales IS good
SELECT x.*, -(sales + cogs) AS gross
FROM (
  SELECT dl5, 
    SUM(CASE WHEN al2 = 'sales' THEN coalesce(gttamt, 0) ELSE 0 END) AS sales,
    SUM(CASE WHEN al2 = 'cogs' THEN coalesce(gttamt, 0) ELSE 0 END) AS cogs
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND year(gtdate) = 2014
    AND month(gtdate) = 7
  WHERE a.name = 'gmfs gross' 
    AND dl4 = 'F&I' 
    AND dl2 = 'RY1'
  GROUP BY dl5) x
-- the problem IS cogs line 3 & 13: chargebacks
/*
added the missing accounts to zcb.gmfs gross
missing accounts IN zcb.gmfs gross 
  used missing account 185101, 185102 
  new missing account 185001, 185002 

*/  
SELECT x.*, -(sales + cogs) AS gross
FROM (
  SELECT dl5, line, 
    SUM(CASE WHEN al2 = 'sales' THEN coalesce(gttamt, 0) ELSE 0 END) AS sales,
    SUM(CASE WHEN al2 = 'cogs' THEN coalesce(gttamt, 0) ELSE 0 END) AS cogs
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND year(gtdate) = 2014
    AND month(gtdate) = 7
  WHERE a.name = 'gmfs gross' 
    AND dl4 = 'F&I' 
    AND dl2 = 'RY1'
  GROUP BY dl5, line) x  


DROP TABLE #market2014;
SELECT storecode, stocknumber, yearmonth, arkSaleDate as saleDate, 
  - (frontEndGross + frontEndCogs) AS frontEndgross,
  - (backEndGross + backEndCogs) AS backEndgross, 
  arkSaleDate - acqDate AS daysOwned, daysAvail
INTO #market2014  
FROM (  
  SELECT e.*, coalesce(i.theDate, CAST(f.fromTs AS sql_date)) AS acqDate, 
    coalesce(j.daysAvail, 0) AS daysAvail
  FROM (  
    SELECT b.*, d.origStockNumber
    FROM (
      SELECT a.storecode, a.stocknumber, a.arkSaleDate, yearmonth,
        SUM(CASE WHEN a.page = 7 AND al2 = 'sales' THEN gttamt ELSE 0 END) AS backEndGross,
        SUM(CASE WHEN a.page = 7 AND al2 = 'cogs' THEN gttamt ELSE 0 END) AS backEndCOGS,
        SUM(CASE WHEN a.page = 6 AND al2 = 'sales' THEN gttamt ELSE 0 END) AS frontEndGross,
        SUM(CASE WHEN a.page = 6 AND al2 = 'cogs' THEN gttamt ELSE 0 END) AS frontEndCOGS        
      FROM #2014 a
      WHERE a.yearmonth BETWEEN 201401 AND 201407
        AND a.saleType = 'retail'
      GROUP BY a.storecode, a.stocknumber, a.arkSaleDate, yearmonth) b 
    LEFT JOIN ( -- intramarket based on acquisitions
      SELECT a.typ, b.stocknumber, b.fromts, b.thruts, c.vin, d.stocknumber AS origStockNumber, d.fromTS, d.thruTS
      FROM dps.vehicleAcquisitions a
      INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
      LEFT JOIN dps.VehicleInventoryItems d on c.VehicleItemID = d.VehicleItemID 
        AND d.stocknumber <> b.stocknumber
        AND CAST(b.fromts AS sql_date) = CAST(d.thruTS AS sql_date)  
      WHERE a.typ = 'VehicleAcquisition_IntraMarketPurchase') d on b.stocknumber = d.stocknumber) e
  LEFT JOIN dps.VehicleInventoryItems f on e.stocknumber = f.stocknumber
  LEFT JOIN extArkonaBOPTRAD g on e.stocknumber = g.stocknumber
  LEFT JOIN factVehicleSale h on g.storeCode = h.storeCode AND g.bopmastKey = h.bmkey
  LEFT JOIN day i on h.originationDateKey = i.datekey   
  LEFT JOIN (
    SELECT VehicleInventoryItemID, 
      SUM(CAST(thruTS AS sql_date) - CAST(fromTS AS sql_date)) AS daysAvail
    FROM dps.VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagAV'
    GROUP BY VehicleInventoryItemID) j on f.VehicleInventoryItemID = j.VehicleInventoryItemID     
  WHERE e.origStockNumber IS NULL) k  -- exclude intra market ws  

  
SELECT storecode, fr, thru, COUNT(*) AS sales, SUM(frontEndGross) AS frontEndGross, 
  SUM(backEndGross) AS backEndGross,
  round(SUM(frontEndGross)/COUNT(*), 0) AS frontEndGrossPer,
  round(SUM(backEndGross)/COUNT(*), 0) AS backEndGrossPer
FROM (
  SELECT 0 AS FR, 7 as THRU FROM system.iota
  union
  SELECT n - 6, n
  FROM tally
  WHERE mod(n, 7) = 0
    AND n > 8
    AND n < 57
  UNION 
  SELECT 57, 1000 FROM system.iota) j
LEFT JOIN #market2014 k on k.daysAvail BETWEEN j.fr AND j.thru  
GROUP BY storecode, fr, thru
  
SELECT storecode, yearmonth, SUM(backEndGross + frontEndGross)
FROM #market2014  
GROUP BY storecode, yearmonth

SELECT * FROM #2014

SELECT storecode, yearmonth, SUM(gttamt)
FROM #2014
GROUP BY storecode, yearmonth