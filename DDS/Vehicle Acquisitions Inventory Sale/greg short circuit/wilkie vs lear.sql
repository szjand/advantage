SELECT a.stocknumber, b.thedate, c.*
FROM factVehicleSale a
INNER JOIN day b on a.originationDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
WHERE storecode = 'ry2'
  AND vehicleTypeCode = 'U'
  
SELECT b.yearmonth, c.saleType, COUNT(*)
FROM factVehicleSale a
INNER JOIN day b on a.originationDateKey = b.datekey
INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
WHERE storecode = 'ry2'
  AND vehicleTypeCode = 'U'  
GROUP BY b.yearmonth, c.saleType  

-- fs accounts for ry2 used
select *
FROM zcb
WHERE name = 'gmfs gross'
  AND storecode = 'ry2'
  AND dl3 = 'variable'
  AND dl5 = 'used'
  
SELECT DISTINCT gmcoa  
FROM zcb
WHERE name = 'gmfs gross'
  AND storecode = 'ry2'
  AND dl3 = 'variable'
  AND dl5 = 'used'

-- used car sales BY month
DROP TABLE #july;
SELECT d.*, f.*, g.*, h.gmdesc
INTO #july
FROM ( 
  SELECT a.stocknumber, b.thedate, c.saleType
  FROM factVehicleSale a
  INNER JOIN day b on a.originationDateKey = b.datekey
  INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  WHERE storecode = 'ry2'
    AND vehicleTypeCode = 'U'  
    AND b.yearmonth = 201407) d
LEFT JOIN (
  SELECT gtctl#, gtacct, gttamt, gtjrnl
  FROM stgArkonaGLPTRNS e
  WHERE e.gtdate BETWEEN '07/01/2014' AND '07/31/2014') f on d.stocknumber = f.gtctl#
INNER JOIN (
  SELECT al2, al3, gmcoa, glAccount, page, line
  FROM zcb
  WHERE name = 'gmfs gross'
    AND storecode = 'ry2'
    AND dl3 = 'variable'
    AND dl5 = 'used') g on f.gtacct = g.glAccount 
LEFT JOIN stgArkonaGLPMAST h on f.gtacct = h.gmacct
  AND h.gmyear = 2014    

SELECT * FROM #july

SELECT page, line, SUM(gttamt)
FROM #july
GROUP BY page, line

select al2, al3, sum(gttamt), MAX(gmdesc)
FROM #july
WHERE saleType = 'Retail'
--  AND al2 = 'Sales'   
GROUP BY al2, al3
    
      
-- used car sales 2014
DROP TABLE #2014;
SELECT d.*, f.*, g.*, h.gmdesc
INTO #2014
FROM ( 
  SELECT a.stocknumber, b.thedate AS arkSaleDate, c.saleType, b.yearMonth
  FROM factVehicleSale a
  INNER JOIN day b on a.originationDateKey = b.datekey
  INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  WHERE storecode = 'ry2'
    AND vehicleTypeCode = 'U'  
    AND b.yearmonth between 201401 AND 201407) d
LEFT JOIN (
  SELECT gtctl#, gtacct, gttamt, gtjrnl
  FROM stgArkonaGLPTRNS e
  WHERE e.gtdate BETWEEN '01/01/2014' AND '07/31/2014') f on d.stocknumber = f.gtctl#
INNER JOIN (
  SELECT dl4, al2, al3, gmcoa, glAccount, page, line
  FROM zcb
  WHERE name = 'gmfs gross'
    AND storecode = 'ry2'
    AND dl3 = 'variable'
    AND dl5 = 'used') g on f.gtacct = g.glAccount 
LEFT JOIN stgArkonaGLPMAST h on f.gtacct = h.gmacct
  AND h.gmyear = 2014          
  
SELECT *
FROM (  
  SELECT stocknumber, arkSaleDate, yearmonth, SUM(gttamt) AS gross 
  FROM #2014 a
  WHERE saleType = 'retail'
    AND al2 = 'sales'
  GROUP BY stocknumber, arkSaleDate, yearMonth) x  
LEFT JOIN (
  SELECT a.VehicleInventoryItemID, b.stocknumber, CAST(b.fromTs AS sql_date) AS viiFromDate, 
    CAST(b.thruTS AS sql_date) AS viiThruDate, CAST(a.soldTS AS sql_date) AS viiSoldDate
  FROM dps.VehicleSales a
  LEFT JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID) y on x.stocknumber = y.stocknumber 
LEFT JOIN dps.vehicleAcquisitions z on y.VehicleInventoryItemID = z.VehicleInventoryItemID 
  
-- retail sales per month
-- good, this matches the fs line 14
  SELECT yearmonth, COUNT(DISTINCT stocknumber)
  FROM #2014 a
  WHERE saleType = 'retail'
    AND al2 = 'sales'
  GROUP BY yearMonth  


-- so, once again, to get relevant aging, need the fucking intramarket info
-- base IS ark sales (factVehicleSales), that determines which vehicles, 
-- arkSaleDate, gross & cogs 
-- this looks pretty good, need aging
SELECT b.*, d.origStockNumber
FROM (
  SELECT a.stocknumber, a.arkSaleDate,
    SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS gross,
    SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs
  FROM #2014 a
  WHERE a.yearmonth BETWEEN 201401 AND 201407
    AND a.saleType = 'retail'
  GROUP BY a.stocknumber, a.arkSaleDate) b 
LEFT JOIN ( -- intramarket based on acquisitions
  SELECT a.typ, b.stocknumber, b.fromts, b.thruts, c.vin, d.stocknumber AS origStockNumber, d.fromTS, d.thruTS
  FROM dps.vehicleAcquisitions a
  INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
  LEFT JOIN dps.VehicleInventoryItems d on c.VehicleItemID = d.VehicleItemID 
    AND d.stocknumber <> b.stocknumber
    AND CAST(b.fromts AS sql_date) = CAST(d.thruTS AS sql_date)  
  WHERE a.typ = 'VehicleAcquisition_IntraMarketPurchase') d on b.stocknumber = d.stocknumber
--WHERE right(TRIM(b.stocknumber), 1) = 'G'

-- intramarket based on acquisitions
SELECT a.typ, b.stocknumber, b.fromts, b.thruts, c.vin, d.stocknumber AS origStockNumber, d.fromTS, d.thruTS
FROM dps.vehicleAcquisitions a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
LEFT JOIN dps.VehicleInventoryItems d on c.VehicleItemID = d.VehicleItemID 
  AND d.stocknumber <> b.stocknumber
  AND CAST(b.fromts AS sql_date) = CAST(d.thruTS AS sql_date)  
WHERE a.typ = 'VehicleAcquisition_IntraMarketPurchase'

-- intramarket based on dps.vehiceSales type ws AND buyer IS ry1 OR ry2
SELECT a.soldTS, left(b.name, 20) AS buyer, c.stocknumber, c.fromts, c.thruts, d.vin, 
  e.stocknumber, e.fromts, e.thruts
FROM dps.VehicleSales a
INNER JOIN dps.organizations b on a.soldTo = b.partyid  collate ads_default_ci
  AND b.name IN ('rydells','honda cartiva')
INNER JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID   
INNER JOIN dps.VehicleItems d on c.VehicleItemID = d.VehicleItemID 
LEFT JOIN dps.VehicleInventoryItems e on d.VehicleItemID = e.VehicleItemID 
  AND c.stocknumber <> e.stocknumber
WHERE a.typ = 'VehicleSale_Wholesale'
  AND cast(c.thruts AS sql_date) = cast(e.fromts AS sql_date)
--  AND c.stocknumber = 'h5985a'


-- total wholesales FROM dps, too erratic (compared to fs), my guess IS sale 
-- dates are goofy (cross months)
SELECT b.owningLocationID, month(soldTS), COUNT(*)
FROM dps.vehicleSales a
INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
WHERE a.typ = 'VehicleSale_Wholesale'
  AND year(soldts) = 2014
GROUP BY b.owningLocationID, month(soldTS)

-- narrow it down to only intra market ws, these should ADD to the vehicleSale (arkona)
-- wholesales to give fs totals
-- this IS inverted: name IS the sold to party
-- this IS closer, but IS NOT accurate
SELECT b.name, month(a.soldTS), COUNT(*)
FROM dps.VehicleSales a
INNER JOIN dps.organizations b on a.soldTo = b.partyid  collate ads_default_ci
  AND b.name IN ('rydells','honda cartiva')
INNER JOIN dps.VehicleInventoryItems c on a.VehicleInventoryItemID = c.VehicleInventoryItemID   
WHERE year(a.soldts) = 2014
GROUP BY b.name, month(soldTS)

-- TRY accounting



-- maybe just need to back the fuck off on doing it ALL
-- narrow the scope to hondas retail sales of their inventory
-- get the $$ AND dates for just that subset first

-- same conundrum on dates, trades are fine, but WHERE DO i get the inventory 
--   date on purchases USING arkona, we are fine on trades, 
-- ?? dps.VehicleInventoryItems/acquisitions ??  


-- 8/14/14
-- this feels pretty good, on non intra market vehicles, acqDate comes FROM
-- arkona on trades, dps on purchases
SELECT b.*, coalesce(f.theDate, CAST(c.fromTs AS sql_date)) AS acqDate
FROM (
  SELECT a.stocknumber, a.arkSaleDate,
    SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS gross,
    SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs
  FROM #2014 a
  WHERE a.yearmonth BETWEEN 201401 AND 201407
    AND a.saleType = 'retail'
    AND RIGHT(TRIM(a.stocknumber),1) <> 'G'
  GROUP BY a.stocknumber, a.arkSaleDate) b
LEFT JOIN dps.VehicleInventoryItems c on b.stocknumber = c.stocknumber
LEFT JOIN extArkonaBOPTRAD d on b.stocknumber = d.stocknumber
LEFT JOIN factVehicleSale e on d.storeCode = e.storeCode AND d.bopmastKey = e.bmkey
LEFT JOIN day f on e.originationDateKey = f.datekey

-- ok, ADD time available
-- that looks ok
SELECT b.*, coalesce(f.theDate, CAST(c.fromTs AS sql_date)) AS acqDate, 
  coalesce(g.daysAvail, 0) AS daysAvail
FROM (
  SELECT a.stocknumber, a.arkSaleDate,
    SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS gross,
    SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs
  FROM #2014 a
  WHERE a.yearmonth BETWEEN 201401 AND 201407
    AND a.saleType = 'retail'
    AND RIGHT(TRIM(a.stocknumber),1) <> 'G'
  GROUP BY a.stocknumber, a.arkSaleDate) b
LEFT JOIN dps.VehicleInventoryItems c on b.stocknumber = c.stocknumber
LEFT JOIN extArkonaBOPTRAD d on b.stocknumber = d.stocknumber
LEFT JOIN factVehicleSale e on d.storeCode = e.storeCode AND d.bopmastKey = e.bmkey
LEFT JOIN day f on e.originationDateKey = f.datekey
LEFT JOIN (
  SELECT VehicleInventoryItemID, 
    SUM(CAST(thruTS AS sql_date) - CAST(fromTS AS sql_date)) AS daysAvail
  FROM dps.VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagAV'
  GROUP BY VehicleInventoryItemID) g on c.VehicleInventoryItemID = g.VehicleInventoryItemID 
  
-- reporting level values
-- ADD yearmonth
SELECT stocknumber, yearmonth, arkSaleDate as saleDate, - (gross + cogs) AS gross, 
  arkSaleDate - acqDate AS daysOwned, daysAvail
INTO #honda2014  
FROM (  
  SELECT b.*, coalesce(f.theDate, CAST(c.fromTs AS sql_date)) AS acqDate, 
    coalesce(g.daysAvail, 0) AS daysAvail
  FROM (
    SELECT a.stocknumber, a.arkSaleDate, yearmonth, 
      SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS gross,
      SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs
    FROM #2014 a
    WHERE a.yearmonth BETWEEN 201401 AND 201407
      AND a.saleType = 'retail'
      AND RIGHT(TRIM(a.stocknumber),1) <> 'G'
    GROUP BY a.stocknumber, a.arkSaleDate, yearmonth) b
  LEFT JOIN dps.VehicleInventoryItems c on b.stocknumber = c.stocknumber
  LEFT JOIN extArkonaBOPTRAD d on b.stocknumber = d.stocknumber
  LEFT JOIN factVehicleSale e on d.storeCode = e.storeCode AND d.bopmastKey = e.bmkey
  LEFT JOIN day f on e.originationDateKey = f.datekey
  LEFT JOIN (
    SELECT VehicleInventoryItemID, 
      SUM(CAST(thruTS AS sql_date) - CAST(fromTS AS sql_date)) AS daysAvail
    FROM dps.VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagAV'
    GROUP BY VehicleInventoryItemID) g on c.VehicleInventoryItemID = g.VehicleInventoryItemID) h 

select * FROM #honda2014 
-- sales/month   
SELECT yearmonth, COUNT(*) AS sales
FROM #honda2014 
GROUP BY yearmonth   
  
SELECT fr, thru, COUNT(*) AS sales, SUM(gross) AS gross,
  round(SUM(gross)/COUNT(*), 0) AS grossPer
FROM (
  SELECT 0 AS FR, 7 as THRU FROM system.iota
  union
  SELECT n - 6, n
  FROM tally
  WHERE mod(n, 7) = 0
    AND n > 8
    AND n < 57
  UNION 
  SELECT 57, 1000 FROM system.iota) j
LEFT JOIN #honda2014 k on k.daysOwned BETWEEN j.fr AND j.thru  
GROUP BY fr, thru

/* same thing for market --------------------------------------------------------*/
DROP TABLE #2014;
SELECT d.*, f.*, g.*, h.gmdesc
INTO #2014
FROM ( 
  SELECT a.storecode, a.stocknumber, b.thedate AS arkSaleDate, c.saleType, b.yearMonth
  FROM factVehicleSale a
  INNER JOIN day b on a.originationDateKey = b.datekey
  INNER JOIN dimCarDealInfo c on a.carDealInfoKey = c.carDealInfoKey
  WHERE vehicleTypeCode = 'U'  
    AND b.yearmonth between 201401 AND 201407) d
LEFT JOIN (
  SELECT gtctl#, gtacct, gttamt, gtjrnl
  FROM stgArkonaGLPTRNS e
  WHERE e.gtdate BETWEEN '01/01/2014' AND '07/31/2014') f on d.stocknumber = f.gtctl#
INNER JOIN (
  SELECT al2, al3, gmcoa, glAccount, page, line
  FROM zcb
  WHERE name = 'gmfs gross'
--    AND storecode = 'ry2'
    AND dl3 = 'variable'
    AND dl5 = 'used') g on f.gtacct = g.glAccount 
LEFT JOIN stgArkonaGLPMAST h on f.gtacct = h.gmacct
  AND h.gmyear = 2014    

DROP TABLE #market2014;
SELECT storecode, stocknumber, yearmonth, arkSaleDate as saleDate, 
  - (backEndGross + backEndCogs) AS backEndgross, 
  - (frontEndGross + frontEndCogs) AS frontEndgross,
  arkSaleDate - acqDate AS daysOwned, daysAvail
INTO #market2014  
FROM (  
  SELECT e.*, coalesce(i.theDate, CAST(f.fromTs AS sql_date)) AS acqDate, 
    coalesce(j.daysAvail, 0) AS daysAvail
  FROM (  
    SELECT b.*, d.origStockNumber
    FROM (
      SELECT a.storecode, a.stocknumber, a.arkSaleDate, yearmonth,
        SUM(CASE WHEN a.page = 7 AND al2 = 'sales' THEN gttamt ELSE 0 END) AS backEndGross,
        SUM(CASE WHEN a.page = 7 AND al2 = 'cogs' THEN gttamt ELSE 0 END) AS backEndCOGS,
        SUM(CASE WHEN a.page = 6 AND al2 = 'sales' THEN gttamt ELSE 0 END) AS frontEndGross,
        SUM(CASE WHEN a.page = 6 AND al2 = 'cogs' THEN gttamt ELSE 0 END) AS frontEndCOGS        
      FROM #2014 a
      WHERE a.yearmonth BETWEEN 201401 AND 201407
        AND a.saleType = 'retail'
      GROUP BY a.storecode, a.stocknumber, a.arkSaleDate, yearmonth) b 
    LEFT JOIN ( -- intramarket based on acquisitions
      SELECT a.typ, b.stocknumber, b.fromts, b.thruts, c.vin, d.stocknumber AS origStockNumber, d.fromTS, d.thruTS
      FROM dps.vehicleAcquisitions a
      INNER JOIN dps.VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
      INNER JOIN dps.VehicleItems c on b.VehicleItemID = c.VehicleItemID 
      LEFT JOIN dps.VehicleInventoryItems d on c.VehicleItemID = d.VehicleItemID 
        AND d.stocknumber <> b.stocknumber
        AND CAST(b.fromts AS sql_date) = CAST(d.thruTS AS sql_date)  
      WHERE a.typ = 'VehicleAcquisition_IntraMarketPurchase') d on b.stocknumber = d.stocknumber) e
  LEFT JOIN dps.VehicleInventoryItems f on e.stocknumber = f.stocknumber
  LEFT JOIN extArkonaBOPTRAD g on e.stocknumber = g.stocknumber
  LEFT JOIN factVehicleSale h on g.storeCode = h.storeCode AND g.bopmastKey = h.bmkey
  LEFT JOIN day i on h.originationDateKey = i.datekey   
  LEFT JOIN (
    SELECT VehicleInventoryItemID, 
      SUM(CAST(thruTS AS sql_date) - CAST(fromTS AS sql_date)) AS daysAvail
    FROM dps.VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagAV'
    GROUP BY VehicleInventoryItemID) j on f.VehicleInventoryItemID = j.VehicleInventoryItemID     
  WHERE e.origStockNumber IS NULL) k     
  
SELECT storecode, fr, thru, COUNT(*) AS sales, SUM(backEndGross) AS backEndGross,
  SUM(frontEndGross) AS frontEndGross, round(SUM(backEndGross)/COUNT(*), 0) AS backEndGrossPer,
  round(SUM(frontEndGross)/COUNT(*), 0) AS frontEndGrossPer
FROM (
  SELECT 0 AS FR, 7 as THRU FROM system.iota
  union
  SELECT n - 6, n
  FROM tally
  WHERE mod(n, 7) = 0
    AND n > 8
    AND n < 57
  UNION 
  SELECT 57, 1000 FROM system.iota) j
LEFT JOIN #market2014 k on k.daysAvail BETWEEN j.fr AND j.thru  
GROUP BY storecode, fr, thru


