CREATE TABLE ucinv_vehicle_sale ( 
      storeCode CIChar( 3 ),
      stockNumber CIChar( 9 ),
      carDealInfoKey Integer,
      vehicleKey Integer,
      originationDateKey Integer,
      approvedDateKey Integer,
      cappedDateKey Integer,
      buyerKey Integer,
      coBuyerKey Integer,
      consultantKey Integer,
      managerKey Integer,
      vehicleCost Numeric( 12 ,2 ),
      vehiclePrice Numeric( 12 ,2 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'PK',
   'storeCode;approvedDateKey;stockNumber',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'STOCKNUMBER',
   'stockNumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'CARDEALINFOKEY',
   'carDealInfoKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'VEHICLEKEY',
   'vehicleKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'ORIGINATIONDATEKEY',
   'originationDateKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'APPROVEDDATEKEY',
   'approvedDateKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'CAPPEDDATEKEY',
   'cappedDateKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'BUYERKEY',
   'buyerKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'COBUYERKEY',
   'coBuyerKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'CONSULTANTKEY',
   'consultantKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_sale',
   'ucinv_vehicle_sale.adi',
   'MANAGERKEY',
   'managerKey',
   '',
   2,
   512,
   '' ); 



EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_sale', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_sale', 
   'Table_Primary_Key', 
   'PK', 'APPEND_FAIL', 'ucinv_vehicle_salefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_sale', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ucinv_vehicle_salefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_sale', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_sale', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'storeCode', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 


EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'stockNumber', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'carDealInfoKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'vehicleKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'originationDateKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'approvedDateKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'cappedDateKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'buyerKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'coBuyerKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'consultantKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'managerKey', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'vehicleCost', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'vehicleCost', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'vehiclePrice', 'Field_Can_Be_Null', 
      'False', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_sale', 
      'vehiclePrice', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'ucinv_vehicle_salefail' ); 



INSERT INTO ucinv_vehicle_sale      
SELECT a.storecode, a.stocknumber, a.cardealinfokey, a.vehiclekey,
  a.originationdatekey, a.approveddatekey, a.cappeddatekey, a.buyerkey,
  a.cobuyerkey, a.consultantkey, a.managerkey, a.vehiclecost, a.vehicleprice
FROM factVehicleSale a
INNER JOIN dimCarDealInfo b on a.cardealinfokey = b.cardealinfokey
  AND b.vehicletypecode = 'u'
INNER JOIN day c on a.approveddatekey = c.datekey
  AND c.theyear = 2015  
  
   
CREATE TABLE ucinv_vehicle_acquisition ( 
      storecode CIChar( 3 ),
      stocknumber CIChar( 9 ),
      vehicleAcquisitionInfoKey Integer,
      vehicleKey Integer,
      dateKey Integer,
      sourceVehicleKey Integer,
      cost Double( 2 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_acquisition',
   'ucinv_vehicle_acquisition.adi',
   'STOCKNUMBER',
   'stocknumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_acquisition',
   'ucinv_vehicle_acquisition.adi',
   'VEHICLEACQUISITIONINFOKEY',
   'vehicleAcquisitionInfoKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_acquisition',
   'ucinv_vehicle_acquisition.adi',
   'VEHICLEKEY',
   'vehicleKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_acquisition',
   'ucinv_vehicle_acquisition.adi',
   'DATEKEY',
   'dateKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_acquisition',
   'ucinv_vehicle_acquisition.adi',
   'SOURCEVEHICLEKEY',
   'sourceVehicleKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_acquisition', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ucinv_vehicle_acquisitionfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_acquisition', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ucinv_vehicle_acquisitionfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_acquisition', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ucinv_vehicle_acquisitionfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_acquisition', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ucinv_vehicle_acquisitionfail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'ucinv_vehicle_acquisition', 
      'cost', 'Field_Default_Value', 
      '0', 'APPEND_FAIL', 'ucinv_vehicle_acquisitionfail' ); 

                                       
INSERT INTO ucinv_vehicle_Acquisition
SELECT a.storecode, a.stocknumber, a.vehicleacquisitioninfokey, a.vehiclekey, 
  a.datekey, a.sourcevehiclekey, a.cost
FROM factVehicleAcquisition a
WHERE EXISTS (
  SELECT 1
  FROM ucinv_vehicle_sale
  WHERE stocknumber = a.stocknumber)
  
CREATE TABLE ucinv_vehicle_inventory ( 
      storeCode CIChar( 3 ),
      stockNumber CIChar( 9 ),
      vehicleKey Integer,
      fromDateKey Integer,
      thruDateKey Integer) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_inventory',
   'ucinv_vehicle_inventory.adi',
   'STORECODE',
   'storeCode',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_inventory',
   'ucinv_vehicle_inventory.adi',
   'STOCKNUMBER',
   'stockNumber',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_inventory',
   'ucinv_vehicle_inventory.adi',
   'VEHICLEKEY',
   'vehicleKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_inventory',
   'ucinv_vehicle_inventory.adi',
   'FROMDATEKEY',
   'fromDateKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'ucinv_vehicle_inventory',
   'ucinv_vehicle_inventory.adi',
   'THRUDATEKEY',
   'thruDateKey',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_inventory', 
   'Table_Auto_Create', 
   'False', 'APPEND_FAIL', 'ucinv_vehicle_inventoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_inventory', 
   'Table_Permission_Level', 
   '2', 'APPEND_FAIL', 'ucinv_vehicle_inventoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_inventory', 
   'Triggers_Disabled', 
   'False', 'APPEND_FAIL', 'ucinv_vehicle_inventoryfail');

EXECUTE PROCEDURE sp_ModifyTableProperty( 'ucinv_vehicle_inventory', 
   'Table_Trans_Free', 
   'False', 'APPEND_FAIL', 'ucinv_vehicle_inventoryfail');

-- current inventory
INSERT INTO ucinv_vehicle_inventory  
SELECT imco#, imstk#, b.vehiclekey, c.datekey, 7306 
FROM stgArkonaINPMAST a
LEFT JOIN dimVehicle b on a.imvin = b.vin   
LEFT JOIN day c on a.imdinv = c.thedate
WHERE imtype = 'u'
  AND imstat = 'i';
  
shit, no acq record for current inventory  
SELECT COUNT(*)
FROM ucinv_vehicle_inventory a
WHERE NOT EXISTS (
  SELECT 1
  FROM ucinv_vehicle_acquisition
  WHERE stocknumber = a.stocknumber)
  