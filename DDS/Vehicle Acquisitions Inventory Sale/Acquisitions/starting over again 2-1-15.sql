
/* current state of generating data -------------------------------------------*/
/* "fine" tuning of scripts arkona acquisitions.sql, dps acquisitions.sql
--< arkona --------------------------------------------------------------------<
/*
DO extArkonaBOPTRAD first
*/
-- trades
-- 3/5/15 why am i USING orig date AS acq date? let us DO a little snooping 
-- should be USING apprv date
DELETE FROM tmpAisArkAcq;
INSERT INTO tmpAisArkAcq
SELECT a.storeCode, a.stockNumber, a.vin, b.bmdtaprv, 'Trade',
  CASE b.bmvtyp
    WHEN 'N' THEN 'New'
    WHEN 'U' THEN 'Used'
    ELSE 'Unknown'
  END,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
  replace(a.stocknumber, 'y', '') AS stockNumberNoY, 'ark-trade'
FROM extArkonaBOPTRAD a
LEFT JOIN stgArkonaBOPMAST b on a.storeCode = b.bmco#
  AND a.bopmastKey = b.bmkey
WHERE year(b.bmdtor) > 2010
  AND b.bmstat <> '' -- eliminates pending deals
  AND right(TRIM(a.stocknumber), 6) <> right(TRIM(a.vin), 6); -- eliminates temp stocknumbers
-- trades with bad stocknumbers  
INSERT INTO tmpAisArkAcq
SELECT storecode, imstk, vin, bmdtor, 'Trade', 
  CASE bmvtyp
    WHEN 'N' THEN 'New'
    WHEN 'U' THEN 'Used'
    ELSE 'Unknown'
  END,
  replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(imstk, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
  replace(imstk, 'y', '') AS stockNumberNoY, 'ark-trade'
FROM (  
  SELECT a.storeCode, a.stocknumber, c.imstk#, b.bmvtyp,
    CASE
      WHEN right(TRIM(a.stocknumber), 6) <> right(TRIM(a.vin), 6) THEN a.stocknumber
      WHEN c.imstk# <> '' AND c.imstk# <> a.stocknumber AND c.imdinv = bmdtor THEN imstk#
      ELSE 'NO'
    END AS imstk, 
    a.stockNumber, a.vin, b.bmdtor, c.imdinv
  FROM extArkonaBOPTRAD a
  LEFT JOIN stgArkonaBOPMAST b on a.storeCode = b.bmco#
    AND a.bopmastKey = b.bmkey
  LEFT JOIN stgArkonaINPMAST c on a.vin = c.imvin  
  WHERE year(bmdtor) > 2010
    AND right(TRIM(a.stocknumber), 6) = right(TRIM(a.vin), 6)
    AND
      CASE
        WHEN right(TRIM(a.stocknumber), 6) <> right(TRIM(a.vin), 6) THEN a.stocknumber
        WHEN c.imstk# <> '' AND c.imstk# <> a.stocknumber AND c.imdinv = bmdtor THEN imstk#
        ELSE 'NO'
      END <> 'NO') x
WHERE NOT EXISTS (
  SELECT 1
  FROM tmpAisArkAcq
  WHERE vin = x.vin);  
  
-- sales  
-- stk# NOT already IN tmpAisArkAcq, acqDate FROM INPMAST
INSERT INTO tmpAisArkAcq
SELECT bmco#, bmstk#, bmvin, acqDate,
  CASE 
    WHEN right(TRIM(bmstk#), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
    ELSE 'Purchase'
  END AS category,
  CASE 
    WHEN right(TRIM(bmstk#), 1) IN ('a','b','c','d','e','f') THEN
      CASE 
        WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
        ELSE 'Used'
      END
    ELSE ''
  END, 
  suffix, stockNumberNoY, 'ark-sale'
FROM (  
  SELECT distinct a.bmco#, a.bmstk#, a.bmvin, 
--    coalesce(c.imdinv, d.gtdate) AS acqDate, 'Purchase', '',
    coalesce(c.imdinv, CAST('12/31/9999' AS sql_date)) AS acqDate, 'Purchase', '',    
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.bmstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix,
    replace(a.bmstk#, 'y', '') AS stockNumberNoY    
  FROM stgArkonaBOPMAST a
  LEFT JOIN tmpAisArkAcq b on a.bmstk# = b.stocknumber 
  LEFT JOIN stgArkonaINPMAST c on a.bmstk# = c.imstk#
  WHERE a.bmstk# <> ''
    AND a.bmstat <> '' -- finished deals
    AND a.bmvtyp = 'u' -- used car sale
    AND year(a.bmdtor) > 2010
    AND b.storecode IS NULL) x;
    
-- current inventory --------------------------------------------
INSERT INTO tmpAisArkAcq
SELECT imco#, imstk#, imvin, imdinv,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN 'Trade'
    ELSE 'Purchase'
  END AS category,
  CASE 
    WHEN right(TRIM(imstk#), 1) IN ('a','b','c','d','e','f') THEN
      CASE 
        WHEN suffix IN ('a','aa','aaa','aaaa') THEN 'New'
        ELSE 'Used'
      END
    ELSE ''
  END, 
  suffix, stockNumberNoY, 'ark-inv'
FROM (  
  SELECT imco#, imstk#, imvin, imdinv, 'Purchase', '',
    replace(replace(replace(replace(replace(replace(replace(replace(replace
      (replace(substring(a.imstk#, 5, 15),'0',''),'1',''),'2',''),'3',''),
        '4',''),'5',''),'6',''),'7',''),'8',''),'9','') AS suffix, 
    replace(a.imstk#, 'y', '') AS stockNumberNoY
  FROM stgArkonaINPMAST a
  WHERE imstk# <> ''
    AND imstat = 'i'
    AND imtype = 'u'
    AND NOT EXISTS (
      SELECT 1
      FROM tmpAisArkAcq
      WHERE stocknumber = a.imstk#)) x;
      
--/> arkona -------------------------------------------------------------------/>      

--< dps -----------------------------------------------------------------------<
-- *a* goofy exc, mazda ws to honda, sitting IN book intra market for 9 days
DELETE FROM tmpAisDpsAcq;
INSERT INTO tmpAisDpsAcq
select 
  CASE c.name 
    WHEN 'rydells' THEN 'RY1'
    WHEN 'honda cartiva' THEN 'RY2'
    ELSE 'XXX'
  END,
  left(trim(a.stocknumber), 10), left(trim(b.vin), 17), cast(a.fromts as sql_date)AS dateAcq, 
  CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
    WHEN 'Trade' THEN 'Trade'
    ELSE 'Purchase'
  END AS acqCategory,
  CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
    WHEN 'Trade' THEN ''
    ELSE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
  END AS acqCategoryClassification,
--  left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20) AS acqCategory,
  left(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9',''), 'Y',''), 9) AS suffix,
  trim(replace(a.stocknumber, 'y', '')) AS stockNumberNoY, 'dps'
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID 
INNER JOIN dps.organizations c on a.owningLocationID = c.partyid
LEFT JOIN dps.vehicleAcquisitions d on a.VehicleInventoryItemID = d.VehicleInventoryItemID  
LEFT JOIN dps.organizations e on d.acquiredFromPartyID = e.partyid
WHERE year(a.fromts) > 2010
  AND d.typ IS NOT NULL
  AND a.stocknumber IS NOT NULL;  --*a* 

UPDATE tmpAisDpsAcq
  SET acqCategoryClassification = 
    CASE 
      WHEN stocknumberSuffix IN ('a','aa','aaa','aaaa','aaaaa') THEN 'New'
      ELSE 'Used'
    END    
WHERE acqCategory = 'trade';

-- 3/5/15 to comply with dds.dimVehicleAcquisitionInfo (come at it FROM dps 2-2-15.sql
UPDATE tmpAisDpsAcq
SET acqCategoryClassification = 
  CASE acqCategoryClassification
    WHEN 'DealerPurchase' THEN 'Dealer Purchase'
    WHEN 'InAndOut' THEN 'In And Out'
    WHEN 'IntraMarketPurchase' THEN 'Intra Market Wholesale'
    WHEN 'LeasePurcha' THEN 'Lease Purchase'
    WHEN 'ServiceLoan' THEN 'Service Loaner'
    ELSE acqCategoryClassification
  END;
--/> dps ----------------------------------------------------------------------/>  

/*
SELECT acqCategory, acqCategoryClassification, stocknumberSuffix, dataSource, COUNT(*)
FROM tmpAisArkAcq
GROUP BY acqCategory, acqCategoryClassification, stocknumberSuffix, dataSource
*/

