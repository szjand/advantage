--- 8/18 ADD acqCategoryClassification  
-- 2/1/15 ADD dataSource attribute: ark-trade, ark-sale, ark-inv, dps
DROP TABLE tmpAisDpsAcq;
CREATE TABLE tmpAisDpsAcq (
  storeCode cichar(3) constraint NOT NULL, -- dimKey
  stockNumber cichar(10) constraint NOT NULL, -- dd
  vin cichar(17) constraint NOT NULL, -- dimKey
  dateAcq date constraint NOT NULL, -- dimKey 
  acqCategory cichar(20) constraint NOT NULL, -- dimKey, trade/purchase
  acqCategoryClassification cichar(40),
  stockNumberSuffix cichar(9) constraint NOT NULL,
  stockNumberNoY cichar(10) constraint NOT NULL, -- y IS a flag for a damaged title 
--       AND IS used inconsistently, need this version of stk# for joining to arkona 
  --  acqType cichar(30), -- dimKey, nctrade/uctrade/auctionpurchase/streetpurchase, etc
  dataSource cichar(12),
  constraint PK primary key (storeCode, stockNumber, vin, dateAcq, acqCategory))
IN database;  

SELECT MAX(length(TRIM(stocknumber))) FROM dps.VehicleInventoryItems `

SELECT * FROM dps.VehicleInventoryItems WHERE length(TRIM(stocknumber)) = 10

-- only 1 VehicleInventoryItems with no acq row 21671XXA
SELECT * 
FROM dps.VehicleInventoryItems a 
WHERE NOT EXISTS (
  SELECT 1 
  FROM dps.vehicleAcquisitions
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)

-- *a* goofy exc, mazda ws to honda, sitting IN book intra market for 9 days
DELETE FROM tmpAisDpsAcq;
INSERT INTO tmpAisDpsAcq
select 
  CASE c.name 
    WHEN 'rydells' THEN 'RY1'
    WHEN 'honda cartiva' THEN 'RY2'
    ELSE 'XXX'
  END,
  left(trim(a.stocknumber), 10), left(trim(b.vin), 17), cast(a.fromts as sql_date)AS dateAcq, 
  CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
    WHEN 'Trade' THEN 'Trade'
    ELSE 'Purchase'
  END AS acqCategory,
  CASE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
    WHEN 'Trade' THEN ''
    ELSE left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20)
  END AS acqCategoryClassification,
--  left(trim(substring(d.typ, position('_' IN d.typ) + 1, length(TRIM(d.typ)) - position('_' IN d.typ))), 20) AS acqCategory,
  left(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace
    (replace(substring(a.stocknumber, 5, 15),'0',''),'1',''),'2',''),'3',''),
      '4',''),'5',''),'6',''),'7',''),'8',''),'9',''), 'Y',''), 9) AS suffix,
  trim(replace(a.stocknumber, 'y', '')) AS stockNumberNoY
FROM dps.VehicleInventoryItems a
INNER JOIN dps.VehicleItems b on a.VehicleItemID = b.VehicleItemID 
INNER JOIN dps.organizations c on a.owningLocationID = c.partyid
LEFT JOIN dps.vehicleAcquisitions d on a.VehicleInventoryItemID = d.VehicleInventoryItemID  
LEFT JOIN dps.organizations e on d.acquiredFromPartyID = e.partyid
WHERE year(a.fromts) > 2010
  AND d.typ IS NOT NULL
  AND a.stocknumber IS NOT NULL;  --*a* 

  
