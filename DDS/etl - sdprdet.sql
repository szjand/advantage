SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
    WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'double,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'double,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'sdprdet' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'sdprdet'  
AND data_type <> 'CHAR'

INSERT INTO jontest values(1);
INSERT INTO jontest values(-11);
INSERT INTO jontest values(1.1234);
INSERT INTO jontest values(1/3.0);
INSERT INTO jontest values(1, (88.88+44.4)/5);


ALTER TABLE jontest
ADD COLUMN field2 money

CREATE TABLE stgArkonaSDPRDET (
ptco#      cichar(3),          
ptro#      cichar(9),          
ptline     integer,             
ptltyp     cichar(1),          
ptseq#     integer,             
ptcode     cichar(2),          
ptdate     date,             
ptlpym     cichar(1),          
ptsvctyp   cichar(2),          
ptpadj     cichar(1),          
pttech     cichar(3),          
ptlopc     cichar(10),         
ptcrlo     cichar(10),         
ptlhrs     double,             
ptlamt     money,             
ptcost     money,             
ptartf     cichar(1),          
ptfail     cichar(6),          
ptslv#     cichar(9),          
ptsli#     cichar(9),          
ptdcpn     integer,             
ptdbas     cichar(1),          
ptvatcode  cichar(1),          
ptvatamt   money,             
ptdispty1  cichar(5),          
ptdispty2  cichar(5),          
ptdisrank1 integer,             
ptdisrank2 integer) IN database;            

-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @table_name string;
DECLARE @col string;
DECLARE @cols string;
@table_name = 'sdprdet';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @table_name);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = (SELECT '[' + TRIM(column_name) + '], '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @table_name);
-- just COLUMN names
  @col = (SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @table_name);
--  @col = (SELECT ':' + TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @table_name);
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;  
 
-- param list for code
DECLARE @table_name string;
@table_name = 'sdprdet';
SELECT 'AdsQuery.ParamByName(' + '''' + TRIM(column_name) + '''' + ').AsXXXX := AdoQuery.FieldByName('+ '''' + TRIM(column_name) + '''' + ').AsXXXX;'
FROM syscolumns
WHERE table_name = @table_name

SELECT *
FROM system.columns
WHERE parent = 'stgArkonasdprdet'

DECLARE @table_name string;
@table_name = 'stgArkonasdprdet';
SELECT 'AdsQuery.ParamByName(' + '''' + TRIM(name) + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;

/* time to get serious */

-- ok, so what should constitute a unique sdprdet row
-- 1. ptco#, ptro#, ptline, ptseq#
--    turns out line & seq can be dupped
--    ADD ptltyp & ptcode

--DROP TABLE #jon
-- dups
SELECT ptco#, ptro#, ptline, ptseq#, ptltyp, ptcode
INTO #jon
FROM stgArkonaSDPRDET
GROUP BY ptco#, ptro#, ptline, ptseq#, ptltyp, ptcode
HAVING COUNT(*) > 1

SELECT COUNT(*) FROM #jon -- 2881 ptco#, ptro#, ptline, ptseq#, ptltyp, ptcode

-- DROP TABLE #wtf
-- ADD rowid to dups
SELECT s.rowid, s.*
INTO #wtf
FROM stgArkonaSDPRDET s
LEFT JOIN #jon j ON s.ptco# = j.ptco#
  AND s.ptro# = j.ptro#
  AND s.ptline = j.ptline
  AND s.ptseq# = j.ptseq#
  AND s.ptltyp = j.ptltyp
  AND s.ptcode = j.ptcode
WHERE j.ptco# IS NOT NULL   

-- 
DELETE 
FROM stgArkonaSDPRDET
WHERE rowid IN (
  SELECT MAX(rowid)
  FROM #wtf
  GROUP BY ptco#, ptro#, ptline, ptseq#, ptltyp, ptcode)
  
  