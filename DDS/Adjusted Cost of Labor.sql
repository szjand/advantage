/*
adj cost of labor = payroll - costed ON closed ros
costing should be 120% of hourlyrate

*/
SELECT t.stactv, e.storecode, e.name, e.hourlyrate, t.stlrat, round(cast(t.stlrat AS sql_double)/cast(e.hourlyrate AS sql_double), 2)* 100
FROM edwEmployeeDim e
INNER JOIN stgArkonaSDPTECH t ON e.technumber = t.sttech
  AND t.stactv <> 'N'
WHERE currentrow = true
  AND ActiveCode = 'A'
ORDER BY storecode, name  


SELECT *
FROM edwEmployeeDim e
INNER JOIN stgArkonaSDPTECH t ON e.technumber = t.sttech
WHERE currentrow = true
  AND ActiveCode = 'A'
  AND hourlyrate = 0
  
SELECT *
FROM stgArkonaSDPTECH 
WHERE stpyemp# = ''
  AND stactv <> 'N'
  
  
SELECT * 
FROM stgArkonaSDPRDET
WHERE pttech = '51'  