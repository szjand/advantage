-- generic heirarchy TABLE
DROP TABLE zcb;
CREATE TABLE zCB (
  Name cichar(20),
  StoreCode cichar(3),
  dl1 cichar(24),
  dl2 cichar(24),
  dl3 cichar(24),
  al1 cichar(24),
  al2 cichar(24),
  al3 cichar(24),
  al4 cichar(24),
  glAccount cichar(10),
  split double(2)) IN database;

DROP TABLE zgmfsdept  
CREATE TABLE zGMFSDept(
  dl1 cichar(24),
  dl2 cichar(24),
  dl3 cichar(24),
  dl4 cichar(24),
  dl5 cichar(24)) IN database;  
SELECT 'Grand Forks','RY1','Fixed','Mechanical','MainShop'

SELECT *
FROM 
  (SELECT 'Grand Forks' FROM system.iota) a,
  (SELECT 'RY1'FROM system.iota) b,
  (SELECT 'RY2' FROM system.iota) c   
  
SELECT distinct 'Grand Forks' as dl1,storecode AS dl2,dl1 AS dl3,dl2 AS dl4,
  dl3 AS dl5 FROM zcb 
WHERE glaccount IS NULL  
  AND name = 'cigar box'
  
SELECT DISTINCT dl1,dl2,dl3
FROM zcb
WHERE name = 'cigar box'
  AND storecode = 'ry1'
  AND glaccount IS NULL   
  
SELECT * FROM zcb
WHERE name = 'cigar box'
  AND storecode = 'ry1'
  AND glaccount IS NULL 
  AND dl2 = 'service'  
-- 11/16
-- why the fuck DO i have double cb service dl3=na NULL account rows?  
-- why the fuck are there even rows for dl3 = na?
SELECT * FROM zcb WHERE name = 'cigar box' AND dl2 = 'service' AND dl3 = 'na' AND storecode = 'ry1'  
-- fuck it, forge ahead with gmfs expenses 

-- 11/18 include F&I AS DISTINCT dl4
--       include new/used AS variable dl5
DROP TABLE #dept;
CREATE TABLE #dept (
  dl1 cichar(24),
  dl2 cichar(24),
  dl3 cichar(24),
  dl4 cichar(24),
  dl5 cichar(24));
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New' FROM system.iota; 
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','Used' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','F&I','New' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','F&I','Used' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','NCR','NCR' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Fixed','Mechanical','Main Shop' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Fixed','Mechanical','PDQ' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Fixed','Mechanical','Detail' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Fixed','Mechanical','Car Wash' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Fixed','Body Shop','Body Shop' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Fixed','Parts','Parts' FROM system.iota;

DROP TABLE #acct;
CREATE TABLE #acct (
  al1 cichar(30),
  al2 cichar(30),
  al3 cichar(40),
  account cichar(3),
  line integer);
-- 11/18, these values are FROM the online GM Accounting manual COA
-- appears there are some differences BETWEEN that document AND the verbage ON TBEdit
-- change these to match TBEdit
-- hmm, for expenses, the line number FROM FS seems to be consistent across pages pages 2,3,4, ADD line number
INSERT INTO #acct SELECT 'Total Expenses', 'Variable Selling Expenses','Vehicle Salespeople Compensation & Other','011',4 FROM system.iota; 
INSERT INTO #acct SELECT 'Total Expenses', 'Variable Selling Expenses','Delivery Expense','013',5 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Variable Selling Expenses','Policy Work-Vehicles','015',6 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Personnel Expenses','Salaries-Owners/Executive Managers','020',8 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Personnel Expenses','Salaries-Supervision','021',9 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Personnel Expenses','Salaries-Clerical','022',10 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Personnel Expenses','Other Salaries & Wages','023',11 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Personnel Expenses','Absentee Compensation','024',12 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Personnel Expenses','Incentives-Supervision','026',13 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Personnel Expenses','Taxes-Payroll','025',14 FROM system.iota;

INSERT INTO #acct SELECT 'Total Expenses', 'Personnel Expenses','Employee Benefits','027',15 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Personnel Expenses','Retirement Benefits','029',16 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Company Vehicle Expense','051',18 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Office Supplies & Expenses','060',19 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Other Supplies','061',20 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','E-Commerce Advertising/Fees','063',21 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Advertising','065',22 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Advertising Rebates','064',23 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Contributions','066',24 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Policy Work-Parts & Service','067',25 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Information Technology Services','068',26 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Outside Services (Other)','069',27 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Travel & Entertainment','070',28 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Membership Dues & Publications','071',29 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Legal & Auditing Expense','072',30 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Telephone','074',31 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Training Expense','075',32 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Interest-Floorplan','076',33 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Interest-Floorplan Credit','078',34 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Interest-Notes Payable (Other)','079',35 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Insurance Inventory','056',36 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Bad Debt Expense','057',37 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Freight, Postage & Shipping','033',38 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Semi-Fixed Expenses','Miscellaneous Expense','077',39 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Rent','080',41 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Amortization-Leaseholds','081',42 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Repairs-Real Estate','082',43 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Depreciation Bldgs & Improvements','083',44 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Taxes-Real Estate','084',45 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Insurance Bldgs & Improvements','085',46 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Interest-Mortgages','086',47 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Utilities','087',48 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Insurance Other','088',50 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Taxes-Other','089',51 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Repairs-Equipment','090',52 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Depreciation-Equipment','091',53 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Fixed Expenses','Equipment Rental','092',54 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Adjustments','Bonuses-Employees','097',999 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Adjustments','Bonuses-Owners','098',999 FROM system.iota;
INSERT INTO #acct SELECT 'Total Expenses', 'Adjustments','Income Taxes - Current Year','099',999 FROM system.iota;


SELECT a.dl2, a.al2, a.al3, a.glaccount, b.gmdesc, c.gmfsaccount
FROM zcb a
LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
WHERE name = 'cigar box'
  AND a.storecode = 'ry1'
  AND a.glaccount IS NOT NULL 
  AND a.dl2 = 'body shop'

SELECT *
FROM (  
  SELECT dl4, al3, account
  FROM #dept,#acct
  WHERE dl4 = 'body shop'
    AND al2 = 'semi-fixed expenses') a
LEFT JOIN (
SELECT a.dl2, a.al2, a.al3, a.glaccount, b.gmdesc, c.gmfsaccount
FROM zcb a
LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
WHERE name = 'cigar box'
  AND a.storecode = 'ry1'
  AND a.glaccount IS NOT NULL 
  AND a.dl2 = 'body shop') b ON a.account = LEFT(b.gmfsaccount, 3)    

the whole question of page AND line  

-- 11/18


while ON the one hand, i DO NOT want to get distracted BY trying to include too much too soon
AND stay focused ON the immediate deliverable: GMFSExpenses
ON the other i think i am nuts to NOT include a broader dept definition than what
IS currently suggested for gmfsExpenses
eg
F&I
New/Used
just fucking DO it - done

SELECT *
FROM #dept, #acct

-- ok, here IS body shop, what to DO with NULL glaccount eg 056, 076, 078
SELECT *
FROM (  
  SELECT dl4, dl5, al3, account
  FROM #dept,#acct
  WHERE dl4 = 'body shop'
    AND al2 = 'semi-fixed expenses') a
LEFT JOIN (
SELECT a.dl2, a.al2, a.al3, a.glaccount, b.gmdesc, c.gmfsaccount
FROM zcb a
LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
WHERE name = 'cigar box'
  AND a.storecode = 'ry1'
  AND a.glaccount IS NOT NULL 
  AND a.dl2 = 'body shop') b ON a.account = LEFT(b.gmfsaccount, 3)   
  
SELECT DISTINCT dl2 FROM zcb WHERE name = 'cigar box'  

-- so part of the mind fucking IS, there are no expenses for F&I, just sales & cogs
-- the relationship BETWEEN different hierarchies: cigar box vs GMFSExpenses
-- fuck DO i need to put IN some sort ORDER for greg?

-- let us start looking at some numbers
-- take out glpmast AND account, GROUP ON fs headers
-- employee benefits L15, missing amount
SELECT dl4, a.al3, sum(coalesce(d.gttamt, 0))
FROM (  
  SELECT dl4, dl5, al3, account, line
  FROM #dept,#acct
  WHERE dl4 = 'body shop'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount
  FROM zcb a
--  LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
--    AND b.gmyear = 2012
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'body shop') b ON a.account = LEFT(b.gmfsaccount, 3)  
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'    
GROUP BY dl4, a.al3, a.line 
ORDER BY a.line  


-- GROUP BY dl4, al2, ORDER BY line
SELECT dl4, a.al2, sum(coalesce(d.gttamt, 0)), MIN(a.line)
FROM (  
  SELECT dl4, dl5, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl4 = 'body shop'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount
  FROM zcb a
--  LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
--    AND b.gmyear = 2012
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'body shop') b ON a.account = LEFT(b.gmfsaccount, 3)  
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'    
GROUP BY dl4, a.al2
ORDER BY min(a.line)  

SELECT *
-- SELECT COUNT(*)
FROM #dept,#acct
ORDER BY dl5, line

-- time out, wtf, how am i putting this shit together
account comes from dimaccount via JOIN ON gmfsaccount
but it turns out gmfsaccount IS NOT good enuf
honda AND crookston are NOT going to be so easy
-- ahhh, fucking gmfsaccount IS the same for ALL depts
SELECT *
FROM #dept,#acct
WHERE account = '070'

glaccount coming FROM zcb/cigar box
so,what cigar box dept matches gmfsdept

SELECT storecode, dl1,dl2,dl3 FROM zcb WHERE name = 'cigar box' and storecode = 'ry1' GROUP BY storecode, dl1,dl2,dl3
SELECT * FROM zcb
so the first thing that comes to mind IS that cigar box departments need to be modified
to be LIKE gmfs IN AS far AS dl1 = market, dl2 = store, thereby eliminating the 
need for the storecode COLUMN 
time for a rule?
any departmental hierarchy must be dl1 = market & dl2 = store
hold off ON that for now
back to gmfs expenses

SELECT *
FROM #dept,#acct

SELECT *
FROM dimaccount 
WHERE gmfsaccountlevel1 = 'Expenses'
  AND cbdeptl1 = 'store'

SELECT gldepartment, gmfsdepartmentlevel2  
FROM dimaccount
WHERE gmfsaccountlevel1 = 'Expenses'
  AND cbdeptl1 = 'store'
GROUP BY  gldepartment, gmfsdepartmentlevel2  

SELECT * FROM #dept

cb            gmfs dl3-dl4-dl5
              variable-sales-new
              
              
              

SELECT a.dl4, a.al2, sum(coalesce(d.gttamt, 0)), MIN(a.line)
FROM (  
  SELECT dl2, dl4, dl5, al2, al3, account, line
  FROM #dept,#acct
  WHERE dl2 = 'RY1') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount
  FROM zcb a
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL) b ON a.account = LEFT(b.gmfsaccount, 3)  
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'    
GROUP BY a.dl4, a.al2
ORDER BY dl4, min(a.line) 

-- fuck it, back to getting what i can FROM existing data (accountdim, zcb.cigar box)
-- body shop ok
SELECT dl4, a.al2, a.al3, sum(coalesce(d.gttamt, 0)), MIN(a.line)
FROM (  
  SELECT dl4, dl5, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl4 = 'body shop'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount
  FROM zcb a
--  LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
--    AND b.gmyear = 2012
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'body shop') b ON a.account = LEFT(b.gmfsaccount, 3)  
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'    
GROUP BY dl4, a.al2, a.al3
ORDER BY min(a.line) 

-- parts ok
SELECT dl4, a.al2, a.al3, sum(coalesce(d.gttamt, 0)), MIN(a.line)
FROM (  
  SELECT dl4, dl5, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl4 = 'parts'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount
  FROM zcb a
--  LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
--    AND b.gmyear = 2012
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'parts') b ON a.account = LEFT(b.gmfsaccount, 3)  
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'    
GROUP BY dl4, a.al2, a.al3
ORDER BY min(a.line) 

-- ncr
-- line 21 query returns 18.77 but fs IS 0
SELECT dl4, a.al2, a.al3, sum(coalesce(d.gttamt, 0)), MIN(a.line)
FROM (  
  SELECT dl4, dl5, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl4 = 'ncr'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount
  FROM zcb a
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'ncr') b ON a.account = LEFT(b.gmfsaccount, 3)  
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'    
GROUP BY dl4, a.al2, a.al3
ORDER BY min(a.line) 

SELECT *
FROM #acct
WHERE line = 21
-- problem IS account 16354, this returns 2 rows
SELECT glaccount, glaccountdescription, gldepartment, gmfsdepartmentlevel2, cbdeptl2, b.*
FROM dimaccount a
LEFT JOIN stgarkonaglptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012' 
--WHERE LEFT(gmfsaccount, 3) = '063'
WHERE glaccount = '16354'

-- mechanical
-- going to have to break out pdq,etc
-- personnel for car wash AND detail wrong ON jeri's cb, doc has been fixed, acct 12324
SELECT dl5, a.al2, sum(coalesce(d.gttamt, 0)), MIN(a.line)
-- SELECT *
FROM (  
  SELECT dl4, dl5, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl4 = 'mechanical'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount, a.dl3
  FROM zcb a
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'service') b ON a.account = LEFT(b.gmfsaccount, 3)  
    AND a.dl5 = b.dl3
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'    
GROUP BY dl5, a.al2
ORDER BY dl5, min(a.line) 
/*
-- october detail fixed lo BY $80? cb IS wrong, query agrees with doc
-- lets look at october fixed detail
-- oct main shop personnel 118 lo, IN this CASE to query agrees with doc
-- so, ok, good enuf
SELECT dl5, a.al2, a.al3, sum(coalesce(d.gttamt, 0)), MIN(a.line)
-- SELECT *
FROM (  
  SELECT dl4, dl5, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl5 = 'main shop'
    AND al2 = 'personnel expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount, a.dl3
  FROM zcb a
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'service') b ON a.account = LEFT(b.gmfsaccount, 3)  
    AND a.dl5 = b.dl3
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'    
GROUP BY dl5, a.al2, a.al3
ORDER BY dl5, min(a.line) 
*/
-- sales
-- NOT even close
SELECT dl5, a.al2, a.al3,sum(coalesce(d.gttamt, 0)), MIN(a.line)
-- SELECT *
FROM (  
  SELECT dl4, dl5, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl5 = 'new'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, dl3, a.al2, a.al3, a.glaccount, c.gmfsaccount, a.dl3
  FROM zcb a
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl3 = 'new vehicles'
    AND al2 = 'total expenses') b ON a.account = LEFT(b.gmfsaccount, 3)  
    AND a.dl5 = LEFT(b.dl3,3)
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012'    
GROUP BY dl5, a.al2, a.al3
ORDER BY dl5, min(a.line) 

-- this IS working
-- new
SELECT al2, al3, sum(gttamt), MIN(a.line) AS line --account line, glaccount, gldepartment,gmfsaccount
FROM (
  SELECT dl5, al2, al3, account, line
  FROM #dept, #acct
  WHERE dl5 = 'new'
    AND dl4 = 'sales'
    AND al1 = 'total expenses') a
LEFT JOIN dimaccount b ON a.account = LEFT(b.gmfsaccount,3)
  AND right(TRIM(b.gmfsaccount),1) = 'a' 
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012' 
GROUP BY a.al2, a.al3  
ORDER BY line 
-- used bingo!
SELECT al2, al3, sum(gttamt), MIN(a.line) AS line --account line, glaccount, gldepartment,gmfsaccount
FROM (
  SELECT dl5, al2, al3, account, line
  FROM #dept, #acct
  WHERE dl5 = 'used'
    AND dl4 = 'sales'
    AND al1 = 'total expenses') a
LEFT JOIN dimaccount b ON a.account = LEFT(b.gmfsaccount,3)
  AND right(TRIM(b.gmfsaccount),1) = 'b' 
LEFT JOIN stgarkonaglptrns d ON b.glaccount = d.gtacct
  AND gtdate BETWEEN '09/01/2012' AND '09/30/2012' 
GROUP BY a.al2, a.al3  
ORDER BY line 

-- generally:  A: New
--             B: Used
--             C: Rental
--             D: Service
--             E: Body
--             F: Parts
-- DO i want to fuck around with a general approach OR just DO dept BY dept

/*
SELECT *
FROM #dept,#acct ORDER BY account

SELECT * FROM #dept
SELECT * FROM #acct

SELECT storecode, dl1,dl2,dl3 FROM zcb WHERE name = 'cigar box' and storecode = 'ry1' GROUP BY storecode, dl1,dl2,dl3

SELECT distinct gmfsdepartmentlevel2 FROM dimaccount
SELECT DISTINCT gmfsaccountlevel1, gmfsaccountlevel2 FROM dimaccount

SELECT * FROM dimaccount WHERE LEFT(gmfsaccount,3) = '060'

*/

-- 11/19 git er dun

CREATE TABLE zCB (
  Name cichar(20),
  StoreCode cichar(3),
  dl1 cichar(24),
  dl2 cichar(24),
  dl3 cichar(24),
  al1 cichar(24),
  al2 cichar(24),
  al3 cichar(24),
  al4 cichar(24),
  glAccount cichar(10),
  split double(2)) IN database;
  
ALTER TABLE zcb 
ADD COLUMN dl4 cichar(24) 
ADD COLUMN dl5 cichar(24)
ADD COLUMN line integer 
ADD COLUMN gmcoa cichar(6)
ALTER COLUMN al2 al2 cichar(30)
ALTER COLUMN al3 al3 cichar(40)
ALTER COLUMN al1 al1 cichar(30);

should there be a row IN zcb for fs lines that have no dept accounts assigned?
IF so, does it become a NULL glaccount?

so even IF body shop has no accounts assigned to variable selling expenses, there will be a row
IN zcb.gmfs exp with no glaccount with an value of NA, avoid NULL issues

al4 IS NA because there IS no node IN this hierarchy at that level

SELECT 'gmfs expenses','RY1','Grand Forks','RY1', dl3 fixed,dl4 body shop,dl5 bodyshop,
  'Total Expenses', al2 personnel, glaccount, split, line
FROM 

WHEN glaccount = NA THEN dl5/al3 are unique
WHEN glaccount <> NA glaccount IS unique

SELECT * FROM zcb WHERE name = 'gmfs expenses'

delete FROM zcb WHERE name = 'gmfs expenses'
-- body shop
INSERT INTO zcb 
SELECT 'gmfs expenses','RY1','Grand Forks','RY1','Fixed',a.dl4,a.dl5,
  a.al1, a.al2, a.al3, 'NA' AS al4, coalesce(glaccount, 'NA') AS glaccount, account, line, 1 
FROM (  
  SELECT dl4, dl5, al1, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl4 = 'body shop'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount
  FROM zcb a
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'body shop') b ON a.account = LEFT(b.gmfsaccount, 3) 

-- service
--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT 'gmfs expenses','RY1','Grand Forks','RY1','Fixed',a.dl4,a.dl5,
  a.al1, a.al2, a.al3, 'NA' AS al4, coalesce(glaccount, 'NA') AS glaccount, account, line, 1 
-- SELECT *
FROM (  
  SELECT dl4, dl5, al1,al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl4 = 'mechanical'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount, a.dl3
  FROM zcb a
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'service') b ON a.account = LEFT(b.gmfsaccount, 3)  
    AND a.dl5 = b.dl3
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1   

-- parts
--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT 'gmfs expenses','RY1','Grand Forks','RY1','Fixed',a.dl4,a.dl5,
  a.al1, a.al2, a.al3, 'NA' AS al4, coalesce(glaccount, 'NA') AS glaccount, account, line, 1 
FROM (  
  SELECT dl4, dl5, al1, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl4 = 'parts'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount
  FROM zcb a
--  LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
--    AND b.gmyear = 2012
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'parts') b ON a.account = LEFT(b.gmfsaccount, 3)  
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1     

-- ncr
INSERT INTO zcb
--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
SELECT 'gmfs expenses','RY1','Grand Forks','RY1','Variable',a.dl4,a.dl5,
  a.al1, a.al2, a.al3, 'NA' AS al4, coalesce(glaccount, 'NA') AS glaccount, account, line, 1 
FROM (  
  SELECT dl4, dl5, al1, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept,#acct
  WHERE dl4 = 'ncr'
    AND al1 = 'total expenses') a
LEFT JOIN (
  SELECT a.dl2, a.al2, a.al3, a.glaccount, c.gmfsaccount
  FROM zcb a
  LEFT JOIN dimaccount c ON a.glaccount = c.glaccount  
  WHERE name = 'cigar box'
    AND a.storecode = 'ry1'
    AND a.glaccount IS NOT NULL 
    AND a.dl2 = 'ncr') b ON a.account = LEFT(b.gmfsaccount, 3)  
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1  

-- new
INSERT INTO zcb
--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
SELECT 'gmfs expenses','RY1','Grand Forks','RY1','Variable',a.dl4,a.dl5,
  a.al1, a.al2, a.al3, 'NA' AS al4, coalesce(glaccount, 'NA') AS glaccount, account, line, 1 
FROM (
  SELECT dl4, dl5, al1, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept, #acct
  WHERE dl5 = 'new'
    AND dl4 = 'sales'
    AND al1 = 'total expenses') a
LEFT JOIN dimaccount b ON a.account = LEFT(b.gmfsaccount,3)
  AND right(TRIM(b.gmfsaccount),1) = 'a' 
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1  


-- used 
INSERT INTO zcb
--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
SELECT 'gmfs expenses','RY1','Grand Forks','RY1','Variable',a.dl4,a.dl5,
  a.al1, a.al2, a.al3, 'NA' AS al4, coalesce(glaccount, 'NA') AS glaccount, account, line, 1 
FROM (
  SELECT dl4, dl5, al1, al2, al3, account, coalesce(line, 0) AS line
  FROM #dept, #acct
  WHERE dl5 = 'used'
    AND dl4 = 'sales'
    AND al1 = 'total expenses') a
LEFT JOIN dimaccount b ON a.account = LEFT(b.gmfsaccount,3)
  AND right(TRIM(b.gmfsaccount),1) = 'b' 
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1    


-- page 2 Total Dealership
SELECT a.dl2, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
WHERE name = 'gmfs expenses'
GROUP BY a.dl2, a.al2, a.al3
ORDER BY MIN(line)
-- page 2 Variable
SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE name = 'gmfs expenses'
  AND dl3 = 'variable'
  AND dl2 = 'ry1'
GROUP BY a.dl2, dl3, a.al2, a.al3
ORDER BY MIN(line)
-- page 2 fixed
SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
WHERE name = 'gmfs expenses'
  AND dl3 = 'fixed'
GROUP BY a.dl2, dl3, a.al2, a.al3
ORDER BY MIN(line)


-- page 2 
SELECT a.dl2, a.line, a.al3, a.expr AS "Total Dealership", b.expr AS Variable, c.expr AS Fixed
FROM (
  SELECT a.dl2, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
  GROUP BY a.dl2, a.al2, a.al3) a
LEFT JOIN ( 
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl3 = 'variable'
  GROUP BY a.dl2, dl3, a.al2, a.al3) b ON a.line = b.line 
LEFT JOIN (
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl3 = 'fixed'
  GROUP BY a.dl2, dl3, a.al2, a.al3) c ON b.line = c.line
WHERE a.line <> 999  
ORDER BY a.line  

-- page 3
SELECT a.dl2, a.line, a.al3, a.expr AS "New Vehicle Dept", b.expr AS "Use Vehicle Dept", c.expr AS NCR
FROM (
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl4 = 'sales'
    AND dl5 = 'new'
  GROUP BY a.dl2, dl3, a.al2, a.al3) a 
LEFT JOIN (
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl4 = 'sales'
    AND dl5 = 'used'
  GROUP BY a.dl2, dl3, a.al2, a.al3) b ON a.line = b.line 
LEFT JOIN (  
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl4 = 'ncr'
  GROUP BY a.dl2, dl3, a.al2, a.al3) c ON b.line = c.line  
WHERE a.line <> 999  
ORDER BY a.line  
 
-- page 4
SELECT a.dl2, a.line, a.al3, a.expr AS "Mechanical", b.expr AS "Body Shop", c.expr AS Parts
FROM (
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl4 = 'mechanical'
  GROUP BY a.dl2, dl3, a.al2, a.al3) a 
LEFT JOIN (
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl4 = 'body shop'
  GROUP BY a.dl2, dl3, a.al2, a.al3) b ON a.line = b.line 
LEFT JOIN (  
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl4 = 'parts'
  GROUP BY a.dl2, dl3, a.al2, a.al3) c ON b.line = c.line  
WHERE a.line <> 999  
ORDER BY a.line  

-- page 4 with mechanical subdepts
SELECT a.dl2, a.line, a.al3, a.expr AS "Mechanical", b.expr as "Main Shop", c.expr as PDQ,
  d.expr as Detail, e.expr as "Car Wash"
FROM (
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl4 = 'mechanical'
  GROUP BY a.dl2, dl3, a.al2, a.al3) a 
LEFT JOIN (
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl5 = 'main shop'
  GROUP BY a.dl2, dl3, a.al2, a.al3) b ON a.line = b.line 
LEFT JOIN (  
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl5 = 'pdq'
  GROUP BY a.dl2, dl3, a.al2, a.al3) c ON a.line = c.line  
LEFT JOIN (  
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl5 = 'detail'
  GROUP BY a.dl2, dl3, a.al2, a.al3) d ON a.line = d.line    
LEFT JOIN (  
  SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
  FROM zcb a
  LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
    AND b.gtdate BETWEEN '09/01/2012' AND '09/30/2012'
  WHERE name = 'gmfs expenses'
    AND dl5 = 'car wash'
  GROUP BY a.dl2, dl3, a.al2, a.al3) e ON a.line = e.line   
WHERE a.line <> 999  
ORDER BY a.line  

SELECT dl1,dl2,dl3,dl4,dl5 FROM zcb WHERE name = 'gmfs expenses' GROUP BY dl1,dl2,dl3,dl4,dl5
SELECT al1,al2,al3, line FROM zcb WHERE name = 'gmfs expenses' GROUP BY al1,al2,al3, line ORDER BY line

/**************** RY2 ********************************************************/
-- nothing IN dimaccount (yet)

SELECT a.dl2, a.dl3, a.al3, a.glaccount, b.gmtype, b.gmdesc, b.gmdept, SUM(c.gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND gmyear = 2012
LEFT JOIN stgArkonaGLPTRNS c ON a.glaccount = c.gtacct 
  AND c.gtdate BETWEEN '10/01/2012' AND '10/31/2012' 
WHERE a.name = 'cigar box'
AND a.storecode = 'ry2'
  AND a.al2 = 'total expenses'
GROUP BY a.dl2, a.dl3, a.al3, a.glaccount, b.gmtype, b.gmdesc, b.gmdept  

-- this generates the accounts FROM cigar box to past INTO honda gmfs.xlsx
SELECT a.dl2, a.dl3, a.al3, a.glaccount, b.gmdesc, SUM(c.gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND gmyear = 2012
LEFT JOIN stgArkonaGLPTRNS c ON a.glaccount = c.gtacct 
  AND c.gtdate BETWEEN '10/01/2012' AND '10/31/2012' 
WHERE a.name = 'cigar box'
AND a.storecode = 'ry2'
  AND a.al2 = 'total expenses'
  AND a.dl2 = 'variable'
  AND glaccount IS NOT null
GROUP BY a.dl2, a.dl3, a.al3, a.glaccount, b.gmtype, b.gmdesc, b.gmdept


SELECT al2,al3, line, gmcoa
FROM zcb
WHERE name = 'gmfs expenses'
GROUP BY al2,al3, line, gmcoa
ORDER BY line

SELECT *
FROM stgArkonaGLPMAST 
WHERE gmacct LIKE '23306%'
  AND gmyear = 2012
  
SELECT month(gtdate), SUM(gttamt)
FROM stgarkonaglptrns
WHERE gtacct = '25706'
  AND year(gtdate) = 2012  
GROUP BY month(gtdate)  

SELECT *
FROM zcb
WHERE glaccount IN ('23306')

INSERT INTO zcb values('Cigar Box','RY2','Store','Parts','NA',NULL,NULL,'Net Profit','Total Expenses','Semi-Fixed Expenses','NA','23306D',NULL,NULL,1)
  
SELECT name,'RY2',dl1,'RY2',dl3,dl4,dl5,al1,al2,al3,'NA',glaccount,gmcoa,line,split
FROM zcb WHERE name = 'gmfs expenses' AND dl5 = 'parts' 

CREATE TABLE zry2 (
  line integer,
  gmcoa integer,
  glaccount cichar(10)) IN database;
insert into zry2 values(4,11,'NA');
...
insert into zry2 values(54,92,'29206');

--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT distinct name,'RY2' AS storecode,dl1,'RY2' AS dl2,dl3,dl4,dl5,al1,al2,al3,'NA' AS al4,b.glaccount,a.gmcoa,a.line,split
--SELECT COUNT(*) -- 89
FROM zcb a
LEFT JOIN zry2 b ON a.line = b.line 
WHERE name = 'gmfs expenses' AND dl5 = 'parts' 
  AND a.line <> 999
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1

-- checks out ok
SELECT c.al3, c.glaccount, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'parts'
  AND dl2 = 'RY2'
GROUP BY c.al3, c.glaccount, c.line  
ORDER BY line

-- main shop
DROP TABLE zry2;
CREATE TABLE zry2 (
  line integer,
  glaccount cichar(10)) IN database;
insert into zry2 values(4,'NA');
...
insert into zry2 values(54,'29204');

--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT distinct name,'RY2' AS storecode,dl1,'RY2' AS dl2,dl3,dl4,dl5,al1,al2,al3,'NA' AS al4,b.glaccount,a.gmcoa,a.line,split
--SELECT COUNT(*) -- 89
FROM zcb a
LEFT JOIN zry2 b ON a.line = b.line 
WHERE name = 'gmfs expenses' 
  AND dl5 = 'main shop' 
  AND a.line <> 999
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1

-- checks out ok
SELECT c.al3, c.glaccount, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'main shop'
  AND dl2 = 'RY2'
GROUP BY c.al3, c.glaccount, c.line  
ORDER BY line

-- pdq
DROP TABLE zry2;
CREATE TABLE zry2 (
  line integer,
  glaccount cichar(10)) IN database;
insert into zry2 values(4,'NA');
...
insert into zry2 values(54,'NA');

--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT distinct name,'RY2' AS storecode,dl1,'RY2' AS dl2,dl3,dl4,dl5,al1,al2,al3,'NA' AS al4,b.glaccount,a.gmcoa,a.line,split
--SELECT COUNT(*) -- 89
FROM zcb a
LEFT JOIN zry2 b ON a.line = b.line 
WHERE name = 'gmfs expenses' 
  AND dl5 = 'pdq' 
  AND a.line <> 999
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1

-- all mechanical checks out ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl4 = 'mechanical'
  AND dl2 = 'RY2'
GROUP BY c.al3, c.line  
ORDER BY line
-- pdq checks out ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'pdq'
  AND dl2 = 'RY2'
GROUP BY c.al3, c.line  
ORDER BY line

-- new
DROP TABLE zry2;
CREATE TABLE zry2 (
  line integer,
  glaccount cichar(10)) IN database;
insert into zry2 values(4,'21101');
...
insert into zry2 values(54,'29210');

--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT distinct name,'RY2' AS storecode,dl1,'RY2' AS dl2,dl3,dl4,dl5,al1,al2,al3,'NA' AS al4,b.glaccount,a.gmcoa,a.line,split
--SELECT COUNT(*) -- 89
FROM zcb a
LEFT JOIN zry2 b ON a.line = b.line 
WHERE name = 'gmfs expenses' 
  AND dl5 = 'new' 
  AND a.line <> 999
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1
-- new checks ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'new'
  AND dl2 = 'RY2'
GROUP BY c.al3, c.line  
ORDER BY line

-- used
DROP TABLE zry2;
CREATE TABLE zry2 (
  line integer,
  glaccount cichar(10)) IN database;
insert into zry2 values(4,'21102');
...
insert into zry2 values(54,'29202');


--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT distinct name,'RY2' AS storecode,dl1,'RY2' AS dl2,dl3,dl4,dl5,al1,al2,al3,'NA' AS al4,b.glaccount,a.gmcoa,a.line,split
--SELECT COUNT(*) -- 89
FROM zcb a
LEFT JOIN zry2 b ON a.line = b.line 
WHERE name = 'gmfs expenses' 
  AND dl5 = 'used' 
  AND a.line <> 999
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1

-- used checks ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'used'
  AND dl2 = 'RY2'
GROUP BY c.al3, c.line  
ORDER BY line

-- total variable checks ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl3 = 'variable'
  AND dl2 = 'RY2'
GROUP BY c.al3, c.line  
ORDER BY line
-- store
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl2 = 'RY2'
GROUP BY c.al3, c.line  
ORDER BY line


/**************** RY2 ********************************************************/

/**************** RY3 ********************************************************/    

-- this generates the accounts FROM cigar box to passINTO ry3 gmfs.xlsx
SELECT a.dl2, a.dl3, a.al3, a.glaccount, b.gmdesc, SUM(c.gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND gmyear = 2012
LEFT JOIN stgArkonaGLPTRNS c ON a.glaccount = c.gtacct 
  AND c.gtdate BETWEEN '10/01/2012' AND '10/31/2012' 
WHERE a.name = 'cigar box'
AND a.storecode = 'ry3'
  AND a.al2 = 'total expenses'
  AND a.dl2 = 'variable'
  AND glaccount IS NOT null
GROUP BY a.dl2, a.dl3, a.al3, a.glaccount, b.gmtype, b.gmdesc, b.gmdept

-- parts
DROP TABLE zry3;
CREATE TABLE zry3 (
  line integer,
  glaccount cichar(10)) IN database;
insert into zry3 values(4,'NA');
...
insert into zry3 values(54,'39206');

--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT distinct name,'RY3' AS storecode,dl1,'RY3' AS dl2,dl3,dl4,dl5,al1,al2,al3,'NA' AS al4,b.glaccount,a.gmcoa,a.line,split
--SELECT COUNT(*) -- 89
FROM zcb a
LEFT JOIN zry3 b ON a.line = b.line 
WHERE name = 'gmfs expenses' 
AND dl5 = 'parts' 
  AND a.line <> 999
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1
-- parts checks ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'parts'
  AND dl2 = 'RY3'
GROUP BY c.al3, c.line  
UNION
SELECT 'total', 999 AS line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'parts'
  AND dl2 = 'RY3'
ORDER BY line

-- service
DROP TABLE zry3;
CREATE TABLE zry3 (
  line integer,
  glaccount cichar(10)) IN database;
insert into zry3 values(4,'NA');
...
insert into zry3 values(54,'39204');

--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT distinct name,'RY3' AS storecode,dl1,'RY3' AS dl2,dl3,dl4,dl5,al1,al2,al3,'NA' AS al4,b.glaccount,a.gmcoa,a.line,split
--SELECT COUNT(*) -- 89
FROM zcb a
LEFT JOIN zry3 b ON a.line = b.line 
WHERE name = 'gmfs expenses' 
  AND dl5 = 'main shop'
  AND a.line <> 999
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1

-- service ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'main shop'
  AND dl2 = 'RY3'
GROUP BY c.al3, c.line  
UNION
SELECT 'total', 999 AS line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'main shop'
  AND dl2 = 'RY3'
ORDER BY line

-- fixed ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl3 = 'fixed'
  AND dl2 = 'RY3'
GROUP BY c.al3, c.line  
UNION
SELECT 'total', 999 AS line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl3 = 'fixed'
  AND dl2 = 'RY3'
ORDER BY line

-- new
DROP TABLE zry3;
CREATE TABLE zry3 (
  line integer,
  glaccount cichar(10)) IN database;
insert into zry3 values(4,'31101');

insert into zry3 values(54,'39201');


--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT distinct name,'RY3' AS storecode,dl1,'RY3' AS dl2,dl3,dl4,dl5,al1,al2,al3,'NA' AS al4,b.glaccount,a.gmcoa,a.line,split
--SELECT COUNT(*) -- 89
FROM zcb a
LEFT JOIN zry3 b ON a.line = b.line 
WHERE name = 'gmfs expenses' 
  AND dl5 = 'new'
  AND a.line <> 999
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1

-- new now ok
/* fix an oops
DELETE FROM zcb WHERE name = 'gmfs expenses' AND dl2 = 'ry3' AND glaccount = '37601';
UPDATE zcb
SET glaccount = '37601'
--SELECT * FROM  zcb
WHERE dl2 = 'ry3'
  AND name = 'gmfs expenses'
  AND al3 = 'Interest-Floorplan'
  AND dl5 = 'new'
 */
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'new'
  AND dl2 = 'RY3'
GROUP BY c.al3, c.line  
UNION
SELECT 'total', 999 AS line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'new'
  AND dl2 = 'RY3'
ORDER BY line



-- USED
DROP TABLE zry3;
CREATE TABLE zry3 (
  line integer,
  glaccount cichar(10)) IN database;
insert into zry3 values(4,'31102');	
insert into zry3 values(5,'31302');	
insert into zry3 values(5,'31302A');	
insert into zry3 values(5,'31302B');	
insert into zry3 values(5,'31302C');	
insert into zry3 values(6,'31502');	
insert into zry3 values(6,'31502A');	
insert into zry3 values(6,'31502B');	
insert into zry3 values(6,'31502C');	
insert into zry3 values(6,'31502D');	
insert into zry3 values(6,'31502E');	
insert into zry3 values(6,'31502F');	
insert into zry3 values(6,'31502L');	
insert into zry3 values(8,'NA');	
insert into zry3 values(9,'32102');	
insert into zry3 values(10,'32202');	
insert into zry3 values(11,'32302');	
insert into zry3 values(12,'NA');	
insert into zry3 values(13,'NA');	
insert into zry3 values(14,'32502');	
insert into zry3 values(15,'32702');	
insert into zry3 values(16,'32902');	
insert into zry3 values(18,'35102');	
insert into zry3 values(19,'36002');	
insert into zry3 values(20,'36102');	
insert into zry3 values(21,'36302');	
insert into zry3 values(21,'36302A');	
insert into zry3 values(21,'36302B');	
insert into zry3 values(22,'36502');	
insert into zry3 values(23,'36402');	
insert into zry3 values(24,'36602');	
insert into zry3 values(25,'NA');	
insert into zry3 values(26,'36802');	
insert into zry3 values(27,'36902');	
insert into zry3 values(28,'37002');	
insert into zry3 values(29,'37102');	
insert into zry3 values(30,'37202');	
insert into zry3 values(31,'37402');	
insert into zry3 values(32,'37502');	
insert into zry3 values(33,'37602');	
insert into zry3 values(34,'NA');	
insert into zry3 values(35,'37902');	
insert into zry3 values(36,'35602');	
insert into zry3 values(37,'NA');	
insert into zry3 values(38,'33302');	
insert into zry3 values(39,'37702');	
insert into zry3 values(41,'38002');	
insert into zry3 values(42,'38102');	
insert into zry3 values(43,'38202');	
insert into zry3 values(44,'NA');	
insert into zry3 values(45,'38402');	
insert into zry3 values(46,'NA');	
insert into zry3 values(47,'NA');	
insert into zry3 values(48,'38702');	
insert into zry3 values(50,'38802');	
insert into zry3 values(51,' NA');	
insert into zry3 values(52,'39002');	
insert into zry3 values(53,'39102');	
insert into zry3 values(54,'39202');	

--SELECT dl5, al3 FROM (
--SELECT glaccount FROM (
INSERT INTO zcb
SELECT distinct name,'RY3' AS storecode,dl1,'RY3' AS dl2,dl3,dl4,dl5,al1,al2,al3,'NA' AS al4,b.glaccount,a.gmcoa,a.line,split
--SELECT COUNT(*) -- 89
FROM zcb a
LEFT JOIN zry3 b ON a.line = b.line 
WHERE name = 'gmfs expenses' 
  AND dl5 = 'used'
  AND a.line <> 999
--) x WHERE glaccount <> 'na' GROUP BY glaccount HAVING COUNT(*) > 1   
--) x WHERE glaccount = 'na' GROUP BY dl5, al3 HAVING COUNT(*) > 1

-- used ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'used'
  AND dl2 = 'RY3'
GROUP BY c.al3, c.line  
UNION
SELECT 'total', 999 AS line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl5 = 'used'
  AND dl2 = 'RY3'
ORDER BY line

-- variable ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl3 = 'variable'
  AND dl2 = 'RY3'
GROUP BY c.al3, c.line  
UNION
SELECT 'total', 999 AS line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl3 = 'variable'
  AND dl2 = 'RY3'
ORDER BY line


-- store ok
SELECT c.al3, c.line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl2 = 'RY3'
GROUP BY c.al3, c.line  
UNION
SELECT 'total', 999 AS line, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl2 = 'RY3'
ORDER BY line

/**************** RY3 ********************************************************/

SELECT * FROM zcb WHERE name = 'gmfs expenses' and dl2 = 'ry2' AND dl4 = 'mechanical'


SELECT dl2, dl3, dl4, c.al2, sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl2 = 'RY1'
GROUP BY dl2, dl3, dl4, c.al2 



SELECT dl3, c.al2, c.al3, line, 
  sum(case when dl5 = 'new' THEN b.gttamt ELSE 0 END) AS new,
  sum(case when dl5 = 'used' THEN b.gttamt ELSE 0 END) AS used,
  sum(case when dl5 = 'ncr' THEN b.gttamt ELSE 0 END) AS ncr,
  SUM(b.gttamt) AS "total variable"
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl2 = 'RY1'
  AND dl3 = 'variable'
GROUP BY dl3, c.al2, c.al3, line
ORDER BY line


SELECT dl3, c.al2,
  sum(case when dl5 = 'new' THEN b.gttamt ELSE 0 END) AS new,
  sum(case when dl5 = 'used' THEN b.gttamt ELSE 0 END) AS used,
  sum(case when dl5 = 'ncr' THEN b.gttamt ELSE 0 END) AS ncr,
  SUM(b.gttamt) AS "total variable"
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount 
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.name = 'gmfs expenses'
  AND dl2 = 'RY1'
  AND dl3 = 'variable'
GROUP BY dl3, c.al2
