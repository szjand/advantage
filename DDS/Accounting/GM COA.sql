--10/10
-- this IS ok, but combines fi with nc & uc WHEN i GROUP BY arkona depts
-- so i need to integrate fs depts
-- only need gmfsdepts for sales & cogs, i am thinking gldepts suffice for expenses, LR
-- 10/11 thought abt adding gmfsdeptlevel3 for variable::f&i:new/used
DROP TABLE #wtf;
SELECT a.gmacct, a.gmtype, a.gmdesc, a.gmdept, b.fxfact, fxfp01 AS split,
  CASE
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 200 AND 296 THEN 'Assets'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 300 AND 359 THEN 'Liabilities'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 360 AND 399 THEN 'Owner Equity'
--    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 509 -- 510 assigned to 172754 a COGS account
--      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 511 AND 599 
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 599      
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 806 AND 811 THEN 'Sales'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 600 AND 744 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 850 AND 861 THEN 'COGS'
--      OR CAST(LEFT(fxfact, 3) AS sql_integer) = 510 THEN 'COGS'   -- 510 assigned to 172754 a COGS account
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 902 AND 910 THEN 'Additions to Income'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 952 AND 955 THEN 'Deductions from Income'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 11 AND 99 THEN 'Expenses' 
  END AS gmfsAccountLevel1,  
  CASE 
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 200 AND 205 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) = 260 THEN 'Cash & Contracts'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 210 AND 225 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 261 AND 264 THEN 'Receivables'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 230 AND 258 THEN 'Inventories'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 270 AND 274 THEN 'Prepaid Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 275 AND 277 THEN 'Working Assets'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 280 AND 289 THEN 'Fixed Assets'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 291 AND 296 THEN 'Other Assets'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 11 AND 15 THEN 'Variable Selling Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 20 AND 29 THEN 'Personnel Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 33 AND 79 THEN 'Semi-Fixed Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 80 AND 92 THEN 'Fixed Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 97 AND 99 THEN 'Adjustment Expenses'

    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 360 AND 399 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 599 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 806 AND 811 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 600 AND 744 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 850 AND 861 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 902 AND 910 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 952 AND 955 THEN 'NA'
        
  END AS gmfsAccountLevel2,
  CASE
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 200 AND 399 THEN 'Dealership'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 457 THEN 'Variable'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 600 AND 657 THEN 'Variable'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 806 AND 811 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 850 AND 860
      OR CAST(LEFT(fxfact, 3) AS sql_integer) IN (443,444,454,455,643,644,654,655)  THEN 'Variable'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 460 AND 492 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 660 AND 692 THEN 'Fixed Operations'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) IN (530,534,535,536, 510, 710)
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 730 AND 738 THEN 'Lease & Rental'
  END AS gmfsDepartmentLevel1,
  CASE
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 200 AND 399 THEN 'NA'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 438 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) IN (440,441,445,457)  THEN 'New Vehicle Department'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 600 AND 638 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) IN (640,641,645,657)  THEN 'New Vehicle Department'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 446 AND 452 OR CAST(LEFT(fxfact, 3) AS sql_integer) = 456 THEN 'Used Vehicle Department'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 646 AND 653 OR CAST(LEFT(fxfact, 3) AS sql_integer) = 656 THEN 'Used Vehicle Department'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 806 AND 811 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 850 AND 860
      OR CAST(LEFT(fxfact, 3) AS sql_integer) IN (443,444,454,455,643,644,654,655)  THEN 'Finance & Insurance'  
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 460 AND 469 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 660 AND 669 THEN 'Mechanical'   
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 470 AND 479 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 670 AND 679 THEN 'Body Shop'  
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 467 AND 492 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 667 AND 692 THEN 'Parts & Accessories' 
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) IN (530,534,535,536)
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 730 AND 738 THEN 'NA'                 
  END AS gmfsDepartmentLevel2
INTO #wtf  
FROM stgArkonaGLPMAST a
LEFT JOIN stgArkonaffpxrefdta b ON a.gmacct = b.fxgact
  AND b.fxconsol = ''
  AND b.fxcyy = 2012
WHERE a.gmyear = 2012
  AND a.gmstyp = 'a'
ORDER BY gmdept, gmacct  



SELECT * FROM #wtf WHERE gmfsDepartmentLevel1 IS NOT NULL
SELECT gmfsaccountlevel1, gmfsaccountlevel2, gmfsdepartmentlevel1, gmfsdepartmentlevel2
FROM #wtf
--WHERE gmfsAccountLevel1 NOT IN ('Assets','Liabilities', 'Owner Equity')
GROUP BY gmfsaccountlevel1, gmfsaccountlevel2, gmfsdepartmentlevel1, gmfsdepartmentlevel2

-- page 2 store totals L1, 2, 57
-- right ON for july/august
-- except for assets/liabilities, NOT going there
SELECT 
  SUM(CASE WHEN gmfsAccountLevel1 = 'Sales' THEN gttamt END) AS "Net Sales",
  SUM(CASE WHEN gmfsAccountLevel1 IN ('Sales', 'COGS') THEN gttamt END) AS "Gross Profit",
  SUM(CASE WHEN gmfsAccountLevel1 = 'Expenses' THEN gttamt END) AS "Total Expenses",
  SUM(CASE WHEN gmfsAccountLevel1 IN ('Additions to Income','Deductions from Income') THEN  gttamt END) AS "Net ADD & Ded",
  SUM(gttamt) AS "Net Profit"
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
  AND gmfsAccountLevel1 NOT IN ('Assets','Liabilities')


-- page 2 var/fixed
-- so, WHERE i am balking IS assignment of expenses to departments
-- DO it based ON gmdept
-- sales, cogs & exp accounts need departments so that i can roll up
-- store
--      variable :: fixed
--      new :: used :: f/i 
SELECT gmfsDepartmentLevel1, gmfsAccountLevel1, SUM(gttamt)
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
  AND gmfsAccountLevel1 IN ('Sales','COGS', 'Expenses')
--  AND gmfsDepartmentLevel1 IN ('Fixed Operations','Variable')
GROUP BY gmfsDepartmentLevel1, gmfsAccountLevel1

SELECT gmfsDepartmentLevel1, gmfsAccountLevel1, SUM(gttamt)
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
  AND gmfsAccountLevel1 IN ('Sales','COGS')
  AND gmfsDepartmentLevel1 IN ('Fixed Operations','Variable')
GROUP BY gmfsDepartmentLevel1, gmfsAccountLevel1

SELECT gmfsDepartmentLevel1,gmdept, 
  SUM(CASE WHEN gmfsAccountLevel1 = 'Sales' THEN gttamt END) AS "Net Sales",
  SUM(CASE WHEN gmfsAccountLevel1 IN ('Sales', 'COGS') THEN gttamt END) AS "Gross Profit",
  SUM(CASE WHEN gmfsAccountLevel1 = 'Expenses' THEN gttamt END) AS "Total Expenses",
  SUM(CASE WHEN gmfsAccountLevel1 IN ('Additions to Income','Deductions from Income') THEN  gttamt END) AS "Net ADD & Ded",
  SUM(gttamt) AS "Net Profit"
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
  AND gmfsAccountLevel1 NOT IN ('Assets','Liabilities')
GROUP BY gmfsDepartmentLevel1,gmdept  


SELECT gmfsDepartmentLevel1, gmfsDepartmentLevel2, gmfsAccountLevel1, SUM(gttamt)
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
GROUP BY gmfsDepartmentLevel1, gmfsDepartmentLevel2, gmfsAccountLevel1 
-- sales & COGS
-- uc ok
-- nc ok Page 7 line 72, 73 ???
-- fi ok
-- bs way hi fucking parts split
-- mech way hi
-- parts way lo
/**************************************  SALES  *******************************/
-- bs
  SELECT fxfact, gmfsaccountlevel1, SUM(gttamt)
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
    WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gmfsDepartmentLevel2 = 'Body Shop'
  GROUP BY fxfact, gmfsaccountlevel1
  
/**************************************  SALES  *******************************/





/************************************* EXPENSES *******************************/
SELECT * FROM #wtf WHERE gmacct LIKE '16354%'
-- bs expenses ok  

-- nc semi-fixed IS high --
-- fixed
  SELECT fxfact, gmfsAccountlevel2, SUM(gttamt)
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gmdept = 'NC'
    AND gmfsAccountlevel2 = 'Semi-Fixed Expenses'
  GROUP BY fxfact, gmfsAccountlevel2  
  ORDER BY fxfact  
  
  063a low
  
  SELECT *
  FROM #wtf
  WHERE fxfact = '063A'
    AND gmdept = 'nc'
    
  SELECT COUNT(*), SUM(gttamt)
  FROM stgArkonaGLPTRNS
  WHERE gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gtacct IN ('16301A', '16301B', '16354', '16354a','16354b') 
  the problem was the routing of 16354 a & b, they are national AND should be routed
  to 063C which Jeri has done  
-- nc semi-fixed IS high --

-- uc expenses
  -- semi off BY 795

  SELECT fxfact, SUM(gttamt)
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gmdept = 'uc'
    AND level2 = 'Semi-Fixed Expenses'
  GROUP BY fxfact  
  ORDER BY fxfact  
  
  problem IS acct 060b
  SELECT *
  FROM #wtf
  WHERE fxfact = '060b'
    AND gmdept = 'uc' 
   
   SELECT gtacct, SUM(gttamt)
   FROM stgArkonaGLPTRNS
   WHERE gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
     AND gtacct IN ('16024','16002','16012')
   GROUP BY gtacct
   
   16024 (detail acct) routed to used cars
   emailed jeri
-- uc expenses

-- pd expenses
  -- personnel exp off BY 516
  SELECT fxfact, gtacct, SUM(gttamt)
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gmdept = 'pd'
    AND level2 = 'Personnel Expenses'
  GROUP BY fxfact, gtacct  
  ORDER BY fxfact   
  
  the diff IS 025F/12506 Taxes-Payroll 
  
  SELECT gtacct, COUNT(*), SUM(gttamt)
  FROM stgArkonaGLPTRNS
  WHERE gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gtacct IN ('12506','12526')
  GROUP BY gtacct
   
  SELECT *
  FROM stgArkonaGLPTRNS
  WHERE gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gtacct = '12526' 
   
  SELECT *
  FROM stgarkonaglpmast   
  WHERE gmdesc LIKE '%sdc%'
    AND gmyear = 2012
  
  SELECT gtacct, SUM(gttamt)
  FROM stgArkonaGLPTRNS
  WHERE gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gtacct IN('12326','12526','12726','12744','12926')  
  GROUP BY gtacct
  
  suspect 12506 should not be routed to 025F, sent jeri an email
-- pd expenses  

-- lr expenses ok (taking INTO account the semi-fixed correction FROM nc)

-- service expenses ok (taking INTO account the semi-fixed correction FROM yc)
  SELECT 
    SUM(CASE WHEN b.level1 = 'Sales' THEN gttamt END) AS Sales,
    SUM(CASE WHEN b.level1 = 'COGS' THEN gttamt END) AS COGS,
    SUM(CASE WHEN b.level1 = 'Expenses' THEN gttamt END) AS Expenses,
    SUM(CASE WHEN b.level2 = 'Variable Selling Expenses' THEN gttamt END) AS "Variable Selling Expenses",
    SUM(CASE WHEN b.level2 = 'Personnel Expenses' THEN gttamt END) AS "Personnel Expenses", 
    SUM(CASE WHEN b.level2 = 'Semi-Fixed Expenses' THEN gttamt END) AS "Semi-Fixed Expenses",
    SUM(CASE WHEN b.level2 = 'Fixed Expenses' THEN gttamt END) AS "Fixed Expenses",
    SUM(CASE WHEN b.level2 = 'Adjustment Expenses' THEN gttamt END) AS "Adjustment Expenses"
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'
    AND gmdept IN ('CW','QL','RE','SD')
    
/************************************* EXPENSES *******************************/    

/**************************************  SALES  *******************************/

  SELECT fxfact, gmacct, SUM(gttamt*split), SUM(gttamt)
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gmdept = 'pd'
    AND level1 = 'Sales'
  GROUP BY fxfact, gmacct  
  ORDER BY fxfact 
  
-- bs off BY 31059 - think this IS the % split
  
  SELECT * FROM #wtf WHERE split <> 1
  SELECT * FROM #wtf WHERE fxfact LIKE '477%'
  
SELECT *
FROM #wtf
WHERE gmdept = 'bs'
  AND level1 = 'Sales'

DROP TABLE #wtf  
SELECT a.gmacct, a.gmtype, a.gmdesc, a.gmdept, b.fxfact, fxfp01 AS Split,
  CASE
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 100 AND 296 THEN 'Assets'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 300 AND 359 THEN 'Liabilities'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 360 AND 399 THEN 'Owner Equity'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 599 THEN 'Sales'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 806 AND 811 THEN 'Sales'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 600 AND 744 THEN 'COGS'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 850 AND 861 THEN 'Sales'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 902 AND 910 THEN 'Additions to Income'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 952 AND 955 THEN 'Deductions from Income'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 11 AND 99 THEN 'Expenses' 
  END AS Level1,  
  CASE 
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 11 AND 15 THEN 'Variable Selling Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 20 AND 29 THEN 'Personnel Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 33 AND 79 THEN 'Semi-Fixed Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 80 AND 92 THEN 'Fixed Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 97 AND 99 THEN 'Adjustment Expenses'
  END AS Level2
INTO #wtf  
FROM stgArkonaGLPMAST a
LEFT JOIN stgArkonaffpxrefdta b ON a.gmacct = b.fxgact
  AND b.fxconsol = ''
  AND b.fxcyy = 2012
WHERE a.gmyear = 2012
  AND a.gmstyp = 'a'
ORDER BY gmdept, gmacct  

SELECT *
FROM stgArkonaffpxrefdta
WHERE fxcyy = 2012
  AND fxconsol = ''
  AND fxfact LIKE '47%'
  
SELECT *
FROM #wtf WHERE gmdept = 'uc' 
  
-- bs   
-- pd
-- nc
-- uc
  SELECT SUM(gttamt)
  --
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gmfsdepartmentlevel2 = 'Used Vehicle Department'
    AND gmfsaccountlevel1 = 'COGS'
  GROUP BY fxfact, gmacct, gmdesc  
  ORDER BY fxfact  
-- sd
-- lr
/**************************************  SALES  *******************************/

/**************************************  COGS  *******************************/
-- bs 
 
-- pd
-- nc
-- uc
-- sd
-- lr
/**************************************  COGS  *******************************/
  
--10/11  
CREATE TABLE resGMCOA
