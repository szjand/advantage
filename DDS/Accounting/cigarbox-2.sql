-- generic heirarchy TABLE
DROP TABLE zcb;
CREATE TABLE zCB (
  Name cichar(20),
  StoreCode cichar(3),
  dl1 cichar(24),
  dl2 cichar(24),
  dl3 cichar(24),
  al1 cichar(24),
  al2 cichar(24),
  al3 cichar(24),
  al4 cichar(24),
  glAccount cichar(10),
  split double(2)) IN database;
  
/* Body Shop *****************************************************************/  
INSERT INTO zCB
SELECT 'Cigar Box', 'RY1', a.*, b.*, CAST(NULL AS sql_varchar) AS glAccount, 1
FROM (
  SELECT cbdeptl1, cbdeptl2, cbdeptl3
  FROM dimaccount
  WHERE cbdeptl1 <> 'NA'
  GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
(
  SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
  FROM dimaccount
  WHERE cbacctl1 <> 'NA'
  GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b  
WHERE cbdeptl2 = 'Body Shop';  

-- al4 <> na
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy';
-- semi-fixed
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';
-- fixed  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na';
-- personnel
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na';
-- sales
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
-- parts split  
INSERT INTO zcb
SELECT distinct a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '147700', .5
FROM zcb a
WHERE dl2 = 'Body Shop'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
-- COGS  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Body Shop' and gldepartment = 'bs' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Body Shop'
  AND al3 = 'COGS'
  AND al4 = 'na';
-- parts split  
INSERT INTO zcb
SELECT distinct a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '167700', .5
FROM zcb a
WHERE dl2 = 'Body Shop'
  AND al3 = 'COGS'
  AND al4 = 'na';   
/*                 body shop back to june, numbers are good                  */               
-- net profit
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
GROUP BY yearmonth, dl2, al1  
-- gross / expenses
SELECT yearmonth, dl2, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
GROUP BY yearmonth, dl2, al2
-- gross / expenses
SELECT dl2, al2, al3, SUM(b.gttamt*split)
FROM zcb a
left JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
GROUP BY dl2, al2, al3
/* body shop *****************************************************************/

/* Service *******************************************************************/  
-- delete FROM zcb WHERE dl2 = 'Service';
INSERT INTO zCB
SELECT 'Cigar Box', 'RY1', a.*, b.*, CAST(NULL AS sql_varchar) AS glAccount, 1
FROM (
  SELECT cbdeptl1, cbdeptl2, cbdeptl3
  FROM dimaccount
  WHERE cbdeptl1 <> 'NA'
  GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
(
  SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
  FROM dimaccount
  WHERE cbacctl1 <> 'NA'
  GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b  
WHERE cbdeptl2 = 'Service';  

-- al4 <> na
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Service' and cbdeptl3 = 'Detail' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy';
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Service' and cbdeptl3 = 'Car Wash' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy'; 
INSERT INTO zcb   
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Service' and cbdeptl3 = 'PDQ' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy'; 
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Service' and cbdeptl3 = 'Main Shop' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy'; 
-- semi-fixed 
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Detail' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Car Wash' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb   
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'PDQ' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Main Shop' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';  
-- fixed
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Detail' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na';
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Car Wash' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb   
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'PDQ' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Main Shop' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na';   
-- personnel  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Detail' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na';
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Car Wash' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb   
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'PDQ' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na'; 
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Main Shop' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na';  
-- sales  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Detail' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'Sales'
  AND al4 = 'na';
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Car Wash' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
INSERT INTO zcb   
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'PDQ' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Main Shop' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
-- parts split  
INSERT INTO zcb
SELECT distinct a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '146700', .5
FROM zcb a
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Sales'
  AND al4 = 'na';    
INSERT INTO zcb
SELECT distinct a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '146800', .5
FROM zcb a
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'Sales'
  AND al4 = 'na';   
INSERT INTO zcb
SELECT distinct a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '147800', .5
FROM zcb a
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'Sales'
  AND al4 = 'na';   
  
-- cogs
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Detail' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Detail'
  AND al3 = 'COGS'
  AND al4 = 'na';
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Car Wash' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Car Wash'
  AND al3 = 'COGS'
  AND al4 = 'na'; 
INSERT INTO zcb   
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'PDQ' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'COGS'
  AND al4 = 'na'; 
INSERT INTO zcb  
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl3 = 'Main Shop' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'COGS'
  AND al4 = 'na';  
-- parts split  
INSERT INTO zcb
SELECT distinct a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '166700', .5
FROM zcb a
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'COGS'
  AND al4 = 'na';    
INSERT INTO zcb
SELECT distinct a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '166800', .5
FROM zcb a
WHERE dl2 = 'Service'
  AND dl3 = 'Main Shop'
  AND al3 = 'COGS'
  AND al4 = 'na';   
INSERT INTO zcb
SELECT distinct a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '167800', .5
FROM zcb a
WHERE dl2 = 'Service'
  AND dl3 = 'PDQ'
  AND al3 = 'COGS'
  AND al4 = 'na';         
/* fixes */
UPDATE zcb
  SET dl3 = 'Detail'
WHERE glaccount = '146302';
    
/*                 service back to june                 */               
-- net profit 
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'service'
GROUP BY yearmonth, dl2, al1  

-- gross / expenses -- good enuf at Total Service level
-- the only discrepances are IN expenses, ~ 0.2% max
SELECT yearmonth, dl2, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'service'
GROUP BY yearmonth, dl2, al2

-- 11/18
-- fuck, did i NOT check the service subdivisions ?
-- yes, i did 12324 was IN carwash doc, no longer is
SELECT yearmonth, dl2, dl3,al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth = 201209
  AND c.dl2 = 'service'
  AND c.al2 = 'total expenses'
GROUP BY yearmonth, dl2, dl3, al3

-- good enuf
        
/* Service *******************************************************************/

/* NCR ***********************************************************************/

INSERT INTO zCB
SELECT 'Cigar Box', 'RY1', a.*, b.*, CAST(NULL AS sql_varchar) AS glAccount, 1
FROM (
  SELECT cbdeptl1, cbdeptl2, cbdeptl3
  FROM dimaccount
  WHERE cbdeptl1 <> 'NA'
  GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
(
  SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
  FROM dimaccount
  WHERE cbacctl1 <> 'NA'
  GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b  
WHERE cbdeptl2 = 'NCR';  

-- policy IS IN variable expense al4
-- semi-fixed
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'NCR' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'NCR'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';
-- fixed  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'NCR' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'NCR'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na';
-- personnel
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'NCR' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'NCR'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na';
-- variable  policy
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'NCR' AND cbacctl3 = 'Variable Expenses' AND cbacctl4 = 'Policy') b
WHERE dl2 = 'NCR'
  AND al3 = 'Variable Expenses'
  AND al4 = 'Policy';
-- variable  delivery
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'NCR' AND cbacctl3 = 'Variable Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'NCR'
  AND al3 = 'Variable Expenses'
  AND al4 = 'na';  
  
-- sales
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'NCR' AND cbacctl3 = 'Sales') b
WHERE dl2 = 'NCR'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
-- COGS  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'NCR' AND cbacctl3 = 'COGS') b
WHERE dl2 = 'NCR'
  AND al3 = 'COGS'
  AND al4 = 'na';  
  
/* proof    */
-- net profit , ehh, so so, october IS right on
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'ncr'
GROUP BY yearmonth, dl2, al1    

-- fuck, cb/doc DO NOT agree, AND zcb IS a 3rd value
-- diff BETWEEN doc AND zcb IS the routing error IF found earlier, subsequently fixed BY jeri
-- good enough
SELECT yearmonth, dl2, al1, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'ncr'
GROUP BY yearmonth, dl2, al1, al2  

/* NCR ***********************************************************************/

/* Parts ***********************************************************************/

INSERT INTO zCB
SELECT 'Cigar Box', 'RY1', a.*, b.*, CAST(NULL AS sql_varchar) AS glAccount, 1
FROM (
  SELECT cbdeptl1, cbdeptl2, cbdeptl3
  FROM dimaccount
  WHERE cbdeptl1 <> 'NA'
  GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
(
  SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
  FROM dimaccount
  WHERE cbacctl1 <> 'NA'
  GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b  
WHERE cbdeptl2 = 'Parts';  

-- al4 <> na
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Parts' and gldepartment = 'PD' AND cbacctl4 <> 'na') b
WHERE dl2 = 'Parts'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'Policy';
-- semi-fixed
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Parts' and gldepartment = 'PD' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Parts'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';
-- fixed  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Parts' and gldepartment = 'pd' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'parts'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na';
-- personnel
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'parts' and gldepartment = 'pd' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'parts'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na';
-- sales
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'parts' and gldepartment = 'pd' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'parts'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
-- COGS  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'parts' and gldepartment = 'pd' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'parts'
  AND al3 = 'COGS'
  AND al4 = 'na';
-- parts split
UPDATE zcb
SET split = .5
WHERE dl2 = 'parts'
  AND glaccount IN ('146700','146800','147700','147800','166700','166800','167700','167800');   
  
/* proof    */
-- net profit pretty fucking CLOSE
-- ~ 0.2% variance IN expenses
-- assume the difference IS BETWEEN accrual (WHEN the cb IS created) AND actual payroll
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'parts'
GROUP BY yearmonth, dl2, al1      


/* Parts ***********************************************************************/

/* Sales ***********************************************************************/
DELETE FROM zcb WHERE dl2 = 'variable';
INSERT INTO zCB
SELECT 'Cigar Box', 'RY1', a.*, b.*, CAST(NULL AS sql_varchar) AS glAccount, 1
FROM (
  SELECT cbdeptl1, cbdeptl2, cbdeptl3
  FROM dimaccount
  WHERE cbdeptl1 <> 'NA'
  GROUP BY cbdeptl1, cbdeptl2, cbdeptl3) a, 
(
  SELECT cbacctl1, cbacctl2, cbacctl3, cbacctl4 
  FROM dimaccount
  WHERE cbacctl1 <> 'NA'
  GROUP BY cbacctl1, cbacctl2, cbacctl3, cbacctl4) b  
WHERE cbdeptl2 = 'Variable';  

-- policy IS IN variable expense al4
-- semi-fixed
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'New Vehicles' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'New Vehicles'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'Used Vehicles' AND cbacctl3 = 'Semi-Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'Semi-Fixed Expenses'
  AND al4 = 'na';  
-- fixed  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'New Vehicles' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'New Vehicles'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na';
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'Used Vehicles' AND cbacctl3 = 'Fixed Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'Fixed Expenses'
  AND al4 = 'na';
-- personnel
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'New Vehicles' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'New Vehicles'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na';
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'Used Vehicles' AND cbacctl3 = 'Personnel Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'Personnel Expenses'
  AND al4 = 'na';
-- variable  policy
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'New Vehicles' AND cbacctl3 = 'Variable Expenses' AND cbacctl4 = 'Policy') b
WHERE dl2 = 'Variable'
  AND dl3 = 'New Vehicles'
  AND al3 = 'Variable Expenses'
  AND al4 = 'Policy';
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'Used Vehicles' AND cbacctl3 = 'Variable Expenses' AND cbacctl4 = 'Policy') b
WHERE dl2 = 'Variable'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'Variable Expenses'
  AND al4 = 'Policy';

-- variable  non policy
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'New Vehicles' AND cbacctl3 = 'Variable Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'New Vehicles'
  AND al3 = 'Variable Expenses'
  AND al4 = 'na';
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'Variable' AND cbdeptl3 = 'Used Vehicles' AND cbacctl3 = 'Variable Expenses' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'Variable Expenses'
  AND al4 = 'na'; 

-- sales
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'variable' and cbdeptl3 = 'New Vehicles' and gldepartment = 'NC' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'New Vehicles'
  AND al3 = 'Sales'
  AND al4 = 'na'; 
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'variable' and cbdeptl3 = 'Used Vehicles' and gldepartment = 'UC' AND cbacctl3 = 'Sales' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'Sales'
  AND al4 = 'na';   
-- COGS  
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'variable' and gldepartment = 'NC' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'New Vehicles'
  AND al3 = 'COGS'
  AND al4 = 'na'; 
INSERT INTO zcb
SELECT a.name, 'RY1', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, b.glAccount, 1
FROM zcb a,
(SELECT * FROM dimaccount WHERE cbdeptl2 = 'variable' and gldepartment = 'UC' AND cbacctl3 = 'COGS' AND cbacctl4 = 'na') b
WHERE dl2 = 'Variable'
  AND dl3 = 'Used Vehicles'
  AND al3 = 'COGS'
  AND al4 = 'na';   
  
/* proof    */
--  CLOSE, but NOT CLOSE enuf
           zcb     cb
june       92671   92075
july       30398   28574
aug        39060   37127
sep        73380   73364
oct        89189   90336
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'variable'
GROUP BY yearmonth, dl2, al1      
/* Sales ***********************************************************************/