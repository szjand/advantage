CREATE TABLE tmpDocs ( 
      lft integer,
      rgt integer,
      ReportNumber Integer,
      Title CIChar( 30 ),
      Description CIChar( 30 ),
      glAccount CIChar( 10 ),
      glCostAccount CIChar( 10 ),
      glAccountType CIChar( 1 ),
      glDepartment CIChar( 2 ),
      l1 Integer,
      l2 Integer,
      l3 Integer,
      l4 Integer,
      l5 Integer,
      l6 Integer,
      l7 Integer,
      l8 Integer,
      l9 Integer,
      l10 Integer,
      l11 Integer,
      l12 Integer) IN DATABASE;
      
INSERT INTO tmpDocs      
SELECT a.g1rpt#, a.g1titl, b.g2desc, c.g3acct, 
  d.glCostAccount, d.glAccountType, d.glDepartment,
  g2lky1, g2lky2, g2lky3, g2lky4, g2lky5,
  g2lky6, g2lky7, g2lky8, g2lky9, g2lky10, g2lky11, g2lky12
FROM stgArkonaglpfard a 
LEFT JOIN stgArkonaGLPFAFD b ON a.g1rpt# = b.g2rpt#
LEFT JOIN (
  SELECT g3rpt#, g3acct, 
  cast(LEFT(g3lkey, 3) AS sql_integer) AS level1,
  cast(substring(g3lkey,4,3) AS sql_integer) AS level2,
  cast(substring(g3lkey,7,3) AS sql_integer) AS level3,
  cast(substring(g3lkey,10,3) AS sql_integer) AS level4,
  cast(substring(g3lkey,13,3) AS sql_integer) AS level5,
  cast(substring(g3lkey,16,3) AS sql_integer) AS level6,
  cast(substring(g3lkey,19,3) AS sql_integer) AS level7,
  cast(substring(g3lkey,22,3) AS sql_integer) AS level8,
  cast(substring(g3lkey,25,3) AS sql_integer) AS level9,
  cast(substring(g3lkey,28,3) AS sql_integer) AS level10,
  cast(substring(g3lkey,31,3) AS sql_integer) AS level11,
  cast(substring(g3lkey,34,3) AS sql_integer) AS level12
  FROM stgArkonaGLPFAAA) c ON a.g1rpt# = c.g3rpt#
    AND c.level1 = b.g2lky1  
    AND c.level2 = b.g2lky2 
    AND c.level3 = b.g2lky3 
    AND c.level4 = b.g2lky4
    AND c.level5 = b.g2lky5 
    AND c.level6 = b.g2lky6 
    AND c.level7 = b.g2lky7 
    AND c.level8 = b.g2lky8 
    AND c.level9 = b.g2lky9 
    AND c.level10 = b.g2lky10 
    AND c.level11 = b.g2lky11
    AND c.level12 = b.g2lky12
LEFT JOIN dimAccount d ON g3acct = d.glAccount;   


SELECT *
FROM tmpDocs a
LEFT JOIN (
  SELECT gtacct, SUM(gttamt)
  FROM stgArkonaGLPTRNS
  WHERE gtdate BETWEEN '08/01/2012' AND '08/31/2012'
  GROUP BY gtacct) b ON a.glAccount = b.gtacct 
WHERE a.reportnumber = 66
   
-- these are ALL the accounts used IN PDQ DOC 
SELECT glAccount 
FROM tmpDocs a
WHERE glAccount IS NOT NULL
  AND reportnumber = 66
UNION 
SELECT a.glCostAccount 
FROM dimAccount a
INNER JOIN tmpDocs b ON a.glAccount = b.glAccount
WHERE b.glAccount IS NOT NULL
  AND a.glCostAccount IS NOT NULL
  AND b.reportnumber = 66
  
 
SELECT *
FROM dimAccount a
INNER JOIN (7
  SELECT glAccount
  FROM tmpDocs a
  WHERE glAccount IS NOT NULL
    AND reportnumber = 66
  UNION 
  SELECT a.glCostAccount 
  FROM dimAccount a
  INNER JOIN tmpDocs b ON a.glAccount = b.glAccount
  WHERE b.glAccount IS NOT NULL
    AND a.glCostAccount IS NOT NULL
    AND b.reportnumber = 66) b ON a.glAccount = b.glAccount
 LEFT JOIN (
   SELECT gtacct, SUM(gttamt) AS amount
  FROM stgArkonaGLPTRNS
  WHERE gtdate BETWEEN '08/01/2012' AND '08/31/2012'
  GROUP BY gtacct) c ON a.glAccount = c.gtacct

-- sales & COGS ok IF i look just at gmdept QL
-- but expenses are off
SELECT glDepartment, glAccountType, SUM(amount)
--SELECT SUM(amount)
FROM (
  SELECT *
  FROM dimAccount a
  INNER JOIN (
    SELECT glAccount, Description
    FROM tmpDocs a
    WHERE glAccount IS NOT NULL
      AND reportnumber = 66
    UNION 
    SELECT a.glCostAccount, 'COGS' 
    FROM dimAccount a
    INNER JOIN tmpDocs b ON a.glAccount = b.glAccount
    WHERE b.glAccount IS NOT NULL
      AND a.glCostAccount IS NOT NULL
      AND b.reportnumber = 66) b ON a.glAccount = b.glAccount
   LEFT JOIN (
     SELECT gtacct, SUM(gttamt) AS amount
    FROM stgArkonaGLPTRNS
    WHERE gtdate BETWEEN '08/01/2012' AND '08/31/2012'
    GROUP BY gtacct) c ON a.glAccount = c.gtacct) z
WHERE glDepartment = 'QL'    
GROUP BY glDepartment, glAccountType
  

-- expenses
-- seems LIKE it IS the fucking adusted cost of labor that IS fucking it up
SELECT glDepartment, glAccountType, SUM(amount), gmfsAccountLevel2, glAccount
--SELECT SUM(amount)
FROM (
  SELECT *
  FROM dimAccount a
  INNER JOIN (
    SELECT glAccount, Description
    FROM tmpDocs a
    WHERE glAccount IS NOT NULL
      AND reportnumber = 66
    UNION 
    SELECT a.glCostAccount, 'COGS' 
    FROM dimAccount a
    INNER JOIN tmpDocs b ON a.glAccount = b.glAccount
    WHERE b.glAccount IS NOT NULL
      AND a.glCostAccount IS NOT NULL
      AND reportnumber = 66) b ON a.glAccount = b.glAccount
  LEFT JOIN (
    SELECT gtacct, SUM(gttamt) AS amount
    FROM stgArkonaGLPTRNS
    WHERE gtdate BETWEEN '08/01/2012' AND '08/31/2012'
    GROUP BY gtacct) c ON a.glAccount = c.gtacct) z
WHERE glDepartment = 'QL' 
  AND glAccountType = '8'   
GROUP BY glDepartment, glAccountType, gmfsAccountLevel2, glAccount

SELECT * FROM tmpDocs

-- nope, this IS NOT it
SELECT 
  MAX(
  CASE 
    WHEN glAccount IS NULL THEN Description
    ELSE glAccount
  END) , min(l1) AS lower, 
  CASE  
    WHEN max(l2) = 999 THEN l2
    WHEN MAX(l3) = 999 THEN l2
    WHEN MAX(l4) = 999 THEN l3
    WHEN MAX(l5) = 999 THEN l4
    WHEN MAX(l6) = 999 THEN l5
  END AS upper 
FROM tmpDocs
WHERE reportnumber = 66
GROUP BY Description, l1,l2,l3,l4,l5

-- ALL total/subtotal lines
SELECT Description, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12
FROM tmpDocs
WHERE reportnumber = 66
  AND glAccount IS NULL 
GROUP BY Description, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12
--ORDER BY l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12
union
-- ALL account/detail lines
SELECT glAccount, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12
FROM tmpDocs
WHERE reportnumber = 66
  AND glAccount IS NOT NULL 
GROUP BY glAccount, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12

SELECT *
from tmpDocs
WHERE reportnumber = 66
ORDER BY l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12

ALTER TABLE tmpDocs
ADD COLUMN lft integer
ADD COLUMN rgt integer

-- ok, this will WORK IF i manually DO it, how to automate it
UPDATE tmpDocs
  SET lft = 1,
      rgt = 24
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 999;
-- 10/19 fuck it, just go ahead AND DO one manually
UPDATE tmpDocs
  SET lft = 2,
      rgt = 15
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 10
  AND l3 = 999;
UPDATE tmpDocs
  SET lft = 3,
      rgt = 10
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 10
  AND l3 = 10
  AND l4 = 999; 
-- WHEN i get to the leaf (no children) categoriztion, that IS the list of relevant accounts
-- no need to limit to level 5 to 999, because i want to include the header with the details   
UPDATE tmpDocs
  SET lft = 4,
      rgt = 5
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 10
  AND l3 = 10
  AND l4 = 10;    
UPDATE tmpDocs
  SET lft = 6,
      rgt = 7
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 10
  AND l3 = 10
  AND l4 = 20;   
UPDATE tmpDocs
  SET lft = 8,
      rgt = 9
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 10
  AND l3 = 10
  AND l4 = 30;    
UPDATE tmpDocs
  SET lft = 11,
      rgt = 14
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 10
  AND l3 = 20
  AND l4 = 999;  
UPDATE tmpDocs
  SET lft = 12,
      rgt = 13
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 10
  AND l3 = 20
  AND l4 = 10; 
UPDATE tmpDocs
  SET lft = 16,
      rgt = 23
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 20
  AND l3 = 999; 
UPDATE tmpDocs
  SET lft = 17,
      rgt = 18
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 20
  AND l3 = 10;      
UPDATE tmpDocs
  SET lft = 19,
      rgt = 20
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 20
  AND l3 = 20;  
UPDATE tmpDocs
  SET lft = 20,
      rgt = 21
-- SELECT * FROM tmpDocs      
WHERE reportnumber = 66
  AND l1 = 10
  AND l2 = 20
  AND l3 = 30;    

SELECT *--Description, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12
FROM tmpDocs
WHERE reportnumber = 66
  AND glAccount IS NULL 
  ORDER BY lft
-- expenses: right on    
SELECT yearmonth, SUM(gttamt)
FROM dimAccount a
INNER JOIN tmpdocs b ON a.glaccount = b.glaccount
  AND reportnumber = 66
  AND lft BETWEEN 16 AND 23
LEFT JOIN stgArkonaGLPTRNS c ON b.glaccount = c.gtacct
--  AND gtdate BETWEEN '08/01/2012' AND '08/31/2012'
INNER JOIN day d ON c.gtdate = d.thedate
  AND yearmonth BETWEEN 201205 AND 201209
GROUP BY yearmonth  
-- gross
SELECT yearmonth, a.glaccount, SUM(gttamt)
FROM dimAccount a
INNER JOIN tmpdocs b ON a.glaccount = b.glaccount
  AND reportnumber = 66
  AND lft BETWEEN 2 AND 15
LEFT JOIN stgArkonaGLPTRNS c ON b.glaccount = c.gtacct
--  AND gtdate BETWEEN '08/01/2012' AND '08/31/2012'
INNER JOIN day d ON c.gtdate = d.thedate
  AND yearmonth BETWEEN 201205 AND 201209
GROUP BY yearmonth, a.glaccount
    
    
SELECT *
FROM dimaccount  
WHERE glaccount IN ('146002','166002','166503')  
-- 10/21 figure out what the fuck IS up with gross  
-- still want to see account discrepancy relative to categorizing pdq BY glDepartment

-- 166503 categorized AS gmfsAccountLevel1 = COGS, should it be an Expense account
-- IF cogs then -1(amt)
-- IF expense THEN amt
-- ????
the immediate goal IS to assume the doc IS correct AND matching it
SELECT * 
FROM tmpdocs
WHERE reportnumber = 66 ORDER BY lft,rgt

-- 103208  doc: 94619
SELECT SUM(gttamt)
FROM dimAccount a
INNER JOIN tmpdocs b ON a.glaccount = b.glaccount
  AND reportnumber = 66
  AND lft BETWEEN 3 AND 10
LEFT JOIN stgArkonaGLPTRNS c ON b.glaccount = c.gtacct
  AND gtdate BETWEEN '08/01/2012' AND '08/31/2012'
  
-- the difference IS adj cost of labor 
SELECT Description, a.glaccount, 
  SUM(CASE WHEN a.glAccountType = '8' THEN -1*gttamt ELSE gttamt END )
FROM dimAccount a
INNER JOIN tmpdocs b ON a.glaccount = b.glaccount
  AND reportnumber = 66
  AND lft BETWEEN 3 AND 10
LEFT JOIN stgArkonaGLPTRNS c ON b.glaccount = c.gtacct
  AND gtdate BETWEEN '08/01/2012' AND '08/31/2012'
GROUP BY description, a.glaccount   

SELECT SUM(CASE WHEN a.glAccountType = '8' THEN -1*gttamt ELSE gttamt END )
FROM dimAccount a
INNER JOIN tmpdocs b ON a.glaccount = b.glaccount
  AND reportnumber = 66
  AND lft BETWEEN 3 AND 10
LEFT JOIN stgArkonaGLPTRNS c ON b.glaccount = c.gtacct
  AND gtdate BETWEEN '08/01/2012' AND '08/31/2012'

-- this matches the doc total pdq labor sales
  
SELECT SUM(gttamt)
FROM dimAccount a
INNER JOIN tmpdocs b ON a.glaccount = b.glaccount
  AND reportnumber = 66
  AND lft BETWEEN 3 AND 10
LEFT JOIN stgArkonaGLPTRNS c ON b.glaccount = c.gtacct
  AND gtdate BETWEEN '08/01/2012' AND '08/31/2012' 
WHERE a.glaccounttype <> '8'   
-- major mind fucking ON the notion of a negative multiplier for an expense account
--   relative to a cogs account
SELECT *
FROM stgarkonaglpmast
WHERE gmstyp = 'a'
  AND gmyear = 2012
  AND gmacct IN ('166503','146002', '166002')

SELECT COUNT(DISTINCT ro)
SELECT COUNT(ro)
FROM factroline
 
-- will get back to sorting ALL the above out
-- shift focus FROM doc to cigarbox
-- WHERE does policy fit in 
-- for variable depts policy IS part of variable expense
-- for fixed depts policy IS part of semi expense

SELECT *
FROM (
SELECT 'PDQ DOC', glaccount
FROM tmpdocs
WHERE reportnumber = 66
  AND glaccount IS NOT NULL 
GROUP BY glaccount) a 
LEFT JOIN 
SELECT gldepartment, glaccount 
FROM dimAccount
WHERE gldepartment = 'QL' 
GROUP BY gldepartment, glaccount 

-- shit, this IS looking LIKE gldeparment should have worked for pdq doc
SELECT *
FROM (
  SELECT a.title, a.glaccount, b.glaccount, b.gldepartment
  FROM tmpdocs a
  full OUTER JOIN dimaccount b ON a.glaccount = b.glaccount) c
WHERE (title = 'RY1 PDQ' OR gldepartment = 'pdq')  
  AND (glaccount IS NOT NULL AND glaccount_1 IS NOT NULL)
  

SELECT *
FROM tmpdocs
WHERE reportnumber = 66  


  
SELECT lft, SUM(gttamt)
FROM dimAccount a
INNER JOIN tmpdocs b ON a.glaccount = b.glaccount
  AND reportnumber = 66
LEFT JOIN stgArkonaGLPTRNS c ON b.glaccount = c.gtacct
  AND gtdate BETWEEN '08/01/2012' AND '08/31/2012' 
GROUP BY lft
HAVING lft BETWEEN 16 AND 23   


SELECT * FROM dimaccount WHERE glaccount LIKE '146%'

glaccount     cbDepartment     cbDepartmentLFT     cbDepartmentRGT   cbAccountType cbAccountTypeLFT cbAccountTypeRGT
146036        PDQ              9                   10                Sales         3                4

-- the problem with the above, how DO i extract non-leaf totals/subtotals, got the sales accounts, 
-- what about the gross profit accounts 
-- how would i roll up pdq to service to store?

-- am looking at a different row for each account, fuck i hope NOT

-- nope, NOT this eg cbaccount levels make no grouping sense
-- instead of levels, just headers, anything IN the hierarchy that IS NOT a leaf
DROP TABLE hierarchies;
CREATE TABLE Hierarchies (
  hierarchy cichar(12),
  hierarchytype cichar(24),
  Description cichar(24),
  lft integer,
  rgt integer) IN database;

SELECT * FROM hierarchies  
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'Store', 1,24);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'Variable', 2,7);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'New', 3,4);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'Used', 5,6);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'Service', 8,17);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'PDQ', 9,10);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'Main Shop', 11,12);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'Car Wash', 13,14);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'Detail', 15,16);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'Parts', 18,19);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'Body Shop', 20,21);
INSERT INTO hierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'cbDepartment', 'NCR', 22,23);

IF i flatten it dimAccount HAVING hierarchy11, hierarchy2, ... how does one know
which GROUP of hierarchy columns to use?
account TABLE with a row for each account/hierarchy combination

-- 2 different hierarchies dept/account type
-- acount will be NULL for totals/subtotals lines ??
-- type will BY department/accounttype
DROP TABLE dimAccountHierarchies;
CREATE TABLE dimAccountHierarchies (
  hierarchy cichar(12),
  hierarchytype cichar(24),
  description cichar(36),
  lft integer,
  rgt integer,
  glAccount cichar(10)) IN database;
  
-- first the basic hierarchy without accounts
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'Store', 1,24);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'Variable', 2,7);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'New', 3,4);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'Used', 5,6);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'Service', 8,17);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'PDQ', 9,10);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'Main Shop', 11,12);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'Car Wash', 13,14);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'Detail', 15,16);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'Parts', 18,19);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'Body Shop', 20,21);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'department', 'NCR', 22,23);  
-- now some accounts
INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'department', 'New', 3,4, glAccount
FROM dimAccount
WHERE glDepartment = 'NC';

INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'department', 'Used', 5,6, glAccount
FROM dimAccount
WHERE glDepartment = 'UC';

INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'department', 'PDQ', 9,10, glAccount
FROM dimAccount
WHERE glDepartment = 'QL';

-- now ADD cigarbox/accounttypes

INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Net Profit', 1,30);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Gross Profit', 2,7);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Sales', 3,4);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'COGS', 5,6);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Total Expense', 8,29);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Total Variable + Personnel Expense', 9,20);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Variable Expense', 10,17);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Compensation Expense', 11,12);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Delivery Expense', 13,14);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Policy Expense', 15,16);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Personnel Expense', 18,19);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Fixed Expense', 21,22);  
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Semi-Fixed Expense', 23,28);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Other Semi-Fixed Expense', 24,25);
INSERT INTO dimAccountHierarchies(hierarchy, hierarchytype, description,lft,rgt)
values ('Cigar Box', 'accounttype', 'Policy Expense', 26,27);


  
--- hm, since i am "resigned" to each hierarchy being 2 part, THEN should each dept/accttype be a row?
-- we will see, forge ahead for now 

-- 10/22
-- it IS fucking hard to get back ON track with ALL the fucking distractions of a monday morning
-- one question IS whether dimAccountHierarchies should be separate hierarchy types of dept/account
-- OR NOT
-- duh seems LIKE the lft/rgt mandates separate hierarchy types
-- i want to be able to roll up ON the different headings

-- fuck, IS this one dept row for every fucking account that applies to the dept? maybe so
SELECT * FROM dimAccountHierarchies
WHERE hierarchy = 'Cigar Box'
-- shooting for something LIKE
SELECT *
FROM dimAccountHierarchies
WHERE hierarchy = 'Cigar Box'
  AND description = 'New'
  AND lft BETWEEN (
    SELECT lft
    FROM dimAccountHierarchies
    WHERE hierarchy = 'Cigar Box'
      AND hierarchytype = 'department'
      AND description = 'Variable'
      )  
    AND (  
    SELECT rgt
    FROM dimAccountHierarchies
    WHERE hierarchy = 'Cigar Box'
      AND hierarchytype = 'department'
      AND description = 'Variable')   
   
-- 10/22 back FROM lunch, greg says he wants to talk abt used cars this afternoon
-- IN the interrim   
-- have added the accts for department pdq
SELECT *
FROM dimAccountHierarchies a 
INNER JOIN dimAccountHierarchies b ON a.glaccount = b.glaccount
WHERE a.hierarchytype = 'department'
  AND b.hierarchytype = 'accounttype'
  

-- one row for every account relevant to that dept
SELECT *
FROM dimAccountHierarchies
WHERE hierarchytype = 'department'  

SELECT description, COUNT(*)
FROM dimAccountHierarchies
WHERE hierarchytype = 'department' 
GROUP BY description

-- need 1 row for every account relative to that accounttype
SELECT *
FROM dimAccountHierarchies
WHERE hierarchytype = 'accounttype' 

-- start with the leaves
-- variable expenses 10/17
-- compensation 11/12
first of ALL, variable expenses
SELECT * 
FROM dimaccount
WHERE gmfsAccountLevel1 = 'Expenses'
  AND LEFT(gmfsaccount, 3) = '011' --IN ('011','013','015')
ORDER BY glDepartment, glaccount

SELECT * FROM dimAccountHierarchies 

INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'accounttype', 'Compensation Expense', 11,12 , glAccount
FROM dimAccount
WHERE gmfsAccountLevel1 = 'Expenses'
  AND LEFT(gmfsaccount, 3) = '011'
  
INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'accounttype', 'Delivery Expense', 13,14, glAccount
FROM dimAccount
WHERE gmfsAccountLevel1 = 'Expenses'
  AND LEFT(gmfsaccount, 3) = '013'  
  
INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'accounttype', 'PolicyExpense', 15,16, glAccount
FROM dimAccount
WHERE gmfsAccountLevel1 = 'Expenses'
  AND LEFT(gmfsaccount, 3) = '015'  
  
INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'accounttype', 'Personnel Expense', 18,19, glAccount
FROM dimAccount
WHERE gmfsAccountLevel2 = 'Personnel Expenses'

INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'accounttype', 'Other Semi-Fixed Expense', 24,25, glAccount
FROM dimAccount
WHERE CAST(LEFT(gmfsaccount, 3) AS sql_integer) BETWEEN 33 AND 79 
  AND CAST(LEFT(gmfsaccount, 3) AS sql_integer) <> 67
  
INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'accounttype', 'Fixed Expense', 21,22, glAccount
FROM dimAccount
WHERE gmfsAccountLevel2 = 'Fixed Expenses'

INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'accounttype', 'Gross Profit', 2,7, glAccount
FROM dimAccount
WHERE gmfsAccountLevel1 IN ('Sales','COGS')

INSERT INTO dimAccountHierarchies
SELECT 'Cigar Box', 'accounttype', 'Sales', 3,4, glAccount
FROM dimAccount
WHERE gmfsAccountLevel1 = 'Sales'

-- shit shit shit, IF i DO NOT INSERT accounts for the roll up accounts how DO i get the subtotals
-- there IN comes the grouping BY lft/rgt?

-- shit, IS that it?
-- let us see what pdq looks LIKE

SELECT *
FROM dimAccountHierarchies a 
INNER JOIN dimAccountHierarchies b ON a.glaccount = b.glaccount
WHERE a.hierarchytype = 'department'
  AND a.description = 'PDQ'
  AND b.hierarchytype = 'accounttype'

  

    SELECT gtacct, SUM(gttamt) AS amount
    FROM stgArkonaGLPTRNS
    WHERE gtdate BETWEEN '08/01/2012' AND '08/31/2012'
    GROUP BY gtacct
   
   
  
SELECT b.description, SUM(gttamt)
FROM dimAccountHierarchies a 
INNER JOIN dimAccountHierarchies b ON a.glaccount = b.glaccount
LEFT JOIN stgArkonaGLPTRNS c ON a.glaccount = c.gtacct
  AND c.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.hierarchytype = 'department'
  AND a.description = 'PDQ'
  AND b.hierarchytype = 'accounttype'
GROUP BY  b.description    

SELECT lft, SUM(gttamt)
FROM (
  SELECT description, lft, rgt, glAccount
  FROM dimAccountHierarchies a
  WHERE hierarchytype = 'accounttype'
    AND EXISTS (
      SELECT 1
      FROM dimAccountHierarchies
      WHERE hierarchytype = 'department'
        AND description = 'PDQ'
        AND glAccount = a.glaccount) 
  UNION 
  SELECT description, lft, rgt, glAccount
  FROM dimAccountHierarchies   
  WHERE hierarchytype = 'accounttype'
    AND glaccount IS NULL) c    
LEFT JOIN stgArkonaGLPTRNS d ON c.glaccount = d.gtacct
  and d.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
GROUP BY lft 
  ha 

  
    
SELECT * FROM dimAccountHierarchies WHERE hierarchytype = 'accounttype' AND glaccount IS NULL 

  
  
SELECT gmfsaccountlevel1, gmfsaccountlevel2
FROM dimaccount
GROUP by gmfsaccountlevel1, gmfsaccountlevel2 
  
  
  

  
  
  
  

/*  
  Description2 cichar(24),
  lft2 integer,
  rgt2 integer,
  Description3 cichar(24),
  lft3 integer,
  rgt3 integer,
  Description4 cichar(24),
  lft4 integer,
  rgt4 integer,
  Description5 cichar(24),
  lft5 integer,
  rft5 integer,
  Description6 cichar(24),
  lft6 integer,
  rgt6 integer,
  Description7 cichar(24),
  lft7 integer,
  rgt7 integer,
  Description8 cichar(24),
  lft8 integer,
  rgt8 integer,
  Description9 cichar(24),
  lft9 integer,
  rgt9 integer,
  Description10 cichar(24),
  lft10 integer,
  rgt10 integer,  
  Description11 cichar(24),
  lft11 integer,
  rgt11 integer,                  
  Description12 cichar(24),
  lft12 integer,
  rgt12 integer,  
  Description13 cichar(24),
  lft13 integer,
  rgt13 integer,  
  Description14 cichar(24),
  lft14 integer,
  rgt14 integer) IN database;  
*/   
  
  
  
// ** trying to figure out how to UPDATE lft/rgt programatically  
-- first i need to know the maximum rgt value
SELECT l1,l2, COUNT(*)
FROM tmpDocs
WHERE reportnumber = 66
GROUP BY l1,l2

SELECT DISTINCT *
FROM (
  SELECT l1,l2,l3,l4, COUNT(*)
  FROM tmpDocs
  WHERE reportnumber = 66
    AND (l1=999 OR l2=999 OR l3=999 or l4=999)
  GROUP BY l1,l2,l3,l4) a
WHERE l2 <> 999
  AND l3 <> 999
  AND l4 <> 999
  AND expr <> 1
  
SELECT l1,l2,l3,l4, COUNT(*)
FROM tmpDocs
WHERE reportnumber = 66
  AND (l1=999 OR l2=999 OR l3=999 or l4=999)
GROUP BY l1,l2,l3,l4

SELECT l1,l2,l3,l4, COUNT(*)
FROM tmpDocs
WHERE reportnumber = 66
  AND (l1=999 OR l2=999 OR l3=999 or l4=999)
GROUP BY l1,l2,l3,l4

SELECT MIN(l2) AS minl2, MAX(l2) AS maxl2

SELECT DISTINCT l1, l2
FROM tmpDocs
WHERE reportnumber = 66
  AND l2 NOT IN (0,999)

SELECT DISTINCT l1,l2,l3
FROM tmpDocs
WHERE reportnumber = 66
  AND l3 NOT IN (0,999)

SELECT DISTINCT l1,l2,l3,l4
FROM tmpDocs
WHERE reportnumber = 66
  AND l4 NOT IN (0,999)  
  
-- 10/18
-- ALL total/subtotal lines
SELECT Description, glaccount, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12
FROM tmpDocs
WHERE reportnumber = 66
  AND glAccount IS NULL 
--ORDER BY l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12
union
-- ALL account/detail lines
SELECT Description, glAccount, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12
FROM tmpDocs
WHERE reportnumber = 66
  AND glAccount IS NOT NULL 
-- root level 1
SELECT * FROM tmpdocs WHERE reportnumber = 66  
  AND l2 = 999
-- level 2   
SELECT * FROM tmpdocs WHERE reportnumber = 66  
  AND l3 = 999
-- level 3   
SELECT * FROM tmpdocs WHERE reportnumber = 66  
  AND l4 = 999  
-- level 4   
SELECT * FROM tmpdocs WHERE reportnumber = 66  
  AND l5 = 999   
  AND glaccount IS NULL 
  
  
SELECT Description, l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12
FROM tmpDocs
WHERE reportnumber = 66
  AND glAccount IS NULL   
    
    
    
  

