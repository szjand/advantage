
-- cigar box
SELECT dl2, al3, SUM(b.gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.name = 'cigar box'
  AND a.al3 = 'personnel expenses'
  AND storecode = 'ry1'
GROUP BY dl2, al3  


SELECT dl2, dl3, al3, glaccount, sum(gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.name = 'cigar box'
  AND a.al3 = 'personnel expenses'
  AND storecode = 'ry1'
  AND a.dl2 = 'Variable'
GROUP BY dl2, dl3, al3, glaccount 
ORDER BY glaccount

-- gmfs expenses
SELECT dl4, al2, SUM(b.gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.name = 'gmfs expenses'
  AND a.al2 = 'personnel expenses'
  AND storecode = 'ry1'
GROUP BY dl4, al2  

SELECT dl4, dl5, al2, glaccount, sum(gttamt)
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.name = 'gmfs expenses'
  AND a.al2 = 'personnel expenses'
  AND storecode = 'ry1'
  AND dl4 = 'Sales'
GROUP BY dl4, dl5, al2, glaccount 
ORDER BY glaccount 

-- expenses IS correct, cigar box leaves out beaucoup (12211/12/14/16 AND others)
SELECT *
FROM dimaccount
WHERE (glaccount LIKE '1221%' OR glaccount LIKE '1231%')