--DROP TABLE #247_pay; 
-- SELECT * FROM #247_pay
SELECT a.storecode, b.gtdate, a.gl_account, a.description, a.department, 
  gtctl# AS control, gtjrnl, gttamt AS amount
INTO #247_pay
FROM gmfs_accounts a
INNER JOIN  stgArkonaGLPTRNS b on a.gl_account = b.gtacct 
WHERE a.gm_account = '247'
  AND b.gtdate between '07/01/2015' AND '07/31/2015'
  AND b.gtjrnl IN ('PAY','GLI')   
-- looking for a natural key 
SELECT gtdate, gl_account, control, gtjrnl 
FROM #247_pay
GROUP BY gtdate, gl_account, control, gtjrnl 
HAVING COUNT(*) > 1
-- but it IS NOT here, overtime AND bonus are separate entries to the account
-- on the same day AS base pay
-- also accrual (GLI) debit & credit are both entered on the last day of the
-- month, don't want to lose the detail of accrual vs reversal, so can't DO
-- a grouping with SUM(amount)
-- the only thing that could WORK IS PK comprised of ALL fields, which IS NOT
-- very useful



SELECT storecode,gtdate,gl_account,description,department,control,gtjrnl,amount
FROM #247_pay
GROUP BY storecode,gtdate,gl_account,description,department,control,gtjrnl,amount
HAVING COUNT(*) > 1

-- added gttrn#, gtseq# for a natural key 
-- DROP TABLE #247_ro; 
-- accounting entries to the labor cost holding accounts; 247 (WIP)
-- SELECT * FROM #247_ro
SELECT a.storecode, b.gtdate, b.gttrn#, b.gtseq#, a.gl_account, a.description, a.department, 
  gtctl# AS control, gtjrnl, gttamt AS amount
INTO #247_ro
FROM gmfs_accounts a
INNER JOIN  stgArkonaGLPTRNS b on a.gl_account = b.gtacct
WHERE a.gm_account = '247'
  AND b.gtdate between '07/01/2015' AND '07/31/2015'
  AND (gtjrnl = 'SVI' OR gtjrnl = 'SCA' OR gtjrnl = 'SWA') 
-- so of course, this combination of attributes IS unique
-- but it IS virtually useless, those values relate to nothing ELSE
-- essentially a fucking surrogate key
SELECT gttrn#, gtseq#
-- SELECT *
FROM #247_ro
GROUP BY gttrn#, gtseq#
HAVING COUNT(*) > 1  
  
-- pay agrees exactly with jeri's sheet   
SELECT * FROM #247_pay WHERE gl_account = '124703'
SELECT SUM(amount) FROM #247_pay WHERE gl_Account = '124703'

-- this includes some ros WHERE the tech IS NOT a bs tech (16xxxxxx) ros
-- 16xx's are only $207 of $112,752
-- the costing applied to BS, but NOT offset with BS pay
SELECT gl_account, description, 
  SUM(CASE WHEN LEFT(control,2) = '16' THEN amount END) AS "16",
  SUM(CASE WHEN LEFT(control,2) = '16' THEN 1 ELSE 0 END) AS "16 Count",
  SUM(CASE WHEN LEFT(control,2) = '18' THEN amount END) AS "18",
  SUM(CASE WHEN LEFT(control,2) = '18' THEN 1 ELSE 0 END) AS "18 Count",
  SUM(CASE WHEN LEFT(control,2) = '19' THEN amount END) AS "19",
  SUM(CASE WHEN LEFT(control,2) = '19' THEN 1 ELSE 0 END) AS "19 Count",
  SUM(CASE WHEN LEFT(control,2) <> '16' AND LEFT(control,2) <> '18' AND LEFT(control,2) <> '19' THEN amount END) AS wtf
FROM #247_ro
GROUP BY gl_account, description

-- 8/19 ADD opcode costing
DROP TABLE #ro_1;
-- SELECT * FROM #ro_1
SELECT a.ro AS control, technumber, name, employeenumber, c.paymenttypecode, 
  d.servicetypecode, sum(flaghours) as flaghours, laborcost, sum(flaghours * laborcost) AS flag_x_cost
INTO #ro_1  
-- SELECT *
FROM factrepairorder a
INNER JOIN dimtech b on a.techkey = b.techkey
INNER JOIN dimpaymenttype c on a.paymenttypekey = c.paymenttypekey
INNER JOIN dimservicetype d on a.servicetypekey = d.servicetypekey
WHERE a.flaghours <> 0
AND a.ro IN (
  SELECT control
  FROM #247_ro)
--  WHERE gl_account = '124703')
GROUP BY a.ro, technumber, name, employeenumber, c.paymenttypecode, 
  d.servicetypecode, b.laborcost


SELECT * 
FROM #247_ro a 
LEFT JOIN #ro_1 b on a.control = b.control
WHERE a.gl_account = '124703'
  AND b.servicetypecode = 'bs'
  AND round(ABS(a.amount),2) = round(ABS(b.flag_x_cost),2)
ORDER BY a.control


SELECT * 
FROM #247_ro a 
LEFT JOIN #ro_1 b on a.control = b.control
WHERE a.gl_account = '124703'
  AND b.servicetypecode = 'bs'
  AND round(ABS(a.amount),2) <> round(ABS(b.flag_x_cost),2)
ORDER BY a.control

/*
what am i trying to DO
i am trying to match every row IN #247_ro (costed labor BY ro) to a tech IN #ro_1
so, exclude those that match AND see what IS left
*/
-- !!!!!!!!!! matches may NOT be to the penny, eg, 18037923 247: 59.05, ro: 59.04
-- here are the matches
--SELECT x.*, abs(round(ABS(x.amount),2) - round(ABS(x.flag_x_cost),2)) FROM (
--SELECT * 
SELECT a.control, a.amount, b.*
FROM #247_ro a 
LEFT JOIN #ro_1 b on a.control = b.control
WHERE a.gl_account = '124703'
  AND b.servicetypecode = 'bs'
  AND abs(round(ABS(a.amount),2) - round(ABS(b.flag_x_cost),2)) < .05  
--  AND (
--    round(ABS(a.amount),2) = round(ABS(b.flag_x_cost),2)
--    OR
--    abs(round(ABS(a.amount),2) - round(ABS(b.flag_x_cost),2)) < .05)
--) x WHERE round(ABS(x.amount),2) <> round(ABS(x.flag_x_cost),2)    
ORDER BY a.control

-- the unmatched rows FROM #247_ro
SELECT a.* -- unmatched #247_ro
FROM #247_ro a
LEFT JOIN ( -- matches
  SELECT a.gttrn#, a.gtseq#, a.control, a.amount  
  FROM #247_ro a 
  LEFT JOIN #ro_1 b on a.control = b.control
  WHERE a.gl_account = '124703'
    AND b.servicetypecode = 'bs'
--    AND round(ABS(a.amount),2) = round(ABS(b.flag_x_cost),2)) c on a.gttrn# = c.gttrn#
    AND abs(round(ABS(a.amount),2) - round(ABS(b.flag_x_cost),2)) < .05) c on a.gttrn# = c.gttrn#    
      AND a.gtseq# = c.gtseq# 
      AND a.control = c.control
      AND a.amount = c.amount   
WHERE a.gl_account = '124703'
  AND c.gttrn# IS NULL 
  
SELECT a.* -- unmatched #ro_1
FROM #ro_1 a
LEFT JOIN ( -- matches
  SELECT a.control, b.flag_x_cost
  FROM #247_ro a 
  LEFT JOIN #ro_1 b on a.control = b.control
  WHERE a.gl_account = '124703'
    AND b.servicetypecode = 'bs'
--    AND round(ABS(a.amount),2) = round(ABS(b.flag_x_cost),2)) c on a.control = c.control
--      AND a.flag_x_cost = c.flag_x_cost
    AND abs(round(ABS(a.amount),2) - round(ABS(b.flag_x_cost),2)) < .05) c on a.control = c.control
      AND abs(a.flag_x_cost - c.flag_x_cost) < .5 
WHERE a.servicetypecode = 'bs'
  AND c.control IS NULL 
  
SELECT *
FROM (
  SELECT a.* -- unmatched #247_ro
  FROM #247_ro a
  LEFT JOIN ( -- matches
    SELECT a.gttrn#, a.gtseq#, a.control, a.amount  
    FROM #247_ro a 
    LEFT JOIN #ro_1 b on a.control = b.control
    WHERE a.gl_account = '124703'
      AND b.servicetypecode = 'bs'
  --    AND round(ABS(a.amount),2) = round(ABS(b.flag_x_cost),2)) c on a.gttrn# = c.gttrn#
      AND abs(round(ABS(a.amount),2) - round(ABS(b.flag_x_cost),2)) < .05) c on a.gttrn# = c.gttrn#    
        AND a.gtseq# = c.gtseq# 
        AND a.control = c.control
        AND a.amount = c.amount   
  WHERE a.gl_account = '124703'
    AND c.gttrn# IS NULL ) e  
LEFT JOIN (
  SELECT a.* -- unmatched #ro_1
  FROM #ro_1 a
  LEFT JOIN ( -- matches
    SELECT a.control, b.flag_x_cost
    FROM #247_ro a 
    LEFT JOIN #ro_1 b on a.control = b.control
    WHERE a.gl_account = '124703'
      AND b.servicetypecode = 'bs'
  --    AND round(ABS(a.amount),2) = round(ABS(b.flag_x_cost),2)) c on a.control = c.control
  --      AND a.flag_x_cost = c.flag_x_cost
      AND abs(round(ABS(a.amount),2) - round(ABS(b.flag_x_cost),2)) < .05) c on a.control = c.control
        AND abs(a.flag_x_cost - c.flag_x_cost) < .5 
  WHERE a.servicetypecode = 'bs'
    AND c.control IS NULL) f ON e.control = f.control    

-- hmm GROUP AND SUM #247_ro unmatched, THEN JOIN to unmatched #ro_1
-- AND UNION that with the matches, AND it looks good, at least for body shop
-- SELECT * FROM #wtf
-- DROP table #wtf;
SELECT *
--INTO #wtf
FROM (
  SELECT a.control, SUM(a.amount) AS amount -- unmatched #247_ro
  FROM #247_ro a
  LEFT JOIN ( -- matches
    SELECT a.gttrn#, a.gtseq#, a.control, a.amount  
    FROM #247_ro a 
    LEFT JOIN #ro_1 b on a.control = b.control
    WHERE a.gl_account = '124703'
      AND b.servicetypecode = 'bs'
  --    AND round(ABS(a.amount),2) = round(ABS(b.flag_x_cost),2)) c on a.gttrn# = c.gttrn#
      AND abs(round(ABS(a.amount),2) - round(ABS(b.flag_x_cost),2)) < .05) c on a.gttrn# = c.gttrn#    
        AND a.gtseq# = c.gtseq# 
        AND a.control = c.control
        AND a.amount = c.amount   
  WHERE a.gl_account = '124703'
    AND c.gttrn# IS NULL
  GROUP BY a.control) e  
LEFT JOIN (
  SELECT a.* -- unmatched #ro_1
  FROM #ro_1 a
  LEFT JOIN ( -- matches
    SELECT a.control, b.flag_x_cost
    FROM #247_ro a 
    LEFT JOIN #ro_1 b on a.control = b.control
    WHERE a.gl_account = '124703'
      AND b.servicetypecode = 'bs'
  --    AND round(ABS(a.amount),2) = round(ABS(b.flag_x_cost),2)) c on a.control = c.control
  --      AND a.flag_x_cost = c.flag_x_cost
      AND abs(round(ABS(a.amount),2) - round(ABS(b.flag_x_cost),2)) < .05) c on a.control = c.control
        AND abs(a.flag_x_cost - c.flag_x_cost) < .5 
  WHERE a.servicetypecode = 'bs'
    AND c.control IS NULL) f ON e.control = f.control      
UNION -- with matches
SELECT a.control, a.amount, b.*
FROM #247_ro a 
LEFT JOIN #ro_1 b on a.control = b.control
WHERE a.gl_account = '124703'
  AND b.servicetypecode = 'bs'
  AND abs(round(ABS(a.amount),2) - round(ABS(b.flag_x_cost),2)) < .05

-- this seems to imply that we are good, those costings FROM #247_ro that are
-- comprised of multiple techs get exposed AND the SUM of their flag_x_cost 
-- equals the amount
SELECT a.*, amount + flag_x_cost
FROM #wtf a 
ORDER BY amount + flag_x_cost
    
select technumber, name, SUM(flaghours) AS flaghours, SUM(flag_x_cost) AS flag_x_cost 
FROM #wtf  
GROUP BY technumber, name
ORDER BY technumber  

SELECT SUM(flag_x_cost) FROM #wtf

select SUM(amount)
FROM #247_ro
WHERE gl_Account = '124703'

SELECT SUM(flag_x_cost)
FROM #wtf



18038022, 18036920,18037103,18037816


18036054

SELECT * 
FROM #247_ro a 
LEFT JOIN #ro_1 b on a.control = b.control
WHERE a.control = '18036054' ORDER BY name

SELECT * 
FROM #247_ro a 
LEFT JOIN #ro_1 b on a.control = b.control
  AND round(ABS(a.amount),2) = round(ABS(b.flag_x_cost),2)
WHERE a.control = '18036054'


SELECT *
FROM #247_ro
WHERE control = '18036054'

SELECT *
FROM #ro_1
WHERE control = '18036054'

SELECT *
FROM stgArkonaGLPTRNS
WHERE gtctl# = '18036054'
  AND gtacct = '124703'

SELECT gtseq#, gtdate, gtacct, gtctl#, gttamt, b.*
-- SELECT *
FROM stgArkonaGLPTRNS a
LEFT JOIN gmfs_accounts b on a.gtacct = b.gl_account
WHERE gttrn# = 2997270
ORDER BY gtacct
ORDER BY gtseq#
ORDER BY ABS(gttamt)

c/s account: 167300
SELECT SUM(gttamt)  -- 612.26
FROM stgArkonaGLPTRNS
WHERE gttrn# = 2997270
  AND gtacct = '167300'
SELECT SUM(gttamt) -- (612.26)
FROM stgArkonaGLPTRNS
WHERE gttrn# = 2997270
  AND gtacct = '124703'  

18036054 IS a good stuck point
glptrns: 124703: 4 entries -> #247_ro
         147300: 2 entries
         167300: 2 entries
#ro_1: 3 rows, one for each tech         
         


SELECT gttrn#, gtseq#
FROM #247_ro
GROUP BY gttrn#, gtseq#
HAVING COUNT(*) > 1

SELECT *
FROM #247_ro
WHERE control = '18036054'

-- the jeri approach, AND it matches her numbers
SELECT name, technumber, laborcost, SUM(flaghours) AS flag_hours, SUM(flag_x_cost) AS flag_x_cost
FROM #ro_1 a
WHERE technumber IN (
  SELECT technumber
  FROM dimtech
  WHERE flagdeptcode = 'bs')
GROUP BY name, technumber, laborcost
ORDER BY technumber 

-- training time acct 167501
SELECT *
FROM stgArkonaGLPTRNs
WHERE gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  AND gtacct IN ('167500','167501')

SELECT SUM(cost) FROM (-- this matches 167501
SELECT c.opcode, a.ro, e.technumber, e.name, e.laborcost, 
  SUM(a.flaghours) AS flaghours, e.laborcost * SUM(a.flaghours) AS cost, d.servicetypecode
FROM factRepairOrder a
INNER JOIN day b on a.flagdatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimServiceType d on a.serviceTypeKey = d.serviceTypeKey
INNER JOIN dimTech e on a.techkey = e.techkey
INNER JOIN day f on a.closedatekey = f.datekey
WHERE f.thedate BETWEEN '07/01/2015' AND '07/31/2015'   
  AND c.opcode IN ('SHOP','ST','TR','TRAIN')
--  AND LEFT(ro,2) = '18'
GROUP BY c.opcode, a.ro, e.technumber, e.name, e.laborcost, d.servicetypecode) x 
ORDER BY technumber

SELECT *  FROM factrepairorder