DROP TABLE #247_pay;
SELECT a.storecode, b.gtdate, a.gl_account, a.description, a.department, 
  gtctl# AS control, gtjrnl, gttamt AS amount
INTO #247_pay
FROM gmfs_accounts a
INNER JOIN  stgArkonaGLPTRNS b on a.gl_account = b.gtacct 
WHERE a.gm_account = '247'
  AND b.gtdate between '08/01/2015' AND '08/31/2015'
  AND b.gtjrnl IN ('PAY','GLI'); 
  
SELECT gl_account, SUM(amount) FROM #247_pay GROUP BY gl_account  


/*
SELECT thedate, teamname, teamprofpptd, SUM(techclockhourspptd) AS clock, 
  SUM(techptohourspptd + techvacationhourspptd + techholidayhourspptd) AS pto,
  SUM(techclockhourspptd + techptohourspptd + techvacationhourspptd + techholidayhourspptd) AS total
--SELECT *
FROM scotest.tpdata
WHERE thedate = payperiodend
  AND teamname NOT LIKE 'Team%'
  AND thedate > '01/20/2015'
GROUP BY thedate, teamname, teamprofpptd
ORDER BY teamname, thedate DESC 



SELECT *
FROM scotest.tpdata
WHERE techptohoursday <> 0
ORDER BY thedate DESC 

*/


1.     whose timeclock DO we track on a daily basis
       anyone who had pay to 247 IN the last 60 days
       fundamentally, whose pay goes INTO adj cost of labor

-- desiree grant (152525) shows up twice, she changed FROM pdq to main shop IN this duration    
-- DROP TABLE #wtf;  
SELECT a.storecode, a.gl_account, a.description, a.department, 
  gtctl# AS control, c.distcode, c.name
-- INTO #wtf  
FROM gmfs_accounts a
INNER JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct 
INNER JOIN edwEmployeeDim c on b.gtctl# = c.employeenumber
  AND c.currentrow = true 
WHERE a.gm_account = '247'
  AND b.gtdate between curdate() - 60 AND curdate()
  AND b.gtjrnl IN ('PAY','GLI')
GROUP BY a.storecode, a.gl_account, a.description, a.department, gtctl#, c.distcode, c.name

-- so, fuck ALL the other attributes, am just looking for a rough list of
-- employees
-- hmm compare this list against a list of current employees with these opcodes
-- employees with pay IN 247 IN past 60 days
SELECT a.storecode, gtctl# AS control, c.name
INTO #test_1
FROM gmfs_accounts a
INNER JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct 
INNER JOIN edwEmployeeDim c on b.gtctl# = c.employeenumber
  AND c.currentrow = true 
WHERE a.gm_account = '247'
  AND b.gtdate between curdate() - 60 AND curdate()
  AND b.gtjrnl IN ('PAY','GLI')
GROUP BY a.storecode, gtctl#, c.name

-- distcodes with pay IN 247 IN past 60 days
-- hm, think this IS ok'ish these distcodes minus PDQW

SELECT a.storecode, c.distcode
-- INTO #wtf  
-- SELECT *
FROM gmfs_accounts a
INNER JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct 
INNER JOIN edwEmployeeDim c on b.gtctl# = c.employeenumber
  AND c.currentrow = true 
--  AND c.distcode = 'pdqw'--b.gtdate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
WHERE a.gm_account = '247'
  AND b.gtdate between curdate() - 60 AND curdate()
  AND b.gtjrnl IN ('PAY','GLI')
GROUP BY a.storecode, c.distcode

select * FROM #247_pay

-- current employees with these distcodes
DROP TABLE #test_2;
SELECT a.storecode, a.employeenumber, a.name, a.hiredate
INTO #test_2
FROM edwEmployeeDim a
INNER JOIN (
  SELECT a.storecode, c.distcode
  -- INTO #wtf  
  FROM gmfs_accounts a
  INNER JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct 
  INNER JOIN edwEmployeeDim c on b.gtctl# = c.employeenumber
    AND c.currentrow = true 
  WHERE a.gm_account = '247'
    AND b.gtdate between curdate() - 60 AND curdate()
    AND b.gtjrnl IN ('PAY','GLI')
  GROUP BY a.storecode, c.distcode) b on a.storecode = b.storecode AND a.distcode = b.distcode
WHERE a.currentrow = true
  AND a.active = 'active'
  
SELECT *
FROM #test_1 a
full OUTER JOIN #test_2 b on a.storecode = b.storecode AND a.control = b.employeenumber
 
ok, wtf, why only one pdq writer with pay going to 247 
hmmm, he just became a writer on 7/15
SELECT * FROM edwEmployeeDim WHERE employeenumber = '110065'

SELECT * FROM #247_pay --WHERE control = '1117600'
WHERE control IN (
SELECT employeenumber--, name
FROM edwEmployeeDim 
WHERE distcode = 'pdqw'
  AND currentrow = true
  AND active = 'active')

SELECT *
FROM vEmpDim
WHERE distcode = 'pdqw'
  AND currentrow = true
  AND active = 'active'

SELECT control
FROM #wtf
GROUP BY control
HAVING COUNT(*) > 1

SELECT *
FROM #wtf
WHERE control IN (
  SELECT control
  FROM #wtf
  GROUP BY control
  HAVING COUNT(*) > 1)
  
SELECT gl_account, distcode
FROM #wtf
GROUP BY gl_account, distcode
HAVING COUNT(*) > 1  

SELECT * FROM edwEmployeeDim WHERE employeenumber = '152525'

SELECT * FROM #247_ro
SELECT * FROM #247_pay

SELECT storecode, gl_account, description, SUM(amount) AS paid FROM #247_pay GROUP BY storecode, gl_account, description


SELECT storecode, gl_account, description, 
  SUM(CASE WHEN LEFT(control,2) = '16' THEN amount END) AS "16",
  SUM(CASE WHEN LEFT(control,2) = '16' THEN 1 ELSE 0 END) AS "16 Count",
  SUM(CASE WHEN LEFT(control,2) = '18' THEN amount END) AS "18",
  SUM(CASE WHEN LEFT(control,2) = '18' THEN 1 ELSE 0 END) AS "18 Count",
  SUM(CASE WHEN LEFT(control,2) = '19' THEN amount END) AS "19",
  SUM(CASE WHEN LEFT(control,2) = '19' THEN 1 ELSE 0 END) AS "19 Count",
  SUM(CASE WHEN LEFT(control,2) <> '16' AND LEFT(control,2) <> '18' AND LEFT(control,2) <> '19' THEN amount END) AS wtf
FROM #247_ro
GROUP BY storecode, gl_account, description;

-- bs
SELECT *
FROM stgArkonaGLPTRNs
WHERE gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  AND gtacct IN ('167500','167501')

-- ms  
SELECT *
FROM stgArkonaGLPTRNs
WHERE gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  AND gtacct IN ('166504','166504A')
UNION
-- pdq    
SELECT *
FROM stgArkonaGLPTRNs
WHERE gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  AND gtacct = '166503'
UNION 
-- detail    
SELECT *
FROM stgArkonaGLPTRNs
WHERE gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  AND gtacct = '166524'  
  
SELECT SUM(cost) FROM (-- this matches 167501
SELECT c.opcode, a.ro, e.technumber, e.name, e.laborcost, 
  SUM(a.flaghours) AS flaghours, e.laborcost * SUM(a.flaghours) AS cost, d.servicetypecode
FROM factRepairOrder a
INNER JOIN day b on a.flagdatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimServiceType d on a.serviceTypeKey = d.serviceTypeKey
INNER JOIN dimTech e on a.techkey = e.techkey
INNER JOIN day f on a.closedatekey = f.datekey
WHERE f.thedate BETWEEN '07/01/2015' AND '07/31/2015'   
  AND c.opcode IN ('SHOP','ST','TR','TRAIN')
--  AND LEFT(ro,2) = '18'
GROUP BY c.opcode, a.ro, e.technumber, e.name, e.laborcost, d.servicetypecode) x 
ORDER BY technumber



  
SELECT *
FROM (
  SELECT DISTINCT control 
  FROM #247_pay) a
LEFT JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true  


SELECT gl_account, description, SUM(amount) AS amount
-- SELECT *
from #247_pay
GROUP BY gl_account, description
union
SELECT gl_account, description, SUM(amount) AS amount
-- SELECT *
FROM #247_ro
GROUP BY gl_account, description


SELECT DISTINCT gtdesc
FROM stgarkonaglptrns
WHERE gtjrnl = 'gli'
  AND gtdate between '07/01/2015' AND '07/31/2015'
  AND LEFT(gtdoc#, 3)  = 'GLI'
  
  
SELECT *
FROM stgarkonaglptrns WHERE gtjrnl = 'gli' AND gtctl# = '118026'

WHERE gtdesc = 'REMOVE FROM PAYROLL ACCRUAL'

WHERE gtjrnl = 'gli' 
  AND gtdate between '07/01/2015' AND '07/31/2015'
  AND LEFT(gtdoc#, 3)  = 'GLI'  
  
SELECT * FROM edwEmployeeDim WHERE employeenumber = '118026'  

SELECT *
FROM stgarkonapymast
WHERE ymdist = 'off'

SELECT *
FROM edwEmployeeDim a
WHERE EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE distcode = 'off'
    AND employeenumber = a.employeenumber)
    
/*
8/29
probably of no use
select a.gl_account, a.gm_account, a.page, a.line, a.col, a.description, a.department, 
  SUM(b.gttamt)
INTO #cost  
FROM gmfs_Accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_Account = b.gtacct
--  AND b.gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.page = 16
  AND a.line BETWEEN 21 AND 39
  AND a.storecode = 'ry1'
  AND a.account_type = '5'
  AND b.gtdate BETWEEN '07/01/2015' AND '07/31/2015'
GROUP BY a.gl_account, a.gm_account, a.page, a.line, a.col, a.description, a.department  

SELECT '', department, sum(expr), 'cost' FROM #cost GROUP BY department
UNION 
SELECT gl_account, description, SUM(amount) AS amount, '247_ro' FROM #247_ro WHERE storecode = 'ry1' GROUP BY gl_account, description 
UNION
SELECT gl_account, description, SUM(amount), '247_pay' FROM #247_pay where storecode = 'ry1' GROUP BY gl_account, description
*/    


-- 8/31  

SELECT b.thedate, c.storecode, 
  c.name, c.employeenumber, c.distcode, c.payrollclass, c.hourlyrate, 
  a.clockhours, a.overtimehours, a.vacationhours, a.ptohours, a.holidayhours
-- SELECT *  
FROM edwClockHoursFact a  
INNER JOIN day b on a.datekey = b.datekey
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  AND c.distcode IN ('stec','btec','ptec','wtec')
WHERE b.yearmonth = 201508   
  AND clockhours + vacationhours + ptohours + holidayhours <> 0
  
SELECT storecode, distcode, name
FROM (
  SELECT b.thedate, c.storecode, 
    c.name, c.employeenumber, c.distcode, c.payrollclass, c.hourlyrate, 
    a.clockhours, a.overtimehours, a.vacationhours, a.ptohours, a.holidayhours
  FROM edwClockHoursFact a  
  INNER JOIN day b on a.datekey = b.datekey
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
    AND c.distcode IN ('stec','btec','ptec','wtec')
  WHERE b.yearmonth = 201508   
    AND clockhours + vacationhours + ptohours + holidayhours <> 0) x  
GROUP BY storecode, distcode, name  

-- damnit, this leaves out at least john gardner, who IS salaried
-- so, start with the relevant people first
-- a person could be listed more than once IF, pay, class, distcode change
-- within the time period, should NOT be a problem, but, we will see
-- this should provide the base census list AS well AS ALL relevant employeekeys
-- for clockhours
-- SELECT name FROM (
SELECT storecode, name, employeenumber, employeekey, distcode, payrollclass, 
  hourlyrate, salary
INTO #acl_employees  
FROM edwEmployeeDim c 
INNER JOIN day b on b.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
  AND b.yearmonth = 201508
WHERE c.distcode IN ('stec','btec','ptec','wtec')
  AND c.currentrow = true
  AND c.active = 'active'
GROUP BY storecode, name, employeenumber, employeekey, distcode, payrollclass, 
  hourlyrate, salary --) x where payrollclass = 'salary'-- ) x GROUP BY name HAVING COUNT(*) > 1
ORDER BY storecode, distcode, name  

SELECT * FROM #acl_employees

-- acl census these are the folks whose pay figures INTO adjusted cost of 
-- labor for each dept
SELECT distcode, name
FROM #acl_employees
GROUP BY distcode, name

-- clockhours
SELECT a.*, c.thedate, b.*
FROM #acl_employees a
LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
INNER JOIN day c on b.datekey = c.datekey
  AND c.yearmonth = 201508

-- clockhours
SELECT a.storecode, a.name, a.employeenumber, a.employeekey, a.distcode, 
  a.payrollclass, a.hourlyrate, a.salary,
  SUM(
    CASE a.payrollclass
      WHEN 'Salary' THEN 0
      WHEN 'Hourly' THEN RegularHours
      WHEN 'Commission' THEN ClockHours
    END) AS ClockHours, 
  SUM(
    CASE a.payrollclass
      WHEN 'Salary' THEN 0
      WHEN 'Hourly' THEN OverTimeHours
      WHEN 'Commission' THEN 0
    END) AS OvertimeHours,  
  SUM(vacationhours + ptohours + holidayhours) AS ptoHours      
INTO #acl_clockhours  
FROM #acl_employees a
LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
INNER JOIN day c on b.datekey = c.datekey
  AND c.yearmonth = 201508
GROUP BY a.storecode, a.name, a.employeenumber, a.employeekey, a.distcode, 
  a.payrollclass, a.hourlyrate, a.salary  
  
-- clockhours BY day
SELECT c.thedate, a.storecode, a.name, a.employeenumber, a.employeekey, a.distcode, 
  a.payrollclass, a.hourlyrate, a.salary,
  SUM(
    CASE a.payrollclass
      WHEN 'Salary' THEN 0
      WHEN 'Hourly' THEN RegularHours
      WHEN 'Commission' THEN ClockHours
    END) AS ClockHours, 
  SUM(
    CASE a.payrollclass
      WHEN 'Salary' THEN 0
      WHEN 'Hourly' THEN OverTimeHours
      WHEN 'Commission' THEN 0
    END) AS OvertimeHours,  
  SUM(vacationhours + ptohours + holidayhours) AS ptoHours      
INTO #acl_clockhours_day  
FROM #acl_employees a
LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
INNER JOIN day c on b.datekey = c.datekey
  AND c.yearmonth = 201508
GROUP BY c.thedate, a.storecode, a.name, a.employeenumber, a.employeekey, a.distcode, 
  a.payrollclass, a.hourlyrate, a.salary    

--SELECT name FROM (  
SELECT *
FROM #acl_employees a
LEFT JOIN scotest.tptechs b on a.employeenumber = b.employeenumber
  AND b.fromdate < '9/01/2015'
  AND b.thrudate > '07/31/2015'
--) x GROUP BY name HAVING COUNT(*) > 1


SELECT *
FROM #acl_employees
WHERE distcode = 'stec'
AND payrollclass = 'hourly'