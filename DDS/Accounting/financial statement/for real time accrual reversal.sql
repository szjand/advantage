-- kristen swanson 1134524 did NOT become bs until 8/13/15
DECLARE @year_month integer;
DECLARE @days_in_month integer;
DECLARE @min_date date;
DECLARE @max_date date;
DECLARE @previous_year_month integer;
@year_month = 201507; ---------------------------------------------------------
@days_in_month = (
  SELECT COUNT(*)
  FROM dds.day
  WHERE yearmonth = @year_month);  
@min_date = (
  SELECT MIN(thedate)
  FROM dds.day
  WHERE yearmonth = @year_month);
@max_date = (
  SELECT MAX(thedate)
  FROM dds.day
  WHERE yearmonth = @year_month);
@previous_year_month = (
  SELECT yearmonth
  FROM dds.day
  WHERE thedate = @min_date - 1);  
SELECT a.check_date, a.gross, pay_period_start_date, pay_period_end_date, 
  b.employee_number, 
  left(trim(b.last_name) + ', ' + TRIM(first_name), 25) AS name,
  CASE 
    WHEN pay_period_start_date < @min_date THEN 
--      round(gross * dayofmonth(pay_period_end_date)/@days_in_month, 2) 
      round(gross * dayofmonth(pay_period_end_date)/14, 2) 
    WHEN pay_period_end_date > @max_date THEN 
--      round(gross * (1 + (@days_in_month - dayofmonth(pay_period_start_date)))/@days_in_month, 2)
      round(gross * (1 + (@days_in_month - dayofmonth(pay_period_start_date)))/14, 2)
    ELSE gross
  END AS adj_gross,
  c.accrual, c.reversal, c.accrual_month,  d.accrual_days, d.accrual_days_14,
  c.reversal_month, e.accrual_days, e.accrual_days_14
FROM acl_paychecks a
INNER JOIN (
  SELECT DISTINCT employee_number, first_name, last_name, store_code, department,
    payroll_class
  FROM acl_census
  WHERE department = 'body_shop' AND employee_number = '1126040'
    AND year_month = @year_month) b on a.employee_number = b.employee_number
LEFT JOIN (
  SELECT aa.*
  FROM acl_accruals aa
  INNER JOIN (
    SELECT distinct employee_number
    FROM acl_census
    WHERE department = 'body_shop'
      AND year_month = 201507) bb on aa.employee_number = bb.employee_number
  WHERE the_date = @max_date) c on a.employee_number = c.employee_number
LEFT JOIN acl_accrual_dates d on d.year_month = @year_month  
LEFT JOIN acl_accrual_dates e on e.year_month = @previous_year_month
WHERE pay_period_end_date >= @min_date
  AND pay_period_start_date <= @max_date
 