
SELECT f.thedate as closeDate, a.ro, a.line, a.flaghours, a.roflaghours, 
  d.servicetypecode, c.opcode, e.technumber, e.name, e.laborcost, 
  flaghours * laborcost AS cost
INTO #wtf  
FROM factRepairOrder a
INNER JOIN day b on a.flagdatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimServiceType d on a.serviceTypeKey = d.serviceTypeKey
INNER JOIN dimTech e on a.techkey = e.techkey
INNER JOIN day f on a.closedatekey = f.datekey
WHERE f.thedate BETWEEN '03/01/2015' AND '03/31/2015'   
  AND c.opcode IN ('SHOP','ST','TR','TRAIN')
ORDER BY ro, technumber

-- need it to be ro/tech level
DROP TABLE #wtf;
SELECT c.opcode, a.ro, e.technumber, e.name, e.laborcost, 
  SUM(a.flaghours) AS flaghours, e.laborcost * SUM(a.flaghours) AS cost
INTO #wtf  
FROM factRepairOrder a
INNER JOIN day b on a.flagdatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimServiceType d on a.serviceTypeKey = d.serviceTypeKey
INNER JOIN dimTech e on a.techkey = e.techkey
INNER JOIN day f on a.closedatekey = f.datekey
WHERE f.thedate BETWEEN '03/01/2015' AND '03/31/2015'   
  AND c.opcode IN ('SHOP','ST','TR','TRAIN')
GROUP BY c.opcode, a.ro, e.technumber, e.name, e.laborcost  


select * FROM #wtf WHERE ro = '16184978'

select *
FROM stgArkonaSDPRDET a
WHERE ptro# IN (
  SELECT a.ro
  FROM factRepairOrder a
  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
  INNER JOIN day f on a.closedatekey = f.datekey
  WHERE f.thedate BETWEEN '03/01/2015' AND '03/31/2015'   
    AND c.opcode IN ('SHOP','ST','TR','TRAIN'))
AND ptpadj = 'X'    

SELECT ptro#, pttech, sum(ptlhrs) AS ptlhrs
-- SELECT *
FROM stgArkonaSDPRDET a
WHERE ptro# IN (
  SELECT ro
  FROM #wtf)
AND ptpadj = 'X'  
GROUP BY ptro#, pttech

need to subtract the hours of adjusted rows, NOT the cost
16184978 574:  15.3 hours  1.00 ptcost, ie, cost will NOT cut it, because
  cost IN #wtf IS derived FROM flaghours*laborcost, so, 15.3 hours IS NOT
  going to be offset BY 1 hour at the labor cost
select * FROM stgArkonaSDPRDET WHERE pttech = '574' AND ptro# = '16184978'

DROP TABLE #wtf1;
SELECT a.*, coalesce(b.ptlhrs, 0) AS ptlhrs
INTO #wtf1
FROM #wtf a
LEFT JOIN (
  SELECT ptro#, pttech, sum(ptlhrs) AS ptlhrs
  -- SELECT *
  FROM stgArkonaSDPRDET a
  WHERE ptro# IN (
    SELECT ro
    FROM #wtf)
  AND ptpadj = 'X'  
  GROUP BY ptro#, pttech) b on a.ro = b.ptro# AND a.technumber = b.pttech

select * FROM #wtf1 ORDER BY ro

-- populate with spreadsheet FROM ark labor op report
CREATE TABLE #ark (
  ro cichar(9),
  tech cichar(3),
  gross curdouble(8,2)) 
  
SELECT a.opcode, a.ro, a.technumber, a.name, a.laborcost, a.flaghours, a.cost, a.ptlhrs, 
  (a.flaghours - a.ptlhrs) * a.laborcost AS adjCost, -1 * b.arkGross AS arkGross
FROM #wtf1 a
LEFT JOIN (
  SELECT ro, tech, SUM(gross) AS arkGross
  FROM #ark
  GROUP BY ro, tech) b on a.ro = b.ro AND a.technumber = b.tech
ORDER BY a.opcode, a.ro, a.technumber  

-- ok, they fucking match
select *
FROM (
  SELECT a.opcode, a.ro, a.technumber, a.name, a.laborcost, a.flaghours, a.cost, a.ptlhrs, 
    (a.flaghours - a.ptlhrs) * a.laborcost AS adjCost, -1 * b.arkGross AS arkGross
  FROM #wtf1 a
  LEFT JOIN (
    SELECT ro, tech, SUM(gross) AS arkGross
    FROM #ark
    GROUP BY ro, tech) b on a.ro = b.ro AND a.technumber = b.tech) z
WHERE round(cast(adjCost AS sql_double), 2) <> round(cast(arkGross AS sql_double), 2)

select *
FROM #ark a
LEFT JOIN #wtf1 b on a.ro = b.ro AND a.tech = b.technumber
WHERE b.ro IS NULL 

SELECT SUM(adjCost)
FROM (
  SELECT a.opcode, a.ro, a.technumber, a.name, a.laborcost, a.flaghours, a.cost, a.ptlhrs, 
    (a.flaghours - a.ptlhrs) * a.laborcost AS adjCost, -1 * b.arkGross AS arkGross
  FROM #wtf1 a
  LEFT JOIN (
    SELECT ro, tech, SUM(gross) AS arkGross
    FROM #ark
    GROUP BY ro, tech) b on a.ro = b.ro AND a.technumber = b.tech) x
WHERE LEFT(ro, 2) = '16'    