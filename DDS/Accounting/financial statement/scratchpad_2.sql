-- july adjusted cost, detail  
select a.*, b.gttamt
FROM dds.gmfs_accounts a
INNER JOIN dds.stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.page = 16 
  AND a.line IN (30,39) 

-- training time IS fucked up, see jeff bear for july, 51.5 hours against 16202234
-- training/shop time  
-- 9/29 change it to closedate - that took care of the bear proble
-- SELECT yearmonth, servicetypecode, round(SUM(cost), 2) FROM (
SELECT a.storecode, c.opcode, a.ro, e.technumber, e.employeenumber, 
  e.flagDeptCode, e.laborcost, SUM(a.flaghours) AS flaghours, 
  e.laborcost * SUM(a.flaghours) AS cost, 
  d.servicetypecode, f.thedate, MAX(f.yearmonth) AS yearmonth
--INTO #wtf
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.closedatekey = b.datekey
INNER JOIN dds.dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dds.dimServiceType d on a.serviceTypeKey = d.serviceTypeKey
INNER JOIN dds.dimTech e on a.techkey = e.techkey
INNER JOIN dds.day f on a.closedatekey = f.datekey
WHERE f.thedate BETWEEN '06/01/2015' AND '08/31/2015'   
  AND c.opcode IN ('SHOP','ST','TR','TRAIN') //AND technumber = '574'
--  AND LEFT(ro,2) = '18'
GROUP BY a.storecode, c.opcode, a.ro, e.technumber, e.employeenumber, 
  e.laborcost, e.flagDeptCode, d.servicetypecode, f.thedate 
--) x GROUP BY yearmonth, servicetypecode

SELECT ro, employeenumber, laborcost FROM #wtf GROUP BY ro, employeenumber, laborcost HAVING COUNT(*) > 1

   
CREATE TABLE acl_shop_time (
  store_code cichar(3) constraint NOT NULL,
  opcode cichar(10) constraint NOT NULL,
  ro cichar(9) constraint NOT NULL,
  tech_number cichar(3) constraint NOT NULL,
  employee_number cichar(7) constraint NOT NULL,
  department_code cichar(2) constraint NOT NULL,
  labor_cost numeric(8,2) constraint NOT NULL,
  flag_hours numeric(8,2) constraint NOT NULL,
  cost numeric(12,2) constraint NOT NULL,
  service_type_code cichar(2) constraint NOT NULL,
  the_date date constraint NOT NULL,
  year_month integer constraint NOT NULL,
  constraint pk primary key(ro,employee_number,labor_cost)) IN database;  
INSERT INTO acl_shop_time
SELECT a.storecode, c.opcode, a.ro, e.technumber, e.employeenumber, 
  e.flagDeptCode, e.laborcost, SUM(a.flaghours) AS flaghours, 
  e.laborcost * SUM(a.flaghours) AS cost, 
  d.servicetypecode, f.thedate, MAX(f.yearmonth) AS yearmonth
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.closedatekey = b.datekey
INNER JOIN dds.dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dds.dimServiceType d on a.serviceTypeKey = d.serviceTypeKey
INNER JOIN dds.dimTech e on a.techkey = e.techkey
INNER JOIN dds.day f on a.closedatekey = f.datekey
WHERE f.thedate BETWEEN '06/01/2015' AND '08/31/2015'   
  AND c.opcode IN ('SHOP','ST','TR','TRAIN') 
GROUP BY a.storecode, c.opcode, a.ro, e.technumber, e.employeenumber, 
  e.laborcost, e.flagDeptCode, d.servicetypecode, f.thedate;   

-- the whole deal with bev adding shop time AS tech time adjustments, those
-- flag hours don't show up IN the reporting
-- jeri generates a monthly number user Sales BY Labor Op report AND manually
-- posts the amount to the appropriate account, which gets added to
-- the adjusted cost of labor
-- isn't this double dipping IN a bad way?

HELP ME GREG - MY HEAD HURTS
should this amount be added to total costing, NOT to adj cost of labor?
they get paid for it, but it does NOT show up IN costed because labor rates 
are SET to 0 for service type OT
SELECT *
FROM dds.stgArkonaGLPTRNS
WHERE gtdate BETWEEN '06/01/2015' AND '08/31/2015'   
  AND gtacct IN ('166504A','167501') 

-- pay AND costed
-- pay: gtjrnl: GLI, PAY
-- costed: gtjrnl: SCA, SVI, SWA
select gtacct, gtjrnl, SUM(gttamt)
-- SELECT SUM(gttamt)
from dds.stgArkonaGLPTRNS a
WHERE a.gtdate between '07/01/2015' AND '07/31/2015'
AND gtpost = 'Y'
--AND gtjrnl IN ('svi','sca','swa')
AND gtjrnl <> 'std'
AND gtacct = '124703'
AND gtacct IN (
  SELECT gl_account
  FROM dds.gmfs_accounts
  WHERE gm_account = '247')i
GROUP BY gtacct, gtjrnl  


select  gtacct, gtjrnl, SUM(gttamt) 
from dds.stgArkonaGLPTRNS a
INNER JOIN dds.day b on a.gtdate = b.thedate
WHERE a.gtdate between '06/01/2015' AND '08/31/2015'
AND gtpost = 'Y'
AND gtjrnl <> 'std'
AND gtacct IN (
  SELECT gl_account
  FROM dds.gmfs_accounts
  WHERE gm_account = '247')
GROUP BY gtacct, gtjrnl  

SELECT * FROM dds.gmfs_accounts WHERE gm_Account = '247'

DROP TABLE #wtf;
select  b.yearmonth, b.thedate, aa.storecode, 
  CASE 
    WHEN a.gtacct IN ('124700', '124704', '224700', '224704') THEN 'Main Shop'
    WHEN a.gtacct IN ('124701','224701', '124723', '224723') THEN 'PDQ'
    WHEN a.gtacct = '124703' THEN 'Body Shop'
    WHEN a.gtacct IN ('124702','124724') THEN 'Detail'
  END AS department, a.gtacct, a.gttamt,
  a.gtjrnl, a.gtctl#
INTO #wtf
from dds.stgArkonaGLPTRNS a
INNER JOIN dds.gmfs_accounts aa on a.gtacct = aa.gl_account
INNER JOIN dds.day b on a.gtdate = b.thedate
WHERE a.gtdate between '02/01/2016' AND '02/29/2016'
  AND a.gtjrnl <> 'std'
  AND aa.gm_account = '247';
--GROUP BY aa.storecode  

-- yep, this IS it
select yearmonth, storecode, department, 
  SUM(CASE WHEN gtjrnl IN ('sca','svi','swa') THEN gttamt ELSE 0 END) AS costed,
  SUM(CASE WHEN gtjrnl IN ('pay','gli') THEN gttamt ELSE 0 END) AS paid
FROM #wtf
GROUP BY yearmonth, storecode, department 

SELECT yearmonth, storecode, department, costed, paid, costed + paid
FROM (
  select yearmonth, storecode, department, 
    SUM(CASE WHEN gtjrnl IN ('sca','svi','swa') THEN gttamt ELSE 0 END) AS costed,
    SUM(CASE WHEN gtjrnl IN ('pay','gli') THEN gttamt ELSE 0 END) AS paid
  FROM #wtf
  GROUP BY yearmonth, storecode, department) x 

SELECT a.*, monthname(thedate) + ' ' + CAST(year(thedate) AS sql_char)
FROM #wtf a
WHERE gtctl# = '1100611' AND thedate = '07/31/2015'


SELECT gtctl#, gtacct, thedate
FROM #wtf
GROUP BY gtctl#, gtacct, thedate
HAVING COUNT(*) > 1

SELECT gtctl#, gtacct, thedate, gtjrnl FROM (
SELECT yearmonth, thedate, storecode, department, gtacct, gtjrnl, gtctl#, SUM(gttamt)
FROM #wtf
GROUP BY yearmonth, thedate, storecode, department, gtacct, gtjrnl, gtctl#
) x GROUP BY gtctl#, gtacct, thedate, gtjrnl
HAVING COUNT(*) > 1

DROP TABLE acl_v1;
CREATE TABLE acl_v1 (
  year_month integer constraint NOT NULL,
  the_date date constraint NOT NULL,
  store_code cichar(3) constraint NOT NULL,
  department cichar(12) constraint NOT NULL,
  account cichar(10) constraint NOT NULL,
  journal cichar(3) constraint NOT NULL,
  control cichar(9) constraint NOT NULL,
  amount numeric(12,2) constraint NOT NULL,
  year_month_display cichar(16) constraint NOT NULL,
  constraint pk primary key (the_date, control, account, journal)) IN database;
INSERT INTO acl_v1
SELECT yearmonth,thedate,storecode,department,gtacct,gtjrnl,gtctl#, sum(gttamt),
  MAX(monthname(thedate) + ' ' + CAST(year(thedate) AS sql_char))
FROM #wtf
GROUP BY yearmonth,thedate,storecode,department,gtacct,gtjrnl,gtctl#;
  
execute procedure sp_createindex90('acl_v1','acl_v1.adi','year_month','year_month','',2,512,'');
execute procedure sp_createindex90('acl_v1','acl_v1.adi','the_date','the_date','',2,512,'');
execute procedure sp_createindex90('acl_v1','acl_v1.adi','store_code','store_code','',2,512,'');
execute procedure sp_createindex90('acl_v1','acl_v1.adi','department','department','',2,512,'');
execute procedure sp_createindex90('acl_v1','acl_v1.adi','year_month_display','year_month_display','',2,512,'');
execute procedure sp_createindex90('acl_v1','acl_v1.adi','control','control','',2,512,'');
execute procedure sp_createindex90('acl_v1','acl_v1.adi','journal','journal','',2,512,'');

DELETE FROM acl_v1 WHERE year_month = 201512
  
SELECT * 
FROM acl_v1;

CREATE PROCEDURE acl_get_dates(
  year_month integer output,
  year_month_display cichar(16) output)
BEGIN
/*
EXECUTE PROCEDURE acl_get_dates();
*/
INSERT INTO __output
SELECT top 10 year_month, year_month_display
FROM acl_v1
GROUP BY year_month, year_month_display
ORDER BY year_month DESC;
END;

CREATE PROCEDURE acl_get_market(
  year_month integer,
  year_month_display cichar(16) output,
  costed integer output,
  paid integer output,
  adjusted integer output)
BEGIN
/*  
EXECUTE PROCEDURE acl_get_market(0);
*/
DECLARE @year_month integer;
@year_month = (SELECT year_month FROM __input);
IF @year_month = 0 THEN
  @year_month = (
    SELECT MAX(year_month)
    FROM acl_v1);
END IF;
INSERT INTO __output
SELECT year_month_display, -costed, paid, -1*(paid+costed) AS adjusted
FROM (
  select year_month_display, 
    round(SUM(CASE WHEN journal IN ('sca','svi','swa') THEN amount ELSE 0 END), 0) AS costed,
    round(SUM(CASE WHEN journal IN ('pay','gli') THEN amount ELSE 0 END), 0) AS paid
  FROM acl_v1
  WHERE year_month = @year_month
  GROUP BY year_month_display) x;
END;



ALTER PROCEDURE acl_get_store(
  year_month integer,  
  store_code cichar(3),
  year_month_display cichar(16) output,  
  store_code cichar(3) output, 
  costed integer output,
  paid integer output,
  adjusted integer output)  
BEGIN
/*  
EXECUTE PROCEDURE acl_get_store(201507, 'RY1');
*/
DECLARE @year_month integer;
DECLARE @store string;
@year_month = (SELECT year_month FROM __input);
IF @year_month = 0 THEN
  @year_month = (
    SELECT MAX(year_month)
    FROM acl_v1);
END IF;
@store = (SELECT store_code FROM __input);
INSERT INTO __output
SELECT year_month_display, store_code, -costed, paid, -1*(paid+costed) AS adjusted
FROM (
  select year_month_display, store_code,
    round(SUM(CASE WHEN journal IN ('sca','svi','swa') THEN amount ELSE 0 END), 0) AS costed,
    round(SUM(CASE WHEN journal IN ('pay','gli') THEN amount ELSE 0 END), 0) AS paid
  FROM acl_v1
  WHERE year_month = @year_month
    AND store_code = @store
  GROUP BY year_month_display, store_code) x;
END;    
  
  
  
ALTER PROCEDURE acl_get_department(
  year_month integer,  
  store_code cichar(3),
  department cichar(12),
  year_month_display cichar(16) output,  
  store_code cichar(3) output, 
  department cichar(12) output,
  costed integer output,
  paid integer output,
  adjusted integer output)  
BEGIN
/*  
EXECUTE PROCEDURE acl_get_department(201507, 'RY1', 'pdq')
*/
DECLARE @year_month integer;
DECLARE @store string;
DECLARE @department string;
@year_month = (SELECT year_month FROM __input);
IF @year_month = 0 THEN
  @year_month = (
    SELECT MAX(year_month)
    FROM acl_v1);
END IF;
@store = (SELECT store_code FROM __input);
--@department = (SELECT department FROM __input);
@department = (
  SELECT 
    CASE
      WHEN (SELECT lower(department) FROM __input) = 'mainshop' THEN 'Main Shop'
      WHEN (SELECT lower(department) FROM __input) = 'bodyshop' THEN 'Body Shop'
      WHEN (SELECT lower(department) FROM __input) = 'detail' THEN 'Detail'
      WHEN (SELECT lower(department) FROM __input) = 'pdq' THEN 'PDQ'
    END
  FROM system.iota); 
INSERT INTO __output
SELECT year_month_display, store_code, department, -costed, paid, -1*(paid+costed) AS adjusted
FROM (
  select year_month_display, store_code, department,
    round(SUM(CASE WHEN journal IN ('sca','svi','swa') THEN amount ELSE 0 END), 0) AS costed,
    round(SUM(CASE WHEN journal IN ('pay','gli') THEN amount ELSE 0 END), 0) AS paid
  FROM acl_v1
  WHERE year_month = @year_month
    AND store_code = @store
    AND department = @department  
  GROUP BY year_month_display, store_code, department) x;   
END; 

/*
-- costed does NOT match jeri's sheet/statement
SELECT employeenumber, first_name, last_name, technumber, round(paid, 0) as paid,
  round(costed, 0) as costed, round(paid-costed, 0) AS adj
--into #wtf1  
FROM (
  SELECT *
  -- SELECT SUM(paid)
  FROM (
    SELECT control, SUM(amount) AS paid
    FROM acl_v1 a
    WHERE a.year_month = 201507
      AND a.department = 'body shop'
      AND a.journal IN ('pay','gli')
    GROUP BY control) a   
  LEFT JOIN (
    SELECT employee_number, first_name, last_name
    FROM acl_census
    WHERE year_month = 201507
      AND department = 'body_shop'
    GROUP BY employee_number, first_name, last_name) b on a.control = b.employee_number
  LEFT JOIN (  
    SELECT c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
    FROM dds.factRepairOrder b
    INNER JOIN dds.dimtech c on b.techkey = c.techkey
    WHERE b.ro IN (  
      SELECT control
      FROM acl_v1 
      WHERE year_month = 201507
        AND department = 'body shop'
        AND journal IN ('sca','svi','swa')   
      GROUP BY control) 
    GROUP BY c.employeenumber, c.technumber) c on b.employee_number = c.employeenumber) d 
    
UNION
SELECT '', '', '', '999', round(sum(paid), 0), round(sum(costed), 0),
  round(sum(paid-costed), 0)
FROM (
  SELECT *
  -- SELECT SUM(paid)
  FROM (
    SELECT control, SUM(amount) AS paid
    FROM acl_v1 a
    WHERE a.year_month = 201507
      AND a.department = 'body shop'
      AND a.journal IN ('pay','gli')
    GROUP BY control) a   
  LEFT JOIN (
    SELECT employee_number, first_name, last_name
    FROM acl_census
    WHERE year_month = 201507
      AND department = 'body_shop'
    GROUP BY employee_number, first_name, last_name) b on a.control = b.employee_number
  LEFT JOIN (  
    SELECT c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
    FROM dds.factRepairOrder b
    INNER JOIN dds.dimtech c on b.techkey = c.techkey
    WHERE b.ro IN (  
      SELECT control
      FROM acl_v1 
      WHERE year_month = 201507
        AND department = 'body shop'
        AND journal IN ('sca','svi','swa')   
      GROUP BY control) 
    GROUP BY c.employeenumber, c.technumber) c on b.employee_number = c.employeenumber) d      
ORDER BY technumber    

-- this looks closer to jeri's sheet
SELECT *
FROM (
  SELECT techkey, servicetypecode, round(SUM(flaghours), 2) AS flag_hours
  FROM dds.factrepairorder a
  INNER JOIN dds.day b on a.closedatekey = b.datekey
    AND b.yearmonth = 201507
  INNER JOIN dds.dimservicetype c on a.servicetypekey = c.servicetypekey  
  GROUP BY techkey, servicetypecode) c
LEFT JOIN (
  SELECT tech_key, tech_number, employee_number, first_name, last_name
  FROM acl_census a
  WHERE year_month = 201507
--    AND department = 'body_shop'
  GROUP BY tech_key, tech_number, employee_number, first_name, last_name) d on c.techkey = d.tech_key  
WHERE d.tech_key IS NOT NULL
  OR c.servicetypecode = 'bs'  
ORDER BY servicetypecode, tech_number



-- what's the fucking difference
DROP TABLE #wtf1;
    SELECT c.employeenumber, b.ro, c.technumber, sum(flaghours) as flaghours, sum(b.flaghours * c.laborcost) AS costed
INTO #wtf1    
    FROM acl_v1 a
    INNER JOIN dds.factRepairOrder b on a.control = b.ro
    INNER JOIN dds.dimtech c on b.techkey = c.techkey
    WHERE a.year_month = 201507
      AND a.department = 'body shop'
      AND a.journal IN ('sca','svi','swa')
      AND c.technumber IN ('241','243','247','251','255')
    GROUP BY c.employeenumber, b.ro, c.technumber
ORDER BY technumber, ro    

-- yep, that fixed it
DROP TABLE #wtf1;
    SELECT c.employeenumber, b.ro, c.technumber, sum(flaghours) as flaghours, sum(b.flaghours * c.laborcost) AS costed
INTO #wtf1  
    FROM dds.factRepairOrder b
    INNER JOIN dds.dimtech c on b.techkey = c.techkey
    WHERE b.ro IN (  
      SELECT control
      FROM acl_v1 
      WHERE year_month = 201507
        AND department = 'body shop'
        AND journal IN ('sca','svi','swa')   
      GROUP BY control) 
    GROUP BY c.employeenumber, b.ro, c.technumber
    
    
DROP TABLE #wtf2;
SELECT *
INTO #wtf2
FROM (
  SELECT ro, techkey, servicetypecode, round(SUM(flaghours), 2) AS flag_hours
  FROM dds.factrepairorder a
  INNER JOIN dds.day b on a.closedatekey = b.datekey
    AND b.yearmonth = 201507
  INNER JOIN dds.dimservicetype c on a.servicetypekey = c.servicetypekey  
  GROUP BY ro, techkey, servicetypecode) c
LEFT JOIN (
  SELECT tech_key, tech_number, employee_number, first_name, last_name
  FROM acl_census a
  WHERE year_month = 201507
    AND department = 'body_shop'
    AND tech_number IN ('241','243','247','251','255')
  GROUP BY tech_key, tech_number, employee_number, first_name, last_name) d on c.techkey = d.tech_key  
WHERE d.tech_number IN ('241','243','247','251','255') 
  AND (d.tech_key IS NOT NULL OR c.servicetypecode = 'bs')  
  
  
SELECT *
FROM #wtf1 a
full OUTER JOIN #wtf2 b on a.technumber = b.tech_number   AND a.ro = b.ro
WHERE a.technumber = b.tech_number
  AND a.ro = b.ro
  AND round(a.flaghours, 2) <> round(b.flag_hours, 2)

-- it appears that for these ros, flag hours are being doubled  
SELECT *
FROM #wtf1
WHERE ro IN (  
  SELECT a.ro
  FROM #wtf1 a
  full OUTER JOIN #wtf2 b on a.technumber = b.tech_number   AND a.ro = b.ro
  WHERE a.technumber = b.tech_number
    AND a.ro = b.ro
    AND round(a.flaghours, 2) <> round(b.flag_hours, 2)) 
-- because of multiple journal entries    
SELECT *
FROM acl_v1 a
WHERE control = '18037052'

-- instead of a JOIN DO EXISTS, don't need any of the values FROM acl_v1
    SELECT c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
    FROM acl_v1 a
    INNER JOIN dds.factRepairOrder b on a.control = b.ro
    INNER JOIN dds.dimtech c on b.techkey = c.techkey
    WHERE a.year_month = 201507
      AND a.department = 'body shop'
      AND a.journal IN ('sca','svi','swa')   
      AND a.control = '18037052'
    GROUP BY c.employeenumber, c.technumber 

SELECT c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
FROM dds.factRepairOrder b
INNER JOIN dds.dimtech c on b.techkey = c.techkey
WHERE b.ro IN (  
  SELECT control
  FROM acl_v1 
  WHERE year_month = 201507
    AND department = 'body shop'
    AND journal IN ('sca','svi','swa')   
  GROUP BY control) 
GROUP BY c.employeenumber, c.technumber  
    
-- costed does NOT match jeri's sheet/statement
-- yep, that did it
-- DROP TABLE #wtf1;
SELECT employeenumber, first_name, last_name, technumber, round(paid, 0) as paid,
  round(costed, 0) as costed, round(paid-costed, 0) AS adj
into #wtf1  
FROM (
  SELECT *
  -- SELECT SUM(paid)
  FROM (
    SELECT control, SUM(amount) AS paid
    FROM acl_v1 a
    WHERE a.year_month = 201507
      AND a.department = 'body shop'
      AND a.journal IN ('pay','gli')
    GROUP BY control) a   
  LEFT JOIN (
    SELECT employee_number, first_name, last_name
    FROM acl_census
    WHERE year_month = 201507
      AND department = 'body_shop'
    GROUP BY employee_number, first_name, last_name) b on a.control = b.employee_number
  LEFT JOIN (  
    SELECT c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
    FROM dds.factRepairOrder b
    INNER JOIN dds.dimtech c on b.techkey = c.techkey
    WHERE b.ro IN (  
      SELECT control
      FROM acl_v1 
      WHERE year_month = 201507
        AND department = 'body shop'
        AND journal IN ('sca','svi','swa')   
      GROUP BY control) 
    GROUP BY c.employeenumber, c.technumber) c on b.employee_number = c.employeenumber) d ORDER BY technumber    
*/

--SELECT employeenumber, first_name, last_name, technumber, round(paid, 0) as paid,
--  round(costed, 0) as costed, round(paid-costed, 0) AS adj
--into #wtf1  
SELECT store_Code, department, last_name, first_name, round(paid, 0) as paid,
  round(costed, 0) as costed, round(paid-costed, 0) AS adj
FROM (
  SELECT *
  -- SELECT SUM(paid)
  FROM (
    SELECT control, SUM(amount) AS paid
    FROM acl_v1 a
    WHERE a.year_month = 201507
--      AND a.department = 'body shop'
      AND a.journal IN ('pay','gli')
    GROUP BY control) a   
  LEFT JOIN (
    SELECT employee_number, first_name, last_name, store_code, department
    FROM acl_census
    WHERE year_month = 201507
--      AND department = 'body_shop'
    GROUP BY employee_number, first_name, last_name, store_code, department) b on a.control = b.employee_number
  LEFT JOIN (  
    SELECT c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
    FROM dds.factRepairOrder b
    INNER JOIN dds.dimtech c on b.techkey = c.techkey
    WHERE b.ro IN (  
      SELECT control
      FROM acl_v1 
      WHERE year_month = 201507
--        AND department = 'body shop'
        AND journal IN ('sca','svi','swa')   
      GROUP BY control) 
    GROUP BY c.employeenumber, c.technumber) c on b.employee_number = c.employeenumber) d     
ORDER BY store_code, department, last_name, first_name    


DROP TABLE #wtf;
--  SELECT store_code, department, last_name, first_name, round(paid, 0), 
--    coalesce(cast(round(costed, 0) AS sql_integer), 0),
--    round(paid, 0) - coalesce(cast(round(costed, 0) AS sql_integer), 0) AS adjusted

SELECT *
INTO #wtf
  FROM (
    SELECT store_code, department, control, SUM(amount) AS paid
    FROM acl_v1 a
    WHERE a.year_month = 201507
--      AND a.department = 'body shop'
      AND a.journal IN ('pay','gli')
    GROUP BY store_code, department, control) a   
  LEFT JOIN (
    SELECT employee_number, first_name, last_name
    FROM acl_census
    WHERE year_month = 201507
--      AND department = 'body_shop'
    GROUP BY employee_number, first_name, last_name) b on a.control = b.employee_number
  LEFT JOIN (  
    SELECT c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
    FROM dds.factRepairOrder b
    INNER JOIN dds.dimtech c on b.techkey = c.techkey
    WHERE b.ro IN (  
      SELECT control
      FROM acl_v1 
      WHERE year_month = 201507
--        AND department = 'body shop'
        AND journal IN ('sca','svi','swa')   
      GROUP BY control) 
    GROUP BY c.employeenumber, c.technumber) c on b.employee_number = c.employeenumber
    
    
SELECT * FROM #wtf WHERE last_name IS NULL 

    
    

alter PROCEDURE acl_get_people (
  year_month integer,
  store_code cichar(3),
  department cichar(12),
  store_code cichar(3) output, 
  department cichar(12) output,
  name cichar(40) output,
  paid integer output,
  costed integer output,
  adjusted integer output) 
BEGIN
/*
EXECUTE PROCEDURE acl_get_people(0,'RY1','mainshop');
*/
DECLARE @year_month integer;
DECLARE @store string;
DECLARE @department string;
@year_month = (SELECT year_month FROM __input);
IF @year_month = 0 THEN
  @year_month = (
    SELECT MAX(year_month)
    FROM acl_v1);
END IF;
@store = (SELECT store_code FROM __input);
@department = (
  SELECT 
    CASE
      WHEN (SELECT lower(department) FROM __input) = 'mainshop' THEN 'Main Shop'
      WHEN (SELECT lower(department) FROM __input) = 'bodyshop' THEN 'Body Shop'
      WHEN (SELECT lower(department) FROM __input) = 'detail' THEN 'Detail'
      WHEN (SELECT lower(department) FROM __input) = 'pdq' THEN 'PDQ'
    END
  FROM system.iota); 
INSERT INTO __output
  SELECT store_code, department, trim(first_name) + ' ' + last_name, cast(round(paid, 0) AS sql_integer), 
    coalesce(cast(round(costed, 0) AS sql_integer), 0),
    coalesce(cast(round(costed, 0) AS sql_integer), 0) - round(paid, 0)AS adjusted
  FROM (
    SELECT store_code, department, control, SUM(amount) AS paid
    FROM acl_v1 a
    WHERE a.year_month = @year_month
      AND store_code = @store
      AND a.department = @department
      AND a.journal IN ('pay','gli')
    GROUP BY store_code, department, control) a   
  LEFT JOIN (
    SELECT employee_number, first_name, last_name
    FROM acl_census
    WHERE year_month = @year_month
      AND store_code = @store
      AND department = @department
    GROUP BY employee_number, first_name, last_name) b on a.control = b.employee_number
  LEFT JOIN (  
    SELECT c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
    FROM dds.factRepairOrder b
    INNER JOIN dds.dimtech c on b.techkey = c.techkey
    WHERE b.ro IN (  
      SELECT control
      FROM acl_v1 
      WHERE year_month = @year_month
        AND store_code = @store
        AND department = @department
        AND journal IN ('sca','svi','swa')   
      GROUP BY control) 
    GROUP BY c.employeenumber, c.technumber) c on b.employee_number = c.employeenumber;
END;    
  
  
-- think this fixes the no name results (at least IN pdq)
-- make the base subselect on acl_census, NOT acl_V1
-- 10/5 body shop, july: jeremy rosenau IS NOT IN census (termed 6/25), but
--      has accrual reversal of -$733
--      so he shows up here with an amount AND no name (NOT IN census)
DECLARE @year_month integer;
DECLARE @store string;
DECLARE @department string;
@year_month = 201507;
@store = 'ry1';
@department = 'body shop';

  SELECT store_code, department, trim(first_name) + ' ' + last_name, 
    coalesce(cast(round(paid, 0) AS sql_integer), 0), 
    coalesce(cast(round(costed, 0) AS sql_integer), 0),
    coalesce(cast(round(costed, 0) AS sql_integer), 0) - round(paid, 0)AS adjusted
--select *    
FROM (
  SELECT store_code, department, employee_number, first_name, last_name
  FROM acl_census
  WHERE year_month = @year_month
    AND store_code = @store
    AND department = @department
  GROUP BY store_code, department, employee_number, first_name, last_name) a
LEFT JOIN (
  SELECT control, SUM(amount) AS paid
  FROM acl_v1 a
  WHERE a.year_month = @year_month
    AND store_code = @store
    AND a.department = @department
    AND a.journal IN ('pay','gli')
  GROUP BY control) b on a.employee_number = b.control
LEFT JOIN (  
  SELECT c.employeenumber, c.technumber, sum(b.flaghours * c.laborcost) AS costed
  FROM dds.factRepairOrder b
  INNER JOIN dds.dimtech c on b.techkey = c.techkey
  WHERE b.ro IN (  
    SELECT control
    FROM acl_v1 
    WHERE year_month = @year_month
      AND store_code = @store
      AND department = @department
      AND journal IN ('sca','svi','swa')   
    GROUP BY control) 
  GROUP BY c.employeenumber, c.technumber) c on a.employee_number = c.employeenumber
ORDER BY store_code, department, first_name    


-- trying to WORK it out
DECLARE @year_month integer;
DECLARE @store string;
DECLARE @department string;
@year_month = 201507;
@store = 'ry1';
@department = 'body shop';

SELECT a.year_month, a.store_code, a.department, a.employee_number, a.first_name, a.last_name, round(SUM(b.amount),0) AS paid
FROM acl_census a
INNER JOIN acl_v1 b on a.employee_number = b.control
  AND b.year_month = 201507--@year_month
  AND b.store_code = 'ry1'--@store
  AND b.department = 'body shop'--@department
  AND b.journal IN ('pay','gli')
WHERE a.year_month = 201507--@year_month
  AND a.store_code = 'ry1'--@store
  AND a.department = 'body shop'--@department
GROUP BY a.year_month, a.store_code, a.department, a.employee_number, a.first_name, a.last_name  

SELECT *
FROM acl_v1 b
WHERE b.year_month = 201507--@year_month
  AND b.store_code = 'ry1'--@store
  AND b.department = 'body shop'--@department
  AND b.journal IN ('pay','gli') ORDER BY amount
  
  
SELECT a.year_month, a.store_code, a.department, a.employee_number, a.first_name, a.last_name, b.amount
FROM acl_census a
INNER JOIN acl_v1 b on a.employee_number = b.control
  AND a.year_month = b.year_month
  AND a.store_code = b.store_code
  AND a.department = b.department
  AND b.journal IN ('pay','gli')
WHERE a.year_month = 201507--@year_month
  AND a.store_code = 'ry1'--@store
  AND a.department = 'body shop'--@department
GROUP BY a.year_month, a.store_code, a.department, a.employee_number, a.first_name, a.last_name, b.amount  
ORDER BY employee_ 