--DROP TABLE #247_pay; 
-- SELECT * FROM #247_pay
SELECT a.storecode, b.gtdate, a.gl_account, a.description, a.department, 
  gtctl# AS control, gtjrnl, gttamt AS amount
INTO #247_pay
FROM gmfs_accounts a
INNER JOIN  stgArkonaGLPTRNS b on a.gl_account = b.gtacct 
WHERE a.gm_account = '247'
  AND b.gtdate between '07/01/2015' AND '07/31/2015'
  AND b.gtjrnl IN ('PAY','GLI')   
-- looking for a natural key 
-- but it IS NOT here, overtime AND bonus are separate entries to the account
-- on the same day AS base pay
-- also accrual (GLI) debit & credit are both entered on the last day of the
-- month, don't want to lose the detail of accrual vs reversal, so can't DO
-- a grouping with SUM(amount)
-- the only thing that could WORK IS PK comprised of ALL fields, which IS NOT
-- very useful


-- added gttrn#, gtseq# for a natural key 
-- DROP TABLE #247_ro; 
-- accounting entries to the labor cost holding accounts; 247 (WIP)
-- SELECT * FROM #247_ro
SELECT a.storecode, b.gtdate, b.gttrn#, b.gtseq#, a.gl_account, a.description, a.department, 
  gtctl# AS control, gtjrnl, gttamt AS amount
INTO #247_ro
FROM gmfs_accounts a
INNER JOIN  stgArkonaGLPTRNS b on a.gl_account = b.gtacct
WHERE a.gm_account = '247'
  AND b.gtdate between '08/01/2015' AND '08/31/2015'
--  AND (gtjrnl = 'SVI' OR gtjrnl = 'SCA' OR gtjrnl = 'SWA') 
  AND gtjrnl IN ('svi','sca','swa')
-- so of course, this combination of attributes IS unique
-- but it IS virtually useless, those values relate to nothing ELSE
-- essentially a fucking surrogate key

-- this might be interesting
-- accounting entries are based on tech, payment type
-- so let's go down this liast rabbit hole
SELECT gl_account, control, amount, '', ''
FROM #247_ro
WHERE control = '16199855'
UNION all
SELECT '', ro, SUM(cost), paymenttypecode, technumber FROM (
SELECT ro, servicetypecode, paymenttypecode, technumber, name, laborcost, flaghours, laborcost*flaghours AS cost
FROM factrepairorder a
INNER JOIN dimservicetype b on a.servicetypekey = b.servicetypekey
INNER JOIN dimtech c on a.techkey = c.techkey
INNER JOIN dimpaymenttype d on a.paymenttypekey = d.paymenttypekey
WHERE ro = '16199855'
) x GROUP BY ro, paymenttypecode, technumber

-- make ro_1 more finely grained: tech, paymenttype, servicetype doesn't matter, i think

-- DROP TABLE #ro_1;
SELECT control, technumber, name, employeenumber, paymenttypecode, SUM(cost) AS ro_cost
INTO #ro_1
FROM (
  SELECT aa.thedate, a.ro AS control, technumber, name, employeenumber, c.paymenttypecode, 
    d.servicetypecode, flaghours, laborcost, e.opcode, f.socmth, f.socamt, LEFT(e.description, 40),
    CASE coalesce(socmth, 'B') -- opcode = N/A
      WHEN 'A' THEN socamt
      WHEN 'B' THEN 
        CASE -- technumber -- dimtech only has customer pay labor rate
          WHEN paymenttypecode = 'C' AND technumber = '999' THEN 0
--          WHEN e.opcode = '24Q' THEN 0
          WHEN technumber = '999' then flaghours * 17 -- 999 IS only tech w/out cust pay labor rate
          else coalesce(flaghours, 0) * coalesce(laborcost, 0)
        END 
    END AS cost
  --INTO #ro_1  
  FROM factrepairorder a
  INNER JOIN day aa on a.closedatekey = aa.datekey
  INNER JOIN dimtech b on a.techkey = b.techkey
  INNER JOIN dimpaymenttype c on a.paymenttypekey = c.paymenttypekey
  INNER JOIN dimservicetype d on a.servicetypekey = d.servicetypekey
  INNER JOIN dimopcode e on a.opcodekey = e.opcodekey
  LEFT JOIN stgArkonaSDPLOPC f on e.opcode = f.solopc
    AND e.storecode = f.soco#
  WHERE a.ro IN (
    SELECT control
    FROM #247_ro)) x 
GROUP BY control, technumber, name, employeenumber, paymenttypecode

DROP TABLE #matches_1;
SELECT *
INTO #matches_1
FROM #247_ro a
INNER JOIN #ro_1 b on a.control = b.control
  AND abs(round(ABS(a.amount),2) - round(ABS(b.ro_cost),2)) < .05


-- hmmm, i believe this IS a better match ratio
-- no matches
SELECT gl_account, description, COUNT(*), SUM(amount)
FROM #247_ro a
WHERE NOT EXISTS (
  SELECT 1
  FROM #matches_1
  WHERE control = a.control
    AND amount = a.amount)
GROUP BY gl_account, description

-- body shop no matches
SELECT a.*
FROM #247_ro a
LEFT JOIN #ro_1 b on a.control = b.control
  AND abs(round(ABS(a.amount),2) - round(ABS(b.ro_cost),2)) < .05
WHERE b.control IS NULL  
  AND gl_account = '124703'
  
-- GROUP them BY control  
DROP TABLE #matches_2;
SELECT *
INTO #matches_2
-- SELECT COUNT(*) 
FROM (
  SELECT a.control, SUM(amount) AS amount
  FROM #247_ro a
--  WHERE gl_account = '124703'
    WHERE NOT EXISTS (
      SELECT 1
      FROM #matches_1
      WHERE control = a.control
        AND amount = a.amount)
  GROUP BY control) c  
INNER JOIN #ro_1 d on c.control = d.control AND abs(round(ABS(c.amount),2) - round(ABS(d.ro_cost),2)) < .05
  AND ro_cost <> 0 
  AND NOT EXISTS (
    SELECT 1
    FROM #matches_1
    WHERE control_1 = d.control
      AND ro_cost = d.ro_cost);

-- can i get the account INTO #matches_2, probably NOT, goofy, sometimes accounting
-- IS split IN a way NOT mirrored on the ro
     
SELECT SUM(amount) FROM (      
SELECT control, amount FROM #matches_1    
UNION all
SELECT control, amount FROM #matches_2   
) x

SELECT * FROM #matches_1    
UNION all
SELECT control, amount FROM #matches_2   
SELECT SUM(amount) FROM #247_ro

SELECT * FROM #247_ro

SELECT SUM(ro_cost) FROM #matches_2 WHERE LEFT(control,2) = '18'

SELECT left(control,2), SUM(ro_cost) FROM #matches_2 GROUP BY left(control,2)

-- no matches to #247_ro
-- this IS WHERE i keep getting stuck, how DO i determine which rows IN 247_ro
have NOT been matched, how DO i extract the individual rows rows FROM 247_ro
that contributed to the grouped matches
so i can NOT determine (AND it seems that i need to) which costings are NOT
directly attributable to a tech

-- unmatched IN matches_1
SELECT *
FROM #247_ro a
WHERE NOT EXISTS (
  SELECT 1
  FROM #matches_1
  WHERE control = a.control
    AND amount = a.amount)

SELECT the_count, COUNT(*) FROM (    
SELECT a.control, COUNT(*) AS the_count
FROM #247_ro a    
INNER JOIN #matches_2 b on a.control = b.control
GROUP BY a.control
) x GROUP BY the_count


-- unmatched IN matches_1
SELECT control
FROM #247_ro a
WHERE NOT EXISTS (
  SELECT 1
  FROM #matches_1
  WHERE control = a.control
    AND amount = a.amount)