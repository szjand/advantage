select *
FROM dds.accrual_history a
LEFT JOIN dds.gmfs_accounts b on a.account = b.gl_account
WHERE control = ( -- '1112411'
  SELECT employeenumber
  FROM dds.edwEmployeeDim
  WHERE lastname = 'preston'
    AND firstname = 'april'
    AND currentrow = true)

-- sept accrual    
SELECT gtdate, gtctl#, gtref#, gtdesc, gttamt
FROM dds.stgArkonaGLPTRNS
WHERE gtctl# = '1112411'
--   AND gtacct in ('124724', '12524')   
  AND gtjrnl = 'gli'
 
    
-- sept accrual    
SELECT gtdate, gtctl#, gtref#, gtdesc, SUM(gttamt)
FROM dds.stgArkonaGLPTRNS
WHERE gtctl# = '1112411'
  AND gtacct in ('124724', '12524')   
  AND gtjrnl = 'gli'
GROUP BY gtdate, gtctl#, gtref#, gtdesc  

-- sept accrual    
SELECT gtdate, gtctl#, gtref#, gtdesc, SUM(gttamt)
FROM dds.stgArkonaGLPTRNS
WHERE gtjrnl = 'gli'
  AND gtdate = '09/30/2015'
  AND gttamt > 0
GROUP BY gtdate, gtctl#, gtref#, gtdesc  
ORDER BY gtctl#


SELECT first_name, last_name, employee_number, round(gross, 0) AS gross, 
  round([11/14 gross], 0) AS "11/14 gross", 
  cast(round(accrued_amount, 0) as sql_integer) AS accrued_amount, 
  cast(round(accrued_amount, 0) as sql_integer) - round([11/14 gross], 0) AS "Accrued - 11/14 Gross"
FROM (  
  SELECT a.first_name, a.last_name, a.employee_number, b.gross, 
    b.gross * 11/14 as "11/14 gross", d.accrued_amount
  FROM acl_census a
  LEFT JOIN acl_paychecks b on a.employee_number = b.employee_number
    AND b.pay_period_start_date = '09/20/2015'
  LEFT JOIN (
    SELECT gtdate, gtctl#, gtref#, gtdesc, SUM(gttamt) AS accrued_amount
    FROM dds.stgArkonaGLPTRNS
    WHERE gtjrnl = 'gli'
      AND gtdate = '09/30/2015'
      AND gttamt > 0
    GROUP BY gtdate, gtctl#, gtref#, gtdesc) d on a.employee_number = d.gtctl#    
  WHERE a.store_Code = 'ry1'
    AND a.department = 'detail'
    AND a.year_month = 201509
    AND coalesce(b.gross, 0) + coalesce(d.accrued_amount) > 0
  GROUP BY a.first_name, a.last_name, a.employee_number, b.gross, d.accrued_amount, 
    b.gross * 11/14) e  

ORDER BY first_name  
  
select *
FROM dds.gmfs_accounts  


SELECT gttrn#, gtdate, gtacct, gtctl#, gtref#, gtdesc, gttamt, b.*
FROM dds.stgArkonaGLPTRNS a
LEFT JOIN dds.gmfs_accounts b on a.gtacct = b.gl_account
WHERE gtctl# = '1112411'
AND gtdesc LIKE '%1009150'