-- page 2 Total Dealership
SELECT a.dl2, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '03/01/2015' AND '03/31/2015'
WHERE name = 'gmfs expenses'
  AND dl2 = 'RY1' -- ry1 only
GROUP BY a.dl2, a.al2, a.al3
ORDER BY MIN(line)
-- page 2 Variable
SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '03/01/2015' AND '03/31/2015'
WHERE name = 'gmfs expenses'
  AND dl3 = 'variable'
  AND dl2 = 'ry1'
GROUP BY a.dl2, dl3, a.al2, a.al3
ORDER BY MIN(line)
-- page 2 fixed
SELECT a.dl2, dl3, a.al2, a.al3, SUM(coalesce(b.gttamt,0)), MIN(line) AS line 
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND b.gtdate BETWEEN '03/01/2015' AND '03/31/2015'
WHERE name = 'gmfs expenses'
  AND dl3 = 'fixed'
  AND dl2 = 'ry1'
GROUP BY a.dl2, dl3, a.al2, a.al3
ORDER BY MIN(line)


SELECT *
FROM zcb
WHERE glaccount IN ('12203','12204','12204A','12204B','12204C','12224') 
  AND name = 'gmfs expenses'
  
  
select *
INTO #miss_acct
from stgArkonaFFPXREFDTA a
LEFT JOIN zcb b on a.fxgact = b.glaccount
  AND b.name = 'gmfs expenses'  
WHERE a.fxcyy = 2015
  AND a.fxcode = 'gm'
  AND LEFT(a.fxgact, 1) = '1'
  AND b.glaccount IS NULL   
  

SELECT *
FROM ( 
  select gtacct, SUM(gttamt)
  from stgArkonaGLPTRNS a
  where a.gtdate BETWEEN '03/01/2015' AND '03/31/2015'  
    AND gtacct IN (
      select fxgact
      from #miss_acct)  
  GROUP BY gtacct) c
left JOIN stgArkonaGLPMAST d on c.gtacct = d.gmacct
  AND d.gmyear = 2015  
WHERE d.gmtype = '8'  
--WHERE d.gmdesc LIKE '%CLERIC%'
ORDER BY gmdept, gtacct


/*
this IS interesting, USING FFPXREFDTA, these fucking queries are right on
so, perhaps, this can be the key to determing which accounts are missing
FROM zcb, zcb IS requ'd for the greater detail, particularily IN Fixed
*/
-- lines 4-6, variable expense  
SELECT SUM(c.gttamt)
FROM stgArkonaFFPXREFDTA a
LEFT JOIN stgArkonaGLPMAST b on a.fxgact = b.gmacct
  AND b.gmyear = 2015
LEFT JOIN stgArkonaGLPTRNS c on b.gmacct = c.gtacct
  AND c.gtdate BETWEEN '03/01/2015' AND '03/31/2015'  
WHERE fxcyy = 2015
  AND fxfact LIKE '01%'
  AND fxconsol = ''  
  
-- lines 8-16, personnel
SELECT SUM(c.gttamt)
FROM stgArkonaFFPXREFDTA a
LEFT JOIN stgArkonaGLPMAST b on a.fxgact = b.gmacct
  AND b.gmyear = 2015
LEFT JOIN stgArkonaGLPTRNS c on b.gmacct = c.gtacct
  AND c.gtdate BETWEEN '03/01/2015' AND '03/31/2015'  
WHERE fxcyy = 2015
  AND fxfact LIKE '02%'
  AND fxconsol = ''    
  
-- lines 18 - 39, semi fixed ($3 off)
SELECT SUM(c.gttamt)
FROM stgArkonaFFPXREFDTA a
LEFT JOIN stgArkonaGLPMAST b on a.fxgact = b.gmacct
  AND b.gmyear = 2015
LEFT JOIN stgArkonaGLPTRNS c on b.gmacct = c.gtacct
  AND c.gtdate BETWEEN '03/01/2015' AND '03/31/2015'  
WHERE fxcyy = 2015
  AND (fxfact LIKE '05%' OR fxfact LIKE '06%' OR fxfact LIKE '07%' OR fxfact LIKE '03%')
  AND fxconsol = ''  

-- lines 41 - 54 fixed ($2 off)
SELECT SUM(c.gttamt)
FROM stgArkonaFFPXREFDTA a
LEFT JOIN stgArkonaGLPMAST b on a.fxgact = b.gmacct
  AND b.gmyear = 2015
LEFT JOIN stgArkonaGLPTRNS c on b.gmacct = c.gtacct
  AND c.gtdate BETWEEN '03/01/2015' AND '03/31/2015'  
WHERE fxcyy = 2015
  AND (fxfact LIKE '08%' OR fxfact LIKE '09%')
  AND fxconsol = ''   
  
-- so, let's start with personnel
-- lines 8-16, personnel
-- those rows with line = NULL are the accounts missing FROM zcb
SELECT m.*, n.line
FROM (
  SELECT a.fxgact, a.fxfact, b.gmdept, b.gmdesc, SUM(c.gttamt) AS amount
  FROM stgArkonaFFPXREFDTA a
  LEFT JOIN stgArkonaGLPMAST b on a.fxgact = b.gmacct
    AND b.gmyear = 2014
  LEFT JOIN stgArkonaGLPTRNS c on b.gmacct = c.gtacct
    AND c.gtdate BETWEEN '03/01/2015' AND '03/31/2015'   
  WHERE fxcyy = 2015
    AND fxfact LIKE '02%'  
    AND fxconsol = ''
  GROUP BY a.fxgact, a.fxfact, b.gmdept, b.gmdesc) m   
LEFT JOIN zcb n on m.fxgact = n.glaccount  
  AND n.name = 'gmfs expenses' 
WHERE coalesce(abs(m.amount)) <> 0      


-- lines 8-16, personnel
-- DROP TABLE #pers;
SELECT a.fxgact, fxfact, b.gmdept, b.gmdesc, c.gtctl#, SUM(c.gttamt) AS amount
INTO #pers
FROM stgArkonaFFPXREFDTA a
LEFT JOIN stgArkonaGLPMAST b on a.fxgact = b.gmacct
  AND b.gmyear = 2015
LEFT JOIN stgArkonaGLPTRNS c on b.gmacct = c.gtacct
  AND c.gtdate BETWEEN '03/01/2015' AND '03/31/2015'  
WHERE fxcyy = 2015
  AND fxfact LIKE '02%'
  AND fxconsol = ''   
GROUP BY a.fxgact, fxfact, b.gmdept, b.gmdesc, c.gtctl#
HAVING SUM(c.gttamt) <> 0

select a.*, b.ymname 
FROM #pers a
LEFT JOIN stgArkonaPYMAST b on a.gtctl# = b.ymempn

-- looks good except for parts
SELECT gmdept, SUM(amount)
FROM #pers
GROUP BY gmdept


-- 23f IS short $2800 line 11 Other Salaries AND Wages
SELECT fxfact, SUM(amount)
FROM #pers
WHERE gmdept = 'pd' OR fxgact = '12306E'
GROUP BY fxfact

-- this IS missing acct 12306E
SELECT fxgact, fxfact, gmdesc, SUM(amount)
FROM #pers
WHERE gmdept = 'pd' OR fxgact = '12306E'
GROUP BY fxgact, fxfact, gmdesc
-- because it IS categorized AS service
-- jeri says she will fix it
SELECT *
FROM #pers
WHERE fxgact = '12306E'


-- 4/23/15
select * FROM #pers

SELECT DISTINCT right(TRIM(fxfact), 1)
FROM #pers

SELECT *
FROM #pers
WHERE right(TRIM(fxfact), 1) = 'Z'

SELECT fxgact, fxfact, gmdept, gmdesc
FROM #pers
WHERE right(TRIM(fxgact), 1) NOT IN ('1','2','3','4','5','6','7','8','9')
GROUP BY fxgact, fxfact, gmdept, gmdesc
ORDER BY  right(TRIM(fxfact), 1), right(TRIM(fxgact), 1)

subaccounts (letter suffix)

gm  dept      
A   new
B   used
C   Rental
D   Service
E   Body
F   Parts
Z   Cartiva

Arkona
G     Maintenance
H      HR/IT

