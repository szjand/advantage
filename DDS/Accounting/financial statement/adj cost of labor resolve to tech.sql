an attempt to take the adjustedd cost of labor FROM the fs, break it down BY tech BY ro

on FS: Mech: P6 - L30
       Body; P6 - L39
       
too fucking many mind fuck landmines doing past shit (why was hunter nelsons
apr 24 commission/bonus posted to 12301)

DO july  
       
-- the accounts       
select a.*
FROM gmfs_accounts a
WHERE a.page = 16 
  AND a.line IN (30,39) 
  
-- july adjusted cost, detail  
select a.*, b.gttamt
FROM dds.gmfs_accounts a
INNER JOIN dds.stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.page = 16 
  AND a.line IN (30,39) 
  
-- july adjusted cost, totals
SELECT storecode, gl_account, department, SUM(gttamt)
FROM (
  select storecode, a.gl_account,
    CASE department
      WHEN 'BS' THEN 'BS'
      ELSE 'SD'
    END AS department,
    b.gttamt
  FROM dds.gmfs_accounts a
  INNER JOIN dds.stgArkonaGLPTRNS b on a.gl_account = b.gtacct
    AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  WHERE a.page = 16 
    AND a.line IN (30,39)) c
GROUP BY storecode, gl_account, department   


SELECT * 
FROM gmfs_accounts
WHERE gm_account LIKE '247%'

-- 247 controlled BY ro, except, WHEN it IS controlled BY employeenumber
select a.*, b.gtdate, b.gtctl#, b.gtdoc#, b.gttamt
FROM gmfs_accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.gl_account = '124702'
ORDER BY gtdate

-- ron adler, detail tech
SELECT gtdate, gtjrnl, gtacct, gtctl#, gtdoc#, gtdesc, gttamt, b.*
FROM stgArkonaGLPTRNS a
LEFT JOIN gmfs_accounts b on a.gtacct = b.gl_account
WHERE gtctl# = '11687'
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
ORDER BY gtacct   
ORDER BY gtdate, gtacct
  
so, it IS looking LIKE the SUM of 124702 AND 124724 should give me the
adj cost of labor 166524  

-- 124702: INV WIP LABOR WASH BAY: controlled BY RO
select a.*, b.gtdate, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt, b.gtdesc
FROM gmfs_accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.gl_account = '124702'
  AND gtjrnl <> 'STD' -- adjusted cost of labor
ORDER BY gtjrnl

select gtacct, SUM(gttamt) AS amount
FROM dds.gmfs_accounts a
LEFT JOIN dds.stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.gl_account = '124702'
  AND gtjrnl <> 'STD' -- adjusted cost of labor
GROUP BY gtacct

-- 124724: WIP LABOR WASH BAY: controlled BY emp #
select a.*, b.gtdate, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt, b.gtdesc
FROM gmfs_accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.gl_account = '124724'
  AND gtjrnl <> 'STD'
ORDER BY gtdate

select gtacct, 
  SUM(case when gtjrnl = 'PAY' then gttamt end) AS PAY,
  SUM(CASE WHEN gtjrnl = 'GLI' THEN gttamt END) AS ACCRUAL
FROM gmfs_accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.gl_account = '124724'
  AND gtjrnl <> 'STD'
GROUP BY gtacct

-- 166524: ADJ CST LBR RECON
select a.*, b.gtdate, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt, b.gtdesc
FROM gmfs_accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.gl_account = '166524'
ORDER BY gtdate


-- 124702: INV WIP LABOR WASH BAY: controlled BY RO
select a.*, b.gtdate, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt, b.gtdesc
FROM gmfs_accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.gl_account = '124702'
  AND gtjrnl <> 'STD' -- adjusted cost of labor
ORDER BY gtjrnl

-- a little mind fucky here,
-- i sense the maelstrom, fuck sales, what we are dealing with here IS 
-- costing vs paid
SELECT a.ro, a.line, a.roflaghours, a.rolaborsales, a.flaghours, a.laborsales, 
  a.techkey, b.employeenumber, b.name, b.technumber, b.laborcost, b.flagdeptcode
FROM factRepairOrder a
INNER JOIN dimtech b on a.techkey = b.techkey
WHERE ro IN (
  select gtctl#
  FROM gmfs_accounts a
  LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
    AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  WHERE a.gl_account = '124702'
    AND gtjrnl <> 'STD')
ORDER BY ro, line   

-- so GROUP BY ro, techkey, get an amount per ro/tech that might match an amount/ro
-- FROM 124702 
SELECT a.ro, sum(a.flaghours) AS flaghours, sum(a.laborsales) AS laborsales, 
  a.techkey, b.employeenumber, b.name, b.technumber, b.laborcost, flagdeptcode
FROM factRepairOrder a
INNER JOIN dimtech b on a.techkey = b.techkey
WHERE flaghours <> 0
  AND ro IN (
    select gtctl#
    FROM gmfs_accounts a
    LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
      AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
    WHERE a.gl_account = '124702'
      AND gtjrnl <> 'STD')  
GROUP BY a.ro, a.techkey, b.employeenumber, b.name, b.technumber, b.laborcost, flagdeptcode

-- so the above IS interesting
-- look at ro 16196988, which has 2 detail techs, which IS NOT unusual, 2 techs on a detail
-- this IS totally mind fucky
select a.*, b.gtdate, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt, b.gtdesc
FROM gmfs_accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
WHERE a.gl_account = '124702'
  AND gtjrnl <> 'STD' -- adjusted cost of labor
  AND gtctl# = '16196988'
ORDER BY gtjrnl


-- different tack
-- start out with grouping BY emp#
DROP TABLE adj_cost_labor_pay;
CREATE TABLE adj_cost_labor_pay (
  yearmonth integer constraint NOT NULL,
  employee_number cichar(9) constraint NOT NULL,
  name cichar(25) constraint NOT NULL,
  pay numeric(12,2) default '0' constraint NOT NULL,
  accrual  numeric(12,2) default '0' constraint NOT NULL,
  accrual_reversal numeric(12,2) default '0' constraint NOT NULL,
  constraint pk primary key (yearmonth, employee_number)) IN database;
-- 124724: WIP LABOR WASH BAY: controlled BY emp #
--select a.*, b.gtdate, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt, b.gtdesc
INSERT INTO adj_cost_labor_pay
select 201507, gtctl#, left(trim(firstname) + ' ' + lastname, 25) AS name,
  SUM(case when gtjrnl = 'PAY' then gttamt end) AS PAY,
  coalesce(SUM(CASE WHEN gtjrnl = 'GLI' and gttamt > 0 THEN gttamt END), 0) AS ACCRUAL,
  coalesce(SUM(CASE WHEN gtjrnl = 'GLI' and gttamt < 0 THEN gttamt END), 0) AS ACCRUAL_REV
FROM stgArkonaGLPTRNS b 
LEFT JOIN edwEmployeeDim c on b.gtctl# = c.employeenumber
  AND c.currentrow = true
WHERE gtacct = '124724'
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  AND gtjrnl <> 'STD'
GROUP BY trim(firstname) + ' ' + lastname, gtctl#;

SELECT * FROM adj_cost_labor_pay

-- 124702: INV WIP LABOR WASH BAY: controlled BY RO
select a.*, b.gtdate, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt, b.gtdesc
FROM gmfs_accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.gl_account = '124702'
  AND gtjrnl <> 'STD' -- adjusted cost of labor
  AND flaghours <> 0

-- trying to DO it ALL IN one, feels LIKE too much  
select b.gtdate, b.gtjrnl, b.gtctl#, b.gttamt, c.ro, c.line, c.techkey, c.flaghours
FROM stgArkonaGLPTRNS b 
LEFT JOIN factRepairOrder c on b.gtctl# = c.ro
  AND c.flaghours <> 0
INNER JOIN dimPaymentType d on c.paymenttypekey = d.paymenttypekey  
  AND 
    CASE b.gtjrnl
      WHEN 'SCA' THEN d.paymentTypeCode = 'C'
      WHEN 'SVI' THEN d.paymentTypeCode = 'I'
      WHEN 'SWA' THEN d.paymentTypeCode = 'W'
    END 
WHERE gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  AND gtacct = '124702'
  AND gtjrnl <> 'STD' -- adjusted cost of labor

SELECT * FROM dimPaymentType
SELECT * FROM dimServiceType
-- there are no ros with payment type of service contract AND service type of RE
SELECT * FROM factrepairorder WHERE paymenttypekey = 4 and servicetypekey = 10 AND year(rocreatedts) = 2015
 
 
  -- what DO i DO about non employee flaghours
DROP TABLE #ro_1;  
SELECT a.ro, a.line, a.roflaghours, a.rolaborsales, a.flaghours, a.laborsales, 
  a.techkey, b.employeenumber, b.name, b.technumber, b.laborcost, b.flagdeptcode
INTO #ro_1  
FROM factRepairOrder a
INNER JOIN dimtech b on a.techkey = b.techkey
WHERE ro IN (
  select gtctl#
  FROM gmfs_accounts a
  LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
    AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  WHERE a.gl_account = '124702'
    AND gtjrnl <> 'STD')
AND flaghours <> 0;    
-- ORDER BY ro, line 

SELECT * FROM #ro_1 



  
  
  
  
SELECT * FROM adj_cost_labor_pay  
  
  
DROP TABLE #ro_1;  
SELECT gtctl# AS ro, gtjrnl, gttamt AS cost
INTO #ro_1
-- SELECT SUM(gttamt)
-- SELECT *
FROM stgArkonaGLPTRNS
WHERE gtdate BETWEEN '07/01/2015' AND '07/31/2015'
  AND gtacct = '124702'
  AND gtjrnl <> 'STD'; 

SELECT * FROM #ro_1 

SELECT * FROM adj_cost_labor_pay  
  

trying to be careful AND NOT prematurely eliminate data based on assumptions, such
AS ALL detail WORK will be service type RE, that may be true, but DO NOT assert 
it with filtering 

i DO want to GROUP BY tech, ie, the accounting entries will be at the grain of ro/tech 

-- aha need to GROUP the costing IN ro2
-- take flag hours out of the grouping
-- comparing amounts requires round AND abs
DROP TABLE #ro_2;
SELECT a.ro, technumber, name, employeenumber, c.paymenttypecode, 
  d.servicetypecode, b.laborcost, sum(flaghours * laborcost) AS costing
INTO #ro_2  
-- SELECT *
FROM factrepairorder a
INNER JOIN dimtech b on a.techkey = b.techkey
INNER JOIN dimpaymenttype c on a.paymenttypekey = c.paymenttypekey
INNER JOIN dimservicetype d on a.servicetypekey = d.servicetypekey
WHERE a.flaghours <> 0
AND a.ro IN (
  SELECT ro
  FROM #ro_1)
GROUP BY a.ro, technumber, name, employeenumber, c.paymenttypecode, 
  d.servicetypecode, b.laborcost
  
select *
FROM #ro_1 e
LEFT JOIN #ro_2 f on e.ro = f.ro --AND abs(round(e.cost, 2)) = abs(round(f.costing, 2))
ORDER BY e.ro  
  
-- aha need to GROUP the costing IN ro2
  
  
  
----------------------------------------------------------------------------------------------------------------------- 
-- #ro_1  include ALL the accounts
-- was HAVING problems matching up, greg suggested i was doing premature optimization 
-- anyway, let us see what we can come up with

DROP TABLE #ro_1; 
SELECT a.storecode, a.gl_account, a.description, a.department, gtctl# AS control, gtjrnl, gttamt AS amount
INTO #ro_1
-- SELECT SUM(gttamt)
-- SELECT *
FROM gmfs_accounts a
LEFT JOIN  stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate between '06/01/2015' AND '06/30/2015'
  AND gtjrnl <> 'STD'      
WHERE a.gm_account = '247'

select * FROM #ro_1 WHERE gl_Account = '124724' 

SELECT DISTINCT account_type FROM #ro_1

-- aha need to GROUP the costing IN ro2
-- take flag hours out of the grouping
-- comparing amounts requires round AND abs
DROP TABLE #ro_2;
SELECT a.ro AS control, technumber, name, employeenumber, c.paymenttypecode, 
  d.servicetypecode, sum(flaghours * laborcost) AS amount
INTO #ro_2  
-- SELECT *
FROM factrepairorder a
INNER JOIN dimtech b on a.techkey = b.techkey
INNER JOIN dimpaymenttype c on a.paymenttypekey = c.paymenttypekey
INNER JOIN dimservicetype d on a.servicetypekey = d.servicetypekey
WHERE a.flaghours <> 0
AND a.ro IN (
  SELECT control
  FROM #ro_1)
GROUP BY a.ro, technumber, name, employeenumber, c.paymenttypecode, 
  d.servicetypecode
  
SELECT technumber, name, control, SUM(amount)
-- SELECT SUM(amount)
FROM #ro_1 a
LEFT JOIN dimtech b on a.control = b.employeenumber
  AND b.currentrow = true
  AND b.active = true
WHERE gl_Account = '124724'
GROUP BY technumber, name, control

-- this does NOT seem good enuf
-- even though it gets the flag hours for ALL techs that got paid (124724)
-- it does NOT include ros flagged to dept techs
SELECT technumber, name, round(sum(flaghours),2) AS flaghours
FROM factRepairOrder a
INNER JOIN dimtech b on a.techkey = b.techkey
WHERE flaghours <> 0
  AND ro IN (
    SELECT control
    FROM #ro_1)
  AND a.techkey IN (
    SELECT techkey
    FROM dimtech
    WHERE employeenumber IN (
      SELECT control
      FROM #ro_1
      WHERE gl_account = '124724'))    
GROUP BY technumber, name    

-- this gives me ALL the ros that have an entry IN 124702
SELECT control
FROM #ro_1
WHERE gl_account = '124702'



select *
FROM #ro_1


 
SELECT *
FROM #ro_1 a WHERE gl_Account = '124724'
LEFT JOIN #ro_2 b on a.control = b.control
WHERE a.gtjrnl not IN ('svi','sca','swa')
ORDER BY a.control

SELECT *


SELECT technumber, name, employeenumber, SUM(costing) AS costing
FROM ( 
  select *
  FROM #ro_1 a
  LEFT JOIN #ro_2 b on a.ro = b.ro AND abs(round(a.cost, 2)) = abs(round(b.costing, 2))
  WHERE a.gtjrnl IN ('svi','sca','swa')
    AND b.servicetypecode = 're') x
GROUP BY technumber, name, employeenumber
  
SELECT * 
FROM adj_cost_labor_pay g
LEFT JOIN (
SELECT technumber, name, employeenumber, SUM(costing) AS costing
FROM ( 
  select *
  FROM #ro_1 a
  LEFT JOIN #ro_2 b on a.ro = b.ro AND abs(round(a.cost, 2)) = abs(round(b.costing, 2))
  WHERE a.gtjrnl IN ('svi','sca','swa')
    AND b.servicetypecode = 're') x
GROUP BY technumber, name, employeenumber) h on g.employee_number = h.employeenumber
  
SELECT SUM(pay + accrual + coalesce(accrual_reversal, 0)) AS pay, SUM(coalesce(costing,0))
--select *
FROM (
SELECT * 
FROM adj_cost_labor_pay g
LEFT JOIN (
SELECT technumber, name, employeenumber, SUM(costing) AS costing
FROM ( 
  select *
  FROM #ro_1 a
  LEFT JOIN #ro_2 b on a.ro = b.ro AND abs(round(a.cost, 2)) = abs(round(b.costing, 2))
  WHERE a.gtjrnl IN ('svi','sca','swa')
    AND b.servicetypecode = 're') x
GROUP BY technumber, name, employeenumber) h on g.employee_number = h.employeenumber) x


SELECT *
FROM #ro_1
WHERE gtjrnl NOT IN ('svi','sca','swa') 

SELECT gl_account, description, SUM(cost)
FROM #ro_1
WHERE gtjrnl NOT IN ('svi','sca','swa')
GROUP BY gl_account, description
ORDER BY gl_Account



/*  jeri looking INTO   
-- why does the commission/bonus NOT show up:  4/24 582 + 250 
-- some are going to 12301 instead of 124724 - jeri looking INTO it
select a.*, b.gtdate, b.gtjrnl, b.gtctl#, b.gtdoc#, b.gttamt, b.gtdesc
FROM gmfs_accounts a
LEFT JOIN stgArkonaGLPTRNS b on a.gl_account = b.gtacct
  AND gtdate BETWEEN '07/01/2015' AND '07/31/2015'
WHERE a.gl_account = '124724'
  AND gtjrnl <> 'STD'
  AND gtctl# = '1100800'
*/

/*
op codes AND costing method
SELECT * FROM gmfs_accounts WHERE gm_account LIKE '247%'

select *
FROM dimopcode a
LEFT JOIN stgArkonaSDPLOPC b on a.opcode = b.solopc AND a.storecode = b.soco#
WHERE opcode = 'ndel'

SELECT *
FROM stgArkonaSDPLOPC
WHERE solopc = 'ndel'

SELECT sormth, soramt, socmth, socamt, COUNT(*)
FROM stgArkonaSDPLOPC
GROUP BY sormth, soramt, socmth, socamt

SELECT soco#, solopc, sodes1, sodes2, sormth, soramt, socmth, socamt, soestm
FROM stgArkonaSDPLOPC
WHERE socmth = 'a'


SELECT soco#, solopc, sodes1, sormth, soramt, socmth, socamt, soestm, soehrs, 
  sopmth, sopamt, b.*
FROM stgArkonaSDPLOPC a
INNER JOIN dimopcode b on a.soco# = b.storecode AND a.solopc = b.opcode
WHERE sormth = 'a'
  AND soramt <> 0
  AND soestm = 'Y'
  AND soco# = 'RY1'  
  AND soehrs <> 0
  AND a.solopc = 'nitropdi'
  
  
SELECT * FROM dimtech  WHERE technumber = 'd23'

SELECT soco#, solopc, sodes1, sormth, soramt, socmth, socamt, soestm, soehrs, 
  sopmth, sopamt, b.*
--SELECT b.opcodekey
FROM stgArkonaSDPLOPC a
INNER JOIN dimopcode b on a.soco# = b.storecode AND a.solopc = b.opcode
WHERE solopc = 'nitropdi'
WHERE sormth = 'a'
  AND soramt <> 0
  AND soestm = 'Y'
  AND soco# = 'RY1'  
  AND soehrs <> 0
  

Retail Method: (SORMTH :: RETAIL_METHOD)
 A=Actual - Use this method to override the computed retail labor charge 
          (labor hours x labor rate) and make the retail labor charge for 
          this labor op code equal to the Retail Amount.
 B=Book - Use this method to compute the retail labor charge as (labor hours x labor rate).
 C=Other Charge (Not in use at this time.) 

Retail Amount (SORAMT ::RETIAIL AMOUNT)
       When used in connection with Retail Method A, the dollar amount 
       entered here will override the computed retail 
       labor charge (labor hours x labor rate) and become the Actual 
       Retail Amount of the labor. Please note that unless the pay type is 
       customer pay, the Retail Amount must be greater than 0.0. 
       (If there will not be a charge for this labor operation, set the Retail Amount to 0.01.) 
       
Cost Method (SOCMTH :: COST_METHOD)
     Defines how the labor cost is computed. 
     Choose from the drop-down Retail or Cost Method Selection Screen. 
     Valid methods for this field include: 
      A=Actual - Use this method to override the computed labor cost 
               (labor hours x tech hourly rate) and make the labor cost 
               for this labor op code equal to the Cost Amount.
      B=Book - Use this method to compute the labor cost as (labor hours x tech hourly rate).
      P=Percent - Use this method to compute the labor cost as a percent 
                of the retail labor charge. When using this method, 
                enter a percent in the Cost Amount field. Percentages should 
                be entered in whole numbers with 
                decimals, e.g., 50% would be entered as 50.00.
                
Cost Amount (SOCAMT :: COST_AMOUNT)
     When used in connection with Cost Method A, the dollar amount 
     entered here will become the cost of labor. When used in connection 
     with Cost Method P this is the percent of the retail labor charge 
     that is the labor cost.   
     
SELECT soco#, solopc, sodes1, sormth, soramt, socmth, socamt, soestm, soehrs, b.*
--SELECT b.opcodekey
FROM stgArkonaSDPLOPC a
INNER JOIN dimopcode b on a.soco# = b.storecode AND a.solopc = b.opcode
WHERE  a.socmth = 'A'     
ORDER BY solopc

SELECT distinct a.storecode, a.ro, b.thedate, c.opcode, c.description, a.flaghours
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey 
WHERE b.theyear = 2015
  AND LEFT(ro,2) <> '19'
  AND a.storecode = 'ry1'
  AND a.opcodekey IN (
    SELECT b.opcodekey
    FROM stgArkonaSDPLOPC a
    INNER JOIN dimopcode b on a.soco# = b.storecode AND a.solopc = b.opcode
    WHERE a.socmth = 'A')               
        
 */