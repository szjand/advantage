/*
this IS good fo amt paid
get market
get store
get department
get peepsi
*/
DECLARE @year_month integer;
DECLARE @days_in_month integer;
DECLARE @min_date date;
DECLARE @max_date date;
@year_month = 201507;
@days_in_month = (
  SELECT COUNT(*)
  FROM dds.day
  WHERE yearmonth = @year_month);  
@min_date = (
  SELECT MIN(thedate)
  FROM dds.day
  WHERE yearmonth = @year_month);
@max_date = (
  SELECT MAX(thedate)
  FROM dds.day
  WHERE yearmonth = @year_month);  

--SELECT @year_month, employee_number, first_name, last_name, store_code, department, 
--  payroll_class, SUM(adj_gross) AS gross_month
SELECT @year_month, store_code, department, SUM(adj_gross)
FROM ( 
  SELECT a.*, b.*,
    CASE 
      WHEN pay_period_start_date < @min_date THEN 
        round(gross * dayofmonth(pay_period_end_date)/@days_in_month, 2) 
      WHEN pay_period_end_date > @max_date THEN 
        round(gross * (1 + (@days_in_month - dayofmonth(pay_period_start_date)))/@days_in_month, 2)
      ELSE gross
    END AS adj_gross
  FROM acl_paychecks a
  INNER JOIN (
    SELECT DISTINCT employee_number, first_name, last_name, store_code, department,
      payroll_class
    FROM acl_census) b on a.employee_number = b.employee_number
  WHERE pay_period_end_date >= @min_date --'08/01/2015'
    AND pay_period_start_date <= @max_date) x -- '09/01/2015'
-- GROUP BY employee_number, first_name, last_name, store_code, department, payroll_class
GROUP BY store_code, department
--ORDER BY store_code, department, payroll_class
*/

/*
costing FROM accounting
market 
store
dept
employee
*/


-- added gttrn#, gtseq# for a natural key 
-- DROP TABLE #247_ro; 
-- accounting entries to the labor cost holding accounts; 247 (WIP)
-- SELECT * FROM #247_ro
SELECT a.storecode, b.gtdate, b.gttrn#, b.gtseq#, a.gl_account, a.description, a.department, 
  gtctl# AS control, gtjrnl, gttamt AS amount
--INTO #247_ro
FROM dds.gmfs_accounts a
INNER JOIN dds.stgArkonaGLPTRNS b on a.gl_account = b.gtacct
WHERE a.gm_account = '247'
  AND b.gtdate between '08/01/2015' AND '08/31/2015'
--  AND (gtjrnl = 'SVI' OR gtjrnl = 'SCA' OR gtjrnl = 'SWA') 
  AND gtjrnl IN ('svi','sca','swa')
-- so of course, this combination of attributes IS unique
-- but it IS virtually useless, those values relate to nothing ELSE
-- essentially a fucking surrogate key

-- this IS much faster
select *
from dds.stgArkonaGLPTRNS a
WHERE a.gtdate between '07/01/2015' AND '07/31/2015'
AND gtpost = 'Y'
AND gtjrnl IN ('svi','sca','swa')
AND gtacct IN (
  SELECT gl_account
  FROM dds.gmfs_accounts
  WHERE gm_account = '247')
  
  
SELECT month(gtdate), gtacct, SUM(gttamt) 

SELECT 
  CASE gtacct
    WHEN '124700' THEN 'main_shop'
    WHEN '124703' THEN 'body_shop'
    WHEN '124701' THEN 'pdq'
    WHEN '124702' THEN 'detail'
    WHEN '224700' THEN 'ry2_main_shop'
    WHEN '224701' THEN 'ry2_pdq'
  END AS dept,
  SUM(CASE WHEN month(gtdate) = 7 THEN gttamt END) AS jul,
  SUM(CASE WHEN month(gtdate) = 8 THEN gttamt END) AS aug,
  SUM(CASE WHEN month(gtdate) = 9 THEN gttamt END) AS sep
-- select *
from dds.stgArkonaGLPTRNS a
WHERE a.gtdate between '07/01/2015' AND curdate() - 1
AND gtpost = 'Y'
AND gtjrnl IN ('svi','sca','swa')
AND gtacct IN (
  SELECT gl_account
  FROM dds.gmfs_accounts
  WHERE gm_account = '247')
GROUP BY month(gtdate), gtacct  