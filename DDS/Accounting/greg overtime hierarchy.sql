
  try
    drop table #phpGetOTHoursBetweenDates;
    catch ADS_SCRIPT_EXCEPTION
    if __ErrCode = 7112 then
    end if;
  end try;
  
  select case StoreCode when 'RY1' then '1-Chevy' when 'RY2' then '2-Honda' when 'RY3' then '3-Crkstn' end as "Store", min(TheDate) as TimeFrame, PyDept as Department, ManagerName as Manager, Name, 
    round(sum(ClockHours), 0) as ClockHours, round(sum(RegularHours), 0) as RegularHours, round(sum(OvertimeHours), 0) as OTHours 
  into #phpGetOTHoursBetweenDates
  from Day a
  inner join edwClockHoursFact b on a.DateKey = b.DateKey and TheYear = 2012
  left join edwEmployeeDim c on b.EmployeeKey = c.EmployeeKey
  where TheYear = 2012  
  group by StoreCode, YearWeek, PyDept, ManagerName, Name
  having sum(ClockHours) > 0
  order by case StoreCode when 'RY1' then '1-Chevy' when 'RY2' then '2-Honda' when 'RY3' then '3-Crkstn' end, min(TheDate) desc, PyDept, ManagerName, Name;
  
  
  try drop table #preLevel1; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #Level1; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #preLevel2; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #Level2; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #preLevel3; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #Level3; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #preLevel4; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #Level4; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #preLevel5; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #Level5; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #preLevel6; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  try drop table #Level6; catch ADS_SCRIPT_EXCEPTION if __ErrCode = 7112 then end if; end try;
  select Store, 'TimeFrame' as TimeFrame, 'Department' as Department, 'Manager' as Manager, 'Name' as Name, 
    sum(ClockHours) as ClockHours, sum(RegularHours) as RegularHours, sum(OTHours) as OTHours, 'H1' as Class 
  into #preLevel1 
  from #phpGetOTHoursBetweenDates
  group by Store;
  select 
    (select count(*) as Level from #preLevel1 b where b.Store <= a.Store) as Level, 
    a.Store, a.TimeFrame, a.Department, a.Manager, a.Name, a.ClockHours, a.RegularHours, a.OTHours, a.Class 
  into #Level1
  from #preLevel1 a;

  select Store, TimeFrame, 'Department' as Department, 'Manager' as Manager, 'Name' as Name, 
    sum(ClockHours) as ClockHours, sum(RegularHours) as RegularHours, sum(OTHours) as OTHours, 'H2' as Class 
  into #preLevel2
  from #phpGetOTHoursBetweenDates
  group by Store, TimeFrame;
  select 
    (select Level from #Level1 c where c.Store = a.Store) as Level1, 
    (select count(*) as Level from #preLevel2 b where b.Store = a.Store and b.TimeFrame <= a.TimeFrame) as Level2,  
    a.Store, a.TimeFrame, a.Department, a.Manager, a.Name, a.ClockHours, a.RegularHours, a.OTHours, a.Class 
  into #Level2
  from #preLevel2 a;

  select Store, TimeFrame, Department, 'Manager' as Manager, 'Name' as Name, 
    sum(ClockHours) as ClockHours, sum(RegularHours) as RegularHours, sum(OTHours) as OTHours, 'H3' as Class 
  into #preLevel3
  from #phpGetOTHoursBetweenDates
  group by Store, TimeFrame, Department;
  select 
    (select Level from #Level1 c where c.Store = a.Store) as Level1, 
    (select Level2 from #Level2 c where c.Store = a.Store and c.TimeFrame = a.TimeFrame) as Level2,
    (select count(*) as Level from #preLevel3 b where b.Store = a.Store and b.TimeFrame = a.TimeFrame and b.Department <= a.Department) as Level3,  
    a.Store, a.TimeFrame, a.Department, a.Manager, a.Name, a.ClockHours, a.RegularHours, a.OTHours, a.Class 
  into #Level3
  from #preLevel3 a;

  select Store, TimeFrame, Department, Manager, 'Name' as Name, 
    sum(ClockHours) as ClockHours, sum(RegularHours) as RegularHours, sum(OTHours) as OTHours, 'H4' as Class 
  into #preLevel4
  from #phpGetOTHoursBetweenDates
  group by Store, TimeFrame, Department, Manager;
  select 
    (select Level from #Level1 c where c.Store = a.Store) as Level1, 
    (select Level2 from #Level2 c where c.Store = a.Store and c.TimeFrame = a.TimeFrame) as Level2,
    (select Level3 from #Level3 c where c.Store = a.Store and c.TimeFrame = a.TimeFrame and c.Department = a.Department) as Level3,
    (select count(*) as Level from #preLevel4 b where b.Store = a.Store and b.TimeFrame = a.TimeFrame and b.Department = a.Department and b.Manager <= a.Manager) as Level4,  
    a.Store, a.TimeFrame, a.Department, a.Manager, a.Name, a.ClockHours, a.RegularHours, a.OTHours, a.Class 
  into #Level4
  from #preLevel4 a;

  select Store, TimeFrame, Department, Manager, Name, 
    sum(ClockHours) as ClockHours, sum(RegularHours) as RegularHours, sum(OTHours) as OTHours, 'H5' as Class 
  into #preLevel5
  from #phpGetOTHoursBetweenDates
  group by Store, TimeFrame, Department, Manager, Name;
  select 
    (select Level from #Level1 c where c.Store = a.Store) as Level1, 
    (select Level2 from #Level2 c where c.Store = a.Store and c.TimeFrame = a.TimeFrame) as Level2,
    (select Level3 from #Level3 c where c.Store = a.Store and c.TimeFrame = a.TimeFrame and c.Department = a.Department) as Level3,
    (select Level4 from #Level4 c where c.Store = a.Store and c.TimeFrame = a.TimeFrame and c.Department = a.Department and c.Manager = a.Manager) as Level4,
    (select count(*) as Level from #preLevel5 b where b.Store = a.Store and b.TimeFrame = a.TimeFrame and b.Department = a.Department and b.Manager = a.Manager and b.Name <= a.Name) as Level5,  
    a.Store, a.TimeFrame, a.Department, a.Manager, a.Name, a.ClockHours, a.RegularHours, a.OTHours, a.Class 
  into #Level5
  from #preLevel5 a;

--SELECT COUNT(*) FROM(  -- 13777
  select Level, 0 as Level2, 0 as Level3, 0 as Level4, 0 as Level5, Store, TimeFrame, Department, Manager, Name, ClockHours, RegularHours, OTHours, Class
  from #Level1
  union
  select Level1, Level2, 0, 0, 0, Store, cast(TimeFrame as SQL_CHAR), Department, Manager, Name, ClockHours, RegularHours, OTHours, Class
  from #Level2
  union
  select Level1, Level2, Level3, 0, 0, Store, cast(TimeFrame as SQL_CHAR), Department, Manager, Name, ClockHours, RegularHours, OTHours, Class
  from #Level3
  union
  select Level1, Level2, Level3, Level4, 0, Store, cast(TimeFrame as SQL_CHAR), Department, Manager, Name, ClockHours, RegularHours, OTHours, Class
  from #Level4
  union
  select Level1, Level2, Level3, Level4, Level5, Store, cast(TimeFrame as SQL_CHAR), Department, Manager, Name, ClockHours, RegularHours, OTHours, Class
  from #Level5
  order by Level, Level2, Level3, Level5;
  
  SELECT * FROM #level5 order by Level1, Level2, Level3, Level5;
