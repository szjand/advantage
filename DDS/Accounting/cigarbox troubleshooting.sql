SELECT yearmonth, dl3, al1, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'car wash'
GROUP BY yearmonth, dl3, al1, al2

-- 12324, kevin hanson, appl to car wash, s/b detail 1978.03
SELECT SUM(gttamt)
FROM stgArkonaGLPTRNS
WHERE gtdate BETWEEN '10/01/2012' AND '10/31/2012'
  AND gtacct = '12324'
  
-- car wash expenses, ok june, NOT july - oct  
SELECT yearmonth, dl3, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'car wash'
  AND al2 = 'Total Expenses'
GROUP BY yearmonth, dl3, al3

-- issue IS personnel it's ALL the kevin hanson deal
-- cigar box spreadsheet IS wrong, but the doc has been corrected
SELECT yearmonth, dl3, al3, gtacct, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'car wash'
  AND al3 = 'Personnel Expenses'
GROUP BY yearmonth, dl3, al3, gtacct

-- net profit totally fucked up
-- drill down to al2
-- doc: $65928  $196794
-- expenses off BY $213
SELECT dl2, al1, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE gtdate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl2 = 'service'
GROUP BY dl2, al1, al2  

-- lets look at gross & service depts first
-- car wash ok
-- detail doc: 39808, zcb: -8910 -- acct 146302 mis-categorized AS main shop, fixec
-- pdq doc/cb: 60202, zcb: 78009 
SELECT dl2, dl3, al1, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE gtdate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl2 = 'service'
  AND c.al2 = 'Gross Profit'
GROUP BY dl2, dl3, al1, al2  

-- wtf IS up with pdq
-- add glpmast
SELECT dl3, al1, al2, al3, gmtype, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
INNER JOIN stgArkonaGLPMAST d ON b.gtacct = d.gmacct
  AND d.gmyear = 2012
WHERE gtdate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl3 = 'pdq'
  AND c.al2 = 'Gross Profit'
GROUP BY dl3, al1, al2, al3, gmtype

SELECT * FROM zcb WHERE dl3 = 'pdq' 

ahh looks LIKE i fucked up parts split

total service pretty CLOSE ON net profit
WHERE there are discrepancies it IS IN expenses
month       doc               zcb
july        368481            368727 -- -246
aug         365392            366188 -- -796
sep         321985            321989 -- ok
oct         359771            359558 -- $213
Good Enuf

-- ncr  policy?
yep, delivery expense AND policy, no sc comp
SELECT * 
FROM dimaccount 
WHERE cbdeptl2 = 'NCR'

SELECT * 
FROM dimaccount WHERE gmfsdepartmentlevel2 = 'Lease & Rental' 
AND gmfsaccountlevel1 <> 'Expenses'

-- parts
-- ~ 0.2% variance IN expenses
SELECT yearmonth, dl2, al1, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'parts'
GROUP BY yearmonth, dl2, al1, al2  

july pers, aug per, oct per
-- assume the difference IS BETWEEN accrual (WHEN the cb IS created) AND actual payroll
SELECT yearmonth, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'parts'
  AND c.al2 = 'total expenses'
GROUP BY yearmonth, al3  

SELECT yearmonth, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'parts'
  AND c.al2 = 'total expenses'
GROUP BY yearmonth, al3  

-- sales
SELECT DISTINCT cbdeptl3 FROM dimaccount WHERE cbdeptl2 = 'variable'
UPDATE dimaccount 
SET cbdeptl3 = 'Used Vehicles'
WHERE cbdeptl3 = 'Used Vehicle'


           zcb     cb
june       92671   92075
july       30398   28574
aug        39060   37127
sep        73380   73364
oct        89189   90336
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'variable'
GROUP BY yearmonth, dl2, al1  

-- both gross & expenses vary
-- oct new expenses zcb $1003 high
SELECT yearmonth, dl2, dl3, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
--WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl2 = 'variable'
GROUP BY yearmonth, dl2, dl3, al2  

-- oct personnel $1000 high
SELECT yearmonth, dl2, dl3, al2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl3 = 'new vehicles'
  AND c.al2 = 'total expenses'
GROUP BY yearmonth, dl2, dl3, al2, al3 

-- this IS the diff IN october
zcb: 76053 includes acct 12401 ($3000) gmfs acct 024a
cb:  75049 who knows
doc: 73054 does NOT include 12401
SELECT yearmonth, al3, c.glaccount, SUM(b.gttamt*c.split), d.glaccountdescription
-- SELECT sum(b.gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN dimaccount d ON c.glaccount = d.glaccount
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl3 = 'new vehicles'
  AND c.al3 = 'personnel expenses'
GROUP BY yearmonth, al3, c.glaccount, d.glaccountdescription

SELECT * FROM dimaccount WHERE glaccount = '12401'
-- new, personnel
jun ok, 
jul 1221 lo
aug ok
sep ok
oct 1000 hi
        jul     
zcb     cb      doc
59975   61196   56976
SELECT yearmonth, dl3, al3, SUM(b.gttamt*c.split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN dimaccount d ON c.glaccount = d.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'new vehicles'
  AND c.al3 = 'personnel expenses'
GROUP BY yearmonth, dl3, al3 

-- aug new
SELECT yearmonth, dl2, dl3, al2, al3, c.glaccount, d.glaccountdescription, SUM(b.gttamt*c.split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN dimaccount d ON c.glaccount = d.glaccount
WHERE a.thedate BETWEEN '08/01/2012' AND '08/31/2012'
  AND c.dl3 = 'new vehicles'
  AND c.al2 = 'total expenses'
GROUP BY yearmonth, dl2, dl3, al2, al3, c.glaccount, d.glaccountdescription 


-- both gross & expenses fucked up
-- ADD dimaccount
retail car cert ok
retail car other ok 
retail tk cert ok
retail tk other ok
ws car ok
ws trk ok
-- missing account 165500
-- aha, need to include 
SELECT yearmonth, dl3, al3, c.glaccount, glAccountDescription, SUM(b.gttamt*c.split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN dimaccount d ON c.glaccount = d.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-5) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'variable'
  AND c.dl3 = 'used vehicles'
  AND c.al2 = 'gross profit'
  AND a.yearmonth = 201210
GROUP BY yearmonth, dl3, al3, c.glaccount, glAccountDescription 
ORDER BY c.glaccount

SELECT * FROM dimaccount WHERE glaccount = '165500'

SELECT * FROM zcb WHERE glaccount = '165500'

SELECT * FROM dimaccount WHERE gmfsdepartmentlevel2 = 'finance & insurance'

-- honda main shop
-- august IS off
-- gross AND exp lo
SELECT yearmonth, dl2, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.thedate BETWEEN '08/01/2012' AND '08/31/2012'
  AND c.dl3 = 'main shop'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al2

-- sales october
-- missings 246600 , 246900      
SELECT yearmonth, dl2, al2, al3, glaccount, d.gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE a.thedate BETWEEN '08/01/2012' AND '08/31/2012'
  AND c.dl3 = 'main shop'
  AND al3 = 'sales'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al2, al3, glaccount, d.gmdesc
ORDER BY glaccount

SELECT * FROM zcb WHERE glaccount = '246900'

-- honda pdq
-- ALL are off
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'pdq'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al1

-- oct gross lo BY 723, expenses right on
SELECT yearmonth, dl2, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'pdq'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al2  

SELECT yearmonth, dl2, al3, glaccount, d.gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl3 = 'pdq'
  AND c.al2 = 'Gross Profit'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al3, glaccount, d.gmdesc  
ORDER BY glaccount

-- parts WAAAAAAAY off
SELECT yearmonth, dl2, al1, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'parts'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al1
-- gross IS the big problem
SELECT yearmonth, dl2, al2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl2 = 'parts'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al2  
-- oct sales/cogs
SELECT yearmonth, dl2, al3, glaccount, d.gmdesc, split, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl2 = 'parts'
  AND c.al2 = 'Gross Profit'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al3, glaccount, d.gmdesc, split  
ORDER BY glaccount

-- oct expenses lo BY $140
-- diff IS IN semi-fixed
SELECT yearmonth, dl2, dl3, al2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND storecode = 'ry2'
  AND c.dl2 = 'parts'
  AND c.al2 = 'total expenses'
GROUP BY yearmonth, dl2, dl3, al2, al3 

-- diff IS IN semi-fixed agrees with doc, but NOT cb
SELECT yearmonth, dl2, dl3, al2, al3, c.glaccount, d.gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
WHERE a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND storecode = 'ry2'
  AND c.dl2 = 'parts'
  AND c.al3 = 'semi-fixed expenses'
GROUP BY yearmonth, dl2, dl3, al2, al3, c.glaccount, d.gmdesc 

-- new vehicles
-- semi off
SELECT yearmonth, dl2, al1, al2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'new vehicles'
  AND storecode = 'ry2'
  AND al2 = 'total Expenses'
GROUP BY yearmonth, dl2, al1, al2, al3  
-- oct 
SELECT yearmonth, dl2, al1, al2, al3, c.glaccount, d.gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE  a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl3 = 'new vehicles'
  AND storecode = 'ry2'
  AND al3 = 'Semi-Fixed Expenses'  
GROUP BY yearmonth, dl2, al1, al2, al3, c.glaccount, d.gmdesc 
-- oct 
SELECT yearmonth, dl2, al1, al2, al3, c.glaccount, d.gmdesc, b.gtdate, b.gttamt
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE  a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl3 = 'new vehicles'
  AND storecode = 'ry2'
  AND al3 = 'Semi-Fixed Expenses'  
  AND c.glaccount = '25101' 
ORDER BY b.gtdate  

-- f&i
-- seems LIKE i just need to build myself a honda sales hierarchy so that i can mix AND match
-- new/used f&1
-- expenses are ok
-- ALL i need to break out now IS new/used within f/i, sales, etc
-- make, cert, other categorizations NOT needed for cigar box
-- but i need to be able verify my numbers against the doc
-- doc doesn't total based ON new/used, but the FS does
-- use FS categories?


SELECT * FROM zcb
SELECT * FROM tmpdocs WHERE title = 'ry2 used veh dept' AND glaccount IS NULL 

SELECT a.description, glaccount, l1,l2,l3,l4,l5,l6, b.*
FROM tmpdocs a
LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
WHERE reportnumber = 24
  AND description LIKE '%used%'
ORDER BY gmtype  


SELECT a.description, glaccount, l1,l2,l3,l4,l5,l6, b.*
FROM tmpdocs a
LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
WHERE reportnumber = 20

-- this IS ALL fine AND dandy, but still missing the cogs accounts NOT specified IN tmpdocs
-- DO the DISTINCT cogs query based ON sales account ?
DELETE FROM zcb WHERE name = 'ry2 f&i';
INSERT INTO zcb 
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  'New' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'Sales' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota; 
INSERT INTO zcb  
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  'New' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'COGS' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota; 
INSERT INTO zcb 
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  'Used' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'Sales' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota; 
INSERT INTO zcb 
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  'Used' AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'COGS' AS al3, 'NA' as al4, CAST(NULL AS sql_char) AS glaccount, 1 AS split
FROM system.iota; 
INSERT INTO zcb   
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  CASE 
    WHEN description LIKE '%new%' THEN 'New'
    WHEN description LIKE '%used%' THEN 'Used'
    ELSE 'XXXX'
  END AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  CASE 
    WHEN gmtype IN ('4','7') THEN 'Sales'
    WHEN gmtype = '5' THEN 'COGS'
    ELSE 'XXXXX'
  END AS al3, 'NA' as al4, glaccount, 1 AS split
FROM tmpdocs a
LEFT JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
WHERE reportnumber = 24
  AND glaccount IS NOT NULL 
UNION 
SELECT 'RY2 F&I'as name,'RY2'as storecode,'F&I'as dl1,
  CASE 
    WHEN description LIKE '%new%' THEN 'New'
    WHEN description LIKE '%used%' THEN 'Used'
    ELSE 'XXXX'
  END AS dl2,'NA' AS dl3,'NA' AS al1,'Gross Profit' AS al2,
  'COGS' AS al3, 'NA' as al4, gmdboa, 1 AS split
FROM tmpdocs a
INNER JOIN stgArkonaGLPMAST b ON a.glaccount = b.gmacct
  AND b.gmyear = 2012
WHERE reportnumber = 24
  AND glaccount IS NOT NULL 
  AND gmtype = '4';  

-- gross matches the doc
SELECT yearmonth, al2, dl2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND name = 'ry2 f&i'
GROUP BY yearmonth, al2, dl2 

-- the new/used numbers to ADD to sales
SELECT yearmonth, dl2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND name = 'ry2 f&i'
GROUP BY yearmonth, dl2 



-- new vehicles
-- semi off
SELECT yearmonth, dl2, al1, al2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'new vehicles'
  AND storecode = 'ry2'
  AND al2 = 'total Expenses'
GROUP BY yearmonth, dl2, al1, al2, al3  
-- oct 
SELECT yearmonth, dl2, al1, al2, al3, c.glaccount, d.gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE  a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl3 = 'new vehicles'
  AND storecode = 'ry2'
  AND al3 = 'Semi-Fixed Expenses'  
GROUP BY yearmonth, dl2, al1, al2, al3, c.glaccount, d.gmdesc 
-- oct 
SELECT yearmonth, dl2, al1, al2, al3, c.glaccount, d.gmdesc, b.gtdate, b.gttamt
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE  a.thedate BETWEEN '10/01/2012' AND '10/31/2012'
  AND c.dl3 = 'new vehicles'
  AND storecode = 'ry2'
  AND al3 = 'Semi-Fixed Expenses'  
  AND c.glaccount = '25101' 
ORDER BY b.gtdate  

-- used
-- expenses
-- semi-fixed ~200 lo
SELECT yearmonth, dl2, al3, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND dl3 = 'used Vehicles'
  AND name = 'Cigar Box'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al3

-- fs L36 insurance inventory NOT IN this
SELECT yearmonth, dl2, al3, gmdesc, glaccount, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE a.yearmonth = 201210
  AND dl3 = 'used Vehicles'
  AND name = 'Cigar Box'
  AND storecode = 'ry2'
GROUP BY yearmonth, dl2, al3,gmdesc, glaccount

-- fs L36 insurance inventory NOT IN this
-- what about new -- missing there too
SELECT yearmonth, dl2, al3, gmdesc, glaccount, SUM(b.gttamt*split)
SELECT SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE a.yearmonth = 201210
  AND dl3 = 'new Vehicles'
  AND name = 'Cigar Box'
  AND storecode = 'ry2'
  AND al3 = 'semi-fixed expenses'
GROUP BY yearmonth, dl2, al3,gmdesc, glaccount
-- what about ry1

SELECT yearmonth, dl2, al3, gmdesc, glaccount, SUM(b.gttamt*split)
--SELECT SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND d.gmyear = 2012
WHERE a.yearmonth = 201209
  AND dl3 = 'new Vehicles'
  AND name = 'Cigar Box'
  AND storecode = 'ry1'
  AND al3 = 'semi-fixed expenses'
GROUP BY yearmonth, dl2, al3,gmdesc, glaccount
-- acct 15601
SELECT * FROM stgarkonaglpmast WHERE gmacct = '25602'

SELECT * FROM tmpdocs WHERE glaccount = '25602'

SELECT * FROM zcb WHERE glaccount like '256%'
doc & zcb missin account 25601, 25602, 25610

SELECT distinct a.name, 'RY2', a.dl1, a.dl2, a.dl3, a.al1, a.al2, a.al3, a.al4, '25602', 1
FROM zcb a
WHERE storecode = 'ry2'
  AND dl3 = 'used Vehicles'
  AND al3 = 'semi-fixed Expenses'
  AND al4 = 'na';  
  
  
-- used sales
-- sales IS way off every month compared to uc doc
-- start with october
SELECT yearmonth, al3, gmdesc, gmacct, SUM(b.gttamt*split)
-- SELECT SUM(gttamt)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND gmyear = 2012
WHERE a.yearmonth = 201210
  AND dl3 = 'used Vehicles'
  AND name = 'Cigar Box'
  AND storecode = 'ry2'
  AND al2 = 'gross profit'
--  AND left(glaccount,4) = '2651'
  AND gmdesc NOT LIKE '%recon%'
GROUP BY yearmonth, al3, gmdesc, gmacct  

SELECT * FROM zcb WHERE storecode = 'ry2' and dl3 = 'used vehicles' AND left(glaccount,4) = '2651'
SELECT al3, gmdesc, gmacct 
FROM zcb a
LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
  AND gmyear = 2012
WHERE storecode = 'ry2' and dl3 = 'used vehicles' AND al3 = 'sales'

SELECT gmacct 
FROM zcb a
LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
  AND gmyear = 2012
WHERE storecode = 'ry2' and dl3 = 'used vehicles' AND al2 = 'gross profit'
  AND gmdesc LIKE 'CST%'
  AND al3 = 'sales'
-- ok, this takes care of categorizing the recon accounts AS cogs
UPDATE zcb
SET al3 = 'COGS'
WHERE glaccount IN (
  SELECT gmacct 
  FROM zcb a
  LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
    AND gmyear = 2012
  WHERE storecode = 'ry2' and dl3 = 'used vehicles' AND al2 = 'gross profit'
    AND gmdesc LIKE 'CST%'
    AND al3 = 'sales')

-- ok, this IS right ON to doc
SELECT yearmonth, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND dl3 = 'used Vehicles'
  AND name = 'Cigar Box'
  AND storecode = 'ry2'
  AND al2 = 'gross profit'
GROUP BY yearmonth



SELECT x.*,
  CASE  
    WHEN al2 = 'Gross profit' THEN  y.amt
    ELSE 0
  END,
  CASE  
    WHEN al2 = 'Gross profit' THEN  round(x.amt + y.amt, 2)
    ELSE 0
  END
FROM (
  SELECT yearmonth, dl2, al1, al2, SUM(b.gttamt*split) AS Amt
  FROM day a
  INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
  INNER JOIN zcb c ON b.gtacct = c.glaccount
  WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
    AND c.dl3 = 'used vehicles'
    AND storecode = 'ry2'
  GROUP BY yearmonth, dl2, al1, al2) x 
LEFT JOIN ( -- f&i
  SELECT yearmonth, SUM(b.gttamt*split) AS Amt
  FROM day a
  INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
  INNER JOIN zcb c ON b.gtacct = c.glaccount
  WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
    AND name = 'ry2 f&i'
    AND dl2 = 'Used'
  GROUP BY yearmonth) y ON x.yearmonth = y.yearmonth
  
-- variable ALL fucked up  
SELECT a.al3, a.glaccount,b.*
FROM zcb a
LEFT JOIN stgarkonaglpmast b ON a.glaccount = b.gmacct
  AND gmyear = 2012
WHERE a.name = 'cigar box'
  AND a.storecode = 'ry2'
  AND a.dl2 = 'variable'  
  AND a.al2 = 'gross profit'
WHERE storecode = 'ry2' AND dl2 = 'variable' AND al2 = 'gross profit' AND glaccount IS NOT NULL

SELECT name, storecode, dl2, glaccount
FROM zcb
WHERE glaccount IS NOT NULL
GROUP BY name, storecode, dl2, glaccount
HAVING COUNT(*) > 1

------shitshitshitshitshitshit
-- ok, f&i IS correct at dl2/al2 level
SELECT yearmonth, al2, dl2, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND name = 'ry2 f&i'
GROUP BY yearmonth, al2, dl2 
-- oct
-- so why the fuck IS dl2/al3 new sales low BY 6600 AND used sales low BY 4600
SELECT yearmonth, al3, dl2, gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND gmyear = 2012
WHERE a.yearmonth = 201210
  AND name = 'ry2 f&i'
GROUP BY yearmonth, al3, dl2, gmdesc 

SELECT * FROM zcb WHERE name = 'ry3 f&i'


-- ry3 f&i
SELECT x.*,
  CASE  
    WHEN al2 = 'Gross profit' THEN  y.amt
    ELSE 0
  END,
  CASE  
    WHEN al2 = 'Gross profit' THEN  round(x.amt + y.amt, 2)
    ELSE 0
  END
FROM (
  SELECT yearmonth, dl2, al1, al2, SUM(b.gttamt*split) AS Amt
  FROM day a
  INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
  INNER JOIN zcb c ON b.gtacct = c.glaccount
  WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
    AND c.dl2 = 'variable'
    AND storecode = 'ry3'
  GROUP BY yearmonth, dl2, al1, al2) x 
LEFT JOIN ( -- f&i
  SELECT yearmonth, SUM(b.gttamt*split) AS Amt
  FROM day a
  INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
  INNER JOIN zcb c ON b.gtacct = c.glaccount
  WHERE a.yearmonth BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
    AND name = 'ry3 f&i'
  GROUP BY yearmonth) y ON x.yearmonth = y.yearmonth
  
-- ry2 new gross
-- total expenses right on
-- oct gross lo 1200 (f&i right ON - only have oct FS to check new/used f&i)
-- sep gross lo 798 
-- aug gross lo 1602 
SELECT yearmonth, dl2, al1, al2, glaccount, gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND gmyear = 2012
WHERE a.yearmonth = 201210 --BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'new vehicles'
  AND storecode = 'ry2'
  AND name = 'cigar box'
  AND c.al3 = 'Sales'
GROUP BY yearmonth, dl2, al1, al2, glaccount, gmdesc  

-- hmm, sales numbers match FS exactly
SELECT yearmonth, dl2, al1, al2, glaccount, gmdesc, SUM(b.gttamt*split)
FROM day a
INNER JOIN stgArkonaGLPTRNS b ON a.thedate = b.gtdate
INNER JOIN zcb c ON b.gtacct = c.glaccount
LEFT JOIN stgArkonaGLPMAST d ON c.glaccount = d.gmacct
  AND gmyear = 2012
WHERE a.yearmonth = 201210 --BETWEEN (year(curdate())*100 + month(curdate())-3) AND (year(curdate())*100 + month(curdate())-1)
  AND c.dl3 = 'new vehicles'
  AND storecode = 'ry2'
  AND name = 'cigar box'
  AND c.al3 = 'Sales'
  AND gmdesc LIKE '%INCENTIVE%'
GROUP BY yearmonth, dl2, al1, al2, glaccount, gmdesc  

