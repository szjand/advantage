ALTER TABLE zcb
ADD COLUMN page integer
ADD COLUMN dl6 cichar(24)
ADD COLUMN dl7 cichar(24)
ADD COLUMN dl8 cichar(24);

DROP TABLE #dept;
CREATE TABLE #dept (
  dl1 cichar(24),
  dl2 cichar(24),
  dl3 cichar(24),
  dl4 cichar(24),
  dl5 cichar(24),
  dl6 cichar(24),
  dl7 cichar(24),
  dl8 cichar(24));
insert into #dept SELECT 'Grand Forks','RY1','Variable','F&I','New','New','New','New' FROM system.iota;  
insert into #dept SELECT 'Grand Forks','RY1','Variable','F&I','Used','Used','Used','Used' FROM system.iota;  
insert into #dept SELECT 'Grand Forks','RY1','Variable','NCR','NCR','NCR','NCR','NCR' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Retail','Cars','Chevrolet' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Retail','Cars','Buick' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Retail','Cars','Cadillac' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Retail','Trucks','Chevrolet' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Retail','Trucks','Buick' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Retail','Trucks','Cadillac' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Retail','Trucks','GMC' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Retail','Trucks','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Internal','Cars','Chevrolet' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Internal','Cars','Buick' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Internal','Cars','Cadillac' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Internal','Trucks','Chevrolet' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Internal','Trucks','Buick' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Internal','Trucks','Cadillac' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Internal','Trucks','GMC' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Internal','Trucks','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Fleet','Cars','Chevrolet' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Fleet','Cars','Buick' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Fleet','Cars','Cadillac' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Fleet','Trucks','Chevrolet' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Fleet','Trucks','Buick' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Fleet','Trucks','Cadillac' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Fleet','Trucks','GMC' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Fleet','Trucks','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Accessories','Accessories','Accessories' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','New','Other','Other','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','Used','Retail','Cars','Certified' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','Used','Retail','Cars','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','Used','Retail','Trucks','Certified' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','Used','Retail','Trucks','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','Used','Wholesale','Cars','Cars' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','Used','Wholesale','Trucks','Trucks' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Variable','Sales','Used','Other','Other','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Fixed','Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
--insert into #dept SELECT 'Grand Forks','RY1','Fixed','Mechanical','PDQ','PDQ','PDQ','PDQ' FROM system.iota;
--insert into #dept SELECT 'Grand Forks','RY1','Fixed','Mechanical','Detail','Detail','Detail','Detail' FROM system.iota;
--insert into #dept SELECT 'Grand Forks','RY1','Fixed','Mechanical','Car Wash','Car Wash','Car Wash','Car Wash' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Fixed','Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY1','Fixed','Parts','Parts','Parts','Parts','Parts' FROM system.iota;
/******************************** Sales New **********************************/ 
-- new retail
DROP TABLE #newretail;
SELECT a.fxgact,a.fxfact, b.gmdboa, b.gmdesc,
  CASE
    WHEN CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 400 AND 418 THEN 'Cars'
    WHEN CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 423 AND 438 THEN 'Trucks'
  END AS shape,
  CASE
    WHEN right(TRIM(fxfact),1) = 'a' THEN 'Chevrolet'
    WHEN right(TRIM(fxfact),1) = 'd' THEN 'Buick'
    WHEN right(TRIM(fxfact),1) = 'e' THEN 'Cadillac'
    WHEN right(TRIM(fxfact),1) = 'f' THEN 'GMC'
    WHEN right(TRIM(fxfact),1) = 'j' THEN 'Other'    
  END AS make
INTO #NewRetail   
FROM stgArkonaFFPXREFDTA a 
LEFT JOIN stgArkonaGLPMAST b ON a.fxgact = b.gmacct
  AND gmyear = 2012
WHERE LEFT(fxfact,1) BETWEEN '0' AND '9'
  AND (
    CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 400 AND 418
    OR 
    CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 423 AND 438)
  AND fxcyy = 2012
  AND fxconsol = ''
  AND right(TRIM(fxfact),1) NOT IN ('b','c','h')
  AND gmdboa <> ''   
/*  
SELECT * FROM zcb WHERE name = 'gmfs expenses'  

SELECT * FROM zcb WHERE name = 'gmfs gross' 

DELETE FROM zcb WHERE name = 'gmfs gross'

SELECT * FROM #dept
select * FROM #newRetail
*/
-- new retail sales
INSERT INTO zcb
SELECT 'gmfs gross', 'RY1', a.*, 'Gross Profit' AS al1, 'Sales' AS al2, 'Sales' AS al3, 
  'NA' as al4, b.fxgact AS glAccount, fxfact AS gmcoa, 888, 1
FROM #dept a
LEFT JOIN #newRetail b ON a.dl7 = b.shape collate ads_default_ci
  AND a.dl8 = b.make collate ads_default_ci
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 = 'retail'
  AND b.gmdboa <> '';
-- new retail COGS
INSERT INTO zcb
SELECT 'gmfs gross', 'RY1', a.*, 'Gross Profit' AS al1, 'COGS' AS al2, 'COGS' AS al3, 
  'NA' as al4, b.gmdboa AS glAccount, fxfact AS gmcoa, 888, 1
FROM #dept a
LEFT JOIN #newRetail b ON a.dl7 = b.shape collate ads_default_ci
  AND a.dl8 = b.make collate ads_default_ci
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 = 'retail'
  AND b.gmdboa <> '';  
-- checks ok  
SELECT dl4, dl5, dl6, dl7, dl8, SUM(gttamt) AS gross,
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs
FROM zcb a
LEFT JOIN stgArkonaGlptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.name = 'gmfs gross'
GROUP BY dl4, dl5, dl6, dl7, dl8

-- new fleet/internal
DROP TABLE #newfleetinternal;
SELECT a.fxgact,a.fxfact, b.gmdboa, b.gmdesc,
  CASE
    WHEN LEFT(fxfact,3) IN ('420','421') THEN 'Cars'
    WHEN LEFT(fxfact,3) IN ('440','441') THEN 'Trucks'
  END AS shape,
  CASE
    WHEN right(TRIM(fxfact),1) = 'a' THEN 'Chevrolet'
    WHEN right(TRIM(fxfact),1) = 'd' THEN 'Buick'
    WHEN right(TRIM(fxfact),1) = 'e' THEN 'Cadillac'
    WHEN right(TRIM(fxfact),1) = 'f' THEN 'GMC'
    WHEN right(TRIM(fxfact),1) = 'j' THEN 'Other'
  END AS make,
  CASE
    WHEN LEFT(fxfact,3) IN ('420','440') THEN 'Fleet'
    WHEN LEFT(fxfact,3) IN ('421','441') THEN 'Internal'
  END AS saletype
INTO  #newfleetinternal 
FROM stgArkonaFFPXREFDTA a 
LEFT JOIN stgArkonaGLPMAST b ON a.fxgact = b.gmacct
  AND gmyear = 2012
WHERE LEFT(fxfact,1) BETWEEN '0' AND '9'
  AND (
    CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 420 AND 421
    OR 
    CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 440 AND 441)
  AND fxcyy = 2012
  AND fxconsol = ''
  AND right(TRIM(fxfact),1) NOT IN ('b','c','h')
  AND gmdboa <> ''; 

--SELECT * FROM #newfleetinternal
-- new fleet/internal sales
INSERT INTO zcb
SELECT 'gmfs gross', 'RY1', a.*, 'Gross Profit' AS al1, 'Sales' AS al2, 'Sales' AS al3, 
  'NA' as al4, b.fxgact AS glAccount, fxfact AS gmcoa, 777, 1
-- SELECT 'gmfs gross', 'RY1', a.*, b.*  
FROM #dept a
LEFT JOIN #newfleetinternal b ON a.dl7 = b.shape collate ads_default_ci
  AND a.dl8 = b.make collate ads_default_ci
  AND a.dl6 = b.saletype collate ads_default_ci
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 in ('internal', 'fleet')
  AND b.gmdboa <> '';
-- new fleet/internal cogs
INSERT INTO zcb
SELECT 'gmfs gross', 'RY1', a.*, 'Gross Profit' AS al1, 'COGS' AS al2, 'COGS' AS al3, 
  'NA' as al4, b.gmdboa AS glAccount, fxfact AS gmcoa, 777, 1
-- SELECT 'gmfs gross', 'RY1', a.*, b.*  
FROM #dept a
LEFT JOIN #newfleetinternal b ON a.dl7 = b.shape collate ads_default_ci
  AND a.dl8 = b.make collate ads_default_ci
  AND a.dl6 = b.saletype collate ads_default_ci
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 in ('internal', 'fleet')
  AND b.gmdboa <> '';  

-- checks ok  
SELECT dl4, dl5, dl6, dl7, dl8, SUM(coalesce(gttamt,0)) AS gross,
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs
FROM zcb a
LEFT JOIN stgArkonaGlptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.name = 'gmfs gross'
GROUP BY dl4, dl5, dl6, dl7, dl8  
ORDER BY dl8, dl7,dl6
  
-- accessories

SELECT * FROM stgArkonaFFPXREFDTA WHERE fxfact LIKE '657%' AND fxcyy = 2012 AND fxconsol = ''
  
INSERT INTO zcb
SELECT 'gmfs gross', 'RY1', a.*, 'Gross Profit' AS al1, 'Sales' AS al2, 'Sales' AS al3, 
  'NA' as al4, '1457001' AS glAccount, '457A' AS gmcoa, 666, 1
FROM #dept a
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 = 'Accessories';  
INSERT INTO zcb
SELECT 'gmfs gross', 'RY1', a.*, 'Gross Profit' AS al1, 'COGS' AS al2, 'COGS' AS al3, 
  'NA' as al4, '1657001' AS glAccount, '657A' AS gmcoa, 666, 1
FROM #dept a
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 = 'Accessories';   
  
-- check again page 7 lines 64-73: New Vehicle sls summary
SELECT dl4, dl5, dl6, dl7, SUM(coalesce(gttamt,0)) AS gross,
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs
FROM zcb a
LEFT JOIN stgArkonaGlptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.name = 'gmfs gross'
GROUP BY dl4, dl5, dl6, dl7
ORDER BY dl7, dl6

-- other  
SELECT * FROM stgArkonaFFPXREFDTA WHERE fxfact LIKE '645%' AND fxcyy = 2012 AND fxconsol = ''

INSERT INTO zcb
SELECT 'gmfs gross', 'RY1', a.*, 'Gross Profit' AS al1, 'Sales' AS al2, 'Sales' AS al3, 
  'NA' as al4, '144500' AS glAccount, '445J' AS gmcoa, 666, 1
FROM #dept a
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 = 'Other';  
INSERT INTO zcb
SELECT 'gmfs gross', 'RY1', a.*, 'Gross Profit' AS al1, 'COGS' AS al2, 'COGS' AS al3, 
  'NA' as al4, '164500' AS glAccount, '645J' AS gmcoa, 666, 1
FROM #dept a
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 = 'Other';   
  
-- check again page 7 lines 64-73: New Vehicle sls summary
SELECT dl4, dl5, dl6, dl7, 
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt,0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGlptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.name = 'gmfs gross'
GROUP BY dl4, dl5, dl6, dl7
ORDER BY dl7, dl6  
/******************************** Sales New **********************************/


/******************************** F&I ****************************************/

ok, gm coa, what the fuck IS the diff BETWEEN f&i protection plan activity AND finance & insurance activity
ahh, f&i protection plan activity IS f&i IN L&R, NOT used

SELECT * FROM zcb WHERE name = 'gmfs gross' AND dl4 <> 'Sales' AND dl5 <> 'New'

-- fs line level detail
DROP TABLE #acct;
CREATE TABLE #acct (
  al1 cichar(30),
  al2 cichar(30),
  al3 cichar(40),
  account cichar(6),
  page integer,
  line integer,
  dl4 cichar(24),
  dl5 cichar(24),
  dl6 cichar(24),
  dl7 cichar(24),
  dl8 cichar(24));
--f&i new 
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Finance Income-New','806',7, 1,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Ins Comm Earned-New','807',7, 2,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Fin & Ins Chargebacks','850',7, 3,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','Accessories','810',7, 4,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Accessories','860',7, 4,'F&I','New','New','New','New' FROM system.iota;
-- skip line 5 Divisional Ext Warranties acct 494 - NOT ON gm coa
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','GM Prot Plans','443',7, 6,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','GM Prot Plans','643',7, 6,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','Other Prot','444',7, 7,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Other Prot','644',7, 7,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Repo Losses-New','853',7, 8,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','F&I Comp','855',7, 9,'F&I','New','New','New','New' FROM system.iota;
--f&i used 
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Finance Income-Used','808',7, 11,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Ins Comm Earned-Used','809',7, 12,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Fin & Ins Chargebacks','851',7, 13,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','Accessories','811',7, 14,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Accessories','861',7, 14,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','GM Prot Plans','454',7, 15,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','GM Prot Plans','654',7, 15,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','Other Prot','455',7, 16,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Other Prot','655',7, 16,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Repo Losses-New','854',7, 17,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','F&I Comp','856',7, 18,'F&I','Used','Used','Used','Used' FROM system.iota;
-- ncr
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Finance Income-Used','535',8, 6,'NCR','NCR','NCR','NCR','NCR' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Gain(Loss) on Disposition','510G',8, 23,'NCR','NCR','NCR','NCR','NCR' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Interest','711',8, 11,'NCR','NCR','NCR','NCR','NCR' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Amortization','722',8, 12,'NCR','NCR','NCR','NCR','NCR' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Insurance (In Service Vehicles)','723',8, 13,'NCR','NCR','NCR','NCR','NCR' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'License, Title & Tax','724',8, 14,'NCR','NCR','NCR','NCR','NCR' FROM system.iota;
-- line 15 NOT used
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Maintenance & Repairs','736',8, 16,'NCR','NCR','NCR','NCR','NCR' FROM system.iota;
-- line 17 NOT used
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Other','728',8, 18,'NCR','NCR','NCR','NCR','NCR' FROM system.iota;
-- used
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Cars Rtl - GM Certified','446A',6, 1,'Sales','Used','Retail','Cars','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Rtl - GM Certified Cost','646A',6, 1,'Sales','Used','Retail','Cars','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Rtl - GM Certified Recon','647A',6, 1,'Sales','Used','Retail','Cars','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Cars Rtl - Other','446B',6, 2,'Sales','Used','Retail','Cars','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Rtl - Other Cost','646B',6, 2,'Sales','Used','Retail','Cars','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Rtl - Other Recon','647B',6, 2,'Sales','Used','Retail','Cars','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Trucks Rtl - GM Certified','450A',6, 4,'Sales','Used','Retail','Trucks','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Rtl - GM Certified Cost','650A',6, 4,'Sales','Used','Retail','Trucks','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Rtl - GM Certified Recon','651A',6, 4,'Sales','Used','Retail','Trucks','Certified' FROM system.iota;   
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Trucks Rtl - Other','450B',6, 5,'Sales','Used','Retail','Trucks','Other' FROM system.iota; 
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Rtl - Other Cost','650B',6, 5,'Sales','Used','Retail','Trucks','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Rtl - Other Recon','651B',6, 5,'Sales','Used','Retail','Trucks','Other' FROM system.iota; 
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Cars Wholesale','448',6, 8,'Sales','Used','Wholesale','Cars','Cars' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Wholesale','648',6, 8,'Sales','Used','Wholesale','Cars','Cars' FROM system.iota;
-- 649 Adjustment NOT used
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Trucks Wholesale','452',6, 10,'Sales','Used','Wholesale','Trucks','Trucks' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Wholesale','652',6, 10,'Sales','Used','Wholesale','Trucks','Trucks' FROM system.iota;
-- 653 Adustment NOT used
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Other Automotive','456',6, 13,'Sales','Used','Other','Other','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Other Automotive','656',6, 13,'Sales','Used','Other','Other','Other' FROM system.iota;
-- fixed mech
-- don't fuck around with pdq/main/detail/main shop at fs level
-- maybe
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Cust Labor Cars & LD Trks','460A',6, 21,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Cust Labor Cars & LD Trks','660A',6, 21,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'S/Cntr Customer Lab - Cars & LD Trks','460b',6, 22,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'S/Cntr Customer Lab - Cars & LD Trks','660B',6, 22,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Q/Srv Lab - Cars & LD Trks','460C',6, 23,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Q/Srv Lab - Cars & LD Trks','660C',6, 23,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Cust Lab Com, Flt & MD Trks','461A',6, 24,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Cust Labor Cars & LD Trks','661A',6, 24,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'S/Cntr Lab Com,Flt & MD Trks','461B',6, 25,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'S/Cntr Lab Com,Flt & MD Trks','661B',6, 25,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Q/Srv Lab - Com,Flt & MD Trks','461C',6, 26,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Q/Srv Lab - Com,Flt & MD Trks','661C',6, 26,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Warranty Claim Labor','462',6, 27,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Warranty Claim Labor','662',6, 27,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Internal Labor','463',6, 28,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Internal Labor','663',6, 28,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'New Veh Insp Lbr','464',6, 29,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'New Veh Insp Lbr','664',6, 29,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Adj Cost of Lbr Sls','665',6, 30,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Shop Supplies','469',6, 32,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Shop Supplies','669',6, 32,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Sublet Repairs','466',6, 33,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Sublet Repairs','666',6, 33,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
-- fixed body shop
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Cust Paint Labor','470',6, 35,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Cust Paint Labor','670',6, 35,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Cust Body Labor','471',6, 36,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Cust Body Labor','671',6, 36,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Warranty Claim Labor','472',6, 37,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Warranty Claim Labor','672',6, 37,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Internal Labor','473',6, 38,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Internal Labor','673',6, 38,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Adj Cost of Lbr Sls','675',6, 39,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Sublet Repairs','476',6, 41,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Sublet Repairs','676',6, 41,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Pnt & Shop Matrls','479',6, 42,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Pnt & Shop Matrls','679',6, 42,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
-- parts
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Warranty Claims','480',6, 45,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Warranty Claims','680',6, 45,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Mech Cars & LD Trk RO','467',6, 46,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Mech Cars & LD Trk RO','667',6, 46,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Mech Com,Flt & MD Trks RO','468',6, 47,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Mech Com,Flt & MD Trks RO','668',6, 47,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Mech Quick Serv RO','478',6, 48,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Mech Quick Serv RO','678',6, 48,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Body Cust RO','477',6, 49,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Body Cust RO','677',6, 49,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Internal','481',6, 50,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Internal','681',6, 50,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Counter - Retail','482',6, 51,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Counter - Retail','682',6, 51,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Wholesale','483',6, 52,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Wholesale','683',6, 52,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Accessories','484',6, 53,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Accessories','684',6, 53,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Purchase Allowances','687',6, 54,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Adj P&A Inventory','688',6, 55,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Tires','490',6, 57,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Tires','690',6, 57,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Gas, Oil & Grease','491',6, 58,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Gas, Oil & Grease','691',6, 58,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Miscellaneous','492',6, 59,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Miscellaneous','692',6, 59,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;



SELECT * FROM zcb WHERE name = 'gmfs gross' AND dl4 = 'F&I'
-- f&i
INSERT INTO zcb
SELECT 'gmfs gross','RY1','Grand Forks','RY1',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',c.fxgact,c.fxfact,line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
  AND a.dl5 = b.dl5
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = c.fxfact
  AND c.fxconsol = ''
  AND c.fxcyy = 2012  
WHERE a.dl4 = 'F&I'  

-- 8/14/14
missing accounts IN zcb.gmfs gross 
  used missing account 185101, 185102 
  new missing account 185001, 185002 
-- new  
INSERT INTO zcb    
SELECT name, storecode, dl1,dl2,dl3,dl4,dl5,dl6,dl7,dl8, al1,al2,al3,al4,al5,
  '185001', gmcoa, line, split, page
FROM zcb
WHERE glaccount = '185000'
  AND name = 'gmfs gross';  
INSERT INTO zcb    
SELECT name, storecode, dl1,dl2,dl3,dl4,dl5,dl6,dl7,dl8, al1,al2,al3,al4,al5,
  '185002', gmcoa, line, split, page
FROM zcb
WHERE glaccount = '185000'
  AND name = 'gmfs gross';   
-- used
INSERT INTO zcb    
SELECT name, storecode, dl1,dl2,dl3,dl4,dl5,dl6,dl7,dl8, al1,al2,al3,al4,al5,
  '185101', gmcoa, line, split, page
FROM zcb
WHERE glaccount = '185100'
  AND name = 'gmfs gross'; 
INSERT INTO zcb    
SELECT name, storecode, dl1,dl2,dl3,dl4,dl5,dl6,dl7,dl8, al1,al2,al3,al4,al5,
  '185102', gmcoa, line, split, page
FROM zcb
WHERE glaccount = '185100'
  AND name = 'gmfs gross';  

SELECT dl4, dl5, al3, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
AND dl4 = 'F&I' 
GROUP BY dl4, dl5, al3, line
ORDER BY line asc

-- ncr
INSERT INTO zcb
SELECT 'gmfs gross','RY1','Grand Forks','RY1',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',c.fxgact,c.fxfact,line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
  AND a.dl5 = b.dl5
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = c.fxfact
  AND c.fxconsol = ''
  AND c.fxcyy = 2012  
WHERE a.dl4 = 'NCR'  

SELECT dl4, dl5, al2, al3, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
AND dl4 = 'NCR' 
GROUP BY dl4, dl5, al2, al3, line
ORDER BY line asc

-- used
INSERT INTO zcb
SELECT 'gmfs gross','RY1','Grand Forks','RY1',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',c.fxgact,c.fxfact,line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
  AND a.dl5 = b.dl5
  AND a.dl6 = b.dl6
  AND a.dl7 = b.dl7
  AND a.dl8 = b.dl8
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = c.fxfact
  AND c.fxconsol = ''
  AND c.fxcyy = 2012  
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'Used'
  
SELECT dl4, dl5, dl6, dl7, dl8, al2, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'sales' 
  AND dl5 = 'used'
GROUP BY dl4, dl5, dl6, dl7, dl8, al2, line
ORDER BY line asc, al2 DESC

-- mechanical
INSERT INTO zcb
SELECT 'gmfs gross','RY1','Grand Forks','RY1',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',c.fxgact,c.fxfact,line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = c.fxfact
  AND c.fxconsol = ''
  AND c.fxcyy = 2012  
WHERE a.dl4 = 'Mechanical'

SELECT dl4, al3, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'mechanical' 
GROUP BY dl4, al3, line
ORDER BY line asc

-- body shop
--DELETE FROM zcb WHERE name = 'gmfs gross' AND dl4 = 'body shop'
INSERT INTO zcb
SELECT 'gmfs gross','RY1','Grand Forks','RY1',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',c.fxgact,c.fxfact,line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = c.fxfact
  AND c.fxconsol = ''
  AND c.fxcyy = 2012  
WHERE a.dl4 = 'body shop';

SELECT dl4, al3, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'body shop' 
GROUP BY dl4, al3, line
ORDER BY line asc;

-- parts
DELETE FROM zcb WHERE name = 'gmfs gross' AND dl4 = 'parts'
INSERT INTO zcb
SELECT 'gmfs gross','RY1','Grand Forks','RY1',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',c.fxgact,c.fxfact,line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = left(c.fxfact, 3)
  AND c.fxconsol = ''
  AND c.fxcyy = 2012 
  AND LEFT(fxgact,1) = '1' 
WHERE a.dl4 = 'parts';

SELECT dl4, al3, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'parts' 
GROUP BY dl4, al3, line
ORDER BY line asc;

-- page 2 line 2
SELECT SUM(coalesce(gttamt, 0)) AS store, 
  SUM(CASE WHEN dl3 = 'variable' THEN gttamt ELSE 0 END) AS variable,
  SUM(CASE WHEN dl3 = 'fixed' THEN gttamt ELSE 0 END) AS fixed
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross'
-- page 6 lines 1- 10
SELECT dl2, dl6, dl7, dl8, line, 
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt, 0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'sales' 
  AND dl5 = 'used'
GROUP BY dl2, dl4, dl5, dl6, dl7, dl8, line
ORDER BY dl2, dl4, dl5, dl6, dl7, dl8, line
-- page 6 fixed lines 21 - 59
SELECT dl2, dl5, line, al3,
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt, 0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl3 = 'fixed'
GROUP BY dl2, dl5, line, al3
ORDER BY dl2, line
-- page 6 lines 15 - 17
SELECT dl2, dl4, 
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt, 0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl3 = 'variable'
GROUP BY dl2, dl4



  

