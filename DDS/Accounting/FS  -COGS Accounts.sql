-- p6 used cogs
-- by account
fs:  3281859
qu:  3385142
diff: 103283
the diff IS the qu includes 165500 & 185100
both are uc cogs IN glpmast
165500 C/S USED OTHER PROT PLANS, fs acct 655:IN the F/I section p7
185100 FIN & INSUR CHGBKS USD, fs acct 851: IN the f/i section p7
remove those 2 accounts, AND qu aggrees with fs
SELECT a.gmacct, a.gmdesc, sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '5' -- cogs
  AND a.gmdept = 'uc'
  AND a.gmacct NOT IN ('165500', '185100')
GROUP BY a.gmacct, a.gmdesc 
UNION  
SELECT 'Total', 'UC COGS', sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '5' -- cogs
  AND a.gmdept = 'uc'
  AND a.gmacct NOT IN ('165500', '185100')
  
  
-- new cogs
fs P7 nv sls summary
L72: 3968673 (115)
L73: 3922116 (114 - *MEMO* total) 
qu:  4039026
           
the diff IS the qu includes 164400 & 185000
both are nc cogs IN glpmast
164300 C/S NEW GM PROT PLAN, fs acct 643: IN the F/I section p7
164400 C/S NEW OTHER PLANS, fs acct 644: IN the F/I section p7
185000 FIN & INSUR CHGBKS NEW, fs acct 850: IN the f/i section p7
remove these accounts, qu: 3968326 - ~300 short of L72 ON fs

SELECT a.gmacct, a.gmdesc, sum(gttamt) 
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '5' -- cogs
  AND a.gmdept = 'nc'
  AND a.gmacct NOT IN ('185000', '164400', '164300')
GROUP BY a.gmacct, a.gmdesc 
UNION  
SELECT 'Total', 'NC COGS', sum(gttamt) 
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '5' -- cogs
  AND a.gmdept = 'nc'
  AND a.gmacct NOT IN ('185000', '164400', '164300')

-- parts COGS
P6 L61: 818536   
had to ADD acct 167701 (bs)
SELECT a.gmacct, a.gmdesc, sum(gttamt) 
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND ((a.gmtype = '5' AND a.gmdept = 'pd')
  OR (a.gmtype = '5' AND a.gmdept = 'bs' AND a.gmacct = '167701'))
GROUP BY a.gmacct, a.gmdesc 
UNION  
SELECT 'Total', 'PD COGS', sum(gttamt) 
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND ((a.gmtype = '5' AND a.gmdept = 'pd')
  OR (a.gmtype = '5' AND a.gmdept = 'bs' AND a.gmacct = '167701'))
  
-- mech COGS
P6 L34: 166478
query:  172468
SELECT a.gmacct, gmdept, a.gmdesc, SUM(gttamt)
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '5' 
  AND a.gmdept IN ('CW','QL','RE','SD')
GROUP BY a.gmacct, a.gmdept, a.gmdesc  
UNION  
SELECT 'Total', 'ALL', 'SD COGS', sum(gttamt) 
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '5' 

-- fuck, this works
-- after ALL, ALL i am trying to derive IS a grouping of accounts  
-- START with ffxrefdta to generate the list of accounts
-- fuck, feels LIKE i have been missing the obvious
SELECT SUM(gttamt)
FROM stgArkonaFFPXREFDTA a
INNER JOIN stgArkonaGLPTRNS b ON a.fxgact = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.fxcyy = 2012 
  AND fxfact IN ('660a','660b','660c','661a','661b','661c','662','663','664','665','669','666')
  AND fxconsol = ''
GROUP BY gtacct  

-- so these accounts 
-- DROP TABLE #wtf1;
SELECT gmdept, gmtype, gmacct, gmdesc
INTO #wtf1
FROM stgArkonaFFPXREFDTA a
INNER JOIN stgArkonaGLPMAST b ON a.fxgact = b.gmacct
WHERE a.fxcyy = 2012 
  AND a.fxfact IN ('660a','660b','660c','661a','661b','661c','662','663','664','665','669','666')
  AND a.fxconsol = ''
  AND b.gmyear = 2012
  AND b.gmstyp = 'a';
-- DROP TABLE #wtf2;  
SELECT gmdept, gmtype, a.gmacct, a.gmdesc
INTO #wtf2
FROM stgarkonaGLPMAST a 
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '5' 
  AND a.gmdept IN ('CW','QL','RE','SD');  

SELECT *
FROM #wtf1 a
full OUTER JOIN #wtf2 b ON a.gmacct = b.gmacct  
WHERE a.gmacct IS NULL 
  OR b.gmacct IS NULL 
  
SELECT *
FROM #wtf1
UNION 
SELECT *
FROM #wtf2

SELECT *
FROM (
  SELECT *
  FROM #wtf1
  UNION 
  SELECT *
  FROM #wtf2) a
LEFT JOIN edwAccountDim b ON a.gmacct = b.account

-- IF i go with edwAccountDim
-- here IS an account that has been added since its inception
-- how DO etl a new account
SELECT MIN(gtdate) FROM stgArkonaGLPTRNS WHERE gtacct = '166424'

SELECT COUNT(*) FROM stgArkonaGLPTRNS WHERE gtacct = '165804'

IS any gl account assigned to multiple gm accts
-- nope
-- 0 gl transactions for acct XFCOPY2012
SELECT fxgact, fxfact
FROM stgArkonaFFPXREFDTA
WHERE fxconsol = ''
  AND fxcyy = 2012
  AND fxfact <> 'XFCOPY2012'
group by fxgact, fxfact  
HAVING COUNT(*) > 1

SELECT *
FROM stgArkonaFFPXREFDTA
WHERE fxconsol = ''
  AND fxcyy = 2012
  AND fxgact = ''
  
SELECT *
FROM stgArkonaGLPTRNS
WHERE gtacct like 'SPLT%'  

SELECT *
FROM stgArkonaFFPXREFDTA
WHERE fxconsol = ''
  AND fxcyy = 2012
  AND fxgact <> ''
  
SELECT a.fxgact, a.fxfact, a.fxfac2, a.fxfp01, b.*
FROM stgArkonaFFPXREFDTA a
LEFT JOIN gmaccounts b ON fxfact = b.fsaccount
WHERE a.fxconsol = ''
  AND a.fxcyy = 2012
  AND a.fxgact <> '' 
-- the only accounts NOT IN gmaccounts are 172654, 172684(National), 133701 (wes note)
--  AND b.fsaccount IS NULL  
SELECT *
FROM gmaccounts a
where a.fsaccount IN ('660a','660b','660c','661a','661b','661c','662','663','664','665','666','669',
  '460a','460b','460c','461a','461b','461c','462','463','464','465','466','469')
  
--SELECT fxgact
--FROM (  
-- 1 row per ffpxrefdta.fxgact
SELECT a.*, c.gmacct, c.gmtype, c.gmdept, c.gmdesc
FROM gmaccounts a
LEFT JOIN stgArkonaFFPXREFDTA b ON a.fsaccount = b.fxfact
  AND b.fxcyy = 2012
  AND b.fxconsol = ''
LEFT JOIN stgArkonaGLPMAST c ON b.fxgact = c.gmacct
  AND c.gmyear = 2012
  AND c.gmstyp = 'A'  
where a.fsaccount IN ('660a','660b','660c','661a','661b','661c','662','663','664','665','666','669',
  '460a','460b','460c','461a','461b','461c','462','463','464','465','466','469')
--GROUP BY fxgact
--HAVING COUNT(*) > 1  

-- exact
SELECT gmfssection, department, SUM(gttamt)
FROM gmaccounts a
LEFT JOIN stgArkonaFFPXREFDTA b ON a.fsaccount = b.fxfact
  AND b.fxcyy = 2012
  AND b.fxconsol = ''
LEFT JOIN stgArkonaGLPMAST c ON b.fxgact = c.gmacct
  AND c.gmyear = 2012
  AND c.gmstyp = 'A'  
LEFT JOIN stgArkonaGLPTRNS d ON c.gmacct = d.gtacct
  and d.gtdate BETWEEN '08/01/2012' AND '08/31/2012'  
where a.fsaccount IN ('660a','660b','660c','661a','661b','661c','662','663','664','665','666','669',
  '460a','460b','460c','461a','461b','461c','462','463','464','465','466','469')
GROUP BY gmfssection, department  
-- 10/6
-- this IS good: gmaccounts.Department, gmaccounts.cigarboxsection
SELECT gmfssection, department, SUM(gttamt)
FROM gmaccounts a
LEFT JOIN stgArkonaFFPXREFDTA b ON a.fsaccount = b.fxfact
  AND b.fxcyy = 2012
  AND b.fxconsol = ''
LEFT JOIN stgArkonaGLPMAST c ON b.fxgact = c.gmacct
  AND c.gmyear = 2012
  AND c.gmstyp = 'A'  
LEFT JOIN stgArkonaGLPTRNS d ON c.gmacct = d.gtacct
  and d.gtdate BETWEEN '08/01/2012' AND '08/31/2012'  
where a.department = 'Service'
  AND cigarboxsection = 'Gross'
GROUP BY gmfssection, department  

-- this AS well: gmaccounts.Department, gmaccounts.gmfssection
SELECT gmfssection, department, SUM(gttamt)
FROM gmaccounts a
LEFT JOIN stgArkonaFFPXREFDTA b ON a.fsaccount = b.fxfact
  AND b.fxcyy = 2012
  AND b.fxconsol = ''
LEFT JOIN stgArkonaGLPMAST c ON b.fxgact = c.gmacct
  AND c.gmyear = 2012
  AND c.gmstyp = 'A'  
LEFT JOIN stgArkonaGLPTRNS d ON c.gmacct = d.gtacct
  and d.gtdate BETWEEN '08/01/2012' AND '08/31/2012'  
where a.department = 'Service'
  AND gmfssection IN ('CostOfSales','Sales')
GROUP BY gmfssection, department  

start with service since it adds up
dimAccount
  AccountKey integer,
  StoreCode cichar(3), 
  Account cichar(10),
  
SELECT a.fxfact, b.*
FROM stgArkonaFFPXREFDTA a
--LEFT JOIN stgArkonaGLPMAST b ON a.fxgact = b.gmacct
--  AND b.gmyear = 2012 
--  AND b.gmstyp = 'a'
LEFT JOIN stgArkonaGLPTRNS b ON a.fxgact = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.fxcyy = 2012
  AND a.fxconsol = ''  
  AND a.fxfact in ('011','013','015')
  
  
SELECT gmstyp, gmyear, gmacct, gmtype, gmdesc, gmdept, gmactive
FROM stgArkonaGLPMAST  
WHERE gmyear = 2012
  AND gmstyp = 'a'

-- the last time transactions against inactive accounts
select a.gmacct, MIN(b.gtdate), MAX(gtdate)
FROM stgArkonaGLPMAST a
inner JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
WHERE gmactive = 'n'
GROUP BY a.gmacct

-- 10/7
SELECT a.*, c.gmacct, c.gmtype, c.gmdept, c.gmdesc
FROM gmaccounts a
LEFT JOIN stgArkonaFFPXREFDTA b ON a.fsaccount = b.fxfact
  AND b.fxcyy = 2012
  AND b.fxconsol = ''
LEFT JOIN stgArkonaGLPMAST c ON b.fxgact = c.gmacct
  AND c.gmyear = 2012
  AND c.gmstyp = 'A'  
where a.fsaccount like '02%'-- IN ('020','021','022','023','024','025','026','027','029')

SELECT c.gmacct, fsaccount, fsdescription, gmfssection, department, SUM(gttamt), gmdept
FROM gmaccounts a
LEFT JOIN stgArkonaFFPXREFDTA b ON a.fsaccount = b.fxfact
  AND b.fxcyy = 2012
  AND b.fxconsol = ''
LEFT JOIN stgArkonaGLPMAST c ON b.fxgact = c.gmacct
  AND c.gmyear = 2012
  AND c.gmstyp = 'A'  
LEFT JOIN stgArkonaGLPTRNS d ON c.gmacct = d.gtacct
  and d.gtdate BETWEEN '08/01/2012' AND '08/31/2012'  
where a.fsaccount like '02%'
  AND department = 'Service'
GROUP BY c.gmacct, fsaccount, fsdescription, gmfssection, department, gmdept  

ORDER BY gmdept

SELECT SUM(gttamt), gmdept
FROM gmaccounts a
LEFT JOIN stgArkonaFFPXREFDTA b ON a.fsaccount = b.fxfact
  AND b.fxcyy = 2012
  AND b.fxconsol = ''
LEFT JOIN stgArkonaGLPMAST c ON b.fxgact = c.gmacct
  AND c.gmyear = 2012
  AND c.gmstyp = 'A'  
LEFT JOIN stgArkonaGLPTRNS d ON c.gmacct = d.gtacct
  and d.gtdate BETWEEN '08/01/2012' AND '08/31/2012'  
where a.fsaccount like '02%'
  AND department = 'Service'
GROUP BY gmdept  

