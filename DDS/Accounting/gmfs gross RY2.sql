DROP TABLE #dept;
CREATE TABLE #dept (
  dl1 cichar(24),
  dl2 cichar(24),
  dl3 cichar(24),
  dl4 cichar(24),
  dl5 cichar(24),
  dl6 cichar(24),
  dl7 cichar(24),
  dl8 cichar(24));
insert into #dept SELECT 'Grand Forks','RY2','Variable','F&I','New','New','New','New' FROM system.iota;  
insert into #dept SELECT 'Grand Forks','RY2','Variable','F&I','Used','Used','Used','Used' FROM system.iota;  
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Retail','Cars','Honda' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Retail','Trucks','Honda' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Retail','Cars','Nissan' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Retail','Trucks','Nissan' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Retail','Trucks','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Internal','Cars','Honda' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Internal','Trucks','Honda' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Internal','Cars','Nissan' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Internal','Trucks','Nissan' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Internal','Trucks','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Fleet','Cars','Honda' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Fleet','Trucks','Honda' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Fleet','Cars','Nissan' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Fleet','Trucks','Nissan' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Fleet','Trucks','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Accessories','Accessories','Accessories' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','New','Other','Other','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','Used','Retail','Cars','Certified' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','Used','Retail','Cars','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','Used','Retail','Trucks','Certified' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','Used','Retail','Trucks','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','Used','Wholesale','Cars','Cars' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','Used','Wholesale','Trucks','Trucks' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Variable','Sales','Used','Other','Other','Other' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Fixed','Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Fixed','Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
insert into #dept SELECT 'Grand Forks','RY2','Fixed','Parts','Parts','Parts','Parts','Parts' FROM system.iota;


-- new sales
DROP TABLE #newSales;
SELECT a.fxgact,a.fxfact, --b.gmacct, b.gmdesc,
  CASE
    WHEN (
      CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 400 AND 418
      OR 
      LEFT(fxfact,3) IN ('420','421')) THEN 'Cars'
    WHEN (
      CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 423 AND 438
      OR 
      LEFT(fxfact,3) IN ('440','441')) THEN 'Trucks'
    WHEN LEFT(fxfact,3) = '445' THEN 'Other'
  END AS shape,
  CASE
    WHEN LEFT(fxfact,3) = '445' THEN 'Other' 
    WHEN right(TRIM(fxfact),1) = 'c' THEN 'Honda'
    WHEN right(TRIM(fxfact),1) = 'j' THEN 'Nissan'
  END AS make,
  CASE
    WHEN (
      CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 400 AND 418
      OR 
      CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 423 AND 438) THEN 'Retail'
    WHEN LEFT(fxfact,3) IN ('420','440') THEN 'Fleet'
    WHEN LEFT(fxfact,3) IN ('421','441') THEN 'Internal'
    WHEN LEFT(fxfact,3) = '445' THEN 'Other'
  END AS saletype  
INTO #NewSales 
FROM stgArkonaFFPXREFDTA a 
--LEFT JOIN stgArkonaGLPMAST b ON a.fxgact = b.gmacct
--  AND gmyear = 2012
WHERE LEFT(fxfact,1) BETWEEN '0' AND '9'
  AND (
    CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 400 AND 418
    OR 
    CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 423 AND 438
    OR 
    LEFT(fxfact,3) IN ('420','421', '440','441', '445'))
  AND fxcyy = 2012
  AND fxconsol = '2'
  AND right(trim(fxfact),1) IN ('c','j');
  
  -- new retail cogs
DROP TABLE #newCOGS;
SELECT a.fxgact,a.fxfact, --b.gmdboa, b.gmdesc,
  CASE
    WHEN (
      CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 600 AND 618
      OR 
      LEFT(fxfact,3) IN ('620','621')) THEN 'Cars'
    WHEN (
      CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 623 AND 638
      OR 
      LEFT(fxfact,3) IN ('640','641')) THEN 'Trucks'
    WHEN LEFT(fxfact,3) = '645' THEN 'Other'
  END AS shape,
  CASE
    WHEN LEFT(fxfact,3) = '445' THEN 'Other' 
    WHEN right(TRIM(fxfact),1) = 'c' THEN 'Honda'
    WHEN right(TRIM(fxfact),1) = 'j' THEN 'Nissan'  
  END AS make,
  CASE
    WHEN (
      CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 600 AND 618
      OR 
      CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 623 AND 638) THEN 'Retail'
    WHEN LEFT(fxfact,3) IN ('620','640') THEN 'Fleet'
    WHEN LEFT(fxfact,3) IN ('621','641') THEN 'Internal'
    WHEN LEFT(fxfact,3) = '645' THEN 'Other'
  END AS saletype  
INTO #NewCOGS
-- SELECT *
FROM stgArkonaFFPXREFDTA a 
--LEFT JOIN stgArkonaGLPMAST b ON a.fxgact = b.gmacct
--  AND gmyear = 2012
WHERE LEFT(fxfact,1) BETWEEN '0' AND '9'
  AND (
    CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 600 AND 618
    OR 
    CAST(LEFT(fxfact,3) AS sql_integer) BETWEEN 623 AND 638
    OR 
    LEFT(fxfact,3) IN ('620','621', '640','641', '645'))
  AND fxcyy = 2012
  AND fxconsol = '2'
  AND right(trim(fxfact),1) IN ('c','j');  

  
  
  -- new retail sales
DELETE FROM zcb WHERE name = 'gmfs gross' AND dl2 = 'ry2';
INSERT INTO zcb
SELECT 'gmfs gross', 'RY2', a.*, 'Gross Profit' AS al1, 'Sales' AS al2, 'Sales' AS al3, 
  'NA' as al4, coalesce(b.fxgact, 'NA') AS glAccount, coalesce(fxfact, 'NA') AS gmcoa, 888, 1,999
FROM #dept a
LEFT JOIN #newSales b ON a.dl7 = b.shape collate ads_default_ci
  AND a.dl8 = b.make collate ads_default_ci
  AND a.dl7 = b.shape collate ads_default_ci
  AND a.dl6 = b.saletype collate ads_default_ci
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New';
  
  -- new retail cogs
INSERT INTO zcb
SELECT 'gmfs gross', 'RY2', a.*, 'Gross Profit' AS al1, 'COGS' AS al2, 'COGS' AS al3, 
  'NA' as al4, coalesce(b.fxgact, 'NA') AS glAccount, coalesce(fxfact, 'NA') AS gmcoa, 888, 1,999
FROM #dept a
LEFT JOIN #newCOGS b ON a.dl7 = b.shape collate ads_default_ci
  AND a.dl8 = b.make collate ads_default_ci
  AND a.dl7 = b.shape collate ads_default_ci
  AND a.dl6 = b.saletype collate ads_default_ci
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'; 
  
-- accessories
INSERT INTO zcb
SELECT 'gmfs gross', 'RY2', a.*, 'Gross Profit' AS al1, 'Sales' AS al2, 'Sales' AS al3, 
  'NA' as al4, '2457001' AS glAccount, '457A' AS gmcoa, 666, 1, 999
FROM #dept a
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 = 'Accessories';  
INSERT INTO zcb
SELECT 'gmfs gross', 'RY2', a.*, 'Gross Profit' AS al1, 'COGS' AS al2, 'COGS' AS al3, 
  'NA' as al4, '2657001' AS glAccount, '657A' AS gmcoa, 666, 1,999
FROM #dept a
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'New'
  AND a.dl6 = 'Accessories';   
  
-- check again page 7 lines 64-73: New Vehicle sls summary
-- honda cars & trucks, sales hi, gross right ON 
-- nissan sales hi, gross right ON
-- fs does NOT show incentives AS sales but figures INTO gross profit
SELECT dl2, dl4, dl5, dl6, dl7,dl8, 
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt,0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGlptrns b ON a.glaccount = b.gtacct
  AND gtdate BETWEEN '10/01/2012' AND '10/31/2012'
WHERE a.name = 'gmfs gross'
  AND dl2 = 'RY2'
GROUP BY dl2, dl4, dl5, dl6, dl7,dl8
ORDER BY dl7, dl6    

-- fs line level detail
DROP TABLE #acct;
CREATE TABLE #acct (
  al1 cichar(30),
  al2 cichar(30),
  al3 cichar(40),
  account cichar(6),
  page integer,
  line integer,
  dl4 cichar(24),
  dl5 cichar(24),
  dl6 cichar(24),
  dl7 cichar(24),
  dl8 cichar(24));
--f&i new 
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Finance Income-New','806',7, 1,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Ins Comm Earned-New','807',7, 2,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Fin & Ins Chargebacks','850',7, 3,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','Accessories','810',7, 4,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Accessories','860',7, 4,'F&I','New','New','New','New' FROM system.iota;
-- skip line 5 Divisional Ext Warranties acct 494 - NOT ON gm coa
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','GM Prot Plans','443',7, 6,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','GM Prot Plans','643',7, 6,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','Other Prot','444',7, 7,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Other Prot','644',7, 7,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Repo Losses-New','853',7, 8,'F&I','New','New','New','New' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','F&I Comp','855',7, 9,'F&I','New','New','New','New' FROM system.iota;
--f&i used 
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Finance Income-Used','808',7, 11,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Ins Comm Earned-Used','809',7, 12,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Fin & Ins Chargebacks','851',7, 13,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','Accessories','811',7, 14,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Accessories','861',7, 14,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','GM Prot Plans','454',7, 15,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','GM Prot Plans','654',7, 15,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales','Other Prot','455',7, 16,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Other Prot','655',7, 16,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','Repo Losses-New','854',7, 17,'F&I','Used','Used','Used','Used' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS','F&I Comp','856',7, 18,'F&I','Used','Used','Used','Used' FROM system.iota;

-- used
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Cars Rtl - Certified','446A',6, 1,'Sales','Used','Retail','Cars','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Rtl - Certified Cost','646A',6, 1,'Sales','Used','Retail','Cars','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Rtl - Certified Recon','647A',6, 1,'Sales','Used','Retail','Cars','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Cars Rtl - Other','446B',6, 2,'Sales','Used','Retail','Cars','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Rtl - Other Cost','646B',6, 2,'Sales','Used','Retail','Cars','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Rtl - Other Recon','647B',6, 2,'Sales','Used','Retail','Cars','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Trucks Rtl - Certified','450A',6, 4,'Sales','Used','Retail','Trucks','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Rtl - Certified Cost','650A',6, 4,'Sales','Used','Retail','Trucks','Certified' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Rtl - Certified Recon','651A',6, 4,'Sales','Used','Retail','Trucks','Certified' FROM system.iota;   
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Trucks Rtl - Other','450B',6, 5,'Sales','Used','Retail','Trucks','Other' FROM system.iota; 
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Rtl - Other Cost','650B',6, 5,'Sales','Used','Retail','Trucks','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Rtl - Other Recon','651B',6, 5,'Sales','Used','Retail','Trucks','Other' FROM system.iota; 
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Cars Wholesale','448',6, 8,'Sales','Used','Wholesale','Cars','Cars' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Cars Wholesale','648',6, 8,'Sales','Used','Wholesale','Cars','Cars' FROM system.iota;
-- 649 Adjustment NOT used
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Trucks Wholesale','452',6, 10,'Sales','Used','Wholesale','Trucks','Trucks' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Trucks Wholesale','652',6, 10,'Sales','Used','Wholesale','Trucks','Trucks' FROM system.iota;
-- 653 Adustment NOT used
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Used Other Automotive','456',6, 13,'Sales','Used','Other','Other','Other' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Used Other Automotive','656',6, 13,'Sales','Used','Other','Other','Other' FROM system.iota;
-- fixed mech
-- don't fuck around with pdq/main/detail/main shop at fs level
-- maybe
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Cust Labor Cars & LD Trks','460A',6, 21,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Cust Labor Cars & LD Trks','660A',6, 21,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'S/Cntr Customer Lab - Cars & LD Trks','460b',6, 22,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'S/Cntr Customer Lab - Cars & LD Trks','660B',6, 22,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Q/Srv Lab - Cars & LD Trks','460C',6, 23,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Q/Srv Lab - Cars & LD Trks','660C',6, 23,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Cust Lab Com, Flt & MD Trks','461A',6, 24,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Cust Labor Cars & LD Trks','661A',6, 24,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'S/Cntr Lab Com,Flt & MD Trks','461B',6, 25,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'S/Cntr Lab Com,Flt & MD Trks','661B',6, 25,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Q/Srv Lab - Com,Flt & MD Trks','461C',6, 26,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Q/Srv Lab - Com,Flt & MD Trks','661C',6, 26,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Warranty Claim Labor','462',6, 27,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Warranty Claim Labor','662',6, 27,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Internal Labor','463',6, 28,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Internal Labor','663',6, 28,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'New Veh Insp Lbr','464',6, 29,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'New Veh Insp Lbr','664',6, 29,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Adj Cost of bbr Sls','665',6, 30,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Shop Supplies','469',6, 32,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Shop Supplies','669',6, 32,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Sublet Repairs','466',6, 33,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Sublet Repairs','666',6, 33,'Mechanical','Mechanical','Mechanical','Mechanical','Mechanical' FROM system.iota;
-- fixed body shop
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Cust Paint Labor','470',6, 35,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Cust Paint Labor','670',6, 35,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Cust Body Labor','471',6, 36,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Cust Body Labor','671',6, 36,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Warranty Claim Labor','472',6, 37,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Warranty Claim Labor','672',6, 37,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Internal Labor','473',6, 38,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Internal Labor','673',6, 38,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Adj Cost of Lbr Sls','675',6, 39,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Sublet Repairs','476',6, 41,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Sublet Repairs','676',6, 41,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Pnt & Shop Matrls','479',6, 42,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Pnt & Shop Matrls','679',6, 42,'Body Shop','Body Shop','Body Shop','Body Shop','Body Shop' FROM system.iota;
-- parts
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Warranty Claims','480',6, 45,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Warranty Claims','680',6, 45,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Mech Cars & LD Trk RO','467',6, 46,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Mech Cars & LD Trk RO','667',6, 46,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Mech Com,Flt & MD Trks RO','468',6, 47,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Mech Com,Flt & MD Trks RO','668',6, 47,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Mech Quick Serv RO','478',6, 48,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Mech Quick Serv RO','678',6, 48,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Body Cust RO','477',6, 49,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Body Cust RO','677',6, 49,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Internal','481',6, 50,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Internal','681',6, 50,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Counter - Retail','482',6, 51,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Counter - Retail','682',6, 51,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Wholesale','483',6, 52,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Wholesale','683',6, 52,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Accessories','484',6, 53,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Accessories','684',6, 53,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Purchase Allowances','687',6, 54,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Adj P&A Inventory','688',6, 55,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Tires','490',6, 57,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Tires','690',6, 57,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Gas, Oil & Grease','491',6, 58,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Gas, Oil & Grease','691',6, 58,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'Sales', 'Miscellaneous','492',6, 59,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;
INSERT INTO #acct SELECT 'Gross Profit', 'COGS', 'Miscellaneous','692',6, 59,'Parts','Parts','Parts','Parts','Parts' FROM system.iota;

-- f&i
DELETE FROM zcb WHERE name = 'gmfs gross' AND dl4 = 'f&i';
INSERT INTO zcb
SELECT 'gmfs gross','RY2','Grand Forks','RY2',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',coalesce(c.fxgact,'NA'),coalesce(c.fxfact,'NA'),line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
  AND a.dl5 = b.dl5
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = c.fxfact
  AND c.fxconsol = '2'
  AND c.fxcyy = 2012  
  AND fxcode = 'GM' 
WHERE a.dl4 = 'F&I'; 
ORDER BY fxfact

-- ok
SELECT dl4, dl5, al3, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'F&I' 
  AND dl2 = 'RY2'
GROUP BY dl4, dl5, al3, line
ORDER BY line asc

-- used
INSERT INTO zcb
SELECT 'gmfs gross','RY2','Grand Forks','RY2',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',coalesce(c.fxgact,'NA'),coalesce(c.fxfact,'NA'),line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
  AND a.dl5 = b.dl5
  AND a.dl6 = b.dl6
  AND a.dl7 = b.dl7
  AND a.dl8 = b.dl8
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = c.fxfact
  AND c.fxconsol = '2'
  AND c.fxcyy = 2012 
  AND c.fxcode = 'GM' 
WHERE a.dl4 = 'Sales'
  AND a.dl5 = 'Used'
-- ok  
SELECT dl4, dl5, dl6, dl7, dl8, al2, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'sales' 
  AND dl5 = 'used'
  AND dl2 = 'ry2'
GROUP BY dl4, dl5, dl6, dl7, dl8, al2, line
ORDER BY line asc, al2 DESC
-- since cogs IS NOT shown ON ry2 fs, sh2 gross IN query
-- ok
SELECT dl5,dl6,dl7,dl8, page, line, 
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt, 0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'sales'
  AND dl2 = 'ry2'
  AND dl5 = 'used' 
GROUP BY dl5,dl6,dl7,dl8, page, line 
ORDER BY line

-- mechanical
delete FROM zcb WHERE dl2 = 'ry3' AND glaccount LIKE '2%'
INSERT INTO zcb
SELECT 'gmfs gross','RY2','Grand Forks','RY2',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',coalesce(c.fxgact,'NA'),coalesce(c.fxfact,'NA'),line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = c.fxfact
  AND c.fxconsol = '2'
  AND c.fxcyy = 2012  
  AND fxcode = 'GM'
WHERE a.dl4 = 'Mechanical'
  AND a.dl2 = 'ry2'
-- ok
SELECT dl4, al3, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'mechanical'
  AND dl2 = 'ry2'
GROUP BY dl4, al3, line
ORDER BY line asc

-- parts
DELETE FROM zcb WHERE name = 'gmfs gross' AND dl4 = 'parts'
INSERT INTO zcb
SELECT 'gmfs gross','RY2','Grand Forks','RY2',a.dl3,a.dl4,a.dl5,a.dl6,a.dl7,a.dl8,
  b.al1,b.al2,b.al3,'NA',c.fxgact,c.fxfact,line,1,page 
FROM #dept a 
LEFT JOIN #acct b ON a.dl4 = b.dl4
LEFT JOIN stgArkonaFFPXREFDTA c ON b.account = left(c.fxfact, 3)
  AND c.fxconsol = '2'
  AND c.fxcyy = 2012  
  AND fxcode = 'GM'
  AND LEFT(fxgact,1) = '2' 
WHERE a.dl4 = 'parts'
  AND dl2 = 'ry2'
-- ok
SELECT dl4, al3, line, SUM(coalesce(gttamt, 0))
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'parts' 
  AND dl2 = 'ry2'
GROUP BY dl4, al3, line
ORDER BY line asc;

-- page 2 line 2
SELECT SUM(coalesce(gttamt, 0)) AS store, 
  SUM(CASE WHEN dl3 = 'variable' THEN gttamt ELSE 0 END) AS variable,
  SUM(CASE WHEN dl3 = 'fixed' THEN gttamt ELSE 0 END) AS fixed
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross'
  AND a.dl2 = 'ry2'
-- page 6 lines 1- 10
SELECT dl6, dl7, dl8, line, 
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt, 0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl4 = 'sales' 
  AND dl5 = 'used'
  AND dl2 = 'ry2'
GROUP BY dl4, dl5, dl6, dl7, dl8, line
ORDER BY dl4, dl5, dl6, dl7, dl8, line
-- page 6 fixed lines 21 - 59
SELECT dl5, line, al3,
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt, 0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl3 = 'fixed'
  AND dl2 = 'ry2'
GROUP BY dl5, line, al3
ORDER BY line
-- page 6 lines 15 - 17
-- look at F&I
-- this IS the deal with line 16, gross IS ok, but other prot plans (455) IS NOT included
-- IN sales  cogs
SELECT dl4, 
  SUM(CASE WHEN al2 = 'sales' THEN gttamt ELSE 0 END) AS sales,
  SUM(CASE WHEN al2 = 'cogs' THEN gttamt ELSE 0 END) AS cogs,
  SUM(coalesce(gttamt, 0)) AS gross
FROM zcb a
LEFT JOIN stgArkonaGLPTRNS b ON a.glaccount = b.gtacct
  AND year(gtdate) = 2012
  AND month(gtdate) = 10
WHERE a.name = 'gmfs gross' 
  AND dl3 = 'variable'
  AND dl2 = 'ry3'
GROUP BY dl4
