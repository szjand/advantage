2/** issues **/
unit COUNT: 17257XX

differences BETWEEN query AND FS
parts sales:  diff betw fs - query: 1108561 - 1069255 = 48306
nc sales: diff betwe fs - query: 4040711 - 4016715 = 23996
bs sales: parts gross
aftermarket split IS suppose to be 1/3 sales/shop/parts, does NOT ADD up
service sales: QU: 520722  FS:  544721 diff: -23999
  doc does NOT include 146604

/** issues **/

-- sales store
-- p2:l1
        total      var      fixed
fs:    9943735   8008386   1935349
qu:    9406401   7519045   1887355
SELECT sum(b.gttamt) AS store,
  SUM(CASE WHEN a.gmdept IN ('NC', 'UC') THEN b.gttamt END) AS Variable,
  SUM(CASE WHEN a.gmdept NOT IN ('NC', 'UC') THEN b.gttamt END) AS Fixed
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale

-- sales store/acct
SELECT a.gmacct, a.gmdesc, sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
--  AND a.gmdept = 'uc'
GROUP BY a.gmacct, a.gmdesc  
UNION
SELECT 'Total', 'Store Sales', sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale

-- sales BY dept
SELECT a.gmdept, sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
--  AND a.gmdept = 'uc'
GROUP BY a.gmdept   

-- p6 uc sales  
August, July, July: right on
SELECT a.gmacct, a.gmdesc, sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'uc'
GROUP BY a.gmacct, a.gmdesc  
UNION
SELECT 'Total', 'US Sales', sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'uc'


-- 10/1
-- nc sales
august
  fs:  4040711
  qu:  4016715
  doc: 3893229
how the fuck am i going to pin this down, the doc does NOT include ALL models
doc does NOT have sonic, spark, mv1 
exclude those models and qu & doc now match EXCEPT doc shows 6751 for accessories
acct 1457001/1657001 : Aftermarket Split 1/3 to sales, service, parts
SELECT a.gmacct, a.gmdesc, sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'nc'
--  AND a.gmacct NOT IN ('1408001','1413001', '1433007')
GROUP BY a.gmacct, a.gmdesc  
UNION
SELECT 'Total', 'NC Sales', sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'nc'  
--  AND a.gmacct NOT IN ('1408001','1413001', '1433007') 
  
SELECT SUM(gttamt), SUM(gttamt)/3 -- 23996
FROM stgArkonaGLPTRNS
WHERE gtacct = '1457001'
   AND gtdate BETWEEN '08/01/2012' AND '08/31/2012'
-- 10/5 nc sales   
   
-- parts sales  
doc: 1059236 (includes P&A Accessories(148400/168400): 6751 (aftermarket split) 
que: 1053504 + 6751 = 1060255
fs:  1108561
doc does NOT include 149142 (SLS GOG INTERNAL OTHER): 1019.66, which accounts for the
  diff BETWEEN doc & query
diff betw fs * query: 1108561 - 1969255 = 48306
SELECT a.gmacct, a.gmdesc, sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'pd'
GROUP BY a.gmacct, a.gmdesc  
UNION
SELECT 'Total', 'Parts Sales', sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'pd'  
  
-- bs sales  
fs:  282067
qu:  313126 - 31059 (parts gross) = 282607
why does the statement NOT include parts sales IN bs sales?
 
SELECT a.gmacct, a.gmdesc, sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'bs'
GROUP BY a.gmacct, a.gmdesc  
UNION
SELECT 'Total', 'BS Sales', sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'bs'  
  
gmacct	gmdesc	EXPR                 query           doc
147000	SLS B/S CUST PAINT LBR	  ($53,178.23)      53178
147100	SLS B/S CUST BODY LBR    ($105,403.02)     105403
147200	SLS-B/S WARR LBR	         ($3,801.19)       3801
147300	SLS-B/S INT LBR	          ($62,910.90)      62911
147600	SLS-B/S SUBLET	           ($5,348.83)       5349
147701	SLS BODY SHOP P&A NON-GM	($31,059.39)    -- this IS parts gross             
147900	SLS-B/S PAINT&MAT	        ($49,897.70)      47298
147901	SALES B/S HARDWARE	       ($1,527.11)     -- included with 147900u
Total	BS Sales	                 ($313,126.37)     277940
 Parts Gross                                        31059
                                                   309000

 
doc paint & mat
147900  sls
167900  cogs
147901  sls
167901  cogs
16105P  exp
167906  cogs (- amount adds to sales)
  
parts gross 147701 ::  477S
 
SELECT a.gmacct, a.gmdesc, cast(cast(sum(gttamt) AS sql_float) AS sql_integer) * -1-- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'bs'
GROUP BY a.gmacct, a.gmdesc  

-- mech sales

qu:  CW  33501   
     QL  94618
     RE  13709
     SD 378894
     ---------
        520722  FS:  544721 
        
SELECT a.gmacct, a.gmdept, a.gmdesc, cast(cast(sum(gttamt) AS sql_float) AS sql_integer) * -1-- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept IN ('CW','QL','RE','SD')
GROUP BY a.gmacct, a.gmdept, a.gmdesc  
ORDER BY gmdept      
-- total BY dept
SELECT a.gmdept, cast(cast(sum(gttamt) AS sql_float) AS sql_integer) * -1-- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept IN ('CW','QL','RE','SD')
GROUP BY a.gmdept
ORDER BY gmdept    
-- let's look at SD     
SELECT a.gmacct, a.gmdesc, cast(cast(sum(gttamt) AS sql_float) AS sql_integer) * -1-- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'SD'
GROUP BY a.gmacct, a.gmdesc  
UNION
SELECT 'Total', 'SD Sales', sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'sd'  
      
SELECT a.gmacct, a.gmdesc, cast(cast(sum(gttamt) AS sql_float) AS sql_integer) * -1-- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'RE'
GROUP BY a.gmacct, a.gmdesc  
UNION
SELECT 'Total', 'SD Sales', sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'RE'  
  
      
SELECT a.gmacct, a.gmdesc, cast(cast(sum(gttamt) AS sql_float) AS sql_integer) * -1-- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'CW'
GROUP BY a.gmacct, a.gmdesc  
UNION
SELECT 'Total', 'SD Sales', sum(gttamt) -- gtdoc#, gttamt
-- SELECT *
FROM stgarkonaGLPMAST a 
INNER JOIN stgArkonaGLPTRNS b ON a.gmacct = b.gtacct
  AND b.gtdate BETWEEN '08/01/2012' AND '08/31/2012'
WHERE a.gmyear = 2012
  AND a.gmstyp = 'A'
  AND a.gmtype = '4' -- sale
  AND a.gmdept = 'CW'   
            
 -- doc does NOT include 146604
  
gmacct	gmdesc	EXPR	                 qu        doc
146000	SLS SRV C/L CARS & LD TKS	  141733      141733
146001	SLS SRV MBI LBR CAR/LD TK	   16499       16499
146007	LABOR SALES ACCESSORIES	      9232        9233
146204	SLQ-WARR LBR-CAR SVC	       60437       60437
146301	SLS-INT LBR N/C PREP	       11233       11233
146302	SLS-INT LBR U/C RECON	       42720         RE
146304	SLS-INT LBR CAR SERV	       88762       88763
146604	SLS-SUBLET CAR SVC	          8275
INSP LABOR N/C PREP 
  146401  SLS-INSP LBR N/C PREP                      0
  146424  SLS NEW VEH PREP WASH                   4945
                                    -------------------
                                    378895      332843
PDQ                                  94619       94619
REC 
doc SET up 
  146020   SLS CUST LABOR CARS SHP C  RE          8765                               
  146302   SLS-INT LBR U/C RECON      SD         42720                                                                            
  146320   SLS INTERNAL SHOP C      -- account NOT used --
query  
gmacct	gmdesc	EXPR	
146020	SLS CUST LABOR CARS SHP C	    8764
146424	SLS NEW VEH PREP WASH	4944    4944
  
                                    487218       478947
                                    
CAR WASH                             33502        33502                                   
                                    520720       512449
SELECT *
FROM stgARkonaGLPTRNS
WHERE gtacct = '146401'
  AND year(gtdate) > 2011
ORDER BY gtdate desc
                                