-- ADD department to expense accts
-- AND level 3 dept to sales/cogs
-- changed rental to level2 under variable
-- may yet END up using gmdept for level3 service accts 

-- struggling a little bit with gross -- maybe just more of a display issue
-- ie p2, 3, 4 display total sales & gross with expense detail
-- 10/13 reclassify parts split accounts (467,468,477,478, 667, 668, 677,678) gmfsDepartmentLevel2  from mech/body to parts
--   fixed level 3 based ON gm dept
--   maybe the same thing for fi
-- 10/17
--   remove gmfsDepartmentLevel3, it IS NOT IN financial statement, DO NOT force it IN here
DROP TABLE #wtf;
SELECT a.gmacct, a.gmdboa as costAccount, a.gmtype, a.gmdesc, a.gmdept, b.fxfact, fxfp01 AS split,
  CASE
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 200 AND 296 THEN 'Assets'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 300 AND 359 THEN 'Liabilities'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 360 AND 399 THEN 'Owner Equity'
--    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 509 -- 510 assigned to 172754 a COGS account
--      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 511 AND 599 
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 599      
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 806 AND 811 THEN 'Sales'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 600 AND 744 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 850 AND 861 THEN 'COGS'
--      OR CAST(LEFT(fxfact, 3) AS sql_integer) = 510 THEN 'COGS'   -- 510 assigned to 172754 a COGS account
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 902 AND 910 THEN 'Additions to Income'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 952 AND 955 THEN 'Deductions from Income'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 11 AND 99 THEN 'Expenses' 
  END AS gmfsAccountLevel1,  
  CASE 
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 200 AND 205 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) = 260 THEN 'Cash & Contracts'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 210 AND 225 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 261 AND 264 THEN 'Receivables'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 230 AND 258 THEN 'Inventories'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 270 AND 274 THEN 'Prepaid Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 275 AND 277 THEN 'Working Assets'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 280 AND 289 THEN 'Fixed Assets'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 291 AND 296 THEN 'Other Assets'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 11 AND 15 THEN 'Variable Selling Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 20 AND 29 THEN 'Personnel Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 33 AND 79 THEN 'Semi-Fixed Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 80 AND 92 THEN 'Fixed Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 97 AND 99 THEN 'Adjustment Expenses'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 360 AND 399 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 599 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 806 AND 811 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 600 AND 744 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 850 AND 861 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 902 AND 910 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 952 AND 955 THEN 'NA'
  END AS gmfsAccountLevel2,
  CASE
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 200 AND 399 THEN 'Dealership'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 457 THEN 'Variable'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 600 AND 657 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 806 AND 811 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 850 AND 860
      OR CAST(LEFT(fxfact, 3) AS sql_integer) IN (530,534,535,536, 510, 710)
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 730 AND 738
      OR CAST(LEFT(fxfact, 3) AS sql_integer) IN (443,444,454,455,643,644,654,655)  THEN 'Variable'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 460 AND 492 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 660 AND 692 THEN 'Fixed Operations'
--    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) IN (530,534,535,536, 510, 710)
--      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 730 AND 738 THEN 'Lease & Rental'
  END AS gmfsDepartmentLevel1,
  CASE
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 200 AND 399 THEN 'NA'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 400 AND 438 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) IN (440,441,445,457)  THEN 'New Vehicle Department'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 600 AND 638 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) IN (640,641,645,657)  THEN 'New Vehicle Department'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 446 AND 452 OR CAST(LEFT(fxfact, 3) AS sql_integer) = 456 THEN 'Used Vehicle Department'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 646 AND 653 OR CAST(LEFT(fxfact, 3) AS sql_integer) = 656 THEN 'Used Vehicle Department'
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 806 AND 811 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 850 AND 860
      OR CAST(LEFT(fxfact, 3) AS sql_integer) IN (443,444,454,455,643,644,654,655)  THEN 'Finance & Insurance'  
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 460 AND 469 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 660 AND 669 THEN 'Mechanical'   
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 470 AND 479 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 670 AND 679 THEN 'Body Shop'  
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 467 AND 492 
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 667 AND 692 THEN 'Parts & Accessories' 
    WHEN CAST(LEFT(fxfact, 3) AS sql_integer) IN (510, 710, 530,534,535,536)
      OR CAST(LEFT(fxfact, 3) AS sql_integer) BETWEEN 730 AND 738 THEN 'Lease & Rental'                 
  END AS gmfsDepartmentLevel2
INTO #wtf  
FROM stgArkonaGLPMAST a
LEFT JOIN stgArkonaffpxrefdta b ON a.gmacct = b.fxgact
  AND b.fxconsol = ''
  AND b.fxcyy = 2012
WHERE a.gmyear = 2012
  AND a.gmstyp = 'a'
ORDER BY gmdept, gmacct;  

--ALTER TABLE #wtf
--ADD COLUMN gmfsDepartmentLevel3 cichar(20);

--UPDATE #wtf
--SET gmfsDepartmentLevel3 = 'NA'
--WHERE gmfsDepartmentLEvel1 = 'Dealership';

UPDATE #wtf
SET gmfsDepartmentLevel1 = 'Variable'
-- SELECT * FROM #wtf
WHERE gmfsAccountLevel1 = 'Expenses'
  AND right(TRIM(fxfact), 1) IN ('A','B','C');
  
UPDATE #wtf
SET gmfsDepartmentLevel1 = 'Fixed Operations'
-- SELECT * FROM #wtf
WHERE gmfsAccountLevel1 = 'Expenses'
  AND right(TRIM(fxfact), 1) IN ('D','E','F');  
  
UPDATE #wtf
SET gmfsDepartmentLevel2 = 'New Vehicle Department'
-- SELECT * FROM #wtf
WHERE gmfsAccountLevel1 = 'Expenses'
  AND right(TRIM(fxfact), 1) = 'A';  
  
UPDATE #wtf
SET gmfsDepartmentLevel2 = 'Used Vehicle Department'
-- SELECT * FROM #wtf
WHERE gmfsAccountLevel1 = 'Expenses'
  AND right(TRIM(fxfact), 1) = 'B';   
  
--UPDATE #wtf
--SET gmfsDepartmentLevel3 = 'NA'
-- SELECT * FROM #wtf
--WHERE gmfsAccountLevel1 = 'Expenses'
--  AND right(TRIM(fxfact), 1) IN ('A','B');   
  
UPDATE #wtf
SET gmfsDepartmentLevel2 = 'Lease & Rental'
-- SELECT * FROM #wtf
WHERE gmfsAccountLevel1 = 'Expenses'
  AND right(TRIM(fxfact), 1) = 'C';   
  
--UPDATE #wtf
--SET gmfsDepartmentLevel3 = 'NA'
-- SELECT * FROM #wtf
--WHERE gmfsAccountLevel1 = 'Expenses'
--  AND right(TRIM(fxfact), 1) = 'C';    
  
--UPDATE #wtf
--SET gmfsDepartmentLevel3 = 'NA'
-- SELECT * FROM #wtf
--WHERE gmfsDepartmentLevel2 IN ('New Vehicle Department', 'Used Vehicle Department',
--  'Finance & Insurance', 'Lease & Rental'); 
  
-- parts split accounts  
UPDATE #wtf
SET gmfsDepartmentLevel2 = 'Parts & Accessories' 
WHERE CAST(LEFT(fxfact, 3) AS sql_integer) IN (467,468,477,478, 667, 668, 677,678);


-- fixed gmfsDepartmentLevel2 expense accts
UPDATE #wtf
SET gmfsDepartmentLevel2 = 
  CASE 
    WHEN right(TRIM(fxfact), 1) = 'd' THEN 'Mechanical'
    WHEN right(TRIM(fxfact), 1) = 'e' THEN 'Body Shop'
    WHEN right(TRIM(fxfact), 1) = 'f' THEN 'Parts & Accessories'
  END 
-- SELECT * FROM #wtf
WHERE gmfsDepartmentLevel1 = 'Fixed Operations'
  AND gmfsAccountLevel1 = 'Expenses';

-- fixed gmfsDepartmentLevel3  
--UPDATE #wtf
--SET gmfsDepartmentLevel3 = 
--  CASE 
--    WHEN gmdept = 'CW' THEN 'Car Wash'
--    WHEN gmdept = 'QL' THEN 'PDQ'
--    WHEN gmdept = 'RE' THEN 'Detail'
--    WHEN gmdept = 'SD' THEN 'Main Shop'
--  END 
-- SELECT * FROM #wtf
--WHERE gmfsDepartmentLevel1 = 'Fixed Operations'
--  AND gmfsDepartmentLevel2 = 'Mechanical';  
   
-- need nulls fo gmdboa
UPDATE #wtf
SET costaccount = NULL
WHERE costaccount = ''


/*
DROP TABLE dimAccount;
CREATE TABLE dimAccount (
  AccountKey autoinc,
  glAccount cichar(10),
  glCostAccount cichar(10),
  glAccountType cichar(1),
  glAccountDescription cichar(30),
  glDepartment cichar(2),
  gmfsAccount cichar(10),
  split double,
  gmfsAccountLevel1 cichar(22),
  gmfsAccountLevel2 cichar(25),
  gmfsDepartmentLevel1 cichar(16),
  gmfsDepartmentLevel2 cichar(23)); 
  
INSERT INTO dimAccount (glAccount, glCostAccount, glAccountType, glAccountDescription,
  glDepartment, gmfsAccount, split, gmfsAccountLevel1, gmfsAccountLevel2,
  gmfsDepartmentLevel1, gmfsDepartmentLevel2)
SELECT gmacct, costaccount, gmtype, gmdesc, gmdept, fxfact, split, 
 gmfsAccountLevel1, gmfsAccountLevel2, gmfsDepartmentLevel1, gmfsDepartmentLevel2  
FROM #wtf; 
*/
/******************************************************************************/



-- page 2 store totals L1, 2, 57, 59, 65
-- right ON for july/august
-- except for assets/liabilities, NOT going there
SELECT 
  SUM(CASE WHEN gmfsAccountLevel1 = 'Sales' THEN gttamt END) AS "Net Sales",
  SUM(CASE WHEN gmfsAccountLevel1 IN ('Sales', 'COGS') THEN gttamt END) AS "Gross Profit",
  SUM(CASE WHEN gmfsAccountLevel1 = 'Expenses' THEN gttamt END) AS "Total Expenses",
  SUM(CASE WHEN gmfsAccountLevel1 IN ('Additions to Income','Deductions from Income') THEN  gttamt END) AS "Net ADD & Ded",
  SUM(gttamt) AS "Net Profit"
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
  AND gmfsAccountLevel1 NOT IN ('Assets','Liabilities', 'Owner Equity')
   
    
 
-- page 2 var/fixed lines 1,2, 57, 58
-- semi-fixed: discrepancy of $790 BETWEEN var & fixed, 
--             statement IS wrong, jeri fixed routing
SELECT *
FROM (
  SELECT gmfsDepartmentLevel1, 
    SUM(CASE WHEN gmfsAccountLevel1 = 'Sales' THEN gttamt END) AS "Net Sales",
    SUM(CASE WHEN gmfsAccountLevel1 IN ('Sales', 'COGS') THEN gttamt END) AS "Gross Profit",
    SUM(CASE WHEN gmfsAccountLevel1 = 'Expenses' THEN gttamt END) AS "Total Expenses",
    SUM(gttamt) AS "Net Profit"
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
    WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
    AND gmfsAccountLevel1 NOT IN ('Assets','Liabilities', 'Owner Equity')
  GROUP BY gmfsDepartmentLevel1) a   
WHERE gmfsDepartmentLevel1 IS NOT NULL   
-- page 2 var/fixed lines 7, 17, 40, 55, 57
SELECT gmfsDepartmentLevel1, gmfsaccountlevel2, SUM(gttamt) AS total
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
  AND gmfsDepartmentLevel1 <> 'Dealership'
  AND gmfsaccountlevel1 = 'Expenses'
GROUP BY gmfsDepartmentLevel1, gmfsaccountlevel2
UNION 
SELECT gmfsDepartmentLevel1, 'Total Expenses', SUM(gttamt)
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
  AND gmfsDepartmentLevel1 <> 'Dealership'
  AND gmfsaccountlevel1 = 'Expenses'
GROUP BY gmfsDepartmentLevel1
 
-- page 3
-- ? net profit
-- ? f/i break down BETWEEN new & used
-- ? f/i transfer (simple split based ON new/used?)
-- look at f/i detail ON page 7
SELECT gmfsDepartmentLevel2, gmfsAccountLevel1, gmfsAccountLevel2, SUM(gttamt) AS total
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  AND gmfsDepartmentLevel1 = 'Variable'
WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
--  AND gmfsAccountLevel2 <> 'NA'
GROUP BY gmfsDepartmentLevel2, gmfsAccountLevel1, gmfsAccountLevel2  

-- page 3 lines 3, 7, 17, 40
SELECT gmfsDepartmentLevel2, 
  CASE gmfsAccountLevel2
    WHEN 'NA' THEN 'Gross'
    ELSE gmfsAccountLevel2 
  END AS gmfsAccountLevel2,
  SUM(total) AS total
FROM (
  SELECT gmfsDepartmentLevel2, gmfsAccountLevel1, gmfsAccountLevel2, SUM(gttamt) AS total
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
    AND gmfsDepartmentLevel1 = 'Variable'
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
  --  AND gmfsAccountLevel2 <> 'NA'
  GROUP BY gmfsDepartmentLevel2, gmfsAccountLevel1, gmfsAccountLevel2) a  
GROUP BY gmfsDepartmentLevel2, gmfsAccountLevel2  
-- additions ot income does this contribute to page 2 line 59?
SELECT *
FROM #wtf
WHERE CAST(LEFT(fxfact, 3) AS sql_integer) IN (902,903,905,909,910)
-- basis for gmfsDepartmentLevel3
SELECT *
FROM #wtf
WHERE gmacct IN (
SELECT gmacct-- , fxfact, gmfsDepartmentLevel2, SUM(gttamt) AS total
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  AND gmfsDepartmentLevel2 = 'Finance & Insurance'
  AND gmfsAccountLevel1 = 'Sales'
WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
GROUP BY gmacct, fxfact, gmfsDepartmentLevel2) 

-- page 4
-- fucking parts split fucking up sales total
-- fixed BY changing split accounts to gmfsDepartmentLevel2 = parts
-- page 4 sales/gross: lines 1, 2
SELECT a.gmfsDepartmentLevel2, b.sales, a.gross
FROM (
  SELECT gmfsDepartmentLevel2, SUM(gttamt) AS gross
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
    AND gmfsDepartmentLevel1 = 'Fixed Operations'
    AND gmfsAccountLevel1 <> 'Expenses'
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'
  GROUP BY gmfsDepartmentLevel2) a
INNER JOIN (
  SELECT gmfsDepartmentLevel2, SUM(gttamt) AS sales
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
    AND gmfsDepartmentLevel1 = 'Fixed Operations'
    AND gmfsAccountLevel1 = 'Sales'
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'
  GROUP BY gmfsDepartmentLevel2) b ON a.gmfsDepartmentLevel2 = b.gmfsDepartmentLevel2 

-- page 4 lines 17, 40, 55, 57  Expenses
SELECT gmfsDepartmentLevel2, gmfsAccountLevel2, SUM(gttamt) AS total
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  AND gmfsDepartmentLevel1 = 'Fixed Operations'
  AND gmfsAccountLevel1 = 'Expenses'
WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'
GROUP BY gmfsDepartmentLevel2, gmfsAccountLevel2  
UNION
SELECT gmfsDepartmentLevel2, 'Total Expenses', SUM(gttamt) AS total
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  AND gmfsDepartmentLevel1 = 'Fixed Operations'
  AND gmfsAccountLevel1 = 'Expenses'
WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'
GROUP BY gmfsDepartmentLevel2

/************** parts split ****************************************************
-- 10/13 fucking parts split
How does parts
-- WHEN IS it used, WHERE does it show up : page 4 line 59
-- accounts affected
SELECT *
FROM #wtf
WHERE fxfact IN ('477a','477s','478a', '478s','467a', '467s', '468a','468s')
-- this might be interesting
account gmfsDepartmentLevel1      gmfsDepartmentLevel2     gmfsDepartmentLevel3
477a    Fixed                     Body Shop
        because bet 460/492       because bet 470/479
        
-- WHERE the fuck am i
these 2 pages agree, parts split shows up ON line 59 contributing to net profit  

page line   dept     sales    gross        
4    1/2    mech     544721   378243
4    1/2    body     282067   171141
4    1/2    parts   1108561   290025
6    34     mech     544721   378243
6    43     body     282067   171141
6    61     parts   1108561   290025

SELECT fxfact, gmacct, sum(gttamt)--, gmfsDepartmentLevel2, gmdept 
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  AND gmfsDepartmentLevel1 = 'Fixed Operations'
WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012' 
--  AND gmfsAccountLevel1 = 'Sales'
--  AND gmfsDepartmentLevel2 = 'Parts & Accessories'
  AND fxfact IN ('477a','477s','478a', '478s','467a', '467s', '468a','468s')
GROUP BY fxfact, gmacct
************** parts split ****************************************************/



-- gmfsDepartmentLevel2 <> gmdept
SELECT *
FROM (       
SELECT gmacct, gmtype, gmdesc, gmdept, fxfact, gmfsDepartmentLevel2,
  CASE 
    WHEN gmdept = 'bs' AND gmfsDepartmentLevel2 <> 'Body Shop' THEN 'body' 
    WHEN gmdept = 'pd' AND gmfsDepartmentLevel2 <> 'Parts & Accessories' THEN 'parts' 
    WHEN gmdept IN ('sd','re','ql','cw') AND gmfsDepartmentLevel2 <> 'Mechanical' THEN 'mech'
    WHEN gmdept = 'FI' AND gmfsDepartmentLevel2 <> 'Finance & Insurance' THEN 'fi'
  END AS dif
FROM #wtf) a
WHERE dif IS NOT null 

-- AND the inverse gmdept <> gmfsDepartmentLevel2 
SELECT *
FROM (       
SELECT gmacct, gmtype, gmdesc, gmdept, fxfact, gmfsDepartmentLevel2,
  CASE 
    WHEN gmfsDepartmentLevel2 = 'Body Shop' AND gmdept <> 'bs' THEN 'body' 
    WHEN gmfsDepartmentLevel2 = 'Parts & Accessories' AND gmdept <> 'pd' THEN 'parts' 
    WHEN gmfsDepartmentLevel2 = 'Mechanical' AND gmdept NOT IN ('sd','re','ql','cw') THEN 'mech'
    WHEN gmfsDepartmentLevel2 = 'Finance & Insurance' AND gmdept = 'FI' THEN 'fi'
  END AS dif
FROM #wtf) a
WHERE dif IS NOT null 
  
  
 
  
SELECT right(TRIM(fxfact), 1), gmfsdepartment, COUNT(*)
FROM #wtf a
LEFT JOIN edwaccountdim b ON a.gmacct = b.account
WHERE gmfsaccountlevel1 = 'Expenses'
GROUP BY right(TRIM(fxfact), 1), gmfsdepartment

-- generally:  A: New
--             B: Used
--             C: Rental
--             D: Service
--             E: Body
--             F: Parts

SELECT *
FROM stgArkonaGLPMAST
WHERE gmyear = 2012
  AND gmstyp = 'a'
  AND gmdesc LIKE '%T/S%'
  
SELECT *
FROM #wtf
WHERE gmdesc LIKE '%T/S%'
  

SELECT right(TRIM(fxfact), 1), gmfsdepartment, gmdept, COUNT(*)
FROM #wtf a
LEFT JOIN edwaccountdim b ON a.gmacct = b.account
WHERE gmfsaccountlevel1 = 'Expenses'
GROUP BY right(TRIM(fxfact), 1), gmdept, gmfsdepartment

SELECT a.gmacct, a.gmtype, a.gmdept, a.fxfact, b.description, b.gmfsaccount, gmfsdepartment
FROM #wtf a
LEFT JOIN edwaccountdim b ON a.gmacct = b.account
WHERE gmfsaccountlevel1 = 'Expenses'
  AND right(TRIM(fxfact), 1) IN ('7','8','9')
  
SELECT a.gmacct, a.gmtype, a.gmdept, a.fxfact, b.description, b.gmfsaccount, gmfsdepartment
FROM #wtf a
LEFT JOIN edwaccountdim b ON a.gmacct = b.account
WHERE gmfsaccountlevel1 = 'Expenses'
  AND right(TRIM(fxfact), 1) = 'F'
  AND gmdept <> 'PD'


-- 10/15 cigar box
-- gross
-- fuck fuck fuck cigar box includes the parts split just pdq/main shop
-- fuck, before we go there detail gross is ALL fucked up
-- pdq IS off  
-- cw right ON
-- 10/16 body shop parts split 147701 ALL goes to the body shop
--    147700 IS split
-- which makes sense, 147701 split = 1, NOT .5
SELECT * FROM #wtf WHERE gmacct IN ('147700', '147701')

-- need to make sure gmfsDepartmentLevel2 IS correct
-- worried abt 147001 should it be parts OR bs, ON the statement it's part of 477,
-- but ON parts split it's body shop

-- per greg the problem with RE totals IS that i used gldept to categorize gmfsDepartmentLevel3 
--  but IN reality IS the service type ON the RO

SELECT a.gmfsDepartmentLevel2, a.gmdept, b.sales, a.gross
--SELECT SUM(b.sales), sum(a.gross)
FROM (
  SELECT gmfsDepartmentLevel2, gmdept, SUM(gttamt) AS gross
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
    AND gmfsDepartmentLevel2 = 'Mechanical'    
    AND gmfsAccountLevel1 IN ('Sales', 'COGS')
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'
  GROUP BY gmfsDepartmentLevel2, gmdept) a
INNER JOIN (
  SELECT gmfsDepartmentLevel2, gmdept, SUM(gttamt) AS sales
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
    AND gmfsDepartmentLevel2 = 'Mechanical'    
    AND gmfsAccountLevel1 = 'Sales'
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'
  GROUP BY gmfsDepartmentLevel2, gmdept) b ON a.gmfsDepartmentLevel2 = b.gmfsDepartmentLevel2 
    AND a.gmdept = b.gmdept


SELECT * FROM #wtf WHERE split <> 1
SELECT * FROM stgarkonaffpxrefdta WHERE fxconsol = '' AND fxcyy = 2012
SELECT * FROM stgarkonaffpxrefdta WHERE fxconsol = '' AND fxcyy = 2012 AND right(TRIM(fxfact), 1) in ('S','A') AND fxfp01 <> 1
SELECT * FROM stgarkonaffpxrefdta WHERE fxconsol = '' AND fxcyy = 2012 AND left(TRIM(fxfact), 3) in ('467','477', '478') AND fxfp01 <> 1
parts split
467/667 (146700/166700) - main shop
478/678 (147800/167800) - pdq
477/677 (147700/167700, 147701/167701) - body shop

SELECT gmacct, gmdept, gmdesc, gmfsDepartmentLevel2, SUM(gttamt)
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  AND b.gmacct IN ('146700','166700', '147700','167700', '147800', '167800', '147701','167701')
WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'
GROUP BY gmacct, gmdept, gmdesc, gmfsDepartmentLevel2

SELECT *
FROM #wtf
WHERE gmacct IN ('146700','166700', '147700','167700', '147800', '167800', '147701','167701')

SELECT *
FROM (    
  SELECT a.gmacct, a.gmfsDepartmentLevel2, a.gmfsAccountLevel1, a.gmdept, a.fxfact, b.gmacct, b.gmfsDepartmentLevel2, b.gmfsAccountLevel1, b.gmdept, b.fxfact
  FROM #wtf a 
  full OUTER JOIN #wtf b ON a.costAccount = b.gmacct) a
WHERE gmacct IN ('146700','147700','147800', '147701')  


SELECT gmacct, gmdept, gmdesc, gmfsDepartmentLevel2, SUM(gttamt)
FROM stgArkonaGLPTRNS a
INNER JOIN #wtf b ON a.gtacct = b.gmacct
  AND LEFT(b.gmacct, 4) IN ('1467', '1477','1478')
WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'
GROUP BY gmacct, gmdept, gmdesc, gmfsDepartmentLevel2


  SELECT fxfact, gmfsDepartmentLevel2, SUM(gttamt) AS sales
  FROM stgArkonaGLPTRNS a
  INNER JOIN #wtf b ON a.gtacct = b.gmacct
    AND gmfsDepartmentLevel1 = 'Fixed Operations'
    AND gmfsAccountLevel1 = 'Sales'
  WHERE a.gtdate BETWEEN '08/01/2012/' AND '08/31/2012'  
  GROUP BY fxfact, gmfsDepartmentLevel2
 
/* this IS ALL an attempt to match sales with cogs accounts, verify gmdept matches    
-- goddamnit why isn't this full OUTER JOIN returning sales accounts with no costaccout?
-- fuck, it's the goddamned scraped data IN glpmast, gmdboa IS never NULL, it equals ''
-- therefor the OUTER JOIN sees no nulls,     
SELECT a.gmacct, a.gmdept, a.fxfact, b.gmacct, b.gmdept, b.fxfact
FROM #wtf a 
full OUTER JOIN #wtf b ON a.costAccount = b.gmacct
WHERE a.gmfsDepartmentLevel2 = 'Mechanical'    
 AND a.gmfsAccountLevel1 = 'Sales'
 AND b.gmfsDepartmentLevel2 = 'Mechanical'    
 AND b.gmfsAccountLevel1 = 'COGS' 
 AND (a.gmacct IS NULL OR b.gmacct IS NULL)
 
-- but this does work 
SELECT *
FROM (    
  SELECT a.gmacct, a.gmfsDepartmentLevel2, a.gmfsAccountLevel1, a.gmdept, a.fxfact, b.gmacct, b.gmfsDepartmentLevel2, b.gmfsAccountLevel1, b.gmdept, b.fxfact
  FROM #wtf a 
  full OUTER JOIN #wtf b ON a.costAccount = b.gmacct) a
WHERE (gmfsDepartmentLevel2 = 'Mechanical' OR gmfsDepartmentLevel2_1 = 'Mechanical')   
  AND (gmfsAccountLevel1 = 'Sales' OR gmfsAccountLevel1_1 = 'COGS')
ORDER BY gmacct, gmacct_1

-- should be returning these 146027D, 145404, 146900 
SELECT *
FROM #wtf a
WHERE a.gmfsDepartmentLevel2 = 'Mechanical'    
 AND a.gmfsAccountLevel1 = 'Sales' 
 AND costaccount IS NULL 
-- AND these 165404, 165904, 166319, 166501, 166502, 166503, 166504, 166524, 166900
 SELECT *
 FROM #wtf a
 WHERE a.gmfsDepartmentLevel2 = 'Mechanical'    
   AND a.gmfsAccountLevel1 = 'COGS' 
   AND NOT EXISTS (
     SELECT 1
     FROM #wtf
     WHERE gmfsDepartmentLevel2 = 'Mechanical'
       AND gmfsAccountLevel1 = 'Sales'
       AND costAccount = a.gmacct)
 ORDER BY gmacct
*/

 
