alter PROCEDURE stgRoHeaderDetail ()
BEGIN 
/*  
getting ALL fucking fuzzy ON nightly
deal with lines with rows IN sdprtxt AND IN pdppdet      
1. IS there overlap
2. IF so, which takes precedence
so my current thinking IS that PDP takes precedence, IF there are changes, that 
IS WHERE they show up 
SDP IS done stuff that "only gets updated BY the migration of PDP"
which ALL feels pretty good
but it seems WHEN i did factro/roline, i SET the precedence AS SDP, ie, ADD FROM PDP
WHERE NOT already IN - fuck, IN what?  
yep, IN sp.stgRO
1. INSERT INTO xfmRO FROM SDP
2. INSERT INTO xfmRO FROM PDP WHERE NOT EXISTS co/ro/line IN xfmro

ok, the assumption IS that factro AND factroline are sufficiently complete
including everything needed FROM SDP & PDP
so ALL i am trying to DO IS to DO anightly UPDATE of Header & Detail, the base
rows of which come FROM factro/roline, but THEN need to be updated with
complaint,cause,correction AND pleasenote
 


SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = linedate) AS linedatekey,
  ServType, PayType, OpCode, coalesce(CorCode, 'N/A') AS CorCode, LineHours
FROM (
  SELECT ptco#, ptro#, ptline,
    max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
    MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
    MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
    SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours
  FROM xfmRO
  WHERE status <> 'VOID'
  GROUP BY ptco#, ptro#, ptline) a
WHERE (linedate IS NULL
  OR servtype IS NULL 
  OR paytype IS NULL
  OR opcode IS NULL)
AND ptline < 900

so why Does line 1 for 2681118, 2681127, 2682382 come up with NULL linedate-servtype-paytype-opcode???

SELECT * FROM xfmro WHERE ptro# IN ('2681118', '2681127', '2682382') AND ptline = 1
because none of them have a ptltyp = A

SELECT * FROM tmpSDPRDET WHERE ptro# IN ('2681118', '2681127', '2682382') AND ptline = 1
hmm, but they DO IN arkona
the culprit IS ptdbas :: discount indicator
ptdbas = V IS filtered out IN the generations of tmpSDPRDET


-- 7/3, ok ALL that shit IS done (fb CASE 279) time to fucking get ON with doing n
-- 1. nightly  2. hourly

ok, the reason i am fucking with xfmro instead of factro IS zROHeader/Detail IS NOT
the entire history, but just since 2013
so BY default xfmro offers a much smaller dataset to manipulate 
ok
header xfmro WHERE ro NOT IN header
detail xfmro WHERE ro in header AND co/ro/line NOT IN detail

EXECUTE PROCEDURE stgROHeaderDetail();
*/

DECLARE @string string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
-- 3. ZROHeader.pleasenote
DECLARE @cur3All cursor AS 
  SELECT distinct a.storecode, a.ro
  FROM zROHeader a
  INNER JOIN tmpSDPRTXTa b ON a.storecode = b.sxco# AND a.ro = b.sxro# AND sxltyp = 'X';
DECLARE @cur3RO cursor AS
  SELECT sxco#, sxro#, sxtext
  FROM tmpSDPRTXTa
  WHERE sxltyp = 'X'
    AND sxco# = @storecode
    AND sxro# = @ro
  ORDER BY sxseq#;
-- 5. zRODetail.Cause
DECLARE @cur5All cursor AS 
  SELECT DISTINCT a.storecode, a.ro, a.line
  FROM zRODetail a
  INNER JOIN tmpSDPRTXTa b ON a.storecode = b.sxco# AND a.ro = b.sxro# AND b.sxltyp = 'L';
DECLARE @cur5RO cursor AS
  SELECT sxco#, sxro#, sxline, sxtext
  FROM tmpSDPRTXTa
  WHERE sxltyp = 'L'
    AND sxco# = @storecode
    AND sxro# = @ro
    AND sxline = @line
  ORDER BY sxseq#;  
-- 6. zRODetail.complaint
DECLARE @cur6All cursor AS 
  SELECT DISTINCT a.storecode, a.ro, a.line
  FROM zRODetail a
  INNER JOIN tmpSDPRTXTa b ON a.storecode = b.sxco# AND a.ro = b.sxro# AND b.sxltyp = 'A';
DECLARE @cur6RO cursor AS
  SELECT sxco#, sxro#, sxline, sxtext
  FROM tmpSDPRTXTa
  WHERE sxltyp = 'A'
    AND sxco# = @storecode
    AND sxro# = @ro
    AND sxline = @line
  ORDER BY sxseq#;  
-- 7. zRODetail.correction
DECLARE @cur7All cursor AS 
  SELECT DISTINCT a.storecode, a.ro, a.line
  FROM zRODetail a
  INNER JOIN tmpSDPRTXTa b ON a.storecode = b.sxco# AND a.ro = b.sxro# AND b.sxltyp = 'Z';
DECLARE @cur7RO cursor AS
  SELECT sxco#, sxro#, sxline, sxtext
  FROM tmpSDPRTXTa
  WHERE sxltyp = 'Z'
    AND sxco# = @storecode
    AND sxro# = @ro
    AND sxline = @line
  ORDER BY sxseq#;  
  
TRY
  BEGIN TRANSACTION;
  TRY -- 4007.100  
--- 1.-------------------------------------------------------------------------
    INSERT INTO zROHeader
    SELECT a.ptco# AS StoreCode, a.ptro# AS RO, ptswid AS WriterNumber, 
      CASE a.status
          WHEN 'Closed' THEN 'Closed'
          WHEN '1' THEN 'Open'
          WHEN '2' THEN 'In-Process'
          WHEN '3' THEN 'Appr-PD'
          WHEN '4' THEN 'Cashier'
          WHEN '5' THEN 'Cashier/D'
          WHEN '6' THEN 'Pre-Inv'
          WHEN '7' THEN 'Odom Rq'
          WHEN '9' THEN 'Parts-A'
          WHEN 'L' THEN 'G/L Error'
          WHEN 'P' THEN 'PI Rq'
          ELSE 'WTF'
      END AS ROStatus,
      c.thedate AS OpenDate, left(c.DayName,3) + ' ' + c.mmdd AS OpenDateDayNameMMDD,
      d.thedate AS CloseDate, left(d.DayName,3) + ' ' + d.mmdd AS CloseDateNameMMDD,
      h.thedate AS FinalCloseDate, left(h.DayName,3) + ' ' + h.mmdd AS FinalCloseDateNameMMDD,
      a.ptcnam AS Customer,
      CASE 
        WHEN e.bnphon  = '0' THEN 'No Phone'
        WHEN e.bnphon  = '' THEN 'No Phone'
        WHEN e.bnphon IS NULL THEN 'No Phone'
        ELSE left(e.bnphon, 12)
      END AS HomePhone, 
      CASE 
        WHEN e.bnbphn  = '0' THEN 'No Phone'
        WHEN e.bnbphn  = '' THEN 'No Phone'
        WHEN e.bnbphn IS NULL THEN 'No Phone'
        ELSE left(e.bnbphn, 12)
      END  AS WorkPhone, 
      CASE 
        WHEN e.bncphon  = '0' THEN 'No Phone'
        WHEN e.bncphon  = '' THEN 'No Phone'
        WHEN e.bncphon IS NULL THEN 'No Phone'
        ELSE left(e.bncphon, 12)
      END  AS CellPhone,
      coalesce(case when e.bnemail = '' THEN 'None' ELSE e.bnemail END, 'None') as CustomerEMail,
      trim(TRIM(f.immake) + ' ' + TRIM(f.immodl)) AS vehicle, '' AS PleaseNote  
    --DROP TABLE #header  
    --INTO #header      
    FROM (
      SELECT ptco#, ptro#, ptcnam, ptckey, ptvin, opendate, closedate, 
        finalclosedate, status, ptswid
      FROM xfmro x
      WHERE status <> 'VOID'
        AND NOT EXISTS ( 
          SELECT 1
          FROM zroheader
          WHERE storecode = x.ptco# AND ro = x.ptro#)
        AND NOT EXISTS ( -- returns just the SDET row IF row EXISTS IN SDET & PDET
          SELECT 1       -- for now just pick one, straighten it out IN the update
          FROM xfmro
          WHERE ptro# = x.ptro#
            AND source = 'SDET'
            AND x.source = 'PDET')      
      GROUP BY ptco#, ptro#, ptcnam, ptckey, ptvin, opendate, closedate, 
        finalclosedate, status, ptswid) a
    LEFT JOIN day c ON a.opendate = c.thedate
    LEFT JOIN day d ON a.closedate = d.thedate
    LEFT JOIN day h ON a.finalclosedate = h.thedate 
    LEFT JOIN (
        select bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail
        from stgArkonaBOPNAME
        group by bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail) e ON a.ptcnam = e.bnsnam
      AND a.ptckey = e.bnkey    
    LEFT JOIN stgArkonaINPMAST f ON a.ptvin = f.imvin;  
--/ 1.-------------------------------------------------------------------------
--/
--- 1a.------------------------------------------------------------------------
-- UPDATE zROHeader.ROStatus & CloseDate & FinalCloseDate
/*
status needs to come FROM the PDP row
closedate needs to come FROM the SDP row      

SELECT ptco#, ptro#,
  MAX(CASE WHEN source = 'SDET' THEN status END) AS sdetStatus,
  MAX(CASE WHEN source = 'PDET' THEN status END) AS pdetStatus,
  MAX(CASE WHEN source = 'SDET' THEN closedate END) AS sdetCloseDate,
  MAX(CASE WHEN source = 'PDET' THEN closedate END) AS pdetClosedate
FROM xfmro
WHERE ptro# IN ('16123982','16123984','16124024','19139376','18022852') 
GROUP BY ptco#, ptro#  
*/
--SELECT * FROM zROHeader WHERE ro = '16123508'
--SELECT * FROM xfmro WHERE ptro# = '16123508'
    UPDATE zROHeader
    SET ROStatus = x.status
    FROM (
      SELECT ptco#, ptro#, 
      MAX (
        CASE a.status
          WHEN 'Closed' THEN 'Closed'
          WHEN '1' THEN 'Open'
          WHEN '2' THEN 'In-Process'
          WHEN '3' THEN 'Appr-PD'
          WHEN '4' THEN 'Cashier'
          WHEN '5' THEN 'Cashier/D'
          WHEN '6' THEN 'Pre-Inv'
          WHEN '7' THEN 'Odom Rq'
          WHEN '9' THEN 'Parts-A'
          WHEN 'L' THEN 'G/L Error'
          WHEN 'P' THEN 'PI Rq'
          ELSE 'WTF'
        END) AS Status  
      FROM xfmRO a
      WHERE status <> 'VOID'
        AND NOT EXISTS ( -- returns just the PDET row IF row EXISTS IN SDET & PDET
          SELECT 1
          FROM xfmro
          WHERE ptro# = a.ptro#
            AND source = 'PDET'
            AND a.source = 'SDET') 
       GROUP BY ptco#, ptro# ) x
    WHERE zROHeader.storecode = x.ptco#
      AND zROHeader.ro = x.ptro#;  
  
    UPDATE zROHeader
    SET CloseDate = x.CloseDate,
        CloseDateDayNameMMDD = x.DayNameMMDD
    FROM (
      SELECT ptco#, ptro#, closedate, MAX(LEFT(b.DayName,3) + ' ' + b.mmdd) AS DayNameMMDD
      FROM xfmRO a
      LEFT JOIN day b ON a.closedate = thedate
      WHERE status <> 'VOID'
        AND NOT EXISTS ( -- returns just the SDET row IF row EXISTS IN SDET & PDET
          SELECT 1
          FROM xfmro
          WHERE ptro# = a.ptro#
            AND source = 'SDET'
            AND a.source = 'PDET') 
       GROUP BY ptco#, ptro#, closedate) x
    WHERE zROHeader.storecode = x.ptco#
      AND zROHeader.ro = x.ptro#;    
  
    UPDATE zROHeader 
    SET FinalCloseDate = x.FinalCloseDate,
        FinalCloseDateDayNameMMDD = x.DayNameMMDD
    FROM (
      SELECT ptco#, ptro#, finalclosedate, MAX(LEFT(b.DayName,3) + ' ' + b.mmdd) AS DayNameMMDD
      FROM xfmRO a
      LEFT JOIN day b ON a.finalclosedate = b.thedate
      WHERE status <> 'VOID'
      GROUP BY ptco#, ptro#, finalclosedate) x
    WHERE zROHeader.storecode = x.ptco#
      AND zROHeader.ro = x.ptro#;
     
--/ 1a.------------------------------------------------------------------------
--- 2.-------------------------------------------------------------------------
    INSERT INTO zRODetail
    SELECT ptco# AS StoreCode, ptro# AS RO, ptline AS Line,
      '' AS LineStatus,
      PayType, OpCode, trim(trim(b.sodes1) + ' ' + TRIM(b.sodes2)) AS OpCodeDescription, '' AS Complaint,
      coalesce(CorCode, 'N/A') AS CorCode, 
      trim(trim(c.sodes1) + ' ' + TRIM(c.sodes2)) AS CorCodeDescription, '' AS Correction,
      '' AS Cause
    --DROP TABLE #detail  
    --INTO #detail  
    FROM (
      SELECT ptco#, ptro#, ptline,
        MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
        MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
        MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode
      FROM xfmRO x
      WHERE status <> 'VOID'
        AND NOT EXISTS (
          SELECT 1
          FROM zRODetail
          WHERE storecode = x.ptco#
            AND ro = x.ptro#
            AND line = x.ptline)
      GROUP BY ptco#, ptro#, ptline) a
    LEFT JOIN stgArkonaSDPLOPC b ON a.ptco# = b.soco# AND a.opcode = b.solopc
    LEFT JOIN stgArkonaSDPLOPC c ON a.ptco# = c.soco# AND a.corcode = c.solopc
    WHERE a.paytype IS NOT NULL
      AND a.opcode IS NOT NULL;  

--/ 2.-------------------------------------------------------------------------
-- updates
/*
DROP TABLE tmpSDPRTXTa;
CREATE TABLE tmpSDPRTXTa ( 
      [sxco#] CIChar( 3 ),
      [sxro#] CIChar( 9 ),
      sxline Integer,
      sxltyp CIChar( 1 ),
      [sxseq#] Integer,
      sxtext CIChar( 50 )) IN DATABASE;         
*/      
    DELETE FROM tmpSDPRTXTa;
    INSERT INTO tmpSDPRTXTa
    SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
    FROM tmpsdprtxt  a
    UNION 
    SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
    FROM tmppdpphdr a
    INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
    WHERE a.ptdtyp = 'RO'
      AND a.ptdoc# <> ''
      AND b.ptcmnt <> '';  
--- 3.-------------------------------------------------------------------------
    @string = '';  
    OPEN @cur3All;
    TRY
      WHILE FETCH @cur3All DO
        @storecode = @cur3ALL.storecode;
        @ro = @cur3All.ro;
        OPEN @cur3RO;
        TRY
          WHILE FETCH @cur3RO DO
            @string = TRIM(@string) + ' ' + TRIM(@cur3RO.sxtext);
          END WHILE;
        FINALLY
          CLOSE @cur3RO;
        END TRY;
        UPDATE zROHEader
        SET PleaseNote = @string
        WHERE storecode = @storecode
          AND ro = @ro;
        @string = '';
      END WHILE;
    FINALLY
      CLOSE @cur3All;
    END TRY;   
--/ 3.-------------------------------------------------------------------------
--- 4.-------------------------------------------------------------------------
-- 4. zRODetail.LineStatus
    UPDATE zRODetail
    SET LineStatus = 
      CASE f.ptlsts
        WHEN 'C' THEN 'Closed'
        WHEN 'I' THEN 'Open'
      END 
    FROM (
      SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptlsts
      FROM stgArkonaPDPPHDR a
      LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
      WHERE b.ptltyp = 'A'
        AND b.ptpkey IS NOT NULL 
        AND b.ptseq# = 0) f
    WHERE zRODetail.storecode = f.ptco#
      AND zRODetail.ro = f.ptdoc#
      AND zRODetail.line = f.ptline
      AND EXISTS (
        SELECT 1
        FROM xfmRO
        WHERE ro = zRODetail.ro); 
--/ 4.-------------------------------------------------------------------------
--- 5.-------------------------------------------------------------------------
    @string = '';  
    OPEN @cur5All;
    TRY
      WHILE FETCH @cur5All DO
        @storecode = @cur5ALL.storecode;
        @ro = @cur5All.ro;
        @line = @cur5All.line;
        OPEN @cur5RO;
        TRY
          WHILE FETCH @cur5RO DO
            @string = TRIM(@string) + ' ' + TRIM(@cur5RO.sxtext);
          END WHILE;
        FINALLY
          CLOSE @cur5RO;
        END TRY;
        UPDATE zRODetail
        SET Cause = @string
        WHERE storecode = @storecode
          AND ro = @ro
          AND line = @line;
        @string = '';
      END WHILE;
    FINALLY
      CLOSE @cur5All;
    END TRY;  
--/ 5.-------------------------------------------------------------------------
--- 6.-------------------------------------------------------------------------
    @string = '';  
    OPEN @cur6All;
    TRY
      WHILE FETCH @cur6All DO
        @storecode = @cur6ALL.storecode;
        @ro = @cur6All.ro;
        @line = @cur6All.line;
        OPEN @cur6RO;
        TRY
          WHILE FETCH @cur6RO DO
            @string = TRIM(@string) + ' ' + TRIM(@cur6RO.sxtext);
          END WHILE;
        FINALLY
          CLOSE @cur6RO;
        END TRY;
        UPDATE zRODetail
        SET Complaint = @string
        WHERE storecode = @storecode
          AND ro = @ro
          AND line = @line;
        @string = '';
      END WHILE;
    FINALLY
      CLOSE @cur6All;
    END TRY; 
--/ 6.-------------------------------------------------------------------------
--- 7.-------------------------------------------------------------------------
    @string = '';  
    OPEN @cur7All;
    TRY
      WHILE FETCH @cur7All DO
        @storecode = @cur7ALL.storecode;
        @ro = @cur7All.ro;
        @line = @cur7All.line;
        OPEN @cur7RO;
        TRY
          WHILE FETCH @cur7RO DO
            @string = TRIM(@string) + ' ' + TRIM(@cur7RO.sxtext);
          END WHILE;
        FINALLY
          CLOSE @cur7RO;
        END TRY;
        UPDATE zRODetail
        SET Correction = @string
        WHERE storecode = @storecode
          AND ro = @ro
          AND line = @line;
        @string = '';
      END WHILE;
    FINALLY
      CLOSE @cur7All;
    END TRY; 
--/ 7.-------------------------------------------------------------------------
  COMMIT WORK;
  CATCH ALL
    ROLLBACK WORK;
    RAISE;
  END TRY;   
CATCH ALL
  RAISE;
END TRY;    

/*
there are ros for which there are multiple rows for the same ro/line/type/seq
looks like the last one happened in 10/2012, guessing the are artifacts from all
the problems with ro lines doubling
thinking it is a test i do against daily download of sdprtxt, generate an email if it comes up

select sxco#,sxro#,sxline,sxltyp, sxseq# 
from (
  Select sxco#,sxro#,sxline,sxltyp,sxseq#,sxtext
  from tmpSDPRTXT
  group by  sxco#,sxro#,sxline,sxltyp,sxseq#,sxtext) x 
group by sxco#,sxro#,sxline,sxltyp, sxseq# 
having count(*) > 1
*/
END;