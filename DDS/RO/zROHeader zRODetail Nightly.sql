SELECT a.storecode, a.ro, writerid, 
  CASE 
    WHEN b.ptrsts IS NOT NULL THEN 
      CASE b.ptrsts
        WHEN '1' THEN 'Open'
        WHEN '2' THEN 'In-Process'
        WHEN '3' THEN 'Appr-PD'
        WHEN '4' THEN 'Cashier'
        WHEN '5' THEN 'Cashier/D'
        WHEN '6' THEN 'Pre-Inv'
        WHEN '7' THEN 'Odom Rq'
        WHEN '9' THEN 'Parts-A'
        WHEN 'L' THEN 'G/L Error'
        WHEN 'P' THEN 'PI Rq'
        ELSE 'WTF'
      END
    ELSE 'Closed'
  END AS ROStatus,
  c.thedate, left(c.DayName,3) + ' ' + c.mmdd AS OpenDate,
  d.thedate, left(d.DayName,3) + ' ' + d.mmdd AS CloseDate,
  h.thedate, left(h.DayName,3) + ' ' + h.mmdd AS FinalCloseDate,
  a.customername,
  CASE 
    WHEN e.bnphon  = '0' THEN 'No Phone'
    WHEN e.bnphon  = '' THEN 'No Phone'
    WHEN e.bnphon IS NULL THEN 'No Phone'
    ELSE left(e.bnphon, 12)
  END AS HomePhone, 
  CASE 
    WHEN e.bnbphn  = '0' THEN 'No Phone'
    WHEN e.bnbphn  = '' THEN 'No Phone'
    WHEN e.bnbphn IS NULL THEN 'No Phone'
    ELSE left(e.bnbphn, 12)
  END  AS WorkPhone, 
  CASE 
    WHEN e.bncphon  = '0' THEN 'No Phone'
    WHEN e.bncphon  = '' THEN 'No Phone'
    WHEN e.bncphon IS NULL THEN 'No Phone'
    ELSE left(e.bncphon, 12)
  END  AS CellPhone,
  coalesce(case when e.bnemail = '' THEN 'None' ELSE e.bnemail END, 'None') as CustomerEMail,
  trim(TRIM(f.immake) + ' ' + TRIM(f.immodl)) AS vehicle   
FROM factro a
LEFT JOIN stgArkonaPDPPHDR b ON a.ro = b.ptdoc#
LEFT JOIN day c ON a.opendatekey = c.datekey
LEFT JOIN day d ON a.closedatekey = d.datekey
LEFT JOIN day h ON a.finalclosedatekey = h.datekey
LEFT JOIN (
    select bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail
    from stgArkonaBOPNAME
    group by bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail) e ON a.customername = e.bnsnam
  AND a.customerkey = e.bnkey    
LEFT JOIN stgArkonaINPMAST f ON a.vin = f.imvin 
WHERE a.void = false
  AND c.thedate > '01/01/2013'
  AND NOT EXISTS (
    SELECT 1
    FROM zroheader
    WHERE ro = a.ro);
    
    
SELECT b.storecode, b.ro, b.line, 
  CASE 
    WHEN h.ptlsts IS NULL THEN 'Closed'
    WHEN h.ptlsts = 'I' THEN 'Open'
    WHEN h.ptlsts = 'C' THEN 'Closed'
  END, b.paytype, 
  b.opcode, trim(trim(d.sodes1) + ' ' + TRIM(d.sodes2)) AS OpCodeDesc,
  b.corcode, trim(trim(e.sodes1) + ' ' + TRIM(e.sodes2)) AS CorCodeDesc
FROM factro a
INNER JOIN factroline b ON a.storecode = b.storecode AND a.ro = b.ro  
INNER JOIN day c ON a.opendatekey = c.datekey
LEFT JOIN stgArkonaSDPLOPC d ON b.storecode = d.soco# AND b.opcode = d.solopc
LEFT JOIN stgArkonaSDPLOPC e ON b.storecode = e.soco# AND b.corcode = e.solopc
LEFT JOIN (
  SELECT f.ptco#, f.ptdoc#, g.ptline, g.ptlsts
  FROM stgArkonaPDPPHDR f
  INNER JOIN stgArkonaPDPPDET g ON f.ptpkey = g.ptpkey
    AND g.ptltyp = 'A'
    AND g.ptseq# = 0) h ON b.storecode = h.ptco# AND b.ro = h.ptdoc# AND b.line = h.ptline
WHERE a.void = false
  AND c.thedate > '01/01/2013'   
  AND NOT EXISTS (
    SELECT 1
    FROM zRODetail
    WHERE storecode = b.storecode
      AND ro = b.ro
      AND line = b.line)

getting ALL fucking fuzzy ON nightly
deal with lines with rows IN sdprtxt AND IN pdppdet      
1. IS there overlap
2. IF so, which takes precedence
so my current thinking IS that PDP takes precedence, IF there are changes, that 
IS WHERE they show up 
SDP IS done stuff that "only gets updated BY the migration of PDP"
which ALL feels pretty good
but it seems WHEN i did factro/roline, i SET the precedence AS SDP, ie, ADD FROM PDP
WHERE NOT already IN - fuck, IN what?  
yep, IN sp.stgRO
1. INSERT INTO xfmRO FROM SDP
2. INSERT INTO xfmRO FROM PDP WHERE NOT EXISTS co/ro/line IN xfmro

SELECT a.ptdoc#, b.ptline
INTO #pdp
FROM tmpPDPPHDR a
INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
WHERE a.ptdtyp = 'RO'
  AND a.ptdoc# <> ''
GROUP BY a.ptdoc#, b.ptline; 

SELECT a.ptro#, b.ptline
INTO #sdp
FROM tmpSDPRHDR a
LEFT JOIN tmpSDPRDET b ON a.ptco# = b.ptco# 
  AND a.ptro# = b.ptro#
  AND b.ptlhrs >= 0
GROUP BY a.ptro#, b.ptline;  

beaucoup overlap
SELECT *
FROM #pdp a
INNER JOIN #sdp b ON a.ptdoc# = b.ptro# AND a.ptline = b.ptline

now WHERE are the differences
-- both lines IN sdp, one line IN pdp, WHEN the
SELECT * FROM #sdp WHERE ptro# = '16117261'
SELECT * FROM #pdp WHERE ptdoc# = '16117261'

SELECT a.ptdoc#, b.ptline
FROM tmpPDPPHDR a
INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
WHERE a.ptdoc# = '16117261'

ok, the assumption IS that factro AND factroline are sufficiently complete
including everything needed FROM SDP & PDP
so ALL i am trying to DO IS to DO anightly UPDATE of Header & Detail, the base
rows of which come FROM factro/roline, but THEN need to be updated with
complaint,cause,correction AND pleasenote

SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
FROM tmpsdprtxt

SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
FROM tmppdpphdr a
INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
WHERE a.ptdtyp = 'RO'
  AND a.ptdoc# <> ''
  AND b.ptcmnt <> ''
  
-- WHERE they match, the comments/txt are the same 
SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext, b.*
FROM tmpsdprtxt a
INNER JOIN (
  SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
  FROM tmppdpphdr a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND b.ptcmnt <> '') b ON sxco# = ptco# AND sxro# = ptdoc# AND sxline = ptline
      AND sxltyp = ptltyp AND sxseq# = ptseq#
AND sxtext <> ptcmnt    


SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
FROM tmpsdprtxt  a
UNION 
  SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
  FROM tmppdpphdr a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND b.ptcmnt <> ''

SELECT sxco#, sxro#, sxline, sxltyp, sxseq#
FROM (    
  SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
  FROM tmpsdprtxt  a
  UNION 
    SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
    FROM tmppdpphdr a
    INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
    WHERE a.ptdtyp = 'RO'
      AND a.ptdoc# <> ''
      AND b.ptcmnt <> '') c    
GROUP BY sxco#, sxro#, sxline, sxltyp, sxseq# HAVING COUNT(*) > 1  

-- zROHeader based ON xfmro WHERE ro NOT IN zROHeader
DROP TABLE #header;
SELECT a.ptco# AS StoreCode, a.ptro# AS RO, ptswid AS WriterNumber, 
  CASE a.status
      WHEN 'Closed' THEN 'Closed'
      WHEN '1' THEN 'Open'
      WHEN '2' THEN 'In-Process'
      WHEN '3' THEN 'Appr-PD'
      WHEN '4' THEN 'Cashier'
      WHEN '5' THEN 'Cashier/D'
      WHEN '6' THEN 'Pre-Inv'
      WHEN '7' THEN 'Odom Rq'
      WHEN '9' THEN 'Parts-A'
      WHEN 'L' THEN 'G/L Error'
      WHEN 'P' THEN 'PI Rq'
      ELSE 'WTF'
  END AS ROStatus,
  c.thedate AS OpenDate, left(c.DayName,3) + ' ' + c.mmdd AS OpenDateDayNameMMDD,
  d.thedate AS CloseDate, left(d.DayName,3) + ' ' + d.mmdd AS CloseDateNameMMDD,
  h.thedate AS FinalCloseDate, left(h.DayName,3) + ' ' + h.mmdd AS FinalCloseDateNameMMDD,
  a.ptcnam AS Customer,
  CASE 
    WHEN e.bnphon  = '0' THEN 'No Phone'
    WHEN e.bnphon  = '' THEN 'No Phone'
    WHEN e.bnphon IS NULL THEN 'No Phone'
    ELSE left(e.bnphon, 12)
  END AS HomePhone, 
  CASE 
    WHEN e.bnbphn  = '0' THEN 'No Phone'
    WHEN e.bnbphn  = '' THEN 'No Phone'
    WHEN e.bnbphn IS NULL THEN 'No Phone'
    ELSE left(e.bnbphn, 12)
  END  AS WorkPhone, 
  CASE 
    WHEN e.bncphon  = '0' THEN 'No Phone'
    WHEN e.bncphon  = '' THEN 'No Phone'
    WHEN e.bncphon IS NULL THEN 'No Phone'
    ELSE left(e.bncphon, 12)
  END  AS CellPhone,
  coalesce(case when e.bnemail = '' THEN 'None' ELSE e.bnemail END, 'None') as CustomerEMail,
  trim(TRIM(f.immake) + ' ' + TRIM(f.immodl)) AS vehicle, '' AS PleaseNote   
INTO #header      
FROM (
  SELECT ptco#, ptro#, ptcnam, ptckey, ptvin, opendate, closedate, 
    finalclosedate, status, ptswid
  FROM xfmro x
  WHERE status <> 'VOID'
    AND NOT EXISTS ( -- premature optimisation
      SELECT 1
      FROM zroheader
      WHERE ro = x.ptro#)
  GROUP BY ptco#, ptro#, ptcnam, ptckey, ptvin, opendate, closedate, 
    finalclosedate, status, ptswid) a
LEFT JOIN day c ON a.opendate = c.thedate
LEFT JOIN day d ON a.closedate = d.thedate
LEFT JOIN day h ON a.finalclosedate = h.thedate 
LEFT JOIN (
    select bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail
    from stgArkonaBOPNAME
    group by bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail) e ON a.ptcnam = e.bnsnam
  AND a.ptckey = e.bnkey    
LEFT JOIN stgArkonaINPMAST f ON a.ptvin = f.imvin  
 

/*  
SELECT ptco#, ptro#, ptline,
  (SELECT datekey FROM day WHERE thedate = linedate) AS linedatekey,
  ServType, PayType, OpCode, coalesce(CorCode, 'N/A') AS CorCode, LineHours
FROM (
  SELECT ptco#, ptro#, ptline,
    max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
    MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
    MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
    SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours
  FROM xfmRO
  WHERE status <> 'VOID'
  GROUP BY ptco#, ptro#, ptline) a
WHERE (linedate IS NULL
  OR servtype IS NULL 
  OR paytype IS NULL
  OR opcode IS NULL)
AND ptline < 900

so why Does line 1 for 2681118, 2681127, 2682382 come up with NULL linedate-servtype-paytype-opcode???

SELECT * FROM xfmro WHERE ptro# IN ('2681118', '2681127', '2682382') AND ptline = 1
because none of them have a ptltyp = A

SELECT * FROM tmpSDPRDET WHERE ptro# IN ('2681118', '2681127', '2682382') AND ptline = 1
hmm, but they DO IN arkona
the culprit IS ptdbas :: discount indicator
ptdbas = V IS filtered out IN the generations of tmpSDPRDET
*/

-- 7/3, ok ALL that shit IS done (fb CASE 279) time to fucking get ON with doing n
-- 1. nightly  2. hourly

ok, the reason i am fucking with xfmro instead of factro IS zROHeader/Detail IS NOT
the entire history, but just since 2013
so BY default xfmro offers a much smaller dataset to manipulate 
ok
header xfmro WHERE ro NOT IN header
detail xfmro WHERE ro in header AND co/ro/line NOT IN detail



SELECT ptco# AS StoreCode, ptro# AS RO, ptline AS Line,
  '' AS LineStatus,
  PayType, OpCode, trim(trim(b.sodes1) + ' ' + TRIM(b.sodes2)) AS OpCodeDescription, '' AS Complaint,
  coalesce(CorCode, 'N/A') AS CorCode, 
  trim(trim(c.sodes1) + ' ' + TRIM(c.sodes2)) AS CorCodeDescription, '' AS Correction,
  '' AS Cause
INTO #detail  
FROM (
  SELECT ptco#, ptro#, ptline,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
    MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode
  FROM xfmRO
  WHERE status <> 'VOID'
  GROUP BY ptco#, ptro#, ptline) a
LEFT JOIN stgArkonaSDPLOPC b ON a.ptco# = b.soco# AND a.opcode = b.solopc
LEFT JOIN stgArkonaSDPLOPC c ON a.ptco# = c.soco# AND a.corcode = c.solopc
WHERE a.paytype IS NOT NULL
  AND a.opcode IS NOT NULL;  

-- ok  
SELECT *
FROM #detail a
WHERE NOT EXISTS (
  SELECT 1
  FROM #header
  WHERE ro = a.ro)
  
SELECT COUNT(*) FROM #header  
SELECT COUNT(*) FROM #detail

ok, got a fresh cut of 14 days of sdprtxt AND tmpPDP

--SELECT COUNT(*) FROM (, shit, it IS only 25000
SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
FROM tmpsdprtxt  a
UNION 
  SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
  FROM tmppdpphdr a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND b.ptcmnt <> ''
--) x  
-- WHERE they match, the comments/txt are the same 
SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext, b.*
FROM tmpsdprtxt a
INNER JOIN (
  SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
  FROM tmppdpphdr a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND b.ptcmnt <> '') b ON sxco# = ptco# AND sxro# = ptdoc# AND sxline = ptline
      AND sxltyp = ptltyp AND sxseq# = ptseq#
AND sxtext <> ptcmnt    

SELECT COUNT(*)
FROM #header a
WHERE NOT EXISTS (
  SELECT 1
  FROM zroheader
  WHERE ro = a.ro)
  
SELECT COUNT(*)
FROM #detail a
WHERE NOT EXISTS (
  SELECT 1
  FROM zrodetail
  WHERE storecode = a.storecode
    AND ro = a.ro
    AND line = a.line)

SELECT *
FROM (
SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
FROM tmpsdprtxt  a
UNION 
  SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
  FROM tmppdpphdr a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND b.ptcmnt <> '')m
WHERE sxltyp = 'A'
AND exists    

-- 7/4
SELECT *
FROM (
SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
FROM tmpsdprtxt  a
UNION 
  SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
  FROM tmppdpphdr a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND b.ptcmnt <> '') a
WHERE sxltyp = 'X'  

    
SELECT COUNT(*) FROM #header a WHERE NOT EXISTS (SELECT 1 FROM zroheader WHERE ro = a.ro)  
FROM xfrmRO

--- 1.-------------------------------------------------------------------------
INSERT INTO zROHeader
SELECT a.ptco# AS StoreCode, a.ptro# AS RO, ptswid AS WriterNumber, 
  CASE a.status
      WHEN 'Closed' THEN 'Closed'
      WHEN '1' THEN 'Open'
      WHEN '2' THEN 'In-Process'
      WHEN '3' THEN 'Appr-PD'
      WHEN '4' THEN 'Cashier'
      WHEN '5' THEN 'Cashier/D'
      WHEN '6' THEN 'Pre-Inv'
      WHEN '7' THEN 'Odom Rq'
      WHEN '9' THEN 'Parts-A'
      WHEN 'L' THEN 'G/L Error'
      WHEN 'P' THEN 'PI Rq'
      ELSE 'WTF'
  END AS ROStatus,
  c.thedate AS OpenDate, left(c.DayName,3) + ' ' + c.mmdd AS OpenDateDayNameMMDD,
  d.thedate AS CloseDate, left(d.DayName,3) + ' ' + d.mmdd AS CloseDateNameMMDD,
  h.thedate AS FinalCloseDate, left(h.DayName,3) + ' ' + h.mmdd AS FinalCloseDateNameMMDD,
  a.ptcnam AS Customer,
  CASE 
    WHEN e.bnphon  = '0' THEN 'No Phone'
    WHEN e.bnphon  = '' THEN 'No Phone'
    WHEN e.bnphon IS NULL THEN 'No Phone'
    ELSE left(e.bnphon, 12)
  END AS HomePhone, 
  CASE 
    WHEN e.bnbphn  = '0' THEN 'No Phone'
    WHEN e.bnbphn  = '' THEN 'No Phone'
    WHEN e.bnbphn IS NULL THEN 'No Phone'
    ELSE left(e.bnbphn, 12)
  END  AS WorkPhone, 
  CASE 
    WHEN e.bncphon  = '0' THEN 'No Phone'
    WHEN e.bncphon  = '' THEN 'No Phone'
    WHEN e.bncphon IS NULL THEN 'No Phone'
    ELSE left(e.bncphon, 12)
  END  AS CellPhone,
  coalesce(case when e.bnemail = '' THEN 'None' ELSE e.bnemail END, 'None') as CustomerEMail,
  trim(TRIM(f.immake) + ' ' + TRIM(f.immodl)) AS vehicle, '' AS PleaseNote  
--DROP TABLE #header  
--INTO #header      
FROM (
  SELECT ptco#, ptro#, ptcnam, ptckey, ptvin, opendate, closedate, 
    finalclosedate, status, ptswid
  FROM xfmro x
  WHERE status <> 'VOID'
    AND NOT EXISTS ( 
      SELECT 1
      FROM zroheader
      WHERE storecode = x.ptco# AND ro = x.ptro#)
    AND NOT EXISTS ( -- returns just the SDET row IF row EXISTS IN SDET & PDET
      SELECT 1       -- for now just pick one, straighten it out IN the update
      FROM xfmro
      WHERE ptro# = x.ptro#
        AND source = 'SDET'
        AND x.source = 'PDET')      
  GROUP BY ptco#, ptro#, ptcnam, ptckey, ptvin, opendate, closedate, 
    finalclosedate, status, ptswid) a
LEFT JOIN day c ON a.opendate = c.thedate
LEFT JOIN day d ON a.closedate = d.thedate
LEFT JOIN day h ON a.finalclosedate = h.thedate 
LEFT JOIN (
    select bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail
    from stgArkonaBOPNAME
    group by bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail) e ON a.ptcnam = e.bnsnam
  AND a.ptckey = e.bnkey    
LEFT JOIN stgArkonaINPMAST f ON a.ptvin = f.imvin;  
--/ 1.-------------------------------------------------------------------------
--/
--- 1a.------------------------------------------------------------------------
-- UPDATE zROHeader.ROStatus & CloseDate & FinalCloseDate
/*
status needs to come FROM the PDP row
closedate needs to come FROM the SDP row      

SELECT ptco#, ptro#,
  MAX(CASE WHEN source = 'SDET' THEN status END) AS sdetStatus,
  MAX(CASE WHEN source = 'PDET' THEN status END) AS pdetStatus,
  MAX(CASE WHEN source = 'SDET' THEN closedate END) AS sdetCloseDate,
  MAX(CASE WHEN source = 'PDET' THEN closedate END) AS pdetClosedate
FROM xfmro
WHERE ptro# IN ('16123982','16123984','16124024','19139376','18022852') 
GROUP BY ptco#, ptro#  
*/
--SELECT * FROM zROHeader WHERE ro = '16123508'
--SELECT * FROM xfmro WHERE ptro# = '16123508'
UPDATE zROHeader
SET ROStatus = x.status
FROM (
  SELECT ptco#, ptro#, 
  MAX (
    CASE a.status
      WHEN 'Closed' THEN 'Closed'
      WHEN '1' THEN 'Open'
      WHEN '2' THEN 'In-Process'
      WHEN '3' THEN 'Appr-PD'
      WHEN '4' THEN 'Cashier'
      WHEN '5' THEN 'Cashier/D'
      WHEN '6' THEN 'Pre-Inv'
      WHEN '7' THEN 'Odom Rq'
      WHEN '9' THEN 'Parts-A'
      WHEN 'L' THEN 'G/L Error'
      WHEN 'P' THEN 'PI Rq'
      ELSE 'WTF'
    END) AS Status  
  FROM xfmRO a
  WHERE status <> 'VOID'
    AND NOT EXISTS ( -- returns just the PDET row IF row EXISTS IN SDET & PDET
      SELECT 1
      FROM xfmro
      WHERE ptro# = a.ptro#
        AND source = 'PDET'
        AND a.source = 'SDET') 
   GROUP BY ptco#, ptro# ) x
WHERE zROHeader.storecode = x.ptco#
  AND zROHeader.ro = x.ptro#;  
  
UPDATE zROHeader
SET CloseDate = x.CloseDate,
    CloseDateDayNameMMDD = x.DayNameMMDD
FROM (
  SELECT ptco#, ptro#, closedate, MAX(LEFT(b.DayName,3) + ' ' + b.mmdd) AS DayNameMMDD
  FROM xfmRO a
  LEFT JOIN day b ON a.closedate = thedate
  WHERE status <> 'VOID'
    AND NOT EXISTS ( -- returns just the SDET row IF row EXISTS IN SDET & PDET
      SELECT 1
      FROM xfmro
      WHERE ptro# = a.ptro#
        AND source = 'SDET'
        AND a.source = 'PDET') 
   GROUP BY ptco#, ptro#, closedate) x
WHERE zROHeader.storecode = x.ptco#
  AND zROHeader.ro = x.ptro#;    
  
UPDATE zROHeader 
SET FinalCloseDate = x.FinalCloseDate,
    FinalCloseDateDayNameMMDD = x.DayNameMMDD
FROM (
  SELECT ptco#, ptro#, finalclosedate, MAX(LEFT(b.DayName,3) + ' ' + b.mmdd) AS DayNameMMDD
  FROM xfmRO a
  LEFT JOIN day b ON a.finalclosedate = b.thedate
  WHERE status <> 'VOID'
  GROUP BY ptco#, ptro#, finalclosedate) x
WHERE zROHeader.storecode = x.ptco#
  AND zROHeader.ro = x.ptro#;
     
--/ 1a.------------------------------------------------------------------------
--- 2.-------------------------------------------------------------------------
INSERT INTO zRODetail
SELECT ptco# AS StoreCode, ptro# AS RO, ptline AS Line,
  '' AS LineStatus,
  PayType, OpCode, trim(trim(b.sodes1) + ' ' + TRIM(b.sodes2)) AS OpCodeDescription, '' AS Complaint,
  coalesce(CorCode, 'N/A') AS CorCode, 
  trim(trim(c.sodes1) + ' ' + TRIM(c.sodes2)) AS CorCodeDescription, '' AS Correction,
  '' AS Cause
--DROP TABLE #detail  
--INTO #detail  
FROM (
  SELECT ptco#, ptro#, ptline,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
    MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
    MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode
  FROM xfmRO x
  WHERE status <> 'VOID'
    AND NOT EXISTS (
      SELECT 1
      FROM zRODetail
      WHERE storecode = x.ptco#
        AND ro = x.ptro#
        AND line = x.ptline)
  GROUP BY ptco#, ptro#, ptline) a
LEFT JOIN stgArkonaSDPLOPC b ON a.ptco# = b.soco# AND a.opcode = b.solopc
LEFT JOIN stgArkonaSDPLOPC c ON a.ptco# = c.soco# AND a.corcode = c.solopc
WHERE a.paytype IS NOT NULL
  AND a.opcode IS NOT NULL;  

--/ 2.-------------------------------------------------------------------------
-- updates
/*
DROP TABLE tmpSDPRTXTa;
CREATE TABLE tmpSDPRTXTa ( 
      [sxco#] CIChar( 3 ),
      [sxro#] CIChar( 9 ),
      sxline Integer,
      sxltyp CIChar( 1 ),
      [sxseq#] Integer,
      sxtext CIChar( 50 )) IN DATABASE;         
*/      
DELETE FROM tmpSDPRTXTa;
INSERT INTO tmpSDPRTXTa
SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
FROM tmpsdprtxt  a
UNION 
SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
FROM tmppdpphdr a
INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
WHERE a.ptdtyp = 'RO'
  AND a.ptdoc# <> ''
  AND b.ptcmnt <> '';  
  
--- 3.-------------------------------------------------------------------------
-- 3. UPDATE ZROHeader: pleasenote
-- 51 seconds
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @curAll cursor AS 
  SELECT distinct a.storecode, a.ro
  FROM zROHeader a
  INNER JOIN tmpSDPRTXTa b ON a.storecode = b.sxco# AND a.ro = b.sxro# AND sxltyp = 'X';
DECLARE @curRO cursor AS
  SELECT sxco#, sxro#, sxtext
  FROM tmpSDPRTXTa
  WHERE sxltyp = 'X'
    AND sxco# = @storecode
    AND sxro# = @ro
  ORDER BY sxseq#;
@string = '';  
OPEN @curAll;
TRY
  WHILE FETCH @curAll DO
    @storecode = @curALL.storecode;
    @ro = @curAll.ro;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.sxtext);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY;
    UPDATE zROHEader
    SET PleaseNote = @string
    WHERE storecode = @storecode
      AND ro = @ro;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY;   
 

--/ 3.-------------------------------------------------------------------------
--- 4.-------------------------------------------------------------------------
-- zRODetail.LineStatus
UPDATE zRODetail
SET LineStatus = 
  CASE f.ptlsts
    WHEN 'C' THEN 'Closed'
    WHEN 'I' THEN 'Open'
  END 
FROM (
  SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptlsts
  FROM stgArkonaPDPPHDR a
  LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE b.ptltyp = 'A'
    AND b.ptpkey IS NOT NULL 
    AND b.ptseq# = 0) f
WHERE zRODetail.storecode = f.ptco#
  AND zRODetail.ro = f.ptdoc#
  AND zRODetail.line = f.ptline
  AND EXISTS (
    SELECT 1
    FROM xfmRO
    WHERE ro = zRODetail.ro) 
--/ 4.-------------------------------------------------------------------------
--- 5.-------------------------------------------------------------------------
-- zRODetail.Cause
-- 3 sec
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @curAll cursor AS 
  SELECT DISTINCT a.storecode, a.ro, a.line
  FROM zRODetail a
  INNER JOIN tmpSDPRTXTa b ON a.storecode = b.sxco# AND a.ro = b.sxro# AND b.sxltyp = 'L';
DECLARE @curRO cursor AS
  SELECT sxco#, sxro#, sxline, sxtext
  FROM tmpSDPRTXTa
  WHERE sxltyp = 'L'
    AND sxco# = @storecode
    AND sxro# = @ro
    AND sxline = @line
  ORDER BY sxseq#;
@string = '';  
OPEN @curAll;
TRY
  WHILE FETCH @curAll DO
    @storecode = @curALL.storecode;
    @ro = @curAll.ro;
    @line = @curAll.line;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.sxtext);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY;
    UPDATE zRODetail
    SET Cause = @string
    WHERE storecode = @storecode
      AND ro = @ro
      AND line = @line;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY;  
--/ 5.-------------------------------------------------------------------------
--- 6.-------------------------------------------------------------------------
-- complaint
-- 2 min
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @curAll cursor AS 
  SELECT DISTINCT a.storecode, a.ro, a.line
  FROM zRODetail a
  INNER JOIN tmpSDPRTXTa b ON a.storecode = b.sxco# AND a.ro = b.sxro# AND b.sxltyp = 'A';
DECLARE @curRO cursor AS
  SELECT sxco#, sxro#, sxline, sxtext
  FROM tmpSDPRTXTa
  WHERE sxltyp = 'A'
    AND sxco# = @storecode
    AND sxro# = @ro
    AND sxline = @line
  ORDER BY sxseq#;
@string = '';  
OPEN @curAll;
TRY
  WHILE FETCH @curAll DO
    @storecode = @curALL.storecode;
    @ro = @curAll.ro;
    @line = @curAll.line;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.sxtext);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY;
    UPDATE zRODetail
    SET Complaint = @string
    WHERE storecode = @storecode
      AND ro = @ro
      AND line = @line;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY; 
--/ 6.-------------------------------------------------------------------------
--- 7.-------------------------------------------------------------------------
-- correction
-- 2 min
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @curAll cursor AS 
  SELECT DISTINCT a.storecode, a.ro, a.line
  FROM zRODetail a
  INNER JOIN tmpSDPRTXTa b ON a.storecode = b.sxco# AND a.ro = b.sxro# AND b.sxltyp = 'Z';
DECLARE @curRO cursor AS
  SELECT sxco#, sxro#, sxline, sxtext
  FROM tmpSDPRTXTa
  WHERE sxltyp = 'Z'
    AND sxco# = @storecode
    AND sxro# = @ro
    AND sxline = @line
  ORDER BY sxseq#;
@string = '';  
OPEN @curAll;
TRY
  WHILE FETCH @curAll DO
    @storecode = @curALL.storecode;
    @ro = @curAll.ro;
    @line = @curAll.line;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.sxtext);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY;
    UPDATE zRODetail
    SET Correction = @string
    WHERE storecode = @storecode
      AND ro = @ro
      AND line = @line;
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY; 
--/ 7.-------------------------------------------------------------------------

