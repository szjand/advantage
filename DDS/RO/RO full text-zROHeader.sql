DROP TABLE zROHeader;
CREATE TABLE zROHeader (
      StoreCode cichar(3),
      RO CICHAR ( 9 ),
      WriterNumber cichar(3),      
      ROStatus CICHAR ( 12 ),
      OpenDate date,
      OpenDateDayNameMMDD cichar(12),
      CloseDate date,
      CloseDateDayNameMMDD CICHAR ( 12 ),
      FinalCloseDate date,
      FinalCloseDateDayNameMMDD CICHAR ( 12 ),      
      Customer CICHAR ( 30 ),
      HomePhone CICHAR ( 12 ),
      WorkPhone CICHAR ( 12 ),
      CellPhone CICHAR ( 12 ),
      CustomerEMail CICHAR ( 60 ),
      Vehicle CICHAR ( 60 ),
--      WriterFirstName CICHAR ( 40 ),
--      WriterLastName cichar(40),
--      WriterFullName cichar(40),
      PleaseNote Memo) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
  'zROHeader','zROHeader.adi','PK','StoreCode;RO','',2051,512,'' );       

--going to have to shortcut the writer info, temporarily
  
-- 1. first the initial scrape OPEN ro stuff  
-- maybe just ALL headers based ON whats IN zrodetail already
-- that should take care of everything except Today
-- 6/28
--   GROUP bopname, ADD closedate AND distinguish FROM final CLOSE date
-- DELETE FROM zroheader;
DELETE FROM zRODetail WHERE ro = '16108686'; -- one goofy dup
INSERT INTO zROHeader
SELECT a.storecode, a.ro, writerid, 
  CASE 
    WHEN b.ptrsts IS NOT NULL THEN 
      CASE b.ptrsts
        WHEN '1' THEN 'Open'
        WHEN '2' THEN 'In-Process'
        WHEN '3' THEN 'Appr-PD'
        WHEN '4' THEN 'Cashier'
        WHEN '5' THEN 'Cashier/D'
        WHEN '6' THEN 'Pre-Inv'
        WHEN '7' THEN 'Odom Rq'
        WHEN '9' THEN 'Parts-A'
        WHEN 'L' THEN 'G/L Error'
        WHEN 'P' THEN 'PI Rq'
        ELSE 'WTF'
      END
    ELSE 'Closed'
  END AS ROStatus,
  c.thedate, left(c.DayName,3) + ' ' + c.mmdd AS OpenDate,
  d.thedate, left(d.DayName,3) + ' ' + d.mmdd AS CloseDate,
  h.thedate, left(h.DayName,3) + ' ' + h.mmdd AS FinalCloseDate,
  a.customername,
  CASE 
    WHEN e.bnphon  = '0' THEN 'No Phone'
    WHEN e.bnphon  = '' THEN 'No Phone'
    WHEN e.bnphon IS NULL THEN 'No Phone'
    ELSE left(e.bnphon, 12)
  END AS HomePhone, 
  CASE 
    WHEN e.bnbphn  = '0' THEN 'No Phone'
    WHEN e.bnbphn  = '' THEN 'No Phone'
    WHEN e.bnbphn IS NULL THEN 'No Phone'
    ELSE left(e.bnbphn, 12)
  END  AS WorkPhone, 
  CASE 
    WHEN e.bncphon  = '0' THEN 'No Phone'
    WHEN e.bncphon  = '' THEN 'No Phone'
    WHEN e.bncphon IS NULL THEN 'No Phone'
    ELSE left(e.bncphon, 12)
  END  AS CellPhone,
  coalesce(case when e.bnemail = '' THEN 'None' ELSE e.bnemail END, 'None') as CustomerEMail,
  trim(TRIM(f.immake) + ' ' + TRIM(f.immodl)) AS vehicle,
  g.note     
FROM factro a
LEFT JOIN stgArkonaPDPPHDR b ON a.ro = b.ptdoc#
LEFT JOIN day c ON a.opendatekey = c.datekey
LEFT JOIN day d ON a.closedatekey = d.datekey
LEFT JOIN day h ON a.finalclosedatekey = h.datekey
--LEFT JOIN dds.stgArkonaBOPNAME f ON a.storecode = f.bnco# -- fucking honda
--  AND a.customerkey = f.bnkey  
--LEFT JOIN stgArkonaBOPNAME e ON a.customername = e.bnsnam --fucking arkona dups
--  AND a.customerkey = e.bnkey  
LEFT JOIN (
    select bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail
    from stgArkonaBOPNAME
    group by bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail) e ON a.customername = e.bnsnam
  AND a.customerkey = e.bnkey    
LEFT JOIN stgArkonaINPMAST f ON a.vin = f.imvin 
LEFT JOIN (
  SELECT *
  FROM zRONotes
  UNION ALL
  SELECT *
  FROM zOpenRONotes) g ON a.ro = g.ro   
WHERE EXISTS (
  SELECT 1
  FROM zrodetail
  WHERE ro = a.ro);
    


/*
DROP TABLE #wtf
SELECT storecode, ro
FROM #wtf
GROUP BY storecode, ro
HAVING COUNT(*) > 1

SELECT rostatus, COUNT(*)
FROM #wtf
GROUP BY rostatus

SELECT *
FROM #wtf
WHERE note <> ''

SELECT * FROM #wtf WHERE customername LIKE 'INVENTORY%'
SELECT * FROM #wtf ORDER BY length(trim(customeremail))


SELECT *
FROM zroheader a
WHERE NOT EXISTS (
  SELECT 1
  FROM zrodetail
  WHERE ro = a.ro)
  
  
SELECT ro,rostatus,opendatedaynamemmdd,closedatedaynamemmdd,customer,homephone,workphone,
  cellphone,customeremail,vehicle, 'WRITER', pleasenote
--SELECT *  
FROM zroheader
WHERE ro = '16122913'  

SELECT ro,line,linestatus,paytype,opcode,opcodedescription,complaint,cause,corcode,corcodedescription,correction 
FROM zrodetail WHERE ro = '16122913'
*/