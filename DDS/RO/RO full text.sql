-------------------------------------------------------------------------------
SELECT soco#, solopc
FROM stgArkonaSDPLOPC
GROUP BY soco#, solopc 
HAVING COUNT(*) > 1


SELECT *
FROM stgArkonaSDPLOPC
WHERE solopc IN (
SELECT solopc
FROM stgArkonaSDPLOPC
GROUP BY soco#, solopc 
HAVING COUNT(*) > 1)
ORDER BY solopc

SELECT * FROM factroline


SELECT opcode, COUNT(*) AS opcodes
FROM factroline
WHERE storecode <> 'RY3'
GROUP BY opcode

SELECT corcode, COUNT(*) AS corcodes
FROM factroline
WHERE storecode <> 'RY3'
GROUP BY corcode

SELECT DISTINCT socorr FROM stgarkonasdplopc

SELECT a.soco#, a.solopc, a.sodes1, a.socorr, b.opcodes, b.opRO, c.corcodes, c.corRO
FROM stgarkonasdplopc a
LEFT JOIN (
  SELECT opcode, COUNT(*) AS opcodes, MAX(ro) AS opRO
  FROM factroline
  WHERE storecode <> 'RY3'
  GROUP BY opcode) b ON a.solopc = b.opcode
LEFT JOIN (
  SELECT corcode, COUNT(*) AS corcodes, MAX(ro) AS corRO
  FROM factroline
  WHERE storecode <> 'RY3'
  GROUP BY corcode) c ON a.solopc = c.corcode
WHERE a.soco# <> 'RY3'
  AND (b.opcodes IS NOT NULL OR c.corcodes IS NOT NULL)
  
  
SELECT COUNT(*) -- 28303
FROM stgArkonaSDPLOPC a
WHERE soco# <> 'RY3'
  AND EXISTS (
    SELECT 1
    FROM factroline
    WHERE (opcode = a.solopc OR corcode = a.solopc))

--6/22 SDPRTXT: story
SELECT *
FROM stgArkonaSDPRTXT 

SELECT COUNT(*)
FROM stgArkonaSDPRTXT
-- this gives dups
SELECT sxco#, sxro#, sxline, sxseq#
FROM stgArkonaSDPRTXT 
GROUP BY sxco#, sxro#, sxline, sxseq#
HAVING COUNT(*) > 1

-- this gives only one dup (16111704)
SELECT sxco#, sxro#, sxltyp, sxline, sxseq#
FROM stgArkonaSDPRTXT 
GROUP BY sxco#, sxro#, sxltyp, sxline, sxseq#
HAVING COUNT(*) > 1

-- one of the dups
2 rows for seq 4, one IS sxltyp A, one IS Z
5250 the A type IS displayed IN yellow directly under the cor code
     the Z type IS IN green after Correction:
SELECT *
FROM stgArkonaSDPRTXT 
WHERE sxro# = '16107259'
--  AND sxline = 6
ORDER BY sxline, sxseq#  

SELECT *
FROM factroline a
WHERE a.ro = '16107259'v

SELECT a.ro, a.line, a.paytype, a.opcode, b.sodes1, a.corcode, c.sodes1,
  d.*
-- SELECT *
FROM factroline a
LEFT JOIN stgArkonaSDPLOPC b ON a.storecode = b.soco# AND a.opcode = b.solopc
LEFT JOIN stgArkonaSDPLOPC c ON a.storecode = c.soco# AND a.corcode = c.solopc
LEFT JOIN stgArkonaSDPRTXT d ON a.storecode = d.sxco# AND a.ro = d.sxro# AND a.line = d.sxline
WHERE a.ro = '16107259'
--WHERE a.ro = '16119794'
WHERE a.ro = '16119385'
WHERE a.ro = '16116828'
ORDER BY sxline, sxltyp, sxseq# 

sxltyp A looks LIKE it goes with the opcode (squawk), IF the opcode for the line = *, the 
  typ A text IS the squawk definition ON the ro, WHEN that IS the CASE, looks LIKE
  the seq = 0
  
SDPRTXT
need a single line for ro/line/sxltyp  

SELECT length('INTERNALLED LABOR TO ADVISOR ERROR-DID NOT SAVE OL') FROM system.iota
an issue IS how to combine the entire text IN a single attribute, how to combine
multiple sequence numbers, there IS no way to know whether to include a space BETWEEN
the individual sequences
i think i will default to adding a space after each sequence

SELECT * FROM stgarkonasdprtxt WHERE sxltyp = 'L'

are there more than one sxcodes for a line/type
!! NO, which, since i DO NOT know what the sxcodes mean, means the sxcodes are irrelevant
SELECT sxro#, sxline, sxltyp FROM (
SELECT sxro#, sxline, sxltyp, sxcode
FROM stgArkonaSDPRTXT
GROUP BY sxro#, sxline, sxltyp, sxcode
) x GROUP BY sxro#, sxline, sxltyp HAVING COUNT(*) > 1

sxltyp  sxcode  sxline  seq
  X       FA     950        5250: Please Note: IN yellow at the END of the ro
                            NOT line specific, but for the entire RO
  A                         complaint
  A                      0  line status
  Z                         5250: correction                    
  
  L                         5250: cause             
  
complaint AND cause seem to be intermingled, IF there IS a discreet, spelled out cause the sxltyp = L ,
otherwise the correction seems to be sxltyp = A        

SELECT sxco#, sxro#, sxline,
  CASE WHEN sxltyp = 'A' THEN sxtext END AS A,
  CASE WHEN sxltyp = 'X' THEN sxtext END AS X,
  CASE WHEN sxltyp = 'Z' THEN sxtext END AS Z,
  CASE WHEN sxltyp = 'L' THEN sxtext END AS L
FROM stgArkonaSDPRTXT
ORDER BY sxco#, sxro#, sxline
WHERE sxltyp <> 'x'    

SELECT a.sxco#, a.sxro#, a.sxline, a.sxseq#, a.sxtext AS Cause, sxltyp
FROM stgArkonaSDPRTXT a
WHERE a.sxltyp = 'L'
ORDER BY a.sxco#, a.sxro#, a.sxline, a.sxseq#

     
     
z: 39894
a: 32049
x: 17038
l: 282
SELECT COUNT(*)
FROM (
  SELECT sxco#, sxro#, sxline
  FROM stgArkonaSDPRTXT
  WHERE sxltyp = 'l'
  GROUP BY sxco#, sxro#, sxline) a     
  


this script parses the multi-row text INTO a single row
/*
DELETE FROM zROCause;
CREATE TABLE zROCause (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  cause memo) IN database;
CREATE TABLE zROComplaint (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  complaint memo) IN database;  
CREATE TABLE zROCorrection (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  correction memo) IN database;    
CREATE TABLE zRONotes (
  storecode cichar(3),
  ro cichar(9),
  note memo) IN database;  
L 1 minute
A 1.5  hours
Z 1.8 hours
X 
*/  
DECLARE @string string;
DECLARE @COUNT integer;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @type string;
DECLARE @TABLE string;
DECLARE @curAll cursor AS 
  SELECT sxco#, sxro#, sxline
  FROM stgArkonaSDPRTXT
  WHERE sxltyp = 'L'
--  WHERE sxltyp = 'A'
--  WHERE sxltyp = 'Z'
--  WHERE sxltyp = 'X'    
  GROUP BY sxco#, sxro#, sxline;   
DECLARE @curRO CURSOR AS 
  SELECT *
  FROM stgArkonaSDPRTXT
  WHERE sxco# = @curAll.sxco#
    AND sxltyp = 'L'
--    AND sxltyp = 'A'
--    AND sxltyp = 'Z'
--    AND sxltyp = 'X'
    AND sxro# = @curAll.sxro#
    AND sxline = @curAll.sxline
  ORDER BY sxseq#;
@COUNT = 0;
@string = '';
OPEN @curAll;

TRY 
WHILE FETCH @curAll DO  
  @storecode = @curAll.sxco#;
  @ro = @curAll.sxro#;
  @line = @curAll.sxline;
  OPEN @curRO;
  TRY
    WHILE FETCH @curRO DO
      @string = TRIM(@string) + ' ' + TRIM(@curRO.sxtext);
    END WHILE;
  FINALLY 
    CLOSE @curRO;
  END TRY;
  INSERT INTO zROCause values(@storecode, @ro, @line, @string);
--  INSERT INTO zROComplaint values(@storecode, @ro, @line, @string);
--  INSERT INTO zROCorrection values(@storecode, @ro, @line, @string);
--  INSERT INTO zRONotes values(@storecode, @ro, @string);
  @string = '';
END WHILE;
  
FINALLY
  CLOSE @curAll;
END TRY;  

SELECT * FROM zRONotes;

                       
-- 6/23
------------------------ WORK FROM scoTest ------------------------------------
SELECT * FROM dds.stgArkonaSDPLOPC   

SELECT a.ro, a.line, a.paytype, a.opcode, b.sodes1, c.complaint, a.corcode
FROM dds.factroline a
LEFT JOIN stgArkonaSDPLOPC b ON a.storecode = b.soco#
  AND a.opcode = b.sodes1
LEFT JOIN zrocomplaint c ON a.ro = c.ro
  AND a.line = c.line
WHERE a.ro = '16121512'        

-- header
SELECT a.ro, b.firstname, /*case*/'Closed' as ROStatus, a.closedate AS [Open/Close]/*case*/ , c.note AS [Please Note]
FROM warrantyros a 
INNER JOIN servicewriters b ON a.eeusername = b.eeusername
LEFT JOIN dds.zronotes c ON a.ro = c.ro
WHERE a.followupcomplete = false         
-- detail
-- CASE the opcode/sodes1 get rid of ALL the fucking customer states ?
--SELECT ro, line FROM (
SELECT b.ro, b.line, /*case*/'Closed' as LineStatus,/*case*/ b.paytype, b.opcode,
  c.sodes1 AS OpDesc, d.complaint, e.cause, b.corcode, f.sodes1 AS CorDesc,
  g.Correction 
--SELECT *  
FROM warrantyros a
LEFT JOIN dds.factroline b ON a.ro = b.ro   
LEFT JOIN dds.stgArkonaSDPLOPC c ON b.storecode = c.soco#
  AND b.opcode = c.solopc
LEFT JOIN dds.zROComplaint d ON b.storecode = d.storecode
  AND b.ro = d.ro AND b.line = d.line 
LEFT JOIN dds.zROCause e ON b.storecode = e.storecode
  AND b.ro = e.ro AND b.line = e.line  
LEFT JOIN dds.stgArkonaSDPLOPC f ON b.storecode = f.soco#
  AND b.corcode = f.solopc  
LEFT JOIN dds.zROCorrection g ON b.storecode = g.storecode
  AND b.ro = g.ro AND b.line = g.line
--) x GROUP BY ro,line HAVING COUNT(*) > 1  

-- ok, that's ALL well AND good for closed ros, what about OPEN ros
-- the whole notion of line status ON OPEN ros

-- 6/23 open ro line status
Select a.ptdoc#, a.ptdate, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt, b.ptlsts
from dds.stgArkonaPDPPHDR a
left join dds.stgArkonapdppdet b on a.ptpkey = b.ptpkey
where a.ptdtyp = 'RO'
  and a.ptco# = 'RY1'
--  and a.ptdate > 20130000
  and left(trim(PTDOC#),2) = '16'
  and b.ptltyp in ('A','Z')
order by ptdoc# desc, ptline asc, ptseq# asc

the first thing i am noticing is that a single line can have multiple statuses

SELECT ptdoc#, ptline, ptltyp
FROM (
  SELECT ptdoc#, ptline, ptltyp, ptlsts
  FROM (
    Select a.ptdoc#, a.ptdate, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt, b.ptlsts
    from dds.stgArkonaPDPPHDR a
    left join dds.stgArkonapdppdet b on a.ptpkey = b.ptpkey
    where a.ptdtyp = 'RO'
      and a.ptco# = 'RY1'
    --  and a.ptdate > 20130000
      and left(trim(PTDOC#),2) = '16'
      and b.ptltyp in ('A','Z')) m
  GROUP BY ptdoc#, ptline, ptltyp, ptlsts) n
GROUP BY ptdoc#, ptline, ptltyp HAVING COUNT(*) > 1    


Select a.ptdoc#, a.ptdate, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt, b.ptlsts
INTO #wtf
from dds.stgArkonaPDPPHDR a
left join dds.stgArkonapdppdet b on a.ptpkey = b.ptpkey
where a.ptdtyp = 'RO'
  and a.ptco# = 'RY1'
--  and a.ptdate > 20130000
  and left(trim(PTDOC#),2) = '16'
  and b.ptltyp in ('A','Z')
  AND ptdoc# IN (
    SELECT ptdoc#
    FROM (
      SELECT ptdoc#, ptline, ptltyp, ptlsts
      FROM (
        Select a.ptdoc#, a.ptdate, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt, b.ptlsts
        from dds.stgArkonaPDPPHDR a
        left join dds.stgArkonapdppdet b on a.ptpkey = b.ptpkey
        where a.ptdtyp = 'RO'
          and a.ptco# = 'RY1'
        --  and a.ptdate > 20130000
          and left(trim(PTDOC#),2) = '16'
          and b.ptltyp in ('A','Z')) m
      GROUP BY ptdoc#, ptline, ptltyp, ptlsts) n
    GROUP BY ptdoc#, ptline, ptltyp HAVING COUNT(*) > 1)   
order by ptdoc# desc, ptline asc, ptltyp, ptseq# asc

SELECT * FROM #wtf

ok, observations AND assumptions
status IS ON ptltyp A only
SELECT DISTINCT ptlsts FROM #wtf WHERE ptltyp = 'z'
the "real" line status IS ON ptltyp A ptseq# 0
SELECT * FROM #wtf WHERE ptltyp = 'A' AND ptseq# = 0 
Turns out that ptlsts corresponds direclty with Stat: IN 5250 ON OPEN ros
which IS NOT ON 5250 for closed ros
which ALL makes sense, for an ro IN cashier status, ALL lines will be closed

so, now, given that typ A seq 0 IS status
need to parse that shit out


FUCK 
  ro 19138403
  Cashier status
  EXISTS IN both sdprhdr AND pdpphdr
  which blows the notion that any ro sdprhdr has been final closed
  5250 
    OPEN Repair ORDER shows line 4 stat C
    Closed Repair ORDER shows line 4 IN red, no lines show Stat
  WTFucketyFuckFuck
  this fucks up the notion that any ro IN cashier status has ALL closed lines
 
Select a.ptdoc#, a.ptdate, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt, b.ptlsts
from dds.stgArkonaPDPPHDR a
left join dds.stgArkonapdppdet b on a.ptpkey = b.ptpkey
where a.ptdtyp = 'RO'
  and a.ptco# = 'RY1'
--  and a.ptdate > 20130000
  and left(trim(PTDOC#),2) = '19'
  and b.ptltyp in ('A','Z')
  
ok, calm the fuck down
the pattern IS to only include PDP shit WHERE the ro does NOT exist IN SDP  

SELECT *
FROM dds.factro--line
WHERE ro = '19138403'

SELECT *
FROM dds.zrocomplaint
WHERE ro = '19138403'

narrow it down, OPEN rolinestatus
so INSERT INTO zRO... WHERE the ro does NOT exist 
zRO IS based IN sdprtxt, now include FROM PDP


this script parses the multi-row text INTO a single row
/*
CREATE TABLE zOpenROComplaint (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  complaint memo) IN database;  
CREATE TABLE zOpenROCorrection (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  correction memo) IN database;    
CREATE TABLE zOpenROCause (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  cause memo) IN database;
CREATE TABLE zOpenRONotes (
  storecode cichar(3),
  ro cichar(9),
  note memo) IN database;    
*/ 
-- line status     
CREATE TABLE zOpenROLineStatus (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  status cichar(9));   
 
--SELECT storecode, ro, line FROM (
INSERT INTO zOpenROLineStatus
SELECT a.ptco# AS storecode, a.ptdoc# AS ro, b.ptline AS line, 
  CASE b.ptlsts
    WHEN 'C' THEN 'Closed'
    WHEN 'I' THEN 'Open'
    ELSE 'WTF'
  END AS status
FROM stgArkonaPDPPHDR a
INNER JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
  AND b.ptltyp = 'A' AND b.ptseq# = 0
WHERE a.ptdtyp = 'RO'
  AND a.ptdoc# <> ''
  AND b.ptpkey IS NOT NULL 
  AND NOT EXISTS ( -- leave out anything already IN SDP
    SELECT 1
    FROM stgArkonaSDPRHDR
    WHERE ptro# = a.ptdoc#)    
--) x GROUP BY storecode, ro, line HAVING COUNT(*) > 1    

/*      RY1: typ A seq 0 IS status only, RY2, typ A seq 0 also contains text     */
/*        which IS ok, include the txt COLUMN FROM both IN the concatenation     */
/*        TRIM the END result                                                    */
-- line complaint/correction
-- A: complaint
-- Z: correction
-- L: cause
-- X: ro level notes
DECLARE @string string;
DECLARE @storecode string;
DECLARE @ptpkey integer;
DECLARE @line integer;
DECLARE @ro string;
DECLARE @curAll cursor AS 
  SELECT a.ptco#, a.ptdoc#, a.ptpkey, b.ptline
  FROM stgArkonaPDPPHDR a
  INNER JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
--    AND b.ptltyp = 'A' AND b.ptseq# <> 0
--    AND b.ptltyp = 'Z' 
--    AND b.ptltyp = 'L'
    AND b.ptltyp = 'X'
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND b.ptpkey IS NOT NULL 
    AND NOT EXISTS ( -- leave out anything already IN SDP
      SELECT 1
      FROM stgArkonaSDPRHDR
      WHERE ptro# = a.ptdoc#)
  GROUP BY a.ptco#, a.ptdoc#, a.ptpkey, b.ptline;   
DECLARE @curRO CURSOR AS
  SELECT ptpkey, ptline, ptltyp, ptseq#, ptcmnt
  FROM stgArkonaPDPPDET
  WHERE ptpkey = @curAll.ptpkey
    AND ptline = @curAll.ptline
--    AND ptltyp = 'A' AND ptseq# <> 0
--    AND ptltyp = 'Z'
--    AND ptltyp = 'L'
    AND ptltyp = 'X'
  ORDER BY ptseq#;           
@string = '';
OPEN @curAll;
TRY 
  WHILE FETCH @curAll DO
    @storecode = @curAll.ptco#;
    @ro = @curAll.ptdoc#;
    @ptpkey = @curAll.ptpkey;
    @line = @curAll.ptline;
    OPEN @curRO;
    TRY
      WHILE FETCH @curRO DO
        @string = TRIM(@string) + ' ' + TRIM(@curRO.ptcmnt);
      END WHILE;
    FINALLY
      CLOSE @curRO;
    END TRY; 
--    INSERT INTO zOpenROComplaint values(@storecode, @ro, @line, @string);
--    INSERT INTO zOpenROCorrection values(@storecode, @ro, @line, @string);
--    INSERT INTO zOpenROCause values(@storecode, @ro, @line, @string);
    INSERT INTO zOpenRONotes values(@storecode, @ro, @string);
    @string = '';
  END WHILE;
FINALLY
  CLOSE @curAll;
END TRY;


-- ok, back to scotest, 
-- OPEN ros (AgedROs)
-- header
SELECT a.ro, b.firstname, /*case*/roStatus, a.opendate AS [Open/Close]/*case*/ , c.note AS [Please Note]
FROM agedros a 
INNER JOIN servicewriters b ON a.eeusername = b.eeusername
LEFT JOIN dds.zOpenronotes c ON a.ro = c.ro
ORDER BY a.ro
    
-- detail
-- CASE the opcode/sodes1 get rid of ALL the fucking customer states ?
--SELECT ro, line FROM (
SELECT b.ro, b.line, /*case*/h.status as LineStatus,/*case*/ b.paytype, b.opcode,
  c.sodes1 AS OpDesc, d.complaint, e.cause, b.corcode, f.sodes1 AS CorDesc,
  g.Correction 
--SELECT *  
FROM AgedROs a
LEFT JOIN dds.factroline b ON a.ro = b.ro   
LEFT JOIN dds.stgArkonaSDPLOPC c ON b.storecode = c.soco#
  AND b.opcode = c.solopc
LEFT JOIN dds.zOpenROComplaint d ON b.storecode = d.storecode
  AND b.ro = d.ro AND b.line = d.line 
LEFT JOIN dds.zOpenROCause e ON b.storecode = e.storecode
  AND b.ro = e.ro AND b.line = e.line  
LEFT JOIN dds.stgArkonaSDPLOPC f ON b.storecode = f.soco#
  AND b.corcode = f.solopc  
LEFT JOIN dds.zOpenROCorrection g ON b.storecode = g.storecode
  AND b.ro = g.ro AND b.line = g.line
LEFT JOIN dds.zOpenROLineStatus h ON b.storecode = h.storecode
  AND b.ro = h.ro AND b.line = h.line  

-- closed ros (warranty)
-- header
SELECT a.ro, b.firstname, /*case*/'Closed' as ROStatus, a.closedate AS [Open/Close]/*case*/ , c.note AS [Please Note]
FROM warrantyros a 
INNER JOIN servicewriters b ON a.eeusername = b.eeusername
LEFT JOIN dds.zronotes c ON a.ro = c.ro
WHERE a.followupcomplete = false         
-- detail
-- CASE the opcode/sodes1 get rid of ALL the fucking customer states ?
--SELECT ro, line FROM (
SELECT b.ro, b.line, /*case*/'Closed' as LineStatus,/*case*/ b.paytype, b.opcode,
  c.sodes1 AS OpDesc, d.complaint, e.cause, b.corcode, f.sodes1 AS CorDesc,
  g.Correction 
--SELECT *  
FROM warrantyros a
LEFT JOIN dds.factroline b ON a.ro = b.ro   
LEFT JOIN dds.stgArkonaSDPLOPC c ON b.storecode = c.soco#
  AND b.opcode = c.solopc
LEFT JOIN dds.zROComplaint d ON b.storecode = d.storecode
  AND b.ro = d.ro AND b.line = d.line 
LEFT JOIN dds.zROCause e ON b.storecode = e.storecode
  AND b.ro = e.ro AND b.line = e.line  
LEFT JOIN dds.stgArkonaSDPLOPC f ON b.storecode = f.soco#
  AND b.corcode = f.solopc  
LEFT JOIN dds.zROCorrection g ON b.storecode = g.storecode 
  AND b.ro = g.ro AND b.line = g.line 

  
-- combine header AND detail for OPEN ros
-- closed ros don't need a line status  
-- OPEN (AgedROS)
SELECT a.ro, i.firstname AS Writer, a.roStatus, a.opendate AS [Opened], j.note AS [Please Note],
  b.line, /*case*/h.status as LineStatus,/*case*/ b.paytype, b.opcode,
  c.sodes1 AS OpDesc, d.complaint, e.cause, b.corcode, f.sodes1 AS CorDesc,
  g.Correction 
FROM AgedROs a
INNER JOIN servicewriters i ON a.eeusername = i.eeusername
LEFT JOIN dds.zOpenRONotes j ON a.ro = j.ro
LEFT JOIN dds.factroline b ON a.ro = b.ro   
LEFT JOIN dds.stgArkonaSDPLOPC c ON b.storecode = c.soco#
  AND b.opcode = c.solopc
LEFT JOIN dds.zOpenROComplaint d ON b.storecode = d.storecode
  AND b.ro = d.ro AND b.line = d.line 
LEFT JOIN dds.zOpenROCause e ON b.storecode = e.storecode
  AND b.ro = e.ro AND b.line = e.line  
LEFT JOIN dds.stgArkonaSDPLOPC f ON b.storecode = f.soco#
  AND b.corcode = f.solopc  
LEFT JOIN dds.zOpenROCorrection g ON b.storecode = g.storecode
  AND b.ro = g.ro AND b.line = g.line
LEFT JOIN dds.zOpenROLineStatus h ON b.storecode = h.storecode
  AND b.ro = h.ro AND b.line = h.line  
ORDER BY a.ro, b.line  



  
-- combine header AND detail for closed ros
-- closed ros don't need a line status - will always be Closed 
-- Closed (warranty)
SELECT a.ro, b.firstname, /*case*/'Closed' as ROStatus, a.closedate AS [Open/Close]/*case*/ , c.note AS [Please Note]
FROM warrantyros a 
INNER JOIN servicewriters b ON a.eeusername = b.eeusername
LEFT JOIN dds.zronotes c ON a.ro = c.ro
WHERE a.followupcomplete = false         
-- detail
-- CASE the opcode/sodes1 get rid of ALL the fucking customer states ?
--SELECT ro, line FROM (
SELECT a.RO AS RO, 'Closed' as ROStatus, '12/31/9999' as OpenDate, a.CloseDate, a.Customer, a.HomePhone, a.WorkPhone, a.CellPhone, 
  'seriously@fuckmylife.com' AS CustomerEMail, a.Vehicle,
  h.FirstName AS WriterFirstName, i.note as PleaseNote, b.Line, 
  /*case*/'Closed' as LineStatus,/*case*/ b.paytype AS PayType, b.opcode AS OpCode,
  c.sodes1 AS OpCodeDesc, d.complaint AS Complaint, e.cause AS Cause, 
  b.corcode AS CorCode, f.sodes1 AS CorCodeDesc,
  g.Correction 
--SELECT *  
FROM warrantyros a
LEFT JOIN dds.factroline b ON a.ro = b.ro   
LEFT JOIN dds.stgArkonaSDPLOPC c ON b.storecode = c.soco#
  AND b.opcode = c.solopc
LEFT JOIN dds.zROComplaint d ON b.storecode = d.storecode
  AND b.ro = d.ro AND b.line = d.line 
LEFT JOIN dds.zROCause e ON b.storecode = e.storecode
  AND b.ro = e.ro AND b.line = e.line  
LEFT JOIN dds.stgArkonaSDPLOPC f ON b.storecode = f.soco#
  AND b.corcode = f.solopc  
LEFT JOIN dds.zROCorrection g ON b.storecode = g.storecode 
  AND b.ro = g.ro AND b.line = g.line 
INNER JOIN servicewriters h ON a.eeusername = h.eeusername
LEFT JOIN dds.zRONotes i ON a.ro = i.ro  
WHERE a.ro = '16119081'

-- 6/24 --------------------------------------------------------------------------
-- generalize it to one GetRO stored proc, called FROM anywhere, OPEN OR closed
SELECT ro
FROM agedros
UNION
SELECT ro
FROM warrantyros

SELECT ro
FROM (
  SELECT ro
  FROM agedros
  UNION
  SELECT ro
  FROM warrantyros) a
GROUP BY ro HAVING COUNT(*) > 1  


SELECT *
FROM dds.stgArkonaBOPNAME

SELECT storecode, ro
FROM (
  SELECT storecode, ro
  FROM dds.factro
  WHERE storecode <> 'ry3'
  UNION 
  SELECT storecode, ro 
  FROM factROToday) b
GROUP BY storecode, ro HAVING COUNT(*) > 1  

DECLARE @ro cichar(9);
@ro = '16119081';
SELECT a.StoreCode, a.RO, a.rostatus, d.thedate AS OpenDate, e.thedate AS CloseDate, 
  a.customername AS Customer, 
  CASE 
    WHEN f.bnphon  = '0' THEN 'No Phone'
    WHEN f.bnphon  = '' THEN 'No Phone'
    WHEN f.bnphon IS NULL THEN 'No Phone'
    ELSE left(f.bnphon, 12)
  END AS HomePhone, 
  CASE 
    WHEN f.bnbphn  = '0' THEN 'No Phone'
    WHEN f.bnbphn  = '' THEN 'No Phone'
    WHEN f.bnbphn IS NULL THEN 'No Phone'
    ELSE left(f.bnbphn, 12)
  END  AS WorkPhone, 
  CASE 
    WHEN f.bncphon  = '0' THEN 'No Phone'
    WHEN f.bncphon  = '' THEN 'No Phone'
    WHEN f.bncphon IS NULL THEN 'No Phone'
    ELSE left(f.bncphon, 12)
  END  AS CellPhone,    
  case when f.bnemail = '' THEN 'None' ELSE f.bnemail END as CustomerEMail, 
  trim(TRIM(g.immake) + ' ' + TRIM(g.immodl)) AS vehicle, h.firstname AS WriterFirstName,
  b.note AS PleaseNote, c.line, coalesce(i.status, 'Closed'), c.paytype AS PayType, 
  c.opcode 
-- SELECT *
FROM (
  SELECT z.*, 'Closed' AS RoStatus
  FROM dds.factro z
  WHERE ro = @ro
  UNION 
  SELECT *
  FROM factROToday
  WHERE ro = @ro) a
LEFT JOIN (
  SELECT *
  FROM dds.zRONotes
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenRONotes
  WHERE ro = @ro) b ON b.ro = a.ro    
LEFT JOIN (
  SELECT *
  FROM dds.factroline
  WHERE ro = @ro
  UNION
  SELECT *
  FROM factROLineToday
  WHERE ro = @ro) c ON a.ro = c.ro
LEFT JOIN dds.day d ON a.opendatekey = d.datekey 
LEFT JOIN dds.day e ON a.finalclosedatekey = e.datekey 
LEFT JOIN dds.stgArkonaBOPNAME f ON a.storecode = f.bnco#
  AND a.customerkey = f.bnkey   
LEFT JOIN dds.stgArkonaINPMAST g ON a.vin = g.imvin 
LEFT JOIN servicewriters h ON a.storecode = h.storecode
  AND a.writerid = h.writernumber  
LEFT JOIN dds.zOpenROLineStatus i ON a.ro = i.ro
LEFT JOIN dds.stgArkonaSDPLOPC j ON c.storecode = j.soco#
  AND c.opcode = j.solopc  

-- header  
DECLARE @ro cichar(9);
@ro = '16119081';
SELECT @ro, a.rostatus, d.thedate AS OpenDate, e.thedate AS CloseDate, 
  a.customername AS Customer, 
  CASE 
    WHEN f.bnphon  = '0' THEN 'No Phone'
    WHEN f.bnphon  = '' THEN 'No Phone'
    WHEN f.bnphon IS NULL THEN 'No Phone'
    ELSE left(f.bnphon, 12)
  END AS HomePhone, 
  CASE 
    WHEN f.bnbphn  = '0' THEN 'No Phone'
    WHEN f.bnbphn  = '' THEN 'No Phone'
    WHEN f.bnbphn IS NULL THEN 'No Phone'
    ELSE left(f.bnbphn, 12)
  END  AS WorkPhone, 
  CASE 
    WHEN f.bncphon  = '0' THEN 'No Phone'
    WHEN f.bncphon  = '' THEN 'No Phone'
    WHEN f.bncphon IS NULL THEN 'No Phone'
    ELSE left(f.bncphon, 12)
  END  AS CellPhone,    
  case when f.bnemail = '' THEN 'None' ELSE f.bnemail END as CustomerEMail, 
  trim(TRIM(g.immake) + ' ' + TRIM(g.immodl)) AS vehicle, h.firstname AS WriterFirstName,
  b.note AS PleaseNote
FROM (
  SELECT storecode, ro, writerid, vin, customername, customerkey, opendatekey, 
    closedatekey, finalclosedatekey, status AS ROStatus
  FROM factrotoday 
  WHERE ro = @ro
  UNION 
  SELECT storecode, ro, writerid, vin, customername, customerkey, opendatekey, 
    closedatekey, finalclosedatekey, 'Closed' AS ROStatus 
  FROM dds.factro
  WHERE ro = @ro
    AND NOT EXISTS(
      SELECT 1
      FROM factrotoday
      WHERE ro = @ro)) a
LEFT JOIN (
  SELECT *
  FROM dds.zRONotes
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenRONotes
  WHERE ro = @ro) b ON b.ro = a.ro   
LEFT JOIN dds.day d ON a.opendatekey = d.datekey 
LEFT JOIN dds.day e ON a.finalclosedatekey = e.datekey    
LEFT JOIN dds.stgArkonaBOPNAME f ON a.storecode = f.bnco#
  AND a.customerkey = f.bnkey  
LEFT JOIN dds.stgArkonaINPMAST g ON a.vin = g.imvin 
LEFT JOIN servicewriters h ON a.storecode = h.storecode
  AND a.writerid = h.writernumber;   
  
--detail
DECLARE @ro cichar(9);
@ro = '16122224';
SELECT @ro,  b.Line, 
  b.paytype AS PayType, b.opcode AS OpCode,
  c.sodes1 AS OpCodeDesc, d.complaint AS Complaint, e.cause AS Cause, 
  b.corcode AS CorCode, f.sodes1 AS CorCodeDesc,
  g.Correction 
FROM (
  SELECT *
  FROM dds.factroline
  WHERE ro = @ro
  UNION
  SELECT *
  FROM factROLineToday
  WHERE ro = @ro) b --ON a.storecode = b.storecode AND a.ro = b.ro  
LEFT JOIN dds.stgArkonaSDPLOPC c ON b.storecode = c.soco#
  AND b.opcode = c.solopc
LEFT JOIN (
  SELECT *
  FROM dds.zROComplaint
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenROComplaint
  WHERE ro = @ro) d ON b.storecode = d.storecode AND b.ro = d.ro AND b.line = d.line 
LEFT JOIN (
  SELECT *
  FROM dds.zROCause
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenROCause
  WHERE ro = @ro) e ON b.storecode = e.storecode AND b.ro = e.ro AND b.line = e.line  
LEFT JOIN dds.stgArkonaSDPLOPC f ON b.storecode = f.soco#
  AND b.corcode = f.solopc  
LEFT JOIN (
  SELECT * 
  FROM dds.zROCorrection
  WHERE ro = @ro
  UNION ALL
  SELECT *
  FROM dds.zOpenROCorrection
  WHERE ro = @ro) g ON b.storecode = g.storecode AND b.ro = g.ro AND b.line = g.line 
LEFT JOIN dds.zOpenROLineStatus h ON b.ro = h.ro AND b.line = h.line;  


-- 6/25 nightly

select *
from ( 
  select distinct sxro# as ro
  from stgArkonasdprtxt) a 
inner join (
  select trim(ptdoc#) as ptdoc#
  from stgArkonapdpphdr
  where ptdoc# <> '') b on trim(a.ro) = trim(b.ptdoc#)

b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt, b.ptlsts
 
SELECT a.*, d.ptline, d.ptltyp, d.ptseq#, d.ptcmnt, d.ptlsts
FROM stgArkonaSDPRTXT a
LEFT JOIN (
  select *
  from stgArkonaPDPPHDR b 
  INNER JOIN stgArkonaPDPPDET c ON b.ptpkey = c.ptpkey) d ON a.sxro# = d.ptdoc# AND a.sxline = d.ptline AND a.sxseq# = d.ptseq# AND a.sxltyp = d.ptltyp
WHERE EXISTS (
  SELECT 1
  FROM stgarkonapdpphdr
  WHERE ptdoc# = a.sxro#)  
ORDER BY sxro#, sxline, sxseq#  

SELECT a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt, b.ptlsts
FROM stgArkonaPDPPHDR a
LEFT JOIN stgArkonaPDPPDET b ON a.ptpkey = b.ptpkey
WHERE a.ptdtyp = 'RO'
  AND NOT EXISTS (
    SELECT 1
    FROM stgArkonaSDPRTXT
    WHERE sxro# = a.ptdoc#)
    
SELECT * FROM stgArkonaSDPRTXT WHERE sxtext = '1/2/13 - TECH REQUESTING OL FOR EXTENSIVE DIAGNOSI S TIME. BEV L'   
    
    
