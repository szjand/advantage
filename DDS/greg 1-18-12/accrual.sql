-- 1. 30459 shows IN result SET AND should NOT
-- 2. 0 amounts empl# 1106360
-- 2/2 from jeri: The following dist codes are for semi-monthly:  BSM, SALE, TEAM, USCM, USCD 
-- issue why does Tyler Dahl emp# 30459 empkey 1289 hired 1/2/08 term 6/30/09 show up
-- AND 0 amounts: removed be exempting jeri's list of dist code, 
-- 2/3/12
--hmm, which are semi-monthly, which IS something i know abt an employee, but WHERE
-- what get's accrued AND what doesn't


/*  30459 problem - employed for the interval ***********/ 
select EmployeeNumber, Name, DistCode, 
  Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) as GrossPay,
  Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.062 as FICA,
  Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.0145 as Medicare
-- SELECT COUNT(*)  
from edwEmployeeDim 
where StoreCodeDesc = 'Rydell' 
  and PayrollClassCodeDesc = 'Salary' 
  AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
  AND employeekeyfromdate <= '01/31/2012' 
  AND employeekeythrudate >= '01/15/2012' 
  AND ActiveCodeDesc = 'Active'
  AND employeenumber = '1106360'
--  and employeenumber = '30459'
/*  30459 problem - employed for the interval ***********/ 

-- WHERE are the rules, what's the connection BETWEEN dist codes AND pay periods?

SELECT DISTINCT ymco#, ym
FROM stgArkonaPYMAST

SELECT ymempn, ymname, ymclas, ympper

FROM stgArkonaPYMAST

SELECT MAX(ymco#), MAX(ymempn), MAX(ymname), ymclas, ympper
FROM stgArkonaPYMAST
WHERE ymactv <> 'T'
GROUP BY ymclas, ympper

SELECT COUNT(*), ymclas, ympper
FROM stgArkonaPYMAST
WHERE ymactv <> 'T'
GROUP BY ymclas, ympper

-- nah what i want IS dist code AND pay period, that was payroll class (H, S, C)

SELECT COUNT(*), ymco#, ymdist, ympper
FROM stgArkonaPYMAST
WHERE ymactv <> 'T'
GROUP BY ymco#, ymdist, ympper

SELECT ymco#, ymdist
FROM (
SELECT COUNT(*), ymco#, ymdist, ympper
FROM stgArkonaPYMAST
WHERE ymactv <> 'T'
GROUP BY ymco#, ymdist, ympper)x
GROUP BY ymco#, ymdist 
  HAVING COUNT(*) > 1
  
SELECT DISTINCT ympper FROM stgArkonaPYMAST -- shit, only 2: B & S

so with only 2 pay periods, IS it AS simple AS only those  
-- 2 dups  bsm, team
SELECT ymname, ymactv, ymdist, ympper
FROM stgarkonapymast
WHERE ymco# = 'RY1'
  AND ymdist IN ('BSM','TEAM')  

period of time: ?  
day past last paycheck through EOM
ooh, does pyhshdta tell me the pay period? 
first things first, get some normal christian dates IN stgArkonaPYHSHDTA




-- emp# fix & excl jeri's dist codes  
select 'RST' as Journal, '1/31/2012' as [Date], Account, EmployeeNumber as Control, 
  'RST-1/31/12' as Document, 'RST-01/31/12' as Reference, sum(Amount) as Amount, 
  'Payroll Accrual' as Description
FROM ( -- x
  select EmployeeNumber, GrossAccount as Account, Gross as Amount
  FROM ( -- pay
    SELECT EmployeeNumber, Name, DistCode, ytaco# as Company, ytagpa as GrossAccount, 
      ytaeta as FICAAccount, ytamed as MedicareAccount, 
      round(GrossPay * ytagpp / 100, 2) as Gross, round(FICA * ytagpp / 100, 2) as FICA, 
      round(Medicare * ytagpp / 100, 2) as Medicare
    FROM (
      SELECT EmployeeNumber, Name, DistCode, 
        SUM(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) as GrossPay,
        SUM(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.062 as FICA,
        SUM(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.0145 as Medicare
      FROM edwClockHoursFact chf
      INNER JOIN edwEmployeeDim ed on ed.EmployeeKey = chf.EmployeeKey  
        AND StoreCodeDesc = 'Rydell' 
        AND PayrollClassCodeDesc = 'Hourly' 
        AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
      INNER JOIN Day on Day.DateKey = chf.DateKey 
        AND TheDate BETWEEN '1/15/2012' AND '1/31/2012'
      GROUP BY EmployeeNumber, Name, DistCode
        HAVING SUM(ClockHours) > 0
    UNION ALL 
    select EmployeeNumber, Name, DistCode, 
      Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) as GrossPay,
      Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.062 as FICA,
      Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.0145 as Medicare
    from edwEmployeeDim 
    where StoreCodeDesc = 'Rydell' 
      and PayrollClassCodeDesc = 'Salary' 
      AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
      AND employeekeyfromdate <= '01/31/2012' 
      AND employeekeythrudate >= '01/15/2012' 
      AND ActiveCodeDesc = 'Active') pay
  LEFT JOIN stgArkonaPYACTGR acct on acct.ytadic = pay.DistCode 
    and acct.ytaco# = 'RY1' 
    and ytadic not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')) a
  union all
  select EmployeeNumber, FICAAccount, FICA
  FROM ( -- b
    select EmployeeNumber, Name, DistCode, ytaco# as Company, ytagpa as GrossAccount, 
      ytaeta as FICAAccount, ytamed as MedicareAccount, 
      round(GrossPay * ytagpp / 100, 2) as Gross, round(FICA * ytagpp / 100, 2) as FICA, 
      round(Medicare * ytagpp / 100, 2) as Medicare
    FROM ( -- pay
      select EmployeeNumber, Name, DistCode, 
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) as GrossPay,
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.062 as FICA,
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.0145 as Medicare
      from edwClockHoursFact chf
      inner join edwEmployeeDim ed on ed.EmployeeKey = chf.EmployeeKey 
        and StoreCodeDesc = 'Rydell' 
        and PayrollClassCodeDesc = 'Hourly' 
        and DistCode not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
      inner join Day on Day.DateKey = chf.DateKey 
        and TheDate between '1/15/2012' and '1/31/2012'
      group by EmployeeNumber, Name, DistCode
        having sum(ClockHours) > 0
      union all
      select EmployeeNumber, Name, DistCode, 
        Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) as GrossPay,
        Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.062 as FICA,
        Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.0145 as Medicare
      from edwEmployeeDim 
      where StoreCodeDesc = 'Rydell' 
        and PayrollClassCodeDesc = 'Salary' 
        and DistCode not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
        AND employeekeyfromdate <= '01/31/2012' 
        AND employeekeythrudate >= '01/15/2012'
        AND ActiveCodeDesc = 'Active') pay
  left join stgArkonaPYACTGR acct on acct.ytadic = pay.DistCode 
    and acct.ytaco# = 'RY1' 
    and ytadic not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')) b
  union all
  select EmployeeNumber, MedicareAccount, Medicare 
  FROM ( -- c
    select EmployeeNumber, Name, DistCode, ytaco# as Company, ytagpa as GrossAccount, 
      ytaeta as FICAAccount, ytamed as MedicareAccount, 
      round(GrossPay * ytagpp / 100, 2) as Gross, round(FICA * ytagpp / 100, 2) as FICA, 
      round(Medicare * ytagpp / 100, 2) as Medicare
    FROM ( -- pay
      select EmployeeNumber, Name, DistCode, sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) as GrossPay,
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.062 as FICA,
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.0145 as Medicare
      from edwClockHoursFact chf
      inner join edwEmployeeDim ed on ed.EmployeeKey = chf.EmployeeKey 
        and StoreCodeDesc = 'Rydell' 
        and PayrollClassCodeDesc = 'Hourly' 
        and DistCode not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
      inner join Day on Day.DateKey = chf.DateKey 
        and TheDate between '1/15/2012' and '1/31/2012'
      group by EmployeeNumber, Name, DistCode
        having sum(ClockHours) > 0
      union all
      select EmployeeNumber, Name, DistCode, 
        Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) as GrossPay,
        Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.062 as FICA,
        Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.0145 as Medicare
      from edwEmployeeDim 
      where StoreCodeDesc = 'Rydell' 
        and PayrollClassCodeDesc = 'Salary' 
        and DistCode not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
        AND employeekeyfromdate <= '01/31/2012' AND employeekeythrudate >= '01/15/2012' /* employed for the interval*/ 
        AND ActiveCodeDesc = 'Active') pay
    left join stgArkonaPYACTGR acct on acct.ytadic = pay.DistCode and acct.ytaco# = 'RY1' and ytadic not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94')) c) x
group by EmployeeNumber, Account
order by Account, EmployeeNumber  
