it has come to my attention (thru a report request FROM jeri) that there are 
some new service types that are NOT included IN dimServiceType

SELECT b.thedate, a.ro, a.line
-- SELECT COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE servicetypekey = 1
ORDER BY b.thedate




select * FROM stgArkonaSDPRDET WHERE trim(ptro#) = '16230462'
 
SELECT MAX(ptdate) FROM stgarkonasdprdet 

SELECT ro, line
FROM factrepairorder
WHERE servicetypekey = 1
ORDER BY ro desc

DROP TABLE z_fix_dim_service_type;
CREATE TABLE z_fix_dim_service_type (
  ro cichar(9),
  line integer,
  new_service_type_key cichar(2));
  
SELECT ro, line
FROM (  
  SELECT ro, line, servicetypekey
  FROM factrepairorder  
  GROUP BY ro, line, servicetypekey) x
GROUP BY ro, line
HAVING COUNT(*) > 1  

select *
FROM dimServiceType

SELECT *
FROM dds.z_fix_dim_service_type

SELECT new_service_type_key, COUNT(*)
FROM z_Fix_Dim_service_type
GROUP BY new_service_type_key

SELECT COUNT(*)
FROM dds.z_fix_dim_service_type

delete
FROM z_fix_dim_service_type

SELECT a.ro, a.line, b.*, c.*
FROM factrepairorder a
LEFT JOIN z_fix_dim_service_type b on a.ro = b.ro AND a.line = b.line
LEFT JOIN dimservicetype c on b.new_service_type_key = c.servicetypecode
WHERE a.servicetypekey = 1
  AND b.ro IS NOT NULL 

SELECT *
FROM dimservicetype

INSERT INTO dimservicetype(servicetypecode, servicetype)
values ('MN','Menu Priced Service');

UPDATE factrepairorder
SET servicetypekey = coalesce(x.servicetypekey, 1)
FROM (
  SELECT a.ro, a.line, c.servicetypekey
  FROM factrepairorder a
  LEFT JOIN z_fix_dim_service_type b on a.ro = b.ro AND a.line = b.line
  LEFT JOIN dimservicetype c on b.new_service_type_key = c.servicetypecode
  WHERE a.servicetypekey = 1
    AND b.ro IS NOT NULL) x
WHERE factrepairorder.servicetypekey = 1
  AND factrepairorder.ro = x.ro
  AND factrepairorder.line = x.line    
 

INSERT INTO z_fix_dim_service_type
SELECT * FROM dds.z_fix_dim_service_type ;

SELECT COUNT(*) FROM z_fix_dim_service_type


-- 8/3/17, QS has been added ----------------------------------------------------------------------
SELECT * FROM dimservicetype ORDER BY servicetypecode

SELECT min(ptdate), MAX(ptdate) FROM tmpsdprdet

SELECT ptsvctyp, COUNT(*)
FROM tmpsdprdet
WHERE ptdate > '05/31/2017'
  AND ptsvctyp IS NOT NULL
GROUP BY ptsvctyp  

-- since 6/20
SELECT MIN(ptdate) FROM tmpsdprdet WHERE ptsvctyp = 'QS'

INSERT INTO dimservicetype(servicetypecode, servicetype)
values ('QS','Quick Service');
-- new key IS 14

DROP TABLE z_fix_dim_service_type;
CREATE TABLE z_fix_dim_service_type (
  ro cichar(9),
  line integer,
  new_service_type_key integer);
INSERT INTO z_fix_dim_service_type  
SELECT ptro#, ptline, 14
FROM tmpsdprdet
WHERE ptsvctyp = 'QS';


SELECT ptdate, COUNT(*)
FROM tmpsdprdet
WHERE ptsvctyp = 'QS'
GROUP BY ptdate

SELECT b.thedate, a.ro, a.line
-- SELECT COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE servicetypekey = 1
ORDER BY b.thedate

UPDATE factrepairorder
SET servicetypekey = coalesce(x.servicetypekey, 1)
FROM (
  SELECT a.ro, a.line, c.servicetypekey
  -- SELECT COUNT(*)
  FROM factrepairorder a
  LEFT JOIN z_fix_dim_service_type b on a.ro = b.ro AND a.line = b.line
  LEFT JOIN dimservicetype c on b.new_service_type_key = c.servicetypekey
  WHERE a.servicetypekey = 1
    AND b.ro IS NOT NULL) x
WHERE factrepairorder.servicetypekey = 1
  AND factrepairorder.ro = x.ro
  AND factrepairorder.line = x.line   