--< DDL -----------------------------------------------------------------------
/*
12/1/13
  adding a new service type for team pay
  OT to include, Training, Shop AND Fencewalk hours
*/
-- TABLE
-- DROP TABLE dimServiceType;
CREATE TABLE dimServiceType (
  ServiceTypeKey autoinc,
  ServiceTypeCode cichar(2),
  ServiceType cichar(24))IN database;
  
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimServiceType','dimServiceType.adi','PK',
   'ServiceTypeKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimServiceType','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'dimServiceTypeObjectsfail');   
-- index: NK 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimServiceType','dimServiceType.adi','NK',
   'ServiceTypeCode', '',2051,512,'' );   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimServiceType','ServiceTypeCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimServiceType','ServiceType', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
--/> DDL -----------------------------------------------------------------------  
--< keyMap --------------------------------------------------------------------
--this IS a trivial, hand crafted dimension, no need for a keymap
--/> keyMap --------------------------------------------------------------------  
--< Dependencies & zProc ------------------------------------------------------
--no nightly updates, no dependencies outside the notion of an unknow dimension row
--/> Dependencies & zProc ------------------------------------------------------  

--< initial load --------------------------------------------------------------
-- unknown row
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType)
values ('UN','UNKNOWN');
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType) values ('AM','Aftermarket');
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType) values ('BS','Body Shop');
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType) values ('CM','Commercial');
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType) values ('CW','Carwash');
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType) values ('EM','Employee');
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType) values ('IC','Intercompany');
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType) values ('MR','Mechanical');
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType) values ('QL','Quick Lube');
INSERT INTO dimServiceType (ServiceTypeCode, ServiceType) values ('RE','Recon');
--/> initial load --------------------------------------------------------------

SELECT * FROM dimServiceType