2 dimensions: dimRoComments - ro level
              dimComplaintCauseCorrection - line level

/*

X     Please Note
A     Complaint
A.0   Line Status (PDPPDET)
Z     Correction
L     Cause
*/

DELETE FROM stgArkonaSDPRTXT WHERE sxco# NOT IN ('RY1','RY2');
SELECT sxco#, sxro#, sxline, sxltyp, COUNT(*)
FROM stgArkonaSDPRTXT
WHERE sxco# IN ('RY1','RY2')
GROUP BY sxco#, sxro#, sxline, sxltyp
ORDER BY COUNT(*) desc

-- need sequence
DROP TABLE stgPleaseNote;
CREATE TABLE stgPleaseNote (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  seq integer,
  Text memo) IN database;
INSERT INTO stgPleaseNote
SELECT sxco#, sxro#, sxline, sxseq#, sxtext
FROM stgArkonaSDPRTXT
WHERE sxltyp = 'X';  
-- indexes
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgPleaseNote','stgPleaseNote.adi','StoreCode','StoreCode',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgPleaseNote','stgPleaseNote.adi','RO','RO',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgPleaseNote','stgPleaseNote.adi','Line','Line',
   '',2,512,'' );       


-- the notion IS, DO NOT have to DO the CURSOR deal for one liners   
SELECT theCount, COUNT(*)
FROM (   
  SELECT ro,COUNT(*) AS theCount
  FROM stgPleaseNote 
  GROUP BY ro) x
GROUP BY theCount;  


DROP TABLE dimRoComment;
-- DELETE FROM dimRoComment;
CREATE TABLE dimRoComment (
  RoCommentKey autoinc,
  StoreCode cichar(3),
  RO cichar(9),
  CommentType cichar(24),
  Comment memo,
  CommentTS timestamp,
  CommentBy cichar(50)) IN database;
-- populate first, THEN ADD indexes AND shit
-- 1 row per RO
INSERT INTO dimRoComment (StoreCode, RO, CommentType, Comment)
SELECT a.StoreCode, a.RO, 'Please Note', Text
FROM stgPleaseNote a
INNER JOIN (
  SELECT storecode, ro
  FROM stgPleaseNote 
  GROUP BY storecode, ro
  HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro;

 
DECLARE @cur3All cursor AS 
  SELECT a.storecode, a.ro
  FROM stgPleaseNote a
  INNER JOIN (
    SELECT storecode, ro
    FROM stgPleaseNote 
    GROUP BY storecode, ro
    HAVING COUNT(*) > 1) b ON a.storecode = b.storecode AND a.ro = b.ro
  GROUP BY a.storecode, a.ro;   
DECLARE @cur3RO cursor AS
  SELECT a.storecode, a.ro, a.text
  FROM stgPleaseNote a
  WHERE a.storecode = @storecode
    AND a.ro = @ro
  ORDER BY seq;
DECLARE @string string;  
DECLARE @storecode string;
DECLARE @ro string;
  @string = '';  
  OPEN @cur3All;
  TRY
    WHILE FETCH @cur3All DO
      @storecode = @cur3ALL.storecode;
      @ro = @cur3All.ro;
      OPEN @cur3RO;
      TRY
        WHILE FETCH @cur3RO DO
          @string = TRIM(@string) + ' ' + TRIM(@cur3RO.text);
        END WHILE;
      FINALLY
        CLOSE @cur3RO;
      END TRY;
      TRY 
        INSERT INTO dimRoComment (StoreCode,Ro,CommentType,Comment)
        values (@storecode, @ro, 'Please Note', @string);
        @string = '';
      CATCH ALL 
        RAISE OMFG(100, @storecode + ' ' + @ro + ' ' + __errtext);
      END TRY;
    END WHILE;
  FINALLY
    CLOSE @cur3All;
  END TRY;  

-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimRoComment','dimRoComment.adi','PK',
   'RoCommentKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimRoComment','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'dimRoStatusObjectsfail');   
-- index: NK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimRoComment','dimRoComment.adi','NK',
   'StoreCode;RO','',2051,512,'' );  -- unique   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimRoComment','RO', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimRoComment','StoreCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimRoComment','CommentType', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimRoComment','Comment', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   

--< N/A row --------------------------------------------------------------------
-- 1 for each store
INSERT INTO dimRoComment (storecode,ro,commenttype,comment)
values ('RY1','N/A','N/A','N/A');
INSERT INTO dimRoComment (storecode,ro,commenttype,comment)
values ('RY2','N/A','N/A','N/A');
--/> N/A row --------------------------------------------------------------------   

  
--- keyMap --------------------------------------------------------------------

--DROP TABLE keyMapDimRoComment;     
CREATE TABLE keyMapDimRoComment (
  RoCommentKey integer,
  StoreCode cichar(3),
  RO cichar(9),
  CommentType cichar(24)) IN database;         
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keyMapDimRoComment','keyMapDimRoComment.adi','PK',
   'RoCommentKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('keyMapDimRoComment','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'keyMapDimRoCommentStatusfail');   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimRoComment','RoCommentKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimRoComment','RO', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimRoComment','StoreCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimRoComment','CommentType', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
--RI  
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimRoStatus-KeyMap');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimRoComment-KeyMap','dimRoComment','keyMapdimRoComment','PK',2,2,NULL,'','');   
      
INSERT INTO keyMapDimRoComment
SELECT RoCommentKey,StoreCode,RO,CommentType FROM dimRoComment;  

INSERT INTO keyMapDimRoComment
SELECT RoCommentKey,StoreCode,RO,CommentType FROM dimRoComment WHERE ro = 'N/A';  

--/ keyMap --------------------------------------------------------------------


--- Dependencies & zProc ------------------------------------------------------
INSERT INTO zProcExecutables values('extSDPRTXT','Daily',CAST('12:15:15' AS sql_time),0);
INSERT INTO zProcProcedures values('extSDPRTXT','tmpSDPRTXT',0,'tmpSDPRTXT');
INSERT INTO DependencyObjects values('extSDPRTXT','tmpSDPRTXT','tmpSDPRTXT');

INSERT INTO zProcExecutables values('dimRoComment','Daily',CAST('12:30:15' AS sql_time),0);
INSERT INTO zProcProcedures values('dimRoComment','dimRoComment',0,'daily');
INSERT INTO DependencyObjects values('dimRoComment','dimRoComment','dimRoComment');

INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimRoComment'
  AND b.dObject = 'tmpSDPRTXT';
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimRoComment'
  AND b.dObject = 'tmpPDPPHDR';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimRoComment'
  AND b.dObject = 'tmpPDPPDET';    
    
--/ Dependencies & zProc -----------------------------------------------------  