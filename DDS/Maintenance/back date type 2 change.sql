previous row
rowchangedate
rowchangedatekey
rowthruts - actual time that the row IS changed
employeekeythrudate = rowchangedate - 1
employeekeythrudatekey

current row
rowfromts (= previous row rowthruts)
empoyeekeyfromdate ( = rowchangedate)
employeekeyfromdatekey

emp# 1116800, dist code changed FROM CWAS to WTEC, 
changed ON 12/22/12, change date needs to be 10/31/12

prev: empkey = 995
current: empkey = 1855

rowchangedate = '10/31/2012'
DECLARE @prevEmpkey integer;
DECLARE @curEmpkey integer;
DECLARE @rcdate date;
DECLARE @rcdatekey integer;
DECLARE @rowthruts timestamp;
DECLARE @ekthrudate date;
DECLARE @ekthrudatekey integer;
DECLARE @ekfromdate date;
DECLARE @ekfromdatekey integer;
/* values requd) */
@prevEmpKey = 995;
@curEmpKey = 1855;
@rcdate = '10/31/2012';
/* values requd) */
@rcdatekey = (SELECT datekey FROM day WHERE thedate = @rcdate);
@rowthruts = timestampadd(sql_tsi_minute, 70, @rcdate); 
@ekthrudate = @rcdate - 1;
@ekthrudatekey = (SELECT datekey FROM day WHERE thedate = @ekthrudate); 
@ekfromdate = @rcdate;
@ekfromdatekey = @rcdatekey;
SELECT @rowthruts, @rcdate, @ekthrudate, @ekthrudatekey FROM system.iota;

UPDATE edwEmployeeDim
  SET rowchangedate = @rcdate,
      rowchangedatekey = @rcdatekey,
      rowthruts = @rowthruts,
      employeekeythrudate = @ekthrudate,
      employeekeythrudatekey = @ekthrudatekey
WHERE employeekey = @prevEmpKey;

UPDATE edwEmployeeDim
  SET rowfromts = @rowthruts,
      employeekeyfromdate = @ekfromdate,
      employeekeyfromdatekey = @ekfromdatekey
WHERE employeekey = @curEmpKey;


