-- name change that should be a correction

/*
6/7

RY1         1148010                Guleed Welli changed to Gulled Welli
RY1         122810                  Brain Covell changed to Brian Covell
RY1         193795                  Jacob D McGowan changed to Jacob D McCowan
RY2         255950                  Dustin J Grohs changed to Dustin J Sangrait

ALL are corrections except dustin, who actually changed his name

AND dustin tech 24 name IS NOT updated IN dimTech wtf

*/
SELECT storecode, employeekey, employeenumber, active, pydept, distribution, name, 
  technumber, employeekeyfromdate, currentrow, rowchangedate, rowchangereason
--SELECT *  
FROM edwEmployeeDim -- WHERE name LIKE 'mcname%'
WHERE employeenumber IN (
    SELECT employeenumber
    FROM edwEmployeeDim
    WHERE rowchangereason = 'name'
      AND employeenumber NOT IN ('16250', '268200', '255950'))
--  AND employeenumber = '193795'      
ORDER BY employeenumber, employeekeyfromdate   
-- what makes me gitchy IS, why can i DO an UPDATE ON the old empkey row AND NOT
-- throw an ri violation
-- That's because i have NOT YET defined a NK IN edwEmployeeDim 

    
/*   
-- need to remove the row added BY the type 2 change
-- AND clean up the row updated BY the type 2 change
-- at this time looks LIKE manual process
*/

-- 1. check for rows IN edwClockHoursFact that include the bogus emplkey
  orig empkey: 1611
  bogus empkey: 1621
--------------------------------------------------------------------------------
DECLARE @origkey integer;
DECLARE @boguskey integer;
DECLARE @name string;
@origkey = 1611;
@boguskey = 1621;  
@name = (SELECT name FROM edwEmployeeDim WHERE employeekey = @boguskey);

  -- the 2 employeekeys DO NOT have any datekey records IN common
IF (  
  SELECT COUNT(*)
  FROM (  
    SELECT datekey 
    FROM edwClockHoursFact 
    WHERE employeekey = @origkey) a
  INNER JOIN (
    SELECT datekey 
    FROM edwClockHoursFact 
    WHERE employeekey = @boguskey) b ON a.datekey = b.datekey) <> 0 THEN
  RAISE NameChange(100, 'Overlapping clockhour days');    
END IF;  

UPDATE edwClockHoursFact
SET employeekey = @origkey
WHERE employeekey = @boguskey;
-- 2.
-- depending ON god knows what, the list of columns to be updated may vary
UPDATE edwEmployeeDim
SET CurrentRow = True,
    RowChangeDate = NULL,
    RowChangeDateKey = NULL,
    RowThruTS = NULL,
    RowChangeReason = NULL, 
    EmployeeKeyThruDate = '12/31/9999',
    EmployeeKeyThruDateKey = 7036,
    Name = @name
WHERE employeekey = @origkey;
/*
-- multiple rows ON which the name correction needs to be applied
UPDATE edwEmployeeDim
SET name = 'WELLI,GULLED'
WHERE employeenumber = '1148010';
*/
DELETE 
FROM edwEmployeeDim
WHERE employeekey = @boguskey; 
--------------------------------------------------------------------------------

SELECT * FROM edwEmployeeDim WHERE employeenumber = '193795';

dimTech: NOT affected  
*/
/* matthew mcnamee */
-- 1. check for rows IN edwClockHoursFact that include the bogus emplkey
  orig empkey: 772
  bogus empkey: 1417
  employeenumber: 294810
  -- the 2 employeekeys DO NOT have any datekey records IN common
  SELECT datekey 
  FROM edwClockHoursFact 
  WHERE employeekey IN (772,1417)
  GROUP BY datekey 
  HAVING COUNT(*) > 1

UPDATE edwClockHoursFact
SET employeekey = 772 -- orig empkey
WHERE employeekey = 1417 -- bogus empkey  

-- 2.
UPDATE edwEmployeeDim
SET CurrentRow = True,
    RowChangeDate = NULL,
    RowChangeDateKey = NULL,
    RowThruTS = NULL,
    RowChangeReason = NULL, 
    EmployeeKeyThruDate = '12/31/9999',
    EmployeeKeyThruDateKey = 7306,
    Name = 'MCNAMEE, MATTHEW L'
WHERE employeekey = 772;

DELETE 
FROM edwEmployeeDim
WHERE employeekey = 1417;  

-- 3. dimTech
-- yep, has the old name
SELECT *
FROM dimtech
WHERE employeenumber = '294810'

UPDATE dimtech 
  SET name = (
    SELECT name
    FROM edwEmployeeDim
    WHERE employeekey = 772)
WHERE employeenumber = '294810'

SELECT *
FROM (
  SELECT storecode, name, employeenumber
  FROM dimTech 
  WHERE name IS NOT NULL) a 
LEFT JOIN (
    SELECT storecode, employeenumber, name
    FROM edwEmployeeDim
    WHERE currentrow = true
    GROUP BY storecode, employeenumber, name) b ON a.employeenumber = b.employeenumber
  AND a.storecode = b.storecode
  AND a.name <> b.name
WHERE b.employeenumber IS not NULL   

UPDATE dimtech
SET name = (
  SELECT name
  FROM edwEmployeeDim
  WHERE employeenumber = '28580'
    AND storecode = 'ry2') 
WHERE employeenumber = '28580'         

-- hmm, this one picks up kbf, but the one FROM the sp does NOT
-- because there IS no rowchangereason
SELECT storecode, employeenumber
FROM (
  SELECT storecode, employeenumber, name
  FROM edwEmployeeDim
  WHERE employeenumber NOT IN ('16250', '268200')
  GROUP BY storecode, employeenumber, name) a
GROUP BY storecode, employeenumber
HAVING COUNT(*) > 1
-- ok, only kevin
SELECT *
FROM edwEmployeeDim
WHERE rowchangedate IS NOT NULL
  AND rowchangereason IS NULL 
/* kevin foster */
  orig empkey: 1239
  bogus empkey: 1390
-- 1. check for rows IN edwClockHoursFact that include the bogus emplkey
-- the 2 employeekeys DO NOT have any datekey records IN common
he doesn''t punch the clock

-- 2.
UPDATE edwEmployeeDim
SET CurrentRow = True,
    RowChangeDate = NULL,
    RowChangeDateKey = NULL,
    RowThruTS = NULL,
    RowChangeReason = NULL, 
    EmployeeKeyThruDate = '12/31/9999',
    EmployeeKeyThruDateKey = 7306,
    Name = 'FOSTER,KEVIN B'
WHERE employeekey = 1239;  

DELETE 
FROM edwEmployeeDim
WHERE employeekey = 1390;

