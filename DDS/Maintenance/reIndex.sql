/*
4/1/12
  stumbled across the most recent inserts INTO stgArkonaGLPTRNS did NOT have the char fields trimmed
  they would NOT trim!
  reindexing the TABLE allowed me successfully TRIM
  
4/4/12
figured it out
ExtractArkonaGLPTRNS runs CountGLPTRNS, at the END of the month, previous month entries 
generate deleting a months worth of data FROM stgArkonaGLPTRNS AND reload that
month's data BUT WITHOUT FUCKING TRIMMING IT  
*/
EXECUTE PROCEDURE sp_reindex('stgArkonaGLPTRNS.adt', 512);

SELECT *
FROM stgArkonaGLPTRNS
WHERE gttrn# = 1863006

UPDATE stgArkonaGLPTRNS
SET gtdoc# = TRIM(gtdoc#)
WHERE gttrn# = 1863004

SELECT *
-- SELECT MAX(gtdate)
FROM stgArkonaGLPTRNS
WHERE gtctl# <> TRIM(gtctl#)

SELECT COUNT(*) FROM stgArkonaGLPTRNS

UPDATE stgArkonaGLPTRNS
SET gtacct = TRIM(gtacct),
    gtctl# = TRIM(gtctl#),
    gtdoc# = TRIM(gtdoc#),
    gtrdoc# = TRIM(gtrdoc#),
    gtref# = TRIM(gtref#)
WHERE gtctl# <> TRIM(gtctl#)    

-- 4/2 same fucking thing
EXECUTE PROCEDURE stgTrimCharacterFields('stgArkonaGLPTRNS');
-- took 1 1/2 hours

-- 4/3 same fucking thing
UPDATE stgArkonaGLPTRNS
SET gtacct = TRIM(gtacct),
    gtctl# = TRIM(gtctl#),
    gtdoc# = TRIM(gtdoc#),
    gtrdoc# = TRIM(gtrdoc#),
    gtref# = TRIM(gtref#)
WHERE gttrn# = 1830734    

SELECT * FROM stgArkonaGLPTRNS WHERE gttrn# = 1830734   

UPDATE stgArkonaGLPTRNS
SET gtacct = TRIM(gtacct),
    gtctl# = TRIM(gtctl#),
    gtdoc# = TRIM(gtdoc#),
    gtrdoc# = TRIM(gtrdoc#),
    gtref# = TRIM(gtref#)
WHERE gttrn# IN (
  SELECT gttrn#
  FROM stgArkonaGLPTRNS
  WHERE gtref# <> TRIM(gtref#))
  