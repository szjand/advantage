DROP TABLE tmp401k;
CREATE TABLE tmp401k (
  Store cichar(3),
  Name cichar(50),
  [Employee Number] cichar(12),
  [SSN] cichar(11),
  [Birth Date] date,
  [Hire Date] date,
  [Term Date] date,
  [Employee Type] cichar(1),
  [YTD Hours] integer,
  [Gross Compensation] double(2),
  [Roth Defferral] double(2),
  [Employer Match] double(2),
  [Profit Sharing] double(2),
  [Street Address] cichar(50),
  City cichar(25),
  State cichar(2),
  Zip integer) IN database;
  
  
  SELECT b.ssn, a.*
-- SELECT COUNT(*)  
  FROM tmp401k a
  LEFT JOIN tmpssn b on a.name = b.name
    AND a.[employee number] = b.employeenumber