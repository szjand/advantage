/*
CREATE TABLE zProcLog (
  executable cichar(100),
  proc cichar(100),
  startTS timestamp,
  endTS timestamp,
  error cichar (300)) IN database;  

CREATE TABLE zProcExecutables( 
  executable cichar(100),
  frequency cichar(24),
  scheduledTS time,
  sequence integer) IN database;  
  
CREATE TABLE zProcProcedures(
  executable cichar(100),
  proc cichar(100),
  sequence integer) IN database;
  
    
CREATE PROCEDURE zProcLogInsert (
  executable cichar(100),
  proc cichar(100),
  startTS timestamp)
  
BEGIN 

INSERT INTO zProcLog (executable, proc, startTS, endTS)
values(
  (SELECT executable FROM __input),
  (SELECT proc FROM __input),
  (SELECT startTS FROM __input),
  '9999-12-31 00:00:00');  
END;  
    
CREATE PROCEDURE zProcLogUpdate(
  executable cichar(100),
  proc cichar(100),
  endTS timestamp,
  error cichar(300))
  
BEGIN
UPDATE zProcLog
SET endTS = (SELECT endTS FROM __input),
    error = (SELECT error FROM __input)
WHERE executable = (SELECT executable FROM __input)
  AND proc = (SELECT proc FROM __input)
  AND CAST(startTS AS sql_date) = CAST((SELECT endTS FROM __input) AS sql_date);
END;      
*/
-- DELETE FROM zProcLog
SELECT LEFT(executable, 25), LEFT(proc,25), startTS, EndTS, error FROM zProcLog

SELECT LEFT(executable, 25), LEFT(proc,25), startTS, EndTs, timestampdiff(sql_tsi_frac_second, startts, endts) FROM zProcLog

SELECT *
FROM zProcLog
WHERE error IS NOT NULL 


INSERT INTO zProcExecutables values('EmployeeDim', 'daily', '01:45:00', 2);
INSERT INTO zProcExecutables values('stgZapAndReload', 'daily', '00:55:00', 1);
INSERT INTO zProcExecutables values('ExtractArkonaGLPTRNS', 'daily', '02:00:00', 3);
INSERT INTO zProcExecutables values('ServiceLine', 'daily', '02:15:00', 4);
INSERT INTO zProcProcedures values('EmployeeDim', 'PPCLKCTL', 1);
INSERT INTO zProcProcedures values('EmployeeDim', 'PYACTGR', 2);
INSERT INTO zProcProcedures values('EmployeeDim', 'PYMAST', 3);
INSERT INTO zProcProcedures values('EmployeeDim', 'xfmEmpDim', 4);
INSERT INTO zProcProcedures values('EmployeeDim', 'fctClockHours', 5);
INSERT INTO zProcProcedures values('stgZapAndReload', 'GLPOPYR', 1);
INSERT INTO zProcProcedures values('stgZapAndReload', 'FFPXREFDTA', 2);
INSERT INTO zProcProcedures values('stgZapAndReload', 'GLPMAST', 3);
INSERT INTO zProcProcedures values('stgZapAndReload', 'FSPagesLines', 4);
INSERT INTO zProcProcedures values('stgZapAndReload', 'GLPDEPT', 5);
INSERT INTO zProcProcedures values('stgZapAndReload', 'GLPPOHD', 6);
INSERT INTO zProcProcedures values('stgZapAndReload', 'PDPTHDR', 6);
INSERT INTO zProcProcedures values('stgZapAndReload', 'PDPPHDR', 7);
INSERT INTO zProcProcedures values('stgZapAndReload', 'INPMAST', 8);
INSERT INTO zProcProcedures values('stgZapAndReload', 'GLPCRHD', 9);
INSERT INTO zProcProcedures values('stgZapAndReload', 'GLPCRDT', 10);
INSERT INTO zProcProcedures values('stgZapAndReload', 'PYPCLOCKIN', 11);
INSERT INTO zProcProcedures values('stgZapAndReload', 'GLPJRND', 12);
INSERT INTO zProcProcedures values('stgZapAndReload', 'SDPTECH', 13);
INSERT INTO zProcProcedures values('stgZapAndReload', 'PYHSHDTA', 14);
INSERT INTO zProcProcedures values('stgZapAndReload', 'PYNCTRL', 15);
INSERT INTO zProcProcedures values('stgZapAndReload', 'PYDEDUCT', 16);
INSERT INTO zProcProcedures values('stgZapAndReload', 'PDPPDET', 17);
INSERT INTO zProcProcedures values('stgZapAndReload', 'SDPXTIM', 18);
INSERT INTO zProcProcedures values('stgZapAndReload', 'GLPCUST', 19);
INSERT INTO zProcProcedures values('stgZapAndReload', 'BOPMAST', 20);
INSERT INTO zProcProcedures values('stgZapAndReload', 'BOPSLSS', 21);
INSERT INTO zProcProcedures values('stgZapAndReload', 'PYPCODES', 22);
INSERT INTO zProcProcedures values('ExtractArkonaGLPTRNS', 'ExtractGlpdtimNightly', 1);
INSERT INTO zProcProcedures values('ExtractArkonaGLPTRNS', 'CountGLPTRNS', 2);
INSERT INTO zProcProcedures values('ServiceLine', 'SDPRHDR', 1);
INSERT INTO zProcProcedures values('ServiceLine', 'SDPRDET', 2);


/* 5/29/12 */
-- wtf IN db2 NOT IN stgArkonaGLPTRNS
SELECT * FROM stgArkonaGLPTRNS WHERE gtdoc# = '16087736'
SELECT DISTINCT gttrn#
FROM stgArkonaGLPTRNS
WHERE gtdate BETWEEN '05/01/2012' AND '05/15/2012'
ORDER BY gttrn#

DROP TABLE ztrns;
CREATE TABLE ztrns (
trns integer) IN database;

SELECT * FROM ztrns

SELECT MIN(trns), MAX(trns) FROM ztrns z
WHERE NOT EXISTS (
  SELECT 1
  FROM stgArkonaGLPTRNS
  WHERE gttrn# = z.trns)
  
  


ALTER TABLE zProcProcedures
ADD COLUMN frequency cichar(24);

UPDATE zProcProcedures
SET frequency = 'Daily';

INSERT INTO zProcProcedures
values('ExtractArkonaGLPTRNS','SingleDate', 999, 'Manual')

-- 6/2
SELECT * FROM zproclog

SELECT executable, proc, MIN(minutes)
FROM (
SELECT executable, proc, timestampdiff(sql_tsi_minute, startts, endts) AS Minutes
-- INTO #wtf
FROM zproclog
WHERE proc <> 'none') a
GROUP BY executable, proc

SELECT executable, proc, min(timestampdiff(sql_tsi_minute, startts, endts)),
  max(timestampdiff(sql_tsi_minute, startts, endts)), 
  avg(timestampdiff(sql_tsi_minute, startts, endts)),
  COUNT(*)   
-- INTO #wtf
-- SELECT *
FROM zproclog
WHERE proc = 'none'
  AND error IS NULL 
  AND endts < now()
  AND CAST(startts AS sql_date) between curdate() - 31 AND curdate() 
GROUP BY executable, proc

SELECT executable, proc, cast(startts as sql_date), timestampdiff(sql_tsi_minute, startts, endts)
 
-- INTO #wtf
-- SELECT *
FROM zproclog
WHERE proc = 'none'
  AND error IS NULL 
  AND endts < now()
  AND CAST(startts AS sql_date) between curdate() - 31 AND curdate() 
ORDER BY executable, startts


-- 6/6/12
-- this IS it: what ran compared to what should have run AND what failed
--6/12 changing error to memo fucked this up
SELECT d.thedate, a.executable, b.proc, b.sequence 
--  CASE 
--    WHEN cast(c.error AS sql_char) <> '' THEN 'Error: ' + trim(a.executable) + '-' + trim(b.proc) + LEFT(cast(error AS sql_char), 25)
--    WHEN c.proc IS NULL THEN 'Not Run: ' + trim(a.executable) + '-' + trim(b.proc)
--  END AS problema 
FROM day d
LEFT JOIN zprocexecutables a ON 1 = 1
LEFT JOIN zprocprocedures b ON a.executable = b.executable
LEFT JOIN zproclog c ON a.executable = c.executable
  AND b.proc = c.proc
  AND CAST(c.startts AS sql_date) = d.thedate
WHERE d.thedate = curdate() 
  AND a.frequency = 'daily'
  AND b.frequency = 'daily'
  AND (c.proc IS NULL OR cast(c.error AS sql_Char) <> '');

-- procs that are run that don't EXISTS IN zprocProcedures
SELECT d.thedate, a.executable, a.proc
FROM day d
LEFT JOIN zproclog a ON d.thedate = CAST(a.startts AS sql_date) 
LEFT JOIN zprocProcedures b ON a.executable = b.executable
  AND a.proc = b.proc
WHERE d.thedate = curdate()
  AND a.proc <> 'none'
  AND b.proc IS NULL 
  AND a.executable <> 'hourly' 
  
-- never finished  
SELECT executable, proc
FROM zproclog
WHERE CAST(startts AS sql_date) = curdate()
  AND year(endts) = 9999  
  
-- 7/21
SELECT a.sequence, left(a.executable, 25), b.proc
FROM zprocexecutables a
LEFT JOIN zprocprocedures b ON a.executable = b.executable
  AND b.frequency = 'daily'
ORDER BY a.sequence , b.sequence 

-- 8/2 speed up FROM move to vm
SELECT CAST(a.startts AS sql_Date), left(a.executable, 25) AS exec, 
  left(a.proc,26) AS proc, sum(timestampdiff(sql_tsi_minute, a.startts, a.endts))
FROM zproclog a
INNER JOIN zprocexecutables b ON a.executable = b.executable
WHERE CAST(a.startts AS sql_Date)> '06/20/2012'
  AND b.frequency = 'daily'
  AND a.proc = 'none'
GROUP BY CAST(a.startts AS sql_Date), a.executable, a.proc
ORDER BY CAST(a.startts AS sql_Date) desc

-- 9/7/12
-- sequential dependencies
-- missing procs

SELECT DISTINCT executable, proc
FROM zproclog a
WHERE NOT EXISTS (
  SELECT 1
  FROM zprocprocedures
  WHERE executable = a.executable
    AND proc = a.proc)
  and executable <> 'hourly' 
  AND proc <> 'none' 
  
edwEmployeeDim:fctClockMinutesByHour
ServiceLine: rptTables 

plus ALL the stgRydellService stuff

so i need a fool proof means to maintain this stuff

UPDATE zprocProcedures
SET sequence = 7
WHERE executable = 'EmployeeDim'
  AND proc = 'maintDimEmployee';
INSERT INTO zprocProcedures values('EmployeeDim','fctClockMinutesByHour', 6, 'daily') 

UPDATE zprocProcedures
SET sequence = 9
WHERE executable = 'ServiceLine'
  AND proc = 'maintDimTech';
INSERT INTO zprocProcedures values('ServiceLine','rptTables', 8, 'daily') 

include with the nightly ri maint?  -- those ri constraints that can't be checked with ri

-- 10/8
-- ADD a proc to employeedim
1.

UPDATE zprocProcedures
SET sequence = sequence + 1
WHERE executable = 'EmployeeDim'
  AND sequence > 3;
INSERT INTO zprocProcedures values('EmployeeDim','PYPRHEAD', 4, 'daily') ;  

-- 2/10/14
INSERT INTO zProcExecutables values (
  'dimSalesPerson', 'daily','00:15:23',0);
INSERT INTO zProcProcedures values(
  'dimSalesPerson', 'BOPSLSS', 1, 'daily');   
INSERT INTO dependencyObjects values(
  'dimSalesPerson', 'BOPSLSS', 'dimSalesPerson');
  
-- 4/8/14
ADD a proc to exe dimCustomer for the email updates

INSERT INTO zProcProcedures values ('dimCustomer','emailUpdate',0,'Daily');
INSERT INTO dependencyObjects values(
  'dimCustomer', 'emailUpdate', 'emailUpdate');

