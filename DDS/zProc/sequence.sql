SELECT left(a.executable, 20), a.sequence * 1000 + b.sequence AS Seq, left(b.proc, 25)
FROM zprocexecutables a
LEFT JOIN zprocprocedures b ON a.executable = b.executable
  AND b.frequency = 'Daily'
ORDER BY a.sequence * 1000 + b.sequence


SELECT left(a.executable, 20), a.sequence * 1000 + b.sequence AS Seq, left(b.proc, 25)
FROM zprocexecutables a
LEFT JOIN zprocprocedures b ON a.executable = b.executable
  AND b.frequency = 'Daily'
ORDER BY a.sequence, b.proc

