SELECT c.thedate, a.ro, b.make, b.model, d.servicetypecode
INTO #wtf
FROM factRepairORder a
INNER JOIN dimVehicle b on a.vehiclekey = b.vehiclekey
INNER JOIN day c on a.finalclosedatekey = c.datekey
INNER JOIN dimServiceType d on a.servicetypekey = d.servicetypekey
WHERE storecode = 'ry2'
  AND b.make = 'honda'
  AND c.theyear = 2013
GROUP BY c.thedate, a.ro, b.make, b.model, d.servicetypecode  
  

SELECT DISTINCT servicetypecode FROM #wtf
  
SELECT b.yearmonth, 
  SUM(CASE WHEN a.servicetypecode = 'AM' THEN 1 ELSE 0 END) AS AM,
  SUM(CASE WHEN a.servicetypecode = 'CW' THEN 1 ELSE 0 END) AS CW,
  SUM(CASE WHEN a.servicetypecode = 'EM' THEN 1 ELSE 0 END) AS EM,
  SUM(CASE WHEN a.servicetypecode = 'MR' THEN 1 ELSE 0 END) AS MR,
  SUM(CASE WHEN a.servicetypecode = 'QL' THEN 1 ELSE 0 END) AS QL
FROM #wtf a
INNER JOIN day b on a.thedate = b.thedate 
group by b.yearmonth
  
DROP TABLE #wtf;  
SELECT c.thedate, a.ro, b.make
INTO #wtf
FROM factRepairORder a
INNER JOIN dimVehicle b on a.vehiclekey = b.vehiclekey
INNER JOIN day c on a.finalclosedatekey = c.datekey
INNER JOIN dimServiceType d on a.servicetypekey = d.servicetypekey
WHERE storecode = 'ry2'
  AND b.make = 'honda'
  AND c.yearmonth BETWEEN 201301 AND 201311
  AND d.servicetypecode IN ('MR','QL')
GROUP BY c.thedate, a.ro, b.make;

SELECT cast(yearmonth AS sql_char) AS Month, COUNT(*)
FROM #wtf a
INNER JOIN day b on a.thedate = b.thedate
GROUP BY yearmonth
UNION
SELECT 'Total', COUNT(*)
FROM #wtf a
INNER JOIN day b on a.thedate = b.thedate

SELECT thedate, ro, make
FROM factRepairORder a
INNER JOIN dimVehicle b on a.vehiclekey = b.vehiclekey
INNER JOIN day c on a.finalclosedatekey = c.datekey
INNER JOIN dimServiceType d on a.servicetypekey = d.servicetypekey
WHERE EXISTS (
  SELECT 1
  FROM factrepairorder
  WHERE ro = a.ro
    AND servicetypekey = (
      SELECT servicetypekey
      FROM dimServiceType
      WHERE servicetypecode = 'MR'))
AND EXISTS (  
  SELECT 1
  FROM factrepairorder
  WHERE ro = a.ro
    AND servicetypekey = (
      SELECT servicetypekey
      FROM dimServiceType
      WHERE servicetypecode = 'QL'))    
AND storecode = 'ry2'
  AND b.make = 'honda'
  AND c.yearmonth BETWEEN 201301 AND 201311
  AND d.servicetypecode IN ('MR','QL')
  
  
SELECT customerkey, COUNT(*)
FROM factRepairorder a
INNER JOIN dimVehicle b on a.vehiclekey = b.vehiclekey
INNER JOIN day c on a.finalclosedatekey = c.datekey
INNER JOIN dimServiceType d on a.servicetypekey = d.servicetypekey
WHERE customerkey IN (2,157753,166504)  
AND storecode = 'ry2'
  AND b.make = 'honda'
  AND c.yearmonth BETWEEN 201301 AND 201311
  AND d.servicetypecode IN ('MR','QL')
GROUP BY customerkey  



