
-- alledgedly
-- shop IS 100% for the month of June
-- with 2 techs over 100  
-- turns out to be true, IF dustin AND joe are LEFT out
-- ry2 June
-- store clockhours : 1240.85
SELECT round(sum(b.clockhours), 2) AS clock
FROM day a
INNER JOIN edwClockHoursFact b ON a.datekey = b.datekey
INNER JOIN (
  SELECT a.employeekey, a.name, b.technumber
  FROM edwEmployeeDim a
  LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
  WHERE a.storecode = 'ry2'
    AND a.distcode = 'TECH' 
    AND b.storecode IS NOT NULL) t ON b.employeekey = t.employeekey
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012';
-- store flag : 1070.3
SELECT sum(b.flaghours) AS flag
FROM day a
INNER JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey 
INNER JOIN (
  SELECT a.employeekey
  FROM edwEmployeeDim a
  LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
  WHERE a.storecode = 'ry2'
    AND a.distcode = 'TECH' 
    AND b.storecode IS NOT NULL) t ON b.employeekey = t.employeekey
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012';

-- store clockhours BY day
SELECT x.thedate, coalesce(y.clock, 0) AS clock, coalesce(z.flag, 0) AS flag
FROM day x
LEFT JOIN (
  SELECT a.thedate, round(sum(b.clockhours), 2) AS clock
  FROM day a
  INNER JOIN edwClockHoursFact b ON a.datekey = b.datekey
  INNER JOIN (
    SELECT a.employeekey, a.name, b.technumber
    FROM edwEmployeeDim a
    LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
    WHERE a.storecode = 'ry2'
      AND a.distcode = 'TECH' 
      AND b.storecode IS NOT NULL) t ON b.employeekey = t.employeekey
  WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
  GROUP BY a.thedate) y ON x.thedate = y.thedate
LEFT JOIN (
  SELECT a.thedate, sum(b.flaghours) AS flag
  FROM day a
  INNER JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey 
  INNER JOIN (
    SELECT a.employeekey
    FROM edwEmployeeDim a
    LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
    WHERE a.storecode = 'ry2'
      AND a.distcode = 'TECH' 
      AND b.storecode IS NOT NULL) t ON b.employeekey = t.employeekey
  WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
  GROUP BY a.thedate) z ON x.thedate = z.thedate
WHERE x.thedate BETWEEN '06/01/2012' AND '06/30/2012'  


-- tech flag BY tech by day the month
SELECT a.thedate, t.name, t.technumber, sum(b.flaghours) AS flag
FROM day a
INNER JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey 
INNER JOIN (
  SELECT a.employeekey, a.name, b.technumber
  FROM edwEmployeeDim a
  LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
  WHERE a.storecode = 'ry2'
    AND a.distcode = 'TECH' 
    AND b.storecode IS NOT NULL) t ON b.employeekey = t.employeekey
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
GROUP BY a.thedate, t.name, t.technumber
-- tech clock
SELECT a.thedate, t.name, t.technumber, round(sum(b.clockhours), 2) AS clock
FROM day a
INNER JOIN edwClockHoursFact b ON a.datekey = b.datekey
INNER JOIN (
  SELECT a.employeekey, a.name, b.technumber
  FROM edwEmployeeDim a
  LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
  WHERE a.storecode = 'ry2'
    AND a.distcode = 'TECH' 
    AND b.storecode IS NOT NULL) t ON b.employeekey = t.employeekey
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
GROUP BY a.thedate, t.name, t.technumber

-- flag: month BY tech
SELECT max(t.name) AS name, t.technumber, sum(b.flaghours) AS flag
FROM day a
INNER JOIN factTechFlagHoursByDay b ON a.datekey = b.flagdatekey 
INNER JOIN (
  SELECT a.employeekey, a.name, b.technumber
  FROM edwEmployeeDim a
  LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
  WHERE a.storecode = 'ry2'
    AND a.distcode = 'TECH' 
    AND b.storecode IS NOT NULL) t ON b.employeekey = t.employeekey
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
GROUP BY t.technumber
-- clock: month BY tech
SELECT max(t.name) AS name, t.technumber, round(sum(b.clockhours), 2) AS clock
FROM day a
INNER JOIN edwClockHoursFact b ON a.datekey = b.datekey
INNER JOIN (
  SELECT a.employeekey, a.name, b.technumber
  FROM edwEmployeeDim a
  LEFT JOIN dimTech b ON a.employeenumber = b.employeenumber
  WHERE a.storecode = 'ry2'
    AND a.distcode = 'TECH' 
    AND b.storecode IS NOT NULL) t ON b.employeekey = t.employeekey
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
GROUP BY t.technumber