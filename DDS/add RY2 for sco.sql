SELECT pttech, MIN(ptdate), MAX(ptdate)
FROM stgArkonasdprdet
WHERE LEFT(pttech, 1) IN ('m','h')
GROUP BY pttech


SELECT *
FROM stgarkonasdprdet
WHERE pttech IN ('m4','m5')
  AND ptdate > '01/01/2012'
  
SELECT * FROM factro

 
SELECT  a.writerid, MIN(b.thedate), MAX(thedate), COUNT(*)
FROM factro a
LEFT JOIN day b ON a.opendatekey = b.datekey
WHERE a.storecode = 'ry2'
  AND EXISTS (
    SELECT 1
    FROM factroline
    WHERE ro = a.ro
      AND servicetype = 'mr')
GROUP BY writerid
ORDER BY MAX(thedate) DESC 

SELECT * FROM dimservicewriter WHERE name LIKE '%andrew%'

SELECT c.*, d.*, e.swactive
FROM (
  SELECT  a.storecode, a.writerid, MIN(b.thedate), MAX(thedate) AS maxDate, COUNT(*)
  FROM factro a
  LEFT JOIN day b ON a.opendatekey = b.datekey
  WHERE a.storecode = 'ry2'
    AND EXISTS (
      SELECT 1
      FROM factroline
      WHERE ro = a.ro
        AND servicetype = 'mr')
  GROUP BY a.storecode, a.writerid) c
LEFT JOIN dimservicewriter d ON c.writerid = d.writernumber  
  AND c.storecode = d.storecode
LEFT JOIN stgArkonaSDPSWTR e ON d.writernumber = e.swswid
  AND d.storecode = e.swco#
ORDER BY maxDate DESC 

-- wtf with ned AND scott writing main shop service at ry2

SELECT b.thedate, a.* 
FROM factro a
LEFT JOIN day b ON a.opendatekey = b.datekey
WHERE storecode = 'ry2'
  AND writerid IN ('644','728')
  AND EXISTS (
    SELECT 1
    FROM factroline
    WHERE ro = a.ro
      AND servicetype = 'mr')
ORDER BY b.thedate DESC       

SELECT * FROM factroline WHERE ro = '2679345'

SELECT * FROM stgarkonasdpswtr

-- redo it using stgArkonaSDPSWTR
-- forget the notion of ro servicetype
--
SELECT *
FROM (
  SELECT a.storecode, a.writerid, MIN(b.thedate), MAX(b.thedate) AS maxDate, COUNT(*)
  FROM factro a
  INNER JOIN day b ON a.opendatekey = b.datekey
  WHERE a.storecode = 'ry2'  
    AND a.void = false
  GROUP BY a.storecode, a.writerid) c
INNER JOIN stgArkonaSDPSWTR d ON c.storecode = d.swco#
  AND c.writerid = d.swswid 
  AND d.swmisc = 'mr'
ORDER BY maxdate DESC    
 
SELECT c.*, d.*, e.swactive
FROM (
  SELECT  a.storecode, a.writerid, MIN(b.thedate), MAX(thedate) AS maxDate, COUNT(*)
  FROM factro a
  LEFT JOIN day b ON a.opendatekey = b.datekey
  WHERE a.storecode = 'ry2'
    AND EXISTS (
      SELECT 1
      FROM factroline
      WHERE ro = a.ro
        AND servicetype = 'mr')
  GROUP BY a.storecode, a.writerid) c
LEFT JOIN dimservicewriter d ON c.writerid = d.writernumber  
  AND c.storecode = d.storecode
LEFT JOIN stgArkonaSDPSWTR e ON d.writernumber = e.swswid
  AND d.storecode = e.swco#
ORDER BY maxDate DESC 

-- changed Ned's default serv type RY2 to QL 
-- Aubrey: no RE serv type at RY2, ask Jeri