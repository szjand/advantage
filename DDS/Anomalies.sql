SELECT LEFT(executable, 25) AS exec, LEFT(proc,25) AS proc, 
  startts, endts, error//, timestampdiff(sql_tsi_second, startts, endts)/60.0
FROM zproclog
WHERE executable NOT IN ('hourly','SchedulerDetail','todayFactRepairOrder','stgRydellService')
   AND proc <> 'none'
-- AND proc IN ('inpmast','bopmast')   
--  AND proc = 'dimOpCode'
--  AND executable = 'dimOpCode'
--  AND executable = 'dimVehicle' AND proc = 'none' AND error IS NULL AND hour(startts) BETWEEN 0 AND 5
  AND CAST(startts AS sql_date) > curdate() -1
--  AND CAST(startts AS sql_date) = curdate()
ORDER BY startts DESC, exec, proc


SELECT LEFT(executable, 25) AS exec, LEFT(proc,25) AS proc, 
  startts, endts, error, timestampdiff(sql_tsi_second, startts, endts)/60.0
FROM zproclog
WHERE executable NOT IN ('hourly','SchedulerDetail','todayFactRepairOrder','stgRydellService')
   AND proc <> 'none'
   AND CAST(startts AS sql_date) = curdate() -1
ORDER BY exec, proc   
   
SELECT *
FROM tmpMaintDimEmployee
ORDER BY rundate DESC 

  select 'May' as month,
  (select last_name from scpp.sales_consultants where employee_number = a.employee_) as last_name,
  (select first_name from scpp.sales_consultants where employee_number = a.employee_),
  a.employee_, 
  sum(case when check_month = 5  then a.total_gross_pay::integer else 0 end) as total_gross_pay
  from dds.ext_pyhshdta a
  where trim(a.distrib_code) = 'SALE'
    and a.payroll_cen_year = 116
    and a.employee_ in ('123425','123600','124120','128530','133017','137220','145840','148080',
      '150040','163700','161325','167600','171150','181665','189100','1106225','1106223','1109815',
      '1112410','1130690','1132420','1135518','1141642','1147250','1151450')
    and a.company_number = 'RY1'
  group by a.employee_
SELECT * FROM zproclog WHERE proc = 'pyprhead' ORDER BY startts desc

SELECT * FROM nightlybatch ORDER BY thedate desc

SELECT LEFT(executable, 25) AS exec, LEFT(proc,25) AS proc, 
  startts, endts, error
FROM zproclog
WHERE  proc = 'bopmast' ORDER BY startts DESC 
  AND CAST(startts AS sql_date) > curdate() -7

SELECT LEFT(executable, 25) AS exec, LEFT(proc,25) AS proc, 
  startts, endts, error
FROM zproclog
WHERE proc = 'bopmast'  
ORDER BY startts DESC

SELECT LEFT(executable, 25) AS exec, LEFT(proc,25) AS proc,
  startts, endts, error,
  timestampdiff(sql_tsi_second, startts, endts)/60.0
FROM zproclog
WHERE executable = 'serviceline'
  AND proc = 'none'
--AND CAST(startts AS sql_date) IN ('03/27/2013','03/28/2013')
ORDER BY startts DESC


SELECT LEFT(a.executable, 25) AS exec, LEFT(a.proc,25) AS proc, 
  a.startts, a.endts,
  timestampdiff(sql_tsi_second, a.startts, a.endts)/60.0,
  b.startts, b.endts,
  CASE 
    WHEN CAST(b.endts AS sql_date) = curdate() THEN 
      timestampdiff(sql_tsi_second, b.startts, b.endts)/60.0
  END 
FROM zproclog a
LEFT JOIN zproclog b ON a.executable = b.executable
  AND a.proc = b.proc
  AND CAST(b.startts AS sql_date) = curdate()
WHERE a.executable = 'stgZapAndReload'
--  AND proc = 'none'
  AND CAST(a.startts AS sql_date) = '04/07/2013'
  AND b.startts > cast('04/13/2013 10:08:33' AS sql_timestamp)
ORDER BY a.startts DESC



SELECT LEFT(executable, 25), LEFT(proc,25) AS proc,
  min(timestampdiff(sql_tsi_second, startts, endts)/60.0),
  max(timestampdiff(sql_tsi_second, startts, endts)/60.0),
  avg(timestampdiff(sql_tsi_second, startts, endts)/60.0)
FROM zproclog
WHERE executable <> 'hourly'
  AND CAST(endts AS sql_date) <> '12/31/9999'
  AND error IS NULL 
GROUP BY LEFT(executable, 25), LEFT(proc,25)

--- overlap
SELECT a.*, c.name, c.managername
FROM tmpPypCLOCKIN a
LEFT JOIN (
  SELECT yico#, yiemp#, yiclkind, min(yiclkint) AS minIn, MAX(yiclkint) AS maxIN,
    MIN(yiclkoutt) AS minOut, MAX(yiclkoutt) AS maxOut
  FROM tmpPYPCLOCKIN  
  WHERE yico# IN ('ry1','ry2')
    AND yiclkind NOT IN ('09/02/2013', '11/28/2013', '12/25/2013', '12/26/2013', 
      '01/01/2014', '05/26/2014', '09/07/2015', '11/30/2015','01/01/2019',
      '01/01/2020','01/07/2020', '05/27/2019','09/02/2019','02/11/2020',
	  '03/09/2020','05/25/2020')--, '09/07/2020')
	and NOT (yiclkind = '03/12/2021' and yiemp# = '152499')
	and NOT (yiclkind = '03/01/2021' and yiemp# = '264110')  -- ry2 waiting for nick
	and NOT (yiclkind = '03/16/2021' and yiemp# = '152499')  -- conner pto overla
	and NOT (yiclkind = '04/06/2021' and yiemp# = '150126')  -- small pto overlap	
	and NOT (yiclkind = '04/16/2021' and yiemp# = '1117960')  -- rogne, salaried, doesn't matter
	AND NOT yiclkind  = '05/31/2021' -- memorial day, sales was OPEN
	AND NOT (yiclkind IN ('07/30/2021','09/23/2021') AND yiemp# = '111210')
	and not (yiclkind = '08/06/2021' and yiemp# = '154976')  -- bina, 7 min pto overlap
	and not (yiclkind = '10/04/2021' and yiemp# = '1149710')  -- woinarowicz,  pto overlap
  GROUP BY yico#, yiemp#, yiclkind
  HAVING COUNT(*) > 1
    AND MAX(yiclkint) < MIN(yiclkoutt)) b ON a.yico# = b.yico#
  AND a.yiemp# = b.yiemp#
  AND a.yiclkind = b.yiclkind
LEFT JOIN (
  SELECT employeenumber, name, managername 
  FROM edwEmployeeDim  
  WHERE currentrow = true
  GROUP BY employeenumber, name, managername) c ON a.yiemp# = c.employeenumber
WHERE b.yico# IS NOT NULL 
ORDER BY a.yiemp#, a.yiclkind, a.yiclkint;

230601
SELECT * FROM stgArkonaPYPCLKL WHERE ylemp# = '230601' AND ylclkind = '02/08/2013' ORDER BY ylclkind 

SELECT * FROM stgarkonapypclkl WHERE ylemp# = '128120' order by ylclkind desc AND ylclkind = '02/26/2013'


    
SELECT *
from tmpPypCLOCKIN
WHERE yiemp# = '1127110'  
  AND yiclkind = '08/03/2012'
  
SELECT *
from tmpPypCLOCKIN
WHERE yiemp# = '17560'  
  AND yiclkind = '08/09/2012'  
  
CREATE TABLE anomaliestmpPYPCLOCKIN ( 
      [yico#] CIChar( 3 ),
      [yiemp#] CIChar( 7 ),
      yiclkind Date,
      yiclkint Time,
      yiclkoutd Date,
      yiclkoutt Time,
      yicode CIChar( 3 ),
      dateDetected date) IN DATABASE;

INSERT INTO anomaliestmpPYPCLOCKIN      
SELECT a.*, curdate()
from tmpPypCLOCKIN a
WHERE yiemp# = '17560'  
  AND yiclkind = '08/09/2012'      
  
-- overlap  
SELECT *
    FROM tmpPypCLOCKIN a
    LEFT JOIN (
      SELECT yico#, yiemp#, yiclkind, min(yiclkint) AS minIn, MAX(yiclkint) AS maxIN,
        MIN(yiclkoutt) AS minOut, MAX(yiclkoutt) AS maxOut
      FROM tmpPYPCLOCKIN  
      GROUP BY yico#, yiemp#, yiclkind
      HAVING COUNT(*) > 1
        AND MAX(yiclkint) < MIN(yiclkoutt)) b ON a.yico# = b.yico#
      AND a.yiemp# = b.yiemp#
      AND a.yiclkind = b.yiclkind
    WHERE b.yico# IS NOT NULL 
ORDER BY a.yiemp#, a.yiclkind   

SELECT a.*, c.name, c.managername
FROM tmpPypCLOCKIN a
LEFT JOIN (
  SELECT yico#, yiemp#, yiclkind, min(yiclkint) AS minIn, MAX(yiclkint) AS maxIN,
    MIN(yiclkoutt) AS minOut, MAX(yiclkoutt) AS maxOut
  FROM tmpPYPCLOCKIN  
  GROUP BY yico#, yiemp#, yiclkind
  HAVING COUNT(*) > 1
    AND MAX(yiclkint) < MIN(yiclkoutt)) b ON a.yico# = b.yico#
  AND a.yiemp# = b.yiemp#
  AND a.yiclkind = b.yiclkind
LEFT JOIN (
  SELECT employeenumber, name, managername 
  FROM edwEmployeeDim  
  GROUP BY employeenumber, name, managername) c ON a.yiemp# = c.employeenumber
WHERE b.yico# IS NOT NULL 
ORDER BY a.yiemp#, a.yiclkind  
    
SELECT * FROM vempdim WHERE employeenumber = '230601'    

--RehireReuse 
-- a rehire/reuse IS excluded FROM the nightly etl UPDATE, but flagged IN
-- maint email, run scripts\RehireReuseManualUpdate.sql
      SELECT edwED.EmployeeKey, x.*	
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE edwED.ActiveCode = 'T' 
        AND x.ActiveCode IN ('P','A')
		
 
-- 2/4/13
-- kim did a bunch of changing shit around (like reactivating termed employees) to accomodate her year END needs
-- don't think she has restored everything that needs to be done    
-- emailed her with the info   
-- turns out the ry1 folks are rehires
SELECT a.storecode, a.employeekey, a.employeenumber, a.name, a.active, a.hiredate, 
  a.termdate, a.currentrow, a.rowchangedate, a.rowchangereason, 
  b.hiredate, b.termdate
FROM edwEmployeeDim a
INNER JOIN (
  SELECT edwED.EmployeeKey, x.*	
  FROM xfmEmployeeDim x
  LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
    AND x.employeeNumber = edwED.EmployeeNumber
    AND edwED.CurrentRow = true
  WHERE edwED.ActiveCode = 'T' 
    AND x.ActiveCode IN ('P','A')) b ON a.storecode = b.storecode AND a.employeenumber = b.employeenumber    
ORDER BY a.employeenumber, a.employeekey    

SELECT *
FROM xfmemployeedim
WHERE storecode = 'RY3'
  AND active = 'active'
ORDER BY name

SELECT * FROM edwEmployeeDim WHERE employeenumber = '34044'


-- employeenumber --------------------------------------------------------------
    SELECT COUNT(*)
    -- SELECT *
    FROM edwEmployeeDim
    WHERE name IN (
      SELECT name
      FROM edwEmployeeDim
      WHERE currentrow = true
      GROUP BY storecode, name 
      HAVING COUNT(*) > 1)
      
      SELECT COUNT(*)
      FROM (
        SELECT EmployeeNumber, StoreCode
        FROM edwEmployeeDim
        WHERE CurrentRow = true
        GROUP BY EmployeeNumber, StoreCode
          HAVING COUNT(*) > 1)x      
      
6/20/13 cory stinar IS a rehire, old emp# was 132590, new emp# IS 1132590, hmmm
so this becomes a situation WHERE the same person IS a different employee (emp#)
at different points IN time
AND each row IS current
IS that ok?
for now, i guess so, exclude him FROM the test IN maintDimEmployee

SELECT * FROM vempdim WHERE name = 'STINAR,CORY'

SELECT DISTINCT employeenumber, length(employeenumber)
FROM edwEmployeeDim 
ORDER BY length(employeenumber) DESC
      
9/1/12 actually got one
HARRIS,SHERIDAN :: RY1, emp# 62800 & 162800   
checked pymast, there IS no record LIKE 62800
looks LIKE it was 62800, somebody fixed it to 162800,
so i can DELETE the 62800 row (empkey 1206), but, anyplace that empkey IS used, the empkey must be changed to 1722
OR
DELETE the ed.empkey1722 row AND fix the emp# ON the 1206 row
what I need IS a script to detect any instance of an empkey IN any TABLE
1. what tables include empkey
SELECT parent
FROM system.columns
WHERE name = 'employeekey'

maybe the real answer IS to enforce RI BETWEEN edwEmployeeDim AND ALL tables with empkey COLUMN

SELECT * FROM system.relations

SELECT left(a.parent, 25), b.* 
FROM system.columns a
LEFT JOIN system.relations b on a.parent = b.ri_foreign_table
WHERE a.name = 'employeekey'

-- ok, ALL tables w/empkey have empkey ri (except flaghours), so it should be ok
-- to DELETE a record FROM edwEmployeeDim, THEN fix the 1206 row
-- ri rule won't allow me to DELETE the row because of records IN edwClockHoursFact 
--   because there are rows IN edwClockHoursFact for every active employee
--   AND keymapdimemp, AND factClockhourstoday AND rptclockhours
select * FROM edwClockHoursFact WHERE employeekey IN (1206,1722) AND vacationhours <> 0

SELECT employeekey, COUNT(*)
FROM edwClockHoursFact
WHERE employeekey IN (1206,1722)
GROUP BY employeekey

-- rows for both empkeys
SELECT *
FROM edwClockHoursFact a
INNER JOIN edwClockHoursFact b ON b.employeekey = 1206
  AND a.datekey = b.datekey
WHERE a.employeekey = 1722  

so

DELETE FROM edwClockHoursFact WHERE employeekey = 1722
DELETE FROM keymapdimemp WHERE employeekey = 1722
DELETE FROM factclockhourstoday WHERE employeekey = 1722
DELETE FROM rptClockHours WHERE employeekey = 1722
DELETE FROM edwEmployeeDim WHERE employeekey = 1722

-- employeenumber --------------------------------------------------------------

-- tech fiasco --------------------------------------------------------------

SELECT * FROM system.columns WHERE name = 'techkey'
-- tech fiasco --------------------------------------------------------------

-- clock vs term

select * FROM edwEmployeeDim WHERE employeekey IN (5654)
SELECT * FROM day where thedate = '12/04/2019'  -- key: 5817
SELECT * FROM edwEmployeeDim WHERE employeenumber = '184635' ORDER BY employeekey
SELECT b.thedate, a.* FROM edwClockHoursFact a join day b on a.datekey = b.datekey where employeekey = 5654 and clockhours <> 0

UPDATE edwEmployeeDim
SET termdate = '12/04/2019', termdatekey = 5817
WHERE employeekey = 4962;

SELECT ek, termdate, MAX(thedate) AS MaxClockDate
FROM (  
  SELECT a.employeekey AS ek, a.termdate, b.*, c.thedate
  FROM edwEmployeeDim a
  LEFT JOIN edwClockHoursFact b ON a.employeekey = b.employeekey
    AND clockhours <> 0
  LEFT JOIN day c ON b.datekey = c.datekey
  WHERE currentrow = true 
    AND activecode = 't'
    AND a.storecode <> 'RY3'
          AND a.employeekey NOT IN (1671,1819,2026,2087, 2179, 1777, 2316, 
            2373, 2604, 2645, 2601, 2714, 2203, 2510, 2911, 3123, 3297, 3413, 
            2828, 2972, 3577, 3432, 3346, 3654, 3731, 3708, 3627, 3249, 4132,
			4212, 4153,4116,3662, 3247,4116,4325,4317,3857,4553,4329, 4290,4715,4730,5006,
			3559,4834,4870,4907,4778,5016,4097,5129,4973,4358,4868,4840,4668,5463,
			4410,5283,5364,5654,5230,5848)) x       
GROUP BY ek, termdate  
HAVING termdate < MAX(thedate)

SELECT * FROM edwEmployeeDim WHERE employeekey in (5848)
      
UPDATE edwEmployeeDim
SET termdate = '01/09/2020', termdatekey = (SELECT datekey FROM day WHERE thedate = '01/09/2020')
WHERE employeekey = 5036    

  
    SELECT *
    FROM (
      SELECT ek, termdate, MAX(thedate) AS MaxClockDate
      FROM (  
        SELECT a.employeekey AS ek, a.termdate, b.*, c.thedate
        FROM edwEmployeeDim a
        LEFT JOIN edwClockHoursFact b ON a.employeekey = b.employeekey
          AND clockhours <> 0
        LEFT JOIN day c ON b.datekey = c.datekey
        WHERE currentrow = true 
          AND activecode = 't') x     
      GROUP BY ek, termdate  
      HAVING termdate < MAX(thedate)) z
-- empDim   
  
-- name change ----------------------------------------------------------------
-- name changes are made IN the nightly etl, but flag
-- a maint email, need to determine whether it IS a legitimate change (kathy hankin to kathy blumhagen)
-- OR just a correction, IF a correction edwEmployeeDim needs to be fixed, deleting
-- the newly added empkey AND making the correction to ALL instances of the emp# rows
-- shit, that also means correcting ALL instances of the new bogus employeekey to the correct value

-- 11/5/13: fb 314, expose name change (no longer processed AS an automatic type 2 change,
--            determine whether it IS a correction OR a type 2 change   
  
  @NameChange = ( -- 1        
    SELECT COUNT(*)
--    SELECT left(x.name, 40), edwed.name, edwed.employeenumber
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE  x.Name <> edwED.name
      AND edwED.ActiveCode <> 'T');  
-- 5/14/15 gulled welli becomes Ahmed Abdi Bashir

select * FROM edwEmployeeDim WHERE employeenumber = '137837'
select * FROM edwEmployeeDim WHERE employeenumber = '137854'

UPDATE edwEmployeeDim 
SET name = 'HIPSAK, SEBASTAIN V', firstname = 'Sebastain', middlename = 'V'
-- SELECT * FROM edwEmployeeDim 
WHERE lastname = 'hipsak'

UPDATE edwEmployeeDim 
SET name = 'SYVERSON, ASHLEY L', lastname = 'Syverson', middlename = 'L'
-- SELECT * FROM edwEmployeeDim 
WHERE employeenumber = '137837'

UPDATE edwEmployeeDim 
SET name = 'GILBERTSON, PAULA C', lastname = 'Gilbertson'
-- SELECT * FROM edwEmployeeDim 
WHERE employeenumber = '143242'

UPDATE edwEmployeeDim 
SET name = 'BARTA, JADE L', lastname = 'Barta'
WHERE employeenumber = '2141225'

UPDATE edwEmployeeDim 
SET name = 'CASSERLY, SAMANTHA', lastname = 'Casserly'
WHERE employeenumber = '164821'


UPDATE edwEmployeeDim 
SET name = 'SCHUMACHER, MATTHEW', firstname = 'Matthew'
WHERE employeenumber = '148462'

UPDATE edwEmployeeDim 
SET name = 'PALACIOS-FOLEY, ERIC', lastname = 'Palacios-Foley'
WHERE employeenumber = '137593'

UPDATE edwEmployeeDim 
SET name = 'KANNEH, FRANCES', lastname = 'Kanneh'
WHERE employeenumber = '259756'

SELECT * FROM edwEmployeeDim WHERE employeenumber = '164785'

BAKKEN, BRAYDON
BAKKEN, BRAYDEN

-- 9/11/14
-- a shit load of name changes (space after comma)
-- 9/24/14, this IS NOT perfect, WHEN danielle anderson got married, her name 
-- changed to danielle streitz, this overwrote the name IN the danielle anderson
-- row to danielle streitz, which showed up IN the nightly AS an inconsistent
-- name failure, because this script did NOT change the first,last,midddle 
-- IN edwEmployeeDim 
/*
only one remaining
SELECT *
FROM edwEmployeeDim
WHERE currentrow = true
  AND active = 'active'
  AND position(', ' IN name) = 0
 */
 
UPDATE edwEmployeeDim 
SET name = x.name
FROM (
  SELECT a.employeenumber, a.name--, b.employeenumber, b.name
  FROM xfmEmployeeDim a
  LEFT JOIN edwEmployeeDim b on a.storecode = b.storecode
    AND a.employeenumber = b.employeenumber
    AND a.name = replace(b.name, ',', ', ')
  WHERE b.employeenumber IS NOT NULL) x 
WHERE edwEmployeeDim.employeenumber = x.employeenumber 
      
SELECT *
FROM edwEmployeeDim
WHERE employeenumber IN (
  SELECT employeenumber
  FROM edwEmployeeDim
  WHERE rowchangereason = 'name'
    AND employeenumber NOT IN ('16250', '268200', '255950', '160930', '136185','17410')) -- known legitimate name changes   
ORDER BY employeenumber, employeekey    

SELECT *
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE  x.Name <> edwED.name
      AND edwED.ActiveCode <> 'T'
      
-- 3/6/14 Lindsey C Aune to Lindsey C Bohlman
-- 5/20 Amanda Hopkins to Amanda Holweger
-- 7/3 brooke k legacie to brooke k johnson
-- 7/14/17 morgan mason to morgan hibma
-- 7/19/17 afton cameron to afton bruggerman
-- 7/28/17 allison scholand to allison nelson
-- 8/15/17 kayle m hendrickson to kayle m metzger
-- 3/14/18 Brooke Sutherland to Brooke Sabin
SELECT * FROM edwEmployeeDim WHERE employeenumber = '181520'
SELECT *
FROM xfmEmployeeDim
WHERE employeenumber = '1123355'   
-- UPDATE the old row



DECLARE @nowTS timestamp;
@nowTS = now();
        UPDATE edwEmployeeDim
        SET RowChangeDate = CAST(@NowTS AS sql_date),
            RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
            RowThruTS = @NowTS,
            CurrentRow = false, 
            RowChangeReason = 'Name',
            EmployeeKeyThruDate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date),
            EmployeeKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST(timestampadd(sql_tsi_day, -1, @NowTS) AS sql_date))
        -- SELECT *   
        FROM (
          SELECT *
          FROM xfmEmployeeDim
          WHERE employeenumber = '256845') x 
        LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
          AND x.employeeNumber = e.EmployeeNumber
          AND e.CurrentRow = true;  
          
    -- INSERT new record        
        INSERT INTO edwEmployeeDim(StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution, Name, BirthDate,  
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            RowFromTS, CurrentRow, EmployeeKeyFromDate, EmployeeKeyFromDateKey,
            EmployeeKeyThruDate, EmployeeKeyThruDateKey, 
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod) 
        SELECT StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution, Name, BirthDate, 
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            @NowTS, TRUE,     
            CAST(@NowTS AS sql_date),
            (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
            (SELECT thedate FROM day WHERE datetype = 'NA'),
            (SELECT datekey FROM day WHERE datetype = 'NA'),
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod           
        FROM (
          SELECT *
          FROM xfmEmployeeDim
          WHERE employeenumber = '256845') x;          

      
select * FROM edwEmployeeDim WHERE employeenumber = '256845'      
-- oops UPDATE these "last" attributes
UPDATE edwEmployeeDim
SET managername = 'LANGENSTEIN, JARED',
lastname = 'Sutherland',
firstname = 'Brooke'
WHERE employeekey = 4408


-- the newly added empkey AND making the correction to ALL instances of the emp# rows
-- shit, that also means correcting ALL instances of the new bogus employeekey to the correct value

--4/1
-- ALL of these FROM 3/29 are adding a space BETWEEN the comma AND first name
emp#    name                   oldkey    newkey
--114200   BHATTARAI, GOPAL       1650      1976
--114210   BHATTARAI, SUK BAHADUR 1777      1977
--115255   BJORNSETH, MARK L.     1425      1979
--120910   BHETWAL, KRISHNA P     1900      1978
5/14
--1106390  OMAR, YASMIN H         2018      2030
5/15
-- 145720  FLIKKA,MATTHEW G       1958      2032
-- 2119120 RUD,JOSEPH A           1921      2033
6/5
-- 174140  JOHNSON, COLE W        2047      2055
6/21 Oldenbury -> Oldenburg
-- 1106310 OLDENBURG,ERIKA L      2066      2076

DECLARE @oldkey integer;
DECLARE @newkey integer;
DECLARE @emp# cichar(7);
DECLARE @CorrectName cichar(25);
@oldkey = 2066;
@newkey = 2076;
@emp# = '1106310';
@CorrectName = 'OLDENBURG,ERIKA L ';
UPDATE edwClockHoursFact SET employeekey = @oldkey WHERE employeekey = @newkey;
UPDATE factClockHoursToday SET employeekey = @oldkey WHERE employeekey = @newkey;
UPDATE factClockMinutesByHour SET employeekey = @oldkey WHERE employeekey = @newkey;
UPDATE factTechFlagHoursByDay SET employeekey = @oldkey WHERE employeekey = @newkey;
DELETE FROM keyMapDimEmp WHERE employeekey = @newkey;
UPDATE rptClockHours SET employeekey = @oldkey WHERE employeekey = @newkey;
UPDATE rptClockMinutesByHour SET employeekey = @oldkey WHERE employeekey = @newkey;
UPDATE rptFlagHours SET employeekey = @oldkey WHERE employeekey = @newkey;
DELETE FROM edwEmployeeDim WHERE employeekey = @newkey;
UPDATE edwEmployeeDim 
  SET name = @CorrectName
  WHERE employeenumber = @emp#;
UPDATE edwEmployeeDim 
  SET currentrow = true,
      rowchangedate = NULL,
      rowchangedatekey = NULL,
      rowthruts = NULL,
      rowchangereason = NULL,
      employeekeyThruDate = '12/31/9999',
      employeekeyThruDateKey = 7306
WHERE employeekey = @oldkey;

    

-- look for instances of the new key
DECLARE @keyname string;
DECLARE @key string; 
DECLARE @parent string;
DECLARE @stmt string;
DECLARE @ParentCur CURSOR AS
  SELECT * FROM tmpParent;

@keyname = 'Employeekey';
@key = '2055';

DROP TABLE tmpParent;
CREATE TABLE tmpParent (
  parent cichar(50));

DROP TABLE tmpParentKeyCount;  
CREATE TABLE tmpParentKeyCount (
  parent cichar(50),
  keycount integer);  
  
@stmt = 'INSERT INTO tmpParent SELECT left(parent, 50) FROM system.columns WHERE name = ' +  '''' + @keyname + '''';
EXECUTE immediate @stmt;


OPEN @ParentCur;
TRY
  WHILE FETCH @ParentCur DO
    @parent = @ParentCur.parent;
    @stmt = 'insert INTO tmpParentKeyCount SELECT ' + '''' + TRIM(@parent) + '''' + ', COUNT(*) FROM ' + '' + @Parent + '' 
      + ' WHERE cast( ' + '' + @keyname + '' + ' AS sql_char) = ' + '''' + @key + '''';
    EXECUTE immediate @stmt;
  END WHILE;
FINALLY
  CLOSE @ParentCur;
END TRY;

-- these are the tables that have to be updated
SELECT * FROM tmpParentKeyCount WHERE keycount <> 0 ORDER BY parent;
SELECT * FROM tmpParent
/*
DROP table tmpParent;
DROP TABLE tmpParentKeyCount;
*/
------------------------------------------------
MAGNUSON,TYLER M to MAGNUSON,TYLER D
new empkey IS 1944, previous empkey IS 1759
UPDATE edwClockHoursFact 
SET employeekey = 1759
WHERE employeekey = 1944;
UPDATE factClockHoursToday
SET employeekey = 1759
WHERE employeekey = 1944;
DELETE FROM keyMapDimEmp WHERE employeekey = 1944;
UPDATE rptClockHours 
SET employeekey = 1759
WHERE employeekey = 1944;
UPDATE edwEmployeeDim
SET name = 'MAGNUSON,TYLER D'
WHERE employeenumber = '190600';
DELETE FROM edwEmployeeDim WHERE employeekey = 1944;
UPDATE edwEmployeeDim 
SET currentrow = true,
    rowchangedate = NULL,
    rowchangedatekey = NULL, 
    rowthruTS = NULL, 
    RowChangeReason = NULL, 
    employeekeyThruDate = '12/31/9999',
    employeekeythrudatekey = 7306
WHERE employeekey = 1759; 
-------------------------------------------------------------------
ACHARYA,DHARMA changed to ACHARYA, DHARMA
new empkey IS 1912, previous empkey IS 1684

UPDATE edwClockHoursFact 
SET employeekey = 1684
WHERE employeekey = 1912;

UPDATE factClockHoursToday
SET employeekey = 1684
WHERE employeekey = 1912;

UPDATE factClockMinutesByHour
SET employeekey = 1684
WHERE employeekey = 1912;

DELETE FROM keyMapDimEmp WHERE employeekey = 1912;

UPDATE rptClockHours 
SET employeekey = 1684
WHERE employeekey = 1912;

UPDATE rptClockMinutesByHour
SET employeekey = 1684
WHERE employeekey = 1912;

UPDATE edwEmployeeDim
SET name = 'ACHARYA, DHARMA'
WHERE employeenumber = '11650';

DELETE FROM edwEmployeeDim WHERE employeekey = 1912;
-- manually fix edwEmployeeDim : DELETE 1850, 
UPDATE edwEmployeeDim 
SET currentrow = true,
    rowchangedate = NULL,
    rowchangedatekey = NULL, 
    rowthruTS = NULL, 
    RowChangeReason = NULL, 
    employeekeyThruDate = '12/31/9999',
    employeekeythrudatekey = 7306
WHERE employeekey = 1684;  

-- 2/26
SELECT * FROM tmpParentKeyCount WHERE keycount <> 0 ORDER BY parent;
KREWSON, TONY D changed to KREWSON, ANTHONY D
new empkey IS 1934, previous empkey IS 1502

UPDATE edwClockHoursFact 
SET employeekey = 1502
WHERE employeekey = 1934;

UPDATE factClockHoursToday
SET employeekey = 1502
WHERE employeekey = 1934;

DELETE FROM keyMapDimEmp WHERE employeekey = 1934;

UPDATE rptClockHours 
SET employeekey = 1502
WHERE employeekey = 1934;



DELETE 
FROM edwEmployeeDim 
WHERE employeekey = 1934;

UPDATE edwEmployeeDim 
SET currentrow = true,
    rowchangedate = NULL,
    rowchangedatekey = NULL, 
    rowthruTS = NULL, 
    RowChangeReason = NULL, 
    employeekeyThruDate = '12/31/9999',
    employeekeythrudatekey = 7306
WHERE employeekey = 1502;  

UPDATE edwEmployeeDim 
SET name = 'KREWSON, ANTHONY D'
WHERE name = 'KREWSON, TONY D';



--/ name change ---------------------------------------------------------------  


        
/*********** empdim ******************/
/*    ClockVsTerm *******************/ 
      SELECT ek, termdate, MAX(thedate) AS MaxClockDate
      FROM (  
        SELECT a.employeekey AS ek, a.termdate, b.*, c.thedate
        FROM edwEmployeeDim a
        LEFT JOIN edwClockHoursFact b ON a.employeekey = b.employeekey
          AND clockhours <> 0
        LEFT JOIN day c ON b.datekey = c.datekey
        WHERE currentrow = true 
          AND activecode = 't') x     
      GROUP BY ek, termdate  
      HAVING termdate < MAX(thedate)    
 
/*    ClockVsTerm                     */    
/*********** empdim ******************/       


-- 11/20/12 -------------------------------------------------------------------
-- bogus type 2 changes
ok, here IS the situation
empl#1107950
           
1. first entered IN arkona with a mis-spelled name
2. first entered IN arkona with no salary

resulting IN 2 type 2 changes that should NOT be type 2 changes
resulting IN 3 employeekeys WHERE there should be only one

empkey: 1815, 1825, 1836

1. IN what tables will i find an employeekey?
SELECT parent FROM system.columns WHERE name = 'employeekey'

SELECT * FROM edwClockHoursFact WHERE employeekey IN (1815, 1825, 1836)
SELECT * FROM factTechFlagHoursByDay WHERE employeekey IN (1815, 1825, 1836)
*** SELECT * FROM keyMapDimEmp WHERE employeekey IN (1815, 1825, 1836) *** this one needs some editing, NOT just update
SELECT * FROM factClockHoursToday WHERE employeekey IN (1815, 1825, 1836)
SELECT * FROM rptClockHours WHERE employeekey IN (1815, 1825, 1836)
SELECT * FROM rptFlagHours WHERE employeekey IN (1815, 1825, 1836)
SELECT * FROM factClockMinutesByHour WHERE employeekey IN (1815, 1825, 1836)
SELECT * FROM rptClockMinutesByHour WHERE employeekey IN (1815, 1825, 1836)

UPDATE edwClockHoursFact SET employeekey = 1815 WHERE employeekey IN (1825,1836);
UPDATE factTechFlagHoursByDay SET employeekey = 1815 WHERE employeekey IN (1825,1836);
UPDATE factClockHoursToday SET employeekey = 1815 WHERE employeekey IN (1825,1836);
UPDATE rptClockHours SET employeekey = 1815 WHERE employeekey IN (1825,1836);
UPDATE rptFlagHours SET employeekey = 1815 WHERE employeekey IN (1825,1836);
UPDATE factClockMinutesByHour SET employeekey = 1815 WHERE employeekey IN (1825,1836);
UPDATE rptClockMinutesByHour SET employeekey = 1815 WHERE employeekey IN (1825,1836);

UPDATE keyMapDimEmp
SET name = 'ORTIZ, GUADALUPE',
    salary = 15.31,
    hourlyrate = 15.31
WHERE employeekey = 1815;
DELETE FROM keyMapDimEmp WHERE employeekey IN (1825,1836); 

DELETE FROM edwEmployeeDim WHERE employeekey IN (1825,1836);

UPDATE edwEmployeeDim
SET name = 'ORTIZ, GUADALUPE',
    currentrow = true,
    rowchangedate = NULL,
    rowchangedatekey = NULL,
    rowthruts = NULL,
    rowchangereason = NULL,
    employeekeythrudate = '12/31/9999',
    employeekeythrudatekey = 7306,
    salary = 15.31,
    hourlyrate = 15.31      
WHERE employeekey = 1815;  

-- 11/20/12 -------------------------------------------------------------------

-- 11/25/12 -------------------------------------------------------------------
-- 1/2/12 got to get this fucking done, but it's a big scary boogy man
-- touching ON empkeyfrom/thru
clock vs term

SELECT *
FROM (
  SELECT ek, termdate, MAX(thedate) AS MaxClockDate
  FROM (  
    SELECT a.employeekey AS ek, a.termdate, b.*, c.thedate
    FROM edwEmployeeDim a
    LEFT JOIN edwClockHoursFact b ON a.employeekey = b.employeekey
      AND clockhours <> 0
    LEFT JOIN day c ON b.datekey = c.datekey
    WHERE currentrow = true 
      AND activecode = 't') x     
  GROUP BY ek, termdate  
  HAVING termdate < MAX(thedate)) z
  
kim bestul  
ry1  10/31 - 11/3   113515 ek 1806
ry2  11/4  -        213515 ek 1827

SELECT thedate, b.*
FROM day a
INNER JOIN edwClockHoursFact b ON a.datekey = b.datekey
WHERE a.monthname = 'november'
  AND b.employeekey IN (1806,1827)
  
so she has been clocking IN AS ry1, steve changed her ry2 login to default to ry2, was ry1
so what i have to DO IS change ALL clock data that shold have been ry2  

should be these tables

1. DELETE ALL records for 1806 after term date, but god damnit, this IS the termdate vs empkeythrudate issue
2. UPDATE 1806 -> 1827 post ry1 term date
SELECT (select thedate from day where datekey = a.datekey), a.* FROM edwClockHoursFact a WHERE employeekey IN (1806,1827) AND clockhours <> 0
-- SELECT * FROM factClockHoursToday WHERE employeekey IN (1806,1827) -- this one should NOT matter, reset daily
SELECT * FROM rptClockHours WHERE employeekey IN (1806,1827) AND clockhours <> 0
-- SELECT * FROM rptFlagHours WHERE employeekey IN (1806,1827) -- no flag hours for her
SELECT (select thedate from day where datekey = a.datekey), a.* FROM factClockMinutesByHour a WHERE employeekey IN (1806,1827)
SELECT (select thedate from day where datekey = a.datekey), a.* FROM rptClockMinutesByHour a WHERE employeekey IN (1806,1827)

termed at ry1 11/3

SELECT distinct a.thedate, b.*
FROM day a
LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  AND b.employeekey IN (1806,1827)
WHERE a.thedate BETWEEN '11/03/2012' AND '11/19/2012'
  AND clockhours <> 0
-- last day with 1806 hours 19th
SELECT max(a.thedate)
FROM day a
LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  AND b.employeekey = 1806
WHERE a.thedate BETWEEN '11/01/2012' AND curdate()
  AND clockhours <> 0

SELECT distinct a.thedate, b.*
FROM day a
LEFT JOIN rptClockHours b ON a.thedate = b.thedate
  AND b.employeekey IN (1806,1827)
WHERE a.thedate BETWEEN '11/01/2012' AND curdate()

-- wtf clockhours for 1806 & 1827 ON 11/19
SELECT distinct a.thedate, b.*
FROM day a
LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  AND b.employeekey IN (1806,1827)
WHERE a.thedate BETWEEN '11/04/2012' AND '11/17/2012'
  AND clockhours <> 0

-- 1/2/13
ok, fuckit, simply change empkey to 1827 for AND clockhours past 11/24 that are 1806

2. UPDATE 1806 -> 1827 post ry1 term date
  a.  SELECT (select thedate from day where datekey = a.datekey), a.* FROM edwClockHoursFact a WHERE employeekey IN (1806,1827) AND clockhours <> 0
  b.  SELECT * FROM rptClockHours WHERE employeekey IN (1806,1827) AND clockhours <> 0
  c.  SELECT (select thedate from day where datekey = a.datekey), a.* FROM factClockMinutesByHour a WHERE employeekey IN (1806,1827)
  d.  SELECT (select thedate from day where datekey = a.datekey), a.* FROM rptClockMinutesByHour a WHERE employeekey IN (1806,1827)
-- a --
DELETE FROM edwClockHoursFact WHERE employeekey = 1806 AND datekey = 3246;  
DELETE FROM edwClockHoursFact WHERE employeekey = 1827
AND datekey IN (
  SELECT datekey FROM 
  edwClockHoursFact 
  WHERE employeekey = 1806
    AND datekey IN (
      SELECT datekey
      FROM day
      WHERE thedate > '11/03/2012'));
UPDATE   
--SELECT * FROM 
edwClockHoursFact 
SET employeekey = 1827
WHERE employeekey = 1806
  AND datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate > '11/03/2012') 
  AND clockhours > 0;    
-- b -- 
DELETE FROM rptclockhours WHERE employeekey = 1806 AND datekey = 3246; 
UPDATE rptclockhours
SET employeekey = 1827
WHERE employeekey = 1806
  AND thedate > '11/03/2012'; 
-- c --  
DELETE FROM factClockMinutesByHour WHERE employeekey = 1806 AND datekey = 3246; 
UPDATE factClockMinutesByHour
SET employeekey = 1827
WHERE employeekey = 1806 AND datekey IN (
  SELECT datekey FROM 
  factClockMinutesByHour 
  WHERE employeekey = 1806
    AND datekey IN (
      SELECT datekey
      FROM day
      WHERE thedate > '11/03/2012'));
-- d --

DELETE FROM rptClockMinutesByHour WHERE employeekey = 1806 AND datekey = 3246; 
UPDATE rptClockMinutesByHour
SET employeekey = 1827
WHERE employeekey = 1806  
  AND thedate > '11/03/2012'
-- 11/25/12 -------------------------------------------------------------------

-- 12/4/12 --------------------------------------------------------------------
-- new tech
1.
INSERT INTO dimTech (storecode, employeenumber, name, description, technumber, 
  laborcost, flagdeptcode, flagdept)
SELECT a.storecode, a.employeenumber, a.name, a.description, a.technumber, 
  a.laborcost, 'RE', 'Detail'
FROM xfmTechDim a
LEFT JOIN (
  SELECT storecode, employeenumber, name
  FROM edwEmployeeDim
  WHERE currentrow = true) b ON a.employeenumber = b.employeenumber
LEFT JOIN dimTech c ON a.storecode = c.storecode
  AND a.employeenumber = c.employeenumber
WHERE a.employeenumber <> ''
  AND c.storecode IS NULL;
2.
EXECUTE PROCEDURE xfmDimTech(); 
3.
-- IF there are flag hours for the new tech
EXECUTE PROCEDURE stgRO(); 
4.
EXECUTE PROCEDURE rptServiceLineRptTables();

-- 12/4/12 --------------------------------------------------------------------

-- 12/11/12 -------------------------------------------------------------------
-- compare against Ray's paycheck
-- ask kim how this was sorted out
SELECT a.thedate, clockhours, regularhours,overtime
FROM day a
INNER JOIN edwClockHoursFact b ON a.datekey = b.datekey
INNER JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
  AND c.employeenumber = '148275'
WHERE biweeklypayperiodenddate = '12/01/2012'
UNION
SELECT cast('12/01/2012' AS sql_Date), 1448, 3258, SUM(clockhours) AS clockhours, 
  SUM(regularhours) AS regularhours, SUM(overtimehours) AS ot,
  SUM(vacationhours) AS vac, SUM(ptohours) AS pto, SUM(holidayhours) AS hol
FROM day a
INNER JOIN edwClockHoursFact b ON a.datekey = b.datekey
INNER JOIN edwEmployeeDim c ON b.employeekey = c.employeekey
  AND c.employeenumber = '148275'
WHERE biweeklypayperiodenddate = '12/01/2012'
GROUP BY b.employeekey
-- / 12/11/12 -----------------------------------------------------------------

-- 1/3/13 ---------------------------------------------------------------------
-- overlap
nur malim, termed at ry1 12/17/12, hired at ry2 12/17/12
SELECT * FROM edwEmployeeDim WHERE name LIKE '%nur%'

this IS goofy because clockhours are generated partially BY the empkeyfrom/thru
his ry2 payroll entry was NOT done until 1/2/13
(DO i need a createdts for new rows?, maybe use the rowfromts)

also getting itchy about the pattern of clockhour rows with 0 hours for any "valid" empkey
need to know, which tables record 0 hour rows for "expired" empkeys

-- so how the fuck are there clockhours for both emp#s ON each day 12/17 -> 12/28
-- i am guessing that ry2 got added right away, but ry1 was NOT termed until 1/2
-- whatever
SELECT *
FROM tmppypclockin
WHERE yiemp# IN ( '190690','290690')
  AND yiclkind > '12/16/2012'
ORDER BY yiclkind, yiemp#

-- fix will be to DELETE / SET to zero ALL 190690 rows > 12/17
-- AND 290690 rows < 12/18
-- shit even IF i DO this, the data will repopulate FROM arkona
-- need to talk to kim AND chris about this (see pypclockin, pypclkl

-- kim & chris fucked up, IN arkona i deleted ALL 190690 records > 12/16  
  
-- /> 1/3/13 ------------------------------------------------------------------

-- dup 
-- 2/12/13, first time this has come up

SELECT * FROM edwEmployeeDim WHERE employeenumber = '137837'

    SELECT *
    FROM (
      SELECT yico#, yiemp#, yiclkind, yiclkint
      FROM tmpPYPCLOCKIN
      GROUP BY yico#, yiemp#, yiclkind, yiclkint
        HAVING COUNT(*) > 1) a

SELECT distinct c.name, a.yiemp#, a.yiclkind, a.yiclkint, 'HANSON, KEVIN W' as manager --c.managername
FROM tmppypclockin a
INNER JOIN (
    SELECT yico#, yiemp#, yiclkind, yiclkint
    FROM tmpPYPCLOCKIN
    GROUP BY yico#, yiemp#, yiclkind, yiclkint
      HAVING COUNT(*) > 1) b ON a.yico# = b.yico#
  AND a.yiemp# = b.yiemp#
  AND a.yiclkind = b.yiclkind
  AND a.yiclkint = b.yiclkint  
LEFT JOIN edwEmployeeDim c ON a.yiemp# = c.employeenumber
  AND c.currentrow = true      
ORDER BY a.yiemp#, a.yiclkind, a.yiclkint  

-- 9/21 NightlyBatch
SELECT * 
FROM NightlyBatch a
WHERE thedate = curdate()


SELECT *
FROM ( -- here's what should have run
  SELECT a.executable, a.frequency, b.Proc, c.dObject
  FROM zProcExecutables a
  INNER JOIN zProcProcedures b on a.Executable = b.executable
  INNER JOIN DependencyObjects c on b.Executable = c.dExecutable
    AND b.Proc = c.dProcedure
  WHERE a.frequency = 'daily' ) d
LEFT JOIN (
  select a.thedate, b.*
  from day a, NightlyBatch b 
  WHERE a.thedate = curdate() - 7)
  e on d.dObject = e.dObject 

2 different failure modes 
1. no row IN nightlybatch
2. row IN nightlybatch with NULL complete


SELECT a.executable, a.frequency, b.Proc
FROM zProcExecutables a
INNER JOIN zProcProcedures b on a.Executable = b.executable
ORDER BY a.executable, b.proc

-- 5/4
jay sorum showed termed on 8/24/13, should NOT have been
came to light WHEN he tried to clock IN on 4/24/14
so, reverse the term
UPDATE edwEmployeeDim
  SET ActiveCode = 'A',
      Active = 'Active',
      TermDate = '12/31/9999',
      TermDateKey = 7306
WHERE employeenumber = '1130428'

@InconsistentName = (
SELECT COUNT(*) 
FROM (       
  SELECT name
  FROM (   
    SELECT name, lastname, firstname, coalesce(middlename, '')
    FROM edwEmployeeDim
    GROUP BY name, lastname, firstname, coalesce(middlename, '')) x
  GROUP BY name HAVING COUNT(*) > 1) a); 
  
SELECT employeenumber, name, lastname, firstname, coalesce(middlename, '') FROM edwEmployeeDim WHERE name LIKE 'ramon%' AND currentrow

UPDATE edwEmployeeDim 
SET lastname = 'Ramon jr'
WHERE employeenumber = '267531'



@FromGtThru = (
  SELECT COUNT(*) 
--  SELECT *
  FROM (    
    SELECT employeekey
    FROM edwEmployeeDim
    WHERE employeenumber IN (
      SELECT employeenumber 
      FROM edwEmployeeDim
      WHERE employeekeyfromdate > employeekeythrudate)) w);      
      
      
select employeekey, employeenumber, name, activecode, employeekeyfromdate, employeekeythrudate
-- SELECT *
FROM edwEmployeeDim
WHERE employeekey IN (
    SELECT employeekey
    FROM edwEmployeeDim
    WHERE employeenumber IN (
      SELECT employeenumber 
      FROM edwEmployeeDim
      WHERE employeekeyfromdate > employeekeythrudate))    
ORDER BY employeenumber, employeekey

UPDATE edwEmployeeDim
SET employeekeythrudate = '12/16/2021'
WHERE employeekey = 5463;

UPDATE edwEmployeeDim
SET employeekeyfromdate = '12/17/2021'
WHERE employeekey = 5915;


/* 
-- 06/04/21
CADEN DAHL
was a rehire but now IS just a hire/term
this one IS goofy OR i misread it entirely
maybe it was a mistaken term don't know, there IS no break IN clock hours BETWEEN 10/7/20 AND 5/26/21

delete FROM edwClockHoursFact WHERE employeekey = 5575
delete FROM keymapdimemp WHERE employeekey = 5575
delete FROM factclockhourstoday WHERE employeekey = 5575
delete FROM factclockminutesbyhour WHERE employeekey = 5575
DELETE FROM edwEmployeeDim WHERE employeekey = 5575
UPDATE edwEmployeeDim
SET currentrow = true, rowchangedate = NULL, rowchangedatekey = NULL,
    rowthruts = NULL, rowchangereason = NULL, 
		employeekeythrudate = '12/31/9999', employeekeythrudatekey = 7306
WHERE employeekey = 5330

-- 04/21/21
should have known, FROM yesterday, the holland & longoria rehire/reuse
make the change date 4/14/21 for both of them

SELECT employeekey, employeenumber, name, activecode, employeekeyfromdate, employeekeythrudate
FROM edwEmployeeDim
WHERE employeekey IN (3930,4415,4918,4952,5527,5528)
ORDER BY name, employeekey

-- holland need to fix 2 rows: ek 4415 & 5527
-- 4415 thrudate
UPDATE edwEmployeeDim
  SET employeekeythrudate = '04/13/2021', 
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '04/13/2021')
WHERE employeekey = 4415;

--5527 fromdate
UPDATE edwEmployeeDim
  SET employeekeyfromdate = '04/14/2021', 
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '04/14/2021')
WHERE employeekey = 5527;

longoria 4952 & 5528
UPDATE edwEmployeeDim
  SET employeekeythrudate = '04/13/2021', 
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '04/13/2021')
WHERE employeekey = 4952;

--5527 fromdate
UPDATE edwEmployeeDim
  SET employeekeyfromdate = '04/14/2021', 
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '04/14/2021')
WHERE employeekey = 5528;


--< 01/02/21 ----------------------------------
-- nicholas nelson, looks LIKE it IS happening to ALL rehire/reuse ?
employeekey	employeenumber	name	 		   activecode	employeekeyfromdate	employeekeythrudate
4470		2100751			"NELSON, NICHOLAS"	T			09/10/2018			12/31/1920
5422		2100751			"NELSON, NICHOLAS"	A			01/01/1921			12/31/9999

UPDATE edwEmployeeDim
  SET employeekeythrudate = '12/31/2020', 
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '12/31/2020')
WHERE employeekey = 4470;


UPDATE edwEmployeeDim
  SET employeekeyfromdate = '01/01/2021', 
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '01/01/2021')
WHERE employeekey = 5422;

select employeekey, employeenumber, name, activecode, employeekeyfromdate, employeekeythrudate
-- SELECT *
FROM edwEmployeeDim
WHERE employeenumber = '2100751';

01/06/21 figured it out
hackey fix IN xfmEmployeeDim() should handle it
SELECT employeekey,employeenumber, name, activecode,employeekeyfromdate, employeekeythrudate
FROM edwEmployeeDim WHERE lastname = 'slator'

UPDATE edwEmployeeDim
  SET employeekeythrudate = '01/03/2021', 
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '01/03/2021')
WHERE employeekey = 5051;

UPDATE edwEmployeeDim
  SET employeekeyfromdate = '01/04/2021', 
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '01/04/2021')
WHERE employeekey = 5424;
--/> 01/02/21 ----------------------------------
    

---< 10/12/20 --------------------------------------------------------------------------
-- mike longoria
UPDATE edwEmployeeDim
  SET employeekeythrudate = '09/30/2020', 
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '09/30/2020')
WHERE employeekey = 4658;

UPDATE edwEmployeeDim
  SET employeekeyfromdate = '10/01/2020', 
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '10/01/2020')
WHERE employeekey = 5327;

-- tyler bedney
UPDATE edwEmployeeDim
  SET employeekeythrudate = '09/30/2020', 
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '09/30/2020')
WHERE employeekey = 4694;

UPDATE edwEmployeeDim
  SET employeekeyfromdate = '10/01/2020', 
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '10/01/2020')
WHERE employeekey = 5328;

---/> 10/12/20 --------------------------------------------------------------------------


UPDATE edwEmployeeDim
  SET employeekeyfromdate = '09/03/2020', 
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '09/03/2020')
WHERE employeekey = 5320;
 
select * FROM edwEmployeeDim WHERE lastname = 'bedney' AND firstname = 'tyler' AND storecode = 'ry1'     
      
UPDATE edwEmployeeDim
SET employeekeythrudate = '09/30/2019', employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '09/30/2019')
WHERE employeekey = 4249      

UPDATE edwEmployeeDim
SET employeekeyfromdate = '10/01/2019', employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '10/01/2019')
WHERE employeekey = 4919    
*/

/*	  
SELECT * FROM edwEmployeeDim WHERE employeenumber = '279720'  
DELETE FROM edwEmployeeDim WHERE employeekey = 4392
SELECT b.thedate, a.* FROM edwClockHoursFact a inner join day b on a.datekey = b.datekey WHERE employeekey = 4392 ORDER BY thedate DESC 
DELETE FROM edwClockHoursFact WHERE employeekey = 4392 AND datekey > 5288
DELETE FROM factClockMinutesByHour  WHERE employeekey = 4392
UPDATE edwEmployeeDim
SET employeekeythrudate = '12/31/9999',
    employeekeythrudatekey = 7306,
	currentrow = true,
	rowchangereason = null
WHERE employeekey = 3247	

UPDATE edwEmployeeDim
SET employeekeythrudate = '09/10/2019',
    employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '09/10/2019')
WHERE employeekey = 4333;

UPDATE edwEmployeeDim
SET employeekeyfromdate = '09/11/2019',
    employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '09/11/2019')
WHERE employeekey = 4898;
*/

@ClockDup = (
  SELECT COUNT(*)
  -- SELECT *
  FROM (
    SELECT yico#, yiemp#, yiclkind, yiclkint
    FROM tmpPYPCLOCKIN
    WHERE yiclkind not in ('09/07/2015', '05/25/2020')
    GROUP BY yico#, yiemp#, yiclkind, yiclkint
      HAVING COUNT(*) > 1) a);   
     
SELECT * FROM edwEmployeeDim WHERE employeenumber = '1126060'
       	  
SELECT a.yiemp#, b.name, a.yiclkind, a.yiclkint, a.yiclkoutt, a.yicode, b.managername
FROM tmpPYPCLOCKIN a
INNER JOIN (
  SELECT yico#, yiemp#, yiclkind, yiclkint
  FROM tmpPYPCLOCKIN
  WHERE yiclkind <> '09/07/2015'
  GROUP BY yico#, yiemp#, yiclkind, yiclkint
    HAVING COUNT(*) > 1) aa on a.yiemp# = aa.yiemp# AND a.yiclkind = aa.yiclkind AND a.yiclkint = aa.yiclkint
LEFT JOIN edwEmployeeDim b on a.yiemp# = b.employeenumber	
  AND b.currentrow = true	  
  
  --  FromGtThru = (
SELECT *
FROM (    
  SELECT employeekey
  FROM edwEmployeeDim
  WHERE employeenumber IN (
    SELECT employeenumber 
    FROM edwEmployeeDim
    WHERE employeekeyfromdate > employeekeythrudate)) w
    
    
SELECT * FROM edwEmployeeDim WHERE employeekey = 4486    

UPDATE edwEmployeeDim
SET employeekeythrudate = '12/31/9999',
    employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '12/31/9999')
WHERE employeekey = 4486