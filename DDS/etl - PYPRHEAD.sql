-- 1. CREATE base COLUMN list for CREATE table
SELECT cast(lcase(column_name) as sql_char(10)),
  CASE
  WHEN data_type = 'varchar' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'char' THEN 'cichar(' + trim(cast(length AS sql_char)) + '),'
	WHEN data_type = 'numeric' THEN 'integer,'
	WHEN data_type = 'timestmp' THEN 'timestamp,'
	WHEN data_type = 'smallint' THEN 'integer,'
	WHEN data_type = 'integer' THEN 'double,'
	WHEN data_type = 'decimal' THEN 'date,'
	WHEN data_type = 'date' THEN 'date,'
	WHEN data_type = 'time' THEN 'cichar(8),'
	ELSE 'aaaOh Shit' -- test for missing data type conversion
  END AS data_type
FROM syscolumns 
WHERE table_name = 'PYPRHEAD' 
--ORDER BY data_type
ORDER BY ordinal_position

-- this IS the golden easy to display field list  
SELECT LEFT(table_name, 8) AS table_name, LEFT(column_name, 12) AS column_name,
  data_type, length, numeric_scale, numeric_precision, column_text
from syscolumns
WHERE table_name = 'PYPRHEAD'  
AND data_type <> 'CHAR'

-- 2. non char fields for spreadsheet, resolve ads data types
SELECT LEFT(column_name, 12) AS column_name, data_type
from syscolumns
WHERE table_name = 'PYPRHEAD'  
AND data_type <> 'CHAR'

-- DROP TABLE stgArkonaPYHSHDTA
-- 3. revise DDL based ON step 2, CREATE TABLE
-- this one IS too fucking much for the spreadsheet, just go thru it here

-- 
CREATE TABLE stgArkonaPYPRHEAD (
yrco#      cichar(3),          
yrempn     cichar(7),          
yrmgrn     cichar(7),          
yrjobd     cichar(10),         
yrjobl     cichar(1),          
yrhdte     date,               
yrhdto     date,               
yrtdte     date,               
yrrdte     date,               
yrndte     date,               
yrcdte     date,               
yrbp01     money,               
yrbp02     money,               
yrbp03     money,               
yrbp04     money,               
yrbp05     money,               
yrbp06     money,               
yrbp07     money,               
yrbp08     money,               
yrbp09     money,               
yrbp10     money,               
yrbp11     money,               
yrbp12     money,               
yrbp13     money,               
yrbp14     money,               
yrbp15     money,               
yrbp16     money,               
yrdp01     date,               
yrdp02     date,               
yrdp03     date,               
yrdp04     date,               
yrdp05     date,               
yrdp06     date,               
yrdp07     date,               
yrdp08     date,               
yrdp09     date,               
yrdp10     date,               
yrdp11     date,               
yrdp12     date,               
yrdp13     date,               
yrdp14     date,               
yrdp15     date,               
yrdp16     date) IN database;            

-- 4. COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PYPRHEAD';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT TRIM(column_name) + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

-- parameter list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'PYPRHEAD';
@i = (
  SELECT MAX(ordinal_position)
  FROM syscolumns
  WHERE table_name = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM syscolumns WHERE ordinal_position = @j AND table_name = @tableName), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;


-- for code, setting parameters 
DECLARE @table_name string;
@table_name = 'stgArkonaPYPRHEAD';
SELECT 'AdsQuery.ParamByName(' + '''' + replace(TRIM(name), '#', '') + '''' + ').' + 
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX'
  END +  ' := AdoQuery.FieldByName('+ '''' + TRIM(name) + '''' + ').' +
  CASE Field_Type
    WHEN 20 THEN 'AsString'
	WHEN 11 THEN 'AsInteger'
	WHEN 3 THEN 'AsDateTime'
	WHEN 18 THEN 'AsCurrency'
	WHEN 10 THEN 'AsFloat'
	ELSE 'AsXXX' 
  END + ';' 
FROM system.columns
WHERE parent = @table_name;
