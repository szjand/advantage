-- this IS a factRepairOrder issue
SELECT ro, line, b.thedate
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE techgroupkey = 1
AND flaghours <> 0
GROUP BY ro, line, b.thedate
ORDER BY b.thedate DESC 

-- guessing i just fucked up on the transition
-- these are fixable
SELECT b.thedate, COUNT(*)
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE techgroupkey = 1
AND flaghours <> 0
GROUP BY b.thedate  


-- this IS a factRepairOrder issue
SELECT ro, line, flaghours, b.thedate, c.ptlhrs, pttech
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
LEFT JOIN stgArkonaSDPRDET c on a.ro = c.ptro# AND a.line = c.ptline
  AND c.ptltyp = 'L'
  AND c.ptcode = 'tt'
  AND c.ptlhrs > 0
WHERE techgroupkey = 1
AND flaghours > 0

-- obviously, i fucked up something IN the transitions to factRepairOrder,
-- most of these are on the
SELECT b.thedate, COUNT(*)
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
LEFT JOIN stgArkonaSDPRDET c on a.ro = c.ptro# AND a.line = c.ptline
  AND c.ptltyp = 'L'
  AND c.ptcode = 'tt'
  AND c.ptlhrs > 0
WHERE techgroupkey = 1
  AND flaghours > 0
GROUP BY thedate  

-- first step, determine IF any of these comprise a new tech GROUP
-- xfmDTG1
-- store, ro, line, linehours, techkey, techhours
DROP TABLE #xfm1;
SELECT a.storecode, a.ro, a.line, flaghours AS linehours, d.techkey, c.ptlhrs AS TechHours
INTO #xfm1
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
LEFT JOIN stgArkonaSDPRDET c on a.ro = c.ptro# AND a.line = c.ptline
  AND c.ptltyp = 'L'
  AND c.ptcode = 'tt'
  AND c.ptlhrs > 0
LEFT JOIN dimtech d on a.storecode = d.storecode 
  AND c.pttech = d.technumber
  AND b.thedate BETWEEN d.techkeyfromdate AND d.techkeythrudate   
WHERE techgroupkey = 1
AND flaghours > 0

-- xfmDTG2
DROP TABLE #xfm2;
SELECT storecode, ro, line, techkey, round(sum(techhours/linehours),4) AS weightfactor
INTO #xfm2
FROM #xfm1
GROUP BY storecode, ro, line, techkey; 

-- xfmDTG3
DECLARE @cur1 CURSOR;
DECLARE @cur2 CURSOR;
DECLARE @i integer;  
DECLARE @str string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @techkey string;  
DECLARE @weightfactor string;
DROP table zxfm3;
CREATE TABLE zxfm3 (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  TechKeyGroup cichar(120)) IN database; 
OPEN @cur1 AS 
  SELECT storecode, ro, line
  FROM #xfm2
  GROUP BY storecode, ro, line
  ORDER BY storecode, ro, line;  
TRY
  WHILE FETCH @cur1 DO
    @storecode = @cur1.storecode;
    @ro = @cur1.ro;
    @line = @cur1.line;
    OPEN @cur2 AS 
      SELECT techkey, weightfactor
      FROM #xfm2
      WHERE storecode = @storecode
        AND ro = @ro
        AND line = @line
      ORDER BY techkey;
    TRY
      @str = '';
      WHILE FETCH @cur2 DO
        @techkey = TRIM(CAST(@cur2.techkey AS sql_char));
        @weightfactor = TRIM(CAST(@cur2.weightfactor AS sql_char));
        @str = @str + @techkey + '|' + @weightfactor + '|';  
      END WHILE;
      INSERT INTO zxfm3 values(@storecode, @ro, @line, @str);
      @str = '';
    FINALLY
      CLOSE @cur2;
    END TRY;
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;

-- looks LIKE only 1 new group
SELECT DISTINCT a.TechKeyGroup
FROM zxfm3 a
LEFT JOIN dimTechGroup b ON a.TechKeyGroup = b.FlatGroup
WHERE b.flatgroup IS NULL;

-- INSERT it
-- first INTO dimTechGroup
-- THEN brTechGroup
-- that was a major revelation WHEN i initially did this shit, DO the dim first

DECLARE @i integer;
DECLARE @cur CURSOR;
BEGIN TRANSACTION;
--TRY 
  @i = (SELECT MAX(TechGroupKey) + 1 FROM dimTechGroup);  
  OPEN @cur AS 
    SELECT DISTINCT a.TechKeyGroup
    FROM zxfm3 a
    LEFT JOIN dimTechGroup b ON a.TechKeyGroup = b.FlatGroup
    WHERE b.flatgroup IS NULL;
  TRY
    WHILE FETCH @cur DO
      INSERT INTO dimTechGroup values (@i, @cur.TechKeyGroup);
      @i = @i + 1; 
    END WHILE;
  FINALLY
    CLOSE @cur;
  END TRY;  
  INSERT INTO brTechGroup
  SELECT a.techgroupkey, c.techkey, c.weightfactor
  FROM dimTechGroup a
  INNER JOIN zxfm3 b ON a.flatgroup = b.Techkeygroup
  INNER JOIN #xfm2 c ON b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line 
  WHERE NOT EXISTS (
    SELECT 1
    FROM brTechGroup
    WHERE TechGroupKey = a.TechGroupKey)
  GROUP BY a.techgroupkey, c.techkey, c.weightfactor;   
COMMIT WORK;  	  
CATCH ALL
  ROLLBACK;
  RAISE xfmDimTechGroup(f, 'dimTechGroup/brTechGroup ' + __errtext);
END TRY; // transaction       
        
DROP table #xfm2; 
SELECT * from zxfm3; 
--then UPDATE the bad dimTechGroupKeys IN factRepairOrder   

-- fuck, had to restart arc, lost the temp tables, should NOT need them    

-- ok, dimTechGroup got updated ok
SELECT *
FROM zxfm3 a
LEFT JOIN dimTechGroup b on a.TechKeyGroup = b.flatgroup 
WHERE b.techgroupkey IS NULL   

SELECT *
FROM zxfm3 a
LEFT JOIN dimTechGroup b on a.TechKeyGroup = b.flatgroup 

SELECT a.storecode, a.ro, a.line, a.techgroupkey, c.*
FROM factRepairOrder a
INNER JOIN zxfm3 b on a.storecode = b.storecode
  AND a.ro = b.ro
  AND a.line = b.line
INNER JOIN dimTechGroup c on b.TechKeyGroup = c.flatgroup
  
-- AND this IS it  
UPDATE factRepairOrder
SET TechGroupKey = x.TechGroupKey
FROM (
  SELECT *
  FROM zxfm3 a
  LEFT JOIN dimTechGroup b on a.TechKeyGroup = b.flatgroup) x
WHERE factRepairOrder.StoreCode = x.StoreCode
  AND factRepairOrder.RO = x.RO 
  AND factRepairOrder.Line = x.Line; 