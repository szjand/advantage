FROM zROHeader zRODetailNightly -2.sql

RO status needs to come FROM the PDP row
closedate needs to come FROM the SDP row

-- rethink xfmro
thinking generate the raw fact TABLE
1. voids
          when ptcnam = '*VOIDED REPAIR ORDER*' then 'VOID'
          when ptchk# like 'v%' then 'VOID'

2. miles
        CASE 
          WHEN ptodom < 0 THEN
            CASE 
              WHEN ptmilo < 0 THEN 0
              ELSE ptmilo
            END
          WHEN ptodom = ptmilo THEN ptodom
          WHEN ptodom < ptmilo THEN ptmilo
          WHEN ptodom > ptmilo THEN ptodom
        END AS miles

-- RHDR
DROP TABLE #RHDR;
SELECT a.ptco# AS storecode, a.ptro# AS ro, a.ptswid AS writer, 
  a.ptckey AS customer, a.ptdate AS opendate, //a.ptcdat AS closedate, a.ptfcdt AS finalclosedate,
  a.ptvin AS vin, 
  CASE 
    WHEN a.ptodom < 0 THEN
      CASE 
        WHEN a.ptmilo < 0 THEN 0
        ELSE a.ptmilo
      END
    WHEN a.ptodom = a.ptmilo THEN a.ptodom
    WHEN a.ptodom < a.ptmilo THEN a.ptmilo
    WHEN a.ptodom > a.ptmilo THEN a.ptodom
  END AS miles, a.ptptot RoParts, a.ptltot AS RoLabor, a.pthdsc AS discount, 
  a.ptcphz AS hazardous, 
  a.ptcpss + a.ptwass + a.ptinss + a.ptscss AS ShopSupplies, pttag# AS tag, 'RHDR' AS source //ptcreate 
-- INTO #RHDR  
FROM tmpSDPRHDR a
WHERE ptchk# NOT LIKE 'v%' -- excludes voids
  AND ptcnam <> '*VOIDED REPAIR ORDER*'; 
--PHDR
DROP TABLE #PHDR;
SELECT b.ptco# AS storecode, /*b.ptpkey,*/ b.ptdoc# AS ro, b.ptswid AS writer, 
  b.ptckey AS customer, b.ptdate AS opendate,
  b.ptvin AS vin, 
  CASE 
    WHEN b.ptodom < 0 THEN
      CASE 
        WHEN b.ptmilo < 0 THEN 0
        ELSE b.ptmilo
      END
    WHEN b.ptodom = b.ptmilo THEN b.ptodom
    WHEN b.ptodom < b.ptmilo THEN b.ptmilo
    WHEN b.ptodom > b.ptmilo THEN b.ptodom
  END AS miles, b.ptptot AS RoParts, b.ptltot as RoLabor, b.pthdsc AS discount, 
  b.ptcphz AS hazardous, 
  b.ptcpss + b.ptwass + b.ptinss + b.ptscss AS ShopSupplies, b.pttag# AS tag, 'PHDR' AS source //b.ptcreate, 'PDET' AS source 
-- SELECT *  
INTO #PHDR
FROM tmpPDPPHDR b
WHERE b.ptdtyp = 'RO'
  AND b.ptdoc# <> ''
  AND EXISTS ( -- exclude ros with no lines
    SELECT 1
    FROM tmpPDPPDET
    WHERE ptpkey = b.ptpkey) 
  AND ptchk# NOT LIKE 'v%'
  AND ptcnam <> '*VOIDED REPAIR ORDER*'  

-- a list of dups, eg, ro exists in RHDR & PHDR excluding ptcreate AND source  
SELECT trim(ro)
FROM (  
  SELECT *
  FROM #RHDR
  UNION 
  SELECT *
  FROM #PHDR) a
GROUP BY ro HAVING COUNT(*) > 1 

-- USING that list of dups, ADD source
SELECT *
FROM (
  SELECT *
  FROM #RHDR
  UNION 
  SELECT *
  FROM #PHDR) a
WHERE ro IN ('16122914','16124425','16124677','16125691','16125991','16126602',
  '16126607','18023400','18023735','19139958','19142002','19142292','19142363',
  '19142675','2682308','2683408','2683467','2683966','2684150','2684151','2684165',
  '2684172','2684177','2684178','2684231','2684242','2684285','2684317','2684347',
  '2684349','2684350','2684352','2684354','2684356','2684361','2684365','2684369',
  '2684372','2684374','2684376','2684379','2684380','2684381','2684384','2684397',
  '2684413','2684416','2684423','2684434','2684440','2684449','2684469','2684486',
  '2684503','2684505')  
ORDER BY ro, source  


thought some of the dups might have NULL closedate, but none of them DO

ok, the theory IS, IF the values conflict, THEN, one of the sources has a zero value
AND it IS the non zero value that should be persisted

the assumption IS that there are no differences WHERE both values are non zero

-- this will cycle thru the individual amounts
-- IN this one snapshot, shopsupplies are the only thing to bring over FROM PHDR
  SELECT a.ro, a.source, a.hazardous, b.ro, b.source, b.hazardous
  FROM #RHDR a
  INNER JOIN #PHDR b ON a.ro = b.ro
  WHERE a.hazardous <> b.hazardous
  ORDER BY a.ro
-- ops i am assuming the only differences IN the dups IS IN the amounts, need to check for
-- other discrepancies
-- ok, everything ELSE looks ok
  SELECT a.ro, a.source, b.ro, b.source
  FROM #RHDR a
  INNER JOIN #PHDR b ON a.ro = b.ro
  WHERE (
    a.writer <> b.writer OR 
    a.customer <> b.customer OR
    a.opendate <> b.opendate OR
    a.vin <> b.vin OR
    a.miles <> b.miles)
  ORDER BY a.ro  
-- for the SET of data tested today, the above assumptions are correct, sir

-- so i am thinking, FROM the scrape, generate the one line/line data AND DO a MERGE ON factRepairOrder
-- base TABLE FROM RHDR, ADD rows FROM PHDR, UPDATE values ON matched rows based ON non zero value  
/*  
-- the notion IS that nothing that EXISTS solely IN SDPR will be updated, those are ALL final closed
SELECT * FROM tmpsdprhdr a WHERE ptfcdt <> '12/31/9999' AND EXISTS (SELECT 1 FROM tmpPDPPHDR WHERE ptdoc# = a.ptro#) 
rebuild the factRepairOrder FROM tmpRO AND replace ALL rows IN factRepairOrder with
the rows FROM tmpRO
that could have an effect ON any stored results based ON previous versions of factRepairOrder
that IS going to happen anyway, IN that this IS an accumlating snapshot
the new replacement rows have the current values AND dimKeys

i am thinking, any changes to a row must be done IN PHDR/PDET AND THEN gets
propagated to SHDR/SDET

-- 8/19
only take SDR with a final CLOSE date, everything ELSE comes FROM PHDR
?? 8/21 i LIKE the sound of this, but don't know IF i have done anything about it
what it looks LIKE i have done IS to take all of SHDR INTO tmpRO include PHDR WHERE NOT alreadey EXISTS IN tmpro
THEN UPDATE tmpRO with PHDR data

***********************************************************************************************************************************/


DROP TABLE tmpRO;
CREATE TABLE tmpRO (
  storecode cichar(3),
  ro cichar(9),
  writer cichar(3),
  customer integer,
  opendate date,
  closedate date,
  finalclosedate date,
  vin cichar(17),
  miles integer,
  roParts money,
  roLabor money,
  discount money,
  hazardous money,
  shopsupplies money,
  tag cichar(5),
  createTS timestamp, 
  roStatus cichar(24)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpRO','tmpRO.adi','PK','storecode;ro',
   '',2051,512, '' );  
DELETE FROM tmpRO;      
INSERT INTO tmpRO
SELECT a.ptco# AS storecode, a.ptro# AS ro, a.ptswid AS writer, 
  a.ptckey AS customer, a.ptdate AS opendate, a.ptcdat AS closedate, a.ptfcdt AS finalclosedate,
  a.ptvin AS vin, 
  CASE 
    WHEN a.ptodom < 0 THEN
      CASE 
        WHEN a.ptmilo < 0 THEN 0
        ELSE a.ptmilo
      END
    WHEN a.ptodom = a.ptmilo THEN a.ptodom
    WHEN a.ptodom < a.ptmilo THEN a.ptmilo
    WHEN a.ptodom > a.ptmilo THEN a.ptodom
  END AS miles, a.ptptot RoParts, a.ptltot AS RoLabor, a.pthdsc AS discount, 
  a.ptcphz AS hazardous, 
  a.ptcpss + a.ptwass + a.ptinss + a.ptscss AS ShopSupplies, 
  IIF(pttag# = '', 'N/A', pttag#) AS tag, 
  ptcreate, 'Closed'
FROM tmpSDPRHDR a
WHERE ptchk# NOT LIKE 'v%' -- excludes voids
  AND ptcnam <> '*VOIDED REPAIR ORDER*'
  AND ptco# IN ('RY1','RY2');                                                 
-- PDP WHERE NOT EXISTS IN tmpR  
INSERT INTO tmpRO  
SELECT b.ptco# AS storecode, b.ptdoc# AS ro, b.ptswid AS writer, 
  b.ptckey AS customer, b.ptdate AS opendate, '12/31/9999','12/31/9999',
  b.ptvin AS vin, 
  CASE 
    WHEN b.ptodom < 0 THEN
      CASE 
        WHEN b.ptmilo < 0 THEN 0
        ELSE b.ptmilo
      END
    WHEN b.ptodom = b.ptmilo THEN b.ptodom
    WHEN b.ptodom < b.ptmilo THEN b.ptmilo
    WHEN b.ptodom > b.ptmilo THEN b.ptodom
  END AS miles, b.ptptot AS RoParts, b.ptltot as RoLabor, b.pthdsc AS discount, 
  b.ptcphz AS hazardous, 
  b.ptcpss + b.ptwass + b.ptinss + b.ptscss AS ShopSupplies, 
  IIF(pttag# = '', 'N/A', pttag#) AS tag,
  b.ptcreate, c.rostatus
-- SELECT *  
FROM tmpPDPPHDR b                                                        
LEFT JOIN dimRoStatus c ON b.ptrsts = c.roStatusCode
WHERE b.ptdtyp = 'RO'
  AND ptco# IN ('RY1','RY2')                                                  
  AND b.ptdoc# <> ''
  AND EXISTS ( -- exclude ros with no lines
    SELECT 1
    FROM tmpPDPPDET                                                     
    WHERE ptpkey = b.ptpkey) 
  AND ptchk# NOT LIKE 'v%'
  AND ptcnam <> '*VOIDED REPAIR ORDER*'
  AND NOT EXISTS (
    SELECT 1
    FROM tmpRO
    WHERE ro = b.ptdoc#);  
-- UPDATE values IN tmpRO WHERE ro EXISTS IN both RHDR & PHDR
-- a: RHDR values FROM tmpRO, d: PHDR values
UPDATE tmpRO
SET roparts =
      CASE 
        WHEN a.roparts = d.roparts THEN a.roparts
        ELSE
          CASE WHEN a.roparts = 0 THEN d.roparts ELSE a.roparts END
      END,
    rolabor = 
      CASE 
        WHEN a.rolabor = d.rolabor THEN a.rolabor
        ELSE
          CASE WHEN a.rolabor = 0 THEN d.rolabor ELSE a.rolabor END
      END,
    discount = 
      CASE 
        WHEN a.discount = d.discount THEN a.discount
        ELSE
          CASE WHEN a.discount = 0 THEN d.discount ELSE a.discount END
      END,
    hazardous = 
      CASE 
        WHEN a.hazardous = d.hazardous THEN a.hazardous
        ELSE
          CASE WHEN a.hazardous = 0 THEN d.hazardous ELSE a.hazardous END
      END,
    shopsupplies = 
      CASE 
        WHEN a.shopsupplies = d.shopsupplies THEN a.shopsupplies
        ELSE
          CASE WHEN a.shopsupplies = 0 THEN d.shopsupplies ELSE a.roparts END
      END,
    rostatus = coalesce(d.roStatus, 'Closed')        
FROM tmpRO a
INNER JOIN (
  SELECT b.ptco# AS storecode, b.ptdoc# AS ro, b.ptswid AS writer, 
    b.ptckey AS customer, b.ptdate AS opendate, '12/31/9999' AS closedate,
    '12/31/9999' AS finalclosedate, b.ptvin AS vin, 
    CASE 
      WHEN b.ptodom < 0 THEN
        CASE 
          WHEN b.ptmilo < 0 THEN 0
          ELSE b.ptmilo
        END
      WHEN b.ptodom = b.ptmilo THEN b.ptodom
      WHEN b.ptodom < b.ptmilo THEN b.ptmilo
      WHEN b.ptodom > b.ptmilo THEN b.ptodom
    END AS miles, b.ptptot AS RoParts, b.ptltot as RoLabor, b.pthdsc AS discount, 
    b.ptcphz AS hazardous, 
    b.ptcpss + b.ptwass + b.ptinss + b.ptscss AS ShopSupplies, b.pttag# AS tag, 
    b.ptcreate, c.rostatus
  FROM tmpPDPPHDR b                                                    
  LEFT JOIN dimRoStatus c ON b.ptrsts = c.roStatusCode
  WHERE b.ptdtyp = 'RO'
    AND b.ptdoc# <> ''
    AND EXISTS ( -- exclude ros with no lines
      SELECT 1
      FROM tmpPDPPDET                                                   
      WHERE ptpkey = b.ptpkey) 
    AND ptchk# NOT LIKE 'v%'
    AND ptcnam <> '*VOIDED REPAIR ORDER*') d ON a.ro = d.ro
WHERE (a.roparts <> d.roparts 
  OR a.rolabor <> d.rolabor   
  OR a.discount <> d.discount
  OR a.hazardous <> d.hazardous
  OR a.shopsupplies <> d.shopsupplies
  OR a.rostatus <> d.rostatus);
  
---------------------- this whacks the voids ----------------------------------
--NOT yet sure WHERE this goes IN the process, but early, for sure
-- it becomes necessary to DO a full scan against the stgArkona tables
-- an ro can become void at any time
DELETE 
-- SELECT *
FROM factRepairOrder                                                                                                           
WHERE ro IN (        
SELECT ptro#
FROM stgArkonaSDPRHDR a
WHERE a.ptchk# LIKE 'v%'
    OR a.ptcnam = '*VOIDED REPAIR ORDER*'
UNION     
SELECT ptdoc#
FROM stgArkonaPDPPHDR a
WHERE a.ptchk# LIKE 'v%'
    OR a.ptcnam = '*VOIDED REPAIR ORDER*') 
       



-- question now becomes are ALL the dimensions prepared?
-- DO ro & line separately?

-- ro level gets RoFlagHours, RoPaintMaterials FROM DET
DROP TABLE tmpRoKeys;
CREATE TABLE tmpRoKeys (
  storecode cichar(3),
  ro cichar(9),
  tag cichar(5),
  opendatekey integer,
  closedatekey integer,
  finalclosedatekey integer,  
  servicewriterkey integer,
  customerkey integer,
  vehiclekey integer,
  rocommentkey integer, //**** dimension state?
  rostatuskey integer,
  rolaborsales money default '0',
  ropartssales money default '0',
  roflaghours double(2) default '0',
  miles integer default '0',
  createTS timestamp default '12/31/9999 00:00:01',
  roshopsupplies money default '0',
  rodiscount money default '0',
  rohazardous money default '0',  
  ropaintmaterials money default '0') IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpROKeys','tmpROKeys.adi','PK','storecode;ro',
   '',2051,512, '' );    
-- nullable  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','storecode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','ro', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','tag', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','opendatekey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','closedatekey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','finalclosedatekey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','servicewriterkey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','customerkey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','vehiclekey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','rocommentkey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoKeys','rostatuskey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );                      

DELETE FROM tmpRoKeys;  
INSERT INTO tmpRoKeys (storecode,ro,tag,opendatekey,closedatekey,
  finalclosedatekey,servicewriterkey,customerkey,vehiclekey,
  rocommentkey,rostatuskey,rolaborsales,ropartssales,
  miles,createTS,roshopsupplies,rodiscount,
  rohazardous)
SELECT a.storecode, a.ro, coalesce(a.tag, 'N/A') AS tag,b.datekey,c.datekey,
  d.datekey, 
  coalesce(e.servicewriterkey, m.servicewriterkey), 
  coalesce(f.customerkey, k.customerkey) as customerkey, 
  coalesce(g.vehiclekey, n.VehicleKey) AS VehicleKey,
  coalesce(h.rocommentkey, i.rocommentkey) AS rocommentkey, rostatuskey, 
  rolabor, roparts,miles, createts, shopsupplies, discount,
  hazardous
FROM tmpRO a
LEFT JOIN day b ON a.opendate = b.thedate
LEFT JOIN day c ON a.closedate = c.thedate
LEFT JOIN day d ON a.finalclosedate = d.thedate
LEFT JOIN dimServiceWriter e ON a.storecode = e.storecode
  AND a.writer = e.writernumber
  AND e.active = true
  AND b.thedate BETWEEN e.servicewriterkeyfromdate AND e.servicewriterkeythrudate
LEFT JOIN dimServiceWriter m ON a.storecode = m.storecode
  AND m.writernumber = 'UNK'  
LEFT JOIN dimCustomer f ON a.customer = f.bnkey  
LEFT JOIN dimCustomer k ON 1 = 1
  AND k.fullname = 'unknown'
LEFT JOIN dimVehicle g ON a.vin = g.vin 
LEFT JOIN dimVehicle n ON 1 = 1 
  AND n.vin = 'UNKNOWN'
LEFT JOIN dimRoComment h ON a.storecode = h.storecode AND a.ro = h.ro
LEFT JOIN dimRoComment i ON a.storecode = i.storecode
  AND i.ro = 'N/A'
LEFT JOIN dimRoStatus j ON a.roStatus = j.roStatus;
 

-- 8/20 ok, i think i have ALL the pieces below
need to define what ALL it IS that needs to get done
1. are the existing techgroupkey values IN factRepairOrder correct? i think NOT 
   but DO NOT actually know
   get that straightened out
   8/23/ done
2. nightly
   a. UPDATE brTechGroup  wait, wtf, UPDATE brTechGroup??? i DO NOT think so
      what needs to be done IS ADD any new rows that are need to brTechGroup AND dimTechGroup
   b. UPDATE the tmpKeys TABLE (based ON the nightly scrape of P/RHDR & P/DET
      
-- 8/19  need the line keys, THEN combine tmpRO/LineKeys INTO tmpFactRepairOrder
-- ro level gets RoFlagHours, RoPaintMaterials FROM DET
-- need line, linedatekey, opcodekey, corcodekey, techgroupkey, statuskey, flaghours, partssales, sublet

-- techbridge has to be separate
-- 8/22 corCodeGroup has to be separate
-- ok, ON to sdprdet
DROP TABLE tmpRoLine;
CREATE TABLE tmpRoLine (
  storecode cichar(3),
  ro cichar(9),
  line integer, 
  linedate date,
  laborsales money,
  sublet money,
  paintmaterials money,
  servtype cichar(2),
  paytype cichar(1),
  opcode cichar(10),
  linehours double(2) default '0',
  status cichar(24)) IN database; 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpROLine','tmpROLione.adi','PK','ro;line',
   '',2051,512, '' );  
   
DELETE FROM tmpRoLine;     
INSERT INTO tmpROLine     
SELECT a.ptco# AS storecode, a.ptro# AS ro, a.ptline AS line,
  min(CASE WHEN ptltyp = 'A' THEN a.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS LineDate,
  coalesce(SUM(CASE WHEN a.ptcode = 'TT' AND a.ptltyp = 'L' THEN ptlamt END), 0) AS LaborSales,
  coalesce(SUM(CASE WHEN a.ptcode = 'SL' AND a.ptltyp = 'N' THEN ptlamt END), 0) AS Sublet, 
  coalesce(SUM(CASE WHEN a.ptcode = 'PM' AND a.ptltyp = 'M' THEN ptlamt END), 0) AS PaintMaterials, 
  MAX(CASE WHEN a.ptltyp = 'A' THEN a.ptsvctyp END) AS ServType,
  MAX(CASE WHEN a.ptltyp = 'A' THEN a.ptlpym END) AS PayType,
  MAX(CASE WHEN a.ptltyp = 'A' THEN a.ptlopc END) AS OpCode,
  coalesce( -- the negative hour fix   
    SUM(
      CASE 
        WHEN ptltyp = 'L' AND ptcode = 'TT' AND ptlhrs < 0 THEN 0
        WHEN ptltyp = 'L' AND ptcode = 'TT' AND ptlhrs >= 0 THEN ptlhrs
      END), 0) AS LineHours,
  'Closed' AS status
FROM tmpSDPRDET a  
INNER JOIN tmpRO b ON a.ptro# = b.ro 
GROUP BY ptco#, ptro#, ptline;


INSERT INTO tmpRoLine
SELECT b.ptco# AS storecode, a.ptdoc# AS ro, b.ptline,
  min(CASE WHEN b.ptltyp = 'A' THEN b.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS LineDate,
  coalesce(SUM(CASE WHEN b.ptcode = 'TT' AND b.ptltyp = 'L' THEN b.ptnet END), 0) AS LaborSales,
  coalesce(SUM(CASE WHEN b.ptcode = 'SL' AND b.ptltyp = 'N' THEN b.ptnet END), 0) AS Sublet,
  coalesce(SUM(CASE WHEN b.ptcode = 'PM' AND b.ptltyp = 'M' THEN b.ptnet END), 0) AS PaintMaterials, 
  MAX(CASE WHEN b.ptltyp = 'A' THEN b.ptsvctyp END) AS ServType,
  MAX(CASE WHEN b.ptltyp = 'A' THEN b.ptlpym END) AS PayType,
  MAX(CASE WHEN b.ptltyp = 'A' THEN b.ptlopc END) AS OpCode,
  coalesce( -- the negative hour fix   
    SUM(
      CASE 
        WHEN ptltyp = 'L' AND ptcode = 'TT' AND ptlhrs < 0 THEN 0
        WHEN ptltyp = 'L' AND ptcode = 'TT' AND ptlhrs >= 0 THEN ptlhrs
      END), 0) AS LineHours,
  MAX(CASE WHEN b.ptltyp = 'A' AND ptlsts = 'I' THEN 'Open' ELSE 'Closed' END) AS status  
FROM tmpPDPPHDR a
INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
WHERE a.ptdtyp = 'RO'
  AND a.ptdoc# <> ''
  AND a.ptchk# NOT LIKE 'v%'
  AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
  AND b.ptline < 900
  AND NOT EXISTS (
    SELECT 1
    FROM tmpRoLine
    WHERE ro = a.ptdoc#
      AND line = b.ptline)
GROUP BY b.ptco#, a.ptdoc#, b.ptline;

-- hmmm, just UPDATE it with PDET values, IF they are wrong, WHEN the ro closes,
-- the final source RDET will prevail
-- subset are those ro/lines that exist IN PDET & RDET AND have different values
-- 8/23 don't think so, need to UPDATE to the non 0 valeu for laborsales AND line hours
UPDATE tmpROLine
SET linedate = d.linedate,
    laborsales = CASE WHEN a.laborsales > d.laborsales THEN a.laborsales ELSE d.laborsales END,
    sublet = CASE WHEN a.sublet > d.sublet THEN a.sublet ELSE d.sublet END,
    paintmaterials = CASE WHEN a.paintmaterials > d.paintmaterials THEN a.paintmaterials ELSE d.paintmaterials END,
    servtype = d.servtype,
    paytype = d.paytype,
    opcode = d.opcode,
    linehours = case when a.linehours > d.linehours then a.linehours else d.linehours END,
    status = d.status
FROM tmpROLine a 
INNER JOIN (
  SELECT c.ptco# AS storecode, b.ptdoc# AS ro, c.ptline,
    min(CASE WHEN c.ptltyp = 'A' THEN c.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS LineDate,
    coalesce(SUM(CASE WHEN c.ptcode = 'TT' AND c.ptltyp = 'L' THEN c.ptnet END), 0) AS LaborSales,
    coalesce(SUM(CASE WHEN c.ptcode = 'SL' AND c.ptltyp = 'N' THEN c.ptnet END), 0) AS Sublet,
    coalesce(SUM(CASE WHEN c.ptcode = 'PM' AND c.ptltyp = 'M' THEN c.ptnet END), 0) AS PaintMaterials, 
    MAX(CASE WHEN c.ptltyp = 'A' THEN c.ptsvctyp END) AS ServType,
    MAX(CASE WHEN c.ptltyp = 'A' THEN c.ptlpym END) AS PayType,
    MAX(CASE WHEN c.ptltyp = 'A' THEN c.ptlopc END) AS OpCode,
    coalesce(SUM(CASE WHEN c.ptltyp = 'L' AND c.ptcode = 'tt' THEN c.ptlhrs END), 0) AS LineHours,
    MAX(CASE WHEN c.ptltyp = 'A' AND c.ptlsts = 'I' THEN 'Open' ELSE 'Closed' END) AS status  
  FROM tmpPDPPHDR b                                      
  INNER JOIN tmpPDPPDET c ON b.ptpkey = c.ptpkey
  WHERE b.ptdtyp = 'RO'
    AND b.ptdoc# <> ''
    AND b.ptchk# NOT LIKE 'v%'
    AND b.ptcnam <> '*VOIDED REPAIR ORDER*'
  GROUP BY c.ptco#, b.ptdoc#, c.ptline) d ON a.ro = d.ro AND a.line = d.ptline
WHERE (a.linedate <> d.linedate
  OR a.laborsales <> d.laborsales 
  OR a.sublet <> d.sublet
  OR a.paintmaterials <> d.paintmaterials
  OR a.servtype <> d.servtype
  OR a.paytype <> d.paytype
  OR a.opcode <> d.opcode
  OR round(a.linehours, 2) <> round(d.linehours, 2)
  OR a.status <> d.status collate ads_default_ci);
  
DROP TABLE tmpRoLineKeys;
CREATE TABLE tmpRoLineKeys (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  linedatekey integer,
  ServiceTypeKey integer,
  PaymentTypeKey integer,
  cccKey integer, 
  OpCodeKey integer, 
  CorCodeGroupKey integer,
  TechGroupKey integer, 
  StatusKey integer, 
  FlagHours double(4) default '0', 
  LaborSales money default '0',
  PartsSales money default '0', -- this info has to come FROM pdpTDET
  Sublet money default '0',
  RoFlagHours double(4), 
  RoPaintMaterials money default '0') IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpROLineKeys','tmpROLineKeys.adi','PK','storecode;ro;line',
   '',2051,512, '' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tmpROLineKeys','tmpROLineKeys.adi','RO','storecode;ro',
   '',2,512, '' );     
-- nullable  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoLineKeys','storecode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoLineKeys','ro', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoLineKeys','line', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoLineKeys','linedatekey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoLineKeys','ccckey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoLineKeys','opcodekey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoLineKeys','CorCodeGroupKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoLineKeys','TechGroupKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tmpRoLineKeys','StatusKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  

/* DO NOT know why, can NOT get this to WORK: 5138 request for a property for an object, bu property NOT set ???
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimTechGroup-factRepairOrder');      
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'tmpRoKeys-tmpRoLineKeys','tmpRoKeys','tmpRoLineKeys','RO',2,2,NULL,'',''); 
*/ 

-- get the ro level values that have to be derived FROM Line level
-- ROFlagHours, ROPaintMAterials   
DELETE FROM tmpRoLineKeys;
INSERT INTO tmpRoLineKeys                                      
SELECT a.storecode, a.ro, a.line, 
  coalesce(d.datekey, dd.datekey) AS linedatekey, 
  coalesce(e.servicetypekey, ee.servicetypekey),
  coalesce(f.PaymentTypeKey, ff.PaymentTypeKey), 
  coalesce(g.cccKey, gg.cccKey) AS cccKey,
  coalesce(h.opcodekey, i.opcodekey) AS OpCodeKey,
  j.CorCodeGroupKey, k.TechGroupKey, m.linestatuskey, a.linehours as FlagHours,
  a.laborsales, 0 AS partsSales, a.sublet,
  b.RoFlagHours, c.RoPaintMaterials
-- select *  
FROM tmpROLine a
LEFT JOIN ( -- RoFlagHours
  SELECT storecode, ro, round(SUM(linehours),4) AS RoFlagHours
  FROM tmproline
  GROUP BY storecode, ro) b ON a.storecode = b.storecode AND a.ro = b.ro
LEFT JOIN ( --RoPaintMaterials
  SELECT storecode, ro, SUM(paintmaterials) AS RoPaintMaterials
  FROM tmproline
  GROUP BY storecode, ro) c ON a.storecode = c.storecode AND a.ro = c.ro
LEFT JOIN day d ON a.linedate = d.thedate 
LEFT JOIN day dd ON 1 = 1
  AND dd.datetype <> 'date'
LEFT JOIN dimServiceType e ON a.servtype = e.servicetypecode
LEFT JOIN dimServiceType ee ON 1 = 1 AND ee.servicetypecode = 'UN'
LEFT JOIN dimPaymentType f ON a.paytype = f.paymentTypeCode
LEFT JOIN dimPaymentType ff ON 1 = 1 AND ff.paymentTypeCode = 'U'
LEFT JOIN dimCCC g ON a.storecode = g.storecode AND a.ro = g.ro AND a.line = g.line
LEFT JOIN dimCCC gg ON a.storecode = gg.storecode
  AND gg.ro = 'N/A'
LEFT JOIN dimOpCode h ON a.storecode = h.storecode AND a.opcode = h.opcode
LEFT JOIN dimOpCode i ON a.storecode = i.storecode
  AND i.opcode = 'N/A'
LEFT JOIN (  
  SELECT a.storecode, a.ro, a.line, coalesce(c.CorCodeGroupKey, d.corcodegroupkey) AS CorCodeGroupKey
  FROM tmpRoLine a
  LEFT JOIN xfmDimCorCodeGroup3 b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  LEFT JOIN dimCorCodeGroup c ON b.corcodekeygroup = c.flatgroup
  LEFT JOIN dimCorCodeGroup d ON 1 = 1 
    AND d.corcodeGroupKey = (
      SELECT corcodegroupkey
      FROM brCorCodeGroup
      WHERE opcodekey = (
        SELECT opcodekey
        FROM dimopcode
        WHERE storecode = 'RY1'
          AND opcode = 'N/A'))) j ON a.storecode = j.storecode AND a.ro = j.ro AND a.line = j.line
LEFT JOIN (
  SELECT a.storecode, a.ro, a.line, coalesce(c.TechGroupKey, d.Techgroupkey) AS TechGroupKey
  FROM tmpRoLine a
  LEFT JOIN xfmDimTechGroup3 b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line  
  LEFT JOIN dimTechGroup c ON b.TechKeyGroup = c.flatgroup
  LEFT JOIN dimTechGroup d ON 1 = 1
    AND d.TechGroupKey = (
      SELECT TechGroupKey
      FROM brTechGroup
      WHERE Techkey = (
        SELECT Techkey
        FROM dimtech
        WHERE storecode = 'RY1'
           AND Technumber = 'N/A'))) k ON a.storecode = k.storecode AND a.ro = k.ro AND a.line = k.line         
LEFT JOIN dimLineStatus m ON a.status = m.linestatus;

DELETE FROM tmpFactRepairOrder; -- identical structure to factRepairOrder
INSERT INTO tmpFactRepairOrder (storecode, ro, line, tag, opendatekey, closedatekey,
  finalclosedatekey, linedatekey, servicewriterkey, customerkey,
  vehiclekey, servicetypekey, paymenttypekey,ccckey,rocommentkey,
  opcodekey,corcodegroupkey,techgroupkey,rostatuskey,statuskey,
  rolaborsales,ropartssales,flaghours,roflaghours,miles,
  rocreatedts,laborsales,partssales,sublet,roshopsupplies,
  rodiscount,rohazardousmaterials,ropaintmaterials)
SELECT a.storecode, a.ro, b.line, a.tag, a.opendatekey, a.closedatekey, 
  a.finalclosedatekey, b.linedatekey, a.servicewriterkey, a.customerkey, 
  a.vehiclekey, b.servicetypekey, b.paymenttypekey, b.ccckey, a.rocommentkey, 
  b.opcodekey, b.corcodegroupkey, b.techgroupkey, a.rostatuskey, b.statuskey, 
  a.rolaborsales, a.ropartssales, b.flaghours, b.roflaghours, a.miles, 
  a.createTS, b.laborsales, b.partssales, b.sublet, a.roshopsupplies, 
  a.rodiscount, a.rohazardous, b.ropaintmaterials
FROM tmpRoKeys a
INNER JOIN tmpRoLineKeys b ON a.storecode = b.storecode AND a.ro = b.ro 
  AND b.line < 900;
  
--< Dependencies & zProc ------------------------------------------------------   
  
INSERT INTO zProcExecutables values('factRepairOrder','realtime',CAST('03:00:15' AS sql_time),0);

INSERT INTO zProcProcedures values('factRepairOrder','factRepairOrder',0,'realtime');
INSERT INTO DependencyObjects values('factRepairOrder','factRepairOrder','factRepairOrder');

INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b on 1 = 1
  WHERE a.dObject = 'factRepairOrder'
  AND b.dObject = 'tmpPDPPHDR';     
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b on 1 = 1
  WHERE a.dObject = 'factRepairOrder'
  AND b.dObject = 'tmpPDPPDET';   
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b on 1 = 1
  WHERE a.dObject = 'factRepairOrder'
  AND b.dObject = 'tmpSDPRHDR';   
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b on 1 = 1
  WHERE a.dObject = 'factRepairOrder'
  AND b.dObject = 'tmpSDPRDET';        
  
--/> Dependencies & zProc ------------------------------------------------------     
  
--< need to fix the fucked up technumber AS flaghours IN QL --------------------------
    -- fixes quick lube only
    UPDATE xfmROToday
    SET ptlhrs = 
        CASE
          WHEN z.opcode LIKE 'pdq%' or z.opcode = 'lof' THEN .37
          WHEN z.opcode IN ('13a','bulb') THEN .2
          WHEN z.opcode = 'cl' THEN .9
          WHEN z.opcode = 'cp' THEN 2
          WHEN z.opcode = 'yx10aa' THEN 1.1
          WHEN z.opcode = 'wash5' THEN 0
          ELSE xfmROToday.ptlhrs
        END
 SELECT FROM xfmROToday
    FROM (
      SELECT *
      FROM ( -- 1 row per line
        SELECT ptco#, ptro#, ptline,
          max(CASE WHEN ptltyp = 'A' THEN linedate END) AS LineDate,
          MAX(CASE WHEN ptltyp = 'A' THEN ptsvctyp END) AS ServType,
          MAX(CASE WHEN ptltyp = 'A' THEN ptlpym END) AS PayType,
          MAX(CASE WHEN ptltyp = 'A' THEN ptlopc END) AS OpCode,
          MAX(CASE WHEN ptltyp = 'L' AND ptcode = 'CR' THEN ptcrlo END) AS CorCode,
          SUM(CASE WHEN ptltyp = 'L' AND ptcode = 'tt' THEN ptlhrs ELSE 0 END) AS LineHours
        FROM xfmROToday
        WHERE status <> 'VOID'
        GROUP BY ptco#, ptro#, ptline) x 
        WHERE linehours > 10 AND servtype = 'ql') z
    WHERE xfmROToday.ptcode = 'TT'
      AND xfmROToday.ptro# = z.ptro#
      AND xfmROToday.ptline = z.ptline;
      
SELECT * FROM dimServiceType
   
SELECT a.ro, a.line, flaghours, b.servicetypecode, d.OpCode
FROM factRepairOrder a
INNER JOIN dimServiceType b ON a.ServiceTypeKey = b.ServiceTypeKey 
  AND b.ServiceTypeCode = 'QL'   
INNER JOIN dimOpCode d ON a.OpCodeKey = d.OpCodeKey
  AND (
    d.opcode LIKE 'pdq%' OR
    d.opcode = 'lof' OR
    d.opcode = '13a' OR
    d.opcode = 'bulb' OR
    d.opcode = 'cl' OR
    d.opcode = 'yx10aa' OR
    d.opcode = 'wash5')
WHERE flaghours > 10   
--/> need to fix the fucked up technumber AS flaghours IN QL --------------------------  
  
-- 8/25 Done AND Good 
ok, i am getting ALL hinky about deleting from factRepairOrder based ON ro IN tmpFactRepairOrder
are there lines for an ro IN fact that are NOT IN tmp?  
this would imply NOT: 
select *
FROM factRepairORder a
WHERE EXISTS (
  SELECT 1
  FROM tmpfactrepairorder
  WHERE ro = a.ro)
AND NOT EXISTS (
  SELECT 1 
  FROM tmpfactrepairorder
  WHERE line = a.line)  
 
SELECT curdate() - 45 FROM system.iota  
this helps a little, at least clarifying what i am thinking,
here IS the scenario
ro opened 60 days ago, NOT closed yet, so it falls outside the nightly scrape of open/closed/finalclose < 45 days ago
BUT a line was added last week, so the fucking line does NOT show up IN the scrape
this would seem to indicate that it happens damn infrequently
additionally it shows, that of those opened > 45 days ago the line was added within 45 days of the ro being opened! so there

select b.thedate, c.opendatekey, c.closedatekey, c.finalclosedatekey, a.*
FROM factRepairOrder a
LEFT JOIN day b ON a.opendatekey = b.datekey
LEFT JOIN tmpfactRepairOrder c ON a.ro = c.ro AND a.line = c.line
WHERE (
  a.linedatekey <> a.opendatekey AND 
  a.linedatekey <> a.closedatekey AND 
  a.linedatekey <> a.finalclosedatekey)
AND CAST(a.rocreatedts AS sql_date) BETWEEN '01/01/2011' AND curdate()    

but one goofy thing, why are 16081577, 16118851, 16119126 & 16121647 IN the scrape?, duh, because they are IN pdp
wahoo? pdp to the rescue, AS long AS it IS OPEN, it IS IN pdp AND IS IN THE SCRAPE!!!!!
  
should be ok :)??  

DELETE 
FROM factRepairOrder
WHERE ro IN (
  SELECT ro
  FROM tmpFactRepairOrder);
  
INSERT INTO factRepairOrder    
SELECT * FROM tmpFactRepairOrder; 

delete FROM factRepairOrder WHERE ro = '2683750'  

INSERT INTO factRepairOrder
SELECT *
FROM  tmpFactRepairOrder 
WHERE ro = '2683750'
  
  
  
-- 8/25
--   DO some "unknown" testing


 
-- shows differences BETWEEN  factRepairORder AND tmpFactRepairORder  
-- for lines that appear IN both
DROP TABLE #wtf;
SELECT *
INTO #wtf
FROM(  
SELECT 'tmp' AS source, a.*
FROM tmpfactRepairOrder a  
UNION 
SELECT 'fct' AS source, a.*
FROM factRepairOrder a
WHERE EXISTS (
  SELECT 1
  FROM tmpFactRepairOrder
  WHERE ro = a.ro
  AND line = a.line)) x
ORDER BY ro, line, source  
-- which allows me to DO a quick AND dirty comparison ON some shit
-- to TRY to pacify my anxiety about deleting lines IN factRepairOrder
-- AND replacing them with lines FROM tmpFactRepairOrder AS a means of updating the Acccumulating Snapshot
1. laborsales: IN fucked up ro, 16117974, tmp IS correct
SELECT a.ro, a.line, a.laborsales AS fct, b.laborsales AS tmp
FROM (
  SELECT *
  FROM #wtf
  WHERE source = 'fct') a
INNER JOIN (
  SELECT * 
  FROM #wtf
  WHERE source = 'tmp') b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
WHERE a.laborsales > b.laborsales    
2. flaghours
16123311, 16124683, 1125751, 16125775, 16125803, 16126149,16126282,16126297,16126403,: bevs fucking negative hours, causing line 4 IN tmp to be 
ALL others, tmp IS correct,
get the fucking negative hours out of tmp

SELECT a.ro, a.line, a.flaghours AS fct, b.flaghours AS tmp
FROM (
  SELECT *
  FROM #wtf
  WHERE source = 'fct') a
INNER JOIN (
  SELECT * 
  FROM #wtf
  WHERE source = 'tmp') b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
WHERE round(a.flaghours, 4) > round(b.flaghours, 4)    

-- the concern here IS the discrepancy IN ROLaborSales
-- leaning toward generating AS a SUM of lines flag hours instead of USING the 
-- value FROM SDP/PDP HDR
-- what i know for sure
-- 1.  the existing rows IN factRepairOrder ALL get RoLaborSales FROM the old
--     stgRO which got the value FROM accounting, i am no longer doing that

this IS proof enuf for me that sdprhdr.ptlot IS NOT reliable
the spot checking i did shows that summing RDET agrees with the 5250 line totals view
so, UPDATE existing factRepairOrder with a summed total
AND change the tmp generation to use the summed total
nope, forget about the tmp generation, just DO this UPDATE:
-- it only ADDs < 30 seconds to the process 
UPDATE factRepairOrder
SET rolaborsales = a.summed
FROM (
  SELECT storecode, ro, rolaborsales, SUM(laborsales) AS summed
  FROM factrepairorder
  GROUP BY storecode, ro, rolaborsales) a
WHERE a.rolaborsales <> a.summed  
AND factRepairOrder.storecode = a.storecode AND factRepairOrder.ro = a.ro; 

SELECT * FROM (
SELECT ro, rolaborsales, SUM(laborsales) AS summed 
FROM factrepairorder
WHERE finalclosedatekey <> 7306
GROUP BY ro, rolaborsales) a
where rolaborsales <> summed

  
/*
this IS proof enuf for me that sdprhdr.ptlot IS NOT reliable
select count(*) --125537
select *
from (
  select trim(a.ptro#) as ptro#, a.ptltot, a.ptdate
  from rydedata.sdprhdr a
  WHERE a.ptchk# NOT LIKE 'v%' -- excludes void
    AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
    AND a.ptco# IN ('RY1','RY2')
    and a.ptfcdt <> 0
    and a.ptdate > 20100000) m
inner join (
select trim(ptro#) as ptro#, sum(ptlamt) as ptlamt 
  from rydedata.sdprdet 
  where ptcode = 'TT' and ptltyp = 'L'
    AND ptco# IN ('RY1','RY2')
group by trim(ptro#)) n on trim(m.ptro#) = trim(n.ptro#) 
WHERE m.ptltot <> n.ptlamt
*/

SELECT * FROM (
SELECT ro, rolaborsales, SUM(laborsales) AS summed 
FROM tmpfactrepairorder
WHERE finalclosedatekey <> 7306
GROUP BY ro, rolaborsales) a


/* was concerned that factRepairOrder was missing lines
   this dissuades me of the notion
DROP TABLE #wtf;
SELECT a.ptro#, max(ptline)
INTO #wtf
FROM stgArkonaSDPRDET a
INNER JOIN stgArkonaSDPRHDR b ON a.ptro# = b.ptro# 
  AND b.ptfcdt <> '12/31/9999'
  AND b.ptchk# NOT LIKE 'v%' -- excludes voids
  AND b.ptcnam <> '*VOIDED REPAIR ORDER*'
  AND b.ptco# IN ('RY1','RY2')
  AND b.ptdate > '08/01/2009'
WHERE a.ptline < 500 
GROUP BY a.ptro#   

SELECT *
FROM (
  SELECT * FROM #wtf a
  INNER JOIN (
    SELECT ro, max(line) 
    FROM factRepairOrder 
    GROUP BY ro) b ON a.ptro# = b.ro AND a.expr <> b.expr) c
WHERE NOT EXISTS (
  SELECT ro
  FROM tmpFactRepairOrder
  WHERE ro = c.ro)
*/

-- roflaghours vs summed looks ok
select *
FROM (
SELECT a.ro, a.roflaghours,
  SUM(flaghours) AS summed
FROM factRepairOrder a
WHERE finalclosedatekey <> 7306
GROUP BY a.ro, a.roflaghours)  a
WHERE round(roflaghours, 4) <> round(summed, 4)

--< void rant -----------------------
i may well generate dimension keys that are subsequently NOT used, the dimensions are
[shaped first, so i may indeed
guess what i am thinking about are voids, it IS NOT unusual, i have realized, that 
an ro IS voided some variable number of days after it was created OR even closed
hence the need to check for AND whack void ros based ON a scrape of entire history
so anywhere i am generating data based ON SDP/PDP THEN that IS WHERE ( IN every CASE)
voids get whacked
--/> void rant ----------------------

--< ORDER of loading rant ---------------------------------------------------------
the ongoing mind fuck IS DO SDP first OR DO PDP first
IF it EXISTS IN one but NOT the other, no issue
IF it EXISTS IN both, what are the correct values
have taken a numbe rof different approahces
most recently IN dimTechGroup:
  take ALL of PDP
  THEN take only SDP that DO NOT EXISTS IN PDP (RO/Line)
the orignal stgRO
  did just the opposite, take ALL SDP AND THEN the PDP that does NOT exist IN SDP
  based ON RO/Line
what i did above, IN tmpRO
  take ALL FROM SDP
  take ALL FROM PDP that DO NOT exist IN SDP based ON RO
  THEN UPDATE the result with values FROM ALL of PDP:  
    WHERE (a.roparts <> d.roparts 
      OR a.rolabor <> d.rolabor   
      OR a.discount <> d.discount
      OR a.hazardous <> d.hazardous
      OR a.shopsupplies <> d.shopsupplies
      OR a.rostatus <> d.rostatus);  
AND IN tmpROLine
    WHERE (a.linedate <> d.linedate
      OR a.laborsales <> d.laborsales 
      OR a.sublet <> d.sublet
      OR a.paintmaterials <> d.paintmaterials
      OR a.servtype <> d.servtype
      OR a.paytype <> d.paytype
      OR a.opcode <> d.opcode
      OR round(a.linehours, 2) <> round(d.linehours, 2)
      OR a.status <> d.status collate ads_default_ci);
      
which seemed goofy AS i came back to it FROM my long sojorun INTO bridge tables,
but now seems, hmm, maybe cool, ie
  ALL SDP ROs
  ALL PDP ROs
  any conflicting values take the value FROM PDP
        
the notion of WHEN there are conflicting values BETWEEN SDP/PDP, which should
take precedence

FROM zROHeader zRODetailNightly -2.sql
RO status needs to come FROM the PDP row
closedate needs to come FROM the SDP row

which the above respects plus ALL the other values that could be updated periodically

so i guess i am ok with how this IS going, time to get ON with line keys
--/> ORDER of loading rant ---------------------------------------------------------


        