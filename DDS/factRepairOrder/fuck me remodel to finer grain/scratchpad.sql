SELECT *
FROM tmpRoKeys a
INNER JOIN tmpRoLineKeys b on a.storecode = b.storecode AND a.ro = b.ro
INNER JOIN tmpRoTechFlagCorKeys c on b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line


SELECT * FROM (
SELECT *
FROM tmpRoKeys a
INNER JOIN tmpRoLineKeys b on a.storecode = b.storecode AND a.ro = b.ro
LEFT JOIN tmpRoTechFlagCorKeys c on b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line
) x WHERE corcodekey IS NULL OR flagdatekey IS NULL OR ptlhrs IS NULL 





SELECT COUNT (*) -- 292446
FROM tmpRoKeys a
INNER JOIN tmpRoLineKeys b on a.storecode = b.storecode AND a.ro = b.ro
LEFT JOIN tmpRoTechFlagCorKeys c on b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line

SELECT COUNT (*) -- 292446
FROM tmpRoKeys a
INNER JOIN tmpRoLineKeys b on a.storecode = b.storecode AND a.ro = b.ro
INNER JOIN tmpRoTechFlagCorKeys c on b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line


SELECT *
FROM tmpRoKeys a
INNER JOIN tmpRoLineKeys b on a.storecode = b.storecode AND a.ro = b.ro
INNER JOIN tmpRoTechFlagCorKeys c on b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line
INNER JOIN day d on c.flagdatekey = d.datekey
INNER JOIN dimTech e on c.techkey = e.techkey
INNER JOIN scotest.tptechs f on e.technumber = f.technumber
WHERE d.yearmonth = 201310


SELECT *
FROM tmpRoKeys a
INNER JOIN tmpRoLineKeys b on a.storecode = b.storecode AND a.ro = b.ro
INNER JOIN tmpRoTechFlagCorKeys c on b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line
INNER JOIN day d on c.flagdatekey = d.datekey
INNER JOIN dimTech e on c.techkey = e.techkey
INNER JOIN scotest.tptechs f on e.technumber = f.technumber
WHERE d.yearmonth = 201310

-- ALL techs totaled for period
SELECT a.*, b.adjhrs
--SELECT SUM(expr) 
FROM (
  SELECT f.lastname, f.technumber, SUM(c.ptlhrs)
  FROM tmpRoKeys a
  INNER JOIN tmpRoLineKeys b on a.storecode = b.storecode AND a.ro = b.ro
  INNER JOIN tmpRoTechFlagCorKeys c on b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line
  INNER JOIN day d on c.flagdatekey = d.datekey
  INNER JOIN dimTech e on c.techkey = e.techkey
  INNER JOIN scotest.tptechs f on e.technumber = f.technumber
  WHERE d.yearmonth = 201311
  GROUP BY f.lastname, f.technumber) a
LEFT JOIN (
  select pttech, round(SUM(ptlhrs), 2) AS adjhrs
  FROM stgarkonasdpxtim
  WHERE ptdate BETWEEN '11/01/2013' AND '11/30/2013'
  GROUP BY pttech) b on a.technumber = b.pttech  
ORDER BY a.technumber

-- with tmpFactRepairOrder
SELECT a.*, b.adjhrs
--SELECT SUM(expr) 
FROM (
  SELECT f.lastname, f.technumber, SUM(a.flaghours)
  FROM FactRepairOrder a
  INNER JOIN day d on a.flagdatekey = d.datekey
  INNER JOIN dimTech e on a.techkey = e.techkey
  INNER JOIN scotest.tptechs f on e.technumber = f.technumber
  WHERE d.thedate BETWEEN '12/15/2013' AND '12/24/2013'
  GROUP BY f.lastname, f.technumber) a
LEFT JOIN (
  select pttech, round(SUM(ptlhrs), 2) AS adjhrs
  FROM stgarkonasdpxtim
  WHERE ptdate BETWEEN '12/15/2013' AND '12/24/2013'
  GROUP BY pttech) b on a.technumber = b.pttech  
ORDER BY a.technumber

individual tech by ro for period
SELECT a.*, b.adjhrs
FROM (
  SELECT f.technumber, a.ro, SUM(c.ptlhrs)
  FROM tmpRoKeys a
  INNER JOIN tmpRoLineKeys b on a.storecode = b.storecode AND a.ro = b.ro
  INNER JOIN tmpRoTechFlagCorKeys c on b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line
  INNER JOIN day d on c.flagdatekey = d.datekey
  INNER JOIN dimTech e on c.techkey = e.techkey
  INNER JOIN scotest.tptechs f on e.technumber = f.technumber
  WHERE d.yearmonth = 201311
    AND f.technumber = '631'
  GROUP BY f.technumber, a.ro) a
LEFT JOIN (
  select pttech, ptro#, round(SUM(ptlhrs), 2) AS adjhrs
  FROM stgarkonasdpxtim
  WHERE ptdate BETWEEN '11/01/2013' AND '11/30/2013'
  GROUP BY pttech, ptro#) b on a.technumber = b.pttech AND a.ro = b.ptro#   
ORDER BY a.ro


-- WHERE DO the differences show up
select * FROM tmpsdprdet WHERE ptro# = '16112297'  AND ptline = 2 ORDER BY ptseq#
SELECT b.* FROM tmppdppdet a INNER JOIN tmppdpphdr b on a.ptpkey = b.ptpkey AND b.ptdoc# = '16132359'
select * FROM tmpRoTechFlagCorSeqGroup WHERE ptro# = '16132359'
select * FROM tmpRoTechFlagCor1  WHERE ro = '16132359'
select * FROM tmpRoTechFlagCor2 WHERE ro = '16132359'

SELECT * FROM stgArkonaSDPXTIM WHERE ptro# = '16132359'


select storecode, ro, line, flagdate, pttech, ptcrlo 
FROM tmpRoTechFlagCor2
GROUP BY storecode, ro, line, flagdate, pttech, ptcrlo 
HAVING COUNT(*) > 1

-- the problem here may be unknown cor codes
-- i am NOT making a clear enough distinction BETWEEN no  corcode on the ro
-- vs AND invalid corcode (NOT IN dimOpCode)
even once i DO that, factrepairorder will NOT have a reliable PK
eg 16112297 line 2
  on 3/6/13 tech 646 transacted cor code PAINT MIX .3 hrs
                              & cor code COLOR CLEA .7 hrs
    both of those codes are nonsense, ie, they DO NOT exist IN dimOpCode 
dimOpCode current Primary Key IS Store/OpCode
want to stay IN synch with sdplopc
but a single opcode key for multiple unk opcodes will NOT work 
fuck, need to probably DO a magical upcode encoding scheme

need to be able to identify the opcode FROM sdprdet but identify the 
dimOpCode rows AS bogus
maybe IN the description ...something LIKE invalid opcode

-- put this IN sp.xfmOpCode
INSERT INTO dimOpCode(storecode, opcode, description)
SELECT ptco#, ptcrlo, 'INVALID OPCODE'
FROM (
  SELECT distinct ptco#, ptcrlo FROM tmpsdprdet WHERE ptcrlo <> '' AND ptcode = 'cr' AND ptltyp = 'l'
  UNION 
  SELECT DISTINCT ptco#, ptcrlo FROM tmppdppdet  WHERE ptcrlo <> '' AND ptcode = 'cr' AND ptltyp = 'l') a
WHERE NOT EXISTS (
  SELECT 1
  FROM dimOpcode
  WHERE storecode = a.ptco# 
    AND opcode = a.ptcrlo);



                                 
SELECT c.thedate, a.*
FROM tmpRoTechFlagCorKeys a
INNER JOIN (
  SELECT storecode, ro, line, flagdatekey, techkey, corcodekey
  FROM tmpRoTechFlagCorKeys
  GROUP BY storecode, ro, line, flagdatekey, techkey, corcodekey
  HAVING COUNT(*) > 1) b on a.ro = b.ro AND a.line = b.line
INNER JOIN day c on a.flagdatekey = c.datekey  


-- tmpRoLine bombing on NOT NULL constraint on linedate
SELECT * 
FROM tmpsdprdet a
WHERE ptline < 900
AND NOT EXISTS (
  SELECT 1
  FROM tmpsdprdet
  WHERE ptro# = a.ptro#
    AND ptline = a.ptline
    AND ptltyp = 'A')
