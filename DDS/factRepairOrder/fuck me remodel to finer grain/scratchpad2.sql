-- with tmpFactRepairOrder
SELECT a.*, b.adjhrs
--SELECT SUM(expr) 
FROM (
  SELECT f.lastname, f.technumber, SUM(a.flaghours)
  FROM FactRepairOrder a
  INNER JOIN day d on a.flagdatekey = d.datekey
  INNER JOIN dimTech e on a.techkey = e.techkey
  INNER JOIN scotest.tptechs f on e.technumber = f.technumber
  WHERE d.thedate BETWEEN '12/01/2013' AND '12/14/2013'
  GROUP BY f.lastname, f.technumber) a
LEFT JOIN (
  select pttech, round(SUM(ptlhrs), 2) AS adjhrs
  FROM stgarkonasdpxtim
  WHERE ptdate BETWEEN '12/01/2013' AND '12/14/2013'
  GROUP BY pttech) b on a.technumber = b.pttech  
ORDER BY a.technumber

--individual tech by ro for period
SELECT a.*, b.adjhrs
FROM (
  SELECT f.technumber, a.ro, SUM(a.flaghours)
  FROM factRepairORder a
  INNER JOIN day d on a.flagdatekey = d.datekey
  INNER JOIN dimTech e on a.techkey = e.techkey
  INNER JOIN scotest.tptechs f on e.technumber = f.technumber
  WHERE d.thedate BETWEEN '12/15/2013' AND '12/24/2013'
    AND f.technumber = '522'
  GROUP BY f.technumber, a.ro) a
LEFT JOIN (
  select pttech, ptro#, round(SUM(ptlhrs), 2) AS adjhrs
  FROM stgarkonasdpxtim
  WHERE ptdate BETWEEN '12/15/2013' AND '12/24/2013'
  GROUP BY pttech, ptro#) b on a.technumber = b.pttech AND a.ro = b.ptro#   
ORDER BY a.ro

SELECT * FROM stgArkonaSDPXTIM WHERE ptro# = '16139315' 

SELECT 'sdp',ptdate, pttech, ptline, ptlhrs, ptseq# FROM tmpsdprdet WHERE ptro# = '16139315' AND pttech = '629'-- AND ptline = 1
union
SELECT 'pdp',b.ptdate, pttech, ptline, ptlhrs, ptseq# FROM tmppdpphdr a inner join tmppdppdet b on a.ptpkey = b.ptpkey WHERE ptdoc# = '16139315' AND pttech = '629' --AND ptline = 1

SELECT * FROM tmpRoTechFlagCor1 WHERE ro = '16139315' AND pttech = '629' ORDER BY flagdate

SELECT * FROM tmpRoTechFlagCor2 WHERE ro = '16139315' AND pttech = '629'

SELECT * FROM tmpRoTechFlagCorSeqGroup WHERE ptro# = '16139935'

SELECT * FROM (
    select a.*, b.seqgroup
    FROM (
      SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, 
        sum(ptlhrs) AS ptlhrs --, SUM(ptlamt) AS ptlamt
      FROM tmpsdprdet
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0 
      GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech 
      UNION 
      SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#, 
        b.ptdate, pttech, sum(ptlhrs) AS ptlhrs -- , SUM(ptarla)
      FROM tmppdpphdr a
      INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0
      GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech)   a
    LEFT JOIN tmpRoTechFlagCorSeqGroup b on a.ptro# = b.ptro# 
      AND a.ptline = b.ptline 
      AND a.ptseq# = b.ptseq# AND b.flagcor = 'flag'
) x WHERE ptro# = '16138960'     


SELECT * FROM (
    select a.*, b.seqgroup
    FROM (
      SELECT 'cor' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, max(ptcrlo) AS ptcrlo --, 0 
      FROM tmpsdprdet
      WHERE ptcode = 'cr'
      GROUP BY  ptco#, ptro#, ptline, ptseq#, ptdate
      union
      SELECT 'cor' AS flagcor, b.ptco#, ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, max(ptcrlo) AS ptcrlo --, 0 
      FROM tmppdpphdr a
      INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'cr'
      GROUP BY  b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate)   a
    LEFT JOIN tmpRoTechFlagCorSeqGroup b on a.ptro# = b.ptro# AND a.ptline = b.ptline 
      AND a.ptseq# = b.ptseq# AND b.flagcor = 'cor'
)x WHERE ptro# = '19155586'      
  
-- ADD seq to DISTINCT OUTER query  
SELECT * FROM (
  SELECT a.storecode, a.ro, a.line, b.ptdate AS flagdate, b.pttech, b.ptlhrs, 
--    ptlamt, b.seqgroup, c.ptcrlo, c.seqgroup
    b.seqgroup, c.ptcrlo, c.seqgroup, b.ptseq#
  FROM tmpRoLine a
  LEFT JOIN (
    select a.*, b.seqgroup
    FROM (
      SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, 
        sum(ptlhrs) AS ptlhrs --, SUM(ptlamt) AS ptlamt
      FROM tmpsdprdet
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0 
      GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech 
      UNION 
      SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#, 
        b.ptdate, pttech, sum(ptlhrs) AS ptlhrs -- , SUM(ptarla)
      FROM tmppdpphdr a
      INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0
      GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech)   a
    LEFT JOIN tmpRoTechFlagCorSeqGroup b on a.ptro# = b.ptro# 
      AND a.ptline = b.ptline 
      AND a.ptseq# = b.ptseq# AND b.flagcor = 'flag') b  on storecode = b.ptco# 
        AND a.ro = b.ptro# AND a.line = b.ptline
  LEFT JOIN (
    select a.*, b.seqgroup
    FROM (
      SELECT 'cor' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, max(ptcrlo) AS ptcrlo --, 0 
      FROM tmpsdprdet
      WHERE ptcode = 'cr'
      GROUP BY  ptco#, ptro#, ptline, ptseq#, ptdate
      union
      SELECT 'cor' AS flagcor, b.ptco#, ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, max(ptcrlo) AS ptcrlo --, 0 
      FROM tmppdpphdr a
      INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'cr'
      GROUP BY  b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate)   a
    LEFT JOIN tmpRoTechFlagCorSeqGroup b on a.ptro# = b.ptro# AND a.ptline = b.ptline 
      AND a.ptseq# = b.ptseq# AND b.flagcor = 'cor') c on a.storecode = c.ptco#
        AND a.ro = c.ptro# AND a.line = c.ptline
        AND 
          CASE 
            WHEN b.seqgroup IS NULL THEN 1 = 1
            ELSE coalesce(b.seqgroup, -1) = coalesce(c.seqgroup, -1)
          END
) x  WHERE ro = '16138960' AND line = 10  
       
     

