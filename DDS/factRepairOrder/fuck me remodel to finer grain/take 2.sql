-- carry over questions AND probems FROM take 1
-- DO NOT know IF i need to persists ptdbas AND ptpadj for analysis of flag hours
-- lets start NOT
-- FUCK, 16127832 line 1 ptpadj = X, ptdbas = V on a line with 1 flag hour

DROP TABLE #flag;
SELECT *
INTO #flag
FROM (
  SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, sum(ptlhrs) AS ptlhrs
  FROM tmpsdprdet
  WHERE ptcode = 'tt'
  AND ptlhrs <> 0
  GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech
  UNION 
  SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, pttech, sum(ptlhrs) AS ptlhrs
  FROM tmppdpphdr a
  INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
  WHERE ptcode = 'tt'
  AND ptlhrs <> 0
  GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech) x;

DROP TABLE #cor;
SELECT * 
INTO #cor
FROM (
  SELECT 'cor' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, max(ptcrlo) AS ptcrlo, 0 
  FROM tmpsdprdet
  WHERE ptcode = 'cr'
  GROUP BY  ptco#, ptro#, ptline, ptseq#, ptdate
  union
  SELECT 'cor' AS flagcor, b.ptco#, ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, max(ptcrlo) AS ptcrlo, 0 
  FROM tmppdpphdr a
  INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
  WHERE ptcode = 'cr'
  GROUP BY  b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate) x;

DROP TABLE zseqgroup;
CREATE TABLE zSeqGroup (
  flagcor cichar(4), 
  ptro# cichar(9),
  ptline integer,
  ptseq# integer,
  seqgroup integer) IN database;
  
-- assumption: cr seq always less than the tt seq#
-- each instance of a cor line generates a new seqgroup   
DECLARE @cur CURSOR AS
SELECT *
FROM (
  select * FROM #flag
  UNION
  SELECT * FROM #cor) x
  ORDER BY ptco#, ptro#, ptline, ptseq#;
DECLARE @i integer;
DECLARE @sql string; 
DECLARE @flagcor string;
DECLARE @ptro string;
DECLARE @ptline integer;
DECLARE @ptseq integer;
OPEN @cur; 
TRY
  @i = 1;
  WHILE FETCH @cur DO
  @flagcor = @cur.flagcor;
  @ptro = @cur.ptro#;
  @ptline = @cur.ptline;
  @ptseq = @cur.ptseq#;
  IF @flagcor = 'cor' THEN 
    @i = @i + 1;
  END IF;
    INSERT INTO zSeqGroup values(@flagcor, @ptro, @ptline, @ptseq, @i);
  END WHILE;
FINALLY
  CLOSE @cur;
END;   

DROP TABLE #wtf;
SELECT a.*, b.ptdate AS flagdate, b.pttech, b.ptlhrs, b.seqgroup, c.ptcrlo, c.seqgroup
INTO #wtf
FROM (
  SELECT ptco#, ptro#, ptline 
  FROM tmpsdprdet
  GROUP BY ptco#, ptro#, ptline        
  union
  SELECT b.ptco#, a.ptdoc# AS ptro#, ptline
  FROM tmppdpphdr a
  INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
    WHERE a.ptdtyp = 'RO'
      AND a.ptdoc# <> ''
      AND a.ptchk# NOT LIKE 'v%'
      AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
      AND b.ptline < 900  
  GROUP BY b.ptco#, ptdoc#, ptline) a
LEFT JOIN (
  select a.*, b.seqgroup
  FROM #flag a
  LEFT JOIN zseqgroup b on a.ptro# = b.ptro# 
    AND a.ptline = b.ptline 
    AND a.ptseq# = b.ptseq# AND b.flagcor = 'flag') b  on a.ptco# = b.ptco# 
      AND a.ptro# = b.ptro# AND a.ptline = b.ptline
LEFT JOIN (
  select a.*, b.seqgroup
  FROM #cor a
  LEFT JOIN zseqgroup b on a.ptro# = b.ptro# AND a.ptline = b.ptline 
    AND a.ptseq# = b.ptseq# AND b.flagcor = 'cor') c on a.ptco# = c.ptco#
      AND a.ptro# = c.ptro# AND a.ptline = c.ptline
      AND 
        CASE 
          WHEN b.seqgroup IS NULL THEN 1 = 1
          ELSE coalesce(b.seqgroup, -1) = coalesce(c.seqgroup, -1)
        END;
        
-- all techs tech/total hours for period
SELECT z.*, coalesce(w.adjhrs, 0) AS adjhrs
FROM (
  SELECT lastname, x.pttech, SUM(ptlhrs)
  FROM (
    SELECT ptco#, ptro#, ptline, 
      coalesce(flagdate, cast('12/31/9999' AS sql_Date)) AS FlagDate, 
      coalesce(pttech, 'N/A') AS pttech, coalesce(ptlhrs, 0) AS ptlhrs,
      coalesce(ptcrlo, 'N/A') AS ptcrlo, seqgroup, b.lastname
    FROM #wtf a
    INNER JOIN scotest.tptechs b on a.pttech = b.technumber) x   
  WHERE flagdate BETWEEN '11/16/2013' AND '11/30/2013'
  GROUP BY lastname, x.pttech) z
LEFT JOIN (
  select pttech, round(SUM(ptlhrs), 2) AS adjhrs
  FROM stgarkonasdpxtim
  WHERE ptdate BETWEEN '11/16/2013' AND '11/30/2013'
  GROUP BY pttech) w on z.pttech = w.pttech   
ORDER BY z.pttech
        
SELECT ptro#, flagdate, SUM(ptlhrs)
FROM (
  SELECT ptco#, ptro#, ptline, 
    coalesce(flagdate, cast('12/31/9999' AS sql_Date)) AS FlagDate, 
    coalesce(pttech, 'N/A') AS pttech, coalesce(ptlhrs, 0) AS ptlhrs,
    coalesce(ptcrlo, 'N/A') AS ptcrlo, seqgroup
  FROM #wtf a
  WHERE coalesce(flagdate, cast('12/31/9999' AS sql_Date)) BETWEEN '11/16/2013' AND '11/30/2013'
    AND pttech = '631') a
GROUP BY ptro#, flagdate   
ORDER BY ptro#         

SELECT * FROM scotest.tptechs ORDER BY technumber
SELECT * FROM stgArkonaSDPXTIM WHERE ptro# = '16137349'
        
