DROP TABLE zseqgroup;
CREATE TABLE zSeqGroup (
  flagcor cichar(4), 
  ptro# cichar(9),
  ptline integer,
  ptseq# integer,
  seqgroup integer) IN database;
  
-- assumption: cr seq always less than the tt seq#
-- each instance of a cor line generates a new seqgroup   
DECLARE @cur CURSOR AS
SELECT *
FROM (
  SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, sum(ptlhrs) AS ptlhrs
  FROM tmpsdprdet
  WHERE ptcode = 'tt'
  AND ptlhrs <> 0
  GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech
  UNION 
  SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, pttech, sum(ptlhrs) AS ptlhrs
  FROM tmppdpphdr a
  INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
  WHERE ptcode = 'tt'
  AND ptlhrs <> 0
  GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech
  union
  SELECT 'cor' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, max(ptcrlo) AS ptcrlo, 0 
  FROM tmpsdprdet
  WHERE ptcode = 'cr'
  GROUP BY  ptco#, ptro#, ptline, ptseq#, ptdate
  union
  SELECT 'cor' AS flagcor, b.ptco#, ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, max(ptcrlo) AS ptcrlo, 0 
  FROM tmppdpphdr a
  INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
  WHERE ptcode = 'cr'
  GROUP BY  b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate) x
ORDER BY ptco#, ptro#, ptline, ptseq#;  
DECLARE @i integer;
DECLARE @sql string; 
DECLARE @flagcor string;
DECLARE @ptro string;
DECLARE @ptline integer;
DECLARE @ptseq integer;
OPEN @cur; 
TRY
  @i = 1;
  WHILE FETCH @cur DO
  @flagcor = @cur.flagcor;
  @ptro = @cur.ptro#;
  @ptline = @cur.ptline;
  @ptseq = @cur.ptseq#;
  IF @flagcor = 'cor' THEN 
    @i = @i + 1;
  END IF;
    INSERT INTO zSeqGroup values(@flagcor, @ptro, @ptline, @ptseq, @i);
  END WHILE;
FINALLY
  CLOSE @cur;
END;   

DROP TABLE #wtf1;
SELECT a.storecode, a.ro, a.line, b.ptdate AS flagdate, b.pttech, b.ptlhrs, b.seqgroup, c.ptcrlo, c.seqgroup
INTO #wtf1
FROM tmpRoLine a
LEFT JOIN (
  select a.*, b.seqgroup
  FROM (
    SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, sum(ptlhrs) AS ptlhrs
    FROM tmpsdprdet
    WHERE ptcode = 'tt'
    AND ptlhrs <> 0
    GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech
    UNION 
    SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, pttech, sum(ptlhrs) AS ptlhrs
    FROM tmppdpphdr a
    INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
    WHERE ptcode = 'tt'
    AND ptlhrs <> 0
    GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech)   a
  LEFT JOIN zseqgroup b on a.ptro# = b.ptro# 
    AND a.ptline = b.ptline 
    AND a.ptseq# = b.ptseq# AND b.flagcor = 'flag') b  on storecode = b.ptco# 
      AND a.ro = b.ptro# AND a.line = b.ptline
LEFT JOIN (
  select a.*, b.seqgroup
  FROM (
    SELECT 'cor' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, max(ptcrlo) AS ptcrlo, 0 
    FROM tmpsdprdet
    WHERE ptcode = 'cr'
    GROUP BY  ptco#, ptro#, ptline, ptseq#, ptdate
    union
    SELECT 'cor' AS flagcor, b.ptco#, ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, max(ptcrlo) AS ptcrlo, 0 
    FROM tmppdpphdr a
    INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
    WHERE ptcode = 'cr'
    GROUP BY  b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate)   a
  LEFT JOIN zseqgroup b on a.ptro# = b.ptro# AND a.ptline = b.ptline 
    AND a.ptseq# = b.ptseq# AND b.flagcor = 'cor') c on a.storecode = c.ptco#
      AND a.ro = c.ptro# AND a.line = c.ptline
      AND 
        CASE 
          WHEN b.seqgroup IS NULL THEN 1 = 1
          ELSE coalesce(b.seqgroup, -1) = coalesce(c.seqgroup, -1)
        END;
 
-- 12/22/2013 the relevant fields FROM #wtf1 need to be grouped
-- see ro 16126541 line 6, there can be duplicate lines IN #wtf1,
SELECT * FROM #wtf1 WHERE ro = '16126541' AND line = 6
SELECT storecode, ro, line, flagdate, pttech, ptlhrs, ptcrlo
FROM #wtf1
WHERE ro = '16126541' AND line = 6
GROUP BY storecode, ro, line, flagdate, pttech, ptlhrs, ptcrlo
-- that IS ok, just need to clean them up   
added seqgroup to grouping
ro 16129162 line 1 same tech, same line, same ptcrlo, multiple transactions
eg 511 flagged 1 hour on line 1 ptcrlo 24M twice, looks LIKE the old dup line
shit except the only things duplicated are cor AND tech, looking IN the story
see the real issue IS that the tech actually did 2 correction codes on the line
but entered the same corcode (IN error_

DROP TABLE #wtf3;
SELECT storecode, ro, line, flagdate, pttech, ptcrlo, SUM(ptlhrs) AS ptlhrs
INTO #wtf3 
FROM (
  SELECT storecode, ro, line, 
    coalesce(flagdate, CAST('12/31/9999' AS sql_date)) AS flagdate,
    coalesce(pttech, 'N/A') AS pttech, coalesce(ptlhrs, 0) AS ptlhrs, 
    coalesce(ptcrlo, 'N/A') AS ptcrlo, seqgroup
  FROM #wtf1
  GROUP BY storecode, ro, line, 
    coalesce(flagdate, CAST('12/31/9999' AS sql_date)),
    coalesce(pttech, 'N/A'), coalesce(ptlhrs, 0),
    coalesce(ptcrlo, 'N/A'), seqgroup) a
GROUP BY storecode, ro, line, flagdate, pttech, ptcrlo;  


-- #wtf3  
-- bingo for september
SELECT z.*, coalesce(w.adjhrs, 0) AS adjhrs
FROM (
  SELECT lastname, x.pttech, SUM(ptlhrs)
  FROM (
    SELECT storecode, ro, line, 
      coalesce(flagdate, cast('12/31/9999' AS sql_Date)) AS FlagDate, 
      coalesce(pttech, 'N/A') AS pttech, coalesce(ptlhrs, 0) AS ptlhrs,
      coalesce(ptcrlo, 'N/A') AS ptcrlo, b.lastname
    FROM #wtf3 a
    INNER JOIN scotest.tptechs b on a.pttech = b.technumber) x   
  WHERE flagdate BETWEEN '09/01/2013' AND '09/30/2013'
  GROUP BY lastname, x.pttech) z
LEFT JOIN (
  select pttech, round(SUM(ptlhrs), 2) AS adjhrs
  FROM stgarkonasdpxtim
  WHERE ptdate BETWEEN '09/01/2013' AND '09/30/2013'
  GROUP BY pttech) w on z.pttech = w.pttech   
ORDER BY z.pttech

-- july bingo
SELECT z.*, coalesce(w.adjhrs, 0) AS adjhrs
FROM (
  SELECT lastname, x.pttech, SUM(ptlhrs)
  FROM (
    SELECT storecode, ro, line, 
      coalesce(flagdate, cast('12/31/9999' AS sql_Date)) AS FlagDate, 
      coalesce(pttech, 'N/A') AS pttech, coalesce(ptlhrs, 0) AS ptlhrs,
      coalesce(ptcrlo, 'N/A') AS ptcrlo, b.lastname
    FROM #wtf3 a
    INNER JOIN scotest.tptechs b on a.pttech = b.technumber) x   
  WHERE flagdate BETWEEN '07/01/2013' AND '07/31/2013'
  GROUP BY lastname, x.pttech) z
LEFT JOIN (
  select pttech, round(SUM(ptlhrs), 2) AS adjhrs
  FROM stgarkonasdpxtim
  WHERE ptdate BETWEEN '07/01/2013' AND '07/31/2013'
  GROUP BY pttech) w on z.pttech = w.pttech   
ORDER BY z.pttech



SELECT ro, flagdate, SUM(ptlhrs)
FROM (
  SELECT storecode, ro, line, 
    FlagDate, pttech, ptlhrs, ptcrlo
  FROM #wtf3 a
  WHERE flagdate BETWEEN '07/01/2013' AND '07/31/2013'
    AND pttech = '625') a
GROUP BY ro, flagdate   
ORDER BY ro     


-- this IS good
select storecode, ro, line, flagdate, pttech, ptcrlo 
FROM tmpRoTechFlagCor2
GROUP BY storecode, ro, line, flagdate, pttech, ptcrlo 
HAVING COUNT(*) > 1
-- this IS bad
-- the problem here may be unknown cor codes
-- i am NOT making a clear enough distinction BETWEEN no  corcode on the ro
-- vs AND invalid corcode (NOT IN dimOpCode)
SELECT c.thedate, a.*
FROM tmpRoTechFlagCorKeys a
INNER JOIN (
  SELECT storecode, ro, line, flagdatekey, techkey, corcodekey
  FROM tmpRoTechFlagCorKeys
  GROUP BY storecode, ro, line, flagdatekey, techkey, corcodekey
  HAVING COUNT(*) > 1) b on a.ro = b.ro AND a.line = b.line
INNER JOIN day c on a.flagdatekey = c.datekey  

fixed dimOpCode to include a key for each invalid cor code,
now this IS good
SELECT storecode, ro, line, flagdatekey, techkey, corcodekey
FROM tmpRoTechFlagCorKeys
GROUP BY storecode, ro, line, flagdatekey, techkey, corcodekey
HAVING COUNT(*) > 1



-- keys for #wtf3
SELECT a.*, b.datekey AS FlagDateKey, 
  CASE 
    WHEN a.storecode = 'RY1' THEN
      CASE WHEN c.TechKey IS NOT NULL THEN c.TechKey ELSE d.TechKey END 
    WHEN a.storecode = 'RY2' THEN 
      CASE WHEN c.TechKey IS NOT NULL THEN c.TechKey ELSE d.TechKey END 
  END AS TechKey,
  coalesce(e.opcodekey, f.opcodekey) AS OpCodeKey,
  ptlhrs AS FlagHours 
FROM #wtf3 a
LEFT JOIN day b on a.flagdate = b.thedate
LEFT JOIN dimTech c on a.storecode = c.storecode
  AND a.pttech = c.technumber
  AND a.flagdate BETWEEN c.techkeyfromdate AND c.techkeythrudate
LEFT JOIN (
  SELECT storecode, TechKey 
  FROM dimTech
  WHERE technumber = 'UNK') d ON a.storecode = d.storecode  
LEFT JOIN dimOpCode e ON a.storecode = e.storecode AND a.ptcrlo = e.opcode
LEFT JOIN dimOpCode f ON a.storecode = f.storecode
  AND f.opcode = 'N/A'  
  
  
SELECT *
FROM #wtf1
WHERE pttech <> 'N/A'  
AND ptlhrs = 0

-- 12/23/13
ADD labor sales at the fact grain
SELECT * FROM tmpsdprdet

SELECT * FROM tmpFactRepairOrder