-- the problem IS here IN the generation of todayRoTechFlagCor1
  SELECT distinct a.storecode, a.ro, a.line, b.ptdate AS flagdate, b.pttech, b.ptlhrs, 
--    ptlamt, b.seqgroup, c.ptcrlo, c.seqgroup
    b.seqgroup, c.ptcrlo, c.seqgroup, b.ptseq#,
-- *e*  
    ptlamt      
  FROM todayRoLine a
  LEFT JOIN (
    select a.*, b.seqgroup
    FROM (
      SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, 
-- *e*       
        sum(ptlhrs) AS ptlhrs, SUM(ptlamt) AS ptlamt
      FROM todaysdprdet
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0 
      GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech 
      UNION 
      SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#,
-- *e*        
        b.ptdate, pttech, sum(ptlhrs) AS ptlhrs, SUM(ptarla)
      FROM todaypdpphdr a
      INNER JOIN todaypdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0
      GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech)   a
    LEFT JOIN todayRoTechFlagCorSeqGroup b on a.ptro# = b.ptro# 
      AND a.ptline = b.ptline 
      AND a.ptseq# = b.ptseq# AND b.flagcor = 'flag') b  on storecode = b.ptco# 
        AND a.ro = b.ptro# AND a.line = b.ptline
  LEFT JOIN (
    select a.*, b.seqgroup
    FROM (
      SELECT 'cor' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, max(ptcrlo) AS ptcrlo --, 0 
      FROM todaysdprdet
      WHERE ptcode = 'cr'
      GROUP BY  ptco#, ptro#, ptline, ptseq#, ptdate
      union
      SELECT 'cor' AS flagcor, b.ptco#, ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, max(ptcrlo) AS ptcrlo --, 0 
      FROM todaypdpphdr a
      INNER JOIN todaypdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'cr'
      GROUP BY  b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate)   a
    LEFT JOIN todayRoTechFlagCorSeqGroup b on a.ptro# = b.ptro# AND a.ptline = b.ptline 
      AND a.ptseq# = b.ptseq# AND b.flagcor = 'cor') c on a.storecode = c.ptco#
        AND a.ro = c.ptro# AND a.line = c.ptline
        AND 
          CASE 
            WHEN b.seqgroup IS NULL THEN 1 = 1
            ELSE coalesce(b.seqgroup, -1) = coalesce(c.seqgroup, -1)
          END
  
WHERE ro = '16144742'    

sure feels LIKE the precedence notion, WHEN an ro IS OPEN pdet has to take precedence
here IS the problem, this IS generating 2 lines because (i think) one has a labor sales amount (ptlamt) AND one does NOT

storecode  ro         line       flagdate   pttech     ptlhrs     seqgroup   ptcrlo     seqgroup_1 ptseq#     ptlamt               
-----------------------------------------------------------------------------------------------------------------------------------
RY1          16144742          1 02/10/2014        575        0.4         55    9100334         55       1020 $0.00                
RY1          16144742          1 02/10/2014        575        0.4         55    9100334         55       1020 $42.22               
                             

-- it IS the top half of the query generating the 2 rows
  SELECT distinct a.storecode, a.ro, a.line, b.ptdate AS flagdate, b.pttech, b.ptlhrs, 
--    ptlamt, b.seqgroup, c.ptcrlo, c.seqgroup
    b.seqgroup, /*c.ptcrlo, c.seqgroup,*/ b.ptseq#,
-- *e*  
    ptlamt      
  FROM todayRoLine a
  LEFT JOIN (
    select a.*, b.seqgroup
    FROM (
      SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, 
-- *e*       
        sum(ptlhrs) AS ptlhrs, SUM(ptlamt) AS ptlamt
      FROM todaysdprdet
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0 
      GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech 
      UNION 
      SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#,
-- *e*        
        b.ptdate, pttech, sum(ptlhrs) AS ptlhrs, SUM(ptarla)
      FROM todaypdpphdr a
      INNER JOIN todaypdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0
      GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech)   a
    LEFT JOIN todayRoTechFlagCorSeqGroup b on a.ptro# = b.ptro# 
      AND a.ptline = b.ptline 
      AND a.ptseq# = b.ptseq# AND b.flagcor = 'flag') b  on storecode = b.ptco# 
        AND a.ro = b.ptro# AND a.line = b.ptline
WHERE ro = '16144742'        

SDPRDET: one line with .4 hrs AND $42.22
      SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, 
-- *e*       
        sum(ptlhrs) AS ptlhrs, SUM(ptlamt) AS ptlamt
      FROM todaysdprdet
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0 
AND ptro# = '16144742'      
      GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech 
PDPPDET: one line with .4 hours AND $0      
      SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#,
-- *e*        
        b.ptdate, pttech, sum(ptlhrs) AS ptlhrs, SUM(ptarla), SUM(ptnet)
      FROM todaypdpphdr a
      INNER JOIN todaypdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0
AND ptdoc# = '16144742'       
      GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech      

therefore, WHEN these 2 are unioned, we get 2 rows because the ptlamt IS different      

IF i take sales out, the UNION generates a single row
      SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, 
-- *e*       
        sum(ptlhrs) AS ptlhrs-- , SUM(ptlamt) AS ptlamt
      FROM todaysdprdet
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0 
AND ptro# = '16144742'      
      GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech 
union    
      SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#,
-- *e*        
        b.ptdate, pttech, sum(ptlhrs) AS ptlhrs--, SUM(ptarla)
      FROM todaypdpphdr a
      INNER JOIN todaypdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0
AND ptdoc# = '16144742'       
      GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech  
      
SELECT * FROM todayRoTechFlagCorSeqGroup WHERE ptro# = '16144742'           

so the question becomes, how to get sales AND NOT double the flag time IN a situation LIKE this

GROUP everything except sales, THEN DO a SUM on sales

OR

it might be, use ptnet with ptcode TT instead of ptarla FROM pdppdet
here IS proof that this IS the correct answer:
select c.*, d.ptdate, d.pttech, d.ptlhrs, d.ptlamt
from (
  select a.ptdoc#, ptline, ptseq#, b.ptdate, b.ptlhrs, b.pttech, b.ptnet, b.ptarla
  from rydedata.pdpphdr a
  inner join rydedata.pdppdet b on a.ptpkey = b.ptpkey
  --where trim(a.ptdoc#) = '16144742'
  where ptcode = 'TT') c
left join rydedata.sdprdet d on trim(c.ptdoc#) = trim(d.ptro#) 
  and c.ptline = d.ptline
  and c.ptseq# = d.ptseq#
  and d.ptcode = 'TT' 
where d.ptro# is not null    
  and ptnet <> ptlamt