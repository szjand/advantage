alter PROCEDURE xfmFactRepairOrder
   ( 
   ) 
BEGIN 
/*

-- 9/15/13
   added UPDATE to tmpRO that checks for the correct bnkey for inventory customer

-- 10/29/13 fb CASE 313
   *a* added flagdatekey
-- 10/31
  finer grained error catching for RoLine 
12/19/13
--*b*
remove the negative hours fix, pull ALL rows whether positive OR negative hours   

12/21/13
  refactor for new granularity
  factRepairORder NK will be store/ro/line/tech/flagdate/corcode
12/25/13
  remove parts sales AND labor sales at every level except RO  
  -- *c*
  tmpRoTechFlagCor2 DO NOT GROUP on ptlhrs, SUM on OUTER level
  -- *d*
  ADD DISTINCT to generation of tmpRoTechFlagCor1
    this does NOT WORK for 19155586 line 5 tech 599
    ADD seq to DISTINCT OUTER query  
    fixed 12/15 - 12/24
  ALTER TABLE tmpRoTechFlagCor1 ADD COLUMN ptseq# integer;    
    
12/31/13
  sco reveals that i am NOT generating RoFlagHours, generate RoFlagHours IN the
  INSERT INTO tmpFactRepairOrder
  
  AND for the old rows without roFlagHours:
  UPDATE factRepairOrder
  SET roFlagHours = x.roFlagHours
  FROM (
    SELECT storecode, ro, SUM(flagHours) AS roFlagHours
    FROM factRepairORder
    GROUP BY storecode, ro) x
  WHERE factRepairOrder.storecode = x.storecode
    AND factRepairOrder.ro = x.ro; 
    
1/2/14
*e*
      roFlagHours IS no longer good enuf, spdrhdr IS NOT being consistently
      updated with labor sales (ptltot) AS revealed BY sco writer labor sale
      month to date being low   
      Have to ADD labor sales at the finest grain (flagdate/tech/corcode) AND
      generate total FROM the SUM      
    
EXECUTE PROCEDURE xfmFactRepairOrder();
*/
-- variables for TechFlagCor Sequence GROUP generation
DECLARE @cur CURSOR AS
SELECT *
FROM (
  SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, sum(ptlhrs) AS ptlhrs
  FROM tmpsdprdet
  WHERE ptcode = 'tt'
  AND ptlhrs <> 0
  GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech
  UNION 
  SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, pttech, sum(ptlhrs) AS ptlhrs
  FROM tmppdpphdr a
  INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
  WHERE ptcode = 'tt'
  AND ptlhrs <> 0
  GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech
  union
  SELECT 'cor' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, max(ptcrlo) AS ptcrlo, 0 
  FROM tmpsdprdet
  WHERE ptcode = 'cr'
  GROUP BY  ptco#, ptro#, ptline, ptseq#, ptdate
  union
  SELECT 'cor' AS flagcor, b.ptco#, ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, max(ptcrlo) AS ptcrlo, 0 
  FROM tmppdpphdr a
  INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
  WHERE ptcode = 'cr'
  GROUP BY  b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate) x
ORDER BY ptco#, ptro#, ptline, ptseq#;  
DECLARE @i integer;
DECLARE @sql string; 
DECLARE @flagcor string;
DECLARE @ptro string;
DECLARE @ptline integer;
DECLARE @ptseq integer;

TRY 
  DELETE 
  -- SELECT *
  FROM factRepairOrder                                                                                                           
  WHERE ro IN (        
    SELECT ptro#
    FROM stgArkonaSDPRHDR a
    WHERE a.ptchk# LIKE 'v%'
        OR a.ptcnam = '*VOIDED REPAIR ORDER*'
    UNION     
    SELECT ptdoc#
    FROM stgArkonaPDPPHDR a
    WHERE a.ptchk# LIKE 'v%'
      OR a.ptcnam = '*VOIDED REPAIR ORDER*'); 
      
  DELETE FROM stgArkonaSDPRHDR WHERE ptro# = '320481';    
  DELETE FROM stgArkonaSDPRDET WHERE ptro# = '320481';   
CATCH ALL
  RAISE xfmFactRepairOrder(1, 'Delete VOIDS ' + __errtext);
END TRY;     
    
BEGIN TRANSACTION;
TRY
  DELETE FROM tmpRO;      
  INSERT INTO tmpRO
  SELECT a.ptco# AS storecode, a.ptro# AS ro, a.ptswid AS writer, 
    a.ptckey AS customer, a.ptdate AS opendate, a.ptcdat AS closedate, a.ptfcdt AS finalclosedate,
    a.ptvin AS vin, 
    CASE 
      WHEN a.ptodom < 0 THEN
        CASE 
          WHEN a.ptmilo < 0 THEN 0
          ELSE a.ptmilo
        END
      WHEN a.ptodom = a.ptmilo THEN a.ptodom
      WHEN a.ptodom < a.ptmilo THEN a.ptmilo
      WHEN a.ptodom > a.ptmilo THEN a.ptodom
    END AS miles, a.ptptot RoParts, a.ptltot AS RoLabor, a.pthdsc AS discount, 
    a.ptcphz AS hazardous, 
    a.ptcpss + a.ptwass + a.ptinss + a.ptscss AS ShopSupplies, 
    IIF(pttag# = '', 'N/A', pttag#) AS tag, 
    ptcreate, 'Closed'
  FROM tmpSDPRHDR a
  WHERE ptchk# NOT LIKE 'v%' -- excludes voids
    AND ptcnam <> '*VOIDED REPAIR ORDER*'
    AND ptco# IN ('RY1','RY2');                                                 
  -- PDP WHERE NOT EXISTS IN tmpRO  
  INSERT INTO tmpRO  
  SELECT b.ptco# AS storecode, b.ptdoc# AS ro, b.ptswid AS writer, 
    b.ptckey AS customer, b.ptdate AS opendate, '12/31/9999','12/31/9999',
    b.ptvin AS vin, 
    CASE 
      WHEN b.ptodom < 0 THEN
        CASE 
          WHEN b.ptmilo < 0 THEN 0
          ELSE b.ptmilo
        END
      WHEN b.ptodom = b.ptmilo THEN b.ptodom
      WHEN b.ptodom < b.ptmilo THEN b.ptmilo
      WHEN b.ptodom > b.ptmilo THEN b.ptodom
    END AS miles, b.ptptot AS RoParts, b.ptltot as RoLabor, b.pthdsc AS discount, 
    b.ptcphz AS hazardous, 
    b.ptcpss + b.ptwass + b.ptinss + b.ptscss AS ShopSupplies, 
    IIF(pttag# = '', 'N/A', pttag#) AS tag,
    b.ptcreate, c.rostatus
  FROM tmpPDPPHDR b                                                        
  LEFT JOIN dimRoStatus c ON b.ptrsts = c.roStatusCode
  WHERE b.ptdtyp = 'RO'
    AND ptco# IN ('RY1','RY2')                                                  
    AND b.ptdoc# <> ''
    AND EXISTS ( -- exclude ros with no lines
      SELECT 1
      FROM tmpPDPPDET                                                     
      WHERE ptpkey = b.ptpkey) 
    AND ptchk# NOT LIKE 'v%'
    AND ptcnam <> '*VOIDED REPAIR ORDER*'
    AND NOT EXISTS (
      SELECT 1
      FROM tmpRO
      WHERE ro = b.ptdoc#);  
  -- UPDATE values IN tmpRO WHERE ro EXISTS IN both RHDR & PHDR
  -- a: RHDR values FROM tmpRO, d: PHDR values
  UPDATE tmpRO
  SET roparts =
        CASE 
          WHEN a.roparts = d.roparts THEN a.roparts
          ELSE
            CASE WHEN a.roparts = 0 THEN d.roparts ELSE a.roparts END
        END,
      rolabor = 
        CASE 
          WHEN a.rolabor = d.rolabor THEN a.rolabor
          ELSE
            CASE WHEN a.rolabor = 0 THEN d.rolabor ELSE a.rolabor END
        END,
      discount = 
        CASE 
          WHEN a.discount = d.discount THEN a.discount
          ELSE
            CASE WHEN a.discount = 0 THEN d.discount ELSE a.discount END
        END,
      hazardous = 
        CASE 
          WHEN a.hazardous = d.hazardous THEN a.hazardous
          ELSE
            CASE WHEN a.hazardous = 0 THEN d.hazardous ELSE a.hazardous END
        END,
      shopsupplies = 
        CASE 
          WHEN a.shopsupplies = d.shopsupplies THEN a.shopsupplies
          ELSE
            CASE WHEN a.shopsupplies = 0 THEN d.shopsupplies ELSE a.roparts END
        END,
      rostatus = coalesce(d.roStatus, 'Closed')        
  FROM tmpRO a
  INNER JOIN (
    SELECT b.ptco# AS storecode, b.ptdoc# AS ro, b.ptswid AS writer, 
      b.ptckey AS customer, b.ptdate AS opendate, '12/31/9999' AS closedate,
      '12/31/9999' AS finalclosedate, b.ptvin AS vin, 
      CASE 
        WHEN b.ptodom < 0 THEN
          CASE 
            WHEN b.ptmilo < 0 THEN 0
            ELSE b.ptmilo
          END
        WHEN b.ptodom = b.ptmilo THEN b.ptodom
        WHEN b.ptodom < b.ptmilo THEN b.ptmilo
        WHEN b.ptodom > b.ptmilo THEN b.ptodom
      END AS miles, b.ptptot AS RoParts, b.ptltot as RoLabor, b.pthdsc AS discount, 
      b.ptcphz AS hazardous, 
      b.ptcpss + b.ptwass + b.ptinss + b.ptscss AS ShopSupplies, b.pttag# AS tag, 
      b.ptcreate, c.rostatus
    FROM tmpPDPPHDR b                                                    
    LEFT JOIN dimRoStatus c ON b.ptrsts = c.roStatusCode
    WHERE b.ptdtyp = 'RO'
      AND b.ptdoc# <> ''
      AND EXISTS ( -- exclude ros with no lines
        SELECT 1
        FROM tmpPDPPDET                                                   
        WHERE ptpkey = b.ptpkey) 
      AND ptchk# NOT LIKE 'v%'
      AND ptcnam <> '*VOIDED REPAIR ORDER*') d ON a.ro = d.ro
  WHERE (a.roparts <> d.roparts 
    OR a.rolabor <> d.rolabor   
    OR a.discount <> d.discount
    OR a.hazardous <> d.hazardous
    OR a.shopsupplies <> d.shopsupplies
    OR a.rostatus <> d.rostatus);
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE xfmFactRepairOrder(2, 'tmpRO ' + __errtext);
END TRY; // transaction    

TRY 
  -- 9/15
  UPDATE tmpRO
  SET customer = 0
  WHERE ro IN (
    SELECT a.ro
    FROM tmpro a 
    LEFT JOIN stgArkonaBOPNAME b on a.customer = b.bnkey  
    WHERE (
      bnsnam LIKE '%inventory%'
      AND bnsnam NOT IN ('EINVENTORYNOW.COM INC','INVENTORY CONTROL SYSTEMS'))
      AND a.customer <> 0); 
CATCH ALL
  RAISE xfmFactRepairOrder(9, 'tmpRO Updates ' + __errtext);
END TRY;         

BEGIN TRANSACTION;
TRY
  DELETE FROM tmpRoKeys;  
  INSERT INTO tmpRoKeys (storecode,ro,tag,opendatekey,closedatekey,
    finalclosedatekey,servicewriterkey,customerkey,vehiclekey, 
    rocommentkey,rostatuskey, rolaborsales, ropartssales,
    miles,createTS,roshopsupplies,rodiscount,
    rohazardous)
  SELECT a.storecode, a.ro, coalesce(a.tag, 'N/A') AS tag,b.datekey,c.datekey,
    d.datekey, 
    coalesce(e.servicewriterkey, m.servicewriterkey), 
    coalesce(f.customerkey, k.customerkey) as customerkey, 
    coalesce(g.vehiclekey, n.VehicleKey) AS VehicleKey,
    coalesce(h.rocommentkey, i.rocommentkey) AS rocommentkey, rostatuskey, 
    rolabor, roparts,miles, createts, shopsupplies, discount,
    hazardous
  FROM tmpRO a
  LEFT JOIN day b ON a.opendate = b.thedate
  LEFT JOIN day c ON a.closedate = c.thedate
  LEFT JOIN day d ON a.finalclosedate = d.thedate
  LEFT JOIN dimServiceWriter e ON a.storecode = e.storecode
    AND a.writer = e.writernumber
    AND e.active = true
    AND b.thedate BETWEEN e.servicewriterkeyfromdate AND e.servicewriterkeythrudate
  LEFT JOIN dimServiceWriter m ON a.storecode = m.storecode
    AND m.writernumber = 'UNK'  
  LEFT JOIN dimCustomer f ON a.customer = f.bnkey  
  LEFT JOIN dimCustomer k ON 1 = 1
    AND k.fullname = 'unknown'
  LEFT JOIN dimVehicle g ON a.vin = g.vin 
  LEFT JOIN dimVehicle n ON 1 = 1 
    AND n.vin = 'UNKNOWN'
  LEFT JOIN dimRoComment h ON a.storecode = h.storecode AND a.ro = h.ro
  LEFT JOIN dimRoComment i ON a.storecode = i.storecode
    AND i.ro = 'N/A'
  LEFT JOIN dimRoStatus j ON a.roStatus = j.roStatus;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE xfmFactRepairOrder(3, 'tmpROKeys ' + __errtext);
END TRY; // transaction 


BEGIN TRANSACTION;
TRY   
  TRY 
    DELETE FROM tmpRoLine;     
    INSERT INTO tmpROLine (storecode, ro, line, linedate, sublet, paintmaterials, 
      servtype, paytype, opcode, status)     
    SELECT a.ptco# AS storecode, a.ptro# AS ro, a.ptline AS line,
      -- there a goofy few with no A line
      coalesce(min(CASE WHEN ptltyp = 'A' THEN a.ptdate ELSE cast('12/31/9999' AS sql_date) END), cast('12/31/9999' AS sql_date)) AS LineDate,
      coalesce(SUM(CASE WHEN a.ptcode = 'SL' AND a.ptltyp = 'N' THEN ptlamt END), 0) AS Sublet, 
      coalesce(SUM(CASE WHEN a.ptcode = 'PM' AND a.ptltyp = 'M' THEN ptlamt END), 0) AS PaintMaterials, 
      MAX(CASE WHEN a.ptltyp = 'A' THEN a.ptsvctyp END) AS ServType,
      MAX(CASE WHEN a.ptltyp = 'A' THEN a.ptlpym END) AS PayType,
      MAX(CASE WHEN a.ptltyp = 'A' THEN a.ptlopc END) AS OpCode,
      'Closed' AS status
    FROM tmpSDPRDET a  
    INNER JOIN tmpRO b ON a.ptro# = b.ro 
    GROUP BY ptco#, ptro#, ptline;
  CATCH ALL
    RAISE xfmFactRepairOrder(401, 'tmpRoLine-SDPRDET ' + __errtext);
  END TRY;
  TRY  
    INSERT INTO tmpRoLine (storecode, ro, line, linedate, sublet, paintmaterials, 
      servtype, paytype, opcode, status)     
    SELECT b.ptco# AS storecode, a.ptdoc# AS ro, b.ptline,
      -- there a goofy few with no A line
      coalesce(min(CASE WHEN ptltyp = 'A' THEN a.ptdate ELSE cast('12/31/9999' AS sql_date) END), cast('12/31/9999' AS sql_date)) AS LineDate,      
      coalesce(SUM(CASE WHEN b.ptcode = 'SL' AND b.ptltyp = 'N' THEN b.ptnet END), 0) AS Sublet,
      coalesce(SUM(CASE WHEN b.ptcode = 'PM' AND b.ptltyp = 'M' THEN b.ptnet END), 0) AS PaintMaterials, 
      MAX(CASE WHEN b.ptltyp = 'A' THEN b.ptsvctyp END) AS ServType,
      MAX(CASE WHEN b.ptltyp = 'A' THEN b.ptlpym END) AS PayType,
      MAX(CASE WHEN b.ptltyp = 'A' THEN b.ptlopc END) AS OpCode,
      MAX(CASE WHEN b.ptltyp = 'A' AND ptlsts = 'I' THEN 'Open' ELSE 'Closed' END) AS status  
    FROM tmpPDPPHDR a
    INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
    WHERE a.ptdtyp = 'RO'
      AND a.ptdoc# <> ''
      AND a.ptchk# NOT LIKE 'v%'
      AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
      AND b.ptline < 900
      AND NOT EXISTS (
        SELECT 1
        FROM tmpRoLine
        WHERE ro = a.ptdoc#
          AND line = b.ptline)
    GROUP BY b.ptco#, a.ptdoc#, b.ptline;
  CATCH ALL
    RAISE xfmFactRepairOrder(402, 'tmpRoLine-PDPPDET ' + __errtext);
  END TRY;
  TRY 
    UPDATE tmpROLine
    SET linedate = d.linedate,
        sublet = CASE WHEN a.sublet > d.sublet THEN a.sublet ELSE d.sublet END,
        paintmaterials = CASE WHEN a.paintmaterials > d.paintmaterials THEN a.paintmaterials ELSE d.paintmaterials END,
        servtype = d.servtype,
        paytype = d.paytype,
        opcode = d.opcode,
        status = d.status
    FROM tmpROLine a 
    INNER JOIN (
      SELECT c.ptco# AS storecode, b.ptdoc# AS ro, c.ptline,
        min(CASE WHEN c.ptltyp = 'A' THEN c.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS LineDate,
        coalesce(SUM(CASE WHEN c.ptcode = 'SL' AND c.ptltyp = 'N' THEN c.ptnet END), 0) AS Sublet,
        coalesce(SUM(CASE WHEN c.ptcode = 'PM' AND c.ptltyp = 'M' THEN c.ptnet END), 0) AS PaintMaterials, 
        MAX(CASE WHEN c.ptltyp = 'A' THEN c.ptsvctyp END) AS ServType,
        MAX(CASE WHEN c.ptltyp = 'A' THEN c.ptlpym END) AS PayType,
        MAX(CASE WHEN c.ptltyp = 'A' THEN c.ptlopc END) AS OpCode,
        MAX(CASE WHEN c.ptltyp = 'A' AND c.ptlsts = 'I' THEN 'Open' ELSE 'Closed' END) AS status  
      FROM tmpPDPPHDR b                                      
      INNER JOIN tmpPDPPDET c ON b.ptpkey = c.ptpkey
      WHERE b.ptdtyp = 'RO'
        AND b.ptdoc# <> ''
        AND b.ptchk# NOT LIKE 'v%'
        AND b.ptcnam <> '*VOIDED REPAIR ORDER*'
      GROUP BY c.ptco#, b.ptdoc#, c.ptline) d ON a.ro = d.ro AND a.line = d.ptline
    WHERE (a.linedate <> d.linedate
      OR a.sublet <> d.sublet
      OR a.paintmaterials <> d.paintmaterials
      OR a.servtype <> d.servtype
      OR a.paytype <> d.paytype
      OR a.opcode <> d.opcode
      OR a.status <> d.status collate ads_default_ci);
  CATCH ALL
    RAISE xfmFactRepairOrder(403, 'tmpRoLine-Update ' + __errtext);
  END TRY;      
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; // transaction 

BEGIN TRANSACTION;
TRY
  DELETE FROM tmpRoLineKeys;
  INSERT INTO tmpRoLineKeys                                      
  SELECT a.storecode, a.ro, a.line, 
    coalesce(d.datekey, dd.datekey) AS linedatekey,    
    coalesce(e.servicetypekey, ee.servicetypekey),
    coalesce(f.PaymentTypeKey, ff.PaymentTypeKey), 
    coalesce(g.cccKey, gg.cccKey) AS cccKey,
    coalesce(h.opcodekey, i.opcodekey) AS OpCodeKey,
    m.linestatuskey, 
--    0 AS partsSales, a.sublet, c.RoPaintMaterials
    a.sublet, c.RoPaintMaterials
   FROM tmpROLine a
  LEFT JOIN ( -- RoFlagHours
    SELECT storecode, ro 
    FROM tmproline
    GROUP BY storecode, ro) b ON a.storecode = b.storecode AND a.ro = b.ro
  LEFT JOIN ( --RoPaintMaterials
    SELECT storecode, ro, SUM(paintmaterials) AS RoPaintMaterials
    FROM tmproline
    GROUP BY storecode, ro) c ON a.storecode = c.storecode AND a.ro = c.ro
  LEFT JOIN day d ON a.linedate = d.thedate 
  LEFT JOIN day dd ON 1 = 1
    AND dd.datetype <> 'date'
  LEFT JOIN dimServiceType e ON a.servtype = e.servicetypecode
  LEFT JOIN dimServiceType ee ON 1 = 1 AND ee.servicetypecode = 'UN'
  LEFT JOIN dimPaymentType f ON a.paytype = f.paymentTypeCode
  LEFT JOIN dimPaymentType ff ON 1 = 1 AND ff.paymentTypeCode = 'U'
  LEFT JOIN dimCCC g ON a.storecode = g.storecode AND a.ro = g.ro AND a.line = g.line
  LEFT JOIN dimCCC gg ON a.storecode = gg.storecode
    AND gg.ro = 'N/A'
  LEFT JOIN dimOpCode h ON a.storecode = h.storecode AND a.opcode = h.opcode
  LEFT JOIN dimOpCode i ON a.storecode = i.storecode
    AND i.opcode = 'N/A'            
  LEFT JOIN dimLineStatus m ON a.status = m.linestatus;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE xfmFactRepairOrder(5, 'tmpROLineKeys ' + __errtext);
END TRY; // transaction 

/*
this IS WHERE THEN generation of tech/flagdate/corcode happens
*/
BEGIN TRANSACTION;
TRY
  DELETE FROM tmpRoTechFlagCorSeqGroup;
  OPEN @cur; 
  TRY
    @i = 1;
    WHILE FETCH @cur DO
    @flagcor = @cur.flagcor;
    @ptro = @cur.ptro#;
    @ptline = @cur.ptline;
    @ptseq = @cur.ptseq#;
    IF @flagcor = 'cor' THEN 
      @i = @i + 1;
    END IF;
      INSERT INTO tmpRoTechFlagCorSeqGroup values(@flagcor, @ptro, @ptline, @ptseq, @i);
    END WHILE;
  FINALLY
    CLOSE @cur;
  END;   
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE xfmFactRepairOrder(600, 'tmpRoTechFlagCorSeqGroup ' + __errtext);
END TRY; // transaction 

BEGIN TRANSACTION;
TRY 
  DELETE FROM tmpRoTechFlagCor1;
  INSERT INTO tmpRoTechFlagCor1
-- *d*  
  SELECT distinct a.storecode, a.ro, a.line, b.ptdate AS flagdate, b.pttech, 
    b.ptlhrs, 
--    ptlamt, b.seqgroup, c.ptcrlo, c.seqgroup
    b.seqgroup, c.ptcrlo, c.seqgroup, b.ptseq#,
-- *e*  
    ptlamt   
  FROM tmpRoLine a
  LEFT JOIN (
    select a.*, b.seqgroup
    FROM (
      SELECT 'flag' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, pttech, 
-- *e*      
        sum(ptlhrs) AS ptlhrs , SUM(ptlamt) AS ptlamt
      FROM tmpsdprdet
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0 
      GROUP BY ptco#, ptro#, ptline, ptseq#, ptdate, pttech 
      UNION 
      SELECT 'flag' AS flagcor, b.ptco#, a.ptdoc# AS ptro#, ptline, ptseq#, 
-- *e*      
        b.ptdate, pttech, sum(ptlhrs) AS ptlhrs, SUM(ptarla)
      FROM tmppdpphdr a
      INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'tt'
      AND ptlhrs <> 0
      GROUP BY b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate, pttech)   a
    LEFT JOIN tmpRoTechFlagCorSeqGroup b on a.ptro# = b.ptro# 
      AND a.ptline = b.ptline 
      AND a.ptseq# = b.ptseq# AND b.flagcor = 'flag') b  on storecode = b.ptco# 
        AND a.ro = b.ptro# AND a.line = b.ptline
  LEFT JOIN (
    select a.*, b.seqgroup
    FROM (
      SELECT 'cor' AS flagcor, ptco#, ptro#, ptline, ptseq#, ptdate, max(ptcrlo) AS ptcrlo --, 0 
      FROM tmpsdprdet
      WHERE ptcode = 'cr'
      GROUP BY  ptco#, ptro#, ptline, ptseq#, ptdate
      union
      SELECT 'cor' AS flagcor, b.ptco#, ptdoc# AS ptro#, ptline, ptseq#, b.ptdate, max(ptcrlo) AS ptcrlo --, 0 
      FROM tmppdpphdr a
      INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
      WHERE ptcode = 'cr'
      GROUP BY  b.ptco#, a.ptdoc#, ptline, ptseq#, b.ptdate)   a
    LEFT JOIN tmpRoTechFlagCorSeqGroup b on a.ptro# = b.ptro# AND a.ptline = b.ptline 
      AND a.ptseq# = b.ptseq# AND b.flagcor = 'cor') c on a.storecode = c.ptco#
        AND a.ro = c.ptro# AND a.line = c.ptline
        AND 
          CASE 
            WHEN b.seqgroup IS NULL THEN 1 = 1
            ELSE coalesce(b.seqgroup, -1) = coalesce(c.seqgroup, -1)
          END;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE xfmFactRepairOrder(601, 'tmpRoTechFlagCor1 ' + __errtext);
END TRY; // transaction         

BEGIN TRANSACTION;
TRY 
  DELETE FROM tmpRoTechFlagCor2;
  INSERT INTO tmpRoTechFlagCor2
  SELECT storecode, ro, line, flagdate, pttech, ptcrlo, SUM(ptlhrs) AS ptlhrs,
-- *e*  
    SUM(ptlamt) AS ptlamt
  FROM (
    SELECT storecode, ro, line, 
      coalesce(flagdate, CAST('12/31/9999' AS sql_date)) AS flagdate,
  -- *c*      
--      coalesce(pttech, 'N/A') AS pttech, coalesce(ptlhrs, 0) AS ptlhrs, 
      coalesce(pttech, 'N/A') AS pttech, sum(coalesce(ptlhrs, 0)) AS ptlhrs,
-- *e*      
      sum(coalesce(ptlamt, 0)) AS ptlamt,
      coalesce(ptcrlo, 'N/A') AS ptcrlo, coalesce(flagseqgroup, corseqgroup)
    FROM tmpRoTechFlagCor1
    GROUP BY storecode, ro, line, 
      coalesce(flagdate, CAST('12/31/9999' AS sql_date)),
-- *c*      
--      coalesce(pttech, 'N/A'), coalesce(ptlhrs, 0), --, coalesce(ptlamt, 0),
      coalesce(pttech, 'N/A'), -- coalesce(ptlhrs, 0), --, coalesce(ptlamt, 0),
      coalesce(ptcrlo, 'N/A'), coalesce(flagseqgroup, corseqgroup)) a
      
  GROUP BY storecode, ro, line, flagdate, pttech, ptcrlo;  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE xfmFactRepairOrder(602, 'tmpRoTechFlagCor2 ' + __errtext);
END TRY; // transaction 

BEGIN TRANSACTION;
TRY 
  DELETE FROM tmpRoTechFlagCorKeys;
  INSERT INTO tmpRoTechFlagCorKeys
  SELECT a.storecode, a.ro, a.line, b.datekey AS FlagDateKey, 
    CASE 
      WHEN a.storecode = 'RY1' THEN
        CASE WHEN c.TechKey IS NOT NULL THEN c.TechKey ELSE d.TechKey END 
      WHEN a.storecode = 'RY2' THEN 
        CASE WHEN c.TechKey IS NOT NULL THEN c.TechKey ELSE d.TechKey END 
    END AS TechKey,
    coalesce(e.opcodekey, f.opcodekey) AS OpCodeKey,
-- *e*    
    ptlhrs AS FlagHours, ptlamt AS laborSales 
  FROM tmpRoTechFlagCor2 a
  LEFT JOIN day b on a.flagdate = b.thedate
  LEFT JOIN dimTech c on a.storecode = c.storecode
    AND a.pttech = c.technumber
    AND a.flagdate BETWEEN c.techkeyfromdate AND c.techkeythrudate
  LEFT JOIN (
    SELECT storecode, TechKey 
    FROM dimTech
    WHERE technumber = 'UNK') d ON a.storecode = d.storecode  
  LEFT JOIN dimOpCode e ON a.storecode = e.storecode AND a.ptcrlo = e.opcode
  LEFT JOIN dimOpCode f ON a.storecode = f.storecode
    AND f.opcode = 'N/A'; 
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE xfmFactRepairOrder(603, 'tmpRoTechFlagKeys ' + __errtext);
END TRY; // transaction 

BEGIN TRANSACTION;
TRY
  DELETE FROM tmpFactRepairOrder; -- identical structure to factRepairOrder
  INSERT INTO tmpFactRepairOrder (storecode, ro, line, tag, opendatekey, closedatekey,
    finalclosedatekey, linedatekey, 
    flagdatekey,    
    servicewriterkey, customerkey,
    vehiclekey, servicetypekey, paymenttypekey,ccckey,rocommentkey,
    opcodekey,rostatuskey,linestatuskey,
    rolaborsales,ropartssales,flaghours,roflaghours,miles,
--    rocreatedts,laborsales,partssales,sublet,roshopsupplies,
    rocreatedts,
    sublet,roshopsupplies,
    rodiscount,rohazardousmaterials,ropaintmaterials,corcodekey, techkey,
-- *e*
    laborSales)
  SELECT a.storecode, a.ro, b.line, a.tag, a.opendatekey, a.closedatekey, 
    a.finalclosedatekey, b.linedatekey, 
    c.flagdatekey,    
    a.servicewriterkey, a.customerkey, 
    a.vehiclekey, b.servicetypekey, b.paymenttypekey, b.ccckey, a.rocommentkey, 
    b.opcodekey, a.rostatuskey, b.statuskey, 
-- *e*    
--    a.rolaborsales, a.ropartssales, c.ptlhrs, d.roFlagHours, a.miles, 
    d.rolaborsales, a.ropartssales, c.ptlhrs, d.roFlagHours, a.miles, 
--    a.createTS, c.ptlamt, b.partssales, b.sublet, a.roshopsupplies, 
    a.createTS, b.sublet, a.roshopsupplies,
    a.rodiscount, a.rohazardous, b.ropaintmaterials, c.corcodekey, c.techkey,
    c.ptlamt
  FROM tmpRoKeys a
  INNER JOIN tmpRoLineKeys b ON a.storecode = b.storecode AND a.ro = b.ro 
    AND b.line < 900
  INNER JOIN tmpRoTechFlagCorKeys c on b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line
  LEFT JOIN (  -- generate roFlagHours, roLaborHours
    SELECT storecode, ro, SUM(ptlhrs) AS RoFlagHours, 
-- *e*
      SUM(ptlamt) AS roLaborSales  
    FROM tmpRoTechFlagCorKeys
    GROUP BY storecode, ro) d on a.storecode = d.storecode
      AND a.ro = d.ro;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE xfmFactRepairOrder(6, 'tmpFactRepairOrder ' + __errtext);
END TRY; // transaction 

BEGIN TRANSACTION;
TRY
  DELETE 
  FROM factRepairOrder
  WHERE ro IN (
    SELECT ro
    FROM tmpFactRepairOrder);
    
  INSERT INTO factRepairOrder    
  SELECT * FROM tmpFactRepairOrder;  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE xfmFactRepairOrder(7, 'xfmFactRepairOrder ' + __errtext);
END TRY; // transaction 


END;
