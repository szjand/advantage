DROP PROCEDURE __xfmEmpDIM;
DROP PROCEDURE zzzohShitPypclkctlHasChanged;

drop TABLE [__tempPYPCLOCKIN];
CREATE TABLE [tmpPYPCLOCKIN] ( 
      [yico#] CIChar( 3 ),
      [yiemp#] CIChar( 7 ),
      yiclkind Date,
      yiclkint Time,
      yiclkoutd Date,
      yiclkoutt Time,
      yicode CIChar( 3 )) IN DATABASE;
drop TABLE [__tempOvertime];      
CREATE TABLE [tmpOvertime] ( 
      [yico#] CIChar( 3 ),
      [yiemp#] CIChar( 7 ),
      SunToSatWeek Integer,
      yiclkind Date,
      RegularHours Double( 15 ),
      OverTimeHours Double( 15 )) IN DATABASE;
      
drop TABLE [__tempPYACTGR];      
CREATE TABLE [tmpPYACTGR] ( 
      [ytaco#] CIChar( 3 ),
      ytadic CIChar( 4 ),
      ytaseq Integer,
      ytadsc CIChar( 25 ),
      ytagco CIChar( 3 ),
      ytagpp Double( 15 ),
      ytagpa CIChar( 10 ),
      ytaopa CIChar( 10 ),
      ytavac CIChar( 10 ),
      ytahol CIChar( 10 ),
      ytasck CIChar( 10 ),
      ytaera CIChar( 10 ),
      ytaeta CIChar( 10 ),
      ytamed CIChar( 10 ),
      ytafuc CIChar( 10 ),
      ytasuc CIChar( 10 ),
      ytacmp CIChar( 10 ),
      ytasdi CIChar( 10 ),
      ytacon CIChar( 10 )) IN DATABASE;      
      
DROP TABLE __tempSDPTECH;
DROP TABLE __edwEmployeeDimOld;

DROP TABLE [__tempPYPCLKCTL];
CREATE TABLE [tmpPYPCLKCTL] ( 
      [yicco#] CIChar( 3 ),
      yicdept CIChar( 2 ),
      yicdesc CIChar( 30 ),
      yicdsw CIChar( 1 ),
      yicsort CIChar( 1 ),
      yicapp1 CIChar( 2 ),
      yicapp2 CIChar( 2 ),
      yicapp3 CIChar( 2 ),
      yicapp4 CIChar( 2 )) IN DATABASE;

      
DROP TABLE __tempEmpDim;

alter PROCEDURE stgArkPYPCLOCKIN
   ( 
   ) 
BEGIN 
/*
name IS goofy because an object (TABLE) already exists with the name stgArkonaPYPCLOCKIN
deletes the last 28 days from last sunday of data FROM stgArkonaPYPCLOCKIN
AND replaces it with the contents of tmpPYPCLOCKIN
DON'T Zap tmpPYPCLOCKIN, will be used to populate edwClockHoursFact 
EXECUTE PROCEDURE stgArkPYPCLOCKIN();
*/

TRY 
  IF ( -- duplicates
    SELECT COUNT(*)
    FROM (
      SELECT yico#, yiemp#, yiclkind, yiclkint
      FROM tmpPYPCLOCKIN
      GROUP BY yico#, yiemp#, yiclkind, yiclkint
        HAVING COUNT(*) > 1) x) <> 0 THEN 
    RAISE SPstgArkPYPCLOCKIN(100, 'dups IN tmp');
  END IF;
  BEGIN TRANSACTION;
  TRY
    DELETE 
    FROM stgArkonaPYPCLOCKIN
    WHERE yiclkind >= (
      SELECT MIN(yiclkind)
      FROM tmpPYPCLOCKIN); 
    INSERT INTO stgArkonaPYPCLOCKIN
    SELECT * FROM tmpPYPCLOCKIN;   
  COMMIT WORK;
  CATCH ALL
    ROLLBACK;
    RAISE SPstgArkPYPCLOCKIN(200, 'transaction failed');
  END TRY; //TRANSACTION
--  EXECUTE PROCEDURE sp_zaptable('tmpPYPCLOCKIN');
  EXECUTE PROCEDURE sp_packtable('stgArkonaPYPCLOCKIN');
  IF ( -- duplicates
    SELECT COUNT(*)
    FROM (
      SELECT yico#, yiemp#, yiclkind, yiclkint
      FROM stgArkonaPYPCLOCKIN
      GROUP BY yico#, yiemp#, yiclkind, yiclkint
        HAVING COUNT(*) > 1) x) <> 0 THEN 
    RAISE SPstgArkPYPCLOCKIN(300, 'dups IN stgArkonaPYPCLOCKIN');
  END IF;  
CATCH ALL
  RAISE;
END TRY; 








END;

alter PROCEDURE ChangesInPYPCLKCTL
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE ChangesInPYPCLKCTL();

this will WORK for small tables that are zap/reloaded
of course, IF a change IS detected, have to figure out what the fuck to DO

INSERT INTO tmpPYPCLKCTL
SELECT * FROM stgArkonaPYPCLKCTL;

*/
DECLARE @stgCount integer;
DECLARE @tempCount integer;

@stgCount = (SELECT COUNT(*) FROM stgArkonaPYPCLKCTL);
@tempCount = (SELECT COUNT(*) FROM tmpPYPCLKCTL);

IF @stgCount - @tempCount <> 0 THEN -- different number of records
  RAISE spPypclkctlChanges (100, 'stgCount <> tempCount');
END IF; 

IF (-- conflicting yicdept
  SELECT COUNT(*) 
  FROM stgArkonaPYPCLKCTL a
  INNER JOIN tmpPYPCLKCTL b ON a.yicco# = b.yicco#
    AND a.yicdept = b.yicdept
    AND a.yicdesc <> b.yicdesc) <> 0 THEN 
  RAISE spPypclkctlChanges (200, 'conflicting yicdept');
END IF; 

-- these are necessary to verify exact matches
IF (-- EXISTS IN stg NOT temp
  SELECT COUNT(*) 
  FROM stgArkonaPYPCLKCTL a
  WHERE NOT EXISTS (
    SELECT 1
    FROM tmpPYPCLKCTL
    WHERE yicco# = a.yicco#
    AND yicdept = a.yicdept)) <> 0 THEN
  RAISE spPypclkctlChanges (300, 'exists IN stg NOT temp');
END IF; 

IF (-- exixts IN temp NOT stg
  SELECT COUNT(*) 
  FROM tmpPYPCLKCTL a
  WHERE NOT EXISTS (
    SELECT 1
    FROM stgArkonaPYPCLKCTL
    WHERE yicco# = a.yicco#
      AND yicdept = a.yicdept)) <> 0 THEN 
  RAISE spPypclkctlChanges (400, 'exists IN temp NOT stg');
END IF;	  



END;



alter PROCEDURE stgArkPYACTGR
   ( 
   ) 
BEGIN 
/*
EXECUTE PROCEDURE stgArkPYACTGR();
1/26/12
IF this throws an error, will have to figure out at that time how to deal with it
IF any error, nothing gets done to stgArkonaPYACTGR
this TABLE IS requ'd for edwEmployeeDim 

*/

IF ( -- unique store, dist code, description
  SELECT COUNT(*)
  FROM (
    SELECT ytaco#, ytadic, ytadsc 
    FROM (
      SELECT ytaco#, ytadic, ytadsc 
      FROM tmpPYACTGR
      GROUP BY ytaco#, ytadic, ytadsc) x
    GROUP BY ytaco#, ytadic, ytadsc 
      HAVING COUNT(*) > 1) y) <> 0 THEN 
  RAISE stgArkPYACTGR (200, 'non unique store/dist code/description in tmpPYACTGR');
END IF;

-- these are necessary to verify exact matches
IF (-- EXISTS IN stg NOT temp
  SELECT COUNT(*) 
  FROM stgArkonaPYACTGR a
  WHERE NOT EXISTS (
    SELECT 1
    FROM tmpPYACTGR
    WHERE ytaco# = a.ytaco#
      AND ytadic = a.ytadic
      AND ytaseq = a.ytaseq)) <> 0 THEN
  RAISE stgArkPYACTGR (300, 'exists IN stg NOT temp');
END IF; 

IF (-- exixts IN temp NOT stg
  SELECT COUNT(*) 
  FROM tmpPYACTGR a
  WHERE NOT EXISTS (
    SELECT 1
    FROM stgArkonaPYACTGR
    WHERE ytaco# = a.ytaco#
      AND ytadic = a.ytadic
      AND ytaseq = a.ytaseq)) <> 0 THEN
  RAISE stgArkPYACTGR (400, 'exists IN temp NOT stg');
END IF;	  


IF (-- description has changed
  SELECT COUNT(*)
  FROM tmpPYACTGR t
  INNER JOIN stgArkonaPYACTGR s ON t.ytaco# = s.ytaco#
    AND t.ytadic = s.ytadic
    AND t.ytadsc <> s.ytadsc) <> 0 THEN 
  RAISE stgArkPYACTGR (500, 'description has changed in PYACTGR');
END IF;  

execute procedure sp_ZapTable('stgArkonaPYACTGR');
INSERT INTO stgArkonaPYACTGR
SELECT * FROM tmpPYACTGR;




END;


