SELECT b.name, b.last, b.notlast, b.first,
  CASE position(' '  IN trim(substring(name, position(',' IN notlast)+1,25)))
    WHEN 0 THEN ''
    ELSE substring(notlast, position(' ' IN notlast) + 1, 25)
  END AS middle
FROM (
  SELECT name, 
    CASE position(',' IN name) -- one name with no commas
      WHEN 0 THEN LEFT(name, position (' ' IN name)- 1) 
      ELSE  LEFT(name, position (',' IN name)- 1) 
    END AS last, 
    trim(substring(name, position(',' IN name)+1,25)) AS notLast,
    CASE position(' '  IN trim(substring(name, position(',' IN name)+1,25)))
      WHEN 0 THEN trim(substring(name, position(',' IN name)+1,25))
      ELSE 
        LEFT (trim(substring(name, position(',' IN name)+1,25)), position(' ' IN trim(substring(name, position(',' IN name)+1,25)))) 
    END AS first     
  FROM (
    SELECT DISTINCT name 
    FROM edwEmployeeDim) a) b
ORDER BY name  


  SELECT name, 
    CASE position(',' IN name) -- one name with no commas
      WHEN 0 THEN LEFT(name, position (' ' IN name)- 1) 
      ELSE  LEFT(name, position (',' IN name)- 1) 
    END AS last, 
    trim(substring(name, position(',' IN name)+1,25)) AS notLast,
    CASE position(' '  IN trim(substring(name, position(',' IN name)+1,25)))
      WHEN 0 THEN trim(substring(name, position(',' IN name)+1,25))
      ELSE 
        LEFT (trim(substring(name, position(',' IN name)+1,25)), position(' ' IN trim(substring(name, position(',' IN name)+1,25)))) 
    END AS first     
  FROM (
    SELECT DISTINCT name 
    FROM edwEmployeeDim) a
ORDER BY name    

SELECT *
FROM edwEmployeeDim 
WHERE position(',' IN name) < 2




