  AND year(b.biweeklypayperiodenddate) - 2000 = yhdeyy
  AND month(b.biweeklypayperiodenddate) = yhdemm 
  AND dayofmonth(b.biweeklypayperiodenddate) = yhdedd 
  
a payroll IS a GROUP of paychecks  
-- what are ALL the dates
check date
payroll ending 

SELECT yhdemm, yhdedd, yhdeyy + 2000, yhdcmm, yhdcdd, yhdcyy + 2000,
  b.thedate AS PayrollDate, b.datekey AS PayrollDateKey,
  c.thedate AS CheckDate, c.datekey AS CheckDateKey
FROM stgArkonaPYHSHDTA a
LEFT JOIN day b ON a.yhdemm = b.MonthOfYear
  AND a.yhdedd = b.DayOfMonth
  AND a.yhdeyy + 2000 = b.theyear
LEFT JOIN day c ON a.yhdcmm = b.MonthOfYear
  AND a.yhdcdd = b.DayOfMonth
  AND a.yhdcyy + 2000 = b.theyear 
WHERE b.thedate IS NULL 
  OR c.thedate IS NULL   

DROP TABLE xfmPayCheck;  
CREATE TABLE xfmPayCheck (
  StoreCode cichar(3),
  Batch integer, 
  CheckDate date,
  CheckDateKey integer,
  PayrollDate date,
  PayrollDateKey integer,
  StoreCode, ) IN database;  
EXECUTE PROCEDURE sp_zaptable('xfmPayCheck'); 
-- 1 row per store/batch with dates 
INSERT INTO xfmPayCheck 
SELECT yhdco# AS StoreCode, ypbnum AS Batch,
  max(c.thedate) AS CheckDate, max(c.datekey) AS CheckDateKey, 
  max(b.thedate) AS PayrollDate, max(b.datekey) AS PayrollDateKey
FROM stgArkonaPYHSHDTA a
LEFT JOIN day b ON a.yhdemm = b.MonthOfYear
  AND a.yhdedd = b.DayOfMonth
  AND a.yhdeyy + 2000 = b.theyear
LEFT JOIN day c ON a.yhdcmm = c.MonthOfYear
  AND a.yhdcdd = c.DayOfMonth
  AND a.yhdcyy + 2000 = c.theyear 
WHERE a.ymdept <> '' 
--  AND (a.yhdeyy <> 9 AND a.yhdemm = 4 AND a.yhdedd <> 1)
--  AND (a.yhdeyy <> 9 AND a.yhdemm <> 7 AND a.yhdedd <> 1)
  AND 
    CASE WHEN a.yhdeyy = 9 THEN a.yhdemm <> 4 ELSE 1 = 1 END
  AND 
    CASE WHEN a.yhdeyy = 9 AND a.yhdemm  = 7 THEN a.yhdedd <> 1 ELSE 1 = 1 END 
GROUP BY yhdco#, ypbnum

 100 + (year(@ThruDate) - 2000) 
-- DROP TABLE #wtf 
SELECT a.*, yhdco#, ypbnum, ymname, yhdemp, ymdept, ymdist, yhhdte, yhclas, yhpper, yhrate, yhsaly,
  a.storecode, batch, payrolldate, name, employeekey, employeenumber, pydeptcode, 
    distcode, hiredate, payrollclasscode, payperiodcode, salary, hourlyrate
INTO #wtf    
FROM xfmPayCheck a 
LEFT JOIN stgArkonaPyhshdta b ON a.storecode = b.yhdco#
  AND a.batch = b.ypbnum
  AND 100 + (year(a.payrolldate) - 2000) = b.ypbcyy
LEFT JOIN edwEmployeeDim c ON c.Employeekeyfromdate = (  
    SELECT MAX(Employeekeyfromdate)
    FROM edwEmployeeDim 
    WHERE employeenumber = c.employeenumber
      AND employeekeyfromdate <= a.checkdate-- a.payrolldate  -- welli, RAISE comes after payroll date, but before checkdate
    GROUP BY employeenumber)   
  AND b.yhdemp = c.employeenumber    

-- there are going to be lots with dept discrepancies 
SELECT *
FROM #wtf
WHERE ymdept <> pydeptcode   
-- fuck, there are going to be a zillion discrepancies
-- FROM the time before i have edwEmployeeDim history
SELECT *
FROM #wtf
WHERE yhrate <> hourlyrate

SELECT rowchangedate FROM edwEmployeeDim WHERE rowchangedate IS NOT NULL ORDER BY rowchangedate

SELECT * FROM #wtf WHERE (ymname like 'WELL%' OR name like 'WELL%') ORDER BY payrolldate


DROP TABLE #ark;
SELECT Max(ymname) AS ymname, yhdemp, yhrate, MAX(payrolldate) AS payrolldate
INTO #ark
FROM #wtf
GROUP BY yhdemp, yhrate;
DROP TABLE #dds;
SELECT max(name) AS name, employeenumber, hourlyrate, MAX(payrolldate) AS payrolldate
INTO #dds
FROM #wtf
GROUP BY employeenumber, hourlyrate;

-- so this looks LIKE ark has a payrolldate of 3/01/12 AND dds doesn't
-- AND dds has a payrolldate of 3/10/12 AND ark doesn't
-- ok, this looks LIKE it's getting CLOSE to what i'm looking for
-- 3/10 dds: $10  ark 9
-- looks LIKE payroll uses the check date, NOT the payroll date for salaries, etc
-- welli got his RAISE ON 3/13, payroll date IS 3/10, check date IS 3/16
SELECT *
FROM #ark a
full OUTER JOIN #dds b ON a.yhdemp = b.employeenumber
--  AND a.yhrate = b.hourlyrate  
  AND a.payrolldate = b.payrolldate
WHERE (ymname like 'BASNET%' OR name like 'BASNET%')  

SELECT * FROM #wtf WHERE (ymname like 'BASNET%' OR name like 'BASNET%') ORDER BY payrolldate

-- fuck, it goes ON AND ON
-- this shows
SELECT *
FROM (
SELECT batch, checkdate, payrolldate, name, yhdemp, yhrate, yhsaly, salary, hourlyrate
FROM #wtf
GROUP BY batch, checkdate, payrolldate, name, yhdemp, yhrate, yhsaly, salary, hourlyrate) z
WHERE yhrate <> hourlyrate
--AND year(payrolldate) = 2012
ORDER BY name, payrolldate

SELECT *
FROM (
SELECT min(checkdate) AS minCheckDate, MAX(checkdate) AS maxCheckDate, 
  min(payrolldate) AS minPayRollDate, MAX(payrolldate) AS maxPayrolldate, 
  max(name) AS name, yhdemp, yhrate, yhsaly, salary, hourlyrate
FROM #wtf
GROUP BY yhdemp, yhrate, yhsaly, salary, hourlyrate) z
WHERE yhrate <> hourlyrate
AND year(minPayRollDate) = 2012
ORDER BY name

SELECT *
FROM (
SELECT min(checkdate) AS minCheckDate, MAX(checkdate) AS maxCheckDate, 
  min(payrolldate) AS minPayRollDate, MAX(payrolldate) AS maxPayrolldate, 
  name, yhdemp, yhrate, yhsaly, salary, hourlyrate
FROM #wtf
GROUP BY batch, checkdate, payrolldate, name, yhdemp, yhrate, yhsaly, salary, hourlyrate) z
WHERE yhrate <> hourlyrate
--AND year(payrolldate) = 2012
ORDER BY name



SELECT a.ypbcyy, a.ypbnum, a.yhdco#, c.*
FROM  stgArkonaPyhshdta a
LEFT JOIN (
  SELECT b.*, 100 + (year(payrolldate) - 2000) AS CenYear 
  FROM xfmPayCheck b) c ON a.yhdco# = c.storecode
  AND a.ypbnum = c.batch
  AND a.ypbcyy = c.cenyear
WHERE c.storecode IS NOT NULL   

SELECT DISTINCT yhdemm, yhdedd, yhdeyy + 2000
FROM stgArkonaPYHSHDTA
ORDER BY yhdeyy + 2000, yhdemm, yhdedd 

-- DROP TABLE #wtf
SELECT yhdco#, ypbnum, ymname, yhdemp, ymdept, ymdist, yhhdte, yhclas, yhpper, yhrate, yhsaly, d.*
-- INTO #wtf
FROM stgArkonaPYHSHDTA c
LEFT JOIN (
  SELECT a.storecode, batch, payrolldate, name, employeekey, employeenumber, pydeptcode, 
    distcode, hiredate, payrollclasscode, payperiodcode, salary, hourlyrate
  FROM xfmPayCheck a
  LEFT JOIN edwEmployeeDim b ON a.PayrollDate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  WHERE b.Employeekeyfromdate = (
    SELECT MAX(Employeekeyfromdate)
    FROM edwEmployeeDim 
    WHERE employeenumber = b.employeenumber
      AND employeekeyfromdate <= a.payrolldate
    GROUP BY employeenumber)) d ON c.yhdco# = d.storecode
  AND c.ypbnum = d.batch
  AND c.yhdemp = d.employeenumber  
WHERE c.ymdept <> '' 
  AND 
    CASE WHEN c.yhdeyy = 9 THEN c.yhdemm <> 4 ELSE c.yhdemm = month(d.payrolldate)
      AND c.yhdedd = dayofmonth(d.payrolldate)
      AND c.yhdeyy + 2000 = year(d.payrolldate) END
  AND 
    CASE WHEN c.yhdeyy = 9 AND c.yhdemm  = 7 THEN c.yhdedd <> 1 ELSE c.yhdemm = d.MonthOfYear
      AND c.yhdedd = d.DayOfMonth
      AND c.yhdeyy + 2000 = c.theyear END

    
c.yhdemm = d.MonthOfYear
  AND c.yhdedd = d.DayOfMonth
  AND c.yhdeyy + 2000 = c.theyear    
  
SELECT * FROM xfmPayCheck WHERE batch = 930000

SELECT payrolldate, COUNT(*)
FROM #wtf
group BY payrolldate

SELECT * FROM #wtf WHERE payrolldate IS NULL 

WHERE pydeptcode <> ymdept  

SELECT *
FROM stgArkonaPYHSHDTA
WHERE ymdept = ''


WHERE payrolldate > 7/31

-- 9/4 
too much historical discrpancy (salary, dept, dist ...) 
i don''t have employeekeys that conform with payroll before jan 2012
so, take the rptTable approach, except it''s NOT really
rpt implies derived FROM fact/dim
this IS not
it IS a a cleaned up scrape of pyhshdta
useful BUT NOT CONFORMED

raise on 3/13, payroll date IS 3/10, check date IS 3/16

payroll END date:  08/25/2012
RAISE date:        08/27/2012
check date:        08/30/2012
hourly rate ON check includes the RAISE
accrual for the payperiod uses the valed employeekey at END of the payperiod, 
which does NOT include the RAISE, because the empnumber gets a new employeekey with the RAISE
after the payroll END date

i am accruing based ON  employeekey valid ON the last day of the month

DROP TABLE xfmPayCheck;  
CREATE TABLE xfmPayCheck (
/*[YHDCO#]*/         [StoreCode]                        cichar(3),
/*[YPBCYY]*/         [PAYROLL CEN + YEAR]               integer,
/*[YPBNUM]*/         [Batch]                            integer,
/*[YHDEMP]*/         [EmployeeNumber]                   cichar(7),
/*[YHDSEQ]*/         [Seq]                              cichar(2),
/*[YHDREF]*/         [CheckNumber]                      cichar(12),
--/*[YHDECC]*/         [PAYROLL CENTURY]                  integer,
--/*[YHDEYY]*/         [PAYROLL ENDING YEAR]              integer,
--/*[YHDEMM]*/         [PAYROLL ENDING MONTH]             integer,
--/*[YHDEDD]*/         [PAYROLL ENDING DAY]               integer,
--/*[YHDCCC]*/         [CHECK CENTURY]                    integer,
--/*[YHDCYY]*/         [CHECK YEAR]                       integer,
--/*[YHDCMM]*/         [CHECK MONTH]                      integer,
--/*[YHDCDD]*/         [CHECK DAY]                        integer,
                     CheckDate                          date,
                     CheckDateKey                       integer,
                     PayrollEndDate                     date,
                     PayrollEndDateKey                  integer,
/*[YHDBSP]*/         [BASE PAY]                         money,
/*[YHDTGP]*/         [1  TOTAL GROSS PAY]               money,
/*[YHDTGA]*/         [2  TOTAL ADJ GROSS]               money,
/*[YHCFED]*/         [3  CURR FEDERAL TAX]              money,
/*[YHCSSE]*/         [4  CURR FICA]                     money,
/*[YHCSSM]*/         [5  CURR EMPLR FICA]               money,
/*[YHCMDE]*/         [6  CURR EMPLOYEE MEDICARE]        money,
/*[YHCMDM]*/         [7  CURR EMPLOYER MEDICARE]        money,
/*[YHCEIC]*/         [8  YTD  EIC PAYMENTS]             money,
/*[YHCST]*/          [9  CURR STATE TAX]                money,
/*[YHCSDE]*/         [10 CURR EMPLOYEE SDI]             money,
/*[YHCSDM]*/         [11 CURR EMPLOYER SDI]             money,
/*[YHCCNT]*/         [12 CURR COUNTY TAX]               money,
/*[YHCCTY]*/         [13 CURR CITY TAX]                 money,
/*[YHCFUC]*/         [14 CURR FUTA]                     money,
/*[YHCSUC]*/         [15 CURR SUTA]                     money,
/*[YHCCMP]*/         [16 WORKMANS COMP]                 money,
/*[YHCRTE]*/         [17 EMPLY CURR RET]                money,
/*[YHCRTM]*/         [18 EMPLR CURR RET]                money,
/*[YHDOTA]*/         [19 OVERTIME AMOUNT]               money,
/*[YHDDED]*/         [20 TOTAL DEDUCTION]               money,
/*[YHCOPY]*/         [21 TOTAL OTHER PAY]               money,
/*[YHCNET]*/         [22 CURRENT NET]                   money,
/*[YHCTAX]*/         [23 CURRENT TAX TOT]               money,
/*[YHAFED]*/         [1  CURR ADJ FED]                  money,
/*[YHASS]*/          [2  CURR ADJ SS]                   money,
/*[YHAFUC]*/         [3  CURR ADJ FUTA]                 money,
/*[YHAST]*/          [4  CURR ADJ STATE]                money,
/*[YHACN]*/          [5  CURR ADJ CNTY]                 money,
/*[YHAMU]*/          [6  CURR ADJ CITY]                 money,
/*[YHASUC]*/         [7  CURR ADJ SUTA]                 money,
/*[YHACM]*/          [8  CURR ADJ COMP]                 money,
/*[YHDVAC]*/         [1  VACATION TAKEN]                double(2),
/*[YHDHOL]*/         [2  HOLIDAY TAKEN]                 double(2),
/*[YHDSCK]*/         [3  SICK LEAVE TAKEN]              double(2),
/*[YHAVAC]*/         [4  VACATION ACC]                  double(2),
/*[YHAHOL]*/         [5  HOLIDAY ACC]                   double(2),
/*[YHASCK]*/         [6  SICK LEAVE ACC]                double(2),
/*[YHDHRS]*/         [7 REG HOURS]                      double(2),
/*[YHDOTH]*/         [8 OVERTIME HOURS]                 double(2),
/*[YHDAHR]*/         [9  ALT PAY  HOURS]                double(2),
/*[YHVACD]*/         [10 DEF VAC HRS]                   double(2),
/*[YHHOLD]*/         [11 DEF HOL HRS]                   double(2),
/*[YHSCKD]*/         [12 DEF SCK HRS]                   double(2),
/*[YHXVAC]*/         [VAC]                              cichar(1),
/*[YHXSCK]*/         [SICK LEAVE]                       cichar(1),
/*[YHXHOL]*/         [HOLIDAY]                          cichar(1),
/*[YHXFED]*/         [FEDERAL TAX EXEMPT]               cichar(1),
/*[YHXSS]*/          [FICA EXEMPT]                      cichar(1),
/*[YHXSIT]*/         [STATE TAX EXEMPT]                 cichar(1),
/*[YHXCNT]*/         [COUNTY TAX EXEMPT]                cichar(1),
/*[YHXMU]*/          [CITY TAX EXEMPT]                  cichar(1),
/*[YHHDTE]*/         [HireDate]                         integer,
/*[YHMARI]*/         [MARITAL STATUS]                   cichar(1),
/*[YHPPER]*/         [PAY PERIOD]                       cichar(1),
/*[YHCLAS]*/         [PAYROLL CLASS]                    cichar(1),
/*[YHEXSS]*/         [EXEMPT FROM FICA]                 cichar(1),
/*[YHFDEX]*/         [FEDERAL EXEMPTIONS]               integer,
/*[YHFDAD]*/         [FEDERAL ADD ON AMOUNT]            money,
/*[YHEEIC]*/         [EIC ELIGABLE]                     cichar(1),
/*[YHECLS]*/         [EIC CLASS]                        cichar(1),
/*[YHSTEX]*/         [STATE EXEMPTIONS]                 integer,
/*[YHSTFX]*/         [STATE FIXED EXEMPTIONS]           money,
/*[YHSTAD]*/         [STATE TAX ADD ON AMOUNT]          money,
/*[YHWKCD]*/         [WKMAN COMP CODE]                  integer,
/*[YHCWFL]*/         [CUM WAGE WITHHLD]                 cichar(1),
/*[YHSTTX]*/         [STATE TAX TAB CODE]               cichar(5),
/*[YHCNTX]*/         [COUNTY TAX TAB CODE]              cichar(5),
/*[YHMUTX]*/         [CITY TAX TAB CODE]                cichar(5),
/*[YHSTCD]*/         [STATE TAX TAB CODE]               cichar(5),
/*[YHCNCD]*/         [COUNTY TAX TAB CODE]              cichar(5),
/*[YHMUCD]*/         [CITY TAX TAB CODE]                cichar(5),
/*[YHSALY]*/         [BASE SALARY]                      money,
/*[YHRATE]*/         [BASE HRLY RATE]                   money,
/*[YHSALA]*/         [ALT  SALARY]                      money,
/*[YHRATA]*/         [ALT  HRLY RATE]                   money,
/*[YHRETE]*/         [ELIG FOR RETIRE]                  cichar(1),
/*[YHRETP]*/         [RETIREMENT %]                     double(2),
/*[YHRFIX]*/         [RETIREMENT FIXED AMOUNT]          money,
/*[YMDVYR]*/         [DEFERRED VAC YEAR]                integer,
/*[YMDHYR]*/         [DEFERRED HOL YEAR]                integer,
/*[YMDSYR]*/         [DEFFERED SIC YEAR]                integer,
/*[YMNAME]*/         [Name]                             cichar(25),
/*[YMDEPT]*/         [pyDeptCode]                       cichar(2),
/*[YMSECG]*/         [SECURITY GROUP]                   integer,
/*[YMSS#]*/          [SOC SEC NUMBER]                   integer,
/*[YMDIST]*/         [DistCode]                         cichar(4),
/*[YMAR#]*/          [A/R CUSTOMER #]                   cichar(10),
/*[YHVOID]*/         [VOIDED FLAG]                      cichar(1),
/*[YHVCYY]*/         [PAYROLL CEN + YEAR]               integer,
/*[YHVNUM]*/         [PAYROLL RUN NUMBER]               integer,
/*[YHVSEQ]*/         [SEQ #/VOID]                       cichar(2),
/*[YHTYPE]*/         [RECORD TYPE]                      cichar(1),
/*[YHACTY]*/         [Adjusted city wages]              money,
/*[YHASDIW]*/        [Adjusted SDI wages]               money,
/*[YHAOTR]*/         [Adjusted other wages]             money,
/*[YHDIRD]*/         [Direct deposit indicator]         cichar(1),
/*[YHFTAP]*/         [Federal additional tax %]         double(2),
/*[YHOLCS]*/         [Show holiday on check]            cichar(1),
/*[YHSTAP]*/         [State additional tax %]           double(2),
/*[YHXFUT]*/         [FUTA override]                    cichar(1),
/*[YHXOVR]*/         [Additional override]              cichar(1),
/*[YHXRTE]*/         [Employee retirement override]     cichar(1),
/*[YHXRTM]*/         [Employer retirement override]     cichar(1),
/*[YHXSDE]*/         [SDI employee override]            cichar(1),
/*[YHXSDM]*/         [SDI employer override]            cichar(1),
/*[YHXSUT]*/         [SUTA override]                    cichar(1),
/*[YHCSUE]*/         [Curr Emplee SUTA]                 money,
/*[YHCST2]*/         [Curr 2nd State Whld]              money,
/*[YHCCN2]*/         [Curr 2nd Cnty Whld]               money,
/*[YHCCT2]*/         [Curr 2nd City Whld]               money,
/*[YHFDST]*/         [Federal Filing Status]            cichar(1),
/*[YHSTST]*/         [State Filing Status]              cichar(1),
/*[YHCNST]*/         [County Filing Status]             cichar(1),
/*[YHMUST]*/         [City Filing Status]               cichar(1),
/*[YHSTS2]*/         [State Filing Status 2]            cichar(1),
/*[YHCNS2]*/         [County Filing Status 2]           cichar(1),
/*[YHMUS2]*/         [City Filing Status 2]             cichar(1),
/*[YHSTT2]*/         [STATE TAX TAB CODE2]              cichar(5),
/*[YHCNT2]*/         [COUNTY TAX TAB CODE2]             cichar(5),
/*[YHMUT2]*/         [CITY TAX TAB CODE2]               cichar(5),
/*[YHSDST]*/         [SDI STATE]                        cichar(5),
/*[YHWKST]*/         [WRKR COMP STATE]                  cichar(5),
/*[YHRPT1]*/         [REPORTING CODE 1]                 cichar(5),
/*[YHRPT2]*/         [REPORTING CODE 2]                 cichar(5),
/*[YHEEOC]*/         [EEOC]                             cichar(2),
/*[YHOTEX]*/         [OVERTIME EXEMPT]                  cichar(1),
/*[YHHGHC]*/         [HIGH COMPENSATE FLAG]             cichar(1),
/*[YHTMP01]*/        [NOTUSED 1]                        cichar(1),
/*[YHTMP02]*/        [NOTUSED 2]                        cichar(1),
/*[YHTMP03]*/        [NOTUSED 3]                        cichar(1),
/*[YHTMP04]*/        [NOTUSED 4]                        cichar(1),
/*[YHTMP05]*/        [NOTUSED 5]                        cichar(1),
/*[YHTMP06]*/        [NOTUSED 6]                        cichar(10),
/*[YHTMP07]*/        [NOTUSED 7]                        cichar(10),
/*[YHTMP08]*/        [NOTUSED 8]                        money,
/*[YHTMP09]*/        [NOTUSED 9]                        money,
                     RowFromDate                        date,
                     RowThruDate                        date) IN database;


EXECUTE PROCEDURE sp_zaptable('xfmPayCheck');
INSERT INTO xfmPayCheck
SELECT YHDCO#, YPBCYY, YPBNUM, YHDEMP, YHDSEQ, YHDREF, 
  c.thedate AS CheckDate, c.datekey AS CheckDateKey, 
  b.thedate AS PayrollDate, b.datekey AS PayrollDateKey,  
  YHDBSP, YHDTGP, YHDTGA, YHCFED, YHCSSE, 
  YHCSSM, YHCMDE, YHCMDM, YHCEIC, YHCST, YHCSDE, YHCSDM, YHCCNT, YHCCTY, YHCFUC, 
  YHCSUC, YHCCMP, YHCRTE, YHCRTM, YHDOTA, YHDDED, YHCOPY, YHCNET, YHCTAX, YHAFED, 
  YHASS, YHAFUC, YHAST, YHACN, YHAMU, YHASUC, YHACM, YHDVAC, YHDHOL, YHDSCK, 
  YHAVAC, YHAHOL, YHASCK, YHDHRS, YHDOTH, YHDAHR, YHVACD, YHHOLD, YHSCKD, 
  YHXVAC, YHXSCK, YHXHOL, YHXFED, YHXSS, YHXSIT, YHXCNT, YHXMU, YHHDTE, 
  YHMARI, YHPPER, YHCLAS, YHEXSS, YHFDEX, YHFDAD, YHEEIC, YHECLS, YHSTEX, 
  YHSTFX, YHSTAD, YHWKCD, YHCWFL, YHSTTX, YHCNTX, YHMUTX, YHSTCD, YHCNCD, 
  YHMUCD, YHSALY, YHRATE, YHSALA, YHRATA, YHRETE, YHRETP, YHRFIX, YMDVYR, 
  YMDHYR, YMDSYR, YMNAME, YMDEPT, YMSECG, YMSS#, YMDIST, YMAR#, YHVOID, 
  YHVCYY, YHVNUM, YHVSEQ, YHTYPE, YHACTY, YHASDIW, YHAOTR, YHDIRD, YHFTAP, 
  YHOLCS, YHSTAP, YHXFUT, YHXOVR, YHXRTE, YHXRTM, YHXSDE, YHXSDM, YHXSUT, 
  YHCSUE, YHCST2, YHCCN2, YHCCT2, YHFDST, YHSTST, YHCNST, YHMUST, YHSTS2, 
  YHCNS2, YHMUS2, YHSTT2, YHCNT2, YHMUT2, YHSDST, YHWKST, YHRPT1, YHRPT2, 
  YHEEOC, YHOTEX, YHHGHC, YHTMP01, YHTMP02, YHTMP03, YHTMP04, YHTMP05, YHTMP06, 
  YHTMP07, YHTMP08, YHTMP09, curdate(), 
  CASE  
    WHEN yhvoid = 'V' THEN curdate() 
    ELSE cast('12/31/9999' AS sql_date)
  END
FROM stgArkonaPYHSHDTA a
LEFT JOIN day b ON a.yhdemm = b.MonthOfYear
  AND a.yhdedd = b.DayOfMonth
  AND a.yhdeyy + 2000 = b.theyear
LEFT JOIN day c ON a.yhdcmm = c.MonthOfYear
  AND a.yhdcdd = c.DayOfMonth
  AND a.yhdcyy + 2000 = c.theyear 
  
SELECT DISTINCT yhvoid FROM stgArkonaPYHSHDTA  
  
-- grain
SELECT storecode, batch, employeenumber, year(checkdate)
FROM xfmPayCheck
GROUP BY storecode, batch, employeenumber, year(checkdate)
  HAVING COUNT(*) > 1   

SELECT * FROM xfmpaycheck WHERE [payroll cen + year] = 112 AND storecode = 'ry1' AND checkdate = '08/31/2012' AND employeenumber = '11650'

SELECT DISTINCT payrollenddate, checkdate FROM xfmpaycheck WHERE [payroll cen + year] = 112 AND storecode = 'ry1' AND [pay period] = 'b' AND [payroll class] = 'h'

-- 9/6
ok i am going fucking nuts
payrollenddate IS NOT matching up with biweekly payrollenddate
i DO NOT fucking understand
last paycheck, i thought payper END would be 8/25
but batch 831000 shows 8/29
so check hours

emp#     hours   ot hours  pto hours
11650    73.5    2.64      8.0

SELECT *
FROM #wtf
WHERE employeenumber = '11650'
  AND biweeklypayperiodenddate = '08/25/2012'

select employeenumber, month(checkdate), SUM([7 reg hours]) AS regHours,
  SUM([8 overtime hours]) AS othours, SUM([3  SICK LEAVE TAKEN]) AS ptohours, 
  SUM([1  vacation taken]) AS vachours
FROM xfmpaycheck WHERE [payroll cen + year] = 112 AND storecode = 'ry1' -- AND checkdate = '08/31/2012' AND employeenumber = '11650'
GROUP BY employeenumber, month(checkdate)

SELECT e.name, e.employeenumber, d.*
FROM (
  select c.monthofyear, b.employeekey, sum(coalesce(ClockHours, 0 )) AS ClockHours,
    sum(coalesce(RegularHours, 0 )) AS RegularHours, 
    sum(coalesce(OvertimeHours, 0 )) AS OvertimeHours, 
    sum(coalesce(VacationHours, 0 )) AS VacationHours,
    sum(coalesce(PTOHours, 0 )) AS PTOHours, 
    sum(coalesce(HolidayHours, 0 )) AS HolidayHours
  from edwClockHoursFact b 
  LEFT JOIN day c ON b.datekey = c.datekey
  WHERE c.thedate BETWEEN '01/01/2012' AND curdate()
  GROUP BY monthofyear, employeekey) d
LEFT JOIN edwEmployeeDim e ON d.employeekey = e.employeekey


SELECT *
FROM (
  select employeenumber, month(checkdate) AS themonth, SUM([7 reg hours]) AS regHours,
    SUM([8 overtime hours]) AS othours, SUM([3  SICK LEAVE TAKEN]) AS ptohours, 
    SUM([1  vacation taken]) AS vachours
  FROM xfmpaycheck WHERE [payroll cen + year] = 112 AND storecode = 'ry1' -- AND checkdate = '08/31/2012' AND employeenumber = '11650'
  GROUP BY employeenumber, month(checkdate)) f
LEFT JOIN (
  SELECT e.name, e.employeenumber, d.*
  FROM (
    select c.monthofyear, b.employeekey, sum(coalesce(ClockHours, 0 )) AS ClockHours,
      sum(coalesce(RegularHours, 0 )) AS RegularHours, 
      sum(coalesce(OvertimeHours, 0 )) AS OvertimeHours, 
      sum(coalesce(VacationHours, 0 )) AS VacationHours,
      sum(coalesce(PTOHours, 0 )) AS PTOHours, 
      sum(coalesce(HolidayHours, 0 )) AS HolidayHours
    from edwClockHoursFact b 
    LEFT JOIN day c ON b.datekey = c.datekey
    WHERE c.thedate BETWEEN '01/01/2012' AND curdate()
    GROUP BY monthofyear, employeekey) d
  LEFT JOIN edwEmployeeDim e ON d.employeekey = e.employeekey) g  ON f.employeenumber = g.employeenumber
  AND f.themonth = g.monthofyear
WHERE themonth = 8  
ORDER BY name