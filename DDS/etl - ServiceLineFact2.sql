CREATE TABLE zAllTheRos (
  ptco# cichar(3),
  ptro# cichar(9),
  ptfran cichar(3),
  ptline integer,
  ptltyp cichar(1),
  ptcode cichar(2),
  pttech cichar(3),
  ptlhrs double,
  ptsvctyp cichar(2),
  ptlpym cichar(1),
  ptdate date,
  ptlamt money,
  ptlopc cichar(10),
  ptcrlo cichar(10)) IN database
  
-- 5/6/12
ALTER TABLE zAllTheRos
--ADD COLUMN source cichar(6);  
ADD COLUMN ptswid cichar(3)
 
-- 16 minutes  
INSERT INTO zAllTheRos
-- 5/4/12: pdet type A: mult dates/lin
--         exclude WHERE ptdate = 0
--         exclude ptdate FROM grouping, SELECT MIN ptdate          
SELECT ph.ptco#, ph.ptdoc# AS ptro#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, min(pd.ptdate) AS ptdate, 0 as ptlamt, pd.ptlopc,
  pd.ptcrlo, 'PDET' AS source, ph.ptswid
FROM stgArkonaPDPPHDR ph
LEFT JOIN stgArkonaPDPPDET pd ON ph.ptco# = pd.ptco#
  AND ph.ptpkey = pd.ptpkey
WHERE ph.ptdtyp = 'RO' 
  AND ph.ptdoc# <> ''
  AND ph.ptco# IN ('RY1','RY2','RY3')
  AND pd.ptdate <> '12/31/9999' -- this could be done IN the scrape: ptdate <> 0 
GROUP BY ph.ptco#, ph.ptdoc#, ph.ptfran, pd. ptline, pd.ptltyp, pd.ptcode,
  pd.pttech, pd.ptlhrs, pd.ptsvctyp, pd.ptlpym, pd.ptlopc, pd.ptcrlo, ph.ptswid 
  
UNION

SELECT sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, max(sd.ptsvctyp) AS ptsvctyp, max(sd.ptlpym) AS ptlpym, 
  sd.ptdate, sd.ptlamt, sd.ptlopc, sd.ptcrlo, 'SDET' AS source, sh.ptswid
FROM stgArkonaSDPRHDR sh
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS (
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#)        
GROUP BY sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptdate, sd.ptlamt, sd.ptlopc, sd.ptcrlo, sh.ptswid;
  
Type Code  Yields                     Grain (store/ro)             
 A    CP||IS||SC||WS
            line creation date        line
            service type              line
            payment method            line 
            opcode                    line        
                                         
 L    TT    tech                      tech
            flag hours                tech, tech/line
 L    CR    
 M
 N
 Z 

-- Every line has an A row -----------------------------------------------------
-- 5/6
-- ptltyp = 'M' AND ptline = 900: paint & materials  
--DROP TABLE #wtf
-- lines with no A record
SELECT *
INTO #wtf
FROM zalltheros z
WHERE NOT EXISTS (
    SELECT 1
    FROM zalltheros
    WHERE ptco# = z.ptco#
      AND ptro# = z.ptro#
      AND ptline = z.ptline
      AND ptltyp = 'A')
  AND ptline < 99
    AND ptltyp <> 'M'     
    
SELECT * FROM stgArkonasdprdet where ptro# = '2620253'    
    
-- lines with no A row    
SELECT *
FROM #wtf 
ORDER BY opendate desc
ORDER BY ptro#, ptline

-- 345 ros
SELECT COUNT(DISTINCT ptro#)
FROM #wtf

-- only 21 FROM 2012
SELECT year(ptdate), COUNT(*)
FROM (
  SELECT ptro#, MAX(ptdate) AS ptdate
  FROM #wtf
  GROUP BY ptro#) x
GROUP BY year(ptdate)  

-- fuck it,  whack them

DELETE FROM zalltheros
--SELECT * FROM zalltheros
WHERE ptro# IN (
  SELECT DISTINCT ptro#
  FROM #wtf)

-- one row per ptco#, ptro#, ptline WHERE A    
SELECT ptco#, ptro#, ptline
FROM(    
  SELECT ptco#, ptro#, ptline, ptltyp
  FROM zalltheros
  WHERE ptltyp = 'A'  
  GROUP BY ptco#, ptro#, ptline, ptltyp) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1  
-- Every line has an A row -----------------------------------------------------


-- so the exercise IS now to verify the above grain
-- A: Line Date: 1 row per store/ro/line/date
-- 5/6
SELECT ptco#, ptro#, ptline
--DROP TABLE #wtf
--INTO #wtf
FROM (
  SELECT ptco#, ptro#, ptline, ptdate
  FROM zalltheros
  WHERE ptltyp = 'A'
  GROUP BY ptco#, ptro#, ptline, ptdate) x
group BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1

-- ros with incomplete A lines
SELECT *
FROM zalltheros
WHERE ptltyp = 'A'
  AND (
    ptdate IS NULL OR
    ptcode IS NULL OR
    ptlopc IS NULL OR
    ptcrlo IS NULL OR
    ptsvctyp IS NULL OR 
    ptlpym IS NULL)
    
DELETE
FROM zalltheros
WHERE ptro# IN ( 
  SELECT ptro#
  FROM zalltheros
  WHERE ptltyp = 'A'
    AND (
      ptdate IS NULL OR
      ptcode IS NULL OR
      ptlopc IS NULL OR
      ptcrlo IS NULL OR
      ptsvctyp IS NULL OR 
      ptlpym IS NULL))    

-- A: ServiceType
-- 5/6
-- one anomaly, 16058003 line 3
SELECT ptco#, ptro#, ptline
INTO #wtf
FROM (
  SELECT ptco#, ptro#, ptline, ptsvctyp
  FROM zalltheros
  WHERE ptltyp = 'A'
  GROUP BY ptco#, ptro#, ptline, ptsvctyp) x
group BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1 

DELETE 
FROM zalltheros 
WHERE rowid = ( -- records with more than 1 service type/line
  SELECT z.rowid
  FROM zalltheros z
  INNER JOIN (
      SELECT ptco#, ptro#, ptline
      FROM (
        SELECT ptco#, ptro#, ptline, ptsvctyp
        FROM zalltheros
        WHERE ptltyp = 'A'
        GROUP BY ptco#, ptro#, ptline, ptsvctyp) x
      GROUP BY ptco#, ptro#, ptline
      HAVING COUNT(*) > 1) w ON z.ptco# = w.ptco#
    AND z.ptro# = w.ptro#
    AND z.ptline = w.ptline
  WHERE z.ptltyp = 'A'  
    AND ptlamt = 0);


-- A: Payment Method
-- 5/6
-- 13 anomalies
-- how to tell FROM just the data which are the correct AND incorrect lines
-- does NOT seem to be any consistent way
SELECT ptco#, ptro#, ptline
-- DROP TABLE #wtf
-- INTO #wtf
FROM (
  SELECT ptco#, ptro#, ptline, ptlpym
  FROM zalltheros
  WHERE ptltyp = 'A'
  GROUP BY ptco#, ptro#, ptline, ptlpym) x
group BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1 

-- so take a hammer to it
DELETE 
FROM zAllTheRos
WHERE ptro# IN (
  SELECT ptro#
  FROM #wtf)
  
SELECT *
FROM zalltheros x
WHERE EXISTS (
  SELECT 1
  FROM #wtf
  WHERE ptco# = x.ptco#
    AND ptro# = x.ptro#
    AND ptline = x.ptline)
--  AND ptltyp = 'A'    
ORDER BY ptco#, ptro#, ptline    

-- A: Op Code
-- 5/6
SELECT ptco#, ptro#, ptline
-- DROP TABLE #wtf
-- INTO #wtf
FROM (
  SELECT ptco#, ptro#, ptline, ptlopc
  FROM zalltheros
  WHERE ptltyp = 'A'
  GROUP BY ptco#, ptro#, ptline, ptlopc) x
group BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1 

-- whack em
DELETE 
FROM zAllTheRos
WHERE ptro# IN (
  SELECT ptro#
  FROM #wtf)

-- 5/6 
-- remaining dups  
-- 9 ros,   
SELECT ptco#, ptro#, ptline, ptsvctyp, ptltyp, ptlpym, ptdate, ptlopc 
FROM zalltheros  
WHERE ptltyp = 'A'
GROUP BY ptco#, ptro#, ptline, ptsvctyp, ptltyp, ptlpym, ptdate, ptlopc
  HAVING COUNT(*) > 1

-- just AS i had hoped, source gives me what i need
SELECT z.*
--DROP TABLE #wtf
INTO #wtf
FROM zalltheros z
INNER JOIN (
    SELECT ptco#, ptro#, ptline, ptsvctyp, ptltyp, ptlpym, ptdate, ptlopc 
    FROM zalltheros  
    WHERE ptltyp = 'A'
    GROUP BY ptco#, ptro#, ptline, ptsvctyp, ptltyp, ptlpym, ptdate, ptlopc
      HAVING COUNT(*) > 1) a ON z.ptco# = a.ptco#
  AND z.ptro# = a.ptro#
  AND z.ptline = a.ptline
  AND z.ptltyp = a.ptltyp      
-- looks LIKE these dups have 1 row for each source  
SELECT * FROM #wtf w
WHERE NOT EXISTS (
  SELECT 1
  FROM #wtf
  WHERE ptro# = w.ptro#
    AND ptline = w.ptline
    AND source = 'PDET')
 
-- so, zap the PDET records  
DELETE
FROM zalltheros 
WHERE rowid IN (  
  SELECT z.rowid
  FROM zalltheros z
  INNER JOIN (
      SELECT ptco#, ptro#, ptline, ptsvctyp, ptltyp, ptlpym, ptdate, ptlopc 
      FROM zalltheros  
      WHERE ptltyp = 'A'
      GROUP BY ptco#, ptro#, ptline, ptsvctyp, ptltyp, ptlpym, ptdate, ptlopc
        HAVING COUNT(*) > 1) a ON z.ptco# = a.ptco#
    AND z.ptro# = a.ptro#
    AND z.ptline = a.ptline
    AND z.ptltyp = a.ptltyp   
  WHERE source = 'PDET')  
  
--------------------------------------------------------------------------------
Type Code  Yields                     Grain (store/ro)             
 A    CP||IS||SC||WS
            line creation date        line
            service type              line
            payment method            line 
            opcode                    line        
                                         
 L    TT    tech                      tech
            flag hours                tech, tech/line/flagdate (?)
 L    CR    
 M
 N
 Z 

SELECT * FROM zalltheros where ptltyp = 'A' ORDER BY ptdate desc, ptro# asc, ptline asc
 
SELECT ptco#, ptro#, ptline, pttech, sum(ptlhrs)
FROM zalltheros
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
GROUP BY ptco#, ptro#, ptline, pttech 

SELECT ptco#, ptro#, ptline
FROM (
SELECT ptco#, ptro#, ptline, pttech
FROM zalltheros
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
GROUP BY ptco#, ptro#, ptline, pttech) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1 

-- ok, i know that a line can have more than one tech
-- can a line have more than 1 cor code?
SELECT ptco#, ptro#, ptline
FROM (
  SELECT ptco#, ptro#, ptline, ptcrlo
  FROM zalltheros
  WHERE ptltyp = 'L'
    AND ptcode = 'CR'
  GROUP BY ptco#, ptro#, ptline, ptcrlo) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1  

-- multiple techs per line
DROP TABLE #tech
SELECT ptco#, ptro#, ptline 
INTO #tech
FROM (
  SELECT ptco#, ptro#, ptline, pttech
  FROM zalltheros
  WHERE pttech IS NOT NULL 
    AND pttech <> ''
  GROUP BY ptco#, ptro#, ptline, pttech) x
GROUP BY ptco#, ptro#, ptline 
HAVING COUNT(*) > 1  

-- multiple cor codes per line
DROP TABLE #cor
SELECT ptco#, ptro#, ptline
INTO #cor
FROM (
  SELECT ptco#, ptro#, ptline, ptcrlo
  FROM  zalltheros 
  WHERE ptcrlo IS NOT NULL
    AND ptcrlo <> ''
  GROUP BY ptco#, ptro#, ptline, ptcrlo) x
GROUP BY ptco#, ptro#, ptline
HAVING COUNT(*) > 1  



-- IS there a TT line for each A-CR line
-- no, eg, 16018568 line 5 L-CR IS a rental, there will be no tech time
SELECT *
FROM zalltheros z
WHERE ptltyp = 'L'
  AND ptcode = 'CR'
  AND NOT EXISTS (
    select 1
    FROM zalltheros
    WHERE ptco# = z.ptco#
      AND ptro# = z.ptro#
      AND ptline = z.ptline
      AND ptcode = 'TT')
-- so the right question IS TT with no CR      
-- oh duh, there will NOT be TT ON many OPEN ros
SELECT *
FROM zalltheros z
WHERE ptltyp = 'L'
  AND ptcode = 'TT'
  AND pttech <> '103'
  AND source = 'SDET'
  AND NOT EXISTS (
    select 1
    FROM zalltheros
    WHERE ptco# = z.ptco#
      AND ptro# = z.ptro#
      AND ptline = z.ptline
      AND source = 'SDET'
      AND ptcode = 'CR')     
ORDER BY ptdate DESC, ptro# asc, ptline asc     

SELECT *
FROM zalltheros

-- so the first thing that pops out to me IS sh labor & parts = 0, total sale IS NOT 0
-- eg, 16078011
SELECT z.ptco#, z.ptro#, sh.ptdate, sh.ptcdat, sh.ptfcdt, sh.ptvin, sh.ptckey, 
  pttest AS Estimate, ptfran, ptptot AS Parts, ptltot AS Labor, ptstot AS Sublet,
  ptcphz AS Haz, ptcpss + ptwass + ptinss + ptscss AS Shop,
  ptpbmf AS "Total Sale", ptpbdl AS "Total Gross" 
FROM (
  SELECT ptco#, ptro#, MIN(ptdate) AS ptdate
  FROM zalltheros 
  GROUP BY ptco#, ptro#) z
LEFT JOIN stgArkonaSDPRHDR sh ON z.ptco# = sh.ptco#
  AND z.ptro# = sh.ptro#  
WHERE sh.ptco# IS NOT NULL
  AND year(z.ptdate) = 2012
  
-- AND 16074228 parts show ON ark ro, parts exist IN pdptdet, but NOT IN SHDR
SELECT z.ptco#, z.ptro#, sh.ptdate, sh.ptcdat, sh.ptfcdt, sh.ptvin, sh.ptckey, 
  pttest AS Estimate, ptfran, ptptot AS Parts, ptltot AS Labor, ptstot AS Sublet,
  ptcphz AS Haz, ptcpss + ptwass + ptinss + ptscss AS Shop,
  ptpbmf AS "Total Sale", ptpbdl AS "Total Gross", z.ptlhrs, z.ptlamt
FROM (  
  SELECT ptco#, ptro#, 
    SUM(CASE when ptltyp = 'L' and ptcode = 'TT' then ptlhrs else 0 end) AS ptlhrs,
    SUM(CASE when ptltyp = 'L' and ptcode = 'TT' then ptlamt else 0 end) AS ptlamt
  FROM zalltheros  
  WHERE year(ptdate) = 2012
  GROUP BY ptco#, ptro#) z
LEFT JOIN stgArkonaSDPRHDR sh ON z.ptco# = sh.ptco#
  AND z.ptro# = sh.ptro#    
WHERE z.ptro# = '16074228'

-- which leads me to believe, at least for closed ros, SHDR IS NOT a good source for totals
-- have to come FROM line detail, OR accting
-- (hrs = 0 & amt <> 0) OR (hrs <> 0 & amt = 0)
DROP TABLE #wtf
SELECT *
INTO #wtf
FROM (  
  SELECT ptco#, ptro#, 
    SUM(CASE when ptltyp = 'L' and ptcode = 'TT' then ptlhrs else 0 end) AS ptlhrs,
    SUM(CASE when ptltyp = 'L' and ptcode = 'TT' then ptlamt else 0 end) AS ptlamt
  FROM zalltheros  
  WHERE year(ptdate) = 2012
  GROUP BY ptco#, ptro#) x
WHERE (
  (ptlhrs = 0 AND ptlamt <> 0) OR
  (ptlhrs <> 0 AND ptlamt = 0))
  
-- shit there's only 500  
SELECT COUNT(*) FROM #wtf

SELECT *
FROM zalltheros
WHERE ptro# IN (
    SELECT distinct ptro#
    FROM #wtf)
AND source = 'SDET' 
AND ptcode = 'TT'   
ORDER BY ptro#, ptline    
 
SELECT z.*, g.gtacct,g.gttamt
FROM zalltheros z
LEFT JOIN stgArkonaGLPTRNS g ON z.ptro# = g.gtdoc#
WHERE ptro# IN (
    SELECT distinct ptro#
    FROM #wtf)
AND source = 'SDET' 
AND ptcode = 'TT'   
AND gtacct IN (
  'BLANK','146000','146001','146002','146007','146020','146027','146100',    
  '146204','146302','146303','146304','146305','146327','146424','147100',    
  '147200','147300','230903','245700','245800','245900','246000','246001',    
  '246100','246101','246102','246200','246300','246302','246303','246305',    
  '246310','246320','346000','346001','346002','346200','346300','346400') 
ORDER BY ptro#, ptline  

SELECT *
FROM (
  SELECT ptro#, SUM(ptlhrs) AS ptlhrs, SUM(ptlamt) AS ptlamt
  FROM zalltheros
  WHERE ptcode = 'TT'
  GROUP BY ptro#) a
LEFT JOIN (
  SELECT gtdoc#, SUM(gttamt)
  FROM stgArkonaGLPTRNS
  WHERE gtacct IN (
  'BLANK','146000','146001','146002','146007','146020','146027','146100',    
  '146204','146302','146303','146304','146305','146327','146424','147100',    
  '147200','147300','230903','245700','245800','245900','246000','246001',    
  '246100','246101','246102','246200','246300','246302','246303','246305',    
  '246310','246320','346000','346001','346002','346200','346300','346400') 
  GROUP BY gtdoc#) b ON a.ptro# = b.gtdoc#

  
-- new car prep sales account NOT IN sdpprice   
SELECT gtdoc#, gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gttamt
FROM stgArkonaGLPTRNS
WHERE gtdoc# = '16008027'

-- narrow this down to salesaccounts
SELECT gtco#, gtacct, COUNT(*)
FROM stgArkonaGLPTRNS
WHERE gtjrnl IN ('SCA','SVI','SWA')
GROUP BY gtco#, gtacct
ORDER BY gtco#, gtacct

SELECT *
FROM stgArkonaGLPMAST
WHERE gmyear = 2009
  AND gmtype = '4'
  AND gmco# = 'ry1'
ORDER BY gmacct  

  
SELECT *
FROM #wtf w
LEFT JOIN zalltheros z ON w.ptco# = z.ptco# 
  AND w.ptro# = z.ptro#
LEFT JOIN   
  
  
 
SELECT w.*, z.gtacct, z.gttamt
-- SELECT *
FROM #wtf w
LEFT JOIN stgArkonaGLPTRNS z ON w.ptco# = z.gtco# 
  AND w.ptro# = z.gtdoc#
  AND gtacct IN (
    SELECT Account
    from edwAccountDim
    WHERE accounttype = '4'
--      AND gldepartmentcode = 'SD' -- include QL & CW
      AND gmfsdepartment = 'Service'
      AND gmfssection = 'Sales')  
  
SELECT * FROM stgArkonaGLPTRNS WHERE gtdoc# = '16078217'

-- ry1 labor sales accts
SELECT *
from edwAccountDim
WHERE accounttype = '4'
--  AND gldepartmentcode = 'SD' -- include QL & CW
  AND gmfsdepartment = 'Service'
  AND gmfssection = 'Sales'


-- FROM pricing MR-C
-- no available categorization for ry1, ry2 labor sales accts
SELECT *
FROM edwAccountDim
WHERE account in ('146000', '246000', '346000')

  
SELECT *
FROM edwAccountDim a
WHERE account IN (  
  --SELECT account, right(trim(account), 5), '2' + right(trim(account), 5), a.*
  SELECT '2' + right(trim(account), 5)
  from edwAccountDim a
  WHERE accounttype = '4'
  --  AND gldepartmentcode = 'SD'
    AND gmfsdepartment = 'Service'
    AND gmfssection = 'Sales')
  OR account IN (
  SELECT '3' + right(trim(account), 5)
  from edwAccountDim a
  WHERE accounttype = '4'
  --  AND gldepartmentcode = 'SD'
    AND gmfsdepartment = 'Service'
    AND gmfssection = 'Sales') 
    
-- < 5/8 -----------------------------------------------------------------------
-- given the above, sales can't be derived FROM a nice simple grouping of 
-- account categories, but will have to be based ON a calc for each tech/serv type/pay meth
-- a sampling of closed ros with beaucoup lines
SELECT sd.ptro#, COUNT(*)
FROM stgArkonaSDPRHDR sh
INNER JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE sh.ptfcdt < curdate()
  AND sh.ptdate >  curdate() - 30
  AND LEFT(sh.ptro#, 2) = '16'
GROUP BY sd.ptro#
ORDER BY COUNT(*) desc  

SELECT *
FROM zalltheros 
WHERE ptro# = '16085942'

-- 5/8 /> ----------------------------------------------------------------------    

-- 5/10 
-- looking for service conract info
-- looks LIKE it IS available only at HDR level
-- find ros with both S & C lines
SELECT *
FROM zalltheros
WHERE ptro# IN (
  SELECT ptro# FROM zalltheros z 
  WHERE source = 'SDET'
    AND ptco# = 'ry1'
    AND ptdate > '04/01/2012'
    AND EXISTS (
      SELECT 1
      FROM zalltheros
      WHERE ptco# = z.ptco#
        AND ptro# = z.ptro#
        AND ptlpym = 'C')
    AND EXISTS (
      SELECT 1
      FROM zalltheros
      WHERE ptco# = z.ptco#
        AND ptro# = z.ptro#
        AND ptlpym = 'S'))       
ORDER BY ptro#, ptline  

16080624 has S (CNA Wester) AND C lines 
  
SELECT *
FROM zalltheros
WHERE ptro# = '16080624'  
  AND ptcode = 'TT'
  
  
--- 5/11 ------------
sdpprice has pricing (labor rates) BY service type/pay meth/fran
looking at 16008027 RE/Warr/GM
sdpprice: RE/W/GM: acct 146424 @ 100.48/hour
BUT glptrns shows labor to acct 146401, which IS NOT IN sdpprice

want to DO a subset of accounts WHERE glpmast.gmtype = 4 (sales)

-- new car prep sales account NOT IN sdpprice   
SELECT gtdoc#, gttrn#, gtseq#, gtjrnl, gtdate, gtacct, gttamt
FROM stgArkonaGLPTRNS
WHERE gtdoc# = '16008027'

ah shit, it IS IN the warranty receivable account

this whole train started with records IN the following with blank glptrns for the following

SELECT *
INTO #wtf
FROM (
  SELECT ptro#, SUM(ptlhrs) AS ptlhrs, SUM(ptlamt) AS ptlamt
  FROM zalltheros
  WHERE ptcode = 'TT'
  GROUP BY ptro#) a
LEFT JOIN (
  SELECT gtdoc#, SUM(gttamt)
  FROM stgArkonaGLPTRNS
  WHERE gtacct IN (
  'BLANK','146000','146001','146002','146007','146020','146027','146100',    
  '146204','146302','146303','146304','146305','146327','146424','147100',    
  '147200','147300','230903','245700','245800','245900','246000','246001',    
  '246100','246101','246102','246200','246300','246302','246303','246305',    
  '246310','246320','346000','346001','346002','346200','346300','346400') 
  GROUP BY gtdoc#) b ON a.ptro# = b.gtdoc#
  
so, either go with an acct list FROM sdpprice that includes warranty rec accts 
OR limit the accounts to glpmast sales 

SELECT * FROM stgArkonaGLPMAST
-- any changes IN glpmast gmtype year to year?
-- nope
SELECT gmyear, gmacct
FROM (
  SELECT gmyear, gmacct, gmtype
  FROM stgArkonaGLPMAST
  GROUP BY gmyear, gmacct, gmtype) x
GROUP BY gmyear, gmacct
HAVING COUNT(*) > 1

SELECT *
FROM stgArkonaGLPTRNS t
INNER JOIN (
  SELECT gmacct, gmtype
  FROM stgArkonaGLPMAST
  GROUP BY gmacct, gmtype) m ON t.gtacct = m.gmacct
WHERE m.gmtype = '4'  

-- ros joined to trns on ro = doc WHERE acct type IS sales
SELECT *
INTO #wtf1
FROM (
  SELECT ptro#, SUM(ptlhrs) AS ptlhrs, SUM(ptlamt) AS ptlamt
  FROM zalltheros
  WHERE ptcode = 'TT'
  GROUP BY ptro#) a
LEFT JOIN (
  SELECT gtdoc#, SUM(gttamt)
  FROM stgArkonaGLPTRNS t
  INNER JOIN (
    SELECT gmacct, gmtype
    FROM stgArkonaGLPMAST
    GROUP BY gmacct, gmtype) m ON t.gtacct = m.gmacct
  WHERE m.gmtype = '4'  
  GROUP BY gtdoc#) b ON a.ptro# = b.gtdoc#
  
SELECT *
FROM #wtf1  
WHERE gtdoc# = '16008027' 

SELECT * 
FROM stgArkonaGLPTRNS
WHERE gtdoc# = '16008027' 

SELECT w.*, ABS(expr) - ptlamt
FROM #wtf1 w
WHERE ptlamt + expr <> 0
  AND LEFT(ptro#, 4) = '1608'
  
16087755 221.95 vs 920.07: includes parts sales but NOT shop supplies

SELECT gtco#, gtdtyp, gttype, gtjrnl, gtacct, gttamt
FROM stgArkonaGLPTRNS t
WHERE gtdoc# = '16087755'

SELECT gtco#, gtdtyp, gttype, gtjrnl, gtacct, gttamt, g.*
FROM stgArkonaGLPTRNS t
LEFT JOIN (
  SELECT gmacct, gmtype, gmdesc, gmdept
  FROM stgArkonaGLPMAST
  WHERE gmyear = 2012
    AND gmtype = '4') g ON t.gtacct = g.gmacct
WHERE gtdoc# = '16087755'

SELECT gtco#, gtacct, gtjrnl, COUNT(gtacct)
FROM stgArkonaGLPTRNS
WHERE gtjrnl IN ('sca','swa','svi')
GROUP BY gtco#, gtacct, gtjrnl

-- DROP TABLE #accts
SELECT *
INTO #accts
FROM (
  SELECT gmacct, gmtype, gmdept, gmdesc 
  FROM stgArkonaGLPMAST 
  WHERE gmyear = 2012
    AND LEFT(gmacct, 1) = '1') g1
LEFT JOIN (  
  SELECT gmacct, gmtype, gmdept, gmdesc 
  FROM stgArkonaGLPMAST 
  WHERE gmyear = 2012 
    AND LEFT(gmacct, 1) = '2') g2 ON right(trim(g1.gmacct), length(TRIM(g1.gmacct)) - 1) = right(trim(g2.gmacct), length(TRIM(g2.gmacct)) - 1)
LEFT JOIN (
  SELECT gmacct, gmtype, gmdept, gmdesc  
  FROM stgArkonaGLPMAST 
  WHERE gmyear = 2012 
    AND LEFT(gmacct, 1) = '3') g3 ON right(trim(g1.gmacct), length(TRIM(g1.gmacct)) - 1) = right(trim(g3.gmacct), length(TRIM(g3.gmacct)) - 1)
-- shit, think i actually got ALL the accounts
-- shit , no i didn't
-- instead of a hard 5, DO a length - 1
-- ry3 looks ok
-- ry2, missing lots and lots of accounts

SELECT *
FROM stgArkonaGLPMAST g
WHERE LEFT(gmacct, 1) = '2'
  AND gmyear = 2012
  AND NOT EXISTS (
    SELECT 1 
    FROM #accts
    WHERE gmacct_1 = g.gmacct) 

-- crookston IS not ok    
SELECT *
FROM stgArkonaGLPMAST g
WHERE LEFT(gmacct, 1) = '3'
  AND NOT EXISTS (
    SELECT 1 
    FROM #accts
    WHERE gmacct_2 = g.gmacct)  
  AND gmdesc LIKE '%sls%'     
    
SELECT * 
FROM #accts WHERE gmacct_2 LIKE '34%'
WHERE gmtype = '4'

UPDATE #accts
SET gmdept_2 = gmdept
-- SELECT * FROM #accts
WHERE gmdept_2 IS NOT null

-- ok, updated ry2/3 depts to match with ry1

-- thinking, just UPDATE sales accts to SD WHERE possible     
SELECT g.gmyear, g.gmacct, g.gmtype, g.gmdesc, a.*
FROM stgArkonaGLPMAST g
LEFT JOIN #accts a ON g.gmacct = a.gmacct_1
WHERE LEFT(g.gmacct, 1) = '2'
  AND g.gmyear = 2012
  AND g.gmtype = '4'
ORDER BY g.gmacct  

-- ok 
-- ros joined to acct# on ro = doc , look for categorization BY dept
SELECT *
-- INTO #wtf1
FROM (
  SELECT ptro#, SUM(ptlhrs) AS ptlhrs, SUM(ptlamt) AS ptlamt
  FROM zalltheros
  WHERE ptcode = 'TT'
  GROUP BY ptro#) a
LEFT JOIN (
  SELECT gtdoc#, SUM(gttamt)
  FROM stgArkonaGLPTRNS t
  INNER JOIN (
    SELECT gmacct, gmtype
    FROM stgArkonaGLPMAST
    GROUP BY gmacct, gmtype) m ON t.gtacct = m.gmacct
  WHERE m.gmtype = '4'  
  GROUP BY gtdoc#) b ON a.ptro# = b.gtdoc#
  
-- labor FROM ro line(zalltheros) AND sales FROM gl based ON doc
SELECT *
FROM #wtf1

SELECT *
-- SELECT COUNT(*) -- 221875
FROM #wtf1
WHERE ptlamt + expr <> 0

SELECT w.*, g.*
FROM #wtf1 w
LEFT JOIN (
  SELECT gtdoc#, gtacct, SUM(gttamt)
  FROM stgArkonaGLPTRNS t
  INNER JOIN (
    SELECT gmacct, gmtype
    FROM stgArkonaGLPMAST
    GROUP BY gmacct, gmtype) m ON t.gtacct = m.gmacct
  WHERE m.gmtype = '4'  
  GROUP BY gtdoc#, gtacct) b ON w.ptdoc# = b.gtdoc# 
ORDER BY w.ptro#  
  
-- 5/12 i need to populate depts IN coa
-- does glpmast change year to year?  
  
SELECT gmco#, gmyear, COUNT(*)
FROM stgArkonaGLPMAST
GROUP BY gmco#, gmyear
-- store IS NOT IN gmco#, store IS IN gmstyp
SELECT gmstyp, gmyear, COUNT(*)
FROM stgArkonaGLPMAST
GROUP BY gmstyp, gmyear
-- same # of accts
-- feeling ok about it
SELECT gmstyp, gmacct
FROM (
  SELECT gmstyp, gmacct, gmtype
  FROM stgArkonaGLPMAST
  GROUP BY gmstyp, gmacct, gmtype) X
GROUP BY gmstyp, gmacct
HAVING COUNT(*) > 1  

-- DO 1-2-3 correspond with A-B-C
-- yep
SELECT * 
FROM stgArkonaGLPMAST
WHERE (
  (LEFT(gmacct,1) = '1' AND gmstyp <> 'A') OR 
  (LEFT(gmacct,1) = '2' AND gmstyp <> 'B') OR 
  (LEFT(gmacct,1) = '3' AND gmstyp <> 'C'))

-- ALL ry1 accts, AND ry2,ry3 WHERE match except first char; 11101 = 21101 = 31101  
SELECT *
INTO #accts
FROM (
  SELECT gmacct, gmtype, gmdept, gmdesc 
  FROM stgArkonaGLPMAST 
  WHERE gmyear = 2012
  AND gmco# = 'RY1'
    AND LEFT(gmacct, 1) = '1') g1
LEFT JOIN (  
  SELECT gmacct, gmtype, gmdept, gmdesc 
  FROM stgArkonaGLPMAST 
  WHERE gmyear = 2012 
    AND LEFT(gmacct, 1) = '2') g2 ON right(trim(g1.gmacct), length(TRIM(g1.gmacct)) - 1) = right(trim(g2.gmacct), length(TRIM(g2.gmacct)) - 1)
LEFT JOIN (
  SELECT gmacct, gmtype, gmdept, gmdesc  
  FROM stgArkonaGLPMAST 
  WHERE gmyear = 2012 
    AND LEFT(gmacct, 1) = '3') g3 ON right(trim(g1.gmacct), length(TRIM(g1.gmacct)) - 1) = right(trim(g3.gmacct), length(TRIM(g3.gmacct)) - 1)
-- ok, just sales accounts
SELECT *
FROM #accts
WHERE gmtype = '4' 
  AND gmdept IN ('SD','PD') 

 -- update dept for ry2/3 with dept FROM ry1 WHERE accounts match
UPDATE #accts
SET gmdept_1 = gmdept
-- SELECT * FROM #accts
WHERE gmdept_1 IS NOT NULL

UPDATE #accts
SET gmdept_2 = gmdept
-- SELECT * FROM #accts
WHERE gmdept_2 IS NOT NULL

SELECT *
FROM stgArkonaGLPMAST
WHERE gmyear = 2012
  AND gmstyp = 'B'
  AND gmtype = '4'

SELECT DISTINCT ptro#
INTO #ry2ro
FROM zalltheros   
WHERE ptco# = 'ry2'

SELECT * 
FROM #ry2ro


SELECT *
FROM (
  SELECT gtdoc#, gtjrnl, gtacct, gttamt
  FROM stgArkonaGLPTRNS
  WHERE gtco# = 'RY1'
    AND gtpost = 'Y') a
INNER JOIN ( -- just the ry2 sales accounts   
  SELECT gmacct, gmdept
  FROM stgArkonaGLPMAST
  WHERE gmyear = 2012
    AND gmtype = '4'
    AND gmstyp = 'B') b ON a.gtacct = b.gmacct

-- sales accounts attached to ry2 ros   
SELECT *
INTO #ry2acct
FROM #ry2ro r
LEFT JOIN (
  SELECT *
  FROM (
    SELECT gtdoc#, gtjrnl, gtacct, gttamt
    FROM stgArkonaGLPTRNS
    WHERE gtco# = 'RY1'
      AND gtpost = 'Y') a
  INNER JOIN ( -- just the ry2 sales accounts   
    SELECT gmacct, gmdept
    FROM stgArkonaGLPMAST
    WHERE gmyear = 2012
      AND gmtype = '4'
      AND gmstyp = 'B') b ON a.gtacct = b.gmacct) c ON r.ptro# = c.gtdoc#   

SELECT gmacct, COUNT(*)
FROM #ry2acct
GROUP BY gmacct

SELECT * FROM #ry2acct
      
SELECT g.gmacct, g.gmtype, g.gmdesc, g.gmdept, a.gmdept_1
FROM stgArkonaGLPMAST g
LEFT JOIN #accts a ON g.gmacct = a.gmacct_1
WHERE g.gmyear = 2012
  AND g.gmacct IN (
  SELECT DISTINCT gmacct
  FROM #ry2acct)      
  
UPDATE edwAccountDIM 
  SET GLDepartmentCode = x.gmdept_1
--SELECT edw.account, edw.gldescription, edw.gldepartmentcode, edw.gldepartment, x.*
FROM edwAccountDim edw
INNER JOIN (
  SELECT g.gmacct, g.gmtype, g.gmdesc, g.gmdept, a.gmdept_1
  FROM stgArkonaGLPMAST g
  LEFT JOIN #accts a ON g.gmacct = a.gmacct_1
  WHERE g.gmyear = 2012
    AND g.gmacct IN (
    SELECT DISTINCT gmacct
    FROM #ry2acct)) x ON edw.Account = x.gmacct
WHERE x.gmdept_1 IS NOT NULL 
  AND edw.gldepartmentcode <> gmdept_1    
      
UPDATE edwAccountDIM
SET gldepartment = 'Service'
WHERE gldepartmentcode = 'SD'

UPDATE edwAccountDIM
SET gldepartment = 'Parts'
WHERE gldepartmentcode = 'PD'

UPDATE edwAccountDIM
SET gldepartment = 'Parts',
   gldepartmentcode = 'PD'
--SELECT * FROM edwAccountDIM  
WHERE storecode = 'ry2'
  AND accounttype = '4'
  AND gldescription LIKE '%P&A%'   
  
SELECT g.Account, g.AccountType, g.GLDescription, g.GLDepartmentCode
FROM edwAccountDIM g
WHERE g.Account IN (
    SELECT DISTINCT gmacct
    FROM #ry2acct) 
  AND gldepartmentcode = 'GN'    
       
  
UPDATE edwAccountDIM
SET gldepartment = 'Service',
   gldepartmentcode = 'SD'
--SELECT * FROM edwAccountDIM  
WHERE storecode = 'ry2'
  AND accounttype = '4'
  AND Account IN ('245800','245900','245901','246200','246300','246310','246600')

SELECT *
FROM (  
  SELECT distinct gtacct
  FROM #ry2acct) a
LEFT JOIN edwAccountDIM d ON a.gtacct = d.account

-- ry2 looks ok
-- ry3 
SELECT *
FROM stgArkonaGLPMAST
WHERE gmyear = 2012
  AND gmstyp = 'B'
  AND gmtype = '4'

SELECT DISTINCT ptro#
INTO #ry3ro
FROM zalltheros   
WHERE ptco# = 'ry3'

SELECT * 
FROM #ry3ro


SELECT *
FROM (
  SELECT gtdoc#, gtjrnl, gtacct, gttamt
  FROM stgArkonaGLPTRNS
  WHERE gtco# = 'RY1'
    AND gtpost = 'Y') a
INNER JOIN ( -- just the ry3 sales accounts   
  SELECT gmacct, gmdept
  FROM stgArkonaGLPMAST
  WHERE gmyear = 2012
    AND gmtype = '4'
    AND gmstyp = 'C') b ON a.gtacct = b.gmacct

-- sales accounts attached to ry2 ros   
SELECT *
INTO #ry3acct
FROM #ry3ro r
LEFT JOIN (
  SELECT *
  FROM (
    SELECT gtdoc#, gtjrnl, gtacct, gttamt
    FROM stgArkonaGLPTRNS
    WHERE gtco# = 'RY1'
      AND gtpost = 'Y') a
  INNER JOIN ( -- just the ry3 sales accounts   
    SELECT gmacct, gmdept
    FROM stgArkonaGLPMAST
    WHERE gmyear = 2012
      AND gmtype = '4'
      AND gmstyp = 'C') b ON a.gtacct = b.gmacct) c ON r.ptro# = c.gtdoc# 

SELECT DISTINCT gtacct, d.*
FROM #ry3acct a
INNER JOIN edwAccountDim d ON a.gtacct = d.account     

SELECT account, gldescription, gldepartmentcode, gldepartment
FROM edwAccountDim
WHERE storecode = 'ry3'
  AND accounttype = '4'
  AND (
    (gldescription LIKE '%srv%') OR
    (gldescription LIKE '%p&a%'))
    

UPDATE edwAccountDIM
SET gldepartment = 'Parts',
    gldepartmentcode = 'PD'
WHERE account IN ('346700','346700','346800','347700','348000','348100','348200','348300','349000','349100','349200')

UPDATE edwAccountDIM
SET gldepartment = 'Parts'
WHERE gldepartmentcode = 'PD'

-- ry3 ok

-- 5/12 labor sales ------------------------------------------------------------
-- ok 
-- ros joined to acct# on ro = doc , look for categorization BY dept
-- DROP TABLE #wtf1
SELECT *
INTO #wtf1
FROM (
  SELECT ptro#, SUM(ptlhrs) AS ptlhrs, SUM(ptlamt) AS ptlamt
  FROM zalltheros
  WHERE ptcode = 'TT'
  GROUP BY ptro#) a
LEFT JOIN (
  SELECT gtdoc#, SUM(gttamt)
  FROM stgArkonaGLPTRNS t
  INNER JOIN (
    SELECT account
    FROM edwAccountDim
    WHERE accounttype = '4'
      AND gldepartmentcode = 'SD') m ON t.gtacct = m.account
  GROUP BY gtdoc#) b ON a.ptro# = b.gtdoc#

  
-- labor FROM ro line(zalltheros) AND sales FROM gl based ON doc
SELECT *
FROM #wtf1
--WHERE ptlamt + expr <> 0
WHERE -2*ptlamt = expr

SELECT COUNT(*) FROM zalltheros WHERE ptcode = 'tt'

SELECT ptro#, ptltyp, ptcode, ptdate, ptlpym, pttech, ptlhrs, ptlamt, ptcrlo
--SELECT *
--SELECT SUM(ptlamt)
FROM stgArkonaSDPRDET
WHERE ptro# = '16080001'
  AND ptline = 7
  
-- labor hours IN zalltheros IS fucked up
-- 16080001 line 7 545 has 2 hours, zalltheros shows 1
-- fuck, this IS depressing
SELECT *
FROM zalltheros
WHERE ptro# = '16080001'
  AND ptltyp = 'L'
  AND ptcode = 'TT'   
  
SELECT sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptsvctyp, sd.ptlpym, 
  sd.ptdate, sd.ptlamt, sd.ptlopc, sd.ptcrlo, 'SDET' AS source, sh.ptswid
FROM stgArkonaSDPRHDR sh
LEFT JOIN stgArkonaSDPRDET sd ON sh.ptco# = sd.ptco#
  AND sh.ptro# = sd.ptro#
WHERE sh.ptro# = '16080001'
  AND ptline = 7
  AND length(trim(sh.ptro#)) > 6 -- exclude conversion data
  AND sh.ptco# IN ('RY1', 'RY2','RY3')
  AND EXISTS (
    SELECT 1
    FROM stgArkonaGLPTRNS
    WHERE gtdoc# = sh.ptro#
      AND gtpost = 'Y')        
GROUP BY sh.ptco#, sh.ptro#, sh.ptfran, sd.ptline, sd.ptltyp, sd.ptcode, 
  sd.pttech, sd.ptlhrs, sd.ptdate, sd.ptlamt, sd.ptlopc, sd.ptcrlo, sh.ptswid  

-- 5/12 labor sales ------------------------------------------------------------

SELECT *
--SELECT SUM(ptlamt)
FROM stgArkonaSDPRDET
WHERE ptro# = '16081008'
  AND ptline = 7