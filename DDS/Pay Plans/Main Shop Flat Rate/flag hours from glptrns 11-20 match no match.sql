-- 11/21/2013 
after talking with Ben
need to figure out what to DO about flag hours with no accounting
those ros will NOT show up IN #gl AND "some" of them are legitimate

DROP TABLE #gl;
SELECT a.gtdate, a.gtdoc#, gtacct, sum(gttamt) AS gttamt, gtjrnl, gtdesc
INTO #gl
FROM stgArkonaGLPTRNS a
WHERE gtdate BETWEEN '11/03/2013' AND '11/16/2013'
  AND left(gtdesc, 3) <> 'ADJ'
  AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
group by a.gtdate, a.gtdoc#, gtacct, gtjrnl, gtdesc;

-- GROUP ro BY serv/paytype 
-- paytype only
DROP TABLE #ro;
SELECT ro, paymenttypekey, sum(laborsales) AS sales
INTO #ro
FROM factRepairOrder b 
WHERE ro  IN ( SELECT gtdoc# FROM #gl)
  AND b.ServiceTypeKey IN (
    SELECT ServiceTypeKey 
    FROM dimServiceType 
    WHERE ServiceTypeCode IN ('AM','EM','MR'))      
GROUP BY ro, paymenttypekey;  

--< #glRO -----------------------------------------------------------------------
-- these match
DROP TABLE #match;
SELECT *
INTO #match
FROM (
  SELECT *
  FROM #gl a
  full OUTER JOIN  #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) x
WHERE gtdoc# IS NOT NULL AND ro IS NOT NULL;  
-- these don't
DROP TABLE #nomatch;
SELECT *
INTO #NoMatch
FROM (
  SELECT *
  FROM #gl a
  full OUTER JOIN  #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) x
WHERE gtdoc# IS NULL OR ro IS NULL;   

-- #gl that don't match #ro
SELECT *
FROM (
  SELECT gtdate, gtdoc#, gtacct, gttamt, gtjrnl, gtdesc
  FROM #nomatch) a
LEFT JOIN (
  SELECT ro, servicetypekey, paymenttypekey, sales
  FROM #nomatch) b on a.gtdoc# = b.ro   
WHERE gtdoc# IS NOT NULL AND ro IS NOT NULL   

-- #ro that don't match #gl  
SELECT b.*, a.*
FROM (
  SELECT ro, servicetypekey, paymenttypekey, sales
  FROM #nomatch) a
LEFT JOIN (
  SELECT gtdate, gtdoc#, gtacct, gttamt, gtjrnl, gtdesc
  FROM #nomatch) b on a.ro = b.gtdoc#   
WHERE ro IS NOT NULL AND gtdoc# IS NOT NULL     
  
 
-- this will yield multiple rows, but should resolve INTO single rows
-- BY joining to rotech 
DROP TABLE #glRO;
SELECT *
INTO #glro
FROM #match
UNION   
-- #gl that don't match #ro
SELECT *
FROM (
  SELECT gtdate, gtdoc#, gtacct, gttamt, gtjrnl, gtdesc
  FROM #nomatch) a
LEFT JOIN (
  SELECT ro, paymenttypekey, sales
  FROM #nomatch) b on a.gtdoc# = b.ro   
WHERE gtdoc# IS NOT NULL AND ro IS NOT NULL   
UNION 
-- #ro that don't match #gl  
SELECT b.*, a.*
FROM (
  SELECT ro, paymenttypekey, sales
  FROM #nomatch) a
LEFT JOIN (
  SELECT gtdate, gtdoc#, gtacct, gttamt, gtjrnl, gtdesc
  FROM #nomatch) b on a.ro = b.gtdoc#   
WHERE ro IS NOT NULL AND gtdoc# IS NOT NULL ;  
--/> #glRO -----------------------------------------------------------------------


DROP TABLE #roTech;
SELECT ro, description, technumber, servicetypekey, paymenttypekey, sum(flaghours*weightfactor) as flaghours, sum(laborsales) AS sales
INTO #roTech
FROM factRepairOrder b 
INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d on c.techkey = d.techkey  
WHERE ro  IN (SELECT gtdoc# FROM #gl)
  AND b.ServiceTypeKey IN (
    SELECT ServiceTypeKey 
    FROM dimServiceType 
    WHERE ServiceTypeCode IN ('AM','EM','MR'))         
GROUP BY ro, description, technumber, servicetypekey, paymenttypekey


DROP TABLE #data;
SELECT DISTINCT b.ro, b.description, technumber, b.paymenttypekey, b.flaghours
INTO #data
FROM #glRO a
LEFT JOIN #roTech b on a.ro = b.ro
  AND a.paymenttypekey = b.paymenttypekey;
  
  
--< 11/3 -> 11/16 -------------------------------------------------------------------  

--SELECT SUM(adjusted) FROM (
SELECT a.*, b.adj, a.flaghours + coalesce(adj, 0) AS adjusted
FROM ( 
  SELECT description, technumber, count(*), round(SUM(flaghours),2) AS flaghours
  FROM #data 
  GROUP BY description, technumber) a  
LEFT JOIN (  
  SELECT technumber, sum(neghours + adjhours - xtimhours) AS Adj
  FROM (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments 
    WHERE thedate between '11/03/2013' AND '11/16/2013'  
    GROUP BY Storecode, ro, technumber) x
  GROUP BY technumber) b on a.technumber = b.technumber
--WHERE description LIKE 'T%'  
--) x 

SELECT paymenttypekey, round(SUM(flaghours),2) FROM #data WHERE description = 'tech 634' GROUP BY paymenttypekey
SELECT * FROM #data WHERE description = 'tech 634' AND paymenttypekey = 2

pdq tech total 4.7 vs 4.33
  l/p does NOT include xtim .37 for ro 19152231
  SELECT * FROM #data WHERE description = 'tech 557' ORDER BY paymenttypekey
511 5.84 vs 5.54
  16136669 .3hr no $$ 6B brake inspection  
519
  16135718 .3 hr no $$ 23i
532 c 4.1 vs 7.3
  removed gtdesc = ADJ FROM gl
536 c 57.48 vs 56.88 
  16135710, 16136621 .3hrs AND no $$
557 c 31.61 vs 31.41
  16135402 2.0 vs 2.5 ro shows 2.5 hrs, l/pr shows 2 this one IS just fucking goofy
**  16136495 missing FROM query another one WHERE neg hours makes total flag hours 0 AND no labor sales entries IN glptrns
  think this IS a cashiering issue, don't know
  & .5 xtim that l/p does NOT include  
566 2 ros with .3hr AND no $$
573 l/p does NOT include 16136453 has -.8 adj
585 another goofy one with adjustments
  c 47.9 vs 46.9
  i 2.4 vs 3.4
**  16136626  this looks lika a query fuckup internal lines did NOT CLOSE until 11/18 (outside the interval)
      but since both lines were for the same amount (107.10), AND the cust pay line was closed 11/15
      query returns both rows
  SELECT * FROM #gl WHERE gtdoc# = '16136626'
  SELECT * FROM #ro WHERE ro = '16136626'
  SELECT * FROM #rotech WHERE ro = '16136626'
599
  c 41.1 vs 40.7
    16136214 .4hrs no $$ FREEROT
  i 14.8 vs 13.6
    19151671 adj 1.2  
    l/p shows 19151671 with 1.2hrs AND no $$
625
  c 34.45 vs 33.95 
    16136650 .5hrs AND no $$    
630
  16136226 .3 hrs no $$  
634 
  161354117, 16135618 each with 5.hrs AND no $$   
       
--/> 11/3 -> 11/16 ------------------------------------------------------------------- 
  
--< 10/20 -> 10/26 -------------------------------------------------------------------   
SELECT paymenttypekey, round(SUM(flaghours),2) FROM #data WHERE description = 'tech 634' GROUP BY paymenttypekey    
536 i 8 vs 8.5 negHours adjustment
SELECT * FROM #data WHERE description = 'tech 536' AND paymenttypekey = 3

557 c 24.18 vs 23.58
SELECT * FROM #data WHERE description = 'tech 557' AND paymenttypekey = 2
labprof include 16133989, 16134358
both have flag hours AND no $

577 s 14.8 vs 9.7
SELECT * FROM #data WHERE description = 'tech 577' AND paymenttypekey = 4
5.1 hours adj ro 16130131

599 c 29.6 vs 29.1
SELECT * FROM #data WHERE description = 'tech 599' AND paymenttypekey = 2
16134349 flag hours no $
NOT IN #gl
SELECT * FROM #gl WHERE gtdoc# = '16134349'

625 c 26.47 vs 26.77
SELECT * FROM #data WHERE description = 'tech 625' AND paymenttypekey = 2
query incl 16133771 .3 hrs
.3hrs no $, this one IS included IN the query because there are other paid lines?
SELECT * FROM #gl WHERE gtdoc# = '16133771'
SELECT * FROM #ro WHERE ro = '16133771'
SELECT * FROM #rotech WHERE ro = '16133771'

SELECT * FROM #rotech WHERE flaghours <> 0 AND sales = 0

626 c 7.9 vs 7.6
SELECT * FROM #data WHERE description = 'tech 626' AND paymenttypekey = 2
ro 16134453 .3 hr no $
626 i 7.9 vs 7.4
SELECT * FROM #data WHERE description = 'tech 626' AND paymenttypekey = 3
query missing 16134340
there are no labor sales entries IN accounting for this ro (checked against db2)

SELECT a.gtdate, a.gtdoc#, gtacct, gttamt, gtjrnl, gtdesc
FROM stgArkonaGLPTRNS a
WHERE gtdoc# = '16134340'

627 c 17.18 vs 16.78
SELECT * FROM #data WHERE description = 'tech 627' AND paymenttypekey = 2
16134142 .4 hrs no $

634 c 25.9 vs 26.9
SELECT * FROM #data WHERE description = 'tech 634' AND paymenttypekey = 2
16134118 1.2 vs 2.2  xtim 1 hr
--/> 10/20 -> 10/26 -------------------------------------------------------------------