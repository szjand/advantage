-- ok, ran it with westerhausen team, labor sales FROM query 150000 more than tech labor sales
-- have to look at labor sales IN glptrns

SELECT dl1,dl2,dl3,dl4,dl5,dl6
FROM zcb
WHERE name = 'cb2'
GROUP BY dl1,dl2,dl3,dl4,dl5,dl6

select *
FROM zcb a
WHERE name = 'cb2'
  AND dl2 = 'RY1'
  AND dl5 = 'Main Shop'
  AND al4 = 'Sales'

SELECT SUM(gttamt)
FROM zcb a
LEFT JOIN stgarkonaglptrns b on a.glaccount = b.gtacct
WHERE name = 'cb2'
  AND dl2 = 'RY1'
  AND dl5 = 'Main Shop'
  AND al4 = 'Sales'
  AND b.gtdate BETWEEN '10/01/2013' AND '10/31/2013'
GROUP BY al5, glaccount, gmcoa  
SELECT * FROM stgarkonaglptrns  

SELECT DISTINCT name FROM zcb

select DISTINCT dl1,dl2,dl3,dl4,dl5,dl6,dl7,dl8
FROM zcb a
WHERE name = 'gmfs gross'
  AND dl2 = 'RY1'
  
-- labor sales accounts - this looks good 
select glaccount, al3
FROM zcb a
WHERE name = 'gmfs gross'
  AND dl2 = 'RY1'
  AND dl4 = 'Mechanical'
  AND al2 = 'Sales'
  AND gmcoa NOT IN ('466','469')

select glaccount, al3 AS "FS Desc", b.gmdesc AS "GL Desc"
FROM zcb a
LEFT JOIN stgarkonaglpmast b on a.glaccount = b.gmacct
  AND b.gmyear = (SELECT theyear FROM day WHERE thedate = curdate())
WHERE name = 'gmfs gross'
  AND dl2 = 'RY1'
  AND dl4 = 'Mechanical'
  AND al2 = 'Sales'
  AND gmcoa NOT IN ('466','469')  


11-13-13
separate flag hours AND sales ?
 
flag hours ALL flag hours on closed ROs for servicetype MR, AM

sales: the accounting entries IN accounts categorized AS Sales IN the ServiceDept
       for the documents that are the ros with flag hours


-- this may prove to be a usedfull breakdown later
a hierarchy of ro detail for a time period
timeperiod flaghours laborsales
timeperiod teamflaghours (teamclockhours)      

-- flag hours for the previous 12 payperiods
SELECT round(SUM(flaghours), 2) AS flaghours,
  (SELECT fromdate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -12
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate)) AS FromDate,
  (SELECT thrudate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -1
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate)) AS ThruDate                
FROM factRepairOrder a
WHERE storecode = 'ry1'
  AND closedatekey IN (  
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN (
      SELECT fromdate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -12
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate))
      AND (
      SELECT thrudate
      FROM dimBWPayPeriod
      WHERE payperiod = (
        SELECT payperiod -1
        FROM dimBWPayPeriod
        WHERE curdate() BETWEEN fromdate AND thrudate)))      
  AND a.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr'));
    
-- labor sales for the previous 12 payperiods   
SELECT SUM(gttamt) -- 
FROM ( -- the ros
  SELECT ro
  FROM factRepairOrder a
  WHERE storecode = 'ry1'
    AND flaghours > 0
    AND closedatekey IN (  
      SELECT datekey
      FROM day
      WHERE thedate BETWEEN (
        SELECT fromdate
        FROM dimBWPayPeriod
        WHERE payperiod = (
          SELECT payperiod -12
          FROM dimBWPayPEriod
          WHERE curdate() BETWEEN fromdate AND thrudate))
        AND (
        SELECT thrudate
        FROM dimBWPayPeriod
        WHERE payperiod = (
          SELECT payperiod -1
          FROM dimBWPayPEriod
          WHERE curdate() BETWEEN fromdate AND thrudate)))      
    AND a.ServiceTypeKey IN (
      SELECT ServiceTypeKey
      FROM dimServiceType
      WHERE ServiceTypeCode IN ('am','mr'))
  GROUP BY ro) a  
LEFT JOIN stgArkonaGLPTRNS d on a.ro = d.gtdoc#   
WHERE d.gtacct IN (-- Service labor sales accounts
    select glaccount
    FROM zcb a
    WHERE name = 'cb2' -- narrows it down to main shop
      AND dl2 = 'RY1'
      and dl3 = 'fixed'
      and dl5 = 'main shop'
      AND al4 = 'Sales'
      AND gmcoa NOT IN ('466','469') -- elim shop supplies, sublet
      AND split = 1); -- eliminate parts 

-- Effective Labor Rate for the last 12 payperiods
SELECT laborsales/flaghours
FROM ( -- flag hours
  SELECT round(SUM(flaghours), 2) AS flaghours              
  FROM factRepairOrder a
  WHERE storecode = 'ry1'
    AND closedatekey IN (  
      SELECT datekey
      FROM day
      WHERE thedate BETWEEN (
        SELECT fromdate
        FROM dimBWPayPeriod
        WHERE payperiod = (
          SELECT payperiod -12
          FROM dimBWPayPEriod
          WHERE curdate() BETWEEN fromdate AND thrudate))
        AND (
        SELECT thrudate
        FROM dimBWPayPeriod
        WHERE payperiod = (
          SELECT payperiod -1
          FROM dimBWPayPEriod
          WHERE curdate() BETWEEN fromdate AND thrudate)))      
    AND a.ServiceTypeKey IN (
      SELECT ServiceTypeKey
      FROM dimServiceType
      WHERE ServiceTypeCode IN ('am','mr'))) a
INNER JOIN ( -- labor sales   
  SELECT SUM(gttamt) * -1 AS laborsales
  FROM ( -- the ros
    SELECT ro
    FROM factRepairOrder a
    WHERE storecode = 'ry1'
      AND flaghours > 0
      AND closedatekey IN (  
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN (
          SELECT fromdate
          FROM dimBWPayPeriod
          WHERE payperiod = (
            SELECT payperiod -12
            FROM dimBWPayPEriod
            WHERE curdate() BETWEEN fromdate AND thrudate))
          AND (
          SELECT thrudate
          FROM dimBWPayPeriod
          WHERE payperiod = (
            SELECT payperiod -1
            FROM dimBWPayPEriod
            WHERE curdate() BETWEEN fromdate AND thrudate)))      
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro) a  
  LEFT JOIN stgArkonaGLPTRNS d on a.ro = d.gtdoc#   
  WHERE d.gtacct IN (-- Service labor sales accounts
    select glaccount
    FROM zcb a
    WHERE name = 'cb2' -- narrows it down to main shop
      AND dl2 = 'RY1'
      and dl3 = 'fixed'
      and dl5 = 'main shop'
      AND al4 = 'Sales'
      AND gmcoa NOT IN ('466','469') -- elim shop supplies, sublet
      AND split = 1)) b on 1 = 1       
        
        
-- accounts used & descriptions
select glaccount, al5 AS "FS Desc", b.gmdesc AS "GL Desc"
FROM zcb a
INNER JOIN stgArkonaGLPMAST b on a.glaccount = b.gmacct
  AND b.gmyear = 2013
WHERE name = 'cb2' -- narrows it down to main shop
  AND dl2 = 'RY1'
  and dl3 = 'fixed'
  and dl5 = 'main shop'
  AND al4 = 'Sales'
  AND gmcoa NOT IN ('466','469') -- elim shop supplies, sublet
  AND split = 1 -- elim parts sales  
ORDER BY glaccount     
/*
ok here i go, for the month of october, how many/much flaghrs for servicetypes mr, am that are NOT mainshop techs
SELECT c.flagdept, min(ro), max(ro), COUNT(*) AS HowMany, round(SUM(flaghours), 0) AS flaghours
FROM factRepairOrder a
INNER JOIN  brTechGroup b on a.techGroupKey = b.TechGroupKey
INNER JOIN dimTech c on b.TechKey = c.TechKey
WHERE a.storecode = 'ry1'
  AND a.flaghours > 0
  AND a.closedatekey IN (
    SELECT datekey
    FROM day
    WHERE theyear = 2013)
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dimServicetype
    WHERE servicetypecode IN (      'mr','am'))
GROUP BY c.flagdept   
SELECT 100.0*(18+123+151)/(18+123+151+37888)*1.0 FROM system.iota
0.7% of the total flaghours for 2013
*/    
/*       
-- mind fuck 1, resolved: ELR does NOT include OPEN ros 
closed ros vs closed lines
are there ros with no CLOSE date that have a flag date?
absolutely: (11/13)
  16135551 lines 1/3
  opened 11/5
  flaghours for tech 536 1.0
SELECT *
FROM factRepairOrder a
WHERE storecode = 'ry1'
  AND closedatekey = (
    SELECT datekey
    FROM day
    WHERE datetype = 'NA') 
  AND servicetypekey IN (
    SELECT servicetypekey
    FROM dimServiceType
    WHERE servicetypecode IN ('MR','AM'))    
  AND EXISTS (
    SELECT 1
    FROM factRepairOrder
    WHERE ro = a.ro
      AND line = a.line
      AND flagdatekey <> 7306)    
      
so now, the question IS whar are the sales for that, duh, are there any entries IN accounting for 
that ro , NOPE (checked against db2)
*/  
