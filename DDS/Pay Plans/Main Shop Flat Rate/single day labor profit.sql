SELECT description, SUM(flaghours - xtim + adj + neg)
FROM (
  SELECT a.ro, description, a.technumber, flaghours, coalesce(xtimhours,0) AS xtim, 
    coalesce(adjhours, 0) AS adj, coalesce(neghours, 0) AS neg, CloseDate, FinalClose
  FROM (   
    SELECT ro, c.description, c.technumber, round(SUM(flaghours), 2) AS flaghours,
      d.thedate AS CloseDate, e.thedate AS FinalClose      
    FROM factRepairOrder a
    INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dimtech c on b.techkey = c.techkey
    INNER JOIN day d on a.closedatekey = d.datekey
    INNER JOIN day e on a.finalclosedatekey = e.datekey
    WHERE a.storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey = (
        select datekey 
        from day 
        where thedate = '09/11/2013')     
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro, c.description, c.technumber, d.thedate, e.thedate) a
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on a.ro = d.ro AND a.Technumber = d.technumber) x 
GROUP BY description    


SELECT x.*
FROM (
  SELECT a.ro, description, a.technumber, flaghours, coalesce(xtimhours,0) AS xtim, 
    coalesce(adjhours, 0) AS adj, coalesce(neghours, 0) AS neg, CloseDate, FinalClose
  FROM (   
    SELECT ro, c.description, c.technumber, round(SUM(flaghours), 2) AS flaghours,
      d.thedate AS CloseDate, e.thedate AS FinalClose      
    FROM factRepairOrder a
    INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dimtech c on b.techkey = c.techkey
    INNER JOIN day d on a.closedatekey = d.datekey
    INNER JOIN day e on a.finalclosedatekey = e.datekey
    WHERE a.storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey = (
        select datekey 
        from day 
        where thedate = '09/11/2013')     
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro, c.description, c.technumber, d.thedate, e.thedate) a
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on a.ro = d.ro AND a.Technumber = d.technumber) x     
WHERE description = 'Tech 575'   
ORDER BY ro

-- single day IS still the way to go, but need to integrate the notion of pay type to know the flag dates (gl posting dates)
-- start with a single tech, have to DO line
-- does the finalclosedate no longer matter at all?  appears that way

CREATE TABLE stgServicePaymentTypeAccounts(
  ServiceTypeCode cichar(2),
  PaymentTypeCode cichar(1),
  Account cichar(6)) IN database;
Insert into stgServicePaymentTypeAccounts values('AM', 'C', '146007');
Insert into stgServicePaymentTypeAccounts values('AM', 'I', '146007');
Insert into stgServicePaymentTypeAccounts values('AM', 'S', '146007');
Insert into stgServicePaymentTypeAccounts values('EM', 'C', '146000');
Insert into stgServicePaymentTypeAccounts values('EM', 'I', '146304');
Insert into stgServicePaymentTypeAccounts values('EM', 'S', '146001');
Insert into stgServicePaymentTypeAccounts values('EM', 'S', '146305');
Insert into stgServicePaymentTypeAccounts values('EM', 'W', '146204');
Insert into stgServicePaymentTypeAccounts values('MR', 'C', '146000');
Insert into stgServicePaymentTypeAccounts values('MR', 'I', '146304');
Insert into stgServicePaymentTypeAccounts values('MR', 'S', '146001');
Insert into stgServicePaymentTypeAccounts values('MR', 'S', '146305');
Insert into stgServicePaymentTypeAccounts values('MR', 'W', '146204');
Insert into stgServicePaymentTypeAccounts values('MR', 'W', '146301');

SELECT w.ro, w.description, SUM(flaghours), x.gtdate
FROM (
  SELECT ro, line, d.paymenttypecode, e.ServiceTypeCode, c.description, c.technumber, flaghours
  FROM factRepairOrder a
  INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
    AND c.technumber = '575' 
  INNER JOIN dimPaymentType d on a.paymenttypekey = d.paymenttypekey   
  INNER JOIN dimServiceType e on a.ServiceTypeKey = e.ServiceTypeKey   
  WHERE a.storecode = 'ry1'
    AND flaghours <> 0
    AND finalclosedatekey in (
      select datekey 
      from day 
      where yearmonth = 201309)     
    AND a.ServiceTypeKey IN (
      SELECT ServiceTypeKey
      FROM dimServiceType
      WHERE ServiceTypeCode IN ('am','mr', 'em'))) w
LEFT JOIN stgArkonaGLPTRNS x on w.ro = x.gtdoc#
INNER JOIN stgServicePaymentTypeAccounts y on w.ServiceTypeCode = y.ServiceTypeCode
  AND w.PaymentTypeCode = y.PaymentTypeCode
  AND x.gtacct = y.account 
GROUP BY w.ro, w.description, x.gtdate    
ORDER BY gtdate, ro
        
        
-- base TABLE will have to be accounting
-- any TRANSACTION IN a service journal IN the date interval on an account IN stgServicePaymentAccounts
     
-- 11/17
-- NOT a bad start, but IS returning 2 rows for each, one EM AND one MR     
SELECT a.gtdoc#, e.line, a.gtdate, b.servicetypecode, b.paymenttypecode, a.gtacct,
  e.flaghours, f.weightfactor, g.description
FROM stgArkonaGLPTRNS a
INNER JOIN stgServicePaymentTypeAccounts b on a.gtacct = b.account
INNER JOIN dimServiceType c on b.ServiceTypeCode = b.ServiceTypeCode
INNER JOIN dimPAymentType d on b.PaymentTypeCode = d.PaymentTypeCode
INNER JOIN factRepairOrder e on a.gtdoc# = e.ro
  AND c.ServiceTypeKey = e.ServiceTypeKey
  AND d.PaymentTypeKey = e.PaymentTypeKey
INNER JOIN brTechGroup f on e.techgroupkey = f.techgroupkey
INNER JOIN dimtech g on f.techkey = g.techkey  
  AND g.technumber = '522'
WHERE a.gtdate = '09/11/2013'
  AND a.gtacct IN (
    SELECT account
    FROM stgServicePaymentTypeAccounts)      
  AND e.ServiceTypeKey IN (
    SELECT ServiceTypeKey
    FROM dimServiceType
    WHERE ServiceTypeCode IN ('am','mr', 'em'))    
ORDER BY description, gtdoc#    

-- ? DO a "premature" grouping on glptrns

-- what DO i need FROM ro
-- one line per ro/line/tech that include servicetype, paymenttype, flaghours
SELECT ro, line, technumber FROM (
SELECT ro, line, servicetypekey, paymenttypekey, sum(flaghours*weightfactor) AS flaghours, 
  technumber,  description 
FROM factrepairorder a
INNER JOIN brTechGroup b on a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c on b.techkey = c.techkey
--WHERE ro = '16126541'
--  AND flaghours <> 0
GROUP BY ro, line, servicetypekey, paymenttypekey, technumber,  description
) x GROUP BY ro, line, technumber HAVING COUNT(*) > 1

SELECT ro, line, servicetypekey, paymenttypekey, 
  sum(flaghours*weightfactor) AS flaghours, b.techkey 
FROM factrepairorder a
INNER JOIN brTechGroup b on a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c on b.techkey = c.techkey
GROUP BY ro, line, servicetypekey, paymenttypekey, b.techkey

-- ro level IS NOT good enuf, need line level, lines can be closed IN accounting
-- WHEN the ro IS NOT
accounting does NOT know shit about techs OR lines
only account AND date

the problem IS the linking BETWEEN ro AND glptrns ?

hmm, forget line, GROUP BY tech AND serv/pay types
DROP TABLE #ro;
SELECT * -- 1 row per ro/servtype/paytype/tech with flag hours
INTO #ro
FROM (
  SELECT ro, servicetypecode, paymenttypecode, technumber, description, 
    SUM(flaghours * weightfactor) AS flaghours
  FROM factRepairOrder a
  INNER JOIN brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
  INNER JOIN dimservicetype d on a.servicetypekey = d.servicetypekey
  INNER JOIN dimpaymenttype e on a.paymenttypekey = e.paymenttypekey
  WHERE a.storecode = 'RY1'
    AND opendatekey IN (
      SELECT datekey
      FROM day
      WHERE yearmonth between 201306 AND 201310)
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dimServiceType
    WHERE servicetypecode IN ('mr','am','em'))    
  GROUP BY ro, servicetypecode, paymenttypecode, technumber, description) p 
WHERE flaghours <> 0   
  
--- hmm, a given account could be for multiple serv/pay  
so i DO NOT know for a glptrns TRANSACTION what the def serv/pay type IS based on the account

journal may help
SCA	Service Sales - Retail	// includes service contract payment types
SVI	Service Sales - Internal	
SWA	Service Sales - Warranty	

SELECT gtjrnl, gtacct, COUNT(*), MIN(gtdoc#), MAX(gtdoc#)
FROM stgArkonaGLPTRNS
WHERE year(gtdate) = 2013
  AND gtjrnl IN ('SWA','SVI','SCA')
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)  
GROUP BY gtjrnl, gtacct

SELECT * from #ro
SELECT DISTINCT servicetypecode, paymenttypecode from #ro

IF an ro IS MR/W the gl account could BY 146204 OR 146301

SELECT gtdate, gtdoc#, gtacct, gtjrnl
FROM stgArkonaGLPTRNS 
WHERE gtdate = '09/11/2013'
  AND gtacct IN (
    SELECT account
    FROM stgServicePaymentTypeAccounts)
  AND gtjrnl IN ('SWA','SVI','SCA')    
GROUP BY gtdate, gtdoc#, gtacct, gtjrnl    
 

SELECT gtdate, gtdoc#, gtacct, gtjrnl
FROM stgArkonaGLPTRNS 
WHERE year(gtdate) = 2013
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND gtjrnl IN ('SWA','SVI','SCA')  
 
-- the same ro (fucking pdis) can have MR/W entries IN 146204 & 146301 
SELECT a.*
FROM (
  SELECT gtdate, gtdoc#, gtacct, gtjrnl, gttamt
  FROM stgArkonaGLPTRNS 
  WHERE year(gtdate) = 2013
    AND gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND gtjrnl IN ('SWA','SVI','SCA')) a  
INNER JOIN stgArkonaGLPTRNS b on a.gtdoc# = b.gtdoc#
INNER JOIN stgArkonaGLPTRNS c on a.gtdoc# = c.gtdoc#
WHERE b.gtacct = '146204'    
  AND c.gtacct = '146301'
  AND b.gtjrnl IN ('SWA','SVI','SCA')
  AND c.gtjrnl IN ('SWA','SVI','SCA')


-- no way to definitely tell serv/pay type FROM journal & account  
SELECT gtjrnl, gtacct, COUNT(*), MIN(gtdoc#), MAX(gtdoc#)
FROM stgArkonaGLPTRNS
WHERE year(gtdate) = 2013
  AND gtjrnl IN ('SWA','SVI','SCA')
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)  
GROUP BY gtjrnl, gtacct

SELECT servicetypecode, paymenttypecode, COUNT(*)
FROM #ro
GROUP BY servicetypecode, paymenttypecode

DROP TABLE #gl;
SELECT gtdate, gtdoc#, gtacct, gtjrnl, gttamt
INTO #gl
FROM stgArkonaGLPTRNS 
WHERE year(gtdate) = 2013
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND gtjrnl IN ('SWA','SVI','SCA')
  

-- #ro, ry1 ros openend 08 - 10 2013 with service type IN ('mr','am','em')
-- 1 row per ro/servtype/paytype/tech with flag hours  
SELECT * 
FROM #ro a
LEFT JOIN #gl b on a.ro = b.gtdoc#
WHERE a.ro = '16131270'
ORDER BY a.ro, b.gtacct

16131270 has c/s/i labor lines
SELECT a.*
FROM #ro a
WHERE ro = '16131270'
ORDER BY servicetypecode, paymenttypecode

SELECT a.*, b.*
FROM #ro a
INNER JOIN stgServicePaymentTypeAccounts b on a.servicetypecode = b.servicetypecode
  AND a.paymenttypecode = b.paymenttypecode
WHERE ro = '16131270'

select *
FROM #gl
WHERE gtdoc# = '16135300'

select *
FROM #ro
WHERE ro = '16135300'


SELECT ro, COUNT(*) FROM #ro GROUP BY ro ORDER BY COUNT(*) DESC 

SELECT gtjrnl, gtacct, COUNT(*), MIN(gtdoc#), MAX(gtdoc#)
FROM #gl
GROUP BY gtjrnl, gtacct

SELECT gtjrnl, gtacct, COUNT(*), MIN(gtdoc#), MAX(gtdoc#)
FROM stgArkonaGLPTRNS
WHERE year(gtdate) = 2013
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND gtjrnl IN ('SWA','SVI','SCA')
GROUP BY gtjrnl, gtacct

SELECT * FROM stgarkonaglptrns WHERE gtdoc# = '16135300'

SELECT * FROM stgarkonaglptrns WHERE gtacct = '146001' AND gtjrnl = 'SVI' AND gtdate BETWEEN '08/01/2013' AND '10/30/2013'
/*  
SELECT gtdoc#, gtacct gtjrnl FROM ( 
SELECT gtdate, gtdoc#, gtacct, gtjrnl
FROM #gl
GROUP BY gtdate, gtdoc#, gtacct, gtjrnl 
) x GROUP BY gtdoc#, gtacct, gtjrnl HAVING count(*) > 1

SELECT * FROM #gl WHERE gtdoc# = '16129947'
SELECT * FROM #ro WHERE ro = '16134540'

SELECT * FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# where gtdoc# = '16129531' ORDER BY gtacct
SELECT * FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# where gtdoc# = '16129681' ORDER BY gtacct



SELECT * FROM stgFlagTimeAdjustments WHERE ro = '16123109'

SELECT gtdate, gtdoc#, gtacct, gtjrnl
FROM #gl
WHERE gtdoc# = '16107293'
GROUP BY gtdate, gtdoc#, gtacct, gtjrnl 

SELECT *
FROM #gl
WHERE gtdoc# = '16106406'
GROUP BY gtdate, gtdoc#, gtacct, gtjrnl 

SELECT *
FROM stgArkonaGLPTRNS
WHERE gtdoc# = '16106406'
*/

-- 11/18 holy shit ALL i see are mind fucks
-- need a strategy
1. generate a list of ros on a date based on accounting
SELECT *
FROM #gl
WHERE gtdate = '09/11/2013'
ORDER BY gtdoc#

SELECT *
FROM #ro
WHERE ro IN (
  SELECT gtdoc#
  FROM #gl
  WHERE gtdate = '09/11/2013')
  
-- that got nowhere
TRY eliminating the anomalous jrnl/acct combinations, eg, SCA/146204

-- these are the "valid" combinations  
Acct      jrnl     servtype     paytype
146000    SCA        EM           C
146000    SCA        MR           C
146001    SCA        EM           S
146001    SCA        MR           S
146007    SCA        AM           S
146007    SVI        AM           I
146007    SCA        AM           S
146204    SWA        EM           W
146204    SWA        MR           W  
146301    SWA        MR           W
146304    SVI        EM           I
146304    SVI        MR           I
146305    SCA        EM           S
146305    SCA        MR           S   
-- 11/18 DO it without journals
-- 11/19 ADD the serv/pay type keys
ALTER TABLE stgValidCombinations
ADD COLUMN ServiceTypeKey integer
ADD COLUMN PaymentTypeKey integer;
UPDATE stgValidCombinations
SET ServiceTypeKey = (
  SELECT ServiceTypeKey
  FROM dimServiceType
  WHERE ServiceTypeCode = stgValidCombinations.ServiceTypeCode);
  
UPDATE stgValidCombinations
SET PaymentTypeKey = (
  SELECT PaymentTypeKey
  FROM dimPaymentType
  WHERE PaymentTypeCode = stgValidCombinations.PaymentTypeCode);  

DROP TABLE stgValidCombinations;
CREATE TABLE stgValidCombinations (
  Account cichar(10), 
--  Journal cichar(3),
  ServiceTypeCode cichar(2),
  PaymentTypeCode cichar(1)) IN database;
insert into stgValidCombinations values('146000','EM','C');
insert into stgValidCombinations values('146000','MR','C');
insert into stgValidCombinations values('146001','AM','S');
insert into stgValidCombinations values('146001','EM','S');
insert into stgValidCombinations values('146001','MR','S');
insert into stgValidCombinations values('146007','AM','I');
insert into stgValidCombinations values('146007','AM','S');
insert into stgValidCombinations values('146007','EM','S');
insert into stgValidCombinations values('146007','MR','S');
insert into stgValidCombinations values('146204','EM','W');
insert into stgValidCombinations values('146204','MR','W');
insert into stgValidCombinations values('146301','MR','W');
insert into stgValidCombinations values('146304','EM','I');
insert into stgValidCombinations values('146304','MR','I');
insert into stgValidCombinations values('146305','AM','S');
insert into stgValidCombinations values('146305','EM','S');
insert into stgValidCombinations values('146305','MR','S');

  
-- modify to include each journal for each service contract account
-- service contract accts: 146001, 146007, 146305
/* orig assuming SCA for service contract labor 
insert into stgValidCombinations values('146000','SCA','EM','C');
insert into stgValidCombinations values('146000','SCA','MR','C');
insert into stgValidCombinations values('146001','SCA','EM','S');
insert into stgValidCombinations values('146001','SCA','MR','S');
insert into stgValidCombinations values('146007','SCA','AM','S');
insert into stgValidCombinations values('146007','SVI','AM','I');
insert into stgValidCombinations values('146007','SCA','AM','S');
insert into stgValidCombinations values('146204','SWA','EM','W');
insert into stgValidCombinations values('146204','SWA','MR','W');
insert into stgValidCombinations values('146301','SWA','MR','W');
insert into stgValidCombinations values('146304','SVI','EM','I');
insert into stgValidCombinations values('146304','SVI','MR','I');
insert into stgValidCombinations values('146305','SCA','EM','S');
insert into stgValidCombinations values('146305','SCA','MR','S');
*/
/*
insert into stgValidCombinations values('146000','SCA','EM','C');
insert into stgValidCombinations values('146000','SCA','MR','C');
insert into stgValidCombinations values('146007','SVI','AM','I');
insert into stgValidCombinations values('146204','SWA','EM','W');
insert into stgValidCombinations values('146204','SWA','MR','W');
insert into stgValidCombinations values('146301','SWA','MR','W');
insert into stgValidCombinations values('146304','SVI','EM','I');
insert into stgValidCombinations values('146304','SVI','MR','I');

insert into stgValidCombinations values('146001','SCA','AM','S');
insert into stgValidCombinations values('146001','SCA','EM','S');
insert into stgValidCombinations values('146001','SCA','MR','S');
insert into stgValidCombinations values('146001','SVI','AM','S');
insert into stgValidCombinations values('146001','SVI','EM','S');
insert into stgValidCombinations values('146001','SVI','MR','S');
insert into stgValidCombinations values('146001','SWA','AM','S');
insert into stgValidCombinations values('146001','SWA','EM','S');
insert into stgValidCombinations values('146001','SWA','MR','S');

insert into stgValidCombinations values('146007','SCA','AM','S');
insert into stgValidCombinations values('146007','SCA','EM','S');
insert into stgValidCombinations values('146007','SCA','MR','S');
insert into stgValidCombinations values('146007','SVI','AM','S');
insert into stgValidCombinations values('146007','SVI','EM','S');
insert into stgValidCombinations values('146007','SVI','MR','S');
insert into stgValidCombinations values('146007','SWA','AM','S');
insert into stgValidCombinations values('146007','SWA','EM','S');
insert into stgValidCombinations values('146007','SWA','MR','S');

insert into stgValidCombinations values('146305','SCA','AM','S');
insert into stgValidCombinations values('146305','SCA','EM','S');
insert into stgValidCombinations values('146305','SCA','MR','S');
insert into stgValidCombinations values('146305','SVI','AM','S');
insert into stgValidCombinations values('146305','SVI','EM','S');
insert into stgValidCombinations values('146305','SVI','MR','S');
insert into stgValidCombinations values('146305','SWA','AM','S');
insert into stgValidCombinations values('146305','SWA','EM','S');
insert into stgValidCombinations values('146305','SWA','MR','S')
*/
SELECT * FROM stgValidCombinations

-- include only the transactions for a valid combination
DROP TABLE #gl;
SELECT gtdate, gtdoc#, gtacct, gtjrnl, gttamt
INTO #gl
FROM stgArkonaGLPTRNS a
WHERE year(gtdate) = 2013
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND gtjrnl IN ('SWA','SVI','SCA')
AND EXISTS (
  SELECT 1
  FROM stgValidCombinations
  WHERE Journal = a.gtjrnl
    AND Account = a.gtacct)  

SELECT description, flaghours, b.* 
FROM #roMR a 
INNER JOIN #gl b on a.ro = b.gtdoc#
WHERE b.gtdate = '09/11/2013'
ORDER BY description, ro

SELECT * FROM #gl WHERE gtdoc# = '16128598'
                
SELECT * -- 1 row per ro/servtype/paytype/tech with flag hours
INTO #roMR
FROM (
  SELECT ro, servicetypecode, paymenttypecode, technumber, description, 
    SUM(flaghours * weightfactor) AS flaghours
  FROM factRepairOrder a
  INNER JOIN brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
  INNER JOIN dimservicetype d on a.servicetypekey = d.servicetypekey
  INNER JOIN dimpaymenttype e on a.paymenttypekey = e.paymenttypekey
  WHERE a.storecode = 'RY1'
    AND opendatekey IN (
      SELECT datekey
      FROM day
      WHERE yearmonth between 201308 AND 201310)
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dimServiceType
    WHERE servicetypecode = 'MR')    
  GROUP BY ro, servicetypecode, paymenttypecode, technumber, description) p 
WHERE flaghours <> 0 

SELECT * -- 1 row per ro/servtype/paytype/tech with flag hours
INTO #roEM
FROM (
  SELECT ro, servicetypecode, paymenttypecode, technumber, description, 
    SUM(flaghours * weightfactor) AS flaghours
  FROM factRepairOrder a
  INNER JOIN brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
  INNER JOIN dimservicetype d on a.servicetypekey = d.servicetypekey
  INNER JOIN dimpaymenttype e on a.paymenttypekey = e.paymenttypekey
  WHERE a.storecode = 'RY1'
    AND opendatekey IN (
      SELECT datekey
      FROM day
      WHERE yearmonth between 201308 AND 201310)
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dimServiceType
    WHERE servicetypecode = 'EM')    
  GROUP BY ro, servicetypecode, paymenttypecode, technumber, description) p 
WHERE flaghours <> 0 

SELECT * -- 1 row per ro/servtype/paytype/tech with flag hours
INTO #roAM
FROM (
  SELECT ro, servicetypecode, paymenttypecode, technumber, description, 
    SUM(flaghours * weightfactor) AS flaghours
  FROM factRepairOrder a
  INNER JOIN brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
  INNER JOIN dimservicetype d on a.servicetypekey = d.servicetypekey
  INNER JOIN dimpaymenttype e on a.paymenttypekey = e.paymenttypekey
  WHERE a.storecode = 'RY1'
    AND opendatekey IN (
      SELECT datekey
      FROM day
      WHERE yearmonth between 201308 AND 201310)
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dimServiceType
    WHERE servicetypecode = 'AM')    
  GROUP BY ro, servicetypecode, paymenttypecode, technumber, description) p 
WHERE flaghours <> 0 


DROP TABLE #gl;
SELECT gtdate, gtdoc#, gtacct, gtjrnl, gttamt
-- INTO #gl
FROM stgArkonaGLPTRNS a
WHERE year(gtdate) = 2013
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND gtjrnl IN ('SWA','SVI','SCA')
AND EXISTS (
  SELECT 1
  FROM stgValidCombinations
  WHERE Journal = a.gtjrnl
    AND Account = a.gtacct)  

    
SELECT description, COUNT(*), SUM(flaghours), SUM(coalesce(xtimhours ,0)),
  SUM(coalesce(adjhours,0)), SUM(coalesce(neghours,0))
FROM (
  SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*, d.*
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtjrnl = b.journal
    AND a.gtacct = b.account
  INNER JOIN #ro c on a.gtdoc# = c.ro
    AND b.ServiceTypeCode = c.ServiceTypeCode
    AND b.PaymentTypeCode = c.PaymentTypeCode  
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on c.ro = d.ro AND c.technumber = d.technumber   
  WHERE year(a.gtdate) = 2013
    AND a.gttamt <= 0
    AND gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND gtjrnl IN ('SWA','SVI','SCA')) x
WHERE gtdate = '09/11/2013'
GROUP BY description
ORDER BY description    
-- ind tech
SELECT *
FROM (
  SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*, d.*
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtjrnl = b.journal
    AND a.gtacct = b.account
  INNER JOIN #ro c on a.gtdoc# = c.ro
    AND b.ServiceTypeCode = c.ServiceTypeCode
    AND b.PaymentTypeCode = c.PaymentTypeCode  
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on c.ro = d.ro AND c.technumber = d.technumber   
  WHERE year(a.gtdate) = 2013
    AND a.gttamt <= 0
    AND gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND gtjrnl IN ('SWA','SVI','SCA')) x
WHERE gtdate = '09/11/2013'
AND technumber = '634' ORDER BY paymenttypecode, ro

tech 519 26.5 vs 27.5
-- DO NOT know how i am going to WORK this one out
labprof includes 16129927, a neg hour adj, query does NOT include the ro OR the neghours
SELECT * FROM stgFlagTimeAdjustments WHERE ro = '16129927'
shows up IN the adjustments TABLE, but, of course, the ro IS NOT listed because
the factRepairOrders does not include ros with negative hours

tech 545 23.5 vs 23.3
ro 16129160 ALL MR/S lines include, one line, 6, MR/C for 0.2 NOT included
on the ro, the line shows $0, AND the summary shows no Customer Pay $$
AND there are no accounting entries for 146000 (MR/C)

tech 557 9.44 vs 8.77
cp
ro 16130001 not included in query 0.3 hours, no $$
warr
ro 16129681
SELECT * FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# where gtdoc# = '16129681' ORDER BY gtacct
warranty line (6 LOFD MR/W) .37 hrs acct 146204 posted to SVI, which IS an invalid acct/jrnl combo, s/b SWA

tech 634 7.87 vs 7.3
cp      
ro 16122015 missing IN query
SELECT * FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# where gtdoc# = '16122015' ORDER BY gtacct
-- NOT IN #ro because it was opened IN June
SELECT * FROM #ro WHERE ro = '16122015'
 
-- so the day went well, let's go for a week IN october, 20 - 26
-- lab/prof
SELECT description, COUNT(*) AS ROs, SUM(flaghours) AS FlagHours, SUM(coalesce(xtimhours ,0)),
  SUM(coalesce(adjhours,0)), SUM(coalesce(neghours,0))
FROM (
  SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*, d.*
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtjrnl = b.journal
    AND a.gtacct = b.account
  INNER JOIN #ro c on a.gtdoc# = c.ro
    AND b.ServiceTypeCode = c.ServiceTypeCode
    AND b.PaymentTypeCode = c.PaymentTypeCode  
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on c.ro = d.ro AND c.technumber = d.technumber   
  WHERE year(a.gtdate) = 2013
    AND a.gttamt <= 0
    AND gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND gtjrnl IN ('SWA','SVI','SCA')) x
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
GROUP BY description
ORDER BY description    
-- group by ind tech paytype
SELECT technumber, paymenttypecode, SUM(flaghours)
FROM (
  SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*, d.*
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtjrnl = b.journal
    AND a.gtacct = b.account
  INNER JOIN #ro c on a.gtdoc# = c.ro
    AND b.ServiceTypeCode = c.ServiceTypeCode
    AND b.PaymentTypeCode = c.PaymentTypeCode  
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on c.ro = d.ro AND c.technumber = d.technumber   
  WHERE year(a.gtdate) = 2013
    AND a.gttamt <= 0
    AND gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND gtjrnl IN ('SWA','SVI','SCA')) x
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
AND technumber = '511'
GROUP BY technumber, paymenttypecode

-- filter by ind tech AND paytype
SELECT *
FROM (
  SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*, d.*
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtjrnl = b.journal
    AND a.gtacct = b.account
  INNER JOIN #ro c on a.gtdoc# = c.ro
    AND b.ServiceTypeCode = c.ServiceTypeCode
    AND b.PaymentTypeCode = c.PaymentTypeCode  
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on c.ro = d.ro AND c.technumber = d.technumber   
  WHERE year(a.gtdate) = 2013
    AND a.gttamt <= 0
    AND gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND gtjrnl IN ('SWA','SVI','SCA')) x
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
AND technumber = '511' 
AND paymenttypecode = 'w'
ORDER BY paymenttypecode, ro

511 3.1 vs .9
missing ro 16133287 (1.3) & 1613368 (.9)
SELECT * FROM #ro WHERE ro IN ('16133287','1613368')
SELECT * FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# where gtdoc# = '16133287' ORDER BY gtacct
warranty lines posted to SVI instead of SWA


-- 11/18 redo it ALL, start with the week, USING the new stgValidCombinations that does NOT include the journal
SELECT description, COUNT(*) AS ROs, SUM(flaghours) AS FlagHours, SUM(coalesce(xtimhours ,0)),
  SUM(coalesce(adjhours,0)), SUM(coalesce(neghours,0))
FROM (
  SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*, d.*
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtacct = b.account
  INNER JOIN #ro c on a.gtdoc# = c.ro
    AND b.ServiceTypeCode = c.ServiceTypeCode
    AND b.PaymentTypeCode = c.PaymentTypeCode  
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on c.ro = d.ro AND c.technumber = d.technumber   
  WHERE year(a.gtdate) = 2013
    AND a.gttamt <= 0
    AND gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND gtjrnl IN ('SWA','SVI','SCA')) x
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
GROUP BY description
ORDER BY description   
-- group by ind tech paytype
SELECT technumber, paymenttypecode, SUM(flaghours)
FROM (
  SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*, d.*
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtacct = b.account
  INNER JOIN #ro c on a.gtdoc# = c.ro
    AND b.ServiceTypeCode = c.ServiceTypeCode
    AND b.PaymentTypeCode = c.PaymentTypeCode  
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on c.ro = d.ro AND c.technumber = d.technumber   
  WHERE year(a.gtdate) = 2013
    AND a.gttamt <= 0
    AND gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND gtjrnl IN ('SWA','SVI','SCA')) x
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
AND technumber = '511'
GROUP BY technumber, paymenttypecode
-- w 3.1 vs 4.4

-- filter by ind tech AND paytype
-- this IS the fucking journal problem
-- this returns 2 rows for 16133287, one for each warranty account which it should
-- but totals the flag hours for each line, shoule be .9/.4 NOT 1.3/1.3
-- i don't need to be returning accounting data, gl IS simply being used to determine
-- which parts of which ros have flag hours
SELECT *
FROM (
  SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*, d.*
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtacct = b.account
  INNER JOIN #ro c on a.gtdoc# = c.ro
    AND b.ServiceTypeCode = c.ServiceTypeCode
    AND b.PaymentTypeCode = c.PaymentTypeCode  
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on c.ro = d.ro AND c.technumber = d.technumber   
  WHERE year(a.gtdate) = 2013
    AND a.gttamt <= 0
    AND gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND gtjrnl IN ('SWA','SVI','SCA')) x
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
AND technumber = '511' 
AND paymenttypecode = 'w'
ORDER BY paymenttypecode, ro

SELECT * FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# where gtdoc# = '16133287' ORDER BY gtacct
SELECT * FROM #ro WHERE ro = '16133287'

SELECT * FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# where gtdoc# = '16133287' ORDER BY gtacct

-- 11/18 redo it ALL, start with the week, USING the new stgValidCombinations that does NOT include the journal
refactor this to use accounting to determine which parts of which ros

-- this might be premature grouping, USING #ro
SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account
INNER JOIN #ro c on a.gtdoc# = c.ro
  AND b.ServiceTypeCode = c.ServiceTypeCode
  AND b.PaymentTypeCode = c.PaymentTypeCode  
WHERE year(a.gtdate) = 2013
  AND a.gttamt <= 0
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND gtjrnl IN ('SWA','SVI','SCA')
  AND ro = '16133287'
  
-- 11/18 redo it ALL, start with the week, USING the new stgValidCombinations that does NOT include the journal

SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*, c.*
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account
INNER JOIN dimServiceType c on b.servicetypecode = b.servicetypecode
INNER JOIN dimpaymenttype d on b.paymenttypecode = d.paymenttypecode
INNER JOIN factRepairOrder e on a.gtdoc# = e.ro
  AND c.servicetypekey = e.servicetypekey
  AND d.paymenttypekey = e.paymenttypekey
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND gtjrnl IN ('SWA','SVI','SCA')
  AND gtdoc# = '16133287'
  
INNER JOIN #ro c on a.gtdoc# = c.ro
  AND b.ServiceTypeCode = c.ServiceTypeCode
  AND b.PaymentTypeCode = c.PaymentTypeCode  

WHERE year(a.gtdate) = 2013
  AND a.gttamt <= 0
  AND gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND gtjrnl IN ('SWA','SVI','SCA')
AND gtdate BETWEEN '10/20/2013' AND '10/26/2013'
AND ro = '16133287'



DROP TABLE #ro;
SELECT * -- 1 row per ro/servtype/paytype/tech with flag hours
INTO #ro
FROM (
  SELECT ro, servicetypecode, paymenttypecode, technumber, description, 
    SUM(flaghours * weightfactor) AS flaghours
  FROM factRepairOrder a
  INNER JOIN brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
  INNER JOIN dimservicetype d on a.servicetypekey = d.servicetypekey
  INNER JOIN dimpaymenttype e on a.paymenttypekey = e.paymenttypekey
  WHERE a.storecode = 'RY1'
    AND opendatekey IN (
      SELECT datekey
      FROM day
      WHERE yearmonth between 201306 AND 201310)
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dimServiceType
    WHERE servicetypecode IN ('mr','am','em'))    
  GROUP BY ro, servicetypecode, paymenttypecode, technumber, description) p 
WHERE flaghours <> 0   