seem to be 3 types: (none of which are accounted for IN factRepairOrder
  SDPXTIM
  Negative hours entered BY dispatch
  Policy Adjustment 
  
-- xtim  
SELECT *
FROM (
  SELECT ptro#, pttech, round(SUM(ptlhrs), 2) AS xHours
  FROM stgarkonaSDPXTIM
  GROUP BY ptro#, pttech) a
WHERE xHours <> 0

-- negative hours (dispatch) ------------------------------------  
SELECT *
FROM stgArkonaSDPRDET
WHERE year(ptdate) = 2013
  AND ptlhrs < 0
--? pdp  yep, happens there too
SELECT *
FROM stgArkonaPDPPDET
WHERE ptlhrs < 0

-- policy adjustment ------------------------------------
select * 
from stgArkonasdprdet 
WHERE ptpadj <> ''
  AND ptlhrs <> 0


-- this one shows 1 hour, ptcode IS, no tech  
-- hour does NOT show on ro, OR IN labor profit
select * 
from stgArkonasdprdet 
WHERE ptro# = '16133638'  

SELECT * FROM stg

-- think this IS the one that go me going on the adjustment
SELECT *
FROM factrepairorder
WHERE ro = '16130131'
-- the 5.1 hours that now i can NOT find
-- shit it shows up IN db2, but NOT IN stgArkonaSDPRDET

FROM etl-sdprdet sdprhdr.sql 07/2013
-- ok, here's the conclusion, when ptcode = CP then ptdbas = V signifies a coupon
-- the rest of the time, it is reversed tech time
-- so, in the scrape, will remove the ptdbas filter, then delete the TT rows later
select ptline, ptcode, count(*)
from rydedata.sdprdet
where ptdbas = 'V'
  and ptdate > 20130000
group by  ptline, ptcode

AND sp.stgArkSDPRDET:
  DELETE
  FROM tmpSDPRDET
  WHERE ptdbas = 'V'
    AND ptcode = 'TT';
    
IN db2, ro 16130131, line 3
  ptltyp: L
  ptlcode: TT
  ptpadj: X
  pttech: 577 
  ptdbas: V

-- ro 16130131  
just talked to bev, WHEN flag hours are added IN cash/delay, the hours go IN 
the data (& accounting), but DO NOT show on the ro reprint  




    
SELECT *
FROM tmpSDPRDET
WHERE ptro# = '16130131' ORDER BY ptro#, ptline, ptseq#

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16130131' ORDER BY ptro#, ptline, ptseq#


    SELECT a.ptco# AS storecode, a.ptro# AS ro, a.ptline AS line,
      min(CASE WHEN ptltyp = 'A' THEN a.ptdate ELSE cast('12/31/9999' AS sql_date) END) AS LineDate,
      -- *a*
      coalesce(MAX(CASE WHEN a.ptcode = 'TT' AND a.ptltyp = 'L' AND ptlhrs <> 0 THEN a.ptdate END), cast('12/31/9999' AS sql_date)) AS FlagDate,     
      coalesce(SUM(CASE WHEN a.ptcode = 'TT' AND a.ptltyp = 'L' THEN ptlamt END), 0) AS LaborSales,
      coalesce(SUM(CASE WHEN a.ptcode = 'SL' AND a.ptltyp = 'N' THEN ptlamt END), 0) AS Sublet, 
      coalesce(SUM(CASE WHEN a.ptcode = 'PM' AND a.ptltyp = 'M' THEN ptlamt END), 0) AS PaintMaterials, 
      MAX(CASE WHEN a.ptltyp = 'A' THEN a.ptsvctyp END) AS ServType,
      MAX(CASE WHEN a.ptltyp = 'A' THEN a.ptlpym END) AS PayType,
      MAX(CASE WHEN a.ptltyp = 'A' THEN a.ptlopc END) AS OpCode,
      coalesce( -- the negative hour fix   
        SUM(
          CASE 
            WHEN ptltyp = 'L' AND ptcode = 'TT' AND ptlhrs < 0 THEN 0
            WHEN ptltyp = 'L' AND ptcode = 'TT' AND ptlhrs >= 0 THEN ptlhrs
          END), 0) AS LineHours,
      'Closed' AS status
    FROM tmpSDPRDET a  
--    INNER JOIN tmpRO b ON a.ptro# = b.ro 
    WHERE ptro# = '16130131'
    GROUP BY ptco#, ptro#, ptline;
    
    
SELECT *
INTO #NegHrs
FROM stgArkonaSDPRDET
WHERE year(ptdate) = 2013
  AND ptlhrs < 0
  
SELECT a.ptro#, a.ptline, a.ptdate, a.pttech, a.ptltyp, a.ptcode, a.ptlhrs, b.*
FROM #NegHrs a
LEFT JOIN (
  SELECT technumber, ro, line, flaghours
  FROM factrepairorder a
  INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dimtech c on b.techkey = c.techkey
  WHERE opendatekey IN (
    SELECT datekey
    FROM day
    WHERE theyear = 2013)) b on a.ptro# = b.ro AND a.ptline = b.line

DROP TABLE stgFlagTimeAdjustments;   
CREATE TABLE stgFlagTimeAdjustments (
  Source cichar(8) CONSTRAINT NOT NULL,
  StoreCode cichar(3) CONSTRAINT NOT NULL,
  RO cichar(9) CONSTRAINT NOT NULL,
  Line integer  CONSTRAINT NOT NULL,
  Technumber cichar(3) CONSTRAINT NOT NULL,
  Hours double CONSTRAINT NOT NULL) IN database;   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgFlagTimeAdjustments','stgFlagTimeAdjustments.adi','RO','RO',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgFlagTimeAdjustments','stgFlagTimeAdjustments.adi','Technumber','Technumber',
   '',2,512,'' );      
   
SELECT * FROM stgFlagTimeAdjustments   

SELECT Storecode, ro, line, technumber,
  coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
  coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
  coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
FROM stgFlagTimeAdjustments  
GROUP BY Storecode, ro, line, technumber

ok, now JOIN this to october ros
-- pretty fucking good for 566
-- looks LIKE xtim will need to be subtracted
-- DO ALL techs, return only ros with an adjustment
problems  this one IS still unfigured out
only one for october
ro 16134106 tech 577, adj hours shows 9.8 but lab/prof does NOT
need to look at accounting on this one
-----------------------------------------------------------------------------------------------------

SELECT * 
FROM stgFlagTimeAdjustments 
WHERE technumber IN ('536','557','599','625','626','627','631','634')
ORDER BY technumber, ro

SELECT * 
FROM stgFlagTimeAdjustments 
WHERE thedate BETWEEN '11/03/2013' AND '11/16/2013'
  AND technumber = '625'
  
SELECT * FROM stgArkonaSDPXTIM  WHERE ptdate BETWEEN '11/03/2013' AND '11/16/2013' AND pttech = '557' ORDER BY ptro#


-- 11/20 ----------------------------------------------------------------------
need to ADD date to stgFlagTimeAdjustments

-- oh shit, NEG & ADJ adjustments MUST COME FROM DB2

DROP TABLE stgFlagTimeAdjustments;   
CREATE TABLE stgFlagTimeAdjustments (
  Source cichar(8) CONSTRAINT NOT NULL,
  theDate date CONSTRAINT NOT NULL,
  StoreCode cichar(3) CONSTRAINT NOT NULL,
  RO cichar(9) CONSTRAINT NOT NULL,
  Line integer  CONSTRAINT NOT NULL,
  Technumber cichar(3) CONSTRAINT NOT NULL,
  Hours double CONSTRAINT NOT NULL) IN database;   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgFlagTimeAdjustments','stgFlagTimeAdjustments.adi','RO','RO',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgFlagTimeAdjustments','stgFlagTimeAdjustments.adi','Technumber','Technumber',
   '',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgFlagTimeAdjustments','stgFlagTimeAdjustments.adi','theDate','theDate',
   '',2,512,'' );         

INSERT INTO stgFlagTimeAdjustments  
SELECT 'XTIM', ptdate, ptco#, ptro#, ptline, pttech, xHours
FROM (
  SELECT ptdate, ptco#, ptro#, ptline, pttech, round(SUM(ptlhrs), 2) AS xHours
  FROM stgarkonaSDPXTIM
  GROUP BY ptdate, ptco#, ptro#, ptline, pttech) a
WHERE xHours <> 0;


SELECT * FROM stgFlagTimeAdjustments WHERE thedate BETWEEN '11/03/2013' AND '11/16/2013'