1. get thedoc # account info

SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt
FROM stgArkonaGLPTRNS a
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND a.gtjrnl IN ('SWA','SVI','SCA')

-- the tendency here IS to start grouping (date, doc, account)
-- but let's wait a bit
-- let's TRY to understand better what we have

SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account 
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND a.gtjrnl IN ('SWA','SVI','SCA')

/* this suggests being able to GROUP on date, dot, acct, servtype, paytype  
SELECT gtdate, gtdoc#, gtacct, servicetypecode, paymenttypecode FROM (
SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account 
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND a.gtjrnl IN ('SWA','SVI','SCA')
) x GROUP BY gtdate, gtdoc#, gtacct, servicetypecode, paymenttypecode HAVING COUNT(*) > 1 
 
SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account 
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND a.gtjrnl IN ('SWA','SVI','SCA')
AND gtdoc# = '16133579'   
*/
/*
-- for a years data, 18 more rows without the jrnl filter, AND they are ALL garbage
--SELECT COUNT(*) FROM (
SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account 
WHERE year(gtdate) = 2013 --gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgValidCombinations)
  AND a.gtjrnl NOT IN ('SWA','SVI','SCA')
    
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
  AND a.gtjrnl IN ('SWA','SVI','SCA')
 */
-- BY tech 
SELECT description, count(*), round(sum(flaghours*weightfactor), 2)
FROM (
  SELECT a.*, b.line, b.flaghours, c.weightfactor, d.description
  FROM ( -- 1 row for each combination of gl & serv/paytype
    SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
    FROM stgArkonaGLPTRNS a
    INNER JOIN stgValidCombinations b on a.gtacct = b.account 
    WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
      AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
      AND a.gtacct IN (
        SELECT distinct account
        FROM stgServicePaymentTypeAccounts)
      AND a.gtjrnl IN ('SWA','SVI','SCA')) a
  INNER JOIN factRepairOrder b on a.gtdoc# = b.ro   
    AND a.ServiceTypeKey = b.ServiceTypeKey
    AND a.PaymentTypeKey = b.PaymentTypeKey
  INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
  INNER JOIN dimtech d on c.techkey = d.techkey  
  WHERE b.ro IN ( -- this really speeds this shit up
    SELECT a.gtdoc#
    FROM stgArkonaGLPTRNS a
    WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
      AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
      AND a.gtacct IN (
        SELECT distinct account
        FROM stgServicePaymentTypeAccounts)
      AND a.gtjrnl IN ('SWA','SVI','SCA'))) c   
  group BY description
  
-- BY tech/paytype 
SELECT description, paymenttypecode, count(*), round(sum(flaghours*weightfactor), 2)
FROM (
  SELECT a.*, b.line, b.flaghours, c.weightfactor, d.description
  FROM ( -- 1 row for each combination of gl & serv/paytype
    SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
    FROM stgArkonaGLPTRNS a
    INNER JOIN stgValidCombinations b on a.gtacct = b.account 
    WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
      AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
      AND a.gtacct IN (
        SELECT distinct account
        FROM stgServicePaymentTypeAccounts)
      AND a.gtjrnl IN ('SWA','SVI','SCA')) a
  INNER JOIN factRepairOrder b on a.gtdoc# = b.ro   
    AND a.ServiceTypeKey = b.ServiceTypeKey
    AND a.PaymentTypeKey = b.PaymentTypeKey
  INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
  INNER JOIN dimtech d on c.techkey = d.techkey  
  WHERE b.ro IN ( -- this really speeds this shit up
    SELECT a.gtdoc#
    FROM stgArkonaGLPTRNS a
    WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
      AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
      AND a.gtacct IN (
        SELECT distinct account
        FROM stgServicePaymentTypeAccounts)
      AND a.gtjrnl IN ('SWA','SVI','SCA'))) c   
GROUP BY description, paymenttypecode  

-- individual tech detail
SELECT description, paymenttypecode, gtdoc#, flaghours, weightfactor
FROM (
  SELECT a.*, b.line, b.flaghours, c.weightfactor, d.description
  FROM ( -- 1 row for each combination of gl & serv/paytype
    SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
    FROM stgArkonaGLPTRNS a
    INNER JOIN stgValidCombinations b on a.gtacct = b.account 
    WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
      AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
      AND a.gtacct IN (
        SELECT distinct account
        FROM stgServicePaymentTypeAccounts)
      AND a.gtjrnl IN ('SWA','SVI','SCA')) a
  INNER JOIN factRepairOrder b on a.gtdoc# = b.ro   
    AND a.ServiceTypeKey = b.ServiceTypeKey
    AND a.PaymentTypeKey = b.PaymentTypeKey
  INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
  INNER JOIN dimtech d on c.techkey = d.techkey  
  WHERE b.ro IN ( -- this really speeds this shit up
    SELECT a.gtdoc#
    FROM stgArkonaGLPTRNS a
    WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
      AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
      AND a.gtacct IN (
        SELECT distinct account
        FROM stgServicePaymentTypeAccounts)
      AND a.gtjrnl IN ('SWA','SVI','SCA'))) c   
WHERE description = 'Tech 536'
  AND paymenttypecode = 'c'
ORDER BY gtdoc#   

-- tech 536 customer pay
query includes 16133579 twice
SELECT a.*, b.line, b.flaghours, c.weightfactor, d.description, b.laborsales
FROM ( -- 1 row for each combination of gl & serv/paytype
  SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtacct = b.account 
  WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
    AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
    AND a.gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts)
    AND a.gtjrnl IN ('SWA','SVI','SCA')) a
INNER JOIN factRepairOrder b on a.gtdoc# = b.ro   
  AND a.ServiceTypeKey = b.ServiceTypeKey
  AND a.PaymentTypeKey = b.PaymentTypeKey
-- fuck me i am thinking of including sales $$ IN JOIN   
--  AND abs(a.gttamt) = abs(b.laborsales)
INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d on c.techkey = d.techkey  
WHERE description = 'Tech 536'
  AND paymenttypecode = 'c'
ORDER BY gtdoc#

line 3 MR/C 1 hour (69.95) tech 599
line 2 EM/C 3 hours (165.00) tech 566
ALL of which goes to account 146000 jrnl sca
-- fuck me i am thinking of including sales $$ IN JOIN 
SELECT gtdate, gtrdate, gtacct, gtjrnl, gtctl#, gtdoc#, gtdesc, gttamt, gquser 
FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# 
where gtdoc# ='16133579' and gtacct = '146000' ORDER BY gtacct
  
SELECT ro, line, description, flaghours, weightfactor, laborsales, (SELECT paymenttypecode FROM dimpaymenttype WHERE paymenttypekey = b.paymenttypekey)
FROM factRepairOrder b 
-- fuck me i am thinking of including sales $$ IN JOIN   
--  AND abs(a.gttamt) = abs(b.laborsales)
INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d on c.techkey = d.techkey  
WHERE ro = '16133579'
  AND paymenttypekey = (SELECT paymenttypekey FROM dimpaymenttype WHERE paymenttypecode = 'c')
  
--< the warranty accounts issue ------------------------------------------------------------------------ 
-- individual tech detail
ok right back to WHERE i was last night, tech 511, ro 16133287 being returned twice
WHERE DO i DO the grouping
line 1 IS being returned twice, once for each account, 146204, 146301
AS IS line 5
line 1 should be 146301 for 92.69
line 5 should be 146204 for 41.20

SELECT * FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# where gtdoc# = '16133287' ORDER BY gtacct
SELECT gtdate, gtrdate, gtacct, gtjrnl, gtctl#, gtdoc#, gtdesc, gttamt, gquser 
FROM stgarkonaglptrns a left join stgArkonaGLPDTIM b on a.gttrn# = gqtrn# 
where gtacct in ('146301', '146204') AND gtdoc# IN ('16135968','16136441')
ORDER BY gtacct

  SELECT a.*, b.line, b.flaghours, c.weightfactor, d.description
  FROM ( -- 1 row for each combination of gl & serv/paytype
    SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
    FROM stgArkonaGLPTRNS a
    INNER JOIN stgValidCombinations b on a.gtacct = b.account 
    WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
      AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
      AND a.gtacct IN (
        SELECT distinct account
        FROM stgServicePaymentTypeAccounts)
      AND a.gtjrnl IN ('SWA','SVI','SCA')) a
  INNER JOIN factRepairOrder b on a.gtdoc# = b.ro   
    AND a.ServiceTypeKey = b.ServiceTypeKey
    AND a.PaymentTypeKey = b.PaymentTypeKey
  INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
  INNER JOIN dimtech d on c.techkey = d.techkey  
  WHERE b.ro IN ( -- this really speeds this shit up
    SELECT a.gtdoc#
    FROM stgArkonaGLPTRNS a
    WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
      AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
      AND a.gtacct IN (
        SELECT distinct account
        FROM stgServicePaymentTypeAccounts)
      AND a.gtjrnl IN ('SWA','SVI','SCA'))
AND gtdoc# = '16133287'      

SELECT * FROM stgValidCombinations
--/> the warranty accounts issue ------------------------------------------------------------------------ 

--< use sale amounts to JOIN ------------------------------------------------------------------------

DROP TABLE #gl;
SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
INTO #gl
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account 
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
    
-- GROUP IN #gl
DROP TABLE #gl;
SELECT a.gtdate, a.gtdoc#, servicetypecode, servicetypekey, paymenttypecode, paymenttypekey, sum(a.gttamt) AS gttamt
INTO #gl
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account 
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
GROUP BY a.gtdate, a.gtdoc#, servicetypecode, servicetypekey, paymenttypecode, paymenttypekey;  

-- GROUP IN #gl only BY paymenttype
DROP TABLE #gl;
SELECT a.gtdate, a.gtdoc#, paymenttypecode, paymenttypekey, sum(a.gttamt) AS gttamt
INTO #gl
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account 
WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
  AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
GROUP BY a.gtdate, a.gtdoc#, paymenttypecode, paymenttypekey;  

DROP TABLE #ro;
SELECT ro, servicetypekey, paymenttypekey, sum(laborsales) AS sales
INTO #ro
FROM factRepairOrder b 
INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d on c.techkey = d.techkey  
WHERE ro  IN (  
  SELECT a.gtdoc#
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtacct = b.account 
  WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
    AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
    AND a.gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts))
GROUP BY ro, servicetypekey, paymenttypekey

-- GROUP ro BY paytype only
-- oops, ro has to limit servicetypes
DROP TABLE #ro;
SELECT ro, paymenttypekey, sum(laborsales) AS sales
INTO #ro
FROM factRepairOrder b 
INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d on c.techkey = d.techkey  
WHERE ro  IN (  SELECT gtdoc# FROM #gl)
--  SELECT a.gtdoc#
--  FROM stgArkonaGLPTRNS a
--  INNER JOIN stgValidCombinations b on a.gtacct = b.account 
--  WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
--    AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
--    AND a.gtacct IN (
--      SELECT distinct account
--      FROM stgServicePaymentTypeAccounts))
  AND b.ServiceTypeKey IN (
    SELECT ServiceTypeKey 
    FROM dimServiceType 
    WHERE ServiceTypeCode IN ('AM','EM','MR'))      
GROUP BY ro, paymenttypekey

DROP TABLE #roTech;
SELECT ro, description, servicetypekey, paymenttypekey, sum(flaghours*weightfactor) as flaghours, sum(laborsales) AS sales
INTO #roTech
FROM factRepairOrder b 
INNER JOIN brtechgroup c on b.techgroupkey = c.techgroupkey
INNER JOIN dimtech d on c.techkey = d.techkey  
WHERE ro  IN (  
  SELECT a.gtdoc#
  FROM stgArkonaGLPTRNS a
  INNER JOIN stgValidCombinations b on a.gtacct = b.account 
  WHERE gtdate BETWEEN '10/20/2013' AND '10/26/2013'
    AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
    AND a.gtacct IN (
      SELECT distinct account
      FROM stgServicePaymentTypeAccounts))
  AND b.ServiceTypeKey IN (
    SELECT ServiceTypeKey 
    FROM dimServiceType 
    WHERE ServiceTypeCode IN ('AM','EM','MR'))         
GROUP BY ro, description, servicetypekey, paymenttypekey
-- have to reconsider #ro tech now that #ro has been grouped

-- ALL tech totals
SELECT description, round(SUM(flaghours), 2) AS flaghours FROM ( 
--SELECT round(SUM(flaghours),2) FROM (
SELECT distinct d.ro, d.description, e.paymenttypecode, d.flaghours
FROM (
  SELECT DISTINCT b.*
  FROM #gl a
  inner JOIN #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) c
LEFT JOIN #roTech d on c.ro = d.ro AND c.paymenttypekey = d.paymenttypekey
LEFT JOIN dimPaymentType e on d.paymentTypeKey = e.paymenttypekey
WHERE d.flaghours <> 0
-- ORDER BY d.description, e.paymenttypecode, d.ro
) x GROUP BY description
ORDER BY description

-- 1 tech total on paytype
SELECT description, paymenttypecode, round(SUM(flaghours), 2) AS flaghours 
FROM ( 
  SELECT distinct d.ro, d.description, e.paymenttypecode, d.flaghours
  FROM (
    SELECT DISTINCT b.*
    FROM #gl a
    inner JOIN #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) c
  LEFT JOIN #roTech d on c.ro = d.ro AND c.paymenttypekey = d.paymenttypekey
  LEFT JOIN dimPaymentType e on d.paymentTypeKey = e.paymenttypekey
  WHERE d.flaghours <> 0) x 
WHERE description = 'Tech 537'  
GROUP BY description, paymenttypecode
ORDER BY description, paymenttypecode

-- 1 tech ALL ros for a paytype
SELECT ro, description, paymenttypecode, round(SUM(flaghours), 2) AS flaghours 
FROM ( 
  SELECT distinct d.ro, d.description, e.paymenttypecode, d.flaghours
  FROM (
    SELECT DISTINCT b.*
    FROM #gl a
    inner JOIN #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) c
  LEFT JOIN #roTech d on c.ro = d.ro AND c.paymenttypekey = d.paymenttypekey
  LEFT JOIN dimPaymentType e on d.paymentTypeKey = e.paymenttypekey
  WHERE d.flaghours <> 0) x 
WHERE description = 'Tech 537'
  AND paymenttypecode = 'i'  
GROUP BY ro, description, paymenttypecode
ORDER BY ro

536 i 8 vs 8.5 : negative hour adjustment (16134340), but this ro IS NOT IN the GROUP of ros FROM #gl
  ALL that means IS that the ro did NOT CLOSE IN the interval
  need a date IN adjustments TABLE, methinks
  
537 i 4.7 vs 4.4
query missing 16133882
SELECT * FROM #gl WHERE gtdoc# = '16133882'
select * FROM #ro WHERE ro = '16133882'
SELECT * FROM #rotech WHERE ro = '16133882'

shit seems LIKE i need to GROUP gl on paytype ?? only ?? that did NOT work
/* pre grouping of gl AND ro
SELECT *
FROM #ro a
LEFT JOIN #rotech b  on a.ro = b.ro AND a.servicetypekey = b.servicetypekey AND a.paymenttypekey = b.paymenttypekey

-- entire period
SELECT sum(flaghours)
FROM #gl a
LEFT JOIN #ro b on a.gtdoc# = b.ro AND a.servicetypekey = b.servicetypekey AND a.paymenttypekey = b.paymenttypekey AND abs(a.gttamt) = abs(b.sales)
LEFT JOIN #rotech c  on b.ro = c.ro AND b.servicetypekey = c.servicetypekey AND b.paymenttypekey = c.paymenttypekey
WHERE b.ro IS NOT NULL 


-- entire period BY tech
SELECT description, sum(flaghours)
FROM #gl a
LEFT JOIN #ro b on a.gtdoc# = b.ro AND a.servicetypekey = b.servicetypekey AND a.paymenttypekey = b.paymenttypekey AND abs(a.gttamt) = abs(b.sales)
LEFT JOIN #rotech c  on b.ro = c.ro AND b.servicetypekey = c.servicetypekey AND b.paymenttypekey = c.paymenttypekey
WHERE b.ro IS NOT NULL 
--  AND description = 'Tech 511'
group by description
ORDER BY description

-- entire period BY individual tech/paytype
SELECT description, paymenttypecode, sum(flaghours)
FROM #gl a
LEFT JOIN #ro b on a.gtdoc# = b.ro AND a.servicetypekey = b.servicetypekey AND a.paymenttypekey = b.paymenttypekey AND abs(a.gttamt) = abs(b.sales)
LEFT JOIN #rotech c  on b.ro = c.ro AND b.servicetypekey = c.servicetypekey AND b.paymenttypekey = c.paymenttypekey
  AND description = 'Tech 536'
group by description, paymenttypecode
ORDER BY description

-- entire period BY individual ro/tech/paytype
SELECT gtdoc#, description, paymenttypecode, flaghours
-- SELECT SUM(flaghours)
FROM #gl a
LEFT JOIN #ro b on a.gtdoc# = b.ro AND a.servicetypekey = b.servicetypekey AND a.paymenttypekey = b.paymenttypekey AND abs(a.gttamt) = abs(b.sales)
LEFT JOIN #rotech c  on b.ro = c.ro AND b.servicetypekey = c.servicetypekey AND b.paymenttypekey = c.paymenttypekey
WHERE description = 'Tech 536'
  AND paymenttypecode = 'c'
ORDER BY gtdoc#


SELECT a.*, c.*
FROM #gl a
LEFT JOIN #ro b on a.gtdoc# = b.ro AND a.servicetypekey = b.servicetypekey AND a.paymenttypekey = b.paymenttypekey AND abs(a.gttamt) = abs(b.sales)
LEFT JOIN #rotech c  on b.ro = c.ro AND b.servicetypekey = c.servicetypekey AND b.paymenttypekey = c.paymenttypekey
WHERE b.ro IS NOT NULL 
  AND description = 'Tech 511'
  AND paymenttypecode = 'w'
*/
/* this one was fixed BY grouping #ro  
536 c 28.27 vs 25.27 
query missing 16133579
line 2 EM/C 536 3@165.00
line 3 MR/C 599 1@ 69.95

SELECT * FROM #gl WHERE gtdoc# = '16133579'

SELECT * FROM #ro WHERE ro = '16133579'
so the problem IS IN linking amounts DO NOT match
feeling LIKE #ro has to be grouped AS well 
#gl amount 234.95
#ro amount 165.00
            69.95
            
line 2 EM/C 536 3@165.00
line 3 MR/C 599 1@ 69.95            

SELECT a.gtdate, a.gtdoc#, a.gtacct, a.gtjrnl, a.gttamt, b.*
FROM stgArkonaGLPTRNS a
INNER JOIN stgValidCombinations b on a.gtacct = b.account 
WHERE gtdoc# = '16133579'
  AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
  AND a.gtacct IN (
    SELECT distinct account
    FROM stgServicePaymentTypeAccounts)
#gl IS grouped BY serv/paytype
so #ro will have to be AS well, oh shit, ro IS already grouped BY serv/pay
the problem IS for #gl there IS no difference BETWEEN EM/C AND MR/C they are both the same account
so, GROUP ro BY paytype?
THEN #rotech will have to tease out the tech lines    
*/
/*            
-- this one was fixed BY grouping #gl  
511 w  3.1 vs 1.8
missing ro 16133287
SELECT * 
FROM #gl a
LEFT JOIN #ro b on a.gtdoc# = b.ro  AND a.servicetypekey = b.servicetypekey AND a.paymenttypekey = b.paymenttypekey AND abs(a.gttamt) = abs(b.sales)
--LEFT JOIN #rotech c on b.ro = c.ro
WHERE gtdoc# = '16133287'

can NOT JOIN on sales until #gl grouped BY serv/paytype

SELECT gtdate, gtdoc#, servicetypecode, paymenttypecode, SUM(gttamt) AS gttamt 
FROM #gl 
WHERE gtdoc# = '16133287'
GROUP BY gtdate, gtdoc#, servicetypecode, paymenttypecode



SELECT a.*, c.*
FROM (
  SELECT gtdoc#, servicetypekey, paymenttypekey, SUM(gttamt) AS gttamt 
  FROM #gl 
  WHERE gtdoc# = '16133287'
  GROUP BY gtdoc#, servicetypekey, paymenttypekey) a
LEFT JOIN #ro b on a.gtdoc# = b.ro AND a.servicetypekey = b.servicetypekey AND a.paymenttypekey = b.paymenttypekey AND abs(a.gttamt) = abs(b.sales)
LEFT JOIN #rotech c  on b.ro = c.ro AND b.servicetypekey = c.servicetypekey AND b.paymenttypekey = c.paymenttypekey
WHERE b.ro IS NOT NULL 
  AND description = 'Tech 511'
  AND a.paymenttypekey = (SELECT paymenttypekey FROM dimpaymenttype WHERE paymenttypecode = 'w')
*/





--/> use sale amounts to JOIN ------------------------------------------------------------------------