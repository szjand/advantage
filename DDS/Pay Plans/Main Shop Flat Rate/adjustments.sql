SELECT * FROM dds.stgArkonaSDPRDET WHERE ptpadj <> '' AND ptdate > '06/01/2014'

SELECT ptpadj, ptdbas, COUNT(*) FROM dds.stgArkonaSDPRDET 
WHERE ptpadj <> '' AND ptdate > '01/01/2014' GROUP BY ptpadj, ptdbas

SELECT *
FROM stgArkonaSDPRDET
WHERE ptro# = '16045948'
  AND ptline = 4 
  
  
SELECT *
FROM ( 
    SELECT ptco#, ptdate, ptpkey, ptro#, ptline, pttech, round(sum(ptlhrs), 2) AS ptlhrs
    FROM stgArkonaSDPXTIM 
    GROUP BY ptco#, ptdate, ptpkey, ptro#, ptline, pttech) x
WHERE ptlhrs <> 0    
  AND pttech = '566'
ORDER BY ptdate desc  


SELECT *
FROM stgArkonaSDPXTIM a
INNER JOIN (
    SELECT ptpkey,  round(sum(ptlhrs), 2) AS ptlhrs
    FROM stgArkonaSDPXTIM 
    GROUP BY ptpkey) x on a.ptpkey = x.ptpkey
WHERE x.ptlhrs <> 0      
  AND pttech = '566'

-- some with multiple adjustments per line  
SELECT *
FROM stgArkonaSDPXTIM a
INNER JOIN (
    SELECT ptpkey,  round(sum(ptlhrs), 2) AS ptlhrs
    FROM stgArkonaSDPXTIM 
    GROUP BY ptpkey) x on a.ptpkey = x.ptpkey
WHERE x.ptlhrs <> 0      
  AND ptro# IN (
    SELECT ptro#
    FROM stgArkonaSDPXTIM a
    INNER JOIN (
        SELECT ptpkey,  round(sum(ptlhrs), 2) AS ptlhrs
        FROM stgArkonaSDPXTIM 
        GROUP BY ptpkey) x on a.ptpkey = x.ptpkey
    WHERE x.ptlhrs <> 0      
    GROUP BY ptro#, ptline
    HAVING COUNT(*) > 1) 

SELECT ptro#, ptline
FROM stgArkonaSDPXTIM a
INNER JOIN (
    SELECT ptpkey,  round(sum(ptlhrs), 2) AS ptlhrs
    FROM stgArkonaSDPXTIM 
    GROUP BY ptpkey) x on a.ptpkey = x.ptpkey
WHERE x.ptlhrs <> 0      
GROUP BY ptro#, ptline
HAVING COUNT(*) > 1