-- the issue IS census vs team membership
-- this IS the query used for updating techTrainingOther
-- it shows to techs that are no longer employed
-- Bear & Eken
DECLARE @d1 date;
DECLARE @d2 date;
DECLARE @yearmonth integer;
@d1 = '11/03/2013';
@d2 = '11/16/2013';
SELECT b.technumber, b.name
FROM (
  SELECT technumber, employeenumber, name
  FROM dimtech a
  WHERE a.flagdeptcode = 'MR'
    AND a.storecode = 'ry1'
    AND a.employeenumber <> 'na'
    AND a.active = true 
    AND a.techkeyfromdate < @d2
    AND a.techkeythrudate > @d1
  GROUP BY a.technumber, a.employeenumber, a.name) b 
GROUP BY b.technumber, b.name  
ORDER BY b.name  

-- "active" techs that have been termed  
SELECT a.name, a.technumber, a.employeenumber, a.rowfromts, a.techkeyfromdate, 
  a.techkeythrudate, b.termdate
FROM dimtech a
LEFT JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.activecode = 't'
  AND b.currentrow = true -- necessary to elim rehire/reuse
WHERE a.storecode = 'ry1'
  AND a.employeenumber <> 'na'
  AND a.flagdeptcode = 'mr'
  AND a.currentrow = true
  AND a.active = true
  AND b.termdate IS NOT NULL 
ORDER BY a.name    

-- the age old question of what DO these different fucking words mean
active, techkey from/thru, etc

