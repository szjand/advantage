--< september -------------------------------------------------------------------------------
DROP TABLE #wtf1;
CREATE TABLE #wtf1(tech cichar(50), ros integer, hours double);
insert into #wtf1 values('QUICK LANE TEAM',2,3);
insert into #wtf1 values('PDQ Tech',26,13.23);
insert into #wtf1 values('Shop Misc',6,0.6);
insert into #wtf1 values('Tech 502',30,91.48);
insert into #wtf1 values('Tech 511',92,163.46);
insert into #wtf1 values('Tech 519',69,151.68);
insert into #wtf1 values('Tech 522',14,82.67);
insert into #wtf1 values('Tech 526',89,89.8);
insert into #wtf1 values('Tech 528',13,130.3);
insert into #wtf1 values('Tech 532',23,27.8);
insert into #wtf1 values('Tech 536',91,152.46);
insert into #wtf1 values('Tech 537',66,97.04);
insert into #wtf1 values('Tech 545',23,116.27);
insert into #wtf1 values('Tech 557',91,142.03);
insert into #wtf1 values('Tech 566',69,108.59);
insert into #wtf1 values('Tech 573',74,83.37);
insert into #wtf1 values('Tech 574',18,84.01);
insert into #wtf1 values('Tech 575',95,133.78);
insert into #wtf1 values('Tech 577',15,89.57);
insert into #wtf1 values('Tech 580',51,113.71);
insert into #wtf1 values('Tech 583',68,80.7);
insert into #wtf1 values('Tech 585',80,100.64);
insert into #wtf1 values('Tech 595',87,114.5);
insert into #wtf1 values('Tech 599',127,140.71);
insert into #wtf1 values('Tech 608',63,121.45);
insert into #wtf1 values('Tech 611',53,80.75);
insert into #wtf1 values('Tech 612',62,80.7);
insert into #wtf1 values('Tech 623',61,80.21);
insert into #wtf1 values('Tech 625',125,154.91);
insert into #wtf1 values('Tech 626',89,78.97);
insert into #wtf1 values('Tech 627',85,71.76);
insert into #wtf1 values('Tech 629',78,114.21);
insert into #wtf1 values('Tech 630',18,11.34);
insert into #wtf1 values('Tech 631',68,123.37);
insert into #wtf1 values('Tech 634',47,83.02);
insert into #wtf1 values('Tech 636',113,109.92);

DROP TABLE #wtf2;
SELECT description, COUNT(*) AS ros, round(SUM(flaghours), 2) AS flaghours, 
  SUM(xtimhours) AS xtimhours, SUM(adjhours) AS adjhours, SUM(neghours) AS neghours
INTO #wtf2
FROM (  
  SELECT a.ro, description, a.technumber, flaghours, xtimhours, adjhours, neghours
  FROM (   
    SELECT ro, c.description, c.technumber, round(SUM(flaghours), 2) AS flaghours      
    FROM factRepairOrder a
    INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dimtech c on b.techkey = c.techkey
    WHERE a.storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey IN (  
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN '09/01/2013' AND '09/30/2013')      
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro, c.description, c.technumber) a       
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on a.ro = d.ro AND a.Technumber = d.technumber) c   
GROUP BY description  

-- 11/16
triple fuck me sideways
the chaos continues
no way to generate a list of ros included IN lab/prof except a tech at a time
can NOT duplicate lab/prof for ros OR flag hours

sep lp: 3422.01 vs qu: 3502.45
-- techs with flaghour diffs
SELECT a.tech, a.ros, b.ros,  a.hours, b.flaghours, b.xtimhours, b.adjhours, b.neghours, 
  round(hours - (flaghours - coalesce(xtimhours,0) + coalesce(adjhours, 0) + coalesce(neghours,0)), 2) as Diff--, b.*--, flaghours-xtimhours+adjhours+neghours, round(hours-flaghours-xtimhours+adjhours+neghours, 2)
FROM #wtf1 a
full OUTER JOIN #wtf2 b on a.tech = b.description
WHERE hours <> flaghours - coalesce(xtimhours,0) + coalesce(adjhours, 0) + coalesce(neghours,0)
UNION
SELECT 'total', SUM(a.ros), SUM(b.ros), SUM(hours), SUM(flaghours), SUM(coalesce(xtimhours,0)), 
  SUM(coalesce(adjhours,0)), SUM(coalesce(neghours,0)), 0
FROM #wtf1 a
full OUTER JOIN #wtf2 b on a.tech = b.description

ok, fuck fuck fuck
DO a spreadsheet for every fucking tech with a difference, list the ros
find the diffs
define the anomalies

CREATE TABLE zLabProf (
  yearmonth integer CONSTRAINT NOT NULL,
  tech cichar(12) CONSTRAINT NOT NULL,
  ro cichar(9) CONSTRAINT NOT NULL,
  hours double CONSTRAINT NOT NULL) IN database;
  
SELECT *
FROM zLabProf  

SELECT a.tech, a.ros, b.ros,  a.hours, b.flaghours, b.xtimhours, b.adjhours, b.neghours, 
  round(hours - (flaghours - coalesce(xtimhours,0) + coalesce(adjhours, 0) + coalesce(neghours,0)), 2) as Diff--, b.*--, flaghours-xtimhours+adjhours+neghours, round(hours-flaghours-xtimhours+adjhours+neghours, 2)
FROM #wtf1 a
full OUTER JOIN #wtf2 b on a.tech = b.description
WHERE hours <> flaghours - coalesce(xtimhours,0) + coalesce(adjhours, 0) + coalesce(neghours,0)


  SELECT a.ro, description, a.technumber, flaghours, coalesce(xtimhours,0) AS xtim, 
    coalesce(adjhours, 0) AS adj, coalesce(neghours, 0) AS neg
--INTO #LabProf    
  FROM (   
    SELECT ro, c.description, c.technumber, round(SUM(flaghours), 2) AS flaghours      
    FROM factRepairOrder a
    INNER JOIN brtechgroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dimtech c on b.techkey = c.techkey
    WHERE a.storecode = 'ry1'
      AND flaghours <> 0
      AND finalclosedatekey IN (  
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN '09/01/2013' AND '09/30/2013')      
      AND a.ServiceTypeKey IN (
        SELECT ServiceTypeKey
        FROM dimServiceType
        WHERE ServiceTypeCode IN ('am','mr'))
    GROUP BY ro, c.description, c.technumber) a       
  LEFT JOIN (
    SELECT Storecode, ro, technumber,
      coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
      coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
      coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
    FROM stgFlagTimeAdjustments  
    GROUP BY Storecode, ro, technumber) d on a.ro = d.ro AND a.Technumber = d.technumber
    
 SELECT *
 FROM zLabProf a
 full OUTER JOIN #LabProf b on a.tech = b.description AND a.ro = b.ro
 WHERE (
   a.ro IS NULL 
   OR b.ro IS NULL) 
   OR hours <> flaghours - xtim + adj + neg)

   
SELECT *
FROM (   
  SELECT tech, SUM(hours)
  FROM zLabProf
  GROUP BY tech) a   
LEFT JOIN (
  SELECT description, SUM(flaghours - xtim + adj + neg)
  FROM #labprof
  GROUP BY description) b on a.tech = b.description
  
SELECT *
INTO #pdq1
FROM zLabProf
WHERE tech = 'PDQ Tech'; 
SELECT *
INTO #pdq2
FROM #labprof
WHERE description = 'PDQ Tech'; 


SELECT *
FROM #pdq1 a
full OUTER JOIN #pdq2 b on a.ro = b.ro
WHERE a.ro IS NULL OR b.ro IS NULL 
   
SELECT * FROM #labprof   


-- different ros
-- this works
SELECT *
INTO #pdq1
FROM zLabProf
WHERE tech = 'PDQ Tech'; 
SELECT *
INTO #pdq2
FROM #labprof
WHERE description = 'PDQ Tech'; 

SELECT *
FROM #pdq1 a
full OUTER JOIN #pdq2 b on a.ro = b.ro
WHERE a.ro IS NULL OR b.ro IS NULL 

-- why doesn't this?
DECLARE @tech string;
@tech = 'PDQ Tech';  
SELECT *
FROM (
  SELECT *
  FROM zLabProf a
  full OUTER JOIN #labprof b on a.ro = b.ro
  WHERE a.tech = @tech
  AND b.description = @tech) c  
WHERE ro IS NULL OR ro_1 IS NULL 
-- beats the hell out of me, but this does
DECLARE @tech string;
@tech = 'PDQ Tech';  
SELECT *
FROM (
  SELECT *
  FROM zLAbProf a
  LEFT JOIN #labprof b on a.tech = b.description AND a.ro = b.ro
  WHERE  b.ro IS NULL 
  UNION 
  SELECT b.*, a.*
  FROM #labprof a
  LEFT JOIN zLabProf b on a.description = b.tech AND a.ro = b.ro
  WHERE b.ro IS NULL) c 
WHERE tech = @tech or description = @tech



-- different hours
DECLARE @tech string;
@tech = 'Tech 511';  
SELECT *
FROM (
  SELECT *
  FROM zLabProf a
  full OUTER JOIN #labprof b on a.ro = b.ro
  WHERE a.tech = @tech
  AND b.description = @tech) c  
WHERE hours <> flaghours - xtim + adj + neg
   
   
SELECT * 
FROM zLabProf
WHERE hours <> 0