From Arkona

-- from Technician Efficiency Report Page
Any repair orders with a final Close Date within the date range selected will be evaluated for the report. 
(The Final Close date is the date the last portion of the RO was closed. 
  If an RO has both Customer pay and Warranty pay lines, the Final Close 
  date will be the date the Warranty portion was closed). 

-- Repair Order: Closed - What is the close date?
The Closed date that displays on the closed repair order is the initial 
close date (the date the customer-pay lines on the Repair Order were closed). 
You may have technician lines that show a flag date on warranty, internal, 
or service contract job lines after the Closed date on the Repair Order. 