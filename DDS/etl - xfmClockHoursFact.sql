/*
1/31/12
xfmClockHoursFactA/B replaced with tmpClockHours
*/
SELECT *
FROM stgArkonaPYPCLOCKIN


SELECT d.thedate, d.dayofweek, c.*,
  timestampadd(sql_tsi_second, second(yiclkint), timestampadd(
    sql_tsi_minute, minute(yiclkint), timestampadd(
    sql_tsi_hour, hour(yiclkint), cast(yiclkind as sql_timestamp)))) AS ClockInTS,
  timestampadd(sql_tsi_second, second(yiclkoutt), timestampadd(
    sql_tsi_minute, minute(yiclkoutt), timestampadd(
    sql_tsi_hour, hour(yiclkoutt), cast(yiclkoutd as sql_timestamp)))) AS ClockOutTS
FROM Day d
LEFT JOIN stgArkonaPYPCLOCKIN c ON d.TheDate = c.yiclkind
  AND c.yicode = 'O' -- clock hours
--  AND c.yiemp# = '1137590'
WHERE d.TheDate BETWEEN '07/31/2009' and curdate()
  AND d.dayofweek = 1
ORDER BY d.thedate desc  


SELECT yicode, COUNT(*)
FROM stgArkonaPYPCLOCKIN
GROUP BY yicode


SELECT *
FROM day
WHERE DayofWeek = 1

-- each record IS unique
SELECT yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
FROM stgArkonaPYPCLOCKIN
GROUP BY yico#, yiemp#, yiclkind, yiclkint, yiclkoutd, yiclkoutt, yicode
  HAVING COUNT(*) > 1
-- each clockin IS unique  
SELECT yico#, yiemp#, yiclkind, yiclkint
FROM stgArkonaPYPCLOCKIN
GROUP BY yico#, yiemp#, yiclkind, yiclkint
  HAVING COUNT(*) > 1  

-- tmpPYPCLOCKIN: 28 days back FROM the most recent Sunday
-- everything matches except yiclkint (clock hours only (O)) -- no records
SELECT *
FROM tmpPYPCLOCKIN t
LEFT JOIN stgArkonaPYPCLOCKIN s ON t.yico# = s.yico#
  AND t.yiemp# = s.yiemp#
  AND t.yiclkind = s.yiclkind
  AND t.yiclkoutd = s.yiclkoutd
  AND t.yiclkoutt = s.yiclkoutt
WHERE t.yicode = 'O'  
  AND t.yiclkint <> s.yiclkint
ORDER BY t.yiemp#, t.yiclkind

SELECT *
FROM tmpPYPCLOCKIN
WHERE yiclkind <> yiclkoutd

-- date of the last day of the previous month
SELECT curdate() - dayofmonth(curdate()) FROM system.iota

-- overtime time IS defined AS sunday -> saturday, clock hours only, over 40 hours
-- 10 sundays ago
SELECT MAX(thedate) - 70
FROM day d
WHERE dayofweek = 1
  AND thedate <= curdate()
-- scrape since 10 sundays ago FROM last sunday.

select d.thedate, d.dayofweek, t.*
FROM (
  SELECT *
  FROM day
  WHERE thedate BETWEEN (
    SELECT MAX(thedate) - 70
    FROM day d
    WHERE dayofweek = 1
      AND thedate <= curdate()) AND curdate()) d
LEFT JOIN tmpPYPCLOCKIN t ON d.thedate = t.yiclkind 
  AND t.yicode= 'O'     
  
--first step: CREATE the ts fields  
SELECT d.thedate, d.dayofweek, c.*,
  timestampadd(sql_tsi_second, second(yiclkint), timestampadd(
    sql_tsi_minute, minute(yiclkint), timestampadd(
    sql_tsi_hour, hour(yiclkint), cast(yiclkind as sql_timestamp)))) AS ClockInTS,
  timestampadd(sql_tsi_second, second(yiclkoutt), timestampadd(
    sql_tsi_minute, minute(yiclkoutt), timestampadd(
    sql_tsi_hour, hour(yiclkoutt), cast(yiclkoutd as sql_timestamp)))) AS ClockOutTS
   
FROM (
  SELECT *
  FROM day
  WHERE thedate BETWEEN (
    SELECT MAX(thedate) - 70
    FROM day d
    WHERE dayofweek = 1
      AND thedate <= curdate()) AND curdate()) d
LEFT JOIN tmpPYPCLOCKIN c ON d.TheDate = c.yiclkind
  AND c.yicode = 'O' -- clock hours
  

-- ok, ON to the -b- TABLE 
-- this IS WHERE we start looking at accumulating emp/code/day totals  
  
SELECT f.*, 
  e.employeekey, e.name, d.datekey, round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)  
FROM xfmClockHoursFacta f
LEFT JOIN edwEmployeeDim e ON f.yiemp# = e.EmployeeNumber
LEFT JOIN day d ON f.yiclkind = d.thedate
WHERE f.yicode = 'O' -- yicode corresponds to fact columns

SELECT 
--  e.employeekey, e.name, d.datekey, round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)  
  f.yico#, e.employeekey, e.name, f.yicode, yiclkind,
  coalesce(CASE WHEN yicode = 'VAC' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) as VacHours,
  coalesce(CASE WHEN yicode = 'PTO' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS PTOHours,
  coalesce(CASE WHEN yicode = 'HOL' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS HolHours,
  coalesce(CASE WHEN yicode = 'O' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS ClockHours
FROM xfmClockHoursFacta f
LEFT JOIN edwEmployeeDim e ON f.yiemp# = e.EmployeeNumber
LEFT JOIN day d ON f.yiclkind = d.thedate
WHERE f.yicode <> 'I' -- yicode corresponds to fact columns
  AND yiclkind > curdate() - 30
  AND employeekey IN (1362, 1391)
GROUP BY f.yico#, e.employeekey, e.name, f.yicode, yiclkind
ORDER BY yiclkind
-- fucking up ON the JOIN to empDim
-- here's the problem
-- danielle did NOT clock IN twice IN 12/8, once AS anderson AND once AS streitz
SELECT 
--  e.employeekey, e.name, d.datekey, round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)  
  f.yico#, e.employeekey, e.name, f.yicode, yiclkind,
  coalesce(CASE WHEN yicode = 'VAC' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) as VacHours,
  coalesce(CASE WHEN yicode = 'PTO' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS PTOHours,
  coalesce(CASE WHEN yicode = 'HOL' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS HolHours,
  coalesce(CASE WHEN yicode = 'O' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS ClockHours
FROM xfmClockHoursFacta f
LEFT JOIN edwEmployeeDim e ON f.yiemp# = e.EmployeeNumber
  AND f.yiclkind BETWEEN 
LEFT JOIN day d ON f.yiclkind = d.thedate
WHERE f.yicode <> 'I' -- yicode corresponds to fact columns
  AND yiclkind > curdate() - 30
  AND employeekey IN (1362, 1391)
GROUP BY f.yico#, e.employeekey, e.name, f.yicode, yiclkind
ORDER BY yiclkind
-- danielle IN parts, type 2 name change
select *
FROM edwEmployeeDim
WHERE employeekey IN (1362, 1391)

SELECT *
FROM edwEmployeeDim
WHERE employeenumber = '16250'

-- fuck with the JOIN to empDim

SELECT 
--  e.employeekey, e.name, d.datekey, round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)  
  f.yico#, e.employeekey, e.name, f.yicode, yiclkind,
  coalesce(CASE WHEN yicode = 'VAC' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) as VacHours,
  coalesce(CASE WHEN yicode = 'PTO' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS PTOHours,
  coalesce(CASE WHEN yicode = 'HOL' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS HolHours,
  coalesce(CASE WHEN yicode = 'O' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS ClockHours
FROM xfmClockHoursFacta f
LEFT JOIN edwEmployeeDim e ON f.yiemp# = e.EmployeeNumber
  AND f.yiclkind BETWEEN e.hiredate AND coalesce(CAST(RowExpirationTS AS sql_date), cast('12/31/9999' AS sql_date))
LEFT JOIN day d ON f.yiclkind = d.thedate
WHERE f.yicode <> 'I' -- yicode corresponds to fact columns
  AND yiclkind > curdate() - 30
  AND employeekey IN (1362, 1391)
GROUP BY f.yico#, e.employeekey, e.name, f.yicode, yiclkind
ORDER BY yiclkind

SELECT coalesce(CAST(RowExpirationTS AS sql_date), cast('12/31/9999' AS sql_date))
FROM edwEmployeeDim
WHERE employeenumber = '16250'

-- hmm thinking, leave at the empdim until we get to b TABLE
-- so that takes use here:
-- one row per co/emp/yiclkind, yicode
-- this IS the basis for figuring overtime
-- greg's overtime query relies ON a self JOIN BETWEEN two result sets that are each one row per co/emp/yiclkind WHERE yicode = O
-- his result sets's are hard coded with a date range
-- i'm thining i can GROUP BY SunToSatWeek
-- so let's put FactA INTO factB
INSERT INTO xfmClockHoursFactB
SELECT yico#, yiemp#, yiclkind, yicode,
  coalesce(CASE WHEN yicode = 'VAC' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) as VacHours,
  coalesce(CASE WHEN yicode = 'PTO' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS PTOHours,
  coalesce(CASE WHEN yicode = 'HOL' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS HolHours,
  coalesce(CASE WHEN yicode = 'O' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS ClockHours
FROM xfmClockHoursFacta 
WHERE yicode <> 'I' 
GROUP BY yico#, yiemp#, yiclkind, yicode

-- tmpOvertime done
-- time to populate edwClockHoursFact

-- summarize
attempting to pppulate edwClockHoursFact
Danielle has 2 employeekeys (2 records IN edwEmployeeDim)
the employeenumber IS the same for both records
due to a change IN her last name
edwClockHoursFact references the employeekey

SELECT *
FROM xfmClockHoursFactB f
WHERE f.yiemp# = '16250'

-- during what time periods are each of the empkey values valid
SELECT *
FROM edwEmployeeDim
WHERE employeenumber = '16250'
-- damn this could get complicated, 
-- define the different groupings for employees
-- eg 
-- were hired IN 2010
-- clocked IN at WORK  ON 12/1/2011 (edwClockHoursFact)
-- how many active/parttime employees ON 10/10/2010
-- WHEN did jamal move FROM pdq to service

so i am struggling with populating the fact TABLE for danielle
ON for what period of time has she been an employee 
IF current row = true AND termdatekey IS NULL THEN
period of time during which a row IS the current row
Dimension GRANULARITY = DAY
Fact Granularity 1 row per store/emp#/currentrow for each day of active employment -- this doesn't feel right yet
  how many hours an employee clocked per day
-- what abt a termed employee that comes back: same employee number?
focus ON delivering the fact table
-- assume
ahh every NK will have a CurrentRow
-- prove
SELECT *
FROM edwEmployeeDim e1
WHERE NOT EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE storecode = e1.storecode
    AND employeenumber = e1.employeenumber
    AND CurrentRow = true)
    
 


-- employees with multiple (EmployeeKey) records  
SELECT *
FROM edwEmployeeDim
WHERE employeenumber IN ( 
  SELECT employeenumber
  FROM edwEmployeeDim
  GROUP BY storecode, employeenumber 
    HAVING COUNT(*) > 1)
ORDER BY employeenumber    

-- obssessing ON needing to know reason why for current row
-- nice to know, may NOT be relevant to populating the fact TABLE
-- the fact TABLE will tell me
active employee: termdate.datetype = 'NA'
SELECT DISTINCT datetype FROM day

feeling fuckit, facttable IS employee key, that's ALL it knows
so, ON any given day of active employment, how many reghrs, vachours, etc did that empkey clock
given the statement of granularity, need to know the period of active employment
SELECT *
-- notion of double counting steers me away FROM employeekey for this value
-- columns ActiveFromDate ActiveThruDate
-- so derive them
-- there will never be a termdate = curdate() because the data IS always FROM yesterday
SELECT storecode, employeekey, employeenumber, hiredate, termdate, 
  CASE
    WHEN termdate = '12/31/9999' THEN 'Active'
    ELSE 'Term'
  END AS act
FROM (  
  SELECT storecode, employeekey, employeenumber, hiredate, MAX(termdate) AS termdate
  FROM edwEmployeeDim  
  GROUP BY storecode, employeekey, employeenumber, hiredate) x
WHERE employeenumber = '16250'  
-- this demonstrates the problem
-- don't need a record for both employeekeys for the entire range of hiredate to termdate
  -- so i need a clock hours for each 

factb AND tempOT are giving me the hours per employeenumber/day which IS good
how DO i break out the hours per employeekey/day?
ADD effectivets to query?

SELECT storecode, employeekey, employeenumber, hiredate, termdate, 
  CASE
-- the employeekey 1362 IS NOT currently active, empkey 1391 is  
-- the empkey IS a comb of effdate AND ? expdate ?
    WHEN termdate = '12/31/9999' THEN 'Active'
    ELSE 'Term'
  END AS act, 
  EffTS 
FROM ( -- 1 row per empkey/effTS
  SELECT storecode, employeekey, employeenumber, hiredate, MAX(termdate) AS termdate,
    CAST(RowEffectiveTS AS sql_date) AS EffTS
  FROM edwEmployeeDim  
  GROUP BY storecode, employeekey, employeenumber, hiredate, CAST(RowEffectiveTS AS sql_date)) x
WHERE employeenumber in (
  '16250', -- name change
  '1111211', -- term
  '171210') -- dept change
ORDER BY employeenumber


-- 1 row per empkey/effTS: check that assumption
SELECT employeekey, effts 
FROM (
  SELECT storecode, employeekey, employeenumber, hiredate, MAX(termdate) AS termdate,
    CAST(RowEffectiveTS AS sql_date) AS EffTS
  FROM edwEmployeeDim  
  GROUP BY storecode, employeekey, employeenumber, hiredate, CAST(RowEffectiveTS AS sql_date)) x
GROUP BY employeekey, effts
  HAVING COUNT(*) > 1  

should be able to CREATE the records to INSERT INTO the fact TABLE with just the natural keys?
etl toolkit p214 the final etl step IS converting the natural keys IN the new input records INTO the correct, contemporary surrogate keys
which has me thinking about empDim NK
store/emp#/currentrow = true:  this IS NOT a NK, but a constraint
store/emp#, efftS 
-- what i want to return IS the appropriate empkey FROM edwEmployeeDim 
-- this IS starting to feel CLOSE, need the empkey FROM edwEmployeeDim 
-- i don't need a record for a yiemp# that has no clock hours ON a day
-- the fact TABLE IS preloaded with zeroes, i will be updating only nonzero values FROM this query
-- so, now, how to ADD empKey
SELECT d.thedate, fb.*, ot.*
FROM day d
LEFT JOIN (
  select *
  from xfmClockHoursFactB 
  WHERE yiemp# IN ('16250','1111211','171210')) fb ON d.thedate = fb.yiclkind
LEFT JOIN (
  SELECT * 
  FROM tmpOvertime  
  WHERE yiemp# IN ('16250','1111211','171210')) ot ON d.thedate = ot.yiclkind
    AND fb.yiemp# = ot.yiemp#
WHERE d.YearMonth in (201112, 201201)
ORDER BY d.thedate, fb.yico#, fb.yiemp#

-- so, now, how to ADD empKey
-- IF TheDate IS BETWEEN whatever fucking period i figure IS the period of active employment
-- so what are the candidates:
if termdate not null then hiredate -> termdate
IF termdate IS NULL
  
SELECT d.thedate, fb.yiemp#, fb.yicode, fb.vachours, fb.ptohours, fb.holhours, 
  fb.clockhours, ot.regularhours, ot.overtimehours, emp.employeekey, emp.name
FROM day d
LEFT JOIN (
  select *
  from xfmClockHoursFactB 
  WHERE yiemp# IN ('16250','1111211','171210')) fb ON d.thedate = fb.yiclkind
LEFT JOIN (
  SELECT * 
  FROM tmpOvertime  
  WHERE yiemp# IN ('16250','1111211','171210')) ot ON d.thedate = ot.yiclkind
    AND fb.yiemp# = ot.yiemp#
LEFT JOIN (
  SELECT *
  FROM edwEmployeeDim WHERE employeenumber IN ('16250','1111211','171210')) emp ON 
    CASE WHEN emp.termdate <> '12/31/9999' 
      THEN d.thedate BETWEEN emp.hiredate AND emp.termdate 
    ELSE
        
    END  
    AND fb.yiemp# = emp.employeenumber  
WHERE d.YearMonth in (201112, 201201)
  AND emp.employeenumber = '1111211'
--AND emp.termdate <> '12/31/9999'
ORDER BY d.thedate, fb.yico#, fb.yiemp#

-- the period of time for which i need to JOIN to tempOT
-- what i'm trying to figure out IS, WHEN there are multiple empkeys per emp#,

-- how to populate the clkhrsFact TABLE with the appropriate empkey value
-- assume 1 row per term activecode & employeenumber
-- prove  should this be IN edwEmployeeDim constraint checks?
SELECT storecode, employeenumber
FROM edwEmployeeDim
WHERE ActiveCode = 'T'
GROUP BY storecode, employeenumber
  HAVING COUNT(*) > 1

  
SELECT * 
FROM edwEmployeeDim
WHERE employeenumber = '16250'
ORDER BY employeenumber

SELECT MIN(yiclkind), MAX(yiclkind)
FROM xfmClockHoursFactB
WHERE yiemp# = '16250'

SELECT d.thedate, fb.yiemp#, fb.yicode, fb.vachours, fb.ptohours, fb.holhours, 
  fb.clockhours, ot.regularhours, ot.overtimehours--, emp.employeekey, emp.name
FROM day d
LEFT JOIN (
  select *
  from xfmClockHoursFactB 
  WHERE yiemp# IN('16250','171210')) fb ON d.thedate = fb.yiclkind
LEFT JOIN (
  SELECT * 
  FROM tmpOvertime  
  WHERE yiemp# IN ('16250','171210')) ot ON d.thedate = ot.yiclkind
    AND fb.yiemp# = ot.yiemp#
WHERE d.thedate BETWEEN '09/12/2011' AND (SELECT MAX(yiclkind) FROM xfmClockHoursFactB)

-- 1/10 still trying to populate the ClHrFact TABLE
-- determine period for which an empkey IS valid
-- period of time for which an empkey (any surrogate key) IS valid
-- could be context sensitive
-- this IS different than the period of active employment

-- NULL rowExp = currentRow(true)
SELECT *
FROM edwEmployeeDim
WHERE RowExpirationTS IS NULL
AND currentrow = false
16250  1362  9/12/12 -> 1/1/12  (hired to 
       1391  1/1/12 -> now
       
-- term AND currentrow 
SELECT *
FROM edwEmployeeDim
WHERE termdatekey <> 7306
AND currentrow = true   

-- term AND currentrow how many have multiple keys
-- none currently DO, but some will
-- danielle currently has 2 empkeys, could have more, due to type 2 changes,
--   WHEN she terms, the currentrow will be type 1 udpated
-- 1st interval IS inclusive (BETWEEN) - second interval starts ON day after END of interval 1
--   AND extends thru the coalesce(rowExp, curdate)
SELECT *
FROM edwEmployeeDim
WHERE termdatekey <> 7306
  AND currentrow = true  
  AND employeenumber IN (
    SELECT employeenumber
    FROM edwEmployeeDim
    GROUP BY storecode, employeenumber
      HAVING COUNT(*) > 1)   

SELECT * FROM edwEmployeeDim WHERE RowUpdatedTS IS NOT NULL

SELECT * FROM edwEmployeeDim WHERE RowExpirationTS IS NULL 

-- thinking first cut IS IS NULL RowExpirationTS

-- 1 row/empkey, with fromDate, thruDate
ahh shit, IF expTs IS NOT NULL, IS the FROM date hiredate OR rowupdatedTS
fuck, this IS why i thought it was important to UPDATE edwEmployeeDim.RowEffectiveTS to 
match hiredate WHERE appropriate 

a new empKey IS valid FROM the hiredate till rowexpirationTS

SELECT employeekey, employeenumber, name, hiredate, termdate,  
  CASE
    WHEN (RowExpirationTS IS NOT NULL and termdatekey = 7306 and CurrentRow = false) THEN hiredate
    WHEN (RowExpirationTS IS NULL AND termdatekey <> 7306 AND currentrow = true) THEN hiredate
    WHEN (RowExpirationTS IS NOT NULL AND termdatekey = 7306 AND CurrentRow = true) THEN hiredate
    WHEN (RowExpirationTS IS NULL AND termdatekey = 7306 AND CurrentRow = true) THEN hiredate
  END AS FromDATE,
  RowLoadedTS, RowUpdatedTS, RowEffectiveTS, RowExpirationTS, CurrentRow
FROM edwEmployeeDim
WHERE employeenumber ='16250'
ORDER BY employeenumber

SELECT *
from edwEmployeeDim
WHERE coalesce(RowUpdatedTS, RowEffectiveTS) <> RowEffectiveTS

-- fuck fuck fuck
ADD yet another record for danielle
DO a type 2 full to part time
/********** 1/10/12 BEGIN *****************************************************/
going with kimball columns
Calendar Date foreign key (date of change)
Row Effective DateTime (exact date-time of change)
Row End DateTime (exact date-time of next change)
Reason for Change (text field)
Current Flag (current/expired)

UPDATE edwEmployeeDim
SET rowchangedate = CAST(NULL AS sql_date),
    rowfromts = CAST(NULL AS sql_timestamp)
WHERE employeenumber NOT IN (
  SELECT employeenumber
  FROM edwEmployeeDim
  GROUP BY storecode, employeenumber
    HAVING COUNT(*) > 1)
    
SELECT *
FROM edwEmployeeDim
WHERE employeenumber IN (
  SELECT employeenumber
  FROM edwEmployeeDim
  GROUP BY storecode, employeenumber
    HAVING COUNT(*) > 1)  
ORDER BY employeenumber     

SELECT employeekey, employeenumber, name,
 coalesce(cast(RowFromTS AS sql_date), HireDate) AS EmpKeyFrom,
 CASE
   WHEN (SELECT datetype FROM day WHERE datekey = e.termdatekey) <> 'NA' THEN termdate
 ELSE
   coalesce(CAST(RowThruTS AS sql_Date), CAST(NULL AS sql_date))  
 END AS EmpKeyThru
FROM edwEmployeeDim e
WHERE employeenumber IN (
  SELECT employeenumber
  FROM edwEmployeeDim
  GROUP BY storecode, employeenumber
    HAVING COUNT(*) > 1)  
ORDER BY employeenumber     

-- FINALLY, this looks good
SELECT storecode, employeekey, employeenumber, name,
 coalesce(cast(RowFromTS AS sql_date), HireDate) AS EmpKeyFrom,
 CASE
   WHEN (SELECT datetype FROM day WHERE datekey = e.termdatekey) <> 'NA' THEN termdate
 ELSE
   coalesce(CAST(RowThruTS AS sql_Date), CAST(NULL AS sql_date))  
 END AS EmpKeyThru
FROM edwEmployeeDim e
ORDER BY employeenumber  

-- trying to see the data for multiple empkeys/emp#
SELECT MIN(yiclkind), MAX(yiclkind) FROM tmpOvertime -- 1/1/10 -> 1/2/12

SELECT MIN(yiclkind), MAX(yiclkind) FROM xfmClockHoursFacta  -- 7/27/07 -> 1/3/12, there's the probl, FactA NOT current
SELECT MIN(yiclkind), MAX(yiclkind) FROM tmpPYPCLOCKIN
SELECT MIN(yiclkind), MAX(yiclkind) FROM stgArkonaPYPCLOCKIN
SELECT MIN(yiclkind), MAX(yiclkind) FROM stgArkonaPYPCLOCKIN WHERE yiemp# = '171210'
SELECT MIN(yiclkind), MAX(yiclkind) FROM xfmClockHoursFacta WHERE yiemp# = '171210'
SELECT MIN(yiclkind), MAX(yiclkind) FROM xfmClockHoursFactb WHERE yiemp# = '171210'


-- this looks LIKE it
SELECT d.thedate, fb.yico#, fb.yiemp#, fb.yicode, fb.vachours, fb.ptohours, fb.holhours, 
  fb.clockhours, ot.regularhours, ot.overtimehours, k.* --, emp.employeekey, emp.name
-- SELECT *  
FROM day d
LEFT JOIN xfmClockHoursFactB fb ON d.thedate = fb.yiclkind
LEFT JOIN tmpOvertime ot ON d.thedate = ot.yiclkind
  AND fb.yiemp# = ot.yiemp#
LEFT JOIN (
    SELECT storecode, employeekey, employeenumber, name,
      coalesce(cast(RowFromTS AS sql_date), HireDate) AS EmpKeyFrom1,
      CASE
        WHEN RowFromTS IS NULL THEN HireDate
      ELSE
        cast(timestampadd(sql_tsi_day, 1, RowFromTS) AS sql_date)
      END AS EmpKeyFrom2,
      CASE
        WHEN (SELECT datetype FROM day WHERE datekey = e.termdatekey) <> 'NA' THEN termdate
      ELSE
        coalesce(CAST(RowThruTS AS sql_Date), (select thedate from day where datetype = 'NA')) --CAST('12/31/9999' AS sql_date))  
      END AS EmpKeyThru
    FROM edwEmployeeDim e) k 
  ON fb.yico# = k.storecode
  AND fb.yiemp# = k.employeenumber
  AND d.thedate BETWEEN k.EmpKeyFrom1 AND k.EmpKeyThru    
WHERE d.thedate BETWEEN '09/12/2011' AND (SELECT MAX(yiclkind) FROM xfmClockHoursFactB)
  AND k.employeekey IS NOT NULL
--  AND employeenumber = '1140870'
--  AND empkeyfrom1 <> empkeyfrom2
--  AND employeenumber IN (
--    SELECT employeenumber
--    FROM edwEmployeeDim
--    GROUP BY storecode, employeenumber
--      HAVING COUNT(*) > 1)  
ORDER BY employeenumber, thedate   

  
SELECT MIN(yiclkind), MAX(yiclkind) FROM xfmClockHoursFactB
SELECT MIN(yiclkind), MAX(yiclkind) FROM tmpOvertime
-- first sunday of 2010
SELECT MIN(thedate)
FROM day
WHERE dayofweek = 1
AND monthofyear = 1
AND theyear = 2010


/********** 1/10/12 END *******************************************************/



/********** 1/11/12 BEGIN *****************************************************/
-- this looks LIKE it
-- check for integrity
-- AND it bombs
SELECT yico#, yiemp#, thedate
FROM (
SELECT d.thedate, fb.yico#, fb.yiemp#, fb.yicode, fb.vachours, fb.ptohours, fb.holhours, 
  fb.clockhours, ot.regularhours, ot.overtimehours, k.* --, emp.employeekey, emp.name
FROM day d
LEFT JOIN xfmClockHoursFactB fb ON d.thedate = fb.yiclkind
LEFT JOIN tmpOvertime ot ON d.thedate = ot.yiclkind
  AND fb.yiemp# = ot.yiemp#
LEFT JOIN (
    SELECT storecode, employeekey, employeenumber, name,
      coalesce(cast(RowFromTS AS sql_date), HireDate) AS EmpKeyFrom1,
      CASE
        WHEN RowFromTS IS NULL THEN HireDate
      ELSE
        cast(timestampadd(sql_tsi_day, 1, RowFromTS) AS sql_date)
      END AS EmpKeyFrom2,
      CASE
        WHEN (SELECT datetype FROM day WHERE datekey = e.termdatekey) <> 'NA' THEN termdate
      ELSE
        coalesce(CAST(RowThruTS AS sql_Date), (select thedate from day where datetype = 'NA')) --CAST('12/31/9999' AS sql_date))  
      END AS EmpKeyThru
    FROM edwEmployeeDim e) k 
  ON fb.yico# = k.storecode
  AND fb.yiemp# = k.employeenumber
  AND d.thedate BETWEEN k.EmpKeyFrom1 AND k.EmpKeyThru    
WHERE d.thedate BETWEEN '09/12/2011' AND (SELECT MAX(yiclkind) FROM xfmClockHoursFactB)
  AND k.employeekey IS NOT NULL) x
GROUP BY yico#, yiemp#, thedate
  HAVING COUNT(*) > 1
  
-- initially looks LIKE factb IS the problem -- it has separate rows for yicode
select *
FROM xfmClockHoursFactB
WHERE yiemp# = '110070'
AND yiclkind = '09/27/2011'

-- so use this instead of TABLE IN JOIN 
SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) asPTOHours, 
  SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
FROM xfmClockHoursFactB
GROUP BY yico#, yiemp#, yiclkind


-- ot looks ok
SELECT yico#, yiemp#, yiclkind
FROM tmpOvertime
GROUP BY yico#, yiemp#, yiclkind
  HAVING COUNT(*) > 1

-- 1 row per day/co#/emp# 
SELECT d.thedate, h.yico#, h.yiemp#, h.VacHours, h.PTOHours, h.HolHours, 
  h.ClockHours, o.RegularHours, o.OvertimeHours
FROM day d
LEFT JOIN (
  SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
    SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
  FROM xfmClockHoursFactB
  GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind 
LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind
  AND h.yico# = o.yico#
  AND h.yiemp# = o.yiemp#
WHERE d.thedate BETWEEN '01/01/2010' AND curdate() 

-- 1 row per day/co#/emp#
-- verified
SELECT thedate, yico#, yiemp#
FROM ( 
SELECT d.thedate, h.yico#, h.yiemp#, h.VacHours, h.PTOHours, h.HolHours, 
  h.ClockHours, o.RegularHours, o.OvertimeHours
FROM day d
LEFT JOIN (
  SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
    SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
  FROM xfmClockHoursFactB
  GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind 
LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind
  AND h.yico# = o.yico#
  AND h.yiemp# = o.yiemp#
WHERE d.thedate BETWEEN '01/01/2010' AND curdate()) x
GROUP BY thedate, yico#, yiemp#
  HAVING COUNT(*) > 1
-- ok, hours ADD up  
SELECT d.thedate, h.yico#, h.yiemp#, h.VacHours, h.PTOHours, h.HolHours, 
  h.ClockHours, o.RegularHours, o.OvertimeHours
FROM day d
LEFT JOIN (
  SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
    SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
  FROM xfmClockHoursFactB
  GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind 
LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind
  AND h.yico# = o.yico#
  AND h.yiemp# = o.yiemp#
WHERE d.thedate BETWEEN '01/01/2010' AND curdate() 
  AND round(ClockHours, 2) <> round(RegularHours + OvertimeHours, 2)  
  
-- ADD EmpDim
-- redo tmpOvertime back to first sunday IN august 2009
SELECT MIN(thedate) -- 8/2
FROM day
WHERE dayofweek = 1
AND monthofyear = 8
AND theyear = 2009

-- 1 row per empkey/date
SELECT d.thedate, h.yico#, k.storecode, h.yiemp#, k.name, k.employeenumber, k.employeekey,
  h.VacHours, h.PTOHours, h.HolHours, 
  h.ClockHours, o.RegularHours, o.OvertimeHours
FROM day d
LEFT JOIN (
  SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
    SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
  FROM xfmClockHoursFactB
  GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind 
LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind
  AND h.yico# = o.yico#
  AND h.yiemp# = o.yiemp#
LEFT JOIN (
    SELECT storecode, employeekey, employeenumber, name,
      coalesce(cast(RowFromTS AS sql_date), HireDate) AS EmpKeyFrom1,
      CASE
        WHEN RowFromTS IS NULL THEN HireDate
      ELSE
        cast(timestampadd(sql_tsi_day, 1, RowFromTS) AS sql_date)
      END AS EmpKeyFrom2,
      CASE
        WHEN (SELECT datetype FROM day WHERE datekey = e.termdatekey) <> 'NA' THEN termdate
      ELSE
        coalesce(CAST(RowThruTS AS sql_Date), (select thedate from day where datetype = 'NA')) --CAST('12/31/9999' AS sql_date))  
      END AS EmpKeyThru
    FROM edwEmployeeDim e) k 
  ON h.yico# = k.storecode
  AND h.yiemp# = k.employeenumber
  AND d.thedate BETWEEN k.EmpKeyFrom1 AND k.EmpKeyThru  
WHERE d.thedate BETWEEN '08/02/2009' AND curdate()  
  AND employeekey IS NOT NULL 
  
-- 1 row per empkey/date
-- verify  hooray
SELECT k.employeekey d.datekey, 
FROM day d
LEFT JOIN (
  SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
    SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
  FROM xfmClockHoursFactB
  GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind 
LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind
  AND h.yico# = o.yico#
  AND h.yiemp# = o.yiemp#
LEFT JOIN (
    SELECT storecode, employeekey, employeenumber, name,
      coalesce(cast(RowFromTS AS sql_date), HireDate) AS EmpKeyFrom1,
      CASE
        WHEN RowFromTS IS NULL THEN HireDate
      ELSE
        cast(timestampadd(sql_tsi_day, 1, RowFromTS) AS sql_date)
      END AS EmpKeyFrom2,
      CASE
        WHEN (SELECT datetype FROM day WHERE datekey = e.termdatekey) <> 'NA' THEN termdate
      ELSE
        coalesce(CAST(RowThruTS AS sql_Date), (select thedate from day where datetype = 'NA')) --CAST('12/31/9999' AS sql_date))  
      END AS EmpKeyThru
    FROM edwEmployeeDim e) k 
  ON h.yico# = k.storecode
  AND h.yiemp# = k.employeenumber
  AND d.thedate BETWEEN k.EmpKeyFrom1 AND k.EmpKeyThru  
WHERE d.thedate BETWEEN '08/02/2009' AND curdate()  
  AND employeekey IS NOT NULL   
GROUP BY d.thedate,  k.employeekey
  HAVING COUNT(*) > 1  
  
  

-- just about there, decide about intervals
-- scrape IS done after midnight, 
-- therefore any changes actually happened the day before
-- change FROM AND thru to MINUS 1 day
-- this makes for valid BETWEEN usage
-- *** save this one, good test bed ***
SELECT *
FROM (
SELECT storecode, employeekey, employeenumber, name, hiredate, termdate, rowchangedate,
  coalesce(cast(RowFromTS AS sql_date), HireDate) AS EmpKeyFrom1,
--  CASE
--    WHEN RowFromTS IS NULL THEN HireDate
--  ELSE
--    cast(timestampadd(sql_tsi_day, -1, RowFromTS) AS sql_date)
--  END AS EmpKeyFrom2,
  CASE
    WHEN (SELECT datetype FROM day WHERE datekey = e.termdatekey) <> 'NA' THEN termdate
  ELSE
    CASE
      WHEN RowThruTS IS NULL THEN (select thedate from day where datetype = 'NA')
    ELSE 
      CAST(timestampadd(sql_tsi_day, -1, RowThruTS) AS sql_date)
-- new row goes INTO effect the day after the change took place, ie, rowchangedate
-- the row actually changed ON that date but semantically the new row takes 
-- it's LIKE a fucking optical illusion, shifting AS a shift my gaze
-- updated row IS valid thru ts - 1
-- new row IS valid FROM ts
    END 
  END AS EmpKeyThru
FROM edwEmployeeDim e
--WHERE RowThruTS IS NOT NULL
WHERE employeenumber IN ('16250','148075','171210','1140870','1145560','268200','291785')) X
ORDER BY employeenumber  






SELECT *
FROM edwEmployeeDim 
WHERE rowchangedate IS NOT NULL 

Error 5147:  Constraint violation while updating column "RegularHours".  The column cannot be updated to a NULL value due to a 'not NULL' 
-- i could just coalesce the value FROM x, but first i want to know why nulls are being returned
-- INNER JOIN to tmpOT: only care about those rows with clockhours
-- 
UPDATE edwClockHoursFact
  SET ClockHours = 0,
      RegularHours = 0,
      OvertimeHours = 0,
      VacationHours = 0,
      PTOHours = 0,
      HolidayHours = 0

UPDATE edwClockHoursFact
  SET ClockHours = x.Clockhours,
      RegularHours = x.RegularHours,
      OvertimeHours = x.OvertimeHours,
      VacationHours = x.VacHours,
      PTOHours = x.PtoHours,
      HolidayHours = x.HolHours
FROM (
  SELECT d.datekey, k.employeekey,
    h.VacHours, h.PTOHours, h.HolHours, 
    h.ClockHours, o.RegularHours, o.OvertimeHours
  FROM day d
  LEFT JOIN (
    SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
      SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
    FROM xfmClockHoursFactB
    GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind 
  INNER JOIN tmpOvertime o ON h.yiclkind = o.yiclkind
    AND h.yico# = o.yico#
    AND h.yiemp# = o.yiemp#
  LEFT JOIN (
    SELECT storecode, employeekey, employeenumber, 
      coalesce(cast(RowFromTS AS sql_date), HireDate) AS EmpKeyFrom,
      CASE
        WHEN (SELECT datetype FROM day WHERE datekey = e.termdatekey) <> 'NA' THEN termdate
      ELSE
        CASE
          WHEN RowThruTS IS NULL THEN (select thedate from day where datetype = 'NA')
        ELSE 
          CAST(timestampadd(sql_tsi_day, -1, RowThruTS) AS sql_date)
        END 
      END AS EmpKeyThru
    FROM edwEmployeeDim e) k 
    ON h.yico# = k.storecode
    AND h.yiemp# = k.employeenumber
    AND d.thedate BETWEEN k.EmpKeyFrom AND k.EmpKeyThru  
  WHERE d.thedate BETWEEN '08/02/2009' AND curdate() -- first sunday IN 8/09 
    AND employeekey IS NOT NULL ) X    
WHERE edwClockHoursFact.employeekey = x.employeekey
  AND edwClockHoursFact.datekey = x.datekey    

/********** 1/11/12 END *****************************************************/
      
/******** sidetrack clockhours past term ********************/

SELECT f.*, e.employeenumber, e.name, e.termdate, e.currentrow
FROM xfmClockHoursFactB f
LEFT JOIN edwEmployeeDim e ON f.yiemp# = e.employeenumber
  AND f.yico# = e.storecode
WHERE e.termdatekey <> 7306
  AND f.yiclkind > e.termdate
-- spreadsheet for maryann  
SELECT f.yico# AS Store, f.yiemp# AS [Emp#], e.name AS Name, e.termdate AS [Term Date],
  sum(ClockHours) AS [Hours Clocked post Term], COUNT(*) AS [Days with time post Term],
  MAX(f.yiclkind) AS [Latest Clock Date]
FROM xfmClockHoursFactB f
LEFT JOIN edwEmployeeDim e ON f.yiemp# = e.employeenumber
  AND f.yico# = e.storecode
WHERE e.termdatekey <> 7306
  AND f.yiclkind > e.termdate
GROUP BY f.yico#, f.yiemp#, e.name, e.termdate  

-- beaucoup that have clock hours ON term date
-- what this IS saying IS, period of employment extends thru term date
SELECT f.*, e.employeenumber, e.name, e.termdate, e.currentrow
FROM xfmClockHoursFactB f
LEFT JOIN edwEmployeeDim e ON f.yiemp# = e.employeenumber
  AND f.yico# = e.storecode
WHERE e.termdatekey <> 7306
  AND f.yiclkind = e.termdate
  
/******** END sidetrack clockhours past term ********************/  
  

    
/**********************
going through this with the large initial scrape
nightly will be 4 sundays back, DELETE & INSERT, no updates
-- per greg, 4 weeks (actually 3 IS enuf), they can change anything they want older
-- than that, but payroll IS done  
4 sundays ago: select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()  
step 1
**********************/
CREATE TABLE xfmClockHoursFactA ( 
      [yico#] CIChar( 3 ),
      [yiemp#] CIChar( 7 ),
      yiclkind Date,
      yicode CIChar( 3 ),
      clockInTS TimeStamp,
      clockOutTS TimeStamp) IN DATABASE;
  
CREATE TABLE xfmClockHoursFactB ( 
      [yico#] CIChar( 3 ),
      [yiemp#] CIChar( 7 ),
      yiclkind Date,
      yicode CIChar( 3 ),
      VacHours double,
      PTOHours double,
      HolHours double,
      ClockHours double) IN DATABASE;  
      
CREATE TABLE edwClockHoursFact (
  EmployeeKey integer,
  DateKey integer,
  RegularHours double,
  OvertimeHours double,
  VacationHours double,
  PTOHours double,
  HolidayHours double) IN database; 
  
-- one row for each day/active employee  
INSERT INTO edwClockHoursFact
SELECT e.EmployeeKey, d.datekey, 0,0,0,0,0
FROM day d
LEFT JOIN edwEmployeeDim e ON d.thedate BETWEEN e.hiredate AND e.termdate
WHERE d.thedate BETWEEN '08/01/2009' AND curdate()    

INSERT INTO xfmClockHoursFacta 
-- DELETE FROM xfmClockHoursFacta
-- 1 row per emp/code/clockints
SELECT yico#, yiemp#, yiclkind, yicode,
  timestampadd(sql_tsi_second, second(yiclkint), timestampadd(
    sql_tsi_minute, minute(yiclkint), timestampadd(
    sql_tsi_hour, hour(yiclkint), cast(yiclkind as sql_timestamp)))) AS ClockInTS,
  timestampadd(sql_tsi_second, second(yiclkoutt), timestampadd(
    sql_tsi_minute, minute(yiclkoutt), timestampadd(
    sql_tsi_hour, hour(yiclkoutt), cast(yiclkoutd as sql_timestamp)))) AS ClockOutTS
FROM stgArkonaPYPCLOCKIN p
-- verify
SELECT yico#, yiemp#, ClockInTS, yicode
FROM xfmClockHoursFacta
GROUP BY yico#, yiemp#, ClockInTS, yicode
  HAVING COUNT(*) > 1
  


INSERT INTO xfmClockHoursFactB
-- DELETE FROM xfmClockHoursFactB
-- one row per co/emp/code/yiclkind
SELECT yico#, yiemp#, yiclkind, --, yicode,
  coalesce(CASE WHEN yicode = 'VAC' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) as VacHours,
  coalesce(CASE WHEN yicode = 'PTO' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS PTOHours,
  coalesce(CASE WHEN yicode = 'HOL' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS HolHours,
  coalesce(CASE WHEN yicode = 'O' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS ClockHours
FROM xfmClockHoursFacta 
WHERE yicode <> 'I' 
GROUP BY yico#, yiemp#, yiclkind, yicode
-- verify
SELECT yico#, yiemp#, yiclkind, yicode
FROM xfmClockHoursFactB
GROUP BY yico#, yiemp#, yiclkind, yicode
  HAVING COUNT(*) > 1
  
-- combine factA & factB INTO one query
SELECT yico#, yiemp#, yiclkind, yicode,
  coalesce(CASE WHEN yicode = 'VAC' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) as VacHours,
  coalesce(CASE WHEN yicode = 'PTO' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS PTOHours,
  coalesce(CASE WHEN yicode = 'HOL' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS HolHours,
  coalesce(CASE WHEN yicode = 'O' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS ClockHours
FROM (    
  SELECT yico#, yiemp#, yiclkind, yicode,
    timestampadd(sql_tsi_second, second(yiclkint), timestampadd(
      sql_tsi_minute, minute(yiclkint), timestampadd(
      sql_tsi_hour, hour(yiclkint), cast(yiclkind as sql_timestamp)))) AS ClockInTS,
    timestampadd(sql_tsi_second, second(yiclkoutt), timestampadd(
      sql_tsi_minute, minute(yiclkoutt), timestampadd(
      sql_tsi_hour, hour(yiclkoutt), cast(yiclkoutd as sql_timestamp)))) AS ClockOutTS
--  FROM stgArkonaPYPCLOCKIN p) x
  FROM tmpPYPCLOCKIN p) x  
WHERE yicode <> 'I' 
GROUP BY yico#, yiemp#, yiclkind, yicode  
-- 1 month: 1 min 22 sec
-- 1 month: 17 sec
-- 4 months 1 MIN
-- 2 years 21 minutes
-- temp TABLE because query takes so fucking long

CREATE TABLE tmpOvertime (
  yico# cichar(3),
  yiemp# cichar(7),
  SunToSatWeek integer, 
  yiclkind date,
  RegularHours double,
  OverTimeHours double) IN database;
  
INSERT INTO  tmpOvertime 
-- DELETE FROM tmpOvertime
-- 1 row per co/emp/yiclkind
SELECT a.yico#, a.yiemp#, w.SundayToSaturdayWeek, a.yiclkind,
  round(
    case 
      when coalesce(sum(b.ClockHours), 0) + a.ClockHours < 40 then a.ClockHours
      when coalesce(sum(b.ClockHours), 0) > 40 then 0
      else 40 - coalesce(sum(b.ClockHours), 0) 
    END, 2) as RegularHours,
  round(
    case 
      when coalesce(sum(b.ClockHours), 0) >= 40 then a.ClockHours
      when coalesce(sum(b.ClockHours), 0) + a.ClockHours < 40 then 0
      else a.ClockHours - (40 - coalesce(sum(b.ClockHours), 0)) 
    END, 2) as OverTimeHours  
-- SELECT *    
FROM (-- weeks
  SELECT MIN(theDate) AS fromDate, MAX(theDate) AS thruDate, SundayToSaturdayWeek  
  FROM day d
--  WHERE d.thedate between '12/26/2011' AND '12/31/2011' -- smaller subset to WORK with
--  WHERE d.Thedate BETWEEN ( -- why go back to before 08/2009?
--      SELECT MIN(yiclkind)
--      FROM xfmClockHoursFactB) 
--      AND (
--      SELECT MAX(yiclkind)
--      FROM xfmClockHoursFactB)  
-- first sunday IN 8/2009 
  WHERE d.Thedate BETWEEN '08/02/2009' AND curdate()-- ( 
--      SELECT MAX(yiclkind)
--      FROM xfmClockHoursFactB)
  GROUP BY SundayToSaturdayWeek) w      
LEFT JOIN ( -- a
  SELECT *
  FROM xfmClockHoursFactB 
  WHERE yicode = 'O'
    AND yiclkind BETWEEN '08/02/2009' AND curdate()) a ON a.yiclkind BETWEEN w.fromDate AND w.thruDate
LEFT JOIN ( -- b
    SELECT *
    FROM xfmClockHoursFactB 
    WHERE yicode = 'O'
      AND yiclkind BETWEEN '08/02/2009' AND curdate()) b ON b.yiclkind BETWEEN w.fromDate AND w.thruDate
  AND a.yico# = b.yico#
  AND a.yiemp# = b.yiemp#
  AND b.yiclkind < a.yiclkind -- this IS the magic line
GROUP BY w.SundayToSaturdayWeek, a.yico#, a.yiemp#, a.yiclkind, a.ClockHours   
-- verify
SELECT yico#, yiemp#, yiclkind
FROM tmpOvertime 
GROUP BY yico#, yiemp#, yiclkind
  HAVING COUNT(*) > 1

/*  
UPDATE edwClockHoursFact
  SET ClockHours = 0,
      RegularHours = 0,
      OvertimeHours = 0,
      VacationHours = 0,
      PTOHours = 0,
      HolidayHours = 0
*/
UPDATE edwClockHoursFact
  SET ClockHours = x.Clockhours,
      RegularHours = x.RegularHours,
      OvertimeHours = x.OvertimeHours,
      VacationHours = x.VacHours,
      PTOHours = x.PtoHours,
      HolidayHours = x.HolHours
FROM (
  SELECT d.datekey, k.employeekey,
    h.VacHours, h.PTOHours, h.HolHours, 
    h.ClockHours, o.RegularHours, o.OvertimeHours
  FROM day d
  LEFT JOIN (
    SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
      SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
    FROM xfmClockHoursFactB
    GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind 
  INNER JOIN tmpOvertime o ON h.yiclkind = o.yiclkind
    AND h.yico# = o.yico#
    AND h.yiemp# = o.yiemp#
  LEFT JOIN (
    SELECT storecode, employeekey, employeenumber, 
      coalesce(cast(RowFromTS AS sql_date), HireDate) AS EmpKeyFrom,
      CASE
        WHEN (SELECT datetype FROM day WHERE datekey = e.termdatekey) <> 'NA' THEN termdate
      ELSE
        CASE
          WHEN RowThruTS IS NULL THEN (select thedate from day where datetype = 'NA')
        ELSE 
          CAST(timestampadd(sql_tsi_day, -1, RowThruTS) AS sql_date)
        END 
      END AS EmpKeyThru
    FROM edwEmployeeDim e) k 
    ON h.yico# = k.storecode
    AND h.yiemp# = k.employeenumber
    AND d.thedate BETWEEN k.EmpKeyFrom AND k.EmpKeyThru  
  WHERE d.thedate BETWEEN '08/02/2009' AND curdate() -- first sunday IN 8/2009 
    AND employeekey IS NOT NULL ) X    
WHERE edwClockHoursFact.employeekey = x.employeekey
  AND edwClockHoursFact.datekey = x.datekey     
  
-- testing  
SELECT SUM(clockhours), SUM(regularhours), SUM(overtimehours)
FROM edwClockHoursFact c
inner JOIN edwEmployeeDim e ON c.employeekey = e.employeekey
INNER JOIN day d ON c.datekey = d.datekey
WHERE e.employeenumber = '1148065'
  AND d.thedate BETWEEN '04/09/2011' AND '04/23/2011'  
  
SELECT * 
FROM edwClockHoursFact
ORDER BY vacationhours desc 

SELECT * FROM day WHERE datekey = 2830


/********** 1/31/12 so what does nightly look LIKE ? *********************/

-- currently have edwClockHoursFact records thru 1/4/12
SELECT d.TheDate, e.Name, ch.*
FROM edwClockHoursFact ch
LEFT JOIN day d ON ch.datekey = d.datekey
LEFT JOIN edwEmployeeDim e ON ch.employeekey = e.employeekey
ORDER BY d.TheDate DESC

-- 4 weeks ago last sunday will cover (1/1/12)
select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()


-- combine factA & factB INTO one query
-- this looks LIKE the place to start, replace xfmClockHoursFactA/B with this query
-- one row per co/emp/day/code
EXECUTE PROCEDURE sp_ZapTable('tmpClockHours');
INSERT INTO tmpClockHours
SELECT yico#, yiemp#, yiclkind, yicode,
  coalesce(CASE WHEN yicode = 'VAC' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) as VacHours,
  coalesce(CASE WHEN yicode = 'PTO' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS PTOHours,
  coalesce(CASE WHEN yicode = 'HOL' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS HolHours,
  coalesce(CASE WHEN yicode = 'O' THEN SUM(round(timestampdiff(sql_tsi_second, clockinTS, clockoutTS)/3600.0, 2)) END, 0) AS ClockHours
FROM ( -- x  
  SELECT yico#, yiemp#, yiclkind, yicode,
    timestampadd(sql_tsi_second, second(yiclkint), timestampadd(
      sql_tsi_minute, minute(yiclkint), timestampadd(
      sql_tsi_hour, hour(yiclkint), cast(yiclkind as sql_timestamp)))) AS ClockInTS,
    timestampadd(sql_tsi_second, second(yiclkoutt), timestampadd(
      sql_tsi_minute, minute(yiclkoutt), timestampadd(
      sql_tsi_hour, hour(yiclkoutt), cast(yiclkoutd as sql_timestamp)))) AS ClockOutTS
  FROM tmpPYPCLOCKIN p) x  
WHERE yicode <> 'I' 
GROUP BY yico#, yiemp#, yiclkind, yicode 

-- verify grain

SELECT yico#, yiemp#, yiclkind, yicode
FROM tmpClockHours
GROUP BY yico#, yiemp#, yiclkind, yicode
  HAVING COUNT(*) > 1
  
SELECT *
FROM tmpClockHours
WHERE yico# = 'RY1'
  AND yiemp# = '1110425'
  AND yiclkind = '1/3/2012'
    
SELECT yico#, yiemp#, yiclkind
FROM (
SELECT yico#, yiemp#, yiclkind
FROM tmpClockHours
GROUP BY yico#, yiemp#, yiclkind) x 
GROUP BY yico#, yiemp#, yiclkind
  HAVING COUNT(*) > 1
  
-- Overtime  
--INSERT INTO  tmpOvertime 
-- DELETE FROM tmpOvertime
-- 1 row per co/emp/yiclkind
EXECUTE PROCEDURE sp_ZapTable('tmpOvertime');
INSERT INTO tmpOvertime
SELECT a.yico#, a.yiemp#, w.SundayToSaturdayWeek, a.yiclkind,
  round(
    case 
      when coalesce(sum(b.ClockHours), 0) + a.ClockHours < 40 then a.ClockHours
      when coalesce(sum(b.ClockHours), 0) > 40 then 0
      else 40 - coalesce(sum(b.ClockHours), 0) 
    END, 2) as RegularHours,
  round(
    case 
      when coalesce(sum(b.ClockHours), 0) >= 40 then a.ClockHours
      when coalesce(sum(b.ClockHours), 0) + a.ClockHours < 40 then 0
      else a.ClockHours - (40 - coalesce(sum(b.ClockHours), 0)) 
    END, 2) as OverTimeHours  
-- SELECT *    
FROM (-- weeks
  SELECT MIN(theDate) AS fromDate, MAX(theDate) AS thruDate, SundayToSaturdayWeek  
  FROM day d
  WHERE d.Thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate()-- ( 
  GROUP BY SundayToSaturdayWeek) w      
LEFT JOIN ( -- a
  SELECT *
  FROM tmpClockHours
  WHERE yicode = 'O') a ON a.yiclkind BETWEEN w.fromDate AND w.thruDate
LEFT JOIN ( -- b
    SELECT *
    FROM tmpClockHours
    WHERE yicode = 'O') b ON b.yiclkind BETWEEN w.fromDate AND w.thruDate
  AND a.yico# = b.yico#
  AND a.yiemp# = b.yiemp#
  AND b.yiclkind < a.yiclkind -- this IS the magic line
WHERE a.yiemp# IS NOT NULL   
GROUP BY w.SundayToSaturdayWeek, a.yico#, a.yiemp#, a.yiclkind, a.ClockHours  

 
-- verify grain
SELECT yico#, yiemp#, yiclkind
FROM tmpOvertime 
GROUP BY yico#, yiemp#, yiclkind
  HAVING COUNT(*) > 1  
  

-- that's much better
SELECT d.thedate, d.dayname, e.employeenumber, 
  (SELECT employeekey FROM edwEmployeeDim WHERE employeenumber = e.employeenumber AND d.thedate BETWEEN employeekeyfromdate AND employeekeythrudate) AS EmpKey,
  coalesce(h.clockhours, 0) AS clockhours,
  coalesce(h.holhours, 0) AS holhours, coalesce(h.vachours, 0) AS vachours, 
  coalesce(h.ptohours, 0) AS ptohours, 
  coalesce(o.regularhours, 0) AS RegHours, coalesce(o.OvertimeHours, 0) AS OTHours
-- DROP TABLE #jon  
-- into #jon  
FROM day d -- one row per day going back 4 weeks FROM last sunday thru today
LEFT JOIN ( -- one row per store/emp#/hiredate/termdate
  SELECT storecode, employeenumber, hiredate, termdate
  FROM edwEmployeeDim
  GROUP BY storecode, employeenumber, hiredate, termdate) e ON d.thedate BETWEEN e.hiredate AND e.termdate
LEFT JOIN ( -- one row per co/emp#/clockindate
    SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
      SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
    FROM tmpClockHours
    GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind   
  AND e.employeenumber = h.yiemp#    
LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind -- one row per emp#/clockindate
  AND h.yico# = o.yico#
  AND h.yiemp# = o.yiemp#  
WHERE d.thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate()

-- AND the real deal

DELETE -- 1548
--SELECT *
FROM edwClockHoursFact
WHERE datekey IN (
  SELECT datekey
  FROM day
  WHERE thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate());

INSERT INTO edwClockHoursFact -- 11973
SELECT 
  (SELECT employeekey FROM edwEmployeeDim WHERE employeenumber = e.employeenumber AND d.thedate BETWEEN employeekeyfromdate AND employeekeythrudate) AS EmployeeKey,
  d.datekey, 
  coalesce(h.clockhours, 0) AS ClockHours,
  coalesce(o.regularhours, 0) AS RegularHours,
  coalesce(o.OvertimeHours, 0) AS OvertimeHours, 
  coalesce(h.vachours, 0) AS VacationHours,
  coalesce(h.ptohours, 0) AS PTOHours,
  coalesce(h.holhours, 0) AS HolidayHours 
FROM day d -- one row per day going back 4 weeks FROM last sunday thru today
LEFT JOIN ( -- one row per store/emp#/hiredate/termdate
  SELECT storecode, employeenumber, hiredate, termdate
  FROM edwEmployeeDim
  GROUP BY storecode, employeenumber, hiredate, termdate) e ON d.thedate BETWEEN e.hiredate AND e.termdate
LEFT JOIN ( -- one row per co/emp#/clockindate
    SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
      SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
    FROM tmpClockHours
    GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind   
  AND e.employeenumber = h.yiemp#    
LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind -- one row per emp#/clockindate
  AND h.yico# = o.yico#
  AND h.yiemp# = o.yiemp#  
WHERE d.thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate();

-- verify grain
-- no dups
SELECT datekey, employeekey
FROM edwClockHoursFact
GROUP BY datekey, employeekey
  HAVING COUNT(*) > 1
  
-- verify against some paychecks 
-- yay looks LIKE it works
SELECT employeekey, SUM(clockHours) AS ClkHrs, SUM(HolidayHours) AS HolHrs,
  SUM(VacationHours) AS VacHrs, SUM(ptohours) AS PTOHrs, SUM(RegularHours) AS RegHrs, 
  SUM(OvertimeHours) AS OTHrs
-- SELECT *  
FROM edwClockHoursFact
WHERE employeekey in (SELECT employeekey FROM edwEmployeeDim WHERE employeenumber =  '11605')
  AND datekey BETWEEN (select datekey from day where thedate = '12/04/2011')
      AND (select datekey from day where thedate = '12/17/2011')
GROUP BY employeekey

SELECT employeekey, SUM(clockHours) AS ClkHrs, SUM(HolidayHours) AS HolHrs,
  SUM(VacationHours) AS VacHrs, SUM(ptohours) AS PTOHrs, SUM(RegularHours) AS RegHrs, 
  SUM(OvertimeHours) AS OTHrs
-- SELECT *  
FROM edwClockHoursFact f
INNER JOIN
GROUP BY employeekey

SELECT *
FROM (
  SELECT SundayToSaturdayWeek, MIN(thedate), max(thedate)
  FROM day 
  GROUP BY SundayToSaturdayWeek) d
  

/*** sidetrack employees with 0 clock hours ******/ 
SELECT *
FROM edwEmployeeDim e
INNER JOIN (
SELECT employeekey
FROM edwClockHoursFact 
GROUP BY employeekey
  HAVING sum(clockhours) = 0) x ON e.employeekey = x.employeekey 
  
-- why no clockhours for 11725 M.Ahmed, hired 12/5/11?  dept = 03 dist = PTEC
-- hmm, none IN arkona either, does pdq NOT clockin:
-- per Ned, this guy worked LIKE a day AND a half AND split, never came back,
-- so the time he worked was before he was IN the system
-- talk to Maryann

SELECT x.*, e.*
FROM edwEmployeeDim e
INNER JOIN (
  SELECT employeekey, sum(clockhours)
  FROM edwClockHoursFact 
  GROUP BY employeekey) x ON e.employeekey = x.employeekey 
WHERE e.distcode = 'PTEC'  

/*** sidetrack employees with 0 clock hours ******/


/********** END 1/31/12 so what does nightly look LIKE ? *********************/



/****** 2/19 failing ***********************************************************/

select *
FROM (
  SELECT 
    (SELECT employeekey FROM edwEmployeeDim WHERE employeenumber = e.employeenumber AND d.thedate BETWEEN employeekeyfromdate AND employeekeythrudate) AS EmployeeKey,
    d.datekey, d.thedate,
    coalesce(h.clockhours, 0) AS ClockHours,
    coalesce(o.regularhours, 0) AS RegularHours,
    coalesce(o.OvertimeHours, 0) AS OvertimeHours, 
    coalesce(h.vachours, 0) AS VacationHours,
    coalesce(h.ptohours, 0) AS PTOHours,
    coalesce(h.holhours, 0) AS HolidayHours 
  FROM day d -- one row per day going back 4 weeks FROM last sunday thru today
  LEFT JOIN ( -- one row per store/emp#/hiredate/termdate
  -- this IS WHERE it IS failing with mult rows 
  -- type 2 changes will have the same hire/term dates, nope that's NOT the issue
  -- issue IS emp# 268200, hired 11/8/11, type 2 term 2/3/12, the date range IS 1/22 - curdate, 
    SELECT storecode, employeenumber, hiredate, termdate
    FROM edwEmployeeDim
    GROUP BY storecode, employeenumber, hiredate, termdate) e ON d.thedate BETWEEN e.hiredate AND e.termdate
  LEFT JOIN ( -- one row per co/emp#/clockindate
      SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
        SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
      FROM tmpClockHours
      GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind   
    AND e.employeenumber = h.yiemp#    
  LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind -- one row per emp#/clockindate
    AND h.yico# = o.yico#
    AND h.yiemp# = o.yiemp#  
  WHERE d.thedate BETWEEN (select max(thedate) - 35 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate()) x
WHERE employeekey = 1383
AND datekey BETWEEN 2930 AND 2956

/********* 2/20/12 the fix *****************************************************/
-- there abosfuckinglutely can be multiple rows for store/emp#/hiredate/termdate
-- type2 changes, none of those attributes change
-- but, there should be no dup rows for store/emp#/empkeyfrom/empkeythru 
-- fuck , but i feel LIKE i'm missing the issue
-- although maybe NOT, empkeyfrom/thru will NOT overlap

-- so it's NOT the issue of tmpClockHours needing to to be at the grain of empkey
-- the issue was joining day to edwEmployeeDim based ON hiredate-termdate interval
-- needs to be the empkeyfrom-empkeythru interval
select employeekey, datekey
FROM (
  SELECT e.EmployeeKey,
    d.datekey, 
    coalesce(h.clockhours, 0) AS ClockHours,
    coalesce(o.regularhours, 0) AS RegularHours,
    coalesce(o.OvertimeHours, 0) AS OvertimeHours, 
    coalesce(h.vachours, 0) AS VacationHours,
    coalesce(h.ptohours, 0) AS PTOHours,
    coalesce(h.holhours, 0) AS HolidayHours 
  FROM day d -- one row per day going back 4 weeks FROM last sunday thru today
--  LEFT JOIN ( -- one row per store/emp#/hiredate/termdate
  -- this IS WHERE it IS failing with mult rows 
  -- type 2 changes will have the same hire/term dates, nope that's NOT the issue
  -- issue IS emp# 268200, hired 11/8/11, type 2 term 2/3/12, the date range IS 1/22 - curdate, 
--    SELECT storecode, employeenumber, hiredate, termdate
--    FROM edwEmployeeDim
  LEFT JOIN -- 1 row per empKey/day, should also be emp#/day, granularity of empDim IS day! there can NOT be multiple records for an emp# ON any given day
    edwEmployeeDim e ON d.thedate BETWEEN e.EmployeekeyFromDate AND e.EmployeeKeyThruDate
--    GROUP BY storecode, employeenumber, hiredate, termdate) e ON d.thedate BETWEEN e.hiredate AND e.termdate
  LEFT JOIN ( -- one row per co/emp#/clockindate
      SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
        SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
      FROM tmpClockHours
      GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind   
    AND e.employeenumber = h.yiemp#    
  LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind -- one row per emp#/clockindate
    AND h.yico# = o.yico#
    AND h.yiemp# = o.yiemp#  
  WHERE d.thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate()) x
GROUP BY employeekey, datekey
HAVING COUNT(*) > 1

-- 1 row per empKey/day, should also be emp#/day, granularity of empDim IS day! there can NOT be multiple records for an emp# ON any given day
-- test it

SELECT datekey, employeenumber
FROM (
SELECT *
FROM day d 
LEFT JOIN edwEmployeeDim e ON d.thedate BETWEEN e.employeekeyfromdate AND e.employeekeythrudate
WHERE thedate BETWEEN curdate() - 700 AND curdate()) x
GROUP BY datekey, employeenumber
HAVING COUNT(*) > 1


/********* 2/20/12 the fix *****************************************************/



  
/******************************************************************************/
--START           the clockin before hired issue 2/19 - 2/20
/******************************************************************************/  
    
-- fuck, first thing ry1 emp# 17130 1/17/12 NULL employeekey

SELECT * FROM edwEmployeeDim WHERE employeenumber = '17130'  

-- this seemed to WORK ON local
SELECT storecode, employeekey, employeenumber, activecodeDesc, name, hiredate, termdate,
  employeekeyfromdate, employeekeythrudate, firstclockin, 
  timestampdiff(sql_tsi_Day, firstclockin, employeekeyfromdate)
FROM edwEmployeeDim e
inner JOIN (
    SELECT yico#, yiemp#, MIN(yiclkind) AS FirstClockIn
    FROM stgarkonaPYPCLOCKIN
    GROUP BY yico#, yiemp#) x ON e.storecode = x.yico#
  AND e.employeenumber = x.yiemp#
WHERE x.FirstClockIN < e.HireDate   
  AND employeenumber NOT IN (
    SELECT employeenumber
    FROM edwEmployeeDim
    GROUP BY employeenumber
    HAVING COUNT(*) > 1)
  AND timestampdiff(sql_tsi_Day, firstclockin, employeekeyfromdate) < 30  
ORDER BY employeenumber   

-- AND here are the rest
SELECT storecode, employeekey, employeenumber, activecodeDesc, name, hiredate, termdate,
  employeekeyfromdate, employeekeythrudate, firstclockin, 
  timestampdiff(sql_tsi_Day, firstclockin, employeekeyfromdate)
FROM edwEmployeeDim e
inner JOIN (
    SELECT yico#, yiemp#, MIN(yiclkind) AS FirstClockIn
    FROM stgarkonaPYPCLOCKIN
    GROUP BY yico#, yiemp#) x ON e.storecode = x.yico#
  AND e.employeenumber = x.yiemp#
WHERE x.FirstClockIN < e.HireDate   
ORDER BY employeenumber  

-- some were clocked IN 1/11/2011 for no apparent reason
    SELECT yico#, yiemp#, MIN(yiclkind) AS FirstClockIn
    FROM stgarkonaPYPCLOCKIN
    GROUP BY yico#, yiemp#     
SELECT * FROM edwEmployeeDim WHERE employeekeyfromdate = '12/31/9999'  
select * FROM edwEmployeeDim WHERE employeenumber = '1102090'   

SELECT *
FROM edwEmployeeDim
WHERE employeenumber = '1102090'



/****** 2/19 failing ***********************************************************/    

/****** 2/20 failing cont ******************************************************/  
/*
primary issue: clock hours prior to hire date
clockhours prior to employeeKeyFromDate   
some are easy to figure, just a few days
the hard ones are the employees with multiple tenures
HAVING a hard time coming up with a way to DO this IN code
looking at changing:  
  hiredate
  employeekeyfromdate
  clockin data (1/11/2011'ers)
*/  
SELECT storecode, employeekey, employeenumber, activecodeDesc, name, hiredate, termdate,
  employeekeyfromdate, employeekeythrudate, firstclockin, 
  timestampdiff(sql_tsi_Day, firstclockin, employeekeyfromdate)
FROM edwEmployeeDim e
inner JOIN (
    SELECT yico#, yiemp#, MIN(yiclkind) AS FirstClockIn
    FROM stgarkonaPYPCLOCKIN
    GROUP BY yico#, yiemp#) x ON e.storecode = x.yico#
  AND e.employeenumber = x.yiemp#
WHERE x.FirstClockIN < e.HireDate   
ORDER BY employeenumber
    
    
-- include stgArkonaPYMAST
SELECT  ymco#, ymempn
FROM stgArkonaPYMAST
GROUP BY ymco#, ymempn
HAVING COUNT(*) > 1

SELECT storecode, employeekey, employeenumber, activecodeDesc, name, hiredate, termdate,
  employeekeyfromdate, employeekeythrudate, firstclockin, 
  timestampdiff(sql_tsi_Day, firstclockin, employeekeyfromdate),
  ymhdte, ymhdto
FROM edwEmployeeDim e
inner JOIN (
    SELECT yico#, yiemp#, MIN(yiclkind) AS FirstClockIn
    FROM stgarkonaPYPCLOCKIN
    GROUP BY yico#, yiemp#) x ON e.storecode = x.yico#
  AND e.employeenumber = x.yiemp#
LEFT JOIN stgArkonaPYMAST p ON e.storecode = p.ymco#
  AND e.employeenumber = p.ymempn  
WHERE x.FirstClockIN < e.HireDate   
ORDER BY employeenumber
  
  
SELECT storecode, employeekey, e.employeenumber, activecodeDesc, name, hiredate, termdate,
  employeekeyfromdate, employeekeythrudate, firstclockin, 
  timestampdiff(sql_tsi_Day, firstclockin, employeekeyfromdate),
  ymhdte, ymhdto, howmanyEmpKeys
FROM edwEmployeeDim e
inner JOIN (
    SELECT yico#, yiemp#, MIN(yiclkind) AS FirstClockIn
    FROM stgarkonaPYPCLOCKIN
    GROUP BY yico#, yiemp#) x ON e.storecode = x.yico#
  AND e.employeenumber = x.yiemp#
LEFT JOIN stgArkonaPYMAST p ON e.storecode = p.ymco#
  AND e.employeenumber = p.ymempn  
LEFT JOIN ( 
  SELECT employeenumber, COUNT(*) AS howmanyEmpKeys
  FROM edwEmployeeDim
  GROUP BY employeenumber) h ON e.employeenumber = h.employeenumber
WHERE x.FirstClockIN < e.HireDate   
ORDER BY e.employeenumber    


hmmm, so IF hmhdte <> hmhdto, use clock data to determine the periods of employment

DROP TABLE #wtf;  
SELECT storecode, employeekey, e.employeenumber, activecodeDesc, name, hiredate, termdate,
  employeekeyfromdate, employeekeythrudate, firstclockin, 
  timestampdiff(sql_tsi_Day, firstclockin, employeekeyfromdate),
  ymhdte, ymhdto, howmanyEmpKeys  
INTO #wtf  
FROM edwEmployeeDim e
INNER JOIN ( -- only those employees with clock data
    SELECT yico#, yiemp#, MIN(yiclkind) AS FirstClockIn
    FROM stgarkonaPYPCLOCKIN
    GROUP BY yico#, yiemp#) x ON e.storecode = x.yico#
  AND e.employeenumber = x.yiemp#
LEFT JOIN stgArkonaPYMAST p ON e.storecode = p.ymco#
  AND e.employeenumber = p.ymempn  
LEFT JOIN ( 
  SELECT employeenumber, COUNT(*) AS howmanyEmpKeys
  FROM edwEmployeeDim
  GROUP BY employeenumber) h ON e.employeenumber = h.employeenumber
WHERE x.FirstClockIN < e.HireDate   
ORDER BY e.employeenumber 

/* 1 */
-- 2/20/12 done ON DDS
-- the 1/11/11 exceptions
SELECT *
FROM #wtf
WHERE firstclockin = '01/11/2011'

SELECT *
FROM #wtf
WHERE firstclockin = '01/03/2011'

SELECT *
FROM stgArkonaPYPCLOCKIN
WHERE yiclkind between '01/11/2011' AND '01/31/2011'
  AND yiemp# IN ('1125570', '171210', '184726', '193790')

-- whackem
DELETE
FROM stgArkonaPYPCLOCKIN
WHERE yiclkind = '01/11/2011'
  AND yiemp# IN ('1125570', '171210', '184726', '193790')
  
-- AND some outliers  
DELETE
FROM stgArkonaPYPCLOCKIN
WHERE yiclkind = '01/03/2011'
  AND yiemp# = '114200'
  
DELETE
FROM stgArkonaPYPCLOCKIN
WHERE yiclkind = '01/02/2010'
  AND yiemp# = '1149500'
    
/* 2 */
-- 1 empkey, 1ymhiredate
-- which means that these are ok to UPDATE  hiredate and empkeyfromdate with yiclkind

SELECT *
-- SELECT COUNT(*) -- 13
FROM #wtf
WHERE ymhdte = ymhdto
AND howmanyEmpKeys = 1

UPDATE edwEmployeeDim 
SET hiredate = x.firstclockin,
    hiredatekey = (SELECT datekey FROM day WHERE thedate = x.firstclockin),
    EmployeeKeyFromDate = x.firstclockin,
    EmployeeKeyFromDateKey = (SELECT datekey FROM day WHERE thedate = x.firstclockin)
FROM #wtf x
WHERE x.ymhdte = ymhdto
  AND x.howmanyEmpKeys = 1
  AND edwEmployeeDim.storecode = x.storecode
  AND edwEmployeeDim.employeenumber = x.employeenumber    
  
/* 3 */
-- 1 empkey
-- which ever IS less firstclockin vs ymhdto
UPDATE edwEmployeeDim 
SET hiredate = CASE WHEN firstclockin < ymhdto THEN x.firstclockin ELSE ymhdto END,
    hiredatekey = (SELECT datekey FROM day WHERE thedate = CASE WHEN firstclockin < ymhdto THEN x.firstclockin ELSE ymhdto END),
    EmployeeKeyFromDate = CASE WHEN firstclockin < ymhdto THEN x.firstclockin ELSE ymhdto END,
    EmployeeKeyFromDateKey = (SELECT datekey FROM day WHERE thedate = CASE WHEN firstclockin < ymhdto THEN x.firstclockin ELSE ymhdto END)
-- SELECT *    
FROM #wtf x
WHERE howmanyempkeys = 1
  AND ymhdte IS NOT NULL 
  AND edwEmployeeDim.storecode = x.storecode
  AND edwEmployeeDim.employeenumber = x.employeenumber  


/* 4 */

-- the remaining discrepancies
-- ALL with multiple empkeys
-- go ahead AND DO these manually
-- rehire/reuse show up (AS a single row IN #wtf), but are NOT an issue

-- aaahhh shit, forgot the fucking datekey fields

SELECT * FROM #wtf ORDER BY employeenumber

SELECT e.storecode, e.employeekey, e.employeenumber, name, hiredate, employeekeyfromdate
FROM edwEmployeeDim e
WHERE e.employeenumber IN ('1145560','124531','131600','136510','138421','181410','2106510') 
ORDER BY employeenumber, employeekey
  
  
UPDATE edwEmployeeDim
SET hiredatekey = (SELECT datekey FROM day WHERE thedate = hiredate),
    employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = EmployeeKeyFromDate)
from edwEmployeeDim 
WHERE employeenumber IN ('1145560','124531','131600','136510','138421','181410','2106510') 
  

 
SELECT *
FROM stgArkonaPYPCLOCKIN
WHERE yiemp# = '187800'
AND yiclkind > '07/28/2009'
ORDER BY yiclkind
/****** 2/20 failing cont ******************************************************/   
/******************************************************************************/
--END       the clockin before hired issue
/******************************************************************************/     
