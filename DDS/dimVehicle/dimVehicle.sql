SELECT *
FROM stgArkonaINPMAST
-- vin IS unique
SELECT imvin
FROM stgArkonaINPMAST
GROUP BY imvin HAVING COUNT(*) > 1

SELECT DISTINCT imco# FROM stgArkonaINPMAST
-- most ALL the other possible arkona columns are relevant to a fact, NOT the dim (acct, license, odom, cost, etc)
-- these are VehicleItems
-- initial use IS just to identify the vehicle ON an RO
-- i know this will grow, but DO it based ON actual need
-- good enuf to get started
DROP TABLE dimVehicle;
CREATE TABLE dimVehicle (
  VehicleKey autoinc,
  Vin cichar(17),
  ModelYear cichar(4),
  Make cichar(25),
  Model cichar(25),
  ModelCode cichar (10),
  Body cichar(25),
  Color cichar(25))IN database;
  
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimVehicle','dimVehicle.adi','PK',
   'VehicleKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimVehicle','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'dimVehicleObjectsfail');   
-- index: NK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimVehicle','dimVehicle.adi','NK',
   'Vin', '',2051,512,'' );   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimVehicle','Vin', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
  
 
--< keyMap --------------------------------------------------------------------
--DROP TABLE keyMapDimVehicle;     
CREATE TABLE keyMapDimVehicle (
  VehicleKey integer CONSTRAINT NOT NULL ,
  Vin cichar(17) CONSTRAINT NOT NULL,  
CONSTRAINT PK PRIMARY KEY (VehicleKey)) IN database;          

--RI  
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimVehicle-KeyMap');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimVehicle-KeyMap','dimVehicle','keyMapDimVehicle','PK',2,2,NULL,'','');   
--/> keyMap --------------------------------------------------------------------  

--< Dependencies & zProc ------------------------------------------------------
--- Dependencies & zProc ------------------------------------------------------
INSERT INTO zProcExecutables values('dimVehicle','daily',CAST('12:15:15' AS sql_time),14);
DELETE FROM zProcProcedures WHERE proc = 'INPMAST';
INSERT INTO zProcProcedures values('dimVehicle','INPMAST',1,'Daily');
INSERT INTO DependencyObjects values('dimVehicle','INPMAST','dimVehicle');
--/ Dependencies & zProc ------------------------------------------------------
--/> Dependencies & zProc ------------------------------------------------------  

--< initial load --------------------------------------------------------------
SELECT * from stgArkonaINPMAST
  VehicleKey autoinc,
  Vin cichar(17),
  ModelYear cichar(4),
  Make cichar(25),
  Model cichar(25),
  ModelCode cichar (10),
  Body cichar(25),
  Color cichar(25))IN database;

EXECUTE PROCEDURE sp_zaptable('dimVehicle')
DELETE FROM dimVehicle;  
INSERT INTO dimvehicle (Vin, ModelYear, Make, Model, ModelCode, Body, Color) 
values('Unknown','Unkn','Unknown','Unknown','Unknown','Unknown','Unknown');
INSERT INTO dimVehicle (Vin, ModelYear, Make, Model, ModelCode, Body, Color)
SELECT imvin, left(cast(imyear AS sql_char), 4), immake, immodl, immcode, imbody, imcolr FROM stgArkonaINPMAST;  
--/> initial load --------------------------------------------------------------

CREATE PROCEDURE xfmDimVehicle ()
BEGIN
/*
*/
INSERT INTO dimvehicle (Vin, ModelYear, Make, Model, ModelCode, Body, Color) 
SELECT imvin, left(cast(imyear AS sql_char), 4), immake, immodl, immcode, imbody, imcolr
FROM stgArkonaINPMAST a
WHERE NOT EXISTS (
  SELECT 1
  FROM dimVehicle
  WHERE vin = a.imvin);
END;  

-- 10/12/13 forgot to DO keyMapDimVehicle
DELETE FROM keyMapDimVehicle;
INSERT INTO keyMapDimVehicle (VehicleKey, VIN)
SELECT VehicleKey, VIN
FROM dimVehicle a
WHERE NOT EXISTS (
  SELECT 1
  FROM keyMapDimVehicle
  WHERE VehicleKey = a.VehicleKey);  
