
/******************************************************************************/
-- COLUMN list for code
DECLARE @i integer;
DECLARE @j integer;
DECLARE @col string;
DECLARE @cols string;
DECLARE @tablename string;
@tableName = 'edwEmployeeDim';
@i = (
  SELECT MAX(Field_Num)
  FROM system.columns
  WHERE parent = @tablename);
@j = 1;
@col = '';
@cols = '';
WHILE @j <= @i DO
-- bracketed COLUMN names
--  @col = coalesce((SELECT '[' + TRIM(column_name) + '], '  FROM #pymast WHERE ordinal_position = @j), '');
-- just COLUMN names
  @col = coalesce((SELECT TRIM(name) + ', '  FROM system.columns WHERE Field_Num = @j and parent = @tableName), '');
-- parameter names  
--  @col = coalesce((SELECT ':' + replace(TRIM(column_name), '#', '') + ', '  FROM #pymast WHERE ordinal_position = @j), '');
  @cols = @cols + @col;
  @j = @j + 1; 
END WHILE;
SELECT @cols FROM system.iota;   

SELECT now() FROM system.iota


-- ahahahah - timestamp fields: ts OR day/time of day keys 
-- 12/28 TS fields for now
-- 11655
SELECT * FROM edwEmployeeDim WHERE EmployeeNumber = '11655'

DECLARE @NowTS timestamp;
@NowTS = (SELECT now() FROM system.iota); 

INSERT INTO edwEmployeeDim(StoreCode, StoreCodeDesc, EmployeeNumber, 
  ActiveCode, ActiveCodeDesc, PyDeptCode, PyDeptCodeDesc, Name, BirthDate, 
  BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, RowLoadedTS, 
  RowUpdatedTS, RowEffectiveTS, RowExpirationTS, CurrentRow)
  
-- type 1  
SELECT x.StoreCode, x.StoreCodeDesc, x.EmployeeNumber, 
  x.ActiveCode, x.ActiveCodeDesc, x.PyDeptCode, x.PyDeptCodeDesc, x.Name, x.BirthDate, 
  x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, '@NowTS', 
  CAST(NULL AS sql_timestamp), '@NowTS', CAST(NULL AS sql_timestamp), true
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
  AND x.employeeNumber = e.EmployeeNumber
WHERE (
--  x.name <> e.Name OR 
  difference(x.name, e.name) <> 4 OR
  x.BirthDateKey <> e.BirthDateKey OR
  x.HireDateKey <> e.HireDateKey)
  
  
SELECT x.name, e.name, difference(x.name, e.name), soundex(x.name), soundex(e.name) 
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
  AND x.employeeNumber = e.EmployeeNumber
WHERE (
  x.name <> e.Name OR 
  x.BirthDateKey <> e.BirthDateKey OR
  x.HireDateKey <> e.HireDateKey)  

-- 12/29 after going down this rabbit hole for a bit, decided it's NOT worth it
-- any name change, therefor will be type 2
/*  
DECLARE @st1 string;
DECLARE @st2 string;
DECLARE @sx1 string;
DECLARE @sx2 string;
@st1 = 'KUTZOL,SEAN M';
@st2 = 'KUTZOL-HANSON,JEAN M'; 
@sx1 = (SELECT soundex(@st1) FROM system.iota);
@sx2 = (SELECT soundex(@st2) FROM system.iota); 
IF @sx1 = @sx2  THEN 
  @sx1 = (SELECT soundex(substring(@st1, 4, length(@st1))) FROM system.iota);
  @sx2 = (SELECT soundex(substring(@st2, 4, length(@st1))) FROM system.iota);
ELSE 
  IF @sx1 = @sx2  THEN 
    @sx1 = (SELECT soundex(substring(@st1, 8, length(@st1))) FROM system.iota);
    @sx2 = (SELECT soundex(substring(@st2, 8, length(@st1))) FROM system.iota);
  END IF;  
END IF;
SELECT @sx1, @sx2 FROM system.iota;
*/

-- type 1  
-- so, i guess that's it, just the 2 type 1 fields
-- nope, term date
SELECT x.StoreCode, x.StoreCodeDesc, x.EmployeeNumber, 
  x.ActiveCode, x.ActiveCodeDesc, x.PyDeptCode, x.PyDeptCodeDesc, x.Name, x.BirthDate, 
  x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, '@NowTS', 
  CAST(NULL AS sql_timestamp), '@NowTS', CAST(NULL AS sql_timestamp), true
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
  AND x.employeeNumber = e.EmployeeNumber
WHERE (
  x.BirthDateKey <> e.BirthDateKey OR
  x.HireDateKey <> e.HireDateKey)
-- need to DO each field separately
SELECT x.StoreCode, x.StoreCodeDesc, x.EmployeeNumber, 
  x.ActiveCode, x.ActiveCodeDesc, x.PyDeptCode, x.PyDeptCodeDesc, x.Name, x.BirthDate, 
  x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, '@NowTS', 
  CAST(NULL AS sql_timestamp), '@NowTS', CAST(NULL AS sql_timestamp), true
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
  AND x.employeeNumber = e.EmployeeNumber
WHERE x.HireDateKey <> e.HireDateKey

INSERT INTO edwEmployeeDim(StoreCode, StoreCodeDesc, EmployeeNumber, 
  ActiveCode, ActiveCodeDesc, PyDeptCode, PyDeptCodeDesc, Name, BirthDate, 
  BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, RowLoadedTS, 
  RowUpdatedTS, RowEffectiveTS, RowExpirationTS, CurrentRow)

DECLARE @NowTS timestamp;
@NowTS = (SELECT now() FROM system.iota);   
UPDATE edwED
  SET edwED.HireDateKey = x.HireDateKey,
      edwED.RowUpdatedTS = @NowTS,
	  edwED.RowEffectiveTS = @NowTS
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
  AND x.employeeNumber = edwED.EmployeeNumber
WHERE x.HireDateKey <> edwED.HireDateKey

-- type 2 changes
DECLARE @NowTS timestamp;
@NowTS = (SELECT now() FROM system.iota); 
SELECT x.StoreCode, x.StoreCodeDesc, x.EmployeeNumber, 
  x.ActiveCode, x.ActiveCodeDesc, x.PyDeptCode, x.PyDeptCodeDesc, x.Name, x.BirthDate, 
  x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, @NowTS, 
  CAST(NULL AS sql_timestamp), @NowTS, CAST(NULL AS sql_timestamp), true
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
  AND x.employeeNumber = edwED.EmployeeNumber
WHERE (
  x.Name <> edwED.Name OR 
  x.ActiveCode <> edwED.ActiveCode OR
  x.PyDeptCode <> edwED.PyDeptCode OR 
  x.TermDateKey <> edwED.TermDateKey) 

  -- shit, i need to UPDATE Depts based ON the changes that were done at the beginning of the month
  -- DO it AS type 1 changes
DECLARE @NowTS timestamp;
@NowTS = (SELECT now() FROM system.iota);   
UPDATE edwED
  SET edwED.PyDeptCode  = x.PyDeptCode,
      edwED.RowUpdatedTS = @NowTS
-- SELECT *	  
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
  AND x.employeeNumber = edwED.EmployeeNumber
WHERE x.PyDeptCode <> edwED.PyDeptCode 
  
  
  
DECLARE @NowTS timestamp;
@NowTS = (SELECT now() FROM system.iota);     

  
UPDATE edwED
  SET edwED.RowUpdatedTS = @NowTS,
	  edwED.RowExpirationTS = @NowTS,
	  edwED.CurrentRow = false
-- SELECT *	  
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
  AND x.employeeNumber = edwED.EmployeeNumber
WHERE (
  x.Name <> edwED.name OR
  x.ActiveCode <> edwED.ActiveCode OR
  x.PyDeptCode <> edwED.PyDeptCode OR 
  x.TermDateKey <> edwED.TermDateKey);   
  
  
INSERT INTO edwEmployeeDim(StoreCode, StoreCodeDesc, EmployeeNumber, 
  ActiveCode, ActiveCodeDesc, PyDeptCode, PyDeptCodeDesc, Name, BirthDate, 
  BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, RowLoadedTS, 
  RowUpdatedTS, RowEffectiveTS, RowExpirationTS, CurrentRow)
SELECT x.StoreCode, x.StoreCodeDesc, x.EmployeeNumber, 
  x.ActiveCode, x.ActiveCodeDesc, x.PyDeptCode, x.PyDeptCodeDesc, x.Name, x.BirthDate, 
  x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, @NowTS, 
  CAST(NULL AS sql_timestamp), @NowTS, CAST(NULL AS sql_timestamp), true
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
  AND x.employeeNumber = e.EmployeeNumber  
WHERE (
  x.Name <> edwED.name OR
  x.ActiveCode <> edwED.ActiveCode OR
  x.PyDeptCode <> edwED.PyDeptCode OR 
  x.TermDateKey <> edwED.TermDateKey);   
  
  
          SELECT COUNT(*)
		  FROM (
		  SELECT EmployeeNumber, StoreCode
          FROM edwEmployeeDim
		  WHERE CurrentRow = true
          GROUP BY EmployeeNumber, StoreCode
    	    HAVING COUNT(*) > 1) x  
			
			
-- 12/30/11
// type 2 changes
  // UPDATE old record
// this part IS ok, those records IN xfmED that have a corresponding
// CURRENT record IN edwED WHERE emp# = emp# AND store = store
// AND (xfmED or name or active or dept OR term date)IS different
DECLARE @nowTS timestamp;
@NowTS = (SELECT now() FROM system.iota);
      UPDATE edwED
        SET edwED.RowUpdatedTS = @NowTS,
      	  edwED.RowExpirationTS = @NowTS,
      	  edwED.CurrentRow = false
-- SELECT x.*	 		  
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
		AND edwED.CurrentRow = true
      WHERE (
        x.Name <> edwED.name OR
        x.ActiveCode <> edwED.ActiveCode OR
        x.PyDeptCode <> edwED.PyDeptCode OR 
        x.TermDateKey <> edwED.TermDateKey);   
  // INSERT new record        
// this one IS making me nervous, need a better way to identify which record to INSERT
// the affected record IS no longer the current row IN edwED
// does that even fucking matter?
// am i simply looking for any record that doesn't match?
// SET up a test CASE
// ooh ooh WHERE there IS no current row IN edwED ?  depends ON ads recognizing 
// the UPDATE within the TRANSACTION
// why IS this shit so hard to articulate
// what i'm seeing IS UPDATE, THEN how DO i single out the new record to INSERT
// because there IS no current row to compare to
      INSERT INTO edwEmployeeDim(StoreCode, StoreCodeDesc, EmployeeNumber, 
        ActiveCode, ActiveCodeDesc, PyDeptCode, PyDeptCodeDesc, Name, BirthDate, 
        BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, RowLoadedTS, 
        RowUpdatedTS, RowEffectiveTS, RowExpirationTS, CurrentRow)
		
DECLARE @NowTS timestamp;
@NowTS = (SELECT now() FROM system.iota);		
      SELECT x.StoreCode, x.StoreCodeDesc, x.EmployeeNumber, 
        x.ActiveCode, x.ActiveCodeDesc, x.PyDeptCode, x.PyDeptCodeDesc, x.Name, x.BirthDate, 
        x.BirthDateKey, x.HireDate, x.HireDateKey, x.TermDate, x.TermDateKey, @NowTS, 
        CAST(NULL AS sql_timestamp), @NowTS, CAST(NULL AS sql_timestamp), true
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber  
      WHERE (
        x.Name <> edwED.name OR
        x.ActiveCode <> edwED.ActiveCode OR
        x.PyDeptCode <> edwED.PyDeptCode OR 
        x.TermDateKey <> edwED.TermDateKey);   			
		
SELECT *
FROM edwEmployeeDim
WHERE employeeNumber IN (
  SELECT employeenumber
  FROM edwEmployeeDIM
  GROUP BY employeenumber, storecode
    HAVING COUNT(*) > 1)
ORDER BY employeenumber			

--------------------------------------
-- this IS a stab at the ooh ooh: no current row
SELECT *
FROM edwEmployeeDim e
WHERE NOT EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = e.employeenumber
	AND currentrow = true)
	
working with emp with mult records already IN edwED
Sean Hutzol 26820
term him  12/30
SELECT *
FROM xfmEmployeeDim
WHERE employeenumber = '268200'
	
	
SELECT * -- 2921, 12/30
FROM day
WHERE thedate = curdate()	

so, the theory IS, UPDATE edwED with the new info (deactivating currentrow)
THEN can SELECT the new record to INSERT based ON there being no current row
-- ok, so edwED IS updated
-- AND this query:
SELECT *
FROM edwEmployeeDim e
WHERE NOT EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = e.employeenumber
	AND currentrow = true)
-- returns both of his rows, so now, INSERT INTO edwED the row(s) FROM xfmED 
-- shit this IS sounding to finicky, works ok, probably with just one row, but, could
-- be updating a ton of type 2 changes, think sdprpdet
SELECT employeenumber, currentrow
FROM edwEmployeeDim

SELECT * FROM edwEmployeeDim WHERE EmployeeNumber = '268200'

--12/31/11 the Kimball merge 
-- this won't fly, they use an OUTPUT clause of the MERGE statement


-- so, am i stuck with a cursor/loop to handle the changed records?

-- OR, let's TRY a temp TABLE
-- 1. identify those rows exhibiting a type 2 change AND INSERT them INTO a temp table
SELECT Utilities.DropTablesIfExist('#Type2') FROM system.iota;
-- DROP TABLE #Type2
SELECT edwED.EmployeeKey, x.*,
  CASE 
    WHEN (
      x.Name <> edwED.name AND
      x.ActiveCode <> edwED.ActiveCode AND
      x.PyDeptCode <> edwED.PyDeptCode AND 
      x.TermDateKey <> edwED.TermDateKey) THEN 'Name, ActiveCode, PyDeptCode, TermDateKey'
	  
  END AS RowChangeReaseon		
--INTO #Type2  
FROM xfmEmployeeDim x
LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
  AND x.employeeNumber = edwED.EmployeeNumber
  AND edwED.CurrentRow = true
WHERE (
  x.Name <> edwED.name OR
  x.ActiveCode <> edwED.ActiveCode OR
  x.PyDeptCode <> edwED.PyDeptCode OR 
  x.TermDateKey <> edwED.TermDateKey);  
select * FROM #Type2;

-- UPDATE the old record
UPDATE edwEmployeeDim
SET RowUpdatedTS = @NowTS,
    RowExpirationTS = @NowTS,
    CurrentRow = false
-- SELECT * FROM edwEmployeeDim
WHERE EmployeeKey IN (
  SELECT EmployeeKey
  FROM #Type2);	
  
// INSERT new record        
INSERT INTO edwEmployeeDim(StoreCode, StoreCodeDesc, EmployeeNumber, 
  ActiveCode, ActiveCodeDesc, PyDeptCode, PyDeptCodeDesc, Name, BirthDate, 
  BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, RowLoadedTS, 
  RowUpdatedTS, RowEffectiveTS, RowExpirationTS, CurrentRow)
SELECT StoreCode, StoreCodeDesc, EmployeeNumber, 
  ActiveCode, ActiveCodeDesc, PyDeptCode, PyDeptCodeDesc, Name, BirthDate, 
  BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, @NowTS, 
  CAST(NULL AS sql_timestamp), @NowTS, CAST(NULL AS sql_timestamp), true
FROM #Type2;     

DELETE FROM #Type2
SELECT COUNT(*) from #type2

/***************
after getting the type 2 shit straightened out (term -> type 1)
want to UPDATE effts to correspond with hire date
1/2 way INTO it AND thinking shit, it doesn't matter, nothing has been changed
***************/

SELECT activecode, name, hiredate, rowupdatedts, roweffectivets, currentrow
FROM edwEmployeeDim

SELECT CAST(roweffectivets AS sql_date), COUNT(*)
FROM edwEmployeeDim
GROUP BY CAST(roweffectivets AS sql_date)

SELECT *
FROM edwEmployeeDim
WHERE roweffectivets IS NULL 

SELECT activecode, employeenumber, name, hiredate, rowupdatedts, roweffectivets, currentrow
FROM edwEmployeeDim
WHERE CAST(roweffectivets AS sql_date) <> '12/16/2011'
ORDER BY employeenumber

-- ooh, WHERE updTS IS NULL
-- these can definitely be updated
SELECT activecode, employeenumber, name, hiredate, rowupdatedts, roweffectivets, 
  currentrow, rowexpirationts
FROM edwEmployeeDim
WHERE rowUpdatedts IS NULL

UPDATE edwEmployeeDim
SET roweffectivets = timestampadd(sql_tsi_hour, 4, cast(hiredate AS sql_timestamp)) 
WHERE RowUpdatedTS IS null

SELECT activecode, employeenumber, name, hiredate, rowupdatedts, roweffectivets, 
  currentrow, rowexpirationts
FROM edwEmployeeDim
WHERE rowUpdatedts IS NOT NULL

SELECT *
FROM edwEmployeeDim 
WHERE currentrow = false




