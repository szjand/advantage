-- attempting to figure out how to tell , at 3:00 ON wednesday who IN pdq was clocked IN
-- AND off course the fuckers will want to DO it "real time" AS well, who IS currently clockied IN for pdq
--
-- AND of course the first place i go IS looking for anomalies IN clock hours
-- hence
-- ALL emp with a straggling yicode = I
SELECT a.*, b.name, b.pydept, b.distcode, c.clockhours
FROM tmppypclockin a
LEFT JOIN edwEmployeeDim b ON a.yico# = b.storecode
  AND a.yiemp# = b.employeenumber
  AND a.yiclkind BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.payrollclass = 'hourly'
LEFT JOIN rptClockHours c ON b.employeekey = c.employeekey  
  AND a.yiclkind = c.thedate
WHERE EXISTS (
  SELECT 1
  FROM tmpPypclockin
  WHERE yico# = a.yico#
    AND yiemp# = a.yiemp#
    AND yiclkind = a.yiclkind
    AND yicode = 'i') 
  AND b.name IS NOT NULL     
ORDER BY yiemp#, yiclkind    

-- yicode = I AND no yicode = O for that date
SELECT a.*, b.name, b.pydept, b.distcode, c.clockhours
FROM stgarkonapypclockin a
LEFT JOIN edwEmployeeDim b ON a.yico# = b.storecode
  AND a.yiemp# = b.employeenumber
  AND a.yiclkind BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.payrollclass = 'hourly'
LEFT JOIN rptClockHours c ON b.employeekey = c.employeekey  
  AND a.yiclkind = c.thedate
WHERE yicode = 'i'
  AND NOT EXISTS (
    SELECT 1
    FROM stgarkonaPYPCLOCKIN
    WHERE yico# = a.yico#
      AND yiemp# = a.yiemp#
      AND yiclkind = a.yiclkind
      AND yicode = 'O')
  AND b.name IS NOT NULL 
ORDER BY yico#, yiclkind      

-- fuck it that's a different problem
-- what  
-- how many minutes clocked IN this hour (OR for this range of hours) BY empkey

-- clocked IN minutes/hour





-- wtf, a whole bunch of 99999 minutes
-- aha! clockin times are ON the hour
-- eg  110910 1/2/12, 121101 1/6/12, 185950 1/5/10
-- also check ON the hour clockouts
-- minute 1/2 to DO tmp
-- test against edwClockHoursFact SUM(minutes) BY day
-- this IS only good for WHEN yiclind = yiclkoutd   
-- 7/25, ok this IS it
-- simply DELETE the offending concurrent logins
-- test for recurrence nightly
-- exclude post midnight minutes
-- 2 runs 1. yiclkind = yiclkoutd  2. yiclkind < yiclkoutd
-- hos DO i want to store the results
at the fact level, just LIKE edwClockHoursFact with a timeofdaykey
at the rpt level, just LIKE rptClockhours
-- ok, 1 more test
-- compare zminutes with edwClockHoursFact
so xfm zminutes INTO the factlevel AND THEN compare
need the empkey, DO it just LIKE edwClockHoursFact 


-- 7/25 first load 
-- god damnit, DO real AND complete zminutes first
CREATE TABLE factClockMinutesByHour (
  EmployeeKey integer,
  DateKey integer,
  TimeOfDayKey integer, 
  Minutes integer) IN database;
INSERT INTO factClockMinutesByHour
SELECT b.employeekey, c.datekey, d.TimeOfDayKey, a.minutes
--SELECT *
FROM zminutes a
LEFT JOIN edwEmployeeDim b ON  a.yico = b.storecode
  AND a.yiemp = b.employeenumber
  AND a.yiclkind BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
LEFT JOIN day c ON a.yiclkind = c.thedate 
LEFT JOIN TimeOfDay d ON a.thetime = d.thetime
WHERE a.minutes < 61
  AND a.yico IN ('ry1','ry2','ry3');

ok, see the spreadsheet, the CASE statement IS good
BUT                 
DELETE FROM zminutes;                    
INSERT INTO zMinutes
SELECT a.yico#, a.yiemp#, a.yiclkind, b.thetime, 
  SUM (
    CASE
      WHEN thetime = yiclkint AND thetime = yiclkoutt -- 1
        OR thetime > yiclkint AND thetime = yiclkoutt -- 7
        OR thetime > yiclkint AND thetime > yiclkoutt AND hour(thetime) > hour(yiclkoutt) -- 9a
          THEN 0
      WHEN thetime = yiclkint AND thetime < yiclkoutt AND hour(thetime) = hour(yiclkoutt) -- 2a
        OR thetime < yiclkint AND thetime < yiclkoutt AND hour(thetime) = hour(yiclkoutt) -- 5a
          THEN minute(yiclkoutt) - minute(yiclkint)
      WHEN thetime = yiclkint AND thetime < yiclkoutt AND hour(thetime) < hour(yiclkoutt) -- 2b
        OR thetime > yiclkint and thetime < yiclkoutt AND hour(thetime) < hour(yiclkoutt) -- 8b
          THEN 60 
      WHEN thetime = yiclkint and thetime > yiclkoutt -- 3
        OR thetime < yiclkint and thetime = yiclkoutt -- 4
        OR thetime < yiclkint and thetime > yiclkoutt -- 6
          THEN 666
      WHEN thetime < yiclkint and thetime < yiclkoutt and hour(thetime) < hour(yiclkoutt) THEN 60 - minute(yiclkint) -- 5b
      WHEN thetime > yiclkint AND thetime < yiclkoutt AND hour(thetime) = hour(yiclkoutt) THEN minute(yiclkoutt) -- 8a 
      ELSE 999999
    END) AS minutes
--SELECT *  
FROM (
  SELECT yico#, yiemp#, yiclkind, yiclkoutd, yicode,
    yiclkint, 
    CASE cast(yiclkoutt AS sql_char) -- handle midnight clockout
      WHEN '00:00:00' THEN CAST('23:59:59' AS sql_time)
      ELSE yiclkoutt
    END AS yiclkoutt    
  FROM stgArkonaPYPCLOCKIN
  WHERE yiclkind = yiclkoutd -- !important
    AND yicode = 'O') a  -- clockhours only, no vac, pto, etc
LEFT JOIN (
  SELECT *
  FROM timeofday 
  WHERE minute(thetime) = 0
    AND second(thetime) = 0) b ON hour(b.thetime) BETWEEN hour(yiclkint) AND hour(yiclkoutt)
GROUP BY a.yico#, a.yiemp#, a.yiclkind, b.thetime  

--WHERE minute(yiclkint) = 0 AND second(yiclkint) = 0     

DROP TABLE zMinutes;
CREATE TABLE zMinutes (
  yico cichar(3),
  yiemp cichar(7),
  yiclkind date,
  thetime time,
  minutes integer) IN database;    

-- uh oh, minutes > 60  
these ALL appear to be FROM emp that have overlapping clockins ON the same day
SELECT *
-- SELECT COUNT(*)
FROM zminutes
WHERE minutes > 60
  AND yiemp = '1106260'
ORDER BY yiclkind DESC


-- this looks LIKE it selects out the overlapping records
-- that account for minutes > 60  
-- include this AS a test IN ETL
SELECT yico#, yiemp#, yiclkind, min(yiclkint) AS minIn, MAX(yiclkint) AS maxIN,
  MIN(yiclkoutt) AS minOut, MAX(yiclkoutt) AS maxOut
FROM stgArkonaPYPCLOCKIN  
--WHERE yiemp# = '114205'
--  AND yiclkind = '06/13/2012'
GROUP BY yico#, yiemp#, yiclkind
HAVING COUNT(*) > 1
  AND MAX(yiclkint) < MIN(yiclkoutt)
ORDER BY yico#, yiemp#, yiclkind  

SELECT *
FROM zminutes
WHERE minutes > 60  
  
SELECT a.*, c.thetime, c.minutes
FROM stgArkonaPYPCLOCKIN a
INNER JOIN (
  SELECT yico#, yiemp#, yiclkind, min(yiclkint) AS minIn, MAX(yiclkint) AS maxIN,
    MIN(yiclkoutt) AS minOut, MAX(yiclkoutt) AS maxOut
  FROM stgArkonaPYPCLOCKIN  
  --WHERE yiemp# = '114205'
  --  AND yiclkind = '06/13/2012'
  GROUP BY yico#, yiemp#, yiclkind
  HAVING COUNT(*) > 1 AND MAX(yiclkint) < MIN(yiclkoutt)) b ON a.yico# = b.yico#
  AND a.yiemp# = b.yiemp#
  AND a.yiclkind = b.yiclkind  
LEFT JOIN zminutes c ON a.yico# = c.yico 
  and a.yiemp# = c.yiemp  
  AND a.yiclkind = c.yiclkind
  AND hour(a.yiclkint)= hour(c.thetime)
  AND c.minutes > 60
ORDER BY a.yico#, a.yiemp#, a.yiclkind      

SELECT COUNT(*) -- 97
FROM (
  SELECT yico, yiemp, yiclkind
  FROM zminutes
  WHERE minutes > 60
  GROUP BY yico, yiemp, yiclkind) a
  
SELECT COUNT(*) -- 150959
FROM (
  SELECT yico, yiemp, yiclkind
  FROM zminutes
  GROUP BY yico, yiemp, yiclkind) a  

SELECT yico, yiemp, yiclkind, thetime
FROM zminutes
GROUP BY yico, yiemp, yiclkind, thetime
HAVING COUNT(*) > 1

        
  

-- now yiclkind <> yiclkoutd
-- given the proposed use for this data, i'm leaning toward excluding post midnight hours from post midnight clockouts

select a.*, b.thetime,
  CASE 
    WHEN thetime < yiclkint THEN minute(yiclkint) - minute(thetime)
    WHEN hour(thetime) = hour(yiclkoutt) THEN minute(yiclkoutt) 
    WHEN thetime > yiclkint AND thetime < yiclkoutt THEN 60
    ELSE 0
  END 
FROM (
  SELECT yico#, yiemp#, yiclkind, yiclkoutd, yicode,
    yiclkint,  CAST('23:59:59' AS sql_time) AS yiclkoutt    
  FROM stgArkonaPYPCLOCKIN
  WHERE yiclkoutd > yiclkind
    AND yiclkoutd - yiclkind = 1
    AND yicode = 'O') a  -- clockhours only, no vac, pto, etc
LEFT JOIN (
  SELECT *
  FROM timeofday 
  WHERE minute(thetime) = 0
    AND second(thetime) = 0) b ON hour(b.thetime) BETWEEN hour(yiclkint) AND hour(yiclkoutt)   
    
    
SELECT outhour, COUNT(*)  
FROM (
  SELECT yico#, yiemp#, yiclkind, yiclkoutd, yicode,
    yiclkint, yiclkoutt, hour(yiclkoutt) as outhour
  FROM stgArkonaPYPCLOCKIN
  WHERE yiclkoutd > yiclkind 
    AND yiclkoutd - yiclkind = 1
    AND yicode = 'O') a 
GROUP BY outhour    
ORDER BY yiclkint desc    


  SELECT yico#, yiemp#, yiclkind, yiclkoutd, yicode,
    yiclkint, yiclkoutt, hour(yiclkoutt) as outhour
  SELECT COUNT(*)
  FROM stgArkonaPYPCLOCKIN
  WHERE yiclkoutd > yiclkind 
    AND yiclkoutd - yiclkind = 1
    AND yicode = 'O'
    AND yiclkoutt < CAST('12:00:00' AS sql_time) -- what the fuck IS this line for
    
    
LEFT JOIN (
  SELECT *
  FROM tally
  WHERE n BETWEEN 0 AND 23) b ON b.n BETWEEN hour(yiclkint) AND 
    case hour(yiclkoutt)
      WHEN 0 THEN 24
      ELSE hour(yiclkoutt)
    end
ORDER BY yiclkoutt desc

select a.*, b.thetime,
  CASE 
    WHEN thetime < yiclkint THEN minute(yiclkint) - minute(thetime)
    WHEN hour(thetime) = hour(yiclkoutt) THEN minute(yiclkoutt) 
    WHEN thetime > yiclkint AND thetime < yiclkoutt THEN 60
    ELSE 0
  END 
  
SELECT *  
FROM (
  SELECT yico#, yiemp#, yiclkind, yiclkoutd, yicode,
    yiclkint, yiclkoutt
  FROM stgArkonaPYPCLOCKIN
  WHERE yiclkoutd > yiclkind 
    AND yiclkoutd - yiclkind = 1
    AND yicode = 'O'
    AND yiclkoutt < CAST('12:00:00' AS sql_time)) a  -- clockhours only, no vac, pto, etc
LEFT JOIN (
  SELECT *
  FROM timeofday 
  WHERE minute(thetime) = 0
    AND second(thetime) = 0) b ON hour(b.thetime) BETWEEN hour(yiclkint) AND hour(yiclkoutt) + 24
WHERE year(yiclkind) = 2012 AND yiemp# = '167800'  

ORDER BY yiclkoutt desc

SELECT *
FROM stgarkonapypclockin
WHERE yiclkoutt > CAST('00:00:00' AS sql_time)
ORDER BY yiclkind desc
      
SELECT yico#, yiemp#, yiclkind, yiclkoutd, yicode,
  yiclkint, yiclkoutt,
  yiclkoutd - yiclkind
FROM stgArkonaPYPCLOCKIN
WHERE yiclkoutd <> yiclkind 
--  AND yiemp# = '1132595'
  AND yiclkoutd - yiclkind > 1
  
1 row for each empkey for each day for each hour w/the the number of minutes clocked IN for that hour
ooh, i can use clockhours facts AS the basis for empkey & days ??

SELECT employeekey, datekey
FROM edwClockHoursFact 
WHERE clockhours <> 0

  
--  INSERT INTO edwClockHoursFact -- 11973
  SELECT 
    (SELECT employeekey FROM edwEmployeeDim WHERE employeenumber = e.employeenumber AND d.thedate BETWEEN employeekeyfromdate AND employeekeythrudate) AS EmployeeKey,
    d.datekey, 
    coalesce(h.clockhours, 0) AS ClockHours,
    coalesce(o.regularhours, 0) AS RegularHours,
    coalesce(o.OvertimeHours, 0) AS OvertimeHours, 
    coalesce(h.vachours, 0) AS VacationHours,
    coalesce(h.ptohours, 0) AS PTOHours,
    coalesce(h.holhours, 0) AS HolidayHours 
  FROM day d -- one row per day going back 4 weeks FROM last sunday thru today
  LEFT JOIN -- 1 row per empKey/day, should also be emp#/day, granularity of empDim IS day! there can NOT be multiple records for an emp# ON any given day
    edwEmployeeDim e ON d.thedate BETWEEN e.EmployeekeyFromDate AND e.EmployeeKeyThruDate
  LEFT JOIN ( -- one row per co/emp#/clockindate
      SELECT yico#, yiemp#, yiclkind , sum(vachours) AS VacHours, SUM(ptohours) as PTOHours, 
        SUM(HolHours) AS HolHours, SUM(ClockHours) AS ClockHours
      FROM tmpClockHours
      GROUP BY yico#, yiemp#, yiclkind) h ON d.thedate = h.yiclkind   
    AND e.employeenumber = h.yiemp#    
  LEFT JOIN tmpOvertime o ON h.yiclkind = o.yiclkind -- one row per emp#/clockindate
    AND h.yico# = o.yico#
    AND h.yiemp# = o.yiemp#  
  WHERE d.thedate BETWEEN (select max(thedate) - 28 as theDate from day where dayofweek = 1 and thedate <= curdate()) AND curdate();
  