-- this gives dailyflag, dailyclock & cum prod (cum range SET BY JOIN BETWEEN day AND e/f
-- for a dept (detail, IN this CASE) SET BY tech# for flaghours AND distcode for clockhours
SELECT d.thedate, coalesce(max(g.flag), 0) AS dailyflag, 
  coalesce(max(h.clock), 0) AS dailyclock,
  round(sum(coalesce(e.flag, 0))/sum(coalesce(f.clock, 0)), 2) * 100 AS prod 
FROM day d
LEFT JOIN (
  SELECT a.thedate, SUM(coalesce(b.flaghours, 0)) AS flag
  FROM day a
  LEFT JOIN  factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.techkey IN (
      SELECT techkey
      FROM dimtech
      WHERE technumber IN ('619','135'))
  GROUP BY a.thedate) e ON e.thedate BETWEEN d.thedate - 3 AND d.thedate - 1
LEFT JOIN ( 
  SELECT a.thedate, SUM(coalesce(b.clockhours, 0)) AS clock
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.employeekey IN (
      SELECT employeekey 
      FROM edwEmployeeDim 
      WHERE distcode = 'WTEC')
  GROUP BY a.thedate) f ON f.thedate BETWEEN d.thedate - 3 AND d.thedate - 1 
LEFT JOIN (
  SELECT a.thedate, SUM(coalesce(b.flaghours, 0)) AS flag
  FROM day a
  LEFT JOIN  factTechFlagHoursByDay b ON a.datekey = b.flagdatekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.techkey IN (
      SELECT techkey
      FROM dimtech
      WHERE technumber IN ('619','135'))
  GROUP BY a.thedate) g ON d.thedate = g.thedate
LEFT JOIN ( 
  SELECT a.thedate, SUM(coalesce(b.clockhours, 0)) AS clock
  FROM day a
  LEFT JOIN edwClockHoursFact b ON a.datekey = b.datekey
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate() - 1
    AND b.employeekey IN (
      SELECT employeekey 
      FROM edwEmployeeDim 
      WHERE distcode = 'WTEC')
  GROUP BY a.thedate) h ON d.thedate = h.thedate
WHERE d.thedate BETWEEN curdate() - 113 AND curdate() - 1  
GROUP BY d.thedate 

-- now what i want IS to store this daily BY tech
-- cartesian tech/distcode nahhh
-- daily BY tech
-- start out with date, techkey, dailyflag, dailyclock, dailyprod,
-- thinking this IS a report TABLE
-- include technumber, name, distcode, dailyflag, dailyclock, dailyprod, servicetype, writer, ro?, roline?
-- these fields are ALL the mix AND match, slice AND dice ways to look at flag/clock

SELECT c.thedate, c.employeekey, coalesce(d.flaghours, 0) AS flaghours
INTO #flag
FROM (
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM factTechFlagHoursByDay
    WHERE employeekey > 0) b
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate()) c
LEFT JOIN factTechFlagHoursByDay d ON c.datekey = d.flagdatekey
  AND c.employeekey = d.employeekey
  
-- 1 row for each day/empkey WHERE empkey has ever flagged hours
SELECT c.thedate, c.employeekey, coalesce(d.clockhours, 0) AS clockhours   
INTO #clock
FROM (
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM edwClockHoursFact 
    WHERE employeekey IN (
      SELECT DISTINCT employeekey
      FROM factTechFlagHoursByDay 
      WHERE employeekey > 0)) b
  WHERE a.thedate BETWEEN curdate() - 120 AND curdate()) c
LEFT JOIN edwClockHoursFact d ON c.datekey = d.datekey
  AND c.employeekey = d.employeekey;   
  
  
SELECT c.thedate, c.employeekey, coalesce(d.flaghours, 0) AS flaghours 
DROP TABLE #flag
INTO #flag
FROM ( -- 365 rows (one for each day) for each empkey with any flaghours IN factTechFlagHoursByDay
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM factTechFlagHoursByDay
    WHERE employeekey > 0) b
  WHERE a.thedate BETWEEN curdate() - 365 AND curdate() - 1) c
LEFT JOIN factTechFlagHoursByDay d ON c.datekey = d.flagdatekey
  AND c.employeekey = d.employeekey;  
  
SELECT c.thedate, c.employeekey, coalesce(d.clockhours, 0) AS clockhours  
--DROP TABLE #clock;
INTO #clock 
FROM ( -- 365 rows (one for each day) for each empkey with any clockhours IN edwClockHoursFact AND also EXISTS IN factTechFlagHoursByDay
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM edwClockHoursFact 
    WHERE employeekey IN (
      SELECT DISTINCT employeekey
      FROM factTechFlagHoursByDay 
      WHERE employeekey > 0)) b
  WHERE a.thedate BETWEEN curdate() - 365 AND curdate() - 1) c
LEFT JOIN edwClockHoursFact d ON c.datekey = d.datekey
  AND c.employeekey = d.employeekey;      
  
-- so the number of records IN each should be the same , AND they are
select COUNT(*) FROM #flag
SELECT COUNT(*) FROM #clock
  
SELECT *
FROM #flag a 
full JOIN #clock b ON a.thedate = b.thedate
  AND a.employeekey = b.employeekey

-- what about non empkey flaghours, still tied to a technumber, feels LIKE it should ALL be IN one table  
ok, this IS why i was going for tech through bridge
DO i change the filter IN #clock? so that would be any empkey IN edwClockHoursFact for which a techkey EXISTS IN 
fuck, there will never be
link BETWEEN clockhours for an empkey with distcode WTEC AND flag hours for dept techs
thinking plump up #flag & #clock with ALL i can tell about each
/*** CLOCK WILL HAVE TO ACCOMODATE TECHTIMEADJUSTMENTS ***/

-- THIS should be 365 rows for every employeekey IN edwClockHoursFact 365*902 dist empkey = 39230
SELECT COUNT(*) FROM (
SELECT c.thedate, c.employeekey, coalesce(d.clockhours, 0) AS clockhours  
--DROP TABLE #clock;
-- INTO #clock 
FROM ( -- 365 rows (one for each day) for each empkey with any clockhours IN edwClockHoursFact AND also EXISTS IN factTechFlagHoursByDay
  SELECT a.thedate, a.datekey, b.employeekey
  FROM day a, (
    SELECT DISTINCT employeekey
    FROM edwClockHoursFact) b
  WHERE a.thedate BETWEEN curdate() - 365 AND curdate() - 1) c
LEFT JOIN edwClockHoursFact d ON c.datekey = d.datekey
  AND c.employeekey = d.employeekey) x; 
  
SELECT COUNT(DISTINCT employeekey)
FROM edwClockHoursFact  

IS it premature to have the day added IN, i think so
but we can limit it too the past year

-- clockhours by day with empkey, emp#, name, store, dept, distcode, technumber
SELECT b.thedate, c.storecode, a.employeekey, c.employeenumber, c.name, c.pydept, 
  c.pydeptcode, c.distcode, coalesce(d.technumber, 'NA') AS technumber, clockhours
FROM edwClockHoursFact a
inner JOIN day b ON a.datekey = b.datekey
  and b.thedate BETWEEN curdate() - 365 AND curdate() - 1
LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
LEFT JOIN dimtech d ON c.storecode = d.storecode
  AND c.employeenumber = d.employeenumber
  
-- clock hours BY day/dept/distcode
SELECT thedate, storecode, pydept, distcode, SUM(clockhours) AS clockhours
FROM (
  SELECT b.thedate, a.employeekey, clockhours, c.storecode, c.name, c.pydept, c.pydeptcode, c.distcode, coalesce(d.technumber, 'NA') 
  FROM edwClockHoursFact a
  inner JOIN day b ON a.datekey = b.datekey
    and b.thedate BETWEEN curdate() - 365 AND curdate() - 1
  LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
  LEFT JOIN dimtech d ON c.storecode = d.storecode
    AND c.employeenumber = d.employeenumber) x
GROUP BY thedate, storecode, pydept, distcode 

-- this IS disturbing
-- STEC total clockhours/day fluctuates wildly BETWEEN 190-230
SELECT thedate, storecode, pydept, distcode, SUM(clockhours) AS clockhours
FROM (
  SELECT b.thedate, a.employeekey, clockhours, c.storecode, c.name, c.pydept, c.pydeptcode, c.distcode, coalesce(d.technumber, 'NA') 
  FROM edwClockHoursFact a
  inner JOIN day b ON a.datekey = b.datekey
    and b.thedate BETWEEN '03/01/2012' AND curdate() - 1
  LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
  LEFT JOIN dimtech d ON c.storecode = d.storecode
    AND c.employeenumber = d.employeenumber) x
WHERE storecode = 'ry1'
  AND distcode = 'stec'   
  AND dayofweek(thedate) BETWEEN 2 AND 6 
GROUP BY thedate, storecode, pydept, distcode   
-- so let's see who worked ON 6/22, total IS only 185 hrs  
SELECT name, technumber, clockhours 
FROM (
  SELECT b.thedate, a.employeekey, clockhours, c.storecode, c.name, c.pydept, c.pydeptcode, c.distcode, 
    coalesce(d.technumber, 'NA') AS technumber
  FROM edwClockHoursFact a
  inner JOIN day b ON a.datekey = b.datekey
    and b.thedate BETWEEN curdate() - 365 AND curdate() - 1
  LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
  LEFT JOIN dimtech d ON c.storecode = d.storecode
    AND c.employeenumber = d.employeenumber) z
WHERE thedate = '06/22/2012'
  AND storecode = 'ry1'
  AND distcode = 'stec'  
ORDER BY clockhours

-- this IS ok, but what i really want IS a full OUTER JOIN
-- but can't DO OUTER JOIN ON subqueries
-- so need to derive a list of ALL techs for both days AND AND use that AS the basis table
SELECT a.name, a.technumber, a.clockhours AS [7/18], b.clockhours AS [6/22]
FROM (
  SELECT name, technumber, clockhours 
  FROM (
    SELECT b.thedate, a.employeekey, clockhours, c.storecode, c.name, c.pydept, c.pydeptcode, c.distcode, 
      coalesce(d.technumber, 'NA') AS technumber
    FROM edwClockHoursFact a
    inner JOIN day b ON a.datekey = b.datekey
      and b.thedate BETWEEN curdate() - 365 AND curdate() - 1
    LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
    LEFT JOIN dimtech d ON c.storecode = d.storecode
      AND c.employeenumber = d.employeenumber) z
  WHERE thedate = '07/18/2012'
    AND storecode = 'ry1'
    AND distcode = 'stec') a  
LEFT JOIN (
  SELECT name, technumber, clockhours 
  FROM (
    SELECT b.thedate, a.employeekey, clockhours, c.storecode, c.name, c.pydept, c.pydeptcode, c.distcode, 
      coalesce(d.technumber, 'NA') AS technumber
    FROM edwClockHoursFact a
    inner JOIN day b ON a.datekey = b.datekey
      and b.thedate BETWEEN curdate() - 365 AND curdate() - 1
    LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
    LEFT JOIN dimtech d ON c.storecode = d.storecode
      AND c.employeenumber = d.employeenumber) z
  WHERE thedate = '06/22/2012'
    AND storecode = 'ry1'
    AND distcode = 'stec') b ON a.name = b.name  
ORDER BY b.clockhours   
-- so need to derive a list of ALL techs for both days AND AND use that AS the basis table
SELECT a.name, a.technumber, a.clockhours AS [7/18], b.clockhours AS [6/22]
FROM ( -- techs with
  SELECT d.technumber
  FROM edwClockHoursFact a
  inner JOIN day b ON a.datekey = b.datekey
    and b.thedate BETWEEN curdate() - 365 AND curdate() - 1
  LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
  LEFT JOIN dimtech d ON c.storecode = d.storecode
    AND c.employeenumber = d.employeenumber 
  WHERE b.thedate = '06/22/2012'
    AND c.storecode = 'ry1'
    AND c.distcode = 'stec'
  UNION  
  SELECT d.technumber
  FROM edwClockHoursFact a
  inner JOIN day b ON a.datekey = b.datekey
    and b.thedate BETWEEN curdate() - 365 AND curdate() - 1
  LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
  LEFT JOIN dimtech d ON c.storecode = d.storecode
    AND c.employeenumber = d.employeenumber 
  WHERE b.thedate = '07/18/2012'
    AND c.storecode = 'ry1'
    AND c.distcode = 'stec') m
LEFT JOIN (
  SELECT name, technumber, clockhours 
  FROM (
    SELECT b.thedate, a.employeekey, clockhours, c.storecode, c.name, c.pydept, c.pydeptcode, c.distcode, 
      coalesce(d.technumber, 'NA') AS technumber
    FROM edwClockHoursFact a
    inner JOIN day b ON a.datekey = b.datekey
      and b.thedate BETWEEN curdate() - 365 AND curdate() - 1
    LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
    LEFT JOIN dimtech d ON c.storecode = d.storecode
      AND c.employeenumber = d.employeenumber) z
  WHERE thedate = '07/18/2012'
    AND storecode = 'ry1'
    AND distcode = 'stec') a ON m.technumber = a.technumber  
LEFT JOIN (
  SELECT name, technumber, clockhours 
  FROM (
    SELECT b.thedate, a.employeekey, clockhours, c.storecode, c.name, c.pydept, c.pydeptcode, c.distcode, 
      coalesce(d.technumber, 'NA') AS technumber
    FROM edwClockHoursFact a
    inner JOIN day b ON a.datekey = b.datekey
      and b.thedate BETWEEN curdate() - 365 AND curdate() - 1
    LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
    LEFT JOIN dimtech d ON c.storecode = d.storecode
      AND c.employeenumber = d.employeenumber) z
  WHERE thedate = '06/22/2012'
    AND storecode = 'ry1'
    AND distcode = 'stec') b ON m.technumber = b.technumber   
-- that's an ok solution
-- shows kirby howerda with NULL clockhours 6/22, aha, he was classified
-- AS PTEC until 7/'6
-- but what i'm really getting at IS how many techs are employed ON any given day
-- the mind fuck IS worrying about payroll data NOT being accurate
        
 
    

  
-- now what i want IS to store this daily BY tech
-- cartesian tech/distcode nahhh
-- daily BY tech
-- start out with date, techkey, dailyflag, dailyclock, dailyprod,
-- thinking this IS a report TABLE
-- include technumber, name, distcode, dailyflag, dailyclock, dailyprod, servicetype, writer, paytype, ro?, roline?
-- these fields are ALL the mix AND match, slice AND dice ways to look at flag/clock  

-- hmm, need to start with flaghours BY date, the finest grain first?
-- because i want to END up with 1 row per techkey per day
-- AND that IS the grain of factTechFlagHoursByDay

SELECT b.thedate, a.*, c.*
FROM factTechFlagHoursByDay a
LEFT JOIN day b ON a.flagdatekey = b.datekey
-- out of these 2 i want tech/dey
LEFT JOIN bridgeTechGroup c ON a.techkey = c.techkey
LEFT JOIN factTechLineFlagDateHours c ON 
-- shit, that's NOT going to happen
-- a tech will flag multiple ros, with multiple paytypes
-- AND will a tech have multiple

SELECT b.thedate AS linedate, a.*, c.*
FROM factroline a
LEFT JOIN day b ON a.linedatekey = b.datekey
LEFT JOIN facttechlineflagdateHours c ON a.storecode = c.storecode
  AND a.ro  = c.ro
  AND a.line = c.line
WHERE c.storecode IS NOT NULL   
ORDER BY linedate

SELECT b.thedate, a.*
FROM factroline a
LEFT JOIN day b ON a.linedatekey = b.datekey
ORDER BY b.thedate

TABLE rptFlagHours ( 
      theDate Date,
      DateKey Integer,
      StoreCode CIChar( 3 ),
      EmployeeKey Integer,
      Employeenumber CIChar( 7 ),
      Name CIChar( 25 ),
      pyDept CIChar( 30 ),
      pyDeptCode CIChar( 2 ),
      DistCode CIChar( 4 ),
      Distribution CIChar( 25 ),
      TechNumber CIChar( 3 ),
      FlagHours Double( 15 )

EXECUTE PROCEDURE sp_zaptable('rptFlagHours');      
INSERT INTO rptFlagHours
SELECT b.thedate, b.datekey, d.storecode, a.employeekey, c.employeenumber, coalesce(c.name, d.description) AS name, c.pydept, 
  c.pydeptcode, c.distcode, distribution, d.technumber, flaghours
FROM factTechFlagHoursByDay a
INNER JOIN day b ON a.flagdatekey = b.datekey
LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey 
LEFT JOIN dimtech d ON a.techkey = d.techkey
ORDER BY thedate desc

SELECT *
FROM rptFlagHours

SELECT b.storecode, b.technumber, b.name, SUM(clockhours) AS clockhours, SUM(flaghours) AS flaghours
FROM rptClockhours a
full JOIN rptFlagHours b ON a.storecode = b.storecode
  AND a.thedate = b.thedate
  AND a.technumber = b.technumber
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
  AND b.thedate BETWEEN '06/01/2012' AND '06/30/2012' 
  AND b.storecode = 'ry1'
  AND a.technumber <> 'NA'
  AND a.pydept = 'service'  
GROUP BY b.storecode, b.technumber, b.name 


SELECT a.*, b.storecode, b.technumber, b.name, b.thedate
FROM rptClockhours a
full JOIN rptFlagHours b ON a.storecode = b.storecode
  AND a.thedate = b.thedate
  AND a.technumber = b.technumber
WHERE a.storecode IS NULL 


-- ry1 service techs june 2012
SELECT b.storecode, b.technumber, b.name, SUM(clockhours) AS clockhours, SUM(flaghours) AS flaghours
FROM rptClockhours a
full JOIN rptFlagHours b ON a.storecode = b.storecode
  AND a.thedate = b.thedate
  AND a.technumber = b.technumber
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
  AND b.thedate BETWEEN '06/01/2012' AND '06/30/2012' 
  AND b.storecode = 'ry1'
  AND a.pydept = 'service'  
GROUP BY b.storecode, b.technumber, b.name 


SELECT b.thedate, b.name, clockhours, flaghours
FROM rptClockhours a
full JOIN rptFlagHours b ON a.storecode = b.storecode
  AND a.thedate = b.thedate
  AND a.technumber = b.technumber
WHERE a.pydept = 'service' 
  AND (b.thedate IS NOT NULL AND b.flaghours IS NOT NULL) 
ORDER BY b.thedate  

SELECT a.thedate, b.name, coalesce(clockhours, 0), 
  coalesce(flaghours, 0)
FROM day a
full join rptClockhours b ON a.thedate = b.thedate
full JOIN rptFlagHours c ON a.thedate = c.thedate
  and b.storecode = c.storecode
  AND b.technumber = c.technumber
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
  AND b.storecode = 'ry1'
  AND b.pydept = 'service'  
ORDER BY b.name, b.thedate  



SELECT a.thedate, b.name, coalesce(clockhours, 0), 
  coalesce(flaghours, 0)
FROM day a
full join rptClockhours b ON a.thedate = b.thedate
full JOIN rptFlagHours c ON a.thedate = c.thedate
  and b.storecode = c.storecode
  AND b.technumber = c.technumber
WHERE a.thedate BETWEEN '06/01/2012' AND '06/30/2012'
  AND (flaghours <> 0 AND clockhours = 0)


-- ros ON which tech 583 flagged hours ON 6/6
SELECT *
from factTechLineFlagDateHours a
INNER JOIN bridgeTechGroup b ON a.techgroupkey = b.techgroupkey
INNER JOIN dimtech c ON b.techkey = c.techkey
WHERE c.name LIKE 'otto%' 
  and a.flagdatekey = 3080
ORDER BY ro, line  


rptTechProductivity
-- of course beaucoup, every flag hour for deptmental techs
SELECT a.thedate, a.datekey, b.storecode, b.employeekey, b.*, c.*
FROM day a
full join rptClockhours b ON a.thedate = b.thedate
full JOIN rptFlagHours c ON a.thedate = c.thedate
  and b.storecode = c.storecode
  AND b.technumber = c.technumber
WHERE coalesce(clockhours, 0) + coalesce(flaghours) <> 0 
  and b.storecode IS NULL 

-- returns no records  
SELECT a.thedate, a.datekey, b.storecode, b.employeekey, b.*, c.*
FROM day a
full join rptClockhours b ON a.thedate = b.thedate
full JOIN rptFlagHours c ON a.thedate = c.thedate
  and b.storecode = c.storecode
  AND b.technumber = c.technumber
WHERE coalesce(clockhours, 0) + coalesce(flaghours) <> 0 
  and c.storecode IS NULL 
  

-- why IS this returning no rows  
-- ? because of the JOIN ON tech number? clock = na flag = 135
SELECT a.thedate, a.datekey, b.storecode, b.employeekey, b.*, c.*
FROM day a
full join rptClockhours b ON a.thedate = b.thedate
full JOIN rptFlagHours c ON a.thedate = c.thedate
  and b.storecode = c.storecode
  AND b.technumber = c.technumber
WHERE a.thedate > '07/01/2012' 
  AND c.storecode = 'ry1'
  AND c.technumber = '135' 
-- but this does 
SELECT a.thedate, b.*
FROM day a
full join rptFlagHours b ON a.thedate = b.thedate
WHERE a.thedate > '07/01/2012' 
  AND b.storecode = 'ry1'
  AND b.technumber = '135' 
  
  
SELECT *
FROM rptclockhours a
full JOIN rptflaghours b ON a.thedate = b.thedate
  AND a.storecode = b.storecode
  AND a.technumber = b.technumber
WHERE a.thedate = '07/02/2012'  
  AND coalesce(clockhours, 0) + coalesce(flaghours, 0) <> 0
ORDER BY b.technumber
  
  
SELECT a.thedate, b.*, c.*
FROM day a
full join rptClockhours b ON a.thedate = b.thedate
full JOIN rptFlagHours c ON a.thedate = c.thedate
  and b.storecode = c.storecode
  AND b.technumber = c.technumber
WHERE a.thedate = '07/02/2012' 
   AND coalesce(clockhours, 0) + coalesce(flaghours, 0) <> 0
ORDER BY c.technumber   
 
SELECT a.thedate, b.*
FROM day a
full join rptFlagHours b ON a.thedate = b.thedate
WHERE a.thedate = '07/02/2012' 
  AND b.storecode = 'ry1'
  AND b.technumber = '135' 
  
SELECT *
FROM rptclockhours
WHERE thedate = '07/02/2012'
UNION 
SELECT *
FROM rptflaghours
WHERE thedate = '07/02/2012'  
    
    
SELECT a.thedate, b.*, c.* --b.technumber, b.clockhours
FROM day a
LEFT JOIN rptclockhours b ON a.thedate = b.thedate  
  AND b.technumber <> 'NA'
LEFT JOIN rptflaghours c ON a.thedate = c.thedate
WHERE a.thedate = '07/12/2012'

SELECT *
FROM rptclockhours
WHERE thedate = '07/02/2012'
  AND technumber <> 'na'
  
-- wait a fucking minute, i'm trying to get tech productivity, NOT dept productivity
SELECT a.thedate, b.storecode, b.employeekey, b.employeenumber, b.name, b.pydept, 
  b.distcode, b.technumber, coalesce(b.clockhours, 0) AS clockhours, 
  coalesce(c.flaghours, 0) AS flaghours
FROM day a
full join rptClockhours b ON a.thedate = b.thedate
  AND b.technumber <> 'NA'
full JOIN rptFlagHours c ON a.thedate = c.thedate
  and b.storecode = c.storecode
  AND b.technumber = c.technumber
WHERE a.thedate BETWEEN '09/01/2009' AND curdate() - 1

-- monthly (calendar)
SELECT y.*, round(flaghours/clockhours, 2) * 100
FROM (
  SELECT storecode, name, technumber, month(thedate), year(thedate), 
    SUM(clockhours) AS clockhours, SUM(flaghours) AS flaghours
  FROM (
    SELECT a.thedate, b.storecode, b.employeekey, b.employeenumber, b.name, b.pydept, 
      b.distcode, b.technumber, coalesce(b.clockhours, 0) AS clockhours, 
      coalesce(c.flaghours, 0) AS flaghours
    FROM day a
    full join rptClockhours b ON a.thedate = b.thedate
      AND b.technumber <> 'NA'
    full JOIN rptFlagHours c ON a.thedate = c.thedate
      and b.storecode = c.storecode
      AND b.technumber = c.technumber
    WHERE a.thedate BETWEEN '09/01/2009' AND curdate() - 1) x
  GROUP BY storecode, name, technumber, month(thedate), year(thedate)
  HAVING SUM(clockhours) <> 0) y 


-- so i want
-- daily clock/flag
-- prod for prev 14
-- prod for payperiod to date
-- prod for month to date
SELECT a.thedate, a.thedate - 14 AS prev14Begin, a.thedate - 1 AS prev14End
FROM day a
WHERE a.thedate BETWEEN '09/01/2009' AND curdate() - 1


SELECT a.storecode, a.ro, a.line, a.servicetype, a.opcode, d.technumber
FROM factroline a
LEFT JOIN factTechLineFlagDateHours b ON a.storecode = b.storecode
  AND a.ro = b.ro
  AND a.line = b.line
LEFT JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey  
LEFT JOIN dimtech d ON c.techkey = d.techkey
WHERE a.lineflaghours > 0


SELECT storecode, ro, line
FROM (
  SELECT a.storecode, a.ro, a.line, a.servicetype, a.opcode, d.technumber
  FROM factroline a
  LEFT JOIN factTechLineFlagDateHours b ON a.storecode = b.storecode
    AND a.ro = b.ro
    AND a.line = b.line
  LEFT JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey  
  LEFT JOIN dimtech d ON c.techkey = d.techkey
  WHERE a.lineflaghours > 0) x
GROUP BY storecode, ro, line
HAVING COUNT(*) > 1

-- lines with more than one tech
SELECT storecode, ro, line
FROM (
SELECT a.storecode, a.ro, a.line, d.technumber
FROM factroline a
LEFT JOIN factTechLineFlagDateHours b ON a.storecode = b.storecode
  AND a.ro = b.ro
  AND a.line = b.line
LEFT JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey  
LEFT JOIN dimtech d ON c.techkey = d.techkey
WHERE a.lineflaghours > 0
GROUP BY a.storecode, a.ro, a.line, d.technumber) x
GROUP BY storecode, ro, line
HAVING COUNT(*) > 1


SELECT a.storecode, a.ro, a.line, a.servicetype, a.opcode, d.technumber, a.lineflaghours,
  d.technumber, b.flaghours
FROM factroline a
LEFT JOIN factTechLineFlagDateHours b ON a.storecode = b.storecode
  AND a.ro = b.ro
  AND a.line = b.line
LEFT JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey  
LEFT JOIN dimtech d ON c.techkey = d.techkey
WHERE a.lineflaghours > 0
  AND weightfactor <> 1 -- to see how time IS split
  
  
SELECT e.opendatekey, a.storecode, a.ro, a.line, a.servicetype, a.opcode, a.corcode, 
  d.technumber, a.lineflaghours, b.flaghours
FROM factro e  
INNER JOIN factroline a ON e.storecode = a.storecode
  AND e.ro = a.ro
LEFT JOIN factTechLineFlagDateHours b ON a.storecode = b.storecode
  AND a.ro = b.ro
  AND a.line = b.line
LEFT JOIN bridgetechgroup c ON b.techgroupkey = c.techgroupkey  
LEFT JOIN dimtech d ON c.techkey = d.techkey
WHERE a.lineflaghours > 0
  AND weightfactor <> 1  
