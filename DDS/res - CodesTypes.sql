create table resArkonaCodes (
      system CIChar( 30 ),
      arkTableName CIChar( 60 ),
      arkColumnName CIChar( 60 ),
      stgTableName CIChar( 60 ),
      stgColumnName CIChar( 60 ),
      edwTableName CIChar( 60 ),
      edwColumnName CIChar( 60 ),
      xfmTableName CIChar( 60 ),
      xfmColumnName CIChar( 60 ),
      code CIChar( 12 ),
      codeDesc CIChar( 30 ),
      populatedBy CIChar( 250 ),
	  storeCode CIChar(3)) IN database;
 

-- Store Codes  
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, codeDesc, populatedBy, storeCode)
  values('Arkona.Rydedata','PYMAST','YMCO#','stgArkonaPYMAST', 'YMCO#', 'RY1','Rydell', 'manual entry', 'ALL');
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, codeDesc, populatedBy, storeCode)
  values('Arkona.Rydedata','PYMAST','YMCO#','stgArkonaPYMAST', 'YMCO#','RY2','Honda', 'manual entry', 'ALL');
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, codeDesc, populatedBy, storeCode)
  values('Arkona.Rydedata','PYMAST','YMCO#','stgArkonaPYMAST', 'YMCO#','RY3','Crookston', 'manual entry', 'ALL');
-- 1/9/12 mod to separate act/term AND full/part
/*  -- orig
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, codeDesc, populatedBy, storeCode)
   values('Arkona.Rydedata','PYMAST','YMACTV','stgArkonaPYMAST', 'YMACTV#','A','Full', 'manual entry', 'ALL');
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, codeDesc, populatedBy, storeCode)
  values('Arkona.Rydedata','PYMAST','YMACTV','stgArkonaPYMAST', 'YMACTV#','P','Part', 'manual entry', 'ALL');
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, codeDesc, populatedBy, storeCode)
  values('Arkona.Rydedata','PYMAST','YMACTV','stgArkonaPYMAST', 'YMACTV#','T','Term', 'manual entry', 'ALL');
*/ 
-- act/term
-- thinking fuckit, this IS a one off, decode it IN the stored proc
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, xfmTableName, xfmColumnName, code, codeDesc, populatedBy, storeCode)
  values('Arkona.Rydedata','PYMAST','YMACTV','stgArkonaPYMAST', 'YMACTV#',
  'xfmEmployeeDim', 'ActiveCode', 'T', 'Term', 'manual entry', 'ALL');
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, xfmTableName, xfmColumnName, code, codeDesc, populatedBy, storeCode)
  values('Arkona.Rydedata','PYMAST','YMACTV','stgArkonaPYMAST', 'YMACTV#',
  'xfmEmployeeDim', 'ActiveCode', 'A','Active', 'manual entry', 'ALL');  
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, xfmTableName, xfmColumnName, codeDesc, populatedBy, storeCode)
  values('Arkona.Rydedata','PYMAST','YMACTV','stgArkonaPYMAST', 'YMACTV#',
  'xfmEmployeeDim', 'ActiveCode', 'P','Active', 'manual entry', 'ALL');  
  
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, codeDesc, populatedBy, storeCode)
   values('Arkona.Rydedata','PYMAST','YMACTV','stgArkonaPYMAST', 'YMACTV#','A','Full', 'manual entry', 'ALL');
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, codeDesc, populatedBy, storeCode)
  values('Arkona.Rydedata','PYMAST','YMACTV','stgArkonaPYMAST', 'YMACTV#','P','Part', 'manual entry', 'ALL');  
  
-- PYPCLKCTL
INSERT into resArkonaCodes (system, arkTableName, arkColumnName, 
  stgTableName, stgColumnName, code, codeDesc, populatedBy, storeCode)
--  values('Arkona.Rydedata','PYMAST','YMCO#','stgArkonaPYMAST', 'YMCO#', 'RY1','Rydell', 'manual entry', 'ALL')
SELECT 'Arkona.Rydedata', 'PYPCLKCTL', 'YICDEPT', 'stgArkonaPYPCLKCTL','yicdept', yicdept, yicdesc, 'from PYPCLKCTL', yicco#
FROM stgArkonaPYPCLKCTL
-- 12/27/11 departments have been added/changed
-- DELETE ALL dept info & reload
DELETE FROM resArkonaCodes
WHERE arkTableName = 'PYPCLKCTL'

-- PYPCLOCKIN
SELECT distinct 'Arkona.Rydedata', 'PYPCLOCKIN', 'YICODE', 'stgArkonaPYPCLOCKIN','yicode', yicode, 'blah blah blah', 'from PYPCLKCTL', yico#
FROM stgArkonaPYPCLOCKIN
ORDER BY yicode, yico#