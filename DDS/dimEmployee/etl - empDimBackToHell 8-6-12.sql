-- this IS a diversion FROM stgTechClockTimeAdjustments.sql
the issue IS that empkeyfrom/thru IS being used to determine the period of validity for 
an empkey
IF someone IS termed, but the empkeythrudate IS LEFT at 12/31/9999
THEN 
  edwClockHoursFact rows are generated?
  the

this also brings up the unresolved question of, WHEN an emp IS termed (type1 change) DO ALL instances
of that employeenumber get updated to termed?

so IF ALL instances get updated to termed, that means that the grain of hire-fire IS emp# NOT empkey

so IS an empkey still valid post term?
  
IS it the same old fucking question of what IS the period of employment
  1. hiredate -> termdate, this should be better now since the previous back to hell fixes (rehire/reuse consistency) 

-- ahh fuck, this turns INTO another etl - empDimBackToHell 8-6-12 
-- this gives some rows with NULL edwClockHoursFact data
SELECT a.thedate, b.employeekey, b.name, d.technumber, c.clockhours, c.vacationhours, c.ptohours, c.holidayhours
FROM day a
LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.pydept = 'service'
  AND b.distcode = 'stec'
LEFT JOIN edwClockHoursFact c ON a.datekey = c.datekey
  AND b.employeekey = c.employeekey  
LEFT JOIN dimTech d ON b.employeenumber = d.employeenumber  
WHERE a.thedate BETWEEN '11/06/2011' AND curdate() - 1
  AND clockhours IS NULL 
  
SELECT b.employeekey, b.name, MIN(thedate), MAX(thedate)
FROM day a
LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.pydept = 'service'
  AND b.distcode = 'stec'
LEFT JOIN edwClockHoursFact c ON a.datekey = c.datekey
  AND b.employeekey = c.employeekey  
LEFT JOIN dimTech d ON b.employeenumber = d.employeenumber  
WHERE a.thedate BETWEEN '11/06/2011' AND curdate() - 1
  AND clockhours IS NULL 
GROUP BY b.employeekey, b.name    

schwann
empkey  hiredate  termdate    ekfrom     ekthru
968     5/16/05   9/10/2010   5/16/05    2/26/12 
1494    2/27/12               2/27/12
syverson                                         
919     10/27/05  9/11/10     10/27/05   6/10/12
1634    6/11/12               6/11/12    7/5/12

-- fuck, they are both rehire/reuse
-- ekthrudate should be the termdate        
-- i think these need to be fixed
-- AND maybe ADD another test

SELECT 
  case when termdate <> employeekeythrudate then 'X' end,
  a.*
FROM vempdim a
WHERE employeenumber IN (
  SELECT employeenumber
  FROM vempdim 
  WHERE termdate <> '12/31/9999'  
    AND termdate <> employeekeythrudate) 
ORDER BY employeenumber        


SELECT b.employeekey, b.name, MIN(thedate), MAX(thedate)
FROM day a
LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  AND b.pydept = 'service'
  AND b.distcode = 'stec'
LEFT JOIN edwClockHoursFact c ON a.datekey = c.datekey
  AND b.employeekey = c.employeekey  
LEFT JOIN dimTech d ON b.employeenumber = d.employeenumber  
WHERE a.thedate BETWEEN '11/06/2011' AND curdate() - 1
  AND clockhours IS NULL 
GROUP BY b.employeekey, b.name

SELECT b.employeekey, b.name, MIN(thedate), MAX(thedate)
FROM day a
LEFT JOIN edwEmployeeDim b ON a.thedate BETWEEN b.employeekeyfromdate AND b.termdate
  AND b.pydept = 'service'
  AND b.distcode = 'stec'
LEFT JOIN edwClockHoursFact c ON a.datekey = c.datekey
  AND b.employeekey = c.employeekey  
LEFT JOIN dimTech d ON b.employeenumber = d.employeenumber  
WHERE a.thedate BETWEEN '11/06/2011' AND curdate() - 1
  AND clockhours IS NULL 
GROUP BY b.employeekey, b.name

-- 8/7 what IS the fucking issue here
-- edwClockHoursFact records ON "invalid" employeekeys ???
-- a big problem seems to be inconsistent handling of employeekeythrudate
SELECT *
FROM vempdim a
WHERE termdate <> '12/31/9999'
  AND termdate = employeekeythrudate
-- vs
SELECT *
FROM vempdim a
WHERE termdate <> '12/31/9999'
  AND termdate <> employeekeythrudate
  
--8/8
emp#          empkey
type1         type2  
core issue: who will the data be used
     what''s the meaning of the empkey duration
currently using the empkey to determine the correct instance of an emp# ON a given day
FROM xfmEmpDim
Type 1: HireDate, BirthDate, Active, TermDate
Type 2: Name, FullPartTimeCode, PyDeptCode, PyDep, DistCode, Distribution, PayrollClassCode, Salary, HourlyRate, PayPeriodCode    

so, since termdate IS type1 it applies to ALL instances of the emp# 
that''s simple AND clear enough, therefore ALL empkeys for an emp# have the same termdate
that''s fine
what about empkeyfrom/thru
the hesitation comes FROM the unreliability of hr data

clockhours post termination

IS there such a thing an empkey duration AND what does it mean
it means the period of time for which that description of that employee IS true
IF the employee IS no longer employed, IS there any value to that description being tied
   to a date post termination
   
currently employed: OPEN empkeythru

ALL of the records WHERE termdate <> ekthrudate (except 1 historical anomaly) are
  the first instance of a rehire/reuse


UPDATE edwEmployeeDim
  SET employeekeythrudate = x.termdate,
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = x.termdate)
FROM edwEmployeeDim x
WHERE termdate <> '12/31/9999'
  AND termdate <> employeekeythrudate     
  
now, anomalous data ?
go back to WHERE this diversion came FROM 

SELECT b.thedate, a.*
FROM edwClockHoursFact a
INNER JOIN day b ON a.datekey = b.datekey  
LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey
  AND b.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
WHERE c.employeekey IS NULL   
  
SELECT a.employeekey, MIN(thedate), MAX(thedate)
FROM edwClockHoursFact a
INNER JOIN day b ON a.datekey = b.datekey  
LEFT JOIN edwEmployeeDim c ON a.employeekey = c.employeekey
  AND b.thedate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
WHERE c.employeekey IS NULL   
GROUP BY a.employeekey
HAVING SUM (clockhours) > 0
    
-- oh shit, back to termdate
SELECT *
FROM vempdim
WHERE employeenumber = '1109110'   

-- this shows a bunch of termed emp#s with non OPEN empkey termdates
SELECT *
FROM vempdim a
WHERE EXISTS (
  SELECT 1
  FROM vempdim
  WHERE employeenumber = a.employeenumber
  AND termdate <> '12/31/9999')
ORDER BY employeenumber, hiredate  

-- 8/9
still looking for clarity

shit, IF term IS type1 AND active IS type1
THEN every instance of an emp# will be termed


-- termed emp# with multiple instances of emp#, NOT ALL of which are termed
SELECT *
FROM edwEmployeeDim a
WHERE EXISTS (
  SELECT 1
  FROM edwEmployeeDim
  WHERE employeenumber = a.employeenumber
    AND active = 'term')
  AND EXISTS (
    SELECT 1
    FROM edwEmployeeDim 
    WHERE employeenumber = a.employeenumber
      AND active = 'active')    
  AND employeenumber NOT IN (
    SELECT employeenumber
    FROM edwEmployeeDim 
    WHERE rowchangereason = 'Rehire/Reuse')      
ORDER BY employeenumber, employeekey 


-- this looks good
-- will have to DO rehire/reuse manually          
UPDATE edwEmployeeDim 
  SET ActiveCode = 'T',
      Active = 'Term',
      TermDate = x.TermDate,
      TermDateKey = x.TermDateKey
-- SELECT *      
FROM (
  SELECT StoreCode, Employeenumber, TermDate, TermDateKey
  FROM edwEmployeeDim a
  WHERE EXISTS (
    SELECT 1
    FROM edwEmployeeDim
    WHERE employeenumber = a.employeenumber
      AND active = 'term')
    AND EXISTS (
      SELECT 1
      FROM edwEmployeeDim 
      WHERE employeenumber = a.employeenumber
        AND active = 'active')    
    AND employeenumber NOT IN (
      SELECT employeenumber
      FROM edwEmployeeDim 
      WHERE rowchangereason = 'Rehire/Reuse')
    AND termdate <> '12/31/9999') x
LEFT JOIN edwEmployeeDim e ON  x.StoreCode = e.StoreCode
  AND x.Employeenumber = e.Employeenumber              
          
          
    

SELECT *
FROM vempdim
WHERE employeenumber IN ('11720','133290','164990','168826','174150','175360','181210','181410','183202','263120','268007','268200','291785')
ORDER BY employeenumber, employeekey

-- will have to DO rehire/reuse manually 
-- NOT ALL will have to be updated
-- only those with type 2 changes ON during the the interval of an employemnet
-- only paige & issa
SELECT name, employeekey, employeenumber, active, hiredate, termdate, employeekeyfromdate, 
  employeekeythrudate, rowchangereason 
FROM vempdim a
WHERE EXISTS (
  SELECT 1
  FROM vempdim
  WHERE employeenumber = a.employeenumber
    AND active = 'term')
  AND EXISTS (
    SELECT 1
    FROM vempdim 
    WHERE employeenumber = a.employeenumber
      AND active = 'active')    
  AND employeenumber IN (
    SELECT employeenumber
    FROM vempdim 
    WHERE rowchangereason = 'Rehire/Reuse')      
ORDER BY employeenumber, employeekey 

-- will have to DO rehire/reuse manually   
1109110,169850      
UPDATE edwEmployeeDim 
  SET ActiveCode = 'T',
      Active = 'Term',
      TermDate = '07/22/2012',
      TermDateKey = (SELECT datekey FROM day WHERE thedate = '07/22/2012')
WHERE employeekey = 1526    

-- ALL FIXED --
now fix xfmEmpDIM