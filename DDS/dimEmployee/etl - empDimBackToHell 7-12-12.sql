-- 7/26 this IS turning INTO an edwEmployeeDim analysis, finding bad data
-- 2 categories rehire/reuse AND term
-- i'm wondering IF the terms are inclusive  to a date range: 
-- this query IS ekfrom > ekthru
-- NOT quite that simple
SELECT employeekey, storecode, employeenumber, activecode, pydept, distcode, name, 
  hiredate, hiredatekey, termdate, termdatekey, rowchangedate, employeekeyfromdate, 
  employeekeyfromdatekey, employeekeythrudate, employeekeythrudatekey, currentrow, rowchangereason
FROM edwEmployeeDim
WHERE employeenumber IN (
  SELECT employeenumber 
  FROM edwEmployeeDim
  WHERE employeekeyfromdate > employeekeythrudate)
ORDER BY employeenumber, employeekeyfromdate
-- i'm wondering IF the terms are inclusive  to a date range: definitely not
SELECT termdate, COUNT(*)
FROM edwEmployeeDim
GROUP BY termdate
ORDER BY COUNT(*)

-- let's pick apart an example
-- mike tweeten
shows rowchangereason AS rehire/reuse
SELECT employeekey, storecode, employeenumber, activecode, pydept, distcode, name, 
  hiredate, termdate, rowchangedate, employeekeyfromdate, 
  employeekeythrudate, currentrow, rowchangereason
FROM edwEmployeeDim
WHERE employeenumber = '142460'

-- hmm, what DO i ON term

-- FROM xfmEmpDim
// Rehire/Reuse: type 2 changes
    TRY 
      SELECT Utilities.DropTablesIfExist('#Rehire') FROM system.iota;
      -- DROP TABLE #Rehire
/*
rehire/reuse: edwEmployeeDim selfjoin
an emp# (FROM db2 scrap) that already EXISTS WHERE the existing emp# is Termed AND the new emp# IS Active
*/      
      SELECT edwED.EmployeeKey, x.*	
      INTO #Rehire 
      FROM xfmEmployeeDim x
      LEFT JOIN edwEmployeeDim edwED ON x.StoreCode = edwED.StoreCode
        AND x.employeeNumber = edwED.EmployeeNumber
        AND edwED.CurrentRow = true
      WHERE edwED.ActiveCode = 'T' 
        AND x.ActiveCode IN ('P','A');
    
      IF (SELECT COUNT(*) FROM #Rehire) > 0 THEN 
    -- UPDATE the old record
        UPDATE edwEmployeeDim 
        SET RowChangeDate = CAST(@NowTS AS sql_date),
            RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = CAST(@NowTS AS sql_date)),
            RowThruTS = @NowTS,
            CurrentRow = false,
            RowChangeReason = 'Rehire/Reuse',
            EmployeeKeyThruDate = cast(timestampadd(sql_tsi_day, -1, x.HireDate) AS sql_date),
            EmployeeKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = cast(timestampadd(sql_tsi_day, -1, x.HireDate) AS sql_date))
        -- SELECT *    
        FROM #Rehire x
        LEFT JOIN edwEmployeeDim e ON x.StoreCode = e.StoreCode
          AND x.employeeNumber = e.EmployeeNumber
          AND e.CurrentRow = true;
    -- INSERT new record        
        INSERT INTO edwEmployeeDim(StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution, Name, BirthDate, 
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            RowFromTS, CurrentRow, EmployeeKeyFromDate, EmployeeKeyFromDateKey,
            EmployeeKeyThruDate, EmployeeKeyThruDateKey,
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod) 
        SELECT StoreCode, Store, EmployeeNumber, 
            ActiveCode, Active, FullPartTimeCode, FullPartTime,
            PyDeptCode, PyDept, DistCode, Distribution,  Name, BirthDate, 
            BirthDateKey, HireDate, HireDateKey, TermDate, TermDateKey, 
            @NowTS, TRUE,
            HireDate AS EKFrom, HireDateKey AS EKFromKey,
            (SELECT thedate FROM day WHERE datetype = 'NA'),
            (SELECT datekey FROM day WHERE datetype = 'NA'),
            PayrollClassCode, PayrollClass, Salary, HourlyRate,
            PayPeriodCode, PayPeriod                     
        FROM #Rehire; 
      END IF; // IF (SELECT COUNT(*) FROM #Rehire) > 0		 
    CATCH ALL
      RAISE SPxfmEmpDIM(555, 'Rehire Reuse');	  
	  END TRY;
    
    
SELECT employeekey, storecode, employeenumber, activecode, pydept, distcode, name, 
  hiredate, hiredatekey, termdate, termdatekey, rowchangedate, employeekeyfromdate, 
  employeekeyfromdatekey, employeekeythrudate, employeekeythrudatekey, currentrow, rowchangereason
FROM edwEmployeeDim 
WHERE employeenumber in ('1102776' , '142460')   
ORDER BY employeenumber, employeekey

-- ok, WHERE am -
figured out that the problem seems to be that a goofy rehire/reuse signal IS sent
eg roy nisbet active going FROM T to P
don't know IF that's the same thing for tweeten, IF so, the bogus active change must get changed back
but
that trigger generates a rehire/reuse UPDATE, but the new records hired date IS the same AS the old hire
date, so the existing record gets ekthru updated to hiredate - 1, which IS, of course, less than
the original hiredate, therefore ekfrom > ekthru
the first thing i''m thinking IS, IN the test for rehire/reuse, verify that the hire date IS different
than the original hiredate

-- ALL with a reh/reuse record
SELECT employeekey, storecode, employeenumber, activecode, fullparttimecode, pydept, distcode, name, 
  hiredate, hiredatekey, termdate, termdatekey, rowchangedate, employeekeyfromdate, 
  employeekeyfromdatekey, employeekeythrudate, employeekeythrudatekey, currentrow, rowchangereason
FROM edwEmployeeDim 
WHERE employeenumber IN (
  SELECT employeenumber
  FROM edwEmployeeDim
  WHERE rowchangereason = 'rehire/reuse')
ORDER BY employeenumber, employeekey  

/* empkey find WHERE any records exist 
does the offending empkey have records IN any table*/
DECLARE @cur CURSOR AS
SELECT parent
FROM system.columns
WHERE name = 'EmployeeKey';
DECLARE @empkey string;
DECLARE @tableName string;
DECLARE @stmt string; 
@tableName = '';
@stmt = '';
@empkey = '1681';
drop TABLE zzzjon;
CREATE TABLE zzzjon (tablename cichar(200), howmany integer) IN database;
OPEN @cur;
TRY
  WHILE FETCH @cur DO 
    @tableName = @cur.parent;
    @stmt = 'insert INTO zzzjon SELECT ' + '''' + @TableName + '''' + ', COUNT(*) FROM ' + '' + @TableName + '' + ' WHERE cast(employeekey AS sql_char) = ' + '''' + @empkey  + ''''; 
    EXECUTE immediate @stmt;
  END WHILE;
FINALLY
  CLOSE @cur;
END;  
SELECT LEFT(tablename, 35), howmany FROM zzzjon WHERE howmany <> 0;



/* FROM local test
  hiredate, hiredatekey, termdate, termdatekey, employeekeyfromdate, 
  employeekeyfromdatekey, employeekeythrudate, employeekeythrudatekey
FROM xfmEmployeeDim
WHERE employeenumber = '1102776'
union
SELECT 'dim', storecode, employeenumber, activecode, pydept, distcode, name, 
  hiredate, hiredatekey, termdate, termdatekey, employeekeyfromdate, 
  employeekeyfromdatekey, employeekeythrudate, employeekeythrudatekey
FROM edwEmployeeDim 
WHERE employeenumber = '1102776'
*/

-- 7/27
don''t know what ELSE to think, the fact that roy nisbet shows up AS P ON 7/25 must be a glitch
so the approach i''m taking IS to DELETE ALL instances of the bogus empkey, AND
edit the original record IN edwEmployeeDim (remove ALL the change metadata)
fuck, this IS messy, but ok, onward
roy: changed pymast active FROM P to T
The other big issue IS rehire/reuse, WHEN the emp does NOT show termed IN arkona, AND the 2 hiredates
are the same
1. focus ON those WHERE ekfrom > ekthru
SELECT employeekey, storecode, employeenumber, activecode, pydept, distcode, name, 
  hiredate, hiredatekey, termdate, termdatekey, employeekeyfromdate AS ekfrom, 
  employeekeyfromdatekey, employeekeythrudate AS ekthru, employeekeythrudatekey, currentrow, 
  rowchangedate, rowchangereason
FROM edwEmployeeDim
WHERE employeenumber =  '1147260'
WHERE employeenumber IN (
  SELECT employeenumber 
  FROM edwEmployeeDim
  WHERE employeekeyfromdate > employeekeythrudate)
ORDER BY employeenumber, employeekeyfromdate
2. al berry
termed 2/29/12, but THEN ON 3/1/12 his distcode was changed
remove the bogus distcode change
3. morlock 113001
termed 2/21, dept changed 2/22

4. korsmoe 179880
termed 2/21 dept chg
5. tweten 142460
looks LIKE a 'glitch' ON 5/8 triggered a rehire/reuse
maybe the termdate changed, orig i showed 4/27, in new record changed to 4/30
7. warsame 1147260 : per maryanne, never actually termed
Arkona shows Orig & Last Hire as 12/5/11
I show Orig Hire 12/5/11 :: Term 5/11/12  :: rehired 5/25/12
this IS WHERE it gets tricky, instead of whacking the employeerecords for the bad key, 
need to replace it with the correct key
1st edwEmployeeDim 
1410 s/b the current row
1603 bogus
-- no overlapping records
SELECT *
FROM (
  SELECT *
  FROM edwClockHoursFact 
  WHERE employeekey = 1603) a
INNER JOIN (
  SELECT *
  FROM edwClockHoursFact 
  WHERE employeekey = 1410)b ON a.datekey = b.datekey;
UPDATE edwClockHoursFact
SET employeekey = 1410
WHERE employeekey = 1603;  
SELECT * FROM keymapdimemp WHERE employeekey IN (888,1603,1410)
DELETE FROM keymapdimemp WHERE employeekey = 1603;
UPDATE keymapdimemp
  SET fullparttimecode = 'A'
WHERE employeekey = 1410;
SELECT * FROM factClockHoursToday WHERE employeekey = 1603
UPDATE factClockHoursToday
  SET employeekey = 1410
WHERE employeekey = 1603;  
SELECT * FROM rptClockHours WHERE employeekey = 1603 ORDER BY thedate
-- no overlap
SELECT *
FROM (SELECT * FROM rptClockHours WHERE employeekey = 1603) a
INNER JOIN (SELECT * FROM rptClockHours WHERE employeekey = 1410) b ON a.thedate = b.thedate;
UPDATE rptClockHours
  SET employeekey = 1410
WHERE employeekey = 1603;  
SELECT * FROM factClockMinutesByHour WHERE employeekey = 1603 ORDER BY thedate
-- no overlap
SELECT *
FROM (SELECT * FROM factClockMinutesByHour WHERE employeekey = 1603) a
INNER JOIN (SELECT * FROM factClockMinutesByHour WHERE employeekey = 1410) b ON a.datekey = b.datekey;
UPDATE factClockMinutesByHour
  SET employeekey = 1410
WHERE employeekey = 1603; 

UPDATE rptClockMinutesByHour
  SET employeekey = 1410
WHERE employeekey = 1603; 
-- the following have been sent to Maryanne for clarification
6. paige 110911
8. hayes 163110
9. mccreary 193800
termed 2/21 deptdesc change 2/22
10. semrau 2126200
termed 11/30/11
row

DECLARE @empkey integer;
@empkey = 1581;
DELETE
FROM edwClockHoursFact
WHERE employeekey = @empkey;
DELETE
FROM keyMapDimEmp
WHERE employeekey = @empkey;
DELETE
FROM factClockHoursToday
WHERE employeekey = @empkey;
DELETE
FROM rptClockHours
WHERE employeekey = @empkey;
DELETE
FROM factClockMinutesByHour
WHERE employeekey = @empkey;
DELETE
FROM rptClockMinutesByHour
WHERE employeekey = @empkey;
DELETE
FROM AccrualCommissions
WHERE employeekey = @empkey;

-- SHIT NOT CHANGING DATEKEYS -- FUCK --
-- ok, these are done
SELECT *
FROM edwEmployeeDim a
WHERE hiredate <> (SELECT thedate FROM day WHERE datekey = a.hiredatekey)
  OR birthdate <> (SELECT thedate FROM day WHERE datekey = a.birthdatekey)
  OR termdate <> (SELECT thedate FROM day WHERE datekey = a.termdatekey)
  OR rowchangedate <> (SELECT thedate FROM day WHERE datekey = a.rowchangedatekey)
  OR employeekeyfromdate <> (SELECT thedate FROM day WHERE datekey = a.employeekeyfromdatekey)
  OR employeekeythrudate <> (SELECT thedate FROM day WHERE datekey = a.employeekeythrudatekey);
  
SELECT employeekey, employeenumber, name, 
  CASE
    WHEN hiredate <> (SELECT thedate FROM day WHERE datekey = a.hiredatekey) THEN 'hiredate'
    WHEN birthdate <> (SELECT thedate FROM day WHERE datekey = a.birthdatekey) THEN 'birthdate'
    WHEN termdate <> (SELECT thedate FROM day WHERE datekey = a.termdatekey) THEN 'termdate'
    WHEN rowchangedate <> (SELECT thedate FROM day WHERE datekey = a.rowchangedatekey) THEN 'rowchangedate'
    WHEN employeekeyfromdate <> (SELECT thedate FROM day WHERE datekey = a.employeekeyfromdatekey) THEN 'employeekeyfromdate'
    WHEN employeekeythrudate <> (SELECT thedate FROM day WHERE datekey = a.employeekeythrudatekey) THEN 'employeekeythrudate' 
  END,   
  hiredate, hiredatekey, (SELECT thedate FROM day WHERE datekey = a.hiredatekey),
  birthdate, birthdatekey, (SELECT thedate FROM day WHERE datekey = a.birthdatekey),
  termdate, termdatekey, (SELECT thedate FROM day WHERE datekey = a.termdatekey),
  rowchangedate, rowchangedatekey, (SELECT thedate FROM day WHERE datekey = a.rowchangedatekey),
  employeekeyfromdate, employeekeyfromdatekey, (SELECT thedate FROM day WHERE datekey = a.employeekeyfromdatekey),
  employeekeythrudate, employeekeythrudatekey, (SELECT thedate FROM day WHERE datekey = a.employeekeythrudatekey)
FROM edwEmployeeDim a
WHERE hiredate <> (SELECT thedate FROM day WHERE datekey = a.hiredatekey)
  OR birthdate <> (SELECT thedate FROM day WHERE datekey = a.birthdatekey)
  OR termdate <> (SELECT thedate FROM day WHERE datekey = a.termdatekey)
  OR rowchangedate <> (SELECT thedate FROM day WHERE datekey = a.rowchangedatekey)
  OR employeekeyfromdate <> (SELECT thedate FROM day WHERE datekey = a.employeekeyfromdatekey)
  OR employeekeythrudate <> (SELECT thedate FROM day WHERE datekey = a.employeekeythrudatekey)  
-- SHIT NOT CHANGING DATEKEYS -- FUCK --  
  
-- 7/31 decided to DO tests IN edwEmployeeDim ekfrom>ekthru, overlapping ekfrom/thru
-- need to look at rehire/reuse w/out a new hiredate
-- but first i'm going to fix the 4 remaining FROM my email to maryanne
SELECT *
FROM vempdim
WHERE employeekeyfromdate > employeekeythrudate


4. timothy a paige ry1 1109110

---------- 1. Austin Hayes  ry1 163110 -----------------------------------------
Arkona shows Active
I show termed 7/6/12
SELECT * FROM vempdim WHERE employeenumber = '163110'
good ek 1201
bad ek 1681
-- clockhours thru the 6th
SELECT a.*, (SELECT thedate FROM day WHERE datekey = a.datekey) FROM edwClockHoursFact a WHERE employeekey = 1681;
-- remove bogus rows due to range of bogus ek
DELETE FROM edwClockHoursFact 
WHERE employeekey = 1681
  AND datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate > '07/06/2012');
UPDATE edwClockHoursFact
SET employeekey = 1201
WHERE employeekey = 1681;    

-- this can just go away
SELECT * FROM keyMapDimEmp WHERE employeekey IN (1681,1201)
DELETE FROM keyMapDimEmp WHERE employeekey = 1681;
-- duh, this one doesn't matter
SELECT * FROM factClockHoursToday WHERE employeekey = 1681;
--
DELETE
FROM edwEmployeeDim WHERE employeekey = 1681;

UPDATE edwEmployeeDim
  SET currentrow = true,
      rowchangedate = NULL,
      rowchangedatekey = NULL,
      rowthruts = NULL,
      rowchangereason = NULL,
      employeekeythrudate = '07/06/2012',
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '07/06/2012')
WHERE employeekey = 1201;  


------------------ 2. Matthew A McCreary ry1 193800 -----------------------------
Arkona shows Orig & Last Hire as 8/1/11
I show Orig Hire 8/1/11 :: Term 2/18/12  :: rehired 6/5/12
change rehiredate to 5/25/12 to match clockhours
"legitimate" rehire/reuse
ek 1072  8/1/11 - 2/18/12 
ek 1618  5/25/12 - 12/31/9999
SELECT * FROM vempdim WHERE employeenumber = '193800'
-- clockhours thru the 6th
SELECT a.*, (SELECT thedate FROM day WHERE datekey = a.datekey) FROM edwClockHoursFact a WHERE employeekey IN (1072, 1618) ORDER BY (SELECT thedate FROM day WHERE datekey = a.datekey)
SELECT a.*, (SELECT thedate FROM day WHERE datekey = a.datekey) FROM edwClockHoursFact a WHERE employeekey IN (1618) ORDER BY (SELECT thedate FROM day WHERE datekey = a.datekey)

-- clkhours for 1618 start ON 5/25/12 :: make that the rehiredate 
-- last clockhours for 1072 IS 2/18/12
DELETE FROM edwClockHoursFact 
WHERE employeekey = 1072
  AND datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate > '02/18/2012');
    
DELETE FROM edwClockHoursFact 
WHERE employeekey = 1618
  AND datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate < '05/25/2012');    

SELECT * FROM keyMapDimEmp WHERE employeekey IN (1072,1618);
UPDATE keyMapDimEmp 
  SET rowfromts = '05/26/2012 01:45:12'
WHERE employeekey = 1618;

UPDATE edwEmployeeDim
  SET rowchangedate = '05/26/2012',
      rowchangedatekey = (SELECT datekey FROM day WHERE thedate = '05/26/2012'),
      rowthruts = '05/26/2012 01:45:12',
      employeekeythrudate = '02/18/2012',
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '02/18/2012')
WHERE employeekey = 1072;

UPDATE edwEmployeeDim
  SET hiredate = '05/25/2012',
      hiredatekey = (SELECT datekey FROM day WHERE thedate = '05/25/2012'),
      rowfromts = '05/26/2012 01:45:12',
      rowchangedatekey = NULL, 
      employeekeyfromdate = '05/25/2012',
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '05/25/2012')
WHERE employeekey = 1618;


------------- 3. taylor semrau ry2 2126200 -------------------------------------
Arkona shows Orig & Last Hire as 6/16/11 and no Term
I show Orig Hire 6/16/11 :: Term 11/30/11 :: rehired 6/26/12
"legitimate" rehire/reuse
ek 743  6/16/11 - 11/30/11
ek 1658  6/26 - 12/31/9999
SELECT * FROM vempdim WHERE employeenumber = '2126200'
SELECT a.*, (SELECT thedate FROM day WHERE datekey = a.datekey) FROM edwClockHoursFact a WHERE employeekey IN (743, 1658) ORDER BY (SELECT thedate FROM day WHERE datekey = a.datekey)

SELECT a.*, (SELECT thedate FROM day WHERE datekey = a.datekey) FROM edwClockHoursFact a WHERE employeekey = 743 AND clockhours <> 0 ORDER BY (SELECT thedate FROM day WHERE datekey = a.datekey)
SELECT a.*, (SELECT thedate FROM day WHERE datekey = a.datekey) FROM edwClockHoursFact a WHERE employeekey = 1658 ORDER BY (SELECT thedate FROM day WHERE datekey = a.datekey)


DELETE FROM edwClockHoursFact 
WHERE employeekey = 743
  AND datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate > '11/30/2011');
    
DELETE FROM edwClockHoursFact 
WHERE employeekey = 1658
  AND datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate < '06/26/2012');    
    
SELECT * FROM keyMapDimEmp WHERE employeekey IN (743,1658);
UPDATE keyMapDimEmp 
  SET rowfromts = '06/27/2012 01:45:13'
WHERE employeekey = 1658;    

UPDATE edwEmployeeDim
  SET rowchangedate = '06/27/2012',
      rowchangedatekey = (SELECT datekey FROM day WHERE thedate = '06/26/2012'),
      rowthruts = '06/27/2012 01:45:13',
      employeekeythrudate = '11/30/2011',
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '11/30/2011')
WHERE employeekey = 743;    

UPDATE edwEmployeeDim
  SET hiredate = '06/26/2012',
      hiredatekey = (SELECT datekey FROM day WHERE thedate = '06/26/2012'),
      rowfromts = '06/27/2012 01:45:13',
      employeekeyfromdate = '06/26/2012',
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '06/26/2012')
WHERE employeekey = 1658;       

----------- 4. timothy a paige ry1 1109110 -------------------------------------
Arkona shows Orig & Last Hire as 11/7/11 and no Term
I show Orig Hire 11/7/11 :: Term 7/2/12  :: rehired 7/9/12
"legitimate" rehire/reuse
ek 1575 4/26/12 - 7/2/12
ek 1664 - think this IS totally bogus, he was NOT rehired AS a wash tech
ek 1665  7/9 - 12/31/9999

SELECT * FROM vempdim WHERE employeenumber = '1109110'
SELECT a.*, (SELECT thedate FROM day WHERE datekey = a.datekey) FROM edwClockHoursFact a WHERE employeekey = 1664 ORDER BY (SELECT thedate FROM day WHERE datekey = a.datekey)

UPDATE edwClockHoursFact
  SET employeekey = 1665
WHERE employeekey = 1664
  AND datekey = 3113;
  
DELETE FROM edwClockHoursFact 
WHERE employeekey = 1664
  AND datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate > '06/30/2012');  

UPDATE edwClockHoursFact
  SET employeekey = 1665
WHERE employeekey = 1664;    
    
    
SELECT * FROM keyMapDimEmp WHERE employeekey IN (1575,1664,1665);
DELETE FROM keyMapDimEmp WHERE employeekey = 1664;   

UPDATE edwEmployeeDim
  SET rowchangedate = '07/10/2012',
      rowchangedatekey = (SELECT datekey FROM day WHERE thedate = '07/10/2012'),
      rowthruts = '07/10/2012 01:45:11',
      employeekeythrudate = '07/02/2012',
      employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '07/02/2012')
WHERE employeekey = 1575;  

DELETE FROM edwEmployeeDim WHERE employeekey = 1664;  

UPDATE edwEmployeeDim
  SET hiredate = '07/09/2012',
      hiredatekey = (SELECT datekey FROM day WHERE thedate = '07/09/2012'),
      rowfromts = '07/10/2012 01:45:11',
      employeekeyfromdate = '07/09/2012',
      employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '07/09/2012')
WHERE employeekey = 1665;  


-- 7/31 -- ok, i think the fixes are ALL done