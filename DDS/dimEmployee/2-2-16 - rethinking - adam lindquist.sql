-- 2/24/16
-- clean up adam lindquist
-- FROM honda to gm to honda
-- the whole notion of a valid employeekey WHEN an employee IS no longer active
-- IS losing it's persuasiveness

UPDATE edwEmployeeDim 
SET hiredate = '06/10/2013',
    hiredatekey = (SELECT datekey FROM day WHERE thedate = '06/10/2013')
WHERE employeekey = 3537    

UPDATE edwEmployeeDim
SET employeekeythrudate = '10/04/2014',
    employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '10/04/2014')
WHERE employeekey = 2628;

UPDATE edwEmployeeDim 
SET activecode = 'T',
    active = 'Term'
WHERE employeenumber = '185801';    
      
UPDATE edwEmployeeDim
SET employeekeythrudate = '02/22/2016',
    employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '02/22/2016')
WHERE employeekey = 3478;      