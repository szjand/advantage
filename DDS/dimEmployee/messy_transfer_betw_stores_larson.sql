SELECT *
FROM edwEmployeeDim
WHERE employeekey IN (
    SELECT employeekey
    FROM edwEmployeeDim
    WHERE employeenumber IN (
      SELECT employeenumber 
      FROM edwEmployeeDim
      WHERE employeekeyfromdate > employeekeythrudate)) 
      
      
select employeekey, employeenumber, active, hiredate, termdate, 
  rowfromts, rowthruts, LEFT(rowchangereason, 15), employeekeyfromdate, employeekeythrudate
FROM edwEmployeeDim
WHERE lastname = 'larson'  
  AND firstname = 'cody'    
ORDER BY employeekey  

DELETE FROM keymapdimemp WHERE employeekey = 4472;
DELETE FROM edwEmployeeDim WHERE employeekey = 4472;

UPDATE edwEmployeeDim
SET termdate = '09/23/2018',
    employeekeythrudate = '09/23/2018'
WHERE employeekey = 4454;
