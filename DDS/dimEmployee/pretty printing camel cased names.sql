SELECT DISTINCT name
INTO #wtf
FROM edwEmployeeDim 

DROP TABLE #wtf;
SELECT distinct name, 
  LEFT(trim(LEFT(name, position(',' IN name) - 1)), 1) + 
    lcase(substring(trim(LEFT(name, position(',' IN name) - 1)),2,length(trim(LEFT(name, position(',' IN name) - 1))))) AS lastname
INTO #wtf    
FROM edwEmployeeDim 
WHERE position(',' IN name) <> 0
  AND name NOT IN ('AHMED,AHMED O','JAMA,JAMA O','MOHAMED, MOHAMED M')

SELECT name, lastname, replace(trim(name), trim(lastname), ''),
  trim(replace(replace(trim(name), trim(lastname), ''),',',''))
FROM #wtf

-- DROP TABLE #wtf1;
SELECT distinct name, 
  LEFT(trim(LEFT(name, position(',' IN name) - 1)), 1) + 
    lcase(substring(trim(LEFT(name, position(',' IN name) - 1)),2,length(trim(LEFT(name, position(',' IN name) - 1))))) AS lastname,
    trim(replace(replace(trim(name), trim(lastname), ''),',','')) AS FirstAndMiddle
INTO #wtf1   
FROM #wtf
WHERE position(',' IN name) <> 0
  AND name NOT IN ('AHMED,AHMED O','JAMA,JAMA O','MOHAMED, MOHAMED M')

-- DROP TABLE #wtf2
SELECT name, lastname,
  LEFT(firstandmiddle, position(' ' IN firstandmiddle)) AS first,
  substring(firstandmiddle, position(' ' IN firstandmiddle), length(firstandmiddle)) AS middle
INTO #wtf2  
FROM #wtf1

SELECT name, trim(lastname) AS lastname, 
  LEFT(first,1) + lcase(substring(first, 2, length(first))) AS first,
  middle
FROM #wtf2

CREATE TABLE zNames (
  ArkonaName cichar(25),
  LastName cichar(25),
  FirstName cichar(25),
  MiddleName cichar(25)) IN database;
INSERT INTO zNames
SELECT name, trim(lastname) AS lastname, 
  LEFT(first,1) + lcase(substring(first, 2, length(first))) AS first,
  middle
FROM #wtf2;

SELECT *
FROM znames

SELECT a.name, b.*
FROM edwEmployeeDim a
LEFT JOIN znames b on a.name = b.arkonaname
ORDER BY name
  
-- anomalies
-- no comma
SELECT *
FROM #wtf
WHERE position(',' IN name) = 0

-- first name AND last name are the same
SELECT *
FROM edwEmployeeDim 
WHERE name IN ('AHMED,AHMED O','JAMA,JAMA O','MOHAMED, MOHAMED M')

-- multiple representations of the same name
SELECT *
FROM edwEmployeeDim
WHERE name LIKE 'ALVARA%'
ORDER BY storecode, employeekey

ALTER TABLE edwEmployeeDim 
ADD COLUMN LastName cichar(25)
ADD COLUMN FirstName cichar(25)
ADD COLUMN MiddleName cichar(25);

SELECT a.name, b.*
FROM edwEmployeeDim a
LEFT JOIN znames b on a.name = b.arkonaname
ORDER BY name

UPDATE edwEmployeeDim
SET LastName = x.LastName,
    FirstName = x.FirstName,
    MiddleName = x.MiddleName
FROM (
  SELECT a.name, b.*
  FROM edwEmployeeDim a
  LEFT JOIN znames b on a.name = b.arkonaname) x  
WHERE edwEmployeeDim.name = x.name
  
SELECT name, active, firstname, lastname, middlename
FROM edwEmployeeDim  
--WHERE name = 'MOHAMED, MOHAMED M'
--WHERE currentrow = true 
WHERE lastname IS NULL 
  
-- 10/7 this IS a pretty good check, should only return valid name changes 
SELECT a.storecode, a.employeenumber, a.firstname, a.middlename, a.lastname
FROM edwEmployeeDim a
INNER JOIN (
  SELECT employeenumber, storecode
  FROM (
    SELECT storecode, employeenumber, firstname, coalesce(middlename, ''), lastname
    FROM edwEmployeeDim 
    GROUP BY storecode, employeenumber, firstname, coalesce(middlename, ''), lastname) x
  GROUP BY storecode, employeenumber
  HAVING COUNT(*) > 1  ) b on a.storecode = b.storecode AND a.employeenumber = b.employeenumber
ORDER BY a.storecode, a.employeenumber  

-- 10/28
this should be usable for processing new rows
AND type 2 UPDATE new rows

SELECT name, lastname, LEFT(first,1) + lcase(substring(first, 2, length(first))) AS firstname, middlename
FROM (
  SELECT name, lastname,
    LEFT(firstandmiddle, position(' ' IN firstandmiddle)) AS first,
    substring(firstandmiddle, position(' ' IN firstandmiddle), length(firstandmiddle)) AS middlename
  FROM (    
    SELECT distinct name, lastname,
        trim(replace(replace(trim(name), trim(lastname), ''),',','')) AS FirstAndMiddle
    FROM (
      SELECT distinct name, 
        LEFT(trim(LEFT(name, position(',' IN name) - 1)), 1) + 
          lcase(substring(trim(LEFT(name, position(',' IN name) - 1)),2,length(trim(LEFT(name, position(',' IN name) - 1))))) AS lastname
      FROM (
        SELECT *
        FROM edwEmployeeDim
        WHERE position(',' IN name) <> 0
          AND name NOT IN ('AHMED,AHMED O','JAMA,JAMA O','MOHAMED, MOHAMED M')) w) x) y) z
 
-- put this IN maint, AND THEN return error IF NOT updated
-- rows with NULL lastname
SELECT name, lastname, LEFT(first,1) + lcase(substring(first, 2, length(first))) AS firstname, middlename
FROM (
  SELECT name, lastname,
    LEFT(firstandmiddle, position(' ' IN firstandmiddle)) AS first,
    substring(firstandmiddle, position(' ' IN firstandmiddle), length(firstandmiddle)) AS middlename
  FROM (    
    SELECT distinct name, lastname,
        trim(replace(replace(trim(name), trim(lastname), ''),',','')) AS FirstAndMiddle
    FROM (
      SELECT distinct name, 
        LEFT(trim(LEFT(name, position(',' IN name) - 1)), 1) + 
          lcase(substring(trim(LEFT(name, position(',' IN name) - 1)),2,length(trim(LEFT(name, position(',' IN name) - 1))))) AS lastname
      FROM (
        SELECT *
        FROM edwEmployeeDim
        WHERE lastname IS NULL  
          AND position(',' IN name) <> 0
          AND name NOT IN ('AHMED,AHMED O','JAMA,JAMA O','MOHAMED, MOHAMED M')) w) x) y) z

-- tests for inconsistent names
SELECT name
FROM (   
  SELECT name, lastname, firstname, coalesce(middlename, '')
  FROM edwEmployeeDim
  GROUP BY name, lastname, firstname, coalesce(middlename, '')) x
GROUP BY name HAVING COUNT(*) > 1
    
    
UPDATE edwEmployeeDim
SET LastName = trim(zz.LastName),
    FirstName = trim(zz.FirstName),
    MiddleName = trim(zz.MiddleName)    
FROM (
  SELECT name, lastname, LEFT(first,1) + lcase(substring(first, 2, length(first))) AS firstname, middlename
  FROM (
    SELECT name, lastname,
      LEFT(firstandmiddle, position(' ' IN firstandmiddle)) AS first,
      substring(firstandmiddle, position(' ' IN firstandmiddle), length(firstandmiddle)) AS middlename
    FROM (    
      SELECT distinct name, lastname,
          trim(replace(replace(trim(name), trim(lastname), ''),',','')) AS FirstAndMiddle
      FROM (
        SELECT distinct name, 
          LEFT(trim(LEFT(name, position(',' IN name) - 1)), 1) + 
            lcase(substring(trim(LEFT(name, position(',' IN name) - 1)),2,length(trim(LEFT(name, position(',' IN name) - 1))))) AS lastname
        FROM (
          SELECT *
          FROM edwEmployeeDim
          WHERE lastname IS NULL  
            AND position(',' IN name) <> 0
            AND name NOT IN ('AHMED,AHMED O','JAMA,JAMA O','MOHAMED, MOHAMED M')) w) x) y) z) zz   
WHERE edwEmployeeDim.name = zz.name
  AND edwEmployeeDim.lastname IS NULL     