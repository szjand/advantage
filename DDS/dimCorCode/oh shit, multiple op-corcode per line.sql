-- kimball desing tip 142
-- the only way to really determine a unique constraint IS BY flattening
-- the key IS to flatten the raw data

1. CREATE the brCorCode TABLE AND populate it
2. go to nightly script, UPDATE current factRepairOrder rows with the correct CorCodeKey
3. take the nightly scrape AND UPDATE tmpRoLineKeys with the correct CorCodeKey

-- what IS the maximum number of cor codes per line
-- 8
SELECT ptro#, ptline, COUNT(*)
FROM (
  SELECT ptro#, ptline, ptcrlo
  FROM tmpsdprdet
  WHERE ptcrlo <> ''
  GROUP BY ptro#, ptline, ptcrlo) x
GROUP BY ptro#, ptline  
ORDER BY COUNT(*) DESC
-- 11
SELECT ptro#, ptline, COUNT(*)
FROM (
  SELECT ptro#, ptline, ptcrlo
  FROM stgArkonasdprdet
  WHERE ptcrlo <> ''
  GROUP BY ptro#, ptline, ptcrlo) x
GROUP BY ptro#, ptline  
ORDER BY COUNT(*) DESC

--< correlation BETWEEN cor codes AND flag hours -------------------------------
-- lines with multiple cor codes  
SELECT ptro#, ptline, corcodes
FROM ( 
    SELECT ptro#, ptline, ptcrlo
    FROM tmpSDPRDET
    WHERE ptcrlo <> ''
    GROUP BY ptro#, ptline, ptcrlo) a
GROUP BY ptro#, ptline    
    HAVING COUNT(*) > 1  
    
-- lines with multiple techs 
SELECT ptro#, ptline, 'techs'
FROM ( 
    SELECT ptro#, ptline, pttech
    FROM tmpSDPRDET
    WHERE pttech <> ''
    GROUP BY ptro#, ptline, pttech) a
GROUP BY ptro#, ptline    
    HAVING COUNT(*) > 1    
    
-- intersection
SELECT z.*
FROM tmpSDPRDET z
INNER JOIN (
  SELECT b.ptro#, b.ptline
  FROM (
    SELECT ptro#, ptline, 'corcodes'
    FROM ( 
      SELECT ptro#, ptline, ptcrlo
      FROM tmpSDPRDET
      WHERE ptcrlo <> ''
      GROUP BY ptro#, ptline, ptcrlo) a
    GROUP BY ptro#, ptline    
    HAVING COUNT(*) > 1) b
  INNER JOIN (
    SELECT ptro#, ptline, 'techs'
    FROM ( 
      SELECT ptro#, ptline, pttech
      FROM tmpSDPRDET
      WHERE pttech <> '' 
        AND ptlhrs > 0
      GROUP BY ptro#, ptline, pttech) a
    GROUP BY ptro#, ptline    
     HAVING COUNT(*) > 1) c ON b.ptro# = c.ptro# AND b.ptline = c.ptline) e ON z.ptro# = e.ptro# AND z.ptline = e.ptline 
WHERE z.ptltyp = 'L'               
ORDER BY z.ptro#, z.ptline, z.ptseq#  

      
-- 8/20 let greg fuck with the correlation,
--/> correlation BETWEEN cor codes AND flag hours -------------------------------
-- i need a basic brCorrectionCode

-- ok, this would be one CorCodeKey
SELECT ptro#, ptline, ptcrlo
FROM tmpsdprdet
WHERE ptro# = '18023438'
  AND ptline = 1
  AND ptcrlo <> '' 
GROUP BY ptro#, ptline, ptcrlo 
ORDER BY ptcrlo 


DROP TABLE zBrCorCode;
CREATE TABLE zBrCorCode (
  corcodekey integer,
  corcode cichar(10),
  seq integer) IN database;
  
-- 1 corcode groups
DECLARE @cur1 CURSOR AS
  SELECT DISTINCT ptcrlo
  FROM tmpsdprdet
  WHERE ptcrlo <> '';
DECLARE @i integer;
OPEN @cur1;
TRY
  @i = 1;
  WHILE FETCH @cur1 DO
    INSERT INTO zBrCorCode values (@i, @cur1.ptcrlo, 0);  
    @i = @i + 1;
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;        
  
-- multiple corcode groups 
DECLARE @cur1 CURSOR AS
  SELECT ptco#, ptro#, ptline
  FROM (        
    SELECT ptco#, ptro#, ptline, ptcrlo
    FROM tmpsdprdet
    WHERE ptcrlo <> ''
    GROUP BY ptco#, ptro#, ptline, ptcrlo) a
  GROUP BY ptco#,ptro#, ptline
  HAVING COUNT(*) > 1 
  ORDER BY ptco#, ptro#, ptline;
DECLARE @cur2 CURSOR AS 
  SELECT ptco#, ptro#, ptline, ptcrlo
  FROM tmpsdprdet
  WHERE ptcrlo <> ''
    AND ptco# = @cur1.ptco#
    AND ptro# = @cur1.ptro#
    AND ptline = @cur1.ptline
  GROUP BY ptco#, ptro#, ptline, ptcrlo
  ORDER BY ptcrlo;
DECLARE @i integer;
DECLARE @j integer;
OPEN @cur1;
TRY
  @i = (SELECT MAX(corcodekey) + 1 FROM zbrcorcode);
  WHILE FETCH @cur1 DO
    OPEN @cur2;
    TRY
      @j = 1;
      WHILE FETCH @cur2 DO
        INSERT INTO zBrCorCode values (@i, @cur2.ptcrlo, @j);
        @j = @j + 1;
      END WHILE;
    FINALLY
      CLOSE @cur2;
    END TRY;
    @i = @i + 1;
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;  


-- so, now, of course the question IS how to determine
-- IF a corcode key already EXISTS, & IF so, what IS it
-- without doing the fucking horizontal ordering, no way i am going to write
-- an IF statement to accomodate ALL combinations of 11 variables


SELECT * 
FROM zbrcorcode a,
  (    
  SELECT ptco#, ptro#, ptline, ptcrlo
  FROM tmpsdprdet
  WHERE ptcrlo <> ''
    AND ptro# = '18023438'
    AND ptline = 1
  GROUP BY ptco#, ptro#, ptline, ptcrlo) b WHERE a.corcode = b.ptcrlo
  

SELECT * 
FROM zbrcorcode a
INNER JOIN (    
  SELECT ptco#, ptro#, ptline, ptcrlo
  FROM tmpsdprdet
  WHERE ptcrlo <> ''
    AND ptro# = '18023438'
    AND ptline = 1
  GROUP BY ptco#, ptro#, ptline, ptcrlo) b on a.corcode = b.ptcrlo  
  
-- bridge side  
SELECT *
FROM zbrcorcode a 
INNER JOIN (
  SELECT corcodekey, COUNT(*) AS howmanyCC
  FROM zbrcorcode
  GROUP BY corcodekey) b ON a.corcodekey = b.corcodekey 

-- ro side
SELECT a.*, b.howmanyRO
FROM (
  SELECT ptco#, ptro#, ptline, ptcrlo
  FROM tmpsdprdet
  WHERE ptcrlo <> ''
    AND ptro# = '18023438'
    AND ptline = 1
  GROUP BY ptco#, ptro#, ptline, ptcrlo) a
INNER JOIN (  
  SELECT ptco#, ptro#, ptline, COUNT(*) AS howmanyRO
  FROM (        
    SELECT ptco#, ptro#, ptline, ptcrlo
    FROM tmpsdprdet
    WHERE ptcrlo <> ''
    GROUP BY ptco#, ptro#, ptline, ptcrlo) a
  GROUP BY ptco#,ptro#, ptline) b ON a.ptro# = b.ptro# AND a.ptline = b.ptline

-- this might actually be it for matches  
SELECT *
FROM (  
  -- bridge side  
  SELECT *
  FROM zbrcorcode a 
  INNER JOIN (
    SELECT corcodekey, COUNT(*) AS howmanyCC
    FROM zbrcorcode
    GROUP BY corcodekey) b ON a.corcodekey = b.corcodekey) d   
INNER JOIN (
  -- ro side
  SELECT a.*, b.howmanyRO
  FROM (
    SELECT ptco#, ptro#, ptline, ptcrlo
    FROM tmpsdprdet
    WHERE ptcrlo <> ''
      AND ptro# like '18023324'
      AND ptline = 1
    GROUP BY ptco#, ptro#, ptline, ptcrlo) a
  INNER JOIN (  
    SELECT ptco#, ptro#, ptline, COUNT(*) AS howmanyRO
    FROM (        
      SELECT ptco#, ptro#, ptline, ptcrlo
      FROM tmpsdprdet
      WHERE ptcrlo <> ''
      GROUP BY ptco#, ptro#, ptline, ptcrlo) a
    GROUP BY ptco#,ptro#, ptline) b ON a.ptro# = b.ptro# AND a.ptline = b.ptline) e ON d.corcode = e.ptcrlo AND d.howmanycc = e.howmanyro  
ORDER BY ptro#, ptline

-- issue: 18023324 line 1, returns 4 corcodegroups
SELECT ptro#, ptline, corcodekey, COUNT(*)
FROM (
SELECT *
FROM (  
  -- bridge side  
  SELECT *
  FROM zbrcorcode a 
  INNER JOIN (
    SELECT corcodekey, COUNT(*) AS howmanyCC
    FROM zbrcorcode
    GROUP BY corcodekey) b ON a.corcodekey = b.corcodekey) d   
INNER JOIN (
  -- ro side
  SELECT a.*, b.howmanyRO
  FROM (
    SELECT ptco#, ptro#, ptline, ptcrlo
    FROM tmpsdprdet
    WHERE ptcrlo <> ''
      AND ptro# like '1802%'
      AND ptline = 1
    GROUP BY ptco#, ptro#, ptline, ptcrlo) a
  INNER JOIN (  
    SELECT ptco#, ptro#, ptline, COUNT(*) AS howmanyRO
    FROM (        
      SELECT ptco#, ptro#, ptline, ptcrlo
      FROM tmpsdprdet
      WHERE ptcrlo <> ''
      GROUP BY ptco#, ptro#, ptline, ptcrlo) a
    GROUP BY ptco#,ptro#, ptline) b ON a.ptro# = b.ptro# AND a.ptline = b.ptline) e ON d.corcode = e.ptcrlo AND d.howmanycc = e.howmanyro) x
GROUP BY ptro#, ptline, corcodekey    

-- ADD a seq to ro side? THEN match that AS well?
-- might be relatively simple
-- IS it good enuf to rely ON seq being generated BY default sorting ORDER ON 2 different datasets?
-- need to DO the 2 step deal: one liners are seq = 0
-- multiliners seq starts with 1

DROP TABLE ztmpsdprdetCC;
CREATE TABLE ztmpsdprdetCC (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  corcode cichar(10),
  seq integer) IN database;
  
-- 1 corcode lines
INSERT INTO ztmpsdprdetCC
SELECT DISTINCT a.ptco#, a.ptro#, a.ptline, a.ptcrlo, 0
FROM tmpsdprdet a
INNER JOIN (
  SELECT ptco#, ptro#, ptline
  FROM (
    SELECT ptco#, ptro#, ptline, ptcrlo
    FROM tmpsdprdet
    WHERE ptcrlo <> ''
    GROUP BY ptco#, ptro#, ptline, ptcrlo) b
  GROUP BY ptco#, ptro#, ptline
  HAVING COUNT(*) = 1) c ON a.ptco# = c.ptco# AND a.ptro# = c.ptro# AND a.ptline = c.ptline
WHERE a.ptcrlo <> '' 
-- multiple corcodes per line
DECLARE @cur1 CURSOR AS   
  SELECT ptco#, ptro#, ptline
  FROM (   
      SELECT ptco#, ptro#, ptline, ptcrlo
      FROM tmpsdprdet
      WHERE ptcrlo <> ''
      GROUP BY ptco#, ptro#, ptline, ptcrlo) a
  GROUP BY ptco#, ptro#, ptline
  HAVING COUNT(*) > 1;  
 
DECLARE @cur2 CURSOR AS
  SELECT ptco#, ptro#, ptline, ptcrlo
  FROM tmpsdprdet
  WHERE ptcrlo <> ''
    AND ptco# = @cur1.ptco#
    AND ptro# = @cur1.ptro#
    AND ptline = @cur1.ptline
  GROUP BY ptco#, ptro#, ptline, ptcrlo
  ORDER BY ptcrlo;
DECLARE @i integer;
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    OPEN @cur2;
    TRY
      @i = 1;
      WHILE FETCH @cur2 DO
        INSERT INTO ztmpsdprdetCC values (@cur2.ptco#, @cur2.ptro#, @cur2.ptline,@cur2.ptcrlo, @i);
        @i = @i + 1;
      END WHILE;
    FINALLY
      CLOSE @cur2;
    END TRY;
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;      
    
--- hmm, seq may NOT be good enuf, seq could be 1 for a corcode IF it IS the only
-- corcode IN the GROUP OR IF it IS the first corcode IN the GROUP 
-- make seq for single corcode groups = 0    

-- what the fuck look at 116127257
-- redo the big COUNT joins including sequence IN the JOIN
-- nope, COUNT will NOT DO it either
corcode: BULB IS seq 1 IN GROUP 1279 AND GROUP 1542 both of which have 2 rows 

SELECT *
FROM ztmpsdprdetcc a
LEFT JOIN zbrcorcode b ON a.corcode = b.corcode AND a.seq = b.seq

another problem 16127616 line 6 detCC shows 23I AS seq 1

SELECT *
FROM ztmpsdprdetcc 
WHERE ro = '16127616'

SELECT *
FROM ztmpsdprdetcc a
INNER JOIN zbrcorcode b ON a.corcode = b.corcode AND a.seq = b.seq
WHERE ro = '16127616'

line 1 IS mult corcodes WHERE seq 1 = 0220
ON this ro the correct GROUP IS 1-0220, 2-10b, 3-DIA
there are multiple groups WHERE 1 = 0220, 2 = 10b AND 3 = DIA


SELECT *
FROM ztmpsdprdetcc a
INNER JOIN zbrcorcode b ON a.corcode = b.corcode AND a.seq = b.seq
WHERE ro = '16127616' AND line = 2

SELECT a.storecode, a.ro, a.line, b.corcodekey, COUNT(*) AS matches
FROM ztmpsdprdetcc a, zbrcorcode b
WHERE a.ro = '16127616'
  AND a.line = 2
  AND a.corcode = b.corcode
  AND a.seq = b.seq
GROUP BY a.storecode, a.ro, a.line, b.corcodekey  

leaning towards COUNT again 
IF the number of matches = the number of corcodes ON the line, that IS it

-- ok, it works for that one ro
SELECT *
FROM (
  SELECT a.storecode, a.ro, a.line, b.corcodekey, COUNT(*) AS matches
  FROM ztmpsdprdetcc a, zbrcorcode b
  WHERE a.ro = '16127616'
    AND a.line = 2
    AND a.corcode = b.corcode
    AND a.seq = b.seq
  GROUP BY a.storecode, a.ro, a.line, b.corcodekey ) c
INNER JOIN (  
  SELECT storecode, ro, line, COUNT(*) AS ccs
  FROM ztmpsdprdetcc
  GROUP BY storecode, ro, line) d ON c.storecode = d.storecode AND c.ro = d.ro 
    AND c.line = d.line AND c.matches = d.ccs
    
lets TRY more  
tentatively promising
until i go with ALL the ros, mucho dups
 SELECT  ro, line FROM (  
SELECT *
FROM (
  SELECT a.storecode, a.ro, a.line, b.corcodekey, COUNT(*) AS matches
  FROM ztmpsdprdetcc a, zbrcorcode b
--  WHERE a.ro LIKE '1802%'
    WHERE a.corcode = b.corcode
    AND a.seq = b.seq
  GROUP BY a.storecode, a.ro, a.line, b.corcodekey ) c
INNER JOIN (  
  SELECT storecode, ro, line, COUNT(*) AS ccs
  FROM ztmpsdprdetcc
  GROUP BY storecode, ro, line) d ON c.storecode = d.storecode AND c.ro = d.ro 
    AND c.line = d.line AND c.matches = d.ccs
--ORDER BY c.ro, c.line
 ) x GROUP BY ro, line HAVING COUNT(*) > 1
 
 -- lets look at some of the dups
 
SELECT *
FROM ztmpsdprdetcc a
INNER JOIN zbrcorcode b ON a.corcode = b.corcode AND a.seq = b.seq
WHERE ro = '16122280' AND line = 1
-- ahaa, each of these groups are identical
-- eg 2 corecodes, seq 1 = 24M & seq 2 = OL
-- the problem IS IN the generation of zbrcorcode
 SELECT *
FROM (
  SELECT a.storecode, a.ro, a.line, b.corcodekey, COUNT(*) AS matches
  FROM ztmpsdprdetcc a, zbrcorcode b
  WHERE a.ro = '16122280'
    AND a.line = 1
    AND a.corcode = b.corcode
    AND a.seq = b.seq
  GROUP BY a.storecode, a.ro, a.line, b.corcodekey ) c
INNER JOIN (  
  SELECT storecode, ro, line, COUNT(*) AS ccs
  FROM ztmpsdprdetcc
  GROUP BY storecode, ro, line) d ON c.storecode = d.storecode AND c.ro = d.ro 
    AND c.line = d.line AND c.matches = d.ccs
INNER JOIN (
  SELECT corcodekey, COUNT(*) AS thecount
  FROM zbrcorcode
  GROUP BY corcodekey) e ON c.corcodekey = e.corcodekey AND c.matches = e.thecount
  
--8/21 this primary issue IS IN generating unique groups IN brCorrectionCode
-- thinking may have to go to flattening to insure uniqueness, can NOT yet figure 
-- out how to DO it IN groups otherwise  

-- here are the problems, eg, non unique groups i believe
-- adding matches shows that the big offender are groups of 2, those should be easy to flatten
SELECT  ro, line, matches FROM (  
SELECT *
FROM (
  SELECT a.storecode, a.ro, a.line, b.corcodekey, COUNT(*) AS matches
  FROM ztmpsdprdetcc a, zbrcorcode b
--  WHERE a.ro LIKE '1802%'
    WHERE a.corcode = b.corcode
    AND a.seq = b.seq
  GROUP BY a.storecode, a.ro, a.line, b.corcodekey ) c
INNER JOIN (  
  SELECT storecode, ro, line, COUNT(*) AS ccs
  FROM ztmpsdprdetcc
  GROUP BY storecode, ro, line) d ON c.storecode = d.storecode AND c.ro = d.ro 
    AND c.line = d.line AND c.matches = d.ccs
--ORDER BY c.ro, c.line
 ) x GROUP BY ro, line, matches HAVING COUNT(*) > 1
 
 -- how many
 
 SELECT *
 INTO #wtf
 FROM zbrcorcode
 WHERE corcodekey IN (
   SELECT corcodekey
   FROM zbrcorcode
   GROUP BY corcodekey
   HAVING COUNT(*) = 2)

DROP TABLE zCorCodeFlat;
CREATE TABLE zCorCodeFlat (
  corcodekey integer,
  code1 cichar(10),
  seq1 integer,
  code2 cichar(10),
  seq2 integer) IN database;   
DELETE FROM zcorcodeflat;     
INSERT INTO zCorCodeFlat (corcodekey)
SELECT DISTINCT corcodekey FROM #wtf;

UPDATE zCorCodeFlat
SET code1 = x.code1,
    seq1 = x.seq1,
    code2 = x.code2,
    seq2 = x.seq2
FROM (
  SELECT corcodekey, code1, seq1, code2, seq2
  FROM (
    SELECT corcodekey, 
      (SELECT corcode FROM #wtf WHERE seq = 1 AND corcodekey = a.corcodekey) AS code1, 1 AS seq1,
      (SELECT corcode FROM #wtf WHERE seq = 2 AND corcodekey = a.corcodekey) AS code2, 2 AS seq2
    FROM #wtf a) b
  GROUP BY corcodekey, code1, seq1, code2, seq2) x 
WHERE zCorCodeFlat.corcodekey = x.corcodekey;

SELECT * FROM zcorcodeflat ORDER BY corcodekey

SELECT * FROM zcorcodeflat a, zcorcodeflat b
WHERE a.code1 = b.code1 AND a.code2 = b.code2 AND a.seq1 = b.seq1 AND a.seq2 = b.seq2
  AND a.corcodekey <> b.corcodekey

SELECT MIN(corcodekey) AS corcodekey, code1, code2, seq1, seq2
FROM zcorcodeflat
GROUP BY code1, code2, seq1, seq2   
ORDER BY corcodekey
     
 