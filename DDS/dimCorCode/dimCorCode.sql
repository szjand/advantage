duh, the reason this IS different than other dimensions IS twofold
1. need a bridge
2. the initial load FROM 'convert FROM existing fact tables' IS just wrong
the difference BETWEEN initial load AND nightly will simply be FROM what dataset xfmDimCorCodeGroup1 IS derived
initial: stgArkona
nightly: tmpSDP/PDP

hmmm will i want to maintain a flat brCorCodeGroup rather than reconstructing it every time
(need the flat TABLE with which to compare to incoming data)

will i need a N/A corcodekey for those lines with no corcode

-- kimball desing tip 142
-- the only way to really determine a unique constraint IS BY flattening
-- the key IS to flatten the raw data
-- requires dimOpCode, tmpPDP/SDP

1. normalized raw data
1 row per store/ro/line/corcode
2. pivot to one row per corcodegroup

SELECT * from dimopcode WHERE opcode LIKE 'UNK%'



CREATE TABLE xfmDimCorCodeGroup1 (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  corcode cichar(10),
  CorCodeKey integer) IN database; 
-- need to GROUP to accomodate stupid arkona doubling lines
-- AND stupid employees entering the cor code twice   
INSERT INTO xfmDimCorCodeGroup1
SELECT a.ptco#, a.ptro#, a.ptline, a.ptcrlo, 
  coalesce(b.opcodekey, c.opcodekey) AS CorCodeKey
FROM stgArkonaSDPRDET a
LEFT JOIN dimOpCode b ON a.ptco# = b.storecode 
  AND a.ptcrlo = b.opcode
LEFT JOIN dimOpCode c ON a.ptco# = c.storecode 
  AND c.opcode = 'UNKNOWN'
WHERE ptcrlo <> ''
  AND ptcode = 'CR'
  AND ptltyp = 'L'
  AND ptco# IN ('ry1','ry2')
GROUP BY a.ptco#, a.ptro#, a.ptline, a.ptcrlo, 
  coalesce(b.opcodekey, c.opcodekey);    

-- uh oh, decision time
-- so, at what point IN the process DO i need actual opcodekeys
-- an example, ro 16001650 line 1 has 4 cor codes, 2 of which are unknown
--    so, IS the corcode GROUP 3 OR 4 corcodes

i am thinking sooner rather than later, both for loading the dimension
AND subsequently assigning keys to the fact TABLE
it alse disambiguates sequencing, integers (corcodekeys) are absolutely going
to sort the same
sequencing simplifies matching
AS long AS i DO the same "normalization" with the nightly raw data, ie, use keys NOT opcodes
should be good
TRY it without a a sequence?? a single COLUMN with ALL the keys IN it? yuck
kimball example uses some fucking XPATH shit that i DO NOT have, so i would have to
come up with a parsing scheme
lets forge ahead

-- multiple UNKNOWN cor codes ON the same line
-- ALL of these dups lines are the unknown shit
-- so GROUP again AND should be ok, AND it IS
DROP TABLE xfmDimCorCodeGroup2;
CREATE TABLE xfmDimCorCodeGroup2 (
  storecode cichar(3),
  ro cichar(9),
  line integer,
  CorCodeKey integer) IN database; 
-- constraint: unique index
EXECUTE PROCEDURE sp_CreateIndex90( 
   'xfmDimCorCodeGroup2','xfmDimCorCodeGroup2.adi','NK',
   'StoreCode;RO;line;corcodekey','',2051,512,'' );  
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimCorCodeGroup2','storecode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimCorCodeGroup2','RO', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimCorCodeGroup2','line', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimCorCodeGroup2','CorCodeKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );      
         
INSERT INTO xfmDimCorCodeGroup2
SELECT storecode, ro, line, corcodekey
FROM (
  SELECT storecode, ro, line, corcodekey
  FROM xfmDimCorCodeGroup1
  GROUP BY storecode, ro, line, corcodekey) a
GROUP BY storecode, ro, line, corcodekey ; 

to pivot, i have to DO a CURSOR
remember, single GROUP sequence IS 0
want to pivot to a single row for each group
oops, this IS WHERE i have to decide the number of columns, one with concantenated values OR x columns

DO the pivot IN 2 steps
1. ptco#,ptro#,ptline,corcodegroup
2. unique corcodegroup
SELECT * FROM #wtf1

-- MAX corcodes per line IS 10
SELECT ptco#, ptro#, ptline, COUNT(*)
FROM #wtf1
GROUP BY ptco#, ptro#, ptline
ORDER BY COUNT(*) DESC 

-- 8/22 thinking single field delimited with |
-- this IS looking really fucking good, no need for sequence
SELECT a.* FROM xfmDimCorCodeGroup2 a
INNER JOIN (
SELECT storecode, ro, line
FROM xfmDimCorCodeGroup2
GROUP by storecode, ro, line
HAVING COUNT(*) = 8) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line

DROP TABLE xfmDimCorCodeGroup3;
CREATE TABLE xfmDimCorCodeGroup3 (
  storecode cichar(3), 
  ro cichar(9),
  line integer,
  CorCodeKeyGroup cichar(120)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'xfmDimCorCodeGroup3','xfmDimCorCodeGroup2.adi','NK',
   'StoreCode;RO;line','',2051,512,'' );  
   
DECLARE @cur1 CURSOR AS 
  SELECT storecode, ro, line
  FROM xfmDimCorCodeGroup2
  GROUP BY storecode, ro, line
  ORDER BY storecode, ro, line;
  
DECLARE @cur2 CURSOR AS 
  SELECT corcodekey
  FROM xfmDimCorCodeGroup2
  WHERE storecode = @storecode
    AND ro = @ro
    AND line = @line
  ORDER BY corcodekey;
DECLARE @str string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @corcodekey string;  
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    @storecode = @cur1.storecode;
    @ro = @cur1.ro;
    @line = @cur1.line;
    OPEN @cur2;
    TRY
      @str = '';
      WHILE FETCH @cur2 DO
        @corcodekey = TRIM(CAST(@cur2.corcodekey AS sql_char));
        @str = @str + @corcodekey + '|';  
      END WHILE;
      INSERT INTO xfmDimCorCodeGroup3 values(@storecode, @ro, @line, @str);
      @str = '';
    FINALLY
      CLOSE @cur2;
    END TRY;
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;  

-- uh oh
-- i DO NOT know how these rows are getting created with a corcodekeygroup = ''
-- this might have been a corrupted index, DO NOT know
-- this can NOT be allowed
SELECT * FROM xfmDimCorCodeGroup3 WHERE corcodekeygroup = ''




SELECT * 
FROM xfmDimCorCodeGroup3 a
LEFT JOIN ( 
  SELECT corcodekeygroup
  FROM xfmDimCorCodeGroup3
  GROUP BY corcodekeygroup) b ON a.corcodekeygroup = b.corcodekeygroup
  
-- maintain the flat view IN dimCorCodeGroup ???
-- yes, i am liking that
-- DO NOT think i will be needing this TABLE, UPDATE dimCorCodeGroup with a DISTINCT FROM xfmDimCorCodeGroup3
-- IN a CURSOR (to generate sequential keys)
DROP TABLE xfmDimCorCodeGroup4;
CREATE TABLE xfmDimCorCodeGroup4 (
  CorCodeGroupKey autoinc,
  FlatGroup cichar(120)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'xfmDimCorCodeGroup4','xfmDimCorCodeGroup4.adi','NK',
   'FlatGroup','',2051,512,'' );vb
   
INSERT INTO xfmDimCorCodeGroup4 (FlatGroup)
SELECT DISTINCT corcodekeygroup FROM xfmDimCorCodeGroup3;  

******** need a N/A corcode for lines with no corcode
so, what does that look LIKE
WHEN joining to factrepairorder, IF there IS no row IN xfmDimCorCodeGroup2

SELECT a.* FROM xfmDimCorCodeGroup2 a

SELECT * FROM xfmDimCorCodeGroup4 

SELECT * FROM xfmDimCorCodeGroup4 WHERE flatgroup LIKE (SELECT opcode FROM dimopcode

SELECT flatgroup FROM xfmDimCorCodeGroup4 GROUP BY flatgroup HAVING COUNT(*) > 1

SELECT * FROM dimOpCode WHERE opcode = 'N/A'

SELECT a.storecode, a.ro, a.line, b.*, c.*
FROM factRepairOrder a
LEFT JOIN xfmDimCorCodeGroup3 b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
LEFT JOIN xfmDimCorCodeGroup4 c ON b.corcodekeygroup = c.flatgroup

SELECT a.storecode, a.ro, a.line, b.*
FROM factrepairorder a
LEFT JOIN xfmDimCorCodeGroup2 b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line

ok, i think i have ALL the pieces, 
what tasks need to be completed
CREATE brCorCodeGroup
CREATE dimCorCode
UPDATE factRepairOrder CorCodeKey
nightly:
  ADD any new corCodeGrouops to dimCorCode & brCorCodeGroup
  UPDATE the nightly version of factRepairOrder
  
DROP TABLE dimCorCodeGroup;  
CREATE TABLE dimCorCodeGroup (
  CorCodeGroupKey integer,
  FlatGroup cichar(120)) IN database;
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCorCodeGroup','dimCorCodeGroup.adi','PK',
   'CorCodeGroupKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimCorCodeGroup','Table_Primary_Key',
  'PK', 'RETURN_ERROR', NULL);   
-- index: NK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCorCodeGroup','dimCorCodeGroup.adi','NK',
   'FlatGroup','',2051,512,'' );  -- unique   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCorCodeGroup','FlatGroup', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   

-- store code IS irrelevant to N/A, no cor code IS simply no cor code  
INSERT INTO dimCorCodeGroup values (1, 'N/A');  

DROP TABLE brCorCodeGroup;
CREATE TABLE brCorCodeGroup (
  CorCodeGroupKey integer,
  OpCodeKey integer) IN database;
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brCorCodeGroup','brCorCodeGroup.adi','PK',
   'CorCodeGroupKey;OpCodeKey','',2051,512,'' );   
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('brCorCodeGroup','Table_Primary_Key',
  'PK', 'RETURN_ERROR', NULL);  
-- index  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brCorCodeGroup','brCorCodeGroup.adi','CorCodeGroupKey',
   'CorCodeGroupKey','',2,512,'' );     
EXECUTE PROCEDURE sp_CreateIndex90( 
   'brCorCodeGroup','brCorCodeGroup.adi','OpCodeKey',
   'OpCodeKey','',2,512,'' );   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'brCorCodeGroup','CorCodeGroupKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'brCorCodeGroup','OpCodeKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
       
--RI gets added ON the many side
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimCorCodeGroup-brCorCodeGroup');
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimOpCode-brCorCodeGroup');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimCorCodeGroup-brCorCodeGroup','dimCorCodeGroup','brCorCodeGroup','CorCodeGroupKey',2,2,NULL,'','');  
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimOpCode-brCorCodeGroup','dimOpCode','brCorCodeGroup','OpCodeKey',2,2,NULL,'','');   
     
INSERT INTO brCorCodeGroup values(1,1);  

DECLARE @cur1 CURSOR AS SELECT DISTINCT corcodekeygroup FROM xfmDimCorCodeGroup3; 
DECLARE @i integer;
@i = (SELECT MAX(corCodeGroupKey) + 1 FROM dimCorCodeGroup);  
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    INSERT INTO dimCorCodeGroup values (@i, @cur1.corCodeKeyGroup);
    @i = @i + 1; 
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;   
              
-- brCorCodeGroup
INSERT INTO brCorCodeGroup
SELECT a.corcodeGroupKey, d.opcodekey
FROM dimCorCodeGroup a
INNER JOIN xfmDimCorCodeGroup3 b ON a.flatgroup = b.corcodekeygroup
INNER JOIN xfmDimCorCodeGroup2 c ON b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line 
LEFT JOIN dimOpCode d ON c.corcodekey = d.opcodekey
GROUP BY a.corcodeGroupKey, d.opcodekey;


-- UPDATE existing rows IN factRepairORder
EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimOpCodeCor-factRepairOrder'); 
DROP INDEX factRepairOrder.CorCodeKey; 
ALTER TABLE factRepairOrder
ALTER COLUMN CorCodeKey CorCodeGroupKey integer;   

UPDATE factRepairOrder
SET corCodeGroupKey = x.CorCodeGroupKey
FROM (
  SELECT a.storecode, a.ro, a.line, coalesce(c.CorCodeGroupKey, d.corcodegroupkey) AS CorCodeGroupKey
  FROM factRepairOrder a
  LEFT JOIN xfmDimCorCodeGroup3 b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  LEFT JOIN dimCorCodeGroup c ON b.corcodekeygroup = c.flatgroup
  LEFT JOIN dimCorCodeGroup d ON 1 = 1 
    AND d.corcodeGroupKey = (
      SELECT corcodegroupkey
      FROM brCorCodeGroup
      WHERE opcodekey = (
        SELECT opcodekey
        FROM dimopcode
        WHERE storecode = 'RY1'
          AND opcode = 'N/A'))) x
WHERE factRepairOrder.storecode = x.storecode
  AND factRepairOrder.ro = x.ro
  AND factRepairOrder.line = x.line; 
  
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'factRepairOrder','factRepairOrder.adi','CorCodeGroupKey','CorCodeGroupKey',
   '',2,512,'' );     
-- nullable   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'factRepairOrder', 
      'CorCodeGroupKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', NULL );       
-- RI      
-- EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimCorCodeGroup-factRepairOrder');      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('dimOpCodeCor-factRepairOrder',
     'dimCorCodeGroup', 'factRepairOrder', 'CorCodeGroupKey', 
     2, 2, NULL, '', '');            

--< Nightly -------------------------------------------------------------------
DELETE FROM xfmDimCorCodeGroup1;
INSERT INTO xfmDimCorCodeGroup1
SELECT a.ptco#, a.ptro#, a.ptline, a.ptcrlo, 
  coalesce(b.opcodekey, c.opcodekey) AS CorCodeKey
FROM tmpSDPRDET a
LEFT JOIN dimOpCode b ON a.ptco# = b.storecode 
  AND a.ptcrlo = b.opcode
LEFT JOIN dimOpCode c ON a.ptco# = c.storecode 
  AND c.opcode = 'UNKNOWN'
WHERE ptcrlo <> ''
  AND ptcode = 'CR'
  AND ptltyp = 'L'
  AND ptco# IN ('ry1','ry2')
GROUP BY a.ptco#, a.ptro#, a.ptline, a.ptcrlo, 
  coalesce(b.opcodekey, c.opcodekey)  
UNION 
SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptcrlo, 
  coalesce(c.opcodekey, d.opcodekey) AS CorCodeKey 
FROM tmpPDPPHDR a
INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
LEFT JOIN dimOpCode c ON a.ptco# = c.storecode 
  AND b.ptcrlo = c.opcode
LEFT JOIN dimOpCode d ON a.ptco# = d.storecode 
  AND d.opcode = 'UNKNOWN'
WHERE a.ptdtyp = 'RO'
  AND a.ptdoc# <> ''
  AND a.ptchk# NOT LIKE 'v%'
  AND a.ptcnam <> '*VOIDED REPAIR ORDER*'
  AND b.ptline < 900
  AND b.ptcrlo <> ''
  AND b.ptcode = 'CR'
  AND b.ptltyp = 'L'
  AND a.ptco# IN ('RY1','RY2')
GROUP BY a.ptco#, a.ptdoc#, b.ptline, b.ptcrlo, 
  coalesce(c.opcodekey, d.opcodekey);   

DELETE FROM xfmDimCorCodeGroup2; 
INSERT INTO xfmDimCorCodeGroup2
SELECT storecode, ro, line, corcodekey
FROM (
  SELECT storecode, ro, line, corcodekey
  FROM xfmDimCorCodeGroup1
  GROUP BY storecode, ro, line, corcodekey) a
GROUP BY storecode, ro, line, corcodekey;   

DELETE FROM xfmDimCorCodeGroup3;
DECLARE @cur1 CURSOR AS 
  SELECT storecode, ro, line
  FROM xfmDimCorCodeGroup2
  GROUP BY storecode, ro, line
  ORDER BY storecode, ro, line;
  
DECLARE @cur2 CURSOR AS 
  SELECT corcodekey
  FROM xfmDimCorCodeGroup2
  WHERE storecode = @storecode
    AND ro = @ro
    AND line = @line
  ORDER BY corcodekey;
DECLARE @str string;
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
DECLARE @corcodekey string;  
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    @storecode = @cur1.storecode;
    @ro = @cur1.ro;
    @line = @cur1.line;
    OPEN @cur2;
    TRY
      @str = '';
      WHILE FETCH @cur2 DO
        @corcodekey = TRIM(CAST(@cur2.corcodekey AS sql_char));
        @str = @str + @corcodekey + '|';  
      END WHILE;
      INSERT INTO xfmDimCorCodeGroup3 values(@storecode, @ro, @line, @str);
      @str = '';
    FINALLY
      CLOSE @cur2;
    END TRY;
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;  

-- necessary test, maybe a trigger at some point, since there IS NOT other way to ADD the constraint
SELECT * FROM xfmDimCorCodeGroup3 WHERE corcodekeygroup = '';

-- these are the NEW CorCodeGroup rows
SELECT DISTINCT a.CorCodeKeyGroup
FROM xfmDimCorCodeGroup3 a
LEFT JOIN dimCorCodeGroup b ON a.corCodeKeyGroup = b.FlatGroup
WHERE b.flatgroup IS NULL 


-- need to ADD new rows to dimCorCodeGroup AND brCorCodeGroup
-- AND that IS fucking IT for nightly UPDATE to dimCorCodeGroup 
-- dimCorCodeGroup
DECLARE @cur1 CURSOR AS 
  SELECT DISTINCT a.CorCodeKeyGroup
  FROM xfmDimCorCodeGroup3 a
  LEFT JOIN dimCorCodeGroup b ON a.corCodeKeyGroup = b.FlatGroup
  WHERE b.flatgroup IS NULL;
DECLARE @i integer;
@i = (SELECT MAX(corCodeGroupKey) + 1 FROM dimCorCodeGroup);  
OPEN @cur1;
TRY
  WHILE FETCH @cur1 DO
    INSERT INTO dimCorCodeGroup values (@i, @cur1.corCodeKeyGroup);
    @i = @i + 1; 
  END WHILE;
FINALLY
  CLOSE @cur1;
END TRY;   

-- brCorCodeGroup
INSERT INTO brCorCodeGroup
SELECT a.corcodeGroupKey, d.opcodekey
FROM dimCorCodeGroup a
INNER JOIN xfmDimCorCodeGroup3 b ON a.flatgroup = b.corcodekeygroup
INNER JOIN xfmDimCorCodeGroup2 c ON b.storecode = c.storecode AND b.ro = c.ro AND b.line = c.line 
LEFT JOIN dimOpCode d ON c.corcodekey = d.opcodekey
WHERE NOT EXISTS (
  SELECT 1
  FROM brCorCodeGroup
  WHERE CorCodeGroupKey = a.CorCodeGroupKey)
GROUP BY a.corcodeGroupKey, d.opcodekey;

--Wa Fucking Hoo
--> Nightly -------------------------------------------------------------------     

INSERT INTO zProcExecutables values('dimCorCodeGroup','Daily',CAST('00:30:15' AS sql_time),0);
INSERT INTO zProcProcedures values('dimCorCodeGroup','dimCorCodeGroup',0,'daily');
INSERT INTO DependencyObjects values('dimCorCodeGroup','dimCorCodeGroup','dimCorCodeGroup');

INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimCorCodeGroup'
  AND b.dObject = 'tmpSDPRHDR';
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimCorCodeGroup'
  AND b.dObject = 'tmpSDPRDET';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimCorCodeGroup'
  AND b.dObject = 'tmpPDPPHDR';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimCorCodeGroup'
  AND b.dObject = 'tmpPDPPDET';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimCorCodeGroup'
  AND b.dObject = 'dimCCC';
    
    