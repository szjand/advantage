/*
-- why the fuck IS ryan lene showing up IN main shop production summary
SELECT * FROM dimTech WHERE technumber = '267'
-- because i fucked up WHEN i created tech 267, flagDeptCode = 'MS', flagDept='Body Shop'
-- AND (i'm guessing) it got fixed on 1/9 overnight, creating a new type 2 row
-- IN dimtech, techkey 519with the correct flagdeptcode

so get rid of the bogus newrow,modify dimtech.techkey 518 to be the correct
flagdeptcode 

what ALL needs to be changed:
dds
  dimTech
  keyMapDimTech
  factRepairOrder
sco
  tmpBenTechCensus
  tmpBen  
  
SELECT * FROM factrepairorder WHERE techkey IN (518,519)  
*/  
BEGIN TRANSACTION;
TRY
UPDATE factRepairOrder
SET techKey = 518
WHERE techkey = 519;

DELETE 
FROM keyMapDimTech
WHERE techKey = 519;

DELETE 
FROM dimTech
WHERE techKey = 519;

UPDATE dimTech
SET flagDeptCode = 'BS',
    currentrow = true,
    rowchangedate = NULL,
    rowchangedatekey = NULL,
    rowthruts = NULL,
    rowchangereason = NULL,
    techkeythrudate = '12/31/9999',
    techkeythrudatekey = 7306
WHERE techKey = 518;

UPDATE scotest.tmpDeptTechCensus
  SET flagdeptcode = 'BS'
WHERE techkey = 518;

UPDATE scotest.tmpDeptTechCensus
SET techKey = 518
WHERE techkey = 519;

UPDATE scotest.tmpBen
SET flagDeptCode = 'BS'
WHERE technumber = '267';

UPDATE scotest.tmpBen
SET techkey = 518
WHERE techkey = 519;

COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  

