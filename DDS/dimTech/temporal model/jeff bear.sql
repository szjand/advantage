jeff bear rehired on 10/14/14
bev reused his old tech number: 574
this came to my attention because IN vision main shop production summary,
bear IS NOT showing up AS an hourly tech

the core issue, of course, IS reuse of the technumber, talked to bev, she will 
stop doing it

she was able to reactivate the technumber IN arkona, i mistakenly assumed that
was NOT possible AND so DO NOT check for techs changing FROM inactive to active IN
sp.xfmDimTech
need to modify that so that it sends me an email WHEN (AND IF) it happens again

think this also needs to be done for service writers

"inactive" techs IN dimtech that are "active" IN arkona
of course it shows bear

SELECT a.*, b.name
FROM xfmTechDim a
LEFT JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.active = true
AND flagdeptcode = 'mr'
AND a.storecode = 'ry1'
AND EXISTS (
  SELECT 1
  FROM dimtech
  WHERE technumber = a.technumber
    AND storecode = a.storecode
    AND currentrow = true
    AND active = false)
ORDER BY a.technumber

/*
this IS a similar situation to what i ran INTO with kyle bragunier (pdq writer)
AND should be checked

SELECT *
FROM xfmServiceWriterDim a
WHERE swactive = 'Y'
AND EXISTS (
  SELECT 1
  FROM dimServiceWriter
  WHERE writerNumber = a.swswid
    AND storecode = a.swco# 
    AND currentrow = true
    AND active = false)
*/    

this IS goofy, he rehiredate IS 10/14/14
but there exist ros with him HAVING flagtime IN september AND early october
SELECT b.yearmonth, COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
INNER JOIN dimTech c on a.techkey = c.techkey
WHERE b.thedate BETWEEN curdate() - 365 AND curdate()
  AND c.technumber = '574'
GROUP BY b.yearmonth

SELECT b.thedate, a.*
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE a.techkey = 390
ORDER BY thedate

of course, factrepairorder references the most recent, current (techkeyfrom/thru)
techkey (390) FROM dimtech, even though that techkey IS active = false

the
so how DO i fix it
nk: storecode,technumber,empnumber,active,flagdeptcode,laborcost
which, just LIKE dimServiceWriter seems fucked up
IN part at least because the notion of active IS fuzzy at best, misleading at worst

the entire notion that a tech becoming inactive generates a new techkey
IN dimtech IS what stinks

so the fix i contemplate IS to make techkey current AND active
unfortunately there are beaucoup ros with the new techkey
so what i am thinking IS make the new techkey active AND the old techkey inactive
that "should" WORK
though, because of the goofy NK will have to jump thru a couple of hoops
UPDATE dimTech
SET flagdeptcode = 'XX'
WHERE techkey = 37;

UPDATE dimtech
SET active = false
WHERE techkey = 37;

UPDATE dimtech 
SET active = true
WHERE techkey = 390;

UPDATE dimTech
SET flagdeptcode = 'MR'
WHERE techkey = 37;

rerun the tmpBen stuff IN sco, see IF bear shows up