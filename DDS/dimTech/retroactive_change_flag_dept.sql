-- guadalupe ortiz moved FROM main shop to body shop IN august 
SELECT *
FROM dimtech
WHERE name LIKE '%ortiz%'

SELECT *
FROM dimtech
WHERE employeenumber = '1107950'


SELECT *
FROM dimtech
WHERE technumber = '612'
  AND storecode = 'ry1'

SELECT rowchangereason, COUNT(*)
FROM dimtech
GROUP BY rowchangereason

SELECT *
FROM dimtech a
WHERE EXISTS (
  SELECT 1
  FROM dimtech b
  WHERE b.technumber = a.technumber
    AND b.rowchangereason = 'flagdept')
    
-- first bs ro IS on 8/8  18053324
SELECT b.thedate, a.*
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE techkey = 595 
ORDER BY b.thedate desc 

-----------------------------------------------------------------------
--< 07/14/22 skyler bruns tech 663 FROM RE to MR
-----------------------------------------------------------------------
1. SET him up correctly retroactively, day 1 = 7/3
   type 2 new row, rowchangereason = flagdept
	 
SELECT * FROM dimtech WHERE technumber	= '663' 


DECLARE @tech_key integer;   
BEGIN TRANSACTION;
TRY
-- UPDATE dimtech
UPDATE dimtech
SET RowChangeDate = curdate(),--CAST(@NowTS AS sql_date),
    RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = curdate()), --CAST(@NowTS AS sql_date)),
    RowThruTS = (SELECT now() FROM system.iota),
    CurrentRow = false, 
    RowChangeReason = 'flagdept',
    TechKeyThruDate = CAST('07/02/2022' AS sql_date),
    TechKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST('07/02/2022' AS sql_date))
WHERE techkey = 2329
  AND currentRow = true;
	
INSERT INTO dimtech (storecode, employeenumber, name, 
  Description, technumber, active, laborcost, flagdeptcode, 
  flagdept, currentrow, rowfromts, rowthruts, 
  techkeyfromdate, techkeyfromdatekey, 
  techkeythrudate, techkeythrudatekey)
values ('RY1', '153442', 'BRUNS, SKYLER M', 
  'Tech 663', '663', true, 18, 'MR', 
  'Service', true, (SELECT now() FROM system.iota), CAST('12/31/9999 01:00:00' AS sql_timestamp), 
  CAST('07/03/2022' AS sql_date), (SELECT datekey FROM day WHERE thedate = CAST('07/03/2022' AS sql_date)),
  CAST('12/31/9999' AS sql_date), 7306);	
	
-- keymap
	@tech_key = (SELECT techkey FROM dimtech WHERE technumber = '663' AND currentrow = true); 
  INSERT INTO keymapdimtech
  SELECT techkey, storecode, technumber, active, flagdeptcode, laborcost, employeenumber
  FROM dimtech
  WHERE techkey = @tech_key; 	
	
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  	
-----------------------------------------------------------------------
--/> 07/14/22 skyler bruns tech 663 FROM RE to MR
-----------------------------------------------------------------------
1. SET him up correctly retroactively, day 1 = 8/8
   type 2 new row, rowchangereason = flagdept
 
DECLARE @tech_key integer;   
BEGIN TRANSACTION;
TRY
-- UPDATE dimtech
UPDATE dimtech
SET RowChangeDate = curdate(),--CAST(@NowTS AS sql_date),
    RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = curdate()), --CAST(@NowTS AS sql_date)),
    RowThruTS = (SELECT now() FROM system.iota),
    CurrentRow = false, 
    RowChangeReason = 'flagdept',
    TechKeyThruDate = CAST('08/07/2017' AS sql_date),
    TechKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST('08/07/2017' AS sql_date))
WHERE techkey = 595;

INSERT INTO dimtech (storecode, employeenumber, name, 
  Description, technumber, active, laborcost, flagdeptcode, 
  flagdept, currentrow, rowfromts, rowthruts, 
  techkeyfromdate, techkeyfromdatekey, 
  techkeythrudate, techkeythrudatekey)
values ('RY1', '1107950', 'ORTIZ, GUADALUPE', 
  'Tech 612', '612', true, 21.89, 'BS', 
  'Body Shop', true, (SELECT now() FROM system.iota), CAST('12/31/9999 01:00:00' AS sql_timestamp), 
  CAST('08/08/2017' AS sql_date), (SELECT datekey FROM day WHERE thedate = CAST('08/08/2017' AS sql_date)),
  CAST('12/31/9999' AS sql_date), 7306);
  
-- keymap
  INSERT INTO keymapdimtech
  SELECT techkey, storecode, technumber, active, flagdeptcode, laborcost, employeenumber
  FROM dimtech
  WHERE techkey = @tech_key; 
   
-- factrepairorder
  @tech_key = (SELECT techkey FROM dimtech WHERE technumber = '612' AND currentrow = true AND storecode = 'ry1');
  UPDATE factrepairorder
  SET techkey = @tech_key
  WHERE techkey = 595
    AND LEFT(ro,2) = '18';
    
-- scotest.tmpDeptTechCensus    
  UPDATE scotest.tmpDeptTechCensus
  SET techKey = @tech_key
  WHERE techkey = 595
    AND thedate > '08/07/2017';

-- scotest.tmpben
  UPDATE scotest.tmpBen
  SET flagDeptCode = 'BS'
  WHERE technumber = '612'
    AND thedate > '08/07/2017';  

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  


-- 10/3/17

TY!
Also, could you move Thomas Saul from Detail to Body Shop as an Hourly Employee on the Vision page please. 
RS.

SELECT *
FROM dimtech
WHERE name LIKE '%saul%'


SELECT b.thedate, a.*
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE techkey = 816
ORDER BY b.thedate DESC 


SELECT *
FROM edwEmployeeDim
WHERE lastname = 'sauls'

FROM detail to bs on 8/15/17

DECLARE @tech_key integer;   
BEGIN TRANSACTION;
TRY
-- UPDATE dimtech
UPDATE dimtech
SET RowChangeDate = curdate(),--CAST(@NowTS AS sql_date),
    RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = curdate()), --CAST(@NowTS AS sql_date)),
    RowThruTS = (SELECT now() FROM system.iota),
    CurrentRow = false, 
    RowChangeReason = 'flagdept',
    TechKeyThruDate = CAST('08/14/2017' AS sql_date),
    TechKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST('08/14/2017' AS sql_date))
WHERE techkey = 775;

INSERT INTO dimtech (storecode, employeenumber, name, 
  Description, technumber, active, laborcost, flagdeptcode, 
  flagdept, currentrow, rowfromts, rowthruts, 
  techkeyfromdate, techkeyfromdatekey, 
  techkeythrudate, techkeythrudatekey)
values ('RY1', '1122352', 'SAULS, THOMAS R', 
  'Thomas Sauls', 'D25', true, 22, 'BS', 
  'Body Shop', true, (SELECT now() FROM system.iota), CAST('12/31/9999 01:00:00' AS sql_timestamp), 
  CAST('08/15/2017' AS sql_date), (SELECT datekey FROM day WHERE thedate = CAST('08/15/2017' AS sql_date)),
  CAST('12/31/9999' AS sql_date), 7306);
  
-- keymap
  @tech_key = (SELECT techkey FROM dimtech WHERE technumber = 'D25' AND currentrow = true AND storecode = 'ry1');
  INSERT INTO keymapdimtech
  SELECT techkey, storecode, technumber, active, flagdeptcode, laborcost, employeenumber
  FROM dimtech
  WHERE techkey = @tech_key; 
   
-- factrepairorder  
  UPDATE factrepairorder
  SET techkey = @tech_key
  WHERE techkey = 775
    AND opendatekey > (SELECT datekey FROM day WHERE thedate = '08/14/2017');
    
-- scotest.tmpDeptTechCensus    
  UPDATE scotest.tmpDeptTechCensus
  SET techKey = @tech_key
  WHERE techkey = 775
    AND thedate > '08/14/2017';

-- scotest.tmpben
  UPDATE scotest.tmpBen
  SET flagDeptCode = 'BS'
  WHERE technumber = 'D25'
    AND thedate > '08/14/2017';  

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  


-- 2/21/19
Will you also remove Thomas Sauls from the vision page he now works in the detail department.
SELECT * FROM edwEmployeeDim WHERE lastname = 'sauls'
moved FROM body shop to detail on 1/22/19
select * FROM dimtech WHERE technumber = 'D25'

select * FROM factrepairorder WHERE techkey = 816 AND opendatekey > (SELECT datekey FROM day WHERE thedate = '01/22/2019');




DECLARE @tech_key integer;   
BEGIN TRANSACTION;
TRY
-- UPDATE dimtech
UPDATE dimtech
SET RowChangeDate = curdate(),--CAST(@NowTS AS sql_date),
    RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = curdate()), --CAST(@NowTS AS sql_date)),
    RowThruTS = (SELECT now() FROM system.iota),
    CurrentRow = false, 
    RowChangeReason = 'flagdept',
    TechKeyThruDate = CAST('01/21/2019' AS sql_date),
    TechKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST('01/21/2019' AS sql_date))
WHERE techkey = 816;

-- this was necessary to acccomodate the natural key
-- for whatever reason, the active attribute for ALL his rows were true
UPDATE dimtech
SET active = false
WHERE technumber = 'D25';

INSERT INTO dimtech (storecode, employeenumber, name, 
  Description, technumber, active, laborcost, flagdeptcode, 
  flagdept, currentrow, rowfromts, rowthruts, 
  techkeyfromdate, techkeyfromdatekey, 
  techkeythrudate, techkeythrudatekey)
values ('RY1', '1122352', 'SAULS, THOMAS R', 
  'Thomas Sauls', 'D25', true, 22, 'RE', 
  'Detail', true, (SELECT now() FROM system.iota), CAST('12/31/9999 01:00:00' AS sql_timestamp), 
  CAST('01/22/2019' AS sql_date), (SELECT datekey FROM day WHERE thedate = CAST('01/22/2019' AS sql_date)),
  CAST('12/31/9999' AS sql_date), 7306);
  
-- keymap
  @tech_key = (SELECT techkey FROM dimtech WHERE technumber = 'D25' AND currentrow = true AND storecode = 'ry1');
  INSERT INTO keymapdimtech
  SELECT techkey, storecode, technumber, active, flagdeptcode, laborcost, employeenumber
  FROM dimtech
  WHERE techkey = @tech_key; 
   
-- factrepairorder  
  UPDATE factrepairorder
  SET techkey = @tech_key
  WHERE techkey = 816
    AND opendatekey > (SELECT datekey FROM day WHERE thedate = '01/21/2019');
    
-- scotest.tmpDeptTechCensus    
  UPDATE scotest.tmpDeptTechCensus
  SET techKey = @tech_key
  WHERE techkey = 816
    AND thedate > '01/21/2019';

-- scotest.tmpben
  UPDATE scotest.tmpBen
  SET flagDeptCode = 'RE'
  WHERE technumber = 'D25'
    AND thedate > '01/21/2019';  

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  

