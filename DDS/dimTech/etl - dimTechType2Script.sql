/* manually DROP RI duh 
-- CREATE the TABLE THEN run dimTech.exe to populate
CREATE TABLE tmpSDPTECH ( 
      [stco#] CIChar( 3 ),
      sttech CIChar( 3 ),
      stname CIChar( 20 ),
      [stemp#] CIChar( 9 ),
      stlrat Money,
      stpwrd CIChar( 6 ),
      [stpyemp#] CIChar( 7 ),
      stactv CIChar( 1 ),
      [stcrt#] CIChar( 10 ),
      stdftsvct CIChar( 2 )) IN DATABASE;  
THEN run this at the END
  **************   AS well AS rebuild indices *****************************
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ( 
     'duh',
     'dimTech', 
     'keyMapDimTech', 
     'TECHKEY', 
     2, 
     2, 
     NULL,
     '', 
     ''); 
     
--EXECUTE PROCEDURE sp_CreateIndex90( 
--   'keyMapDimTech',
--   'keymapDimTech.adi',
--   'NK',
--   'StoreCode;TechNumber',
--   '',
--   2051,
--   512,
--   '' ); 


can't ADD an NK, have to DO it with constraints    
*/

DROP index dimtech.nk;
DROP index dimtech.pk;
DROP index keymapDimTech.nk;


ALTER TABLE dimtech
ADD COLUMN CurrentRow logical
ADD COLUMN RowChangeDate date
ADD COLUMN RowChangeDateKey integer
ADD COLUMN RowFromTS timestamp
ADD COLUMN RowThruTS timestamp
ADD COLUMN RowChangeReason cichar(100)
ADD COLUMN TechKeyFromDate date
ADD COLUMN TechKeyFromDateKey integer
ADD COLUMN TechKeyThruDate date
ADD COLUMN TechKeyThruDateKey integer;

UPDATE dimtech
SET currentrow = true;

UPDATE dimtech
  SET rowfromts = x.rowfromts,
      rowthruts = x.rowthruts,
      techkeyfromdate = x.techkeyfromdate,
      techkeythrudate = x.techkeythrudate
FROM (  
  SELECT a.storecode, a.technumber, techkey, a.employeenumber, a.name, 
    a.description, b.hiredate, b.termdate,
    CASE 
      WHEN a.employeenumber IS NULL THEN CAST('01/01/2009 08:00:00' AS sql_timestamp)
      ELSE timestampadd(sql_tsi_hour, 1, CAST(hiredate AS sql_timestamp))
    END AS RowFromTS,
    CASE 
      WHEN a.employeenumber IS NULL THEN CAST('12/31/9999 00:00:00' AS sql_timestamp)
      ELSE 
        CASE
          WHEN termdate = '12/31/9999' THEN CAST(termdate AS sql_timestamp)
          ELSE timestampadd(sql_tsi_hour, 1, CAST(termdate AS sql_timestamp))
        END
    END AS RowThruTS,  
    CASE 
      WHEN a.employeenumber IS NULL THEN CAST('01/01/2009' AS sql_date)
      ELSE hiredate
    END AS techKeyFromDate,
    CASE 
      WHEN a.employeenumber IS NULL THEN CAST('12/31/9999' AS sql_date)
      ELSE termdate
    END AS techKeyThruDate  
  FROM dimtech a
  LEFT JOIN edwEmployeeDim b ON a.employeenumber = b.employeenumber
    AND b.currentrow = true) x
WHERE dimtech.techkey = x.techkey;
  
  
UPDATE dimtech
  SET techkeyfromdatekey = (SELECT datekey FROM day WHERE thedate = techkeyfromdate),
      techkeythrudatekey = (SELECT datekey FROM day WHERE thedate = techkeythrudate);
        
-- 620 UPDATE old row
--DECLARE @NowTS timestamp;
--@NowTS = (SELECT now() FROM system.iota);
UPDATE dimtech
SET RowChangeDate = curdate(),--CAST(@NowTS AS sql_date),
    RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = curdate()), --CAST(@NowTS AS sql_date)),
    RowThruTS = (SELECT now() FROM system.iota),
    CurrentRow = false, 
    RowChangeReason = 'Employee to DeptTech',
    TechKeyThruDate = CAST('11/05/2010' AS sql_date),
    TechKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST('11/05/2010' AS sql_date))
WHERE techkey = 101;

INSERT INTO dimtech (storecode, Description, technumber, laborcost, flagdeptcode, 
  flagdept, currentrow, rowfromts, rowthruts, 
  techkeyfromdate, techkeyfromdatekey, 
  techkeythrudate, techkeythrudatekey)
values ('RY1', 'Recon Tech2', '620', 15, 'RE', 
  'Detail', true, (SELECT now() FROM system.iota), CAST('12/31/9999 00:00:00' AS sql_timestamp), 
  CAST('11/06/2010' AS sql_date), (SELECT datekey FROM day WHERE thedate = CAST('11/06/2010' AS sql_date)),
  CAST('12/31/9999' AS sql_date), 7306);
  
-- 1  
INSERT INTO keymapDimTech
SELECT TechKey, StoreCode, TechNumber
FROM dimTech a
WHERE NOT EXISTS (
  SELECT 1
  FROM keymapDimTech
  WHERE TechKey = a.TechKey);
-- 2  
INSERT INTO bridgeTechGroup (TechKey, WeightFactor)        
SELECT c.techkey, d.weightfactor
FROM (
  SELECT a.techkey
  FROM dimtech a
  WHERE NOT EXISTS (
    SELECT 1
    FROM bridgetechgroup b 
    where techkey = a.techkey)) c,
(
  SELECT n/100.0 AS weightfactor
  FROM tally
  WHERE n BETWEEN 1 AND 100) d;     
-- 3
INSERT INTO keymapBridgeTechGroup
SELECT TechGroupKey, TechKey, WeightFactor
FROM BridgeTechGroup a
WHERE NOT EXISTS (
  SELECT 1
  FROM keymapBridgeTechGroup
  WHERE TechKey = a.TechKey
    AND WeightFactor = a.WeightFactor); 
-- 4    
UPDATE facttechflaghoursbyday
SET techkey = (
  SELECT techkey
  FROM dimtech
  WHERE technumber = '620'
    AND currentrow = true)
WHERE techkey = 101;
           
-- 621 UPDATE old row
--DECLARE @NowTS timestamp;
--@NowTS = (SELECT now() FROM system.iota);
UPDATE dimtech
SET RowChangeDate = curdate(), --CAST(@NowTS AS sql_date),
    RowChangeDateKey = (SELECT datekey FROM day WHERE thedate = curdate()),--CAST(@NowTS AS sql_date)),
    RowThruTS = (SELECT now() FROM system.iota),
    CurrentRow = false, 
    RowChangeReason = 'Employee to DeptTech',
    TechKeyThruDate = CAST('04/03/2012' AS sql_date),
    TechKeyThruDateKey = (SELECT datekey FROM day WHERE thedate = CAST('04/03/2012' AS sql_date))
WHERE techkey = 70;

INSERT INTO dimtech (storecode, Description, technumber, laborcost, flagdeptcode, 
  flagdept, currentrow, rowfromts, rowthruts, 
  techkeyfromdate, techkeyfromdatekey, 
  techkeythrudate, techkeythrudatekey)
values ('RY1', 'Recon Tech3', '621', 15, 'RE', 
  'Detail', true, (SELECT now() FROM system.iota), CAST('12/31/9999 00:00:00' AS sql_timestamp), 
  CAST('04/03/2012' AS sql_date), (SELECT datekey FROM day WHERE thedate = CAST('04/03/2012' AS sql_date)),
  CAST('12/31/9999' AS sql_date), 7306);
  
-- 1  
INSERT INTO keymapDimTech
SELECT TechKey, StoreCode, TechNumber
FROM dimTech a
WHERE NOT EXISTS (
  SELECT 1
  FROM keymapDimTech
  WHERE TechKey = a.TechKey);
-- 2  
INSERT INTO bridgeTechGroup (TechKey, WeightFactor)        
SELECT c.techkey, d.weightfactor
FROM (
  SELECT a.techkey
  FROM dimtech a
  WHERE NOT EXISTS (
    SELECT 1
    FROM bridgetechgroup b 
    where techkey = a.techkey)) c,
(
  SELECT n/100.0 AS weightfactor
  FROM tally
  WHERE n BETWEEN 1 AND 100) d;       
-- 3
INSERT INTO keymapBridgeTechGroup
SELECT TechGroupKey, TechKey, WeightFactor
FROM BridgeTechGroup a
WHERE NOT EXISTS (
  SELECT 1
  FROM keymapBridgeTechGroup
  WHERE TechKey = a.TechKey
    AND WeightFactor = a.WeightFactor); 
-- 4    
UPDATE facttechflaghoursbyday
SET techkey = (
  SELECT techkey
  FROM dimtech
  WHERE technumber = '621'
    AND currentrow = true)
WHERE techkey = 70;    
 
UPDATE dimtech
SET flagdeptcode = 'NA',
    flagdept = 'NA'
WHERE flagdeptcode IS NULL;

UPDATE dimtech
SET employeenumber = 'NA'
WHERE employeenumber IS NULL;

/*
-- durable key
drop TABLE keyMapDimTechDurable; 
CREATE TABLE keyMapDimTechDurable (
  techKeyDurable autoinc, 
  StoreCode cichar(3),
  Technumber cichar(3),
  FlagDeptCode cichar(2)) IN database;
  
INSERT INTO keyMapDimTechDurable (storecode, technumber, flagdeptcode)      
SELECT DISTINCT storecode, technumber, flagdeptcode
FROM dimtech;

ALTER TABLE dimtech
ADD COLUMN TechKeyDurable integer;
DROP index dimtech.techkeydurable;
ALTER TABLE dimtech
drop COLUMN TechKeyDurable;

-- manually reorder columns IN dimtech ADD index WHILE there
*/
/*
 gulled
the old record must be closed, tkfrom/thru generates the dept daily COUNT
current row ON D17 remains true
fuck feels LIKE i am confused about closing out a type2 old row versus ekthru
that actually IS ALL i am doing here, technumber D17 techkey 201 techkey thrudate,
so it IS NOT a change to a new tecchkey for the store/tech#/flagdept (the pseudo durable NK)u
SIGH, leave it for now with the type2 metadata
NOT HAPPY, oh well
*/
UPDATE dimtech 
SET rowchangedate = curdate(),
    rowchangedatekey = (SELECT datekey FROM day WHERE thedate = curdate()),
    rowthruts = now(),
    rowchangereason = 'emp to new tech#/dept',
    techkeythrudate = '11/10/2012',
    techkeythrudatekey = (SELECT datekey FROM day WHERE thedate = '11/10/2012')  
WHERE techkey = 201;

INSERT INTO dimtech (storecode, employeenumber, name, technumber, laborcost, 
  flagdeptcode, flagdept, currentrow, rowfromts, rowthruts, techkeyfromdate,
  techkeyfromdatekey, techkeythrudate, techkeythrudatekey)
SELECT stco#, stpyemp#, b.name, sttech, stlrat,
  'BS', 'Body Shop', true, now(), CAST('12/31/9999 00:00:00' AS sql_timestamp), '11/11/2012',
  (SELECT datekey FROM day WHERE thedate = '11/11/2012'), '12/31/9999', 7306
FROM tmpsdptech a
LEFT JOIN edwEmployeeDim b ON a.stpyemp# = b.employeenumber
  AND b.currentrow = true
WHERE sttech = '215';  

-- forgot ALL the mapping shit
-- need to DO the entire dimTech exe
-- INSERT INTO keymapDimTech
-- 1  
INSERT INTO keymapDimTech
SELECT TechKey, StoreCode, TechNumber
FROM dimTech a
WHERE NOT EXISTS (
  SELECT 1
  FROM keymapDimTech
  WHERE TechKey = a.TechKey);
-- 2  
INSERT INTO bridgeTechGroup (TechKey, WeightFactor)        
SELECT c.techkey, d.weightfactor
FROM (
  SELECT a.techkey
  FROM dimtech a
  WHERE NOT EXISTS (
    SELECT 1
    FROM bridgetechgroup b 
    where techkey = a.techkey)) c,
(
  SELECT n/100.0 AS weightfactor
  FROM tally
  WHERE n BETWEEN 1 AND 100) d;       
-- 3
INSERT INTO keymapBridgeTechGroup
SELECT TechGroupKey, TechKey, WeightFactor
FROM BridgeTechGroup a
WHERE NOT EXISTS (
  SELECT 1
  FROM keymapBridgeTechGroup
  WHERE TechKey = a.TechKey
    AND WeightFactor = a.WeightFactor);   
/*  
-- now, since i am fucking doing everything manually
-- WHERE does ALL the fucking validation go  
-- 1/20  
-- batch
-- fuck, just DO a timestamp
ALTER TABLE zproclog
ADD COLUMN pass logical;
ALTER TABLE zproclog
alter COLUMN BatchNumber BatchNumber numeric(24,12);
ALTER TABLE zproclog
DROP COLUMN batchnumber
ADD COLUMN BatchNumber timestamp;

UPDATE zproclog
SET batchnumber = (
  SELECT year(curdate())*10000 + month(curdate())*100 + dayofmonth(curdate())
    + (hour(now())*10000 + minute(now())*100 + second(now()))/1000000.0 
  FROM system.iota)
WHERE CAST(startts AS sql_date) = curdate()  
*/            

-- chris kronlund
UPDATE dimtech
SET techkeythrudate = '11/21/2012',
    techkeythrudatekey = (SELECT datekey FROM day WHERE thedate = '11/21/2012') 
WHERE techkey = 97  