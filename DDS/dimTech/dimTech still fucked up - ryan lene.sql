ryan lene
a rehire IN body shop
given a new tech number (old tech# 242), 267

this IS the problem, currently 3 rows IN dim tech, 2 for 242, the second one IS
the nutty notion that i have implemented of an inactive tech HAVING a separate
dimtech row WHERE the thrudate IS 12/31/9999

what IS type2 what IS type1
what does active MEAN
what does techKeyFrom/Thru MEAN

this IS now causing body shop to break

current reality
techkey   technumber     active    techkeyfrom  techkeythru
100       242            true      10/13/2008   9/6/2012
266       242            false     9/7/2012     12/31/9999
518       267            true      1/5/15       12/31/9999

body shop query looks for techs with techkey thrudate = 12/31/9999

ok, no rows with the inactive techkey
SELECT *
FROM factrepairorder WHERE techkey = 266                            

SELECT *
FROM dimtech
WHERE description LIKE '%lene%'

so, what i am doing IS
DELETE row for techkey 266
make techkey active = false
that should be good

BEGIN TRANSACTION;
TRY 
  DELETE FROM keymapDimTech
  WHERE techkey = 266;
  DELETE 
  FROM dimTech
  WHERE techkey = 266;
  UPDATE dimtech
    SET active = false       
  WHERE techkey = 100;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  


-- here IS a rough approx of the SET that need to be fixed/understood

SELECT *
FROM dimtech a
WHERE EXISTS (
  SELECT 1
  FROM dimtech
  WHERE storecode = a.storecode
    AND technumber = a.technumber
    AND rowchangereason = 'active to inactive')
ORDER BY storecode, technumber    

-- 9/9/15
a continuation of this same discussion
trying to generate a census for adjusted cost of labor on a daily basis
AND have 2 techs (swanson AND magnuson) that have more than 1 "active" technumber
which makes no sense
this IS again caused BY the misguided notion of active to inactive being a type 2
change that requires a new row WHERE the tech IS inactive but the new
techkey IS "Active"

the END result, i believe should be that any person IN dimtech should have 
only 1 active row at the most, AND that active row will be the current row,
though for a tech no longer IN service, the currentrow will be active = false


so, this IS NOT fixing everything at the moment, but just those 2 techs

SELECT * FROM dimtech WHERE employeenumber = '1134524' --in (SELECT employeenumber FROM edwEmployeeDim WHERE lastname = 'swanson')
the bogus techkey IS 649, for technumber D50
-- check ros
SELECT * FROM factrepairorder WHERE techkey = 649

BEGIN TRANSACTION;
TRY 
  DELETE FROM keymapDimTech
  WHERE techkey = 649;
  DELETE 
  FROM dimTech
  WHERE techkey = 649;
  UPDATE dimtech
    SET active = false       
  WHERE techkey IN (529,578);
  UPDATE dimtech
  SET rowchangereason = 'flagdept technumber'
  WHERE techkey = 578;
  UPDATE dimtech
  SET rowfromts = '08/13/2015 01:15:05'
  WHERE techkey = 652;  
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY; 

SELECT * FROM dimtech WHERE employeenumber = '190600' --in (SELECT employeenumber FROM edwEmployeeDim WHERE lastname = 'magnuson')

SELECT * FROM factrepairorder WHERE techkey IN (429, 647, 577)
SELECT techkey, max(b.thedate) FROM factrepairorder a inner join day b on a.opendatekey = b.datekey WHERE techkey IN (196,440,456) GROUP BY techkey

ok, this one IS a bit different, tyler IS NOT currently flagging hours, does NOT
have a current tech number, hourly
currentrow becomes the most recent row

BEGIN TRANSACTION;
TRY 
  DELETE FROM keymapDimTech
  WHERE techkey IN (429, 647, 577, 454);
  DELETE 
  FROM dimTech
  WHERE techkey IN (429, 647, 577, 454);  
  UPDATE dimtech
  SET active = false       
  WHERE techkey IN (529,578, 196,440,456);
  UPDATE dimtech
  SET currentrow = true,
      techkeythrudate = '03/20/2015',
      techkeythrudatekey = (SELECT datekey FROM day WHERE thedate = '03/20/2015')
  WHERE techkey = 456;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY; 

AND now, tyler espelien AS well
SELECT * FROM dimtech WHERE employeenumber = '241310'
SELECT * FROM factrepairorder WHERE techkey = 287

BEGIN TRANSACTION;
TRY 
  DELETE FROM keymapDimTech
  WHERE techkey = 287;
  DELETE 
  FROM dimTech
  WHERE techkey = 287;
  UPDATE dimtech
  SET active = false       
  WHERE techkey = 287;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY; 



SELECT * FROM dimtech WHERE employeenumber = '1109827' --in (SELECT employeenumber FROM edwEmployeeDim WHERE lastname = 'perez')

SELECT b.thedate, a.* FROM factrepairorder a inner join day b on a.opendatekey = b.datekey WHERE techkey = 534


SELECT * FROM dimtech WHERE employeenumber in (SELECT employeenumber FROM edwEmployeeDim WHERE lastname = 'larson')


SELECT * FROM dimtech WHERE employeenumber = '1148010' ORDER BY technumber, techkey

SELECT * FROM factrepairorder WHERE techkey IN (301,628)

BEGIN TRANSACTION;
TRY 
  DELETE FROM keymapDimTech
  WHERE techkey IN (301,628);
  DELETE 
  FROM dimTech
  WHERE techkey IN (301,628);
  UPDATE dimtech
  SET active = false       
  WHERE techkey IN  (211,201,549);
  UPDATE dimtech
  SET currentrow = true
  WHERE techkey = 549;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;

-- 9/16 jacob berry

SELECT * FROM dimtech WHERE employeenumber = '113560' --in (SELECT employeenumber FROM edwEmployeeDim WHERE lastname = 'perez')

SELECT b.thedate, a.* FROM factrepairorder a inner join day b on a.opendatekey = b.datekey WHERE techkey IN (431,575)


SELECT * FROM dimtech WHERE employeenumber in (SELECT employeenumber FROM edwEmployeeDim WHERE lastname = 'larson')


SELECT * FROM dimtech WHERE employeenumber = '1148010' ORDER BY technumber, techkey

SELECT * FROM factrepairorder WHERE techkey IN (301,628)

BEGIN TRANSACTION;
TRY 
  DELETE FROM keymapDimTech
  WHERE techkey IN (431,575);
  DELETE 
  FROM dimTech
  WHERE techkey IN (431,575);
  UPDATE dimtech
  SET active = false       
  WHERE techkey IN  (352,451);
  UPDATE dimtech
  SET currentrow = true
  WHERE techkey IN  (352,451);
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;