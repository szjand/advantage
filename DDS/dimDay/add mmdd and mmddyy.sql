UPDATE day
  set mmdd = 
    LEFT(
       CASE 
         WHEN month(thedate) < 10 THEN 
           '0' + trim(cast(month(thedate) AS sql_char))
           + '-' +
           CASE 
             WHEN dayofmonth(thedate) < 10 THEN
               '0' + TRIM(CAST(dayofmonth(thedate) AS sql_char))
             ELSE TRIM(CAST(dayofmonth(thedate) AS sql_char))
           END 
         ELSE 
           trim(cast(month(thedate) AS sql_char))
           + '-' + 
           CASE 
             WHEN dayofmonth(thedate) < 10 THEN
               '0' + TRIM(CAST(dayofmonth(thedate) AS sql_char))
             ELSE TRIM(CAST(dayofmonth(thedate) AS sql_char))
           END      
       END, 5)
 
 UPDATE day
   SET mmddyy = 
     LEFT (
       CASE 
         WHEN month(thedate) < 10 THEN 
           '0' + trim(cast(month(thedate) AS sql_char))
           + '-' +
           CASE 
             WHEN dayofmonth(thedate) < 10 THEN
               '0' + TRIM(CAST(dayofmonth(thedate) AS sql_char))
             ELSE TRIM(CAST(dayofmonth(thedate) AS sql_char))
           END
           + '-' +
           right(trim(cast(year(thedate) AS sql_char)), 2) 
         ELSE 
           trim(cast(month(thedate) AS sql_char))
           + '-' + 
           CASE 
             WHEN dayofmonth(thedate) < 10 THEN
               '0' + TRIM(CAST(dayofmonth(thedate) AS sql_char))
             ELSE TRIM(CAST(dayofmonth(thedate) AS sql_char))
           END
           + '-' +
           right(trim(cast(year(thedate) AS sql_char)), 2)            
       END, 8)