SELECT MAX(thedate) 
FROM dds.day
WHERE thedate <> '12/31/9999'

SELECT MIN(thedate), MAX(thedate)
FROM dds.day
WHERE datetype = 'date'
  AND theyear > 2012
GROUP BY sundayToSaturdayWeek

holidays: 
  memorial day: last monday of may
  labor day: first monday of september
  thanksgiving: thursday of the last full week of november (Last Saturday of November - 2)
  christmas day
  new years day
  
SELECT *
FROM dds.day
WHERE holiday = true  


SELECT b.thedate, b.dayname, SUM(holidayhours), max(b.holiday)
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
WHERE holidayhours > 0
GROUP BY b.thedate, b.dayname
ORDER BY thedate

-- memorial day: last monday of may
UPDATE dds.day
SET holiday = true
WHERE theDate IN (
  SELECT max(thedate)
  FROM dds.day
  WHERE datetype = 'date'
    AND monthOfYear = 5
    AND dayofWeek = 2
  GROUP BY theYear)

-- labor day: first monday of september  
UPDATE dds.day
SET holiday = true
WHERE theDate IN (
  SELECT min(thedate)
  FROM dds.day
  WHERE datetype = 'date'
    AND monthOfYear = 9
    AND dayofWeek = 2
  GROUP BY theYear)
  
-- christmas, newyears, 4th july
UPDATE dds.day
SET holiday = true
WHERE (
  (monthOfYear = 12 AND dayOfMonth = 25)
  OR (monthOfYear = 1 AND dayOfMonth = 1)
  OR (monthOfYear = 7 AND dayOfMonth = 4))
  
-- thanksgiving: thursday of the last full week of november (Last Saturday of November - 2)
UPDATE dds.day
SET holiday = true
WHERE theDate IN (
  SELECT MAX(thedate) - 2
  FROM dds.day
  WHERE datetype = 'date'
    AND monthOfYear = 11
    AND dayofWeek = 7
  GROUP BY theYear)  
  
UPDATE dds.day
SET holiday = false
WHERE holiday IS NULL 

ALTER TABLE dds.day ALTER holiday holiday logical constraint NOT null