DROP index z_jon_day.datekey;
DROP index z_jon_day.thedate;

DROP TABLE z_jon_day;
CREATE TABLE z_jon_day ( 
      DateKey integer,
      TheDate Date,
      DayOfWeek Integer,
      DayName CIChar( 12 ),
      DayOfMonth Integer,
      DayOfYear Integer,
      MonthOfYear Integer,
      MonthName CIChar( 12 ),
      FirstDayOfMonth Logical,
      LastDayOfMonth Logical,	
      WeekDay Logical,
      Weekend Logical,
      Holiday Logical,
      TheYear Integer,
      ISOWeek Integer) IN DATABASE;
			
CREATE unique index datekey on z_jon_day(datekey);
CREATE unique index thedate on z_jon_day(thedate);	
		
INSERT INTO z_jon_day(datekey,thedate,dayofweek,dayname,dayofmonth,dayofyear,
  monthofyear,monthname,firstdayofmonth,lastdayofmonth,weekday,weekend,holiday,
	theyear,isoweek)	
SELECT datekey,thedate,dayofweek,dayname,dayofmonth,dayofyear,
  monthofyear,monthname,firstdayofmonth,lastdayofmonth,weekday,weekend,holiday,
	theyear,isoweek
FROM day;		

ALTER TABLE z_jon_day ALTER COLUMN datekey datekey autoinc;


DECLARE @i integer;
DECLARE @Date date;
DECLARE @tf logical;
@Date = '12/30/2023'; -- starting date - 1
@i = 1;
TRY 
  WHILE @i < 9864 DO -- this should take us to 12/31/2050
    @tf = iif(month((@Date + @i) + 1) = month(@Date + @i), false, true);
    INSERT INTO z_jon_day (TheDate, DayOfWeek, DayName, DayOfMonth, DayOfYear,
      MonthOfYear, MonthName, FirstDayOfMonth, LastDayOfMonth, WeekDay, Weekend,
	  Holiday, TheYear, ISOWeek)
    values(
      @Date + @i,
      DayOfWeek(@Date + @i),
      DayName(@Date + @i),
      DayOfMonth(@Date + @i),
      DayOfYear(@Date + @i),
      Month(@Date + @i),
      MonthName(@Date + @i),
      case 
        when DayOfMonth(@Date + @i) = 1 THEN true 
        ELSE false
      END,
      @tf,
      case 
        when DayOfWeek NOT IN (1,7) THEN True 
        ELSE false
      end,
      case 
        when DayOfWeek IN (1,7) THEN true 
        ELSE false
      END,
	  NULL, 
	  Year(@Date + @i),
	  ISOWeek(@Date + @i));
    @i = @i + 1;
  END WHILE; 
CATCH ALL
  RAISE;
END TRY; 


-- christmas, newyears, 4th july
UPDATE z_jon_day
SET holiday = true
WHERE (
  (monthOfYear = 12 AND dayOfMonth = 25)
  OR (monthOfYear = 1 AND dayOfMonth = 1)
  OR (monthOfYear = 7 AND dayOfMonth = 4));
  
-- thanksgiving: thursday of the last full week of november (Last Saturday of November - 2)
UPDATE z_jon_day
SET holiday = true
WHERE theDate IN (
  SELECT MAX(thedate) - 2
  FROM z_jon_day
  WHERE monthOfYear = 11
    AND dayofWeek = 7
  GROUP BY theYear);  
  
UPDATE z_jon_day
SET holiday = false
WHERE holiday IS NULL; 

ALTER TABLE day ALTER COLUMN datekey datekey integer;

-- INSERT INTO day
SELECT *
FROM z_jon_day a
WHERE NOT EXISTS (
  SELECT 1
	FROM day
	WHERE thedate = a.thedate);

