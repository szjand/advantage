DROP TABLE Month;
CREATE TABLE Month ( 
      MonthKey AutoInc,
      MonthOfYear Integer,
      MonthName CIChar( 12 ),
      MonthAbbr CIChar(3),
      TheYear Integer) IN DATABASE;
      
INSERT INTO month (MonthOfYear, MonthName, MonthAbbr, TheYear)
SELECT d.MonthOfYear, d.MonthName,
  CASE d.MonthOfYear
    WHEN 1 THEN 'Jan'
    WHEN 2 THEN 'Feb'
    WHEN 3 THEN 'Mar'
    WHEN 4 THEN 'Apr'
    WHEN 5 THEN 'May'
    WHEN 6 THEN 'Jun'
    WHEN 7 THEN 'Jul'
    WHEN 8 THEN 'Aug'
    WHEN 9 THEN 'Sep'
    WHEN 10 THEN 'Oct'
    WHEN 11 THEN 'Nov'
    WHEN 12 THEN 'Dec'
  END, d.TheYear
FROM (  
  SELECT DISTINCT MonthOfYear, MonthName, TheYear
  FROM Day) d;      