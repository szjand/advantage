﻿"E:\sql\advantage\DDS\dimDay\working_days-postgres.sql"
-- drop table if exists dds.working_days;
-- create table dds.working_days (
--   department citext,
--   the_date date,
--   wd_in_year integer, 
--   wd_of_year_elapsed integer, 
--   wd_of_year_remaining integer, 
--   wd_of_year integer, 
--   wd_in_month integer, 
--   wd_of_month_elapsed integer, 
--   wd_of_month_remaining integer,  
--   wd_of_month integer, 
--   wd_in_biweekly_pay_period integer, 
--   wd_of_biweekly_pay_period_elapsed integer, 
--   wd_of_bi_weekly_pay_period_remaining integer,   
--   wd_of_biweekly_pay_period integer,
--   primary key (department,the_date));
-- comment on table dds.working_days is 'department specific working day calendar, 2009 - 2023, sales,pdq:mon-sat, service,parts,body shop,detail: mon-fri, carwash: all except xmas, new year, thanksgiving';
-- create index on dds.working_days(the_date); 

/*
departments & working days: 
  mon - fri: service, parts, body shop, detail
  mon - sat: pdq, sales
  all exc thanksgiving, ny, xmas: carwash
*/
-- sales
drop table if exists wd;
create temp table wd as
select 'carwash'::citext as department, x.the_date, 
  x.wd_in_year, 
  row_number() over (partition by the_year order by the_date) - 1 as wd_of_year_elapsed, 
  wd_in_year - row_number() over (partition by the_year order by the_date) + 1 as wd_of_year_remaining,
  row_number() over (partition by the_year order by the_date) as wd_of_year, 
  x.wd_in_month, 
  row_number() over (partition by year_month order by the_date) - 1 as wd_of_month_elapsed, 
  wd_in_month - row_number() over (partition by year_month order by the_date) + 1 as wd_of_month_remaining, 
  row_number() over (partition by year_month order by the_date) as wd_of_month,  
  x.wd_in_biweekly_pay_period,
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) - 1 as wd_of_biweekly_pay_period_elapsed, 
  wd_in_biweekly_pay_period - row_number() over (partition by biweekly_pay_period_sequence order by the_date) + 1 as wd_of_bi_weekly_pay_period_remaining,   
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) as wd_of_biweekly_pay_period
from (
  select the_year, year_month, the_date, biweekly_pay_period_sequence, day_name, first_of_month, last_of_month,
    count(*) over (partition by the_year) as wd_in_year,
    count(*) over (partition by year_month) as wd_in_month,
    count(*) over (partition by biweekly_pay_period_sequence) as wd_in_biweekly_pay_period
  from dds.dim_date 
--   where not holiday
--     and day_of_week <> 1 -- mon-sat
--     and day_of_week between 2 and 6 -- mon-fri
  where the_year > 2008 -- carwash
    and the_date not in (select the_date from dds.dim_date where month_of_year = 1 and holiday)
    and the_date not in (select the_date from dds.dim_date where month_of_year = 11 and holiday)
    and the_date not in (select the_date from dds.dim_date where month_of_year = 12 and holiday)  
  order by the_date) x;  
      
insert into dds.working_days(department, the_date)
select 'carwash', the_date
from dds.dim_date
where the_year > 2008;

update dds.working_days y
set wd_in_year = z.wd_in_year,
    wd_of_year_elapsed = z.wd_of_year_elapsed,
    wd_of_year_remaining = z.wd_of_year_remaining ,
    wd_of_year = z.wd_of_year ,
    wd_in_month = z.wd_in_month,
    wd_of_month_elapsed = z.wd_of_month_elapsed ,
    wd_of_month_remaining = z.wd_of_month_remaining ,
    wd_of_month = z.wd_of_month ,
    wd_in_biweekly_pay_period = z.wd_in_biweekly_pay_period,
    wd_of_biweekly_pay_period_elapsed = z.wd_of_biweekly_pay_period_elapsed ,
    wd_of_bi_weekly_pay_period_remaining = z.wd_of_bi_weekly_pay_period_remaining  ,
    wd_of_biweekly_pay_period = z.wd_of_biweekly_pay_period
from (
  select cal_date, 'carwash'::citext as department,  
    case x.day_of_month
      when 1 then 
        coalesce(coalesce(wd_in_year, lead(x.wd_in_year, 1) over (order by cal_date)), lead(x.wd_in_year, 2) over (order by cal_date))
      else 
        coalesce(coalesce(wd_in_year, lag(x.wd_in_year, 1) over (order by cal_date)), lag(x.wd_in_year, 2) over (order by cal_date)) 
    end as wd_in_year,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_year_elapsed, lead(x.wd_of_year_elapsed, 1) over (order by cal_date)), lead(x.wd_of_year_elapsed, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_of_year_elapsed, lag(x.wd_of_year_elapsed, 1) over (order by cal_date)), lag(x.wd_of_year_elapsed, 2) over (order by cal_date)) 
    end as wd_of_year_elapsed,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_year_remaining, lead(x.wd_of_year_remaining, 1) over (order by cal_date)), lead(x.wd_of_year_remaining, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_of_year_remaining, lag(x.wd_of_year_remaining, 1) over (order by cal_date)), lag(x.wd_of_year_remaining, 2) over (order by cal_date))
    end as wd_of_year_remaining,
    coalesce(wd_of_year, 0) as wd_of_year,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_in_month, lead(x.wd_in_month, 1) over (order by cal_date)), lead(x.wd_in_month, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_in_month, lag(x.wd_in_month, 1) over (order by cal_date)), lag(x.wd_in_month, 2) over (order by cal_date))
    end as wd_in_month,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date)), lead(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
      else
        coalesce(coalesce(wd_of_month_elapsed, lag(x.wd_of_month_elapsed, 1) over (order by cal_date)), lag(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
    end as wd_of_month_elapsed,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_month_remaining, lead(x.wd_of_month_remaining, 1) over (order by cal_date)), lead(x.wd_of_month_remaining, 2) over (order by cal_date)) 
      else
        coalesce(coalesce(wd_of_month_remaining, lag(x.wd_of_month_remaining, 1) over (order by cal_date)), lag(x.wd_of_month_remaining, 2) over (order by cal_date)) 
    end as wd_of_month_remaining,
    coalesce(wd_of_month, 0) as wd_of_month, -- any non working day = 0
    coalesce(coalesce(wd_in_biweekly_pay_period, lag(x.wd_in_biweekly_pay_period, 1) over (order by cal_date)), lag(x.wd_in_biweekly_pay_period, 2) over (order by cal_date)) as wd_in_biweekly_pay_period,
    coalesce(coalesce(wd_of_biweekly_pay_period_elapsed, lag(x.wd_of_biweekly_pay_period_elapsed, 1) over (order by cal_date)), lag(x.wd_of_biweekly_pay_period_elapsed, 2) over (order by cal_date)) as wd_of_biweekly_pay_period_elapsed,
    coalesce(coalesce(wd_of_bi_weekly_pay_period_remaining, lag(x.wd_of_bi_weekly_pay_period_remaining, 1) over (order by cal_date)), lag(x.wd_of_bi_weekly_pay_period_remaining, 2) over (order by cal_date)) as wd_of_bi_weekly_pay_period_remaining,
    coalesce(coalesce(wd_of_biweekly_pay_period, lag(x.wd_of_biweekly_pay_period, 1) over (order by cal_date)), lag(x.wd_of_biweekly_pay_period, 2) over (order by cal_date)) as wd_of_biweekly_pay_period
  from (  
    select a.the_date as cal_date, a.day_of_month, b.*
    from dds.dim_date a
    left join wd b on a.the_Date = b.the_date
    where a.the_year > 2008) x) z
where y.the_date = z.cal_date
  and y.department = z.department ;

select department, count(*)
from dds.working_days
group by department





---------------------------------------------------------------------------------------------------------- 
-- this is what i originally put into dds.dim_date, mon-sat
---------------------------------------------------------------------------------------------------------- 
drop table if exists wd;
create temp table wd as
select x.the_date, 
  x.wd_in_year, 
  row_number() over (partition by the_year order by the_date) - 1 as wd_of_year_elapsed, 
  wd_in_year - row_number() over (partition by the_year order by the_date) + 1 as wd_of_year_remaining,
  row_number() over (partition by the_year order by the_date) as wd_of_year, 
  
  x.wd_in_month, 
  row_number() over (partition by year_month order by the_date) - 1 as wd_of_month_elapsed, 
  wd_in_month - row_number() over (partition by year_month order by the_date) + 1 as wd_of_month_remaining, 
  row_number() over (partition by year_month order by the_date) as wd_of_month,
  
  x.wd_in_biweekly_pay_period,
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) - 1 as wd_of_biweekly_pay_period_elapsed, 
  wd_in_biweekly_pay_period - row_number() over (partition by biweekly_pay_period_sequence order by the_date) + 1 as wd_of_bi_weekly_pay_period_remaining,   
  row_number() over (partition by biweekly_pay_period_sequence order by the_date) as wd_of_biweekly_pay_period
  
from (
  select the_year, year_month, the_date, biweekly_pay_period_sequence, day_name, first_of_month, last_of_month,
    count(*) over (partition by the_year) as wd_in_year,
    count(*) over (partition by year_month) as wd_in_month,
    count(*) over (partition by biweekly_pay_period_sequence) as wd_in_biweekly_pay_period
  from dds.dim_date 
  where not holiday
    and day_of_week <> 1
    and the_year > 2008
  order by the_date) x;  

select * from wd

alter table dds.dim_date
add column wd_in_year integer, 
add column wd_of_year_elapsed integer, 
add column wd_of_year_remaining integer, 
add column wd_of_year integer, 
add column wd_in_month integer, 
add column wd_of_month_elapsed integer, 
add column wd_of_month_remaining integer,  
add column wd_of_month integer, 
add column wd_in_biweekly_pay_period integer, 
add column wd_of_biweekly_pay_period_elapsed integer, 
add column wd_of_bi_weekly_pay_period_remaining integer,   
add column wd_of_biweekly_pay_period integer;


-- this works, including 2 consecutive nulls: 09/02, 09/03
-- there are no instances of 3 nulls in a row, that would require 2 holidays on consecutive days
-- select * from wd limit 10
-- on the first of each month, need the following value, not the preceding value

update dds.dim_date y
set wd_in_year = z.wd_in_year,
    wd_of_year_elapsed = z.wd_of_year_elapsed,
    wd_of_year_remaining = z.wd_of_year_remaining ,
    wd_of_year = z.wd_of_year ,
    wd_in_month = z.wd_in_month,
    wd_of_month_elapsed = z.wd_of_month_elapsed ,
    wd_of_month_remaining = z.wd_of_month_remaining ,
    wd_of_month = z.wd_of_month ,
    wd_in_biweekly_pay_period = z.wd_in_biweekly_pay_period,
    wd_of_biweekly_pay_period_elapsed = z.wd_of_biweekly_pay_period_elapsed ,
    wd_of_bi_weekly_pay_period_remaining = z.wd_of_bi_weekly_pay_period_remaining  ,
    wd_of_biweekly_pay_period = z.wd_of_biweekly_pay_period
from (
  select cal_date,    
    case x.day_of_month
      when 1 then 
        coalesce(coalesce(wd_in_year, lead(x.wd_in_year, 1) over (order by cal_date)), lead(x.wd_in_year, 2) over (order by cal_date))
      else 
        coalesce(coalesce(wd_in_year, lag(x.wd_in_year, 1) over (order by cal_date)), lag(x.wd_in_year, 2) over (order by cal_date)) 
    end as wd_in_year,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_year_elapsed, lead(x.wd_of_year_elapsed, 1) over (order by cal_date)), lead(x.wd_of_year_elapsed, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_of_year_elapsed, lag(x.wd_of_year_elapsed, 1) over (order by cal_date)), lag(x.wd_of_year_elapsed, 2) over (order by cal_date)) 
    end as wd_of_year_elapsed,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_year_remaining, lead(x.wd_of_year_remaining, 1) over (order by cal_date)), lead(x.wd_of_year_remaining, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_of_year_remaining, lag(x.wd_of_year_remaining, 1) over (order by cal_date)), lag(x.wd_of_year_remaining, 2) over (order by cal_date))
    end as wd_of_year_remaining,
    coalesce(wd_of_year, 0) as wd_of_year,
--     case x.day_of_month
--       when 1 then
--         coalesce(coalesce(wd_of_year, lead(x.wd_of_year, 1) over (order by cal_date)), lead(x.wd_of_year, 2) over (order by cal_date))
--       else
--         coalesce(coalesce(wd_of_year, lag(x.wd_of_year, 1) over (order by cal_date)), lag(x.wd_of_year, 2) over (order by cal_date))
--     end as wd_of_year,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_in_month, lead(x.wd_in_month, 1) over (order by cal_date)), lead(x.wd_in_month, 2) over (order by cal_date))
      else
        coalesce(coalesce(wd_in_month, lag(x.wd_in_month, 1) over (order by cal_date)), lag(x.wd_in_month, 2) over (order by cal_date))
    end as wd_in_month,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_month_elapsed, lead(x.wd_of_month_elapsed, 1) over (order by cal_date)), lead(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
      else
        coalesce(coalesce(wd_of_month_elapsed, lag(x.wd_of_month_elapsed, 1) over (order by cal_date)), lag(x.wd_of_month_elapsed, 2) over (order by cal_date)) 
    end as wd_of_month_elapsed,
    case x.day_of_month
      when 1 then
        coalesce(coalesce(wd_of_month_remaining, lead(x.wd_of_month_remaining, 1) over (order by cal_date)), lead(x.wd_of_month_remaining, 2) over (order by cal_date)) 
      else
        coalesce(coalesce(wd_of_month_remaining, lag(x.wd_of_month_remaining, 1) over (order by cal_date)), lag(x.wd_of_month_remaining, 2) over (order by cal_date)) 
    end as wd_of_month_remaining,
    coalesce(wd_of_month, 0) as wd_of_month,
--     coalesce(coalesce(wd_of_month, lag(x.wd_of_month, 1) over (order by cal_date)), lag(x.wd_of_month, 2) over (order by cal_date)) as wd_of_month,
    coalesce(coalesce(wd_in_biweekly_pay_period, lag(x.wd_in_biweekly_pay_period, 1) over (order by cal_date)), lag(x.wd_in_biweekly_pay_period, 2) over (order by cal_date)) as wd_in_biweekly_pay_period,
    coalesce(coalesce(wd_of_biweekly_pay_period_elapsed, lag(x.wd_of_biweekly_pay_period_elapsed, 1) over (order by cal_date)), lag(x.wd_of_biweekly_pay_period_elapsed, 2) over (order by cal_date)) as wd_of_biweekly_pay_period_elapsed,
    coalesce(coalesce(wd_of_bi_weekly_pay_period_remaining, lag(x.wd_of_bi_weekly_pay_period_remaining, 1) over (order by cal_date)), lag(x.wd_of_bi_weekly_pay_period_remaining, 2) over (order by cal_date)) as wd_of_bi_weekly_pay_period_remaining,
    coalesce(coalesce(wd_of_biweekly_pay_period, lag(x.wd_of_biweekly_pay_period, 1) over (order by cal_date)), lag(x.wd_of_biweekly_pay_period, 2) over (order by cal_date)) as wd_of_biweekly_pay_period
  from (  
    select a.the_date as cal_date, a.day_of_month, b.*
    from dds.dim_date a
    left join wd b on a.the_Date = b.the_date) x) z
where y.the_date = z.cal_date ;   
