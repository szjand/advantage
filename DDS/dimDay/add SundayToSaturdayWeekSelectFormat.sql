ALTER TABLE day
ADD COLUMN SundayToSaturdayWeekSelectFormat cichar(65);

UPDATE day
SET SundayToSaturdayWeekSelectFormat = z.display
FROM (
SELECT sundaytosaturdayweek, TRIM(pt1) + ' ' + TRIM(pt2) AS display
FROM (
  SELECT distinct sundaytosaturdayweek, fromdate, thrudate, 
    (SELECT trim(monthname) + ' '
      + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
      FROM day
      WHERE thedate = a.fromdate) AS pt1,
      
    (SELECT trim(monthname) + ' ' 
      + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
      + TRIM(CAST(theyear AS sql_char))
      FROM day
      WHERE thedate = a.thrudate) AS pt2
  FROM (
    SELECT sundaytosaturdayweek, MIN(thedate) AS fromdate, MAX(thedate) AS thrudate
    FROM day 
    GROUP BY sundaytosaturdayweek) a) y) z
WHERE day.sundaytosaturdayweek = z.sundaytosaturdayweek    

