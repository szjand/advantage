-- 6/3 decide need seconds
-- DROP TABLE TimeOfDay;

DECLARE @time Time;  
DECLARE @hour integer;
DECLARE @minute integer;
DECLARE @second integer;
DECLARE @time_st string;
DECLARE @hour_st string;
DECLARE @minute_st string;
DECLARE @second_st string;


CREATE TABLE TimeOfDay (
  TimeOfDayKey autoinc,
  theTime Time) IN database;
@hour = 0;
@minute = 0;
@second = 0;
TRY 
  WHILE @hour < 24 DO
    WHILE @minute < 60 DO
      WHILE @second < 60 DO
        @hour_st = IIF(@hour < 10, '0' + trim(CAST(@hour AS sql_char)), trim(CAST(@hour AS sql_char)));
        @minute_st = IIF(@minute < 10, '0' + trim(CAST(@minute AS sql_char)), trim(CAST(@minute AS sql_char))); 
        @second_st = IIF(@second < 10, '0' + trim(CAST(@second AS sql_char)), trim(CAST(@second AS sql_char)));
        @time_st = @hour_st + ':' + @minute_st + ':' + @second_st;
        @time = CAST(@time_st AS sql_time);
        INSERT INTO TimeOfDay (theTime) values(@time);
        @second = @second + 1;
      END WHILE; -- @second
      @minute = @minute + 1;
      @second = 0;
    END WHILE; -- @minute
    @hour = @hour + 1;
    @minute = 0;
  END WHILE; -- @hour
CATCH ALL
  RAISE;
END TRY;


