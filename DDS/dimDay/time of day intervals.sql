
SELECT timeofdaykey, thetime, 
  case 
    when minute(thetime) BETWEEN 0 AND 14 THEN 1
	when minute(thetime) BETWEEN 15 AND 29 THEN 2
	when minute(thetime) BETWEEN 30 AND 44 THEN 3
	when minute(thetime) BETWEEN 45 AND 59 THEN 4
  END AS int15
FROM timeofday

SELECT int15, hour(theTime), MIN(theTime), MAX(theTime)
FROM (
  SELECT timeofdaykey, thetime, 
    case 
      when minute(thetime) BETWEEN 0 AND 14 THEN 1
  	when minute(thetime) BETWEEN 15 AND 29 THEN 2
  	when minute(thetime) BETWEEN 30 AND 44 THEN 3
  	when minute(thetime) BETWEEN 45 AND 59 THEN 4
    END AS int15
  FROM timeofday) x
GROUP BY int15, hour(theTime) 