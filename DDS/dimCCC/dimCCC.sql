/*
this shit IS ALL about me stressing out about one dimension line per fact line
turns out it IS about 1 for every 3, which, mollifies me

SELECT *
FROM zrodetail

SELECT COUNT(*) -- 154567
FROM zrodetail

SELECT COUNT(*) -- 113954 ???
--SELECT *
FROM zrodetail
WHERE complaint IS NULL 

SELECT COUNT(*) -- 105452 ???
--SELECT *
FROM zrodetail
WHERE correction IS NOT NULL 

SELECT COUNT(*) -- 94874
FROM zrodetail
WHERE complaint IS NULL
  AND correction IS NULL
  AND cause IS NULL 

  -- only one dup
SELECT left(cast(complaint AS sql_char), 200),left(cast(correction AS sql_char), 200),left(cast(cause AS sql_char), 200), COUNT(*)
FROM zrodetail
WHERE complaint IS NOT NULL
  AND correction IS NOT NULL
  AND cause IS NOT NULL  
GROUP BY left(cast(complaint AS sql_char), 200),left(cast(correction AS sql_char), 200),left(cast(cause AS sql_char), 200) HAVING COUNT(*) > 1   
  
SELECT * 
FROM zroheader

SELECT COUNT(*) --50991
FROM zroheader  

SELECT COUNT(*) --45376
FROM zroheader  
WHERE pleasenote IS NULL 
*/
2 dimensions: dimRoComments - ro level
              dimComplaintCauseCorrection - line level

/*

X     Please Note
A     Complaint
A.0   Line Status (PDPPDET)
Z     Correction
L     Cause
*/

DELETE FROM stgArkonaSDPRTXT WHERE sxco# NOT IN ('RY1','RY2');

-- up to 40 lines of comments per line
SELECT sxco#, sxro#, sxline, sxltyp, COUNT(*)
FROM stgArkonaSDPRTXT
WHERE sxco# IN ('RY1','RY2')
GROUP BY sxco#, sxro#, sxline, sxltyp
ORDER BY COUNT(*) desc

DELETE FROM stgArkonaSDPRTXT WHERE sxltyp = 'A' AND sxtext = '';  

CREATE TABLE dimCCC (
  CCCKey autoinc,
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  Complaint memo, 
  Cause memo,
  Correction memo) IN database;
-- ADD a unique key ON ro/line so the loading does NOT get fucked up  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCCC','dimCCC.adi','RoLine',
   'RO;Line','',2051,512,'' );  

-- DO correction first, it IS the largest
DROP TABLE stgCorrection;
CREATE TABLE stgCorrection (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  seq integer,
  Text memo) IN database;
INSERT INTO stgCorrection
SELECT sxco#, sxro#, sxline, sxseq#, sxtext
FROM stgArkonaSDPRTXT
WHERE sxltyp = 'Z'; 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgCorrection','stgCorrection.adi','StoreCode','StoreCode',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgCorrection','stgCorrection.adi','RO','RO',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
  'stgCorrection','stgCorrection.adi','Line','Line',
   '',2,512,'' ); 
-- DO single liners first
SELECT theCount, COUNT(*)
FROM (   
  SELECT ro, line, COUNT(*) AS theCount
  FROM stgCorrection 
  GROUP BY ro, line) x
GROUP BY theCount;    

INSERT INTO dimCCC (StoreCode, RO, Line, Correction)
SELECT a.StoreCode, a.RO, a.Line, a.Text
FROM stgCorrection a
INNER JOIN (
  SELECT storecode, ro, line
  FROM stgCorrection 
  GROUP BY storecode, ro, line
  HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line;
  
DECLARE @cur3All cursor AS 
  SELECT a.storecode, a.ro, a.line
  FROM stgCorrection a
  INNER JOIN (
    SELECT storecode, ro, line
    FROM stgCorrection 
    GROUP BY storecode, ro, line
    HAVING COUNT(*) > 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  GROUP BY a.storecode, a.ro, a.line;   
DECLARE @cur3RO cursor AS
  SELECT a.storecode, a.ro, a.line, a.text
  FROM stgCorrection a
  WHERE a.storecode = @storecode
    AND a.ro = @ro
    AND a.line = @line
  ORDER BY seq;
DECLARE @string string;  
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
  @string = '';  
  OPEN @cur3All;
  TRY
    WHILE FETCH @cur3All DO
      @storecode = @cur3ALL.storecode;
      @ro = @cur3All.ro;
      @line = @cur3All.line;
      OPEN @cur3RO;
      TRY
        WHILE FETCH @cur3RO DO
          @string = TRIM(@string) + ' ' + TRIM(@cur3RO.text);
        END WHILE;
      FINALLY
        CLOSE @cur3RO;
      END TRY;
      TRY 
        INSERT INTO dimCCC (StoreCode, RO, Line, Correction)
        values (@storecode, @ro, @line, @string);
        @string = '';
      CATCH ALL 
        RAISE OMFG(100, @storecode + ' ' + @ro + ' ' + __errtext);
      END TRY;
    END WHILE;
  FINALLY
    CLOSE @cur3All;
  END TRY;    

   
DROP TABLE stgComplaint;
CREATE TABLE stgComplaint (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  seq integer,
  Text memo) IN database;
INSERT INTO stgComplaint
SELECT sxco#, sxro#, sxline, sxseq#, sxtext
FROM stgArkonaSDPRTXT
WHERE sxltyp = 'A'; 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgComplaint','stgComplaint.adi','StoreCode','StoreCode',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgComplaint','stgComplaint.adi','RO','RO',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
  'stgComplaint','stgComplaint.adi','Line','Line',
   '',2,512,'' );   
   
-- DO single liners first
SELECT theCount, COUNT(*)
FROM (   
  SELECT ro, line, COUNT(*) AS theCount
  FROM stgComplaint 
  GROUP BY ro, line) x
GROUP BY theCount;    

-- this has to be 2 stages
-- 1: ADD a row WHERE one does NOT EXISTS
-- 2. UPDATE existing rows with complaint value
-- shit this IS just the one liners
INSERT INTO dimCCC (StoreCode, RO, Line, Complaint)
SELECT a.StoreCode, a.RO, a.Line, a.Text
FROM stgComplaint a
INNER JOIN (
  SELECT storecode, ro, line
  FROM stgComplaint 
  GROUP BY storecode, ro, line
  HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
WHERE NOT EXISTS (
  SELECT 1
  FROM dimCCC
  WHERE ro = a.ro
    AND line = a.line)

UPDATE dimCCC
SET complaint = x.text
FROM ( 
  SELECT a.StoreCode, a.RO, a.Line, a.Text
  FROM stgComplaint a
  INNER JOIN (
    SELECT storecode, ro, line
    FROM stgComplaint 
    GROUP BY storecode, ro, line
    HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  WHERE EXISTS (
    SELECT 1
    FROM dimCCC
    WHERE ro = a.ro
      AND line = a.line)) x 
WHERE dimCCC.ro = x.ro
  AND dimCCC.line = x.line       
  
-- turn the multiliner INTO one liners with the CURSOR
-- THEN DO the two stage insert/UPDATE USING stgComplaintA
-- DROP TABLE stgComplaintA;
CREATE TABLE stgComplaintA (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  Text memo) IN database;
DECLARE @cur3All cursor AS 
  SELECT a.storecode, a.ro, a.line
  FROM stgComplaint a
  INNER JOIN (
    SELECT storecode, ro, line
    FROM stgComplaint 
    GROUP BY storecode, ro, line
    HAVING COUNT(*) > 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  GROUP BY a.storecode, a.ro, a.line;   
DECLARE @cur3RO cursor AS
  SELECT a.storecode, a.ro, a.line, a.text
  FROM stgComplaint a
  WHERE a.storecode = @storecode
    AND a.ro = @ro
    AND a.line = @line
  ORDER BY seq;
DECLARE @string string;  
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
  @string = '';  
  OPEN @cur3All;
  TRY
    WHILE FETCH @cur3All DO
      @storecode = @cur3ALL.storecode;
      @ro = @cur3All.ro;
      @line = @cur3All.line;
      OPEN @cur3RO;
      TRY
        WHILE FETCH @cur3RO DO
          @string = TRIM(@string) + ' ' + TRIM(@cur3RO.text);
        END WHILE;
      FINALLY
        CLOSE @cur3RO;
      END TRY;
      TRY 
        INSERT INTO stgComplaintA (StoreCode, RO, Line, Text)
        values (@storecode, @ro, @line, @string);
        @string = '';
      CATCH ALL 
        RAISE OMFG(100, @storecode + ' ' + @ro + ' ' + __errtext);
      END TRY;
    END WHILE;
  FINALLY
    CLOSE @cur3All;
  END TRY;     
  
-- this has to be 2 stages
-- 1: ADD a row WHERE one does NOT EXISTS
-- 2. UPDATE existing rows with complaint value
INSERT INTO dimCCC (StoreCode, RO, Line, Complaint)
SELECT a.StoreCode, a.RO, a.Line, a.Text
FROM stgComplaintA a
INNER JOIN (
  SELECT storecode, ro, line
  FROM stgComplaintA 
  GROUP BY storecode, ro, line
  HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
WHERE NOT EXISTS (
  SELECT 1
  FROM dimCCC
  WHERE ro = a.ro
    AND line = a.line)

UPDATE dimCCC
SET complaint = x.text
FROM ( 
  SELECT a.StoreCode, a.RO, a.Line, a.Text
  FROM stgComplaintA a
  INNER JOIN (
    SELECT storecode, ro, line
    FROM stgComplaintA 
    GROUP BY storecode, ro, line
    HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  WHERE EXISTS (
    SELECT 1
    FROM dimCCC
    WHERE ro = a.ro
      AND line = a.line)) x 
WHERE dimCCC.ro = x.ro
  AND dimCCC.line = x.line   
  
  
DROP TABLE stgCause;   
CREATE TABLE stgCause (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  seq integer,
  Text memo) IN database;
INSERT INTO stgCause
SELECT sxco#, sxro#, sxline, sxseq#, sxtext
FROM stgArkonaSDPRTXT
WHERE sxltyp = 'L'; 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgCause','stgCause.adi','StoreCode','StoreCode',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'stgCause','stgCause.adi','RO','RO',
   '',2,512,'' ); 
EXECUTE PROCEDURE sp_CreateIndex90( 
  'stgCause','stgCause.adi','Line','Line',
   '',2,512,'' );  
   
-- DO single liners first
-- 2 stages: INSERT / UPDATE 
INSERT INTO dimCCC (StoreCode, RO, Line, Correction)
SELECT a.StoreCode, a.RO, a.Line, a.Text
FROM stgCause a
INNER JOIN (
  SELECT storecode, ro, line
  FROM stgCause 
  GROUP BY storecode, ro, line
  HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
WHERE NOT EXISTS (
  SELECT 1
  FROM dimCCC
  WHERE ro = a.ro
    AND line = a.line);  

UPDATE dimCCC
SET cause = x.text
FROM ( 
  SELECT a.StoreCode, a.RO, a.Line, a.Text
  FROM stgCause a
  INNER JOIN (
    SELECT storecode, ro, line
    FROM stgCause 
    GROUP BY storecode, ro, line
    HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  WHERE EXISTS (
    SELECT 1
    FROM dimCCC
    WHERE ro = a.ro
      AND line = a.line)) x 
WHERE dimCCC.ro = x.ro
  AND dimCCC.line = x.line; 
  
CREATE TABLE stgCauseA (
  StoreCode cichar(3),
  RO cichar(9),
  Line integer,
  Text memo) IN database;
  
DECLARE @cur3All cursor AS 
  SELECT a.storecode, a.ro, a.line
  FROM stgCause a
  INNER JOIN (
    SELECT storecode, ro, line
    FROM stgCause 
    GROUP BY storecode, ro, line
    HAVING COUNT(*) > 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  GROUP BY a.storecode, a.ro, a.line;   
  
DECLARE @cur3RO cursor AS
  SELECT a.storecode, a.ro, a.line, a.text
  FROM stgCause a
  WHERE a.storecode = @storecode
    AND a.ro = @ro
    AND a.line = @line
  ORDER BY seq;
DECLARE @string string;  
DECLARE @storecode string;
DECLARE @ro string;
DECLARE @line integer;
  @string = '';  
  OPEN @cur3All;
  TRY
    WHILE FETCH @cur3All DO
      @storecode = @cur3ALL.storecode;
      @ro = @cur3All.ro;
      @line = @cur3All.line;
      OPEN @cur3RO;
      TRY
        WHILE FETCH @cur3RO DO
          @string = TRIM(@string) + ' ' + TRIM(@cur3RO.text);
        END WHILE;
      FINALLY
        CLOSE @cur3RO;
      END TRY;
      TRY 
        INSERT INTO stgCauseA (StoreCode, RO, Line, Text)
        values (@storecode, @ro, @line, @string);
        @string = '';
      CATCH ALL 
        RAISE OMFG(100, @storecode + ' ' + @ro + ' ' + __errtext);
      END TRY;
    END WHILE;
  FINALLY
    CLOSE @cur3All;
  END TRY;              
   
   
-- 2 stages: INSERT / UPDATE 
INSERT INTO dimCCC (StoreCode, RO, Line, Correction)
SELECT a.StoreCode, a.RO, a.Line, a.Text
FROM stgCauseA a
INNER JOIN (
  SELECT storecode, ro, line
  FROM stgCauseA 
  GROUP BY storecode, ro, line
  HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
WHERE NOT EXISTS (
  SELECT 1
  FROM dimCCC
  WHERE ro = a.ro
    AND line = a.line);  

UPDATE dimCCC
SET cause = x.text
FROM ( 
  SELECT a.StoreCode, a.RO, a.Line, a.Text
  FROM stgCauseA a
  INNER JOIN (
    SELECT storecode, ro, line
    FROM stgCauseA 
    GROUP BY storecode, ro, line
    HAVING COUNT(*) = 1) b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  WHERE EXISTS (
    SELECT 1
    FROM dimCCC
    WHERE ro = a.ro
      AND line = a.line)) x 
WHERE dimCCC.ro = x.ro
  AND dimCCC.line = x.line; 
  
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCCC','dimCCC.adi','PK',
   'CCCKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimCCC','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'dimRoStatusObjectsfail');   
-- index: NK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCCC','dimCCC.adi','NK',
   'RO;Line','',2051,512,'' );  -- unique  
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCCC','RO', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCCC','StoreCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCCC','Line', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
 
--< N/A row --------------------------------------------------------------------
-- 1 for each store
INSERT INTO dimCCC (storecode,ro,line,complaint,cause,correction)
values ('RY1','N/A',0,'N/A','N/A','N/A');
INSERT INTO dimCCC (storecode,ro,line,complaint,cause,correction)
values ('RY2','N/A',-1,'N/A','N/A','N/A');
--/> N/A row --------------------------------------------------------------------      

UPDATE dimCCC
SET complaint = 'N/A' 
WHERE complaint IS NULL; 
UPDATE dimCCC
SET cause = 'N/A' 
WHERE cause IS NULL; 
UPDATE dimCCC
SET correction = 'N/A' 
WHERE correction IS NULL; 

--- keyMap --------------------------------------------------------------------

--DROP TABLE keyMapDimRoComment;     
CREATE TABLE keyMapDimCCC (
  CCCKey integer,
  StoreCode cichar(3),
  RO cichar(9),
  Line integer) IN database;         
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keyMapDimCCC','keyMapDimCCC.adi','PK',
   'CCCKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('keyMapDimCCC','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'keyMapDimRoCommentStatusfail');   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCCC','CCCKey', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCCC','RO', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCCC','StoreCode', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCCC','Line', 
  'Field_Can_Be_Null', 'False', 'RETURN_ERROR', NULL );   
--RI  
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimRoStatus-KeyMap');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimCCC-KeyMap','dimCCC','keyMapdimCCC','PK',2,2,NULL,'','');   
      
INSERT INTO keyMapDimCCC
SELECT CCCKey,StoreCode,RO,Line FROM dimCCC;  u

INSERT INTO keyMapDimCCC
SELECT CCCKey,StoreCode,RO,Line FROM dimCCC WHERE ro = 'N/A';  


--/ keyMap --------------------------------------------------------------------


--- Dependencies & zProc ------------------------------------------------------
INSERT INTO zProcExecutables values('dimCCC','Daily',CAST('12:30:15' AS sql_time),0);
INSERT INTO zProcProcedures values('dimCCC','dimCCC',0,'daily');
INSERT INTO DependencyObjects values('dimCCC','dimCCC','dimCCC');

INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimCCC'
  AND b.dObject = 'tmpSDPRTXT';
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimCCC'
  AND b.dObject = 'tmpPDPPHDR';  
INSERT INTO Dependencies
  SELECT a.dExecutable, a.dProcedure,a.dObject, b.dExecutable, b.dProcedure,b.dObject, 'High' 
  FROM DependencyObjects a
  full OUTER JOIN DependencyObjects b ON 1 = 1
  WHERE a.dObject = 'dimCCC'
  AND b.dObject = 'tmpPDPPDET';    
    
--/ Dependencies & zProc -----------------------------------------------------    

couple of thins
1. IN the back load i did NOT DO pdp
2. NOT sure i need the interim stg TABLE created IN the script
3. NULL values for any COLUMN should be coalesced to 'N/A'
   
select *
FROM dimccc
WHERE cause IS NULL OR complaint IS NULL OR correction IS NULL

SELECT COUNT(*) FROM (
SELECT a.sxco#, a.sxro#, a.sxLine, a.sxText
FROM tmpSDPRTXT a
INNER JOIN (
  SELECT sxco#, sxro#, sxline
  FROM tmpSDPRTXT 
  WHERE sxltyp = 'Z'
  GROUP BY sxco#, sxro#, sxline
  HAVING COUNT(*) = 1) b ON a.sxco# = b.sxco# AND a.sxro# = b.sxro# AND a.sxline = b.sxline
WHERE sxltyp = 'Z'  
) x

SELECT COUNT(*) FROM tmpSDPRTXT 
  WHERE sxltyp = 'Z'
  
  
  
--  INSERT INTO tmpSDPRTXTa
SELECT COUNT(*) FROM (
  SELECT sxco#, sxro#, sxline, sxltyp, sxseq#, sxtext
  FROM tmpsdprtxt a
  WHERE sxltyp = 'z') z
  UNION 
  SELECT a.ptco#, a.ptdoc#, b.ptline, b.ptltyp, b.ptseq#, b.ptcmnt
  FROM tmppdpphdr a
  INNER JOIN tmpPDPPDET b ON a.ptpkey = b.ptpkey
  WHERE a.ptdtyp = 'RO'
    AND a.ptdoc# <> ''
    AND b.ptltyp = 'z'
    AND b.ptcmnt <> '';    