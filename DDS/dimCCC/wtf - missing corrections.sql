-- wtf, for some reason, ro lines with correction are being generated AS N/A
-- loaded tmpSDPRTXT with 90 days of data

SELECT *
FROM tmpSDPRTXT a
WHERE sxltyp = 'A'
AND NOT EXISTS (
  SELECT 1
  FROM tmpsdprtxt
  WHERE sxro# = a.sxro#
    AND sxltyp = 'Z')
    
SELECT *
FROM tmpSDPRTXT a
WHERE sxro# = '16135480'    

SELECT DISTINCT ro, line
INTO #wtf
FROM (
SELECT *
FROM dimccc
WHERE correction = 'N/A'
  AND LEFT(ro, 2) = '16') a
LEFT JOIN tmpsdprtxt b on a.ro = b.sxro#
  AND a.line = b.sxline
WHERE b.sxltyp = 'Z'
  AND b.sxro# IS NOT NULL   
  
SELECT month(thedate), COUNT(*)
FROM factrepairorder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE ro IN (
  SELECT ro
  FROM #wtf)
GROUP BY month(thedate)


SELECT COUNT(*) FROM tmpsdprtxt

-- just look at january ros see IF there are discrpancies
-- i can fix the current state, but i DO NOT know what IS causing the problem
  
  
SELECT *
FROM tmpSDPRTXT a
WHERE sxro# IN (
  SELECT ro
  FROM factrepairorder b
  INNER JOIN day c on b.opendatekey = c.datekey
  WHERE c.yearmonth = 201401)
AND sxltyp = 'Z'  
AND EXISTS (
  SELECT 1
  FROM factrepairorder x
  INNER JOIN dimccc y on x.ccckey = y.ccckey
  WHERE x.ro = a.sxro#
    AND x.line = a.sxline
    AND y.correction = 'N/A')
  
  
SELECT a.ro, a.line, b.*
FROM factrepairorder a
INNER JOIN dimccc b on a.ccckey = b.ccckey
WHERE a.ro = '16140984' 

SELECT * FROM stgcorrection WHERE ro = '16140984' 

SELECT * FROM tmpDimCCC WHERE ro = '16140984' 


SELECT *      
  FROM tmpDimCCC a
  INNER JOIN dimCCC b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  WHERE (
    trim(cast(a.correction AS sql_char)) <> trim(cast(b.correction AS sql_char))
    OR trim(cast(a.complaint AS sql_char)) <> trim(cast(b.complaint AS sql_char))
    OR trim(cast(a.cause AS sql_char)) <> trim(cast(b.cause AS sql_char)))
and a.ro = '16139079'    
 
SELECT *
FROM tmpsdprtxt
WHERE sxro# = '16139079'
  AND sxline = 1
  
SELECT b.*
FROM tmppdpphdr a
INNER JOIN tmppdppdet b on a.ptpkey = b.ptpkey
WHERE a.ptdoc#= '16139079'  

-- here IS the fucking problem
-- updating dimCCC to the old value (b->dimCCC) instead of the new value: a->tmpDimCCC
  UPDATE dimCCC
  SET Complaint = b.Complaint,
      Correction = b.Correction,
      Cause = b.Cause
-- SELECT *      
  FROM tmpDimCCC a
  INNER JOIN dimCCC b ON a.storecode = b.storecode AND a.ro = b.ro AND a.line = b.line
  WHERE (
    trim(cast(a.correction AS sql_char)) <> trim(cast(b.correction AS sql_char))
    OR trim(cast(a.complaint AS sql_char)) <> trim(cast(b.complaint AS sql_char))
    OR trim(cast(a.cause AS sql_char)) <> trim(cast(b.cause AS sql_char)));
    
ok, i have 90 days worth of data IN tmpsdprtxt, go ahead AND run sp.stgDimCCC
against that, THEN will have to go back AND extract data for the entire year 
thinking, a month at a time

SELECT yearmonth, COUNT(*)
FROM (
SELECT DISTINCT ro, line, ccckey, thedate
FROM factRepairOrder a
INNER JOIN day b on a.opendatekey = b.datekey
WHERE ccckey in (
  SELECT ccckey
  FROM dimccc
  WHERE correction = 'N/A')) a
INNER JOIN day b on a.thedate = b.thedate 
GROUP BY yearmonth 
  
-- the other thing that come to light IS that i am NOT storing the nightly scrapes
-- IN stgArkonaSDPRTXT starting IN august, which IS WHEN i refactored to extSDPRTXT
SELECT month(thedate), COUNT(*)
FROM stgArkonaSDPRTXT a
INNER JOIN factrepairorder b on a.sxro# = b.ro
INNER JOIN day c on b.opendatekey = c.datekey
WHERE c.theyear = 2013
GROUP BY month(thedate)  