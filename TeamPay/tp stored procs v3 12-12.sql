refactor to include the display of multiple time periods
defaulting to current AND 2 previous time periods
for every screen ON which this IS an option, the code will send
a payperiod parameter which will drive the data being returned
0 = current pay period
-1 = previous pay period
etc

also going to rename stored procs to include the appcode
eg GetManagerSummary becomes GetTpManagerSummary

--< Manager Splash ------------------------------------------------------------< Manager Splash
/* Summary
replace GetManagerSummary with GetTpPayPeriods
PROCEDURE GetTpPayPeriods( 
      payPeriodDisplay cichar(65) output,
	  payPeriodIndicator integer OUTPUT)
replace GetShopTotals with GetTpShopTotals	  
PROCEDURE GetTpShopTotals( 
      sessionID CICHAR ( 50 ),
	  payPeriodIndicator integer,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay Money OUTPUT) 	  
replace GetTeamTotals with GetTpTeamTotals
PROCEDURE GetTpTeamTotals( 
      sessionID CICHAR ( 50 ),
	  payPeriodIndicator integer,
      teamName CICHAR ( 52 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay Money OUTPUT) 
replace GetEmployeeTotals with GetTpEmployeeTotals	  
PROCEDURE GetTpEmployeeTotals(
  sessionID cichar(50),
  payPeriodIndicator integer,
  teamName cichar(52) output,
  tech cichar(60) output,
  flagHours double output,
  clockHours double output,
  teamProf double output,
  payHours double output,
  regularPay money output)
*/

DROP PROCEDURE getManagerSummary;
ALTER  PROCEDURE GetTpPayPeriods
   ( 
      payPeriodDisplay cichar(65) output,
	  payPeriodIndicator integer output
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpPayPeriods();
*/
-- DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @fromPayPeriodSeq integer;
DECLARE @thruPayPeriodSeq integer;
@thrupayperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = curdate());
@fromPayPeriodSeq = @thrupayperiodseq - 2;
INSERT INTO __output
SELECT payperiodselectformat, payperiodseq - @thruPayPeriodSeq
FROM (
  SELECT DISTINCT payperiodselectformat, payperiodseq
  FROM tpdata 
  WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq) x;

END; 

DROP PROCEDURE getshoptotals;
ALTER PROCEDURE GetTpShopTotals( 
      sessionID CICHAR ( 50 ),
	  payPeriodIndicator integer,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay Money OUTPUT) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpShopTotals('[40d41022-e71e-2a4d-afd5-12c3ee607032]', 0);
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN   
  INSERT INTO __output
  SELECT sum(FlagHours), SUM(clockhours), round(SUM(FlagHours)/SUM(ClockHours), 4),
    sum(PayHours), SUM(regularPay)
  FROM (
    SELECT a.*, b.RegularPay 
    FROM (
      SELECT distinct teamname, 
        (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
        teamclockhourspptd AS ClockHours, teamprofpptd,
        round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
      FROM tpData
      WHERE thedate = @date) a
    LEFT JOIN (
    SELECT teamname, SUM(RegularPay) AS RegularPay
      FROM (
      SELECT distinct teamname, teamkey, technumber, techkey,
        round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
      FROM tpData
      WHERE thedate = @date) r
    GROUP BY teamname) b on a.teamname = b.teamname) s;
  ENDIF;  
END;

DROP PROCEDURE getteamtotals;
alter PROCEDURE GetTpTeamTotals( 
      sessionID CICHAR ( 50 ),
	  payPeriodIndicator integer,
      teamName CICHAR ( 52 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay Money OUTPUT) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpTeamTotals('[40d41022-e71e-2a4d-afd5-12c3ee607032]', 0);
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN 
  INSERT INTO __output
  SELECT a.*, b.RegularPay
  FROM (
    SELECT distinct teamname, 
      (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
      teamclockhourspptd, teamprofpptd/100,
      round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
    FROM tpData
    WHERE thedate = @date) a
  LEFT JOIN (
  SELECT teamname, SUM(RegularPay) AS RegularPay
    FROM (
    SELECT distinct teamname, teamkey, technumber, techkey,
      round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
    FROM tpData
    WHERE thedate = @date) r
  GROUP BY teamname) b on a.teamname = b.teamname;
ENDIF;  
END;


DROP PROCEDURE getemployeetotals;
alter PROCEDURE GetTpEmployeeTotals(
  sessionID cichar(50),
  payPeriodIndicator integer,
  teamName cichar(52) output,
  tech cichar(60) output,
  flagHours double output,
  clockHours double output,
  teamProf double output,
  payHours double output,
  regularPay money output)
BEGIN
/*
EXECUTE PROCEDURE GetTpEmployeeTotals('[92fe4938-efd2-bf43-bdcd-36eadbef9399]', -1);
12/7
  IF user IS a manager return ALL teams
  IF user IS a teamleader, return just the team leaders team
*/

DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @userName string;
DECLARE @role string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
@role = 'teamlead';
@userName = (SELECT username FROM sessions WHERE sessionid = @sessionID);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN   
   INSERT INTO __output
  SELECT top 1000 *
  FROM (
    SELECT teamname, trim(firstname) + ' '  + lastname AS tech, 
      (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd + TechFlagHourAdjustmentspptd) AS "Flag Hours",
      techclockhourspptd AS "clock hours", 
      teamprofpptd/100 AS "team prof", round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
      round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS "Regular Pay"
    FROM tpdata a
    WHERE thedate = @date
      AND 
        CASE 
          WHEN EXISTS (
            SELECT 1
            FROM EmployeeAppRoles
            WHERE username = @userName
              AND role = @role) THEN teamname = (
                SELECT lastname 
                FROM tpEmployees
                WHERE username = @userName)
          ELSE 1 = 1
        END) x

  ORDER BY teamname, tech;
ENDIF;
END;

--/> Manager Splash -----------------------------------------------------------</ Manager Splash

--< Tech Splash ---------------------------------------------------------------< Tech Splash
/* summary
techsummary: getTpTechSummary -> getTpTechSummaryNew
DROP PROCEDURE gettptechsummarynew;
PROCEDURE GetTpTechSummaryNew( 
      SessionID CICHAR ( 50 ),
	  payPeriodIndicator integer,
      techName CICHAR ( 60 ) OUTPUT,
      teamName CICHAR ( 25 ) OUTPUT,
      regularRate Money OUTPUT,
      otherRate Money OUTPUT,
      teamProficiency DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      otherHours DOUBLE ( 15 ) OUTPUT,
      otherPay Money OUTPUT,
      regularPay Money OUTPUT,
      teamFlagHours DOUBLE ( 15 ) OUTPUT,
      teamClockHours DOUBLE ( 15 ) OUTPUT,
      totalGrossPay Money OUTPUT) 
techdetails: getTpTechDetail -> getTpTechDetailNew
PROCEDURE GetTpTechDetailNew (
  SessionID cichar(50),
  payPeriodIndicator integer,
  date date output,
  flagHours double output,
  clockHours double output,
  teamProf double output,
  payHours double output,
  regularPay money output)
techtotals: getTpTechTotal -> getTpTechTotalNew
PROCEDURE getTpTechTotalNew (
  SessionID cichar(50),
  payPeriodIndicator integer,
  flagHours double output,
  clockHours double output,
  teamProf double output,
  payHours double output,
  regularPay money output)

techotherdetials: getTechOtherHoursDetail -> getTpTechOtherHoursDetail
PROCEDURE getTpTechOtherHoursDetail (
  sessionID cichar(50),
  payPeriodIndicator integer,
  theDate date output,
  vacationHours double output,
  ptoHours double output,
  holidayHours double output)
techoterhtotals: getTechOtherHoursTotal -> GetTpTechOtherHoursTotal
PROCEDURE GetTpTechOtherHoursTotal (
  sessionID cichar(50),
  payPeriodIndicator integer,
  vacationHours double output,
  ptoHours double output,
  holidayHours double output)
*/
alter PROCEDURE GetTpTechSummary( 
      SessionID CICHAR ( 50 ),
	  payPeriodIndicator integer,
      techName CICHAR ( 60 ) OUTPUT,
      teamName CICHAR ( 25 ) OUTPUT,
      regularRate Money OUTPUT,
      otherRate Money OUTPUT,
      teamProficiency DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      otherHours DOUBLE ( 15 ) OUTPUT,
      otherPay Money OUTPUT,
      regularPay Money OUTPUT,
      teamFlagHours DOUBLE ( 15 ) OUTPUT,
      teamClockHours DOUBLE ( 15 ) OUTPUT,
      totalGrossPay Money OUTPUT) 
BEGIN 
/*
execute PROCEDURE GetTpTechSummaryNew('[b46ac096-ea81-db4a-8999-26c16b899a84]', 0);
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @UserName cichar(50);
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);

INSERT INTO __output
  SELECT distinct TRIM(c.firstname) + ' ' + TRIM(c.lastname) AS techName, 
    a.teamName, a.TechTFRRate AS regularRate, a.TechHourlyRate AS otherRate, 
    a.teamProfPPTD/100 AS teamProficiency, 
    a.techClockHoursPPTD AS clockHours,
    round(a.techClockHoursPPTD * a.teamProfPPTD/100, 2) AS payHours,
    a.techVacationHoursPPTD + a.techPTOHoursPPTD + a.techHolidayHoursPPTD AS otherHours,
    a.TechHourlyRate * (a.techVacationHoursPPTD + a.techPTOHoursPPTD
       + a.techHolidayHoursPPTD) AS otherPay,
    round(a.TechTFRRate * (a.techClockHoursPPTD * a.teamProfPPTD/100),2) AS regularPay,
    a.teamFlagHoursPPTD AS teamFlagHours, teamClockHoursPPTD AS teamClockHours,
    (a.TechHourlyRate * (a.techVacationHoursPPTD + a.techPTOHoursPPTD
       + a.techHolidayHoursPPTD))  
       + round(a.TechTFRRate * (a.techClockHoursPPTD * a.teamProfPPTD/100),2) AS totalGrossPay       
  FROM tpdata a
  INNER JOIN tpTechs b on a.techkey = b.techkey
  INNER JOIN tpEmployees c on b.employeenumber = c.employeenumber
  WHERE c.Username = @UserName
    AND thedate = @date;
END;


DROP PROCEDURE GetTpTechDetailNew;
alter PROCEDURE GetTpTechDetail (
  SessionID cichar(50),
  payPeriodIndicator integer,
  date date output,
  flagHours double output,
  clockHours double output,
  teamProf double output,
  payHours double output,
  regularPay money output)
BEGIN
/*
EXECUTE PROCEDURE GetTpTechDetailNew('[6d24944b-8eae-d24c-b646-a6d2b4afb217]', 0);
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
INSERT INTO __output
SELECT thedate, 
  (techflaghoursday + techOtherHoursDay + techTrainingHoursDay + 
    TechFlagHourAdjustmentsDay) AS "Flag Hours",
  techclockhoursday AS "clock hours", 
  b.teamprofpptd/100 AS "team prof", 
  round(techclockhoursday*b.teamprofpptd/100, 2) AS "Pay Hours",
  round(techtfrrate*techclockhoursday*b.teamprofpptd/100, 2) AS "Regular Pay"
FROM tpdata a
LEFT JOIN (
  SELECT teamprofpptd
  FROM tpdata  
  WHERE techkey = @TechKey
    AND @date BETWEEN payperiodstart AND payperiodend 
    AND thedate = (
      SELECT MAX(thedate)
      FROM tpdata
      WHERE techkey = @TechKey
        AND @date BETWEEN payperiodstart AND payperiodend 
        AND teamprofpptd <> 0)) b on 1 = 1 
WHERE techkey = @TechKey
  AND @date BETWEEN payperiodstart AND payperiodend;
END;  

DROP PROCEDURE getTpTechTotalNew;
alter PROCEDURE getTpTechTotal (
  SessionID cichar(50),
  payPeriodIndicator integer,
  flagHours double output,
  clockHours double output,
  teamProf double output,
  payHours double output,
  regularPay money output)
BEGIN
/*

INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 
  'bcahalan@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) 
FROM system.iota;

EXECUTE PROCEDURE getTpTechTotalNew('[6d24944b-8eae-d24c-b646-a6d2b4afb217]', -2);
*/

DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
	
INSERT INTO __output
SELECT
  (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd + 
    TechFlagHourAdjustmentspptd) AS "Flag Hours",
  techclockhourspptd AS "clock hours", 
  teamprofpptd/100 AS "team prof", 
  round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
  round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS "Regular Pay"
FROM tpdata a
WHERE techkey = @TechKey
  AND thedate = @date;
END;  



DROP PROCEDURE gettechotherhoursdetail;
alter PROCEDURE getTpTechOtherHoursDetail (
  sessionID cichar(50),
  payPeriodIndicator integer,
  theDate date output,
  vacationHours double output,
  ptoHours double output,
  holidayHours double output)
BEGIN
/*  
EXECUTE PROCEDURE getTpTechOtherHoursDetail('[bf95dfc4-3839-954c-9eee-306a2982d01d]', 0);
*/

DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);

INSERT INTO __output    
SELECT thedate, techvacationhoursday AS vacationHours, 
  techptohoursday AS ptoHours, 
  techholidayhoursday AS holidayHours
FROM tpdata
WHERE @date BETWEEN payperiodstart AND payperiodend
  AND techkey = @techkey
  AND techvacationhoursday + techptohoursday + techholidayhoursday <> 0;
END;  

DROP PROCEDURE gettechotherhourstotal;
alter PROCEDURE GetTpTechOtherHoursTotal (
  sessionID cichar(50),
  payPeriodIndicator integer,
  vacationHours double output,
  ptoHours double output,
  holidayHours double output)
BEGIN
/*  
EXECUTE PROCEDURE GetTpTechOtherHoursTotal('[bf95dfc4-3839-954c-9eee-306a2982d01d]', -2);
*/ 
-- tech vac pto hol, daily detail PPTD Total   
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
INSERT INTO __output
SELECT sum(techvacationhoursday) AS vacationHours, 
  sum(techptohoursday) AS ptoHours, 
  sum(techholidayhoursday) AS holdayHours
FROM tpdata
WHERE payperiodseq = @payperiodseq
  AND techkey = @techkey
  AND techvacationhoursday + techptohoursday + techholidayhoursday <> 0;  
END;  
--/> Tech Splash --------------------------------------------------------------/> Tech Splash
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
--< How Pay Works --------------------------------------------------------------< How Pay Works 
-- getTechPayCalc
DROP PROCEDURE getTechPayCalc;
alter PROCEDURE GetTpTechPayCalc(
  sessionID cichar(50),
  payPeriodIndicator integer,
  teamFlagHours double OUTPUT,
  teamClockHours double OUTPUT,
  clockHours double OUTPUT,
  payRate money OUTPUT)
BEGIN
/*
EXECUTE PROCEDURE getTpTechPayCalc('[0baa2b11-7ba6-8c4b-9291-bf98f8772650]', -1)
*/  

DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@sessionID = (SELECT sessionID FROM __input);
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
INSERT INTO __output   
SELECT teamFlagHoursPPTD, teamClockhoursPPTD, techClockHoursPPTD, techTFRRate
FROM tpdata a
WHERE a.thedate = @date
  AND a.techkey = @techkey;
END;      
  
--/> How Pay Works -------------------------------------------------------------/> How Pay Works 
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
--< Tech RO List --------------------------------------------------------------< Tech RO List
/* summary
replace getTechRosByDay with GetTpTechRosByDay
PROCEDURE GetTpTechROsByDay( 
      SessionID CICHAR ( 50 ),
      userName cichar(50),
	  payPeriodIndicator integer,
      techName cichar(60) output,
      flagDate DATE OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      line integer output,
      flagHours DOUBLE ( 15 ) OUTPUT
	  
replace GetTechRoTotals with GetTpTechRoTotals
CREATE PROCEDURE GetTpTechRoTotals( 
      SessionID CICHAR ( 50 ),
      userName cichar(50),
	  payPeriodIndicator integer,
      techName cichar(60) output,
      flagHours DOUBLE ( 15 ) OUTPUT) 	
	  
replace GetTechRosAdjustmentsByDay with GetTpTechRosAdjustmentsByDay
PROCEDURE GetTpTechRosAdjustmentsByDay (
  SessionID cichar(50),
  userName cichar(50),
  payPeriodIndicator integer,
  techName cichar(60) output,
  adjustmentDate date output,
  ro cichar(9) output,
  line integer output, 
  type cichar(24) output,
  flagHours double output)	
  
replace GetTechAdjustmentTotals with GetTpTechAdjustmentTotals
PROCEDURE GetTpTechAdjustmentTotals( 
      SessionID CICHAR ( 50 ),
      userName cichar(50),
	  payPeriodIndicator integer,
      techName cichar(60) output,
      flagHours DOUBLE ( 15 ) OUTPUT      
*/
DROP PROCEDURE GetTechROsByDay;
create PROCEDURE GetTpTechROsByDay( 
      SessionID CICHAR ( 50 ),
      userName cichar(50),
	  payPeriodIndicator integer,
      techName cichar(60) output,
      flagDate DATE OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      line integer output,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpTechROsByDay('[2a84f8b2-c14d-c547-8875-14ee1cfa544e]', '', -2);
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
DECLARE @sessionID string;
@sessionID = (SELECT sessionID FROM __input);
@UserName = 
  CASE 
    WHEN (SELECT userName FROM __input) <> '' THEN
      (SELECT userName FROM __input)
    ELSE 
      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
  END;
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);  
INSERT INTO __output
SELECT top 1000 *
FROM (
  SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    d.thedate AS flagDate, ro, line, round(sum(a.flaghours * b.weightfactor), 2)
  FROM dds.factRepairOrder a  
  INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dds.dimTech c on b.techkey = c.techkey
  INNER JOIN tpTechs cc on c.technumber = cc.technumber
  INNER JOIN dds.day d on a.flagdatekey = d.datekey
  WHERE flaghours <> 0
    AND a.storecode = 'ry1'
    AND a.flagdatekey IN (
      SELECT datekey
      FROM dds.day
      WHERE thedate BETWEEN @fromDate AND @thruDate) 
    AND a.servicetypekey IN (
      SELECT servicetypekey
      FROM dds.dimservicetype
      WHERE servicetypecode IN ('AM','EM','MR'))   
    AND c.technumber = @techNumber
  GROUP BY TRIM(cc.firstname) + ' ' + TRIM(cc.lastname), d.thedate, a.ro, a.line) x
ORDER BY techName, flagDate, ro, line  ; 
END;



DROP PROCEDURE GetTechRoTotals;
CREATE PROCEDURE GetTpTechRoTotals( 
      SessionID CICHAR ( 50 ),
      userName cichar(50),
	  payPeriodIndicator integer,
      techName cichar(60) output,
      flagHours DOUBLE ( 15 ) OUTPUT) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpTechRoTotals('[2c04bb3c-cd80-0746-97c6-28f34245c677]', '', -2);
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
DECLARE @sessionID string;
@sessionID = (SELECT sessionID FROM __input);
@UserName = 
  CASE 
    WHEN (SELECT userName FROM __input) <> '' THEN
      (SELECT userName FROM __input)
    ELSE 
      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
  END;
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);  
INSERT INTO __output
  SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    round(sum(a.flaghours * b.weightfactor), 2)
  FROM dds.factRepairOrder a  
  INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dds.dimTech c on b.techkey = c.techkey
  INNER JOIN tpTechs cc on c.technumber = cc.technumber
  INNER JOIN dds.day d on a.flagdatekey = d.datekey
  WHERE flaghours <> 0
    AND a.storecode = 'ry1'
    AND a.flagdatekey IN (
      SELECT datekey
      FROM dds.day
      WHERE thedate BETWEEN @fromDate AND @thruDate) 
    AND a.servicetypekey IN (
      SELECT servicetypekey
      FROM dds.dimservicetype
      WHERE servicetypecode IN ('AM','EM','MR'))   
    AND c.technumber = @techNumber
  GROUP BY TRIM(cc.firstname) + ' ' + TRIM(cc.lastname);
END;


DROP PROCEDURE GetTechRosAdjustmentsByDay;
alter PROCEDURE GetTpTechRosAdjustmentsByDay (
  SessionID cichar(50),
  userName cichar(50),
  payPeriodIndicator integer,
  techName cichar(60) output,
  adjustmentDate date output,
  ro cichar(9) output,
  line integer output, 
  type cichar(24) output,
  flagHours double output)
BEGIN
/*
EXECUTE PROCEDURE GetTpTechRosAdjustmentsByDay('[0a734ed6-4a67-224c-97b8-e23d5643745e]', '', 0);
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @techName string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
DECLARE @sessionID string;
@sessionID = (SELECT sessionID FROM __input);
@UserName = 
  CASE 
    WHEN (SELECT userName FROM __input) <> '' THEN
      (SELECT userName FROM __input)
    ELSE 
      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
  END;
@techName = (
  SELECT TRIM(firstName) + ' ' + lastName
  FROM tpEmployees
  WHERE username = @userName);   
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);   
INSERT INTO __output
  SELECT @techName, thedate, ro, line,
  CASE Source
    WHEN 'XTIM' THEN 'Adjust after Close'
    WHEN 'NEG' THEN 'Negative Hours'
    WHEN 'ADJ' THEN 'Policy Adjust'
  END AS Type, 
  CASE Source
    WHEN 'XTIM' THEN  -1*Hours 
    else Hours
  END as flaghours   
  FROM dds.stgFlagTimeAdjustments 
  WHERE storecode = 'RY1'
    AND technumber = @TechNumber
    AND thedate BETWEEN @fromDate AND @thruDate;
END;  

DROP PROCEDURE GetTechAdjustmentTotals;
ALTER PROCEDURE GetTpTechAdjustmentTotals( 
      SessionID CICHAR ( 50 ),
      userName cichar(50),
	  payPeriodIndicator integer,
      techName cichar(60) output,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpTechAdjustmentTotals('[0a734ed6-4a67-224c-97b8-e23d5643745e]', '', -1);
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @techName string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
DECLARE @sessionID string;
@sessionID = (SELECT sessionID FROM __input);
@UserName = 
  CASE 
    WHEN (SELECT userName FROM __input) <> '' THEN
      (SELECT userName FROM __input)
    ELSE 
      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
  END;
@techName = (
  SELECT TRIM(firstName) + ' ' + lastName
  FROM tpEmployees
  WHERE username = @userName);   
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);     
INSERT INTO __output
  SELECT TRIM(firstName) + ' ' + lastName, 
  SUM( 
    CASE Source
      WHEN 'XTIM' THEN  -1*Hours 
      else Hours
    END) as flaghours   
  FROM dds.stgFlagTimeAdjustments a
  INNER JOIN tpTechs b on a.technumber = b.technumber
  WHERE storecode = 'RY1'
    AND a.technumber = @TechNumber
    AND thedate BETWEEN @fromDate AND @thruDate
  GROUP BY TRIM(firstName) + ' ' + lastName;
END;  

--/> Tech RO List -------------------------------------------------------------/> Tech RO List
