1. date interval: 12/2 - 12/6 ------------------------------------------------------------
   16132348 dds .4 less than Arkokna
   line 1 566 .4  12/4
   line 1 566 .6  12/9
   Arkona includes the .4, dds does NOT, flag hours IN dds are BY tech/line, 
   NOT tech/line/flagdate
   i believe that this IS a bit of a rare situation, the same tech on the same line
   on multiple days

-- fuck me, this IS NOT ALL that rare, 157 of 84058 ros exhibit this phenomenon   
-- 16132348 does NOT show up IN this query because it IS still open   
DROP TABLE #wtf;
SELECT ptro#, ptline, ptdate, pttech, sum(ptlhrs) AS ptlhrs
INTO #wtf
FROM dds.stgArkonaSDPRDET   
WHERE ptcode = 'TT' AND ptltyp = 'L'
  AND year(ptdate) = 2013
  AND ptlhrs > 0
GROUP BY ptro#, ptline, ptdate, pttech

SELECT COUNT(DISTINCT ptro#) FROM #wtf -- 84058

-- SELECT COUNT(DISTINCT ptro#) FROM ( -- 157
SELECT a.*
FROM #wtf a
INNER JOIN (
  SELECT ptro#, ptline, pttech
  FROM #wtf
  GROUP BY ptro#, ptline, pttech HAVING COUNT(*) > 1) b on a.ptro# = b.ptro# 
    AND a.ptline = b.ptline AND a.pttech = b.pttech
-- ) x    
ORDER BY a.ptro#, a.ptline    

SELECT ptro#, ptline, pttech, MIN(ptdate) AS minDate, MAX(ptdate) AS maxDate, 
  MAX(ptdate) - MIN(ptdate) AS dateDiff
FROM (
  SELECT a.*
  FROM #wtf a
  INNER JOIN (
    SELECT ptro#, ptline, pttech
    FROM #wtf
    GROUP BY ptro#, ptline, pttech HAVING COUNT(*) > 1) b on a.ptro# = b.ptro# 
      AND a.ptline = b.ptline AND a.pttech = b.pttech) x
GROUP BY ptro#, ptline, pttech 
ORDER BY MAX(ptdate) - MIN(ptdate) DESC

2. --------------------------------------------------------------------------------
ro 16137772 combination of multiple dates, AND negative hours but they DO NOT show
up IN adjustments because ptdbas = V
fuck me sideways

SELECT *
FROM tpTechs a
INNER JOIN tpTeamTechs b on a.techkey = b.techkey
ORDER BY technumber

-- tech AND total flag hours for period
SELECT technumber, lastname, sum(techflaghoursday), sum(techflaghouradjustmentsday)
FROM tpdata
where technumber IN (
  SELECT technumber
  FROM tpTechs a
  INNER JOIN tpTeamTechs b on a.techkey = b.techkey)
  AND thedate BETWEEN '12/02/2013' AND '12/06/2013'
GROUP BY technumber,  lastname
ORDER BY technumber

-- tech AND ros for period
  SELECT a.technumber, ro, sum(coalesce(b.flaghours, 0)) AS flaghours
  FROM tpData a
  LEFT JOIN (
    SELECT d.thedate, ro, line, c.technumber, a.flaghours, b.weightfactor
    FROM dds.factRepairOrder a  
    INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dds.dimTech c on b.techkey = c.techkey
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'
      AND a.flagdatekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN '12/02/2013' AND '12/06/2013')   
 
      AND c.technumber IN (
        SELECT technumber
        FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
where a.technumber = '511'   
AND flaghours <> 0    
  GROUP BY a.technumber, ro 
  
    SELECT d.thedate, ro, line, c.technumber, a.flaghours, b.weightfactor
    FROM dds.factRepairOrder a  
    INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dds.dimTech c on b.techkey = c.techkey
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
    WHERE ro = '16137900'
      AND a.storecode = 'ry1'
      AND a.flagdatekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN '12/02/2013' AND '12/06/2013')   


SELECT * from dds.stgFlagTimeAdjustments
WHERE ro = '16137772'

SELECT *
FROM dds.stgArkonaSDPXTIM
WHERE ptro# = '16137772'

SELECT *
FROM dds.stgArkonaSDPRDET
WHERE ptro# = '16137772'


-- adj connection --------------------------------------------------------------------------------------------------------------------
-- tech/flaghours for period
SELECT technumber, lastname, sum(round(flaghours * weightfactor, 2)) AS flaghours, SUM(adjhours) AS adjhours
FROM (
    SELECT d.thedate, ro, line, c.technumber, f.lastname, a.flaghours, b.weightfactor, coalesce(e.adjhours, 0) AS adjhours
    FROM adj.factRepairOrder a  
    INNER JOIN adj.brTechGroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN adj.dimTech c on b.techkey = c.techkey
    INNER JOIN adj.day d on a.flagdatekey = d.datekey
    LEFT JOIN (
      SELECT ptro#, ptline, ptdate, pttech, SUM(ptlhrs) AS AdjHours
      FROM dds.stgArkonaSDPXTIM  
      GROUP BY ptro#, ptline, ptdate, pttech) e on a.ro = e.ptro# AND a.line = e.ptline
        AND d.thedate = e.ptdate AND c.technumber = e.pttech    
    INNER JOIN tpTechs f on c.technumber = f.technumber
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'
      AND a.flagdatekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN '12/01/2013' AND '12/14/2013')   
       AND c.technumber IN (
        SELECT technumber
        FROM tpTechs)
) x GROUP BY technumber, lastname     
ORDER BY technumber   
        


select 'XTIM' as source, z.*
from (
  select cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date) as theDate,
    ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdpxtim
  where ptdate > 20121231
  group by cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
    ptco#, ptro#, ptline, pttech) z
where hours <> 0
union 
select  'ADJ' as source, a.*
from (
  select cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
    ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, sum(ptlhrs) as hours
  from rydedata.sdprdet
  --where ptpadj = 'X'
  where ptpadj <> ''
--    and ptdbas = 'V'
--    and ptco# = 'RY1'
    and ptdate > 20121231
    and ptltyp = 'L'
    and ptcode = 'TT'
  group by cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date), 
    ptco#, ptro#, ptline, pttech) a
where hours <> 0
union 
select 'NEG' as source,  
  cast(left(digits(ptdate), 4) || '-' || substr(digits(ptdate), 5, 2) || '-' || substr(digits(ptdate), 7, 2) as date),
  ptco# as storecode, trim(ptro#) as ro, ptline as line, pttech as technumber, ptlhrs as hours
from rydedata.sdprdet
where ptdate > 20121231
  and ptlhrs < 0
  and ptdbas <> 'V'
  
SELECT * 
FROM adj.tmpsdprdet a
INNER JOIN tptechs b on a.pttech = b.technumber  
WHERE ptpadj <> ''
  AND ptdate BETWEEN '12/01/2013' AND '12/14/2013'

  
SELECT *
FROM adj.tmpsdprdet
WHERE ptlhrs < 0
  AND ptdbas <> 'V' ORDER BY ptdate desc
  AND ptdate BETWEEN '12/01/2013' AND '12/14/2013'