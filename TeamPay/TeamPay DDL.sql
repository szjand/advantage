/*
12/6
    added tpShopValues
12/15
    turns out the pool percentage IS NOT a shop value, but a team value
    eg, team 1: 23%, team 2: 18.5%, etc  
12/17
	 Other pay rate has become the previous hourly rate, last hourly rate IN arkona
	 ADD it AS an attribute IN tpTechValues
	 ADD it AS a COLUMN IN tpdata, nope, just UPDATE the existing COLUMN TechHourlyRate
	 to these new values
*/

EXECUTE PROCEDURE sp_dropreferentialintegrity('zDepartments-tpTechs');
EXECUTE PROCEDURE sp_dropreferentialintegrity('tpTechs-tpTeamTechs');
DROP TABLE tpTechs;
CREATE TABLE tpTechs (
  TechKey autoinc constraint NOT NULL,
  DepartmentKey integer constraint NOT NULL,
  TechNumber cichar(3) constraint NOT NULL,
  FirstName cichar(25) constraint NOT NULL,
  LastName cichar(25) constraint NOT NULL,
  EmployeeNumber cichar(9) constraint NOT NULL,
  constraint PK Primary key (TechKey)) IN database;
-- unique indexes
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechs','tpTechs.adi','NK','DepartmentKey;TechNumber',
   '',2051,512, '' );   
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechs','tpTechs.adi','TechNumber','TechNumber',
   '',2,512,'' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechs','tpTechs.adi','DepartmentKey','DepartmentKey',
   '',2,512,'' );  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechs','tpTechs.adi','EmployeeNumber','EmployeeNumber',
   '',2,512,'' );    
-- RI    
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('zDepartments-tpTechs',
     'zDepartments', 'tpTechs', 'DepartmentKey', 
     2, 2, NULL, '', '');       
             
INSERT INTO tpTechs (departmentkey, technumber, firstname, lastname, employeenumber)
SELECT 18, a.technumber, b.firstname, b.lastname, a.employeenumber
FROM (
  SELECT DISTINCT technumber, employeenumber
  FROM dds.dimTech
  WHERE currentrow = true
    AND technumber IN ('585','545','575','536','522','631','511','634','557','577')
    AND storecode = 'ry1'
  GROUP by technumber, employeenumber) a
LEFT JOIN (
  select distinct employeenumber, firstname, lastname
  FROM dds.edwEmployeeDim
  WHERE currentrow = true) b on a.employeenumber = b.employeenumber;
-- ADD the rest
INSERT INTO tpTechs (departmentkey, technumber, firstname, lastname, employeenumber)
SELECT 18, a.technumber, b.firstname, b.lastname, a.employeenumber
FROM (
  SELECT DISTINCT technumber, employeenumber
  FROM dds.dimTech
  WHERE currentrow = true
    AND technumber IN ('519','611','612','608','537','629','566','528','532','636','580','526','623','625','599','627')
    AND storecode = 'ry1'
  GROUP by technumber, employeenumber) a
LEFT JOIN (
  select distinct employeenumber, firstname, lastname
  FROM dds.edwEmployeeDim
  WHERE currentrow = true) b on a.employeenumber = b.employeenumber; 

-- DROP TABLE tpTeams;  
CREATE TABLE tpTeams (
  TeamKey autoinc constraint NOT NULL,
  DepartmentKey integer constraint NOT NULL,
  TeamName cichar(25) constraint NOT NULL,  
  FromDate date constraint NOT NULL,
  ThruDate date constraint NOT NULL default '12/31/9999',
  Census integer constraint NOT NULL,
  Budget money constraint NOT NULL,
  constraint pk primary key (TeamKey)) IN database;
-- unique indexes
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeams','tpTeams.adi','NK','TeamName;DepartmentKey;FromDate',
   '',2051,512, '' );    
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeams','tpTeams.adi','DepartmentKey','DepartmentKey',
   '',2,512,'' );  
-- RI      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('zDepartments-tpTeams',
     'zDepartments', 'tpTeams', 'DepartmentKey', 
     2, 2, NULL, '', '');      
INSERT INTO tpTeams (departmentkey, teamname, fromdate, census, budget)
values(18, 'Westerhausen', '09/08/2013', 5, 87.78);    
INSERT INTO tpTeams (departmentkey, teamname, fromdate, census, budget)
values(8, 'Olson', '09/08/2013', 5, 87.78); 
-- INSERT the rest
INSERT INTO tpTeams (departmentkey, teamname, fromdate, census, budget)
values(18, 'McVeigh', '09/08/2013', 4, 87.78); 
INSERT INTO tpTeams (departmentkey, teamname, fromdate, census, budget)
values(18, 'Duckstad', '09/08/2013', 4, 87.78); 
INSERT INTO tpTeams (departmentkey, teamname, fromdate, census, budget)
values(18, 'Rogne', '09/08/2013', 5, 87.78); 
INSERT INTO tpTeams (departmentkey, teamname, fromdate, census, budget)
values(8, 'Stryker', '09/08/2013', 3, 87.78); 


EXECUTE PROCEDURE sp_dropreferentialintegrity('tpTechs-tpTeamTechs');
EXECUTE PROCEDURE sp_dropreferentialintegrity('tpTeams-tpTeamTechs');
DROP TABLE tpTeamTechs;

CREATE TABLE tpTeamTechs (
  TeamKey integer constraint NOT NULL,
  TechKey integer constraint NOT NULL,
  FromDate date constraint NOT NULL,
  ThruDate date constraint NOT NULL default '12/31/9999',
  TechPercentage double constraint NOT NULL,
  constraint pk primary key (TeamKey,TechKey,FromDate)) IN database;
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeamTechs','tpTeamTechs.adi','TeamKey','TeamKey',
   '',2,512,'' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeamTechs','tpTeamTechs.adi','TechKey','TechKey',
   '',2,512,'' );     
-- RI      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('tpTechs-tpTeamTechs',
     'tpTechs', 'tpTeamTechs', 'TechKey', 
     2, 2, NULL, '', '');          
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('tpTeams-tpTeamTechs',
     'tpTeams', 'tpTeamTechs', 'TeamKey', 
     2, 2, NULL, '', '');      

INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)    
SELECT a.teamkey, b.techkey, '09/08/2013', .24 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Westerhausen' and b.technumber = '585'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)    
SELECT a.teamkey, b.techkey, '09/08/2013', .25 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Westerhausen' and b.technumber = '545'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)    
SELECT a.teamkey, b.techkey, '09/08/2013', .17 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Westerhausen' and b.technumber = '575'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)    
SELECT a.teamkey, b.techkey, '09/08/2013', .17 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Westerhausen' and b.technumber = '536'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)    
SELECT a.teamkey, b.techkey, '09/08/2013', .17 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Westerhausen' and b.technumber = '522'; 

INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .26 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Olson'  and b.technumber = '631'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)       
SELECT a.teamkey, b.techkey, '09/08/2013', .20 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Olson'  and b.technumber = '511'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .185 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Olson'  and b.technumber = '634'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .17 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Olson'  and b.technumber = '557'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .185 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Olson'  and b.technumber = '577'; 
-- ADD the rest
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .28 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'McVeigh'  and b.technumber = '519'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .24 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'McVeigh'  and b.technumber = '611';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .24
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'McVeigh'  and b.technumber = '612';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .24 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'McVeigh'  and b.technumber = '608';

INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .29 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Duckstad'  and b.technumber = '537'; 
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .22 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Duckstad'  and b.technumber = '629';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .23 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Duckstad'  and b.technumber = '566';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .26 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Duckstad'  and b.technumber = '528';

INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .35 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Rogne'  and b.technumber = '532';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .15 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Rogne'  and b.technumber = '636';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .18 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Rogne'  and b.technumber = '580';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .17 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Rogne'  and b.technumber = '526';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .15 
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Rogne'  and b.technumber = '623';

INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .38
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Stryker'  and b.technumber = '625';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .32
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Stryker'  and b.technumber = '599';
INSERT INTO tpTeamTechs (teamkey, techkey, fromdate, TechPercentage)        
SELECT a.teamkey, b.techkey, '09/08/2013', .25
FROM tpTeams a, tpTechs b WHERE a.TeamName = 'Stryker'  and b.technumber = '627';

-- UPDATE 
-- adding rows for payperiods back to 09/08/2013 with ELR (budget) FROM labor profit
-- which leads to refactoring tpTeams
-- factor out Census, Budget INTO tpTeamValues

ALTER TABLE tpTeams
DROP COLUMN Census
DROP COLUMN Budget;
-- DROP TABLE tpTeams;  
CREATE TABLE tpTeamValues (
  TeamKey integer constraint NOT NULL,
  Census integer constraint NOT NULL,
  Budget money constraint NOT NULL,  
  FromDate date constraint NOT NULL,
  ThruDate date constraint NOT NULL default '12/31/9999',
  constraint pk primary key (TeamKey, FromDate)) IN database;
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeamValues','tpTeamValues.adi','TeamKey','TeamKey',
   '',2,512,'' );     
-- RI      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('tpTeams-tpTeamValues',
     'tpTeams', 'tpTeamValues', 'TeamKey', 
     2, 2, NULL, '', '');     
  
SELECT *
FROM tpteams 

INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.08, '09/08/2013', '09/21/2013'
FROM tpteams WHERE teamname = 'Westerhausen';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.08, '09/08/2013', '09/21/2013'
FROM tpteams WHERE teamname = 'Olson';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*88.08, '09/08/2013', '09/21/2013'
FROM tpteams WHERE teamname = 'McVeigh';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*88.08, '09/08/2013', '09/21/2013'
FROM tpteams WHERE teamname = 'Duckstad';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.08, '09/08/2013', '09/21/2013'
FROM tpteams WHERE teamname = 'Rogne';
INSERT INTO tpTeamValues
SELECT teamkey, 3, 3*.2*88.08, '09/08/2013', '09/21/2013'
FROM tpteams WHERE teamname = 'Stryker';

INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*87.98, '09/22/2013', '10/05/2013'
FROM tpteams WHERE teamname = 'Westerhausen';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*87.98, '09/22/2013', '10/05/2013'
FROM tpteams WHERE teamname = 'Olson';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*87.98, '09/22/2013', '10/05/2013'
FROM tpteams WHERE teamname = 'McVeigh';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*87.98, '09/22/2013', '10/05/2013'
FROM tpteams WHERE teamname = 'Duckstad';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*87.98, '09/22/2013', '10/05/2013'
FROM tpteams WHERE teamname = 'Rogne';
INSERT INTO tpTeamValues
SELECT teamkey, 3, 3*.2*87.98, '09/22/2013', '10/05/2013'
FROM tpteams WHERE teamname = 'Stryker';

INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.23, '10/06/2013', '10/19/2013'
FROM tpteams WHERE teamname = 'Westerhausen';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.23, '10/06/2013', '10/19/2013'
FROM tpteams WHERE teamname = 'Olson';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*88.23, '10/06/2013', '10/19/2013'
FROM tpteams WHERE teamname = 'McVeigh';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*88.23, '10/06/2013', '10/19/2013'
FROM tpteams WHERE teamname = 'Duckstad';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.23, '10/06/2013', '10/19/2013'
FROM tpteams WHERE teamname = 'Rogne';
INSERT INTO tpTeamValues
SELECT teamkey, 3, 3*.2*88.23, '10/06/2013', '10/19/2013'
FROM tpteams WHERE teamname = 'Stryker';

INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.14, '10/20/2013', '11/02/2013'
FROM tpteams WHERE teamname = 'Westerhausen';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.14, '10/20/2013', '11/02/2013'
FROM tpteams WHERE teamname = 'Olson';
INSERT INTO tpTeamValues
SELECT teamkey, 3, 4*.2*88.14, '10/20/2013', '11/02/2013'
FROM tpteams WHERE teamname = 'McVeigh';
INSERT INTO tpTeamValues
SELECT teamkey, 3, 4*.2*88.14, '10/20/2013', '11/02/2013'
FROM tpteams WHERE teamname = 'Duckstad';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.14, '10/20/2013', '11/02/2013'
FROM tpteams WHERE teamname = 'Rogne';
INSERT INTO tpTeamValues
SELECT teamkey, 3, 3*.2*88.14, '10/20/2013', '11/02/2013'
FROM tpteams WHERE teamname = 'Stryker';

INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.04, '11/03/2013', '11/16/2013'
FROM tpteams WHERE teamname = 'Westerhausen';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.04, '11/03/2013', '11/16/2013'
FROM tpteams WHERE teamname = 'Olson';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*88.04, '11/03/2013', '11/16/2013'
FROM tpteams WHERE teamname = 'McVeigh';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*88.04, '11/03/2013', '11/16/2013'
FROM tpteams WHERE teamname = 'Duckstad';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88.04, '11/03/2013', '11/16/2013'
FROM tpteams WHERE teamname = 'Rogne';
INSERT INTO tpTeamValues
SELECT teamkey, 3, 3*.2*88.04, '11/03/2013', '11/16/2013'
FROM tpteams WHERE teamname = 'Stryker';

INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88, '11/17/2013', '11/30/2013'
FROM tpteams WHERE teamname = 'Westerhausen';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88, '11/17/2013', '11/30/2013'
FROM tpteams WHERE teamname = 'Olson';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*88, '11/17/2013', '11/30/2013'
FROM tpteams WHERE teamname = 'McVeigh';
INSERT INTO tpTeamValues
SELECT teamkey, 4, 4*.2*88, '11/17/2013', '11/30/2013'
FROM tpteams WHERE teamname = 'Duckstad';
INSERT INTO tpTeamValues
SELECT teamkey, 5, 5*.2*88, '11/17/2013', '11/30/2013'
FROM tpteams WHERE teamname = 'Rogne';
INSERT INTO tpTeamValues
SELECT teamkey, 3, 3*.2*88, '11/17/2013', '11/30/2013'
FROM tpteams WHERE teamname = 'Stryker';

 
 
-- need to factor tech% out of tpTeamTech 
-- CREATE a TABLE tpTeamTechValues
ALTER TABLE tpTeamTechs
DROP column TechPercentage;
-- 11/23 changed my mind, make it tpTechValues, round AND round AND round :: fuck it
-- it was either that OR ADD a surrogate key to tpTeamTechs OR to implement RI
-- tpTeamTechValues would have needed to FromDate columns one AS part of a composite
-- foreign key AND one for the NK, which seemed goofy to me
-- attracted to the seeming symmetry of teams ---<teamvalues AND techs---<techvalues

-- Table Type of tpTeamTechs is ADT
-- 12/17 ADD previousHourlyRate

Create Table tpTechValues(
   TechKey Integer constraint NOT NULL,
   FromDate Date constraint NOT NULL,
   ThruDate Date constraint NOT NULL,
   TechTeamPercentage Double constraint NOT NULL,
   constraint pk primary key (TechKey, FromDate)) IN database;
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechValues','tpTechValues.adi','TechKey','TechKey',
   '',2,512,'' );     
-- RI      
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('tpTechs-tpTechValues',
     'tpTechs', 'tpTechValues', 'TechKey', 
     2, 2, NULL, '', '');       

INSERT INTO "tpTechValues" VALUES( 8, '2013-09-08', '9999-12-31', 0.24 );
INSERT INTO "tpTechValues" VALUES( 4, '2013-09-08', '9999-12-31', 0.25 );
INSERT INTO "tpTechValues" VALUES( 6, '2013-09-08', '9999-12-31', 0.17 );
INSERT INTO "tpTechValues" VALUES( 3, '2013-09-08', '9999-12-31', 0.17 );
INSERT INTO "tpTechValues" VALUES( 2, '2013-09-08', '9999-12-31', 0.17 );
INSERT INTO "tpTechValues" VALUES( 9, '2013-09-08', '9999-12-31', 0.26 );
INSERT INTO "tpTechValues" VALUES( 1, '2013-09-08', '9999-12-31', 0.2 );
INSERT INTO "tpTechValues" VALUES( 10, '2013-09-08', '9999-12-31', 0.185 );
INSERT INTO "tpTechValues" VALUES( 5, '2013-09-08', '9999-12-31', 0.17 );
INSERT INTO "tpTechValues" VALUES( 7, '2013-09-08', '9999-12-31', 0.185 );
INSERT INTO "tpTechValues" VALUES( 11, '2013-09-08', '9999-12-31', 0.28 );
INSERT INTO "tpTechValues" VALUES( 20, '2013-09-08', '9999-12-31', 0.24 );
INSERT INTO "tpTechValues" VALUES( 21, '2013-09-08', '9999-12-31', 0.24 );
INSERT INTO "tpTechValues" VALUES( 19, '2013-09-08', '9999-12-31', 0.24 );
INSERT INTO "tpTechValues" VALUES( 15, '2013-09-08', '9999-12-31', 0.29 );
INSERT INTO "tpTechValues" VALUES( 25, '2013-09-08', '9999-12-31', 0.22 );
INSERT INTO "tpTechValues" VALUES( 16, '2013-09-08', '9999-12-31', 0.23 );
INSERT INTO "tpTechValues" VALUES( 13, '2013-09-08', '9999-12-31', 0.26 );
INSERT INTO "tpTechValues" VALUES( 14, '2013-09-08', '9999-12-31', 0.35 );
INSERT INTO "tpTechValues" VALUES( 26, '2013-09-08', '9999-12-31', 0.15 );
INSERT INTO "tpTechValues" VALUES( 17, '2013-09-08', '9999-12-31', 0.18 );
INSERT INTO "tpTechValues" VALUES( 12, '2013-09-08', '9999-12-31', 0.17 );
INSERT INTO "tpTechValues" VALUES( 22, '2013-09-08', '9999-12-31', 0.15 );
INSERT INTO "tpTechValues" VALUES( 23, '2013-09-08', '9999-12-31', 0.38 );
INSERT INTO "tpTechValues" VALUES( 18, '2013-09-08', '9999-12-31', 0.32 );
INSERT INTO "tpTechValues" VALUES( 24, '2013-09-08', '9999-12-31', 0.25 );

ALTER TABLE tpTechValues
ADD COLUMN previousHourlyRate double;

UPDATE tpTechValues
SET previousHourlyRate = x.hourlyrate
FROM (
  SELECT b.*, c.hourlyrate
  FROM tpTechValues a
  LEFT JOIN tpTechs b ON a.techkey = b.techkey
  LEFT JOIN dds.edwEmployeeDim c ON b.employeenumber = c.employeenumber
    AND c.currentrow = true) x
WHERE tpTechValues.techkey = x.techkey;	

-- populated FROM Arkona
drop table tpstgGrossPay;
CREATE TABLE tpStgGrossPay (
  PayPeriodEnd date,
  EmployeeNumber cichar(9),
  GrossPay money) IN database;
-- 12/15
ALTER TABLE tpSTgGrossPay
ALTER COLUMN PayPeriodEnd PayPeriodStart date;  

ADD the
  
/*   
-- 1 row per date/tech
SELECT thedate, technumber FROM (  
SELECT a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  a.biweeklypayperiodsequence, dayinbiweeklypayperiod,
  b.technumber, b.employeenumber, b.firstname, b.lastname
-- SELECT *
FROM dds.day a, tpTechs b 
WHERE a.thedate BETWEEN '09/28/2013' AND curdatE()
) x GROUP BY thedate, technumber HAVING count(*) > 1
*/    

CREATE TABLE tpShopValues (
  departmentKey integer constraint NOT NULL,
  fromDate date constraint NOT NULL,
  thruDate date constraint NOT NULL default '12/31/9999',
  poolPercentage double constraint NOT NULL,
  effectiveLaborRate double constraint NOT NULL,
  constraint PK Primary key (departmentKey, fromDate)) IN database;  

INSERT INTO tpShopValues  
SELECT DISTINCT 18, payperiodstart, payperiodend, .20, teambudget
FROM tpdata
where teamcensus = 5;

INSERT INTO tpShopValues (departmentkey, fromdate, poolpercentage, effectivelaborrate)
values(18, '12/01/2013', .20, 88.01);
  

-- 12/15
ALTER TABLE tpShopValues
DROP COLUMN poolPercentage;

-- 12/15 tpTeamValues
ALTER TABLE tpTeamValues
ADD COLUMN poolPercentage double POSITION 4;

SELECT *
FROM tpTeams, tpShopValues
