mark koller returned to WORK IN the middle of last pay period (6/29 - 7/12/14)
since before his medical leave, he has been assigned to team Girodat.
He has been clocking (and flagging) hours since 7/8 AND they are being applied
to the Girodat team for last pay period.
Nobody wants that, so, what i need to DO IS remove him FROM team pay effective
6/29.

what needs to be changed

ahh shit, that fucks with team budget AND percentages

-- need a snapshot of ALL relevant values for the payperiod 6/29 - 7/12
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
ORDER BY teamname  

SELECT a.teamname, b.*, c.*
FROM tpTeams a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
INNER JOIN tpTeamTechs c on a.teamKey = c.teamKey
WHERE a.teamName = 'girodat'
  AND b.fromDate = '06/29/2014'

-- TRY zeroing out koller clock & flag hours, THEN rerun xfmTpData to regenerate team values 
UPDATE tpdata
SET techClockHoursDay = 0,
    techClockHoursPPTD = 0,
	techFlagHoursDay = 0,
	techFlagHoursPPTD = 0
WHERE lastname = 'koller'
  AND payperiodstart = '06/29/2014'
  
EXECUTE PROCEDURE xfmTpData()  

-- nope, does NOT WORK, he IS still assigned so the values get regenerated
SELECT *
FROM tpdata
WHERE lastname = 'koller'
  AND payperiodstart = '06/29/2014'
  
-- take 2, remove him FROM teamTechs effective 06/29, rerun xfmTpData
-- yep, this worked girodat team prof went FROM 129.9 to 136.9
-- even though this works, it leaves some kinks IN data consistency
-- 1. teamValues, census shows 4 AND IS actually now 3, which effects the budget
--      which effects tech commission rate, none of which has been adjusted
--      i don't think it IS any real problem, he said NOT wanting to fucking change everything
select * 
FROM tpTeamTechs a
INNER JOIN tpTechs b on a.techKey = b.techKey
WHERE b.lastname = 'koller'

-- ran this against production on 7/14, will let overnight xfmTpData take care of it
UPDATE tpTeamTechs
SET thruDate = '06/28/2014'
WHERE teamKey = 21
  AND techKey = 3;

DELETE FROM tpData  
WHERE lastname = 'koller'
  AND payperiodstart = '06/29/2014';
  
EXECUTE PROCEDURE xfmTpData()  ;  
  
  
SELECT *
FROM tpdata
WHERE thedate in ('06/12/2014','06/28/2014','06/29/2014','07/13/2014')  
  AND lastname = 'girodat'
  