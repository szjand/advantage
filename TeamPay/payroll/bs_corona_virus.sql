CREATE TABLE bs_corona_guarantee (
  employee_number cichar(9),
  employee_name cichar(60),
  guarantee double);
  
insert into bs_corona_guarantee values('1146991','WALDEN, CHRIS W',2702.83);
insert into bs_corona_guarantee values('1110425','PETERSON, BRIAN D',3524.93);
insert into bs_corona_guarantee values('160930','BLUMHAGEN, KATHERINE M',2373.15);
insert into bs_corona_guarantee values('135770','DRISCOLL, TERRANCE',3146.12);
insert into bs_corona_guarantee values('188338','LUEKER, DAVID C',2082.68);
insert into bs_corona_guarantee values('165357','HILL, BRIAN',2610.08);
insert into bs_corona_guarantee values('1118722','ROSE, CORY',3142.21);
insert into bs_corona_guarantee values('1147061','WALTON, JOSHUA',2232.54);
insert into bs_corona_guarantee values('141069','EVAVOLD, DANIEL J',2587.88);
insert into bs_corona_guarantee values('191350','MAVITY, ROBERT',2308.358);
insert into bs_corona_guarantee values('1125565','SEVIGNY, SCOTT',2498.52);
insert into bs_corona_guarantee values('171055','JACOBSON, PETER A',3597.72);
insert into bs_corona_guarantee values('1108200','OSTLUND, ANNE B',2077.79);
insert into bs_corona_guarantee values('150105','GARDNER, CHAD A',2701.67);
insert into bs_corona_guarantee values('1111325','POWELL, GAYLA R',1921.75);
insert into bs_corona_guarantee values('1106400','OLSON, JUSTIN S',1874.03);
insert into bs_corona_guarantee values('1107950','ORTIZ, GUADALUPE',2123.10);
insert into bs_corona_guarantee values('150120','GARDNER, JOHN T',2104.61466666667);
insert into bs_corona_guarantee values('184920','LENE, RYAN',2708.65);
insert into bs_corona_guarantee values('1114921','REUTER, TIMOTHY J',2240.31);
insert into bs_corona_guarantee values('11732','AGUILAR, ANNA',1325.78);
insert into bs_corona_guarantee values('184614','LAMONT, BRANDON',2553.16);
insert into bs_corona_guarantee values('11660','ADAM, PATRICK',2538.45);
insert into bs_corona_guarantee values('1100625','OLSON, CODEY',2196.09);
insert into bs_corona_guarantee values('137101','EBERLE, MARCUS M',1725.38);
insert into bs_corona_guarantee values('165275','HICKS, EMILY',1131.82);
insert into bs_corona_guarantee values('1126040','SHERECK, LOREN',2490.70);
insert into bs_corona_guarantee values('151702','MULHERN, ALAN',1872.18);
insert into bs_corona_guarantee values('145789','BUCHANAN, KYLE',2471.67);
insert into bs_corona_guarantee values('122558','GRYSKIEWICZ, JONATHAN',1740.45);
insert into bs_corona_guarantee values('15648','DIBI, ANDREW',1577.37);
insert into bs_corona_guarantee values('165327','GUST, MICAH',122.16);
insert into bs_corona_guarantee values('198542','MCMILLIAN, TARA',1432.57);
insert into bs_corona_guarantee values('167934','PFAFF, JONATHAN',1444.97);
insert into bs_corona_guarantee values('157953','BIES II, DAVID',2901.87);
insert into bs_corona_guarantee values('142589','HLAVAC, TERRI',1419.32);
insert into bs_corona_guarantee values('1109700','PASCHKE, MATTHEW G',2135.08);
insert into bs_corona_guarantee values('162984','WIKSTROM, ABIGAIL',1080.57);
insert into bs_corona_guarantee values('174384','MOGAVERO, MICHAEL',1464.46);
  