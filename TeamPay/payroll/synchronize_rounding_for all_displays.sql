proc - josh:
  SELECT techName, teamName, commissionRate, ptoRate, bonusRate,
    CASE WHEN totalHours > 90 THEN clockhours - (totalhours - 90) ELSE clockHours END AS commissionHours,
    ptoHours, 
    CASE WHEN totalHours > 90 THEN round(totalHours - 90, 2) ELSE 0 END AS bonusHours,
    teamFlagHours, teamClockHours, teamProficiency, effectiveLaborRate
  FROM (   
    SELECT TRIM(firstName) + ' ' + lastName AS techName, teamName, 
      round(techTFRRate, 2) AS commissionRate,
      techHourlyRate AS ptoRate,
      2 * round(techTFRRate, 2) AS bonusRate,
      techClockHoursPPTD AS clockHours,
      techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours,
      techvacationhourspptd + techptohourspptd + techholidayhourspptd AS ptoHours,
      teamFlagHoursPPTD + teamFlagHourAdjustmentsPPTD AS teamFlagHours,
      teamClockHoursPPTD AS teamClockHours,
      teamProfPPTD AS teamProficiency,
      b.effectiveLaborRate
    FROM tpdata a
    LEFT JOIN tpShopValues b on '09/14/2019' BETWEEN b.fromDate AND b.thruDate
      AND b.departmentKey = 18
    WHERE thedate = '09/14/2019'
      AND a.departmentKey = 18
      AND techKey = 7) a

-- this seems to prove out, only DO rounding at the highest level      
script - kim  
SELECT trim(x.lastname) + ', ' + trim(firstName) as Name, 
  x.employeenumber AS [Emp#],
  commRate AS [Comm Rate], 
  round(commHours * teamProf/100, 2) AS [Comm Clock Hours],
  round(round(commHours * teamProf/100, 2) * commRate, 2) AS [Total Comm Pay], round((commhours * teamprof/100) * commrate, 2)  AS total_comm_pay_2,
  bonusRate AS [Bonus Rate],
  round(bonusHours * teamProf/100, 2) AS [Bonus Hours],
  round(round(bonusHours * teamProf/100, 2) * bonusRate, 2) AS [Total Bonus Pay], round((bonushours * teamprof/100) * bonusrate, 2) AS bonus_pay_2, -- this now matches pay page
  round(ptoRate, 2) AS [Vac PTO Hol Rate],
  vacationHours + ptoHours AS [Vac PTO Hours],
  round(ptoRate, 2) * (vacationHours + ptoHours) AS [Vac PTO Pay],
  holidayHours AS [Hol Hours],
  ptoRate * holidayHours AS [Hol Pay]
  FROM (      
  select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
    d.bonusRate, d.teamProf, d.flagHours, e.clockHours, e.vacationHours, e.ptoHours, e.holidayHours, 
    e.totalHours,
--    CASE WHEN e.totalHours > 90 THEN 90 else e.clockHours end AS commHours,
--    round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours
    CASE WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
    round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
  FROM (
    select c.teamname AS Team, lastname, firstname, employeenumber, 
      techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
      2 * round(techtfrrate, 2) AS BonusRate, teamprofpptd AS teamProf, 
      TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf  
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamkey = b.teamkey
      -- AND b.thrudate > curdate()
      AND b.thrudate >= '09/14/2019'
    LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    WHERE thedate = '09/14/2019'
      AND a.departmentKey = 18) d
  LEFT JOIN (
    select employeenumber, techclockhourspptd AS clockHours, 
      techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
      techholidayhourspptd AS holidayHours,
      techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamkey = b.teamkey
      -- AND b.thrudate > curdate()
      AND b.thrudate >= '09/14/2019'
    LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    WHERE thedate = '09/14/2019'
      AND a.departmentKey = 18) e on d.employeenumber = e.employeenumber
WHERE lastname = 'heffernan') x      

want to go down the line i used for honda pay, one query to generate ALL possible necessary numbers,
THEN extract whatever  numbers are needed for whatever display

displays:
payroll for kim
payroll summary manager for managers
techs page
teamleaders page