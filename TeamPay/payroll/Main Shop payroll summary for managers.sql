/*
08/24 
  separated body shop AND main shop
  remove transition
  ADD clock hour bonus

make rounding consistent with what gets sent to kim
AND what shows on individual tech pay pages
what gets rounded:
commPay seems to be off, on kim:
  round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2) AS [Total Commission Pay],
  
9/29
Bonus, after mindfucking myself INTO a frenzy, ben clarified it 
total hours (pto + vac + clock) > 90

clock hours = 80
pto hours = 20
comm pay = 70 * commRate * teamProf
bonus pay = 10 * bonusRate * teamProf
pto pay = 20 * ptoRate

clock hours = 100
pto hours = 16
comm pay = 74 * commRate * teamProf
bonus pay = 26 * bonusRate * teamProf
pto pay = 16 * ptoRate

summary: 
IF total hours > 90
bonus hours = total hours - 90
commission hours = clock hours - bonus hours  
ELSE commision hours = clock hours

eg
  CASE WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
  round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
  
02/03/20
  *a*  bonus only applis IF team prof >= 100  
04/26/20 corona guarantee  
06/21/20: corona guarantee finished
11/08/20: ADD diagnostic team guarantee AND snap cell spiffs, need to first
		  populate TABLE tp_main_shop_guarantee_spiffs
01/19/21: TABLE tp_main_shop_guarantee_spiffs no longer needed
          adjust the script to generate a guarantee for the diagnostic team
		  the guarantee IS based solely on clock hours with a fixed hourly rate
		  requires regular hours, OT hours, bonus hours & pto hours
		  		   				   	  		 hourly rate  pto rate
		  		 Flaat,Gavin	145801		 35.98		  36.09
				 Olson,Jay		1106399		 38.56		  38.68
				 Rogers,Mitch	1117901		 26.79		  24.80
				 Gray,Nathan	152410		 29.62		  29.80
			
			the hourly rate IS constant
			the pto rate here will change WHEN ALL the other pto rates are updated
	
CREATE TABLE blueprint_guarantee_rates (
  employee_number cichar(8),
  name cichar(30),
  hourly_rate double,
  pto_rate double,
  from_date date,
  thru_date date);				 
			
SELECT * FROM blueprint_guarantee_rates;
			
INSERT INTO blueprint_guarantee_rates values('145801','flaat, gavin', 35.98, 36.09, '01/01/2021','12/31/9999');
INSERT INTO blueprint_guarantee_rates values('1106399','olson, jay', 38.56, 38.68, '01/01/2021','12/31/9999');
INSERT INTO blueprint_guarantee_rates values('1117901','rogers, mitch', 26.79, 24.8, '01/01/2021','12/31/9999');
INSERT INTO blueprint_guarantee_rates values('152410','gray, nathan', 29.62, 29.80, '01/01/2021','12/31/9999');
 	
02/14/2021 new hourly rate for blueprint_guarantee, also, everyone IS getting pto adjustment 	
Gavin Flaat: old $35.98  new $38.00
Nate Gray: old $29.62  new $32.00
Jay Olson: old $38.56 new $40.00
Mitch Rogers: old $26.70 new $31.00

UPDATE blueprint_guarantee_rates
SET thru_date = '02/13/2021'
WHERE thru_date > curdate();

INSERT INTO blueprint_guarantee_rates values('145801','flaat, gavin', 38.00, 36.98, '02/14/2021','12/31/9999');
INSERT INTO blueprint_guarantee_rates values('1106399','olson, jay', 40.00, 41.98, '02/14/2021','12/31/9999');
INSERT INTO blueprint_guarantee_rates values('1117901','rogers, mitch', 31.00, 27.71, '02/14/2021','12/31/9999');
INSERT INTO blueprint_guarantee_rates values('152410','gray, nathan', 32.00, 30.92, '02/14/2021','12/31/9999');

SELECT * FROM blueprint_guarantee_rates ORDER BY name
	  
*/

DECLARE @department integer;
DECLARE @payRollEnd date; 
DECLARE @payRollStart date;
@department = 18;
@payRollStart = '12/19/2021';
@payRollEnd = '01/01/2022';
SELECT team,lastname,firstname,employeenumber AS "emp #",ptorate,commrate,bonusrate,teamprof,
  flaghours,adjustments,total_flaghours,clockhours,vacationhours,ptohours,
  holidayhours,total,guarantee,
  CASE
    WHEN guarantee IS NULL THEN total
	WHEN guarantee > total THEN guarantee
	ELSE total
  END AS "total pay"  
/*  
  holidayhours,total,guarantee,snap_cell,ot_variance,
  CASE
    WHEN guarantee IS NULL THEN total + coalesce(snap_cell, 0) + coalesce(ot_variance, 0)
	ELSE guarantee + coalesce(snap_cell, 0) + coalesce(ot_variance, 0)
  END AS "total pay"
*/  
FROM (  
  SELECT x.*, 
    round(commHours * teamProf/100 * commRate, 2) AS commPay,
    round(bonusHours * teamProf/100 * bonusRate, 2) AS bonusPay,
    round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS ptoPay,
    round(commHours * teamProf/100 * commRate, 2) +  
    round(bonusHours * teamProf/100 * bonusRate, 2) +
    round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS total,
	y.guarantee
--    y.guarantee, y.snap_cell, ot_variance
  FROM (  
    select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
      d.bonusRate, d.teamProf, d.flagHours, d.adjustments, 
      d.flaghours + d.adjustments as total_flaghours,e.clockHours, e.vacationHours, 
      e.ptoHours, e.holidayHours, e.totalHours,
    -- *a*  
      CASE 
        WHEN e.totalHours > 90 and teamprof >= 100 THEN e.clockhours - (totalhours - 90) 
        else e.clockHours 
      end AS commHours,
      round(
        CASE 
          WHEN totalHours > 90 and teamprof >= 100 THEN totalHours - 90 
          ELSE 0 
        END, 2) AS bonusHours
    FROM (
      select c.teamname AS Team, lastname, firstname, employeenumber, 
        techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
        2 * round(techtfrrate, 2) AS BonusRate, -- teamprofpptd AS teamProf, 
        TechFlagHoursPPTD as FlagHours, techFlagHourAdjustmentsPPTD as Adjustments,
        teamprofpptd AS TeamProf   
      FROM tpdata a
      INNER JOIN tpteamtechs b on a.techkey = b.techkey
        AND a.teamkey = b.teamkey
    	AND @payrollend BETWEEN b.fromdate AND b.thrudate
    --    AND b.thrudate > curdate()
      LEFT JOIN tpTeams c on b.teamKey = c.teamKey
      WHERE thedate = @payRollEnd
        AND a.departmentKey = @department) d
    LEFT JOIN (
      select employeenumber, techclockhourspptd AS clockHours, 
        techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
        techholidayhourspptd AS holidayHours,
        techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
      FROM tpdata a
      INNER JOIN tpteamtechs b on a.techkey = b.techkey
        AND a.teamkey = b.teamkey
    	AND @payrollend BETWEEN b.fromdate AND b.thrudate
    --    AND b.thrudate > curdate()
      LEFT JOIN tpTeams c on b.teamKey = c.teamKey
      WHERE thedate = @payRollEnd
        AND a.departmentKey = @department) e on d.employeenumber = e.employeenumber) x
  LEFT JOIN ( -- guarantee for the blueprint team
    SELECT employee_number, reg + ot + pto + bonus AS guarantee
    FROM (
      SELECT aa.name, aa.employee_number, round(aa.hourly_rate * bb.clock, 2) AS reg, 
        round(aa.hourly_rate * 1.5 * bb.ot, 2) AS ot, 
        round(aa.pto_rate * (vac + pto + hol), 2) AS pto,
        case
          when bb.clock + bb.ot + bb.pto > 90 THEN  ((bb.clock + bb.ot + bb.pto) - 90) * 2 * aa.hourly_rate
      	ELSE 0
        END AS bonus
      FROM blueprint_guarantee_rates aa
      JOIN (			 
        select c.name, c.employeenumber, SUM(a.regularhours) AS clock, SUM(a.overtimehours) AS ot,
          SUM(a.vacationhours) AS vac, SUM(a.ptohours) AS pto, SUM(a.holidayhours) AS hol 
        FROM dds.edwclockhoursfact a
        JOIN dds.day b on a.datekey = b.datekey
          AND b.thedate BETWEEN @payRollStart AND @payRollEnd
        JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
          AND c.employeenumber IN ('145801','1106399','1117901','152410')
        GROUP BY c.name, c.employeenumber) bb on aa.employee_number = bb.employeenumber
      WHERE aa.thru_date = '12/31/9999') cc) y on x.employeenumber = y.employee_number) z		  		
/*	  
  LEFT JOIN tp_main_shop_guarantee_spiffs y on x.employeenumber = y.employee_number	
    AND y.the_date = @payRollEnd) y
*/	
ORDER BY lastname --team, firstname, lastname
/**/


/*
blueprint guarantee detail		 
        select c.name, c.employeenumber, SUM(a.regularhours) AS clock, SUM(a.overtimehours) AS ot,
          SUM(a.vacationhours) AS vac, SUM(a.ptohours) AS pto, SUM(a.holidayhours) AS hol 
        FROM dds.edwclockhoursfact a
        JOIN dds.day b on a.datekey = b.datekey
          AND b.thedate BETWEEN '01/31/2021' AND '02/13/2021'
        JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
          AND c.employeenumber IN ('145801','1106399','1117901','152410')
        GROUP BY c.name, c.employeenumber
*/		
	  
	  
/*
-- corona guarantee
-- 04/26 this looks good
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 18;
@payRollEnd = '06/20/2020';
SELECT aa.*,
  CASE
    WHEN guarantee > earned_pay THEN guarantee
    ELSE earned_pay
  END AS actual_pay
FROM (
  SELECT x.*, 
    round(commHours * teamProf/100 * commRate, 2) AS commPay,
    round(bonusHours * teamProf/100 * bonusRate, 2) AS bonusPay,
    round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS ptoPay,
    round(commHours * teamProf/100 * commRate, 2) +  
    round(bonusHours * teamProf/100 * bonusRate, 2) +
    round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS earned_pay,
    guarantee
--    CASE
--      WHEN guarantee > round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) THEN guarantee
--      ELSE round(ptoRate * (vacationHours + ptoHours + holidayHours), 2)
--    END AS actual_pay
  FROM (  
    select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
      d.bonusRate, d.teamProf, d.flagHours, d.adjustments, 
      d.flaghours + d.adjustments as total_flaghours,e.clockHours, e.vacationHours, 
      e.ptoHours, e.holidayHours, e.totalHours,
    -- *a*  
      CASE 
        WHEN e.totalHours > 90 and teamprof >= 100 THEN e.clockhours - (totalhours - 90) 
        else e.clockHours 
      end AS commHours,
      round(
        CASE 
          WHEN totalHours > 90 and teamprof >= 100 THEN totalHours - 90 
          ELSE 0 
        END, 2) AS bonusHours
    FROM (
      select c.teamname AS Team, lastname, firstname, employeenumber, 
        techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
        2 * round(techtfrrate, 2) AS BonusRate, -- teamprofpptd AS teamProf, 
        TechFlagHoursPPTD as FlagHours, techFlagHourAdjustmentsPPTD as Adjustments,
        teamprofpptd AS TeamProf   
      FROM tpdata a
      INNER JOIN tpteamtechs b on a.techkey = b.techkey
        AND a.teamkey = b.teamkey
        AND b.thrudate > curdate()
      LEFT JOIN tpTeams c on b.teamKey = c.teamKey
      WHERE thedate = @payRollEnd
        AND a.departmentKey = @department) d
    LEFT JOIN (
      select employeenumber, techclockhourspptd AS clockHours, 
        techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
        techholidayhourspptd AS holidayHours,
        techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
      FROM tpdata a
      INNER JOIN tpteamtechs b on a.techkey = b.techkey
        AND a.teamkey = b.teamkey
        AND b.thrudate > curdate()
      LEFT JOIN tpTeams c on b.teamKey = c.teamKey
      WHERE thedate = @payRollEnd
        AND a.departmentKey = @department) e on d.employeenumber = e.employeenumber) x
   LEFT JOIN ms_corona_guarantee f on x.employeenumber = f.employee_number) aa
ORDER BY team, firstname, lastname
*/