/*
08/24 
  separated body shop AND main shop
  remove transition
  
10/7/14
  fuck me, somehow, bsFlatRateData showed 25 hours metal flag hours for 9/22
  could NOT figure out WHERE it came FROM, but this query (LIKE payroll)
  uses factRepairOrder instead of bsFlatRateData, so the difference showed up
  comparing this output to the vision page
  fuck me
  fuck me
  fuck me  
10/28/17: added guarantee per Randy  
04/26/20: corona guarantee
07/07/20: remove corona guarantee
*/  

/*
02/16/21
fucking gardner keeps adding adjustments after i send him the spreadsheet
today IS over the limit, he's adding LIKE 20, so build a TABLE comprised of ALL 
employees, date (payroll END date) AND amount
1 row for each employee for each date
DROP TABLE bs_adjustments;
CREATE TABLE bs_adjustments (
  first_name cichar(25),
  last_name cichar(25),
  employee_number cichar(9),
  payroll_end_date date,
  adjustment double);
  
/**/
-- adjustments
-- INSERT the base data for the pay period
-- THEN pull up the TABLE to edit the amount
-- ALL techs need a value even IF it IS 0
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 13;
@payRollEnd = '01/01/2022';	  
INSERT INTO bs_adjustments(last_name, first_name, employee_number, payroll_end_date,adjustment)
select lastname, firstname, employeenumber, @payRollEnd, 0 AS adjustment
FROM tpdata a
INNER JOIN tpteamtechs b on a.techkey = b.techkey
  AND a.teamkey = b.teamkey
  AND b.thruDate >= @payrollEnd
LEFT JOIN tpTeams c on b.teamKey = c.teamKey
WHERE thedate = @payRollEnd
  AND a.departmentKey = @department;
  
-- 10/23/21 brian peterson IS now a team of 1, no longer being paid on 2 separate rates
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 13;
@payRollEnd = '01/01/2022';	  
SELECT z.* 
--  CASE 
--    WHEN lastname = 'Wetch' AND clockhours > 89.99 AND totalpay < 2565 THEN '   2565.00'
--    ELSE '    N/A'
--  END AS Guarantee
FROM (
  select x.*-- , round(TotalCommPay - transition, 2) AS [Comm - Transition]
  FROM (
    select c.teamname AS Team, lastname, firstname, employeenumber AS "emp#", 
      techhourlyrate AS HourlyRate, round(techtfrrate, 2) AS CommRate, 
      techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
      round(techclockhourspptd*TeamProfPPTD/100, 2) as CommHours, 
      round(round(techtfrrate, 2)*round(TeamProfPPTD*techclockhourspptd/100, 2), 2) AS CommPay,
      techvacationhourspptd AS VacationHours,
      techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
	  d.adjustment, 
      round(round(techtfrrate, 2) * round(techclockhourspptd * teamprofpptd/100, 2), 2) + 
        ((techvacationhourspptd + techptohourspptd + 
          techholidayhourspptd)*techhourlyrate) + adjustment  AS TotalPay
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamkey = b.teamkey
      AND b.thruDate >= @payrollEnd
  --  INNER JOIN tpteams c on b.teamkey = c.teamkey
    LEFT JOIN tpTeams c on b.teamKey = c.teamKey
	LEFT JOIN bs_adjustments d on a.employeenumber = d.employee_number
	  AND payroll_end_date = @payRollEnd
    WHERE thedate = @payRollEnd
      AND a.departmentKey = @department) x) z order BY lastname --team



/*
effective 09/26/21 Brian Peterson IS no longer on 2 different rates for pdr AND metal WORK
but IS now IN the above AS team 22
*/	  
  
/*
PDR


-- first cut at a finished output  
-- looks good
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @pdr_rate double;
DECLARE @metal_rate double;
DECLARE @hourly_rate double;
@pdr_rate = (SELECT pdrrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@metal_rate = (SELECT metalrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@hourly_rate = (SELECT otherrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@from_date = '09/12/2021'; --------------------- change dates
@thru_date = '09/25/2021';

SELECT n.*, o.*, pdrPay + metalPay + hourlyPay AS totalPay
FROM ( 
  SELECT pdrRate, round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay,
    metalRate, 
    round(SUM(metalFlagHours), 2) AS metalFlagHours,
    round(metalRate * round(SUM(metalFlagHours), 2), 2) AS metalPay
  FROM (  
    SELECT theDate, @pdr_rate AS pdrRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      @metal_rate AS metalRate,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.closedatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.theDate BETWEEN @from_date AND @thru_date) m    
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT @hourly_rate AS hourlyRate, SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
    sum(holidayHours) AS holidayHours,
    @hourly_rate * (SUM(vacationHours) + SUM(ptoHours) + sum(holidayHours)) AS hourlyPay
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.theDate BETWEEN @from_date AND @thru_date) o  on 1 = 1 
*/   



/*
-- corona queries
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 13;
@payRollEnd = '06/20/2020';
SELECT z.*, 
  (SELECT guarantee FROM bs_corona_guarantee WHERE employee_number = z.employeenumber) AS guarantee,
  CASE 
    WHEN (SELECT guarantee FROM bs_corona_guarantee WHERE employee_number = z.employeenumber)
		 > z.earned_pay THEN (SELECT guarantee FROM bs_corona_guarantee WHERE employee_number = z.employeenumber)
    else z.earned_pay
  END AS actual_pay
FROM (
  select x.*-- , round(TotalCommPay - transition, 2) AS [Comm - Transition]
  FROM (
    select c.teamname AS Team, lastname, firstname, employeenumber, 
      techhourlyrate AS HourlyRate, round(techtfrrate, 2) AS CommRate, 
      techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
      round(techclockhourspptd*TeamProfPPTD/100, 2) as CommHours, 
      round(round(techtfrrate, 2)*round(TeamProfPPTD*techclockhourspptd/100, 2), 2) AS CommPay,
      techvacationhourspptd AS VacationHours,
      techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
      round(round(techtfrrate, 2) * round(techclockhourspptd * teamprofpptd/100, 2), 2) + 
        (techvacationhourspptd + techptohourspptd + 
          techholidayhourspptd)*techhourlyrate AS earned_pay
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamkey = b.teamkey
      AND b.thruDate >= @payrollEnd
  --  INNER JOIN tpteams c on b.teamkey = c.teamkey
    LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    WHERE thedate = @payRollEnd
      AND a.departmentKey = @department) x) z order BY lastname

DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @pdr_rate double;
DECLARE @metal_rate double;
DECLARE @hourly_rate double;
DECLARE @employee_number string;
DECLARE @guarantee double;

@employee_number = '1110425';
@guarantee = (SELECT guarantee FROM bs_corona_guarantee WHERE employee_number = '1110425');
@pdr_rate = (SELECT pdrrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@metal_rate = (SELECT metalrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@hourly_rate = (SELECT otherrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@from_date = '06/07/2020'; --------------------- change dates
@thru_date = '06/20/2020';

SELECT n.*, o.*, pdrPay + metalPay + hourlyPay AS earned_pay, @guarantee AS guarantee,
  CASE
    WHEN @guarantee > pdrPay + metalPay + hourlyPay THEN @guarantee
	ELSE pdrPay + metalPay + hourlyPay
  END AS actual_pay
FROM ( 
  SELECT pdrRate, round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay,
    metalRate, 
    round(SUM(metalFlagHours), 2) AS metalFlagHours,
    round(metalRate * round(SUM(metalFlagHours), 2), 2) AS metalPay
  FROM (  
    SELECT theDate, @pdr_rate AS pdrRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      @metal_rate AS metalRate,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.closedatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.theDate BETWEEN @from_date AND @thru_date) m    
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT @hourly_rate AS hourlyRate, SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
    sum(holidayHours) AS holidayHours,
    @hourly_rate * (SUM(vacationHours) + SUM(ptoHours) + sum(holidayHours)) AS hourlyPay
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.theDate BETWEEN @from_date AND @thru_date) o  on 1 = 1; 	 
*/	