DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 18;
@payRollEnd = '12/21/2019';
SELECT x.*, 
  round(commHours * 1 * commRate, 2) AS commPay,
  round(bonusHours * 1 * bonusRate, 2) AS bonusPay,
  round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS ptoPay,
  round(commHours * 1 * commRate, 2) +  
  round(bonusHours * 1 * bonusRate, 2) +
  round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS totalPay  
FROM (  
select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
  d.bonusRate, 100, d.flagHours, d.adjustments, 
  d.flaghours + d.adjustments as total_flaghours,e.clockHours, e.vacationHours, 
  e.ptoHours, e.holidayHours, e.totalHours,
  CASE 
    WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) 
    else e.clockHours 
  end AS commHours,
  round(
    CASE 
      WHEN totalHours > 90 THEN totalHours - 90 
      ELSE 0 
    END, 2) AS bonusHours  
FROM (
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
    2 * round(techtfrrate, 2) AS BonusRate, -- teamprofpptd AS teamProf, 
    TechFlagHoursPPTD as FlagHours, techFlagHourAdjustmentsPPTD as Adjustments,
    teamprofpptd AS TeamProf   
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
    AND b.thrudate > curdate()
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = @payRollEnd
    AND a.departmentKey = @department) d
LEFT JOIN (
  select employeenumber, techclockhourspptd AS clockHours, 
    techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
    techholidayhourspptd AS holidayHours,
    techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
    AND b.thrudate > curdate()
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = @payRollEnd
    AND a.departmentKey = @department) e on d.employeenumber = e.employeenumber) x
WHERE  lastname = 'rogers' 
ORDER BY team, firstname, lastname

  