/*
8/25
revised to include bonus info
AND rounding to agree with individual tech pay pages

9/29
Bonus, after mindfucking myself INTO a frenzy, ben clarified it 
total hours (pto + vac + clock) > 90

clock hours = 80
pto hours = 20
comm pay = 70 * commRate * teamProf
bonus pay = 10 * bonusRate * teamProf
pto pay = 20 * ptoRate

clock hours = 100
pto hours = 16
comm pay = 74 * commRate * teamProf
bonus pay = 26 * bonusRate * teamProf
pto pay = 16 * ptoRate

summary: 
IF total hours > 90
bonus hours = total hours - 90
commission hours = clock hours - bonus hours  
ELSE commision hours = clock hours

eg
  CASE WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
  round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
  
9/22/19 
  *a*
  after getting INTO how ken fucked up josh's paycheck for 8/18 -> 8/31 ( ken entered
        shop time AS vac hours, sheesh)
        josh noticed that on the ched for 9/1 -> 9/14, his pay page should 5 cents
        more than his paycheck, the difference, of course, was IN the difference
        IN rounding IN the 2 scripts, this script was incorrect
*/
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 18;
@payRollEnd = '12/21/2019'; 
SELECT z.*, 
  [Total Comm Pay]+[Total Bonus Pay]+[Vac PTO Pay]+[Hol Pay] AS [Total Gross Pay]
FROM (
  SELECT trim(x.lastname) + ', ' + trim(firstName) as Name, 
    x.employeenumber AS [Emp#],
    commRate AS [Comm Rate], 
    round(commHours * 1, 2) AS [Comm Clock Hours],
-- *a*    
    -- round(round(commHours * teamProf/100, 2) * commRate, 2) AS [Total Comm Pay], round((commhours * teamprof/100) * commrate, 2)  AS total_comm_pay_2,
    round((commhours * 1) * commrate, 2)  AS [Total Comm Pay],
    bonusRate AS [Bonus Rate],
    round(bonusHours * 1, 2) AS [Bonus Hours],
-- *a*    
    -- round(round(bonusHours * teamProf/100, 2) * bonusRate, 2) AS [Total Bonus Pay], round((bonushours * teamprof/100) * bonusrate, 2) AS bonus_pay_2, -- this now matches pay page
    round((bonushours * teamprof/100) * bonusrate, 2) AS [Total Bonus Pay],
    round(ptoRate, 2) AS [Vac PTO Hol Rate],
    vacationHours + ptoHours AS [Vac PTO Hours],
    round(ptoRate, 2) * (vacationHours + ptoHours) AS [Vac PTO Pay],
    holidayHours AS [Hol Hours],
    ptoRate * holidayHours AS [Hol Pay]
  FROM (  
  select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
    d.bonusRate, d.teamProf, d.flagHours, e.clockHours, e.vacationHours, e.ptoHours, e.holidayHours, 
    e.totalHours,
--    CASE WHEN e.totalHours > 90 THEN 90 else e.clockHours end AS commHours,
--    round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours
    CASE WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
    round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
  FROM (
    select c.teamname AS Team, lastname, firstname, employeenumber, 
      techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
      2 * round(techtfrrate, 2) AS BonusRate, teamprofpptd AS teamProf, 
      TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf  
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamkey = b.teamkey
      -- AND b.thrudate > curdate()
      AND b.thrudate >= @payRollEnd
    LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    WHERE thedate = @payRollEnd
      AND a.departmentKey = @department) d
  LEFT JOIN (
    select employeenumber, techclockhourspptd AS clockHours, 
      techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
      techholidayhourspptd AS holidayHours,
      techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamkey = b.teamkey
      -- AND b.thrudate > curdate()
      AND b.thrudate >= @payRollEnd
    LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    WHERE thedate = @payRollEnd
      AND a.departmentKey = @department) e on d.employeenumber = e.employeenumber) x) z
WHERE "Emp#" = '1117901'
ORDER BY name