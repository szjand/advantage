/*
4/27/20
There are 2 changes needed please. Carter and Mitch were recalculated because both of them missed time in the 4th quarter with injury. Their new 80 % safety net will be this going forward. Everything else looks good! Thanks this was much easier!!!

Mitch Rogers - $2180.00

Carter Ferry - $1946.50


Andrew

*/

SELECT *
FROM ms_corona_guarantee
WHERE employee_name LIKE '%rogers%'
  OR employee_name LIKE '%ferry%'

UPDATE ms_corona_guarantee
SET guarantee = 2180
-- SELECT * FROM ms_corona_guarantee
WHERE employee_number = '1117901';

UPDATE ms_corona_guarantee
SET guarantee = 1946.50
-- SELECT * FROM ms_corona_guarantee
WHERE employee_number = '159863';

********************************************************************************
********************************************************************************

CREATE TABLE ms_corona_guarantee (
  employee_number cichar(9),
  employee_name cichar(60),
  guarantee double);
  
insert into ms_corona_guarantee values('152235','GIRODAT, JEFF S',3612.56);
insert into ms_corona_guarantee values('131370','DAVID, GORDON',1885.81);
insert into ms_corona_guarantee values('164015','HEFFERNAN, JOSH',3006.41);
insert into ms_corona_guarantee values('18005','AXTMAN, DALE M',3259.78);
insert into ms_corona_guarantee values('1106399','OLSON, JAY P',3508.79);
insert into ms_corona_guarantee values('136170','DUCKSTAD, JARED J',2761.49);
insert into ms_corona_guarantee values('1137590','THOMPSON, WYATT',2555.832);
insert into ms_corona_guarantee values('1146989','WALDBAUER, ALEX',1691.43);
insert into ms_corona_guarantee values('194675','MCVEIGH, DENNIS',2544.59);
insert into ms_corona_guarantee values('167580','HOLWERDA, KIRBY W',2590.813);
insert into ms_corona_guarantee values('152410','GRAY, NATHAN J',2497.51);
insert into ms_corona_guarantee values('1124436','SCHWAN, MICHAEL',3854.28);
insert into ms_corona_guarantee values('1135410','SYVERSON, JOSHUA L',2743.15);
insert into ms_corona_guarantee values('152525','GRANT, DESIREE L',1423.19);
insert into ms_corona_guarantee values('178050','KIEFER, LUCAS M',1565.11);
insert into ms_corona_guarantee values('1106421','O''NEIL, TIMOTHY J',2807.79);
insert into ms_corona_guarantee values('111210','BEAR, JEFFERY L',1982.02);
insert into ms_corona_guarantee values('150126','GEHRTZ, CLAYTON J',5343.25);
insert into ms_corona_guarantee values('13725','ANDERSON, SHAWN W',5217.71);
insert into ms_corona_guarantee values('145801','FLAAT, GAVIN',2672.81);
insert into ms_corona_guarantee values('159863','FERRY, CARTER',1231.95);
insert into ms_corona_guarantee values('168753','NORTHAGEN, JOSHUA',1962.23);
insert into ms_corona_guarantee values('1117960','ROGNE, CRAIG A',1794.87);
insert into ms_corona_guarantee values('1140870','TROFTGRUBEN, RODNEY',3232.69);
insert into ms_corona_guarantee values('141060','EULISS, NED',2345.56);
insert into ms_corona_guarantee values('187675','LONGORIA, BEVERLEY A',2597.96);
insert into ms_corona_guarantee values('140790','ESPELUND, KENNETH M',2664.62);
insert into ms_corona_guarantee values('1130700','STALLMO, RICHARD D',960.56);
insert into ms_corona_guarantee values('123525','CARLSON, KENNETH A',728.84);
insert into ms_corona_guarantee values('191060','MARO, NICHOLAS',2958.39);
insert into ms_corona_guarantee values('152499','GOTHBERG, CONNER J',1993.72);
insert into ms_corona_guarantee values('1117842','RODRIGUEZ, JUSTIN',2040.85);
insert into ms_corona_guarantee values('165732','KLOETY, HALEY',1223.49);
insert into ms_corona_guarantee values('115021','WOODS, SCOTT',985.87);
insert into ms_corona_guarantee values('169843','BOOKER, JEREMIAH',614.31);
insert into ms_corona_guarantee values('165823','BRODEN, MARK',952.68);
insert into ms_corona_guarantee values('172308','JANKOWSKI, GARRETT',2027.29);
insert into ms_corona_guarantee values('195628','EVENSON, JOSHUA',1487.92);
insert into ms_corona_guarantee values('159857','GONZALEZ, DANIEL',890.05);
insert into ms_corona_guarantee values('168575','ALOISIO, MADYSON',351.49);
insert into ms_corona_guarantee values('198567','SPIVA, NOLAN',911.11);
insert into ms_corona_guarantee values('198234','MELBY, BAILEY',817.47);
insert into ms_corona_guarantee values('153835','ELLINGSON, DANE',251.51);
insert into ms_corona_guarantee values('1113940','RADKE, DUANE',1810.43);
insert into ms_corona_guarantee values('116185','BOHM, DOUGLAS',1648.16);
insert into ms_corona_guarantee values('1117901','ROGERS, MITCH A',1868.773);
insert into ms_corona_guarantee values('1124400','SCHUPPERT, KODEY',1347.47);
insert into ms_corona_guarantee values('152964','SOBERG, NICHOLAS',1420.48);
insert into ms_corona_guarantee values('168573','AAMODT, MATTHEW',1427.37);
insert into ms_corona_guarantee values('1147062','WALIOR, SUSAN E',1687.19);
insert into ms_corona_guarantee values('157493','OTTO, ALYSSA',880.07);
  