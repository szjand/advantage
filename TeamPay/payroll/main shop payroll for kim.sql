/*
8/25
revised to include bonus info
AND rounding to agree with individual tech pay pages

9/29
Bonus, after mindfucking myself INTO a frenzy, ben clarified it 
total hours (pto + vac + clock) > 90

clock hours = 80
pto hours = 20
comm pay = 70 * commRate * teamProf
bonus pay = 10 * bonusRate * teamProf
pto pay = 20 * ptoRate

clock hours = 100
pto hours = 16
comm pay = 74 * commRate * teamProf
bonus pay = 26 * bonusRate * teamProf
pto pay = 16 * ptoRate

summary: 
IF total hours > 90
bonus hours = total hours - 90
commission hours = clock hours - bonus hours  
ELSE commision hours = clock hours

eg
  CASE WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
  round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
  
9/22/19 
  *a*
  after getting INTO how ken fucked up josh's paycheck for 8/18 -> 8/31 ( ken entered
        shop time AS vac hours, sheesh)
        josh noticed that on the ched for 9/1 -> 9/14, his pay page should 5 cents
        more than his paycheck, the difference, of course, was IN the difference
        IN rounding IN the 2 scripts, this script was incorrect
02/03/20
  *b* bonus only applies IF prof >= 100    
  
04/26: corona guarantee      
06/21: removed corona guarantee
11/08/20: ADD diagnostic team guarantee AND snap cell spiffs, need to first
		  populate TABLE tp_main_shop_guarantee_spiffs
11/23/20: ADD ot variance	
01/19/21: ALL the spiffs are gone, ADD just the guarantee for the blueprint team
		  more details IN "Main Shop payroll summary for managers.sql"  
*/
/**/
-- original query
DECLARE @department integer;
DECLARE @payRollEnd date; 
DECLARE @payRollStart date;
@department = 18;
@payRollStart = '12/19/2021';
@payRollEnd = '01/01/2022';
SELECT name AS [Name], [Emp#],[Comm Rate],[Comm Clock Hours],[Total Comm Pay],[Bonus Rate],
  [Bonus Hours],[Total Bonus Pay],[Vac PTO Hol Rate],[Vac PTO Hours],[Vac PTO Pay],
  [Hol Hours],[Hol Pay],[Total],guarantee AS [Guarantee], -- snap_cell AS [Snap Cell], ot_variance AS [OT Var],
  CASE
    WHEN guarantee IS NULL THEN [Total]
	WHEN guarantee > [Total] THEN guarantee
	ELSE [Total]
  END AS [Total Pay] 
/*  
  CASE
    WHEN guarantee IS NULL THEN [Total] + coalesce(snap_cell, 0) + coalesce(ot_variance, 0)
	ELSE guarantee + coalesce(snap_cell, 0) + coalesce(ot_variance, 0)
  END AS [Total Pay] 
*/  
FROM (  
  SELECT z.*, 
    [Total Comm Pay]+[Total Bonus Pay]+[Vac PTO Pay]+[Hol Pay] AS [Total],
    y.guarantee -- , y.snap_cell, y.ot_variance
  FROM (
    SELECT trim(x.lastname) + ', ' + trim(firstName) as Name, 
      x.employeenumber AS [Emp#],
      commRate AS [Comm Rate], 
      round(commHours * teamProf/100, 2) AS [Comm Clock Hours],
  -- *a*    
      -- round(round(commHours * teamProf/100, 2) * commRate, 2) AS [Total Comm Pay], round((commhours * teamprof/100) * commrate, 2)  AS total_comm_pay_2,
      round((commhours * teamprof/100) * commrate, 2)  AS [Total Comm Pay],
      bonusRate AS [Bonus Rate],
      round(bonusHours * teamProf/100, 2) AS [Bonus Hours],
  -- *a*    
      -- round(round(bonusHours * teamProf/100, 2) * bonusRate, 2) AS [Total Bonus Pay], round((bonushours * teamprof/100) * bonusrate, 2) AS bonus_pay_2, -- this now matches pay page
      round((bonushours * teamprof/100) * bonusrate, 2) AS [Total Bonus Pay],
      round(ptoRate, 2) AS [Vac PTO Hol Rate],
      vacationHours + ptoHours AS [Vac PTO Hours],
      round(ptoRate, 2) * (vacationHours + ptoHours) AS [Vac PTO Pay],
      holidayHours AS [Hol Hours],
      ptoRate * holidayHours AS [Hol Pay]
    FROM (  
    select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
      d.bonusRate, d.teamProf, d.flagHours, e.clockHours, e.vacationHours, e.ptoHours, e.holidayHours, 
      e.totalHours,
  -- *b*
      CASE WHEN e.totalHours > 90 and TeamProf >= 100 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
      round(CASE WHEN totalHours > 90 and TeamProf >= 100 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
    FROM (
      select c.teamname AS Team, lastname, firstname, employeenumber, 
        techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
        2 * round(techtfrrate, 2) AS BonusRate, teamprofpptd AS teamProf, 
        TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf  
      FROM tpdata a
      INNER JOIN tpteamtechs b on a.techkey = b.techkey
        AND a.teamkey = b.teamkey
        -- AND b.thrudate > curdate()
        AND b.thrudate >= @payRollEnd
      LEFT JOIN tpTeams c on b.teamKey = c.teamKey
      WHERE thedate = @payRollEnd
        AND a.departmentKey = @department) d
    LEFT JOIN (
      select employeenumber, techclockhourspptd AS clockHours, 
        techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
        techholidayhourspptd AS holidayHours,
        techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
      FROM tpdata a
      INNER JOIN tpteamtechs b on a.techkey = b.techkey
        AND a.teamkey = b.teamkey
        -- AND b.thrudate > curdate()
        AND b.thrudate >= @payRollEnd
      LEFT JOIN tpTeams c on b.teamKey = c.teamKey
      WHERE thedate = @payRollEnd
        AND a.departmentKey = @department) e on d.employeenumber = e.employeenumber) x) z
		
  LEFT JOIN ( -- guarantee for the blueprint team
    SELECT employee_number, reg + ot + pto + bonus AS guarantee
    FROM (
      SELECT aa.name, aa.employee_number, round(aa.hourly_rate * bb.clock, 2) AS reg, 
        round(aa.hourly_rate * 1.5 * bb.ot, 2) AS ot, 
        round(aa.pto_rate * (vac + pto + hol), 2) AS pto,
        case
          when bb.clock + bb.ot + bb.pto > 90 THEN  ((bb.clock + bb.ot + bb.pto) - 90) * 2 * aa.hourly_rate
      	ELSE 0
        END AS bonus
      FROM blueprint_guarantee_rates aa
      JOIN (			 
        select c.name, c.employeenumber, SUM(a.regularhours) AS clock, SUM(a.overtimehours) AS ot,
          SUM(a.vacationhours) AS vac, SUM(a.ptohours) AS pto, SUM(a.holidayhours) AS hol 
        FROM dds.edwclockhoursfact a
        JOIN dds.day b on a.datekey = b.datekey
          AND b.thedate BETWEEN @payRollStart AND @payRollEnd
        JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
          AND c.employeenumber IN ('145801','1106399','1117901','152410')
        GROUP BY c.name, c.employeenumber) bb on aa.employee_number = bb.employeenumber
      WHERE aa.thru_date = '12/31/9999') cc) y on z.[Emp#] = y.employee_number) zz	
/*	  	
  LEFT JOIN tp_main_shop_guarantee_spiffs y on z.[Emp#] = y.employee_number	
    AND y.the_date = @payRollEnd) zz   
*/	
ORDER BY name
/**/

/*  corona
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 18;
@payRollEnd = '06/20/2020'; 
SELECT z.*, 
  [Total Comm Pay]+[Total Bonus Pay]+[Vac PTO Pay]+[Hol Pay] AS [Total Earned], aa.guarantee AS Guarantee,
  CASE
    WHEN aa.guarantee > [Total Comm Pay]+[Total Bonus Pay]+[Vac PTO Pay]+[Hol Pay] THEN aa.guarantee
    ELSE [Total Comm Pay]+[Total Bonus Pay]+[Vac PTO Pay]+[Hol Pay]
  END AS [Actual Pay]
  FROM (
    SELECT trim(x.lastname) + ', ' + trim(firstName) as Name, 
      x.employeenumber AS [Emp#],
      commRate AS [Comm Rate], 
      round(commHours * teamProf/100, 2) AS [Comm Clock Hours],
  -- *a*    
      -- round(round(commHours * teamProf/100, 2) * commRate, 2) AS [Total Comm Pay], round((commhours * teamprof/100) * commrate, 2)  AS total_comm_pay_2,
      round((commhours * teamprof/100) * commrate, 2)  AS [Total Comm Pay],
      bonusRate AS [Bonus Rate],
      round(bonusHours * teamProf/100, 2) AS [Bonus Hours],
  -- *a*    
      -- round(round(bonusHours * teamProf/100, 2) * bonusRate, 2) AS [Total Bonus Pay], round((bonushours * teamprof/100) * bonusrate, 2) AS bonus_pay_2, -- this now matches pay page
      round((bonushours * teamprof/100) * bonusrate, 2) AS [Total Bonus Pay],
      round(ptoRate, 2) AS [Vac PTO Hol Rate],
      vacationHours + ptoHours AS [Vac PTO Hours],
      round(ptoRate, 2) * (vacationHours + ptoHours) AS [Vac PTO Pay],
      holidayHours AS [Hol Hours],
      ptoRate * holidayHours AS [Hol Pay]
    FROM (  
      select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
        d.bonusRate, d.teamProf, d.flagHours, e.clockHours, e.vacationHours, e.ptoHours, e.holidayHours, 
        e.totalHours,
    -- *b*
        CASE WHEN e.totalHours > 90 and TeamProf >= 100 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
        round(CASE WHEN totalHours > 90 and TeamProf >= 100 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
      FROM (
        select c.teamname AS Team, lastname, firstname, employeenumber, 
          techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
          2 * round(techtfrrate, 2) AS BonusRate, teamprofpptd AS teamProf, 
          TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf  
        FROM tpdata a
        INNER JOIN tpteamtechs b on a.techkey = b.techkey
          AND a.teamkey = b.teamkey
          -- AND b.thrudate > curdate()
          AND b.thrudate >= @payRollEnd
        LEFT JOIN tpTeams c on b.teamKey = c.teamKey
        WHERE thedate = @payRollEnd
          AND a.departmentKey = @department) d
    LEFT JOIN (
      select employeenumber, techclockhourspptd AS clockHours, 
        techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
        techholidayhourspptd AS holidayHours,
        techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
      FROM tpdata a
      INNER JOIN tpteamtechs b on a.techkey = b.techkey
        AND a.teamkey = b.teamkey
        -- AND b.thrudate > curdate()
        AND b.thrudate >= @payRollEnd
      LEFT JOIN tpTeams c on b.teamKey = c.teamKey
      WHERE thedate = @payRollEnd
        AND a.departmentKey = @department) e on d.employeenumber = e.employeenumber) x) z
  LEFT JOIN ms_corona_guarantee aa on z.[Emp#] = aa.employee_number      
  WHERE z.[Emp#] <> '1149300'  
ORDER BY name
*/