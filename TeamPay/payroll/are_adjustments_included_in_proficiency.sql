/*
FROM ken espelund 11/15/19
Can you review team Gray's pay role they are off a bit.  Gordy shows $1848.85, paid out $1844.74 short $4.11, Nate 2375.18 paid out 2369.89 short $5.29, Wyatt 2380.26 paid out $2374.97 short $5.29, Zach  $2391.76 paid out
$2386.45 short $5.31.   

answered with
The issue is that their pages in vision today show a team proficiency of 112.43% for that pay period.
At the time payroll was generated, the team proficiency was 112.18%, as you can see on the spreadsheet that I sent to you at payroll time.
*/

which IS true, but
there IS more going on

i think the vision page IS including adjustments IN proficiency calculation
but the spreadsheet queries are NOT

added a TABLE tpdata_tracking, populated at END of xfmTpData

i dont really know what IS going on
too tired today

DO adjustments get figured INTO teamprofpptd, looks LIKE it

on the main shop flat rate summary spreadsheet
total flag hours: 399.1
total clock hours: 356.49
SELECT 399.1/356.49 FROM system.iota -- 1.1195

the flag hour diff betw the spreadsheet AND the vision page today
flag hours  spreadsheet     vision
gordon         93.1          94.0
winkler       127.6         128.4
total         399.1         400.8
team shows +2.3 adjustment, dont know WHERE OR IF that IS included          

SELECT thedate, teamprofpptd, teamclockhourspptd, teamflaghourspptd, teamflaghouradjustmentspptd,
  case
    when teamclockhourspptd = 0 then 0
    else round(teamflaghourspptd/teamclockhourspptd, 5)
  END,
  case
    when teamclockhourspptd = 0 then 0
    else round((teamflaghourspptd + teamflaghouradjustmentspptd)/teamclockhourspptd, 5)
  end  
-- SELECT *
FROM tpdata
WHERE employeenumber = '131370'
  AND thedate BETWEEN '10/27/2019' AND '11/09/2019'
  
SELECT 398.5/356.49 FROM system.iota  -- 1.1178

SELECT 400.8/356.49 FROM system.iota  -- 1.1242

the difference IS because, that at the time payroll was generated, the proficiency
for the team (AS shown on the spreadsheet i sent to you) was 112.18
so


SELECT *
FROM tpdata_tracking
WHERE thedate = curdate();

IN vision, both team display AND individual display, both individual AND
team flaghours include adjustments, team proficiency also includes adjustments

on the payroll summary for managers spreadsheet
the flaghours COLUMN does NOT include adjustments, but the team prof does include
adjustments
on the spreadsheet, adjustments dont show anywhere, so it can be deceptive
calculating prof FROM the displayed flag/clock hours will NOT match the
team prof value IF there are any adjustments

most straightforward fix for this would be to ADD an adjustments & total 
flag hours COLUMN, i believe, 
the spreadsheet will now have ALL figures necessary to match vision

SELECT thedate, teamprofpptd, teamclockhourspptd, teamflaghourspptd, teamflaghouradjustmentspptd,
  case
    when teamclockhourspptd = 0 then 0
    else round(teamflaghourspptd/teamclockhourspptd, 5) 
  END AS prof_without_adj,
  case
    when teamclockhourspptd = 0 then 0
    else round((teamflaghourspptd + teamflaghouradjustmentspptd)/teamclockhourspptd, 5)
  end AS prof_with_adj 
-- SELECT *
FROM tpdata
WHERE employeenumber = '131370'
  AND thedate BETWEEN '11/10/2019' AND curdate()
  AND teamname = 'gray'
  
  
need to come up with a post pay period change detection query USING
tpdata_tracking  