DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 13;
@payRollEnd = '01/01/2022';	  
    select c.teamname AS Team, lastname, firstname, employeenumber AS "emp#", 
			round(techtfrrate, 4) AS flat_rate, 
      techclockhourspptd AS clock_hours, TechFlagHoursPPTD as flag_hours, teamprofpptd AS team_prof, 
      round(techclockhourspptd*TeamProfPPTD/100, 4) as commission_hours, 
			round(techtfrrate * TeamProfPPTD * techclockhourspptd/100, 2)  AS commission_pay
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamkey = b.teamkey
      AND b.thruDate >= @payrollEnd
    LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    WHERE thedate = @payRollEnd
      AND a.departmentKey = @department 
			and lastname <> 'mavity'
			order by team