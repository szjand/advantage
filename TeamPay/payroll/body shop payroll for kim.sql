/*
10/7
  *a* hourly rate change for brian peterson to have gone INTO effect 9/21, start
      of payperiod, actual change IN payroll was NOT done until 9/25
      changed to just return the current ro FROM edwEmployeeDim 
?? why am i NOT USING bsFlatRateTechs AND bsFlatRateData ??   
*b*
     AND AS of 3/21/15 this IS an issue because of changing otherRate (PTO)
     AND the fact that IN edwEmployeeDim they are ALL commission now AND show
     no hour rate   
11/2/15: back to separate teams, pdr separate     
10/28/17: ADD guarantee per Randy, wetch only
04/26/20: ADD corona guarantee
i believe it will be easiest to break brian peterson out INTO a separate query for this
07/07/20: remove corona guarantee
02/16/21: added gardners adjustments (looks LIKE snap cell spiffs)
*/

/**/

DECLARE @thruDate date;
DECLARE @fromDate date;
DECLARE @pdr_rate double;
DECLARE @metal_rate double;
DECLARE @hourly_rate double;
@pdr_rate = (SELECT pdrrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@metal_rate = (SELECT metalrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@hourly_rate = (SELECT otherrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@fromDate = '12/05/2021'; -- beginning of payroll for figuring pdr flat rate shit
@thruDate = '12/18/2021'; -- last day of the pay period
SELECT Name,[Emp#],[Comm Rate],[Clock Hours],[Total Comm Pay],
  [Vac PTO Hol Rate],[Vac PTO Hours],[Vac PTO Pay],[Hol Hours],[Hol Pay], [Adj], [Total Gross Pay]  
--  CASE 
--    WHEN name = 'Wetch, Anthony' AND actual_clock_hours > 89.99 AND [Total Gross Pay] < 2565 THEN '   2565.00'
--    ELSE '    N/A'
--  END AS Guarantee
FROM (
  SELECT clockhours AS actual_clock_hours,
    trim(lastname) + ', ' + trim(firstname) AS Name, employeenumber AS [Emp#], 
    round(commrate, 2) AS [Comm Rate],
    round(teamprof*clockhours/100, 2) AS [Clock Hours],
    round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2) AS [Total Comm Pay],
    HourlyRate AS [Vac PTO Hol Rate],
    vacationhours + ptohours AS [Vac PTO Hours],
    round(hourlyrate * (vacationhours + ptohours), 2) AS [Vac PTO Pay],
    HolidayHours AS [Hol Hours],
    round(hourlyrate * holidayhours, 2) AS [Hol Pay],
	a.adjustment AS [Adj],
    round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2)
        + round(hourlyrate * holidayhours, 2)
        + round(hourlyrate * (vacationhours + ptohours), 2) 
		+ a.adjustment AS [Total Gross Pay]    
  FROM (
    select lastname, firstname, employeenumber,  
      techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
      techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
      techvacationhourspptd AS VacationHours,
      techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
      round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
        (techvacationhourspptd + techptohourspptd + 
          techholidayhourspptd)*techhourlyrate AS CommPay,
       round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
         techholidayhourspptd)*techhourlyrate, 2) AS TransitionPay, c.adjustment
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey
      AND b.thrudate >= @thruDate
	LEFT JOIN bs_adjustments c on a.employeenumber = c.employee_number
	  AND payroll_end_date = @thruDate	  
    WHERE thedate = @thruDate
      AND departmentKey = 13) a) x
ORDER BY name    

/*
effect 09/26/21 Brian Peterson IS no longer paid on 2 different rates, but
a single rate only
therefor this IS no longer needed

UNION

SELECT p.name, p.employeenumber, n.pdrRate, n.pdrFlagHours, n.pdrPay, 
  p.hourlyRate, o.vacationHours,
  round(p.hourlyRate * (o.vacationHours + o.ptoHours), 2) AS vacationPay, 
  o.holidayHours, round(p.hourlyRate * holidayHours, 2) AS holidayPay, 
  0 AS adj,
   n.pdrPay + round(p.hourlyRate * (o.vacationHours + o.ptoHours), 2) 
     + round(p.hourlyRate * holidayHours, 2) AS total -- '    N/A' AS guarantee
FROM ( 
  SELECT pdrRate, round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay
  FROM (  
    SELECT @pdr_rate AS pdrRate, 
      coalesce(flaghours, 0) AS pdrFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.closedatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND c.opCode = 'PDR'
      AND b.theDate BETWEEN @fromDate AND @thruDate) m
  GROUP BY pdrRate) n      
LEFT JOIN (
  SELECT SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
    sum(holidayHours) AS holidayHours
      FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.theDate BETWEEN @fromDate AND @thruDate) o  on 1 = 1  
LEFT JOIN (    
  SELECT trim(lastname) + ', ' + firstname AS Name, employeenumber, 
    @hourly_rate AS hourlyrate  
  FROM dds.edwEmployeeDim a
  WHERE lastname = 'peterson' 
    AND firstname = 'brian'   
-- *a*    
    AND currentrow = true) p on 1 = 1    
--    AND employeeKeyFromDate < @fromDate
--    AND employeeKeyThruDate > @thruDate) p on 1 = 1
UNION
SELECT p.name, p.employeenumber, n.metalRate, n.metalFlagHours, n.metalPay, 
  0,0,0,0,0,0 AS adj, n.metalPay -- '    N/A'
FROM ( 
  SELECT metalRate, round(SUM(metalFlagHours),2) AS metalFlagHours, 
    round(metalRate * round(SUM(metalFlagHours),2), 2) AS metalPay
  FROM (  
    SELECT @metal_rate AS metalRate, 
      coalesce(flaghours, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.closedatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND c.opCode <> 'PDR'
      AND b.theDate BETWEEN @fromDate AND @thruDate) m
  GROUP BY metalRate) n      
LEFT JOIN (    
  SELECT trim(lastname) + ', ' + firstname AS Name, employeenumber, hourlyRate 
  FROM dds.edwEmployeeDim a
  WHERE lastname = 'peterson' 
    AND firstname = 'brian'   
-- *a*    
    AND currentrow = true) p on 1 = 1    
--    AND employeeKeyFromDate < @fromDate
--    AND employeeKeyThruDate > @thruDate) p on 1 = 1   
/**/


/* corona queries
-- 04/26 ALL except brian
-- looks good
DECLARE @thruDate date;
DECLARE @fromDate date;
DECLARE @pdr_rate double;
DECLARE @metal_rate double;
DECLARE @hourly_rate double;
@pdr_rate = (SELECT pdrrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@metal_rate = (SELECT metalrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@hourly_rate = (SELECT otherrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@fromDate = '06/07/2020'; -- beginning of payroll for figuring pdr flat rate shit
@thruDate = '06/20/2020'; -- last day of the pay period

SELECT Name,[Emp#],[Comm Rate],[Clock Hours],[Total Comm Pay],
  [Vac PTO Hol Rate],[Vac PTO Hours],[Vac PTO Pay],[Hol Hours],[Hol Pay],[Earned Pay],  
  (SELECT guarantee FROM bs_corona_guarantee WHERE employee_number = x.[Emp#]) AS Guarantee,
  CASE 
    WHEN (SELECT guarantee FROM bs_corona_guarantee WHERE employee_number = x.[Emp#])
		 > x.[Earned Pay] THEN (SELECT guarantee FROM bs_corona_guarantee WHERE employee_number = x.[Emp#])
    else x.[Earned Pay]
  END AS [Actual Pay]
FROM (
  SELECT clockhours AS actual_clock_hours,
    trim(lastname) + ', ' + trim(firstname) AS Name, employeenumber AS [Emp#], 
    round(commrate, 2) AS [Comm Rate],
    round(teamprof*clockhours/100, 2) AS [Clock Hours],
    round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2) AS [Total Comm Pay],
    HourlyRate AS [Vac PTO Hol Rate],
    vacationhours + ptohours AS [Vac PTO Hours],
    round(hourlyrate * (vacationhours + ptohours), 2) AS [Vac PTO Pay],
    HolidayHours AS [Hol Hours],
    round(hourlyrate * holidayhours, 2) AS [Hol Pay],
    round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2)
        + round(hourlyrate * holidayhours, 2)
        + round(hourlyrate * (vacationhours + ptohours), 2) AS [Earned Pay]    
  FROM (
    select lastname, firstname, employeenumber,  
      techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
      techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
      techvacationhourspptd AS VacationHours,
      techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
      round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
        (techvacationhourspptd + techptohourspptd + 
          techholidayhourspptd)*techhourlyrate AS CommPay,
       round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
         techholidayhourspptd)*techhourlyrate, 2) AS TransitionPay
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey
      AND b.thrudate >= @thruDate
    WHERE thedate = @thruDate
      AND departmentKey = 13) a) x
ORDER BY name    

********************************************************************************
********************************************************************************

-- 04/26 Brian only
-- this IS hackey AS hell, but good enough for now

DECLARE @thruDate date;
DECLARE @fromDate date;
DECLARE @pdr_rate double;
DECLARE @metal_rate double;
DECLARE @hourly_rate double;
DECLARE @employee_number string;
DECLARE @guarantee double;

@employee_number = '1110425';
@guarantee = (SELECT guarantee FROM bs_corona_guarantee WHERE employee_number = '1110425');
@pdr_rate = (SELECT pdrrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@metal_rate = (SELECT metalrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@hourly_rate = (SELECT otherrate FROM bsFlatRateTechs WHERE thrudate > curdate());
@fromDate = '06/07/2020'; -- beginning of payroll for figuring pdr flat rate shit
@thruDate = '06/20/2020'; -- last day of the pay period
-- DROP TABLE #wtf;
SELECT *
INTO #wtf
FROM (
  SELECT p.name, p.employeenumber, n.pdrRate, n.pdrFlagHours, n.pdrPay, 
    p.hourlyRate, o.vacationHours,
    round(p.hourlyRate * (o.vacationHours + o.ptoHours), 2) AS vacationPay, 
    o.holidayHours, round(p.hourlyRate * holidayHours, 2) AS holidayPay, 
     n.pdrPay + round(p.hourlyRate * (o.vacationHours + o.ptoHours), 2) 
       + round(p.hourlyRate * holidayHours, 2) AS total, @guarantee AS Guarantee
  FROM ( 
    SELECT pdrRate, round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
      round(pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay
    FROM (  
      SELECT @pdr_rate AS pdrRate, 
        coalesce(flaghours, 0) AS pdrFlagHours
      FROM dds.factRepairOrder a
      INNER JOIN dds.day b on a.closedatekey = b.datekey
      INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
      INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
      INNER JOIN dds.dimTech g on a.techKey = g.techKey
      WHERE g.technumber = '213'
        AND c.opCode = 'PDR'
        AND b.theDate BETWEEN @fromDate AND @thruDate) m
    GROUP BY pdrRate) n      
  LEFT JOIN (
    SELECT SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
      sum(holidayHours) AS holidayHours
        FROM dds.edwClockHoursFact a
    INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
    INNER JOIN dds.dimTech c on b.storecode = c.storecode
      AND b.employeenumber = c.employeenumber
      AND c.currentrow = true
    INNER JOIN dds.day d on a.datekey = d.datekey
    WHERE c.storecode = 'ry1'
      AND c.technumber = '213'
      AND d.theDate BETWEEN @fromDate AND @thruDate) o  on 1 = 1  
  LEFT JOIN (    
    SELECT trim(lastname) + ', ' + firstname AS Name, employeenumber, 
      @hourly_rate AS hourlyrate  
    FROM dds.edwEmployeeDim a
    WHERE lastname = 'peterson' 
      AND firstname = 'brian'   
  -- *a*    
      AND currentrow = true) p on 1 = 1    
  --    AND employeeKeyFromDate < @fromDate
  --    AND employeeKeyThruDate > @thruDate) p on 1 = 1
  UNION
  SELECT p.name, p.employeenumber, n.metalRate, n.metalFlagHours, n.metalPay, 
    0,0,0,0,0, n.metalPay, @guarantee AS Guarantee
  FROM ( 
    SELECT metalRate, round(SUM(metalFlagHours),2) AS metalFlagHours, 
      round(metalRate * round(SUM(metalFlagHours),2), 2) AS metalPay
    FROM (  
      SELECT @metal_rate AS metalRate, 
        coalesce(flaghours, 0) AS metalFlagHours
      FROM dds.factRepairOrder a
      INNER JOIN dds.day b on a.closedatekey = b.datekey
      INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
      INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
      INNER JOIN dds.dimTech g on a.techKey = g.techKey
      WHERE g.technumber = '213'
        AND c.opCode <> 'PDR'
        AND b.theDate BETWEEN @fromDate AND @thruDate) m
    GROUP BY metalRate) n      
  LEFT JOIN (    
    SELECT trim(lastname) + ', ' + firstname AS Name, employeenumber, hourlyRate 
    FROM dds.edwEmployeeDim a
    WHERE lastname = 'peterson' 
      AND firstname = 'brian'   
  -- *a*    
      AND currentrow = true) p on 1 = 1) aa;
	  
select * FROM #wtf a
union	  
select '','',0,0,0,0,0,0,0,0,SUM(total), avg(Guarantee)
FROM #wtf  
GROUP BY employeenumber
ORDER BY name DESC
*/