/*
12/30/13
  pay period ended 12/28
  Kim needs payroll BY tomorrow
  Bev catches up on batching of timeclock entries (holiday, pto, vacation) 
    AND shop/training/fence time that has NOT yet been entered for the pay period
so, to get ALL the timeclock data entered
had to run:
first disable:
 todayTpData
 timeCockHourly
 todayBsFlatRateData
THEN run: 
   extPYPCLOCKIN    
   factClockHours
   tpData
   todayTpData
re-enable realtime stuff   
-- 3/11
       need to distinguish BETWEEN main shop AND body shop   
*/
-- huots sheet
-- ADD the rounding same AS payroll
-- 1/26 uh oh, longoria team showing up AND it should NOT
-- INNER JOIN to tpTeamTechs on techKey & teamKey, use LEFT JOIN to tpTeams for name
select x.*, round(TotalCommPay - transition, 2) AS [Comm - Transition]
FROM (
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS HourlyRate, round(techtfrrate, 2) AS CommRate, 
    techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
    round(techclockhourspptd*TeamProfPPTD/100, 2) as CommHours, 
    round(round(techtfrrate, 2)*round(TeamProfPPTD*techclockhourspptd/100, 2), 2) AS CommPay,
    techvacationhourspptd AS VacationHours,
    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
--    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
    round(round(techtfrrate, 2) * round(techclockhourspptd * teamprofpptd/100, 2), 2) + 
      (techvacationhourspptd + techptohourspptd + 
        techholidayhourspptd)*techhourlyrate AS TotalCommPay,
     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
       techholidayhourspptd)*techhourlyrate, 2) AS Transition
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
--  INNER JOIN tpteams c on b.teamkey = c.teamkey
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = '03/22/2014'
    AND a.departmentKey = 18
  union
  select c.teamname AS Team, left(cast(NULL AS sql_char), 1),
    left(cast(NULL AS sql_char), 1),left(cast(NULL AS sql_char), 1),
    CAST(NULL AS sql_integer),CAST(NULL AS sql_integer),
    sum(techclockhourspptd) AS ClockHours, sum(TechFlagHoursPPTD) as FlagHours, 
    max(teamprofpptd) AS TeamProf, 
    CAST(NULL AS sql_integer), CAST(NULL AS sql_integer),
    sum(techvacationhourspptd) AS VacationHours,
    sum(techptohourspptd) AS PTOHours, sum(techholidayhourspptd) AS HolidayHours,
    CAST(NULL AS sql_integer),CAST(NULL AS sql_integer)
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
--  INNER JOIN tpteams c on b.teamkey = c.teamkey
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = '03/22/2014'
    AND a.departmentKey = 18
  GROUP BY c.TeamName) x

-- ro audit list
SELECT c.technumber, ro, sum(a.flaghours) AS flaghours
FROM dds.factRepairOrder a  
INNER JOIN dds.dimTech c on a.techkey = c.techkey
INNER JOIN dds.day d on a.flagdatekey = d.datekey
WHERE flaghours <> 0
  AND a.storecode = 'ry1'
  AND a.flagdatekey IN (
    SELECT datekey 
    FROM dds.day
    WHERE biweeklypayperiodsequence = (
      SELECT biweeklypayperiodsequence
      FROM dds.day
      WHERE thedate = curdate()))
  AND c.technumber IN (
    SELECT technumber
    FROM tpTechs)
GROUP BY c.technumber, ro    
UNION
SELECT c.technumber, 'Total', sum(a.flaghours) AS flaghours
FROM dds.factRepairOrder a  
INNER JOIN dds.dimTech c on a.techkey = c.techkey
INNER JOIN dds.day d on a.flagdatekey = d.datekey
WHERE flaghours <> 0
  AND a.storecode = 'ry1'
  AND a.flagdatekey IN (
    SELECT datekey 
    FROM dds.day
    WHERE biweeklypayperiodsequence = (
      SELECT biweeklypayperiodsequence
      FROM dds.day
      WHERE thedate = curdate()))
  AND c.technumber IN (
    SELECT technumber
    FROM tpTechs)
GROUP BY c.technumber

SELECT *
FROM dds.stgArkonaSDPXTIM

-- sent the results of this to kim
SELECT trim(lastname) + ', ' + trim(firstname) AS Name, employeenumber AS [Employee Number], 
  CASE WHEN commpay >= transitionpay THEN commrate ELSE hourlyrate END AS [Commission Rate],
  CASE WHEN commpay >= transitionpay
    THEN round(teamprof*clockhours/100, 2)
    ELSE clockhours
  END AS [Clock Hours],
  CASE WHEN commpay >= transitionpay
    THEN round(commrate*teamprof*clockhours/100, 2)
    ELSE round(hourlyrate*clockhours, 2)
  END AS [Total Commission Pay],
  HourlyRate AS [Vac PTO Hol Rate],
  vacationhours + ptohours AS [Vacation PTO Hours],
  round(hourlyrate * (vacationhours + ptohours), 2) AS [Vacation PTO Pay],
  HolidayHours AS [Holiday Hours],
  round(hourlyrate * holidayhours, 2) AS [Holiday Pay],
  CASE WHEN commpay >= transitionpay
    THEN round(commrate*teamprof*clockhours/100, 2) 
      + round(hourlyrate * holidayhours, 2)
      + round(hourlyrate * (vacationhours + ptohours), 2)
    ELSE round(hourlyrate*clockhours, 2)
      + round(hourlyrate * holidayhours, 2)
      + round(hourlyrate * (vacationhours + ptohours), 2)
  END AS [Total Gross Pay]            
FROM (
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
    techvacationhourspptd AS VacationHours,
    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
      (techvacationhourspptd + techptohourspptd + 
        techholidayhourspptd)*techhourlyrate AS CommPay,
     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
       techholidayhourspptd)*techhourlyrate, 2) AS TransitionPay
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
  INNER JOIN tpteams c on b.teamkey = c.teamkey
  WHERE thedate = '01/11/2014'
    AND a.departmentKey = 18) a
ORDER BY name  


-- 1/13/14  

-- sent the results of this to kim
-- 1. round commission rate
-- 2. round clockhours*teamprof
-- 1/26 
-- 3 returning Longoria team techs twice, same AS above with JOIN to tpTeamTechs
-- 		DO NOT need teamName
SELECT trim(lastname) + ', ' + trim(firstname) AS Name, employeenumber AS [Employee Number], 
--  CASE WHEN commpay >= transitionpay THEN commrate ELSE hourlyrate END AS [Commission Rate],
-- 1
  CASE WHEN commpay >= transitionpay THEN round(commrate, 2) ELSE hourlyrate END AS [Commission Rate],
  CASE WHEN commpay >= transitionpay
    THEN round(teamprof*clockhours/100, 2)
    ELSE clockhours
  END AS [Clock Hours],
  CASE WHEN commpay >= transitionpay
--    THEN round(commrate*teamprof*clockhours/100, 2)
-- 1, 2
    THEN round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2)
    ELSE round(hourlyrate*clockhours, 2)
  END AS [Total Commission Pay],
  HourlyRate AS [Vac PTO Hol Rate],
  vacationhours + ptohours AS [Vacation PTO Hours],
  round(hourlyrate * (vacationhours + ptohours), 2) AS [Vacation PTO Pay],
  HolidayHours AS [Holiday Hours],
  round(hourlyrate * holidayhours, 2) AS [Holiday Pay],
  CASE WHEN commpay >= transitionpay
--    THEN round(commrate*teamprof*clockhours/100, 2) 
-- 1, 2
    THEN round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2)
      + round(hourlyrate * holidayhours, 2)
      + round(hourlyrate * (vacationhours + ptohours), 2)
    ELSE round(hourlyrate*clockhours, 2)
      + round(hourlyrate * holidayhours, 2)
      + round(hourlyrate * (vacationhours + ptohours), 2)
  END AS [Total Gross Pay]    
FROM (
-- 3
--  select c.teamname AS Team, lastname, firstname, employeenumber, 
  select lastname, firstname, employeenumber,  
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
    techvacationhourspptd AS VacationHours,
    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
      (techvacationhourspptd + techptohourspptd + 
        techholidayhourspptd)*techhourlyrate AS CommPay,
     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
       techholidayhourspptd)*techhourlyrate, 2) AS TransitionPay
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
-- 3  
    AND a.teamKey = b.teamkey
--  INNER JOIN tpteams c on b.teamkey = c.teamkey
  WHERE thedate = '02/08/2014') a
ORDER BY name  



-- send the results of this to kim
-- 2/24 AS of last pay period (ending 2/22) no more transition pay
-- 		refactor to return only commission info
--		AND now need to know for which dept

SELECT trim(lastname) + ', ' + trim(firstname) AS Name, employeenumber AS [Employee Number], 
  round(commrate, 2) AS [Commission Rate],
  round(teamprof*clockhours/100, 2) AS [Clock Hours],
  round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2) AS [Total Commission Pay],
  HourlyRate AS [Vac PTO Hol Rate],
  vacationhours + ptohours AS [Vacation PTO Hours],
  round(hourlyrate * (vacationhours + ptohours), 2) AS [Vacation PTO Pay],
  HolidayHours AS [Holiday Hours],
  round(hourlyrate * holidayhours, 2) AS [Holiday Pay],
  round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2)
      + round(hourlyrate * holidayhours, 2)
      + round(hourlyrate * (vacationhours + ptohours), 2) AS [Total Gross Pay]    
FROM (
  select lastname, firstname, employeenumber,  
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf, 
    techvacationhourspptd AS VacationHours,
    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) + 
      (techvacationhourspptd + techptohourspptd + 
        techholidayhourspptd)*techhourlyrate AS CommPay,
     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
       techholidayhourspptd)*techhourlyrate, 2) AS TransitionPay
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = '03/22/2014'
    AND departmentKey = 18) a
ORDER BY name  