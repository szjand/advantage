﻿
clock hours: select 86.83 + 68.43 + 78.03 = 233.29
flag hours: select 153.4 + 153.1 + 115.9 = 422.4
      prof: select 422.4/233.29 = 181.06
commission pay: prof * flat rate * comm hours
      mcveigh: select 1.8106 * 20 * 82 = 2969.38
      girodat: select 1.8106 * 32 * 66 = 3823.99
      oneil: select 1.8106 * 22.48 * 74 = 3011.97
bonus pay: prof * bonus rate * bonus hours
      mcveigh: select 1.8106 * 40 * 4.83 = 349.81
      girodat: select 1.8106 * 64 * 2.43 = 281.58
      oneil: select 1.8106 * 44.96 * 4.03 = 328.06
total pay: comm pay + bonus pay +  pto pay
      mcveigh: select 2969.38 + 349.81 + 232.64 = 3551.83
      girodat: select 3823.99 + 281.58 + 1140.24 = 5245.81
      oneil: select 3011.97 + 328.06 + 518.24 = 3858.27

flag hours: select 153.4 + 153.1 + 115.9 - 63 = 359.4
      prof: select 359.4/233.29 = 154.06
commission pay: prof * flat rate * comm hours
      mcveigh: select 1.5406 * 20 * 82 = 2526.58
      girodat: select 1.5406 * 32 * 66 = 3253.75
      oneil: select 1.5406 * 22.48 * 74 = 2562.82
bonus pay: prof * bonus rate * bonus hours
      mcveigh: select 1.5406 * 40 * 4.83 = 297.64
      girodat: select 1.5406 * 64 * 2.43 = 239.59
      oneil: select 1.5406 * 44.96 * 4.03 = 279.14
total pay: comm pay + bonus pay +  pto pay      
      mcveigh: select 2526.58 + 297.64 + 232.64 = 3056.86
      girodat: select 3253.75 + 239.59 + 1140.24 = 4633.58
      oneil: select 2562.82 + 279.14 + 518.24 = 3360.20
