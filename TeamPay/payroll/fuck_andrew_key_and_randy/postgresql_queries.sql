﻿select *
from ads.ext_tpdata

alter table ads.ext_tpdata
rename to ext_tp_data

-- flag hours
-- matches
select c.teamname AS Team, lastname, firstname, employeenumber, 
  techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
  2 * round(techtfrrate, 2) AS BonusRate, -- teamprofpptd AS teamProf, 
  TechFlagHoursPPTD as FlagHours, techFlagHourAdjustmentsPPTD as Adjustments,
  teamprofpptd AS TeamProf   
FROM ads.ext_tp_data a
INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
  AND a.teamkey = b.teamkey
  AND b.thrudate > current_date
LEFT JOIN ads.ext_tp_teams c on b.teamKey = c.teamKey
WHERE thedate = '07/18/2020'
  AND a.departmentKey = 18
order by team, firstname  

-- flag hours 7/5 - 7/11
select c.teamname AS Team, lastname, firstname, employeenumber, 
  techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
  2 * round(techtfrrate, 2) AS BonusRate, -- teamprofpptd AS teamProf, 
  TechFlagHoursPPTD as FlagHours, techFlagHourAdjustmentsPPTD as Adjustments,
  teamprofpptd AS TeamProf   
FROM ads.ext_tp_data a
INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
  AND a.teamkey = b.teamkey
  AND b.thrudate > current_date
LEFT JOIN ads.ext_tp_teams c on b.teamKey = c.teamKey
WHERE thedate = '07/11/2020'
  AND a.departmentKey = 18
order by team, firstname  

-- flag hours 7/12 - 7/18
-- pptd won't work for this
select c.teamname AS Team, lastname, firstname, employeenumber, 
  techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
  2 * round(techtfrrate, 2) AS BonusRate, -- teamprofpptd AS teamProf, 
  TechFlagHoursPPTD as FlagHours, techFlagHourAdjustmentsPPTD as Adjustments,
  teamprofpptd AS TeamProf   
FROM ads.ext_tp_data a
INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
  AND a.teamkey = b.teamkey
  AND b.thrudate > current_date
LEFT JOIN ads.ext_tp_teams c on b.teamKey = c.teamKey
WHERE thedate between '07/12/2020' and '07/18/2020'
  AND a.departmentKey = 18
order by team, firstname  

-- clockhours
select c.teamname AS Team, lastname, firstname,employeenumber, techclockhourspptd AS clockHours, 
  techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
  techholidayhourspptd AS holidayHours,
  techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
FROM ads.ext_tp_data a
INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
  AND a.teamkey = b.teamkey
  AND b.thrudate > current_Date
LEFT JOIN ads.ext_tp_teams c on b.teamKey = c.teamKey
WHERE thedate = '07/18/2020'
  AND a.departmentKey = 18
 order by team, firstname   

-- clockhours 7/5 - 7/11
select c.teamname AS Team, lastname, firstname,employeenumber, techclockhourspptd AS clockHours, 
  techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
  techholidayhourspptd AS holidayHours,
  techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
FROM ads.ext_tp_data a
INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
  AND a.teamkey = b.teamkey
  AND b.thrudate > current_Date
LEFT JOIN ads.ext_tp_teams c on b.teamKey = c.teamKey
WHERE thedate = '07/11/2020'
  AND a.departmentKey = 18
 order by team, firstname    

-- base clock and flag data by tech
select sunday_to_saturday_week, thedate, teamname, firstname, lastname, techhourlyrate, techhourlyrate*2 as techbonusrate, techtfrrate, 
  techflaghoursday, techflaghouradjustmentsday,
  techclockhoursday, techvacationhoursday, techptohoursday, techholidayhoursday
-- select *  
from ads.ext_tp_data a
join dds.dim_date b on a.thedate = b.the_date
where departmentkey = 18 
order by teamname,firstname, thedate 

-- base clock and flag data by tech by week
select sunday_to_saturday_week, teamname, firstname, lastname, techhourlyrate, techhourlyrate*2 as techbonusrate, techtfrrate, 
  sum(techflaghoursday) as flag, sum(techflaghouradjustmentsday) as adjustments,
  sum(techclockhoursday) as clock, sum(techvacationhoursday + techptohoursday + techholidayhoursday) as pto
-- select *  
from ads.ext_tp_data a
join dds.dim_date b on a.thedate = b.the_date
where departmentkey = 18 
group by sunday_to_saturday_week, teamname, firstname, lastname, techhourlyrate, techhourlyrate*2, techtfrrate
order by teamname,firstname, sunday_to_saturday_week

-- techs
select a.teamname, c.firstname, c.lastname, c.employeenumber, c.technumber, d.techteampercentage, d.previoushourlyrate
from ads.ext_tp_teams a
join ads.ext_tp_team_techs b on a.teamkey = b.teamkey
  and b.thrudate > current_Date
join ads.ext_tp_techs c on b.techkey = c.techkey  
join ads.ext_tp_tech_values d on c.techkey = d.techkey
  and d.thrudate > current_date
where a.departmentkey = 18
and a.thrudate > current_date
order by a.teamname, c.firstname



-- no changes, completely old team


SELECT x.*, 
  round(commHours * teamProf/100 * commRate, 2) AS commPay,
  round(bonusHours * teamProf/100 * bonusRate, 2) AS bonusPay,
  round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS ptoPay,
  round(commHours * teamProf/100 * commRate, 2) +  
  round(bonusHours * teamProf/100 * bonusRate, 2) +
  round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS totalPay  
FROM (  
select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
  d.bonusRate, d.teamProf, d.flagHours, d.adjustments, 
  d.flaghours + d.adjustments as total_flaghours,e.clockHours, e.vacationHours, 
  e.ptoHours, e.holidayHours, e.totalHours,
-- *a*  
  CASE 
    WHEN e.totalHours > 90 and teamprof >= 100 THEN e.clockhours - (totalhours - 90) 
    else e.clockHours 
  end AS commHours,
  round(
    CASE 
      WHEN totalHours > 90 and teamprof >= 100 THEN totalHours - 90 
      ELSE 0 
    END, 2) AS bonusHours  
FROM (
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
    2 * round(techtfrrate, 2) AS BonusRate, -- teamprofpptd AS teamProf, 
    TechFlagHoursPPTD as FlagHours, techFlagHourAdjustmentsPPTD as Adjustments,
    teamprofpptd AS TeamProf   
  FROM ads.ext_tp_data a
  INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
    AND b.thrudate > current_date
  LEFT JOIN ads.ext_tp_teams c on b.teamKey = c.teamKey
  WHERE thedate = '07/18/2020'
    AND a.departmentKey = 18) d
LEFT JOIN (
  select employeenumber, techclockhourspptd AS clockHours, 
    techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
    techholidayhourspptd AS holidayHours,
    techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
  FROM ads.ext_tp_data a
  INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
    AND b.thrudate > current_date
  LEFT JOIN ads.ext_tp_teams c on b.teamKey = c.teamKey
  WHERE thedate = '07/18/2020'
    AND a.departmentKey = 18) e on d.employeenumber = e.employeenumber) x
ORDER BY team, firstname, lastname

-- blueprint team
gavin flaat
mitch rogers
nate gray
jay olson

need to separate the weeks

-- teams minus blueprint and minus teams not affected by bluepring team
drop table if exists pay_teams;
create temp table pay_teams as
select c.teamname AS Team, lastname, firstname, employeenumber, 
  techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
  2 * round(techtfrrate, 2) AS BonusRate 
FROM ads.ext_tp_data a
INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
  AND a.teamkey = b.teamkey
  AND b.thrudate > current_date
LEFT JOIN ads.ext_tp_teams c on b.teamKey = c.teamKey
WHERE thedate = '07/18/2020'
  AND a.departmentKey = 18
  and lastname not in('flaat','rogers','gray')
  and c.teamname not in ('waldbauer','axtman','bear','longoria','olson')
order by team, firstname   

drop table if exists pay_tp_data;
create temp table pay_tp_data as
select a.*, b.flag, b.adjustments, b.clock, b.vacation, b.pto, b.holiday
from pay_teams a
left join (-- base clock and flag data for pay period
select teamname, firstname, lastname, techhourlyrate, techhourlyrate*2 as techbonusrate, techtfrrate, 
  sum(techflaghoursday) as flag, sum(techflaghouradjustmentsday) as adjustments,
  sum(techclockhoursday) as clock, sum(techvacationhoursday) as vacation, sum(techptohoursday) as pto, sum(techholidayhoursday) as holiday
-- select *  
from ads.ext_tp_data a
join dds.dim_date b on a.thedate = b.the_date
where departmentkey = 18 
group by teamname, firstname, lastname, techhourlyrate, techhourlyrate*2, techtfrrate) b on a.lastname = b.lastname;

-- this is the mgr final for teams affected by the blueprint team
SELECT x.*, 
  round(comm_hours * prof * commRate, 2) AS commPay,
  round(bonus_hours * prof * bonusRate, 2) AS bonusPay,
  round(ptoRate * (vacation + pto + holiday), 2) AS ptoPay,
  round(comm_hours * prof * commRate, 2) +  
  round(bonus_hours * prof * bonusRate, 2) +
  round(ptoRate * (vacation + pto + holiday), 2) AS totalPay  
FROM (  
select c.*, 
  case
    when total_hours > 90 and prof >= 1.00 then clock - (total_hours - 90)
    else clock
  end as comm_hours,
  round(
    case
      when total_hours > 90 and prof >= 1.00 then total_hours - 90 
      else 0
    end, 2) as bonus_hours
from (    
  select a.team, lastname,firstname,employeenumber,ptorate,commrate,bonusrate,prof,
    flag, adjustments, flag + adjustments as total_flag, clock, vacation, 
    pto,holiday, clock + vacation + pto + holiday as total_hours
   from pay_tp_data a
  left join ( -- prof
    select team, sum(flag)/sum(clock) as prof
    from pay_tp_data
    group by team) b on a.team = b.team) c) x