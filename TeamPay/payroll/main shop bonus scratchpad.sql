/*
make rounding consistent with what gets sent to kim
AND what shows on individual tech pay pages
what gets rounded:
commPay seems to be off, on kim:
  round(round(commrate, 2)*round(teamprof*clockhours/100, 2), 2) AS [Total Commission Pay],
*/
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 18;
@payRollEnd = '08/23/2014';
SELECT x.*, 
  round(commHours * teamProf/100 * commRate, 2) AS commPay,
  round(bonusHours * teamProf/100 * bonusRate, 2) AS bonusPay,
  round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS ptoPay,
  round(commHours * teamProf/100 * commRate, 2) +  
    round(bonusHours * teamProf/100 * bonusRate, 2) +
    round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS totalPay  
FROM (  
select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
  d.bonusRate, d.teamProf, d.flagHours, e.clockHours, e.vacationHours, e.ptoHours, e.holidayHours, 
  e.totalHours,
  CASE WHEN e.totalHours > 90 THEN 90 else e.clockHours end AS commHours,
  round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours
   -- --, round(TotalCommPay - transition, 2) AS [Comm - Transition]
--  vacationHours + ptoHours + holidayHours AS totalPtoHours,
--  ptoRate * (vacationHours + ptoHours + holidayHours) AS totalPtoPay,
--  clockHours - bonusHours AS totalCommHours,
FROM (
  select c.teamname AS Team, lastname, firstname, employeenumber, 
    techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
    2 * round(techtfrrate, 2) AS BonusRate, teamprofpptd AS teamProf, 
--    techclockhourspptd AS ClockHours, 
    TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf 
--    round(techclockhourspptd*TeamProfPPTD/100, 2) as CommHours, 
    
--    techvacationhourspptd AS VacationHours,
--    techptohourspptd AS PTOHours, techholidayhourspptd AS HolidayHours,
--    techvacationhourspptd+techptohourspptd+techholidayhourspptd AS totalHolidayHours,
--    techHourlyRate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd) AS ptoPay,
--    techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours,
--    CASE 
--      WHEN (techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd) > 90 
--        THEN round((techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd) - 90, 2)
--      ELSE 0
--    END AS bonusHours
--    round(round(techtfrrate, 2)*round(TeamProfPPTD*techclockhourspptd/100, 2), 2) AS CommPay,
    
--    round(round(techtfrrate, 2) * round(techclockhourspptd * teamprofpptd/100, 2), 2) + 
--      (techvacationhourspptd + techptohourspptd + 
--        techholidayhourspptd)*techhourlyrate AS TotalPay
--     round((techclockhourspptd + techvacationhourspptd + techptohourspptd + 
 --      techholidayhourspptd)*techhourlyrate, 2) AS Transition
     
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = @payRollEnd
    AND a.departmentKey = @department) d
LEFT JOIN (
  select employeenumber, techclockhourspptd AS clockHours, 
    techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
    techholidayhourspptd AS holidayHours,
    techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamkey = b.teamkey
  LEFT JOIN tpTeams c on b.teamKey = c.teamKey
  WHERE thedate = @payRollEnd
    AND a.departmentKey = @department) e on d.employeenumber = e.employeenumber) x

ORDER BY team, firstname, lastname

