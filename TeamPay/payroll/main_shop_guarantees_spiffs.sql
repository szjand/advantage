
/*
based on main shop payroll for managers.sql
generate both diagnostics team guarantee AND snap cell spiffs AS a TABLE
rather than twiddling with the spreadsheet
11/23/20: added ot variance on snap cell
*/
-- DROP TABLE tp_main_shop_guarantee_spiffs;
CREATE TABLE tp_main_shop_guarantee_spiffs (
  the_date date,
  team cichar(50),
  name cichar(60),  
  employee_number cichar(9),
  guarantee double,
  snap_cell double);

CREATE INDEX team_name ON tp_main_shop_guarantee_spiffs (team,name);
  
SELECT * FROM tp_main_shop_guarantee_spiffs;
-- generate a base TABLE for the current pay period  


-- DROP TABLE #wtf;
-- DELETE FROM tp_main_shop_guarantee_spiffs;
DECLARE @department integer;
DECLARE @payRollEnd date; 
@department = 18;
@payRollEnd = '01/02/2021'; ------------------------------------ payroll END date
SELECT @payRollEnd, trim(a.teamname), trim(firstname) + ' ' + trim(lastname), employeenumber
INTO #wtf
FROM tpdata a
INNER JOIN tpteamtechs b on a.techkey = b.techkey
  AND a.teamkey = b.teamkey
AND @payrollend BETWEEN b.fromdate AND b.thrudate
LEFT JOIN tpTeams c on b.teamKey = c.teamKey
WHERE thedate = @payRollEnd
  AND a.departmentKey = @department;
  
INSERT INTO tp_main_shop_guarantee_spiffs(the_date,team,name,employee_number)
SELECT * FROM #wtf;
	