--< 7/20/20  Brady 401k Audi---------------------------------------------------
/*
07/07/20 FROM Kim
For the Brady Audit
this would be for payroll 8b  4/19/2019
Carter Ferry          $2,970.65
Clayton Gehrtz        $4,824.35
Brandon Johnson       $3,164.4
Josiah Keller         $1,051.2

pay period IS 3/31/19 - 4/13/19
*/

-- main shop
select a.teamname, c.employeenumber, c.technumber, 
  c.lastname, c.firstname, d.effectivelaborrate,
  e.census, e.budget, e.poolpercentage, f.techteampercentage,
  round(budget * techteampercentage, 2), previoushourlyrate
-- SELECT *  
FROM tpteams a
inner join tpteamtechs b on a.teamkey = b.teamkey
  AND '04/01/2019' BETWEEN b.fromdate AND b.thrudate
INNER JOIN tptechs c on b.techkey = c.techkey 
LEFT JOIN tpshopvalues d on 1 = 1
  AND d.departmentkey = 18
  AND '04/01/2019' BETWEEN d.fromdate AND d.thrudate
LEFT JOIN tpteamvalues e on a.teamkey = e.teamkey  
  AND '04/01/2019' BETWEEN e.fromdate AND e.thrudate
LEFT JOIN tptechvalues f on b.techkey = f.techkey 
  AND '04/01/2019' BETWEEN f.fromdate AND f.thrudate
WHERE a.departmentkey = 18
  AND lastname IN ('ferry','gehrtz')
ORDER BY lastname, firstname

the other 2 are honda, AND that IS IN postgres





--/> 7/20/20  Brady 401k Audi---------------------------------------------------



--< 6/20/19  Brady 401k Audi---------------------------------------------------
/*
Good morning Jon I know you are out today but when you can. 
Can I get a time sheet break down for 3  tech on payroll 5b paydate 03/09/18
They are Patrick Adam,   Gavin Flaat,   and Chad A Gardner

select * FROM dds.day WHERE yearmonth = 201803 ORDER BY thedate
check date: 3/9/18
pay period : 2/18/18 - 3/3/18

*/

-- main shop
select a.teamname, c.employeenumber, c.technumber, 
  c.lastname, c.firstname, d.effectivelaborrate,
  e.census, e.budget, e.poolpercentage, f.techteampercentage,
  round(budget * techteampercentage, 2), previoushourlyrate
-- SELECT *  
FROM tpteams a
inner join tpteamtechs b on a.teamkey = b.teamkey
  AND '03/01/2018' BETWEEN b.fromdate AND b.thrudate
INNER JOIN tptechs c on b.techkey = c.techkey 
LEFT JOIN tpshopvalues d on 1 = 1
  AND d.departmentkey = 18
  AND '03/01/2018' BETWEEN d.fromdate AND d.thrudate
LEFT JOIN tpteamvalues e on a.teamkey = e.teamkey  
  AND '03/01/2018' BETWEEN e.fromdate AND e.thrudate
LEFT JOIN tptechvalues f on b.techkey = f.techkey 
  AND '03/01/2018' BETWEEN f.fromdate AND f.thrudate
WHERE a.departmentkey = 18
  AND lastname = 'flaat'
ORDER BY lastname, firstname

select techclockhoursPPTD, teamProfPPTD, techPTOHoursPPTD
-- SELECT *
FROM tpdata WHERE employeenumber = '145801' AND thedate = '03/03/2018'

-- body shop
select a.teamname, c.employeenumber, c.technumber, 
  c.lastname, c.firstname, d.effectivelaborrate,
  e.census, e.budget, e.poolpercentage, f.techteampercentage,
  round(budget * techteampercentage, 2), previoushourlyrate
-- SELECT *  
FROM tpteams a
inner join tpteamtechs b on a.teamkey = b.teamkey
  AND '03/01/2018' BETWEEN b.fromdate AND b.thrudate
INNER JOIN tptechs c on b.techkey = c.techkey 
LEFT JOIN tpshopvalues d on 1 = 1
  AND d.departmentkey = 13
  AND '03/01/2018' BETWEEN d.fromdate AND d.thrudate
LEFT JOIN tpteamvalues e on a.teamkey = e.teamkey  
  AND '03/01/2018' BETWEEN e.fromdate AND e.thrudate
LEFT JOIN tptechvalues f on b.techkey = f.techkey 
  AND '03/01/2018' BETWEEN f.fromdate AND f.thrudate
WHERE a.departmentkey = 13
  AND lastname = 'adam'
ORDER BY lastname, firstname

select techclockhoursPPTD, teamProfPPTD, techPTOHoursPPTD
-- SELECT *
FROM tpdata WHERE employeenumber = '11660' AND thedate = '03/03/2018'

-- body shop
select a.teamname, c.employeenumber, c.technumber, 
  c.lastname, c.firstname, d.effectivelaborrate,
  e.census, e.budget, e.poolpercentage, f.techteampercentage,
  round(budget * techteampercentage, 2), previoushourlyrate
-- SELECT *  
FROM tpteams a
inner join tpteamtechs b on a.teamkey = b.teamkey
  AND '03/01/2018' BETWEEN b.fromdate AND b.thrudate
INNER JOIN tptechs c on b.techkey = c.techkey 
LEFT JOIN tpshopvalues d on 1 = 1
  AND d.departmentkey = 13
  AND '03/01/2018' BETWEEN d.fromdate AND d.thrudate
LEFT JOIN tpteamvalues e on a.teamkey = e.teamkey  
  AND '03/01/2018' BETWEEN e.fromdate AND e.thrudate
LEFT JOIN tptechvalues f on b.techkey = f.techkey 
  AND '03/01/2018' BETWEEN f.fromdate AND f.thrudate
WHERE a.departmentkey = 13
  AND lastname = 'gardner'
ORDER BY lastname, firstname

select techclockhoursPPTD, teamProfPPTD, techPTOHoursPPTD
-- SELECT *
FROM tpdata WHERE employeenumber = '150105' AND thedate = '03/03/2018'

--< 6/20/19  Brady 401k Audi---------------------------------------------------


-- 6/4/18  Brady
-- main shop
select a.teamname, c.employeenumber, c.technumber, 
  c.lastname, c.firstname, d.effectivelaborrate,
  e.census, e.budget, e.poolpercentage, f.techteampercentage,
  round(budget * techteampercentage, 2), previoushourlyrate
-- SELECT *  
FROM tpteams a
inner join tpteamtechs b on a.teamkey = b.teamkey
  AND '10/10/2017' BETWEEN b.fromdate AND b.thrudate
INNER JOIN tptechs c on b.techkey = c.techkey 
LEFT JOIN tpshopvalues d on 1 = 1
  AND d.departmentkey = 18
  AND '10/10/2017' BETWEEN d.fromdate AND d.thrudate
LEFT JOIN tpteamvalues e on a.teamkey = e.teamkey  
  AND '10/10/2017' BETWEEN e.fromdate AND e.thrudate
LEFT JOIN tptechvalues f on b.techkey = f.techkey 
  AND '10/10/2017' BETWEEN f.fromdate AND f.thrudate
WHERE a.departmentkey = 18
  AND lastname = 'thompson'
ORDER BY lastname, firstname

-- body shop
select a.teamname, c.employeenumber, c.technumber, 
  c.lastname, c.firstname, d.effectivelaborrate,
  e.census, e.budget, e.poolpercentage, f.techteampercentage,
  round(budget * techteampercentage, 2), previoushourlyrate
-- SELECT *  
FROM tpteams a
inner join tpteamtechs b on a.teamkey = b.teamkey
  AND '10/10/2017' BETWEEN b.fromdate AND b.thrudate
INNER JOIN tptechs c on b.techkey = c.techkey 
LEFT JOIN tpshopvalues d on 1 = 1
  AND d.departmentkey = 13
  AND '10/10/2017' BETWEEN d.fromdate AND d.thrudate
LEFT JOIN tpteamvalues e on a.teamkey = e.teamkey  
  AND '10/10/2017' BETWEEN e.fromdate AND e.thrudate
LEFT JOIN tptechvalues f on b.techkey = f.techkey 
  AND '10/10/2017' BETWEEN f.fromdate AND f.thrudate
WHERE a.departmentkey = 13
  AND lastname = 'lene'
ORDER BY lastname, firstname