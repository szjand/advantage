/*

I have added Mitch to Team Girodat starting with the pay period beginning 5/27/18. 
I also sent the change in to Kim. That is the only change on this spread sheet. 
Can you make this happen please?

*/
-- SELECT * FROM tpteams WHERE teamname = 'girodat'  -- 21

-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 21
ORDER BY teamname, firstname  

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '05/27/2018';
@thru_date = '05/26/2018';
@dept_key = 18;
@elr = 91.46;
@team_key = 21;  -- team girodat
@census = 4;
@pool_perc = 0.28;

BEGIN TRANSACTION;
TRY
  DELETE 
  FROM tpdata
  WHERE teamkey = @team_key
    AND thedate > @thru_date;
	
-- tptechs, employeeappauthorization, he already EXISTS IN ptoemployees
  INSERT INTO tpTechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values(18,'626','Mitch','Rogers','1117901','05/27/2018');

  INSERT INTO employeeappauthorization
  SELECT 'mrogers@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'dmcveigh@rydellchev.com'
    AND appcode = 'tp';
-- team techs
  @tech_key = (
    SELECT techkey FROM tptechs
	WHERE lastname = 'rogers'); 
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);	
-- team values
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);  
-- tech values    
  -- girodat
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'girodat' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3124; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  
  -- mcveigh
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'mcveigh' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .1952; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- o'neil
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'o''neil' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2195; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- rogers
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'rogers' AND thrudate > curdate()); 
  @pto_rate = 20;
  @tech_perc = .1952; -----------------------------------------   
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   