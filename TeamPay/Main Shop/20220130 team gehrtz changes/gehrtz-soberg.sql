--Technician Pay Rate Changes Effective 1-30-22
--Jon,
--I updated the spreadsheet to reflect rate increases for Clayton Gehrtz and 
--Nick Soberg. I spoke to Laura, Ben, Jeri and Randy already about them but it 
--looks like the requests are sitting in Randy�s Compli inbox. They are considerable 
--increases to reflect the offers they received from other facilities trying to steal 
--them. Let me know if you have any questions. TY
--Andrew Neumann

gehrtz: 27.03 -> 31.50 (38.27%)
soberg: 18 -> 24.00 (29.16%)
schuppert: no change 18.00 (21.87%)
Team numbers remain the sam
		 census: 3
		 percentage: 30.0%
		 budget: 82.314

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 65  -- team gehrtz
ORDER BY teamname, firstname  


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @budget double;

@eff_date = '01/30/2022';
@thru_date = '01/29/2022';
@dept_key = 18;
@elr = 91.46;
@budget = 3;
@budget = 82.314;
@team_key = 65;  -- team gehrtz

BEGIN TRANSACTION;
TRY
  @tech_key = ( -- 58
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'gehrtz');
-- gehrtz: 27.03 -> 31.50 (38.27%)
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
  @tech_perc = .3827; 
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  	
	
-- soberg: 18 -> 24.00 (29.16%)
  @tech_key = ( -- 89
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'soberg');
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
  @tech_perc = .2916; 
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  

			
	DELETE 
  FROM tpdata
  WHERE teamkey = @team_key
    AND thedate > @thru_date;
  
  EXECUTE PROCEDURE xfmTpData();

       
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;     	

