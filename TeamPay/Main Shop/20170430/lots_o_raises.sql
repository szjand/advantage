/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
ORDER BY teamname, firstname  

the spread sheet doesn't really pan out
i am assuming the dollar amount IS what for each tech AS the bottom line
figure tpTechValues.techTeamPercentage FROM tech rate/team budget
change only techTeamValues
raises:
	   team girodat:
	   		pool perc 
	   		krewson 15.50 -> 16.86  SELECT 16.86/93.66 FROM system.iota  .18
			mcveigh 18.75 -> 20.00  SELECT 20/93.66 FROM system.iota	 .21
			girodat	30.87 -> 32.00  SELECT 32/93.66 FROM system.iota	 .34
			oneil 21.34 -> 22.48    SELECT 22.48/93.66 FROM system.iota  .24
 	   team gray:
	   		gray 21.63 ->22.63		SELECT 22.63/84.14 FROM system.iota  .2689
			winkler 17.63 -> 18.63  SELECT 18.63/84.14 FROM system.iota  .2214
	   team bear:
	   		grant: 16.00 -> 16.78   SELECT 16.78/76.83 FROM system.iota  .2184
	   team anderson:
	   		flaat: 20.00 -> 25.00	SELECT 25.00/79.57 FROM system.iota  .3141
			gehrtz: 20.19 -> 23.00	SELECT 23.00/79.57 FROM system.iota  .289			
fuck me, need to change team values			
*/	
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @dept_key integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @team_key integer;
DECLARE @census integer;
@eff_date = '04/30/2017';
@thru_date = '04/29/2017';
@dept_key = 18;
@elr = 91.46;

BEGIN TRANSACTION;
TRY
-- team girodat
-- team values
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'girodat' AND thrudate > curdate());
  @pool_perc = .256;
  @census = 4;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
/*	
-- techValues
  -- girodat
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'girodat' -----------
    AND firstname = 'jeff' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .34; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	
  -- krewson
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'krewson' -----------
    AND firstname = 'anthony' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .18; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	   
  -- mcveigh
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'mcveigh' -----------
    AND firstname = 'dennis' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .21; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- oneil
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'o''neil' -----------
    AND firstname = 'timothy' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .24; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);    
*/  
-- team gray
-- team values
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'gray' AND thrudate > curdate());
  @pool_perc = .23;
  @census = 4;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
/*	
-- techValues
  -- gray
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gray' -----------
    AND firstname = 'nathan' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2689; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);         
  -- gray
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'winkler' -----------
    AND firstname = 'zachary' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2214; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);       
*/   
-- team bear
-- team values
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'bear' AND thrudate > curdate());
  @pool_perc = .21;
  @census = 4;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
/*	
-- techValues
  -- grant
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'grant' -----------
    AND firstname = 'desiree' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2184; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);       
*/  
-- team anderson
-- team values
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'anderson' AND thrudate > curdate());
  @pool_perc = .29;
  @census = 3;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
/*	
-- techValues
  -- gehrtz
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gehrtz' -----------
    AND firstname = 'clayton' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .289; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- flaat
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'flaat' -----------
    AND firstname = 'gavin' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3141; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);       
*/     
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date
	AND teamkey IN (
      SELECT teamkey
      FROM tpteams
      WHERE teamname IN ('anderson','bear','girodat','gray')
        AND thrudate > curdate());
		 
  EXECUTE PROCEDURE xfmTpData();  
  EXECUTE PROCEDURE todayxfmTpData();   
    
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    			

