

--Jon,
--Effective next Sunday 2/13/2022 we will move Kirby Holwerda off the Alignment team and to Team ( O�Neil)
--Josh Northagen will take over Team Alignment as team leader.

-- SELECT * FROM tpteams WHERE departmentkey = 18 AND thrudate > curdatE()
/*
-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey IN (8, 21)  -- alignment, oneil
ORDER BY teamname, firstname  

-- alignment
pool% stays the same @ .215
census FROM 3 -> 2
budget FROM 58.9917 to 39.33
remove Holwerda
goodoien stays @ 16.50, % FROM .2797 -> 41.95
northagen stays @ 20, % FROM .339 -> .5085

-- o'neil
pool% stays the same @ .28
census FROM 2 -> 3
budget FROM 51.218  to 76.83
ADD Holwerda
holwerda stays @ 17.89, % FROM .3033 to .2329
oneil stays @ 23.50, % FROM .4588 -> 3059
aamodt stays @ 17.85, % FROM .3485 -> .2323
*/

/*

********************************************************
********************************************************
********************************************************
********************************************************
********************************************************
********************************************************

can't fucking figure it out, thought i had resolved the link issue 
xfmTpData() calls ukg.clock_hours
but it will NOT WORK today on zzTest

everything works up to calling that function, so, guess i will just have to 
DO that manually on the production server AND fucking hope it works

********************************************************
********************************************************
********************************************************
********************************************************
********************************************************
********************************************************
*/


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '02/13/2022';
@thru_date = '02/12/2022';
@dept_key = 18;
@elr = 91.46;
--@team_key = 22;  -- team bear
--@census = 4;
--@pool_perc = 0.23;

BEGIN TRANSACTION;
TRY


-- team alignment
  @team_key = (
    SELECT teamkey
  	FROM tpteams
  	WHERE teamname = 'alignment'
  	  AND thrudate > curdate());
  -- holwerda
  @tech_key = (
    SELECT techkey
  	FROM tptechs
  	WHERE lastname = 'holwerda'
  	  AND thrudate > curdate());	

	UPDATE tpteamtechs
	SET thrudate = @thru_date
	WHERE teamkey = @team_key
	  AND techkey = @tech_key
		AND thrudate > curdate();
		
	
--SELECT * FROM tpteamvalues WHERE teamkey = 8		
--SELECT 2*91.46*.215 FROM system.iota  -- 39.3278
  UPDATE tpteamvalues
	SET census = 2,
	    budget = 39.3278
	WHERE teamkey = @team_key
	  AND thrudate > curdate();
  -- goodoien
	-- SELECT 16.5/39.3278 FROM system.iota  --.4195
	@tech_key = ( -- 87
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'goodoien'
		  AND thrudate > curdate());		
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	@tech_perc = .4195; -----------------------------------------   
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	

  -- northagen
	-- SELECT 20.0/39.3278 FROM system.iota  --.5085
	@tech_key = ( -- 84
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'northagen'
		  AND thrudate > curdate());		
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	@tech_perc = .5085; -----------------------------------------   
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	

-- team o'neil
  @team_key = (
    SELECT teamkey
  	FROM tpteams
  	WHERE teamname = 'o''neil'
  	  AND thrudate > curdate());
  -- holwerda
  @tech_key = (
    SELECT techkey
  	FROM tptechs
  	WHERE lastname = 'holwerda'
  	  AND thrudate > curdate());	

  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
	values(@team_key, @tech_key, @eff_date);


-- SELECT 3*91.46*.28 FROM system.iota  -- 76.8264
  UPDATE tpteamvalues
	SET census = 3,
	    budget = 76.8264
	WHERE teamkey = @team_key
	  AND thrudate > curdate();

  -- o''neil
	-- SELECT 23.5/76.8264 FROM system.iota  --.3059
	@tech_key = ( -- 54
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'o''neil'
		  AND thrudate > curdate());		
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	@tech_perc = .3059; -----------------------------------------   
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	




  -- holwerda
	-- SELECT 17.89/76.8264 FROM system.iota  --.2329
	@tech_key = ( -- 24
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'holwerda'
		  AND thrudate > curdate());		
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	@tech_perc = .2329; -----------------------------------------   
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	

  -- aamodt
	-- SELECT 17.85/76.8264 FROM system.iota  --.2323
	@tech_key = ( -- 90
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'aamodt'
		  AND thrudate > curdate());		
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	@tech_perc = .2323; -----------------------------------------   
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	

/*
	
  DELETE 
  FROM tpdata 
  WHERE teamkey IN (8,21)
    AND thedate > '02/12/2022' -- @thru_date;

  EXECUTE PROCEDURE xfmTpData();
 
*/  

--done
--IN postgresql tp.team_leaders replace howerda w ith northagen for team alignment


	 
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   			

