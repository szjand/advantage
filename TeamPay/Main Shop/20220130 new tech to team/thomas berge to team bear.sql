/*
Good afternoon Jon,
Effective 1/30/22 we are moving Thomas Berge from hourly; to Team (Bear) and flat rate.
Andrew will send you the spread sheet for him.
Kim this is just an FYI.  Compli notice has been sent to Laura also.
*/

SELECT * FROM tpteams WHERE teamname = 'bear'
-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 22  -- team bear
ORDER BY teamname, firstname  


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '01/30/2022';
@thru_date = '01/29/2022';
@dept_key = 18;
@elr = 91.46;
@team_key = 22;  -- team bear
@census = 4;
@pool_perc = 0.23;

BEGIN TRANSACTION;
TRY
  DELETE 
  FROM tpdata
  WHERE teamkey = @team_key
    AND thedate > @thru_date;
	
-- tptechs, employeeappauthorization, he already EXISTS IN ptoemployees
  INSERT INTO tpTechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values(18,'659','Thomas','Berge','195359',@eff_date);

  INSERT INTO tpemployees(username,firstname,lastname,employeenumber,password,
	  membergroup,storecode,fullname)
  values('tberge@rydellcars.com','Thomas','Berge','195359','Xer35aP?','Fuck You','RY1','Thomas Berge');
	
  INSERT INTO employeeappauthorization
  SELECT 'tberge@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
	-- SELECT *
  FROM employeeappauthorization
  WHERE username = 'dgrant@rydellcars.com'
    AND appcode = 'tp';
-- team techs
  @tech_key = (
    SELECT techkey FROM tptechs
  	WHERE lastname = 'berge'); 
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);	
-- team values
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);  
	
-- tech values    
  -- bear
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'bear' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2839; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  
  -- grant
  @tech_key = (
    SELECT techkey 
		-- SELECT *
    FROM tptechs 
    WHERE lastname = 'grant' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2094; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- buckholz
  @tech_key = (
    SELECT techkey 
		-- SELECT *
    FROM tptechs 
    WHERE lastname = 'buckholz' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .1901; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- berge
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'berge' AND thrudate > curdate()); 
  @pto_rate = 16;
  @tech_perc = .1901; -----------------------------------------   
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  EXECUTE PROCEDURE xfmTpData();
   
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   	