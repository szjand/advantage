Jon, 
I moved Desiree Grant from Quick Service on an hourly pay rate back to 
Midas on Team Jeff Bear. That should be the only change starting on 1/21/17. 
Let me know of any questions. Thanks
Andrew Neumann

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 22
ORDER BY teamname, firstname  

-- she already EXISTS IN tptecs, techkey: 70
SELECT * FROM tpteamtechs WHERE teamkey = 22


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '01/21/2018';
@thru_date = '01/20/2018';
@dept_key = 18;
@elr = 91.46;
@team_key = 22;  -- team bear
@census = 4;
@pool_perc = 0.218;
-- select * FROM tpteamvalues WHERE teamkey = 22
BEGIN TRANSACTION;
TRY
  DELETE 
  FROM tpdata
  WHERE teamkey = @team_key
    AND thedate > @thru_date;
-- team techs
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'grant');
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);
-- team values
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
-- tech values    
  -- bear
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'bear' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2995;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);
  
  -- holwerda
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'holwerda' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2243;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate); 
  
  -- strandell
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'strandell' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2243;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  -- grant
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'grant' AND thrudate > curdate()); 
  @pto_rate = 15;
  @tech_perc = .2104;    
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY; 

/*
SELECT * FROM tptechvalues WHERE techkey IN (55,24,26) AND thrudate > curdate()
select * FROM tptechs ORDER BY lastname
SELECT * FROM tptechvalues WHERE techkey = 70

select *
FROM tpdata
WHERE teamkey = 22
ORDER BY thedate DESC

*/