/*
techs did NOT LIKE the ELR for this pay period, 1/10 -> 1/23/16
whined to andrew AND ben
ben said bump ELR to 91.46 FROM 90.5 (that was the elr for 10/18 -> 10/31/15)
doing this on Tuesday, day 3 of the pay period

SELECT *
FROM tpdata
WHERE departmentkey = 18
AND thedate = curdate() - 1
AND teamname = 'olson'
*/

DECLARE @new_elr double;
DECLARE @department_key integer;
@new_elr = 91.46;
@department_key = 18;

BEGIN TRANSACTION;
TRY 
  UPDATE tpShopValues
  SET effectivelaborrate = @new_elr
  --SELECT *
  --FROM tpshopvalues
  WHERE departmentkey = 18
    AND thrudate > curdate();
  
  UPDATE tpTeamValues
  SET budget =  round(census * 91.46 * poolpercentage, 2)
  --SELECT a.*, round(census * 91.46 * poolpercentage, 2) AS new_budget
  --FROM tpTeamValues a
  WHERE thrudate > curdate() 
    AND teamkey IN ( 
      SELECT teamkey
      FROM tpteams  
      WHERE departmentkey = 18
        AND thrudate > curdate());

UPDATE tpdata 
SET teambudget = x.new_team_budget,
    techtfrrate = x.new_tech_rate
FROM (	   	  
  SELECT e.techkey, a.teamname, e.lastname, e.firstname,
    round(census * 90.5 * poolpercentage, 2), 
    round(census * 91.46 * poolpercentage, 2) AS new_team_budget, 
	g.techtfrrate,
	census * 91.46 * poolpercentage * f.techteampercentage AS new_tech_rate 
  FROM tpteams a
  INNER JOIN tpTeamValues b on a.teamkey  = b.teamkey
    AND b.thrudate > curdatE()
  INNER JOIN tpshopvalues c on 1 = 1 
    AND c.departmentkey = 18
    AND c.thrudate > curdate()  
  INNER JOIN tpteamtechs d on a.teamkey = d.teamkey
    AND d.thrudate > curdate()
  INNER JOIN tptechs e on d.techkey = e.techkey   
  INNER JOIN tptechValues f on e.techkey = f.techkey
    AND f.thrudate > curdate() 
  LEFT JOIN tpdata g on f.techkey = g.techkey
    AND g.thedate = curdate()  
  WHERE a.departmentkey = 18
    AND a.thrudate > curdate()) x
WHERE payperiodseq = 185
  AND departmentkey = 18
  AND tpdata.techkey = x.techkey;

COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END;    