--I think I did this correctly. Kyle Grochow is starting at the Honda store on 
--11-14-16 so I have removed him from team Andersonís pay table. Gavin Flaat 
--will not be moving to flat-rate until mid-December so I will have to add him 
--and send that change to you when it happens. Let me know if I need to do 
--anything else.     Thanks!!


/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND teamname = 'anderson'

-- current reality
teamName	ELR	    poolPercentage	teamCensus	teamBudget	EXPR	  	 	 techTeamPercentage	techTFRRate	teamKey	techkey
Anderson	91.46	0.245			3			67.2231		Clayton Gehrtz	 0.3004				20.19381924	23		58
Anderson	91.46	0.245			3			67.2231		Shawn Anderson	 0.4392				29.52438552	23		59
Anderson	91.46	0.245			3			67.2231		Kyle Grochow	 0.2429				16.32849099	23		64
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @pto_rate double;
DECLARE @tech_team_perc double;

@eff_date = '11/13/2016';
@thru_date = '11/12/2016';
@dept_key = 18;
@elr = 91.46;
@pool_perc = .275;
@team_key = (
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'anderson');
@tech_key = (
  select techkey
  FROM tptechs 
  WHERE lastname = 'grochow'
    AND firstname = 'kyle'
	AND thrudate > curdate());	
BEGIN TRANSACTION;
TRY	
  UPDATE tpTechs 
  SET thrudate = @thru_date
  WHERE techkey = @tech_key;

  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND techkey = @tech_key
    AND thrudate > curdate();
	
  @census = (
    SELECT COUNT(*)
	FROM tpteamtechs
	WHERE teamkey = @team_key
	  AND thrudate > @thru_date);
    
  UPDATE tpteamvalues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > @eff_Date;
	
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, @census, round(@census * @elr * @pool_perc, 4),  @pool_perc, @eff_date);
  
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE lastname = 'anderson'
	  AND thrudate > @thru_date);
  @pto_rate = (
    SELECT previousHourlyRate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > @thru_date);
  @tech_team_perc = .5868;	  
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > @thru_date;
  INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyRate)
  values(@tech_key, @eff_date, @tech_team_perc, @pto_rate);	
	
	
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE lastname = 'gehrtz'
	  AND thrudate > @thru_date);
  @pto_rate = (
    SELECT previousHourlyRate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > @thru_date);
  @tech_team_perc = .4013;	  
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > @thru_date;
  INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyRate)
  values(@tech_key, @eff_date, @tech_team_perc, @pto_rate);	
  	 
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date;	  		  
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();   
  
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
         
