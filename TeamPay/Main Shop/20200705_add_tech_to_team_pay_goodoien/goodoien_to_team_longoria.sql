/*

I replaced Charles Zacha with Derek Goodoien on Team Bev because we want to
 move him to flat rate starting 4-5-2020. Let me know if you have any questions. 
 TY

Andrew Neumann
06/30/20

*/
-- SELECT * FROM tpteams WHERE teamname = 'longoria'  -- 8
/*
-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 8
ORDER BY teamname, firstname  
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '07/05/2020';
@thru_date = '07/04/2020';
@dept_key = 18;
@elr = 91.46;
@team_key = 8;  -- team longoria
@census = 2;
@pool_perc = 0.215;

BEGIN TRANSACTION;
TRY
  DELETE 
  FROM tpdata
  WHERE teamkey = @team_key
    AND thedate > @thru_date;
/*	
select * from dds.edwEmployeeDim WHERE lastname = 'goodoien' emp# 153865
select * FROM ptoemployees WHERE employeenumber = '153865' nope
SELECT * FROM tptechs WHERE lastname = 'goodoien'  nope
SELECT * FROM dds.dimtech WHERE employeenumber = '153865'  303
select * FROM tpemployees WHERE employeenumber = '153865'  nope
select * FROM employeeappauthorization WHERE username = 'dgoodoien@rydellcars.com' nope

he has workd a shitload of hours but has been part time IN payroll since hired
don't know what to DO about pto

select min(c.thedate) AS from_date, MAX(c.thedate) AS thru_date, SUM(clockhours), SUM(ptohours)
FROM dds.edwEmployeeDim a
JOIN dds.edwClockHoursFact b on a.employeekey = b.employeekey
JOIN dds.day c on b.datekey = c.datekey
WHERE a.employeenumber = '153865'
  AND clockhours <> 0
GROUP BY sundaytosaturdayweek  
order BY sundaytosaturdayweek   DESC

select * FROM tptechs
*/


-- tptechs, employeeappauthorization, he already dow not yet EXIST IN or tpemployees ptoemployees

-- first tpemployees
  INSERT INTO tpemployees(username,firstname,lastname,employeenumber,password,membergroup,storecode,fullname)
  values('dgoodoien@rydellcars.com','Derek','Goodoien','153865','2sdRKPm5y','Fuck You','RY1','Derek Goodoien');

  INSERT INTO tpTechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values(18,'303','Derek','Goodoien','153865','07/11/2019');

  INSERT INTO employeeappauthorization
  SELECT 'dgoodoien@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'dmcveigh@rydellchev.com'
    AND appcode = 'tp';
-- team techs
  @tech_key = (
    SELECT techkey FROM tptechs
	WHERE lastname = 'goodoien'); 
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);	
-- team values
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);  
-- tech values    
  -- holwerda
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'holwerda' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .455; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  
  -- goodoien
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'goodoien' AND thrudate > curdate()); 
  @pto_rate = 16.5;
  @tech_perc = .4195; -----------------------------------------   
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   