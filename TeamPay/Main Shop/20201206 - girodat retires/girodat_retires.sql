
/*
12/03/20
Jeff Girodat has retired. Effective 12/6/20 can you remove him off the team,
rename it to O'neil AND give ALL 3 access to their team numbers.
Tim O'Neil, Dennis McVeigh, Gordon David
*/

SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 21
ORDER BY teamname, firstname  

AS i determined IN the 11/11/19 script, it IS good enough to just UPDATE tpTeamTechs
it persists incorrect teamCensus, but maintains correct tech flat rate

Budget: 102.44
mcveigh:  102.44 * .1952 = 20	  'dmcveigh@rydellchev.com'
david: 102.44 * .19875 = 20.36	  'gdavid@rydellchev.com'
oneil: 102.44 * .2194 = 22.48	  'toneil@rydellcars.com'

DELETE 
FROM tpdata
WHERE techkey = 4
  and thedate = '12/06/2020';
  
UPDATE tpTeamTechs
SET thrudate = '12/05/2020'
WHERE techkey = 4
  AND teamkey = 21;
  
UPDATE tpdata  
SET teamname = 'O''Neil'
-- SELECT * FROM tpdata 
WHERE teamkey = 21 AND thedate = curdate();

-- shit forgot the new team name, just rename it
SELECT * 
FROM tpteams
WHERE teamkey = 21

UPDATE tpteams
SET teamname = 'O''Neil'
WHERE teamkey = 21;
-- now, the new part IS to give ALL remaining team members team leader acces 

SELECT 'dgoodoien@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
-- SELECT *
FROM employeeappauthorization
WHERE username = 'dmcveigh@rydellchev.com'
  AND appcode = 'tp';
  
SELECT *
FROM employeeappauthorization
WHERE username = 'jgirodat@rydellchev.com'
  AND appcode = 'tp';  
  
DELETE 
-- SELECT *
FROM employeeAppAuthorization
WHERE username IN ('dmcveigh@rydellchev.com','gdavid@rydellchev.com','toneil@rydellcars.com')  
AND appcode = 'tp';

INSERT INTO employeeAppAuthorization
SELECT 'dmcveigh@rydellchev.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'jgirodat@rydellchev.com'
  AND appcode = 'tp';
  
INSERT INTO employeeAppAuthorization
SELECT 'gdavid@rydellchev.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'jgirodat@rydellchev.com'
  AND appcode = 'tp';
  
INSERT INTO employeeAppAuthorization
SELECT 'toneil@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
FROM employeeappauthorization
WHERE username = 'jgirodat@rydellchev.com'
  AND appcode = 'tp';    