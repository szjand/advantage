/*
The following techs are included in the rate increases starting on 4/11/21. Their 
new wage is listed below as well as updated on the spreadsheet. 
Nick Soberg - $18.00  team anderson
Kodey Schuppert - $18.00  team anderson
Tim Oneil - $23.60  team o'neil
Dennin McVeigh - $21.50  team o'neil

go ahead AND arcchive 2020 data
SELECT MAX(thedate) FROM tpdata_archive_thru_2019  -- 12/31/2019
SELECT COUNT(*) FROM tpdata WHERE thedate > '12/31/2020'
INSERT INTO tpdata_archive_thru_2020
SELECT * FROM tpdata WHERE thedate BETWEEN '01/01/2020' AND '12/31/2020';
SELECT MIN(thedate), MAX(thedate), COUNT(*) FROM tpdata_archive_thru_2020
delete FROM tpdata WHERE thedate BETWEEN '01/01/2020' AND '12/31/2020';
SELECT MIN(thedate), MAX(thedate), COUNT(*) FROM tpdata


select * FROM tpteams WHERE departmentkey = 18 AND thrudate > curdate()
-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey IN (21,23)
  ORDER BY teamname, firstname  
	
initially, i am thinking, the only thing that needs to change IS techvalues	
that IS true for team anderson,
for team oneil, he changed the team budget	
*/

--	select * FROM tpteamvalues WHERE teamkey = 21 ORDER BY teamkey
--	select * FROM tpdata WHERE teamkey = 21 AND thedate BETWEEN '04/11/21' AND '04/25/2021' ORDER BY lastname, thedate
	
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '04/11/2021';
@thru_date = '04/10/2021';
@dept_key = 18;
@elr = 91.46;

BEGIN TRANSACTION;
TRY

-- team anderson
-- tech values     
-- schuppert
  @pto_rate = 19.68;
	@tech_key = 88;
  @tech_perc = .164; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);   

-- soberg
  @pto_rate = 19.06;
	@tech_key = 89;
  @tech_perc = .164; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);   	
	
-- team oneil	
-- team values
  @team_key = 21;
  @census = 3;
  @pool_perc = .28;  	
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date); 
	

-- oneil
  @pto_rate = 33.7;
	@tech_key = 54;
  @tech_perc = .3059; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  	
	
-- david
  @pto_rate = 25.94;
	@tech_key = 16;
  @tech_perc = .265; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  		
	
-- mcveigh
  @pto_rate = 32.07;
	@tech_key = 11;
  @tech_perc = .2799; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	
	
	DELETE 
  FROM tpdata
  WHERE teamkey IN (21)
    AND thedate > @thru_date;
  
  EXECUTE PROCEDURE xfmTpData();

       
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;     