/*
03/03/20
remove Charles Zacha FROM team Longoria
Charles is moving to the parts department. Can you remove him off team Longoria effective 3/1/20 please.

AS i determined IN the 11/11/19 script, it IS good enough to just UPDATE tpTeamTechs
Budget 37.279096
zacha .5201 * 37.279096 = 19.388857
holwerda .4798 * 37.279096 = 17.88651

*/
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND teamName = 'longoria'
ORDER BY teamname, firstname  

SELECT * FROM tptechs WHERE techkey = 62

DELETE 
FROM tpdata
WHERE techkey = 62
  and thedate >= '02/29/2020';
  
UPDATE tpTeamTechs
SET thrudate = '01/04/2020'
WHERE techkey = 62
  AND teamkey = 8;
      