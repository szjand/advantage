/*
Pito is moving to the Body Shop on Monday so can you please remove him 
from the Aftermarket Team Waldbauer please? 
I sent the payroll notice to Taylor and Kim as well. Thank you sir.

SELECT * FROM tptechs WHERE lastname = 'ortiz'

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 20
ORDER BY teamname, firstname  
*/


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '08/06/2017';
@thru_date = '08/05/2017';
@dept_key = 18;
@elr = 91.46;
@team_key = 20;
BEGIN TRANSACTION;
TRY
-- remove ortiz FROM ALL relevant tables
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'ortiz');
  UPDATE tpTechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key;
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key;
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
     
-- UPDATE team values
-- SELECT * FROM tpTeamValues WHERE teamkey = 20
  UPDATE tpTeamValues
  SET thrudate = @thru_Date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  -- census * elr * wages% (FROM spreadsheet_
  -- 2 * 91.46 * .199 = 36.40
  INSERT INTO tpTeamValues (teamkey,census, budget,poolpercentage,fromdate)
  values(@team_key, 2, 36.4, .199, @eff_date);
-- UPDATE tech values
-- remaining 2 techs, rate remains the same, % needs to be adjusted
-- waldbauer: 18.81/36.40 = .5168
-- kiefer: 17.45/36.40 = .4794
-- select * from tpTechValues where techkey in (SELECT techkey FROM tptechs WHERE lastname IN ('waldbauer','kiefer')) AND thrudate > curdate()
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'waldbauer' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@tech_key, @eff_date, .5168, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'kiefer' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@tech_key, @eff_date, .4794, @pto_rate);  
  DELETE FROM tpData
  WHERE departmentkey = @dept_key
    AND teamkey = @team_key
    AND thedate > @thru_date;
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData(); 
    
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY; 