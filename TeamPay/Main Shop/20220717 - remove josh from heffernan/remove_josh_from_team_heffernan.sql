/*
Josh Heffernan IS temporarily being removed FROM team heffernan
he will be assigned to a team of one
but will eventually be returned to team heffernan
SELECT * FROM tpteams WHERE thrudate > curdate() AND departmentkey = 18

Josh's current flat rate IS $27.02
he IS being given a 130% guarantee, to be monitored BY key
*/

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate() 
  AND departmentKey = 18
  AND a.teamkey = 3 -- (team heffernan)
ORDER BY teamname, firstname  

-- remove josh FROM team heffernan
DELETE 
-- SELECT * 
FROM tpdata
WHERE techkey = 7
  and thedate > '07/16/2022';
  
UPDATE tpteamtechs
SET thrudate = '07/16/2022'
WHERE techkey = 7
  AND teamkey = 3
	AND thrudate > curdate();
	
EXECUTE PROCEDURE xfmTpData();	

-- CREATE a new team for Josh

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
@eff_date = '07/17/2022';
@thru_date = '07/16/2022';
@dept_key = 18;

BEGIN TRANSACTION;
TRY
	 INSERT INTO tpTeams(departmentkey,teamname,fromdate)
	 values(@dept_key,'Josh',@eff_date);
	 @team_key = (
	   SELECT teamkey
		 FROM tpteams
		 WHERE teamName = 'Josh'
		   AND thrudate > curdate());
	 @tech_key = (
	   SELECT techkey
		 FROM tptechs
		 WHERE lastname = 'heffernan'
		   AND firstname = 'josh'
			 AND thrudate > curdate());
	 INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
	 values(@team_key,@tech_key,@eff_date);
	 INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate)
	 values(@team_key,1,27.02,1,@eff_date);
	 UPDATE tpTechValues
	 SET thruDate = @thru_date
	 WHERE techKey = 7
	   AND thrudate > curdate();
	 INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
	 values(@tech_key,@eff_date,1,35.41);
	 
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  


