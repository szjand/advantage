/*
I have a rate change I was finally able to get approved for Shawn Anderson. The 
increase will be 0.99 per flat rate hour. How much work would it be to find out 
what the additional amount would have been paid out to Shawn if this raise had 
been put into effect on 2-14-21. I spoke to Kim and she mentioned it may be 
easier to pay Shawn out the additional pay in a lump sum  or something along 
those lines. Thoughts?

This IS what i think: make it effective 4/25, THEN the balance of 2/14 - 4/24
can be an adjustment on the next check


SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 23
ORDER BY teamname, firstname  

no change IN poolpercentage, remains at .3, only thing changing IS Shawn's 
techteampercentage, FROM .3001 to .3091, rate FROM 32.93 to 33.92

so, only change IS to shawns tech values

*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;

@eff_date = '04/25/2021';
@thru_date = '04/24/2021';
@dept_key = 18;
@elr = 91.46;
@team_key = 23;
@tech_key = 59; 
@pool_perc = .3;
@census = 4;
BEGIN TRANSACTION;
TRY
	
-- techValues
  -- shawn
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3091; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	    
  
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey = @team_key
		AND techkey = @tech_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();      

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    
