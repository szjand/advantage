/*

WORK against connection zjon:  \\67.135.158.12:6363\advantage\zjon\sco.add 
  a copy of production scotest\sco.add
changes to take effect 7/13/14

PRACTICE RUN: DO the changes AS though they take effect 06/29

-- these are necesssary to simulate going back to previous payperiod
DELETE FROM tpData WHERE thedate > '06/29/2014';
DELETE FROM tpShopValues WHERE fromDate = '06/29/2014' AND departmentKey = 18;
UPDATE tpShopValues SET thruDate = '12/31/9999' WHERE thruDate = '06/29/2014' AND departmentKey = 18;

-- these 2 take care of the last UPDATE to tpTeamValues AS a result of ELR change
-- this IS a total shortcut, assuming nothing besides ELR for main shop changed
--  IN the previous pay period
DELETE FROM tpTeamValues WHERE fromDate = '06/29/2014' AND thruDate = '12/31/9999';
UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '06/15/2014';   
*/  


/* here IS current data formatted the same AS the spreadsheet
  Main Shop Flat Rate July Edition.jon summary
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
ORDER BY teamname  
*/
/* ORDER of UPDATE FROM march 14 
-- new teams
    tpteams
-- remove teams
    tpTeams  
    tpteamTechs - removed teams
-- assign techs to teams 
    tpTeamTechs
    tpTechValues  
-- shop values, new ELR
    tpShopValues
-- team values
    tpTeamValues
        CLOSE current row
        INSERT new row
-- tpData
-- employeeAppAuthorization         
*/
/*
what IS new with this UPDATE
  new techs to team pay: 
    Lucas Kiefer: lkieferWrydellcars.com
    Christopher Stinar: cstinar1@rydellcars.com 
    Zachary Winkler: zwinkler@rydellcars.com 
changes:
  Remove Duckstad Team:
    stryker: no longer employed
    david: to gray team  
    duckstad: to hourly
    thompson: to gray team

first, feels LIKE we are only dealing with valid time (snodgrass) here, NOT 
  TRANSACTION time, these are NOT bitemporal tables    
so, IF i terminate the duckstad team (finite thrudate), there will still
be current assignments to tpTeamTechs for that tema
constraint: 
  there can be no rows IN tpTeamTechs WHERE the referencing
  at no time can there be a techTeam assignment outside the period of team validity
    same thing for tpTeamValues
  so, IN addition to the RI, tpTeams.PK -> tpTeamTechs.FK1
*/
DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
-- testing
--  @effDate = '06/29/2014';  
--  @newELR = 91.05;
-- production
  @effDate = '07/13/2014';  
  @newELR = 91.07;  
  @departmentKey = 18;  
BEGIN TRANSACTION;
TRY 
/* this portion IS necessary for backfilling the existing payperiod, for testing
DELETE FROM tpData WHERE thedate > '06/29/2014';
DELETE FROM tpShopValues WHERE fromDate = '06/29/2014' AND departmentKey = 18;
UPDATE tpShopValues SET thruDate = '12/31/9999' WHERE thruDate = '06/29/2014' AND departmentKey = 18;
DELETE FROM tpTeamValues WHERE fromDate = '06/29/2014' AND thruDate = '12/31/9999';
UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '06/15/2014'; 
/**/

/*
production: assuming this IS being run on Sunday, day 1 of the new pay period,
  the first day of which IS @effDate
  overnight run of tpData generated a row IN tpData for @effDate with the old
  team configs, DELETE that row, it will be repopulated at the END of this script
*/  
--< production -----------------------------------------------------------------<
  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;

-- tpShopValues: new ELR  
    -- CLOSE previous pay period
  UPDATE tpShopValues
  SET thruDate = @effDate - 1
  WHERE thruDate > curdate()
    AND departmentKey = @departmentKey;      
  -- INSERT row for new pay period
  INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
  values(@departmentKey, @effDate, @newELR);    
  
--/> production ----------------------------------------------------------------/>

--< new techs -----------------------------------------------------------------<
-- 1. tpEmployees  
  @user = 'lkiefer@rydellcars.com';
  @password = 'wadjKv882T';  
  INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
    membergroup, storecode, fullname)
  SELECT @user, firstname, lastname, employeenumber, @password, 
    'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
  FROM dds.edwEmployeeDim 
  WHERE lastname = 'kiefer' 
    AND firstname = 'lucas'
    AND currentrow = true 
    AND active = 'active';
	    
  @user = 'cstinar1@rydellcars.com';
  @password = '34t6Kv882T';  
  INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
    membergroup, storecode, fullname)
  SELECT @user, firstname, lastname, employeenumber, @password, 
    'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
  FROM dds.edwEmployeeDim 
  WHERE lastname = 'stinar' 
    AND firstname = 'christopher'
    AND currentrow = true 
    AND active = 'active';  
    
  @user = 'zwinkler@rydellcars.com';
  @password = 'K2t686ab2T';  
  INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
    membergroup, storecode, fullname)
  SELECT @user, firstname, lastname, employeenumber, @password, 
    'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
  FROM dds.edwEmployeeDim 
  WHERE lastname = 'winkler' 
    AND firstname = 'zachary'
    AND currentrow = true 
    AND active = 'active'; 
	   
  -- employeeAppAuthorization
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 100
    AND b.approle = 'technician'
    AND a.username IN('lkiefer@rydellcars.com','cstinar1@rydellcars.com',
      'zwinkler@rydellcars.com'); 	
  	
-- 2. tpTechs 
  insert into tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT 18, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE (
        a.lastname = 'winkler' AND a.firstname = 'zachary'
        OR a.lastname = 'stinar' AND a.firstname = 'christopher'
        OR a.lastname = 'kiefer' AND a.firstname = 'lucas')
      AND a.currentrow = true 
      AND a.active = 'active';  
--/> new techs ----------------------------------------------------------------/> 

--< remove duckstad team ------------------------------------------------------<
-- 1. first UPDATE tpTeamValues & tpTeamTechs since i have NOT yet 
--      implemented constraints
  @teamKey = (
    SELECT teamKey FROM tpTeams
    WHERE teamName = 'duckstad'
      AND thruDate > curdate());
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate(); 
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();  
  UPDATE tpTeams
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();  
-- 2. duckstad IS going hourly 
  DELETE 
  FROM employeeAppAuthorization 
  WHERE username = 'jduckstad@rydellchev.com'
    AND appcode = 'tp';
--/> remove duckstad team -----------------------------------------------------/>

--< team waldbauer ------------------------------------------------------------<
  -- 1. tpTeamTechs
  -- 2. tpTechValues
  -- 3. tpTeamValues
-- 1. ADD lucas kiefer, remove sigette
  @poolPerc = .191;
  @teamKey = (
    SELECT teamKey 
    FROM tpTeams WHERE teamName = 'waldbauer' AND thruDate > curDate());
  -- ADD kiefer    
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'kiefer' AND thrudate > curdate());
  INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
  values (@teamKey, @techKey, @effDate);
  -- remove sigette  
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'sigette' AND thrudate > curdate());  
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate > curdate();
  -- employeeAppAuthorization       
  DELETE 
  FROM employeeAppAuthorization 
  WHERE username = (
      SELECT username
      FROM tpEmployees
      WHERE lastname = 'sigette');   
-- 2. only kiefer, no other changes
  @techPerc = .294; /************************/
  @hourlyRate = 15; /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'kiefer' AND thrudate > curdate());
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
-- 3.
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));   
--/> team waldbauer -----------------------------------------------------------/>

--< team girodat --------------------------------------------------------------<
-- 1. tpTeamTechs
-- 2. tpTechValues
-- 3. tpTeamValues
-- 1. ADD stinar, remove koller 
  @poolPerc = .22;   
  @teamKey = (
    SELECT teamKey 
    FROM tpTeams WHERE teamName = 'girodat' AND thruDate > curDate());  
  -- ADD stinar, new tech to team    
  @techPerc = .18; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'stinar' AND thrudate > curdate());
  INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
  values (@teamKey, @techKey, @effDate);
  -- remove koller FROM team  
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'koller' AND thrudate > curdate()); 
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate > curdate(); 
  -- employeeAppAuthorization
  DELETE 
  FROM employeeAppAuthorization 
  WHERE appcode = 'tp'
    AND username = (
      SELECT username
      FROM tpEmployees
      WHERE lastname = 'koller');          
-- 2. ALL get changed
  -- stinar, first entry IN tpTeamValues
  @techPerc = .18; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'stinar' AND thrudate > curdate());
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, 15);
  -- edit girodat values
  @techPerc = .365; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'girodat' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- edit winfield values
  @techPerc = .222; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'winfield' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- edit mcveigh values
  @techPerc = .217; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'mcveigh' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);   
-- 3.
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));   
--/> team girodat -------------------------------------------------------------/>

--< team gray -----------------------------------------------------------------<
-- 1. tpTeamTechs: 
  --      remove Swift, Hamman
  --      ADD w thompson & g david (already IN tpTechs)
  --      ADD zachary winkler (new tech)
-- 1.
  @poolPerc = .19; /****************************/  
  @teamKey = (
    SELECT teamKey 
    FROM tpTeams WHERE teamName = 'gray' AND thruDate > curDate());  
  -- ADD winkler
  @techPerc = .174; /****************************/  
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'winkler' AND thrudate > curdate());
  INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
  values (@teamKey, @techKey, @effDate);
  -- ADD thompson
  @techPerc = .225; /****************************/  
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'thompson' AND thrudate > curdate());
  INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
  values (@teamKey, @techKey, @effDate); 
  -- ADD david
  @techPerc = .207; /****************************/  
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'david' AND thrudate > curdate());
  INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
  values (@teamKey, @techKey, @effDate);   
  -- remove swift
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'swift' AND thrudate > curdate()); 
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate > curdate(); 
  -- employeeAppAuthorization
  DELETE 
  FROM employeeAppAuthorization 
  WHERE username = (
      SELECT username
      FROM tpEmployees
      WHERE lastname = 'swift');    
  -- remove hamman
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'hamman' AND thrudate > curdate()); 
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate > curdate();     
  -- employeeAppAuthorization
  DELETE 
  FROM employeeAppAuthorization 
  WHERE username = (
      SELECT username
      FROM tpEmployees
      WHERE lastname = 'hamman');    
-- 2. tpTechValues: every body changes
  -- winkler, first entry IN tpTeamValues
  @techPerc = .174; /****************************************/
  @hourlyRate = 15;
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'winkler' AND thrudate > curdate());
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- edit gray values
  @techPerc = .22; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'gray' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- edit strandell values
  @techPerc = .174; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'strandell' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- edit thompson values
  @techPerc = .225; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'thompson' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);    
  -- edit david values
  @techPerc = .207; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'david' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
-- 3. tpTeamValues
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));  
--/> team gray ----------- ----------------------------------------------------/>

--< team longoria ------------------------------------------------------------<
-- 1. tpTeamTechs
-- 2. tpTechValues
-- 3. tpTeamValues
  @poolPerc = .17; /****************************/  
  @teamKey = (
    SELECT teamKey 
    FROM tpTeams WHERE teamName = 'longoria' AND thruDate > curDate());  
-- 1. tpTeamTechs: remove yanish
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'yanish' AND thrudate > curdate());  
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate > curdate();  
  -- employeeAppAuthorization
  DELETE 
  FROM employeeAppAuthorization 
  WHERE username = (
      SELECT username
      FROM tpEmployees
      WHERE lastname = 'yanish');    
    
-- 2. tpTechValues: ALL get changed  
  -- edit syverson values
  @techPerc = .51; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'syverson' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- edit holwerda values
  @techPerc = .49; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'holwerda' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);   
-- 3.
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));   
--/> team longoria ------------------------------------------------------------/>

--< team olson ----------------------------------------------------------------<
-- 1. tpTeamTechs: no changes
-- 2. tpTechValues: no changes
-- 3. tpTeamValues: only ELR (subsequently budget) change, LIKE a normal month
  @teamKey = (
    SELECT teamKey 
    FROM tpTeams WHERE teamName = 'olson' AND thruDate > curDate());  
  @poolPerc = (
    SELECT poolPercentage
    FROM tpTeamValues WHERE teamKey = @teamKey AND thruDate > curDate());  
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));      
--/> team olson ---------------------------------------------------------------/>
-- AND FINALLY, UPDATE tpdata  
    EXECUTE PROCEDURE xfmTpData();
    EXECUTE PROCEDURE todayxfmTpData();  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 
