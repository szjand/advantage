/*
I changed the flat rate wage for Mike Schwan and Josh Heffernan. 
The change went into effect on beginning of this last pay period on 9/26/21. 
I just received approval for all of them yesterday. Thank you!


*/
/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamname IN ('heffernan')
  ORDER BY teamname, firstname  
	
initially, i am thinking, the only thing that needs to change IS techvalues	
the only team affected IS heffernan, AND the budget of 106.09 remains the same

*/
	
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '09/26/2021';
@thru_date = '09/25/2021';
@dept_key = 18;
@elr = 91.46;	

BEGIN TRANSACTION;
TRY

-- heffernan:  24.50 -> 25.70
  @pto_rate = 35.41;
	@tech_key = 7;
  @tech_perc = .24225; -- .2422 -> 25.69, .2423-> 25.71, .24225
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  	
	
-- schwan: 25.50 -> 26.78
  @pto_rate = 43.57;
	@tech_key = 1;
  @tech_perc = .2524; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  

			
	DELETE 
  FROM tpdata
  WHERE techKey IN (1,7)
    AND thedate > @thru_date;
  
  EXECUTE PROCEDURE xfmTpData();

       
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;     	