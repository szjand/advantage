/*
-- view LIKE ben's spreadsheet
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, a.techkey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
ORDER BY teamname, firstname   
*/

at the last minute it was decided that inad ihsan should NOT be on the team
use the orig script AND just change what needs to be changed

actually quick AND dirty may DO it


BEGIN TRANSACTION;
TRY 
-- 1. DELETE ALL team 23 FROM tpData
DELETE FROM tpData WHERE teamkey = 23;
-- 2. DELETE inad ihsan
--techkey 60
--FROM 
--tpTechs
--tpTeamTechs
--tpTechValues
DELETE FROM tpTechValues WHERE techkey = 60;
DELETE FROM tpTeamTechs WHERE techkey = 60;
DELETE FROM tpTechs WHERE techkey = 60;

-- 3.  UPDATE tpTechValues.techTeamPercentage for clayton techkey 58 & shawn tehckey 59
UPDATE tpTechValues
SET techTeamPercentage = .615
WHERE techkey = 59;
UPDATE tpTechValues
SET techTeamPercentage = .385
WHERE techkey = 58;

-- 4. UPDATE teamkey 23 tpTeamValues: census, poolPercentage,budget
UPDATE tpTeamValues
SET census = 2 
WHERE teamkey = 23;

UPDATE tpTeamValues
SET poolPercentage = .2491
WHERE teamkey = 23;

UPDATE tpTeamValues
SET budget = 45.55
WHERE teamkey = 23
  AND fromdate = '03/08/2015';
  
UPDATE tpTeamValues
SET budget = 45.88
WHERE teamkey = 23
  AND fromdate = '03/22/2015';

UPDATE tpTeamValues
SET budget = 46
WHERE teamkey = 23
  AND fromdate = '04/05/2015';  
EXECUTE PROCEDURE xfmTpData();
EXECUTE PROCEDURE todayxfmTpData();  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

