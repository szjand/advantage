/*
-- view LIKE ben's spreadsheet
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
ORDER BY teamname, firstname   

3/20/15
adding a new team comprised of new techs (ie NOT already IN team pay)
ran this 3/20/15 against production
works just fine: no changes to existing teams,
just adds the new team AND backfills their data to the beginning
of the pay period (3/8/15)
*/

DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
-- testing
--  @effDate = '12/28/2014';  
--  @newELR = 89.75;
--  @departmentKey = 18;
-- production
  @effDate = '03/08/2015';  
  @newELR = 91.43; ---------------------------------------------------------*
  @departmentKey = 18;  

BEGIN TRANSACTION;
TRY   
  
TRY 
INSERT INTO tpTeams (departmentKey,teamName,fromDate)
values(@departmentKey, 'Anderson', @effDate);  
CATCH ALL
  RAISE scratchpad(3, __errtext);
END TRY;  

TRY 
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @departmentKey, b.techNumber, a.firstName, a.lastName, a.employeeNumber, @effDate
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE a.lastname in ('anderson', 'ihsan', 'gehrtz')
      AND a.currentrow = true 
      AND a.active = 'active'
      AND b.currentrow = true;  
  -- ALL 3 are already IN tpEmployees with auth to pto only    
  -- employeeAppAuthorization
  -- teamlead
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 100
    AND b.approle = 'teamlead'
    AND a.username = 'sanderson@rydellcars.com';      
  -- techs    
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 100
    AND b.approle = 'technician'
    AND a.username in ('cgehrtz@rydellcars.com','iinad@rydellcars.com'); 
CATCH ALL 
  RAISE scratchpad(3, __errtext);
END TRY;  

TRY 
-- teamtechs
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Anderson');
  @techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'anderson');
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate);
    @techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'anderson');
  
  @techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'ihsan');  
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate);
  
  @techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'gehrtz');  
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate);  
CATCH ALL
  raise scratchpad(4, __errtext);
END TRY;  

TRY 
  -- team values
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'anderson');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .23; ---------------------------------------------------------*
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));        
CATCH ALL
  raise scratchpad(5, __errtext);
END TRY; 

TRY
  -- tech values
  @techPerc = .444; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'anderson' AND thrudate > curdate());  
  @hourlyRate = 28;    
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate); 
  @techPerc = .278; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'ihsan' AND thrudate > curdate());  
  @hourlyRate = 17.5;    
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  @techPerc = .278; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'gehrtz' AND thrudate > curdate());  
  @hourlyRate = 17.5;    
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);    
    
CATCH ALL
  raise scratchpad(6, __errtext);
END TRY; 

EXECUTE PROCEDURE xfmTpData();
EXECUTE PROCEDURE todayxfmTpData();  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    



BEGIN TRANSACTION;
TRY   

EXECUTE PROCEDURE xfmTpData();
EXECUTE PROCEDURE todayxfmTpData();  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    