/*
06/20/22
Jon,

I am sorry but I unfortunately have another rate change that went through the 
Compli side with Kim and was never submitted to you back when it happened in 
March by me. The rate change for Kodey Schuppert should have gone through 
on 3/27/22 and the amount should have been $18.00 per flat rate hour to $22.00 
per flat rate hour. He JUST actually noticed it. We will have to back pay him 
as an adjustment on his payroll  and also get the changed submitted. I am 
not sure if it is too late for this past pay period or not. I have attached 
the main shop payroll spreadsheet.  Sorry to create all this extra work! 
Thanks Jon

Andrew Neumann
*/

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate() 
  AND departmentKey = 18
  AND a.teamkey = 65  -- (team Gehrtz)
ORDER BY teamname, firstname  

*** did the back pay on the spreadsheet, back pay entered AS adjustments
*** on June 24 2022 check

DECLARE @eff_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @department_key integer;
DECLARE @pto_rate double;
DECLARE @census integer;
DECLARE @budget double;
DECLARE @pool_percentage double;
DECLARE @elr integer;
DECLARE @tech_team_percentage double;
DECLARE @new_rate double;
DECLARE @team string;
@department_key = 18;
@thru_date = '06/18/2022';
@eff_date = '06/19/2022';
@elr = 65;

BEGIN TRANSACTION;
TRY 
	@tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'schuppert' AND thrudate > curdate());
	@pto_rate = (SELECT previousHourlyRate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
	@new_rate = 0.2673;
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues(techkey,fromDate,TechTeamPercentage,previousHourlyRate)
  values(@tech_key, @eff_date, @new_rate, @pto_rate);   
	
	DELETE 
  FROM tpdata
  WHERE techkey = @tech_key
    AND thedate > @thru_date;
  
  EXECUTE PROCEDURE xfmTpData();
     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  