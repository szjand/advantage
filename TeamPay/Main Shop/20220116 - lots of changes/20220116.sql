--
-- Effect this Sunday Jan16 2022
-- Gordon David will come off team O�Neil and go to individual flat rate as (Team David).
-- Matt Aamodt will move from individual flat rate Team Aamodt to (Team O�Neil).
-- Dennis McVeigh will move from team O�Neil to individual flat rate (Team Mcveigh).

-- I also see Shawn Anderson still showing up on Team Anderson, 
-- Shawn can come off Team Anderson and put to (hourly). 
-- That team will have Clayton Gehrtz as team leader and be renamed (Team Gehrtz).

/*


SELECT * 
FROM tpteams
WHERE departmentkey = 18
  AND thrudate > curdate()
	AND teamkey = 21

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
 -- AND a.teamkey = 21
ORDER BY teamname, firstname  

*/


/*
for teams of 1, andrew's spreadsheet IS usually wrong
for team values, i make the team budget = tech rate

!!!
zztest no longer works, the ukg link doesn't WORK, have to WORK out the path, 
go through this script one tech at a time IN production 
*/
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;

@eff_date = '01/16/2022';
@thru_date = '01/15/2022';
@dept_key = 18;
@elr = 91.46;
-- @team_key = 21;  -- team oneil
-- @tech_key = 13; 
BEGIN TRANSACTION;
TRY
/*
-- mcveigh, 
	@tech_key = ( -- 11
	  SELECT techkey 
		FROM tptechs 
		WHERE lastname = 'mcveigh' AND thrudate > curdate());
	@team_key = (  --oneil team
	  SELECT teamkey
		FROM tpteams
		WHERE teamname = 'o''neil');
	-- remove mcveigh FROM team oneil
	UPDATE tpteamtechs
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND teamkey = @team_key;
	-- create a new team
	-- SELECT * FROM tpteams WHERE teamname = 'McVeigh'
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, 'McVeigh', @eff_date);
	@team_key = (
	  SELECT teamkey
		FROM tpteams
		WHERE teamname = 'McVeigh'
		  AND thrudate > curdate());
	-- ADD McVeigh to his own team
	INSERT INTO tpteamtechs(teamkey,techkey,fromdate,thrudate)
	values(@team_key, @tech_key, @eff_date, '12/31/9999');
  -- CREATE tpteamvalues record for the new McVeigh tam
	-- SELECT * FROM tpteamvalues WHERE teamkey = 3
	-- budget - 91.46 * census * poolpercentage
	-- so for a team of 1, poolpercentage = techrate/91.46
  INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate,thrudate)
	values(@team_key, 1, 31.5, 0.3444, @eff_date, '12/31/9999');
	-- UPDATE mcveighs techvalues
	-- SELECT * FROM tptechvalues WHERE techkey = 11
	@pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey,fromdate,thrudate,techteampercentage,previoushourlyrate)
	values(@tech_key, @eff_date, '12/31/9999', 1, @pto_rate);

-- gordon david, team gordy, 
	@tech_key = ( -- 16
	  SELECT techkey 
		FROM tptechs 
		WHERE lastname = 'david' AND thrudate > curdate());
	@team_key = (  --oneil team
	  SELECT teamkey
		FROM tpteams
		WHERE teamname = 'o''neil');
	-- remove gordy FROM team oneil
	UPDATE tpteamtechs
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND teamkey = @team_key;
	-- create a new team
	-- SELECT * FROM tpteams
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Gordy', @eff_date);
	@team_key = (
	  SELECT teamkey
		FROM tpteams
		WHERE teamname = 'Gordy');
	-- ADD gordy to his own team
	INSERT INTO tpteamtechs(teamkey,techkey,fromdate,thrudate)
	values(@team_key, @tech_key, @eff_date, '12/31/9999');
  -- CREATE tpteamvalues record for the new gordy tam
	-- SELECT * FROM tpteamvalues
	-- budget - 91.46 * census * poolpercentage
	-- so for a team of 1, poolpercentage = techrate/91.46	
  INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate,thrudate)
	values(@team_key, 1, 20.36, .2226, @eff_date, '12/31/9999');
	-- UPDATE gordy techvalues
--	SELECT * FROM tptechvalues WHERE techkey = 16
	@pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey,fromdate,thrudate,techteampercentage,previoushourlyrate)
	values(@tech_key, @eff_date, '12/31/9999', 1, @pto_rate);	

-- aamodt, CLOSE the team move him to oneil,
-- UPDATE oneil team data AND oneil techvalues
-- SELECT * FROM tpteams WHERE departmentkey = 18	
	@tech_key = ( -- aamodt 90
	  SELECT techkey 
		FROM tptechs 
		WHERE lastname = 'aamodt' AND thrudate > curdate());
	@team_key = (  -- team aamodt 52
	  SELECT teamkey
		FROM tpteams
		WHERE teamname = 'aamodt');
  -- remove matt FROM team aamodt
	UPDATE tpteamtechs
	SET thrudate = @thru_date
	WHERE techkey = @tech_key;
	-- CLOSE team aamodt
	UPDATE tpteams
	SET thrudate = @thru_date
	WHERE teamkey = @team_key
	  AND thrudate > curdate();
	-- change teamkey to oneil
	@team_key = (  -- team oneil 21
	  SELECT teamkey
		FROM tpteams
		WHERE teamname = 'o''neil'
		  AND thrudate > curdate());  		
	-- ADD aamodt to team oneil
	-- SELECT * FROM tpteamtechs WHERE teamkey = 21
	INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
	values(@team_key,@tech_key,@eff_date);
	-- change the team values for team oneil
	-- SELECT * FROM tpteamvalues WHERE teamkey = 21
	UPDATE tpteamvalues
	SET thrudate = @thru_date
	WHERE teamkey = @team_key
	  AND thrudate > curdate();
	INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate)
	values(@team_key, 2, 51.218, 0.28, @eff_date);
	-- AND aamodt techvalues
	-- select * FROM tptechvalues WHERE techkey = 90
	@pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
	INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
	values(@tech_key,@eff_date,0.3485,@pto_rate);
	
	-- UPDATE oneil techvalues, no change IN flat rate, 23.50
	-- select * FROM tptechvalues WHERE techkey = 54
	@tech_key = ( -- oneil 54
	  SELECT techkey 
		FROM tptechs
		WHERE lastname = 'o''neil'
		  AND thrudate > curdate());
		
	@pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
			
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();			
		
	INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
	values(@tech_key,@eff_date,0.4588,@pto_rate);  
*/	
	
-- team anderson
-- SELECT * FROM tpteams WHERE departmentkey = 18 AND thrudate > curdatE()

	@team_key = ( -- 23 anderson
	  SELECT teamkey
		FROM tpteams
		WHERE teamname = 'anderson'
		  AND thrudate > curdate());
  -- remove ALL techs FROM team anderson			
	-- SELECT * FROM tpteamtechs WHERE teamkey = 23
	UPDATE tpteamtechs
	SET thrudate = @thru_date
	WHERE teamkey = @team_key
	  AND thrudate > curdate();
	-- CLOSE team anderson
	UPDATE tpteams
	SET thrudate = @thru_date
	WHERE teamkey = @team_key;
	-- CREATE new team gehrtz
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Gehrtz', @eff_date);
	@team_key = (
	  SELECT teamkey
		FROM tpteams
		WHERE teamname = 'Gehrtz'
		  AND thrudate > curdate());	
	-- assign ALL 3 techs to the new team gehrtz
	-- Schuppert
	@tech_key = (
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'schuppert'
		  AND thrudate > curdate());
	INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
	values(@team_key, @tech_key, @eff_date);
	-- Soberg
	@tech_key = (
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'Soberg'
		  AND thrudate > curdate());
	INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
	values(@team_key, @tech_key, @eff_date);	
	-- Gehrtz
	@tech_key = (
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'Gehrtz'
		  AND thrudate > curdate());
	INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
	values(@team_key, @tech_key, @eff_date);		
	-- CREATE team values for team gehrtz
	-- SELECT * FROM tpteamvalues
	INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate)
	values(@team_key, 3, 82.314, 0.3, @eff_date); 
	-- UPDATE ALL 3 tech techvalues for team gehrtz
	@tech_key = (
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'schuppert'
		  AND thrudate > curdate());	
	-- SELECT * FROM tptechvalues WHERE techkey = 88 AND thrudate > curdate()
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
	values(@tech_key,@eff_date,0.2187,@pto_rate);
	@tech_key = (
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'soberg'
		  AND thrudate > curdate());	
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
	values(@tech_key,@eff_date,0.2187,@pto_rate);	
	@tech_key = (
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'gehrtz'
		  AND thrudate > curdate());	
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
	values(@tech_key,@eff_date,0.3284,@pto_rate);	
		
/**/
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
	
/**/	

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    

/*
select * 
FROM tpdata 
WHERE thedate = curdate()
  AND departmentkey = 18
ORDER BY teamname, lastname
*/	