11/11/19
effective for pay period starting 11/10/19 thru 11/23/19
Carter Ferry off of team Anderson
no other changes

SELECT * FROM tpteams WHERE teamname = 'anderson'
teamkey: 23

SELECT * FROM tptechs WHERE lastname = 'anderson'
techkeys:
         ferry: 80
         gehrtz: 58
         flaat: 73
         anderson: 59
     
/*

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 23
ORDER BY teamname, firstname  
*/


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '11/10/2019';
@thru_date = '11/09/2019';
@dept_key = 18;
@elr = 91.46;
@team_key = 23;
BEGIN TRANSACTION;
TRY
-- remove carter FROM team 
--  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'ferry');
  UPDATE tpTeamTechs
  SET thrudate = '11/09/2019'
  WHERE techkey = 80
    AND teamkey = 23;
     
DELETE 
FROM tpdata
WHERE techkey = 80
  and thedate > '11/09/2019';
  
wait a minute, IF i dont change anything, we should be good
just one less guy on the team, percentages remain the same
should NOT have to change anything ELSE, just take him off the team

SELECT 32.93/109.752 FROM system.iota = .3000
SELECT 25.50/109.752 FROM system.iota = .23234


    
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY; 

--03/01/2020
-- this IS ALL it took to reinstate carter ferry

SELECT *
FROM tpteamtechs
WHERE techkey = 80
  AND teamkey = 23
  
INSERT INTO tpteamtechs (teamkey,techkey,fromdate)
values(23, 80, '03/01/2020');

DELETE 
-- SELECT *
FROM tpdata  
WHERE teamkey = 23
  AND thedate = curdate()
  
EXECUTE PROCEDURE xfmTpData();