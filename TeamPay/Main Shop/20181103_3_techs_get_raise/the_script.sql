/*
Jon,
I have received approval from Jeri and Ben for rate increased for the following techs. 
The pay change starts with this past payroll starting on 10/28. 
The spreadsheet should reflect the changes. 
Sorry I was planning to have this to you last week but I was waiting for approval� 
Let me know if you have questions. Thanks!!

Josh Syverson moves from $19.50 to $22.50 per flat rate hour
Clayton Gehrtz moves from $23.00 to $25.50 per flat rate hour
Gavin Flaat moves from $25.00 to $25.50 per flat rate hour
Andrew Neumann

*/
/*
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND teamName IN ( 'anderson','olson')
ORDER BY teamname, firstname  


*/	

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @dept_key integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @team_key integer;
DECLARE @census integer;
@eff_date = '10/28/2018';
@thru_date = '10/27/2018';
@dept_key = 18;
@elr = 91.46;

BEGIN TRANSACTION;
TRY
/*
to implement raises only
tpTeamValues AND tpTechValues are ALL that need to be changed
*/

-- Josh Syverson moves from $19.50 to $22.50 per flat rate hour: team olson
-- team values
  @team_key = 3;
  @pool_perc = .29;
  @census = 4;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
-- techValues
  -- jay olson
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'olson' 
    AND firstname = 'jay' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2733; -----------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	
  
  -- josh heffernan
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'heffernan' 
    AND firstname = 'josh' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2309; -----------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);  	   
 
   -- joshua syverson
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'syverson' 
    AND firstname = 'joshua' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2121; -----------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
  
   -- michael schwan
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'schwan' 
    AND firstname = 'michael' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2404; -----------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   

-- Clayton Gehrtz moves from $23.00 to $25.50 per flat rate hour: team anderson
-- Gavin Flaat moves from $25.00 to $25.50 per flat rate hour: team flaat 
-- team values
  @team_key = 23;

-- techValues
   -- clayton gehrtz
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gehrtz' 
    AND firstname = 'clayton' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2323; -----------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);  

   -- gavin flaat
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'flaat' 
    AND firstname = 'gavin' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2323; -----------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);     
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @thru_date
	AND teamkey = @team_key;

  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date
	AND teamkey IN (3,23);
		 
  EXECUTE PROCEDURE xfmTpData();  
  EXECUTE PROCEDURE todayxfmTpData();   
    
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    			

