/*
07/20/20
I removed all the technicians that are moving to the Pre-Diagnosis Team 
and made a separate one with the same wages that everyone was previously at. 
I also changed several of the team names to the Heavy technician associated 
with that team as well as changing Team Bev to Alignment team. 
I think I should have everything correct if you could let me know is 
something looks messed up to you please?

this ALL has to be backdated to 7/19/20 AS the effective date

*/
/*
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
--  AND teamName = 'Waldbauer'
ORDER BY teamname, firstname  

team anderon: remove gavin
team girodat: remove mitch
team gray: remove nathan
team olson: remove jay

ALL of whom go on the pre-diagnosis team

team longoria -> alignment team
team olson -> team heffernan
team gray -> team thompson


*/	

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @dept_key integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @team_key integer;
DECLARE @census integer;
@eff_date = '07/19/2020';
@thru_date = '07/18/2020';
@dept_key = 18;
@elr = 91.46;


BEGIN TRANSACTION;
TRY

-- CREATE pre-diagnosis team
INSERT INTO tpteams (departmentkey,teamname,fromdate)
values(18, 'Pre-Diagnosis',@eff_date);
-- rename teams
-- after back AND forthing, renaming rather than closing the existing team
-- AND creating a new team AND ALL the association team value AND team tech changes
UPDATE tpteams
SET teamname = 'Alignment'
WHERE teamname = 'longoria';
UPDATE tpteams
SET teamname = 'Heffernan'
WHERE teamname = 'olson';
UPDATE tpteams
SET teamname = 'Thompson'
WHERE teamname = 'gray';

--*************************************
--dont fucking run it yet
--*************************************

-- team values
  @pool_perc = .22;
  @census = 2;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);

-- techValues
  -- waldbauer
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'waldbauer' 
    AND firstname = 'alex' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .497; 
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	
  
  -- kiefer
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'kiefer'
    AND firstname = 'lucas' AND thrudate > curdate()); 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .4722;
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	   
 
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @thru_date
	AND teamkey = @team_key;
		 
  EXECUTE PROCEDURE xfmTpData();  
  EXECUTE PROCEDURE todayxfmTpData();   
    
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    			

