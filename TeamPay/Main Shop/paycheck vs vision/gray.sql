/*
gray's team are ALL ~ $5 short on their paychecks compared to what their vision page currently says

*/

-- this IS the kim query, for a single person for 2015
-- DO this for ALL current techs, AND JOIN it to paychecks to compare what was
-- actually paid to what vision NOW!!!! shows
SELECT z.*, 
  round([Total Comm Pay]+[Total Bonus Pay]+[Vac PTO Pay]+[Hol Pay], 2) AS [Total Gross Pay]
FROM (
  SELECT thedate, trim(x.lastname) + ', ' + trim(firstName) as Name, 
    x.employeenumber AS [Emp#],
    commRate AS [Comm Rate], 
    round(commHours * teamProf/100, 2) AS [Comm Clock Hours],
    round(round(commHours * teamProf/100, 2) * commRate, 2) AS [Total Comm Pay],
    bonusRate AS [Bonus Rate],
    round(bonusHours * teamProf/100, 2) AS [Bonus Hours],
    round(round(bonusHours * teamProf/100, 2) * bonusRate, 2) AS [Total Bonus Pay], 
    round(ptoRate, 2) AS [Vac PTO Hol Rate],
    vacationHours + ptoHours AS [Vac PTO Hours],
    round(ptoRate, 2) * (vacationHours + ptoHours) AS [Vac PTO Pay],
    holidayHours AS [Hol Hours],
    ptoRate * holidayHours AS [Hol Pay]
  FROM (  
  select d.thedate, d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
    d.bonusRate, d.teamProf, d.flagHours, e.clockHours, e.vacationHours, e.ptoHours, e.holidayHours, 
    e.totalHours,
--    CASE WHEN e.totalHours > 90 THEN 90 else e.clockHours end AS commHours,
--    round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours
    CASE WHEN e.totalHours > 90 THEN e.clockhours - (totalhours - 90) else e.clockHours end AS commHours,
    round(CASE WHEN totalHours > 90 THEN totalHours - 90 ELSE 0 END, 2) AS bonusHours   
  FROM (
    select a.thedate, c.teamname AS Team, lastname, firstname, employeenumber, 
      techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
      2 * round(techtfrrate, 2) AS BonusRate, teamprofpptd AS teamProf, 
      TechFlagHoursPPTD as FlagHours, teamprofpptd AS TeamProf  
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamkey = b.teamkey
    LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    WHERE thedate > '01/01/2015'
      AND a.lastname = 'thompson'
      AND thedate = payperiodend
      AND a.departmentKey = 18) d
  LEFT JOIN (
    select thedate, employeenumber, techclockhourspptd AS clockHours, 
      techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
      techholidayhourspptd AS holidayHours,
      techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamkey = b.teamkey
    LEFT JOIN tpTeams c on b.teamKey = c.teamKey
    WHERE thedate > '01/01/2015'
      AND lastname = 'thompson'
      AND thedate = payperiodend
      AND a.departmentKey = 18) e on d.employeenumber = e.employeenumber AND d.thedate = e.thedate) x) z

ORDER BY thedate DESC 