/*
Thomas Berge termed 06/03/22
he IS on team bear

---------------------------------------------------------------
-- this will have consequences WHEN the team subsequently changes
the team census shows AS 4, though it IS now 3
 just looking for the short cut today
----------------------------------------------------------------

*/

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate() 
  AND departmentKey = 18
  AND a.teamkey = 22 -- (team bear)
ORDER BY teamname, firstname  

DELETE 
-- SELECT * 
FROM tpdata
WHERE techkey = 102
  and thedate > '06/04/2022';
  
UPDATE tpteamtechs
SET thrudate = '06/04/2022'
WHERE techkey = 102
  AND teamkey = 22
	AND thrudate > curdate();
	
EXECUTE PROCEDURE xfmTpData();	

