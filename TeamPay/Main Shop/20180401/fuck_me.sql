/*
somehow last weekend WHILE configuring the UPDATE script that was to go INTO effect on 4/1
i fucking managed to UPDATE the live date
so this payroll IS fucked up
need to unwind the changes

no need to fix team girodat, that was an overdue remooval of a tech termed months ago

team olson AND team gray need to be rolled back

select *
FROM 
SELECT *
from tpteamvalues
WHERE teamkey = 3

SELECT *
FROM tptechvalues
WHERE techkey = 9


SELECT * FROM tpdata WHERE techkey = 1 AND thedate > '03/15/2018' ORDER BY thedate
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
@dept_key = 18;
@elr = 91.46;
BEGIN TRANSACTION;
TRY
-- team olson
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'olson');
  DELETE 
  FROM tpTeamValues
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  UPDATE tpTeamValues
  SET thrudate = '12/31/9999'
  WHERE teamkey = @team_key
    AND thrudate = '03/17/2018';
  -- olson
  @tech_key = (SELECT techkey from tptechs WHERE lastname = 'olson' AND firstname = 'jay');
  -- SELECT * FROM tpTechValues WHERE techkey = 9;
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  UPDATE tpTechValues
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key
    AND thrudate = '03/17/2018';
  -- schwan
  @tech_key = (SELECT techkey from tptechs WHERE lastname = 'schwan');
  -- SELECT * FROM tpTechValues WHERE techkey = 1;
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  UPDATE tpTechValues
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key
    AND thrudate = '03/17/2018';  
  -- syverson
  @tech_key = (SELECT techkey from tptechs WHERE lastname = 'syverson');
  -- SELECT * FROM tpTechValues WHERE techkey = 18;
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  UPDATE tpTechValues
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key
    AND thrudate = '03/17/2018';    
-- heffernan
  @tech_key = (SELECT techkey from tptechs WHERE lastname = 'heffernan');
  -- SELECT * FROM tpTechValues WHERE techkey = 7;
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  UPDATE tpTechValues
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key
    AND thrudate = '03/17/2018';  

-- team gray
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'gray');
  -- SELECT * FROM tpteamvalues WHERE teamkey = 6
  DELETE 
  FROM tpTeamValues
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  UPDATE tpTeamValues
  SET thrudate = '12/31/9999'
  WHERE teamkey = @team_key
    AND thrudate = '03/17/2018';  
	
-- winkler
  @tech_key = (SELECT techkey from tptechs WHERE lastname = 'winkler');
  -- SELECT * FROM tpTechValues WHERE techkey = 51;
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  UPDATE tpTechValues
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key
    AND thrudate = '03/17/2018'; 
	
  -- gray
  @tech_key = (SELECT techkey from tptechs WHERE lastname = 'gray');
  -- SELECT * FROM tpTechValues WHERE techkey = 6;
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  UPDATE tpTechValues
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key
    AND thrudate = '03/17/2018'; 
	
  -- thompson
  @tech_key = (SELECT techkey from tptechs WHERE lastname = 'thompson');
  -- SELECT * FROM tpTechValues WHERE techkey = 13;
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  UPDATE tpTechValues
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key
    AND thrudate = '03/17/2018'; 
	
  -- david
  @tech_key = (SELECT techkey from tptechs WHERE lastname = 'david');
  -- SELECT * FROM tpTechValues WHERE techkey = 16;
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  UPDATE tpTechValues
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key
    AND thrudate = '03/17/2018'; 	
	
		
  DELETE FROM tpData
  WHERE departmentkey = @dept_key
    AND teamkey IN (SELECT teamkey FROM tpteams WHERE teamname IN ('olson','gray'))
	AND thedate > '03/17/2018';
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY; 
