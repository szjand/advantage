/*
The techs that have rate changes are Zach W, Nate G, Wyatt T, Jay O, Mike S, Josh H 
and Josh S. I have already talked with Jeri and Ben and submitted everything through 
Compli for payroll. I was hoping to put it into effect on 4/1/18 with the start of 
new pay period. Let me know if you have questions. Thanks!

select * from tpteams
bunch of raises for team olson, gray.  past time to remove krewson FROM team girodat (10.13/17)

doing this on 3.25 against zztest
need to run against production on 4/1
*/
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey iN (6,3,21)
ORDER BY teamname, firstname  


SELECT * FROM tptechvalues WHERE techkey = 1
SELECT * FROM tpteamvalues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'olson')
select * FROM tpteamtechs WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'girodat')
*/
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '04/01/2018'; -------------------------------------------------------------------------
@thru_date = '03/31/2018'; ------------------------------------------------------------------------
@dept_key = 18;
@elr = 91.46;
BEGIN TRANSACTION;
TRY
/* girodat team got fixed IN the fuck up, no need to DO anything today
-- 1. team girodat remove krewson 
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'krewson');
@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'girodat');
@pool_perc = .28;
-- tptechs
UPDATE tptechs
SET thrudate = @thru_date
WHERE techkey = @tech_key;
--tpTeamTechs
UPDATE tpTeamTechs
SET thrudate = @thru_date
WHERE teamkey = @team_key
  AND techkey = @tech_key 
  AND thrudate > @thru_date;
-- tpTeamValues  
UPDATE tpTeamValues
SET thrudate = @thru_date
WHERE teamkey = @team_key
  AND thrudate > @thru_date;
INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
values(@team_key, 3, 3*@elr*@pool_perc, @pool_perc, @eff_date);
-- tpTechValues
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
-- girodat
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'girodat');
@tech_perc = .4165;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate);  
-- mcveigh
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'mcveigh');
@tech_perc = .2603;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
-- girodat
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'o''neil');
@tech_perc = .2926;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
*/
-- 2. team olson
@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'olson');
@pool_perc = .27;
UPDATE tpTeamValues
SET thrudate = @thru_date
WHERE teamkey = @team_key
  AND thrudate > @thru_date;
INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
values(@team_key, 4, 4*@pool_perc*@elr, @pool_perc, @eff_date);
-- olson
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'olson' AND firstname = 'jay');
@tech_perc = .2936;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate);  
-- schwann
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'schwan');
@tech_perc = .2582;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate);  
-- syverson
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'syverson');
@tech_perc = .1974;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate); 
-- heffernan
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'heffernan');
@tech_perc = .248;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate); 

-- 2. team gray
@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'gray');
@pool_perc = .24;
UPDATE tpTeamValues
SET thrudate = @thru_date
WHERE teamkey = @team_key
  AND thrudate > @thru_date;
INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
values(@team_key, 4, 4*@pool_perc*@elr, @pool_perc, @eff_date);
-- winkler
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'winkler');
@tech_perc = .2293;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate);
-- gray
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'gray');
@tech_perc = .2748;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate);
-- thompson
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'thompson');
@tech_perc = .265;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate);
-- gordon
@tech_key = (SELECT techkey from tptechs WHERE lastname = 'david');
@tech_perc = .2323;
@pto_rate = (
  SELECT previousHourlyRate 
  FROM tpTechValues 
  WHERE techkey = @tech_key 
    AND thrudate > @thru_date);
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND thrudate > @thru_date;
INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyrate)
values (@tech_key, @eff_date, @tech_perc, @pto_rate);  

  DELETE FROM tpData
  WHERE departmentkey = @dept_key
    AND teamkey IN (SELECT teamkey FROM tpteams WHERE teamname IN ('olson','gray'))
	AND thedate > @thru_date;
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY; 


