/*
Jon,

We would like to move Coty Perez from flat rate pay back to his PDQ hourly rate 
he was making before. Ned needs another pit tech for the summer and we do not 
have the work load in the aftermarket department to support 4 techs over 
the summer. He will be moving back to the aftermarket team later this fall 
when the work load increases. I have a payroll change request in with Kim already. 
I would like this to go into effect 6-14-15 if possible. Let me know what you think.

Andrew Neumann
*/
/*
-- current reality
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
ORDER BY teamname, firstname;

only change IS removing Coty Perez FROM team Waldbauer
AND doing the ELR update

*/
DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
DECLARE @thrudate date;

@effDate = '06/14/2015';  
@newELR = 93.10;
@departmentKey = 18;  
@thrudate = '12/31/9999';   

BEGIN TRANSACTION;
TRY

/*
production: assuming this IS being run on Sunday, day 1 of the new pay period,
  the first day of which IS @effDate
  overnight run of tpData generated a row IN tpData for @effDate with the old
  team configs, DELETE that row, it will be repopulated at the END of this script
*/  
--< production -----------------------------------------------------------------<
  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;

-- 1. tpShopValues: new ELR  
    -- CLOSE previous pay period
  UPDATE tpShopValues
  SET thruDate = @effDate - 1
  WHERE thruDate > curdate()
    AND departmentKey = @departmentKey;      
  -- INSERT row for new pay period
  INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
  values(@departmentKey, @effDate, @newELR);    
  
--/> production ----------------------------------------------------------------/>

/* approach on a team BY team basis, only team changing IS Waldbauer*/
--< Waldbauer -----------------------------------------------------------------<
/* remove coty perez */
@teamKey = (SELECT teamkey FROM tpteams WHERE teamname = 'waldbauer');
@techkey = (SELECT techkey FROM tpTechs WHERE lastname = 'perez' AND firstname = 'coty');

-- tpTechs SET thrudate
  UPDATE tpTechs
  SET thruDate = @effDate - 1
  WHERE techkey = @techkey
    AND thrudate = @thrudate;

-- employeeAppAuthorization - remove team pay
  DELETE 
  FROM  employeeAppAuthorization
  WHERE username =  'cperez@rydellcars.com'
    AND appcode = 'tp';
   
-- tpTeamTechs - CLOSE out coty
  UPDATE tpTeamTechs 
  SET thrudate = @effdate - 1 
  WHERE techkey = @techkey
    AND teamkey = @teamkey
    AND thrudate = @thrudate;
  
-- tpTeamValues -- use the code FROM maintenance, ALL teams have new values
--                 because of the new ELR, waldbauer IS NOT unusual, changes
--                 to tpTeamTechs takes care of that team   
-- changes USING @thrudate & @effdate                  
  UPDATE tpTeamValues
  SET thruDate = @effdate - 1
  WHERE thrudate = @thrudate
    AND teamKey IN (
      SELECT teamKey 
      FROM tpTeams
      WHERE departmentKey = @departmentKey);        
  INSERT INTO tpTeamValues (teamKey, Census, Budget, poolPercentage, fromDate)       
  SELECT a.teamKey, b.census, round(b.census*a.poolPercentage * @newELR, 2) as budget, 
    a.poolPercentage, @effDate
  FROM tpTeamValues a
  LEFT JOIN (
    SELECT teamkey, COUNT(*) AS census
    FROM tpTeamTechs 
    WHERE thruDate = @thrudate
    GROUP BY teamkey) b on a.teamKey = b.teamKey	  
  LEFT JOIN (
    SELECT effectiveLaborRate AS ELR
    FROM tpShopValues
    WHERE thrudate = @thrudate
      AND departmentkey = @departmentkey) c on 1 = 1  
  WHERE thrudate = @effdate -1
    AND a.teamKey IN (
      SELECT teamKey
      FROM tpTeams
      WHERE departmentKey = @departmentKey
        AND thrudate = @thrudate);  
         
--tech values change for waldbauer       
  -- waldbauer
  @techkey = (select techkey FROM tptechs WHERE lastname = 'waldbauer');
  @techPerc = .3607; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);     
  -- ortiz
  @techkey = (select techkey FROM tptechs WHERE lastname = 'ortiz');
  @techPerc = .3458; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);   
  -- kiefer
  @techkey = (select techkey FROM tptechs WHERE lastname = 'kiefer');
  @techPerc = .3347; ----------------------------------------------------------*
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());  
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);      
       
-- AND FINALLY, UPDATE tpdata  
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    

