/*
axtman AND duckstad going hourly

SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = (
    SELECT teamkey
	FROM tpteams
	WHERE teamname = 'axtman'
	  AND thrudate > curdate())
ORDER BY teamname, firstname  

*/



DECLARE @thru_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;

BEGIN TRANSACTION;
TRY

@thru_date = '07/16/2022';

@team_key = (
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'axtman'
    AND thrudate > curdate());
		
  UPDATE tpTeams 
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();		

				
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
		
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
		
  DELETE FROM tpData  
  WHERE teamkey = @team_key
    AND thedate >= @thru_date;

-- axtman
@tech_key = (
  SELECT techkey 
	FROM tptechs 
	WHERE lastname = 'axtman' 
	  and firstname = 'dale' 
		AND thrudate > curdate());	
				
  UPDATE tpTechs 
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();		
 
   UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();			
		
-- duckstad
@tech_key = (
  SELECT techkey 
	FROM tptechs 
	WHERE lastname = 'duckstad' 
	  and firstname = 'jared' 
		AND thrudate > curdate());	
				
  UPDATE tpTechs 
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();		
 
   UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();					
				
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   	
