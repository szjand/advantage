/*
WORK against connection zjon:  \\67.135.158.12:6363\advantage\zjon\sco.add 
  a copy of production scotest\sco.add
changes to take effect 01/11/2015

test script AS though changes take effect 12/28/2014, with ELR = 89.75

-- view LIKE ben's spreadsheet
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
--ORDER BY firstname  
ORDER BY teamname, firstname  

No Changes: Waldbauer, Girodat, Olson

so, based on the flow chart (main shop teams.sdr, notes:team pay 11-29-14)
the ORDER of changes are:
1. tpShopValues: ELR
2. tpTeams: new team: Bear
3. tpTechs: new tech: Bear (tpTechs, employeeAppAuthorization)
4. tpTeamTechs: 
     Travis Strandell FROM Gray to Bear
     Kirby Holwerda FROM Longoria to Bear
     Jeff Bear to Bear
5. tpTeamValues: 
     Gray
     Longoria
     Bear (new team)
     AND because of the new ELR, the rest of teams also get a new row
6. tpTechValues: techTeamPercentages change
     Gray:David  
*/
DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
-- testing
--  @effDate = '12/28/2014';  
--  @newELR = 89.75;
--  @departmentKey = 18;
-- production
  @effDate = '06/14/2015';  
  @newELR = 89.86; ---------------------------------------------------------*
  @departmentKey = 18;  
BEGIN TRANSACTION;
TRY    
/**/ -- this portion IS necessary for backfilling the existing payperiod, for testing 
--DELETE FROM tpData WHERE thedate > @effDate;
--DELETE FROM tpShopValues WHERE fromDate = @effDate AND departmentKey = @departmentKey;
--UPDATE tpShopValues SET thruDate = '12/31/9999' WHERE thruDate = @effDate AND departmentKey = @departmentKey;
--DELETE FROM tpTeamValues WHERE fromDate = @effDate AND thruDate = '12/31/9999';
--UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '12/14/2014'; 
/**/
/*
production: assuming this IS being run on Sunday, day 1 of the new pay period,
  the first day of which IS @effDate
  overnight run of tpData generated a row IN tpData for @effDate with the old
  team configs, DELETE that row, it will be repopulated at the END of this script
*/  
--< production -----------------------------------------------------------------<
  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;

-- 1. tpShopValues: new ELR  
    -- CLOSE previous pay period
TRY      
  UPDATE tpShopValues
  SET thruDate = @effDate - 1
  WHERE thruDate > curdate()
    AND departmentKey = @departmentKey;      
  -- INSERT row for new pay period
  INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
  values(@departmentKey, @effDate, @newELR);     
CATCH ALL
  RAISE scratchpad(1, __errtext);
END TRY;    
-- 2. tpTeams: new team Bear
TRY 
INSERT INTO tpTeams (departmentKey,teamName,fromDate)
values(@departmentKey, 'Bear', @effDate);  
CATCH ALL
  RAISE scratchpad(3, __errtext);
END TRY;  
-- 3. tpTechs: Jeff Bear
TRY 
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @departmentKey, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE a.lastname = 'bear'
      AND a.currentrow = true 
      AND a.active = 'active'
      AND b.currentrow = true;  
    
  -- employeeAppAuthorization
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 100
    AND b.approle = 'teamlead'
    AND a.username = 'jbear@rydellcars.com';     
CATCH ALL 
  RAISE scratchpad(3, __errtext);
END TRY;  
 
-- 4. tpTeamTechs: 
--     Travis Strandell FROM Gray to Bear
--     Kirby Holwerda FROM Longoria to Bear
--     Jeff Bear to Bear   
  -- strandell   
TRY  
  @techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'strandell');
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Gray');
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate > curdate();
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Bear');
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate);
  -- holwerda
  @techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'holwerda');
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Longoria');
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate > curdate();
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Bear');
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate);
  -- bear
  @techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'bear');
  INSERT INTO tpTeamTechs (teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate);  
CATCH ALL
  raise scratchpad(4, __errtext);
END TRY;  
--5. tpTeamValues: 
--     Gray
--     Longoria
--     Bear (new team)
-- gray
TRY   
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'gray');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .21; ---------------------------------------------------------*   
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));       
-- longoria
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'longoria');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .174; ---------------------------------------------------------*   
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));    
-- bear
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'bear');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = .21; ---------------------------------------------------------*     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));     
    
-- waldbauer: only ELR change, LIKE a normal month
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'waldbauer');    
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = (
    SELECT poolPercentage
    FROM tpTeamValues WHERE teamKey = @teamKey AND thruDate > curDate());    
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));       
    
-- girodat: only ELR change, LIKE a normal month
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'girodat');    
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = (
    SELECT poolPercentage
    FROM tpTeamValues WHERE teamKey = @teamKey AND thruDate > curDate());    
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));      
    
-- olson: only ELR change, LIKE a normal month
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'olson');    
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());  
  @poolPerc = (
    SELECT poolPercentage
    FROM tpTeamValues WHERE teamKey = @teamKey AND thruDate > curDate());     
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();     
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));          
CATCH ALL
  RAISE scratchpad(5, __errtext);
END TRY;
      
--6. tpTechValues: techTeamPercentages change
-- team gray
  -- gray
TRY   
  @techPerc = .262; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'gray' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  -- thompson
  @techPerc = .268; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'thompson' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- david
  @techPerc = .254; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'david' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  -- winkler
  @techPerc = .197; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'winkler' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
-- team longoria
  -- syverson
  @techPerc = 1.0; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'syverson' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
-- team bear
  -- holwerda
  @techPerc = .274; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'holwerda' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);   
  -- strandell
  @techPerc = .281; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'strandell' AND thrudate > curdate());  
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > curdate());    
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > curDate();  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);   
  -- bear
  @techPerc = .389; /****************************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'bear' AND thrudate > curdate());  
  @hourlyRate = 22;    
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);   
CATCH ALL
  RAISE scratchpad(6, __errtext);
END TRY;                     
-- AND FINALLY, UPDATE tpdata  
    EXECUTE PROCEDURE xfmTpData();
    EXECUTE PROCEDURE todayxfmTpData();  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    