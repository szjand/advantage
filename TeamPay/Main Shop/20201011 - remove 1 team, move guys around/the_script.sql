/*
10/09/20
per andrew, no rates are changeing
adding a couple guys to team pay
removing one team, moving the guys around

kodey schuppert ($17 hourly) & nicholas soberg ($16 hourly) going to team anderson
team thompson gone
gordon david to team girodat
wyatt thompson to team heffernan


*/
/*
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey IN (23,21,6,3)
ORDER BY teamname, firstname  

*/	

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @dept_key integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @team_key integer;
DECLARE @from_team_key integer;
DECLARE @to_team_key integer;
DECLARE @census integer;
@eff_date = '10/11/2020';
@thru_date = '10/10/2020';
@dept_key = 18;
@elr = 91.46;


BEGIN TRANSACTION;
TRY

-- first tpemployees
-- SCHUPPERT, KODEY :: 642 :: 1124400 :: kschuppert@rydellcars.com
-- SOBERG, NICHOLAS :: 654 :: 152964 ::nsoberg@rydellcars.com

  INSERT INTO tpTechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values(18,'642','Kodey','Schuppert','1124400',@eff_date);

  INSERT INTO tpTechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values(18,'654','Nicholas','Soberg','152964',@eff_date);
    
  INSERT INTO employeeappauthorization
  SELECT 'kschuppert@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'dmcveigh@rydellchev.com'
    AND appcode = 'tp';

  INSERT INTO employeeappauthorization
  SELECT 'nsoberg@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'dmcveigh@rydellchev.com'
    AND appcode = 'tp';


-- CLOSE out team thompson 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'thompson');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  UPDATE tpTeams 
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
 	

-- teamtechs
   -- SELECT * FROM tpteamtechs  
  -- remove gordon david FROM team thompson AND put him on team girodat
  @from_team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'thompson' AND thrudate > curdate());
  @to_team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'girodat' AND thrudate > curdate());
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'david' and firstname = 'gordon' AND thrudate > curdate());
  UPDATE tpteamtechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @from_team_key;
  INSERT INTO tpteamtechs (teamkey,techkey,fromdate)
  values(@to_team_key, @tech_key,@eff_date);
  
  -- remove wyatt thompson FROM team thompson AND put him on team heffernan
  @from_team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'thompson' AND thrudate > curdate());
  @to_team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'heffernan' AND thrudate > curdate());
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'thompson' and firstname = 'wyatt' AND thrudate > curdate());
  UPDATE tpteamtechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @from_team_key;
  INSERT INTO tpteamtechs (teamkey,techkey,fromdate)
  values(@to_team_key, @tech_key,@eff_date);

  -- ADD nicholas soberg to team anderson
  @to_team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'anderson' AND thrudate > curdate());
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'soberg' and firstname = 'nicholas' AND thrudate > curdate());
  INSERT INTO tpteamtechs (teamkey,techkey,fromdate)
  values(@to_team_key, @tech_key,@eff_date);

  -- ADD kody schuppert to team anderson
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'schuppert' and firstname = 'kodey' AND thrudate > curdate());
  INSERT INTO tpteamtechs (teamkey,techkey,fromdate)
  values(@to_team_key, @tech_key,@eff_date);

-- teamvalues
-- select * FROM tpteamvalues WHERE thrudate > curdate() AND teamkey = 3
   -- team anderson
   @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'anderson' AND thrudate > curdate());
   UPDATE tpTeamValues
   SET thrudate = @thru_date
   WHERE teamkey = @team_key
     AND thrudate > curdate();
   INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromdate)
   values(@team_key, 4, 109.75, .3, @eff_date);
  
   -- team girodat
   @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'girodat' AND thrudate > curdate());
   UPDATE tpTeamValues
   SET thrudate = @thru_date
   WHERE teamkey = @team_key
     AND thrudate > curdate();
   INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromdate)
   values(@team_key, 4, 102.44, .28, @eff_date);  
  
   -- team heffernan
   @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'heffernan' AND thrudate > curdate());
   UPDATE tpTeamValues
   SET thrudate = @thru_date
   WHERE teamkey = @team_key
     AND thrudate > curdate();
   INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromdate)
   values(@team_key, 4, 106.09, .29, @eff_date);   


-- techvalues, DO it BY teams
  -- anderson: gehrtz, rate needs to be 25.50, 25.50/109.75 = .23235
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gehrtz' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.23235;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
  -- anderson: anderson, rate needs to be 32.93, 32.93/109.75 = .30005
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'anderson' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.30005;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);  
  -- anderson: soberg, rate needs to be 16, 16/109.75 = .1458
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'soberg' AND thrudate > curdate());
   @pto_rate = 16;
   @tech_perc = 0.1458;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);    
  -- anderson: schuppert, rate needs to be 17, 17/109.75 = .1549
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'schuppert' AND thrudate > curdate());
   @pto_rate = 17;
   @tech_perc = 0.1549;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);  

    
  
  -- girodat: girodat, rate needs to be 32, 32/102.44 = .3124
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'girodat' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.3124;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
  -- girodat: david, rate needs to be 20.36, 20.36/102.44 = .19875
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'david' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.19875;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);  
  -- girodat: mcveigh, rate needs to be 20, 20/102.44 = .1952
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'mcveigh' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.1952;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);    
  -- girodat: oneil, rate needs to be 22.48, 22.48/102.44 = .2194
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'o''neil' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.2194;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);    

  -- heffernan: heffernan, rate needs to be 24.5, 24.5/106.09 = .2309
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'heffernan' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.2309;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);

  -- heffernan: schwan, rate needs to be 25.5, 25.5/106.09 = .24036
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'schwan' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.24036;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate); 
     
   
  -- heffernan: syverson, rate needs to be 22.5, 22.5/106.09 = .2121
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'syverson' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.2121;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);   

  
   
  -- heffernan: thompson, rate needs to be 23.27, 23.27/106.09 = .2193
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'thompson' AND thrudate > curdate());
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.2193;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);  
   
    
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @thru_date;
		 
  EXECUTE PROCEDURE xfmTpData();  
--  EXECUTE PROCEDURE todayxfmTpData();   


COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   


