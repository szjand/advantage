-- current values
/*
SELECT a.teamKey, a.teamName, b.census, b.budget, b.poolPercentage, d.lastName, 
  d.techKey, e.techTeamPercentage, budget * techTeamPercentage
FROM tpTeams a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thrudate > curdate()
LEFT JOIN tpTeamTechs c on a.teamKey = c.teamKey
LEFT JOIN tpTechs d on c.techKey = d.techKey  
LEFT JOIN tpTechValues e on d.techKey = e.techKey
  AND e.thruDate > curdate()
WHERE a.thrudate > curdate()
  AND a.departmentKey = 18
*/  

PRACTICE RUN: DO the changes AS though they take effect 03/09

-- these were necesssary to simulate going back to previous payperiod
DELETE FROM tpData WHERE thedate > '03/08/2014';
DELETE FROM tpShopValues WHERE fromDate = '03/09/2014' AND departmentKey = 18;
UPDATE tpShopValues SET thruDate = '12/31/9999' WHERE thruDate = '03/08/2014' AND departmentKey = 18;
DELETE FROM tpTeamValues WHERE fromDate = '03/09/2014' AND thruDate = '12/31/9999';
UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '02/23/2014'; 

Changes:
  ELR changes from 89.48 to 89.75
  Teams:
    remove teams McVeigh, Westerhausen
    ADD teams Waldbauer, Girodat  
  Techs:
    Westerhausen gone
	McVeigh to team Girodat
  ALL tpTechValues for techs on Waldbauer,Girodat change
4/3
  what i did NOT DO was
  girodat & waldbauer need teamlead access (remove technician access)
  mcveigh remove teamlead access, give technician access  
  
3/21/2014 TRY to DO it ALL IN one large TRANSACTION 
  	
DECLARE @effDate date;
DECLARE @lastName string;
DECLARE @teamName string;
DECLARE @techKey integer;
DECLARE @teamKey integer;
DECLARE @perc double;
DECLARE @hourly double;
DECLARE @departmentKey integer;
DECLARE @newELR double;
DECLARE @census integer;
DECLARE @budget double;
BEGIN TRANSACTION;
  TRY 

--  BEGIN TRANSACTION;
  TRY 
    @effDate = '03/23/2014';
	@departmentKey = 18;
	@newELR = 90.08;
-- this IS assuming that this script IS being run on the first day of the new
-- pay period (3/23/14), last night generated a new row IN tpData for 3/23
-- that data needs to be deleted
    DELETE FROM tpData WHERE theDate = curdate();		
-- new teams	
    INSERT INTO tpTeams (departmentKey, teamName, fromDate)
    values (18, 'Waldbauer',@effDate);	
    INSERT INTO tpTeams (departmentKey, teamName, fromDate)
    values (18, 'Girodat',@effDate);	
-- remove teams	
    UPDATE tpTeams 
	SET thruDate = @effDate - 1
	WHERE teamName = 'McVeigh';	
    UPDATE tpTeams 
	SET thruDate = @effDate - 1
	WHERE teamName = 'Westerhausen';	
-- remove techTeamAssignments
-- need to be more specific about which assignment to UPDATE, yanish has been
-- on multiple teams, DO NOT want to UPDATE the assignment that ended on 1/25
-- just the current assignment
-- this IS the snodgrass deal with sequenced constraint
-- at no time can a tech be assignmed to more than one team AND that IS exactly
-- what i broke BY leaving out WHERE thruDate = '12/31/9999' IN the teamTech UPDATE
-- 
    UPDATE tpTeamTechs
    SET thruDate = @effDate - 1
  	WHERE thruDate = '12/31/9999'
        AND teamKey = (
          SELECT teamKey
          FROM tpTeams
          WHERE teamName = 'McVeigh');
    UPDATE tpTeamTechs
    SET thruDate = @effDate - 1
  	WHERE thruDate = '12/31/9999'
        AND teamKey = (
          SELECT teamKey
          FROM tpTeams
          WHERE teamName = 'Westerhausen');		  	
--  COMMIT WORK;
  CATCH ALL
--    ROLLBACK;
    RAISE Updates(1, 'Teams ' + __errtext);
  END TRY;
  
-- assign techs to teams 
-- UPDATE tpTechValues 
--  BEGIN TRANSACTION;
  TRY
--   
-- team waldbauer  
--
    @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Waldbauer');
	@techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'Waldbauer');
	@perc = .3594; /************************/
	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- assign tech to team	
    INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
    values (@teamKey, @techKey, @effDate);
  -- CLOSE current tpTechValues row	
	UPDATE tpTechValues
	SET thruDate = @effDate - 1
	WHERE techKey = @techKey
	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);	
		 
	@techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'Ortiz');
	@perc = .3435; /************************/
	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- assign tech to team	
    INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
    values (@teamKey, @techKey, @effDate);
  -- CLOSE current tpTechValues row	
	UPDATE tpTechValues
	SET thruDate = @effDate - 1
	WHERE techKey = @techKey
	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);	
	 
	@techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'Sigette');
	@perc = .2936; /************************/
	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- assign tech to team	
    INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
    values (@teamKey, @techKey, @effDate);
  -- CLOSE current tpTechValues row	
	UPDATE tpTechValues
	SET thruDate = @effDate - 1
	WHERE techKey = @techKey
	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);	
--		
-- team girodat		
--	
    @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Girodat');
	@techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'Girodat');
	@perc = .3463; /************************/
	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- assign tech to team	
    INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
    values (@teamKey, @techKey, @effDate);
  -- CLOSE current tpTechValues row	
	UPDATE tpTechValues
	SET thruDate = @effDate - 1
	WHERE techKey = @techKey
	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);	
			
	@techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'McVeigh');
	@perc = .2056; /************************/
	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- assign tech to team	
    INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
    values (@teamKey, @techKey, @effDate);
  -- CLOSE current tpTechValues row	
	UPDATE tpTechValues
	SET thruDate = @effDate - 1
	WHERE techKey = @techKey
	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);			
	
	@techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'Koller');
	@perc = .2327; /************************/
	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- assign tech to team	
    INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
    values (@teamKey, @techKey, @effDate);
  -- CLOSE current tpTechValues row	
	UPDATE tpTechValues
	SET thruDate = @effDate - 1
	WHERE techKey = @techKey
	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);	
		
	@techKey = (SELECT techKey FROM tpTechs WHERE lastname = 'Winfield');
	@perc = .211; /************************/
	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- assign tech to team	
    INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
    values (@teamKey, @techKey, @effDate);
  -- CLOSE current tpTechValues row	
	UPDATE tpTechValues
	SET thruDate = @effDate - 1
	WHERE techKey = @techKey
	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);			
--  COMMIT WORK;
  CATCH ALL
--    ROLLBACK;
    RAISE Updates(2, 'TeamTechs ' + __errtext);
  END TRY;  
  
--  BEGIN TRANSACTION;
  TRY
  -- shop values
    -- CLOSE previous pay period
    UPDATE tpShopValues
    SET thruDate = @effDate - 1
    WHERE thrudate = '12/31/9999'
   AND departmentKey = @departmentKey;       
    -- INSERT row for new pay period
    INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
    values(@departmentKey, @effDate, @newELR);   
--  COMMIT WORK;
  CATCH ALL
--    ROLLBACK;
    RAISE Updates(3, 'shop values ' + __errtext);
  END TRY;   

--  BEGIN TRANSACTION;
  TRY
  -- teamValues NOT only the ELR, but census & pool percentage
  -- so need to DO the census/perc deal for the new teams, THEN
  -- can UPDATE ALL main shop teams budget (census * elr * poopercent)
  -- duh, there IS no teamValues row for the new teams yet
  
  -- team value rows for the new teams
    @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Waldbauer');
	@perc = .191;
    @census = (
      SELECT COUNT(*) 
      FROM tpTeamTechs 
      WHERE teamKey = @teamKey 
        AND @effDate < thruDate);	
    @budget = round(@census * @perc * @newELR, 2);
    INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
    values (@teamKey, @effDate, @census, @perc, @budget);	
		
    @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Girodat');
	@perc = .231;
    @census = (
      SELECT COUNT(*) 
      FROM tpTeamTechs 
      WHERE teamKey = @teamKey 
        AND @effDate < thruDate);	
    @budget = round(@census * @perc * @newELR, 2);
    INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
    values (@teamKey, @effDate, @census, @perc, @budget);			
	
  -- UPDATE teamValues for the 4 teams that are unchanged but for budget
    @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Duckstad');
    @perc = (
      SELECT poolPercentage
      FROM tpTeamValues
      WHERE teamKey = @teamKey
        AND thruDate = '12/31/9999');
    @census = (
      SELECT COUNT(*) 
      FROM tpTeamTechs 
      WHERE teamKey = @teamKey 
        AND @effDate < thruDate);
    @budget = round(@census * @perc * @newELR, 2);	
    UPDATE tpTeamValues
    SET thruDate = @effDate - 1
    WHERE teamKey = @teamKey
      AND thrudate = '12/31/9999'; 
    INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
    values (@teamKey, @effDate, @census, @perc, @budget); 
  
    @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Longoria');
    @perc = (
      SELECT poolPercentage
      FROM tpTeamValues
      WHERE teamKey = @teamKey
        AND thruDate = '12/31/9999');
    @census = (
      SELECT COUNT(*) 
      FROM tpTeamTechs 
      WHERE teamKey = @teamKey 
        AND @effDate < thruDate);
    @budget = round(@census * @perc * @newELR, 2);	
    UPDATE tpTeamValues
    SET thruDate = @effDate - 1
    WHERE teamKey = @teamKey
      AND thrudate = '12/31/9999'; 
    INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
    values (@teamKey, @effDate, @census, @perc, @budget);  
	
    @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Gray');
    @perc = (
      SELECT poolPercentage
      FROM tpTeamValues
      WHERE teamKey = @teamKey
        AND thruDate = '12/31/9999');
    @census = (
      SELECT COUNT(*) 
      FROM tpTeamTechs 
      WHERE teamKey = @teamKey 
        AND @effDate < thruDate);
    @budget = round(@census * @perc * @newELR, 2);	
    UPDATE tpTeamValues
    SET thruDate = @effDate - 1
    WHERE teamKey = @teamKey
      AND thrudate = '12/31/9999'; 
    INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
    values (@teamKey, @effDate, @census, @perc, @budget);	
	
    @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'Olson');
    @perc = (
      SELECT poolPercentage
      FROM tpTeamValues
      WHERE teamKey = @teamKey
        AND thruDate = '12/31/9999');
    @census = (
      SELECT COUNT(*) 
      FROM tpTeamTechs 
      WHERE teamKey = @teamKey 
        AND @effDate < thruDate);
    @budget = round(@census * @perc * @newELR, 2);	
    UPDATE tpTeamValues
    SET thruDate = @effDate - 1
    WHERE teamKey = @teamKey
      AND thrudate = '12/31/9999'; 
    INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
    values (@teamKey, @effDate, @census, @perc, @budget);	
--  COMMIT WORK;
  CATCH ALL
--    ROLLBACK;
    RAISE Updates(4, 'team values ' + __errtext);
  END TRY;  

-- AND FINALLY, UPDATE tpdata  
--  BEGIN TRANSACTION;
  TRY  
    EXECUTE PROCEDURE xfmTpData();
    EXECUTE PROCEDURE todayxfmTpData();  
--  COMMIT WORK;
  CATCH ALL
--    ROLLBACK;
    RAISE Updates(5, 'data ' + __errtext);
  END TRY;    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 

  
-- 4/13/14
BEGIN TRANSACTION;
TRY 
-- girodat
DELETE 
FROM employeeappauthorization
WHERE username = 'jgirodat@rydellchev.com'
  AND appcode = 'tp';
  
INSERT INTO employeeAppAuthorization
SELECT userName, appName, appSeq, appCode, appRole, functionality, 18
FROM tpEmployees a, applicationMetadata b
WHERE b.appcode = 'tp'
  AND b.appseq = 100
  AND b.approle = 'teamlead'
  AND a.username = 'jgirodat@rydellchev.com';
  
-- waldbauer 
DELETE 
-- SELECT *
FROM employeeappauthorization
WHERE username = 'awaldbauer@rydellchev.com'
  AND appcode = 'tp';
  
INSERT INTO employeeAppAuthorization
SELECT userName, appName, appSeq, appCode, appRole, functionality, 18
FROM tpEmployees a, applicationMetadata b
WHERE b.appcode = 'tp'
  AND b.appseq = 100
  AND b.approle = 'teamlead'
  AND a.username = 'awaldbauer@rydellchev.com'; 
  
-- mcveigh  
DELETE 
-- SELECT *
FROM employeeappauthorization
WHERE username = 'dmcveigh@rydellchev.com'
  AND appcode = 'tp';
  
INSERT INTO employeeAppAuthorization
SELECT userName, appName, appSeq, appCode, appRole, functionality, 18
FROM tpEmployees a, applicationMetadata b
WHERE b.appcode = 'tp'
  AND b.appseq = 100
  AND b.approle = 'technician'
  AND a.username = 'dmcveigh@rydellchev.com';  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    
   
  
  