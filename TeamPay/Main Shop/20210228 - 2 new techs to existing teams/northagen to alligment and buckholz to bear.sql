
/*
I t should be just Josh Northagen and Zach Buckholz that we will be moving to 
flat rate so the spreadsheet reflects those changes. 
I see I still have Jeff Girodat on here which I will remove so it is 
correct going forward. Effective date would be 2-28-21. TY


select * FROM tpteams WHERE departmentkey = 18 AND thrudate > curdate()
-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey IN (22,8)
  ORDER BY teamname, firstname  
 

*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '02/28/2021';
@thru_date = '02/27/2021';
@dept_key = 18;
@elr = 91.46;

BEGIN TRANSACTION;
TRY


/*	
-- first tpemployees
select * FROM tpemployees WHERE lastname IN ('northagen','buckholz')
joshua northagen 168753  jnorthagen@rydellcars.com
zachary buckholz 155477  zbuckholz@rydellcars.com
SELECT * FROM dds.dimtech where employeenumber = '155477'
select * FROM tptechs WHERE employeenumber IN ('168753','155477')
SELECT * FROM employeeappauthorization WHERE username IN ('jnorthagen@rydellcars.com','zbuckholz@rydellcars.com')
*/  
  INSERT INTO tpTechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values(18,'306','Zachary','Buckholz','155477','01/04/2021');

  INSERT INTO employeeappauthorization
  SELECT 'zbuckholz@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'jnorthagen@rydellcars.com'
    AND appcode = 'tp';	
	
-- lets DO allignment first	
  @team_key = 8;
  @census = 3;
  @pool_perc = .215;
-- team techs
-- first the new guy
  @tech_key = (
  SELECT techkey FROM tptechs
  WHERE lastname = 'northagen'
    AND firstname = 'joshua'
    AND thrudate > curdate()); 
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);	
-- team values
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);     
-- tech values     
  @pto_rate = 20;
  @tech_perc = .339; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- holwerda
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'holwerda' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());  
  @tech_perc = .3033; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- goodoien
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'goodoien' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());  
  @tech_perc = .2797; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    

  
-- team bear
  @team_key = 22;
  @census = 3;
  @pool_perc = .23;  
-- first the new guy
  @tech_key = (
  SELECT techkey FROM tptechs
  WHERE lastname = 'buckholz'
    AND firstname = 'zachary'
    AND thrudate > curdate()); 
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);	
-- team values
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);      
-- tech values     
  @pto_rate = 15;
  @tech_perc = .2377; -----------------------------------------   
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);     
  -- bear
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'bear' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());  
  @tech_perc = .3785; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  
  -- grant
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'grant' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());  
  @tech_perc = .2659; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);      
  
  DELETE 
  FROM tpdata
  WHERE teamkey IN (22,8)
    AND thedate > @thru_date;
  
  EXECUTE PROCEDURE xfmTpData();

       
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;     