/*
Jon,

Are you able to get Gavin Flaat switched over to flat rate for the pay period starting on Sunday?
(team Anderson)bv
Andrew Neumann

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 23
ORDER BY teamname, firstname  


select *
FROM (
  select a.teamname, a.teamkey, COUNT(*)
  FROM tpTeams a
  INNER JOIN tpTeamTechs b on a.teamkey = b.teamkey
    AND b.thrudate > curdate()
  WHERE a.departmentkey = 18  
  GROUP BY a.teamname, a.teamkey) c
LEFT JOIN tpTeamValues d on c.teamkey = d.teamkey
  AND d.thrudate > curdate() 

*/

-- don't forget to subscribe new techs to teampay
-- AND unsubscribe 
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '12/11/2016';
@thru_date = '12/10/2016';
@dept_key = 18;
@elr = 91.46;
@team_key = 23;
BEGIN TRANSACTION;
TRY
-- new techs:
-- gavin flaat
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,
    fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber,
    @eff_date
  FROM dds.edwEmployeeDim a
  LEFT JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'flaat'
    AND a.firstname = 'gavin'
    AND a.currentrow = true;
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'flaat'  
    AND firstname = 'gavin' AND thrudate > curdate());  
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date); 
     
-- teamValues
  @pool_perc = .26;
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key
    AND thrudate > curdate());
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
    values (@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
-- techValues
  -- Anderson
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'anderson' -----------
    AND firstname = 'shawn' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .4138; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  -- gehrtz
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gehrtz' -------------
    AND firstname = 'clayton' AND thrudate > curdate()); ------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .283; -------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  -- flaat
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'flaat' --------------
    AND firstname = 'gavin' AND thrudate > curdate()); --------------------------- 
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2804; -------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   

 
-- don't forget vision subscribe/unsubscribe
  INSERT INTO employeeAppAuthorization 
  SELECT 'gflaat@rydellcars.com', appname, appseq, appcode, approle, 
    functionality, appDepartmentKey
  FROM employeeAppAuthorization
  WHERE username = 'kholwerda@rydellchev.com'
    AND appname IN ('teampay');

BEGIN TRANSACTION;
TRY
    
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey = @team_keyy
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();      

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
         