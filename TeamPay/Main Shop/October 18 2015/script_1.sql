/*
Can you make this adjustment to Clayton on team Shawn, we are giving him a raise, 
goes in to effect start of next pay period.

Thanks,

Ben

Techs	 $91.94 	23.0%
3	Budget	 $63.44 
Shawn	44.4%	 $28.17 
Clayton	32%	 $20.30 
Kyle	23.5%	 $14.91 
*/

DECLARE @teamkey integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @departmentkey integer;
DECLARE @poolPercent double;
DECLARE @fromdate date;
DECLARE @thrudate date;
DECLARE @techkey integer;
@fromdate = '10/18/2015';
@thrudate = '10/17/2015';
@poolPercent = .23;
@departmentkey = 18;
@teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'Anderson');
@census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @teamkey AND thrudate > curdate());
@elr = (SELECT effectiveLaborRate FROM tpShopValues WHERE thrudate > curdate() AND departmentkey = @departmentkey);
BEGIN TRANSACTION;
TRY
  UPDATE tpTeamValues
--  SELECT * FROM tpteamValues
  SET poolPercentage = @poolPercent,
      budget = round(@census * @elr * @poolPercent, 2)
  WHERE teamkey = @teamkey
    AND thrudate > curdate();
    
  UPDATE tptechvalues  
  SET thrudate = @thrudate
  WHERE techkey IN (
    SELECT techkey
    FROM tptechs
    WHERE lastname IN ('anderson','gehrtz','grochow')
      AND thrudate > curdate())
  AND thrudate > curdate();
  
  @techkey = (SELECT techkey FROM tptechs WHERE lastname = 'anderson' AND thrudate > curdate());   
  INSERT INTO tptechvalues (techkey,fromdate,thrudate,techteampercentage,previoushourlyrate)
  SELECT @techkey, @fromdate, '12/31/9999', .444, previoushourlyrate
  FROM tptechvalues
  WHERE techkey = @techkey
    AND thrudate = @thrudate;
   
  @techkey = (SELECT techkey FROM tptechs WHERE lastname = 'gehrtz' AND thrudate > curdate());   
  INSERT INTO tptechvalues (techkey,fromdate,thrudate,techteampercentage,previoushourlyrate)
  SELECT @techkey, @fromdate, '12/31/9999', .32, previoushourlyrate
  FROM tptechvalues
  WHERE techkey = @techkey
    AND thrudate = @thrudate;
    
  @techkey = (SELECT techkey FROM tptechs WHERE lastname = 'grochow' AND thrudate > curdate());   
  INSERT INTO tptechvalues (techkey,fromdate,thrudate,techteampercentage,previoushourlyrate)
  SELECT @techkey, @fromdate, '12/31/9999', .235, previoushourlyrate
  FROM tptechvalues
  WHERE techkey = @techkey
    AND thrudate = @thrudate;  
COMMIT;  
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  