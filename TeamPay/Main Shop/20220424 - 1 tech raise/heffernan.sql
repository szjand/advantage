/*
I fucked up and didnt send you the flat rate adjustment for Josh Heffernan 
that went into effect 3/27. I am going to have to back pay him the amount we 
shorted him since then. How difficult is it to determine what that amount is? 
I can have Kim add it to his check as a bonus. I mistakenly thought that once 
we shifted over to UKG that we didnt have to use this spreadsheet anymore


SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate() 
  AND departmentKey = 18
  AND a.teamkey = 3  -- (team heffernan)
ORDER BY teamname, firstname  

SELECT * FROM tpteams ORDER BY teamname teamkey 3
SELECT * FROM tptechs WHERE lastname = 'heffernan'  techkey 7

SELECT * FROM tpteamvalues WHERE teamkey = 3 AND thrudate > curdate()
SELECT * FROM tptechvalues WHERE techkey = 7 AND thrudate > curdate()
heffernan FROM 24.225% ($25.70) to 25.47% (27.02) no other chganges

SELECT 27.02/106.09 FROM system.iota
*/


DECLARE @eff_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @department_key integer;
DECLARE @pto_rate double;
DECLARE @census integer;
DECLARE @budget double;
DECLARE @pool_percentage double;
DECLARE @elr integer;
DECLARE @tech_team_percentage double;
DECLARE @new_rate double;
DECLARE @team string;
@department_key = 13;
@thru_date = '04/23/2022';
@eff_date = '04/24/2022';
@elr = 60;

BEGIN TRANSACTION;
TRY 
	@tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'heffernan' AND thrudate > curdate());
	@pto_rate = (SELECT previousHourlyRate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
	@new_rate = 0.2547;
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues(techkey,fromDate,TechTeamPercentage,previousHourlyRate)
  values(@tech_key, @eff_date, @new_rate, @pto_rate);   
	
	DELETE 
  FROM tpdata
  WHERE techkey = @tech_key
    AND thedate > @thru_date;
  
  EXECUTE PROCEDURE xfmTpData();
     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   


SELECT * FROM tpteamtechs WHERE teamkey = 32
back pay:
		 10/24 - 11/06  107.12 comm hrs  1.9 bonus hrs 
		 90hrs * 119.02% = 107.118 comm hrs * 23.27 = 2492.63586
		                                    * 24.2  = 2592.2556  reg time diff: 99.62
		 1.6hrs * 119.02% = 1.90432 bonus hrs * 46.54 = 88.627
		 1.6hrs * 119.02% = 1.90432 bonus hrs * 48.40 = 92.1691  bonus diff: 3.54
		                                                         total diff: 103.16
																														 
select * FROM tptechvalues WHERE techkey = 7 ORDER BY fromdate desc																														 
back pay
		03/26 -> 04/09
		04/10 -> 04/23
		
SELECT * FROM tpdata WHERE thedate = '04/09/2022' AND lastname = 'heffernan' 		
comm_hours: 90
						select 111.31 - (111.31 - 90) FROM system.iota
bonus_hours: 21.31
						SELECT 111.31 - 90 FROM system.iota		
old comm_pay: 3405.47
						select 1.4723 * 90 * 25.7003 FROM system.iota 	
old bonus_pay: 1612.68
						select 1.4723 * 21.31 * 51.4006 FROM system.iota 					
new comm_pay: 3580.34		
new bonus_pay: 1695.49		
SELECT (3580.34 + 1695.49) - (3405.47 + 1612.68) FROM system.iota	
back pay: 257.68           
						
SELECT * FROM tpdata WHERE thedate = '04/23/2022' AND lastname = 'heffernan' 						\
team prof: 1.1632
tech clock: 96.02
old_comm_pay: 2690.51 -- select 1.1632 * 90 * 25.7003 FROM system.iota
old_bonus_pay: 359.93 -- SELECT 1.1632 * 6.02 * 51.4006 FROM system.iota
new_comm_pay: 2828.67 -- SELECT 1.1632 * 90 * 27.02 FROM system.iota
new_bonus_pay: 378.41 --SELECT 1.1632 * 6.02 * 54.04 FROM system.iota
back pay: 156.64  -- SELECT (2828.67 + 378.41) - (2690.51 + 359.93) FROM system.iota