/*
07/20/20
I removed all the technicians that are moving to the Pre-Diagnosis Team 
and made a separate one with the same wages that everyone was previously at. 
I also changed several of the team names to the Heavy technician associated 
with that team as well as changing Team Bev to Alignment team. 
I think I should have everything correct if you could let me know is 
something looks messed up to you please?

this ALL has to be backdated to 7/19/20 AS the effective date

*/
/*
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
--  AND a.teamkey IN (23,21,6,8,3,58)
ORDER BY teamname, firstname  

team anderon: remove gavin
team girodat: remove mitch
team gray: remove nathan
team olson: remove jay

ALL of whom go on the pre-diagnosis team

team longoria -> alignment team
team olson -> team heffernan
team gray -> team thompson

fucking andrew added THE 2 new techs the to spreadsheet but doesn't want them
on flat rate yet

SELECT * FROM tpteams
*/	

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @dept_key integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @team_key integer;
DECLARE @census integer;
@eff_date = '07/19/2020';
@thru_date = '07/18/2020';
@dept_key = 18;
@elr = 91.46;


BEGIN TRANSACTION;
TRY

-- teams
  -- CREATE pre-diagnosis team
  INSERT INTO tpteams (departmentkey,teamname,fromdate)
  values(18, 'Pre-Diagnosis',@eff_date);
  -- rename teams
  -- after back AND forthing, renaming rather than closing the existing team
  -- AND creating a new team AND ALL the association team value AND team tech changes
  UPDATE tpteams
  SET teamname = 'Alignment'
  WHERE teamname = 'longoria';
  UPDATE tpteams
  SET teamname = 'Heffernan'
  WHERE teamname = 'olson';
  UPDATE tpteams
  SET teamname = 'Thompson'
  WHERE teamname = 'gray';
 
/*
-- techs
  -- Nicholas Soberg added to team pay
  INSERT INTO tptechs(departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values(18,'654', 'Nicholas', 'Soberg', '152964', @eff_date);
  -- Kodey Schuppert added to team pay
  INSERT INTO tptechs(departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values(18,'642', 'Kodey', 'Schuppert', '1124400', @eff_date);
*/   

-- teamtechs
-- SELECT * FROM tpteamtechs  
  -- Heffernan (olson)
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'heffernan' AND thrudate > curdate());
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'olson' and firstname = 'jay' AND thrudate > curdate());
  UPDATE tpteamtechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key;
  -- thompson (gray)
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'thompson' AND thrudate > curdate());
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gray' AND thrudate > curdate());
  UPDATE tpteamtechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key;
  -- anderson (flaat)
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'anderson' AND thrudate > curdate());
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'flaat' AND thrudate > curdate());
  UPDATE tpteamtechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key;	

  -- girodat
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'girodat' AND thrudate > curdate());
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'rogers' AND thrudate > curdate());
  UPDATE tpteamtechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key
	AND thrudate > curdate();	
	
  -- pre-diagnose
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'pre-diagnosis' AND thrudate > curdate());
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'olson' and firstname = 'jay' AND thrudate > curdate());
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gray' AND thrudate > curdate());
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'flaat' AND thrudate > curdate());
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'rogers' AND thrudate > curdate());
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);     
  
 
-- teamvalues
-- select * FROM tpteamvalues WHERE thrudate > curdate() ORDER BY teamkey
-- so i can't use the numbers FROM the spreadsheet for teams thompson AND anderson
  -- Heffernan (olson)
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'heffernan' AND thrudate > curdate());
  UPDATE tpteamvalues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,3,79.57,0.29,@eff_date);
  -- thompson (gray)
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'thompson' AND thrudate > curdate());  
  UPDATE tpteamvalues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();  
  INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,2,65.85,0.24,@eff_date);  
  -- anderson (flaat)
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'anderson' AND thrudate > curdate());  
  UPDATE tpteamvalues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();  
  INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,2,82.31,0.3,@eff_date);    
  -- girodat
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'girodat' AND thrudate > curdate());  
  UPDATE tpteamvalues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();  
  INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,3,76.83,0.28,@eff_date);     
  -- pre-diagnose
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'pre-diagnosis' AND thrudate > curdate());  
  INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,4,109.75,0.3,@eff_date);    

-- techvalues, DO it BY teams
-- SELECT * FROM tptechvalues
-- anderson: gehrtz 58
-- rate needs to be 25.50, 25.50/82.31 = .3098
   @tech_key = 58;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.3098;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- anderson: anderson 59
-- rate needs to be 32.93, 32.93/82.31
   @tech_key = 59;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.4001;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
/*   
-- anderson: schuppert ??
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'schuppert');
   @pto_rate = 17;
   @tech_perc = 0.2065;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key;
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
*/   
-- girodat: girodat 4
   @tech_key = 4;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.4165;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- girodat: oneil 54
   @tech_key = 54;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.2926;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- girodat: mcveigh 11
   @tech_key = 11;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.2603;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- thompson: david 16
-- rate needs to be 20.4,  20.40/65.8500 = .3098
   @tech_key = 16;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.3098;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- thompson: thompson 13
-- rate needs to be 23.27, 23.27/65.85 = .3534
   @tech_key = 13;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.3534;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
/*   
-- thompson: soberg ??
   @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'soberg');
   @pto_rate = 16;
   @tech_perc = 0.243;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key;
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
*/   
-- heffernan: heffernan 7
   @tech_key = 7;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.3079;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();   
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- heffernan: syverson 18
   @tech_key = 18;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.2828;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();   
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- heffernan: schwan 1
   @tech_key = 1;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.3205;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();   
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- pre-diagnosis: olson 9
   @tech_key = 9;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.2642;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();   
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- pre-diagnosis: gray  6
   @tech_key = 6;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.2199;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- pre-diagnosis: flaat 73
   @tech_key = 73;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.23234;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);
-- pre-diagnosis: rogers: 79
   @tech_key = 79;
   @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
   @tech_perc = 0.1822;
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate > curdate();    
   INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
   values(@tech_key,@eff_date,@tech_perc,@pto_rate);  
   
   

  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @thru_date;
		 
  EXECUTE PROCEDURE xfmTpData();  
--  EXECUTE PROCEDURE todayxfmTpData();   
   

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   