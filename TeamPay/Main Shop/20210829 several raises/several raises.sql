-- I have included the updated spreadsheet with some rate changes. 
-- Clayton Gehrtz, Matt Aamodt, Desiree Grant and Zach Buckholz are all getting 
-- raises and the spreadsheet should reflect the correct %�s. I have all 4 of 
-- these approved through Randy, Ben and Jeri already but just need the vision 
-- reporting to reflect it please. Can you put in effect on 8/29/21 please?

/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamname IN ('aamodt','anderson','bear')
  ORDER BY teamname, firstname  
	
initially, i am thinking, the only thing that needs to change IS techvalues	
that IS true for 2 teams: aamodt & bear
aamodt IS a team of 1, i didnt SET team budget to match the spreadsheet, but
			 rather his rate, so, need to adjust team budget to match the spreadsheet

*/
	
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '08/29/2021';
@thru_date = '08/28/2021';
@dept_key = 18;
@elr = 91.46;	

BEGIN TRANSACTION;
TRY

-- team anderson
-- tech values     
-- gehrtz
  @pto_rate = 49.65;
	@tech_key = 58;
  @tech_perc = .2463; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);   
	
-- team Bear
-- tech values     
-- Grant
  @pto_rate = 18.65;
	@tech_key = 70;
  @tech_perc = .2792; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  	
	
-- Buckholz
  @pto_rate = 15;
	@tech_key = 93;
  @tech_perc = .2536; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	
	
-- team Aamodt	
-- team values
  @team_key = 52;
  @census = 1;
  @pool_perc = .1968;  	
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date); 
	
-- team Aamodt
-- tech values     
-- Aamodt
  @pto_rate = 18.34;
	@tech_key = 90;
  @tech_perc = .9915; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate); 

			
	DELETE 
  FROM tpdata
  WHERE teamkey IN (22, 23, 52)
    AND thedate > @thru_date;
  
  EXECUTE PROCEDURE xfmTpData();

       
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;     	