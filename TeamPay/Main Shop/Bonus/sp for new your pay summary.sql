create PROCEDURE getTpMainShopTechSummary(
  sessionID cichar(50),
  payPeriodIndicator Integer,
  techName CICHAR ( 60 ) OUTPUT,
  teamName CICHAR ( 25 ) OUTPUT,
  commissionRate DOUBLE ( 15 ) OUTPUT,
  ptoRate DOUBLE ( 15 ) OUTPUT,
  bonusRate double(15) output,
  commissionHours DOUBLE ( 15 ) OUTPUT,
  ptoHours DOUBLE ( 15 ) OUTPUT,
  bonusHours double(15) output, 
  commissionPay DOUBLE ( 15 ) OUTPUT,
  ptoPay DOUBLE ( 15 ) OUTPUT,
  bonusPay double(15) output,
  teamFlagHours DOUBLE ( 15 ) OUTPUT,
  teamClockHours DOUBLE ( 15 ) OUTPUT,
  teamProficiency DOUBLE ( 15 ) OUTPUT,
  totalGrossPay DOUBLE ( 15 ) OUTPUT) 
BEGIN   
/*
offshoot FROM sp.GetTpTechSummary, with the redefinition of terms AND adding
of bonus, seems LIKE main shop needs to be a separate call FROM body shop 
(which can still use sp.GetTpTechSummary)

-- 9/16
this query IS always used to generate pay data for an individual for a pay period
IF it IS the current incomplete pay period, the data IS to date
what should the person identifier be?
what it IS currently doing (AND i question whether this IS good OR good enuf) IS:
input parameter IS username, so
tpEmployees.userName -> tpEmployees.employeeNumber -> 
  tpTechs.employeeNumber -> tpEmployees.techKey ALL of which IS one loosey-goosey
  relationship chain
ahh fuck, good enuf (barely)
  because no enforcement of questions LIKE:
  tpEmployees: PK IS username, can a username emp# change?
  tpTechs: PK: techKey
           NK: (unique)techNumber, departmentKey 
		     can the employeeNumber change for a NK?		 
added unique indexes on tpEmployees.employeeNumber AND tpTechs.employeeNumber
enuf FOR now		 
get on with it:			 
DO a base TABLE to get ALL the "hours" figures

9/29/14
fixed bonus, figuring bonus hours AND commission hours
see Intranet SQL Scripts\TeamPay\payroll\Main Shop payroll summary for managers.sql

changed input parameter to sessionid per greg's request

EXECUTE PROCEDURE getTpMainShopTechSummary('jayolson@rydellchev.com', 0);

EXECUTE PROCEDURE getTpMainShopTechSummary((
  SELECT top 1 sessionid 
  FROM sessions 
  WHERE username = 'jayolson@rydellchev.com' 
    AND validthruts > now() 
    ORDER BY validthruts desc), -1);
*/

DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @UserName cichar(50);
DECLARE @employeeNumber string;
DECLARE @departmentKey integer;
DECLARE @techKey integer;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq) + (SELECT payPeriodIndicator FROM __input);
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
-- @UserName = (SELECT UserName FROM __input);
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@employeenumber = (
  SELECT employeeNumber 
  FROM tpEmployees 
  WHERE username = @username);
@departmentKey = (
  SELECT departmentKey 
  FROM tpTechs
  WHERE employeenumber = @employeeNumber);
@techKey = (
  SELECT techKey
  FROM tpTechs
  WHERE employeeNumber = @employeeNumber);  
INSERT INTO __output
SELECT techName, teamName, commissionRate, ptoRate, bonusRate,
  commissionHours, ptoHours, bonusHours,
  round(commissionHours * teamProficiency/100 * commissionRate, 2) AS commissionPay, 
  round(ptoRate * ptoHours, 2) AS ptoPay,
  round(bonusHours * teamProficiency/100 * bonusRate, 2) AS bonusPay,
  teamFlagHours, teamClockHours, 
  teamProficiency/100, -- greg needed it AS a decimal (NOT * 100)
  round(commissionHours * teamProficiency/100 * commissionRate, 2) + 
    round(ptoRate * ptoHours, 2) +
	round(bonusHours * teamProficiency/100 * bonusRate, 2) AS totalGrossPay
FROM (  
  SELECT techName, teamName, commissionRate, ptoRate, bonusRate,
    CASE WHEN totalHours > 90 THEN clockhours - (totalhours - 90) ELSE clockHours END AS commissionHours,
    ptoHours, 
    CASE WHEN totalHours > 90 THEN round(totalHours - 90, 2) ELSE 0 END AS bonusHours,
    teamFlagHours, teamClockHours, teamProficiency
  FROM (   
    SELECT TRIM(firstName) + ' ' + lastName AS techName, teamName, 
      round(techTFRRate, 2) AS commissionRate,
      techHourlyRate AS ptoRate,
      2 * round(techTFRRate, 2) AS bonusRate,
      techClockHoursPPTD AS clockHours,
      techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours,
      techvacationhourspptd + techptohourspptd + techholidayhourspptd AS ptoHours,
      teamFlagHoursPPTD + teamFlagHourAdjustmentsPPTD AS teamFlagHours,
      teamClockHoursPPTD AS teamClockHours,
      teamProfPPTD AS teamProficiency
    FROM tpdata 
    WHERE thedate = @date
      AND departmentKey = @departmentKey
      AND techKey = @techKey) a) b;

END;
