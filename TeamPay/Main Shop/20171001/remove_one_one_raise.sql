
Jon,

We are moving Ihsan Inad over to the Honda shop because they need the help. 
I updated the spread sheet to include removal of Ihsan from the Midas 
team and I also increased Travis Strandellís wage to $17.89 per flat 
rate hour. Let me know if you have questions. Thanks!

select * from tpteams
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 22
ORDER BY teamname, firstname  

SELECT * FROM tptechs
SELECT * FROM tpteamtechs WHERE teamkey = 22 AND techkey = 63
SELECT * FROM tptechvalues WHERE techkey = 63
SELECT * FROM tpteamvalues WHERE teamkey = 22
SELECT * FROM tptechvalues WHERE techkey = 55
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '10/01/2017';
@thru_date = '09/30/2017';
@dept_key = 18;
@elr = 91.46;
@team_key = 22;  -- team bear
BEGIN TRANSACTION;
TRY
-- 1. expire ihnad, AND his techteam, techvalue
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'inad'); -- 63
  UPDATE tptechs
  SET thrudate = @thru_date 
  WHERE techkey = @tech_key;
  
  SELECT * FROM tpteamtechs WHERE teamkey = 22
  
  UPDATE tpteamtechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND techkey = @tech_key
    AND thrudate = '12/31/9999';
    
  UPDATE tpTechValues
  SET thruDate = @thru_date
  WHERE techkey = @tech_key  
    AND thrudate = '12/31/9999';

-- 2. team values
-- budget = 91.46 * 3 * 21.8(FROM andrew's spreadsheet) = 59.815
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate = '12/31/9999';
    
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromDate)
  values (@team_key,3,59.815, .218, @eff_date);  
-- 3. tech values
  -- bear
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'bear' 
      AND thrudate = '12/31/9999');    
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   -- techteampercentage = 23.89/59.815 (numerator FROM andrew's spreadsheet)
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @eff_date, .3994, @pto_rate);
  -- holwerda   
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'holwerda' 
      AND thrudate = '12/31/9999');    
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @eff_date, .2991, @pto_rate);  
  -- holwerda   
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'strandell' 
      AND thrudate = '12/31/9999');    
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   -- techteampercentage = 17.89/59.815 (numerator FROM andrew's spreadsheet)
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @eff_date, .2991, @pto_rate);    
   
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY; 

