/*

We are moving Kirby effective this upcoming pay period starting on 3-3-19. 
He is moving from Team Jeff to Team Bev in the alignment area. I have hired a 
replacement for Kirby at Midas that will be moving to flat rate 3months from 
his start date pending his performance. 
Can you please make the adjustments for me?
*/

so, need to remove him FROM team bear -> adj rates for ALL remaining bear
    team members to stay the same
AND ADD him to team longoria -> adj zacha to stay the same
    
SELECT * FROM tpteams WHERE teamname = 'bear'  -- 22
census FROM 4 to 3
budget = 79.75312
poolPercentage = .218
grant: 16.78
bear:  23.89
strandell: 17.89
total new tech pay = 58.56 SELECT 16.78 + 23.89 + 17.89 FROM system.iota
grant: .2865  SELECT 16.78/58.56 FROM system.iota
bear: .4079 SELECT 23.89/58.56 FROM system.iota
strandell .3054 SELECT 17.89/58.56 FROM system.iota
poolPercentage = budget/(census * elr)
                 SELECT 58.56/(3*91.46) FROM system.iota
                 .21342
                 
SELECT * FROM tpteams WHERE teamname = 'longoria' -- 8
kirby: 0.4798 select 17.89/37.28 FROM system.iota
zacha:  0.5201 select 19.39/37.28 FROM system.iota
budget = 37.28 SELECT 17.89 + 19.39 FROM system.iota
poolPercentage = budget/(census * elr)
                 SELECT 37.28/(2*91.46) FROM system.iota
                 .2038
/*
-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey IN (8, 22)
ORDER BY teamname, firstname  
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '03/03/2019';
@thru_date = '3/02/2019';
@dept_key = 18;
@elr = 91.46;

--@census = 4;
--@pool_perc = 0.3;

BEGIN TRANSACTION;
TRY
-- start with team bear
  @team_key = 22;  -- team bear
  @census = 3;
/*
poolPercentage = budget/(census * elr)
                 SELECT 58.56/(3*91.46) FROM system.iota
                 .21342
*/                   
  @pool_perc = 0.21342;
-- tpTeamTechs
  @tech_key = (
    SELECT techkey FROM tptechs
	  WHERE lastname = 'holwerda'); 
  -- remove kirby 
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND techkey = @tech_key
    AND thrudate > curdate();
    
-- team values

  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);  
  
-- tech values    
  -- grant: .2865  SELECT 16.78/58.56 FROM system.iota
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'grant' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2865; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  
  -- bear: .4079 SELECT 23.89/58.56 FROM system.iota
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'bear' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .4079; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- strandell: .3054 SELECT 17.89/58.56 FROM system.iota
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'strandell' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3054; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  
  
-- team longoria
  @team_key = 8;  -- team bear
  @census = 2;
/*
poolPercentage = budget/(census * elr)
                 SELECT 37.28/(2*91.46) FROM system.iota
                 .2038
*/                   
  @pool_perc = 0.2038;  

-- tpTeamTechs
  @tech_key = (
    SELECT techkey FROM tptechs
	  WHERE lastname = 'holwerda');   
  INSERT INTO tpTeamTechs values(@team_key, @tech_key, @eff_date, '12/31/9999');
  
-- team values
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);    
  
  -- zacha:  0.5201 select 19.39/37.28 FROM system.iota
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'zacha' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .5201; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  
  
  -- kirby: 0.4798 select 17.89/37.28 FROM system.iota
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'holwerda' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .4798; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);     
   
  DELETE FROM tpData
  WHERE departmentkey = @dept_key
    AND thedate >= @thru_date
    AND teamkey IN (8,22);
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   