/*
03/14/2021
    effective 02/28/21, team waldbauer IS no more
	lucas moved to honda (virtually) to WORK on non gm aftermarket for gm vehciles
	both alex AND lucas going on hourly for the mean time



SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = (
    SELECT teamkey
	FROM tpteams
	WHERE teamname = 'waldbauer'
	  AND thrudate > curdate())
ORDER BY teamname, firstname  

*/


DECLARE @thru_date date;
DECLARE @team_key integer;

BEGIN TRANSACTION;
TRY

@thru_date = '02/27/2021';

@team_key = (
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'waldbauer'
    AND thrudate > curdate());
	
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  UPDATE tpTeams 
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();	
	
  DELETE FROM tpData  
  WHERE teamkey = @team_key
    AND thedate >= @thru_date;
		 

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   	

