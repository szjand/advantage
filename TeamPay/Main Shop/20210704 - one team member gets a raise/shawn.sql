/*
Jon,
07/01/21

I have attached the flat rate pay scale spreadsheet and the only change would be the
 $1.00 per flat rate hour increase for Shawn Anderson that will go into effect the pay period beginning 7-4-21. His previous rate increase that was back-dated was already all paid out through Kim. We should be good after this. Randy has the Compli form turned in as well. 

Thank you!



SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 23
ORDER BY teamname, firstname  

no change IN poolpercentage, remains at .3, only thing changing IS Shawn's 
techteampercentage, FROM .3091 to .3182, rate FROM 33.93 to 34.92

so, only change IS to shawns tech values
 
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;

@eff_date = '07/04/2021';
@thru_date = '07/03/2021';
@dept_key = 18;
@elr = 91.46;
@team_key = 23;
@tech_key = 59; 
@pool_perc = .3;
@census = 4;
BEGIN TRANSACTION;
TRY
	
-- techValues
  -- shawn
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3182; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	    
  
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey = @team_key
		AND techkey = @tech_key
    AND thedate >= @eff_date;
    
		
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey = @team_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
	
 

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    
