/*

Moving Carter Ferry to flat rate as of 7/22/18
The only adjustment on this spreadsheet will be the addition of Carter Ferry to 
Team Anderson so they will be now a team of 4. I made the % adjustments so nobody 
else on the team should be changed as far as pay rate. I did send the Compli change 
through to Kim already. I would like this to go into effect 7/22/18 please. 
Thanks!
*/
-- SELECT * FROM tpteams WHERE teamname = 'anderson'  -- 23
/*
-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 23
ORDER BY teamname, firstname  
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '07/22/2018';
@thru_date = '07/21/2018';
@dept_key = 18;
@elr = 91.46;
@team_key = 23;  -- team anderson
@census = 4;
@pool_perc = 0.3;

BEGIN TRANSACTION;
TRY
  DELETE 
  FROM tpdata
  WHERE teamkey = @team_key
    AND thedate > @thru_date;
/*	
select * from dds.edwEmployeeDim WHERE lastname = 'ferry' emp# 159863
select * FROM ptoemployees WHERE employeenumber = '159863'
SELECT * FROM tptechs WHERE lastname = 'ferry'
SELECT * FROM dds.dimtech WHERE employeenumber = '159863'
select * FROM tpemployees WHERE employeenumber = '159863'
select * FROM employeeappauthorization WHERE username = 'cferry@rydellcars.com'
*/
-- tptechs, employeeappauthorization, he already EXISTS IN ptoemployees
  INSERT INTO tpTechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values(18,'285','Carter','Ferry','159863','07/22/2018');

  INSERT INTO employeeappauthorization
  SELECT 'cferry@rydellcars.com', appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'dmcveigh@rydellchev.com'
    AND appcode = 'tp';
-- team techs
  @tech_key = (
    SELECT techkey FROM tptechs
	WHERE lastname = 'ferry'); 
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);	
-- team values
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);  
-- tech values    
  -- anderson
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'anderson' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .3; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  
  -- flaat
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'flaat' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2278; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- gehrtz
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'gehrtz' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2096; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  -- rogers
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'ferry' AND thrudate > curdate()); 
  @pto_rate = 19;
  @tech_perc = .1731; -----------------------------------------   
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   