/*
7/22/22
I have 3 pay rate changes for Josh Northagen, Des Grant and Derrick Goodoien 
that will go into effect on 7/17/22. It has been approved 
by HR, Jeri and Ben as of this morning. 

I will have a couple more next week for Kirby Holwerda and Matt Aamodt. 
With the recent pay changes in PDQ, we are having to move the lower 
paid techs in the main shop up. 

select * FROM tpteams where departmentkey = 18 AND thrudate > curdatE()

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate() 
  AND departmentKey = 18
  AND a.teamkey in(8,22)  -- (alignment,bear)
ORDER BY teamname, firstname  

previously did shortcut tech removal on team bear, need to redo the team now
alignment exceeds current budget, redo  them AS well


for this TRANSACTION to function IN zzTest, DELETE the ukg link
AND re-create it : 

EXECUTE PROCEDURE 
  sp_CreateLink ( 
     'ukg',
     '\\67.135.158.12:6363\advantage\ukg\ukg.add',
     TRUE,
     TRUE,
     TRUE,
     '',
     '');
*/


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;

@eff_date = '07/17/2022';
@thru_date = '07/16/2022';
@dept_key = 18;
@elr = 91.46;

BEGIN TRANSACTION;
TRY
-- team keys
	-- alignment 8
	-- bear 22
-- tech keys
	-- goodoien 87
	-- northagen 84
	-- grant 70
	-- bear 55
	
-- team alignment	
	-- team values
	@team_key = 8;
	@census = 2;
	@pool_perc = .24;
	UPDATE tpteamValues
	SET thrudate = @thru_date
	WHERE teamkey = @team_key
	  AND thrudate > curdate();
  INSERT INTO tpteamValues(teamkey,census,budget,poolpercentage,fromdate)
	values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
	-- techValues
  -- goodoien
	@tech_key = 87;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .4328; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);  
  -- northagen
	@tech_key = 84;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .4875; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);  
	
-- team bear	
	-- team values
	@team_key = 22;
	@census = 2;
	@pool_perc = .24;
	UPDATE tpteamValues
	SET thrudate = @thru_date
	WHERE teamkey = @team_key
	  AND thrudate > curdate();
  INSERT INTO tpteamValues(teamkey,census,budget,poolpercentage,fromdate)
	values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);	
	-- techValues
  -- grant
	@tech_key = 70;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .4328; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);  	
  -- bear
	@tech_key = 55;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .5442; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);	
	
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey IN (8,22)
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
   
	
 

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY; 	