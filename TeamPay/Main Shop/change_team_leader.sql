/*
Team Anderson will now be Team Gehrtz.  Can you give Clayton team access please 
and  make Shawn normal.  Shawn can stay on there for now as he will 
be working a few more days.
*/
select * FROM (
SELECT * 
FROM employeeappauthorization
WHERE username LIKE 'sanderson%'
union
SELECT * 
FROM employeeappauthorization
WHERE username LIKE 'cgehrtz%'
) a WHERE appname = 'teampay'
ORDER BY username


SELECT * FROM tpteams ORDER BY teamname

UPDATE tpteams
SET teamname = 'Gehrtz'
WHERE teamkey = 23
  AND teamname = 'anderson'

userName			 	 					 appName	appSeq	appCode	appRole		 			functionality								appDepartmentKey
cgehrtz@rydellcars.com		 teampay	100			tp			technician			dropdown										18
cgehrtz@rydellcars.com		 teampay	100			tp			technician			how pay works								18
cgehrtz@rydellcars.com		 teampay	100			tp			technician			ro list											18
cgehrtz@rydellcars.com		 teampay	100			tp			technician			tech pay period summary			18
sanderson@rydellcars.com	 teampay	100			tp			teamlead				dropdown										18
sanderson@rydellcars.com	 teampay	100			tp			teamlead				how pay works								18
sanderson@rydellcars.com	 teampay	100			tp			teamlead				ro list											18
sanderson@rydellcars.com	 teampay	100			tp			teamlead				store / team totals					18
sanderson@rydellcars.com	 teampay	100			tp			teamlead				teamlead pay period summary	18

DELETE FROM employeeappauthorization
--select * FROM employeeappauthorization
WHERE username = 'sanderson@rydellcars.com'
  AND functionality in ('store / team totals','teamlead pay period summary')
	AND appname = 'teampay';

UPDATE employeeappauthorization
SET approle = 'technician'
WHERE username = 'sanderson@rydellcars.com'
  AND appname = 'teampay';
	
INSERT INTO employeeappauthorization values('sanderson@rydellcars.com','teampay',100,'tp','technician','tech pay period summary',18);



DELETE FROM employeeappauthorization
--SELECT * FROM employeeappauthorization
WHERE username = 'cgehrtz@rydellcars.com'
  AND functionality = 'tech pay period summary'
	AND appname = 'teampay';
	
UPDATE employeeappauthorization
SET approle = 'teamlead'
WHERE username = 'cgehrtz@rydellcars.com'
  AND appname = 'teampay';
	
INSERT INTO employeeappauthorization values('cgehrtz@rydellcars.com','teampay',100,'tp','teamlead','store / team totals',18);
INSERT INTO employeeappauthorization values('cgehrtz@rydellcars.com','teampay',100,'tp','teamlead','teamlead pay period summary',18);	

UPDATE tpdata
-- SELECT * FROM tpdata
SET teamname = 'Gehrtz'
WHERE thedate = curdate()
  AND teamname = 'anderson'
	
	
	
insert into employeeappauthorization values ('cgehrtz@rydellcars.com',		 'teampay',	100,			'tp',			'technician',			'dropdown',                   18);
insert into employeeappauthorization values ('cgehrtz@rydellcars.com',		 'teampay',	100,		'tp',			'technician',			'how pay works',								18);
insert into employeeappauthorization values ('cgehrtz@rydellcars.com',		 'teampay',	100,		'tp',			'technician',			'ro list',											18);
insert into employeeappauthorization values ('cgehrtz@rydellcars.com',		 'teampay',	100,		'tp',			'technician',			'tech pay period summary',			18);

DELETE FROM employeeappauthorization
--SELECT * FROM employeeappauthorization
WHERE username = 'cgehrtz@rydellcars.com'
	AND appname = 'teampay';