    UPDATE tpdata
    SET TechClockhoursDay = x.clockhours,
        TechVacationHoursDay = x.Vacation,
        TechPTOHoursDay = x.PTO,
        TechHolidayHoursDay = x.Holiday
SELECT * 
    FROM (    
      SELECT b.thedate, c.employeenumber, SUM(clockhours) AS clockhours,
        SUM(coalesce(VacationHours,0)) AS vacation,
        SUM(coalesce(PTOHours,0)) AS pto,
        SUM(coalesce(HolidayHours,0)) AS holiday
      FROM dds.edwClockHoursFact a
      INNER JOIN dds.day b on a.datekey = b.datekey
        AND b.thedate BETWEEN '01/11/2015' AND curdate()
      LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
      GROUP BY b.thedate, c.employeenumber) x 

select *
FROM tpemployees
WHERE employeenumber = '1137595'      


SELECT b.thedate, c.employeenumber, SUM(clockhours) AS clockhours,
  SUM(coalesce(VacationHours,0)) AS vacation,
  SUM(coalesce(PTOHours,0)) AS pto,
  SUM(coalesce(HolidayHours,0)) AS holiday
FROM dds.edwClockHoursFact a
INNER JOIN dds.day b on a.datekey = b.datekey
  AND b.thedate BETWEEN '06/28/2015' AND curdate()
LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
GROUP BY b.thedate, c.employeenumber


need to implement the notion of cutting off payroll, disabling any further updates to
the underlying data
the problem came up AS the gray team noticed that looking at 2 pay periods ago
IN vision, their pay AS shown IN vision was ~$5 more than their pay check for that
pay period ( they did NOT notice that IF they went back 3 pay periods, the 
pay on the vision page was ~$100 less than their pay check for that pay period)
so, after the pay checks have been cut (based on vision data at that point IN time)
something IS subsequently getting changed, whether it IS clock hours OR flag hours,
that changes the pay calculations for previous pay periods.
what the fuck does that look LIKE

SELECT * FROM dds.day WHERE thedate = curdate() -3

date: 7/27
dayinbiweeklypayperiod: 2
biweeklypayperiodsequence: 173
biweeklypayperiodstartdate: 7/26
biweeklypayperiodenddate: 8/8
previous biweeklypayperiodstartdate: 7/12
previous biweeklypayperiodenddate: 7/25

tpdata gets updated for current pay period + 3 days only

curdate() : dayinpp > 3 UPDATE current pp only
          : dayinpp <= 3 UPDATE current AND previous pp only

USING a hard coded 3 days IS iffy, assumes that the payroll will be done on 
  Tuesday, always, every time
  it acknowledges that both shops batch UPDATE stuff LIKE holidays, shop time,
  AND that kind of shit after the payperiod ends, usually on monday
  
another option would be to UPDATE a TABLE with the date on which i send payroll
data to Kim, THEN extract that date AS the cut off date IN the stored proc
        
so, depending on what day IN the pp curdate() IS determines the date range for
scraping clock hours AND flag hours for updateing tpdata

simply need to generate the FROM date, thru date will always be curdate()  

DECLARE @current_date date;
DECLARE @day_in_pp integer;
DECLARE @cur_pp_from_date date;
DECLARE @prev_pp_from_date date;
DECLARE @from_date date;
@current_date = (SELECT curdate() FROM system.iota);
@day_in_pp = (
  SELECT dayinbiWeeklyPayPeriod
  FROM dds.day
  WHERE thedate = @current_date);
@cur_pp_from_date = (
  SELECT biWeeklyPayPeriodStartDate
  FROM dds.day
  WHERE thedate = @current_date);
@prev_pp_from_date = (
  SELECT distinct biWeeklyPayPeriodStartDate
  FROM dds.day
  WHERE biWeeklyPayPeriodSequence = (
    SELECT biWeeklyPayPeriodSequence - 1
    FROM dds.day
    WHERE thedate = @current_date)); 
IF @day_in_pp > 3 THEN
  @from_date = @cur_pp_from_date;
ELSE 
  @from_date = @prev_pp_from_date;
END;     
SELECT @current_date,@day_in_pp,@cur_pp_from_date,@prev_pp_from_date,@from_date 
FROM system.iota;    

-- ahh fuck 
must also SET date ranges IN the updates of pptd attributes, have NOT thought that one out
***************
OR DO i,
none of the granular data will have been changed so updating the entire fucking
TABLE IS NOT an issue except for performance

so the generation of a "cut off" from_date IS good
just need to play with the implementation IN the entire sp (xfmTpData) a bit


-- 7/30
base data for updating clock hours pptd

seems LIKE it should be cool to ADD a.payperiodseq = current payperiodseq

ADD an index AND this IS shitloads faster 

      SELECT a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday, SUM(b.techclockhoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
--        AND a.payperiodseq = 173
        -- OR limit the date range of a
        AND a.thedate BETWEEN '07/12/2015' AND curdatE()
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday
      
am i every updating daily data WHERE the payperiod IS NOT the current pay period
yes, i think so, IF we are IN the first 3 days of the new payperiod      
so, IN the pptd UPDATE, limit the daterange of tpData a