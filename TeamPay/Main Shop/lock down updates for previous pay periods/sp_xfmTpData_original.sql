CREATE PROCEDURE xfmTpData_original
   ( 
   ) 
BEGIN 
/*
2/27/14
  v.2.2.0
    new row
      ADD bodyshop to base values for new row
	  change daterange to 01/12/2014 - curdate()
	  team census/budget: bs mod
3/11/14	  
  added UPDATE date to teamProf to combine body shop paint teams (cody - 18, pete - 19)
  INTO a single proficiency number
  
7/24/14
*a* base new row, SELECT the current techHourlyRate  
*b*: total hack to accomodate cody johnson being on a team 12/28 -> 1/10 but
       his hours (clock & flag) should NOT be part of the team calculations
       had to DO it here because clock AND flaghours overwrite existing past
       data IN tpData
6/30/15
  body shop coming back on to team pay, ALL techs on one team
  flag hours based on CLOSE date NOT flag date
  also took the opportunity to reduce the scrape periods
  	  
EXECUTE PROCEDURE xfmTpData_original();
*/
BEGIN TRANSACTION; -- new row   
TRY
  TRY 
    -- base values for a new row
-- main Shop	
    INSERT INTO tpData(DateKey, TheDate, PayPeriodStart,PayPeriodEnd,PayPeriodSeq,
      DayOfPayPeriod, DayName, 
      DepartmentKey, TechKey, TechNumber, EmployeeNumber, FirstName, LastName)  
    SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      a.biweeklypayperiodsequence, dayinbiweeklypayperiod, a.DayName,
      (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'mainshop' AND dl5 = 'Shop'),
      b.TechKey, b.technumber, b.employeenumber, b.firstname, b.lastname
    FROM dds.day a, tpTechs b 
    WHERE a.thedate BETWEEN '01/11/2015' AND curdate()
	    AND b.departmentKey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'mainshop' AND dl5 = 'Shop')
      AND NOT EXISTS (
        SELECT 1
        FROM tpdata
        WHERE thedate = a.thedate
          AND techkey = b.techkey)
      AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
        SELECT 1
        FROM tpTeamTechs
        WHERE techkey = b.techkey
          AND a.thedate BETWEEN fromdate AND thrudate); 
-- bs	
    INSERT INTO tpData(DateKey, TheDate, PayPeriodStart,PayPeriodEnd,PayPeriodSeq,
      DayOfPayPeriod, DayName, 
      DepartmentKey, TechKey, TechNumber, EmployeeNumber, FirstName, LastName)  
    SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      a.biweeklypayperiodsequence, dayinbiweeklypayperiod, a.DayName,
      (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop'),
      b.TechKey, b.technumber, b.employeenumber, b.firstname, b.lastname
    FROM dds.day a, tpTechs b 
    WHERE a.thedate BETWEEN '01/11/2015' AND curdate()
	  AND b.departmentKey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop')
      AND NOT EXISTS (
        SELECT 1
        FROM tpdata
        WHERE thedate = a.thedate
          AND techkey = b.techkey)
      AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
        SELECT 1
        FROM tpTeamTechs
        WHERE techkey = b.techkey
          AND a.thedate BETWEEN fromdate AND thrudate); 	  
    -- payPeriodSelectFormat for new row
    UPDATE tpdata
    SET payPeriodSelectFormat = z.ppsel
    FROM (
      SELECT payperiodseq, TRIM(pt1) + ' ' + TRIM(pt2) AS ppsel
        FROM (  
        SELECT distinct payperiodstart, payperiodend, payperiodseq, 
          (SELECT trim(monthname) + ' '
            + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
            FROM dds.day
            WHERE thedate = a.payperiodstart) AS pt1,
          (SELECT trim(monthname) + ' ' 
            + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
            + TRIM(CAST(theyear AS sql_char))
            FROM dds.day
            WHERE thedate = a.payperiodend) AS pt2
        FROM tpdata a) y) z
    WHERE tpdata.payperiodseq = z.payperiodseq;  
    -- teamname/key for new row
    UPDATE tpData
    SET TeamKey = x.teamkey,
        teamname = x.teamname
    FROM (    
      SELECT *
      FROM ( 
        SELECT thedate
        FROM tpData
        GROUP BY thedate) a
      INNER JOIN (  
        SELECT a.techkey, c.teamkey, c.teamname, b.fromdate, b.thrudate
        FROM tptechs a
        INNER JOIN tpteamtechs b on a.techkey = b.techkey
        INNER JOIN tpteams c on b.teamkey = c.teamkey) b on a.thedate BETWEEN b.fromdate AND b.thrudate) x
    WHERE tpData.TechKey = x.techkey
      AND tpData.theDate = x.theDate; 
	         
    -- team census, budget for new row
    /* bs mod	
        UPDATE tpData 
        SET TeamCensus = x.census,
            teamBudget = x.Budget
        FROM ( 
          SELECT *
          FROM tpTeamValues) x
        WHERE tpData.payPeriodStart = x.fromDate
          AND tpData.teamkey = x.teamkey;  
    */	             
    UPDATE tpData
      SET teamCensus = x.census,
          teamBudget = x.budget
      FROM tpTeamValues x
      WHERE (tpData.teamCensus IS NULL OR tpData.teamBudget IS NULL) 
        AND tpData.teamKey = x.teamKey
        AND tpData.theDate BETWEEN x.fromDate AND x.thruDate; 
		  
-- *a*			  
    -- techteamPercentage: FROM tpTechValues for new row
	-- AND techHourlyRate
    UPDATE tpData
    SET TechTeamPercentage = x.TechTeamPercentage,
	    techHourlyRate = x.previousHourlyRate
    FROM (
      SELECT a.thedate, b.techkey, b.techTeamPercentage, b.previousHourlyRate
      FROM tpData a
      INNER JOIN tpTechValues b on a.techKey = b.techKey
        AND a.thedate BETWEEN b.fromdate AND b.thrudate) x
    WHERE tpData.TechKey = x.TechKey
      AND tpData.thedate = x.theDate;    
    -- TechTFRRate for new row 
    UPDATE tpdata
    SET TechTFRRate = techteampercentage * teambudget;    
    -- TechHourlyRate for new row
--    UPDATE tpdata
--    SET techhourlyrate = (
--      SELECT distinct previoushourlyrate
--      FROM tptechvalues
--      WHERE techkey = tpdata.techkey);  
            
  CATCH ALL 
    RAISE;-- tpdata(1, 'New Row ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- techs
TRY
  TRY   
    -- ClockHoursDay    
    UPDATE tpdata
    SET TechClockhoursDay = x.clockhours,
        TechVacationHoursDay = x.Vacation,
        TechPTOHoursDay = x.PTO,
        TechHolidayHoursDay = x.Holiday
    FROM (    
      SELECT b.thedate, c.employeenumber, SUM(clockhours) AS clockhours,
        SUM(coalesce(VacationHours,0)) AS vacation,
        SUM(coalesce(PTOHours,0)) AS pto,
        SUM(coalesce(HolidayHours,0)) AS holiday
      FROM dds.edwClockHoursFact a
      INNER JOIN dds.day b on a.datekey = b.datekey
        AND b.thedate BETWEEN '01/11/2015' AND curdate()
      LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
      GROUP BY b.thedate, c.employeenumber) x 
    WHERE tpdata.thedate = x.thedate
      AND tpdata.employeenumber = x.employeenumber;
    -- ClockHoursPPTD
    UPDATE tpData
    SET TechClockHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday, SUM(b.techclockhoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey; 
    -- TechVacationPPTD  
    UPDATE tpData
    SET TechVacationHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay, SUM(b.TechVacationHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
    -- TechPTOHoursPPTD
    UPDATE tpData
    SET TechPTOHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay, SUM(b.TechPTOHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;  
    -- TechHolidayHoursPPTD
    UPDATE tpData
    SET TechHolidayHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay, SUM(b.TechHolidayHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;      
-- flag hours  
    -- TechFlagHoursDay 
    -- main shop
    UPDATE tpData  
    SET TechFlagHoursDay = x.flaghours
    FROM (
      SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
      FROM tpData a
      LEFT JOIN (
        SELECT d.thedate, ro, line, c.technumber, a.flaghours
        FROM dds.factRepairOrder a  
        INNER JOIN dds.dimTech c on a.techkey = c.techkey
        INNER JOIN dds.day d on a.flagdatekey = d.datekey
        WHERE flaghours <> 0
          AND a.storecode = 'ry1'
          AND a.flagdatekey IN (
            SELECT datekey
            FROM dds.day
            WHERE thedate BETWEEN '01/11/2015' AND curdate())   
          AND c.technumber IN (
            SELECT technumber
            FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
      GROUP BY a.technumber, a.thedate) x
    WHERE tpdata.departmentkey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'mainshop' AND dl5 = 'Shop')
      AND tpdata.thedate = x.thedate
      AND tpdata.technumber = x.technumber;     
    -- TechFlagHoursDay 
    -- body shop
    UPDATE tpData  
    SET TechFlagHoursDay = x.flaghours
    FROM (
      SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
      FROM tpData a
      LEFT JOIN (
        SELECT d.thedate, ro, line, c.technumber, a.flaghours
        FROM dds.factRepairOrder a  
        INNER JOIN dds.dimTech c on a.techkey = c.techkey
        INNER JOIN dds.day d on a.closedatekey = d.datekey
        WHERE flaghours <> 0
          AND a.storecode = 'ry1'
          AND a.closedatekey IN (
            SELECT datekey
            FROM dds.day
            WHERE thedate BETWEEN '01/11/2015' AND curdate())   
          AND c.technumber IN (
            SELECT technumber
            FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
      GROUP BY a.technumber, a.thedate) x
    WHERE tpdata.departmentkey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop')
      AND tpdata.thedate = x.thedate
      AND tpdata.technumber = x.technumber;         
    -- TechFlagHoursPPTD  
    UPDATE tpData
    SET TechFlagHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday, SUM(b.techflaghoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
-- adjustments
    UPDATE tpData
    SET TechFlagHourAdjustmentsDay = x.Adj
    FROM (
      SELECT a.technumber, a.lastname, a.thedate, coalesce(Adj, 0) AS Adj
      FROM tpData a
      LEFT JOIN (  
        select pttech, ptdate, round(SUM(ptlhrs), 2) AS adj
        FROM dds.stgArkonaSDPXTIM a
        INNER JOIN tpTechs b on a.pttech = b.technumber
        GROUP BY pttech, ptdate) b on a.technumber = b.pttech AND a.thedate = b.ptdate) x    
    WHERE tpData.technumber = x.technumber
      AND tpData.thedate = x.thedate;
    -- tech flag time adjustments pptd 
    UPDATE tpData
    SET TechFlagHourAdjustmentsPPTD = x.pptd
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay, SUM(b.TechFlagHourAdjustmentsDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;       
-- *b*      
    UPDATE tpData
      SET techClockHoursDay = 0,
          techVacationHoursDay = 0,
          techPtoHoursDay = 0,
          techHolidayHoursDay = 0,
          techClockHoursPPTD = 0,
          techVacationHoursPPTD = 0,
          techPtoHoursPPTD = 0,
          techHolidayHoursPPTD = 0, 
          techFlagHoursDay = 0,   
          TechFlagHoursPPTD = 0,  
          TechFlagHourAdjustmentsDay = 0,
          TechFlagHourAdjustmentsPPTD = 0
    WHERE payperiodstart = '12/28/2014'
      AND lastname = 'johnson' AND firstname = 'cody';      
  CATCH ALL 
    RAISE tpdata(2, 'Techs ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- teams
TRY
  TRY  
    -- teamclockhoursDay
    UPDATE tpData 
    SET teamclockhoursday = x.teamclockhoursday
    FROM (
      SELECT thedate, teamkey, SUM(techclockhoursday) AS teamclockhoursday
      FROM tpdata
      GROUP BY thedate, teamkey) x 
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;  
    -- teamclockhours pptd
    UPDATE tpData
    SET teamclockhourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;       
    -- teamflaghoursday
    UPDATE tpData
    SET TeamFlagHoursDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(techflaghoursday) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;
    -- teamflaghour pptd  
    UPDATE tpData
    SET teamflaghourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;  
    -- TeamFlagHourAdjustmentsDay
    UPDATE tpData
    SET TeamFlagHourAdjustmentsDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(TechFlagHourAdjustmentsDay) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey; 
    -- TeamFlagHourAdjustmentsPPTD 
    UPDATE tpData
    SET TeamFlagHourAdjustmentsPPTD = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x 
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;   
    -- team prof day          
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhoursday = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhoursday, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay) AS TotalFlag, 
          teamclockhoursday
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay), 
          teamclockhoursday) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;      
    -- team prof pptd
    UPDATE tpdata
    SET teamprofpptd = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhourspptd = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhourspptd, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS TotalFlag, 
          teamclockhourspptd
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD), 
          teamclockhourspptd) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;        
  CATCH ALL 
    RAISE tpdata(3, 'Teams ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  


END;


