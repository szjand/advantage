/*
1/19/20
Mitch Rogers needs to come off team Girodat, he will be payed hourly until he full recovers
AND returns to work.  this should have been effective for pay period 1/5 -> 1/18
26 hours clock time was added that need to be removed

AS i determined IN the 11/11/19 script, it IS good enough to just UPDATE tpTeamTechs
Budget 102.4352
mcveigh .1952 * 102.4352 = 19.99535104
girodat .3124 * 102.4352 = 32.00075648
oneil   .2195 * 102.4352 = 22.4845264
*/
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND teamName = 'girodat'
ORDER BY teamname, firstname  

DELETE 
FROM tpdata
WHERE techkey = 79
  and thedate >= '01/05/2020';
  
UPDATE tpTeamTechs
SET thrudate = '01/04/2020'
WHERE techkey = 79
  AND teamkey = 21;
      