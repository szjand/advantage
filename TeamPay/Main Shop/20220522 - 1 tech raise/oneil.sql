/*
05/24/22
We were approved to move Tim ONeal from his current wage to $25.50/Flat Rate Hour.
 I have updated the spreadsheet attached. 

 SELECT * FROM tpteams WHERE departmentkey = 18

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 21 -- team oneil
ORDER BY teamname, firstname  

*!*!*!*!*!*!*!*!*!*!*!*!*!*!*
for this TRANSACTION to function IN zzTest, DELETE the ukg link
AND re-create it : 

EXECUTE PROCEDURE 
  sp_CreateLink ( 
     'ukg',
     '\\67.135.158.12:6363\advantage\ukg\ukg.add',
     TRUE,
     TRUE,
     TRUE,
     '',
     '');
		 
SELECT * FROM ukg.employees		 

 
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;

@eff_date = '05/22/2022';
@thru_date = '05/21/2022';
@dept_key = 18;
@elr = 91.46;
@team_key = 21;
@pool_perc = .28;
@census = 4;
BEGIN TRANSACTION;
TRY
-- tech keys
	 -- holwerda 24
	 -- aamodt 90
	 -- simon 98
	 -- oneil 54
-- team values
--  SELECT * FROM tpteamvalues WHERE teamkey = 21	AND thrudate > curdate()
	UPDATE tpteamValues
	SET thrudate = @thru_date
	WHERE teamkey = @team_key
	  AND thrudate > curdate();
  INSERT INTO tpteamValues(teamkey,census,budget,poolpercentage,fromdate)
	values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);
-- techValues
  -- holwerda
	@tech_key = 24;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .17465; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	    
  
  -- aamodt
	@tech_key = 90;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .1743; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);  
		
  -- simon
	@tech_key = 98;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .24405; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);  
			
  -- oneil
	@tech_key = 54;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2489; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);
				
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey = @team_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
   
	
 

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    


