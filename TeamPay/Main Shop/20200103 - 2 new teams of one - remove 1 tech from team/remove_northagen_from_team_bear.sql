/*
I have created individual flat rate teams for Johnny Johnson and Matt Aamodt. 
Please let me know if something does not look correct. Ken submitted a compli 
request to Kim for both but their rates stay the same. The only other change I 
did was to remove Josh Northagen from Jeff Bear�s team as we are moving him to 
hourly for 30 days as we transition him over to the alignment rack. We will 
be moving him onto Kirby�s team in February. We also have 2 PDQ techs we 
are moving to the main shop but they will stay hourly for at least 90 days.
*/




take the shortcut, just UPDATE tpteamtechs
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
/*

SELECT * FROM tptechs WHERE lastname = 'northagen'
techkey: 84
SELECT * FROM tpteams WHERE teamname = 'bear'
teamkey: 22

*/
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
--  AND teamName = 'bear'
ORDER BY teamname, firstname  



DELETE 
-- SELECT *
FROM tpdata
WHERE techkey = 84
  and thedate > '01/02/2021';
  
UPDATE tpTeamTechs
SET thrudate = '01/02/2021'
-- SELECT * FROM tpteamtechs
WHERE techkey = 84
  AND teamkey = 22
  AND thrudate > curdatE()