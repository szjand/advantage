/*
I have created individual flat rate teams for Johnny Johnson and Matt Aamodt. 
Please let me know if something does not look correct. Ken submitted a compli 
request to Kim for both but their rates stay the same. The only other change I 
did was to remove Josh Northagen from Jeff Bear�s team as we are moving him to 
hourly for 30 days as we transition him over to the alignment rack. We will be 
moving him onto Kirby�s team in February. We also have 2 PDQ techs we are moving 
to the main shop but they will stay hourly for at least 90 days.
*/

-- Aamodt
DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
@from_date = '01/03/2021';
@dept_key = 18;
BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Aamodt', @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Aamodt');
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
  -- SELECT *
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'Aamodt' -- AND a.firstname = 'Johnny'
    AND a.currentrow = true
	AND a.storecode = 'RY1';
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'Aamodt' AND firstname = 'Matthew');
  
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;

  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, 17.38, .19, @from_date);

  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, .9781, 17);

--  DELETE FROM tpData  
--  WHERE departmentkey = @dept_key
--    AND techkey = @tech_key
--    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
  INSERT INTO employeeappauthorization
  SELECT 'maamodt@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'cgehrtz@rydellcars.com'
    AND appname = 'teampay';     
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

  
-- Johnson
DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
@from_date = '01/03/2021';
@dept_key = 18;
BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Johnson', @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Johnson');
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
  -- SELECT *
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'Johnson' AND a.firstname = 'Johnny'
    AND a.currentrow = true
	AND a.storecode = 'RY1';
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'Johnson' AND firstname = 'Johnny');
  
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;

  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, 18.29, .2, @from_date);

  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, .9841, 18);

--  DELETE FROM tpData  
--  WHERE departmentkey = @dept_key
--    AND techkey = @tech_key
--    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
  INSERT INTO employeeappauthorization
  SELECT 'jjohnson@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'cgehrtz@rydellcars.com'
    AND appname = 'teampay';     
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    