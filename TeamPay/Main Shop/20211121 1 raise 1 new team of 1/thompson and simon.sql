/*
Kim, 
The rate increase for Wyatt Thompson will have gone into effect 10/24/21 so we 
will have to retro that back please. It took quite some amount of time to get 
everyone to approve it. You should have the approved Compli form already. 

Jon, 
The change of flat rate for Mitchell Simon will go into effect 11/21/21 for 
the next pay period and I have attached the spreadsheet for you Jon.  

Please let me know if I need to do anything else!
Thank you!!

Andrew Neumann
*/
-- Wyatt IS on team heffernan
-- SELECT * FROM tpteams
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
--  AND a.teamkey = 3
ORDER BY teamname, firstname  

no change IN poolpercentage, remains at .29, only thing changing IS Wyatt's 
techteampercentage, FROM .2193 to .2281, rate FROM 23.27 to 24.20
budget: 106.0936
so, only change IS to Wyatt's  tech values
also need to figure back pay
 
back pay:
		 10/24 - 11/06  107.12 comm hrs  1.9 bonus hrs 
		 90hrs * 119.02% = 107.118 comm hrs * 23.27 = 2492.63586
		                                    * 24.2  = 2592.2556  reg time diff: 99.62
		 1.6hrs * 119.02% = 1.90432 bonus hrs * 46.54 = 88.627
		 1.6hrs * 119.02% = 1.90432 bonus hrs * 48.40 = 92.1691  bonus diff: 3.54
		                                                         total diff: 103.16
																				
SELECT * FROM tpdata WHERE thedate = '11/06/2021' AND techkey = 13		 
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;

@eff_date = '11/07/2021';
@thru_date = '11/06/2021';
@dept_key = 18;
@elr = 91.46;
@team_key = 3;
@tech_key = 13; 
BEGIN TRANSACTION;
TRY
	
-- techValues
  -- wyatt
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .2281; ------------------------------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	    
  
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey = @team_key
		AND techkey = @tech_key
    AND thedate >= @eff_date;
    
		
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey = @team_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
	
 

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    

-- new team of one
-- Mitchell Simon  tech # 658

DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
@from_date = '11/07/2021';
@dept_key = 18;
BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Simon', @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Simon');
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
  -- SELECT *
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'Simon' -- AND a.firstname = 'Johnny'
    AND a.currentrow = true
	AND a.storecode = 'RY1';
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'Simon' AND firstname = 'Mitchell');
  
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;

	-- on the spreadsheet, andrew fucked up, LEFT census AS 4
	-- on these teams of 1, i prefer to SET up with census = 1
	-- budget: 25, pool%: .27334
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, 25, .27334, @from_date);
  -- with 100% techTeamPerc
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, 1, 25);

--  DELETE FROM tpData  
--  WHERE departmentkey = @dept_key
--    AND techkey = @tech_key
--    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
  -- i believe tpEmployees IS also required
  INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, membergroup, storecode, fullname)
  values('msimon@rydellcars.com','Mitchell','Simon','189325','ab43Hr?3','Fuck You', 'RY1','Mitchell Simon');			
		
  INSERT INTO employeeappauthorization
  SELECT 'msimon@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'maamodt@rydellcars.com'
    AND appname = 'teampay';     
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

