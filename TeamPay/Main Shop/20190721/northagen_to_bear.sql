
-- We moved Dale Axtman and Jared Duckstad to their own flat rate team with a 
-- considerable bump. I also added Josh Northagen to Jeff Bear�s team. 
-- I have submitted everything to Jeri and Kim already. Looking to put it 
-- into effect on 7/21/19 if possible. 
-- Are you able to see if all 3 of them have user/password information for 
-- the old vision page by chance?

/*
just the northagen to bear part
*/

    
SELECT * FROM tpteams WHERE teamname = 'bear'  -- 22
census FROM 3 to 4
budget = 79.75312
poolPercentage = .218
grant: 16.78
bear:  23.89
strandell: 17.89
northagen: 20.00

total new tech pay = 78.56 SELECT 16.78 + 23.89 + 17.89 + 20 FROM system.iota

grant: .210399  SELECT 16.780/79.75312 FROM system.iota
bear: .299549 SELECT 23.8900/79.75312 FROM system.iota
strandell: .224317 SELECT 17.8900/79.75312 FROM system.iota
northagen: .250773 SELECT 20.0000/79.75312 FROM system.iota

poolPercentage = budget/(census * elr)
                 SELECT 79.75312/(4*91.4600) FROM system.iota
                 .218
                 

/*
-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey IN (22)
ORDER BY teamname, firstname  
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @employee_number string;
@eff_date = '07/21/2019';
@thru_date = '07/20/2019';
--@eff_date = '07/07/2019';
--@thru_date = '07/06/2019';
@dept_key = 18;
@elr = 91.46;
@employee_number = '168753';
@pool_perc = .218;

BEGIN TRANSACTION;
TRY

--!!!!!!!!!!!!!! check the fucking dates !!!!!!!!!!!!!!!!!!!!!!

  @team_key = 22;  -- team bear
  @census = 4;

  -- employeeappauthorization 
  INSERT INTO employeeappauthorization    
  SELECT 'jnorthagen@rydellcars.com',appname,appseq,appcode,approle,functionality,
    appdepartmentkey 
  FROM employeeappauthorization 
  WHERE username = 'dgrant@rydellcars.com'
    AND appname = 'teampay';
    
  -- tpTechs
  
  insert into tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT 18, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @eff_date
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
      AND b.currentrow = true
    WHERE a.employeenumber = '168753' 
      AND a.currentrow = true;    
                     
   -- tpTeamTechs
   @tech_key = (SELECT techkey FROM tptechs WHERE employeenumber = @employee_number);
   INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
   values(@team_key, @tech_key, @eff_date);
    
-- team values

  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);  

-- tech values    
  -- grant: .210399  SELECT 16.780/79.75312 FROM system.iota
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'grant' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .210399; -----------------------------------------   

  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
    
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  

  -- bear: .299549 SELECT 23.8900/79.75312 FROM system.iota
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'bear' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .299549; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    
  
  -- strandell: .224317 SELECT 17.8900/79.75312 FROM system.iota
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'strandell' AND thrudate > curdate()); 
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .224317; -----------------------------------------   
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  
  
  -- northagen .250773 SELECT 20.0000/79.75312 FROM system.iota
  @tech_key = (
    SELECT techkey 
    FROM tptechs 
    WHERE lastname = 'northagen' AND thrudate > curdate());   
  @tech_perc = .250773;
  @pto_rate = 20;
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);    

  DELETE FROM tpData
  WHERE departmentkey = @dept_key
    AND thedate >= @thru_date
    AND teamkey = @team_key;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
/**/  
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   