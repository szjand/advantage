
-- We moved Dale Axtman and Jared Duckstad to their own flat rate team with a 
-- considerable bump. I also added Josh Northagen to Jeff Bear�s team. 
-- I have submitted everything to Jeri and Kim already. Looking to put it 
-- into effect on 7/21/19 if possible. 
-- Are you able to see if all 3 of them have user/password information for 
-- the old vision page by chance?

/*
team axtman
*/

    
SELECT * FROM tpteams WHERE teamname = 'bear'  -- 22
census 2
poolPercentage = .36
budget = poolPercentage * ELR * census = SELECT .36 * 91.46 * 2 FROM system.iota
       = 65.8512
axtman: 35.00
duckstad: 30.00

axtman: .5315  SELECT 35.0000/65.8512 FROM system.iota
duckstad: .455572 SELECT 30.0000/65.8512 FROM system.iota

select * FROM tpteams WHERE teamname = 'axtman'
/*
-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey IN (48)
ORDER BY teamname, firstname  
*/


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @employee_number string;
@eff_date = '07/21/2019';
@thru_date = '07/20/2019';
@dept_key = 18;
@elr = 91.46;
@pool_perc = .36;
@census = 2;

BEGIN TRANSACTION;
TRY

!!!!!!!!!!!!!! check the fucking dates !!!!!!!!!!!!!!!!!!!!!!

  -- tpTeams
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Axtman', @eff_date);

-- axtman
  -- employeeappauthorization 
  INSERT INTO employeeappauthorization    
  SELECT 'daxtman@rydellcars.com',appname,appseq,appcode,approle,functionality,
    appdepartmentkey 
  FROM employeeappauthorization 
  WHERE username = 'jgirodat@rydellchev.com'
    AND appname = 'teampay';
    
  -- tpTechs
  @employee_number = (
    SELECT DISTINCT employeenumber 
    FROM dds.edwEmployeeDim 
    WHERE lastname = 'axtman');
  insert into tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT 18, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @eff_date
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
      AND b.currentrow = true
    WHERE a.employeenumber = @employee_number
      AND a.currentrow = true;    
                     
   -- tpTeamTechs
   @team_key = (
     SELECT teamkey
     FROM tpTeams
     WHERE teamName = 'axtman');
   @tech_key = (SELECT techkey FROM tptechs WHERE employeenumber = @employee_number);
   INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
   values(@team_key, @tech_key, @eff_date);
    
-- team values
  INSERT INTO tpTeamValues (teamkey,census,budget,poolPercentage,fromDate)
  values(@team_key, @census, @census * @elr * @pool_perc, @pool_perc, @eff_date);  

-- tech values    
  -- axtman: .5315  SELECT 35.0000/65.8512 FROM system.iota
  @pto_rate = 35;
  @tech_perc = .5315; -----------------------------------------     
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  


-- duckstad: .299549 SELECT 23.8900/79.75312 FROM system.iota
  -- employeeappauthorization 
  @employee_number = (
    SELECT DISTINCT employeenumber 
    FROM dds.edwEmployeeDim 
    WHERE lastname = 'duckstad');
     
  INSERT INTO employeeappauthorization    
  SELECT 'jduckstad@rydellchev.com',appname,appseq,appcode,approle,functionality,
    appdepartmentkey 
  FROM employeeappauthorization 
  WHERE username = 'dgrant@rydellcars.com'
    AND appname = 'teampay';
                      
   -- tpTeamTechs
   @team_key = (
     SELECT teamkey
     FROM tpTeams
     WHERE teamName = 'axtman');
   @tech_key = (SELECT techkey FROM tptechs WHERE employeenumber = @employee_number);
   INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
   values(@team_key, @tech_key, @eff_date);
 
  -- duckstad .455572 SELECT 30.0000/65.8512 FROM system.iota
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  @pto_rate = 30;
  @tech_perc = .455572; -----------------------------------------     
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);  


  DELETE FROM tpData
  WHERE departmentkey = @dept_key
    AND thedate >= @thru_date
    AND teamkey = @team_key;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();     
/**/  
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   