7/24
per mgmt promise, the hourly rate IN arkona IS being adjusted to reflect the
results of team pay
kim has updated arkona
want these to take affect IN the current pay period (7/13 - 7/26)

need to UPDATE 
1. tpTechValues
2. tpData rows for this pay period
3. sp.xfmTpData

looks LIKE xfmTpData IS fucked up, simply does a DISTINCT, rather than the relevant 
values for the date
todayXfmTpData IS NOT affected, hourly rate IS generated AS part of the daily
  new row IN xfmTpData
  
payroll IS NOT affected: pulls ALL data FROM tpData

this needs to be IN place for this pay period, AS there are techs with pto/vac
  IN this pay period, AND payroll needs to reflect the new values:
SELECT techKey, technumber, employeenumber, firstname, lastname,
  techPtoHoursPPTD, techVacationHoursPPTD
FROM tpData
WHERE thedate = curdate()
  AND departmentkey = 18
ORDER BY lastname  


-- techs currently assigned to a team AND their current tpTechValues row AND
-- current hourly rate FROM arkona
select a.techKey, a.techNumber, a.firstName, a.lastName,
  b.fromDate, b.thruDate, b.techTeamPercentage, b.previousHourlyRate, c.hourlyRate,
  b.previousHourlyRate - c.hourlyRate
FROM tpTechs a
LEFT JOIN tpTechValues b on a.techkey = b.techkey
  AND b.thruDate > curdate()
LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
WHERE a.departmentKey = 18  
  AND EXISTS ( -- currently assigned to a team
    SELECT 1
    FROM tpTeamTechs
    WHERE techKey = a.techKEy
    AND thruDate > curdate())
ORDER BY a.lastname

-- techs currently assigned to a team AND their current tpTechValues row AND
-- current hourly rate FROM arkona  
-- these are the techs that need to have their most recent 
--   tpTechValues.previouslyHourlyRate updated
select a.techKey, a.techNumber, a.firstName, a.lastName,
  b.fromDate, b.thruDate, b.techTeamPercentage, b.previousHourlyRate, c.hourlyRate
FROM tpTechs a
LEFT JOIN tpTechValues b on a.techkey = b.techkey
  AND b.thruDate > curdate()
LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
WHERE a.departmentKey = 18  
  AND EXISTS ( -- currently assigned to a team
    SELECT 1
    FROM tpTeamTechs
    WHERE techKey = a.techKEy
    AND thruDate > curdate())
  AND b.previousHourlyRate - c.hourlyRate <> 0  -- only those techs with a change 
ORDER BY a.lastname
    
let us pop over to test server AND play with the update    
be sure to check for desired results on localhost

-- oh shit, IS this wrong, 
for someone LIKE doug bohm, who has one row IN techvalues, it IS NOT an UPDATE,
it IS a CLOSE previous row, CREATE new row

-- at least 2 stages: those WHERE most current tpTechValue row fromDate = 7/13/14
--   these just get updated
-- but the others, need to CLOSE the current row on 7/12 AND CREATE a new row
--  starting 7/13 AND carrying over the techTeamPercentage FROM the previous row
UPDATE tpTechValues
SET previousHourlyRate = x.hourlyRate
FROM (
  select a.techKey, a.techNumber, a.firstName, a.lastName,
    b.fromDate, b.thruDate, b.techTeamPercentage, b.previousHourlyRate, c.hourlyRate
  FROM tpTechs a
  LEFT JOIN tpTechValues b on a.techkey = b.techkey
    AND b.thruDate > curdate()
	AND b.fromDate = '07/13/2014' -- only those rows that were new at the start of the payperiod
  LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
    AND c.currentrow = true 
  WHERE a.departmentKey = 18  
    AND EXISTS ( -- currently assigned to a team
      SELECT 1
      FROM tpTeamTechs
      WHERE techKey = a.techKEy
      AND thruDate > curdate())
    AND b.previousHourlyRate - c.hourlyRate <> 0) x -- only those techs with a change 
WHERE tpTechValues.techKey = x.techKey    
  AND tpTechValues.fromDate = x.fromDate
  AND tpTechValues.thruDate = x.thruDate;
  
-- AND these are the ones that need a new row 
-- fuck me, a script that essentially does a type 2
-- CLOSE the current row, ADD a new row

select a.techKey, a.techNumber, a.firstName, a.lastName,
  b.fromDate, b.thruDate, b.techTeamPercentage, b.previousHourlyRate, c.hourlyRate
INTO #typ2	
FROM tpTechs a
LEFT JOIN tpTechValues b on a.techkey = b.techkey
  AND b.thruDate > curdate()
LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
WHERE a.departmentKey = 18  
  AND EXISTS ( -- currently assigned to a team
    SELECT 1
    FROM tpTeamTechs
    WHERE techKey = a.techKEy
    AND thruDate > curdate())
  AND b.previousHourlyRate - c.hourlyRate <> 0; 
-- UPDATE current row	
UPDATE tpTechValues
SET thruDate = '07/12/2014'
WHERE EXISTS (
  SELECT 1
  FROM #typ2
  WHERE techKey = tpTechValues.techKey
    AND fromDate = tpTechValues.fromDate);
-- INSERT new row	
INSERT INTO tpTechValues
SELECT techKey, '07/13/2014', '12/31/9999', techTeamPercentage, hourlyRate
FROM #typ2;	
	 
  
  
  
  
  
looks LIKE on the tech display page, the stored proc IS NOT getting the correct value
probably because it IS pulling FROM tpData
yep, that IS what it looks LIKE
so, need to UPDATE tpdata
  
UPDATE tpData
SET techHourlyRate = x.previousHourlyRate
FROM (
  SELECT *
  FROM tpTechValues
  WHERE thruDate > curdate()) x
WHERE departmentKey = 18
  AND payPeriodSeq = (
    SELECT distinct payPeriodSeq
    FROM tpData
    WHERE thedate = curdate())
  AND tpData.techKey = x.techKey;

ok, that ALL looks good, now modify xfmTpData

-- *a*			  
    -- techteamPercentage: FROM tpTechValues for new row
	-- AND techHourlyRate
    UPDATE tpData
    SET TechTeamPercentage = x.TechTeamPercentage,
	    techHourlyRate = x.previousHourlyRate
    FROM (
      SELECT a.thedate, b.techkey, b.techTeamPercentage, b.previousHourlyRate
      FROM tpData a
      INNER JOIN tpTechValues b on a.techKey = b.techKey
        AND a.thedate BETWEEN b.fromdate AND b.thrudate) x
    WHERE tpData.TechKey = x.TechKey
      AND tpData.thedate = x.theDate;    
    -- TechTFRRate for new row 
    UPDATE tpdata
    SET TechTFRRate = techteampercentage * teambudget;    
    -- TechHourlyRate for new row
--    UPDATE tpdata
--    SET techhourlyrate = (
--      SELECT distinct previoushourlyrate
--      FROM tptechvalues
--      WHERE techkey = tpdata.techkey);  

