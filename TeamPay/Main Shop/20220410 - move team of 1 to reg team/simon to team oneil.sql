/*
Good morning,
Effective this Sunday 4/10/22  Mitchell Simon will be added to Team Tim. 
He will no longer be on his own Team.
*/
SELECT * FROM tpteams WHERE teamname = 'simon' -- team key 60
SELECT * FROM tpteams WHERE teamname = 'o''neil' -- team key 21
mitchells rates are 25 flat & hourly

-- current config
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = 21  -- team simon
ORDER BY teamname, firstname  

holwerda: 17.89
aamodt:  17.85
oneil: 23.50
simon: 25.00

-- get rid of team simon, no need to keep anything
delete
FROM tpteamtechs
WHERE teamkey = 60;
delete
FROM tpteamvalues
WHERE teamkey = 60;
delete
FROM tpteams
WHERE teamkey = 60;

-- now, ADD simon to team oneil AND adjust everybody to keep the same rates
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '04/10/2022';
@thru_date = '04/09/2022';
@dept_key = 18;
@elr = 91.46;
@team_key = 21;  -- team bear
@census = 4;

BEGIN TRANSACTION;
TRY
  -- ADD simon to team oneil
	@tech_key = (
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'simon');

  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
	values(@team_key, @tech_key, @eff_date);
	
	-- now i have to figure out how to bump team values to accomodate a fourth tech
	-- SELECT * FROM tpteamvalues WHERE teamkey = 21 AND thrudate > curdate()
  UPDATE tpteamvalues
	SET thrudate = @thru_date
	WHERE teamkey = @team_key
	  AND thrudate > curdate();
  INSERT INTO tpteamvalues(teamkey,census,budget,poolpercentage,fromdate)
	values(@team_key, 4, 91.46, 0.25, @eff_date);
  
	-- now the individual techs
  -- holwerda
	-- SELECT 17.89/91.46 FROM system.iota  --.1956
	@tech_key = ( 
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'holwerda'
		  AND thrudate > curdate());		
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	@tech_perc = .1956; -----------------------------------------   
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	
	
  -- aamodt
	-- SELECT 17.85/91.46 FROM system.iota  --.1951
	@tech_key = ( 
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'aamodt'
		  AND thrudate > curdate());		
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	@tech_perc = .1951; -----------------------------------------   
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	
		
  -- oneil
	-- SELECT 23.50/91.46 FROM system.iota  --.2569
	@tech_key = ( 
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'o''neil'
		  AND thrudate > curdate());		
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	@tech_perc = .2569; -----------------------------------------   
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	 
	
  -- simon
	-- SELECT 25/91.46 FROM system.iota  --.27
	@tech_key = ( 
	  SELECT techkey
		FROM tptechs
		WHERE lastname = 'simon'
		  AND thrudate > curdate());		
  @pto_rate = (
	  SELECT previoushourlyrate
		FROM tptechvalues
		WHERE techkey = @tech_key
		  AND thrudate > curdate());
	@tech_perc = .2569; -----------------------------------------   
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, @tech_perc, @pto_rate);	 	
			
  DELETE 
  FROM tpdata 
  WHERE teamkey IN (60, 21)
    AND thedate > @thru_date;

  EXECUTE PROCEDURE xfmTpData();


	 
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   

