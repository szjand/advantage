/*
WE moved Johnny from GM Main shop to GM used car. we will temporary put him back 
to his hourly wage until he can get trained and settled in. we also are back 
dating his pay for the start of this pay period 6/6/2021

John, can you update your doc.


SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey = (
    SELECT teamkey
	FROM tpteams
	WHERE teamname = 'johnson'
	  AND thrudate > curdate())
ORDER BY teamname, firstname  

*/



DECLARE @thru_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;

BEGIN TRANSACTION;
TRY

@thru_date = '06/05/2021';

@team_key = (
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'johnson'
    AND thrudate > curdate());
	
@tech_key = (
  SELECT techkey 
	FROM tptechs 
	WHERE lastname = 'johnson' 
	  and firstname = 'johnny' 
		AND thrudate > curdate());	
		
  UPDATE tpTeams 
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();		

  UPDATE tpTechs 
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();		
				
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
		
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
		
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();	
		
  DELETE FROM tpData  
  WHERE teamkey = @team_key
    AND thedate >= @thru_date;

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;   	
