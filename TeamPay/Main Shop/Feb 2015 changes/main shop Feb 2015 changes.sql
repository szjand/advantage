pto rate change retroactive to 2/8
based on earnings for ALL of 2014


-- 1/26 might be crossing streams here, but leaning towards gross pay - pto pay
--    FROM pyhshdta, AND clockhours FROM dds
--    make this effective 1/25/15 -- oops, i fucked up, am a month late

/*
DROP TABLE #techs;
DROP TABLE #pay;
DROP TABLE #hours;
*/
-- so, current tp techs
SELECT a.firstname, a.lastname, a.employeenumber, c.previousHourlyRate
INTO #techs
FROM tpTechs a
INNER JOIN tpTeamTechs b on a.techkey = b.techkey
INNER JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()
WHERE b.thrudate > curdate() 
  AND a.departmentkey = 18;

-- pay  
SELECT yhdemp, SUM(gross) AS grossPay, SUM(totalPtoPay) AS ptoPay
INTO #pay
FROM (
SELECT yhdemp, yhsaly,
  yhdtgp as gross,
  yhdvac as vacHours,
  yhdhol as holHours,
  yhdsck  as ptoHours,
  yhsaly*(yhdvac+yhdhol+yhdsck) AS totalPtoPay,
  cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
  cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
FROM dds.stgArkonaPYHSHDTA   
WHERE ypbcyy = 114
  AND yhdemp IN (
    SELECT employeenumber
    FROM #techs)
  AND yhdtgp <> 0) x
GROUP BY yhdemp;    

-- hours
  
SELECT d.employeenumber, SUM(clockhours) AS clockhours, 
  SUM(vacationhours+ptohours+holidayHours) AS ptoHours
INTO #hours  
FROM dds.edwClockHoursFact a
INNER JOIN dds.day c on a.datekey = c.datekey
  AND year(c.thedate) = 2014  
INNER JOIN dds.edwEmployeeDim d on a.employeekey = d.employeekey  
  AND d.employeenumber IN (
    SELECT employeenumber
    FROM #techs)  
GROUP BY d.employeenumber;  
  
-- this IS what i sent to cahalan, etc
SELECT trim(a.firstname) + ' ' + a.lastname AS Name, a.employeenumber AS [Emp#], 
  b.grosspay-b.ptopay AS [Comm Pay], c.clockhours AS [Clock Hours], 
  (b.grosspay-b.ptopay)/c.clockhours AS [New PTO Rate],
  a.previoushourlyrate AS [Current PTO Rate], ((b.grosspay-b.ptopay)/c.clockhours) - a.previoushourlyrate AS change
FROM #techs a
LEFT JOIN #pay b on a.employeenumber = b.yhdemp
LEFT JOIN #hours c on a.employeenumber = c.employeenumber
ORDER BY firstname, lastname;

-- this looks LIKE a reasonable base FROM which to run an UPDATE
-- the pto rate for every tech changes
SELECT d.*, f.*
FROM (
  SELECT trim(a.firstname) + ' ' + a.lastname AS Name, a.employeenumber AS [Emp#], 
    b.grosspay-b.ptopay AS [Comm Pay], c.clockhours AS [Clock Hours], 
    (b.grosspay-b.ptopay)/c.clockhours AS [New PTO Rate],
    a.previoushourlyrate AS [Current PTO Rate], ((b.grosspay-b.ptopay)/c.clockhours) - a.previoushourlyrate AS change
  FROM #techs a
  LEFT JOIN #pay b on a.employeenumber = b.yhdemp
  LEFT JOIN #hours c on a.employeenumber = c.employeenumber) d
LEFT JOIN tpTechs e on d.[Emp#] = e.employeenumber
LEFT JOIN tpTechValues f on e.techkey = f.techkey
  AND f.thrudate > curdate()  
ORDER BY d.name  

-- any current techs NOT IN #techs?, nope -------------------------------------
SELECT a.*
FROM tpTechs a
INNER JOIN tpTeamTechs b on a.techkey = b.techkey
  AND b.thrudate > curdate()
LEFT JOIN #techs c on a.employeenumber = c.employeenumber
WHERE c.firstname IS NULL   
  AND a.departmentkey = 18

select a.*
FROM tpdata a
LEFT JOIN #techs b on a.employeenumber = b.employeenumber
WHERE a.thedate = curdate()
  AND a.departmentkey = 18
  AND b.employeenumber IS NULL 
-------------------------------------------------------------------------------

SELECT *
FROM tpdata
WHERE payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpdata
  WHERE thedate = curdate())
  AND departmentkey = 18
ORDER BY technumber, thedate


-- this looks LIKE a reasonable base FROM which to run an UPDATE
-- the pto rate for every tech changes
SELECT d.*, f.*
INTO #newPTO
FROM (
  SELECT trim(a.firstname) + ' ' + a.lastname AS Name, a.employeenumber AS [Emp#], 
    b.grosspay-b.ptopay AS [Comm Pay], c.clockhours AS [Clock Hours], 
    (b.grosspay-b.ptopay)/c.clockhours AS [New PTO Rate],
    a.previoushourlyrate AS [Current PTO Rate], ((b.grosspay-b.ptopay)/c.clockhours) - a.previoushourlyrate AS change
  FROM #techs a
  LEFT JOIN #pay b on a.employeenumber = b.yhdemp
  LEFT JOIN #hours c on a.employeenumber = c.employeenumber) d
LEFT JOIN tpTechs e on d.[Emp#] = e.employeenumber
LEFT JOIN tpTechValues f on e.techkey = f.techkey
  AND f.thrudate > curdate()  


SELECT * FROM #newPTO

-- need to UPDATE tpTechValues with new pto rate
-- AND tpData for the current payperiod with the new pto rate
-- tpTechValues: no current rows have a fromdate of 02/08/2015
BEGIN TRANSACTION;
TRY 
UPDATE tpTechValues
SET thruDate = '02/07/2015'
WHERE techkey IN (
  SELECT techkey
  FROM #newPTO)
AND thruDate = '12/31/9999';

INSERT INTO tpTechValues
SELECT techkey, '02/08/2015','12/31/9999', techTeamPercentage, [new pto rate]
FROM #newPTO;

UPDATE tpData
SET techHourlyRate = x.previousHourlyRate
FROM (
  SELECT *
  FROM tpTechValues
  WHERE thruDate > curdate()) x
WHERE departmentKey = 18
  AND payPeriodSeq = (
    SELECT distinct payPeriodSeq
    FROM tpData
    WHERE thedate = curdate())
  AND tpData.techKey = x.techKey;      
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;    
  
  
SELECT *
FROM tptechvalues
WHERE techkey IN (
  SELECT techkey
  FROM #newPTO)
ORDER BY thrudate DESC   