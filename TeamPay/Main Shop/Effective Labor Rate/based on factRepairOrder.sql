/*
wish i could get to a query based derivation of ELR
this IS pretty fucking CLOSE, USING final CLOSE date OR CLOSE date
but NOT perfect, ie, NOT verifiable with arkona
*/

-- total for time period
-- excludes departmental tech numbers (pdq tech, shop misc, etc)
-- service type AM AND MR only
4/6 -> 9/20
                    ROs	    hours	  sales	    rate
Arkona              9623    20723   1857248   89.62        
query               9494	  20684	  1857162	  89.79

so, IF i take arkona, subtract out dept techs, i get
                    9416    20635   1853190   89.8
so i am either really fucking CLOSE, OR this IS a hell of a coincidence                    

-- DROP TABLE #elrTotal;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '04/06/2014';
@thruDate = '09/20/2014';
SELECT COUNT(DISTINCT ro) AS ROs,
  round(SUM(flaghours), 0) AS hours, 
  round(SUM(laborsales), 0) AS sales, 
  iif(sum(flaghours) = 0, 0, round(SUM(laborsales)/SUM(flaghours), 2)) AS rate
-- SELECT *
INTO #elrTotal
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
INNER JOIN dds.dimserviceType c on a.serviceTypeKey = c.serviceTypeKey
  AND c.servicetypekey IN (
    SELECT serviceTypeKey
    FROM dds.dimservicetype
    WHERE serviceTypeCode IN ('AM','MR'))
INNER JOIN dds.dimtech d on a.techkey = d.techkey    
WHERE a.storecode = 'RY1'
--  AND a.laborsales <> 0
--  AND a.flaghours <> 0
--  AND (a.laborsales + a.flaghours) <> 0
  AND b.thedate BETWEEN @fromDate AND @thruDate
  AND d.techkey IN (
    SELECT techKey
    FROM dds.dimtech
    WHERE flagdeptcode = 'MR');  
  AND d.techkey NOT IN (
    SELECT techKey
    FROM dds.dimtech
    WHERE employeenumber = 'NA');

SELECT * FROM dds.dimtech
    
-- GROUP BY tech
-- DROP TABLE #elrByTech;
-- ADD adjustmnets, no apparent effect
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '04/06/2014';
@thruDate = '09/20/2014';
SELECT d.techNumber, d.name, 
-- SELECT 
  COUNT(DISTINCT ro) AS ROs,
  round(SUM(flaghours), 0) AS hours, 
  round(SUM(laborsales), 0) AS sales, 
  iif(sum(flaghours) = 0, 0, round(SUM(laborsales)/SUM(flaghours), 2)) AS rate
--  round(SUM(e.adjHours), 2)
-- SELECT *
INTO #elrByTech
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
INNER JOIN dds.dimserviceType c on a.serviceTypeKey = c.serviceTypeKey
  AND c.servicetypekey IN (
    SELECT serviceTypeKey
    FROM dds.dimservicetype
    WHERE serviceTypeCode IN ('AM','MR'))
INNER JOIN dds.dimtech d on a.techkey = d.techkey    
--LEFT JOIN (
--  SELECT ptro#, pttech, SUM(ptlhrs) AS adjHours
--  FROM dds.stgArkonaSDPXTIM
--  GROUP BY ptro#, pttech
--  HAVING SUM(ptlhrs) <> 0) e on d.technumber = e.pttech AND a.ro = e.ptro#  
WHERE a.storecode = 'RY1'
  AND b.thedate BETWEEN @fromDate AND @thruDate
  AND d.techkey NOT IN (
    SELECT techKey
    FROM dds.dimtech
    WHERE employeenumber = 'NA')
GROUP BY d.techNumber, d.name;

SELECT * FROM #elrByTech;

            techNumber	ROs	hours	sales	rate
query       637	        85	176	  17166	97.48
arkona      637         77  144   13799 95.69

-- an individual tech, with ro detail
-- DROP TABLE #techElrDetail
/*
tech 637
arkona does NOT show ros 
BE FUCKING CAUSE labor profit analysis IS USING finalCloseDate
these ALL have a finalCloseDate > 9/20 AND a closeDate < 9/20
these all
16164120  16.4 hrs, 1731.18 sales
16164230   1.2       126.67
16164554   2.2       232.23


*/
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @techNumber string;
@fromDate = '04/06/2014';
@thruDate = '09/20/2014';
@techNumber = '637';
SELECT d.techNumber, ro, round(sum(flaghours), 2) AS hours, 
  round(sum(laborsales), 2) AS sales
-- SELECT *
INTO #techElrDetail
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
INNER JOIN dds.dimserviceType c on a.serviceTypeKey = c.serviceTypeKey
  AND c.servicetypekey IN (
    SELECT serviceTypeKey
    FROM dds.dimservicetype
    WHERE serviceTypeCode IN ('AM','MR'))
INNER JOIN dds.dimtech d on a.techkey = d.techkey    
WHERE a.storecode = 'RY1'
  AND b.thedate BETWEEN @fromDate AND @thruDate
  AND d.techNumber = @techNumber
GROUP BY d.techNumber, ro  
UNION
SELECT d.techNumber, 'Total', round(sum(flaghours), 2) AS hours, 
  round(sum(laborsales), 2) AS sales
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimserviceType c on a.serviceTypeKey = c.serviceTypeKey
  AND c.servicetypekey IN (
    SELECT serviceTypeKey
    FROM dds.dimservicetype
    WHERE serviceTypeCode IN ('AM','MR'))
INNER JOIN dds.dimtech d on a.techkey = d.techkey    
WHERE a.storecode = 'RY1'
  AND b.thedate BETWEEN @fromDate AND @thruDate
  AND d.techNumber = @techNumber
GROUP BY d.techNumber;

select * FROM #techElrDetail;


AS this gets closer to being usable, generate a base TABLE FROM which the elr
can be generated for any period
i want to be able to match up what has been used IN team pay thus far, so go
back to 1/1/14



-- DROP TABLE #wtf;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '01/05/2014';
@thruDate = curdate();
SELECT thedate, technumber, flagdeptcode, ro, round(SUM(flaghours), 2) AS hours,
  round(SUM(laborsales), 2) AS sales
INTO #wtf  
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
INNER JOIN dds.dimserviceType c on a.serviceTypeKey = c.serviceTypeKey
  AND c.servicetypekey IN (
    SELECT serviceTypeKey
    FROM dds.dimservicetype
    WHERE serviceTypeCode IN ('AM','MR'))
INNER JOIN dds.dimtech d on a.techkey = d.techkey    
WHERE a.storecode = 'RY1'
  AND b.thedate BETWEEN @fromDate AND @thruDate
--  AND d.techkey NOT IN (
--    SELECT techKey
--    FROM dds.dimtech
--    WHERE technumber = '103')
GROUP BY thedate, technumber, flagdeptcode, ro    
-- this IS promising, i think i have regenerated labor sales IN factRepairOrder
-- back to 1/2014, given that, IF i look at just flagdeptcode = MR, these
-- are ALL within a few cents of the ELR that was used IN team pay
SELECT flagdeptcode, round(SUM(hours), 0), round(SUM(sales), 0), 
  iif(sum(hours) = 0, 0, round(SUM(sales)/SUM(hours), 2))
FROM #wtf
WHERE thedate BETWEEN '01/12/2014' AND '06/28/2014'
GROUP BY flagdeptcode

SELECT round(SUM(hours), 0), round(SUM(sales), 0), 
  iif(sum(hours) = 0, 0, round(SUM(sales)/SUM(hours), 2))
FROM #wtf
WHERE flagdeptcode = 'mr'
 AND thedate BETWEEN '12/15/2013' AND '05/31/2014'
 
 
SELECT b.thedate AS opendate, ro, laborsales, c.thedate
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.day c on a.finalclosedatekey = c.datekey 
WHERE b.thedate > '11/01/2013'
  AND laborsales IS NOT NULL 
-- WHEN did real labor sales kick IN (i only updated so far back, based on final CLOSE date)  
SELECT MAX(opendate)
FROM (
SELECT b.thedate AS opendate, ro, laborsales, c.thedate
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.day c on a.finalclosedatekey = c.datekey 
WHERE b.thedate > '11/01/2013') x
WHERE x.laborsales IS NULL 

feeling real good about this shit,
generate some random elr IN arkona USING periods of time relevant to #wtf


SELECT round(SUM(hours), 2), round(SUM(sales), 2), 
  iif(sum(hours) = 0, 0, round(SUM(sales)/SUM(hours), 2))
FROM #wtf
WHERE flagdeptcode = 'mr'
 AND thedate BETWEEN '10/01/2014' AND '10/12/2014' 
 
 
SELECT technumber, round(SUM(hours), 2), round(SUM(sales), 2), 
  iif(sum(hours) = 0, 0, round(SUM(sales)/SUM(hours), 2))
FROM #wtf
WHERE flagdeptcode = 'mr'
 AND thedate BETWEEN '04/20/2014' AND '10/04/2014' 
GROUP BY technumber

SELECT *
FROM #wtf
WHERE technumber = '557'
  AND thedate BETWEEN '04/20/2014' AND '10/04/2014' 
ORDER BY ro  
------------------------------------------------------------------------------- 
--</ 10/15/2017 -----------------------------------------------------------------
-------------------------------------------------------------------------------
wondering how jeri gets ELR for service type IN her budgeting spreadsheets
use the above wtf query to get started, looks pretty good

first question, WHERE IS laborsales IN factrepairorder coming from?
just AS i feared, coming FROM sdprdet, but, operationally may be good enuf

SELECT * FROM dds.dimtech
-- DROP TABLE #wtf;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '10/01/2017';
@thruDate = curdate();
SELECT thedate, technumber, d.name, flagdeptcode, e.paymenttypecode, ro, round(SUM(flaghours), 2) AS hours,
  round(SUM(laborsales), 2) AS sales
INTO #wtf  
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
-- INNER JOIN dds.day b on a.closedatekey = b.datekey
INNER JOIN dds.dimserviceType c on a.serviceTypeKey = c.serviceTypeKey
--  AND c.servicetypekey IN (
--    SELECT serviceTypeKey
--    FROM dds.dimservicetype
--    WHERE serviceTypeCode IN ('AM','MR'))
INNER JOIN dds.dimtech d on a.techkey = d.techkey    
INNER JOIN dds.dimpaymenttype e on a.paymenttypekey = e.paymenttypekey
WHERE a.storecode = 'RY1'
  AND b.thedate BETWEEN @fromDate AND @thruDate
--  AND d.techkey NOT IN (
--    SELECT techKey
--    FROM dds.dimtech
--    WHERE technumber = '103')
GROUP BY thedate, technumber, d.name, e.paymenttypecode, flagdeptcode, ro    
-- this IS promising, i think i have regenerated labor sales IN factRepairOrder
-- back to 1/2014, given that, IF i look at just flagdeptcode = MR, these
-- are ALL within a few cents of the ELR that was used IN team pay

SELECT technumber, paymenttypecode, SUM(hours), SUM(sales) FROM #wtf GROUP BY paymenttypecode, technumber 
UNION
SELECT technumber, 'Z', SUM(hours), SUM(sales) FROM #wtf GROUP BY  technumber 
  ORDER BY technumber, paymenttypecode

SELECT *
FROM #wtf
WHERE technumber = '537'
  AND paymenttypecode = 'c'  
ORDER BY ro
  
SELECT b.thedate, c.servicetypecode, a.*
FROM dds.factrepairorder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
  AND b.yearmonth = 201710
INNER JOIN dds.dimservicetype c on a.servicetypekey = c.servicetypekey
  AND c.servicetypecode IN ('MR','AM')
    
SELECT *
FROM dds.factrepairorder
WHERE ro = '16291513'

 SELECT *
 FROM dds.dimlinestatus
 
 -- DO some comparison to jeris spreadsheet
 -- shit, i can DO this IN pg
 SELECT *
 FROM dds.factrepairorder a
 inner
-------------------------------------------------------------------------------
--/> 10/15/2017 -----------------------------------------------------------------
-------------------------------------------------------------------------------
-- 10/5
looks pretty good
sorting thru diffs
tech 532 ark rate: 97.67, query rate: 97.52 
arkona includes ro 16166631, 4.5 hrs 481.95, query does NOT
ro IS IN cashier status, closed but NOT final closed, warr line OPEN
maybe look at this FROM the persepective of line status
SELECT * FROM dds.dimlinestatus 
nope, DO NOT think that will WORK, DO NOT have a date the line closed
may be good enuf just USING final CLOSE date
check another tech with a diff
557:
      hours     sales        rate
ark   1080.17   100880.55    93.39     
query 1100.47   103027.14    93.62 

ro       IN ark   IN query

print the ark to a csv
CREATE TABLE tmp577 (
  ro cichar(9),
  sales numeric (9,2),
  hours numeric (9,2),
  rate numeric (9,2)) IN database;
insert into tmp577 values ('16139274',17.5,0.5,35);
...
insert into tmp577 values ('18031859',374.85,3.5,107.1);

-- IN query,NOT IN arkona
SELECT *
FROM (
  SELECT *
  FROM #wtf
  WHERE technumber = '557'
    AND thedate BETWEEN '04/20/2014' AND '10/04/2014') a
LEFT JOIN tmp577 b on a.ro = b.ro
WHERE b.ro IS NULL     
-- IN arkona, NOT IN query  
SELECT *
FROM tmp577 a
LEFT JOIN (
  SELECT *
  FROM #wtf
  WHERE technumber = '557'
    AND thedate BETWEEN '04/20/2014' AND '10/04/2014') b on a.ro = b.ro
WHERE b.ro IS NULL       
-- hours don't match
SELECT *
FROM tmp577 a
INNER JOIN (
  SELECT *
  FROM #wtf
  WHERE technumber = '557'
    AND thedate BETWEEN '04/20/2014' AND '10/04/2014') b on a.ro = b.ro
WHERE a.hours <> b.hours
 OR a.sales <> b.sales