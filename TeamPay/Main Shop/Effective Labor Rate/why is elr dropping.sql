DROP TABLE #wtf;

SELECT b.thedate, a.ro, c.opcode, e.serviceTypeCode, f.paymenttype, d.name, a.flaghours, a.laborsales, 
  round(a.laborsales/a.flaghours, 2) AS elr
INTO #wtf
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
INNER JOIN dds.dimOpCode c on a.corcodekey = c.opcodekey
INNER JOIN dds.dimtech d on a.techkey = d.techkey
INNER JOIN dds.dimServiceType e on a.serviceTypeKey = e.serviceTypeKey
INNER JOIN dds.dimPaymentType f on a.paymenttypekey = f.paymenttypekey
WHERE b.thedate BETWEEN '01/26/2014' AND '10/04/2014'
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('mr','am'))
  AND flaghours <> 0    
  AND laborsales <> 0
  AND a.storecode = 'ry1'
  
SELECT 'high', opcode, servicetypecode, paymenttype, COUNT(*) howMany, MIN(elr) AS mini, MAX(elr) AS maxi, 
  round(avg(elr), 2) AS aver
FROM #wtf
WHERE elr < 90
  AND thedate BETWEEN '01/26/2014' AND '07/12/2014'
GROUP BY opcode, servicetypecode, paymenttype HAVING count(*) > 10
UNION
SELECT 'low', opcode, COUNT(*) howMany, MIN(elr) AS mini, MAX(elr) AS maxi, 
  round(avg(elr), 2) AS aver
FROM #wtf
WHERE elr < 90
  AND thedate BETWEEN '04/20/2014' AND '10/04/2014'
GROUP BY opcode HAVING COUNT(*) > 10
ORDER BY opcode


SELECT *
FROM #wtf
WHERE opcode = 'n/a'
  AND elr < 90

-- 10/13 % of pay types over duration
-- neither of these shows me much of anything
SELECT total, round(100.0*cp/total, 1) AS cp, round(100.0*internal/total, 1) AS internal,
  round(100.0*war/total,1) AS war, round(100.0*sc/total, 1) AS sc
FROM (  
  SELECT COUNT(*) AS total,
    SUM(CASE WHEN paymenttype = 'customer pay' THEN 1 ELSE 0 END) AS cp,
    SUM(CASE WHEN paymenttype = 'internal' THEN 1 ELSE 0 END) AS internal,
    SUM(CASE WHEN paymenttype = 'warranty' THEN 1 ELSE 0 END) AS war,
    SUM(CASE WHEN paymenttype = 'service contract' THEN 1 ELSE 0 END) AS sc
  FROM #wtf  
  WHERE thedate BETWEEN '01/26/2014' AND '07/12/2014') x
UNION
SELECT total, round(100.0*cp/total, 1) AS cp, round(100.0*internal/total, 1) AS internal,
  round(100.0*war/total,1) AS war, round(100.0*sc/total, 1) AS sc
FROM (  
  SELECT COUNT(*) AS total,
    SUM(CASE WHEN paymenttype = 'customer pay' THEN 1 ELSE 0 END) AS cp,
    SUM(CASE WHEN paymenttype = 'internal' THEN 1 ELSE 0 END) AS internal,
    SUM(CASE WHEN paymenttype = 'warranty' THEN 1 ELSE 0 END) AS war,
    SUM(CASE WHEN paymenttype = 'service contract' THEN 1 ELSE 0 END) AS sc
  FROM #wtf  
  WHERE thedate BETWEEN '04/20/2014' AND '10/04/2014') x  
  

SELECT total, round(100.0*cp/total, 1) AS cp, round(100.0*internal/total, 1) AS internal,
  round(100.0*war/total,1) AS war, round(100.0*sc/total, 1) AS sc
FROM (  
  SELECT round(SUM(flaghours), 0) AS total,
    round(SUM(CASE WHEN paymenttype = 'customer pay' THEN flaghours ELSE 0 END), 0) AS cp,
    round(SUM(CASE WHEN paymenttype = 'internal' THEN flaghours ELSE 0 END), 0) AS internal,
    round(SUM(CASE WHEN paymenttype = 'warranty' THEN flaghours ELSE 0 END), 0) AS war,
    round(SUM(CASE WHEN paymenttype = 'service contract' THEN flaghours ELSE 0 END), 0) AS sc
  FROM #wtf  
  WHERE thedate BETWEEN '01/26/2014' AND '07/12/2014') x
UNION
SELECT total, round(100.0*cp/total, 1) AS cp, round(100.0*internal/total, 1) AS internal,
  round(100.0*war/total,1) AS war, round(100.0*sc/total, 1) AS sc
FROM (  
  SELECT round(SUM(flaghours), 0) AS total,
    round(SUM(CASE WHEN paymenttype = 'customer pay' THEN flaghours ELSE 0 END), 0) AS cp,
    round(SUM(CASE WHEN paymenttype = 'internal' THEN flaghours ELSE 0 END), 0) AS internal,
    round(SUM(CASE WHEN paymenttype = 'warranty' THEN flaghours ELSE 0 END), 0) AS war,
    round(SUM(CASE WHEN paymenttype = 'service contract' THEN flaghours ELSE 0 END), 0) AS sc
  FROM #wtf  
  WHERE thedate BETWEEN '04/20/2014' AND '10/04/2014') x   
  
-- what ben IS actually looking at IS flag hours with no labor sales  
-- this IS NOT it
DROP TABLE #wtf1;
SELECT b.thedate, a.ro, c.opcode, e.serviceTypeCode, f.paymenttype, d.name, a.flaghours, a.laborsales, 
  round(a.laborsales/a.flaghours, 2) AS elr
INTO #wtf1
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
INNER JOIN dds.dimOpCode c on a.corcodekey = c.opcodekey
INNER JOIN dds.dimtech d on a.techkey = d.techkey
INNER JOIN dds.dimServiceType e on a.serviceTypeKey = e.serviceTypeKey
INNER JOIN dds.dimPaymentType f on a.paymenttypekey = f.paymenttypekey
WHERE b.thedate BETWEEN '01/26/2014' AND '10/04/2014'
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('mr','am'))
  AND flaghours > 0    
  AND laborsales = 0
  AND a.storecode = 'ry1'
  
SELECT round(SUM(flaghours), 1)
FROM #wtf1  
WHERE thedate BETWEEN '01/26/2014' AND '07/12/2014'
  AND paymenttype = 'internal'

SELECT round(SUM(flaghours), 1)
FROM #wtf1  
WHERE thedate BETWEEN '04/20/2014' AND '10/04/2014'

-- 10/16
look at elr on internal WORK across the 2 periods
AND paymenttype = 'internal'

-- DROP TABLE #wtf;
SELECT b.thedate, a.ro, c.opcode, e.serviceTypeCode, f.paymenttype, d.name, a.flaghours, a.laborsales 
--  round(a.laborsales/a.flaghours, 2) AS elr
INTO #wtf
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
INNER JOIN dds.dimOpCode c on a.corcodekey = c.opcodekey
INNER JOIN dds.dimtech d on a.techkey = d.techkey
INNER JOIN dds.dimServiceType e on a.serviceTypeKey = e.serviceTypeKey
INNER JOIN dds.dimPaymentType f on a.paymenttypekey = f.paymenttypekey
WHERE b.thedate BETWEEN '02/01/2014' AND '09/30/2014'
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('mr','am'))
  AND a.paymentTypeKey = (
    SELECT paymentTypeKey
    FROM dds.dimPaymentType
    WHERE paymentTypeCode = 'I')   
--  AND flaghours <> 0    
--  AND laborsales <> 0
  AND a.storecode = 'ry1';

  
SELECT b.monthName, round(SUM(flaghours), 1) AS flaghours, 
  round(SUM(laborsales), 1) AS laborsales,
  round(round(SUM(laborsales), 1)/round(SUM(flaghours), 1), 2) AS elr
FROM #wtf a
INNER JOIN dds.day b on a.thedate = b.thedate
GROUP BY month(a.thedate), b.monthName


-- DO a grouping on paytype
-- DROP TABLE #wtf;
SELECT b.thedate, a.ro, c.opcode, e.serviceTypeCode, f.paymenttype, d.name, a.flaghours, a.laborsales
INTO #wtf
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.finalclosedatekey = b.datekey
INNER JOIN dds.dimOpCode c on a.corcodekey = c.opcodekey
INNER JOIN dds.dimtech d on a.techkey = d.techkey
INNER JOIN dds.dimServiceType e on a.serviceTypeKey = e.serviceTypeKey
INNER JOIN dds.dimPaymentType f on a.paymenttypekey = f.paymenttypekey
WHERE b.thedate BETWEEN '02/01/2014' AND '09/30/2014'
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('mr','am'))
--  AND flaghours <> 0    
--  AND laborsales <> 0
  AND a.storecode = 'ry1'
  
SELECT b.monthName, paymenttype, round(SUM(flaghours), 1) AS flaghours, 
round(SUM(laborsales), 1) AS laborsales,
round(round(SUM(laborsales), 1)/round(SUM(flaghours), 1), 2) AS elr
FROM #wtf a
INNER JOIN dds.day b on a.thedate = b.thedate
GROUP BY month(a.thedate), b.monthName, paymenttype