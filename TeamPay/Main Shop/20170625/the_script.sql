/*
We are moving Desiree from Midas and taking her off of flat rate to work in our quick service department.
Her wage is staying at $16.78 but hourly. We are also moving Travis Strandell to Midas
and moving him from $15 per hour to $16.78 per flat rate hour on team Midas.
I submitted both of these changes to Kim already and hoping to go into effect on 6-25-17 if possible. 
Let me know if you have questions. Thanks!!

We will keep the PTO rate at his previous hourly rate until we change in Feb 2018 please. Same goes for Joshua Syverson please. TY
select * FROM tpteams WHERE teamname = 'bear'
SELECT * FROM tptechs WHERE lastname = 'strandell'
SELECT * FROM tptechvalues WHERE techkey = 26

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
  AND a.teamkey =22
ORDER BY teamname, firstname  

SELECT *
FROM tpteamvalues
WHERE thrudate > curdate()

the from/thru dates on tptechs seem pointless, this IS my latest "operational" thinking
*/


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
@eff_date = '06/25/2017';
@thru_date = '06/24/2017';
@dept_key = 18;
@elr = 91.46;
BEGIN TRANSACTION;
TRY
-- remove desiree FROM team bear
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'grant');
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'bear');
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key
	AND thrudate > curdate();
-- UPDATE desiree's row IN tpTechFalues	
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  
-- ADD travis strandell to team bear
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'strandell');
-- first undo the thru date IN tptechs for travis  
  UPDATE tptechs
  SET thrudate = '12/31/9999'
  WHERE techkey = @tech_key;
-- ADD travis to team bear  
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);
/*
budget remains the same: 76.83, no change to team values
travis's percentage = 16.78/76.83 = 21.84
his pto rate = 15.00, 
he still has an OPEN row IN tpTechValues
*/
-- CLOSE old row
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
-- new row
  INSERT INTO tpTechValues (techkey,fromdate,techTeamPercentage,previousHourlyRate)
  values (@tech_key, @eff_date, 16.78/76.83, 15);
     
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date
	AND teamkey IN (
      SELECT teamkey
      FROM tpteams
      WHERE teamkey = @team_key
        AND thrudate > curdate());
			  
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();      

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
