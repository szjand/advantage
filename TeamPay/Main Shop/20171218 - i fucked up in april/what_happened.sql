SELECT teamName, 
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 18
--  AND a.teamkey = 22
ORDER BY teamname, firstname  


SELECT *
FROM tpdata
WHERE lastname IN ('david','thompson','anderson')
AND thedate BETWEEN '04/28/2017' AND '05/02/2017'
ORDER BY lastname, thedate

-- no change IN techTeamPercentage that correlates to 4/30/17
SELECT *
FROM tptechvalues a
INNER JOIN tptechs b on a.techkey = b.techkey
WHERE lastname IN ('david','thompson','anderson')
ORDER BY b.lastname, a.fromdate

-- these (AND some other) teams got a budget increase, NOT census change, on 4/30/17
SELECT *
FROM tpteamvalues a
INNER JOIN tpteams b on a.teamkey = b.teamkey
WHERE departmentkey = 18
--  and teamname IN ('gray','anderson')
  and a.fromdate > '01/01/2016'
ORDER BY teamname, a.fromdate

what i neglected to DO was to adjust down the techs techteamrate on teams WHERE the
budget increased but those techs did NOT get an increase