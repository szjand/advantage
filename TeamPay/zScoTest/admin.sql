SELECT d.teamName, techNumber, left(trim(firstName) + ' ' + lastname, 25) AS Name, 
  techTeamPercentage, e.budget, round(techTeamPercentage*e.budget, 2) AS TechCommRate,
  previousHourlyRate AS HourlyRate 
FROM tpTechs a
INNER JOIN tpTechValues b on a.techKey = b.techKey
INNER JOIN tpTeamTechs c on a.techKey = c.techkey
INNER JOIN tpTeams d on c.teamKey = d.teamKey
INNER JOIN tpTeamValues e on d.teamKey = e.teamKey
WHERE b.thrudate > curdate()
  AND c.thrudate > curdate()
  AND d.thrudate > curdate()
  AND e.thruDate > curdate()
ORDER BY teamName, technumber


the goal: a single script that closes out the existing payperiod AND generates
the appropriate data for the new payperiod

DELETE current pay period data FROM tpdata AND ROLLBACK changes made to 
accommodate the current pay period, DO a script to DO the changes with an effective date,
run tpdata AND check that ALL IS well
use the new team structure AS the test

are the updates for the new payperiod being done before that payperiod actually starts OR after

tables

distinct changes:
  new ELR
  remove tech
  ADD tech
  remove team
  ADD team
  change techTeamPercentage
  ADD tech to team
  remove tech FROM team
  
tables.attributes that can change
tpTechValues.fromDate
tpTechValues.thruDate
tpTechValues.techTeamPercentage

tpShopValues.fromDate
tpShopValues.thruDate
tpShopValues.effectiveLaborRate

tpTeamTechs.teamKey
tpTeamTechs.techKey
tpTeamTechs.fromDate
tpTeamTechs.thruDate

tpTeamValues.fromDate
tpTeamValues.thruDate
tpTeamValues.census
tpTeamValues.poolPercentage
tpTeamValues.budget

tpTeams.teamName
tpTeams.fromDate
tpTeams.thruDate
/*
-- 1/22/
WHERE i LEFT off, primarky key for tpTechs
the snodgrass notion of sequenced unique rows
instead of making FROM OR thru date part of the key, just the values that need
to be unique at any point IN time AND a trigger assertion
so, for techs, what i want IS that there will never be more than one combination
of dept/tech#payType at any point IN time
AND the dates DO NOT guarantee that supposedly
wait a minute
a tech (dept/tech#payType) can occur more than once, but NOT more than once 
at the same time
fuck, so i am thinking, i am ok with fromDate
wait wait wait
at any point IN time there can only be one dept/tech# instance
dept/tech# must be unique
wait wait wait
IF i am going to include the paytype IN the tpTech TABLE, that has to be part of it
feels LIKE i need to decompose fucking paytype out of tpTech
the paytype can vary independently of the dept/tech#
what about rehire/reuse
arkona does NOT allow reuse of tech/writer numbers, but we are NOT diligent about
updating arkona with inactive tech numbers: think Garrett

this IS NOT a fucking dimensional model
*/

-- 1/25

need to define tpTeams IN terms of the temporal data
believe it IS the same AS tpTechs, from/thru define the period of validity
just AS with techs (there will always only be 1 row for dept/tech#), there
will only only be 1 row for dept/teamName
which means that from/thru are NOT part of the NK
(hmm, same thing for tpShopValues - nope, repeating values for dept/ELR can
	  occur at different times)
	  
now for tpTeamTechs
    thinking that only one row with thru = NULL handles these first 2 
	a tech can be assigned to the same team at different points IN time
	a tech can be assigned to only one team at any point IN time
	a team/tech assignment can only be for an interval IN which both the
	  tech AND team are valid - this one will have to be an assertion

yes, DO this:	
WHEN i removed team stryker, should i at that time CLOSE ALL teamtech assignments
think so
that should yield teampay techs without a team assignment for the new period
teamtechs IS WHERE the rubber meets the road
	
ok, tpTechValues
	for a techKey:
	  previousHourlyRate IS a constant (never changes)
1-25  shit, previousHourlyRate should actually be an attribute of tpTechs, it never changes
but since it will have no relevance after the upcoming payperiod, fuck it for now	  
	  only 1 techTeamPercentage at any point in time
	  can have the same techTeamPerc at different points IN time
	  .fromDate must be >= tpTech.fromDate AND < tpTech.thruDate for a given techKey
   	
	
	  
--< clean up -------------------------------------------------------------------< 
-- get rid of non teampay techs (no data IN tpData)
delete
FROM tptechs 
WHERE NOT EXISTS (
  SELECT 1
  FROM tpteamtechs
  WHERE techkey = tptechs.techkey);
-- ok, restore to previous payperiod
-- 1/26 lets DO real time periods on zscotest
--DELETE FROM tpTeamValues WHERE fromDate = '01/12/2014';
--UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromDate = '12/29/2013';
--DELETE FROM tpShopValues WHERE fromDate = '01/12/2014';
--UPDATE tpShopValues SET thruDate = '12/31/9999' WHERE fromDate = '12/29/2013';
--DELETE FROM tpTeamValues WHERE fromDate = '01/12/2014';
--UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromDate = '12/29/2013';
--DELETE FROM tpData WHERE theDate >= '01/12/2014';
DELETE FROM tpData WHERE theDate >= '01/26/2014';
--/> clean up ------------------------------------------------------------------/> 

--< DDL ------------------------------------------------------------------------<
--tpTechs DDL
--  have to synchronize from/thru dates 
--  ALL queries will have to honor payType, from/thru
  
CREATE TABLE tpTechPayTypes (
  payType cichar(24) constraint NOT NULL,
  constraint PK Primary Key (payType)) IN database;
INSERT INTO tpTechPayTypes values('TeamPay');
INSERT INTO tpTechPayTypes values('Hourly');
INSERT INTO tpTechPayTypes values('Probationary');  

CREATE TABLE tpTechPay (
  techKey integer constraint NOT NULL,
  payType cichar(24) constraint NOT NULL,
  fromDate date,  
  thruDate date constraint not null default '12/31/9999',
  constraint PK primary key (techKey,payType,thruDate)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechPay','tpTechPay.adi','FK1','payType',
   '',2,512,'' );
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechPay','tpTechPay.adi','FK2','techkey',
   '',2,512,'' );    
    
ALTER TABLE tpTechs
ADD COLUMN fromDate date 
ADD COLUMN thruDate date default '12/31/9999';
      
UPDATE tpTechs
SET fromDate = '09/08/2013';
UPDATE tpTechs
SET thruDate = '12/31/9999'; 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpTechs', 
      'fromDate', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpTechsDatafail' );
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpTechs', 
      'thruDate', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpTechsDatafail' );		   

EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('tpTechs-tpTechPay',
     'tpTechs', 'tpTechPay', 'FK2', 
     2, 2, NULL, '', ''); 
	 
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('tpTechPayTypes-tpTechPay',
     'tpTechPayTypes', 'tpTechPay', 'FK1', 
     2, 2, NULL, '', ''); 	

EXECUTE PROCEDURE sp_DropReferentialIntegrity('tpTeams-tpTeamTechs');
EXECUTE PROCEDURE sp_DropReferentialIntegrity('tpTechs-tpTeamTechs');
DROP index tpTeams.NK;
DROP index tpTeamTechs.techKey;
DROP index tpTeamTechs.teamKey;
DROP index tpTeamTechs.PK;

EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeams','tpTeams.adi','NK','departmentKey;teamName',
   '',2051,512, '' );  
   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeamTechs','tpTeamTechs.adi','PK','teamKey;techKey;thruDate',
   '',2051,512, '' ); 	
EXECUTE PROCEDURE sp_ModifyTableProperty ('tpTeamTechs','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR',''); 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeamTechs','tpTeamTechs.adi','FK1','teamKey',
   '',2,512, '' ); 	 
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeamTechs','tpTeamTechs.adi','FK2','techKey',
   '',2,512, '' );   
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('tpTeams-tpTeamTechs',
     'tpTeams', 'tpTeamTechs', 'FK1', 
     2, 2, NULL, '', ''); 	
EXECUTE PROCEDURE 
  sp_CreateReferentialIntegrity ('tpTechs-tpTeamTechs',
     'tpTechs', 'tpTeamTechs', 'FK2', 
     2, 2, NULL, '', ''); 		
	 
-- techValues
DROP index tpTechValues.PK; 
ALTER TABLE tpTechValues DROP CONSTRAINT PRIMARY KEY;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechValues','tpTechValues.adi','PK','techKey;thruDate',
   '',2051,512, '' ); 
EXECUTE PROCEDURE sp_ModifyTableProperty ('tpTechValues','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR','');    
  
-- tpTeamValues  fuckit, going for consistency with thruDate AS part of PK  
-- there can ever only be 1 row with a thrudate of 12/31/9999
DROP index tpTeamValues.PK; 
ALTER TABLE tpTeamValues DROP CONSTRAINT PRIMARY KEY;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTeamValues','tpTeamValues.adi','PK','teamKey;thruDate',
   '',2051,512, '' ); 
EXECUTE PROCEDURE sp_ModifyTableProperty ('tpTeamValues','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR','');
  
-- tpShopValues  fuckit, going for consistency with thruDate AS part of PK  
-- there can ever only be 1 row with a thrudate of 12/31/9999  
DROP index tpShopValues.PK; 
ALTER TABLE tpShopValues DROP CONSTRAINT PRIMARY KEY;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpShopValues','tpShopValues.adi','PK','departmentKey;thruDate',
   '',2051,512, '' ); 
EXECUTE PROCEDURE sp_ModifyTableProperty ('tpShopValues','Table_PRIMARY_KEY',
  'PK', 'VALIDATE_RETURN_ERROR','');     
  
-- populate tpTechPay
INSERT INTO tpTechPay (payType, techkey, fromDate)
SELECT 'teamPay', techkey, '09/08/2013' 
FROM tpTechs;     	     
--/> DDL -----------------------------------------------------------------------/>

-- new tech
/* 
should actually ADD ALL hourly, main shop techs at this point  
but will instead pussy out AND just DO sigette because he IS being added to teampay
*/
-- ADD kyle tpTech 
INSERT INTO tpTechs (departmentKey, techNumber, firstName, lastName, 
  employeeNumber, fromDate)    	  
SELECT 18, a.technumber, b.firstname, b.lastname, a.employeenumber,
  cast('12/30/2013' AS sql_date)
FROM dds.dimTech a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
WHERE a.technumber = '613' 
  AND a.storecode = 'ry1'  
  AND b.currentrow = true;  
-- new row requires tpTechPay row
INSERT INTO tpTechPay (payType, techkey, fromDate)
SELECT 'hourly', techkey, '12/30/2013'
FROM tpTechs
WHERE techNumber = '613'
  AND departmentKey = 18;
      
-- UPDATE kyle AS teamPay
-- effective date 1/26/14
DECLARE @effDate date;
@effDate = '01/26/2014';
UPDATE tpTechPay
SET thruDate = @effDate - 1
WHERE techKey = (
  select techKey
  from tpTechs
  WHERE techNumber = '613'
    AND departmentKey = 18)
  AND thruDate = '12/31/9999';
INSERT INTO tpTechPay (payType, techkey, fromDate)
SELECT 'teamPay', techkey, @effDate
FROM tpTechs
WHERE techNumber = '613'
  AND departmentKey = 18;  

-- new team   
INSERT INTO tpTeams (departmentKey, teamName, fromDate)
values (18, 'Longoria','01/26/2014'); -- effDate

-- "remove" team
UPDATE tpTeams
SET thruDate = '01/25/2014' -- effDate - 1
WHERE teamname = 'Stryker';
-- remove ALL teamtech assignments for the removed team
UPDATE tpTeamTechs
SET thruDate = '01/25/2014' -- effDate - 1
WHERE teamKey = (
  SELECT teamKey
  FROM tpTeams
  WHERE teamName = 'stryker');




--< tpTeamTechs - tpTechValues -------------------------------------------------<
-- team Longoria, ADD Yanish, Syverson, Holwerda, ALL moving FROM another team
-- 		 a off team McVeigh
--		 b on team longoria
-- 		 c tpTechValues
-- 1b
DECLARE @effDate date;
DECLARE @lastName string;
DECLARE @teamName string;
DECLARE @techKey integer;
DECLARE @teamKey integer;
DECLARE @perc double;
DECLARE @hourly double;

BEGIN TRANSACTION;
TRY 
@effDate = '01/26/2014';
@lastName = 'Yanish';
@teamName = 'Longoria';
@perc = .33;
@hourly = 11.22;
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);

UPDATE tpTeamTechs
SET thruDate = @effDate - 1
WHERE techKey = @techKey;

INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
values (@teamKey, @techKey, @effDate);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly);

@lastName = 'Syverson';
@teamName = 'Longoria';
@perc = .35;
@hourly = 12.5;
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
values (@teamKey, @techKey, @effDate);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly);

@lastName = 'Holwerda';
@teamName = 'Longoria';
@perc = .32;
@hourly = 10.82;
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
values (@teamKey, @techKey, @effDate);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly);

-- team Duckstad
--   tpTeamTechs: ADD stryker
--   tpTechValues: adj ALL techTeamPercentage
@lastName = 'Stryker';
@teamName = 'DuckStad';
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
values (@teamKey, @techKey, @effDate);

@lastName = 'Duckstad';
@perc = .29; -- ****************
@hourly = 19.83; -- ************
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly);

@lastName = 'Stryker';
@perc = .21; -- ****************
@hourly = 14; -- ************
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly);

@lastName = 'David';
@perc = .24; -- ****************
@hourly = 16.58; -- ************
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly);

@lastName = 'Thompson';
@perc = .26; -- ****************
@hourly = 17.51; -- ************
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly);

-- team McVeigh
--   tpTeamTechs, tpTechValues: ADD Sigette
--   tpTechValues: adj ALL techTeamPercentage
-- ADD Sigette to tpTeamTechs
@lastName = 'Sigette';
@teamName = 'McVeigh';
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
values (@teamKey, @techKey, @effDate);

-- ADD sigette to tpTechValues
@lastName = 'Sigette';
@perc = .22; -- ****************
@hourly = 14; -- ************
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly);

-- adjust other McVeigh team tpTechValues.techTeamPercentage
@lastName = 'McVeigh';
@perc = .25; -- ****************
@hourly = 13.5; -- ************
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly); 

@lastName = 'Ortiz';
@perc = .259; -- ****************
@hourly = 16.07; -- ************
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly); 
 
@lastName = 'Waldbauer';
@perc = .271; -- ****************
@hourly = 16; -- ************
@techKey = (SELECT techKey FROM tpTechs WHERE lastName = @lastName);
UPDATE tpTechValues
SET thruDate = @effDate -1
WHERE techKey = @techKey;
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @perc, @hourly);  

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 
--/> tpTeamTechs - tpTechValues ------------------------------------------------/>

--< Shop Values ----------------------------------------------------------------<
-- these are the actual values for this payperiod
-- actually, use huots number for zscotest, check his team math
  -- CLOSE previous pay period
  UPDATE tpShopValues
  SET thruDate = '01/25/2014'
  WHERE thrudate = '12/31/9999';       
  -- INSERT row for new pay period
  INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
  values(18, '01/26/2014', 89.21); 
--/> Shop Values ---------------------------------------------------------------/> 


--< Team Values ----------------------------------------------------------------<
-- shit, ALL team teamValues have changed due to the new ELR
-- new team Longoria -- first time entry IN to tpTeamValues
DECLARE @effDate date;
DECLARE @lastName string;
DECLARE @teamName string;
DECLARE @techKey integer;
DECLARE @teamKey integer;
DECLARE @census double;
DECLARE @ELR double;
DECLARE @perc double;
DECLARE @budget double;

BEGIN TRANSACTION;
TRY 
@effDate = '01/26/2014'; -- ***************
@teamName = 'Longoria'; -- ***************
@perc = .15; -- ***************
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@census = (
  SELECT COUNT(*) 
  FROM tpTeamTechs 
  WHERE teamKey = @teamKey 
    AND @effDate < thruDate);
@ELR = (
  SELECT effectiveLaborRate
  FROM tpShopValues
  WHERE @effDate < thruDate);	
@budget = round(@census * @perc * @ELR, 2);
INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
values (@teamKey, @effDate, @census, @perc, @budget);
-- CLOSE out tpTeamValues and insert new row for teams Duckstad AND McVeigh
-- with percentage AND census changes
@teamName = 'DuckStad'; -- ***************
@perc = .205; -- ***************
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@census = (
  SELECT COUNT(*) 
  FROM tpTeamTechs 
  WHERE teamKey = @teamKey 
    AND @effDate < thruDate);
@budget = round(@census * @perc * @ELR, 2);	
UPDATE tpTeamValues
SET thruDate = @effDate - 1
WHERE teamKey = @teamKey
  AND thrudate = '12/31/9999';
INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
values (@teamKey, @effDate, @census, @perc, @budget);  

@teamName = 'McVeigh'; -- ***************
@perc = .19; -- ***************
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@census = (
  SELECT COUNT(*) 
  FROM tpTeamTechs 
  WHERE teamKey = @teamKey 
    AND @effDate < thruDate);
@budget = round(@census * @perc * @ELR, 2);	
UPDATE tpTeamValues
SET thruDate = @effDate - 1
WHERE teamKey = @teamKey
  AND thrudate = '12/31/9999';
INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
values (@teamKey, @effDate, @census, @perc, @budget);  
-- CLOSE out tpTeamValues and insert new row for teams Gray, Olson & Westerhausen
-- with only ELR changes 
@teamName = 'Gray'; -- ***************
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@perc = (
  SELECT poolPercentage
  FROM tpTeamValues
  WHERE teamKey = @teamKey
    AND thruDate = '12/31/9999');
@census = (
  SELECT COUNT(*) 
  FROM tpTeamTechs 
  WHERE teamKey = @teamKey 
    AND @effDate < thruDate);
@budget = round(@census * @perc * @ELR, 2);	
UPDATE tpTeamValues
SET thruDate = @effDate - 1
WHERE teamKey = @teamKey
  AND thrudate = '12/31/9999';
INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
values (@teamKey, @effDate, @census, @perc, @budget);  

@teamName = 'Olson'; -- ***************
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@perc = (
  SELECT poolPercentage
  FROM tpTeamValues
  WHERE teamKey = @teamKey
    AND thruDate = '12/31/9999');
@census = (
  SELECT COUNT(*) 
  FROM tpTeamTechs 
  WHERE teamKey = @teamKey 
    AND @effDate < thruDate);
@budget = round(@census * @perc * @ELR, 2);	
UPDATE tpTeamValues
SET thruDate = @effDate - 1
WHERE teamKey = @teamKey
  AND thrudate = '12/31/9999'; 
INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
values (@teamKey, @effDate, @census, @perc, @budget);  

@teamName = 'Westerhausen'; -- ***************
@teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = @teamName);
@perc = (
  SELECT poolPercentage
  FROM tpTeamValues
  WHERE teamKey = @teamKey
    AND thruDate = '12/31/9999');
@census = (
  SELECT COUNT(*) 
  FROM tpTeamTechs 
  WHERE teamKey = @teamKey 
    AND @effDate < thruDate);
@budget = round(@census * @perc * @ELR, 2);	
UPDATE tpTeamValues
SET thruDate = @effDate - 1
WHERE teamKey = @teamKey
  AND thrudate = '12/31/9999';
INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
values (@teamKey, @effDate, @census, @perc, @budget);  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 
--/> Team Values ---------------------------------------------------------------/> 

/* assertions */

EXECUTE PROCEDURE xfmTpData();
EXECUTE PROCEDURE todayxfmTpData();
