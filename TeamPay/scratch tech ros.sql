DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionid string;
DECLARE @UserName cichar(50);
DECLARE @techNumber cichar(3);  
DECLARE @fromDate date;
DECLARE @thruDate date;
@date = curdate(); -- (SELECT date FROM __input);
@sessionid = '6a5a6eb244a9ee429bc9e83b0415ee70'; 
--(SELECT SessionID FROM __input) AND now() <= ValidThruTS)
@payperiodseq = (
  SELECT DISTINCT payperiodseq 
  FROM tpData 
  WHERE theDate = @date);
@UserName = (SELECT UserName FROM sessions WHERE sessionID = @sessionid);
@TechNumber = (
  SELECT techNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
@fromDate = (
  SELECT MIN(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
  
SELECT *
FROM dds.stgTechTrainingOther 
WHERE thedate BETWEEN @fromDate AND @thruDate
  AND technumber = @technumber 
              
SELECT d.thedate, ro,/* line, c.technumber,*/ round(sum(a.flaghours * b.weightfactor), 2)
FROM dds.factRepairOrder a  
INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
INNER JOIN dds.dimTech c on b.techkey = c.techkey
INNER JOIN dds.day d on a.flagdatekey = d.datekey
WHERE flaghours <> 0
  AND a.storecode = 'ry1'
  AND a.flagdatekey IN (
    SELECT datekey
    FROM dds.day
    WHERE thedate BETWEEN @fromDate AND @thruDate) 
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('AM','EM','MR'))   
  AND c.technumber = @techNumber
GROUP BY d.thedate, a.ro, c.technumber
        
            
    
  SELECT a.technumber, a.thedate, b.ro, b.line, sum(coalesce(b.flaghours, 0)) AS flaghours
  FROM tpData a
  LEFT JOIN (
    SELECT d.thedate, ro, line, c.technumber, a.flaghours, b.weightfactor
    FROM dds.factRepairOrder a  
    INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
    INNER JOIN dds.dimTech c on b.techkey = c.techkey
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'
      AND a.flagdatekey IN (
        SELECT datekey
        FROM dds.day
--        WHERE thedate BETWEEN '09/08/2013' AND curdate())  
        WHERE thedate BETWEEN @fromDate AND @thruDate) 
      AND a.servicetypekey IN (
        SELECT servicetypekey
        FROM dds.dimservicetype
        WHERE servicetypecode IN ('AM','EM','MR'))   
      AND c.technumber IN (
        SELECT technumber
        FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
    WHERE b.technumber = @techNumber   
    AND a.payperiodseq = @payperiodseq    
  GROUP BY a.technumber, a.thedate, b.ro, b.line