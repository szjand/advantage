/*
1/5/14
  going nuts with "rounding errors", eg, the payroll spreadsheet sent to kim
  Girodat:
  Commission Rate	Clock Hours	Total Commission Pay
  28.43	      77.41	             2,200.33
  whereas IN reality, 28.43 * 77.41 = 2200.77
  
  my first out of the ass hypothesis IS an unholy mixture of money AND double data types,
  so, change ALL money datatypes to double
  
  done, but did NOT seem to help
  
ALTER TABLE tpData
ALTER COLUMN TechHourlyRate TechHourlyRate double
ALTER COLUMN TechTFRRate TechTFRRate double
ALTER COLUMN TeamBudget TeamBudget double;
ALTER TABLE tpTeamValues
ALTER COLUMN Budget Budget double;

AND WHILE i am here, DO some cleanup
EXECUTE PROCEDURE sp_RenameDDObject( 'tpStgGrossPay', 'zUnused_tpStgGrossPay', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'zRoTech', 'zUnused_zRoTech', 1, 0);
EXECUTE PROCEDURE sp_RenameDDObject( 'zGlRo', 'zUnused_zGlRo', 1, 0);
*/

/*
DROP TABLE tpData;  
CREATE TABLE tpData (
  DateKey integer constraint NOT NULL,
  TheDate date constraint NOT NULL,
  PayPeriodStart date constraint NOT NULL,
  PayPeriodEnd date constraint NOT NULL,
  PayPeriodSeq integer constraint NOT NULL,
  DayOfPayPeriod integer constraint NOT NULL,
  DayName cichar(12) constraint NOT NULL,
  DepartmentKey integer constraint NOT NULL,
  TechKey integer, 
  TechNumber cichar(3),
  EmployeeNumber cichar(9),
  FirstName cichar(25),
  LastName cichar(25),
  TechTeamPercentage double,
  TechHourlyRate money, 
  TechTFRRate money, 
  TechClockHoursDay double,
  TechClockHoursPPTD double,
  TechPVHHoursDay double,
  TechPVHHoursPPTD double,
  TechFlagHoursDay double,
  TechFlagHoursPPTD double,
  TechFlagHourAdjustmentsDay double, 
  TechFlagHourAdjustmentsPPTD double,
  TeamKey integer,
  TeamName cichar(25),
  TeamCensus integer,
  TeamBudget money,
  TeamClockHoursDay double,
  TeamClockHoursPPTD double,
  TeamFlagHoursDay double,
  TeamFlagHoursPPTD double, 
  TeamPVHHoursDay double,
  TeamPVHHoursPPTD double,
  TeamProfDay double,
  TeamProfPPTD double,
  constraint PK primary key (TheDate, DepartmentKey, TechNumber)) IN database;
-- regular indexes   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpData','tpData.adi','TechDay','TechNumber;TheDate',
   '',2,512,'' );   
   
-- 11/25 break out vac pto hol  
ALTER table tpData   
DROP COLUMN TechPVHHoursDay
DROP COLUMN TechPVHHoursPPTD
ADD COLUMN TechVacationHoursDay double
ADD COLUMN TechVacationHoursPPTD double
ADD COLUMN TechPTOHoursDay double
ADD COLUMN TechPTOHoursPPTD double
ADD COLUMN TechHolidayHoursDay double
ADD COLUMN TechHolidayHoursPPTD double; 

-- ADD Other & Training
ALTER TABLE tpData
ADD COLUMN TechOtherHoursDay double
ADD COLUMN TechOtherHoursPPTD double
ADD COLUMN TechTrainingHoursDay double
ADD COLUMN TechTrainingHoursPPTD double;

ALTER TABLE tpData
ADD COLUMN TeamOtherHoursDay double
ADD COLUMN TeamOtherHoursPPTD double
ADD COLUMN TeamTrainingHoursDay double
ADD COLUMN TeamTrainingHoursPPTD double;

-- team numbers need to include flagtimeadjustments
ALTER TABLE tpData
ADD COLUMN TeamFlagHourAdjustmentsDay double
ADD COLUMN TeamFlagHourAdjustmentsPPTD double;

-- these are NOT relevant to teams
ALTER TABLE tpData
DROP COLUMN teampvhhoursday
DROP COLUMN teampvhhourspptd;

-- 12/11 payperiod SELECT dates
ALTER TABLE tpdata ADD COLUMN payPeriodSelectFormat cichar(65);
UPDATE tpdata
SET payPeriodSelectFormat = z.ppsel
FROM (
  SELECT payperiodseq, TRIM(pt1) + ' ' + TRIM(pt2) ppsel
    FROM (  
    SELECT distinct payperiodstart, payperiodend, payperiodseq, 
      (SELECT trim(monthname) + ' '
        + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
        FROM dds.day
        WHERE thedate = a.payperiodstart) AS pt1,
      (SELECT trim(monthname) + ' ' 
        + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
        + TRIM(CAST(theyear AS sql_char))
        FROM dds.day
        WHERE thedate = a.payperiodend) AS pt2
    FROM tpdata a) y) z
WHERE tpdata.payperiodseq = z.payperiodseq    
   

-- 12/6 
-- ****************this has NOT been done yet
       we keep USING Other to designate Vac-PTO-Holiday hours
       current tpData structure uses OtherHours AS IN Training/Other
       change the existing Other to ShopTime
ALTER TABLE tpData ALTER COLUMN TechOtherHoursDay TechShopTimeHoursDay double;     
ALTER TABLE tpData ALTER COLUMN TechOtherHoursPPTD TechShopTimeHoursPPTD double;  
ALTER TABLE tpData ALTER COLUMN TeamOtherHoursDay TeamShopTimeHoursDay double;  
ALTER TABLE tpData ALTER COLUMN TeamOtherHoursPPTD TeamShopTimeHoursPPTD double;    


ALTER TABLE tpData ALTER COLUMN TechShopTimeHoursDay  TechOtherHoursDay double;     
ALTER TABLE tpData ALTER COLUMN TechShopTimeHoursPPTD TechOtherHoursPPTD double;  
ALTER TABLE tpData ALTER COLUMN TeamShopTimeHoursDay TeamOtherHoursDay double;  
ALTER TABLE tpData ALTER COLUMN TeamShopTimeHoursPPTD TeamOtherHoursPPTD double;   

-- 12/28
leave the existing fields IN tpData for history
but these values are no longer generated, Other & Training hours are now just
flag hours on ros

ALTER TABLE tpData ALTER COLUMN TechOtherHoursDay TechOtherHoursDay integer DEFAULT '0';
ALTER TABLE tpData ALTER COLUMN TechOtherHoursPPTD TechOtherHoursPPTD integer DEFAULT '0';
ALTER TABLE tpData ALTER COLUMN TechTrainingHoursDay TechTrainingHoursDay integer DEFAULT '0';
ALTER TABLE tpData ALTER COLUMN TechTrainingHoursPPTD TechTrainingHoursPPTD integer DEFAULT '0';
ALTER TABLE tpData ALTER COLUMN TeamOtherHoursDay TeamOtherHoursDay integer DEFAULT '0';
ALTER TABLE tpData ALTER COLUMN TeamOtherHoursPPTD TeamOtherHoursPPTD integer DEFAULT '0';
ALTER TABLE tpData ALTER COLUMN TeamTrainingHoursDay TeamTrainingHoursDay integer DEFAULT '0';
ALTER TABLE tpData ALTER COLUMN TeamTrainingHoursPPTD TeamTrainingHoursPPTD integer DEFAULT '0';

UPDATE tpdata
  SET TechOtherHoursDay = 0,
      TechOtherHoursPPTD = 0,
      TechTrainingHoursDay = 0,
      TechTrainingHoursPPTD = 0,
      TeamOtherHoursDay = 0,
      TeamOtherHoursPPTD = 0,
      TeamTrainingHoursDay = 0,
      TeamTrainingHoursPPTD = 0
      
WHERE thedate = curdate()
*/
-- 12/15
-- yikes, here we go
-- DELETE FROM tpData;
-- date stuff 
/*

   remove servicetype filter IN generating techFlagHoursDay
*/

INSERT INTO tpData(DateKey, TheDate, PayPeriodStart,PayPeriodEnd,PayPeriodSeq,
  DayOfPayPeriod, DayName, 
  DepartmentKey, TechKey, TechNumber, EmployeeNumber, FirstName, LastName)  
SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
  a.biweeklypayperiodsequence, dayinbiweeklypayperiod, a.DayName,
  (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'Main Shop' AND dl5 = 'Shop'),
  b.TechKey, b.technumber, b.employeenumber, b.firstname, b.lastname
FROM dds.day a, tpTechs b 
WHERE a.thedate BETWEEN '09/08/2013' AND curdate()
  AND NOT EXISTS (
    SELECT 1
    FROM tpdata
    WHERE thedate = a.thedate
      AND techkey = b.techkey)
  AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
    SELECT 1
    FROM tpTeamTechs
    WHERE techkey = b.techkey
      AND a.thedate BETWEEN fromdate AND thrudate);     
      
UPDATE tpdata
SET payPeriodSelectFormat = z.ppsel
FROM (
  SELECT payperiodseq, TRIM(pt1) + ' ' + TRIM(pt2) ppsel
    FROM (  
    SELECT distinct payperiodstart, payperiodend, payperiodseq, 
      (SELECT trim(monthname) + ' '
        + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
        FROM dds.day
        WHERE thedate = a.payperiodstart) AS pt1,
      (SELECT trim(monthname) + ' ' 
        + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
        + TRIM(CAST(theyear AS sql_char))
        FROM dds.day
        WHERE thedate = a.payperiodend) AS pt2
    FROM tpdata a) y) z
WHERE tpdata.payperiodseq = z.payperiodseq;          

-- techteamPercentage: FROM tpTechValues
UPDATE tpData
SET TechTeamPercentage = x.TechTeamPercentage
FROM (
  SELECT a.thedate, b.techkey, b.techTeamPercentage
  FROM tpData a
  INNER JOIN tpTechValues b on a.techKey = b.techKey
    AND a.thedate BETWEEN b.fromdate AND b.thrudate) x
WHERE tpData.TechKey = x.TechKey
  AND tpData.thedate = x.theDate;



-- ClockHoursDay    
UPDATE tpdata
SET TechClockhoursDay = x.clockhours,
    TechVacationHoursDay = x.Vacation,
    TechPTOHoursDay = x.PTO,
    TechHolidayHoursDay = x.Holiday
FROM (    
  SELECT b.thedate, c.employeenumber, SUM(clockhours) AS clockhours,
    SUM(coalesce(VacationHours,0)) AS vacation,
    SUM(coalesce(PTOHours,0)) AS pto,
    SUM(coalesce(HolidayHours,0)) AS holiday
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.day b on a.datekey = b.datekey
    AND b.thedate BETWEEN '09/08/2013' AND curdate()
  LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
  GROUP BY b.thedate, c.employeenumber) x 
WHERE tpdata.thedate = x.thedate
  AND tpdata.employeenumber = x.employeenumber;
-- ClockHoursPPTD
UPDATE tpData
SET TechClockHoursPPTD = x.PPTD
FROM (
  SELECT a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday, SUM(b.techclockhoursday) AS pptd
  FROM tpData a, tpdata b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.techkey = b.techkey
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday) x
WHERE tpData.thedate = x.thedate
  AND tpData.techkey = x.techkey; 
-- TechVacationPPTD  
UPDATE tpData
SET TechVacationHoursPPTD = x.PPTD
FROM (
  SELECT a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay, SUM(b.TechVacationHoursDay) AS pptd
  FROM tpData a, tpdata b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.techkey = b.techkey
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay) x
WHERE tpData.thedate = x.thedate
  AND tpData.techkey = x.techkey;   
-- TechPTOHoursPPTD
UPDATE tpData
SET TechPTOHoursPPTD = x.PPTD
FROM (
  SELECT a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay, SUM(b.TechPTOHoursDay) AS pptd
  FROM tpData a, tpdata b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.techkey = b.techkey
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay) x
WHERE tpData.thedate = x.thedate
  AND tpData.techkey = x.techkey;  
-- TechHolidayHoursPPTD
UPDATE tpData
SET TechHolidayHoursPPTD = x.PPTD
FROM (
  SELECT a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay, SUM(b.TechHolidayHoursDay) AS pptd
  FROM tpData a, tpdata b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.techkey = b.techkey
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay) x
WHERE tpData.thedate = x.thedate
  AND tpData.techkey = x.techkey;      

-- TechFlagHoursDay -- FROM remodeled factRepairOrder
UPDATE tpData  
SET TechFlagHoursDay = x.flaghours
FROM (
  SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
  FROM tpData a
  LEFT JOIN (
    SELECT d.thedate, ro, line, c.technumber, a.flaghours
    FROM dds.factRepairOrder a  
    INNER JOIN dds.dimTech c on a.techkey = c.techkey
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'
      AND a.flagdatekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN '09/08/2013' AND curdate())   
      AND c.technumber IN (
        SELECT technumber
        FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
  GROUP BY a.technumber, a.thedate) x
WHERE tpdata.thedate = x.thedate
  AND tpdata.technumber = x.technumber;       
  
-- TechFlagHoursPPTD  
UPDATE tpData
SET TechFlagHoursPPTD = x.PPTD
FROM (
  SELECT a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday, SUM(b.techflaghoursday) AS pptd
  FROM tpData a, tpdata b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.techkey = b.techkey
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday) x
WHERE tpData.thedate = x.thedate
  AND tpData.techkey = x.techkey;   
  


-- tech flag time adjustments day
-- 12/9, lots of void ros IN adjustments, filter them out
-- 12/25 updated to just use stgArkonaSDPXTIM
UPDATE tpData
SET TechFlagHourAdjustmentsDay = x.Adj
FROM (
  SELECT a.technumber, a.lastname, a.thedate, coalesce(Adj, 0) AS Adj
  FROM tpData a
  LEFT JOIN (  
    select pttech, ptdate, round(SUM(ptlhrs), 2) AS adj
    FROM dds.stgArkonaSDPXTIM a
    INNER JOIN tpTechs b on a.pttech = b.technumber
    GROUP BY pttech, ptdate) b on a.technumber = b.pttech AND a.thedate = b.ptdate) x
/*
  SELECT a.technumber, a.lastname, a.thedate, coalesce(Adj, 0) AS Adj
  FROM tpData a
  LEFT JOIN (

    SELECT technumber, thedate, sum(neghours + adjhours + xtimhours) AS Adj
    FROM (
      SELECT technumber, thedate,
        coalesce(SUM(CASE WHEN source = 'xtim' THEN hours END), 0) AS xtimHours,
        coalesce(SUM(CASE WHEN source = 'adj' THEN hours END), 0) AS adjHours,
        coalesce(SUM(CASE WHEN source = 'neg' THEN hours END), 0) AS negHours
      FROM dds.stgFlagTimeAdjustments f
      WHERE storecode = 'RY1'
  	    AND EXISTS (
   	    SELECT 1 
    		FROM dds.factRepairOrder
    		WHERE ro = f.ro)  
      GROUP BY technumber, thedate) x
    GROUP BY technumber, thedate) b on a.technumber = b.technumber 
      AND a.thedate = b.thedate) x   
*/       
WHERE tpData.technumber = x.technumber
  AND tpData.thedate = x.thedate;
  
-- tech flag time adjustments pptd 
UPDATE tpData
SET TechFlagHourAdjustmentsPPTD = x.pptd
FROM (
  SELECT a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay, SUM(b.TechFlagHourAdjustmentsDay) AS pptd
  FROM tpData a, tpdata b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.techkey = b.techkey
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay) x
WHERE tpData.thedate = x.thedate
  AND tpData.techkey = x.techkey;     
  
-- teams
UPDATE tpData
SET TeamKey = x.teamkey,
    teamname = x.teamname
FROM (    
  SELECT *
  FROM ( 
    SELECT thedate
    FROM tpData
    GROUP BY thedate) a
  INNER JOIN (  
    SELECT a.techkey, c.teamkey, c.teamname, b.fromdate, b.thrudate
    FROM tptechs a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
    INNER JOIN tpteams c on b.teamkey = c.teamkey) b on a.thedate BETWEEN b.fromdate AND b.thrudate) x
WHERE tpData.TechKey = x.techkey
  AND tpData.theDate = x.theDate;  
  
-- team census
/*
UPDATE tpData 
SET TeamCensus = x.Census
-- SELECT *    
FROM (
  SELECT a.thedate, b.teamkey, COUNT(*) AS census
  FROM (
    SELECT thedate
    FROM tpData a
    GROUP BY thedate) a
  INNER JOIN tpTeamTechs b on a.thedate BETWEEN b.fromdate AND b.thrudate  
  GROUP BY a.thedate, b.teamkey) x
WHERE tpData.thedate = x.thedate
  AND tpData.teamkey = x.teamkey;
-- team budget
UPDATE tpData 
SET TeamBudget = x.Budget
-- SELECT *    
FROM (
  SELECT distinct a.thedate, a.teamkey, 
    round(b.poolPercentage*b.effectivelaborrate*a.teamcensus, 2) AS budget
  FROM tpData a
  INNER JOIN tpShopValues b on a.departmentkey = b.departmentkey
    AND a.thedate between b.fromdate and b.thrudate) x
WHERE tpData.thedate = x.thedate
  AND tpData.teamkey = x.teamkey;    
*/
-- 12/15 team census, budget
UPDATE tpData 
SET TeamCensus = x.census,
    teamBudget = x.Budget
FROM ( 
  SELECT *
  FROM tpTeamValues) x
WHERE tpData.payPeriodStart = x.fromDate
  AND tpData.teamkey = x.teamkey;  
     
-- teamclockhoursDay
UPDATE tpData 
SET teamclockhoursday = x.teamclockhoursday
FROM (
  SELECT thedate, teamkey, SUM(techclockhoursday) AS teamclockhoursday
  FROM tpdata
  GROUP BY thedate, teamkey) x 
WHERE tpData.thedate = x.thedate
  AND tpdata.teamkey = x.teamkey;  
-- teamclockhours pptd
UPDATE tpData
SET teamclockhourspptd = x.pptd
FROM (
  SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
  FROM (
    SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
    FROM tpdata
    GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) a,
    (
    SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
    FROM tpdata
    GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) b  
  WHERE a.payperiodseq = b.payperiodseq
    AND a.teamkey = b.teamkey
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
WHERE tpData.thedate = x.thedate
  AND tpData.teamkey = x.teamkey;  
 
-- teamflaghoursday
UPDATE tpData
SET TeamFlagHoursDay = x.hours
FROM (
  SELECT thedate, teamkey, SUM(techflaghoursday) AS hours
  FROM tpdata 
  GROUP BY thedate, teamkey) x
WHERE tpData.thedate = x.thedate
  AND tpdata.teamkey = x.teamkey;
  


-- teamflaghour pptd  
UPDATE tpData
SET teamflaghourspptd = x.pptd
FROM (
  SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
  FROM (
    SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
    FROM tpdata
    GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) a,
    (
    SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
    FROM tpdata
    GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) b  
  WHERE a.payperiodseq = b.payperiodseq
    AND a.teamkey = b.teamkey
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
WHERE tpData.thedate = x.thedate
  AND tpData.teamkey = x.teamkey;  
      

-- TeamFlagHourAdjustmentsDay
UPDATE tpData
SET TeamFlagHourAdjustmentsDay = x.hours
FROM (
  SELECT thedate, teamkey, SUM(TechFlagHourAdjustmentsDay) AS hours
  FROM tpdata 
  GROUP BY thedate, teamkey) x
WHERE tpData.thedate = x.thedate
  AND tpdata.teamkey = x.teamkey;
  
-- TeamFlagHourAdjustmentsPPTD 
UPDATE tpData
SET TeamFlagHourAdjustmentsPPTD = x.pptd
FROM (
  SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
  FROM (
    SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
    FROM tpdata
    GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) a,
    (
    SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
    FROM tpdata
    GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) b  
  WHERE a.payperiodseq = b.payperiodseq
    AND a.teamkey = b.teamkey
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x 
WHERE tpData.thedate = x.thedate
  AND tpData.teamkey = x.teamkey;    
    
-- teamprofday
-- 11/25 rework to include training/other
/*
UPDATE tpdata
SET teamprofday = x.prof
FROM (
  SELECT a.*,
    CASE 
      WHEN teamflaghoursday = 0 OR teamclockhoursday = 0 THEN 0
      ELSE round(100 * teamflaghoursday/teamclockhoursday, 2)
    END AS prof
  FROM (
    SELECT thedate, teamkey, teamflaghoursday, teamclockhoursday
    FROM tpdata
    GROUP BY thedate, teamkey, teamflaghoursday, teamclockhoursday) a) x
WHERE tpdata.thedate = x.thedate
  AND tpdata.teamkey = x.teamkey   
*/  
UPDATE tpdata
SET teamprofday = x.prof
FROM (
  SELECT a.*,
    CASE 
      WHEN TotalFlag = 0 OR teamclockhoursday = 0 THEN 0
      ELSE round(100 * TotalFlag/teamclockhoursday, 2)
    END AS prof
  FROM (
    SELECT thedate, teamkey, 
      (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay) AS TotalFlag, 
      teamclockhoursday
    FROM tpdata
    GROUP BY thedate, teamkey, 
      (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay), 
      teamclockhoursday) a) x
WHERE tpdata.thedate = x.thedate
  AND tpdata.teamkey = x.teamkey;    
  
-- team prof pptd
UPDATE tpdata
SET teamprofpptd = x.prof
FROM (
  SELECT a.*,
    CASE 
      WHEN TotalFlag = 0 OR teamclockhourspptd = 0 THEN 0
      ELSE round(100 * TotalFlag/teamclockhourspptd, 2)
    END AS prof
  FROM (
    SELECT thedate, teamkey, 
      (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS TotalFlag, 
      teamclockhourspptd
    FROM tpdata
    GROUP BY thedate, teamkey, 
      (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD), 
      teamclockhourspptd) a) x
WHERE tpdata.thedate = x.thedate
  AND tpdata.teamkey = x.teamkey;    
   
-- TechTFRRate
UPDATE tpdata
SET TechTFRRate = techteampercentage * teambudget;

-- tech Hourly Rate
-- 11/23 per Greg: previous 12 payperiod gross pay/clockhours
-- clockhours = regular + vac + pto + hol

-- 11/24
/**/
--< holy shit with the prev 12 pay stuff --------------------------------------< holy shit with the prev 12 pay stuff
/*
the issue was with trying to shoehorn payroll data INTO the notion of payrollenddate
(re?)discoverd pyptbdta which has batch AND dates AND MOST IMPORTANTLY conicides
with pay roll START dates
wa-fucking-hoo
generated IN db2.Service - teampay.sql
import that shit INTO tpStgGrossPay
*/
--/> holy shit with the prev 12 pay stuff -------------------------------------/> holy shit with the prev 12 pay stuff
/* 
12/18 replace this value with tpTechValues.previousHourlyRate
*/
UPDATE tpdata
SET techhourlyrate = (
  SELECT previoushourlyrate
  FROM tptechvalues
  WHERE techkey = tpdata.techkey);

