team of 1
teamname team 18
teamkey 47
techkey 82
dept 13
technumber 280
employeenumber 122558

SELECT * 
FROM tpteams
WHERE teamname = 'team 18';

UPDATE tpteams
SET thrudate = '04/09/2021'
WHERE teamkey = 47;

update tptechs
SET thrudate = '04/09/2021'
-- SELECT * FROM tptechs
WHERE lastname = 'GRYSKIEWICZ';

update tpteamtechs
SET thrudate = '04/09/2021'
-- SELECT * FROM tpteamtechs
WHERE teamkey = 47
  AND thrudate > curdate();

UPDATE tpteamvalues  
SET thrudate = '04/09/2021'
-- SELECT * FROM tpteamvalues
WHERE teamkey = 47  
  AND thrudate > curdate();
  
UPDATE tptechvalues
SET thrudate = '04/09/2021'
-- SELECT * FROM tptechvalues
WHERE techkey = 82 
  AND thrudate > curdate();
  
DELETE 
-- SELECT *
FROM tpdata
WHERE techkey = 82
  AND thedate > '04/10/2021'; -- accomodate END of last active payperiod