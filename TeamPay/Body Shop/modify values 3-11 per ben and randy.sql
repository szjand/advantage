SELECT *
FROM tpteams

-- cody
SELECT * 
FROM tpTeamValues
WHERE teamKey = 18

-- pete
SELECT * 
FROM tpTeamValues
WHERE teamKey = 19

SELECT d.firstname, d.lastname, a.*
FROM tptechValues a
INNER JOIN tpTeamTechs b on a.techkey = b.techKey
INNER JOIN tpTeams c on b.teamKey = c.teamKey
LEFT JOIN tpTechs d on a.techKey = d.techKey 
WHERE c.teamKey = 18

-- teamValues
-- tpTechValues
-- tpData -- need to UPDATE the existing data, THEN run xfmTodayTpData
--       techTeamPercentage
--       techTFRRate      
--       teamBudget  
BEGIN TRANSACTION;
TRY 
-- tpTeamValues
  -- cody
  UPDATE tpTeamValues
    SET poolPercentage = .35,
        budget = 59.85
  WHERE teamKey = 18;
  -- pete
  UPDATE tpTeamValues
    SET poolPercentage = .36,
        budget = 61.56
  WHERE teamKey = 19;
-- tpTechValues 
  -- cody-jeremy
  UPDATE tpTechValues
  SET techTeamPercentage = .335
  WHERE techKey = 44;
  -- cody-mavrik 
   UPDATE tpTechValues
  SET techTeamPercentage = .3
  WHERE techKey = 39;   
  -- pete-chris
  UPDATE tpTechValues
  SET techTeamPercentage = .32
  WHERE techKey = 31;
  -- pete-pete
  UPDATE tpTechValues
  SET techTeamPercentage = .38
  WHERE techKey = 43;  
  -- rob
  UPDATE tpTechValues
  SET techTeamPercentage = .29
  WHERE techKey = 45;  
-- tpData
  -- teamBudget  
  UPDATE tpData
  SET teamBudget = x.budget
  FROM tpTeamValues x
  WHERE x.teamKey IN (18,19)
    AND tpData.teamKey = x.teamKey;  
  -- techTeamPercentage
  UPDATE tpData
  SET TechTeamPercentage = x.TechTeamPercentage
  FROM (
    SELECT a.thedate, b.techkey, b.techTeamPercentage
    FROM tpData a
    INNER JOIN tpTechValues b on a.techKey = b.techKey
      AND a.thedate BETWEEN b.fromdate AND b.thrudate
      AND a.teamKey IN (18,19)) x
  WHERE tpData.TechKey = x.TechKey
    AND tpData.thedate = x.theDate
    AND tpData.teamKey IN (18,19);   
  -- TechTFRRate for new row 
  UPDATE tpdata
  SET TechTFRRate = techteampercentage * teambudget
    WHERE teamKey IN (18,19);    
--hold er down newt  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  