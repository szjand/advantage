SELECT name, employeenumber, technumber, laborcost, techkeyfromdate AS [from], techkeythrudate AS thru
FROM dimTech a
WHERE flagdept = 'body shop'
  AND EXISTS (
    SELECT 1
    FROM scotest.tptechs
    WHERE technumber = a.technumber)
  AND EXISTS (
    SELECT 1
    FROM dimTech
    WHERE technumber = a.technumber
      AND rowchangereason = 'laborcost')
  AND EXISTS (
    SELECT 1
    FROM edwEmployeeDim
    WHERE employeenumber = a.employeenumber
      AND currentrow = true
      AND termdate = '12/31/9999')     
  AND a.techkeyfromdate >= '11/12/2014'      
ORDER BY technumber, techkey, techkeyfromdate        

SELECT name, employeenumber, technumber, laborcost,
  CASE techkeyfromdate WHEN '11/12/2014' THEN laborcost END AS laborcost,
  CASE techkeyfromdate WHEN '11/12/2014' THEN techkeyfromdate END AS [from],
  CASE techkeyfromdate WHEN '11/12/2014' THEN techkeythrudate END AS thru,
  CASE techkeyfromdate WHEN '11/29/2014' THEN laborcost END AS laborcost,
  CASE techkeyfromdate WHEN '11/29/2014' THEN techkeyfromdate END AS [from],
  CASE techkeyfromdate WHEN '11/29/2014' THEN techkeythrudate END AS thru
FROM dimTech a
WHERE flagdept = 'body shop'
  AND EXISTS (
    SELECT 1
    FROM scotest.tptechs
    WHERE technumber = a.technumber)
  AND EXISTS (
    SELECT 1
    FROM dimTech
    WHERE technumber = a.technumber
      AND rowchangereason = 'laborcost')
  AND EXISTS (
    SELECT 1
    FROM edwEmployeeDim
    WHERE employeenumber = a.employeenumber
      AND currentrow = true
      AND termdate = '12/31/9999')     
  AND a.techkeyfromdate >= '11/12/2014'      
ORDER BY technumber, techkey, techkeyfromdate        

SELECT a.*, b.[labor cost from 11/29]
FROM (
  SELECT name, employeenumber, technumber, laborcost AS [labor cost from 11/12 -> 11/28]
  FROM dimTech a
  WHERE flagdept = 'body shop'
    AND EXISTS (
      SELECT 1
      FROM scotest.tptechs
      WHERE technumber = a.technumber)
    AND EXISTS (
      SELECT 1
      FROM dimTech
      WHERE technumber = a.technumber
        AND rowchangereason = 'laborcost')
    AND EXISTS (
      SELECT 1
      FROM edwEmployeeDim
      WHERE employeenumber = a.employeenumber
        AND currentrow = true
        AND termdate = '12/31/9999')     
    AND a.techkeyfromdate = '11/12/2014') a      
LEFT JOIN (
  SELECT name, employeenumber, technumber, laborcost AS [labor cost from 11/29]
  FROM dimTech a
  WHERE flagdept = 'body shop'
    AND EXISTS (
      SELECT 1
      FROM scotest.tptechs
      WHERE technumber = a.technumber)
    AND EXISTS (
      SELECT 1
      FROM dimTech
      WHERE technumber = a.technumber
        AND rowchangereason = 'laborcost')
    AND EXISTS (
      SELECT 1
      FROM edwEmployeeDim
      WHERE employeenumber = a.employeenumber
        AND currentrow = true
        AND termdate = '12/31/9999')     
    AND a.techkeyfromdate = '11/29/2014') b on a.name = b.name    
       
