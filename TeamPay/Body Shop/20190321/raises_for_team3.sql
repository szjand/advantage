Josh Walton and Chad Gardner have both had approved raises starting as of 3/15/2020.  
Chad Gardner�s raise will be from $22.00 to $22.50 and Josh Walton�s 
will be from $21.20 to $22.20.  Kim has received both Compli forms.

Thank you
03/18/2020
John Gardner

-- did this same thing with chad a year ago
-- IN ..\Body Shop\20190317\one guy on team gets raise\chad_raise.sql1

SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND teamName = 'team 3'
ORDER BY teamname, firstname  

NOT sure how im going to hanldle this one
randy has long ago abandoned the spreadsheet maintaining team bucget
AND this team, team 3, IS already out of spec
budget = 42, rates = 22.5 & 22.2 which = 44.7

just boost chad?
i think so, dont change team values, just tech values, AND since it IS retroactive 
to 3/15/19, DELETE AND UPDATE tpdata

-- old rates
SELECT * FROM tpdata WHERE teamkey = 32 AND thedate > '03/14/2020' ORDER BY techkey, thedate
DateKey	TheDate	PayPeriodStart	PayPeriodEnd	PayPeriodSeq	DayOfPayPeriod	DayName	DepartmentKey	TechKey	TechNumber	EmployeeNumber	FirstName	LastName	TechTeamPercentage	TechHourlyRate	TechTFRRate	TechClockHoursDay	TechClockHoursPPTD	TechFlagHoursDay	TechFlagHoursPPTD	TechFlagHourAdjustmentsDay	TechFlagHourAdjustmentsPPTD	TeamKey	TeamName	TeamCensus	TeamBudget	TeamClockHoursDay	TeamClockHoursPPTD	TeamFlagHoursDay	TeamFlagHoursPPTD	TeamProfDay	TeamProfPPTD	TechVacationHoursDay	TechVacationHoursPPTD	TechPTOHoursDay	TechPTOHoursPPTD	TechHolidayHoursDay	TechHolidayHoursPPTD	TechOtherHoursDay	TechOtherHoursPPTD	TechTrainingHoursDay	TechTrainingHoursPPTD	TeamOtherHoursDay	TeamOtherHoursPPTD	TeamTrainingHoursDay	TeamTrainingHoursPPTD	TeamFlagHourAdjustmentsDay	TeamFlagHourAdjustmentsPPTD	payPeriodSelectFormat
5919	03/15/2020	03/15/2020	03/28/2020	294	1	  Sunday	  13	30	251	150105	Chad	Gardner	0.5238	35.68	21.9996	0	    0	0	0	0	0	32	Team 3	2	42	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5920	03/16/2020	03/15/2020	03/28/2020	294	2	  Monday	  13	30	251	150105	Chad	Gardner	0.5238	35.68	21.9996	8.5	  8.5	5.5	5.5	0	0	32	Team 3	2	42	17.19	17.19	5.5	5.5	32	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5921	03/17/2020	03/15/2020	03/28/2020	294	3	  Tuesday	  13	30	251	150105	Chad	Gardner	0.5238	35.68	21.9996	7.89	16.39	3.1	8.6	0	0	32	Team 3	2	42	14.62	31.81	20.2	25.7	138.17	80.79	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5922	03/18/2020	03/15/2020	03/28/2020	294	4	  Wednesday	13	30	251	150105	Chad	Gardner	0.5238	35.68	21.9996	8.73	25.12	6.4	15	0	0	32	Team 3	2	42	17.22	49.03	6.4	32.1	37.17	65.47	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5923	03/19/2020	03/15/2020	03/28/2020	294	5	  Thursday	13	30	251	150105	Chad	Gardner	0.5238	35.68	21.9996	8.81	33.93	6.2	21.2	0	0	32	Team 3	2	42	17.08	66.11	14.6	46.7	85.48	70.64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5924	03/20/2020	03/15/2020	03/28/2020	294	6	  Friday	  13	30	251	150105	Chad	Gardner	0.5238	35.68	21.9996	9.03	42.96	15.1	36.3	0	0	32	Team 3	2	42	17.3	83.41	43.9	90.6	253.76	108.62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5925	03/21/2020	03/15/2020	03/28/2020	294	7	  Saturday	13	30	251	150105	Chad	Gardner	0.5238	35.68	21.9996	0	    42.96	0	36.3	0	0	32	Team 3	2	42	0	83.41	0	90.6	0	108.62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5919	03/15/2020	03/15/2020	03/28/2020	294	1	  Sunday	  13	34	246	1147061	Joshua	Walton	0.49	31.72	20.58	0	0	    0	    0	0	0	32	Team 3	2	42	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5920	03/16/2020	03/15/2020	03/28/2020	294	2	  Monday	  13	34	246	1147061	Joshua	Walton	0.49	31.72	20.58	8.69	  8.69	0	0	0	0	32	Team 3	2	42	17.19	17.19	5.5	5.5	32	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5921	03/17/2020	03/15/2020	03/28/2020	294	3	  Tuesday	  13	34	246	1147061	Joshua	Walton	0.49	31.72	20.58	6.73	  15.42	17.1	17.1	0	0	32	Team 3	2	42	14.62	31.81	20.2	25.7	138.17	80.79	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5922	03/18/2020	03/15/2020	03/28/2020	294	4	  Wednesday	13	34	246	1147061	Joshua	Walton	0.49	31.72	20.58	8.49	  23.91	0	17.1	0	0	32	Team 3	2	42	17.22	49.03	6.4	32.1	37.17	65.47	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5923	03/19/2020	03/15/2020	03/28/2020	294	5	  Thursday	13	34	246	1147061	Joshua	Walton	0.49	31.72	20.58	8.27	  32.18	8.4	25.5	0	0	32	Team 3	2	42	17.08	66.11	14.6	46.7	85.48	70.64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5924	03/20/2020	03/15/2020	03/28/2020	294	6	  Friday	  13	34	246	1147061	Joshua	Walton	0.49	31.72	20.58	8.27	  40.45	28.8	54.3	0	0	32	Team 3	2	42	17.3	83.41	43.9	90.6	253.76	108.62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5925	03/21/2020	03/15/2020	03/28/2020	294	7	  Saturday	13	34	246	1147061	Joshua	Walton	0.49	31.72	20.58	0	40.45	0	    54.3	0	0	32	Team 3	2	42	0	83.41	0	90.6	0	108.62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"

-- new rates
SELECT * FROM tpdata WHERE teamkey = 32 AND thedate > '03/14/2020' ORDER BY techkey, thedate
DateKey	TheDate	PayPeriodStart	PayPeriodEnd	PayPeriodSeq	DayOfPayPeriod	DayName	DepartmentKey	TechKey	TechNumber	EmployeeNumber	FirstName	LastName	TechTeamPercentage	TechHourlyRate	TechTFRRate	TechClockHoursDay	TechClockHoursPPTD	TechFlagHoursDay	TechFlagHoursPPTD	TechFlagHourAdjustmentsDay	TechFlagHourAdjustmentsPPTD	TeamKey	TeamName	TeamCensus	TeamBudget	TeamClockHoursDay	TeamClockHoursPPTD	TeamFlagHoursDay	TeamFlagHoursPPTD	TeamProfDay	TeamProfPPTD	TechVacationHoursDay	TechVacationHoursPPTD	TechPTOHoursDay	TechPTOHoursPPTD	TechHolidayHoursDay	TechHolidayHoursPPTD	TechOtherHoursDay	TechOtherHoursPPTD	TechTrainingHoursDay	TechTrainingHoursPPTD	TeamOtherHoursDay	TeamOtherHoursPPTD	TeamTrainingHoursDay	TeamTrainingHoursPPTD	TeamFlagHourAdjustmentsDay	TeamFlagHourAdjustmentsPPTD	payPeriodSelectFormat
5919	03/15/2020	03/15/2020	03/28/2020	294	1	Sunday	  13	30	251	150105	Chad	Gardner	0.5357	35.68	22.4994	0	0	0	0	0	0	32	Team 3	2	42	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5920	03/16/2020	03/15/2020	03/28/2020	294	2	Monday	  13	30	251	150105	Chad	Gardner	0.5357	35.68	22.4994	8.5	8.5	5.5	5.5	0	0	32	Team 3	2	42	17.19	17.19	5.5	5.5	32	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5921	03/17/2020	03/15/2020	03/28/2020	294	3	Tuesday	  13	30	251	150105	Chad	Gardner	0.5357	35.68	22.4994	7.89	16.39	3.1	8.6	0	0	32	Team 3	2	42	14.62	31.81	20.2	25.7	138.17	80.79	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5922	03/18/2020	03/15/2020	03/28/2020	294	4	Wednesday	13	30	251	150105	Chad	Gardner	0.5357	35.68	22.4994	8.73	25.12	6.4	15	0	0	32	Team 3	2	42	17.22	49.03	6.4	32.1	37.17	65.47	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5923	03/19/2020	03/15/2020	03/28/2020	294	5	Thursday	13	30	251	150105	Chad	Gardner	0.5357	35.68	22.4994	8.81	33.93	6.2	21.2	0	0	32	Team 3	2	42	17.08	66.11	14.6	46.7	85.48	70.64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5924	03/20/2020	03/15/2020	03/28/2020	294	6	Friday	  13	30	251	150105	Chad	Gardner	0.5357	35.68	22.4994	9.03	42.96	15.1	36.3	0	0	32	Team 3	2	42	17.3	83.41	43.9	90.6	253.76	108.62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5925	03/21/2020	03/15/2020	03/28/2020	294	7	Saturday	13	30	251	150105	Chad	Gardner	0.5357	35.68	22.4994	0	42.96	0	36.3	0	0	32	Team 3	2	42	0	83.41	0	90.6	0	108.62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5919	03/15/2020	03/15/2020	03/28/2020	294	1	Sunday	  13	34	246	1147061	Joshua	Walton	0.52857	31.72	22.19994	0	0	0	0	0	0	32	Team 3	2	42	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5920	03/16/2020	03/15/2020	03/28/2020	294	2	Monday	  13	34	246	1147061	Joshua	Walton	0.52857	31.72	22.19994	8.69	8.69	0	0	0	0	32	Team 3	2	42	17.19	17.19	5.5	5.5	32	32	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5921	03/17/2020	03/15/2020	03/28/2020	294	3	Tuesday	  13	34	246	1147061	Joshua	Walton	0.52857	31.72	22.19994	6.73	15.42	17.1	17.1	0	0	32	Team 3	2	42	14.62	31.81	20.2	25.7	138.17	80.79	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5922	03/18/2020	03/15/2020	03/28/2020	294	4	Wednesday	13	34	246	1147061	Joshua	Walton	0.52857	31.72	22.19994	8.49	23.91	0	17.1	0	0	32	Team 3	2	42	17.22	49.03	6.4	32.1	37.17	65.47	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5923	03/19/2020	03/15/2020	03/28/2020	294	5	Thursday	13	34	246	1147061	Joshua	Walton	0.52857	31.72	22.19994	8.27	32.18	8.4	25.5	0	0	32	Team 3	2	42	17.08	66.11	14.6	46.7	85.48	70.64	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5924	03/20/2020	03/15/2020	03/28/2020	294	6	Friday	  13	34	246	1147061	Joshua	Walton	0.52857	31.72	22.19994	8.27	40.45	28.8	54.3	0	0	32	Team 3	2	42	17.3	83.41	43.9	90.6	253.76	108.62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"
5925	03/21/2020	03/15/2020	03/28/2020	294	7	Saturday	13	34	246	1147061	Joshua	Walton	0.52857	31.72	22.19994	0	40.45	0	54.3	0	0	32	Team 3	2	42	0	83.41	0	90.6	0	108.62	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 15 - March 28, 2020"


DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @dept_key integer;
@eff_date = '03/15/2020';
@thru_date = '03/14/2020';
@dept_key = 13;

BEGIN TRANSACTION;
TRY
/*
to implement raises only
tpTechValues are ALL that need to be changed
his new techTeamPercentage
rate = tech% * budget
tech% = rate/budget
chad: 22.5/42 = .5357
josh: 22.2/42 = .52857
techkeys
chad: 30
josh: 34
*/
-- techValues
  -- chad gardner
  @tech_key = 30;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .53571; -------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	

    -- joshua walton
  @tech_key = 34;
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .52857; -----------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   
  
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date
	AND techkey IN (30, 34);
		 
  EXECUTE PROCEDURE xfmTpData();  
  EXECUTE PROCEDURE todayxfmTpData();   
    
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    			

