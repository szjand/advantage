/*
Starting 2/17/2020 I will be moving Brandom Lamont from a paint technician to 
an independent metal technician.  If you could please remove him from the paint 
team and put him on his own.  We will be paying Brandon at $20 flat rate.  
I will not be adding anyone to the paint team at this time.  I will also be 
moving Ben Johnson from detail technician to begin training as a PDR technician 
in the body shop if you could get him to show up on the page.  Cory Rose is 
going to have shoulder surgery and will be gone for 8 months, I am unsure if 
you need to know that information at the end.

Thank you
John Gardner
*/

/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND a.teamkey = 29
ORDER BY teamname, firstname  
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @team string;
DECLARE @budget double;
DECLARE @tech_team_percentage double;
DECLARE @previous_hourly_rate double;

@eff_date = '02/16/2020';
@thru_date = '02/15/2020';
@dept_key = 13;
@elr = 60;
@team_key = 29;  -- paint team
BEGIN TRANSACTION;
TRY
-- first take care of the paint team
-- 1. expire lamont FROM paint team
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'lamont'); -- 67
  UPDATE tpteamtechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND techkey = @tech_key
    AND thrudate = '12/31/9999';

-- 2. tpteamvalues
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate = '12/31/9999';
    
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromDate)
  values (@team_key, 4, 76.8, .32, @eff_date);  
  
-- 3. tech values for the remaining techs
  -- walden 31 19.33
  @tech_key = 31;   
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   -- new techrate = current techrate/budget: 19.33/76.8 = .2517
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @eff_date, .2517, @pto_rate);  
  -- adam 72 15.25
  @tech_key = 72;   
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   -- new techrate = current techrate/budget: 15.25/76.8 = .19857
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @eff_date, .19857, @pto_rate);  
  -- jacobson 43 23.04 
  @tech_key = 43;   
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   -- new techrate = current techrate/budget: 23.04/76.8 = .19857
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @eff_date, .3, @pto_rate);  
  -- mavity 45 16.83
  @tech_key = 45;   
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   -- new techrate = current techrate/budget: 16.83/76.8 = .21914
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @eff_date, .21914, @pto_rate);     
     
     
 -- new team  for brandon lamont
  @team = 'Team 20';
  @tech_key = 67;
  @census = 1;
  @budget = 20.00;
  @pool_perc = 1;
  @tech_team_percentage = 1;
  @previous_hourly_rate = 27.60;
  
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, @team, @eff_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @team);
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @eff_date
  FROM system.iota;
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, @census, @budget, @pool_perc, @eff_date);
  
  UPDATE tptechvalues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
    
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_team_percentage, @previous_hourly_rate);      
    
  DELETE FROM tpdata WHERE thedate = curdate() AND teamkey = 29;
   
  EXECUTE PROCEDURE xfmTpData();
  
--  EXECUTE PROCEDURE todayxfmTpData();     
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY; 
