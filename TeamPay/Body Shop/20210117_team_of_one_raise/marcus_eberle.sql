/*
Marcus Eberles flat rate pay rate is going from $18.00 to $19.00 effective 1/18/2021. 
1/22/21
John Gardner
*/
                    
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND a.teamkey = 45
ORDER BY teamname, firstname  

select * 
FROM tpdata
WHERE thedate = curdate()
and lastname = 'eberle'

select * FROM tpteamvalues WHERE teamkey = 45 order BY fromdate;
select * FROM tptechvalues WHERE techkey = 78 order BY fromdate;

ok, looks LIKE the only thing that needs to change IS tp.teamvalues.budget, FROM 18 to 19:

UPDATE tpteamvalues
SET thrudate = '01/16/2021'
WHERE teamkey = 45
  AND thrudate > curdate();
  
INSERT INTO tpteamvalues values(45,1,19,1,'01/17/2021','12/31/9999');

DELETE FROM tpData  
WHERE departmentkey = 13
  AND teamkey = 45
  AND thedate > '01/16/2021';
  
EXECUTE PROCEDURE xfmTpData();


      