/*
Can you switch Tim Reuter from hourly to flat rate and his rate is $23.50 and can we get a vision login for him to view his hours please.


-- SELECT * FROM dds.edwEmployeeDim  WHERE lastname = 'reuter'  -- 1114921
-- select * FROM tpemployees WHERE employeenumber = '1114921'
-- select * FROM tpteams WHERE departmentkey = 13 ORDER BY teamname

-- he already EXISTS IN tptechs AS techkey 74 with a thru date of 8/5/17
select * FROM tptechs WHERE lastname = 'reuter'
SELECT * FROM tpteamtechs WHERE techkey = 74

-- already IN tpemployees AS treuter@rydellcars.com, password: Polaris600
SELECT * FROM tpemployees WHERE username LIKE 'treuter%'

AND he IS already IN employeeappauthorization with teampay access 
SELECT * FROM employeeappauthorization WHERE username = 'treuter@rydellcars.com'
*/

DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
DECLARE @budget double;
DECLARE @previoushourlyrate double;
DECLARE @new_team string;

@from_date = '11/21/2021';
@dept_key = 13;
@budget = 23.50;
@previoushourlyrate = 23.50;
@new_team = 'Team 24';

BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, @new_team, @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @new_team);
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
--select *  
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'reuter'
    AND a.currentrow = true;
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'reuter' AND thrudate > curdate());
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, @budget, 1, @from_date);
  
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, 1, @previoushourlyrate);
/*
  -- NOT needed, no prior data IN tpData for nichols
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND techkey = @tech_key
    AND thedate >= @from_date;
*/    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
   
/* he IS already SET up IN tpEmployees AND employeeappauthorization	    
  INSERT INTO tpEmployees
  SELECT 'nnichols@rydellcars.com', firstname, lastname, employeenumber,
    '!aseTr8D9', 'Fuck You', storecode, TRIM(firstname) + ' ' + lastname
  --SELECT *  
  FROM dds.edwEmployeeDim 
  WHERE lastname = 'nichols'
    AND currentrow = true;			
			
  INSERT INTO employeeappauthorization
  SELECT 'nnichols@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'crose@rydellcars.com'
    AND appname = 'teampay';     
*/	          

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

