Current pay period please. 
/*
Current pay period please.
Sent: Friday, July 14, 2017 12:57 PM
Please increase Mavrik Peterson�s commission rate from $16.80 per hour to $17.80 per hour. Ben ok�d!
*/



SELECT teamName,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, a.techkey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND a.teamkey = 29
ORDER BY teamname, firstname  

team budget = 96
current allocation IS only 92.97% of budget
therefore, the only thing that needs to change IS petersons techteampercentage to .1854

teamkey: 29
techkey: 39

thrudate 07/08/17
effective date 07/09/17

SELECT * FROM tpdata WHERE techkey = 39 AND thedate = curdate();

SELECT * FROM tptechvalues WHERE techkey = 39

UPDATE tptechvalues
SET thrudate = '07/08/2017'
WHERE techkey = 39
  AND thrudate > curdate();
  
INSERT INTO tptechvalues(techkey,fromdate,thrudate,techteampercentage,previoushourlyrate)
values(39, '07/09/2017', '12/31/9999', .1854, 29.62);  

UPDATE tpdata
SET techteampercentage = .1854,
    techtfrrate = 17.80
-- SELECT * FROM tpdata   
WHERE techkey = 39
  AND thedate BETWEEN '07/09/2017' and curdate();



