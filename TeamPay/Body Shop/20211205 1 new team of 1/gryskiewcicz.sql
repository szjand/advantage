/*
GRYSKIEWICZ, JONATHAN

rehired on 12/6
fucking gardner never told me, so kim was looking for him on the flat rate pay
cobbled together his pay for the period IN Jon Gryskiewic pay for 12-05 - 12-18-2021.sql

now, i need to ADD him to team pay (still have NOT heard FROM gardner)

SELECT * FROM tptechs WHERE lastname LIKE 'gr%'
select * FROM dds.dimtech WHERE name LIKE 'gry%'
                                                       
-- SELECT * FROM dds.edwEmployeeDim  WHERE lastname = 'GRYSKIEWICZ'  -- 122558
-- select * FROM tpemployees WHERE employeenumber = '122558'  -- jgryskiewicz@rydellcars.com
-- select * FROM tpteams WHERE departmentkey = 13 ORDER BY teamname  -- team 25

-- he already EXISTS IN tptechs AS techkey 82 with a thru date of 4/9/21
select * FROM tptechs WHERE lastname = 'gryskiewicz'
SELECT * FROM tpteamtechs WHERE techkey = 82

-- already IN tpemployees AS jgryskiewicz@rydellcars.com, password: Thisisstupid1
SELECT * FROM tpemployees WHERE username LIKE 'jgrysk%'

AND he IS already IN employeeappauthorization with teampay access 
SELECT * FROM employeeappauthorization WHERE username = 'jgryskiewicz@rydellcars.com'
*/

DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
DECLARE @budget double;
DECLARE @previoushourlyrate double;
DECLARE @new_team string;

@from_date = '12/05/2021';
@dept_key = 13;
@budget = 20;
@previoushourlyrate = 20;
@new_team = 'Team 25';

BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, @new_team, @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @new_team);
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
--select *  
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'gryskiewicz' ------------------------
    AND a.currentrow = true;
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gryskiewicz' AND thrudate > curdate());
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, @budget, 1, @from_date);
  
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, 1, @previoushourlyrate);
/*
  -- NOT needed, no prior data IN tpData for nichols
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND techkey = @tech_key
    AND thedate >= @from_date;
*/    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
   
/* he IS already SET up IN tpEmployees AND employeeappauthorization	    
  INSERT INTO tpEmployees
  SELECT 'nnichols@rydellcars.com', firstname, lastname, employeenumber,
    '!aseTr8D9', 'Fuck You', storecode, TRIM(firstname) + ' ' + lastname
  --SELECT *  
  FROM dds.edwEmployeeDim 
  WHERE lastname = 'nichols'
    AND currentrow = true;			
			
  INSERT INTO employeeappauthorization
  SELECT 'nnichols@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'crose@rydellcars.com'
    AND appname = 'teampay';     
*/	          

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

