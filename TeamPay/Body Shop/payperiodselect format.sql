SELECT *
FROM system.storedprocedures
--WHERE name = 'gettptechdetail'
WHERE upper(CAST(proc_input AS sql_char)) LIKE '%SESSIONID%'

WHERE contains(upper(proc_input), 'SESSIONID')
		

-- to be added to xfmTpData		
    UPDATE tpdata
    SET payPeriodSelectFormat = z.ppsel
    FROM (		
      SELECT payperiodseq, left(TRIM(pt1) + ' ' + TRIM(pt2) + ' ' + pt3, 100) AS ppsel
      FROM ( 		
        SELECT distinct payperiodstart, payperiodend, payperiodseq, 
          (SELECT trim(monthname) + ' '
            + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
            FROM dds.day
            WHERE thedate = a.payperiodstart) AS pt1,
          (SELECT trim(monthname) + ' ' 
            + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
            + TRIM(CAST(theyear AS sql_char))
            FROM dds.day
            WHERE thedate = a.payperiodend) AS pt2,
            CASE 
              WHEN a.payperiodend < curdate() THEN ' ---- pay period complete'
              WHEN a.payperiodend = curdate() THEN ' ---- 1 day left in pay period'
			  WHEN a.payperiodstart = curdate() THEN ' ---- 14 day left in pay period'
              ELSE ' ---- ' +  trim(CAST(a.payperiodend - curdate() + 1 AS sql_char)) + ' days left in pay period'
        	  END collate ads_default_ci AS pt3
        FROM tpdata a) y) z
          WHERE tpdata.payperiodseq = z.payperiodseq;  	
	
	
alter PROCEDURE GetTpPayPeriods
   ( 
      payPeriodDisplay CICHAR ( 100 ) OUTPUT,
      payPeriodIndicator Integer OUTPUT
   ) 
BEGIN 
/*
2-8-14: return 8 payperiods
2/22/14: change width of payPeriodDisplay
EXECUTE PROCEDURE GetTpPayPeriods();
*/
-- DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @fromPayPeriodSeq integer;
DECLARE @thruPayPeriodSeq integer;
@thrupayperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = curdate());
@fromPayPeriodSeq = @thrupayperiodseq - 7;
INSERT INTO __output
SELECT top 10 payperiodselectformat, payperiodseq - @thruPayPeriodSeq
FROM (
  SELECT DISTINCT payperiodselectformat, payperiodseq
  FROM tpdata 
  WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq) x
ORDER BY payperiodseq - @thruPayPeriodSeq DESC ;
END;	