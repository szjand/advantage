/*
SELECT *
FROM tpdata
WHERE lastname = 'shereck'
ORDER BY thedate DESC

SELECT MAX(thedate)
FROM tpdata
WHERE lastname = 'shereck'
  and techflaghoursday > 0
  
he was NOT officially changed until 11/16/16, but he was absent AND NOT working BETWEEN july AND november

teams were disbanded AND reorganized AS individuals on 10/2/16

DepartmentKey = 13
TeamKey 33  Team 4
last valid pay period: 198  7/11 -> 7/23


select * FROM dds.edwEmployeeDim WHERE firstname = 'loren' ORDER BY employeekey

so, DELETE team 4 effective 10/2/16

select * FROM tpteamtechs a inner join tptechs b on a.techkey = b.techkey ORDER BY a.techkey
SELECT * FROM tpteams WHERE teamkey = 33
*/

DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @thru_date date;
@team_key = 33;
@tech_key = 37;
@thru_date = '10/01/2016';

BEGIN TRANSACTION;
TRY
  -- teams
  UPDATE tpteams
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > @thru_date;
  -- techs
  UPDATE tptechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
     AND thrudate > @thru_date;
  --team techs
  UPDATE tpteamtechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key
     AND thrudate > @thru_date;
  -- team values 
  DELETE 
  FROM tpteamvalues
  WHERE teamkey = 33
    AND thrudate > @thru_date;
  -- tech values
  DELETE 
  FROM tpTechValues
  WHERE techkey = 37
    AND thrudate > @thru_date;
-- tpdata  
  DELETE 
  FROM tpdata
  WHERE techkey = @tech_key
    AND thedate > @thru_date;  
           
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
