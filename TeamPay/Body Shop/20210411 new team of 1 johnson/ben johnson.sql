/*
Ben Johnson has been moved to flat rate at a wage of $15.10 per hour as of 4/11/2021.

Thank you
John Gardner

*/

-- SELECT * FROM dds.edwEmployeeDim  WHERE lastname = 'johnson'  -- 184620
-- select * FROM ptoemployees WHERE employeenumber = '184620' -- bjohnson1@rydellcars.com
-- select * FROM tpemployees WHERE employeenumber = '184620' -- he IS IN, never logged IN, pw = uWa71Los
-- select * FROM tpteams WHERE departmentkey = 13 -- next team IS Team 21
--SELECT * FROM dds.dimtech WHERE employeenumber = '184620'

-- his last hourly rate was 18, that IS what i will make IS pto


DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
DECLARE @budget double;
DECLARE @previoushourlyrate double;
DECLARE @new_team string;
DECLARE @employeenumber string;
DECLARE @technumber string;
DECLARE @username string;

@from_date = '04/11/2021';
@dept_key = 13;
@budget = 15.10;
@previoushourlyrate = 18;
@new_team = 'Team 21';
@employeenumber = '184620';
@technumber = '232';
@username = 'bjohnson1@rydellcars.com';

BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, @new_team, @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @new_team);
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
--select *  
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
		AND b.technumber = @technumber
  WHERE a.employeenumber = @employeenumber
    AND a.currentrow = true;
  
  @tech_key = (SELECT techkey FROM tptechs WHERE technumber = @technumber AND thrudate> curdate());
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, @budget, 1, @from_date);
  
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, 1, @previoushourlyrate);

  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND techkey = @tech_key
    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
  INSERT INTO employeeappauthorization
  SELECT @username,appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'ssevigny@rydellcars.com'
    AND appname = 'teampay';     
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  