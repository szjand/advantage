
-- We need to split Aaron Lindon and Terry Driscoll. 
-- Aaron will become his own team and his commission rate will stay the same as it is now.
-- Terry Driscoll will be his own team and his commission rate will be the same as it is now.
-- Anthony Wetch will be moved from hourly to commission, his commission rate will be $20.51 per hour.
-- Kyle Buchanan will also move from hourly to commission, his commission rate will be $20.51 per hour. 
-- Cody Olson will move from hourly to commission, his commission rate will be $                       
-- (Waiting for Ben to determine what the wage will be)
-- The last issue is Thomas Sauls,  as of right now, he is moving from the Detail 
-- department to the Body Shop as an hourly employee in the paint department. 
-- Based on what John Gardner has told me, Thomas�s hourly rate based on what 
-- he was earning in detail would be $20.00 an hour. I will submit to Kim in Compli, 
-- a payroll rate change for that amount to start on next Monday�s payroll. 
-- If that number is incorrect based on your knowledge of his commission earnings, 
-- please let know what you believe his commission rate should be

/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
ORDER BY firstname

ok, to summarize
team 2 goes away
new teams:
team 11: aaron lindom
team 12: terrance driscoll
team 13: anthony wetch
team 14: kely buchanan
team 15: cody olson
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr integer;
DECLARE @percentage double;
DECLARE @pto_rate double;
@eff_date = '08/06/2017';
@thru_date = '08/05/2017';
@dept_key = 13;
@elr = 60;
BEGIN TRANSACTION;
TRY

-- move aaron lindom FROM team 2 to new team 11
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'lindom' AND thrudate > curdate());
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 2' AND thrudate > curdate());
  -- remove him FROM team 2
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND techkey = @tech_key
    AND thrudate > curdate();
  -- CREATE new team 11 
  -- SELECT * FROM tpteams SELECT * FROM tpteamtechs SELECT * FROM tpTeamValues
  -- SELECT * FROM tptechvalues
  INSERT INTO tpteams (departmentkey,teamname,fromdate)
  values(13, 'Team 11', @eff_date); 
  -- tpTeamTechs
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'team 11');
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);
  -- tpTeamValues, 
  -- budget: his current rate, poolpercentage = 100%, eg 1.0
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key, 1, 16.13, 1, @eff_date);
  -- tpTechValues
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,TechTeamPercentage,PreviousHourlyRate)
  values(@tech_key, @eff_date, 1, @pto_rate);
    
-- move terrance driscoll FROM team 2 to team 12
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'driscoll' AND thrudate > curdate());
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 2' AND thrudate > curdate());
  -- remove him FROM team 2
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND techkey = @tech_key
    AND thrudate > curdate();
  -- CREATE new team 12 
  -- SELECT * FROM tpteams SELECT * FROM tpteamtechs SELECT * FROM tpTeamValues
  -- SELECT * FROM tptechvalues
  INSERT INTO tpteams (departmentkey,teamname,fromdate)
  values(13, 'Team 12', @eff_date); 
  -- tpTeamTechs
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'team 12');
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);
  -- tpTeamValues, 
  -- budget: his current rate, poolpercentage = 100%, eg 1.0
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key, 1, 25.34, 1, @eff_date);
  -- tpTechValues
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,TechTeamPercentage,PreviousHourlyRate)
  values(@tech_key, @eff_date, 1, @pto_rate);
-- CLOSE team 2: tpteams, tpteamvalues
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 2' AND thrudate > curdate());
  UPDATE tpTeams
  SET thrudate = @thru_date
  WHERE teamkey = @team_key;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key;
  
-- CREATE new team 13 AND new tech
  -- team 13: anthony wetch emp#: 1147900, tech#: 278, hourly: 27, flat rate: 20.51
  -- tpTechs
  INSERT INTO tpTechs(departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values (@dept_key,'278','Anthony','Wetch','1147900',@eff_date);
  -- tpTeams
  INSERT INTO tpteams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Team 13', @eff_date);   
  -- tpTeamTechs
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'team 13');
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'wetch');
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);
  -- tpTeamValues
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key,1,20.51,1,@eff_date);
  --tpTechValues
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key,@eff_date,1,27);

-- CREATE new team 14 AND new tech
  -- team 14: kyle buchanan emp#: 145789, tech#: 279, hourly: 21, flat rate: 20.51
  -- tpTechs
  INSERT INTO tpTechs(departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values (@dept_key,'279','Kyle','Buchanan','145789',@eff_date);
  -- tpTeams
  INSERT INTO tpteams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Team 14', @eff_date); 
  -- tpTeamTechs
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'team 14');
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'buchanan');
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);
  -- tpTeamValues
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key,1,20.51,1,@eff_date);
  --tpTechValues
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key,@eff_date,1,21);
-- CREATE new team 15 AND new tech  

  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
               
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

/*
08/13/17
1. forgot employeeAppAuthorization for new employees
2. cody olson
3. tim reuter off teams

*/
-- 1. forgot employeeAppAuthorization for new employees
-- kyle buchanan
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,
  approle,functionality,appDepartmentKey)
SELECT 'kbuchanan@rydellcars.com', appname,appseq,appcode,
  approle,functionality,appDepartmentKey
FROM employeeAppAuthorization
WHERE username LIKE 'padam%'
  AND appcode <> 'pto'; 
  
-- anthony wetch
INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,
  approle,functionality,appDepartmentKey)
SELECT 'awetch@rydellcars.com', appname,appseq,appcode,
  approle,functionality,appDepartmentKey
FROM employeeAppAuthorization
WHERE username LIKE 'padam%'
  AND appcode <> 'pto'; 
  
-- 2. 
-- CREATE new team 15 AND new tech
  -- team 15: codey olson emp#: 1100625, tech#: 275, hourly: 16, flat rate: 15.41
  -- tpTechs
DECLARE @eff_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr integer;
DECLARE @percentage double;
DECLARE @pto_rate double;
@eff_date = '08/06/2017';
@dept_key = 13;
@elr = 60;  
BEGIN TRANSACTION;
TRY
  INSERT INTO tpTechs(departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  values (@dept_key,'275','Codey','Olson','1100625','08/06/2017');
  -- tpTeams
  INSERT INTO tpteams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Team 15', '08/06/2017'); 
  -- tpTeamTechs
  @team_key = (SELECT teamkey FROM tpTeams WHERE teamname = 'team 15');
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'olson' AND firstname = 'codey');
  INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);
  -- tpTeamValues
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key,1,15.41,1,@eff_date);
  --tpTechValues
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key,@eff_date,1,16);
    
  INSERT INTO employeeAppAuthorization(username,appname,appseq,appcode,
    approle,functionality,appDepartmentKey)
  SELECT 'colson3@rydellcars.com', appname,appseq,appcode,
    approle,functionality,appDepartmentKey
  FROM employeeAppAuthorization
  WHERE username LIKE 'padam%'
    AND appcode <> 'pto';  
       
  DELETE FROM tpData  
  WHERE techkey= @tech_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();    
    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;      


-- 3. team 10: 
teamkey = 39
teckey = 74

SELECT * FROM tpteams

UPDATE tpteams
SET thrudate = '08/05/2017'
WHERE teamname = 'team 10';

update tpteamtechs
SET thrudate = '08/05/2017'
WHERE teamkey = 39
  AND techkey = 74;
  
update tptechs
SET thrudate = '08/05/2017'
WHERE techkey = 74;  

delete
FROM tpdata
WHERE techkey = 74
and thedate > '08/05/2017';