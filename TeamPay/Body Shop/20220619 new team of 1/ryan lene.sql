/*
07/01/2022
Happy  (almost) weekend Jon

Ryan Lene returned to work Monday so can you add him to the vision page please so he can see his progress (tech 267)

I am sure you need something else from someone else to get started so head me in the right direction please

Gayla Powell 

*/

OK big time, i winged this one, seems to have worked ok

select * FROM dds.factrepairorder WHERE techkey = 1633
SELECT * FROM dds.factrepairorder WHERE ro = '18091120'
SELECT * FROM dds.dimtech WHERE techkey = 1090
SELECT * FROM dds.dimtech WHERE technumber = '267'
SELECT * FROM tptechs WHERE technumber = '267'  -- techkey 56
SELECT * FROM tpteamtechs WHERE techkey = 56  -- teamkey 38
SELECT * FROM tpteams WHERE teamkey = 38
SELECT * FROM tpemployees WHERE lastname = 'lene'
SELECT * FROM employeeappauthorization WHERE username = 'rlene@rydellcars.com'

SELECT * FROM dds.dimtech WHERE rowchangereason = 'inactive to active'
select * FROM dds.dimtech WHERE employeenumber = '295148' ORDER BY techkey

-- these (both) throw NK violation: StoreCode;TechNumber;EmployeeNumber;Active;flagDeptCode;LaborCost
-- change labor cost for 1633 to 26, AND updates worked
-- so, should be good to go for factrepairorder
UPDATE dds.dimtech
SET laborcost = 26
WHERE techkey = 1633;
UPDATE dds.dimtech
SET active = false
where techkey IN (518,668,700);
UPDATE dds.dimtech
SET active = true, rowchangereason = 'rehire 6/27/22'
WHERE techkey = 1633;

DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
DECLARE @budget double;
DECLARE @previoushourlyrate double;
DECLARE @new_team string;

@from_date = '06/19/2022';
@dept_key = 13;
@budget = 30; -- initial flat rate of 30, team of 1 therefor budget = 30,  tech % = 1
@previoushourlyrate = 30;
@new_team = 'Team Ryan';

BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, @new_team, @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @new_team);
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, '267', 'Ryan', 'Lene', '184920', @from_date
  FROM system.iota;

  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'lene' AND thrudate > curdate());
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, @budget, 1, @from_date);
  
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, 1, @previoushourlyrate);
/*
  -- NOT needed, no prior data IN tpData for this pay period
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND techkey = @tech_key
    AND thedate >= @from_date;
*/    
  
--	

      
/* none of this IS needed anymore, nothing IN old vision for team pay apps
			
  INSERT INTO tpEmployees
  SELECT 'nnichols@rydellcars.com', firstname, lastname, employeenumber,
    '!aseTr8D9', 'Fuck You', storecode, TRIM(firstname) + ' ' + lastname
  --SELECT *  
  FROM dds.edwEmployeeDim 
  WHERE lastname = 'nichols'
    AND currentrow = true;			
			
  INSERT INTO employeeappauthorization
  SELECT 'nnichols@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'crose@rydellcars.com'
    AND appname = 'teampay';     
*/
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

EXECUTE PROCEDURE xfmTpData();

SELECT * FROM tpdata WHERE teamname = 'team ryan' ORDER BY thedate desc