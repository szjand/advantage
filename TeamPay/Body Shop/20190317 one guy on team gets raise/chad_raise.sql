
3/20/19
Can you change Chad Gardnerís flat rate pay from $21.75 to $22.00 effective immediately please.

Thank you
John Gardner


SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND teamName = 'team 3'
ORDER BY teamname, firstname  

NOT sure how im going to hanldle this one
randy has long ago abandoned the spreadsheet maintaining team bucget
AND this team, team 3, IS already out of spec
budget = 42, rates = 21.75 & 20.58 which = 42.33

just boost chad?
i think so, dont change team values, just tech values, AND since it IS retroactive 
to 3/17/19, DELETE AND UPDATE tpdata


SELECT * FROM tpdata WHERE teamkey = 32 AND thedate = curdate() ORDER BY techkey
DateKey	TheDate	PayPeriodStart	PayPeriodEnd	PayPeriodSeq	DayOfPayPeriod	DayName	DepartmentKey	TechKey	  clk     flag	                        clk     flag    prof
5562	3/24/2019	3/17/2019	3/30/2019	268	8	Sunday	13	30	251	150105	Chad	Gardner	0.5179	34.68	21.7518	0	42.87	0	 42.9	0	0	32	Team 3	2	42	0	85.11	0	150.6	0	176.95	
5562	3/24/2019	3/17/2019	3/30/2019	268	8	Sunday	13	34	246	1147061	Joshua	Walton	0.49	31.72	20.58	  0	42.24	0	107.7	0	0	32	Team 3	2	42	0	85.11	0	150.6	0	176.95	

DateKey	TheDate	PayPeriodStart	PayPeriodEnd	PayPeriodSeq	DayOfPayPeriod	DayName	DepartmentKey	TechKey	TechNumber	EmployeeNumber	FirstName	LastName	TechTeamPercentage	TechHourlyRate	TechTFRRate	TechClockHoursDay	TechClockHoursPPTD	TechFlagHoursDay	TechFlagHoursPPTD	TechFlagHourAdjustmentsDay	TechFlagHourAdjustmentsPPTD	TeamKey	TeamName	TeamCensus	TeamBudget	TeamClockHoursDay	TeamClockHoursPPTD	TeamFlagHoursDay	TeamFlagHoursPPTD	TeamProfDay	TeamProfPPTD	TechVacationHoursDay	TechVacationHoursPPTD	TechPTOHoursDay	TechPTOHoursPPTD	TechHolidayHoursDay	TechHolidayHoursPPTD	TechOtherHoursDay	TechOtherHoursPPTD	TechTrainingHoursDay	TechTrainingHoursPPTD	TeamOtherHoursDay	TeamOtherHoursPPTD	TeamTrainingHoursDay	TeamTrainingHoursPPTD	TeamFlagHourAdjustmentsDay	TeamFlagHourAdjustmentsPPTD	payPeriodSelectFormat
5562	3/24/2019	3/17/2019	3/30/2019	268	8	Sunday	13	30	251	150105	Chad	Gardner	0.52381	34.68	22.00002	0	42.87	0	42.9	0	0	32	Team 3	2	42	0	85.11	0	150.6	0	176.95	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 17 - March 30, 2019"
5562	3/24/2019	3/17/2019	3/30/2019	268	8	Sunday	13	34	246	1147061	Joshua	Walton	0.49	31.72	20.58	    0	42.24	0	107.7	0	0	32	Team 3	2	42	0	85.11	0	150.6	0	176.95	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	"March 17 - March 30, 2019"




DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @dept_key integer;
@eff_date = '03/17/2019';
@thru_date = '03/16/2019';
@dept_key = 13;

BEGIN TRANSACTION;
TRY
/*
to implement raises only
tpTechValues are ALL that need to be changed
his new techTeamPercentage
rate = tech% * budget
22 = 42 * tech%
tech% = .52381
*/
-- techValues
  -- chad gardner
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gardner' 
    AND firstname = 'chad' AND thrudate > curdate());
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .52381; -----------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	

  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date
	AND techkey =  @tech_key;
		 
  EXECUTE PROCEDURE xfmTpData();  
  EXECUTE PROCEDURE todayxfmTpData();   
    
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    			

