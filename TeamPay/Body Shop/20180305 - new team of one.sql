/*
Body Shop, Marcus Eberle
It is effective starting 3/5/2018, he is at $18.00 for his rate, he will also be at $18.00 for his pto rate.  He will be paid as an individual.

SELECT *
FROM tpTeams
WHERE departmentkey = 13
  AND thrudate > curdate()
*/
DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
@from_date = '03/04/2017';
@dept_key = 13;
BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, 'Team 16', @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 16');
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
  -- SELECT *
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'eberle'
    AND a.currentrow = true;
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'eberle');
  
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;

  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, 18, 1, @from_date);

  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, 1, 18);

--  DELETE FROM tpData  
--  WHERE departmentkey = @dept_key
--    AND techkey = @tech_key
--    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
  INSERT INTO employeeappauthorization
  SELECT 'meberle@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'blamont@rydellcars.com'
    AND appname = 'teampay';     
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

  
  
  