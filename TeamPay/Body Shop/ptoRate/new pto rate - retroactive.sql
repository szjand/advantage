-- to be effective 9/21/14
-- AS of today (9/30), these bs team members have pto hours
SELECT techKey, technumber, employeenumber, firstname, lastname,
  techPtoHoursPPTD, techVacationHoursPPTD
FROM tpData
WHERE thedate = curdate()
  AND departmentkey = 13
  AND techptohoursPPTD + techVacationHoursPPTD <> 0
ORDER BY lastname  
-- use time period of 2/9/14 -> 8/23/14
-- body shop
-- team techs
-- this IS the query that generated the new ptoRates for ben/randy
-- they approved AND sent these values to kim for UPDATE pymast
SELECT lastname, firstname, HourlyRate AS ptoRate, 
  SUM(clockHours) AS clockHours, SUM(commPay) AS commPay,
  round(SUM(commPay)/SUM(clockHours) , 2) AS newPtoRate
-- INTO #newPtoRate  
FROM (  
  select thedate, lastname, firstname, employeenumber,  
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, 
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = a.payperiodend
    AND departmentKey = 13
	AND thedate BETWEEN '02/09/2014' AND '08/23/2014'
  AND EXISTS (
    SELECT 1
    FROM tpTeamTechs
    WHERE techKey = a.techKey
      AND thruDate > curdate())) x
GROUP BY lastname, firstname, HourlyRate	
UNION
-- pdr
SELECT j.lastname, j.firstname, j.otherRate AS hourlyRate, 
  SUM(techClockHoursPPTD) AS clockHours, SUM(metalPay + pdrPay) AS commPay,
  round(SUM(metalPay + pdrPay)/SUM(techClockHoursPPTD), 2) AS newHourlyRate
-- SELECT *
FROM (-- clockhours
  SELECT theDate, techNumber, techClockHoursPPTD
  FROM bsFlatRateData
  WHERE thedate BETWEEN '02/09/2014' AND '08/23/2014'
    AND thedate = payperiodend) g
LEFT JOIN (-- metal pay
  SELECT techNumber, payPeriodEnd, round(SUM(techMetalFlagHoursDay * metalRate), 2) AS metalPay
  FROM (
    SELECT a.theDate, a.payPeriodEnd, a.techNumber, a.techMetalFlagHoursDay, b.metalRate
    FROM bsFlatRateData a
    LEFT JOIN bsFlatRateTechs b on a.technumber = b.technumber
      AND a.theDate BETWEEN b.fromDate AND b.thruDate
    WHERE thedate BETWEEN '02/09/2014' AND '08/23/2014') c 
  GROUP BY techNumber, payPeriodEnd) h on g.theDate = h.payPeriodEnd
LEFT JOIN (-- pdr pay
  SELECT techNumber, payPeriodEnd, round(SUM(techPdrFlagHoursDay * pdrRate), 2) AS pdrPay
  FROM (
    SELECT a.theDate, a.payPeriodEnd, a.techNumber, a.techPdrFlagHoursDay, b.pdrRate
    FROM bsFlatRateData a
    LEFT JOIN bsFlatRateTechs b on a.technumber = b.technumber
      AND a.theDate BETWEEN b.fromDate AND b.thruDate
    WHERE thedate BETWEEN '02/09/2014' AND '08/23/2014') c 
  GROUP BY techNumber, payPeriodEnd) i on g.theDate = i.payPeriodEnd
LEFT JOIN (
  SELECT DISTINCT lastname, firstname, otherRate
  FROM bsFlatRateTechs) j on 1 = 1  
GROUP BY j.lastname, j.firstname, j.otherRate  
ORDER BY lastname, firstname;

-- voila, arkona has been updated
SELECT a.*, cast(b.hourlyrate AS sql_double) AS hourly
FROM #newPtoRate a
LEFT JOIN dds.edwEmployeeDim b on a.lastname = b.lastname AND a.firstname = b.firstname
  AND b.currentrow = true
WHERE newPtoRate <> cast(b.hourlyrate AS sql_double)

9/30
of course, they want this to go INTO effect retroactive to pay period starting 9/21
tables that need to be updated:
PDR
  bsFlatRateTechs.otherRate
  unlike tpData, this value IS NOT stored IN bsFlatRateData
  
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '09/21/2014';
@thruDate = @fromDate - 1;
BEGIN TRANSACTION;
  TRY
    UPDATE bsFlatRateTechs
    SET thruDate = @thruDate
    WHERE thruDate > curdate();    
    INSERT INTO bsFlatRateTechs (storecode, departmentkey, technumber, employeenumber,
      username, firstname, lastname, fullname, pdrrate, metalrate, otherrate,
      fromdate, thrudate)
    SELECT a.storecode, a.departmentkey, a.technumber, a.employeenumber, 
      a.username, a.firstname, a.lastname, a.fullname, a.pdrRate, a.metalRate,
      b.hourlyRate, @fromDate, '12/31/9999'
    FROM bsFlatRateTechs a
    LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
      AND b.currentrow = true
    WHERE a.thrudate = @thruDate;      
  COMMIT WORK;
  CATCH ALL
    ROLLBACK;
    RAISE;
  END TRY;  
  
Teams
  tpTechValues.previousHourlyRate
  tpData.TechHourlyRate
  
no bs team tech has a techValue row that went INTO effect on 9/21 (IF that was
NOT the CASE, the techValue row that went INTO effect on 9/21 would simply
be updated with the new ptoRate)
/*
SELECT * 
FROM tpTechValues
WHERE fromdate = '09/21/2014'
*/
therefore
need to CLOSE current rows AND INSERT new rows for all

i keep fucking around with a multitude of different ways to come up with some
basic realities, feels LIKE the model IS bad OR i DO NOT understand how to use
it OR something
eg
which bs techs get a new ptoRate
underneath i am convinced it IS because i have NOT correctly AND completely 
implemented (or understood) the correct temporal (sequenced) constraints AND RI

anyway, stop your whining AND get the shit done 

back to whining
DO i UPDATE based on 
those that get a new ptoRate are IN #newPtoRate
SELECT * FROM #newPtoRate
OR FROM some notion of a tech being currently assigned to a bs team joined
to dds.edwEmployeeDim 

IN this CASE, going with the use of the #newPtoRate TABLE because it IS so
similar to type 2 udpates IN dds, AND that pattern uses the notion of a temp
TABLE (#typ2) that contain the subset of folks to be updated

part of the problem IS the ongoing refusal to take a fucking minute AND articulate
the required queries IN plain english

for those body shop techs listed IN #newPtoRate, UPDATE the current row of
tpTechValues with thruDate = 09/20/2014, AND INSERT a new row with 
fromDate = 09/21/14 AND previousHourlyRate = #newPtoRate.newPtoRate

/*
alternative way to derive those that need to be updated
current employees currently assigned to a team WHERE hourlyrate IN 
tpTechValues <> dds.edwEmployeeDim.hourlyrate

SELECT a.techkey, a.technumber, a.firstname, a.lastname, a.employeenumber, 
  b.previousHourlyRate, c.hourlyrate
FROM tpTechs a
INNER JOIN tpTechValues b on a.techkey = b.techkey
  AND b.thruDate > curdate()
INNER JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true
  AND c.active = 'active'
WHERE b.previousHourlyRate <> c.hourlyrate
  AND EXISTS (
    SELECT 1
    FROM tpTeamTechs
    WHERE techKey = a.techkey
      AND thruDate > curdatE())
ORDER BY a.techkey      
*/

-- just the team guys (no pdr)
-- include techkey IN #table
-- DROP TABLE #newPtoRate;
SELECT techkey, employeenumber, lastname, firstname, HourlyRate AS ptoRate, 
  SUM(clockHours) AS clockHours, SUM(commPay) AS commPay,
  round(SUM(commPay)/SUM(clockHours) , 2) AS newPtoRate
INTO #newPtoRate  
FROM (  
  select a.techkey, thedate, lastname, firstname, employeenumber,  
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, 
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = a.payperiodend
    AND departmentKey = 13
	AND thedate BETWEEN '02/09/2014' AND '08/23/2014'
  AND EXISTS (
    SELECT 1
    FROM tpTeamTechs
    WHERE techKey = a.techKey
      AND thruDate > curdate())) x
GROUP BY techkey, employeenumber, lastname, firstname, HourlyRate	

SELECT * FROM #newPtoRate;

DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '09/21/2014';
@thruDate = @fromDate - 1;
BEGIN TRANSACTION;
  TRY
    UPDATE tpTechValues
    SET thruDate = @thruDate
    WHERE thruDate > curdate()
      AND techKey IN (
        SELECT techKey 
        FROM #newPtoRate);  
        
    INSERT INTO tpTechValues (techKey, fromdate,thrudate,techteampercentage,
      previoushourlyrate)
    SELECT a.techKey, @fromDate, '12/31/9999', b.techTeamPercentage, a.newPtoRate
    FROM #newPtoRate a  
    LEFT JOIN tpTechValues b on a.techKey = b.techKey   
      AND b.thruDate = @thruDate;
      
    UPDATE tpData
    SET techHourlyRate = x.previousHourlyRate
    FROM (
      SELECT *
      FROM tpTechValues
      WHERE thruDate > curdate()) x
    WHERE departmentKey = 13
      AND payPeriodSeq = (
        SELECT distinct payPeriodSeq
        FROM tpData
        WHERE thedate = curdate())
      AND tpData.techKey = x.techKey;      
  COMMIT WORK;
  CATCH ALL
    ROLLBACK;
    RAISE;
  END TRY;  



