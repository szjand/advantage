changes to go INTO effect for pay period starting 7/27/14

Remove Robert Johnson FROM team
move justin olson FROM team terry to team loren
team terry
  census: 4 -> 2
  pool%: no change
  budget: 75.6 -> 37.8
  techs:
    remove FROM team:
	  robert johnson
	  justin olson
	tech%
	  mattthew ramirez .1761 -> .355
	  terrance driscoll .308 -> .616
team loren
  census: 3 -> 4
  pool%: .36 -> .35	  
  budget: 64.8 -> 84
    techs: 
	  add justin olson to team
	    tech%: .233 -> .21
	  chad gardner
	    tech%: .305 -> .236
	  joshua walton
	    tech%: .305 -> .236
	  loren shereck
	    tech%: .375 -> .29
		
tables that get changed:
  tpTeamValues
  tpTeamTechs
  tpTechValues		
		
the issue, AS always, IS the ORDER IN which to make these changes, 
the seems arduous every time, makes mesuspicious of the data model, eg, techTeamPercentage,
  should that be IN tpTechValues because it only makes sense IN the context
  of a tech assigned to a team. fuzzily, i keep thinking IF i get the constraints
  AND RI correct, that context IS implied AND coherently manifested
  
DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
DECLARE @elr integer;

@effDate = '07/27/2014'; -- first day of pay period  
@departmentKey = 13;  
@elr = ( -- 60
  SELECT effectiveLaborRate 
  FROM tpShopValues WHERE departmentKey = 13 AND thruDate > curdate());
  
BEGIN TRANSACTION;
TRY 
--< production -----------------------------------------------------------------<
/*
production: assuming this IS being run on Sunday, day 1 of the new pay period,
  the first day of which IS @effDate
  overnight run of tpData generated a row IN tpData for @effDate with the old
  team configs, DELETE that row, it will be repopulated at the END of this script
*/  
DELETE 
FROM tpData
WHERE theDate = @effDate
  AND departmentKey = @departmentKey;
--/> production ----------------------------------------------------------------/>
 
--< team terry -----------------------------------------------------------------<
@poolPerc = .315; ------------------------------
@teamKey = ( -- 17
  SELECT teamKey 
  FROM tpTeams WHERE teamName = 'Team Terry' AND thruDate > curdate());
-- remove robert johnson & justin olson FROM tpTeamTechs
@techKey = (-- 46
  SELECT techKey 
  FROM tpTechs WHERE lastname = 'johnson' AND firstname = 'robert');
UPDATE tpTeamTechs
SET thruDate = @effDate - 1
WHERE techKey = @techKey 
  AND teamKey = @teamKey
  AND thruDate > curdate();
@techKey = ( -- 35
  SELECT techKey 
  FROM tpTechs WHERE lastname = 'olson' AND firstname = 'justin');
UPDATE tpTeamTechs
SET thruDate = @effDate - 1
WHERE techKey = @techKey 
  AND teamKey = @teamKey
  AND thruDate > curdate();  
-- change tpTeamValues  
@census = (
  SELECT COUNT(*) 
  FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());
-- CLOSE current row
UPDATE tpTeamValues
SET thruDate = @effDate - 1
WHERE teamKey = @teamKey
  AND thruDate > curdate();
-- new row
INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
values (@teamKey, @effDate, @census, @poolPerc, 
  round(@census * @poolPerc * @elr, @census));  
-- tpTechValues 
-- ramirez 
@techKey = ( -- 38
  SELECT techKey
  FROM tpTechs WHERE lastname = 'ramirez' AND firstname = 'matthew'); 
@hourlyRate = (
  SELECT previousHourlyRate
  FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());  
@techPerc = .355; -------------------------------------
-- CLOSE current row
UPDATE tpTechValues
SET thruDate = @effDate - 1
WHERE techKey = @techKey
  AND thruDate > curDate();
-- new row
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @techPerc, @hourlyRate);
-- driscoll
@techKey = ( -- 42
  SELECT techKey
  FROM tpTechs WHERE lastname = 'driscoll' AND firstname = 'terrance'); 
@hourlyRate = (
  SELECT previousHourlyRate
  FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());  
@techPerc = .616; -------------------------------------  
-- CLOSE current row
UPDATE tpTechValues
SET thruDate = @effDate - 1
WHERE techKey = @techKey
  AND thruDate > curDate();
-- new row
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @techPerc, @hourlyRate); 
--/> team terry ----------------------------------------------------------------/>

--< team loren -----------------------------------------------------------------<
@poolPerc = .35; -----------------------------------
@teamKey = ( -- 15
  SELECT teamKey 
  FROM tpTeams WHERE teamName = 'Team Loren' AND thruDate > curdate());
-- ADD justin olson to team
@techKey = ( -- 35
  SELECT techKey 
  FROM tpTechs WHERE lastname = 'olson' AND firstname = 'justin');
-- CLOSE current row
UPDATE tpTeamTechs
SET thruDate = @effDate - 1
WHERE techKey = @techKey 
  AND teamKey = @teamKey
  AND thruDate > curdate();  
-- new row
INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
values (@teamKey, @techKey, @effDate);

-- change tpTeamValues  
@census = (
  SELECT COUNT(*) 
  FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());
-- CLOSE current row
UPDATE tpTeamValues
SET thruDate = @effDate - 1
WHERE teamKey = @teamKey
  AND thruDate > curdate();
-- new row
INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
values (@teamKey, @effDate, @census, @poolPerc, 
  round(@census * @poolPerc * @elr, @census));    
  
-- tpTechValues 
-- gardner
@techKey = ( -- 30
  SELECT techKey
  FROM tpTechs WHERE lastname = 'gardner' AND firstname = 'chad'); 
@hourlyRate = (
  SELECT previousHourlyRate
  FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());  
@techPerc = .236; -------------------------------------
-- CLOSE current row
UPDATE tpTechValues
SET thruDate = @effDate - 1
WHERE techKey = @techKey
  AND thruDate > curdate(); 
-- new row
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @techPerc, @hourlyRate);  
-- walton
@techKey = ( -- 34
  SELECT techKey
  FROM tpTechs WHERE lastname = 'walton' AND firstname = 'joshua'); 
@hourlyRate = (
  SELECT previousHourlyRate
  FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());  
@techPerc = .236; -------------------------------------
-- CLOSE current row
UPDATE tpTechValues
SET thruDate = @effDate - 1
WHERE techKey = @techKey
  AND thruDate > curdate(); 
-- new row
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @techPerc, @hourlyRate); 
-- shereck
@techKey = ( -- 37
  SELECT techKey
  FROM tpTechs WHERE lastname = 'shereck' AND firstname = 'loren'); 
@hourlyRate = (
  SELECT previousHourlyRate
  FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());  
@techPerc = .29; -------------------------------------
-- CLOSE current row
UPDATE tpTechValues
SET thruDate = @effDate - 1
WHERE techKey = @techKey
  AND thruDate > curdate(); 
-- new row
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @techPerc, @hourlyRate); 
-- olson
@techKey = ( -- 35
  SELECT techKey
  FROM tpTechs WHERE lastname = 'olson' AND firstname = 'justin'); 
@hourlyRate = (
  SELECT previousHourlyRate
  FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());  
@techPerc = .21; -------------------------------------
-- CLOSE current row
UPDATE tpTechValues
SET thruDate = @effDate - 1
WHERE techKey = @techKey
  AND thruDate > curdate(); 
-- new row
INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
values (@techKey, @effDate, @techPerc, @hourlyRate); 
--/> team loren ----------------------------------------------------------------/>
-- AND FINALLY, UPDATE tpdata  
EXECUTE PROCEDURE xfmTpData();
-- EXECUTE PROCEDURE todayxfmTpData();  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 