/*
2/1/19 FROM john gardner
Codey Olson was given a raise and approved by Ben and Jeri, 
do I need to let you know before it goes into affect. 
I am guessing you need to know for the payroll sheet you send Randy.
He went from $17.25 flat rate to $19.00 flat rate, and it will be 
effective this current pay period we are in.
*/
SELECT *
FROM tpemployees
WHERE lastname = 'olson'
  AND firstname = 'codey'
  
-- current reality  
SELECT a.teamname, a.teamkey, 
  b.census, b.budget, b.poolpercentage,
  d.lastname, d.firstname, d.techkey, 
  e.techteampercentage,
  e.techteampercentage * b.budget AS tech_rate
FROM tpTeams a
INNER JOIN tpteamvalues b on a.teamkey = b.teamkey
  AND b.thrudate > curdate()
INNER JOIN tpteamtechs c on a.teamkey = c.teamkey
  AND c.thrudate > curdate()  
INNER JOIN tptechs d on c.techkey = d.techkey  
INNER JOIN tptechvalues e on d.techkey = e.techkey
  AND e.thrudate > curdate()
WHERE a.departmentkey = 13 
  AND a.thrudate > curdate()  
  
  
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @new_percentage double;
DECLARE @budget double;
DECLARE @team_key integer;
DECLARE @census integer;
DECLARE @hourly double;
DECLARE @tech_team_perc double;
@from_date = '01/20/2019';
@thru_date = '01/19/2019';
BEGIN TRANSACTION;
TRY  

-- team of 1, just UPDATE teamvalues, 
-- codey olson  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 15');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, 19.00, 1, @from_date);  
  
  DELETE 
  FROM tpdata 
  WHERE departmentkey = 13
    AND teamname = 'team 15'
	AND thedate > @thru_date;
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData(); 

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    

-- SELECT * FROM tpdata WHERE teamkey = 44 AND thedate > '01/19/2019'