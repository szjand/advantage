/*
06/16/22
Hello!

Kyle Buchanan received an increase to $25.50 approved on 6/6.
Justin Olson received an increase to $27.50 approved on 6/6.
Codey Olson received an increase to $25.50 approved on 6/6.
Scott Sevigny received an increase to $27.50 approved on 6/6.

Thank you, 
Ashley

*/

SELECT *
FROM tptechs
WHERE lastname = 'sevigny'
  AND firstname = 'codey'


SELECT * FROM tpteams WHERE teamname = 'team 14'	
  
kyle buchanan team 14 tech_key: 76   team_key: 43
justin olson team 6   tech_key: 35   team_key: 35
codey olson   team 15 tech_key: 77   team_key: 44
scott sevigny team 7  tech_key: 41   team_key: 36

-- current reality  
SELECT a.teamname, a.teamkey, 
  b.census, b.budget, b.poolpercentage,
  d.lastname, d.firstname, d.techkey, 
  e.techteampercentage,
  e.techteampercentage * b.budget AS tech_rate
FROM tpTeams a
INNER JOIN tpteamvalues b on a.teamkey = b.teamkey
  AND b.thrudate > curdate()
INNER JOIN tpteamtechs c on a.teamkey = c.teamkey
  AND c.thrudate > curdate()  
INNER JOIN tptechs d on c.techkey = d.techkey  
INNER JOIN tptechvalues e on d.techkey = e.techkey
  AND e.thrudate > curdate()
WHERE a.departmentkey = 13 
  AND a.thrudate > curdate()  
	AND a.teamkey IN (43,35,44,36)
	
/*
for this TRANSACTION to function IN zzTest, DELETE the ukg link
AND re-create it : 

EXECUTE PROCEDURE 
  sp_CreateLink ( 
     'ukg',
     '\\67.135.158.12:6363\advantage\ukg\ukg.add',
     TRUE,
     TRUE,
     TRUE,
     '',
     '');

SELECT * FROM ukg.employees			 
*/		 
		 
	 
  
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @new_percentage double;
DECLARE @budget double;
DECLARE @team_key integer;
DECLARE @census integer;
DECLARE @hourly double;
DECLARE @tech_team_perc double;
@from_date = '06/05/2022';
@thru_date = '06/04/2022';
BEGIN TRANSACTION;
TRY  

/*
Kyle Buchanan received an increase to $25.50 approved on 6/6.
Justin Olson received an increase to $27.50 approved on 6/6.
Codey Olson received an increase to $25.50 approved on 6/6.
Scott Sevigny received an increase to $27.50 approved on 6/6.

kyle buchanan team 14 tech_key: 76   team_key: 43
justin olson team 6   tech_key: 35   team_key: 35
codey olson   team 15 tech_key: 77   team_key: 44
scott sevigny team 7  tech_key: 41   team_key: 36
*/
-- team of 1, just UPDATE teamvalues, 
-- kyle buchanan
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 14');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, 25.50, 1, @from_date);  
 
-- justin olson
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 6');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, 27.50, 1, @from_date);  

-- codey olson
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 15');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, 25.50, 1, @from_date);  	
	
-- scott sevigny
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 7');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, 27.50, 1, @from_date);  		
	  
  DELETE 
  FROM tpdata 
  WHERE departmentkey = 13
    AND teamkey IN (43,35,44,36)
	  AND thedate > @thru_date;
		
	
  EXECUTE PROCEDURE xfmTpData();


COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    
	