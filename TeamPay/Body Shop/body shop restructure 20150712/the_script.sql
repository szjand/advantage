/*
-- current reality
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
ORDER BY teamname, firstname;

remove jeremy rosenau FROM team The Shop
so, what ALL has to change:
tpTechs
tpTeamTechs
tpTeamValues
tpTechValues
team census FROM 19 to 18
team budget FROM 399 to 378
each remaining tech, the techTeamPerc changes buth the techTFRRate must remain the same
*/
-- run on 7/12/15, first day of new payperiod IN which the changes take effect
DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @budget integer;
DECLARE @poolPerc double;

@effDate = '07/12/2015';  
@departmentKey = 13; 
@techkey = (SELECT techkey FROM tpTechs WHERE lastname = 'rosenau');
@teamkey = (SELECT teamkey FROM tpTeams WHERE departmentKey = @departmentKey
  AND teamName = 'The Shop');
@census = 18;
@budget = 378; 
@poolPerc = .35;
  
BEGIN TRANSACTION;
TRY
  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;
  TRY     
  -- techs    
    UPDATE tpTechs
    SET thruDate = @effDate - 1
    WHERE techkey = @techkey
      AND thruDate > @effDate -1;
  CATCH ALL
    raise tptechs(1, __errtext);
  END TRY;  
  TRY 
  -- tpTeamTechs
    UPDATE tpTeamTechs
    SET thrudate = @effDate - 1
    -- SELECT * FROM tpTeamTechs
    WHERE teamkey = @teamKey
      AND techkey = @techkey
      AND thrudate > @effDate - 1;  
  CATCH ALL
    raise tpTeamTechs(1, __errtext);
  END TRY; 
  TRY
  -- tpTeamValues
  -- CLOSE old row
  UPDATE tpTeamValues
  SET thrudate = @effDate -1 
  WHERE teamKey = @teamKey
    AND thruDate > @effDate -1;
  -- INSERT new row
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
  values(@teamKey,@census,@budget,@poolPerc,@effDate);
  CATCH ALL
    RAISE tpTeamValues(1, __errtext);
  END TRY;
  TRY
  -- tpTechValues
  -- CLOSE out rosenau, but DO NOT INSERT a new row
  -- CLOSE the old rows
  UPDATE tpTechValues
  SET thruDate = @effDate -1
  WHERE techkey IN (
    SELECT a.techkey
    FROM tpTechs a
    INNER JOIN tpTeamTechs b on a.techkey = b.techkey
    INNER JOIN tpTeams c on b.teamkey = c.teamkey
      AND c.teamKey = @teamKey
    WHERE a.thrudate > @effDate -1) 
  AND thruDate > @effDate - 1; 
  -- new rows 
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
  SELECT a.techkey, @effDate, round(d.techtfrrate/378, 6), e.previoushourlyrate 
  FROM tpTechs a
  INNER JOIN tpTeamTechs b on a.techkey = b.techkey
  INNER JOIN tpTeams c on b.teamkey = c.teamkey
    AND c.teamKey = @teamKey
  LEFT JOIN tpdata d on a.techkey = d.techkey
    AND d.thedate = @effDate - 1
  LEFT JOIN tpTechValues e on a.techkey = e.techkey
    AND e.thrudate = @effDate - 1  
  WHERE a.techkey <> @techkey;
  CATCH ALL
    RAISE tpTechValues(1, __errtext);
  END TRY;  
  TRY 
  -- AND FINALLY, UPDATE tpdata  
    EXECUTE PROCEDURE xfmTpData();
    EXECUTE PROCEDURE todayxfmTpData();  
    EXECUTE PROCEDURE xfmDeptTechCensus();
    EXECUTE PROCEDURE xfmTmpBen();
  CATCH ALL
    RAISE xfmTpData(1, __errtext);
  END TRY;  

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;


