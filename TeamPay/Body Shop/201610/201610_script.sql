/*
FROM ben cahalan 10/6/16
changes made AND deployed 10/9/16

Summary of changes to be made.

Corey Rose - $24.76 per commission hour - Individual flat rate

Ryan Lene - $24.76 per commission hour - Individual flat rate 

Scott Sevigny - same wage as before - Individual flat rate

Brandon Lamont - same wage as before - Individual flat rate

Justin Olson - $17.22 per commission hour - individual flat rate


Pat Adams - Increase commission rate to $13.25 per hour - No other change

*/

/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
ORDER BY teamname

*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr integer;
DECLARE @percentage double;
DECLARE @pto_rate double;
@eff_date = '10/02/2016';
@thru_date = '10/01/2016';
@dept_key = 13;
@elr = 60;
BEGIN TRANSACTION;
TRY
-- 1. the easy one: Pat Adam2 gets a RAISE, 12.67 -> 13.25
-- 	  there IS enuf overhead IN current team budget, 96, to accomodate his increase
--	  so, ALL that IS needed IS to increase IS techTeamPercentage FROM .132 to .1380
  @tech_key = (
    SELECT techkey 
	FROM tpTechs
	WHERE firstname = 'Patrick' AND lastname = 'Adam');
  @pto_rate = (
    SELECT previousHourlyRate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());
  UPDATE tptechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey,fromdate,techTeamPercentage)
  values(@tech_key, @eff_date, .1380); 
-- 2. new teams
  INSERT INTO tpteams (departmentKey,teamName,fromDate) 
  values (@dept_key, 'Team 7', @eff_date);
  INSERT INTO tpteams (departmentKey,teamName,fromDate) 
  values (@dept_key, 'Team 8', @eff_date);
  INSERT INTO tpteams (departmentKey,teamName,fromDate) 
  values (@dept_key, 'Team 9', @eff_date);    
-- 3. team 1 remove scott AND put him on team 7
  -- team 1
  @team_key = (
    SELECT teamkey
	FROM tpteams
	WHERE teamname = 'team 1');
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE firstname = 'scott' AND lastname = 'sevigny');
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamKey = @team_key
    AND techKey = @tech_key;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,1,16.99,.28317,@eff_date);
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE firstname = 'brandon' AND lastname = 'lamont');  
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, 1, @pto_rate);	  
  -- team 7
  @team_key = (
    SELECT teamkey
	FROM tpteams
	WHERE teamname = 'team 7');
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE firstname = 'scott' AND lastname = 'sevigny'); 	
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,1,19.54,.3257,@eff_date);  
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, 1, @pto_rate);		
-- 4. team 4 remove cory put him on team 8 w/rate of 24.76
  -- team 4
  @team_key = (
    SELECT teamkey
	FROM tpteams
	WHERE teamname = 'team 4');
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE firstname = 'cory' AND lastname = 'rose');  
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamKey = @team_key
    AND techKey = @tech_key; 
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,1,22.76,.3793,@eff_date);	
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE firstname = 'loren' AND lastname = 'shereck');  
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, 1, @pto_rate);	 
-- team 8
  @team_key = (
    SELECT teamkey
	FROM tpteams
	WHERE teamname = 'team 8');	
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE firstname = 'cory' AND lastname = 'rose');  	
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);	
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,1,24.76,.4127,@eff_date); 	
  @pto_rate = (
    SELECT previousHourlyRate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, 1, @pto_rate);	  
-- 5. team 6 remove ryan put him on team 9 w/rate of 24.76, justin stays on
--      team 6 with an increase to 17.22
  -- team 6
  @team_key = (
    SELECT teamkey
	FROM tpteams
	WHERE teamname = 'team 6');  
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE firstname = 'ryan' AND lastname = 'lene');
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamKey = @team_key
    AND techKey = @tech_key; 
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();	
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,1,17.22,.287,@eff_date);		
  @tech_key = (	
    SELECT techkey
	FROM tptechs
	WHERE firstname = 'justin' AND lastname = 'olson');  
  @pto_rate = (
    SELECT previousHourlyRate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());	
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, 1, @pto_rate);	    
   -- team 9
  @team_key = (
    SELECT teamkey
	FROM tpteams
	WHERE teamname = 'team 9');  
  @tech_key = (
    SELECT techkey
	FROM tptechs
	WHERE firstname = 'ryan' AND lastname = 'lene');  
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values (@team_key, @tech_key, @eff_date);	
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();	
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key,1,24.76,.4127,@eff_date); 	
  @pto_rate = (
    SELECT previousHourlyRate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, 1, @pto_rate);	  

  
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
               
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  