-- current values
SELECT a.teamKey, a.teamName, b.census, b.budget, b.poolPercentage, d.firstName, d.lastName, 
  d.techKey, e.techTeamPercentage, budget * techTeamPercentage AS rate, 
  (budget * techTeamPercentage)/96 AS "New %"
FROM tpTeams a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thrudate > curdate()
LEFT JOIN tpTeamTechs c on a.teamKey = c.teamKey
LEFT JOIN tpTechs d on c.techKey = d.techKey  
LEFT JOIN tpTechValues e on d.techKey = e.techKey
  AND e.thruDate > curdate()
WHERE a.thrudate > curdate()
  AND a.teamname = 'quick team'
  
SELECT username, password
FROM tpemployees
WHERE lastname IN ('driscoll','mavity','lindom','ramirez','iverson')  
  
10/30/15  

Remove Kristen Swanson from quick team effective in current pay period 10/18 -> 10/31
This IS NOT a CASE of her being on a team, THEN off a team THEN on a team
this IS a CASE of fucking randy sattler NOT HAVING his shit together, Kristen 
should NOT have gone on a team until 11/1

THEN she goes back on 11/1

DECLARE @from_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
@from_date = '10/18/2015';
@thru_date = '12/31/9999';
@tech_key = 66; -- kristen
@team_key = 25; -- quick team

BEGIN TRANSACTION;
TRY 
/**/
--  UPDATE tptechs
--  SET thrudate = @from_date -1
--  WHERE techkey = @tech_key;
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key;
--  UPDATE tpteamtechs 
--  SET thrudate = @from_date - 1
--  WHERE teamkey = @team_key
--    AND techkey = @tech_key;
  DELETE 
  FROM tpTeamTechs
  WHERE techkey = @tech_key;
  
  DELETE 
  FROM tpTechs
  WHERE techkey = @tech_key;  
  
  UPDATE tpteamvalues
  SET census = 5,
      budget = 96
  WHERE teamkey = @team_key
    AND fromdate = @from_date;
/**/    
/* 
  current elr = 60
  new budget = 5 * 60 * .32 = 96
  rate/budget = techTeamPercentage
  see results FROM current values query above    
*/               
  @tech_key = 42; -- driscoll
  UPDATE tptechvalues
  SET techTeamPercentage = .264
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
    
  @tech_key = 45; -- mavity
  UPDATE tptechvalues
  SET techTeamPercentage = .222
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;  
    
  @tech_key = 52; -- lindom
  UPDATE tptechvalues
  SET techTeamPercentage = .162
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
    
  @tech_key = 57; -- ramirez
  UPDATE tptechvalues
  SET techTeamPercentage = .162
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
    
  @tech_key = 65; -- iverson
  UPDATE tptechvalues
  SET techTeamPercentage = .192
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;        
/**/ 

  DELETE 
  FROM tpdata
  WHERE teamkey = @team_key
    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();   
/**/
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   
 
  
  