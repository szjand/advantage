team proficiency for team pete (19) AND team cody (18) needs to be based on the 
totals for the 2 teams

-- UPDATE existing data
-- modify xfmTpData
-- modify todayXfmTpData

so, go ahead AND run the regular UPDATE IN the stored procs,
THEN run these 2 for the paint teams

  
-- day
-- no need for OtherHours
-- one row per team/day
SELECT distinct thedate, teamkey,
  teamflaghoursday, teamTrainingHoursDay, 
  TeamFlagHourAdjustmentsDay, teamclockhoursday
FROM tpdata
WHERE teamkey IN (18,19) 
ORDER BY thedate, teamkey

-- 1 row per day, SUM of the 2 teams
SELECT thedate, 
  SUM(teamFlagHoursDay + teamTrainingHoursDay 
    + teamFlagHourAdjustmentsDay) AS flagHours,
  SUM(teamClockHoursDay) AS clockHours
FROM ( 
  SELECT distinct thedate, teamkey,
    teamflaghoursday, teamTrainingHoursDay, 
    TeamFlagHourAdjustmentsDay, teamclockhoursday
  FROM tpdata
  WHERE teamkey IN (18,19)) a 
GROUP BY thedate  

-- final for day 
UPDATE tpdata
SET teamprofday = x.prof
FROM (
  SELECT b.*,
    CASE 
      WHEN totalFlag = 0 OR totalClock = 0 THEN 0
      ELSE round(100 * totalFlag/totalClock, 2)
    END AS prof  
	FROM (
      SELECT thedate, 
        SUM(teamFlagHoursDay + teamTrainingHoursDay 
          + teamFlagHourAdjustmentsDay) AS totalFlag,
        SUM(teamClockHoursDay) AS totalClock
      FROM ( 
        SELECT distinct thedate, teamkey,
          teamflaghoursday, teamTrainingHoursDay, 
          TeamFlagHourAdjustmentsDay, teamclockhoursday
        FROM tpdata
        WHERE teamkey IN (18,19)) a 
      GROUP BY thedate) b) x 
WHERE tpData.teamKey IN (18,19)
  AND tpData.theDate = x.theDate;	  		
  
-- final for pptd
UPDATE tpdata
SET teamprofPPTD = x.prof
FROM (
  SELECT b.*,
    CASE 
      WHEN totalFlag = 0 OR totalClock = 0 THEN 0
      ELSE round(100 * totalFlag/totalClock, 2)
    END AS prof  
	FROM (
      SELECT thedate, 
        SUM(teamFlagHoursPPTD + teamTrainingHoursPPTD 
          + teamFlagHourAdjustmentsPPTD) AS totalFlag,
        SUM(teamClockHoursPPTD) AS totalClock
      FROM ( 
        SELECT distinct thedate, teamkey,
          teamflaghoursPPTD, teamTrainingHoursPPTD, 
          TeamFlagHourAdjustmentsPPTD, teamclockhoursPPTD
        FROM tpdata
        WHERE teamkey IN (18,19)) a 
      GROUP BY thedate) b) x 
WHERE tpData.teamKey IN (18,19)
  AND tpData.theDate = x.theDate;	 
	  
  