the only change IS that Josh Walton (blue team) gets a $1/hr RAISE
team budget does NOT need to change, 
budget IS SET at $84, 
but the SUM of tech team % for the 4 techs IS only 88.3%, $74.17
which leaves head room IN the budget for individual raises

Also changing pto rate for everyone, go ahead AND DO josh IN this script, 
leave him out of the general pto rate update

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
AND teamname = 'blue team'

SELECT *
FROM tptechvalues
WHERE techkey = 34

SELECT .2499 * 84 FROM system.iota

DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @new_percentage double;
DECLARE @team_budget integer;
@from_date = '02/07/2016';
@thru_date = @from_date - 1;
@tech_key = 34;
@new_pto_rate = 27.79;
@new_percentage = .2499;
@team_budget = 84;
--SELECT @thru_date FROM system.iota;
BEGIN TRANSACTION;
TRY 
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techKey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, @new_percentage, @new_pto_rate); 
  UPDATE tpdata
  SET techteampercentage = @new_percentage, 
      techhourlyrate = @new_pto_rate,
      techTFRRate = @team_budget * @new_percentage
  WHERE thedate = curdate()
    AND techkey = @tech_key;
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
  