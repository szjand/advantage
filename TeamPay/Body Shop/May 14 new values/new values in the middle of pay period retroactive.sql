-- current values
/*
SELECT a.teamKey, a.teamName, b.census, b.budget, b.poolPercentage, d.firstName, d.lastName, 
  d.techKey, e.techTeamPercentage, budget * techTeamPercentage
FROM tpTeams a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thrudate > curdate()
LEFT JOIN tpTeamTechs c on a.teamKey = c.teamKey
LEFT JOIN tpTechs d on c.techKey = d.techKey  
LEFT JOIN tpTechValues e on d.techKey = e.techKey
  AND e.thruDate > curdate()
WHERE a.thrudate > curdate()
  AND a.departmentKey = 13
*/  

/*
5/14/14
current payperiod: 5/4/14/ -> 5/17/14
randy IS changing shop values AND ALL team percentages 
AND pdr metal rate AND wants it to be
retroactive, that ALL this shit takes effect for the current pay period

working against X:\DailyCut\scoTest\Tuesday\copy, a copy of scotest FROM last night

WHEN finished, check values FROM execute procedure getTPEmployeeTotalsForDepartment('bodyshop',0)
with the picture i took of the live app this morning


i am leaning toward a less generalized solution to a narrow solution that fits 
just this particular scenario (which means a shit load of hard coding values IN queries)
eg the only changes are to 
  tpShopValues.EffectiveLaborRate
  tpTeamValues.budget
  tpTeamValues.poolPercentage (1 team only)
  tpTechValues.techTeamPercentage
Changes:
-- ** don't forget pdr
  DELETE ALL current payperiod tpData
--  reset tpTeamValues, tpTechValues to previous payperiod, turns out don't need
--  this, there IS currently only a single value for each team
  tpShopValues: 
    ELR changes from 57 -> 60
  tpTeamValues:
    poolPercentage & budget changes for every team
  tpTechValues:
    techTeamPercentage changes for every tech except 1
  run xfmTpData

*/

/* 
5/16/14
  PDR, forgot to UPDATE xfmBsFlatRateData AND todayXfmBsFlatRateData to 
    accomodate there being multiple rows, now, IN bsFlatRateTechs,
    so it IS ALL fucked up, 2 base rows per day,
    doubling the flaghours, etc
    modified both stored procs to fix
*/
DECLARE @effDate date;
DECLARE @lastName string;
DECLARE @teamName string;
DECLARE @techKey integer;
DECLARE @teamKey integer;
DECLARE @perc double;
DECLARE @hourly double;
DECLARE @departmentKey integer;
DECLARE @newELR double;
DECLARE @census integer;
DECLARE @budget double;
DECLARE @newPdrMetalRate double;
BEGIN TRANSACTION;
  TRY 

  @effDate = '05/04/2014';
	@departmentKey = 13;
	@newELR = 60;
  @newPdrMetalRate = 20.4;

-- these were necesssary to simulate going back to beginning of payperiod
  DELETE FROM tpData WHERE departmentKey = 13 AND thedate >= @effDate;
-- pdr, ALL changes
  DELETE FROM bsFlatRateData WHERE thedate >= @effDate;
  UPDATE bsFlatRateTechs
    SET thruDate = @effDate - 1
  WHERE thruDate = '12/31/9999';
  INSERT INTO bsFlatRateTechs (storeCode, departmentKey, techNumber, employeeNumber, 
    username, firstName, lastName, fullName, pdrRate, metalRate, otherRate, 
    fromDate, thruDate)
  SELECT storeCode, departmentKey, techNumber, employeeNumber, 
    username, firstName, lastName, fullName, pdrRate, @newPdrMetalRate, otherRate, 
    @effDate, '12/31/9999'
  FROM bsFlatRateTechs
  WHERE fromDate = '01/12/2014';
--  DELETE FROM tpShopValues WHERE fromDate = '03/09/2014' AND departmentKey = 18;
--  UPDATE tpShopValues SET thruDate = '12/31/9999' WHERE thruDate = '03/08/2014' AND departmentKey = 18;
--  DELETE FROM tpTeamValues WHERE fromDate = '03/09/2014' AND thruDate = '12/31/9999';
 -- UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '02/23/2014';  

  TRY
-- tpShopValues.EffectiveLaborRate
    -- CLOSE previous pay period
    UPDATE tpShopValues
    SET thruDate = @effDate - 1
    WHERE thrudate = '12/31/9999'
      AND departmentKey = @departmentKey;       
    -- INSERT row for new pay period
    INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
    values(@departmentKey, @effDate, @newELR);   
  CATCH ALL
    RAISE Updates(3, 'shop values ' + __errtext);
  END TRY;   
  

  TRY
--  tpTeamValues.budget
--  tpTeamValues.poolPercentage  
/*
select b.teamName, a.*
FROM tpTeamValues a
INNER JOIN tpTeams b on a.teamKey = b.teamKey
WHERE b.departmentKey = 13 ORDER BY a.teamKey; 

only 1 team has a changed pool percentage
ALL the others, budget changes AS a result of tpShopValues.ELR changing only
*/
    UPDATE tpTeamValues
    SET thruDate = @effDate -1
    WHERE thruDate = '12/31/9999'
      AND teamKey IN (
        SELECT teamKey
        FROM tpTeams
        WHERE departmentKey = @departmentKey);
        
    INSERT INTO tpTeamValues (teamKey, Census, poolPercentage, budget, fromDate, thruDate)
    SELECT a.teamKey, census, 
      CASE teamName -- only team terry pool percentage
        WHEN 'Team Terry' THEN .315
        ELSE poolPercentage  
      END,     
      CASE teamName
        WHEN 'team loren' THEN 64.8  
        WHEN 'team cory' THEN 61.2
        WHEN 'team terry' THEN 75.6
        WHEN 'team cody' THEN 63
        WHEN 'team pete' THEN 64.8
      END, 
      @effDate, '12/31/9999'      
    FROM tpTeamValues a
    INNER JOIN tpTeams b on a.teamKey = b.teamKey
    WHERE b.departmentKey = 13;   
  CATCH ALL
    RAISE Updates(4, 'tpTeamValues ' + __errtext);
  END TRY;       

  TRY
-- tpTechValues.techTeamPercentage  
    -- Chris Walden
  	@techKey = 31;
  	@perc = .315; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);	

    -- Peter Jacobson
  	@techKey = 43;
  	@perc = .375; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);		 

    -- Robert Mavity
  	@techKey = 45;
  	@perc = .285; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);	    

    -- Mavrik Peterson
  	@techKey = 39;
  	@perc = .30; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);    
     
    -- Jeremy Rosenau
  	@techKey = 44;
  	@perc = .332; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);    
    
    --  Cody Johnson
  	@techKey = 47;
  	@perc = .34; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);     
   
    --  Justin Olson
  	@techKey = 35;
  	@perc = .233; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);              

    --  Matthew Ramirez
  	@techKey = 38;
  	@perc = .1761; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);    
     
    --  Terrance Driscoll
  	@techKey = 42;
  	@perc = .308; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly); 
  
    --   Robert Johnson
  	@techKey = 46;
  	@perc = .243; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);
    
    --   Cory Rose
  	@techKey = 32;
  	@perc = .395; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);
        
    --   David Perry
  	@techKey = 33;
  	@perc = .277; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);
    
    --    Scott Sevigny
  	@techKey = 41;
  	@perc = .311; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);    
       
    --   Chad Gardner
  	@techKey = 30;
  	@perc = .305; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);               
     
    --    Joshua Walton
  	@techKey = 34;
  	@perc = .305; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);     
        
    --   Loren Shereck
  	@techKey = 37;
  	@perc = .375; 
  	@hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techKey = @techKey AND thruDate > curdate());
  -- CLOSE current tpTechValues row	
  	UPDATE tpTechValues
  	SET thruDate = @effDate - 1
  	WHERE techKey = @techKey
  	  AND thruDate > curdate();
  -- new tpTechValues row	
    INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
    values (@techKey, @effDate, @perc, @hourly);  
                        
  CATCH ALL
    RAISE Updates(5, 'tpTechValues ' + 'techKey: ' + CAST(@techKey AS sql_char)  + ' .  ' + __errtext);
  END TRY;  
  
-- AND FINALLY, UPDATE tpdata  
  TRY  
    EXECUTE PROCEDURE xfmTpData();
    EXECUTE PROCEDURE xfmBsFlatRateData();
    EXECUTE PROCEDURE todayxfmTpData();  
  CATCH ALL
    RAISE Updates(6, 'data ' + __errtext);
  END TRY;    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 


   
  
  