/*
1.
9/15/14
ben wants to ADD 2 techs to bs team Terry, effective at beginning of current
pay period (9/7 - 9/20)

pay period effective rate	$60.00 
Techs 4 	
Team %: 31.5%		 				 
Budget	 $75.60 
		
Terry D	 		30.8%	 $23.28 
Matt R			19.5%	 $14.74 
Travis B		18.9%	 $14.29 
Aaron L			18.9%	 $14.29 

need to ADD new techs
refer to teampay\mainshop\main shop team resructure july 2014\july2014scratchpad.sql

*/
DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @ELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;

@effDate = '09/07/2014';  
@departmentKey = 13;
@teamKey = (
  SELECT teamKey
  FROM tpTeams WHERE teamName = 'team terry' AND thruDate > curdate());
@ELR = (
  SELECT effectiveLaborRate
  FROM tpShopValues 
  WHERE departmentKey = @departmentKey 
    AND thruDate > curdate());
@poolPerc = .315;

BEGIN TRANSACTION;
TRY 

-- this portion IS necessary for backfilling the existing payperiod
DELETE FROM tpData WHERE thedate > '09/06/2014' AND teamKey = @teamKEy;



-- 1. new techs to tpEmployees 
-- travis beniot
TRY 
  @user = 'tbeniot@rydellcars.com';
  @password = 'Yav66Wne';
  INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
    membergroup, storecode, fullname)
  SELECT @user, firstname, lastname, employeenumber, @password, 
    'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
  FROM dds.edwEmployeeDim 
  WHERE lastname = 'beniot' 
    AND firstname = 'travis'
    AND currentrow = true 
    AND active = 'active';
-- aaron lindom
  @user = 'alindom@rydellcars.com';
  @password = 'gue55Ypn';
  INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
    membergroup, storecode, fullname)
  SELECT @user, firstname, lastname, employeenumber, @password, 
    'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName) 
  FROM dds.edwEmployeeDim 
  WHERE lastname = 'lindom' 
    AND firstname = 'aaron'
    AND currentrow = true 
    AND active = 'active';
CATCH ALL
  RAISE scratchpad(1, '1 ' + __errtext);
END TRY; 	
  
--2. new techs to employeeAppAuthorization
-- tp
TRY 
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 101 -- body shop team pay specific
    AND b.approle = 'technician'
    AND a.username IN('tbeniot@rydellcars.com','alindom@rydellcars.com'); 	
-- cstfb	
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'cstfb'
    AND b.approle = 'all'
    AND a.username IN('tbeniot@rydellcars.com','alindom@rydellcars.com'); 
CATCH ALL
  RAISE scratchpad(2, '2 ' + __errtext);
END TRY; 		
--3. new techs to tpTechs
TRY 
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @departmentKey,
    b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE (
        a.lastname = 'beniot' AND a.firstname = 'travis'
        OR a.lastname = 'lindom' AND a.firstname = 'aaron')
      AND a.currentrow = true 
      AND a.active = 'active';  
CATCH ALL
  RAISE scratchpad(3, '3 ' + __errtext);
END TRY; 	
--4. new techs to tpTechValues
-- travis
TRY 
  @techPerc = 0.189; /************************/
  @hourlyRate = 14;  /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'beniot' AND thrudate > curdate());
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
-- aaron  
  @techPerc = 0.189; /************************/
  @hourlyRate = 12;  /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'lindom' AND thrudate > curdate());
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
CATCH ALL
  RAISE scratchpad(4, '4 ' + __errtext);
END TRY; 	  
--5. edit tpTechValues for existing techs on team
-- terry
TRY 
  @techPerc = 0.308; /************************/
  @hourlyRate = 19.25;  /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'driscoll' AND thrudate > curdate());
  -- CLOSE current row
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey
    AND thruDate > curdate();
  -- new row
  INSERT INTO tpTechValues (techKey,fromDate,thruDate,techTeamPercentage,
    previousHourlyRate)
  values(@techKEy, @effDate, '12/31/9999', @techPerc, @hourlyRate); 
-- matt
  @techPerc = 0.195; /************************/
  @hourlyRate = 11;  /************************/
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'ramirez' AND thrudate > curdate());
  -- CLOSE current row
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey
    AND thruDate > curdate();
  -- new row
  INSERT INTO tpTechValues (techKey,fromDate,thruDate,techTeamPercentage,
    previousHourlyRate)
  values(@techKEy, @effDate, '12/31/9999', @techPerc, @hourlyRate); 
CATCH ALL
  RAISE scratchpad(5, '5 ' + __errtext);
END TRY; 	  
--6. new techs to tpTeamTechs
  -- travis
TRY   
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'beniot' AND thrudate > curdate());
  INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
  values (@teamKey, @techKey, @effDate);
  -- aaron
  @techKey = (
    SELECT techKey 
    FROM tpTechs WHERE lastname = 'lindom' AND thrudate > curdate());
  INSERT INTO tpTeamTechs (teamKey, techKey, fromDate)
  values (@teamKey, @techKey, @effDate);
CATCH ALL
  RAISE scratchpad(6, '6 ' + __errtext);
END TRY; 	
--7. UPDATE tpTeamValues for team terry
TRY 
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > curdate());
  -- CLOSE current row
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > curdate();
  -- new row
  INSERT INTO tpTeamValues(teamKey,census,budget,poolpercentage,fromdate)
  values(@teamKey, @census, round(@census * @poolPerc * @ELR, 2), 
  @poolPerc, @effDate);
CATCH ALL
  RAISE scratchpad(7, '7 ' + __errtext);
END TRY; 	
--8. AND FINALLY, UPDATE tpdata  
    EXECUTE PROCEDURE xfmTpData();
    EXECUTE PROCEDURE todayxfmTpData();  

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY; 











/*

2.
9/15 actuall ben wants the new pto rates to take effect next pay period, 9/20
AS well, possibly AS updating everyones pto payrate, also effective beginnig of
current pay period, for this one to happen, kim needs to change the hourly rate
IN arkona, OR anyone with pto pay, will see the new higher rate on the vision
page, but will be paid BY the old rate IN arkona
*/