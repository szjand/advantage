/*
Starting yesterday 5/23/2021 Ryan Lene�s flat rate wage was 
increased by $1.24 from $24.72 to $25.96.
*/

SELECT *
FROM tpemployees
WHERE lastname = 'lene'
  AND firstname = 'ryan'
  
-- current reality  
SELECT a.teamname, a.teamkey, 
  b.census, b.budget, b.poolpercentage,
  d.lastname, d.firstname, d.techkey, 
  e.techteampercentage,
  e.techteampercentage * b.budget AS tech_rate
FROM tpTeams a
INNER JOIN tpteamvalues b on a.teamkey = b.teamkey
  AND b.thrudate > curdate()
INNER JOIN tpteamtechs c on a.teamkey = c.teamkey
  AND c.thrudate > curdate()  
INNER JOIN tptechs d on c.techkey = d.techkey  
INNER JOIN tptechvalues e on d.techkey = e.techkey
  AND e.thrudate > curdate()
WHERE a.departmentkey = 13 
  AND a.thrudate > curdate()  
	AND lastname = 'lene'
  
  
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @new_percentage double;
DECLARE @budget double;
DECLARE @team_key integer;
DECLARE @census integer;
DECLARE @hourly double;
DECLARE @tech_team_perc double;
@from_date = '05/23/2021';
@thru_date = '05/22/2021';
BEGIN TRANSACTION;
TRY  

-- team of 1, just UPDATE teamvalues, 
-- ryan lene
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 9');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, 25.96, 1, @from_date);  
  
  DELETE 
  FROM tpdata 
  WHERE departmentkey = 13
    AND teamname = 'team 9'
	AND thedate > @thru_date;
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData(); 

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    

