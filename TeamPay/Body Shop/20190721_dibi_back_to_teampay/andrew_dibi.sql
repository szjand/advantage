
I know that Kim Miller is gone this week and I wanted to make sure that you are 
aware that we want to flip Andrew Dibi back to flat rate starting the next pay 
period please. I did put the change in Compli, 
but I believe that you don�t see that.
Thank you!
Randy 

Please put him back at his flat rate of $17.75 please. 
His PTO rate can stay at $16.00 until we can get more data on him to 
determine when we should change it. 

select * 
FROM tpteams a
JOIN tpteamtechs b on a.teamkey = b.teamkey
JOIN tptechs c on b.techkey = c.techkey
WHERE teamname = 'team 17'

select * FROM tpteamvalues WHERE teamkey = 46

select * FROM tptechvalues WHERE techkey = 81

select * FROM employeeappauthorization WHERE username = 'adibi@rydellcars.com'

select * FROM tpemployees WHERE username = 'adibi@rydellcars.com'

-- current reality  
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 13
  AND a.teamkey IN (46)
ORDER BY teamname, firstname 
  
this one IS different, andrew use to be on team pay, THEN was taken off,
now returning

so, ALL the structure IS IN place, but with finite thru dates  


DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
DECLARE @census integer;
DECLARE @budget double;
DECLARE @pool_percentage double;
DECLARE @tech_team_percentage double;
DECLARE @previous_hourly_rate double;
DECLARE @team string;
DECLARE @employee_key integer;
DECLARE @employee_number string;
@from_date = '07/21/2019';
@thru_date = '07/20/2019';
--@eff_date = '07/07/2019';
--@thru_date = '07/06/2019';
@dept_key = 13;
@team_key = 46;
@tech_key = 81;
@census = 1;
@budget = 17.75;
@pool_percentage = 1;
@previous_hourly_rate = 16;
@tech_team_percentage = 1;
@team = 'Team 17';

--!!!!!!!!!!!!!!!! check the fucking dates !!!!!!!!!!!!!!!!!!!!!!!!
BEGIN TRANSACTION;
TRY
  -- tpTeams has a unique index on deptkey & name, so no new row, UPDATE the old one
--  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
--  values(@dept_key, @team, @from_date);
  UPDATE tpteams
  SET thrudate = '12/31/9999'
  WHERE teamkey = @team_key;
      
-- tp techs was NOT updated, so thru date IS alreade 12/31/9999    
/*   so don't need thsi
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.employeekey = @employee_key;  
*/    
/**/  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;  
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, @census, @budget, @pool_percentage, @from_date);  
  
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, @tech_team_percentage, @previous_hourly_rate);  
  
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @from_date
    AND teamkey = @team_key;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();    
/**/  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    