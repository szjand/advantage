-- SELECT * FROM tpData WHERE departmentKey = 13
--DELETE FROM tpData WHERE departmentKEy = 13;
--DELETE FROM tpData WHERE thedate >= '02/09/2014';
-- DELETE FROM tpData WHERE thedate = curdate()
--DELETE FROM tpData WHERE techkey IN (
--  SELECT techKey FROM tpTechs WHERE departmentKey = 13);
-- 2/19, DELETE back to 01/12, DO ALL (body AND main shop IN a single quer
--       separate section for new row only
/*
HAVING fucks own time getting data for both shops IN place
*/
DECLARE @date date;
@date = '01/12/2014';
--BEGIN TRANSACTION; -- new row   
--TRY
--  TRY 
  -- base values for a new row
--    INSERT INTO tpData(DateKey, TheDate, PayPeriodStart,PayPeriodEnd,PayPeriodSeq,
--      DayOfPayPeriod, DayName, 
--      DepartmentKey, TechKey, TechNumber, EmployeeNumber, FirstName, LastName)  
    SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      a.biweeklypayperiodsequence, dayinbiweeklypayperiod, a.DayName,
     (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop'),
      b.TechKey, b.technumber, b.employeenumber, b.firstname, b.lastname
    FROM dds.day a, tpTechs b 
    WHERE a.thedate BETWEEN @date AND curdate() - 1
-- bs    
      AND b.departmentKey = (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'bodyshop')
      AND NOT EXISTS (
        SELECT 1
        FROM tpdata
        WHERE thedate = a.thedate
          AND techkey = b.techkey)
      AND EXISTS (-- 12/5, techs gone, but still exist IN tpTechs, don't want them IN tpData
        SELECT 1
        FROM tpTeamTechs
        WHERE techkey = b.techkey
          AND a.thedate BETWEEN fromdate AND thrudate); 
    -- payPeriodSelectFormat for new row
    UPDATE tpdata
    SET payPeriodSelectFormat = z.ppsel
    FROM (
      SELECT payperiodseq, TRIM(pt1) + ' ' + TRIM(pt2) ppsel
        FROM (  
        SELECT distinct payperiodstart, payperiodend, payperiodseq, 
          (SELECT trim(monthname) + ' '
            + trim(CAST(dayofmonth AS sql_char)) collate ads_default_ci + ' - '
            FROM dds.day
            WHERE thedate = a.payperiodstart) AS pt1,
          (SELECT trim(monthname) + ' ' 
            + TRIM(CAST(dayofmonth as sql_char)) collate ads_default_ci + ', ' 
            + TRIM(CAST(theyear AS sql_char))
            FROM dds.day
            WHERE thedate = a.payperiodend) AS pt2
        FROM tpdata a) y) z
    WHERE tpdata.payperiodseq = z.payperiodseq;  
    -- teamname/key for new row
    UPDATE tpData
    SET TeamKey = x.teamkey,
        teamname = x.teamname
    FROM (    
      SELECT *
      FROM ( 
        SELECT thedate
        FROM tpData
        GROUP BY thedate) a
      INNER JOIN (  
        SELECT a.techkey, c.teamkey, c.teamname, b.fromdate, b.thrudate
        FROM tptechs a
        INNER JOIN tpteamtechs b on a.techkey = b.techkey
        INNER JOIN tpteams c on b.teamkey = c.teamkey) b on a.thedate BETWEEN b.fromdate AND b.thrudate) x
    WHERE tpData.TechKey = x.techkey
-- bs -- no need to UPDATE every fucking row IN tpData, just the ones that need to be
      AND tpData.teamKey IS NULL     
      AND tpData.theDate = x.theDate;     
       
    -- team census, budget for new row
    
/*
the bs problem, script assumes a new teamValues row for each payperiod, which
works for the shop, elr changes each pay period,
body shop does NOT
on a givendate, what IS the census AND budget for a team
thedate BETWEEN fromDate AND thruDate

AND again, only the NULL rows, no need to keep updating the whole table
*/      
--    UPDATE tpData 
--    SET TeamCensus = x.census,
--        teamBudget = x.Budget
--    FROM ( 
--      SELECT *
--      FROM tpTeamValues) x
--    WHERE tpData.payPeriodStart = x.fromDate
--      AND tpData.teamkey = x.teamkey;    
      
UPDATE tpData
  SET teamCensus = x.census,
      teamBudget = x.budget
  FROM tpTeamValues x
  WHERE (tpData.teamCensus IS NULL OR tpData.teamBudget IS NULL) 
    AND tpData.teamKey = x.teamKey
    AND tpData.theDate BETWEEN x.fromDate AND x.thruDate;    
      
        
    -- techteamPercentage: FROM tpTechValues for new row
-- bs, OK    
    UPDATE tpData
    SET TechTeamPercentage = x.TechTeamPercentage
    FROM (
      SELECT a.thedate, b.techkey, b.techTeamPercentage
      FROM tpData a
      INNER JOIN tpTechValues b on a.techKey = b.techKey
        AND a.thedate BETWEEN b.fromdate AND b.thrudate) x
    WHERE tpData.TechKey = x.TechKey
      AND tpData.thedate = x.theDate;   
      
    -- TechTFRRate for new row 
    UPDATE tpdata
    SET TechTFRRate = techteampercentage * teambudget;    
    -- TechHourlyRate for new row
    UPDATE tpdata
    SET techhourlyrate = (
      SELECT distinct previoushourlyrate
      FROM tptechvalues
      WHERE techkey = tpdata.techkey);      
     
  CATCH ALL 
    RAISE tpdata(1, 'New Row ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- techs
TRY
  TRY   
    -- ClockHoursDay    
    UPDATE tpdata
    SET TechClockhoursDay = x.clockhours,
        TechVacationHoursDay = x.Vacation,
        TechPTOHoursDay = x.PTO,
        TechHolidayHoursDay = x.Holiday
    FROM (    
      SELECT b.thedate, c.employeenumber, SUM(clockhours) AS clockhours,
        SUM(coalesce(VacationHours,0)) AS vacation,
        SUM(coalesce(PTOHours,0)) AS pto,
        SUM(coalesce(HolidayHours,0)) AS holiday
      FROM dds.edwClockHoursFact a
      INNER JOIN dds.day b on a.datekey = b.datekey
        AND b.thedate BETWEEN curdate() - 7 AND curdate()
      LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
      GROUP BY b.thedate, c.employeenumber) x 
    WHERE tpdata.thedate = x.thedate
      AND tpdata.employeenumber = x.employeenumber;
    -- ClockHoursPPTD
    UPDATE tpData
    SET TechClockHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday, SUM(b.techclockhoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey; 
    -- TechVacationPPTD  
    UPDATE tpData
    SET TechVacationHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay, SUM(b.TechVacationHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechVacationHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
    -- TechPTOHoursPPTD
    UPDATE tpData
    SET TechPTOHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay, SUM(b.TechPTOHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechPTOHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;  
    -- TechHolidayHoursPPTD
    UPDATE tpData
    SET TechHolidayHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay, SUM(b.TechHolidayHoursDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechHolidayHoursDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;      
-- flag hours  
    -- TechFlagHoursDay 
    UPDATE tpData  
    SET TechFlagHoursDay = x.flaghours
    FROM (
      SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
      FROM tpData a
      LEFT JOIN (
        SELECT d.thedate, ro, line, c.technumber, a.flaghours
        FROM dds.factRepairOrder a  
        INNER JOIN dds.dimTech c on a.techkey = c.techkey
        INNER JOIN dds.day d on a.flagdatekey = d.datekey
        WHERE flaghours <> 0
          AND a.storecode = 'ry1'
          AND a.flagdatekey IN (
            SELECT datekey
            FROM dds.day
            WHERE thedate BETWEEN curdate() -7 AND curdate())   
          AND c.technumber IN (
            SELECT technumber
            FROM tpTechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
      GROUP BY a.technumber, a.thedate) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.technumber = x.technumber;       
    -- TechFlagHoursPPTD  
    UPDATE tpData
    SET TechFlagHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday, SUM(b.techflaghoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
-- adjustments
    UPDATE tpData
    SET TechFlagHourAdjustmentsDay = x.Adj
    FROM (
      SELECT a.technumber, a.lastname, a.thedate, coalesce(Adj, 0) AS Adj
      FROM tpData a
      LEFT JOIN (  
        select pttech, ptdate, round(SUM(ptlhrs), 2) AS adj
        FROM dds.stgArkonaSDPXTIM a
        INNER JOIN tpTechs b on a.pttech = b.technumber
        GROUP BY pttech, ptdate) b on a.technumber = b.pttech AND a.thedate = b.ptdate) x    
    WHERE tpData.technumber = x.technumber
      AND tpData.thedate = x.thedate;
    -- tech flag time adjustments pptd 
    UPDATE tpData
    SET TechFlagHourAdjustmentsPPTD = x.pptd
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay, SUM(b.TechFlagHourAdjustmentsDay) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.TechFlagHourAdjustmentsDay) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;       
  CATCH ALL 
    RAISE tpdata(2, 'Techs ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- teams
TRY
  TRY  
    -- teamclockhoursDay
    UPDATE tpData 
    SET teamclockhoursday = x.teamclockhoursday
    FROM (
      SELECT thedate, teamkey, SUM(techclockhoursday) AS teamclockhoursday
      FROM tpdata
      GROUP BY thedate, teamkey) x 
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;  
    -- teamclockhours pptd
    UPDATE tpData
    SET teamclockhourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;       
    -- teamflaghoursday
    UPDATE tpData
    SET TeamFlagHoursDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(techflaghoursday) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;
    -- teamflaghour pptd  
    UPDATE tpData
    SET teamflaghourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;  
    -- TeamFlagHourAdjustmentsDay
    UPDATE tpData
    SET TeamFlagHourAdjustmentsDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(TechFlagHourAdjustmentsDay) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey; 
    -- TeamFlagHourAdjustmentsPPTD 
    UPDATE tpData
    SET TeamFlagHourAdjustmentsPPTD = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x 
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;   
    -- team prof day          
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhoursday = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhoursday, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay) AS TotalFlag, 
          teamclockhoursday
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay), 
          teamclockhoursday) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;      
    -- team prof pptd
    UPDATE tpdata
    SET teamprofpptd = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhourspptd = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhourspptd, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS TotalFlag, 
          teamclockhourspptd
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD), 
          teamclockhourspptd) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;        
  CATCH ALL 
    RAISE tpdata(3, 'Teams ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
  

         
