/*
Jon,
Attached is a new spreadsheet for the body shop, we are breaking the guys in to 
smaller teams and putting paint back as one team.  Arnie is listed on a team 
but he is not part of team pay.  This goes in to effect on Monday but does not 
have to display right away on Monday on vision, I will stop back Monday morning 
to discuss any questions you have or I can pop in any time this weekend 
just let me know.
Ben
*/
/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
ORDER BY firstname

SELECT a.teamkey, a.teamname, c.firstname, c.lastname, 
  d.techteampercentage, d.techteampercentage * aa.budget,
  aa.census, aa.budget, aa.poolpercentage
FROM tpteams a
INNER JOIN tpteamvalues aa on a.teamkey = aa.teamkey
  AND aa.fromdate = '03/06/2016'
INNER JOIN tpteamtechs b on a.teamkey = b.teamkey
  AND b.fromdate = '03/06/2016'
INNER JOIN tptechs c on b.techkey = c.techkey
  AND c.thrudate > curdate()  
INNER JOIN tptechvalues d on c.techkey = d.techkey
  AND d.fromdate = '03/06/2016'  
WHERE a.fromdate = '03/06/2016'
  AND a.departmentkey = 13
ORDER BY a.teamkey, c.firstname  

-- Paint
SELECT techkey FROM tptechs WHERE firstname = 'Peter' AND lastname = 'Jacobson' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Mavrik' AND lastname = 'Peterson' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Chris' AND lastname = 'Walden' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Robert' AND lastname = 'Mavity' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Kevin' AND lastname = 'Trosen' AND thrudate > curdate()
-- Team 1
SELECT techkey FROM tptechs WHERE firstname = 'David' AND lastname = 'Perry' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Scott' AND lastname = 'Sevigny' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Brandon' AND lastname = 'Lamont' AND thrudate > curdate()
-- Team 2
SELECT techkey FROM tptechs WHERE firstname = 'Terrance' AND lastname = 'Driscoll' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Aaron' AND lastname = 'Lindom' AND thrudate > curdate()
-- Team 3
SELECT techkey FROM tptechs WHERE firstname = 'Chad' AND lastname = 'Gardner' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Joshua' AND lastname = 'Walton' AND thrudate > curdate()
-- Team 4
SELECT techkey FROM tptechs WHERE firstname = 'Loren' AND lastname = 'Shereck' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Cory' AND lastname = 'Rose' AND thrudate > curdate()
-- team 5
SELECT techkey FROM tptechs WHERE firstname = 'Matthew' AND lastname = 'Ramirez' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Kristen' AND lastname = 'Swanson' AND thrudate > curdate()
-- Team 6
SELECT techkey FROM tptechs WHERE firstname = 'Ryan' AND lastname = 'Lene' AND thrudate > curdate()
SELECT techkey FROM tptechs WHERE firstname = 'Justin' AND lastname = 'Olson' AND thrudate > curdate()

*/
-- ALL new teams
DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr integer;
DECLARE @percentage double;
DECLARE @pto_rate double;
@eff_date = '03/06/2016';
@thru_date = '03/05/2016';
@dept_key = 13;
@elr = 60;
BEGIN TRANSACTION;
TRY
-- CLOSE existing teams
  UPDATE tpteams
  SET thrudate = @thru_date
  WHERE departmentkey = @dept_key
    AND thrudate > curdate();
-- CREATE new teams  
  INSERT INTO tpTeams(DepartmentKey, TeamName,fromdate)
  values(@dept_key, 'Paint Team', @eff_date);
  INSERT INTO tpTeams(DepartmentKey, TeamName,fromdate)
  values(@dept_key, 'Team 1', @eff_date);
  INSERT INTO tpTeams(DepartmentKey, TeamName,fromdate)
  values(@dept_key, 'Team 2', @eff_date);
  INSERT INTO tpTeams(DepartmentKey, TeamName,fromdate)
  values(@dept_key, 'Team 3', @eff_date);
  INSERT INTO tpTeams(DepartmentKey, TeamName,fromdate)
  values(@dept_key, 'Team 4', @eff_date);
  INSERT INTO tpTeams(DepartmentKey, TeamName,fromdate)
  values(@dept_key, 'Team 5', @eff_date);
  INSERT INTO tpTeams(DepartmentKey, TeamName,fromdate)
  values(@dept_key, 'Team 6', @eff_date);
-- no new techs, pete jacobson IS back on a team, his one pay period of being
-- off team pay was handled BY simply closing his row IN tpTeamTechs  
-- CLOSE ALL current tpTeamTechAssignments
  UPDATE tpTeamTechs
  SET thrudate = @thru_date
  WHERE teamkey IN (
    SELECT teamkey
    FROM tpteams
    WHERE departmentkey = @dept_key
      AND thrudate = @thru_date)
    AND thrudate > curdate();     
-- assign techs to new teams
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Paint Team');
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  SELECT @team_key, a.techkey, @eff_date
  FROM (
    SELECT techkey FROM tptechs WHERE firstname = 'Peter' AND lastname = 'Jacobson' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Mavrik' AND lastname = 'Peterson' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Chris' AND lastname = 'Walden' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Robert' AND lastname = 'Mavity' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Kevin' AND lastname = 'Trosen' AND thrudate > curdate()) a;  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 1');
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  SELECT @team_key, a.techkey, @eff_date
  FROM (
    SELECT techkey FROM tptechs WHERE firstname = 'David' AND lastname = 'Perry' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Scott' AND lastname = 'Sevigny' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Brandon' AND lastname = 'Lamont' AND thrudate > curdate()) a; 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 2');
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  SELECT @team_key, a.techkey, @eff_date
  FROM (
    SELECT techkey FROM tptechs WHERE firstname = 'Terrance' AND lastname = 'Driscoll' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Aaron' AND lastname = 'Lindom' AND thrudate > curdate()) a; 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 3');
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  SELECT @team_key, a.techkey, @eff_date
  FROM (
    SELECT techkey FROM tptechs WHERE firstname = 'Chad' AND lastname = 'Gardner' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Joshua' AND lastname = 'Walton' AND thrudate > curdate()) a;  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 4');
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  SELECT @team_key, a.techkey, @eff_date
  FROM (
    SELECT techkey FROM tptechs WHERE firstname = 'Loren' AND lastname = 'Shereck' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Cory' AND lastname = 'Rose' AND thrudate > curdate()) a; 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 5');
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  SELECT @team_key, a.techkey, @eff_date
  FROM (
    SELECT techkey FROM tptechs WHERE firstname = 'Matthew' AND lastname = 'Ramirez' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Kristen' AND lastname = 'Swanson' AND thrudate > curdate()) a;  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 6');
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  SELECT @team_key, a.techkey, @eff_date
  FROM (   
    SELECT techkey FROM tptechs WHERE firstname = 'Ryan' AND lastname = 'Lene' AND thrudate > curdate()
    union
    SELECT techkey FROM tptechs WHERE firstname = 'Justin' AND lastname = 'Olson' AND thrudate > curdate()) a;    
-- tpTeamValues
-- CLOSE out old teams
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey IN (
    SELECT teamkey 
    FROM tpTeams
    WHERE departmentkey = 13
      AND thrudate = @thru_date)
    AND thrudate > curdate();      
-- new values
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Paint Team');  
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key AND fromdate = @eff_date);
  @percentage = .32; ----------------------------------------------------------
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key, @census, @census * @elr * @percentage, @percentage, @eff_date);   
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 1');  
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key AND fromdate = @eff_date);
  @percentage = .32; ----------------------------------------------------------
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key, @census, @census * @elr * @percentage, @percentage, @eff_date);  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 2');  
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key AND fromdate = @eff_date);
  @percentage = .32; ----------------------------------------------------------
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key, @census, @census * @elr * @percentage, @percentage, @eff_date); 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 3');  
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key AND fromdate = @eff_date);
  @percentage = .35; ----------------------------------------------------------
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key, @census, @census * @elr * @percentage, @percentage, @eff_date); 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 4');  
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key AND fromdate = @eff_date);
  @percentage = .35; ----------------------------------------------------------
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key, @census, @census * @elr * @percentage, @percentage, @eff_date); 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 5');  
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key AND fromdate = @eff_date);
  @percentage = .35; ----------------------------------------------------------
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key, @census, @census * @elr * @percentage, @percentage, @eff_date); 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'Team 6');  
  @census = (SELECT COUNT(*) FROM tpteamtechs WHERE teamkey = @team_key AND fromdate = @eff_date);
  @percentage = .35; ----------------------------------------------------------
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromdate)
  values(@team_key, @census, @census * @elr * @percentage, @percentage, @eff_date); 
-- tpTechValues    
-- Paint
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Peter' AND lastname = 'Jacobson' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .24; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Mavrik' AND lastname = 'Peterson' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .175; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Chris' AND lastname = 'Walden' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .185; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Robert' AND lastname = 'Mavity' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .17; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Kevin' AND lastname = 'Trosen' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .15; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  -- Team 1
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'David' AND lastname = 'Perry' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .295; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Scott' AND lastname = 'Sevigny' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .315; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Brandon' AND lastname = 'Lamont' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .295; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  -- Team 2
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Terrance' AND lastname = 'Driscoll' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .66; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Aaron' AND lastname = 'Lindom' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .42; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  -- Team 3
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Chad' AND lastname = 'Gardner' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .47; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Joshua' AND lastname = 'Walton' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .49; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  -- Team 4
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Loren' AND lastname = 'Shereck' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .5418; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Cory' AND lastname = 'Rose' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .5418; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  -- team 5
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Matthew' AND lastname = 'Ramirez' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .371; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Kristen' AND lastname = 'Swanson' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .36; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  -- Team 6
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Ryan' AND lastname = 'Lene' AND thrudate > curdate());
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .5418; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);
  @tech_key = (SELECT techkey FROM tptechs WHERE firstname = 'Justin' AND lastname = 'Olson' AND thrudate > curdate()); 
  @pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = @tech_key AND thrudate > curdate());
  @percentage = .37; ------------------------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key 
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey, fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_Date, @percentage, @pto_rate);    

  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
               
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  