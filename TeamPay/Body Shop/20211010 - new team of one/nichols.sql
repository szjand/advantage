/*
If you could please change Nathan Nichols from hourly to flat rate at $20.50 effective 10/10/2021 please.

Thank you
John Gardner
*/

-- SELECT * FROM dds.edwEmployeeDim  WHERE lastname = 'nichols'  -- 160432
-- select * FROM tpemployees WHERE employeenumber = '160432'
-- select * FROM tpteams WHERE departmentkey = 13



DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
DECLARE @budget double;
DECLARE @previoushourlyrate double;
DECLARE @new_team string;

@from_date = '10/10/2021';
@dept_key = 13;
@budget = 20;
@previoushourlyrate = 20;
@new_team = 'Team 23';

BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, @new_team, @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @new_team);
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
--select *  
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'nichols'
    AND a.currentrow = true;
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'nichols');
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, @budget, 1, @from_date);
  
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, 1, @previoushourlyrate);
/*
  -- NOT needed, no prior data IN tpData for nichols
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND techkey = @tech_key
    AND thedate >= @from_date;
*/    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
  INSERT INTO tpEmployees
  SELECT 'nnichols@rydellcars.com', firstname, lastname, employeenumber,
    '!aseTr8D9', 'Fuck You', storecode, TRIM(firstname) + ' ' + lastname
  --SELECT *  
  FROM dds.edwEmployeeDim 
  WHERE lastname = 'nichols'
    AND currentrow = true;			
			
  INSERT INTO employeeappauthorization
  SELECT 'nnichols@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'crose@rydellcars.com'
    AND appname = 'teampay';     
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

