paint team
adams gets a RAISE FROM 13.25 to 14.25


SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND a.teamkey = 29
ORDER BY teamname, firstname  

current allocation IS only 92.97% of budget
SELECT a.*, b.lastname, c.*
-- SELECT SUM(techteampercentage)
FROM tpteamtechs a
LEFT JOIN tptechs b on a.techkey = b.techkey
LEFT JOIN tptechvalues c on a.techkey = c.techkey
  AND c.thrudate > curdate()
WHERE a.teamkey = 29
  AND a.thrudate > curdate()  
  
therefore, the only thing that needs to change IS adam techteampercentage to .1484

teamkey: 29
techkey: 72

thrudate 5/13/17
effective date 5/14/17

SELECT * FROM tpdata WHERE techkey = 72 AND thedate = curdate();

SELECT * FROM tptechvalues WHERE techkey = 72

UPDATE tptechvalues
SET thrudate = '05/13/2017'
WHERE techkey = 72
  AND thrudate > curdate();
  
INSERT INTO tptechvalues(techkey,fromdate,thrudate,techteampercentage,previoushourlyrate)
values(72, '05/14/2017', '12/31/9999', .1484, 21);  

UPDATE tpdata
SET techteampercentage = .1484,
    techtfrrate = 14.246
WHERE techkey = 72
  AND thedate = curdate();



