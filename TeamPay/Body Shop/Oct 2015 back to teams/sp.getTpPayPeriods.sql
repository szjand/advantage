alter PROCEDURE GetTpPayPeriods
   ( 
      department CICHAR ( 12 ),
      payPeriodDisplay CICHAR ( 100 ) OUTPUT,
      payPeriodIndicator Integer OUTPUT
   ) 
BEGIN 
/*
2-8-14: return 8 payperiods
2/22/14: change width of payPeriodDisplay
3/21/14: ADD pdq

6/2/14
  increase body shop to 10 periods (just LIKE main shop)
  
6/30/15
-- *b*
  body shop back on team pay, ALL techs on one team
  effective 6/28/15, ben wants to expose no history  
10/14/15
  *c*
     back on teams, limit bs to 10/18/15 -> 
  
EXECUTE PROCEDURE GetTpPayPeriods('bodyshop');
*/
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @fromPayPeriodSeq integer;
DECLARE @thruPayPeriodSeq integer;
DECLARE @department string;
@department = (SELECT department FROM __input);
@thrupayperiodseq = (
  SELECT distinct payperiodseq FROM tpData WHERE thedate = curdate());
@fromPayPeriodSeq = @thrupayperiodseq - 7;
IF @department = 'mainshop' THEN 
  INSERT INTO __output
  SELECT top 10 payperiodselectformat, payperiodseq - @thruPayPeriodSeq
  FROM (
    SELECT DISTINCT payperiodselectformat, payperiodseq
    FROM tpdata 
    WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq) x
  ORDER BY payperiodseq - @thruPayPeriodSeq DESC ;

ELSE IF @department = 'bodyshop' THEN 
  INSERT INTO __output
  -- *a*
  SELECT top 10 payperiodselectformat, payperiodseq - @thruPayPeriodSeq
  FROM (
    SELECT DISTINCT payperiodselectformat, payperiodseq
    FROM tpdata 
    WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq
  -- *c*
      AND payperiodseq >= (
        SELECT distinct payperiodseq
        FROM tpdata
        WHERE thedate = '10/04/2015')) x
  ORDER BY payperiodseq - @thruPayPeriodSeq DESC;
/*
ELSE IF @department = 'pdq' THEN
INSERT INTO __output
SELECT top 4 payperiodselectformat, payperiodseq - @thruPayPeriodSeq
FROM (
  SELECT DISTINCT payperiodselectformat, payperiodseq
  FROM pdqWriterPayrollData 
  WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq) x
ORDER BY payperiodseq - @thruPayPeriodSeq DESC ;
END;
*/
END;
END;


END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'GetTpPayPeriods', 
   'COMMENT', 
   '');

