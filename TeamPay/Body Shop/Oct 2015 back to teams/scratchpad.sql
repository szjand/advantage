employeeAppAuthorization

SELECT a.*
FROM tptechs a
INNER JOIN tpteamtechs b on a.techkey = b.techkey
  AND b.thrudate > curdate()
INNER JOIN tpteams c on b.teamkey = c.teamkey
  AND c.departmentkey = 13
ORDER BY firstname  


select a.firstname, a.lastname, a.employeenumber, b.username, b.password, 
  c.appname, c.approle, c.functionality
FROM tpdata a
LEFT JOIN tpemployees b on a.employeenumber = b.employeenumber
LEFT JOIN employeeAppAuthorization c on b.username = c.username
WHERE a.departmentkey = 13
  AND a.thedate = curdate()
  AND c.appname = 'teampay'
ORDER BY a.firstname, a.lastname, c.appname, c.approle  
  
  
SELECT * 
FROM employeeAppAuthorization  
WHERE username LIKE 'lsher%'
  
  
  
SELECT * 
FROM employeeAppAuthorization m
INNER JOIN ( 
  select distinct a.firstname, a.lastname, a.employeenumber, b.username
  FROM tpdata a
  LEFT JOIN tpemployees b on a.employeenumber = b.employeenumber
  LEFT JOIN employeeAppAuthorization c on b.username = c.username
  WHERE a.departmentkey = 13
    AND a.thedate = curdate()
    AND c.appname = 'teampay') n on m.username = n.username
WHERE appcode = 'tp'
ORDER BY m.username

SELECT *
FROM employeeappauthorization
WHERE appcode = 'tp'
  AND appdepartmentkey = 13
  AND approle <> 'manager'
ORDER BY username  
  
  
SELECT DISTINCT username
FROM employeeappauthorization
WHERE appcode = 'tp'
  AND approle = 'technician'
  AND appdepartmentkey = 13
  
-- these are the body shop techs (minus brian peterson - he's NOT on a team )
SELECT *
FROM tpemployees a
INNER JOIN tptechs b on a.employeenumber = b.employeenumber
INNER JOIN tpteamtechs c on b.techkey = c.techkey
  AND c.thrudate > curdate()
INNER JOIN tpteams d on c.teamkey = d.teamkey
  AND d.departmentkey = 13
  AND d.thrudate > curdate()   
ORDER BY a.firstname  
  

--ADD brandon to tpemployees  
@username = 'blamont@rydellcars.com';
@password = 'LaMjmv783n';  
INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
  membergroup, storecode, fullname)
SELECT @username, firstname, lastname, employeenumber, @password, 
  'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
FROM dds.edwEmployeeDim 
WHERE lastname = 'Lamont' AND firstname = 'Brandon'
  AND currentrow = true 
  AND active = 'active'; 
  
  
remove technician authorization  
DELETE 
-- SELECT *
FROM employeeAppAuthorization
WHERE appname = 'teampay'
  AND appdepartmentkey = 13
  AND approle = 'technician' 
  AND username <> 'bpeterson@rydellcars.com'


give teamlead auth to ALL bs techs that don't already have it
-- these are the techs that need teamlead aut
SELECT username
FROM tpemployees a
INNER JOIN tptechs b on a.employeenumber = b.employeenumber
INNER JOIN tpteamtechs c on b.techkey = c.techkey
  AND c.thrudate > curdate()
INNER JOIN tpteams d on c.teamkey = d.teamkey
  AND d.departmentkey = 13
  AND d.thrudate > curdate()  
WHERE NOT EXISTS (
  SELECT 1
  FROM employeeappauthorization
  WHERE username = a.username
    AND appcode = 'tp'
    AND appdepartmentkey = 13)   
ORDER BY a.firstname  


-- employeeAppAuthorization
INSERT INTO employeeAppAuthorization
SELECT userName, appName, appSeq, appCode, appRole, functionality, 
  @departmentKey
FROM tpEmployees a, applicationMetadata b
WHERE b.appcode = 'tp'
  AND b.appseq = 101
  AND b.approle = 'technician'
  AND a.username = 'rlene@rydellcars.com';   
    
SELECT username
FROM tpemployees a
INNER JOIN tptechs b on a.employeenumber = b.employeenumber
INNER JOIN tpteamtechs c on b.techkey = c.techkey
  AND c.thrudate > curdate()
INNER JOIN tpteams d on c.teamkey = d.teamkey
  AND d.departmentkey = 13
  AND d.thrudate > curdate()  
WHERE NOT EXISTS (
  SELECT 1
  FROM employeeappauthorization
  WHERE username = a.username
    AND appcode = 'tp'
    AND appdepartmentkey = 13)     

INSERT INTO employeeappAuthorization    
SELECT m.username, n.appname, n.appseq, n.appcode, n.approle, n.functionality, 
  n.appdepartmentkey
FROM (
  SELECT username
  FROM tpemployees a
  INNER JOIN tptechs b on a.employeenumber = b.employeenumber
  INNER JOIN tpteamtechs c on b.techkey = c.techkey
    AND c.thrudate > curdate()
  INNER JOIN tpteams d on c.teamkey = d.teamkey
    AND d.departmentkey = 13
    AND d.thrudate > curdate() 
  WHERE a.username <> 'bpeterson@rydellcars.com'
    AND NOT EXISTS (
      SELECT 1
      FROM employeeappauthorization
      WHERE username = a.username
        AND appcode = 'tp'
        AND appdepartmentkey = 13)) m,  
(
  SELECT appname, appseq, appcode, approle, functionality, appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'lshereck@rydellcars.com'
    AND appcode = 'tp'
    AND approle = 'teamlead') n     

-- brian peterson, leave him AS the only tech IN employeeAppAuthorization
    
INSERT INTO employeeAppAuthorization   
  SELECT 'bpeterson@rydellcars.com', appname, appseq, appcode, approle, functionality, appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'lshereck@rydellcars.com'
    AND appcode = 'tp'
    AND approle = 'teamlead'    
    
    
SELECT *
FROM employeeappauthorization
WHERE username = 'bpeterson@rydellcars.com'

    