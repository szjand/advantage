alter PROCEDURE todayXfmBsFlatRateData
   ( 
   ) 
BEGIN 
/*
today
  clockHoursDay
  clockHoursPPTD
  techPdrFlagHoursDay
  techPdrFlagHoursPPTD
  techMetalFlagHoursDay
  techMetalFlagHoursPPTD
  techProficiencyDay
  techProficiencyPPTD
  
5/16
  *a*
    new metal rate for brian (eg, multiple rows IN bsFlatRateTechs)
    flag hours  
5/19 
  *b*
  oh shit, forgot to limit to curdate()
  
10/14/15
  *c*
     back to teams, change flagdate to closedate 
    
  
EXECUTE PROCEDURE todayXfmBsFlatRateData();

*/
BEGIN TRANSACTION;
TRY 
  TRY -- clock hours
-- clock hours day
    UPDATE bsFlatRateData
    SET techClockHoursDay = x.clockHours
    FROM (       
      SELECT c.thedate, d.departmentkey, d.technumber, SUM(a.clockHours) AS clockHours
      FROM dds.factClockHoursToday a
      INNER JOIN dds.edwEmployeeDim b on  a.employeeKey = b.employeeKey
      INNER JOIN dds.day c on a.dateKey = c.dateKey  
-- *b*	  
	    AND c.thedate = curdate()
      INNER JOIN (
        SELECT DISTINCT departmentKey, technumber, employeenumber
        FROM bsFlatRateTechs) d on b.employeenumber = d.employeenumber 
      GROUP BY c.thedate, d.departmentkey, d.technumber) x 
    WHERE bsFlatRateData.thedate = x.thedate
      AND bsFlatRateData.departmentKey = x.departmentKey
      AND bsFlatRateData.techNumber = x.techNumber; 
    -- clock hours PPTD        
    UPDATE bsFlatRateData
    SET techClockHoursPPTD = x.reg
    FROM (
      SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
        a.techClockHoursDay, SUM(b.techClockHoursDay) AS reg
      FROM bsFlatRateData a, bsFlatRateData b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.departmentKey = b.departmentKey
        AND a.techNumber = b.techNumber
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
        a.techClockHoursDay) x
    WHERE bsFlatRateData.theDate = x.theDate
      AND bsFlatRateData.departmentKey = x.departmentKey
      AND bsFlatRateData.techNumber = x.techNumber;  
  CATCH ALL 
    RAISE todayXfmBsFlatRateData(1, 'Clock Hours ' + __errtext);
  END TRY;  
  
  TRY -- flag hours
    -- flag hours day
    UPDATE bsFlatRateData
    SET techPdrFlagHoursDay = x.techPdrFlagHours
    FROM (
      SELECT b.theDate, e.departmentKey, e.techNumber, 
        SUM(flagHours) AS techPdrFlagHours
      FROM dds.todayFactRepairOrder a
--*c*      
--      INNER JOIN dds.day b on a.flagDateKey = b.dateKey
      INNER JOIN dds.day b on a.closeDateKey = b.dateKey
      INNER JOIN dds.dimTech c on a.techKey = c.techKey
      INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
      INNER JOIN bsFlatRateTechs e on c.storeCode = e.storeCode
        AND c.techNumber = e.techNumber
-- *a*  
        AND b.theDate BETWEEN e.fromDate AND e.thruDate        
      WHERE d.opcode = 'PDR'
-- *b*
        AND b.theDate = curdate()	  
      GROUP BY b.theDate, e.departmentKey, e.techNumber) x
    WHERE bsFlatRateData.theDate = x.theDate
      AND bsFlatRateData.departmentKey = x.departmentKey
      AND bsFlatRateData.techNumber = x.techNumber;     
      
    UPDATE bsFlatRateData
    SET techMetalFlagHoursDay = x.techMetalFlagHours
    FROM (
      SELECT b.theDate, e.departmentKey, e.techNumber, 
        SUM(flagHours) AS techMetalFlagHours
      FROM dds.todayFactRepairOrder a
-- *c*      
--      INNER JOIN dds.day b on a.flagDateKey = b.dateKey
      INNER JOIN dds.day b on a.closeDateKey = b.dateKey
      INNER JOIN dds.dimTech c on a.techKey = c.techKey
      INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
      INNER JOIN bsFlatRateTechs e on c.storeCode = e.storeCode
        AND c.techNumber = e.techNumber
-- *a*  
        AND b.theDate BETWEEN e.fromDate AND e.thruDate          
      WHERE d.opcode <> 'PDR'
-- *b*
        AND b.theDate = curdate()		  
      GROUP BY b.theDate, e.departmentKey, e.techNumber) x
    WHERE bsFlatRateData.theDate = x.theDate
      AND bsFlatRateData.departmentKey = x.departmentKey
      AND bsFlatRateData.techNumber = x.techNumber;    
    
    -- flag hours PPTD
    UPDATE bsFlatRateData
    SET techPdrFlagHoursPPTD = x.pdr
    FROM (
      SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq,  
        a.techPdrFlagHoursDay, SUM(b.techPdrFlagHoursDay) AS pdr
      FROM bsFlatRateData a, bsFlatRateData b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.departmentKey = b.departmentKey
        AND a.techNumber = b.techNumber
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
        a.techPdrFlagHoursDay) x
    WHERE bsFlatRateData.theDate = x.theDate
      AND bsFlatRateData.departmentKey = x.departmentKey
      AND bsFlatRateData.techNumber = x.techNumber; 
    
    UPDATE bsFlatRateData
    SET techMetalFlagHoursPPTD = x.pdr
    FROM (
      SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq,  
        a.techMetalFlagHoursDay, SUM(b.techMetalFlagHoursDay) AS pdr
      FROM bsFlatRateData a, bsFlatRateData b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.departmentKey = b.departmentKey
        AND a.techNumber = b.techNumber
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
        a.techMetalFlagHoursDay) x
    WHERE bsFlatRateData.theDate = x.theDate
      AND bsFlatRateData.departmentKey = x.departmentKey
      AND bsFlatRateData.techNumber = x.techNumber;    
  CATCH ALL 
    RAISE todayXfmBsFlatRateData(2, 'Flag Hours ' + __errtext);
  END TRY; 
   
  TRY -- proficiency
    -- proficiency day
    UPDATE bsFlatRateData
    SET techProficiencyDay = x.prof
    FROM (
      SELECT thedate, departmentKey, techNumber, 
        CASE techClockHoursDay
          WHEN 0 THEN 0
          ELSE round(1.0 * (techPdrFlagHoursDay + 
      	  techMetalFlagHoursDay + techFlagHourAdjustmentsDay)/techClockHoursDay, 2) 
        END AS prof
      FROM bsFlatRateData
      WHERE theDate = curdate()) x   
    WHERE bsFlatRateData.theDate = x.theDate
      AND bsFlatRateData.departmentKey = x.departmentKey
      AND bsFlatRateData.techNumber = x.techNumber;  
      
    -- proficiency day
    UPDATE bsFlatRateData
    SET techProficiencyPPTD = x.prof
    FROM (
      SELECT thedate, departmentKey, techNumber, 
        CASE techClockHoursPPTD
          WHEN 0 THEN 0
          ELSE round(1.0 * (techPdrFlagHoursPPTD + 
    	    techMetalFlagHoursPPTD + 
    		techFlagHourAdjustmentsPPTD)/techClockHoursPPTD, 2) 
        END AS prof
      FROM bsFlatRateData
      WHERE theDate = curdate()) x   
    WHERE bsFlatRateData.theDate = x.theDate
      AND bsFlatRateData.departmentKey = x.departmentKey
      AND bsFlatRateData.techNumber = x.techNumber;     
  CATCH ALL 
    RAISE todayXfmBsFlatRateData(3, 'Proficiency ' + __errtext);
  END TRY; 
       
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  



END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'todayXfmBsFlatRateData', 
   'COMMENT', 
   '');

