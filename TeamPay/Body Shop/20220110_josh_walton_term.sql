/*
josh walton termed on 1/11/22

AS i determined IN the 11/11/19 script, it IS good enough to just UPDATE tpTeamTechs


*/
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND teamName = 'team 3'
ORDER BY teamname, firstname  

team key: 32
tech key: 34

DELETE 
FROM tpdata
WHERE techkey = 34
  and thedate >= '01/11/2022';
  
UPDATE tpTeamTechs
SET thrudate = '01/10/2022'
WHERE techkey = 34
  AND teamkey = 32;
	
UPDATE tptechs
SET thrudate = '01/10/2022'
WHERE techkey = 34;	
