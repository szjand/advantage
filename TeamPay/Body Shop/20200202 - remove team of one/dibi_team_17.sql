/*
Tim Reuter Team 24, taken off flat rate effective 2/13/22
*/

SELECT * FROM tpteams WHERE teamname = 'team 24'
teamkey = 61
 
UPDATE tpteams
SET thrudate = '02/12/2022'
WHERE teamkey = 61;

UPDATE tpteamtechs
SET thrudate = '02/12/2022'
-- SELECT * FROM tpteamtechs
WHERE teamkey = 61
  AND thrudate > curdate();
	
update tptechs
SET thrudate = '02/12/2022'
-- SELECT * FROM tptechs
WHERE lastname = 'reuter'
  AND thrudate > curdate();	

UPDATE tpteamvalues  
SET thrudate = '02/12/2022'
-- SELECT * FROM tpteamvalues
WHERE teamkey = 61  
  AND thrudate > curdate();
  
UPDATE tptechvalues
SET thrudate = '02/12/2022'
-- SELECT * FROM tptechvalues
WHERE techkey = 99  
  AND thrudate > curdate();
  
DELETE 
-- select *
FROM tpdata
WHERE techkey = 99
  AND thedate > '02/12/2022';
	
		
/*
Ryan Lene termed 8/13/21
*/

team of 1
teamname team 9
teamkey 38
techkey 56
dept 13
technumber 267
employeenumber 184920

SELECT * 
FROM tpteams
WHERE teamname = 'team 9';

SELECT * FROM tptechs WHERE lastname = 'lene';

SELECT MAX(thedate) FROM tpdata WHERE techkey = 56 AND techclockhoursday <> 0
 
UPDATE tpteams
SET thrudate = '08/13/2021'
WHERE teamname = 'Team 9';

update tptechs
SET thrudate = '08/13/2021'
-- SELECT * FROM tptechs
WHERE lastname = 'lene';

update tpteamtechs
SET thrudate = '08/13/2021'
-- SELECT * FROM tpteamtechs
WHERE teamkey = 38
  AND thrudate > curdate();

UPDATE tpteamvalues  
SET thrudate = '08/13/2021'
-- SELECT * FROM tpteamvalues
WHERE teamkey = 38  
  AND thrudate > curdate();
  
UPDATE tptechvalues
SET thrudate = '08/13/2021'
-- SELECT * FROM tptechvalues
WHERE techkey = 56  
  AND thrudate > curdate();
  
DELETE 
FROM tpdata
WHERE techkey = 56
  AND thedate > '08/13/2021';
	
/*
01/28/20
We are switching Andrew Dibi from flat rate of $17.75 to hourly $17.75 starting 2/3/2020.  I have already submitted the Compli form to Laura.

Thank you
John Gardner
*/

team of 1
teamname team 17
teamkey 46
techkey 81
dept 13
technumber 282
employeenumber 15648

SELECT * 
FROM tpteams
WHERE teamname = 'team 17';

UPDATE tpteams
SET thrudate = '02/01/2020'
WHERE teamname = 'Team 17';

update tptechs
SET thrudate = '02/01/2020'
-- SELECT * FROM tptechs
WHERE lastname = 'dibi';

update tpteamtechs
SET thrudate = '02/01/2020'
-- SELECT * FROM tpteamtechs
WHERE teamkey = 46
  AND thrudate > curdate();

UPDATE tpteamvalues  
SET thrudate = '02/01/2020'
-- SELECT * FROM tpteamvalues
WHERE teamkey = 46  
  AND thrudate > curdate();
  
UPDATE tptechvalues
SET thrudate = '02/01/2020'  
-- SELECT * FROM tptechvalues
WHERE techkey = 81  
  AND thrudate > curdate();
  
DELETE 
FROM tpdata
WHERE techkey = 81
  AND thedate > '02/01/2020';