Can you change Brandon Lamont from $16.99 to $18.00 per commission hour effective 2/19/17.


SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
AND teamname = 'team 1'

teamName	ELR	poolPercentage	teamCensus	teamBudget	EXPR	            techTeamPercentage	techTFRRate	teamKey	techkey
Team 1	  60	0.28317	        1	          16.99	      Brandon Lamont	  1	                  16.99	      30	    67

-- new row for team values budget -> 18, poolPercentage -> .3
SELECT *
FROM tpteamvalues
WHERE teamkey = 30

-- tech values DO NOT change, no change to techTeamPercentage OR previousHourlyRate
SELECT *
FROM tptechvalues
WHERE techkey = 67
 
SELECT * FROM tpdata where techkey = 67 AND thedate = curdate()

DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @new_percentage double;
DECLARE @team_budget integer;
DECLARE @team_key integer;
@from_date = '02/19/2017';
@thru_date = @from_date - 1;
@tech_key = 67;
@team_key = 30;
@team_budget = 18;
@new_percentage = .3;
--SELECT @thru_date FROM system.iota;
BEGIN TRANSACTION;
TRY 
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamKey = @team_key
    AND thrudate > curdate();
    
  INSERT INTO tpTeamValues (teamkey,census,Budget,poolPercentage,fromdate)
  values (@team_key, 1, @team_budget, @new_percentage, @from_date); 
  
  UPDATE tpdata
  SET techTFRRate = @team_budget,
      TeamBudget = @team_budget
  WHERE thedate between @from_date and curdate()
    AND techkey = @tech_key;
    
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  
  
