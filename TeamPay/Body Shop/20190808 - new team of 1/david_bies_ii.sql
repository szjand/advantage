/*
Good afternoon, I am emailing you today because we will be switching Dave Bies 
from hourly to a flat rate technician in the body shop, his technician 
number is 293.  He will be switching from $30.50 hourly to $26.00 flat rate.  
Please let me know if you need anything else.

Hourly/PTO: $30.50

Thank you
John Gardner
*/

-- SELECT * FROM dds.edwEmployeeDim  WHERE lastname = 'bies ii'  -- 157953
-- select * FROM ptoemployees WHERE employeenumber = '157953' -- dbies@rydellcars.com
-- select * FROM tpemployees WHERE employeenumber = '157953'
-- select * FROM tpteams WHERE departmentkey = 13



DECLARE @from_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
DECLARE @budget double;
DECLARE @previoushourlyrate double;
DECLARE @new_team string;

@from_date = '08/18/2019';
@dept_key = 13;
@budget = 26;
@previoushourlyrate = 30.50;
@new_team = 'Team 19';

BEGIN TRANSACTION;
TRY
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, @new_team, @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @new_team);
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
--select *  
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'bies ii'
    AND a.currentrow = true;
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'bies ii');
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, 1, @budget, 1, @from_date);
  
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, 1, @previoushourlyrate);

  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND techkey = @tech_key
    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
  INSERT INTO employeeappauthorization
  SELECT 'dbies@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'adibi@rydellcars.com'
    AND appname = 'teampay';     
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  