
Ben summary:
Oh and the changes were moving Rob from quick lane to brown lane and 
moving Kevin T from hourly to team pay on the quick lane team.

Ben 2/19:
This was supposed to go in to effect at the beginning of the current 
pay period.  I am not sure how to handle Pete yet, what I want to do 
for this pay period is calculate his wage as if there was no change to the brown team.

randy 2/8:
We need to move Kevin Trosen from an hourly Tech to the Quick Lane team and to 
a commission rate. I believe Kevin�s commission rate should be the same as 
Brandon Lamont at $15.12.  
We need to move Rob Mavity from the Quick Lane team to the Brown Team at the 
same commission rate that he is currently at. 
Then we need to move Pete Jacobson from the Brown Team to an Hourly Tech. 
Ben C. will get you Pete�s hourly rate when he determines what that rate will be. 

2/29
goes INTO effect effective 2/21/16 - retroactive to beginning of pay period

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
  AND teamname IN ('brown team','quick team')
ORDER BY teamname, firstname

DECLARE @eff_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @department_key integer;
DECLARE @pto_rate double;
DECLARE @user_name string;
@department_key = 13;
@eff_date = '02/21/2016';
-- @thru_date = '12/31/9999';
-- @tech_key = 66; -- kristen
-- @team_key = 25; -- quick team

BEGIN TRANSACTION;
TRY
-- quick team
-- 1 ADD tech, kevin trosen to tptechs
-- tptechs
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @department_Key, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @eff_Date
  -- SELECT *
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
      AND b.currentrow = true
    WHERE a.lastname = 'trosen'
      AND a.firstname = 'kevin'
      AND a.currentrow = true 
      AND a.active = 'active'
      AND a.currentrow = true; 
-- tpEmployees, ptoEmployees already exists      
-- employeeAppAuth - same AS kristen swansen
  INSERT INTO employeeAppAuthorization (username,appname,appseq,appcode,approle,
    functionality,appDepartmentKey)
  SELECT 'ktrosen@rydellcars.com', appname,appseq,appcode,approle,
    functionality,appDepartmentKey
  FROM employeeAppAuthorization
  WHERE username = 'kswanson@rydellcars.com'
    AND appcode = 'tp';  
-- tpTeamTechs  
-- ADD kevin trosen  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'quick team');
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'trosen' AND firstname = 'kevin');
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);
-- remove rob mavity FROM quick team
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'mavity');  
  UPDATE tpteamtechs
  SET thrudate = @eff_date - 1
  WHERE teamkey = @team_key
    AND techkey = @tech_key;
-- ADD rob to brown team
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'brown team');  
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@team_key, @tech_key, @eff_date);  
-- remove pete FROM brown team    
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'jacobson' AND firstname = 'peter');
  UPDATE tpTeamTechs
  SET thrudate = @eff_date - 1
  WHERE teamkey = @team_key
    AND techkey = @tech_key;
-- tpTechValues
-- kevin    
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'trosen' AND firstname = 'kevin');
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, .1902, 15.12);
-- rob  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'mavity');
  @pto_rate = (
    SELECT previoushourlyrate 
    FROM tptechvalues 
    WHERE techkey = @tech_key
      AND thruDate > curdate());
  UPDATE tpTechValues
  SET thrudate = @eff_date - 1
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@tech_key, @eff_date, .2, @pto_rate);  
  
--
  DELETE FROM tpData  
  WHERE departmentkey = @department_key
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  