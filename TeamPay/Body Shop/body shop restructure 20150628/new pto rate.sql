-- per ben use previous year commission pay/clockhours
SELECT c.lastname, c.firstname, c.employeenumber, a.technumber
FROM tptechs a
INNER JOIN tpteamtechs b on a.techkey = b.techkey
  AND b.thrudate > curdate()
LEFT JOIN dds.edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true  
WHERE departmentkey = 13
UNION 
SELECT b.lastname, b.firstname, b.employeenumber, a.technumber
FROM dds.dimtech a
LEFT JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE flagdeptcode = 'bs'
AND a.active = true
AND a.currentrow = true
AND a.technumber IN ('213','209')


select *
FROM tptechs a
WHERE a.departmentkey = 13
  AND a.thrudate > curdate()
  AND NOT EXISTS (
    SELECT 1
    FROM tpteamtechs
    WHERE techkey = a.techkey
      AND thrudate > curdate())
      
select lastname, previoushourlyrate
FROM tptechs a
LEFT JOIN tpteamtechs b on a.techkey = b.techkey
LEFT JOIN tptechvalues c on a.techkey = c.techkey
WHERE a.departmentkey = 13
  AND a.thrudate > curdate()   
  AND b.thrudate > curdate()
  AND c.thrudate > curdate()
ORDER BY lastname  
  
  
select a.firstname, a.lastname, previoushourlyrate, clockhours, commpay, newptorate
--select a.firstname, a.lastname, previoushourlyrate, newptorate
FROM (  
select firstname, lastname, previoushourlyrate 
  FROM tptechs a
  LEFT JOIN tpteamtechs b on a.techkey = b.techkey
  LEFT JOIN tptechvalues c on a.techkey = c.techkey
  WHERE a.departmentkey = 13
    AND a.thrudate > curdate()   
    AND b.thrudate > curdate()
    AND c.thrudate > curdate()) a
LEFT JOIN (  -- FROM Z:\E\Intranet SQL Scripts\TeamPay\Body Shop\ptoRate\new pto rate - retroactive.sql
  SELECT lastname, firstname, 
    SUM(clockHours) AS clockHours, SUM(commPay) AS commPay,
    round(SUM(commPay)/SUM(clockHours) , 2) AS newPtoRate
  -- INTO #newPtoRate  
  FROM (  
    select thedate, lastname, firstname, employeenumber,  
      techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
      techclockhourspptd AS ClockHours, 
      round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey
    WHERE thedate = a.payperiodend
      AND departmentKey = 13
    AND thedate BETWEEN '05/18/2014' AND '05/30/2015'
  --	AND thedate BETWEEN '05/18/2014' AND '05/30/2015'
    AND EXISTS (
      SELECT 1
      FROM tpTeamTechs
      WHERE techKey = a.techKey
        AND thruDate > curdate())) x
  GROUP BY lastname, firstname) b on a.lastname = b.lastname AND a.firstname = b.firstname   
UNION
-- pdr
SELECT j.lastname, j.firstname, 35.95,
  SUM(techClockHoursPPTD) AS clockHours, SUM(metalPay + pdrPay) AS commPay,
  round(SUM(metalPay + pdrPay)/SUM(techClockHoursPPTD), 2) AS newHourlyRate
-- SELECT *
FROM (-- clockhours
  SELECT theDate, techNumber, techClockHoursPPTD
  FROM bsFlatRateData
  WHERE thedate BETWEEN '05/18/2014' AND '05/30/2015'
    AND thedate = payperiodend) g
LEFT JOIN (-- metal pay
  SELECT techNumber, payPeriodEnd, round(SUM(techMetalFlagHoursDay * metalRate), 2) AS metalPay
  FROM (
    SELECT a.theDate, a.payPeriodEnd, a.techNumber, a.techMetalFlagHoursDay, b.metalRate
    FROM bsFlatRateData a
    LEFT JOIN bsFlatRateTechs b on a.technumber = b.technumber
      AND a.theDate BETWEEN b.fromDate AND b.thruDate
    WHERE thedate BETWEEN '05/18/2014' AND '05/30/2015') c 
  GROUP BY techNumber, payPeriodEnd) h on g.theDate = h.payPeriodEnd
LEFT JOIN (-- pdr pay
  SELECT techNumber, payPeriodEnd, round(SUM(techPdrFlagHoursDay * pdrRate), 2) AS pdrPay
  FROM (
    SELECT a.theDate, a.payPeriodEnd, a.techNumber, a.techPdrFlagHoursDay, b.pdrRate
    FROM bsFlatRateData a
    LEFT JOIN bsFlatRateTechs b on a.technumber = b.technumber
      AND a.theDate BETWEEN b.fromDate AND b.thruDate
    WHERE thedate BETWEEN '05/18/2014' AND '05/30/2015') c 
  GROUP BY techNumber, payPeriodEnd) i on g.theDate = i.payPeriodEnd
LEFT JOIN (
  SELECT DISTINCT lastname, firstname, otherRate
  FROM bsFlatRateTechs) j on 1 = 1  
GROUP BY j.lastname, j.firstname
