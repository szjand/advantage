/*
-- current reality
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
ORDER BY teamname, firstname;

lots of changes:
after being off team pay for a month, now they want to go back
changes: 
  1 team, 18 techs, includes brian peterson AND arnie iverson
  closed hours NOT flag hours
 
stored procs that have to change: 
  xfmTpData
  todayXfmTpData
  getTpPayPeriods  
  GetTpTechROsByDay
  GetTpTechRoTotals
  xfmTmpBen
  
*/
DECLARE @user string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
---- testing
  @effDate = '06/28/2015';  
--  @newELR = 92.48;
  @departmentKey = 13;
-- production
--  @effDate = '05/31/2015';  
--  @newELR = 92.84; 
--  @departmentKey = 18;     

BEGIN TRANSACTION;
TRY
/* this portion IS necessary for backfilling the existing payperiod, for testing */
DELETE FROM tpData WHERE thedate >= @effDate AND departmentkey = @departmentkey;
-- discont updating bsFlatRateData, deleting current data keeps the pdr section
-- FROM showing on the managers pagge
DELETE FROM bsFlatRateData WHERE thedate >= @effDate;
--DELETE FROM tpTeamValues WHERE fromDate = @effDate AND thruDate = '12/31/9999';
--UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '05/03/2015'; 
/**/
/*
production: assuming this IS being run on Sunday, day 1 of the new pay period,
  the first day of which IS @effDate
  overnight run of tpData generated a row IN tpData for @effDate with the old
  team configs, DELETE that row, it will be repopulated at the END of this script
*/  

  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;

TRY     
-- techs    
-- ADD techs new to team pay, arnie iverson, 
-- brian peterson IS already IN tpTechs, just NOT assigned to a team   
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @departmentKey, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
  -- SELECT *
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE a.lastname = 'iverson'
      AND a.firstname = 'arnold'
      AND a.currentrow = true 
      AND a.active = 'active'
      AND b.currentrow = true;  
      
  --"reactivate" cody johnson
  UPDATE tpTechs
  SET thrudate = '12/31/9999'
  WHERE technumber = '247';     
  
  -- some old CATCH up, closing out tptech rows for techs no longer employed
  -- kevin stafford, robert karpenko, robert johnson
  UPDATE tptechs
  SET thrudate = x.techkeyFromDate - 1
  FROM (
    SELECT a.techkey, a.technumber, a.firstname, a.lastname, b.techkeyfromdate
    FROM tpTechs a 
    INNER JOIN dds.dimtech b on a.technumber = b.technumber  
    WHERE a.technumber IN ('262','259','214')
      AND b.currentrow = true) x
  WHERE tptechs.technumber = x.technumber
    AND tptechs.thrudate > @effDate -1; 
CATCH ALL
  raise tptechs(1, __errtext);
END TRY;  

TRY  
  -- CLOSE out tpTechValues current rows
  UPDATE tpTechValues
  SET thrudate = @effDate - 1
  WHERE techkey IN (
    SELECT a.techkey
    FROM tpTechs a 
    where departmentkey = @departmentKey
    AND a.thrudate > curdate())
  AND thrudate > curdate();  
CATCH ALL
  raise tpTechValues(1, __errtext);
END TRY;       
 
TRY           
  -- CLOSE ALL bs teams
  -- ALL current bs teams
  -- so first CLOSE out referencing tables: tpTeamValues, tpTeamTechs
  -- maybe first techs to CLOSE out the tpTechValues rows
  -- get techkeys FROM tpteamtechs  
  UPDATE tpTeamValues
  SET thrudate = @effDate - 1
  -- SELECT * FROM tpTeamValues
  WHERE teamkey IN (
    SELECT teamkey
    FROM tpteams
    WHERE departmentkey = @departmentKey
      AND thrudate > @effDate - 1)
    AND thrudate > @effDate - 1;
    
  UPDATE tpTeamTechs
  SET thrudate = @effDate - 1
  -- SELECT * FROM tpTeamTechs
  WHERE teamkey IN (
    SELECT teamkey
    FROM tpteams
    WHERE departmentkey =  @departmentKey
      AND thrudate > @effDate - 1)
    AND thrudate > @effDate - 1;
    
  UPDATE tpTeams
  SET thruDate = @effDate - 1
  WHERE departmentkey = @departmentKey
    AND thrudate > @effDate - 1;   
CATCH ALL
  raise closing(1, __errtext);
END TRY; 

TRY   
  -- CREATE the new team
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(13,'The Shop', @effDate);
CATCH ALL
  raise newTeam(1, __errtext);
END TRY;

TRY 
-- assign techs
  @teamkey = (SELECT teamkey FROM tpTeams WHERE teamname = 'The Shop');
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'David' AND lastname = 'Perry' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Cory' AND lastname = 'Rose' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Cody' AND lastname = 'Johnson' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Scott' AND lastname = 'Sevigny' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Chad' AND lastname = 'Gardner' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Ryan' AND lastname = 'Lene' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Justin' AND lastname = 'Olson' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Loren' AND lastname = 'Shereck' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Joshua' AND lastname = 'Walton' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Peter' AND lastname = 'Jacobson' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Robert' AND lastname = 'Mavity' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Mavrik' AND lastname = 'Peterson' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Jeremy' AND lastname = 'Rosenau' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Chris' AND lastname = 'Walden' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Terrance' AND lastname = 'Driscoll' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Aaron' AND lastname = 'Lindom' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Matthew' AND lastname = 'Ramirez' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Arnold' AND lastname = 'Iverson' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Brian' AND lastname = 'Peterson' AND thrudate > @effDate - 1);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromdate)
  values(@teamkey, @techkey, @effdate);
CATCH ALL
  raise tpTechTeams(1, __errtext);
END TRY;

TRY 
  -- tpTeamValues
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
  values(@teamKey, 19, 399, .35, @effdate);
CATCH ALL
  raise tpTeamValues(2, __errtext);
END TRY;

TRY 
  -- tpTechValues
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'David' AND lastname = 'Perry' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0426, 21);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Cory' AND lastname = 'Rose' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0616, 30.24);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Cody' AND lastname = 'Johnson' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0483, 21.42);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Scott' AND lastname = 'Sevigny' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0497, 23.85);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Chad' AND lastname = 'Gardner' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate,.0497, 27.02);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Ryan' AND lastname = 'Lene' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0628, 34.59);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Justin' AND lastname = 'Olson' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate,.0444, 23.65);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Loren' AND lastname = 'Shereck' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0652, 33.66);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Joshua' AND lastname = 'Walton' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0497, 26.98);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Peter' AND lastname = 'Jacobson' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate,.0635, 37.35);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Robert' AND lastname = 'Mavity' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0493, 28.29);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Mavrik' AND lastname = 'Peterson' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0493, 29.01);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Jeremy' AND lastname = 'Rosenau' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0526, 32.32);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Chris' AND lastname = 'Walden' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0549, 31.34);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Terrance' AND lastname = 'Driscoll' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0585, 27.42);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Aaron' AND lastname = 'Lindom' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate,.036, 16.25);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Matthew' AND lastname = 'Ramirez' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0361, 16.22);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Arnold' AND lastname = 'Iverson' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0445, 21.5);
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = 'Brian' AND lastname = 'Peterson' AND thrudate > @effDate - 1);
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values(@techkey, @effdate, .0578, 33.99);
CATCH ALL
  raise tpTechValues(2, __errtext);
END TRY;

TRY  
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 101
    AND b.approle = 'teamlead'
    AND a.username IN ('lshereck@rydellcars.com', 'crose@rydellcars.com','pjacobson@rydellcars.com');
 
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 101
    AND b.approle = 'technician'
    AND a.username IN (  
      SELECT b.username
      FROM tptechs a
      LEFT JOIN tpemployees b on a.employeenumber = b.employeenumber
      WHERE a.departmentkey = @departmentKey
        AND a.thrudate > curdate()) 
    AND a.username NOT IN ('lshereck@rydellcars.com', 'crose@rydellcars.com','pjacobson@rydellcars.com');  
CATCH ALL
  RAISE employeeAppAuthorization(2, __errtext);
END TRY;    

TRY 
-- AND FINALLY, UPDATE tpdata  
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
  EXECUTE PROCEDURE xfmDeptTechCensus();
  EXECUTE PROCEDURE xfmTmpBen();
CATCH ALL
  RAISE xfmTpData(1, __errtext);
END TRY;  

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;


