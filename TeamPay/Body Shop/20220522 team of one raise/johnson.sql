/*
Ben Johnson was approved for a pay increase from $15.10 to $16 flat rate.

Thank you!
Ashley
Oh Im sorry! 

Can we do 5/22?
*/

SELECT *
FROM tpemployees
WHERE lastname = 'johnson'
  AND firstname = 'benjamin'
  
-- current reality  
SELECT a.teamname, a.teamkey, 
  b.census, b.budget, b.poolpercentage,
  d.lastname, d.firstname, d.techkey, 
  e.techteampercentage,
  e.techteampercentage * b.budget AS tech_rate
FROM tpTeams a
INNER JOIN tpteamvalues b on a.teamkey = b.teamkey
  AND b.thrudate > curdate()
INNER JOIN tpteamtechs c on a.teamkey = c.teamkey
  AND c.thrudate > curdate()  
INNER JOIN tptechs d on c.techkey = d.techkey  
INNER JOIN tptechvalues e on d.techkey = e.techkey
  AND e.thrudate > curdate()
WHERE a.departmentkey = 13 
  AND a.thrudate > curdate()  
	AND lastname = 'johnson'
  
/*
for this TRANSACTION to function IN zzTest, DELETE the ukg link
AND re-create it : 

EXECUTE PROCEDURE 
  sp_CreateLink ( 
     'ukg',
     '\\67.135.158.12:6363\advantage\ukg\ukg.add',
     TRUE,
     TRUE,
     TRUE,
     '',
     '');

SELECT * FROM ukg.employees			 
*/		 
		 
	 
  
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @new_percentage double;
DECLARE @budget double;
DECLARE @team_key integer;
DECLARE @census integer;
DECLARE @hourly double;
DECLARE @tech_team_perc double;
@from_date = '05/22/2022';
@thru_date = '05/21/2022';
BEGIN TRANSACTION;
TRY  

-- team of 1, just UPDATE teamvalues, 
-- Ben Johnson
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 21');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, 16, 1, @from_date);  
  
  DELETE 
  FROM tpdata 
  WHERE departmentkey = 13
    AND teamname = 'team 21'
	AND thedate > @thru_date;
  EXECUTE PROCEDURE xfmTpData();


COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    

