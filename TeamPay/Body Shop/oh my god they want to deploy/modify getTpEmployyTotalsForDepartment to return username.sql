alter PROCEDURE getTpEmployeeTotalsForDepartment
   ( 
      department CICHAR ( 12 ),
      payPeriodIndicator Integer,
      teamName CICHAR ( 52 ) OUTPUT,
      tech CICHAR ( 60 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay DOUBLE ( 15 ) OUTPUT,
      username cichar (50) output
   ) 
BEGIN 
/*
3/5
  ADD username AS an output
execute procedure getTPEmployeeTotalsForDepartment('bodyshop',0)
*/	
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @department integer;
@department = 
  CASE (SELECT department FROM __input)
    WHEN 'mainshop' THEN 18
	WHEN 'bodyshop' THEN 13
  END;
@curPayPeriodSeq = (
  SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (
    SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
      
  INSERT INTO __output
  SELECT top 1000 *
  FROM (
    SELECT teamname, trim(firstname) + ' '  + lastname AS tech, 
      (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd 
	    + TechFlagHourAdjustmentspptd) AS "Flag Hours",
      techclockhourspptd AS "clock hours", 
      teamprofpptd/100 AS "team prof", 
	    round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
      coalesce(round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2), 0) AS "Regular Pay",
      (SELECT username FROM tpEmployees WHERE firstname = a.firstname AND lastname = a.lastname)
    FROM tpdata a
    WHERE thedate = @date
	  AND departmentKey = @department) x
  ORDER BY teamname, tech;

END;



