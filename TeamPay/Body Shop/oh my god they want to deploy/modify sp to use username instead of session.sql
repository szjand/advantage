alter PROCEDURE GetTpTechRosAdjustmentsByDay
   ( 
--      SessionID CICHAR ( 50 ),
      userName CICHAR ( 50 ),
      payPeriodIndicator Integer,
      techName CICHAR ( 60 ) OUTPUT,
      adjustmentDate DATE OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      line Integer OUTPUT,
      type CICHAR ( 24 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpTechRosAdjustmentsByDay('jwesterhausen@rydellchev.com', -2);
12/19 removed minus FROM xtim
12/25/13
  adjustments FROM stgArkonaSDPXTIM only
3/5
  modify to username AS the only input param    
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @techName string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
--DECLARE @sessionID string;
--@sessionID = (SELECT sessionID FROM __input);
@UserName = (SELECT username FROM __input);
--  CASE 
--    WHEN (SELECT userName FROM __input) <> '' THEN
--      (SELECT userName FROM __input)
--    ELSE 
--      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
--  END;
@techName = (
  SELECT TRIM(firstName) + ' ' + lastName
  FROM tpEmployees
  WHERE username = @userName);   
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);   
INSERT INTO __output
  SELECT *
  FROM (  
    SELECT @techName, ptdate, ptro#, ptline, 'Adjust after Close', 
      round(SUM(ptlhrs), 2) AS flaghours
    FROM dds.stgarkonasdpxtim a
    INNER JOIN tpTechs b on a.pttech = b.technumber
    WHERE b.technumber = @technumber
      AND ptdate BETWEEN @fromDate AND @thruDate
    GROUP BY ptdate, ptro#, ptline) x
  WHERE flaghours <> 0;    



END;

alter PROCEDURE GetTpTechAdjustmentTotals
   ( 
--      SessionID CICHAR ( 50 ),
      userName CICHAR ( 50 ),
      payPeriodIndicator Integer,
      techName CICHAR ( 60 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpTechAdjustmentTotals('jwesterhausen@rydellchev.com', -2);
12/19 remove the - FROM xtim
12/15 adjustments FROM stgArkonaSDPXTIM only
3/5
  modify to username AS the only input param 

*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @techName string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
DECLARE @sessionID string;
--@sessionID = (SELECT sessionID FROM __input);
@UserName = (SELECT username FROM __input); 
--  CASE 
--    WHEN (SELECT userName FROM __input) <> '' THEN
--      (SELECT userName FROM __input)
--    ELSE 
--      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
--  END;
@techName = (
  SELECT TRIM(firstName) + ' ' + lastName
  FROM tpEmployees
  WHERE username = @userName);   
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);     
INSERT INTO __output
  SELECT TRIM(firstName) + ' ' + lastName, round(SUM(ptlhrs), 2) AS flaghours
  FROM dds.stgarkonasdpxtim a
  INNER JOIN tpTechs b on a.pttech = b.technumber
  WHERE b.technumber = @technumber
    AND ptdate BETWEEN @fromDate AND @thruDate
  GROUP BY TRIM(firstName) + ' ' + lastName;  



END;


alter PROCEDURE GetTpTechROsByDay
   ( 
      userName CICHAR ( 50 ),
      payPeriodIndicator Integer,
      techName CICHAR ( 60 ) OUTPUT,
      flagDate DATE OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpTechROsByDay('pjacobson@rydellchars.com', 0);
12/25/13
  modified to accomodate restructured factRepairOrder
1/2/14
  need to ADD todayFactRepairORder for real time updating of tech page
1/13/14
  GROUP BY ro, remove line  
2/10/14 *a*
  WHILE looking at previous payperiod returns ro FROM today
  eg 16144742 (2-10) shows for payperiod 1-26 -> 2/8  
3/5
  modify to username AS the only input param   
3/11/14 *c*
  holy shit, double counting flag hours WHEN exact same row EXISTS IN
  factRepairOrder AND todayFactRepairOrder
  the fucking issue seems to be unioning on flaghours AS a double sees the
  same values AS different, therefore returns the row twice AND summing the flag hours
  short term bandaid, round flag hours IN the base queries before unioning
*/

DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
--DECLARE @sessionID string;
--@sessionID = (SELECT sessionID FROM __input);
@UserName = (SELECT username FROM __input);
--  CASE 
--    WHEN (SELECT userName FROM __input) <> '' THEN
--      (SELECT userName FROM __input)
--    ELSE 
--      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
--  END;
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);  
INSERT INTO __output
SELECT top 1000 *
FROM (
  SELECT techname, flagdate, ro, round(SUM(flaghours), 2) AS flaghours
  FROM (
    SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
  	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
    FROM dds.factRepairOrder a  
    INNER JOIN dds.dimTech c on a.techkey = c.techkey
    INNER JOIN tpTechs cc on c.technumber = cc.technumber
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'
      AND a.flagdatekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN @fromDate AND @thruDate)
      AND c.technumber = @techNumber 
    UNION
    SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
  	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
    FROM dds.todayfactRepairOrder a  
    INNER JOIN dds.dimTech c on a.techkey = c.techkey
    INNER JOIN tpTechs cc on c.technumber = cc.technumber
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'  
-- *a*          
      AND a.flagdatekey in (
        select datekey
        from dds.day
        WHERE thedate BETWEEN @fromDate AND @thruDate)                
      AND c.technumber = @techNumber) y
  GROUP BY techname, flagdate, ro) x	
ORDER BY techName, flagDate, ro; 



END;


alter PROCEDURE GetTpTechRoTotals
   ( 
      userName CICHAR ( 50 ),
      payPeriodIndicator Integer,
      techName CICHAR ( 60 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetTpTechRoTotals('pjacobson@rydellcars.com',0);
12/25/13
  modified to accomodate restructured factRepairOrder
1/2/14
  need to ADD todayFactRepairORder for real time updating of tech page  
2/10/14 *a*
  WHILE looking at previous payperiod returns ro FROM today
  eg 16144742 (2-10) shows for payperiod 1-26 -> 2/8  
  once that was fixed, realized the total was also incorrect, same fix 
3/5
  modify to username AS the only input param      
  
3/11/14 *c*
  holy shit, double counting flag hours WHEN exact same row EXISTS IN
  factRepairOrder AND todayFactRepairOrder
  the fucking issue seems to be unioning on flaghours AS a double sees the
  same values AS different, therefore returns the row twice AND summing the flag hours
  short term bandaid, round flag hours IN the base queries before unioning  
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
--DECLARE @sessionID string;
--@sessionID = (SELECT sessionID FROM __input);
@UserName = (SELECT username FROM __input);
--  CASE 
--    WHEN (SELECT userName FROM __input) <> '' THEN
--      (SELECT userName FROM __input)
--    ELSE 
--      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
--  END;
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);  
INSERT INTO __output
SELECT techname, round(SUM(flaghours), 2)
FROM (
  SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
    FROM dds.factRepairOrder a  
    INNER JOIN dds.dimTech c on a.techkey = c.techkey
    INNER JOIN tpTechs cc on c.technumber = cc.technumber
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'
      AND a.flagdatekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN @fromDate AND @thruDate) 
      AND c.technumber = @techNumber 
  UNION  
  SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours
    FROM dds.todayfactRepairOrder a  
    INNER JOIN dds.dimTech c on a.techkey = c.techkey
    INNER JOIN tpTechs cc on c.technumber = cc.technumber
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'
-- *a*          
      AND a.flagdatekey in (
        select datekey
        from dds.day
        WHERE thedate BETWEEN @fromDate AND @thruDate)                
      AND c.technumber = @techNumber) y
GROUP BY techname; 




END;


