

-- current values
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName), lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
--  AND teamname = 'paint team'
ORDER BY teamname, lastname


DECLARE @eff_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @department_key integer;
DECLARE @pto_rate double;
DECLARE @census integer;
DECLARE @budget double;
DECLARE @pool_percentage double;
DECLARE @elr integer;
DECLARE @tech_team_percentage double;
DECLARE @new_rate double;
DECLARE @team string;
@department_key = 13;
@thru_date = '09/25/2021';
@eff_date = '09/26/2021';
@elr = 60;

BEGIN TRANSACTION;
TRY 
-- single member teams, strainght flat rate, the only thing that
-- needs to change IS tpTeamValues
-- cory rose
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 8');
	@new_rate = 26;
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT * FROM tpteams WHERE teamname = 'team 8')
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);   
 
	
-- scott sevigny
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 7');
	@new_rate = 24;
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 7')
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);		
	
-- david bies II
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 19'); ------------------------------------
	@new_rate = 26.50; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 19') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);			
	
-- kyle buchanan
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 19'); ------------------------------------
	@new_rate = 24; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 14') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);			
	
-- Terry Driscoll - $26.34
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 12'); ------------------------------------
	@new_rate = 26.34; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 12') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);			
	
-- Brandon Lamont - $20.50
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 20'); ------------------------------------
	@new_rate = 20.50; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 20') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);	
	
-- Codey Olson - $23.00
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 15'); ------------------------------------
	@new_rate = 23.00; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 15') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);		
	
-- Justin Olson - $23.00 (18.25)
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 6'); ------------------------------------
	@new_rate = 23.00;---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 6') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);	

-- Marcus Eberle - $19.50 (19)
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 16'); ------------------------------------
	@new_rate = 19.5; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 16') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);	


-- new team of 1, Brian peterson
-- brian IS already IN tpTechs: techkey = 48
-- next team IS 22
  @team = 'Team 22';
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'Peterson' AND firstname = 'Brian');
	
	INSERT INTO tpTeams(departmentkey, teamname, fromdate)
	values(@department_key, @team, @eff_date);
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = @team);
	
	INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
	values(@team_key, @tech_key, @eff_date);
	
	INSERT INTO tpTeamValues(teamkey,census,budget, poopercentage,fromdate)
	values(@team_key, 1, 20.30, 1, @eff_date);

  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
	values(@team_key, @eff_date, 1, 40.31);

  DELETE 
  -- select
  FROM employeeappauthorization
  WHERE username = 'bpeterson@rydellcars.com'
    AND appname = 'teampay';
  	
  INSERT INTO empaloyeeappauthorization	(username,appname,appseq,appcode,approle,functionality,appdepartmentkey)
  SELECT 'bpeterson@rydellcars.com', appname, appseq, appcode, approle, 
    functionality, appdepartmentkey
  FROM employeeappauthorization 
  WHERE username = 'crose@rydellcars.com' AND appname = 'teampay';
	
---- team 3
--Josh Walton � $24.25  (22.20)
--Chad Gardner - $24.00  (22.50)	







			
--SELECT * 
--FROM tpteams a
--LEFT JOIN tpteamvalues b on a.teamkey = b.teamkey
--WHERE departmentkey = 13
--ORDER BY a.teamname, b.fromdate
	
	
	
	
	
	
  DELETE FROM tpData  
  WHERE departmentkey = @department_key
    AND thedate >= @eff_date;
		
  EXECUTE PROCEDURE xfmTpData();
--  EXECUTE PROCEDURE todayxfmTpData();  
      
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  	

/*	

	
*/
	
	
	