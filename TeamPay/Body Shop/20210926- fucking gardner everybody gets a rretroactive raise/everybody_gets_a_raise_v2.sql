/*
As of 9/27/2021 every technician received a raise in the body shop.
Below are their new pay rates if you could please change it on your pay sheet and in the vision page.
--Cory Rose - $26.00 (24.76)
--Scott Sevigny - $24 (20.54)
--Dave Bies - $26.50 (26)
--Kyle Buchanan - $24.00 (20.51)
--Terry Driscoll - $26.34 (25.34)
--Brandon Lamont - $20.50 (20)
--Codey Olson - $23.00 (19)
--Justin Olson - $23.00 (18.25)
--Marcus Eberle - $19.50 (19)

---- new team of 1
---- this means the bsFlatRate tables are no longer required
--Brian Peterson - $20.30  We are switching Brian Peterson to one rate going forward
---- paint team
--Rob Mavity - $17.25 (16.83)
--Pat Adams - $16.25  (15.25)
--Chris Walden - $19.80  (19.33)
--Pete Jacobson - $25.04  (23.04)

---- team 3
Josh Walton $24.25 (22.20)
Chad Gardner $24.00 (22.50)

-- current values
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName), lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
--  AND teamname = 'paint team'
ORDER BY teamname, lastname
*/

DECLARE @eff_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @department_key integer;
DECLARE @pto_rate double;
DECLARE @census integer;
DECLARE @budget double;
DECLARE @pool_percentage double;
DECLARE @elr integer;
DECLARE @tech_team_percentage double;
DECLARE @new_rate double;
DECLARE @team string;
@department_key = 13;
@thru_date = '09/25/2021';
@eff_date = '09/26/2021';
@elr = 60;


BEGIN TRANSACTION;
TRY 
-- single member teams, strainght flat rate, the only thing that
-- needs to change IS tpTeamValues


-- cory rose
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 8');
	@new_rate = 26;
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 8')
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);   

-- scott sevigny
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 7');
	@new_rate = 24;
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 7')
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);		

-- david bies II
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 19'); ------------------------------------
	@new_rate = 26.50; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 19') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);			
	
-- kyle buchanan
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 14'); ------------------------------------
	@new_rate = 24; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 14') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);		

-- Terry Driscoll - $26.34
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 12'); ------------------------------------
	@new_rate = 26.34; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 12') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);			
	

-- Brandon Lamont - $20.50
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 20'); ------------------------------------
	@new_rate = 20.50; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 20') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);	
	

-- Codey Olson - $23.00
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 15'); ------------------------------------
	@new_rate = 23.00; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 15') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);				
	
-- Justin Olson - $23.00 (18.25)
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 6'); ------------------------------------
	@new_rate = 23.00;---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 6') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);	

-- Marcus Eberle - $19.50 (19)
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 16'); ------------------------------------
	@new_rate = 19.5; ---------------------------------------------------------------------------------------
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 16') -----
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);		

-- new team of 1, Brian peterson
-- brian IS already IN tpTechs: techkey = 48
-- next team IS 22
  @team = 'Team 22';
  @tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'Peterson' AND firstname = 'Brian');
	
	INSERT INTO tpTeams(departmentkey, teamname, fromdate)
	values(@department_key, @team, @eff_date);
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = @team);
	
	INSERT INTO tpTeamTechs(teamkey,techkey,fromdate)
	values(@team_key, @tech_key, @eff_date);
	
	INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
	values(@team_key, 1, 20.30, 1, @eff_date);

-- SELECT * FROM tpTechValues WHERE techKey = 48	
	UPDATE tpTechValues
	SET thrudate = @thru_date
	WHERE techKey = @tech_key
	  AND thruDate > curdate();
		
  INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
	values(@tech_key, @eff_date, 1, 40.31);

  DELETE 
  -- select
  FROM employeeappauthorization
  WHERE username = 'bpeterson@rydellcars.com'
    AND appname = 'teampay';
  	
  INSERT INTO employeeappauthorization	(username,appname,appseq,appcode,approle,functionality,appdepartmentkey)
  SELECT 'bpeterson@rydellcars.com', appname, appseq, appcode, approle, 
    functionality, appdepartmentkey
  FROM employeeappauthorization 
  WHERE username = 'crose@rydellcars.com' AND appname = 'teampay';
	
	
	
---- team 3
-- Josh Walton $24.25 (22.20)
-- Chad Gardner $24.00 (22.50)
/*
elr * census *  poolpercentage = budget
budget * techTeamPerc = tech rate
budget needs to accomodate the SUM of the tech rates
so, lets make the budget 50
now poolPerc = budget/elr * census = 50/(60 * 2) = 0.4167
walton techTeamPerc = 24.25/50 = 0.485
gardner techTeamPerc = 24/50 = .48
*/

-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'team 3')
	 @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 3');
	 UPDATE  tpTeamValues
	 SET thruDate = @thru_date
	 WHERE teamKey = @team_key
	   AND thrudate > curdate();
	INSERT INTO tpTeamValues(teamKey,census,budget,poolPercentage,fromDate)
	values(@team_key, 2, 50, 0.4167, @eff_date);

	-- walton  34
	@tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'walton' AND firstname = 'joshua');
	@pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
	-- SELECT * FROM tpTechValues WHERE techkey = 34
	UPDATE tpTechValues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
	INSERT INTO tpTechValues(techKey,fromDate,TechTeamPercentage,previousHourlyRate)
	values(@tech_key, @eff_date, 0.485, @pto_rate);
	-- gardner  30
	@tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'gardner' AND firstname = 'chad');
	@pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
	UPDATE tpTechValues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
	INSERT INTO tpTechValues(techKey,fromDate,TechTeamPercentage,previousHourlyRate)
	values(@tech_key, @eff_date, 0.48, @pto_rate);	

	
---- paint team
--Rob Mavity - $17.25 (16.83)
--Pat Adams - $16.25  (15.25)
--Chris Walden - $19.80  (19.33)
--Pete Jacobson - $25.04  (23.04)	
/*	
total of new tech rates IS 78.34	
SELECT 17.25 + 16.25 + 19.8 + 25.04 FROM system.iota;	
lets make the budget 90
new poolPerc	= 90/(60 * 4) = 0.375
mavity techTeamPerc = 17.25/90 = 0.1917
adam techTeamPerc = 16.25/90 = 0.18056
walden techTeamPerc = 19.80/90 = 0.22
jacobson techTeamPerc = 25.04/90 = 0.2782
*/
-- SELECT * FROM tpTeamValues WHERE teamkey = (SELECT teamkey FROM tpteams WHERE teamname = 'paint team')	
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'paint team');
  UPDATE  tpTeamValues
  SET thruDate = @thru_date
  WHERE teamKey = @team_key
    AND thrudate > curdate();
	INSERT INTO tpTeamValues(teamKey,census,budget,poolPercentage,fromDate)
	values(@team_key, 4, 90, 0.375, @eff_date);	
	
	-- mavity  .1917 too hi, .19165->17.24485, .19167
	@tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'mavity' AND firstname = 'robert');
	@pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
	-- SELECT * FROM tpTechValues WHERE techkey = 45
	UPDATE tpTechValues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
	INSERT INTO tpTechValues(techKey,fromDate,TechTeamPercentage,previousHourlyRate)
	values(@tech_key, @eff_date, 0.19167, @pto_rate);	
	
	-- adam
	@tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'adam' AND firstname = 'patrick');
	@pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
	UPDATE tpTechValues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
	INSERT INTO tpTechValues(techKey,fromDate,TechTeamPercentage,previousHourlyRate)
	values(@tech_key, @eff_date, 0.18056, @pto_rate);		
	
	-- walden
	@tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'walden' AND firstname = 'chris');
	@pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
	UPDATE tpTechValues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
	INSERT INTO tpTechValues(techKey,fromDate,TechTeamPercentage,previousHourlyRate)
	values(@tech_key, @eff_date, 0.22, @pto_rate);			
	
	-- jacobson  .2782 too low, .2783 -> 25.047, .27822
	@tech_key = (SELECT techkey FROM tpTechs WHERE lastname = 'jacobson' AND firstname = 'peter');
	@pto_rate = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
	UPDATE tpTechValues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
	INSERT INTO tpTechValues(techKey,fromDate,TechTeamPercentage,previousHourlyRate)
	values(@tech_key, @eff_date, 0.27822, @pto_rate);		
	
	
  DELETE FROM tpData  
  WHERE departmentkey = @department_key
    AND thedate >= @eff_date;
		
  EXECUTE PROCEDURE xfmTpData();
      
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  			