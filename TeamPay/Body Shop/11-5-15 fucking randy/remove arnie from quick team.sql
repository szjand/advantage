-- current values
SELECT a.teamKey, a.teamName, b.census, b.budget, b.poolPercentage, d.firstName, d.lastName, 
  d.techKey, e.techTeamPercentage, budget * techTeamPercentage AS rate, 
  (budget * techTeamPercentage)/96 AS "New %"
FROM tpTeams a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thrudate > curdate()
LEFT JOIN tpTeamTechs c on a.teamKey = c.teamKey
LEFT JOIN tpTechs d on c.techKey = d.techKey  
LEFT JOIN tpTechValues e on d.techKey = e.techKey
  AND e.thruDate > curdate()
WHERE a.thrudate > curdate()
  AND a.teamname = 'quick team'
  
SELECT username, password
FROM tpemployees
WHERE lastname IN ('driscoll','mavity','lindom','ramirez','iverson')  
  
11/05/15  
Randy just realized that arnie should never have been on the quick team
but should have been houry since 10/18
so, remove him FROM quick team effective, shit
should technically be AS of 10/18
leaning toward just doing since 11/1
DO i really want to get INTO rewriting/preserving prior pay period history
why, lets give it a shot

IF i can DO it (on test) with out altering history, ok

DO it IN 2 steps
1. 10/18 -> curdate  remove arnie & kristen
2. 11/01 -> curdate  ADD kristen

11/9 no fucking way, removing arnie FROM the 10/18 - 10/31 pay period
would change the team proficiency
so fuck it
just remove him FROM current (11/1 -> 11/14) pay period

use zztest connection to \\67.135.158.12:6363\DailyCut\scoTest\sunday\copy\sco.ADD

1. remove arnie 
SELECT * FROM tptechs WHERE lastname = 'iverson'

DECLARE @from_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @budget double;
DECLARE @rate double;
@from_date = '11/01/2015';
@thru_date = '12/31/9999';
@tech_key = 65; -- arnie
@team_key = 25; -- quick team


BEGIN TRANSACTION;
TRY 
/**/
  DELETE 
  FROM tpTechValues
  WHERE techkey = @tech_key;

  DELETE 
  FROM tpTeamTechs
  WHERE techkey = @tech_key;
  
  DELETE 
  FROM tpTechs
  WHERE techkey = @tech_key;  
  
  UPDATE tpteamvalues
  SET census = 5,
      budget = 96
  WHERE teamkey = @team_key
    AND fromdate = @from_date;
/**/    
/* 
  current elr = 60
  new budget = 4 * 60 * .32 = 76.8
  rate/budget = techTeamPercentage
  see results FROM current values query above    
*/               
  @tech_key = 42; -- driscoll
  UPDATE tptechvalues
  SET techTeamPercentage = .264
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
    
  @tech_key = 45; -- mavity
  UPDATE tptechvalues
  SET techTeamPercentage = .222
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;  
    
  @tech_key = 52; -- lindom
  UPDATE tptechvalues
  SET techTeamPercentage = .162
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
    
  @tech_key = 57; -- ramirez
  UPDATE tptechvalues
  SET techTeamPercentage = .162
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;
    
  @tech_key = 68; -- swanson
  UPDATE tptechvalues
  SET techTeamPercentage = .144
  WHERE techkey = @tech_key
    AND thrudate = @thru_date;        
/**/ 

  DELETE 
  FROM tpdata
  WHERE teamkey = @team_key
    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();   
/**/
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   
 
  
  