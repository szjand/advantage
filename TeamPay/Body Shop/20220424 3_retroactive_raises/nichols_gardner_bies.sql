/*
4/25/22
I literally just found out like two moments ago that when I give a rate increase 
to a commissioned employee I need to alert you
I have approved increases for Nathan Nichols (New rate $23) 
Chad Gardner (new rate $28) and Dave Bies (new rate $28) going back 3/14, 3/23, and 3/23
Can you please help me get these on this payroll period?
I only found out when I messaged Kim Miller and asked why they werent showing on 
UKG AND she explained to me that you DO them
Ashley

pay periods:
		02/27 -> 03/12
		03/13 -> 03/26
		03/26 -> 04/09
		04/10 -> 04/23
		04/24 -> 05/07
		
		
nichols: 20.50 -> 23  3/13  team 23  teamkey 59  techkey 97
gardner:    24 -> 28  2/27  team 3   teamkey 32  techkey 30
bies:     26.5 -> 28  3/13  team 19  teamkey 49  techkey 85


SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, techTeamPercentage, 
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate() 
  AND departmentKey = 13
  AND a.teamkey IN (59,32,49)
ORDER BY teamname, firstname  


*/


DECLARE @eff_date date;
DECLARE @tech_key integer;
DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @department_key integer;
DECLARE @pto_rate double;
DECLARE @census integer;
DECLARE @budget double;
DECLARE @pool_percentage double;
DECLARE @elr integer;
DECLARE @tech_team_percentage double;
DECLARE @new_rate double;
DECLARE @team string;
@department_key = 13;
@thru_date = '04/23/2022';
@eff_date = '04/24/2022';
@elr = 60;


BEGIN TRANSACTION;
TRY 
-- single member teams, straight flat rate, the only thing that
-- needs to change IS tpTeamValues IF techTeamPercentage = 1

-- nichols
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 23');
	@new_rate = 23;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);   

-- gardner
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 3');
	@tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gardner');
	@pto_rate = (SELECT previoushourlyrate FROM tptechvalues WHERE techkey = 30 AND thrudate > curdate());
	-- SELECT * FROM techvalues WHERE teamkey = 32
	-- SELECT * FROM tptechvalues WHERE techkey = 30 AND thrudate > curdate()
	UPDATE tptechvalues
	SET thrudate = @thru_date
	WHERE techkey = @tech_key
	  AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey,fromdate,techteampercentage,previoushourlyrate)
	values(@tech_key,@eff_date,1,@pto_rate);
	@new_rate = 28;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);   

-- bies
	@team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 19');
	-- SELECT * FROM tpteamvalues WHERE teamkey = 49
	@new_rate = 28;
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, @new_rate, 1, @eff_date);   
	
	
	DELETE 
  FROM tpdata
  WHERE teamkey IN (59,32,49)
    AND thedate > @thru_date;
  
  EXECUTE PROCEDURE xfmTpData();

       
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    	

SELECT * FROM tpteams WHERE teamkey IN (59,32,49)
SELECT * FROM tpteamtechs WHERE teamkey = 32
back pay:
		 10/24 - 11/06  107.12 comm hrs  1.9 bonus hrs 
		 90hrs * 119.02% = 107.118 comm hrs * 23.27 = 2492.63586
		                                    * 24.2  = 2592.2556  reg time diff: 99.62
		 1.6hrs * 119.02% = 1.90432 bonus hrs * 46.54 = 88.627
		 1.6hrs * 119.02% = 1.90432 bonus hrs * 48.40 = 92.1691  bonus diff: 3.54
		                                                         total diff: 103.16

nichols back pay
		03/13 -> 03/26
		03/26 -> 04/09
		04/10 -> 04/23
  	
SELECT * FROM tpdata WHERE thedate = '03/26/2022' AND lastname = 'nichols'
			 flag_hours: 141.50
			 clock_hours  90.09
			 3254.60 - 2900.84 = 353.76
SELECT * FROM tpdata WHERE thedate = '04/09/2022' AND lastname = 'nichols'
			 flag_hours: 124.65
			 clock_hours  81.69
			 2866.97 - 2555.34 = 311.63
SELECT * FROM tpdata WHERE thedate = '04/23/2022' AND lastname = 'nichols'
			 flag_hours: 153.05
			 clock_hours  88.70
			 3520.19 - 3520.19 = 0		
			 
gardner back pay
	  02/27 -> 03/12
		03/13 -> 03/26
		03/27 -> 04/09
		04/10 -> 04/23
		
bies back pay
		03/13 -> 03/26
		03/27 -> 04/09
		04/10 -> 04/23
		
SELECT * FROM tpdata WHERE thedate = '04/23/2022' AND lastname = 'bies ii'  
/*
SELECT * into #nichols FROM tpdata WHERE thedate = curdate() - 1 AND teamkey = 59 AND departmentkey = 13

SELECT * FROM #nichols
UNION all
SELECT *  FROM tpdata WHERE thedate = curdate() - 1 AND teamkey = 59 AND departmentkey = 13


SELECT * into #gardner FROM tpdata WHERE thedate = curdate() - 1 AND teamkey = 32 AND departmentkey = 13

SELECT * FROM #gardner
UNION all
SELECT *  FROM tpdata WHERE thedate = curdate() - 1 AND teamkey = 32 AND departmentkey = 13


DELETE FROM tpdata WHERE thedate < '01/01/2022'

SELECT * FROM ukg.employees

*/

back pay math IN postgresql
do $$
declare
	clock numeric := 68.81;
	flag numeric := 103.2;
	new_rate numeric := 28.00;
	old_rate numeric := 24.00;
begin
drop table if exists wtf;
create temp table wtf as	
select (select round(new_rate * clock * round(flag/clock, 4), 2)) as new, 
  (select round(old_rate * clock * round(flag/clock, 4), 2)) as old,
	(select round(new_rate * clock * round(flag/clock, 4), 2))
	-
	(select round(old_rate * clock * round(flag/clock, 4), 2)) as diff;
end $$;

select * from wtf;



DELETE FROM tpdata WHERE thedate < '01/01/2022';

EXECUTE PROCEDURE 
  sp_CreateLink ( 
     'ukg',
     '\\67.135.158.12:6363\advantage\ukg\ukg.add',
     TRUE,
     TRUE,
     TRUE,
     '',
     ''/* YOUR PASSWORD GOES HERE */ );
		 
SELECT * FROM ukg.employees		 