/*
4/13/18

Alright I will look into the spread sheet that Andrew uses and see if we can implement something of that sort.  
For these current pay raises,  
Pat Adams is moving from $14.25 to $15.25.  
Chad Gardner is moving from $20.74 to $21.75.  
Justin Olson is moving from $17.22 to $18.25 
and Cody Olson is moving from $15.41 to $16.25.  
These have all been approved by Jeri and Ben and Kim has updated her stuff on these technicians.

To go INTO effect 4/1/18


select * FROM tpshopvalues WHERE departmentkey = 13
elr: 60
-- current reality
DROP TABLE #new;
SELECT a.teamname, a.teamkey, 
  b.census, b.budget, b.poolpercentage,
  d.lastname, d.firstname, d.techkey, 
  e.techteampercentage,
  e.techteampercentage * b.budget AS tech_rate
INTO #new
FROM tpTeams a
INNER JOIN tpteamvalues b on a.teamkey = b.teamkey
  AND b.thrudate > curdate()
INNER JOIN tpteamtechs c on a.teamkey = c.teamkey
  AND c.thrudate > curdate()  
INNER JOIN tptechs d on c.techkey = d.techkey  
INNER JOIN tptechvalues e on d.techkey = e.techkey
  AND e.thrudate > curdate()
WHERE a.departmentkey = 13 
  AND a.thrudate > curdate()
  
SELECT * FROM #current
  
SELECT SUM(tech_rate)
FROM #current  
WHERE teamkey = 29

-- paint team 
-- adam's raise of $1, does NOT cause paint team to exceed budget

team budget IS 96, increasing patrick FROM 14.25 to 15.25 does NOT exceed that
only thing that needs to be changed IS tpTechValues.TechTeamPercentage 
new %: rate/budget: 15.25/96 = .15885

UPDATE tpTechValues
SET thrudate = '09/16/2017'
WHERE techkey = 30 
  AND thrudate > curdate();
  
INSERT INTO tpTechValues(techkey,fromdate,techTeamPercentage,previousHourlyRate)
values(30, '09/17/2017', .4938, 30.59);

select * FROM tptechvalues WHERE techkey = 72
SELECT * FROM tpteams
SELECT * FROM #current
*/

DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @new_percentage double;
DECLARE @budget double;
DECLARE @team_key integer;
DECLARE @census integer;
DECLARE @hourly double;
DECLARE @tech_team_perc double;
@from_date = '04/01/2018';
@thru_date = '03/31/2018';
BEGIN TRANSACTION;
TRY 
-- patrick adam: only need to UPDATE tpTechValues

  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'adam');
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'paint team' AND thrudate > curdate());
  @hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
  @new_percentage = .15885;
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues(techkey,fromdate,techTeamPercentage,previousHourlyRate)
  values(@tech_key, @from_date, @new_percentage, @hourly);
  
-- chad gardner: hmmm, his RAISE will put team 3 over budget BY .33, let's see what happens 
 
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'gardner');
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 3' AND thrudate > curdate());
  @hourly = (SELECT previousHourlyRate FROM tpTechValues WHERE techkey = @tech_key AND thrudate > curdate());
  @new_percentage = .51786;
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tpTechValues(techkey,fromdate,techTeamPercentage,previousHourlyRate)
  values(@tech_key, @from_date, @new_percentage, @hourly);

-- justin olson   
-- team of 1, just UPDATE teamvalues, 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 6');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, 18.25, 1, @from_date);  
-- codey olson  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = 'team 15');
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, 1, 16.25, 1, @from_date);  
  
  DELETE 
  FROM tpdata 
  WHERE departmentkey = 13
    AND teamname IN ('paint team','team 15','team 3','team 6')
	AND thedate > @thru_date;
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData(); 

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

-- SELECT * FROM #new

