/*
Can you please put Michael Mogavero on the paint team starting 3/2/2020 please.
We will put Michael at $11.50 flat rate and his PTO rate will be $17.00 an hour please.  

Thank you
John Gardner


SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND a.teamkey = 29
ORDER BY teamname, firstname  

*/

-- SELECT * FROM dds.edwEmployeeDim  WHERE lastname = 'mogavero'  -- 174384
-- select * FROM ptoemployees WHERE employeenumber = '174384' -- mmogavero@rydellcars.com
-- select * FROM tpemployees WHERE employeenumber = '157953'
-- select * FROM tpteams WHERE departmentkey = 13 AND thrudate > curdate() -- Paint Team



DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
DECLARE @budget double;
DECLARE @previoushourlyrate double;
DECLARE @pto_rate double;

@from_date = '03/01/2020';
@thru_date = '02/29/2020';
@dept_key = 13;
@previoushourlyrate = 17.00;
@team_key  = 29;

BEGIN TRANSACTION;
TRY

  -- CREATE tptech
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
--select *  
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.lastname = 'mogavero'
    AND a.currentrow = true;
  
  @tech_key = (SELECT techkey FROM tptechs WHERE lastname = 'mogavero');
  
  -- assign new tech to paint team
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;
  
  -- UPDATE tpteamvalues
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate = '12/31/9999';
  
  INSERT INTO tpTeamValues(teamkey,census,budget,poolPercentage,fromDate)
  values (@team_key, 5, 96, .32, @from_date);    

  -- CREATE tpTechValues row  
  -- techTeamPerc: = techrate/budget: 11.5/96 = 0.1198
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, 0.1198, @previoushourlyrate);
  
  -- tech values for the remaining techs
  -- have already created a row effective 03/01 for those techs who got a pto increase
  -- which IS only adam
  
    -- adam 72 15.25
  @tech_key = 72;   
   -- new techrate = current techrate/budget: 15.25/76.8 = .19857  15.25/96 = .1589
   update tptechvalues 
   SET techteampercentage = .1589
   WHERE techkey = @tech_key
     AND fromdate = @from_date;
   
    -- walden 31 19.33
  @tech_key = 31;   
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   -- new techrate = current techrate/budget: 19.33/76.8 = .2517  19.33/96 = .2014
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @from_date, .2014, @pto_rate);  

  -- jacobson 43 23.04 
  @tech_key = 43;   
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   -- new techrate = current techrate/budget: 23.04/76.8 = .3  23.04/96 = .24
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @from_date, .24, @pto_rate);  
  -- mavity 45 16.83
  @tech_key = 45;   
   @pto_rate = (
     SELECT previousHourlyRate 
     FROM tptechvalues     
     WHERE techkey = @tech_key
       AND thrudate = '12/31/9999');
   UPDATE tptechvalues
   SET thrudate = @thru_date
   WHERE techkey = @tech_key
     AND thrudate = '12/31/9999';
   -- new techrate = current techrate/budget: 16.83/76.8 = .21914 16.83/96 = .1753
   INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate) 
   values(@tech_key, @from_date, .1753, @pto_rate);  
   
      
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND teamkey = @team_key
    AND thedate = curdate();
    
  EXECUTE PROCEDURE xfmTpData();
      
  INSERT INTO employeeappauthorization
  SELECT 'mmogavero@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'adibi@rydellcars.com'
    AND appname = 'teampay';     
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  