/*
FROM ben cahalan 11/6/17

Two techs changed, both on paint team, Rob Mavity and Chris Walden.  
No other changes, was supposed to do this 4 weeks ago.

effective 12/25/16
*/

/*
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
ORDER BY teamname


*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @dept_key integer;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @census integer;
DECLARE @elr integer;
DECLARE @percentage double;
DECLARE @pto_rate double;
@eff_date = '12/25/2016';
@thru_date = '12/24/2016';
@dept_key = 13;
@elr = 60;
@team_key = (select teamkey FROM tpteams WHERE teamname = 'paint team');
BEGIN TRANSACTION;
TRY
-- 1. this is easy one: 2 guys get raises
-- 	  there IS enuf overhead IN current team budget, 96, to accomodate their increases
--	  so, ALL that IS needed IS to increase IS techTeamPercentage for each tech
  @tech_key = (
    SELECT techkey 
	FROM tpTechs
	WHERE lastname = 'Walden');
  @pto_rate = (
    SELECT previousHourlyRate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());
  UPDATE tptechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey,fromdate,techTeamPercentage,previousHourlyRate)
  values(@tech_key, @eff_date, .2014, @pto_rate); 

  @tech_key = (
    SELECT techkey 
	FROM tpTechs
	WHERE lastname = 'Mavity');
  @pto_rate = (
    SELECT previousHourlyRate
	FROM tpTechValues
	WHERE techkey = @tech_key
	  AND thrudate > curdate());
  UPDATE tptechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  INSERT INTO tptechvalues(techkey,fromdate,techTeamPercentage,previousHourlyRate)
  values(@tech_key, @eff_date, .1753, @pto_rate); 
  
    
  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND techkey IN (
	  SELECT techkey 
	  FROM tpTechs
	  WHERE lastname IN ('Walden','Mavity'))
    AND thedate >= @eff_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
               
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  