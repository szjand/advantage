Anthony Wetch, Team 13,, term 12/29/17
Remove efffective 1/21/18

Andrew Dibi goes to hourly effective 2/17/19

DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @tech_key integer;
@team_key = ( -- 46
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'Team 17');
@thru_date = '02/16/2019';
@tech_key = ( --81
  SELECT techkey
  FROM tpTechs
  WHERE lastname = 'dibi');
-- tpTeams
UPDATE tpTeams
SET thrudate = @thru_date
where teamkey = @team_key;
-- tpTechs
-- DO nothing
-- tpTeamTechs
update tpTeamTechs
SET thrudate = @thru_date
WHERE techkey = @tech_key
  AND teamkey = @team_key;
-- tpTeamValues  
UPDATE tpTeamValues
SET thrudate = @thru_date
WHERE teamkey = @team_key;
-- tpTechValues
UPDATE tpTechValues
SET thrudate = @thru_date
WHERE techkey = @tech_key;

DELETE FROM tpdata
WHERE teamkey = @team_key
AND thedate > @thru_date;

SELECT * FROM tptechvalues where techkey = 81