/*
7/26/17
get caught up, terms: kevin trosen, david perry, matthew ramirez
make it effective for this pay period

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
  AND teamname = 'paint team'
ORDER BY firstname

select * FROM tptechs
SELECT * FROM tpteamtechs;

select * FROM tpteamvalues
WHERE teamkey = 29

SELECT c.lastname, a.*
FROM tptechvalues a
INNER JOIN tpteamtechs b on a.techkey = b.techkey
  AND b.teamkey = 29
INNER JOIN tptechs c on a.techkey = c.techkey  
WHERE a.thrudate > curdate() ;

paint team 
kevin trosen
*/
DECLARE @techkey integer;
DECLARE @enddate date;
DECLARE @teamkey integer;
DECLARE @fromdate date;
DECLARE @census integer;
DECLARE @poolperc double;
DECLARE @elr double;
DECLARE @techperc double;
DECLARE @budget double;
DECLARE @old_budget double;
DECLARE @hourly double;
@enddate = '07/23/2016';
@fromdate = '07/24/2016';

-- paint team 
-- kevin trosen
@teamkey = (
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'paint team');
@techkey = (
  select techkey
  FROM tptechs 
  WHERE lastname = 'trosen'
    AND firstname = 'kevin'
	AND thrudate > curdate());	
@poolperc = (
  SELECT poolpercentage
  FROM tpteamvalues
  WHERE teamkey = @teamkey
    AND thrudate > curdate());
@elr = 60;
BEGIN TRANSACTION;
TRY	
  UPDATE tptechs
  SET thrudate = @enddate
  WHERE techkey = @techkey;	
  
  UPDATE tpTeamTechs
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND teamkey = @teamkey
    AND thrudate > curdate();
    
  UPDATE tpTeamValues
  SET thrudate = @enddate
  WHERE teamkey = @teamkey;  
  
  @census = (
    SELECT COUNT(*)
    FROM tpteamtechs
    WHERE teamkey = @teamkey
      AND thrudate > curdate());
  @old_budget = (
    SELECT budget
    FROM tpteamvalues
    WHERE teamkey = @teamkey
      AND thrudate = @enddate);
  	
  @budget = @census * @elr * @poolperc;
  
  INSERT INTO tpTeamValues(teamkey, census, budget, poolpercentage, fromdate)
  values (@teamkey, @census, @budget, @poolperc, @fromdate);
  
  -- each remaining team member has to have their techteampercentage adjusted
  -- such that their rate (tpdata.techtfrrate) remains the same
  -- first, CLOSE the trosen row
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
    
  @techkey = (
    SELECT techkey
    FROM tptechs
    WHERE lastname = 'walden'
      AND firstname = 'chris'
  	AND thrudate > curdate());
  @techperc = (
    SELECT techteampercentage
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  @hourly = (
    SELECT previoushourlyrate
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  	
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
    
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@techkey,@fromdate,(@techperc*@old_budget)/@budget, @hourly);  
  
  @techkey = (
    SELECT techkey
    FROM tptechs
    WHERE lastname = 'peterson'
      AND firstname = 'mavrik'
  	AND thrudate > curdate());
  @techperc = (
    SELECT techteampercentage
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  @hourly = (
    SELECT previoushourlyrate
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@techkey,@fromdate,(@techperc*@old_budget)/@budget, @hourly);  
  
  @techkey = (
    SELECT techkey
    FROM tptechs
    WHERE lastname = 'jacobson'
      AND firstname = 'peter'
  	AND thrudate > curdate());
  @techperc = (
    SELECT techteampercentage
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  @hourly = (
    SELECT previoushourlyrate
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@techkey,@fromdate,(@techperc*@old_budget)/@budget, @hourly); 
  
  @techkey = (
    SELECT techkey
    FROM tptechs
    WHERE lastname = 'mavity'
      AND firstname = 'robert'
  	AND thrudate > curdate());
  @techperc = (
    SELECT techteampercentage
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  @hourly = (
    SELECT previoushourlyrate
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@techkey,@fromdate,(@techperc*@old_budget)/@budget, @hourly); 

-- team 1
-- david perry
@teamkey = (
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'team 1');
@techkey = (
  select techkey
  FROM tptechs 
  WHERE lastname = 'perry'
    AND firstname = 'david'
	AND thrudate > curdate());	
@poolperc = (
  SELECT poolpercentage
  FROM tpteamvalues
  WHERE teamkey = @teamkey
    AND thrudate > curdate());
@elr = 60;

  UPDATE tptechs
  SET thrudate = @enddate
  WHERE techkey = @techkey;	
  
  UPDATE tpTeamTechs
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND teamkey = @teamkey
    AND thrudate > curdate();
    
  UPDATE tpTeamValues
  SET thrudate = @enddate
  WHERE teamkey = @teamkey;  
  
  @census = (
    SELECT COUNT(*)
    FROM tpteamtechs
    WHERE teamkey = @teamkey
      AND thrudate > curdate());
  @old_budget = (
    SELECT budget
    FROM tpteamvalues
    WHERE teamkey = @teamkey
      AND thrudate = @enddate);
  	
  @budget = @census * @elr * @poolperc;
  
  INSERT INTO tpTeamValues(teamkey, census, budget, poolpercentage, fromdate)
  values (@teamkey, @census, @budget, @poolperc, @fromdate);
  
  -- each remaining team member has to have their techteampercentage adjusted
  -- such that their rate (tpdata.techtfrrate) remains the same
  -- first, CLOSE the trosen row
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
    
  @techkey = (
    SELECT techkey
    FROM tptechs
    WHERE lastname = 'lamont'
      AND firstname = 'brandon'
  	AND thrudate > curdate());
  @techperc = (
    SELECT techteampercentage
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  @hourly = (
    SELECT previoushourlyrate
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  	
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
    
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@techkey,@fromdate,(@techperc*@old_budget)/@budget, @hourly);  
  
  @techkey = (
    SELECT techkey
    FROM tptechs
    WHERE lastname = 'sevigny'
      AND firstname = 'scott'
  	AND thrudate > curdate());
  @techperc = (
    SELECT techteampercentage
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  @hourly = (
    SELECT previoushourlyrate
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@techkey,@fromdate,(@techperc*@old_budget)/@budget, @hourly);  

-- team 5
-- matthew rameriz
@teamkey = (
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'team 5');
@techkey = (
  select techkey
  FROM tptechs 
  WHERE lastname = 'ramirez'
    AND firstname = 'matthew'
	AND thrudate > curdate());	
@poolperc = (
  SELECT poolpercentage
  FROM tpteamvalues
  WHERE teamkey = @teamkey
    AND thrudate > curdate());
@elr = 60;

  UPDATE tptechs
  SET thrudate = @enddate
  WHERE techkey = @techkey;	
  
  UPDATE tpTeamTechs
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND teamkey = @teamkey
    AND thrudate > curdate();
    
  UPDATE tpTeamValues
  SET thrudate = @enddate
  WHERE teamkey = @teamkey;  
  
  @census = (
    SELECT COUNT(*)
    FROM tpteamtechs
    WHERE teamkey = @teamkey
      AND thrudate > curdate());
  @old_budget = (
    SELECT budget
    FROM tpteamvalues
    WHERE teamkey = @teamkey
      AND thrudate = @enddate);
  	
  @budget = @census * @elr * @poolperc;
  
  INSERT INTO tpTeamValues(teamkey, census, budget, poolpercentage, fromdate)
  values (@teamkey, @census, @budget, @poolperc, @fromdate);
  
  -- each remaining team member has to have their techteampercentage adjusted
  -- such that their rate (tpdata.techtfrrate) remains the same
  -- first, CLOSE the trosen row
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
    
  @techkey = (
    SELECT techkey
    FROM tptechs
    WHERE lastname = 'swanson'
      AND firstname = 'kristen'
  	AND thrudate > curdate());
  @techperc = (
    SELECT techteampercentage
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  @hourly = (
    SELECT previoushourlyrate
    FROM tptechvalues
    WHERE techkey = @techkey
      AND thrudate > curdate());
  	
  UPDATE tptechvalues
  SET thrudate = @enddate
  WHERE techkey = @techkey
    AND thrudate > curdate();
    
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values(@techkey,@fromdate,(@techperc*@old_budget)/@budget, @hourly);  
  
  DELETE FROM tpData  
  WHERE departmentkey = 13
    AND thedate >= @fromdate;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();      

COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
         