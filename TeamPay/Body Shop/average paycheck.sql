SELECT *
FROM tptechs a
INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
  AND b.active = 'active'
WHERE departmentkey = 13
  AND a.thrudate > curdate()

SELECT * FROM tpemployees  

SELECT yhdemp, yhsaly,
   yhdtgp as gross,
cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
FROM dds.stgArkonaPYHSHDTA   
WHERE ypbcyy = 114
  AND yhdemp in (
    SELECT a.employeenumber
    FROM tptechs a
    INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
      AND b.currentrow = true
      AND b.active = 'active'
    WHERE departmentkey = 13
      AND a.thrudate > curdate())    
  AND yhdtgp <> 0  
ORDER BY yhdemp, cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date)

-- 2014 checks
SELECT yhdemp, fullname, COUNT(*) AS checks, SUM(gross) AS totalGross,
  SUM(gross)/COUNT(*) 
FROM (
  SELECT b.fullname, yhdemp, yhsaly,
     yhdtgp as gross,
  cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
  cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
  FROM dds.stgArkonaPYHSHDTA a
  LEFT JOIN tpEmployees b on a.yhdemp = b.employeenumber
  WHERE ypbcyy IN (114)
    AND yhdemp in (
      SELECT a.employeenumber
      FROM tptechs a
      INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
        AND b.currentrow = true
        AND b.active = 'active'
      WHERE departmentkey = 13
        AND a.thrudate > curdate())    
    AND yhdtgp <> 0) x  
GROUP BY yhdemp, fullname
ORDER BY fullname

-- 2014 thru curdate() 3/4/15
SELECT yhdemp, fullname, COUNT(*) AS checks, SUM(gross) AS totalGross,
  SUM(gross)/COUNT(*) 
FROM (
  SELECT b.fullname, yhdemp, yhsaly,
     yhdtgp as gross,
  cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
  cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
  FROM dds.stgArkonaPYHSHDTA a
  LEFT JOIN tpEmployees b on a.yhdemp = b.employeenumber
  WHERE ypbcyy IN (114,115)
    AND yhdemp in (
      SELECT a.employeenumber
      FROM tptechs a
      INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
        AND b.currentrow = true
        AND b.active = 'active'
      WHERE departmentkey = 13
        AND a.thrudate > curdate())    
    AND yhdtgp <> 0) x  
GROUP BY yhdemp, fullname
ORDER BY fullname