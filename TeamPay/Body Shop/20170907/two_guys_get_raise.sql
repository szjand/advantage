
Started with the next pay roll on 09/17/17

Please move Scott Sevigny�s commission rate from $19.54 to $20.54 per commission hour.

Please adjust Chad Gardner�s commission rate from  $19.74 to $20.74 per commission hour.

All ok�d by Ben Cahalan.


Thank you!


-- Scott Sevigny: only guy on team7

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
-- SELECT SUM(techTeamPercentage), SUM(techtfrrate)
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
AND teamname = 'team 7'
-- for a single tech team, WHERE techTeamPerc IS already configured AS 100%,
-- no change to hourly
-- no change required, his flat rate comes FROM tpTeamValues.poolPercentage
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @new_percentage double;
DECLARE @budget double;
DECLARE @team_key integer;
DECLARE @census integer;
DECLARE @hourly double;
DECLARE @tech_team_perc double;
@from_date = '09/17/2017';
@thru_date = '09/16/2017';
@tech_key = 41;
@team_key = 36;
@budget = 20.54;
@new_percentage = .3423;
@census = 1;
BEGIN TRANSACTION;
TRY 
UPDATE tpTeamValues
SET thrudate = @thru_date
WHERE teamkey = @team_key
  AND thrudate > curdate();
INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
values(@team_key, @census, @budget, @new_percentage, @from_date);  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  


-- scott gardner

SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey, techkey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
AND teamname = 'team 3'

team budget IS 42, increasing chad FROM 19.74 to 20.74 does NOT exceed that: 20.58 + 20.74 = 41.32
only thing that needs to be changed IS tpTechValues.TechTeamPercentage FROM .47 to .4938

UPDATE tpTechValues
SET thrudate = '09/16/2017'
WHERE techkey = 30 
  AND thrudate > curdate();
  
INSERT INTO tpTechValues(techkey,fromdate,techTeamPercentage,previousHourlyRate)
values(30, '09/17/2017', .4938, 30.59);  