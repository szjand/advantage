1. matt ramirez  retroactive to 2/8

2. ALL pto rate  retroactive to 2/8

3. Loren Sherek  effective 2/22

/* 
FROM ben cahalan
Body Shop Team Flat Rate February 2015.xlsx

Jon,
Update the sheet moving Matt R to Team Terry and also made some changes to Team Loren, 
Loren is getting a raise so I had to adjust all percentages to account for that.
The change with Matt R is effective for the pay period beginning 2/8/15

Change to Team Loren is effective for the pay period starting 2/22/15
*/
matthew ramirez
departmentkey: 13
techkey: 38
technumber: 237
employeenumber: 1114120
last team pay rate: $14.40
determine hourly AS part of the pto rate update
/*
SELECT * FROM tpdata WHERE lastname = 'ramirez' ORDER BY thedate DESC 
was last on a team (Team Cory) during the 12/28/14 -> 1/10/15 pay period
they want him on team terry, making the same that he last did on team cory,
  AND the others on team terry with no changes
 
this should be IN effect (retroactive) for the pay period 2/8/15 ->2/21/15
Team Terry current reality:
  % assigned to wages: 32%
  Aaron    37.3%   $14.32
  Terry    60.7%   $23.31
therefore current budget = 2 * .32 * 60 = $38.40
Mats current hourly rate IS 19.00
ADD a 3rd tech, budget beccomes: 3 * .32 * 60 = $57.60  
57.60 - 14.32 - 23.31 = 19.97 
another approach
14.32 + 23.31 + 14.40 = 52.03
which means that %assigned to wages would be 52.03/(60 * 3) = 28.91 %
team budget: 3 * .2891 * 60 = 52.04, much closer
aaron = 14.32/52.04 = 27.52%
terry = 23.31/52.04 = 44.79%
matt = 14.4/52.04 = 27.67%
*/

1.  Matthew
    issues
          tpTechs 
            primary key: TechKey (surrogate)
            natural key: DepartmentKey,TechNumber
            unique idx: EmployeeNumber
            
  matthew was team pay, THEN was NOT, THEN was again
  so techkey 38 has a from/thru of 1/12/14 -> 1/10/15
              
the general issue i have been trying to frame lately, being able to reconstruct
reality at a given point IN time BY tracking history 
vs
deriving a picture of reality IN each snapshot period, 
  eg tpData: ALL the info IS there, does NOT matter IF a tech key IS no 
    longer extant, "all" the data one would need IS stored IN tpData, names,
    team names, etc          
  eg Arkona: the manner IN which they essentially store store reports AS data
  
does it make sense to have currently valid (thrudate 12/31/9999) techvalues
for a tech that IS no longer valid
the whole question of a valid tech  

-- 2/19
this IS the first CASE of a tech leaving team pay, THEN returning to team pay
tpTechs:
change the natural key to DepartmentKey,TechNumber,ThruDate
employeenumber IS no longer unique
RI referencing tpTechs IS NOT affected, because the PK remains techKey
Matt gets a new techkey, i guess, 

select * FROM tpEmployees WHERE employeenumber = '1114120'
select * FROM employeeAppAuthorization WHERE username = 'mramirez@rydellcars.com'

-- here we go
change the natural key to DepartmentKey,TechNumber,ThruDate
employeenumber IS no longer unique
--< 1. Matthew Ramirez --------------------------------------------------------<
DROP index tpTechs.NK;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechs', 'tpTechs.adi', 'NK', 'DepartmentKey;TechNumber;ThruDate',
   '', 2051, 512, '' );
DROP index tpTechs.employeenumber;  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpTechs', 'tpTechs.adi', 'EMPLOYEENUMBER', 'EmployeeNumber',
   '', 2, 512, '' );    

/*
allowing multiple techkeys for a tech required each of these stored procs
to be edited, include tpTechs.thrudate IN determination of techkey
getTpTechOtherHoursTotal
getTpTechOtherHoursDetail
GetTpTechTotal
GetTpTechRosAdjustmentsByDay
GetTpTechAdjustmentTotals
GetTpTechRoTotals
GetTpTechROsByDay
GetTpTechDetail
*/   
BEGIN TRANSACTION;
TRY  
  --  tpTechs
  INSERT INTO tpTechs (departmentkey,technumber,firstname,lastname,employeenumber,
    fromdate,thrudate)
  SELECT departmentkey,technumber,firstname,lastname,employeenumber,'02/08/2015',
    '12/31/9999'
  FROM tpTechs WHERE techkey = 38;  

  -- employeeAppAuthorization
  INSERT INTO employeeAppAuthorization
  SELECT a.username, b.appname, b.appseq, b.appcode, b.approle, b.functionality, 13
  FROM tpEmployees a, 
       applicationMetaData b
  WHERE a.employeenumber = '1114120'
    AND b.appcode = 'tp'
    AND b.approle = 'technician'
    AND b.appseq = 101;
  
  -- tpTeamTechs 
  INSERT INTO tpTeamTechs (teamKey,techKey,fromdate)
  SELECT 
    (SELECT teamkey FROM tpteams WHERE teamname = 'team terry'),
    (SELECT techkey FROM tpTechs WHERE employeenumber = '1114120' AND thrudate > curdate()),
    '02/08/2015'
  FROM system.iota;
  
  -- tpTeamValues
  UPDATE tpTeamValues
  SET thruDate = '02/07/2015'
  WHERE teamkey = 17
    AND thrudate = '12/31/9999';
  INSERT INTO tpTeamValues (teamkey,census,budget,poolpercentage,fromdate)
  values(17,3,57.6,.32,'02/08/2015');
  
  -- tpTechValues
  -- aaron
  UPDATE tpTechValues
  SET thruDate = '02/07/2015'
  WHERE techkey = 52
  AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage, 
    previoushourlyrate)
  values(52,'02/08/2015',.249,14);
  
  -- terry
  UPDATE tpTechValues
  SET thruDate = '02/07/2015'
  WHERE techkey = 42
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage, 
    previoushourlyrate)
  values(42,'02/08/2015',.405,27.19);  
  
  -- matthew
  UPDATE tpTechValues
  SET thruDate = '02/07/2015'
  WHERE techkey = 38
    AND thrudate > curdate();
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage, 
    previoushourlyrate)
  values( (SELECT techkey FROM tpTechs WHERE employeenumber = '1114120' AND thrudate > curdate()),'02/08/2015',.25,15.87);   
  
-- AND FINALLY, UPDATE tpdata  
  DELETE FROM tpdata WHERE departmentkey = 13 AND thedate BETWEEN '02/08/2015' AND curdate();
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();    
  
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;   
--/> 1. Matthew Ramirez -------------------------------------------------------/>


--< New Hourly Rate -----------------------------------------------------------<
-- for 03/01/2014 thru 12/31/2014
-- body shop
-- team techs
-- does NOT include ryan lene
DROP TABLE #bspto;
SELECT *
INTO #bspto
FROM (
  SELECT TRIM(firstname) + ' ' + lastname AS Name, employeenumber as [Emp#], 
    SUM(commPay) AS [Comm Pay], SUM(clockHours) AS [Clock Hours], 
    round(SUM(commPay)/SUM(clockHours) , 2) AS [New PTO Rate], 
    max(HourlyRate) AS [Current PTO Rate], 
    round(round(SUM(commPay)/SUM(clockHours) , 2) - max(HourlyRate), 2) AS Change
  FROM (  
    select thedate, lastname, firstname, employeenumber,  
      techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
      techclockhourspptd AS ClockHours, 
      round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey
    WHERE thedate = a.payperiodend
      AND departmentKey = 13
  	AND thedate BETWEEN '03/01/2014' AND '12/31/2014'
    AND EXISTS (
      SELECT 1
      FROM tpTeamTechs
      WHERE techKey = a.techKey
        AND thruDate > curdate())) x
  GROUP BY lastname, firstname, employeenumber
  UNION
  -- pdr
  SELECT TRIM(firstname) + ' ' + lastname AS Name, employeenumber as [Emp#],   
    SUM(metalPay + pdrPay) AS [Comm Pay],
    SUM(techClockHoursPPTD) AS  [Clock Hours], 
    round(SUM(metalPay + pdrPay)/SUM(techClockHoursPPTD), 2) AS [New PTO Rate],
    max(j.otherRate) AS [Current PTO Rate], 
    round(round(SUM(metalPay + pdrPay)/SUM(techClockHoursPPTD), 2)  - max(j.otherRate), 2) AS Change
  FROM (-- clockhours
    SELECT theDate, techNumber, techClockHoursPPTD, employeenumber
    FROM bsFlatRateData
    WHERE thedate BETWEEN '03/01/2014' AND '12/31/2014'
      AND thedate = payperiodend) g
  LEFT JOIN (-- metal pay
    SELECT techNumber, payPeriodEnd, round(SUM(techMetalFlagHoursDay * metalRate), 2) AS metalPay
    FROM (
      SELECT a.theDate, a.payPeriodEnd, a.techNumber, a.techMetalFlagHoursDay, b.metalRate
      FROM bsFlatRateData a
      LEFT JOIN bsFlatRateTechs b on a.technumber = b.technumber
        AND a.theDate BETWEEN b.fromDate AND b.thruDate
      WHERE thedate BETWEEN '03/01/2014' AND '12/31/2014') c 
    GROUP BY techNumber, payPeriodEnd) h on g.theDate = h.payPeriodEnd
  LEFT JOIN (-- pdr pay
    SELECT techNumber, payPeriodEnd, round(SUM(techPdrFlagHoursDay * pdrRate), 2) AS pdrPay
    FROM (
      SELECT a.theDate, a.payPeriodEnd, a.techNumber, a.techPdrFlagHoursDay, b.pdrRate
      FROM bsFlatRateData a
      LEFT JOIN bsFlatRateTechs b on a.technumber = b.technumber
        AND a.theDate BETWEEN b.fromDate AND b.thruDate
      WHERE thedate BETWEEN '03/01/2014' AND '12/31/2014') c 
    GROUP BY techNumber, payPeriodEnd) i on g.theDate = i.payPeriodEnd
  LEFT JOIN (
    SELECT DISTINCT lastname, firstname, otherRate
    FROM bsFlatRateTechs) j on 1 = 1  
  GROUP BY j.lastname, j.firstname, employeenumber
  UNION -- matthew ramirez
  SELECT TRIM(firstname) + ' ' + lastname AS Name, employeenumber as [Emp#], 
    SUM(commPay) AS [Comm Pay], SUM(clockHours) AS [Clock Hours], 
    round(SUM(commPay)/SUM(clockHours) , 2) AS [New PTO Rate], 
    max(HourlyRate) AS [Current PTO Rate], 
    round(round(SUM(commPay)/SUM(clockHours) , 2) - max(HourlyRate), 2) AS Change
  FROM (  
    select thedate, lastname, firstname, employeenumber,  
      techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
      techclockhourspptd AS ClockHours, 
      round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey
    WHERE thedate = a.payperiodend
      AND departmentKey = 13
  	AND thedate BETWEEN '03/01/2014' AND '12/31/2014'
    AND a.employeenumber = '1114120') x
  GROUP BY lastname, firstname, employeenumber) z

-- DO pdr separately
-- team terry techs already have a tpTechValues row effective 2/8/15  
SELECT a.*, b.techkey, b.technumber, c.* 
FROM #bspto a
LEFT JOIN tpTechs b on a.emp# = b.employeenumber
  AND b.thrudate = '12/31/9999'
LEFT JOIN tpTechValues c on b.techkey = c.techkey
  AND c.thrudate = '12/31/9999'
WHERE c.fromdate <> '02/08/2015'  
ORDER BY b.techkey

-- pdr 
UPDATE bsFlatRateTechs
SET thrudate = '02/07/2015'
WHERE thrudate = '12/31/9999';

INSERT INTO bsFlatRateTechs  
SELECT storecode, departmentkey,technumber, employeenumber, username, firstname,
  lastname, fullname, pdrrate,metalrate,35.95, '02/08/2015','12/31/9999'
FROM bsFlatRateTechs
WHERE fromdate = '09/21/2014';

-- team terry 
UPDATE tpTechValues
SET previousHourlyRate = 27.3
WHERE techkey = 42
  AND fromdate = '02/08/2015';

UPDATE tpTechValues
SET previousHourlyRate = 16.03
WHERE techkey = 52
  AND fromdate = '02/08/2015';
  
UPDATE tpTechValues
SET previousHourlyRate = 16.86
WHERE techkey = 57
  AND fromdate = '02/08/2015';  
  
-- AND the other bs teams
SELECT a.*, b.techkey, b.technumber, c.* 
INTO #bsteams
FROM #bspto a
LEFT JOIN tpTechs b on a.emp# = b.employeenumber
  AND b.thrudate = '12/31/9999'
LEFT JOIN tpTechValues c on b.techkey = c.techkey
  AND c.thrudate = '12/31/9999'
WHERE c.fromdate <> '02/08/2015';  

UPDATE tpTechValues 
SET thruDate = '02/07/2015'
-- SELECT * FROM tpTechValues
WHERE thrudate = '12/31/9999'
  AND techkey IN (
    SELECT techkey
    FROM #bsteams);

INSERT INTO tpTechValues 
SELECT techkey, '02/08/2015', '12/31/9999', techTeamPercentage, [new pto rate]
-- SELECT *
FROM #bsteams;

UPDATE tpData
SET techHourlyRate = x.previousHourlyRate
FROM (
  SELECT *
  FROM tpTechValues
  WHERE thruDate > curdate()) x
WHERE departmentKey = 13
  AND payPeriodSeq = (
    SELECT distinct payPeriodSeq
    FROM tpData
    WHERE thedate = curdate())
  AND tpData.techKey = x.techKey;      
  
--/> New Hourly Rate ----------------------------------------------------------/>

--< Loren ---------------------------------------------------------------------<
tpTeamValues:
    Budget  108 -> 109.50
    Pool%   .36 -> .365 
tpTechValues:
    loren    37   .226 -> .2374  24.41 -> 26.00
    chad     30   .184 -> .1815  19.87 -> 19.87
    joshua   34   .184 -> .1815  19.87 -> 19.87
    justin   35   .164 -> .1617  17.71 -> 17.71
    ryan     56   .232 -> .2289  25.06 -> 25.06
  
OK, that ALL seems to WORK, looks good for body shop until sunday,
WHEN i need to DO loren

DECLARE @teamKey integer;
DECLARE @techKey integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @hourly double;
DECLARE @percentage double;
@fromDate = curdate();
@thruDate = @fromDate - 1;
@teamKey = (SELECT teamKey from tpTeams WHERE teamname = 'team loren' AND thrudate = '12/31/9999');
BEGIN TRANSACTION;
TRY 
  UPDATE tpTeamValues
  SET thrudate = @thruDate
  WHERE teamKey = @teamKey
    AND thrudate = '12/31/9999';
  INSERT INTO tpTeamValues
  values(@teamKEy, 5,109.5,.365, @fromDate, '12/31/9999');
  
  @techkey = (
    SELECT techkey 
    FROM tpTechs 
    WHERE lastname = 'shereck' AND firstname = 'loren' AND thrudate = '12/31/9999');
  @hourly = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techkey = @techkey
      AND thrudate = '12/31/9999');
  @percentage = .2374;   
  UPDATE tpTechValues 
  SET thrudate = @thruDate
  WHERE techkey = @techkey
    AND thrudate = '12/31/9999';
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values (@techKey,@fromDate,@percentage,@hourly); 
  
  @techkey = (
    SELECT techkey 
    FROM tpTechs 
    WHERE lastname = 'lene' AND firstname = 'ryan' AND thrudate = '12/31/9999');
  @hourly = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techkey = @techkey
      AND thrudate = '12/31/9999');
  @percentage = .2289;   
  UPDATE tpTechValues 
  SET thrudate = @thruDate
  WHERE techkey = @techkey
    AND thrudate = '12/31/9999';
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values (@techKey,@fromDate,@percentage,@hourly); 
  
  @techkey = (
    SELECT techkey 
    FROM tpTechs 
    WHERE lastname = 'gardner' AND firstname = 'chad' AND thrudate = '12/31/9999');
  @hourly = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techkey = @techkey
      AND thrudate = '12/31/9999');
  @percentage = .1815;   
  UPDATE tpTechValues 
  SET thrudate = @thruDate
  WHERE techkey = @techkey
    AND thrudate = '12/31/9999';
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values (@techKey,@fromDate,@percentage,@hourly);   
  
  @techkey = (
    SELECT techkey 
    FROM tpTechs 
    WHERE lastname = 'walton' AND firstname = 'joshua' AND thrudate = '12/31/9999');
  @hourly = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techkey = @techkey
      AND thrudate = '12/31/9999');
  @percentage = .1815;   
  UPDATE tpTechValues 
  SET thrudate = @thruDate
  WHERE techkey = @techkey
    AND thrudate = '12/31/9999';
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values (@techKey,@fromDate,@percentage,@hourly);    
  
  @techkey = (
    SELECT techkey 
    FROM tpTechs 
    WHERE lastname = 'olson' AND firstname = 'justin' AND thrudate = '12/31/9999');
  @hourly = (
    SELECT previousHourlyRate
    FROM tpTechValues
    WHERE techkey = @techkey
      AND thrudate = '12/31/9999');
  @percentage = .1617;   
  UPDATE tpTechValues 
  SET thrudate = @thruDate
  WHERE techkey = @techkey
    AND thrudate = '12/31/9999';
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previousHourlyRate)
  values (@techKey,@fromDate,@percentage,@hourly);  
  
  DELETE 
  FROM tpData
  WHERE teamkey = @teamKey
    AND theDate = @fromDate;
  
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();      
    
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  

--/> Loren --------------------------------------------------------------------/>