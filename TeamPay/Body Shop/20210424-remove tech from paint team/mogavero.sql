/*
moravero termed on 4/24

AS i determined IN the 11/11/19 script, it IS good enough to just UPDATE tpTeamTechs


*/
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND teamName = 'paint team'
ORDER BY teamname, firstname  

team key: 29
tech key: 86

DELETE 
FROM tpdata
WHERE techkey = 86
  and thedate >= '04/24/2021';
  
UPDATE tpTeamTechs
SET thrudate = '04/24/2021'
WHERE techkey = 86
  AND teamkey = 29;