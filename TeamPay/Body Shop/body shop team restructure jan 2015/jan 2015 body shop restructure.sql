

/*
WORK against connection zjon:  \\67.135.158.12:6363\advantage\zjon\sco.add 
  a copy of production scotest\sco.add
changes to take effect 01/11/2015

test script AS though changes take effect 12/28/2014

-- view LIKE ben's spreadsheet
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 13 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techTeamPercentage,
  techTFRRate, a.teamKey
--SELECT *
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = (SELECT MAX(thedate) FROM tpData)
  AND departmentKey = 13
--ORDER BY firstname  
ORDER BY teamname, firstname  



so, based on the flow chart (main shop teams.sdr, notes:team pay 11-29-14)
the ORDER of changes are:
1. tpShopValues: no changes
2. tpTeams: remove team cody
3. tpTechs: 
     mat ramirez & cody johnson leaving team pay 
     ADD ryan lene
     (tpTechs, tpEmployees employeeAppAuthorization)
4. tpTeamTechs: 
     Travis Strandell FROM Gray to Bear
     Kirby Holwerda FROM Longoria to Bear
     Jeff Bear to Bear
5. tpTeamValues: 
     Gray
     Longoria
     Bear (new team)
     AND because of the new ELR, the rest of teams also get a new row
6. tpTechValues: techTeamPercentages change
     Gray:David  
     
     
*/
DECLARE @username string;
DECLARE @firstname string;
DECLARE @lastname string;
DECLARE @password string;
DECLARE @effDate date;
DECLARE @departmentKey integer;
DECLARE @teamKey integer;
DECLARE @newELR double;
DECLARE @techKey integer;
DECLARE @techPerc double;
DECLARE @hourlyRate double;
DECLARE @census integer;
DECLARE @poolPerc double;
-- testing
--  @effDate = '12/28/2014';  
--  @newELR = 60;
--  @departmentKey = 13;
-- production
  @effDate = '01/11/2015';  
  @newELR = 60; ---------------------------------------------------------*
  @departmentKey = 13;  
BEGIN TRANSACTION;
TRY    
/**/ -- this portion IS necessary for backfilling the existing payperiod, for testing 
--DELETE FROM tpData WHERE thedate > @effDate AND departmentkey = @departmentkey;
-- no change IN tpShopValues, row FROM 5/4/14 still valid
--DELETE FROM tpShopValues WHERE fromDate = @effDate AND departmentKey = @departmentKey;
--UPDATE tpShopValues SET thruDate = '12/31/9999' WHERE thruDate = @effDate AND departmentKey = @departmentKey;
-- this IS NOT needed either
--DELETE FROM tpTeamValues WHERE fromDate = @effDate AND thruDate = '12/31/9999';
--UPDATE tpTeamValues SET thruDate = '12/31/9999' WHERE fromdate = '12/14/2014'; 
/**/
/*
production: assuming this IS being run on Sunday, day 1 of the new pay period,
  the first day of which IS @effDate
  overnight run of tpData generated a row IN tpData for @effDate with the old
  team configs, DELETE that row, it will be repopulated at the END of this script
*/  
--< production -----------------------------------------------------------------<
  DELETE 
  FROM tpData
  WHERE theDate = @effDate
    AND departmentKey = @departmentKey;
/*
-- 1. tpShopValues: new ELR  
    -- CLOSE previous pay period
TRY      
  UPDATE tpShopValues
  SET thruDate = @effDate - 1
  WHERE thruDate > curdate()
    AND departmentKey = @departmentKey;      
  -- INSERT row for new pay period
  INSERT INTO tpShopValues (departmentkey, fromdate, effectivelaborrate)
  values(@departmentKey, @effDate, @newELR);     
CATCH ALL
  RAISE scratchpad(1, __errtext);
END TRY;  
*/  
-- 2. tpTeams: team cody IS no more
TRY 
@teamkey = (SELECT teamkey FROM tpTeams WHERE teamname = 'team cody');
  UPDATE tpTeams
  SET thruDate = @effDate - 1
  WHERE teamkey = @teamkey 
    AND thruDate > curdate();
CATCH ALL
  RAISE scratchpad(3, __errtext);
END TRY;  
-- 3. tpTechs: cody johnson AND mat ramirez, travis beniot no longer IN teampay, ADD ryan lene
TRY 
  -- cody
  @firstname = 'cody';
  @lastname = 'johnson';
  @techKey = (SELECT techkey FROM tptechs WHERE firstname = @firstname AND lastname = @lastname);
  @username = (SELECT username FROM tpEmployees WHERE firstname = @firstname AND lastname = @lastname);
  UPDATE tpTechs
  SET thrudate = @effdate - 1
  WHERE techKey = @techKey;
  -- employeeAppAuthorization
  DELETE 
  FROM employeeAppAuthorization
  WHERE username = @username
    AND appcode = 'tp';
   
  -- matthew
  @firstname = 'matthew';
  @lastname = 'ramirez';
  @techKey = (SELECT techkey FROM tptechs WHERE firstname = @firstname AND lastname = @lastname);
  @username = (SELECT username FROM tpEmployees WHERE firstname = @firstname AND lastname = @lastname);
  UPDATE tpTechs
  SET thrudate = @effdate - 1
  WHERE techKey = @techKey;
  -- employeeAppAuthorization
  DELETE 
  FROM employeeAppAuthorization
  WHERE username = @username
    AND appcode = 'tp';   
    
  -- travis
  @firstname = 'travis';
  @lastname = 'beniot';
  @techKey = (SELECT techkey FROM tptechs WHERE firstname = @firstname AND lastname = @lastname);
  @username = (SELECT username FROM tpEmployees WHERE firstname = @firstname AND lastname = @lastname);
  UPDATE tpTechs
  SET thrudate = @effdate - 1
  WHERE techKey = @techKey;
  
  -- employeeAppAuthorization
  DELETE 
  FROM employeeAppAuthorization
  WHERE username = @username
    AND appcode = 'tp';      
    
  -- ryan
  @firstname = 'Ryan';
  @lastname = 'Lene';
  INSERT INTO tpTechs (departmentKey,techNumber,firstName,lastName,
    employeeNumber,fromDate) 
  SELECT @departmentKey, b.techNumber, a.firstName, a.lastName, a.employeeNumber,  @effDate
    FROM dds.edwEmployeeDim a
    LEFT JOIN dds.dimTech b on a.storeCode = b.storeCode
      AND a.employeenumber = b.employeenumber
    WHERE a.lastname = @lastname
      AND a.firstname = @firstname
      AND a.currentrow = true 
      AND a.active = 'active'
      AND b.currentrow = true;  
  -- tpEmployees  
  @username = 'rlene@rydellcars.com';
  @password = 'waMjKv882n';  
  INSERT INTO tpEmployees(username, firstname, lastname, employeenumber, password, 
    membergroup, storecode, fullname)
  SELECT @username, firstname, lastname, employeenumber, @password, 
    'Fuck You', 'RY1', TRIM(firstName) + ' ' + TRIM(lastName)
  FROM dds.edwEmployeeDim 
  WHERE lastname = @lastname 
    AND firstname = @firstname
    AND currentrow = true 
    AND active = 'active';    
  -- employeeAppAuthorization
  INSERT INTO employeeAppAuthorization
  SELECT userName, appName, appSeq, appCode, appRole, functionality, 
    @departmentKey
  FROM tpEmployees a, applicationMetadata b
  WHERE b.appcode = 'tp'
    AND b.appseq = 101
    AND b.approle = 'technician'
    AND a.username = 'rlene@rydellcars.com';       
    
CATCH ALL 
  RAISE scratchpad(3, __errtext);
END TRY;  
 
-- 4. tpTeamTechs: 
--      team cody: finished
--        jeremy & mavrik to team pete
--      team loren  ADD ryan lene
--      team cory  remove matt ramirez
--      team terry remove travis beniot
--      team pete ADD jeremy AND mavrik 
TRY 
  -- team cody
  @teamkey = (SELECT teamkey FROM tpTeams WHERE teamName = 'team cody');
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamkey
    AND thruDate >= @effDate - 1; -- only the current assignments
  -- team loren
  @teamkey = (SELECT teamkey FROM tpTeams WHERE teamName = 'team loren');  
  @firstname = 'ryan';
  @lastname = 'lene'; 
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = @firstname AND lastname = @lastname);
  INSERT INTO tpTeamTechs (teamkey,techkey,fromDate)
  values (@teamKey, @techKey, @effDate);
  -- team cory
  @teamkey = (SELECT teamkey FROM tpTeams WHERE teamName = 'team cory');  
  @firstname = 'matthew';
  @lastname = 'ramirez'; 
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = @firstname AND lastname = @lastname);
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND techKey = @techKey
    AND thruDate >= @effDate - 1;
  -- team terry  
  @teamkey = (SELECT teamkey FROM tpTeams WHERE teamName = 'team terry');  
  @firstname = 'travis';
  @lastname = 'beniot'; 
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = @firstname AND lastname = @lastname); 
  UPDATE tpTeamTechs
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
     AND techKey = @techKey
     AND thruDate >= @effDate - 1;
  -- team pete  
  @teamkey = (SELECT teamkey FROM tpTeams WHERE teamName = 'team pete');  
  @firstname = 'jeremy';
  @lastname = 'rosenau'; 
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = @firstname AND lastname = @lastname); 
  INSERT INTO tpTeamTechs(teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate);     
  @firstname = 'mavrik';
  @lastname = 'peterson'; 
  @techkey = (SELECT techkey FROM tpTechs WHERE firstname = @firstname AND lastname = @lastname); 
  INSERT INTO tpTeamTechs(teamKey,techKey,fromDate)
  values(@teamKey, @techKey, @effDate); 
 
CATCH ALL
  raise scratchpad(4, __errtext);
END TRY;  

--5. tpTeamValues: 
--     team cody: CLOSE it out
TRY
  -- team cody
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'team cody');
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate > @effDate - 1;

  -- team loren
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'team loren');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > @effDate -1);  
  @poolPerc = .36; ---------------------------------------------------------*   
  -- CLOSE current row  
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate >= @effDate -1; 
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));   
    
  -- team cory
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'team cory');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > @effDate -1);  
  @poolPerc = .34; ---------------------------------------------------------*   
  -- CLOSE current row  
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate >= @effDate -1; 
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));      
    
  -- team terry
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'team terry');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > @effDate -1);  
  @poolPerc = .32; ---------------------------------------------------------*   
  -- CLOSE current row  
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate >= @effDate -1; 
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));  
    
  -- team pete
  @teamKey = (SELECT teamKey FROM tpTeams WHERE teamName = 'team pete');
  @census = (
    SELECT COUNT(*) 
    FROM tpTeamTechs WHERE teamKey = @teamKey AND thruDate > @effDate -1);  
  @poolPerc = .36; ---------------------------------------------------------*   
  -- CLOSE current row  
  UPDATE tpTeamValues
  SET thruDate = @effDate - 1
  WHERE teamKey = @teamKey
    AND thruDate >= @effDate -1; 
  -- new row
  INSERT INTO tpTeamValues (teamKey, fromDate, census, poolPercentage, budget)
  values (@teamKey, @effDate, @census, @poolPerc, 
    round(@census * @poolPerc * @newELR, 2));             
        
CATCH ALL
  RAISE scratchpad(5, __errtext);
END TRY;
      
--6. tpTechValues: techTeamPercentages change
TRY 
  @firstname = 'loren'; ----------------------------------------------------------*
  @lastname = 'shereck'; ----------------------------------------------------------*
  @techPerc = .226; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
       
  @firstname = 'chad'; ----------------------------------------------------------*
  @lastname = 'gardner'; ----------------------------------------------------------*
  @techPerc = .184; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
         
  @firstname = 'joshua'; ----------------------------------------------------------*
  @lastname = 'walton'; ----------------------------------------------------------*
  @techPerc = .184; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  
  @firstname = 'justin'; ----------------------------------------------------------*
  @lastname = 'olson'; ----------------------------------------------------------*
  @techPerc = .164; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  

  @firstname = 'ryan'; ----------------------------------------------------------*
  @lastname = 'lene'; ----------------------------------------------------------*
  @techPerc = .232; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate); 
  
  @firstname = 'cory'; ----------------------------------------------------------*
  @lastname = 'rose'; ----------------------------------------------------------*
  @techPerc = .4015; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  
  @firstname = 'scott'; ----------------------------------------------------------*
  @lastname = 'sevigny'; ----------------------------------------------------------*
  @techPerc = .315; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  

  @firstname = 'david'; ----------------------------------------------------------*
  @lastname = 'perry'; ----------------------------------------------------------*
  @techPerc = .2775; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);
  
  @firstname = 'aaron'; ----------------------------------------------------------*
  @lastname = 'lindom'; ----------------------------------------------------------*
  @techPerc = .373; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  
  @firstname = 'terrance'; ----------------------------------------------------------*
  @lastname = 'driscoll'; ----------------------------------------------------------*
  @techPerc = .607; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  
  @firstname = 'peter'; ----------------------------------------------------------*
  @lastname = 'jacobson'; ----------------------------------------------------------*
  @techPerc = .225; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  
  @firstname = 'chris'; ----------------------------------------------------------*
  @lastname = 'walden'; ----------------------------------------------------------*
  @techPerc = .189; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  
  @firstname = 'robert'; ----------------------------------------------------------*
  @lastname = 'mavity'; ----------------------------------------------------------*
  @techPerc = .171; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  
  @firstname = 'jeremy'; ----------------------------------------------------------*
  @lastname = 'rosenau'; ----------------------------------------------------------*
  @techPerc = .194; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
  
  @firstname = 'mavrik'; ----------------------------------------------------------*
  @lastname = 'peterson'; ----------------------------------------------------------*
  @techPerc = .175; ----------------------------------------------------------*
  @techKey = (
    SELECT techKey
    FROM tpTechs 
    WHERE lastName = @lastname AND firstname = @firstname
      AND thruDate > @effDate);
  @hourlyRate = (
    SELECT previousHourlyRate
    FROM tpTechValues WHERE techKey = @techKey AND thrudate > @effDate); 
  UPDATE tpTechValues
  SET thruDate = @effDate - 1
  WHERE techKey = @techKey 
    AND thruDate > @effDate;  
  INSERT INTO tpTechValues (techKey, fromDate, techTeamPercentage, previousHourlyRate)
  values (@techKey, @effDate, @techPerc, @hourlyRate);  
     
CATCH ALL
  RAISE scratchpad(6, __errtext);
END TRY;                     
-- AND FINALLY, UPDATE tpdata  
    EXECUTE PROCEDURE xfmTpData();
    EXECUTE PROCEDURE todayxfmTpData();  
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    