-- current pay period (12/28 -> 1/10) remove cody FROM team cody
-- remove ALL his clock AND flag hours for the pay period

rather than reconfigure teams for current period, simply 0 out ALL of cody

SELECT *
FROM tpData a
WHERE payperiodstart = '12/28/2014'
  AND lastname = 'johnson' AND firstname = 'cody'
/*  
ARRRGGGGHHHHHHHHH fuck me i ran the DELETE against scotest NOT THE FREAKING zztest
well, will have to clean that up  
delete
FROM tpData 
WHERE payperiodstart = '12/28/2014'
  AND lastname = 'johnson' AND firstname = 'cody'  

ok take a breath
disable todayTpData
FROM scoTest, query zztest
get the data FROM 12/28 -> 01/09
INSERT that INTO scotest
reactivate todayTpData AND run it
AND, whew, looks LIKE we are ok

INSERT INTO tpData
SELECT *
FROM zztest.tpData a
WHERE thedate between '12/28/2014' AND '01/09/2015'
  AND lastname = 'johnson' AND firstname = 'cody'
*/

so the problem i am HAVING IS fine cody data IS gone, but now what about ALL the
team shit, that also has to be modified to remove cody
so, am i actually better off reconfiguring the teams for this pay period retroactively

i am thinking, DELETE the cody data FROM tpdata, THEN run xfmTpData,
just the teams parts without 
generating a new row, AND that  should UPDATE ALL the team data AS will 

** maybe instead of deleting the cody data, SET it ALL to 0
LIKE what currently EXISTS for travis beniot (termed but still on team)
SELECT *
FROM tpData a
WHERE payperiodstart = '12/28/2014'
  AND lastname = 'beniot'
  
  
looks LIKE this worked:
-- 1/11 looks LIKE this needs to go INTO xfmTpData since it does such a broad
--   scrape   
UPDATE tpData
  SET techClockHoursDay = 0,
      techVacationHoursDay = 0,
      techPtoHoursDay = 0,
      techHolidayHoursDay = 0,
      techClockHoursPPTD = 0,
      techVacationHoursPPTD = 0,
      techPtoHoursPPTD = 0,
      techHolidayHoursPPTD = 0, 
      techFlagHoursDay = 0,   
      TechFlagHoursPPTD = 0,  
      TechFlagHourAdjustmentsDay = 0,
      TechFlagHourAdjustmentsPPTD = 0
WHERE payperiodstart = '12/28/2014'
  AND lastname = 'johnson' AND firstname = 'cody';

BEGIN TRANSACTION; -- teams
TRY
  TRY  
    -- teamclockhoursDay
    UPDATE tpData 
    SET teamclockhoursday = x.teamclockhoursday
    FROM (
      SELECT thedate, teamkey, SUM(techclockhoursday) AS teamclockhoursday
      FROM tpdata
      GROUP BY thedate, teamkey) x 
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;  
    -- teamclockhours pptd
    UPDATE tpData
    SET teamclockhourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;       
    -- teamflaghoursday
    UPDATE tpData
    SET TeamFlagHoursDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(techflaghoursday) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;
    -- teamflaghour pptd  
    UPDATE tpData
    SET teamflaghourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;  
    -- TeamFlagHourAdjustmentsDay
    UPDATE tpData
    SET TeamFlagHourAdjustmentsDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(TechFlagHourAdjustmentsDay) AS hours
      FROM tpdata 
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey; 
    -- TeamFlagHourAdjustmentsPPTD 
    UPDATE tpData
    SET TeamFlagHourAdjustmentsPPTD = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, TeamFlagHourAdjustmentsDay) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x 
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;   
    -- team prof day          
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhoursday = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhoursday, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay) AS TotalFlag, 
          teamclockhoursday
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay), 
          teamclockhoursday) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;      
    -- team prof pptd
    UPDATE tpdata
    SET teamprofpptd = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhourspptd = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhourspptd, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS TotalFlag, 
          teamclockhourspptd
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD), 
          teamclockhourspptd) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;        
  CATCH ALL 
    RAISE tpdata(3, 'Teams ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- paint teams combined proficiency
TRY
  TRY  
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT b.*,
        CASE 
          WHEN totalFlag = 0 OR totalClock = 0 THEN 0
          ELSE round(100 * totalFlag/totalClock, 2)
        END AS prof  
    	FROM (
          SELECT thedate, 
            SUM(teamFlagHoursDay + teamTrainingHoursDay 
              + teamFlagHourAdjustmentsDay) AS totalFlag,
            SUM(teamClockHoursDay) AS totalClock
          FROM ( 
            SELECT distinct thedate, teamkey,
              teamflaghoursday, teamTrainingHoursDay, 
              TeamFlagHourAdjustmentsDay, teamclockhoursday
            FROM tpdata
            WHERE teamkey IN (18,19)) a 
          GROUP BY thedate) b) x 
    WHERE tpData.teamKey IN (18,19)
      AND tpData.theDate = x.theDate;	  		

    UPDATE tpdata
    SET teamprofPPTD = x.prof
    FROM (
      SELECT b.*,
        CASE 
          WHEN totalFlag = 0 OR totalClock = 0 THEN 0
          ELSE round(100 * totalFlag/totalClock, 2)
        END AS prof  
    	FROM (
          SELECT thedate, 
            SUM(teamFlagHoursPPTD + teamTrainingHoursPPTD 
              + teamFlagHourAdjustmentsPPTD) AS totalFlag,
            SUM(teamClockHoursPPTD) AS totalClock
          FROM ( 
            SELECT distinct thedate, teamkey,
              teamflaghoursPPTD, teamTrainingHoursPPTD, 
              TeamFlagHourAdjustmentsPPTD, teamclockhoursPPTD
            FROM tpdata
            WHERE teamkey IN (18,19)) a 
          GROUP BY thedate) b) x 
    WHERE tpData.teamKey IN (18,19)
      AND tpData.theDate = x.theDate;	          
  CATCH ALL 
    RAISE tpdata(4, 'Paint Teams Proficiency ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

     