/*
today
  clockHoursDay
  clockHoursPPTD
  techPdrFlagHoursDay
  techPdrFlagHoursPPTD
  techMetalFlagHoursDay
  techMetalFlagHoursPPTD
  techProficiencyDay
  techProficiencyPPTD
*/  
-- clock hours day
UPDATE bsFlatRateData
SET techClockHoursDay = x.clockHours
FROM (       
  SELECT c.thedate, d.departmentkey, d.technumber, SUM(a.clockHours) AS clockHours
  FROM dds.factClockHoursToday a
  INNER JOIN dds.edwEmployeeDim b on  a.employeeKey = b.employeeKey
  INNER JOIN dds.day c on a.dateKey = c.dateKey  
  INNER JOIN (
    SELECT DISTINCT departmentKey, technumber, employeenumber
    FROM bsFlatRateTechs) d on b.employeenumber = d.employeenumber 
  GROUP BY c.thedate, d.departmentkey, d.technumber) x 
WHERE bsFlatRateData.thedate = x.thedate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber; 
-- clock hours PPTD        
UPDATE bsFlatRateData
SET techClockHoursPPTD = x.reg
FROM (
  SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
    a.techClockHoursDay, SUM(b.techClockHoursDay) AS reg
  FROM bsFlatRateData a, bsFlatRateData b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
    a.techClockHoursDay) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;    
  
-- flag hours day
UPDATE bsFlatRateData
SET techPdrFlagHoursDay = x.techPdrFlagHours
FROM (
  SELECT b.theDate, e.departmentKey, e.techNumber, 
    SUM(flagHours) AS techPdrFlagHours
  FROM dds.todayFactRepairOrder a
  INNER JOIN dds.day b on a.flagDateKey = b.dateKey
  INNER JOIN dds.dimTech c on a.techKey = c.techKey
  INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
  INNER JOIN bsFlatRateTechs e on c.storeCode = e.storeCode
    AND c.techNumber = e.techNumber
  WHERE d.opcode = 'PDR'
  GROUP BY b.theDate, e.departmentKey, e.techNumber) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;     
  
UPDATE bsFlatRateData
SET techMetalFlagHoursDay = x.techMetalFlagHours
FROM (
  SELECT b.theDate, e.departmentKey, e.techNumber, 
    SUM(flagHours) AS techMetalFlagHours
  FROM dds.todayFactRepairOrder a
  INNER JOIN dds.day b on a.flagDateKey = b.dateKey
  INNER JOIN dds.dimTech c on a.techKey = c.techKey
  INNER JOIN dds.dimOpcode d on a.opcodeKey = d.opcodeKey
  INNER JOIN bsFlatRateTechs e on c.storeCode = e.storeCode
    AND c.techNumber = e.techNumber
  WHERE d.opcode <> 'PDR'
  GROUP BY b.theDate, e.departmentKey, e.techNumber) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;    

-- flag hours PPTD
UPDATE bsFlatRateData
SET techPdrFlagHoursPPTD = x.pdr
FROM (
  SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq,  
    a.techPdrFlagHoursDay, SUM(b.techPdrFlagHoursDay) AS pdr
  FROM bsFlatRateData a, bsFlatRateData b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
    a.techPdrFlagHoursDay) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber; 

UPDATE bsFlatRateData
SET techMetalFlagHoursPPTD = x.pdr
FROM (
  SELECT a.thedate, a.departmentKey, a.techNumber, a.payperiodseq,  
    a.techMetalFlagHoursDay, SUM(b.techMetalFlagHoursDay) AS pdr
  FROM bsFlatRateData a, bsFlatRateData b
  WHERE a.payperiodseq = b.payperiodseq
    AND a.departmentKey = b.departmentKey
    AND a.techNumber = b.techNumber
    AND b.thedate <= a.thedate
  GROUP BY a.thedate, a.departmentKey, a.techNumber, a.payperiodseq, 
    a.techMetalFlagHoursDay) x
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;  

  
-- proficiency day
UPDATE bsFlatRateData
SET techProficiencyDay = x.prof
FROM (
  SELECT thedate, departmentKey, techNumber, 
    CASE techClockHoursDay
      WHEN 0 THEN 0
      ELSE round(1.0 * (techPdrFlagHoursDay + 
  	  techMetalFlagHoursDay + techFlagHourAdjustmentsDay)/techClockHoursDay, 2) 
    END AS prof
  FROM bsFlatRateData
  WHERE theDate = curdate()) x   
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;  
  
-- proficiency day
UPDATE bsFlatRateData
SET techProficiencyPPTD = x.prof
FROM (
  SELECT thedate, departmentKey, techNumber, 
    CASE techClockHoursPPTD
      WHEN 0 THEN 0
      ELSE round(1.0 * (techPdrFlagHoursPPTD + 
	    techMetalFlagHoursPPTD + 
		techFlagHourAdjustmentsPPTD)/techClockHoursPPTD, 2) 
    END AS prof
  FROM bsFlatRateData
  WHERE theDate = curdate()) x   
WHERE bsFlatRateData.theDate = x.theDate
  AND bsFlatRateData.departmentKey = x.departmentKey
  AND bsFlatRateData.techNumber = x.techNumber;      
	

