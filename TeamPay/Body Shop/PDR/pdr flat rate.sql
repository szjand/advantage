SELECT b.thedate AS flagDate, a.ro, a.line, a.flaghours, c.opcode, d.opcode AS corCode, 
  f.paymentTypeCode AS payType, left(h.complaint, 50) AS complaint, 
  left(h.cause, 50) AS cause, LEFT(h.correction,50) AS correction
FROM factRepairOrder a
INNER JOIN day b on a.flagdatekey = b.datekey
INNER JOIN dimOpcode c on a.opcodeKey = c.opcodeKey
INNER JOIN dimOpcode d on a.corCodeKey = d.opcodeKey
INNER JOIN dimservicetype e on a.serviceTypeKey = e.serviceTypeKey
INNER JOIN dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
INNER JOIN dimTech g on a.techKey = g.techKey
INNER JOIN dimCCC h on a.ccckey = h.ccckey
WHERE g.technumber = '213'
  AND b.thedate > curdate() - 31
ORDER BY thedate desc  


SELECT b.thedate AS flagDate, a.ro, a.line, a.flaghours, c.opcode,
  CASE c.opcode
    WHEN 'PDR' THEN 14.4 * flaghours
    ELSE 19.95 * flaghours
  END AS flatRatePay
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
INNER JOIN dds.dimTech g on a.techKey = g.techKey
WHERE g.technumber = '213'
  AND b.theDate BETWEEN '03/09/2014' AND curdate()
ORDER BY thedate desc  

SELECT flagdate, SUM(flatRatePay)
FROM (
  SELECT b.thedate AS flagDate, a.ro, a.line, a.flaghours, c.opcode,
    CASE c.opcode
      WHEN 'PDR' THEN 14.4 * flaghours
      ELSE 19.95 * flaghours
    END AS flatRatePay
  FROM factRepairOrder a  
  INNER JOIN day b on a.flagdatekey = b.datekey
  INNER JOIN dimOpcode c on a.opcodeKey = c.opcodeKey
  INNER JOIN dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
  INNER JOIN dimTech g on a.techKey = g.techKey
  WHERE g.technumber = '213'
    AND b.thedate > curdate() - 31) x
GROUP BY flagdate    


-- total flat rate pay for the payperiod

SELECT b.thedate AS flagDate, a.ro, a.line, a.flaghours, c.opcode,
  CASE c.opcode
    WHEN 'PDR' THEN 14.4 * flaghours
    ELSE 19.95 * flaghours
  END AS flatRatePay
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
INNER JOIN dds.dimTech g on a.techKey = g.techKey
WHERE g.technumber = '213'
  AND b.theDate BETWEEN '03/23/2014' AND '04/05/2014'
  
hourly rate = 18.00
SELECT SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
  sum(holidayHours) AS holidayHours
FROM dds.edwClockHoursFact a
INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
INNER JOIN dds.dimTech c on b.storecode = c.storecode
  AND b.employeenumber = c.employeenumber
  AND c.currentrow = true
INNER JOIN dds.day d on a.datekey = d.datekey
WHERE c.storecode = 'ry1'
  AND c.technumber = '213'
  AND d.theDate BETWEEN '03/09/2014' AND '03/22/2014'
  
  
SELECT b.thedate AS flagDate, a.ro, a.line, a.flaghours, c.opcode,
  CASE c.opcode
    WHEN 'PDR' THEN 14.4 * flaghours
    ELSE 19.95 * flaghours
  END AS flatRatePay
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
INNER JOIN dds.dimTech g on a.techKey = g.techKey
WHERE g.technumber = '213'
  AND b.theDate BETWEEN '03/09/2014' AND '03/22/2014'  


-- first cut at a finished output  
-- looks good
SELECT n.*, o.*, pdrPay + metalPay + hourlyPay AS totalPay
FROM ( 
  SELECT pdrRate, round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay,
    metalRate, 
    round(SUM(metalFlagHours), 2) AS metalFlagHours,
    round(metalRate * round(SUM(metalFlagHours), 2), 2) AS metalPay
  FROM (  
    SELECT theDate, 14.40 AS pdrRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      19.95 AS metalRate,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.theDate BETWEEN '04/20/2014' AND '05/03/2014') m
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT 18.80 AS hourlyRate, SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
    sum(holidayHours) AS holidayHours,
    18.8 * (SUM(vacationHours) + SUM(ptoHours) * sum(holidayHours)) AS hourlyPay
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.theDate BETWEEN '04/20/2014' AND '05/03/2014') o  on 1 = 1  
    
-- make it WORK for payroll   
-- 2 lines, one for each rate
DECLARE @thruDate date;
DECLARE @fromDate date;
@thruDate = '04/05/2014'; -- last day of the pay period
@fromDate = '03/23/2014'; -- beginning of payroll for figuring pdr flat rate shit
SELECT p.name, p.employeenumber, n.pdrRate, n.pdrFlagHours, n.pdrPay, 
  p.hourlyRate, o.vacationHours,
  round(p.hourlyRate * (o.vacationHours + o.ptoHours), 2) AS vacationPay, 
  o.holidayHours, round(p.hourlyRate * holidayHours, 2) AS holidayPay, 
   n.pdrPay + round(p.hourlyRate * (o.vacationHours + o.ptoHours), 2) 
     + round(p.hourlyRate * holidayHours, 2) AS total
FROM ( 
  SELECT pdrRate, round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay
  FROM (  
    SELECT 14.40 AS pdrRate,
      coalesce(flaghours, 0) AS pdrFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND c.opCode = 'PDR'
      AND b.theDate BETWEEN @fromDate AND @thruDate) m
  GROUP BY pdrRate) n      
LEFT JOIN (
  SELECT SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
    sum(holidayHours) AS holidayHours
      FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.theDate BETWEEN @fromDate AND @thruDate) o  on 1 = 1  
LEFT JOIN (    
  SELECT trim(lastname) + ', ' + firstname AS Name, employeenumber, hourlyRate 
  FROM dds.edwEmployeeDim a
  WHERE lastname = 'peterson' 
    AND firstname = 'brian'   
    AND employeeKeyFromDate < @fromDate
    AND employeeKeyThruDate > @thruDate) p on 1 = 1
UNION
SELECT p.name, p.employeenumber, n.metalRate, n.metalFlagHours, n.metalPay, 
  0,0,0,0,0, n.metalPay
FROM ( 
  SELECT metalRate, round(SUM(metalFlagHours),2) AS metalFlagHours, 
    round(metalRate * round(SUM(metalFlagHours),2), 2) AS metalPay
  FROM (  
    SELECT 19.95 AS metalRate,
      coalesce(flaghours, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND c.opCode <> 'PDR'
      AND b.theDate BETWEEN @fromDate AND @thruDate) m
  GROUP BY metalRate) n      
LEFT JOIN (    
  SELECT trim(lastname) + ', ' + firstname AS Name, employeenumber, hourlyRate 
  FROM dds.edwEmployeeDim a
  WHERE lastname = 'peterson' 
    AND firstname = 'brian'   
    AND employeeKeyFromDate < @fromDate
    AND employeeKeyThruDate > @thruDate) p on 1 = 1    