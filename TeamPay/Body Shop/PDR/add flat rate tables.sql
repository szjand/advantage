DROP TABLE bsFlatRateTechs;
CREATE TABLE bsFlatRateTechs (
  storeCode cichar(3) constraint NOT NULL,
  departmentKey integer constraint NOT null,
  techNumber cichar(3) constraint NOT null,
  employeeNumber cichar(9) constraint NOT null,
  username cichar(50) constraint NOT NULL,
  firstName cichar(25) constraint NOT null,
  lastName cichar(25) constraint NOT null,
  fullName cichar(51) constraint NOT null,
  pdrRate double constraint NOT null,
  metalRate double constraint NOT null,
  otherRate double constraint NOT null,
  fromDate date constraint NOT null,
  thruDate date  constraint NOT null default '12/31/9999',
  constraint PK primary key (departmentKey,techNumber,thruDate)) IN database;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'bsFlatRateTechs','bsFlatRateTechs.adi','departmentKey','departmentKey',
   '',2,512, '' );      
EXECUTE PROCEDURE sp_CreateIndex90( 
   'bsFlatRateTechs','bsFlatRateTechs.adi','employeeNumber','employeeNumber',
   '',2,512, '' );   
EXECUTE PROCEDURE sp_CreateIndex90( 
   'bsFlatRateTechs','bsFlatRateTechs.adi','techNumber','techNumber',
   '',2,512, '' );          
  
INSERT INTO bsFlatRateTechs values (
  'RY1',13,'213','1110425','bpeterson@rydellcars.com','Brian','Peterson','Brian Peterson',14.4,19.95,18,'01/12/2014','12/31/9999');
 
DROP TABLE bsFlatRateData;  
CREATE TABLE bsFlatRateData (
  dateKey integer constraint NOT null,
  theDate date constraint NOT null,
  payPeriodStart date constraint NOT null,  
  payPeriodEnd date constraint NOT null, 
  payPeriodSeq integer constraint NOT NULL,
  dayOfPayPeriod integer constraint NOT null, 
  dayName cichar(12) constraint NOT null, 
  payPeriodSelectFormat cichar(100) constraint NOT null, 
  storeCode cichar(3) constraint NOT NULL,
  departmentKey integer constraint NOT null,
  techNumber cichar(3) constraint NOT null,
  employeeNumber cichar(9) constraint NOT NULL,
  username cichar(50) constraint NOT NULL,
  techClockHoursDay double default '0' constraint NOT null, 
  techClockHoursPPTD double default '0' constraint NOT null, 
  techVacationHoursDay double default '0' constraint NOT null,
  techVacationHoursPPTD double default '0' constraint NOT null,
  techHolidayHoursDay double default '0' constraint NOT null,
  techHolidayHoursPPTD double default '0' constraint NOT null,
  techPtoHoursDay double default '0' constraint NOT null,
  techPtoHoursPPTD double default '0' constraint NOT null,
  techPdrFlagHoursDay double default '0' constraint NOT null,
  techPdrFlagHoursPPTD double default '0' constraint NOT null,
  techMetalFlagHoursDay double default '0' constraint NOT null,
  techMetalFlagHoursPPTD double default '0' constraint NOT null,
  techOtherFlagHoursDay double default '0' constraint NOT null,
  techOtherFlagHoursPPTD double default '0' constraint NOT null,
  techFlagHourAdjustmentsDay double default '0' constraint NOT null,
  techFlagHourAdjustmentsPPTD double default '0' constraint NOT null,
  techProficiencyDay double default '0' constraint NOT null,
  techProficiencyPPTD double default '0' constraint NOT null,
  constraint PK primary key (theDate,departmentKey,techNumber)) IN database;

EXECUTE PROCEDURE sp_CreateIndex90( 
   'bsFlatRateData','bsFlatRateData.adi','payPeriodSeq','payPeriodSeq',
   '',2,512, '' );    
EXECUTE PROCEDURE sp_CreateIndex90( 
   'bsFlatRateData','bsFlatRateData.adi','username','username',
   '',2,512, '' );   
