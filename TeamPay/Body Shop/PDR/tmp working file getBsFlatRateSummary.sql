DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string;
DECLARE @date date;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM bsFlatRateData
  WHERE payperiodseq = @curPayPeriodSeq + -2);--(SELECT payPeriodIndicator FROM __input));
@date = (
  SELECT MAX(thedate)
  FROM bsFlatRateData
  WHERE payPeriodSeq = @payPeriodSeq);
  
@username = 'bpeterson@rydellcars.com'; -- (SELECT username FROM __input);

SELECT techPdrFlagHoursPPTD + techMetalFlagHoursPPTD AS flagHours, 
  round(techPdrFlagHoursPPTD * pdrRate, 2) + 
    round(techMetalFlagHoursPPTD * metalRate,2) AS commissionPay
FROM bsFlatRateData a
INNER JOIN bsFlatRateTechs b on a.departmentKey = b.departmentKey
  AND a.techNumber = b.techNumber
  AND b.thruDate > curdate()
WHERE a.username = @username
  AND a.thedate = @date;
  