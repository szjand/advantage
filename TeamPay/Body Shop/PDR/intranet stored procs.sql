insert into tpEmployees values('bpeterson@rydellcars.com','Brian','Peterson','1110425','123Abc45','Fuck You','RY1','Brian Peterson');

-- don't know IF i need to ADD him to tpTechs OR NOT
-- to reuse the adjustment stored procs, need to have him IN tpTechs
insert into tpTechs 
  (departmentKey,techNumber,firstName,lastName,employeeNumber,fromDate,thruDate) 
  values(13,'213','Brian','Peterson','1110425','01/12/2014','12/31/9999');

-- 4/9 

--per conv w/g&l, ADD approle of flatratetechnician
INSERT INTO applicationRoles (appname, role, appcode) values('teampay','flatratetechnician','tp');
-- AND the relevant new rows FROM applicationMetaData
INSERT INTO applicationMetaData
SELECT appname, appcode, appseq, 'flatratetechnician', functionality, 
  left(replace(configMapMarkupKey, 'tp', 'frp'), 50),
  left(replace(pubsub, 'tech','frtech'),50) , navtype, navtext, navseq
FROM applicationMetaData
WHERE appcode = 'tp'
  AND appseq = 101
  AND approle = 'technician';  
  
-- and the new employeeAppAuthorization	
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, 13
FROM tpEmployees a, applicationMetaData b
WHERE employeeNumber = '1110425'
  AND  b.appcode = 'tp'
  AND b.approle = 'flatratetechnician'
  AND b.appseq = 101;	
  
INSERT INTO employeeAppAuthorization
SELECT username, appname, appseq, appcode, approle, functionality, CAST(NULL AS sql_integer)
FROM tpEmployees a, applicationMetaData b
WHERE employeeNumber = '1110425'
  AND  b.appcode = 'cstfb'	;  

--< your pay summary page ------------------------------------------------------<

-- flat rate version of getTpTechSummary
--DROP PROCEDURE getBsFlatRateSummary;
CREATE PROCEDURE getBsFlatRateSummary (
  username cichar(50),
  payPeriodIndicator integer,
  techName cichar(60) output,
  pdrRate double output,
  metalRate double output,
  otherRate double output, 
  proficiency double output,
  pdrHours double output,
  metalHours double output,
  otherHours double output,
  pdrPay double output,
  metalPay double output,
  otherPay double output,
  totalCommissionPay double output,
  totalGrossPay double output)
BEGIN

/*
EXECUTE PROCEDURE getBsFlatRateSummary('bpeterson@rydellcars.com', -1);
*/
--DECLARE @date date;
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
DECLARE @otherRate double;
DECLARE @name string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';
@name = (
  SELECT fullname
  FROM tpEmployees
  WHERE username = @username);
@pdrRate = 14.4;
@metalRate = 19.95;
@otherRate = (
  SELECT hourlyRate
  FROM dds.edwEmployeeDim 
  WHERE employeenumber = @employeeNumber
    AND employeeKeyFromDate < @fromDate
    AND employeeKeyThruDate > @thruDate);
INSERT INTO __output
SELECT @name AS name, @pdrRate AS pdrRate, @metalRate AS metalRate, 
  @otherRate AS otherRate, 
  round((pdrFlagHours + metalFlagHours)/clockHours, 2) as proficiency,
  n.pdrFlagHours, n.metalFlagHours, otherHours, pdrPay, metalPay, hourlyPay,
  pdrPay + metalPay AS totalCommissionPay, 
  pdrPay + metalPay + hourlyPay AS totalGrossPay
FROM ( 
  SELECT round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(@pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay,
    round(SUM(metalFlagHours), 2) AS metalFlagHours,
    round(@metalRate * round(SUM(metalFlagHours), 2), 2) AS metalPay
  FROM (  
    SELECT theDate, @pdrRate AS pdrRate, @metalRate AS metalRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq) m
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT sum(clockHours) as clockHours, SUM(vacationHours) + sum(ptoHours) + sum(holidayHours) AS otherHours, 
    @otherRate * (SUM(vacationHours) + sum(ptoHours) + sum(holidayHours)) AS hourlyPay
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.biWeeklyPayPeriodSequence = @payPeriodSeq) o  on 1 = 1;
END;

--------------------------------------------------------------------------------
-- flat rate version of getTpTechDetail
--DROP PROCEDURE getBsFlatRateDetail;
create PROCEDURE getBsFlatRateDetail (  
  username cichar(50),
  payPeriodIndicator integer,
  date date OUTPUT, 
  workType cichar(6) OUTPUT,
  flagHours double OUTPUT,
  regularPay double output)
BEGIN
/*
GROUP BY date AND workType (pdr/metal)
EXECUTE PROCEDURE getBsFlatRateDetail('bpeterson@rydellcars.com', -1);
*/
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';
@pdrRate = 14.4;
@metalRate = 19.95;
INSERT INTO __output
SELECT theDate, workType, SUM(flagHours) AS flagHours, SUM(pay) AS pay
FROM (
    SELECT theDate, 'PDR' as workType, flagHours, 
	  round(@pdrRate * flagHours, 2) AS pay 
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq
	  AND c.opcode = 'PDR'
	
	UNION ALL
	 
    SELECT theDate, 'Metal', flagHours , 
	  round(@metalRate * flagHours, 2) AS pay
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq	
	  AND c.opcode <> 'PDR') x
GROUP BY theDate, workType;	    
	  
END; 

--------------------------------------------------------------------------------
-- bs flat rate version of GetTpTechTotal
create PROCEDURE getBsFlatRateTotal( 
      username cichar( 50 ),
      payPeriodIndicator Integer,
      flagHours DOUBLE  OUTPUT,
      commissionPay double OUTPUT) 
BEGIN 
/*
EXECUTE PROCEDURE GetBsFlatRateTotal ('bpeterson@rydellcars.com', -1);
*/
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';
@pdrRate = 14.4;
@metalRate = 19.95;
INSERT INTO __output
SELECT SUM(flagHours) AS flagHours, round(SUM(pay), 2) AS pay
FROM (
    SELECT flagHours, 
	  round(@pdrRate * flagHours, 2) AS pay 
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq
	  AND c.opcode = 'PDR'
	
	UNION ALL
	 
    SELECT flagHours , 
	  round(@metalRate * flagHours, 2) AS pay
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq	
	  AND c.opcode <> 'PDR') x;	    
	  
END; 
--------------------------------------------------------------------------------
-- bs flat rate version of getTpTechOtherHoursDetail
CREATE PROCEDURE getBsFlatRateOtherHoursDetail( 
  username cichar(50),
  payPeriodIndicator Integer,
  theDate DATE OUTPUT,
  vacationHours DOUBLE ( 15 ) OUTPUT,
  ptoHours DOUBLE ( 15 ) OUTPUT,
  holidayHours DOUBLE ( 15 ) OUTPUT) 
BEGIN 
/*  
EXECUTE PROCEDURE getBsFlatRateOtherHoursDetail('bpeterson@rydellcars.com', -1);
*/
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';


INSERT INTO __output

  SELECT theDate, vacationHours, ptoHours, holidayHours
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.biWeeklyPayPeriodSequence = @payPeriodSeq   
    AND vacationHours + ptoHours + holidayHours <> 0;
END;
--------------------------------------------------------------------------------
-- bs flat rate version of getTpTechOtherHoursTotals
CREATE PROCEDURE getBsFlatRateOtherHoursTotals( 
  username cichar(50),
  payPeriodIndicator Integer,
  vacationHours DOUBLE ( 15 ) OUTPUT,
  ptoHours DOUBLE ( 15 ) OUTPUT,
  holidayHours DOUBLE ( 15 ) OUTPUT) 
BEGIN 
/*  
EXECUTE PROCEDURE getBsFlatRateOtherHoursTotals('bpeterson@rydellcars.com', -1);
*/
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@UserName = (SELECT username FROM __input);
@employeeNumber = '1110425';


INSERT INTO __output

  SELECT sum(vacationHours) AS vacationHours, 
    sum(ptoHours) AS ptoHours, sum(holidayHours) AS holdayHours
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.biWeeklyPayPeriodSequence = @payPeriodSeq   
    AND vacationHours + ptoHours + holidayHours <> 0;
END;
--/> your pay summary page -----------------------------------------------------/>


--< pay period ros page --------------------------------------------------------<
--Adjustments, use the existing calls
-- data call: ...php/data.php/adjustments
-- sproc: getTpTechRoAdjustmentsByDy

-- data call: ...php/data.php/adjustmentsTotals
-- sproc: getTpTechAdjustmentTotals

--------------------------------------------------------------------------------
-- data call: ...php/data.php/flaggedros
-- bs flat rate version of getTpTechRosByDay

CREATE PROCEDURE GetBsFlatRateROsByDay( 
      userName CICHAR ( 50 ),
      payPeriodIndicator Integer,
      techName CICHAR ( 60 ) OUTPUT,
      flagDate DATE OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
	  pdr cichar(3) output,
	  metal cichar(3) output,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetBsFlatRateROsByDay('bpeterson@rydellcars.com', -1);

2/10/14 *a*
  WHILE looking at previous payperiod returns ro FROM today
  eg 16144742 (2-10) shows for payperiod 1-26 -> 2/8  
 
3/11/14 *c*
  holy shit, double counting flag hours WHEN exact same row EXISTS IN
  factRepairOrder AND todayFactRepairOrder
  the fucking issue seems to be unioning on flaghours AS a double sees the
  same values AS different, therefore returns the row twice AND summing the flag hours
  short term bandaid, round flag hours IN the base queries before unioning
*/

DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;  
DECLARE @curPayPeriodSeq integer;  
DECLARE @payperiodseq integer;  
@UserName = (SELECT username FROM __input);
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);  
INSERT INTO __output
SELECT top 1000 *
FROM (
  SELECT techname, flagdate, ro, 
    MAX(CASE WHEN workType = 'PDR' THEN 'Yes' else 'No' END) AS PDR,
	MAX(CASE WHEN workType = 'Metal' THEN 'Yes' else 'No' END) AS Metal,
	round(SUM(flaghours), 2) AS flaghours
  FROM (
    SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
  	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours, 
	CASE e.opcode
	  WHEN 'PDR' THEN 'PDR'
	  ELSE 'Metal' 
	END AS workType
    FROM dds.factRepairOrder a  
    INNER JOIN dds.dimTech c on a.techkey = c.techkey
    INNER JOIN tpTechs cc on c.technumber = cc.technumber
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
	INNER JOIN dds.dimOpcode e on a.opcodeKey = e.opcodeKey
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'
      AND a.flagdatekey IN (
        SELECT datekey
        FROM dds.day
        WHERE thedate BETWEEN @fromDate AND @thruDate)
      AND c.technumber = @techNumber 
    UNION
    SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
  	d.thedate AS flagDate, ro, line, corcodekey, round(flaghours, 2) AS flaghours,
	CASE e.opcode
	  WHEN 'PDR' THEN 'PDR'
	  ELSE 'Metal' 
	END AS workType
    FROM dds.todayfactRepairOrder a  
    INNER JOIN dds.dimTech c on a.techkey = c.techkey
    INNER JOIN tpTechs cc on c.technumber = cc.technumber
    INNER JOIN dds.day d on a.flagdatekey = d.datekey
	INNER JOIN dds.dimOpcode e on a.opcodeKey = e.opcodeKey
    WHERE flaghours <> 0
      AND a.storecode = 'ry1'        
      AND a.flagdatekey in (
        select datekey
        from dds.day
        WHERE thedate BETWEEN @fromDate AND @thruDate)                
      AND c.technumber = @techNumber) y
  GROUP BY techname, flagdate, ro) x	
ORDER BY techName, flagDate, ro; 

END;
--------------------------------------------------------------------------------
-- Totals: use the existing calls
-- data call: ...php/data.php/totalflaggedros
-- sproc: getTpTechRoTotals

--------------------------------------------------------------------------------
--/> pay period ros page -------------------------------------------------------/>


--< manager page ---------------------------------------------------------------<

CREATE PROCEDURE getBsFlatRateManagerDetails (
      department cichar(12),
      payPeriodIndicator Integer,
	  username cichar(50) output, 
      techName CICHAR ( 60 ) OUTPUT,
	  workType cichar(12) output,
      flagHours DOUBLE ( 15 ) OUTPUT,
      commissionPay DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 

/*
EXECUTE PROCEDURE getBsFlatRateManagerDetails('mainshop',-0);
*/
--DECLARE @date date;
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
DECLARE @name string;
DECLARE @username string;
DECLARE @department string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@employeeNumber = '1110425';
@name = (
  SELECT fullname
  FROM tpEmployees
  WHERE employeeNumber = @employeeNumber);
@username = (
  SELECT username
  FROM tpEmployees
  WHERE employeeNumber = @employeeNumber);
@pdrRate = 14.4;
@metalRate = 19.95;
@department = (SELECT upper(department) FROM __input);
IF @department <> 'BODYSHOP' THEN
  SELECT * FROM system.iota WHERE 1 = 2;
ELSE 
INSERT INTO __output

    SELECT @username, @name, 'PDR' as workType, sum(flagHours) AS pdrHours, 
	  round(@pdrRate * sum(flagHours), 2) AS pdrPay 
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq
	  AND c.opcode = 'PDR'
	
	UNION ALL
	 
    SELECT @username, @name, 'Metal', sum(flagHours) AS metalHours , 
	  round(@metalRate * sum(flagHours), 2) AS metalPay
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq	
	  AND c.opcode <> 'PDR';
END IF;
END;

CREATE PROCEDURE getBsFlatRateManagerTotals(
      department cichar(12),
      payPeriodIndicator Integer,
      flagHours double output, 
	  clockHours double output, 
	  proficiency double output,
	  commissionPay double output)

BEGIN 

/*
EXECUTE PROCEDURE getBsFlatRateManagerTotals('bodyshop', -1);
*/
--DECLARE @date date;
DECLARE @payPeriodSeq integer;
DECLARE @curPayPeriodSeq integer;
DECLARE @username string; 
DECLARE @employeeNumber string;
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @pdrRate double;
DECLARE @metalRate double;
DECLARE @otherRate double;
DECLARE @name string;
DECLARE @department string;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + (SELECT payPeriodIndicator FROM __input));
@thruDate = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@fromDate = (
  SELECT MIN(theDate)
  FROM tpData
  WHERE payperiodSeq = @payPeriodSeq);  
@employeeNumber = '1110425';
@name = (
  SELECT fullname
  FROM tpEmployees
  WHERE username = @username);
@pdrRate = 14.4;
@metalRate = 19.95;
@otherRate = (
  SELECT hourlyRate
  FROM dds.edwEmployeeDim 
  WHERE employeenumber = @employeeNumber
    AND employeeKeyFromDate < @fromDate
    AND employeeKeyThruDate > @thruDate);
@department = (SELECT upper(department) FROM __input);	
IF @department <> 'BODYSHOP' THEN
  SELECT * FROM system.iota WHERE 1 = 2;
ELSE 
	
INSERT INTO __output
SELECT flagHours, clockHours, 
  CASE clockHours
    WHEN 0 THEN 0
	ELSE round(1.0 * flagHours/clockHours, 2) 
  END AS proficiency,
  commissionPay 
FROM ( 
  SELECT round(SUM(pdrFlagHours),2) + round(SUM(metalFlagHours), 2) AS flagHours, 
    round(@pdrRate * round(SUM(pdrFlagHours),2), 2) + round(@metalRate * round(SUM(metalFlagHours), 2), 2) AS commissionPay
  FROM (  
    SELECT theDate, @pdrRate AS pdrRate, @metalRate AS metalRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM dds.factRepairOrder a
    INNER JOIN dds.day b on a.flagdatekey = b.datekey
    INNER JOIN dds.dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dds.dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dds.dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.biWeeklyPayPeriodSequence = @payPeriodSeq) m
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT sum(clockHours) as clockHours, SUM(vacationHours) + sum(ptoHours) + sum(holidayHours) AS otherHours, 
    @otherRate * (SUM(vacationHours) + sum(ptoHours) + sum(holidayHours)) AS hourlyPay
  FROM dds.edwClockHoursFact a
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN dds.day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.biWeeklyPayPeriodSequence = @payPeriodSeq) o  on 1 = 1;
END IF;
END;
--/> your pay summary page -----------------------------------------------------/>