/*
Starting 4/15/2019 Brandon Lamont is being moved from a stand alone metal 
technician to being a painter as part of the paint team.  If you could please 
switch the vision page and how his hours are being accounted for to be part 
of the paint team I would appreciate it.  Mavrik Petersons last day is 
4/12/2019 if you could please remove him from the vision page as well 
and take him off the paint team pay.

Thank you
John Gardner


John will put in a change in Compli, but his rate will change to $17.50 commission please.

TY!

so , remove mavrik FROM paint team
put brandon on paint team, remove team 1

select * FROM tpteams

-- current status
SELECT teamName, a.teamkey,
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, techkey, techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
WHERE thedate = curdate()
  AND departmentKey = 13
  AND teamName = 'paint team'
ORDER BY teamname, firstname  
*/

DECLARE @eff_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @tech_perc double;
DECLARE @pto_rate double;
DECLARE @dept_key integer;
DECLARE @elr double;
DECLARE @pool_perc double;
DECLARE @team_key integer;
DECLARE @census integer;
@eff_date = '04/14/2019';
@thru_date = '04/13/2019';
@dept_key = 13;

BEGIN TRANSACTION;
TRY

-- team 1 IS done
@tech_key = (  
  SELECT techkey 
  FROM tptechs 
  where lastname = 'lamont' 
    AND firstname = 'brandon' 
    AND thrudate > curdate()); 
@team_key = (
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'team 1'   
    AND thrudate > curdate());
-- take brandon off team 1
UPDATE tpteamtechs
SET thrudate = @thru_date
WHERE teamkey = @team_key
  AND techkey = @tech_key
  AND thrudate > curdate();
-- CLOSE out team 1     
UPDATE tpteams
SET thrudate = @thru_date
WHERE teamkey = @team_key;    
    
-- term mavrik
@tech_key = (
  SELECT techkey 
  FROM tptechs 
  where lastname = 'peterson' 
    AND firstname = 'mavrik' 
    AND thrudate > curdate());
UPDATE tptechs
SET thrudate = @thru_date
WHERE techkey = @tech_key 
  AND thrudate > curdate();
@team_key = (
  SELECT teamkey
  FROM tpteams
  WHERE teamname = 'paint team'   
    AND thrudate > curdate()); 
UPDATE tpteamtechs
SET thrudate = @thru_date
WHERE teamkey = @team_key
  AND techkey = @tech_key
  AND thrudate > curdate();
  
-- put brandon on paint team
@tech_key = (  
  SELECT techkey 
  FROM tptechs 
  where lastname = 'lamont' 
    AND firstname = 'brandon' 
    AND thrudate > curdate());  
INSERT INTO tpteamtechs (teamkey,techkey,fromdate)
values(@team_key, @tech_key, @eff_date);

 
-- techValues
  -- brandon
  @pto_rate = (SELECT previousHourlyRate FROM tpTechValues 
    WHERE techkey = @tech_key AND thrudate > curdate());
  @tech_perc = .1823; -----------------------------------------------
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();  
  INSERT INTO tpTechValues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @eff_date, @tech_perc, @pto_rate);   	

  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @eff_date
	AND techkey IN (39, 67);

	 
  EXECUTE PROCEDURE xfmTpData();  
  EXECUTE PROCEDURE todayxfmTpData();   
    
COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;    			

