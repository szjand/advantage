/*
We are switching Andrew Dibi to flat rate at $16.00 starting 8/20/2018. 
Jon G. (GRYSKIEWICZ, JONATHAN) is switching to flat rate at $18.00 starting 8/20/2018.  
We increased Codey Olson from $16.25 to $17.50 starting 8/20/2018.  
I am not sure if both Andrew and Jon have active vision pages that they can view.  
If you would be able to do this for us I would greatly appreciate it.

SELECT *
FROM tpTeams
WHERE departmentkey = 13
  AND thrudate > curdate()

-- current reality  
SELECT teamName, 
  (SELECT effectiveLaborRate 
    FROM tpShopValues 
    WHERE departmentKey = 18 AND thruDate > curdate()) AS ELR,
    b.poolPercentage, a.teamCensus, a.teamBudget,  
  TRIM(firstName) + ' ' + lastName, a.techkey, a.techTeamPercentage,
  round(techTFRRate, 2) AS tech_rate, a.teamKey, previousHourlyRate
FROM tpdata a
LEFT JOIN tpTeamValues b on a.teamKey = b.teamKey
  AND b.thruDate > curdate()
LEFT JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
WHERE thedate = curdate()
  AND departmentKey = 13
  AND a.teamkey IN (46)
ORDER BY teamname, firstname 


MAX team IS Team 16

SELECT * FROM dds.dimtech WHERE name LIKE '%jon%'

SELECT * FROM tptechs ORDER BY lastname
*/
---------------------------------------------------------------------------------------------------
-- new team
---------------------------------------------------------------------------------------------------
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @team_key integer;
DECLARE @tech_key integer;
DECLARE @dept_key integer;
DECLARE @census integer;
DECLARE @budget double;
DECLARE @pool_percentage double;
DECLARE @tech_team_percentage double;
DECLARE @previous_hourly_rate double;
DECLARE @team string;
DECLARE @employee_key integer;
DECLARE @employee_number string;
@from_date = '08/19/2018';
@thru_date = '08/18/2018';
@dept_key = 13;

BEGIN TRANSACTION;
TRY
-- new teams Andrew Dibi team 17
  @team = 'Team 17';
  @employee_key = (SELECT MAX(employeekey) FROM dds.edwEmployeeDim WHERE lastname = 'dibi' AND firstname = 'andrew');
  @employee_number = (SELECT employeenumber FROM dds.edwEmployeeDim WHERE employeekey = @employee_key);
  @census = 1;
  @budget = 16.00;
  @pool_percentage = 1;
  @tech_team_percentage = 1;
  @previous_hourly_rate = 16.00;
  
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, @team, @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @team);
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.employeekey = @employee_key;
  
  @tech_key = (SELECT techkey FROM tptechs WHERE employeenumber = @employee_number);
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, @census, @budget, @pool_percentage, @from_date);
  
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, @tech_team_percentage, @previous_hourly_rate);

 -- new teams GRYSKIEWICZ, JONATHAN team 18
  @team = 'Team 18';
  @employee_key = (SELECT MAX(employeekey) FROM dds.edwEmployeeDim WHERE lastname = 'GRYSKIEWICZ' AND firstname = 'JONATHAN');
  @employee_number = (SELECT employeenumber FROM dds.edwEmployeeDim WHERE employeekey = @employee_key);
  @census = 1;
  @budget = 18.00;
  @pool_percentage = 1;
  @tech_team_percentage = 1;
  @previous_hourly_rate = 18.00;
  
  INSERT INTO tpTeams (departmentkey,teamname,fromdate)
  values(@dept_key, @team, @from_date);
  
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @team);
  
  INSERT INTO tptechs (departmentkey,technumber,firstname,lastname,employeenumber,fromdate)
  SELECT @dept_key, b.technumber, a.firstname, a.lastname, a.employeenumber, @from_date
  FROM dds.edwEmployeeDim a
  INNER JOIN dds.dimtech b on a.employeenumber = b.employeenumber
    AND b.currentrow = true
  WHERE a.employeekey = @employee_key;
  
  @tech_key = (SELECT techkey FROM tptechs WHERE employeenumber = @employee_number);
  
  INSERT INTO tpteamtechs(teamkey,techkey,fromdate)
  SELECT @team_key, @tech_key, @from_date
  FROM system.iota;
  
  INSERT INTO tpteamvalues (teamkey,census,budget,poolpercentage,fromdate)
  values (@team_key, @census, @budget, @pool_percentage, @from_date);
  
  INSERT INTO tptechvalues (techkey,fromdate,techteampercentage,previoushourlyrate)
  values (@tech_key, @from_date, @tech_team_percentage, @previous_hourly_rate);
  

  --------------------------------------------------------------------------------------------------
 -- RAISE
 --------------------------------------------------------------------------------------------------
 
-- codey olson, team of one, only need to UPDATE tpTeamValues
  @team = 'Team 15';
  @census = 1;
  @budget = 17.50;
  @pool_percentage = 1;
 
  @team_key = (SELECT teamkey FROM tpteams WHERE teamname = @team);
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  INSERT INTO tpTeamValues(teamkey,census,budget,poolpercentage,fromdate)
  values(@team_key, @census, @budget, @pool_percentage, @from_date);   
  

  DELETE FROM tpData  
  WHERE departmentkey = @dept_key
    AND thedate >= @from_date;
    
  EXECUTE PROCEDURE xfmTpData();
  EXECUTE PROCEDURE todayxfmTpData();  
      
  INSERT INTO employeeappauthorization
  SELECT 'adibi@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'blamont@rydellcars.com'
    AND appname = 'teampay';     
	
  INSERT INTO employeeappauthorization
  SELECT 'jgryskiewicz@rydellcars.com',appname,appseq,appcode,approle,functionality,appdepartmentkey
  FROM employeeappauthorization
  WHERE username = 'blamont@rydellcars.com'
    AND appname = 'teampay';	
	          
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

  
