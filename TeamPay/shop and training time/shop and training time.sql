SELECT c.opcode, a.*
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimOpcode c on a.opcodekey = c.opcodekey
WHERE flagdateKey IN (
  SELECT datekey
  FROM dds.day
  WHERE thedate BETWEEN '03/09/2014' AND '03/22/2014')  
AND LEFT(a.ro, 2) = '18'  
ORDER BY b.thedate desc  


SELECT c.opcode, a.*
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimOpcode c on a.opcodekey = c.opcodekey
WHERE flagdateKey IN (
  SELECT datekey
  FROM dds.day
  WHERE thedate BETWEEN '03/09/2014' AND '03/22/2014')  
AND LEFT(a.ro, 2) = '18'  

ORDER BY b.thedate desc  


SELECT sum(flaghours)
FROM dds.factRepairOrder m
INNER JOIN dds.day n on m.flagdatekey = n.datekey
INNER JOIN dds.dimopcode o on m.opcodekey = o.opcodekey
WHERE n.thedate BETWEEN '03/09/2014' AND '03/22/2014'
AND o.opcode IN ( 'shop', 'train')
AND m.techkey IN (
  SELECT b.techKey
  FROM tpTechs a
  INNER JOIN dds.dimTech b on a.techNumber = b.techNumber
    AND b.storecode = 'ry1'
  WHERE departmentKey = 18)
  
SELECT opcode, COUNT(*)
FROM dds.factRepairOrder a
INNER JOIN dds.dimopcode b on a.opcodekey = b.opcodekey
WHERE flagdatekey IN (
  SELECT datekey
  FROM dds.day
  WHERE thedate BETWEEN '03/09/2014' AND '03/22/2014')
GROUP BY opcode  
ORDER BY opcode

-- 4/14

-- body shop

SELECT c.description, d.opcode
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimTech c on a.techKey = c.techKey
INNER JOIN dds.dimOpCode d on a.opcodekey = d.opcodeKey
WHERE theDate BETWEEN '03/01/2014' AND curdatE()
  AND technumber IN (
    SELECT techNumber
	FROM tpTechs
	WHERE departmentKey = 13)
GROUP BY c.description, d.opcode	
-- body shop opcodes used IN march 2014
SELECT d.opcode, COUNT(*), MIN(ro), MAX(ro)
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimTech c on a.techKey = c.techKey
INNER JOIN dds.dimOpCode d on a.opcodekey = d.opcodeKey
WHERE theDate BETWEEN '03/01/2014' AND curdatE()
  AND technumber IN (
    SELECT techNumber
	FROM tpTechs
	WHERE departmentKey = 13)
GROUP BY d.opcode	

SELECT c.description, SUM(flaghours), MIN(ro), MAX(ro), MIN(thedate), MAX(thedate)
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.flagdatekey = b.datekey
INNER JOIN dds.dimTech c on a.techKey = c.techKey
INNER JOIN dds.dimOpCode d on a.opcodekey = d.opcodeKey
WHERE theDate BETWEEN '03/01/2014' AND curdatE()
  AND technumber IN (
    SELECT techNumber
	FROM tpTechs
	WHERE departmentKey = 13)
  AND d.opcode = 'shop'
GROUP BY c.description


-- customer & vehicle for shop ros ?
SELECT b.fullname, c.*
FROM dds.factRepairOrder a
INNER JOIN dds.dimCustomer b on a.customerKey = b.customerKey
INNER JOIN dds.dimVehicle c on a.vehicleKey = c.vehicleKey
WHERE ro = '18028391'

select * 
FROM dds.factRepairOrder
WHERE vehicleKey = 140852

-- asked gayla why no shop time last pay period, she said there was, eg 18038725

SELECT * FROM dds.factRepairOrder WHERE ro = '18028725'

SELECT b.*
FROM dds.factRepairOrder a
INNER JOIN dds.dimOpcode b on a.opcodekey = b.opcodekey
WHERE a.ro = '18028725'

-- 7/7/14
SELECT *
FROM dimopcode
WHERE description LIKE '%shop%'
  OR description LIKE '%fence%'
  OR description LIKE '%traini%'
  
select * -- 0
FROM factRepairOrder
WHERE opcodekey = 7298 

select opcodekey, COUNT(*)
FROM factRepairOrder
WHERE opcodekey IN (7169, 7298, 14077, 15559, 17935, 22037, 22106, 22107, 22108)
GROUP BY opcodekey

the only 3 returned are:
  22106 - TRAIN 
  22107 - SHOP 
  22108 - FENCE

SELECT * FROM factRepairOrder

SELECT a.ro, b.thedate, b.yearmonth, a.flaghours, c.opcode, d.name, d.flagdeptCode
FROM factRepairOrder a
INNER JOIN day b on a.closedatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimtech d on a.techkey = d.techkey
WHERE c.opcodekey IN (22106, 22107, 22108)

SELECT a.ro, a.line, b.thedate, b.yearmonth, a.flaghours, c.opcode, d.name, 
  d.technumber, d.flagdeptCode, e.correction
FROM factRepairOrder a
INNER JOIN day b on a.flagdatekey = b.datekey
INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
INNER JOIN dimtech d on a.techkey = d.techkey
INNER JOIN dimccc e on a.ccckey = e.ccckey
WHERE c.opcodekey IN (22106, 22107, 22108)
  AND flaghours <> 0
ORDER BY thedate  
  
  
SELECT yearmonth, opcode, flagdeptcode, round(SUM(flaghours), 0) AS flaghours
FROM (
  SELECT a.ro, a.line, b.thedate, b.yearmonth, a.flaghours, c.opcode, d.name, 
    d.technumber, d.flagdeptCode, e.correction
  FROM factRepairOrder a
  INNER JOIN day b on a.flagdatekey = b.datekey
  INNER JOIN dimopcode c on a.opcodekey = c.opcodekey
  INNER JOIN dimtech d on a.techkey = d.techkey
  INNER JOIN dimccc e on a.ccckey = e.ccckey
  WHERE c.opcodekey IN (22106, 22107, 22108)
    AND flaghours <> 0) x
GROUP BY yearmonth, opcode, flagdeptcode    