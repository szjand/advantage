
----!!!!!!! need RI
DROP TABLE tpEmployees;
CREATE TABLE tpEmployees (
  UserName cichar(50) constraint NOT NULL,
  FirstName cichar(25) constraint NOT NULL,
  LastName cichar(25) constraint NOT NULL,
  EmployeeNumber cichar(9),
  constraint PK Primary key (UserName)) IN database;
ALTER TABLE tpEmployees
ADD COLUMN password cichar(50);  
ALTER TABLE tpEmployees
ADD COLUMN MemberGroup cichar(24)
ADD COLUMN StoreCode cichar(3);

UPDATE tpEmployees
SET password = 'password';

UPDATE tpEmployees
SET MemberGroup = 'Fuck You',
    StoreCode = 'RY1';

INSERT INTO tpEmployees
SELECT lcase(TRIM(LEFT(firstname,1))) + lcase(TRIM(lastname))  + '@rydellchev.com',
  firstname, lastname, employeenumber
FROM tpTechs;   

INSERT INTO tpEmployees values('mhuot@rydellchev.com', 'Michael','Huot', '168845');
INSERT INTO tpEmployees values('bcahalan@rydellchev.com', 'Benjamin','Cahalan', '123100');

CREATE TABLE Applications (
  AppName cichar(50)) IN database;
INSERT INTO Applications values('TeamPay'); 

CREATE TABLE Roles (
  Role cichar(25)) IN database;
INSERT INTO Roles values('Manager');
INSERT INTO Roles values('TeamLeader');
INSERT INTO Roles values('Technician');  

CREATE TABLE ApplicationRoles (
  AppName cichar(50),
  Role cichar(25),
  constraint PK Primary key (AppName,Role)) IN database;
INSERT INTO ApplicationRoles values('TeamPay','Manager');
INSERT INTO ApplicationRoles values('TeamPay','TeamLeader');
INSERT INTO ApplicationRoles values('TeamPay','Technician');

DROP TABLE EmployeeAppRoles;  
CREATE TABLE EmployeeAppRoles (
  UserName cichar(50) constraint NOT NULL,
  AppName cichar(50) constraint NOT NULL,
  Role cichar(25) constraint NOT NULL,
  FromDate date constraint NOT NULL,
  ThruDate date constraint NOT NULL default '12/31/9999',
  constraint PK primary key (UserName,AppName,Role,FromDate)) IN database;

INSERT into EmployeeAppRoles (UserName, AppName, Role, FromDate)
SELECT b.username, 'TeamPay','Technician', '09/08/2013'
FROM tpTechs a
INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
WHERE a.lastname NOT IN ('Westerhausen','olson','stryker','rogne','duckstad','mcveigh');

INSERT into EmployeeAppRoles (UserName, AppName, Role, FromDate)
SELECT b.username, 'TeamPay','Manager', '09/08/2013'
FROM tpEmployees b 
WHERE b.lastname IN ('Cahalan','Huot');

-- 12/2/13
INSERT into EmployeeAppRoles (UserName, AppName, Role, FromDate)
SELECT b.username, 'TeamPay','teamlead', '09/08/2013'
FROM tpTechs a
INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
WHERE a.lastname IN ('Westerhausen','olson','stryker','rogne','duckstad','mcveigh');


-- DROP TABLE ApplicationURIs;
CREATE TABLE ApplicationURIs (
  appName cichar(50) constraint NOT NULL,
  uri cichar(50) constraint NOT NULL) IN database;
INSERT INTO ApplicationURIs values('teampay','techsummary');
INSERT INTO ApplicationURIs values('teampay','techdetail');
INSERT INTO ApplicationURIs values('teampay','techtotals');
INSERT INTO ApplicationURIs values('teampay','shopsummary');
INSERT INTO ApplicationURIs values('teampay','shoptotals');
INSERT INTO ApplicationURIs values('teampay','teamtotals');
INSERT INTO ApplicationURIs values('teampay','adjustments');
INSERT INTO ApplicationURIs values('teampay','closedros');

-- DROP TABLE AppRoleUriAccess;
CREATE table AppRoleUriAccess (
  appRoleApplication cichar(50) constraint NOT NULL,
  role cichar(25),
  appUriApplication cichar(50) constraint NOT NULL,
  uri cichar(50) constraint NOT NULL,
  httpGet logical constraint NOT NULL,
  httpPost logical constraint NOT NULL,
  httpDelete logical constraint NOT NULL,
  FromDate date constraint NOT NULL,
  ThruDate date  constraint NOT NULL default '12/31/9999') IN database;

INSERT INTO approleuriaccess (approleapplication, role, appuriapplication, uri, 
  httpGet, httpPost, httpDelete, fromdate)  
SELECT  a.*, b.*, 
  CASE 
    WHEN role = 'Manager' AND uri IN ('shopsummary','shoptotals') THEN true 
    WHEN role = 'teamleader' AND uri IN ('shoptotals','teamtotals') THEN true
    WHEN role = 'technician' AND uri IN ('techsummary','techdetail',
      'techtotals', 'adjustments','closedros') THEN true
    ELSE false
  END AS htmlGet,
  false, false, cast('09/28/2013' AS sql_date)
FROM ApplicationRoles a, ApplicationURIs b

alter PROCEDURE AuthorizeDataCall (
  sessionId cichar(50),
  httpVerb cichar(4),
  applicationName cichar(50),
  uri cichar(50),
  returnCode integer output)
BEGIN
/*
204, 405
execute procedure AuthorizeDataCall ('fc33252a59527c40ad18daf641322237',
  'get', 'teampay', 'shoptotals')
*/
DECLARE @httpVerb string;
DECLARE @username string;
@username = (
  SELECT username
  FROM sessions
  WHERE sessionid = (SELECT sessionId FROM __input) 
    AND now() <= validThruTS);
@httpVerb = 
  CASE (SELECT httpVerb FROM __input)
    WHEN 'get' THEN 'httpGet'
    WHEN 'post' THEN 'httpPost'
    WHEN 'delete' THEN 'httpDelete'
  END;
INSERT INTO __output
SELECT 
  CASE
    WHEN EXISTS (
      SELECT 1
      FROM EmployeeAppRoles a
      INNER JOIN ApplicationRoles b on a.appname = b.appname
        AND a.role = b.role
      INNER JOIN AppRoleUriAccess c on b.appname = c.appRoleApplication
        AND b.role = c.role
        AND c.appUriApplication = (SELECT applicationName FROM __input)
        AND c.uri = (SELECT uri FROM __input)  
        AND 
          CASE @httpVerb
            WHEN 'httpGet' THEN httpGet = true
            WHEN 'httpPost' THEN httpPost = true
            WHEN 'httpDelete' THEN httpDelete = true
          END 
        AND curdate() BETWEEN c.fromdate AND c.thrudate
      WHERE a.UserName = @username)
    THEN 204
    ELSE 405
  END
FROM system.iota;    
END;

  
  
-- 12/2/13

DROP TABLE ApplicationMetaData;
CREATE TABLE ApplicationMetaData (
  appName cichar(50),
  appCode cichar(6), 
  appSeq integer,
  role cichar(25), 
  functionality cichar(50),
  configMapMarkupKey cichar(50),
  pubSub cichar(50),
  navType cichar(50),
  navText cichar(50),
  navSeq integer,
  dataUri cichar(50)) IN database;
INSERT INTO ApplicationMetaData values('teampay','tp', 100,'technician', 'tech pay period summary','mainlink_tp_tech_html','tp.tech.summary','leftnavbar','team pay (app header)',40,'\tp\techsummary');
INSERT INTO ApplicationMetaData values('teampay','tp', 100,'technician', 'ro list','sublink_tp_tech_ros_html','tp.tech.rolist','leftnavbar','pay period ros)',50,'\tp\techrolist');
INSERT INTO ApplicationMetaData values('teampay','tp', 100,'technician', 'how pay works','sublink_tp_tech_hpw_htmlsublink_tp_tech_hpw_html','tp.tech.hpw','leftnavbar','how pay works)',60,'\tp\techhpw');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'teamlead',  	   'teamlead pay period summary',   'mainlink_tp_teamlead_html',               'tp.teamlead.summary',		'leftnavbar',			    'team pay (app header)',	      40,		'\tp\teamleadsummary');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'teamlead',  	   'admin settings (main)',			'sublink_tp_teamlead_admin_html',          'tp.teamlead.adminmain',		'leftnavbar',				'admin settings',				  50,		'\tp\teamleadadmin');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'teamlead',  	   'admin - modal',					'sublink_tp_teamlead_admin_html',          'tp.teamlead.adminmodal',	'leftnavbar',				'admin settings',				  60,		'\tp\teamleadadmin_editemployeesettings');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'teamlead',  	   'store / team totals',		    'sublink_tp_teamlead_teamtotals_html',     'tp.teamlead.teamtotal', 	'leftnavbar',			    'store / team totals',		      70,		'\tp\teamleadstoretotals');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'teamlead',  	   'ro list',						'sublink_tp_teamlead_ros_html',			   'tp.teamlead.rolist',  		'leftnavbar',				'pay period ros',				  80,		'\tp\teamleadrolist');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'teamlead',  	   'how pay works',					'sublink_tp_teamlead_hpw_html',			   'tp.teamlead.hpw',			'leftnavbar',				'how pay works',				  90,		'\tp\teamleadhpw');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'manager',   	   'store / team totals',           'mainlink_tp_mgr_html',	                   'tp.mgr.summary',	 		'leftnavbar',				'team pay (app header)',	      40,		'\tp\mgrstoretotals');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'manager',   	   'admin settings (main)',			'sublink_tp_mgr_admin_html',			   'tp.mgr.adminmain',			'leftnavbar',				'admin settings',				  50,		'\tp\mgradmin');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'manager',   	   'admin - modal',					'sublink_tp_mgr_admin_html',	           'tp.mgr.adminmodal',			'leftnavbar',				'admin settings',				  60,		'\tp\mgradmin_editemployeesettings');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'manager',   	   'ro list',					    'sublink_tp_mgr_ros_html',                 'tp.mgr.rolist',				'leftnavbar',				'pay period ros',			      70,		'\tp\mgrrolist');
INSERT INTO ApplicationMetaData values('teampay','tp',	 100,				'manager',   	   'how pay works',					'sublink_tp_mgr_hpw_html',			       'tp.mgr.hpw',				'leftnavbar',				'how pay works',				  80,		'\tp\mgrhpw');


ALTER PROCEDURE getApplicationAuthorizattion (
  sessionId cichar(50),
  applicationName cichar(50)  output)
BEGIN
/*
INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 
  'mkoller@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) 
FROM system.iota;

INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 
  'crogne@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) 
FROM system.iota;

DELETE FROM sessions WHERE validthruts < now()
execute procedure getApplicationAuthorizattion ('05775a818092ee4eb9a954da52e4a078')

*/
DECLARE @username string;
@username = (
  SELECT username
  FROM sessions
  WHERE sessionid = (SELECT sessionId FROM __input) 
    AND now() <= validThruTS);
INSERT INTO __output
  SELECT distinct a.appname
  FROM applicationmetadata a
  INNER JOIN applicationroles b ON a.appname = b.appname AND a.role = b.role
  INNER JOIN employeeapproles c ON b.appname = c.appname AND b.role = c.role
  WHERE c.username = @username; 
END;

alter PROCEDURE getApplicationNavigation (
  sessionId cichar(50),
  appCode cichar(6),
  appCode cichar(6) output,
  configMapMarkupKey cichar(50) output,
  navType cichar(50) output,
  pubSub cichar(50) output)
BEGIN
/*
INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 
  'mkoller@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) 
FROM system.iota;

INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 
  'crogne@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) 
FROM system.iota;

DELETE FROM sessions WHERE validthruts < now()

execute procedure getApplicationNavigation ('05775a818092ee4eb9a954da52e4a078', 'tp')
*/
DECLARE @username string;
DECLARE @appCode string;
@appCode = (SELECT appCode FROM __input);
@username = (
  SELECT username
  FROM sessions
  WHERE sessionid = (SELECT sessionId FROM __input) 
    AND now() <= validThruTS);
INSERT INTO __output
  SELECT top 100 a.appCode, a.configMapMarkupKey, a.navType, a.pubSub
  FROM applicationmetadata a
  INNER JOIN applicationroles b ON a.appname = b.appname AND a.role = b.role
  INNER JOIN employeeapproles c ON b.appname = c.appname AND b.role = c.role
  WHERE c.username = @userName
    AND CASE
	  WHEN @appCode = '' THEN 1 = 1
	  WHEN @appCode <> '' THEN a.appCode = @appCode
	END
  ORDER BY appSeq, navSeq;
END;

