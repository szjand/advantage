SELECT *
FROM tptechvalues


SELECT a.lastname, a.firstname, a.employeenumber, a.techkey, b.*
FROM tptechs a
LEFT JOIN tptechvalues b on a.techkey = b.techkey
WHERE a.departmentkey = 13
  -- AND technumber NOT IN ('282','280','247','','','','','',''
  AND a.lastname NOT IN ('dibi','gryskiewicz','johnson','wetch','lindom')
    AND (a.lastname <> 'peterson' AND a.firstname <> 'brian')
  AND EXISTS (
    SELECT 1
  	FROM tptechs
  	WHERE technumber = a.technumber
  	  AND thrudate > curdate())
 ORDER BY a.lastname
 
 
 SELECT a.teamname, a.teamkey, 
  b.census, b.budget, b.poolpercentage,
  d.lastname, d.firstname, d.techkey, 
  e.techteampercentage,
  e.techteampercentage * b.budget AS tech_rate
FROM tpTeams a
INNER JOIN tpteamvalues b on a.teamkey = b.teamkey
  AND b.thrudate > curdate()
INNER JOIN tpteamtechs c on a.teamkey = c.teamkey
  AND c.thrudate > curdate()  
INNER JOIN tptechs d on c.techkey = d.techkey  
INNER JOIN tptechvalues e on d.techkey = e.techkey
  AND e.thrudate > curdate()
WHERE a.departmentkey = 13 
  AND a.thrudate > curdate()
  
  
SELECT a.lastname, a.firstname, a.employeenumber, a.techkey, b.*, c.*, d.*
FROM tptechs a
JOIN tpteamtechs b on a.techkey = b.techkey
JOIN tpteamvalues c on b.teamkey = c.teamkey
JOIN tptechvalues d on a.techkey = d.techkey
WHERE a.techkey = 72


brian peterson data stored separately
SELECT * 
FROM bsflatratedata

SELECT * 
FROM bsflatratetechs


 



for each tech, need history of flat rate AND pto rate

SELECT top 100 * FROM tpdata 
-- USING tpdata looks very promising
SELECT lastname, firstname, techkey, techtfrrate, MIN(thedate) AS from_date, MAX(thedate) AS thru_date
FROM tpdata 
WHERE departmentkey = 13
GROUP BY lastname, firstname, techkey, techtfrrate, techhourlyrate
HAVING MAX(thedate) > curdate() - 3
ORDER BY lastname, firstname

select x.lastname, x.firstname, y.techtfrrate, y.from_date, x.techhourlyrate, x.from_Date
FROM (
SELECT a.lastname, a.firstname, a.techkey, a.techhourlyrate, 
  MIN(a.thedate) AS from_date, MAX(a.thedate) AS thru_date
FROM tpdata a
WHERE a.departmentkey = 13
  AND a.techkey IN (
    SELECT a.techkey
    FROM tptechs a
    WHERE a.departmentkey = 13
      AND a.lastname NOT IN ('dibi','gryskiewicz','johnson','wetch','lindom')
      AND technumber <> '213' -- brian peterson
      AND EXISTS (
        SELECT 1
      	FROM tptechs
      	WHERE technumber = a.technumber
      	  AND thrudate > curdate()))  
GROUP BY a.lastname, a.firstname, a.techkey, a.techhourlyrate
HAVING MAX(a.thedate) > curdate() - 3) x
-- ORDER BY a.lastname, a.firstname
LEFT JOIN (
SELECT a.lastname, a.firstname, a.techkey, round(a.techtfrrate, 2) AS techtfrrate,
  MIN(a.thedate) AS from_date, MAX(a.thedate) AS thru_date
FROM tpdata a
WHERE a.departmentkey = 13
  AND a.techkey IN (
    SELECT a.techkey
    FROM tptechs a
    WHERE a.departmentkey = 13
      AND a.lastname NOT IN ('dibi','gryskiewicz','johnson','wetch','lindom')
      AND technumber <> '213' -- brian peterson
      AND EXISTS (
        SELECT 1
      	FROM tptechs
      	WHERE technumber = a.technumber
      	  AND thrudate > curdate()))  
GROUP BY a.lastname, a.firstname, a.techkey, a.techtfrrate
HAVING MAX(a.thedate) > curdate() - 3) y on x.techkey = y.techkey
ORDER BY x.lastname, y.firstname







 SELECT lastname, firstname, rowchangereason, employeekeyfromdate, employeekeythrudate, salary, hourlyrate, payrollclass
 -- SELECT *
 FROM dds.edwEmployeeDim 
 WHERE (
   (lastname = 'hicks' AND firstname = 'emily') OR
   (lastname = 'aguilar' AND firstname = 'anna') OR
   (lastname = 'benth' AND firstname = 'morgan') OR
   (lastname = 'magnuson' AND firstname = 'tyler') OR
   (lastname = 'ortiz' AND firstname = 'guadalupe') OR
   (lastname = 'powell' AND firstname = 'gayla') or
   (lastname = 'reuter' AND firstname = 'timothy') OR
   (lastname = 'sauls' AND firstname = 'thomas'))
ORDER BY lastname, employeekey   


SELECT name, termdate
FROM dds.edwEmployeeDim
WHERE termdate < curdate()
ORDER BY termdate DESC 