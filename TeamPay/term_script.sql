SELECT * FROM users WHERE username LIKE 'jwal%'


DECLARE @PartyID string;
DECLARE @NowTS timestamp;

@PartyID = (SELECT PartyID FROM users WHERE username = 'sferdon');
@NowTS = (SELECT now() FROM system.iota);

BEGIN TRANSACTION;
TRY 
  UPDATE contactmechanisms
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE users
  SET Active = False
  WHERE partyid = @PartyID;
  
  UPDATE ApplicationUsers
  SET ThruTS = @NowTS
  WHERE partyid = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE PartyRelationships
  SET ThruTS = @NowTS
  WHERE partyid2 = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE PartyPrivileges
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END;  