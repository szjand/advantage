/*
DELETE FROM tpdata WHERE thedate = curdate()

a combination of sps: GetTpShopTotals AND GetTpTeamTotals

*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @curPayPeriodSeq integer;
@curPayPeriodSeq = (SELECT DISTINCT payperiodseq FROM tpdata WHERE thedate = curdate());
@payperiodseq = (
  SELECT distinct payperiodseq
  FROM tpData
  WHERE payperiodseq = @curPayPeriodSeq + 0); -- which payperiod
@date = (
  SELECT MAX(thedate)
  FROM tpData
  WHERE payperiodseq = @payperiodseq);

  SELECT a.*, b.RegularPay
  FROM (
    SELECT distinct teamname, 
      (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
      teamclockhourspptd, teamprofpptd/100,
      round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
    FROM tpData
    WHERE thedate = @date) a
  LEFT JOIN (
  SELECT teamname, SUM(RegularPay) AS RegularPay
    FROM (
    SELECT distinct teamname, teamkey, technumber, techkey,
      round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
    FROM tpData
    WHERE thedate = @date) r
  GROUP BY teamname) b on a.teamname = b.teamname
UNION     
  SELECT 'zTotal', sum(FlagHours), SUM(clockhours), 
    CASE SUM(clockhours)
	  WHEN 0 THEN 0
	  ELSE round(SUM(FlagHours)/SUM(ClockHours), 4)
	END,
    sum(PayHours), SUM(regularPay)
  FROM (
    SELECT a.*, b.RegularPay 
    FROM (
      SELECT distinct teamname, 
        (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
        teamclockhourspptd AS ClockHours, teamprofpptd,
        round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
      FROM tpData
      WHERE thedate = @date) a
    LEFT JOIN (
    SELECT teamname, SUM(RegularPay) AS RegularPay
      FROM (
      SELECT distinct teamname, teamkey, technumber, techkey,
        round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
      FROM tpData
      WHERE thedate = @date) r
    GROUP BY teamname) b on a.teamname = b.teamname) s;