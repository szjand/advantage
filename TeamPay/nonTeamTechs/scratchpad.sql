SELECT d.*, a.ro, a.line, b.thedate, a.flaghours
FROM dds.factrepairorder a
INNER JOIN dds.day b ON a.opendatekey = b.datekey
INNER JOIN (-- non team techs
  SELECT techkey, technumber, employeenumber, description, name
  FROM dds.dimtech a
  WHERE a.currentrow = true
    AND active = true
    AND a.storecode = 'ry1'
    AND a.flagdeptcode = 'MR'
    AND NOT EXISTS (
      SELECT 1
    	FROM (-- techs currently assigned to team 
    	  SELECT a.technumber
          FROM tptechs a
          INNER JOIN tpteamtechs b ON a.techkey = b.techkey) c 
  	WHERE c.technumber = a.technumber)) d ON a.techkey = d.techkey
WHERE b.yearmonth IN (201312,201401)	
  
-- base values for a new ro 
 		  
SELECT techkey, technumber, a.employeenumber, coalesce(b.firstname, description), 
  coalesce(b.lastname, description)
FROM dds.dimtech a
LEFT JOIN (
  SELECT employeenumber, firstname, lastname
  FROM dds.edwEmployeeDim
  WHERE currentrow = true 
  GROUP BY employeenumber, firstname, lastname) b on a.employeenumber = b.employeenumber
WHERE a.currentrow = true
  AND active = true
  AND a.storecode = 'ry1'
  AND a.flagdeptcode = 'MR'
  AND a.technumber NOT IN ('574','611','572','634','526')
  AND NOT EXISTS (
    SELECT 1
  	FROM (-- techs currently assigned to team 
  	  SELECT a.technumber
        FROM tptechs a
        INNER JOIN tpteamtechs b ON a.techkey = b.techkey) c 
	WHERE c.technumber = a.technumber)		
	
-- this IS it	
    SELECT a.datekey, a.thedate, a.biweeklypayperiodstartdate, a.biweeklypayperiodenddate, 
      a.biweeklypayperiodsequence, dayinbiweeklypayperiod, a.DayName,
      (SELECT departmentkey FROM zDepartments WHERE dl2 = 'ry1' AND dl4 = 'Main Shop' AND dl5 = 'Shop'),
      b.TechKey, b.technumber, b.employeenumber, b.firstname, b.lastname
    FROM dds.day a, 
	  (
        SELECT techkey, technumber, a.employeenumber, 
		  coalesce(b.firstname, description) AS firstname, 
          coalesce(b.lastname, description) AS lastname
        FROM dds.dimtech a
        LEFT JOIN (
          SELECT employeenumber, firstname, lastname
          FROM dds.edwEmployeeDim
          WHERE currentrow = true 
          GROUP BY employeenumber, firstname, lastname) b on a.employeenumber = b.employeenumber
        WHERE a.currentrow = true
          AND active = true
          AND a.storecode = 'ry1'
          AND a.flagdeptcode = 'MR'
          AND a.technumber NOT IN ('574','611','572','634','526', '999')
          AND NOT EXISTS (
            SELECT 1
          	FROM (-- techs currently assigned to team 
          	  SELECT a.technumber
                FROM tptechs a
                INNER JOIN tpteamtechs b ON a.techkey = b.techkey) c 
        	WHERE c.technumber = a.technumber))	   b 
    WHERE a.thedate BETWEEN '09/08/2013' AND curdate()
      AND NOT EXISTS (
        SELECT 1
        FROM tpNonTeamData
        WHERE thedate = a.thedate
          AND techkey = b.techkey)
 		  
-- TechHourlyRate for new row
delete FROM tpnonteamdata WHERE techhourlyrate IS NULL

UPDATE tpNonTeamData
SET techhourlyrate = x.hourlyrate
FROM (
  SELECT thedate, a.employeenumber, b.hourlyrate
  FROM tpNonTeamData a
  INNER JOIN dds.edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  WHERE a.employeenumber <> 'NA') x
WHERE tpNonTeamData.thedate = x.thedate
  AND tpNonTeamData.employeenumber = x.employeenumber;   
		  
-- 1/17 ok, data loaded
SELECT thedate, technumber, lastname, techhourlyrate, techclockhourspptd, 
  techflaghourspptd, techflaghouradjustmentspptd, techprofpptd, teamprofpptd
FROM tpnonteamdata
WHERE thedate = curdate() - 1  

SELECT c.technumber, MIN(thedate), MAX(thedate), MIN(ro), MAX(ro)
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.dimTech c on a.techkey = c.techkey
WHERE c.technumber IN ('QS1','501','139')
GROUP BY c.technumber

SELECT c.technumber, thedate, ro, line, flaghours, d.name
FROM dds.factRepairOrder a
INNER JOIN dds.day b on a.opendatekey = b.datekey
INNER JOIN dds.dimTech c on a.techkey = c.techkey
LEFT JOIN dds.dimServiceWriter d on a.servicewriterkey = d.servicewriterkey
WHERE c.technumber = '501'
ORDER BY thedate DESC

-- 2/6/14
-- mike wants the spreadsheet for jan 2014
-- i see that i have created AND popluated TABLE tpNonTeamData
-- but it IS only populated thru 1/17/2014
-- can NOT fucking find WHERE i populated it FROM
-- aha sp.xfmTpNonTeamTechData

ok, now mikes spreadsheet

select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, technumber, 
  coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
FROM tpnonteamdata a
INNER JOIN dds.day b on a.thedate = b.thedate
WHERE b.yearmonth = 201401


SELECT *
FROM (
  SELECT name, SUM(flagHours) AS flagHours, 
    SUM(clockHours) AS clockHours, 
    round(1.00 *SUM(flaghours)/SUM(clockhours), 2) AS techProf
  FROM (
    select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, 
      coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
    FROM tpnonteamdata a
    INNER JOIN dds.day b on a.thedate = b.thedate
    WHERE b.yearmonth = 201401) x
  GROUP BY name) m  
LEFT JOIN (
  SELECT 
    round(1.00 *SUM(flaghours)/SUM(clockhours), 2) AS groupProf
  FROM (
    select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, 
      coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
    FROM tpnonteamdata a
    INNER JOIN dds.day b on a.thedate = b.thedate
    WHERE b.yearmonth = 201401) x) n on 1 = 1


  SELECT 'Total', SUM(flagHours) AS flagHours, 
    SUM(clockHours) AS clockHours, 
    round(1.00 *SUM(flaghours)/SUM(clockhours), 2) AS techProf
  FROM (
    select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, 
      coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
    FROM tpnonteamdata a
    INNER JOIN dds.day b on a.thedate = b.thedate
    WHERE b.yearmonth = 201401) x

-- 2/24 a spread sheet for a period
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '02/09/2014';
@thruDate = '02/22/2014';
SELECT name, SUM(flagHours) AS flagHours, 
  SUM(clockHours) AS clockHours, 
  CASE SUM(clockHours)
    WHEN 0 THEN 0
    ELSE round(1.00 *SUM(flaghours)/SUM(clockhours), 2) 
  END AS techProf
FROM (
  select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, 
    coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
  FROM tpnonteamdata a
  WHERE a.thedate BETWEEN @fromDate AND @thruDate) x
GROUP BY name
union
SELECT 'zTotal', SUM(flagHours) AS flagHours, 
  SUM(clockHours) AS clockHours, 
  round(1.00 *SUM(flaghours)/SUM(clockhours), 2) AS techProf
FROM (
  select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, 
    coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
  FROM tpnonteamdata a
  WHERE  a.thedate BETWEEN @fromDate AND @thruDate) x
  
    
-- entire shop    
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromDate = '02/09/2014';
@thruDate = '02/22/2014';   
SELECT 'Whole Shop' AS a , SUM(flagHours) AS flagHours,
  SUM(clockHours) AS clockHours,
  round(1.00 *SUM(flaghours)/SUM(clockhours), 2) AS techProf
FROM (  
  SELECT 'Hourly', SUM(flagHours) AS flagHours, 
    SUM(clockHours) AS clockHours, 
    round(1.00 *SUM(flaghours)/SUM(clockhours), 2) AS techProf
  FROM (
    select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, 
      coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
    FROM tpnonteamdata a
    WHERE  a.thedate BETWEEN @fromDate AND @thruDate) x
  union    
  SELECT 'Team', SUM(flagHours) AS flagHours, 
    SUM(clockHours) AS clockHours, 
    round(1.00 *SUM(flaghours)/SUM(clockhours), 2) AS techProf
  FROM (      
    select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, 
      coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
    FROM tpdata a
    WHERE a.departmentKey = 18
      AND a.thedate BETWEEN @fromDate AND @thruDate) a) z 
UNION 
SELECT 'Hourly', SUM(flagHours) AS flagHours, 
  SUM(clockHours) AS clockHours, 
  round(1.00 *SUM(flaghours)/SUM(clockhours), 2) AS techProf
FROM (
  select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, 
    coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
  FROM tpnonteamdata a
  WHERE  a.thedate BETWEEN @fromDate AND @thruDate) x   
UNION   
SELECT 'Team', SUM(flagHours) AS flagHours, 
  SUM(clockHours) AS clockHours, 
  round(1.00 *SUM(flaghours)/SUM(clockhours), 2) AS techProf
FROM (      
  select a.thedate, TRIM(lastname) + ', ' + TRIM(firstname) AS name, 
    coalesce(techClockHoursDay, 0) AS clockHours, coalesce(techFlagHoursDay, 0) AS flagHours
  FROM tpdata a
  WHERE a.departmentKey = 18
    AND a.thedate BETWEEN @fromDate AND @thruDate) a     