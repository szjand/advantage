DROP TABLE tpNonTeamData;
CREATE TABLE tpNonTeamData ( 
      DateKey Integer,
      TheDate Date,
      PayPeriodStart Date,
      PayPeriodEnd Date,
      PayPeriodSeq Integer,
      DayOfPayPeriod Integer,
      DayName CIChar( 12 ),
      DepartmentKey Integer,
      TechKey Integer,
      TechNumber CIChar( 3 ),
      EmployeeNumber CIChar( 9 ),
      FirstName CIChar( 25 ),
      LastName CIChar( 25 ),
--      TechTeamPercentage Double( 15 ),
      TechHourlyRate Double( 15 ),
--      TechTFRRate Double( 15 ),
      TechClockHoursDay Double( 15 ),
      TechClockHoursPPTD Double( 15 ),
      TechFlagHoursDay Double( 15 ),
      TechFlagHoursPPTD Double( 15 ),
      TechFlagHourAdjustmentsDay Double( 15 ),
      TechFlagHourAdjustmentsPPTD Double( 15 ),
--      TeamKey Integer,
--      TeamName CIChar( 25 ),
--      TeamCensus Integer,
--      TeamBudget Double( 15 ),
      TechProfDay double,
	  TechProfPPTD double,
      TeamClockHoursDay Double( 15 ),
      TeamClockHoursPPTD Double( 15 ),
      TeamFlagHoursDay Double( 15 ),
      TeamFlagHoursPPTD Double( 15 ),
      TeamProfDay Double( 15 ),
      TeamProfPPTD Double( 15 ),
      TechVacationHoursDay Double( 15 ),
      TechVacationHoursPPTD Double( 15 ),
      TechPTOHoursDay Double( 15 ),
      TechPTOHoursPPTD Double( 15 ),
      TechHolidayHoursDay Double( 15 ),
      TechHolidayHoursPPTD Double( 15 ),
--      TechOtherHoursDay Integer,
--      TechOtherHoursPPTD Integer,
--      TechTrainingHoursDay Integer,
--      TechTrainingHoursPPTD Integer,
--      TeamOtherHoursDay Integer,
--      TeamOtherHoursPPTD Integer,
--      TeamTrainingHoursDay Integer,
--      TeamTrainingHoursPPTD Integer,
      TeamFlagHourAdjustmentsDay Double( 15 ),
      TeamFlagHourAdjustmentsPPTD Double( 15 ),
      payPeriodSelectFormat CIChar( 65 )) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpNonTeamData',
   'tpNonTeamData.adi',
   'PK',
   'TheDate;DepartmentKey;TechNumber',
   '',
   2051,
   512,
   '' ); 


EXECUTE PROCEDURE sp_CreateIndex90( 
   'tpNonTeamData',
   'tpNonTeamData.adi',
   'TECHDAY',
   'TechNumber;TheDate',
   '',
   2,
   512,
   '' ); 


EXECUTE PROCEDURE sp_ModifyTableProperty( 'tpNonTeamData', 
   'Table_Primary_Key', 
   'PK', 'RETURN_ERROR', 'tpNonTeamDatafail');

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpNonTeamData', 
      'DateKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpNonTeamDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpNonTeamData', 
      'TheDate', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpNonTeamDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpNonTeamData', 
      'PayPeriodStart', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpNonTeamDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpNonTeamData', 
      'PayPeriodEnd', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpNonTeamDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpNonTeamData', 
      'PayPeriodSeq', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpNonTeamDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpNonTeamData', 
      'DayOfPayPeriod', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpNonTeamDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpNonTeamData', 
      'DayName', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpNonTeamDatafail' ); 

EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'tpNonTeamData', 
      'DepartmentKey', 'Field_Can_Be_Null', 
      'False', 'RETURN_ERROR', 'tpNonTeamDatafail' ); 

