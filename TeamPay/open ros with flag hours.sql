SELECT *
FROM #glro

SELECT b.thedate, a.ro, a.line, a.flaghours, laborsales
FROM dds.factRepairorder a
INNER JOIN dds.day b on a.closedatekey = b.datekey
INNER JOIN dds.dimServiceType c on a.ServiceTypeKey = c.ServiceTypeKey
WHERE a.storecode = 'ry1'
  AND b.thedate BETWEEN '09/28/2013' AND curdate()
  AND a.flaghours <> 0
  AND c.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('am','em','mr'))
  AND NOT EXISTS (
    SELECT 1
    FROM #glro
    WHERE gtdoc# = a.ro)  
    
16132291 NOT IN #gl line 2 tech 599 .4hrs, tech 623 -.4 hours, net time/labor sales = 0
  but 599 should be credited with .4 hrs
  
  
-- for the stored proc v1
-- ALL ros with an openline AND flaghours
-- DROP TABLE #OPEN;
SELECT ro, line, b.linestatus, c.rostatus, f.technumber, a.flaghours, 
  e.weightfactor, d.thedate,  a.laborsales,  a.paymenttypekey
--INTO #open  
FROM dds.factRepairOrder a
INNER JOIN dds.dimLineStatus b on a.statuskey = b.linestatuskey
INNER JOIN dds.dimRoStatus c on a.rostatuskey = c.rostatuskey  
INNER JOIN dds.day d on a.flagdatekey = d.datekey
INNER JOIN dds.brTechGroup e on a.techgroupkey = e.techgroupkey
INNER JOIN dds.dimTech f on e.techkey = f.techkey
--INNER JOIN tpTechs ff on f.technumber = ff.technumber
INNER JOIN dds.dimservicetype g on a.servicetypekey = g.servicetypekey
WHERE flaghours <> 0
  AND a.storecode = 'RY1'
  AND b.linestatus = 'Open'
  AND g.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('AM','EM','MR'))
  AND f.technumber IN (
    SELECT technumber
    FROM tpTechs)    
    
-- ros with OPEN & closed lines & flaghours
-- DROP TABLE #openClosed;
SELECT *
INTO #openclosed
FROM (    
SELECT distinct ro
FROM dds.factrepairorder
WHERE statuskey = 1
  AND storecode = 'RY1'
  AND flaghours <> 0
  AND servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('AM','EM','MR'))) a   
INNER JOIN (
SELECT distinct ro
FROM dds.factrepairorder
WHERE statuskey = 2
  AND storecode = 'RY1'
  AND flaghours <> 0
  AND servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('AM','EM','MR'))) b on a.ro = b.ro
    
SELECT *
FROM #OPEN a
full OUTER JOIN #openclosed b on a.ro = b.ro    


select *
FROM #OPEN a
LEFT JOIN #glro b on a.ro = b.gtdoc# AND a.paymenttypekey = b.paymenttypekey
ORDER BY a.ro, a.paymenttypekey


SELECT *
FROM dds.stgarkonaglptrns
WHERE gtdoc# = '16133947'
ORDER BY gtacct

SELECT a.*
FROM (
  SELECT ro, technumber, paymenttypekey, sum(flaghours*weightfactor) AS flaghours
  FROM #OPEN 
  GROUP BY ro, technumber, paymenttypekey) a
LEFT JOIN #glro b on a.ro = b.gtdoc# AND a.paymenttypekey = b.paymenttypekey
WHERE b.gtdoc# IS NULL 


-- speed this up enuf so temp TABLE NOT needed
-- fucking A, 3 sec to 30msec, go fucking figure
SELECT ro, line, f.technumber, a.flaghours,
  e.weightfactor, d.thedate,  a.laborsales,  a.paymenttypekey
--INTO #open  
FROM dds.factRepairOrder a
INNER JOIN dds.day d on a.flagdatekey = d.datekey
INNER JOIN dds.brTechGroup e on a.techgroupkey = e.techgroupkey
INNER JOIN dds.dimTech f on e.techkey = f.techkey
WHERE flaghours <> 0
  AND a.storecode = 'RY1'
  AND a.statuskey = (  
    SELECT linestatuskey
    FROM dds.dimlinestatus
    WHERE linestatus = 'Open')
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('AM','EM','MR'))
  AND f.technumber IN (
    SELECT technumber
    FROM tpTechs)   

-- USING this for the stored proc 11/25
SELECT technumber, COUNT(*) FROM (
SELECT d.thedate as FlagDate, ro, f.technumber, sum(a.flaghours *  e.weightfactor) AS flaghours, 
  a.paymenttypekey
--INTO #open  
FROM dds.factRepairOrder a
INNER JOIN dds.day d on a.flagdatekey = d.datekey
INNER JOIN dds.brTechGroup e on a.techgroupkey = e.techgroupkey
INNER JOIN dds.dimTech f on e.techkey = f.techkey
WHERE flaghours <> 0
  AND a.storecode = 'RY1'
  AND a.statuskey = (  
    SELECT linestatuskey
    FROM dds.dimlinestatus
    WHERE linestatus = 'Open')
  AND a.servicetypekey IN (
    SELECT servicetypekey
    FROM dds.dimservicetype
    WHERE servicetypecode IN ('AM','EM','MR'))
  AND f.technumber IN (
    SELECT technumber
    FROM tpTechs)   
  AND NOT EXISTS (
    SELECT 1 
    FROM #glro
    WHERE gtdoc# = a.ro
      AND paymenttypekey = a.paymenttypekey)    
GROUP BY ro, f.technumber, d.thedate,  a.paymenttypekey    
) x GROUP BY technumber ORDER BY COUNT(*) desc
