select b.yearmonth, a.lastname, sum(techflaghoursday) AS flaghours, 
  sum(techclockhoursday) AS clockhours, sum(techflaghouradjustmentsday) AS adjustments
-- SELECT *
FROM tpdata a
JOIN dds.day b on a.thedate = b.thedate
  AND b.yearmonth IN (202002, 202003)
WHERE lastname IN ('ferry','axtman','grant','duckstad','heffernan','syverson','holwerda','schwan','gray','anderson','o''neil')  
GROUP BY b.yearmonth, a.lastname
ORDER BY lastname,yearmonth


select b.yearmonth, teamname, a.lastname, sum(techflaghoursday) AS flaghours, 
  sum(techclockhoursday) AS clockhours, sum(techflaghouradjustmentsday) AS adjustments
-- SELECT *
FROM tpdata a
JOIN dds.day b on a.thedate = b.thedate
  AND b.yearmonth IN (202002, 202003)
WHERE lastname IN ('ferry','axtman','grant','duckstad','heffernan','syverson','holwerda','schwan','gray','anderson','o''neil')    



select b.yearmonth, teamname, sum(techflaghoursday) AS flaghours, 
  sum(techclockhoursday) AS clockhours, sum(techflaghouradjustmentsday) AS adjustments
-- SELECT *
FROM tpdata a
JOIN dds.day b on a.thedate = b.thedate
WHERE teamname IN (
  SELECT DISTINCT teamname
  FROM tpdata 
  WHERE thedate BETWEEN '02/01/2020' AND '03/31/2020'
    AND lastname IN ('ferry','axtman','grant','duckstad','heffernan','syverson','holwerda','schwan','gray','anderson','o''neil') 