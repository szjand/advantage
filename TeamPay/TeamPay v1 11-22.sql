
--DROP TABLE #gl;
SELECT a.gtdate, a.gtdoc#, gtacct, sum(gttamt) AS gttamt, gtjrnl, gtdesc
INTO #gl
FROM dds.stgArkonaGLPTRNS a
WHERE gtdate BETWEEN '09/08/2013' AND curdate()
  AND left(gtdesc, 3) <> 'ADJ'
  AND gttamt < 0 -- gets rid of correcting/adjusting entries which are irrelevant to closing date, i think
  AND a.gtacct IN (
    SELECT distinct account
    FROM dds.stgServicePaymentTypeAccounts)
group by a.gtdate, a.gtdoc#, gtacct, gtjrnl, gtdesc;
--DROP TABLE #ro;
SELECT ro, paymenttypekey, sum(laborsales) AS sales
INTO #ro
FROM dds.factRepairOrder b 
WHERE ro  IN ( SELECT gtdoc# FROM #gl)
  AND b.ServiceTypeKey IN (
    SELECT ServiceTypeKey 
    FROM dds.dimServiceType 
    WHERE ServiceTypeCode IN ('AM','EM','MR'))      
GROUP BY ro, paymenttypekey;  
--DROP TABLE #match;
SELECT *
INTO #match
FROM (
  SELECT *
  FROM #gl a
  full OUTER JOIN  #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) x
WHERE gtdoc# IS NOT NULL AND ro IS NOT NULL;  
--DROP TABLE #nomatch;
SELECT *
INTO #NoMatch
FROM (
  SELECT *
  FROM #gl a
  full OUTER JOIN  #ro b on a.gtdoc# = b.ro AND abs(a.gttamt) = abs(b.sales)) x
WHERE gtdoc# IS NULL OR ro IS NULL;  
--DROP TABLE #glRO;
SELECT *
INTO #glro
FROM #match
UNION   
-- #gl that don't match #ro
SELECT *
FROM (
  SELECT gtdate, gtdoc#, gtacct, gttamt, gtjrnl, gtdesc
  FROM #nomatch) a
LEFT JOIN (
  SELECT ro, paymenttypekey, sales
  FROM #nomatch) b on a.gtdoc# = b.ro   
WHERE gtdoc# IS NOT NULL AND ro IS NOT NULL   
UNION 
-- #ro that don't match #gl  
SELECT b.*, a.*
FROM (
  SELECT ro, paymenttypekey, sales
  FROM #nomatch) a
LEFT JOIN (
  SELECT gtdate, gtdoc#, gtacct, gttamt, gtjrnl, gtdesc
  FROM #nomatch) b on a.ro = b.gtdoc#   
WHERE ro IS NOT NULL AND gtdoc# IS NOT NULL ;  

--DROP TABLE #roTech;
SELECT ro, description, technumber, servicetypekey, paymenttypekey, sum(flaghours*weightfactor) as flaghours, sum(laborsales) AS sales
INTO #roTech
FROM dds.factRepairOrder b 
INNER JOIN dds.brtechgroup c on b.techgroupkey = c.techgroupkey
INNER JOIN dds.dimtech d on c.techkey = d.techkey  
WHERE ro  IN (SELECT gtdoc# FROM #gl)
  AND b.ServiceTypeKey IN (
    SELECT ServiceTypeKey 
    FROM dds.dimServiceType 
    WHERE ServiceTypeCode IN ('AM','EM','MR'))         
GROUP BY ro, description, technumber, servicetypekey, paymenttypekey;
--DROP TABLE #data;
SELECT DISTINCT b.ro, b.description, technumber, b.paymenttypekey, b.flaghours
INTO #data
FROM #glRO a
LEFT JOIN #roTech b on a.ro = b.ro
  AND a.paymenttypekey = b.paymenttypekey;
