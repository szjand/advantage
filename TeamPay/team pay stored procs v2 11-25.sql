
-- 11/26 after the 9oclock today, we are going to TRY something along the line
-- of, IF an app needs data, i will be handed a spec, which includes:
--  a name
--  input paramters
--  output field names with any formatting requests
-- so, forge ahead, stub out some known data requests AND THEN tweak them
-- based on the spec that gets submitted



--< Tech Splash ---------------------------------------------------------------< Tech Splash
alter PROCEDURE GetTpTechSummary
   ( 
      SessionID CICHAR ( 50 ),
      techName CICHAR ( 60 ) OUTPUT,
      teamName CICHAR ( 25 ) OUTPUT,
      fromDate DATE OUTPUT,
      thruDate DATE OUTPUT,
      regularRate Money OUTPUT,
      otherRate Money OUTPUT,
      teamProficiency DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      otherHours DOUBLE ( 15 ) OUTPUT,
      otherPay Money OUTPUT,
      regularPay Money OUTPUT,
      teamFlagHours DOUBLE ( 15 ) OUTPUT,
      teamClockHours DOUBLE ( 15 ) OUTPUT,
      totalGrossPay Money OUTPUT
   ) 
BEGIN 
/*
INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 'mkoller@rydellchev.com', 
timestampadd(sql_tsi_hour, 4, now()) FROM system.iota;

execute PROCEDURE GetTpTechSummary('[346e15f7-f7bf-6a48-af2d-663a2511c565]');
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@date = curdate(); -- @date = (SELECT thedate FROM __input);
@payperiodseq = (
  SELECT DISTINCT payperiodseq
  FROM tpData 
  WHERE thedate = @date);  
INSERT INTO __output


  SELECT distinct TRIM(c.firstname) + ' ' + TRIM(c.lastname) AS techName, 
    a.teamName, a.payperiodstart AS fromDate, a.payperiodend AS thruDate,
    a.TechTFRRate AS regularRate, a.TechHourlyRate AS otherRate, 
    a.teamProfPPTD/100 AS teamProficiency, 
    a.techClockHoursPPTD AS clockHours,
    round(a.techClockHoursPPTD * a.teamProfPPTD/100, 2) AS payHours,
    a.techVacationHoursPPTD + a.techPTOHoursPPTD + a.techHolidayHoursPPTD AS otherHours,
    a.TechHourlyRate * (a.techVacationHoursPPTD + a.techPTOHoursPPTD
       + a.techHolidayHoursPPTD) AS otherPay,
    round(a.TechTFRRate * (a.techClockHoursPPTD * a.teamProfPPTD/100),2) AS regularPay,
    a.teamFlagHoursPPTD AS teamFlagHours, teamClockHoursPPTD AS teamClockHours,
    (a.TechHourlyRate * (a.techVacationHoursPPTD + a.techPTOHoursPPTD
       + a.techHolidayHoursPPTD))  
       + round(a.TechTFRRate * (a.techClockHoursPPTD * a.teamProfPPTD/100),2) AS totalGrossPay       
  FROM tpdata a
  INNER JOIN tpTechs b on a.techkey = b.techkey
  INNER JOIN tpEmployees c on b.employeenumber = c.employeenumber
  WHERE c.Username = @UserName
    AND thedate = (
      SELECT MAX(thedate)
      FROM tpdata
      WHERE payperiodseq = @payperiodseq);

END;



alter PROCEDURE GetTpTechDetail (
  SessionID cichar(50),
  date date output,
  flagHours double output,
  clockHours double output,
  teamProf double output,
  payHours double output,
  regularPay money output)
BEGIN
/*
INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 
  'jwesterhausen@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) 
FROM system.iota;

EXECUTE PROCEDURE GetTpTechDetail('[5c639abe-3706-964d-ab37-6c6fc3a7c29a]');
*/
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
INSERT INTO __output
SELECT thedate, 
  (techflaghoursday + techOtherHoursDay + techTrainingHoursDay + 
    TechFlagHourAdjustmentsDay) AS "Flag Hours",
  techclockhoursday AS "clock hours", 
  b.teamprofpptd AS "team prof", 
  round(techclockhoursday*b.teamprofpptd/100, 2) AS "Pay Hours",
  round(techtfrrate*techclockhoursday*b.teamprofpptd/100, 2) AS "Regular Pay"
FROM tpdata a
LEFT JOIN (
  SELECT teamprofpptd
  FROM tpdata  
  WHERE techkey = @TechKey
    AND curdate() BETWEEN payperiodstart AND payperiodend 
    AND thedate = (
      SELECT MAX(thedate)
      FROM tpdata
      WHERE techkey = @TechKey
        AND curdate() BETWEEN payperiodstart AND payperiodend 
        AND teamprofpptd <> 0)) b on 1 = 1 
WHERE techkey = @TechKey
  AND curdate() BETWEEN payperiodstart AND payperiodend;
END;  


alter PROCEDURE GetTpTechTotal (
  SessionID cichar(50),
  flagHours double output,
  clockHours double output,
  teamProf double output,
  payHours double output,
  regularPay money output)
BEGIN
/*

INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 
  'bcahalan@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) 
FROM system.iota;

EXECUTE PROCEDURE GetTpTechTotal('[5c639abe-3706-964d-ab37-6c6fc3a7c29a]');
*/

DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
@UserName = (
  SELECT UserName 
  FROM sessions 
  WHERE sessionID = (SELECT SessionID FROM __input) AND now() <= ValidThruTS);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
INSERT INTO __output

SELECT
  (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd + 
    TechFlagHourAdjustmentspptd) AS "Flag Hours",
  techclockhourspptd AS "clock hours", 
  teamprofpptd AS "team prof", 
  round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
  round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS "Regular Pay"
FROM tpdata a
WHERE techkey = @TechKey
  AND thedate = (SELECT MAX(thedate) FROM tpdata);
END;  

CREATE PROCEDURE GetTechOtherHoursDetail (
  sessionID cichar(50),
  theDate date output,
  vacationHours double output,
  ptoHours double output,
  holidayHours double output)
BEGIN
/*  
EXECUTE PROCEDURE GetTechOtherHoursDetail('[5c639abe-3706-964d-ab37-6c6fc3a7c29a]');
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionid string;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;

@date = curdate(); -- (SELECT date FROM __input);
@sessionid = (SELECT SessionID FROM __input);
@payperiodseq = (
  SELECT DISTINCT 
  payperiodseq 
  FROM tpData WHERE theDate = @date);
@UserName = (SELECT UserName FROM sessions WHERE sessionID = @sessionid);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
INSERT INTO __output    
SELECT thedate, techvacationhoursday AS vacationHours, 
  techptohoursday AS ptoHours, 
  techholidayhoursday AS holidayHours
FROM tpdata
WHERE payperiodseq = @payperiodseq
  AND techkey = @techkey
  AND techvacationhoursday + techptohoursday + techholidayhoursday <> 0;
END;  

CREATE PROCEDURE GetTechOtherHoursTotal (
  sessionID cichar(50),
  vacationHours double output,
  ptoHours double output,
  holidayHours double output)
BEGIN
/*  
EXECUTE PROCEDURE GetTechOtherHoursTotal('[5c639abe-3706-964d-ab37-6c6fc3a7c29a]');
*/ 
-- tech vac pto hol, daily detail PPTD Total   
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionid string;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;

@date = curdate(); -- (SELECT date FROM __input);
@sessionid = (SELECT SessionID FROM __input);
@payperiodseq = (
  SELECT DISTINCT payperiodseq 
  FROM tpData WHERE theDate = @date);
@UserName = (SELECT UserName FROM sessions WHERE sessionID = @sessionid);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
SELECT sum(techvacationhoursday) AS vacationHours, 
  sum(techptohoursday) AS ptoHours, 
  sum(techholidayhoursday) AS holdayHours
FROM tpdata
WHERE payperiodseq = @payperiodseq
  AND techkey = @techkey
  AND techvacationhoursday + techptohoursday + techholidayhoursday <> 0;  
END;  


-- getTechPayCalc
ALTER PROCEDURE GetTechPayCalc(
  sessionID cichar(50),
  teamFlagHours double OUTPUT,
  teamClockHours double OUTPUT,
  clockHours double OUTPUT,
  payRate money OUTPUT)
BEGIN
/*
EXECUTE PROCEDURE getTechPayCalc('[5c639abe-3706-964d-ab37-6c6fc3a7c29a]')
*/  
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionid string;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;  

@date = curdate(); -- (SELECT date FROM __input);
@sessionid = (SELECT SessionID FROM __input );
@payperiodseq = (
  SELECT DISTINCT payperiodseq 
  FROM tpData 
  WHERE theDate = @date);
@UserName = (SELECT UserName FROM sessions WHERE sessionID = @sessionid);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);  
INSERT INTO __output   
SELECT teamFlagHoursPPTD, teamClockhoursPPTD, techClockHoursPPTD, techTFRRate
FROM tpdata a
WHERE a.thedate = @date
  AND a.techkey = @techkey;
END;      
  
--/> Tech Splash --------------------------------------------------------------/> Tech Splash
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
--< Team Leader Splash --------------------------------------------------------< Team Leader Splash 
-- team leaders summary  
  
DECLARE @sessionid string;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
DECLARE @date date;
DECLARE @payperiodseq integer;
@sessionid = 'fbfd2e7983f562498e4289c2ad95f6ef'; 
--(SELECT SessionID FROM __input) AND now() <= ValidThruTS)
@UserName = (SELECT UserName FROM sessions WHERE sessionID = @sessionid);
@date = curdate(); -- (SELECT date FROM __input);
@payperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = @date);

SELECT distinct TRIM(c.firstname) + ' ' + TRIM(c.lastname), a.payperiodstart, 
  a.payperiodend
FROM tpdata a
INNER JOIN tpTechs b on a.techkey = b.techkey
INNER JOIN tpEmployees c on b.employeenumber = c.employeenumber
WHERE c.Username = @UserName
  AND payperiodseq = @payperiodseq;
  
-- team leaders shop totals detail 
DECLARE @date date;
DECLARE @payperiodseq integer;
@date = curdate(); -- (SELECT date FROM __input);
@payperiodseq = (SELECT DISTINCT payperiodseq FROM tpData WHERE thedate = @date);
SELECT a.*, b.RegularPay
FROM (
  SELECT distinct teamname, 
    (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + 
    TeamFlagHourAdjustmentsPPTD) AS FlagHours,
    teamclockhourspptd, teamprofpptd,
    round(teamclockhourspptd*teamprofpptd/100, 2) AS payHours
  FROM tpData
  WHERE thedate = @date) a -- curdate() ) a-- (SELECT MAX(thedate) FROM tpdata)) a
LEFT JOIN (
SELECT teamname, SUM(RegularPay) AS RegularPay
  FROM (
  SELECT distinct teamname, teamkey, technumber, techkey,
    round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
  FROM tpData
  WHERE thedate = @date) r -- curdate() ) r-- (SELECT MAX(thedate) FROM tpdata)) r
GROUP BY teamname) b on a.teamname = b.teamname;    

-- team leaders shop totals detail total
DECLARE @date date;
DECLARE @payperiodseq integer;
@date = curdate(); -- (SELECT date FROM __input);
@payperiodseq = (SELECT DISTINCT payperiodseq FROM tpData WHERE thedate = @date);
SELECT 'Total', 
  sum(FlagHours) AS flagHours, SUM(clockhours) AS clockHours, 
    round(SUM(FlagHours)/SUM(ClockHours), 2) AS shopProf,
  sum(PayHours) AS payHours, SUM(regularPay) AS regularPay
FROM (
  SELECT a.*, b.RegularPay 
  FROM (
    SELECT distinct teamname, 
      (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + 
        TeamFlagHourAdjustmentsPPTD) AS FlagHours,
      teamclockhourspptd AS ClockHours, teamprofpptd,
      round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
    FROM tpData
    WHERE thedate = @date) a
  LEFT JOIN (
  SELECT teamname, SUM(RegularPay) AS RegularPay
    FROM (
    SELECT distinct teamname, teamkey, technumber, techkey,
      round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
    FROM tpData
    WHERE thedate  = @date) r
  GROUP BY teamname) b on a.teamname = b.teamname) s;
  
-- team leader my team detail
-- for a team, pptd for each tech on that team
DECLARE @sessionid string;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @teamKey integer;
@sessionid = 'fbfd2e7983f562498e4289c2ad95f6ef'; 
--(SELECT SessionID FROM __input) AND now() <= ValidThruTS)
@UserName = (SELECT UserName FROM sessions WHERE sessionID = @sessionid);
@date = curdate(); -- (SELECT date FROM __input);
@payperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = @date);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
@teamKey = (
  SELECT DISTINCT teamKey
  FROM tpData
  WHERE techKey = @techKey);     
SELECT TRIM(firstName) + ' ' + lastName, techFlagHoursPPTD, techClockHoursPPTD,
  teamProfPPTD, round(techClockHoursPPTD * teamProfPPTD/100, 2) AS payHours, 
  round(techClockHoursPPTD * teamProfPPTD/100 * techTfrRate, 2) AS wrenchPay
FROM tpData a
WHERE thedate = @date  
  AND teamKey = @teamKey;
  
-- team leader my team total
DECLARE @sessionid string;
DECLARE @UserName cichar(50);
DECLARE @TechKey integer;
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @teamKey integer;
@sessionid = 'fbfd2e7983f562498e4289c2ad95f6ef'; 
--(SELECT SessionID FROM __input) AND now() <= ValidThruTS)
@UserName = (SELECT UserName FROM sessions WHERE sessionID = @sessionid);
@date = curdate(); -- (SELECT date FROM __input);
@payperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = @date);
@TechKey = (
  SELECT techkey
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName);
@teamKey = (
  SELECT DISTINCT teamKey
  FROM tpData
  WHERE techKey = @techKey);     
SELECT 'Total', sum(techFlagHoursPPTD) AS flagHours, 
  sum(techClockHoursPPTD) AS clockHours, teamProfPPTD AS teamProf, 
  sum(round(techClockHoursPPTD * teamProfPPTD/100, 2))  AS payHours, 
  sum(round(techClockHoursPPTD * teamProfPPTD/100 * techTfrRate, 2)) AS wrenchPay
FROM tpData a
WHERE thedate = @date  
  AND teamKey = @teamKey
GROUP BY teamProfPPTD; 

--/> Team Leader Splash -------------------------------------------------------/> Team Leader Splash
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
--< Manager Splash ------------------------------------------------------------< Manager Splash
-- 12/4/13 refactored AND renamed
alter PROCEDURE GetManagerSummary(
  SessionID CICHAR ( 50 ),
  fromDate date output,
  thruDate date output)
BEGIN 
/*
EXECUTE PROCEDURE getManagerSummary('a4c456a2fb46f54a863f3e933bf232c0');
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
@date = curdate(); 
-- (SELECT date FROM __input); -- will eventually be an input parameterb
@payperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = @date);
@sessionID = (SELECT sessionID FROM __input);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN   
  INSERT INTO __output
  SELECT distinct payperiodstart, payperiodend
  FROM tpdata 
  WHERE payperiodseq = @payperiodseq;
ENDIF;  
END; 


CREATE PROCEDURE GetTpManagerSummary
   ( 
      SessionID CICHAR ( 50 ),
      payPeriod cichar(65),
	  payPeriodSequence integer)
   ) 
BEGIN 
/*
EXECUTE PROCEDURE getManagerSummary('[4d5b3c10-bd34-4847-82d9-8d21ba8c61c0]');
*/
-- DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @fromPayPeriodSeq integer;
DECLARE @thruPayPeriodSeq integer;
@thrupayperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = curdate());
@fromPayPeriodSeq = @thrupayperiodseq - 2;
/*
@sessionID = (SELECT sessionID FROM __input);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN   
*/
--  INSERT INTO __output
  SELECT DISTINCT payperiodselectformat, payperiodseq
  FROM tpdata 
  --WHERE thedate = (SELECT MAX(thedate) FROM tpdata);
  WHERE payperiodseq BETWEEN  @frompayperiodseq AND @thrupayperiodseq;
-- ENDIF;  

END; 



alter PROCEDURE GetTeamTotals
   ( 
      sessionID CICHAR ( 50 ),
      teamName CICHAR ( 52 ) OUTPUT,
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay Money OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetTeamTotals('[5d321fb6-d565-cb41-b185-64a663c8165c]');
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
@date = curdate(); 
-- (SELECT date FROM __input); -- will eventually be an input parameterb
@payperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = @date);
@sessionID = (SELECT sessionID FROM __input);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN 
  INSERT INTO __output
  SELECT a.*, b.RegularPay
  FROM (
    SELECT distinct teamname, 
      (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
      teamclockhourspptd, teamprofpptd/100,
      round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
    FROM tpData
    WHERE thedate = @date) a
  LEFT JOIN (
  SELECT teamname, SUM(RegularPay) AS RegularPay
    FROM (
    SELECT distinct teamname, teamkey, technumber, techkey,
      round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
    FROM tpData
    WHERE thedate = @date) r
  GROUP BY teamname) b on a.teamname = b.teamname;
ENDIF;  
END;

alter PROCEDURE GetShopTotals
   ( 
      sessionID CICHAR ( 50 ),
      flagHours DOUBLE ( 15 ) OUTPUT,
      clockHours DOUBLE ( 15 ) OUTPUT,
      teamProf DOUBLE ( 15 ) OUTPUT,
      payHours DOUBLE ( 15 ) OUTPUT,
      regularPay Money OUTPUT
   ) 
BEGIN 
/*
EXECUTE PROCEDURE GetShopTotals('[bc1712cb-c6b5-394f-8ac3-f6d62fda3277]');
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
@date = curdate(); 
-- (SELECT date FROM __input); -- will eventually be an input parameterb
@payperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = @date);
@sessionID = (SELECT sessionID FROM __input);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN   
  INSERT INTO __output
  SELECT sum(FlagHours), SUM(clockhours), round(SUM(FlagHours)/SUM(ClockHours), 4),
    sum(PayHours), SUM(regularPay)
  FROM (
    SELECT a.*, b.RegularPay 
    FROM (
      SELECT distinct teamname, 
        (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS FlagHours,
        teamclockhourspptd AS ClockHours, teamprofpptd,
        round(teamclockhourspptd*teamprofpptd/100, 2) AS PayHours
      FROM tpData
      WHERE thedate = @date) a--(SELECT MAX(thedate) FROM tpdata)) a
    LEFT JOIN (
    SELECT teamname, SUM(RegularPay) AS RegularPay
      FROM (
      SELECT distinct teamname, teamkey, technumber, techkey,
        round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS RegularPay
      FROM tpData
      WHERE thedate = @date) r
    GROUP BY teamname) b on a.teamname = b.teamname) s;
  ENDIF;  
END;


alter PROCEDURE GetEmployeeTotals(
  sessionID cichar(50),
  teamName cichar(52) output,
  tech cichar(60) output,
  flagHours double output,
  clockHours double output,
  teamProf double output,
  payHours double output,
  regularPay money output)
BEGIN
/*
EXECUTE PROCEDURE GetEmployeeTotals('76caaa1bbcd4714186837ef5292f3833');
12/7
  IF user IS a manager return ALL teams
  IF user IS a teamleader, return just the team leaders team
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @userName string;
DECLARE @role string;
@role = 'teamlead';
@date = curdate(); 
-- (SELECT date FROM __input); -- will eventually be an input parameterb
@payperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = @date);
@sessionID = (SELECT sessionID FROM __input);
@userName = (SELECT username FROM sessions WHERE sessionid = @sessionID);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN   
   INSERT INTO __output
  SELECT top 1000 *
  FROM (
    SELECT teamname, trim(firstname) + ' '  + lastname AS tech, 
      (techflaghourspptd + techOtherHourspptd + techTrainingHourspptd + TechFlagHourAdjustmentspptd) AS "Flag Hours",
      techclockhourspptd AS "clock hours", 
      teamprofpptd AS "team prof", round(techclockhourspptd*teamprofpptd/100, 2) AS "Pay Hours",
      round(techtfrrate*techclockhourspptd*teamprofpptd/100, 2) AS "Regular Pay"
    FROM tpdata a
    WHERE thedate = (SELECT MAX(thedate) FROM tpdata)
      AND 
        CASE 
          WHEN EXISTS (
            SELECT 1
            FROM EmployeeAppRoles
            WHERE username = @userName
              AND role = @role) THEN teamname = (
                SELECT lastname 
                FROM tpEmployees
                WHERE username = @userName)
          ELSE 1 = 1
        END) x

  ORDER BY teamname, tech;
ENDIF;
END;

--/> Manager Splash -----------------------------------------------------------</ Manager Splash

---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
--< Tech RO List --------------------------------------------------------------< Tech RO List
alter PROCEDURE GetTechROsByDay( 
      SessionID CICHAR ( 50 ),
      userName cichar(50),
      techName cichar(60) output,
      flagDate DATE OUTPUT,
      ro CICHAR ( 9 ) OUTPUT,
      line integer output,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 'mkoller@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) FROM system.iota;
EXECUTE PROCEDURE GetTechROsByDay('20cfa660c5d38c4380b3763033893b12', '');
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;    
DECLARE @payperiodseq integer;  
DECLARE @sessionID string;
@sessionID = (SELECT sessionID FROM __input);
@UserName = 
  CASE 
    WHEN (SELECT userName FROM __input) <> '' THEN
      (SELECT userName FROM __input)
    ELSE 
      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
  END;
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@date = curdate(); -- eventually an input parameter   
@payperiodseq = (
  SELECT distinct payperiodseq 
  FROM tpData
  WHERE thedate = @date);
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);  
INSERT INTO __output
SELECT top 1000 *
FROM (
  SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    d.thedate AS flagDate, ro, line, round(sum(a.flaghours * b.weightfactor), 2)
  FROM dds.factRepairOrder a  
  INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dds.dimTech c on b.techkey = c.techkey
  INNER JOIN tpTechs cc on c.technumber = cc.technumber
  INNER JOIN dds.day d on a.flagdatekey = d.datekey
  WHERE flaghours <> 0
    AND a.storecode = 'ry1'
    AND a.flagdatekey IN (
      SELECT datekey
      FROM dds.day
      WHERE thedate BETWEEN @fromDate AND @thruDate) 
    AND a.servicetypekey IN (
      SELECT servicetypekey
      FROM dds.dimservicetype
      WHERE servicetypecode IN ('AM','EM','MR'))   
    AND c.technumber = @techNumber
  GROUP BY TRIM(cc.firstname) + ' ' + TRIM(cc.lastname), d.thedate, a.ro, a.line) x
ORDER BY techName, flagDate, ro, line  ; 
END;

CREATE PROCEDURE GetTechRoTotals( 
      SessionID CICHAR ( 50 ),
      userName cichar(50),
      techName cichar(60) output,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 'mkoller@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) FROM system.iota;
EXECUTE PROCEDURE GetTechRoTotals('20cfa660c5d38c4380b3763033893b12', '');
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;    
DECLARE @payperiodseq integer;  
DECLARE @sessionID string;
@sessionID = (SELECT sessionID FROM __input);
@UserName = 
  CASE 
    WHEN (SELECT userName FROM __input) <> '' THEN
      (SELECT userName FROM __input)
    ELSE 
      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
  END;
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@date = curdate(); -- eventually an input parameter   
@payperiodseq = (
  SELECT distinct payperiodseq 
  FROM tpData
  WHERE thedate = @date);
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);  
INSERT INTO __output
  SELECT TRIM(cc.firstname) + ' ' + TRIM(cc.lastname) AS techName,
    round(sum(a.flaghours * b.weightfactor), 2)
  FROM dds.factRepairOrder a  
  INNER JOIN dds.brTechGroup b on a.techgroupkey = b.techgroupkey
  INNER JOIN dds.dimTech c on b.techkey = c.techkey
  INNER JOIN tpTechs cc on c.technumber = cc.technumber
  INNER JOIN dds.day d on a.flagdatekey = d.datekey
  WHERE flaghours <> 0
    AND a.storecode = 'ry1'
    AND a.flagdatekey IN (
      SELECT datekey
      FROM dds.day
      WHERE thedate BETWEEN @fromDate AND @thruDate) 
    AND a.servicetypekey IN (
      SELECT servicetypekey
      FROM dds.dimservicetype
      WHERE servicetypecode IN ('AM','EM','MR'))   
    AND c.technumber = @techNumber
  GROUP BY TRIM(cc.firstname) + ' ' + TRIM(cc.lastname);
END;

CREATE PROCEDURE GetTechRosAdjustmentsByDay (
  SessionID cichar(50),
  userName cichar(50),
  techName cichar(60) output,
  adjustmentDate date output,
  ro cichar(9) output,
  line integer output, 
  type cichar(24) output,
  flagHours double output)
BEGIN
/*
INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 'gevenseon@rydellchev.com', 
  timestampadd(sql_tsi_hour, 4, now()) FROM system.iota;
EXECUTE PROCEDURE GetTechRosAdjustmentsByDay('20cfa660c5d38c4380b3763033893b12', '');
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;    
DECLARE @payperiodseq integer;  
DECLARE @sessionID string;
DECLARE @techName string;
@sessionID = (SELECT sessionID FROM __input);
@UserName = 
  CASE 
    WHEN (SELECT userName FROM __input) <> '' THEN
      (SELECT userName FROM __input)
    ELSE 
      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
  END;
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@techName = (
  SELECT TRIM(firstName) + ' ' + lastName
  FROM tpEmployees
  WHERE username = @userName);    
@date = curdate(); -- eventually an input parameter   
@payperiodseq = (
  SELECT distinct payperiodseq 
  FROM tpData
  WHERE thedate = @date);
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);   
INSERT INTO __output
  SELECT @techName, thedate, ro, line,
  CASE Source
    WHEN 'XTIM' THEN 'Adjust after Close'
    WHEN 'NEG' THEN 'Negative Hours'
    WHEN 'ADJ' THEN 'Policy Adjust'
  END AS Type, 
  CASE Source
    WHEN 'XTIM' THEN  -1*Hours 
    else Hours
  END as flaghours   
  FROM dds.stgFlagTimeAdjustments 
  WHERE storecode = 'RY1'
    AND technumber = @TechNumber
    AND thedate BETWEEN @fromDate AND @thruDate;
END;  

alter PROCEDURE GetTechAdjustmentTotals( 
      SessionID CICHAR ( 50 ),
      userName cichar(50),
      techName cichar(60) output,
      flagHours DOUBLE ( 15 ) OUTPUT
   ) 
BEGIN 
/*
INSERT INTO sessions (sessionid, username, validthruts)
SELECT (SELECT newidstring(n) FROM system.iota), 'mkoller@rydellchev.com', timestampadd(sql_tsi_hour, 4, now()) FROM system.iota;
EXECUTE PROCEDURE GetTechAdjustmentTotals('20cfa660c5d38c4380b3763033893b12', '');
*/
DECLARE @UserName string;
DECLARE @TechNumber string;
DECLARE @date date;
DECLARE @fromdate date;
DECLARE @thrudate date;    
DECLARE @payperiodseq integer;  
DECLARE @sessionID string;
DECLARE @techName string;
@sessionID = (SELECT sessionID FROM __input);
@UserName = 
  CASE 
    WHEN (SELECT userName FROM __input) <> '' THEN
      (SELECT userName FROM __input)
    ELSE 
      (SELECT UserName FROM sessions WHERE sessionID = @sessionID AND now() <= ValidThruTS)
  END;
@TechNumber = (
  SELECT TechNumber
  FROM tpTechs a
  INNER JOIN tpEmployees b on a.employeenumber = b.employeenumber
    AND b.UserName = @UserName); 
@techName = (
  SELECT TRIM(firstName) + ' ' + lastName
  FROM tpEmployees
  WHERE username = @userName);    
@date = curdate(); -- eventually an input parameter   
@payperiodseq = (
  SELECT distinct payperiodseq 
  FROM tpData
  WHERE thedate = @date);
@fromDate = (
  SELECT distinct payperiodstart
  FROM tpData
  WHERE payperiodseq = @payperiodseq);
@thruDate = (
  SELECT DISTINCT payperiodend
  FROM tpData
  WHERE payperiodseq = @payperiodseq);   
INSERT INTO __output
  SELECT TRIM(firstName) + ' ' + lastName, 
  SUM( 
    CASE Source
      WHEN 'XTIM' THEN  -1*Hours 
      else Hours
    END) as flaghours   
  FROM dds.stgFlagTimeAdjustments a
  INNER JOIN tpTechs b on a.technumber = b.technumber
  WHERE storecode = 'RY1'
    AND a.technumber = @TechNumber
    AND thedate BETWEEN @fromDate AND @thruDate
  GROUP BY TRIM(firstName) + ' ' + lastName;
END;  

--/> Tech RO List -------------------------------------------------------------/> Tech RO List
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
--< Admin ---------------------------------------------------------------------< Admin

alter PROCEDURE GetTpAdmin(
  sessionID cichar(50),
  teamName cichar(52) OUTPUT,
  teamBudget double OUTPUT,
  techName cichar(60) OUTPUT,
  techPercentage double OUTPUT,
  techRate double OUTPUT)
BEGIN
/*
EXECUTE PROCEDURE GetTpAdmin('[78c4438f-a6ea-d740-aad5-5ed77d1c249d]');
EXECUTE PROCEDURE GetTpAdmin('[3b31f64e-93d8-3045-b29d-7f16ac506f8a]');
12/7
  IF user IS a manager return ALL teams
  IF user IS a teamleader, return just the team leaders team
*/
DECLARE @date date;
DECLARE @payperiodseq integer;
DECLARE @sessionID string;
DECLARE @userName string;
DECLARE @role string;

@role = 'teamlead';
@date = curdate(); 
-- (SELECT date FROM __input); -- will eventually be an input parameterb
@payperiodseq = (SELECT distinct payperiodseq FROM tpData WHERE thedate = @date);
@sessionID = (SELECT sessionID FROM __input);
@userName = (SELECT username FROM sessions WHERE sessionid = @sessionID);
IF EXISTS (
  SELECT 1 
  FROM sessions
  WHERE sessionid = @sessionid
  AND now() <= ValidThruTS)
THEN   
  INSERT INTO __output
  SELECT top 100 *
  FROM (
    SELECT m.teamName, 
      m.teamCensus * n.poolPercentage * n.effectiveLaborRate AS teamBudget,
      trim(p.firstname) + ' '  + p.lastname AS techName,
      q.techTeamPercentage, 
      round(m.teamCensus * n.poolPercentage * n.effectiveLaborRate * q.techTeamPercentage, 2) AS techRate
    FROM (
      SELECT a.departmentKey, a.teamKey, a.teamName, COUNT(*) AS teamCensus 
      FROM tpTeams a
      INNER JOIN tpTeamTechs b ON a.teamKey = b.teamKey
        AND @date BETWEEN b.fromDate AND b.thruDate
      WHERE @date BETWEEN a.fromDate AND a.thruDate
      GROUP BY a.departmentKey, a.teamKey, a.teamName) m
    LEFT JOIN (
      SELECT  * 
      FROM tpShopValues
      WHERE @date BETWEEN fromDate AND thruDate) n ON m.departmentKey = n.departmentKey
    LEFT JOIN tpTeamTechs o ON m.teamKey = o.teamKey
      AND @date BETWEEN o.fromDate AND o.thruDate  
    LEFT JOIN tpTechs p ON o.techKey = p.techKey  
    LEFT JOIN tpTechValues q ON p.techKey = q.techKey
      AND @date BETWEEN q.fromDate AND q.thruDate
    WHERE 
      CASE 
        WHEN EXISTS (
          SELECT 1
          FROM EmployeeAppRoles
          WHERE username = @userName
            AND role = @role) THEN teamname = (
              SELECT lastname 
              FROM tpEmployees
              WHERE username = @userName)
        ELSE 1 = 1
      END) x  
	ORDER BY teamName, techName;
ENDIF;
END;

--/> Admin --------------------------------------------------------------------/> Admin