SELECT payperiodstart,payperiodend, lastname, firstname, techteampercentage,
  techhourlyrate,techtfrrate,techclockhourspptd,techflaghourspptd,
	teamprofpptd
	
-- SELECT *	
INTO #tpdata
FROM (
SELECT a.*
FROM tpdata a
JOIN dds.day b on a.thedate = b.thedate
  AND b.thedate = biweeklypayperiodenddate
WHERE a.employeenumber = '1106421'	

UNION

SELECT a.*
FROM tpdata_archive_thru_2021 a
JOIN dds.day b on a.thedate = b.thedate
  AND b.thedate = biweeklypayperiodenddate
WHERE a.employeenumber = '1106421') b
ORDER BY thedate DESC


/********

********/
DECLARE @department integer;
DECLARE @payRollEnd date; 
DECLARE @payRollStart date;
@department = 18;
@payRollStart = '12/05/2021';
@payRollEnd = '12/18/2021';
SELECT team,lastname,firstname,employeenumber AS "emp #",ptorate,commrate,bonusrate,teamprof,
  flaghours,adjustments,total_flaghours,clockhours,vacationhours,ptohours,
  holidayhours,total,guarantee,
  CASE
    WHEN guarantee IS NULL THEN total
	WHEN guarantee > total THEN guarantee
	ELSE total
  END AS "total pay"  
/*  
  holidayhours,total,guarantee,snap_cell,ot_variance,
  CASE
    WHEN guarantee IS NULL THEN total + coalesce(snap_cell, 0) + coalesce(ot_variance, 0)
	ELSE guarantee + coalesce(snap_cell, 0) + coalesce(ot_variance, 0)
  END AS "total pay"
*/  
FROM (  
  SELECT x.*, 
    round(commHours * teamProf/100 * commRate, 2) AS commPay,
    round(bonusHours * teamProf/100 * bonusRate, 2) AS bonusPay,
    round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS ptoPay,
    round(commHours * teamProf/100 * commRate, 2) +  
    round(bonusHours * teamProf/100 * bonusRate, 2) +
    round(ptoRate * (vacationHours + ptoHours + holidayHours), 2) AS total,
	y.guarantee
--    y.guarantee, y.snap_cell, ot_variance
  FROM (  
    select d.team, d.lastname, d.firstname, d.employeenumber, d.ptoRate, d.commRate, 
      d.bonusRate, d.teamProf, d.flagHours, d.adjustments, 
      d.flaghours + d.adjustments as total_flaghours,e.clockHours, e.vacationHours, 
      e.ptoHours, e.holidayHours, e.totalHours,
    -- *a*  
      CASE 
        WHEN e.totalHours > 90 and teamprof >= 100 THEN e.clockhours - (totalhours - 90) 
        else e.clockHours 
      end AS commHours,
      round(
        CASE 
          WHEN totalHours > 90 and teamprof >= 100 THEN totalHours - 90 
          ELSE 0 
        END, 2) AS bonusHours
    FROM (
      select c.teamname AS Team, lastname, firstname, employeenumber, 
        techhourlyrate AS ptoRate, round(techtfrrate, 2) AS CommRate, 
        2 * round(techtfrrate, 2) AS BonusRate, -- teamprofpptd AS teamProf, 
        TechFlagHoursPPTD as FlagHours, techFlagHourAdjustmentsPPTD as Adjustments,
        teamprofpptd AS TeamProf   
      FROM #tpdata a
      INNER JOIN tpteamtechs b on a.techkey = b.techkey
        AND a.teamkey = b.teamkey
    	AND @payrollend BETWEEN b.fromdate AND b.thrudate
    --    AND b.thrudate > curdate()
      LEFT JOIN tpTeams c on b.teamKey = c.teamKey
      WHERE thedate = @payRollEnd
        AND a.departmentKey = @department) d
    LEFT JOIN (
      select employeenumber, techclockhourspptd AS clockHours, 
        techvacationhourspptd AS vacationHours,  techptohourspptd AS ptoHours, 
        techholidayhourspptd AS holidayHours,
        techclockhourspptd + techvacationhourspptd + techptohourspptd + techholidayhourspptd AS totalHours  
      FROM #tpdata a
      INNER JOIN tpteamtechs b on a.techkey = b.techkey
        AND a.teamkey = b.teamkey
    	AND @payrollend BETWEEN b.fromdate AND b.thrudate
    --    AND b.thrudate > curdate()
      LEFT JOIN tpTeams c on b.teamKey = c.teamKey
      WHERE thedate = @payRollEnd
        AND a.departmentKey = @department) e on d.employeenumber = e.employeenumber) x
  LEFT JOIN ( -- guarantee for the blueprint team
    SELECT employee_number, reg + ot + pto + bonus AS guarantee
    FROM (
      SELECT aa.name, aa.employee_number, round(aa.hourly_rate * bb.clock, 2) AS reg, 
        round(aa.hourly_rate * 1.5 * bb.ot, 2) AS ot, 
        round(aa.pto_rate * (vac + pto + hol), 2) AS pto,
        case
          when bb.clock + bb.ot + bb.pto > 90 THEN  ((bb.clock + bb.ot + bb.pto) - 90) * 2 * aa.hourly_rate
      	ELSE 0
        END AS bonus
      FROM blueprint_guarantee_rates aa
      JOIN (			 
        select c.name, c.employeenumber, SUM(a.regularhours) AS clock, SUM(a.overtimehours) AS ot,
          SUM(a.vacationhours) AS vac, SUM(a.ptohours) AS pto, SUM(a.holidayhours) AS hol 
        FROM dds.edwclockhoursfact a
        JOIN dds.day b on a.datekey = b.datekey
          AND b.thedate BETWEEN @payRollStart AND @payRollEnd
        JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
          AND c.employeenumber IN ('145801','1106399','1117901','152410')
        GROUP BY c.name, c.employeenumber) bb on aa.employee_number = bb.employeenumber
      WHERE aa.thru_date = '12/31/9999') cc) y on x.employeenumber = y.employee_number) z		  		
/*	  
  LEFT JOIN tp_main_shop_guarantee_spiffs y on x.employeenumber = y.employee_number	
    AND y.the_date = @payRollEnd) y
*/	
WHERE employeenumber = '1106421'
ORDER BY lastname --team, firstname, lastname