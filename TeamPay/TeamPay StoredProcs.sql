    
Proc: TeamPayDailyforTech
Inputs: Tech
Outputs: Date, Flag Hours, Clock hours, Team Prof, Pay Hours, Regular Pay
Scope: Current Pay Period

SELECT DISTINCT a.gtdate, b.ro, b.description, technumber, b.paymenttypekey, b.flaghours
FROM #glRO a
LEFT JOIN #roTech b on a.ro = b.ro
  AND a.paymenttypekey = b.paymenttypekey;
  
  
select * FROM #rotech
SELECT * FROM #glro

-- team flag hours
SELECT teamname, round(SUM(flaghours), 2) AS flaghours
FROM (
  SELECT distinct teamname, a.gtdate, b.technumber, c.lastname, flaghours
  FROM #glRO a
  LEFT JOIN #roTech b on a.ro = b.ro
    AND a.paymenttypekey = b.paymenttypekey
  INNER JOIN tpTechs c on b.technumber = c.technumber 
  INNER JOIN tpTeamTechs d on c.techkey = d.techkey
  INNER JOIN tpTeams e on d.teamkey = e.teamkey
  WHERE b.technumber <> 'N/A'  
    AND a.gtdate BETWEEN d.fromdate AND d.thrudate) x
GROUP BY teamname  
-- tech flag hours BY day    
SELECT teamname, gtdate, technumber, lastname, round(SUM(flaghours), 2)
FROM (
  SELECT distinct teamname, a.gtdate, b.technumber, c.lastname, flaghours
  FROM #glRO a
  LEFT JOIN #roTech b on a.ro = b.ro
    AND a.paymenttypekey = b.paymenttypekey
  INNER JOIN tpTechs c on b.technumber = c.technumber 
  INNER JOIN tpTeamTechs d on c.techkey = d.techkey
  INNER JOIN tpTeams e on d.teamkey = e.teamkey
  WHERE b.technumber <> 'N/A'  
    AND a.gtdate BETWEEN d.fromdate AND d.thrudate) x
GROUP BY teamname, gtdate, technumber, lastname   

-- clockhours BY tech BY day
SELECT thedate, technumber, lastname, round(SUM(clockhours), 2) AS clockhours
FROM (
  SELECT c.thedate, b.employeenumber, a.clockhours
  FROM dds.edwClockHoursFact a 
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.day c on a.datekey  = c.datekey
  WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
INNER JOIN tpTechs b on a.employeenumber = b.employeenumber 
GROUP BY thedate, technumber, lastname

-- team clockhours for payperiod
select teamname, round(SUM(clockhours), 2) AS clockhours
FROM (
  SELECT c.thedate, b.employeenumber, a.clockhours
  FROM dds.edwClockHoursFact a 
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.day c on a.datekey  = c.datekey
  WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
INNER JOIN tpTechs b on a.employeenumber = b.employeenumber
INNER JOIN tpTeamTechs c on b.techkey = c.techkey
INNER JOIN tpTeams d on c.teamkey = d.teamkey
GROUP BY teamname

-- team proficiency payperiod to date
SELECT a.teamname, round(flaghours/clockhours, 2)
FROM (
SELECT teamname, round(SUM(flaghours), 2) AS flaghours
FROM (
  SELECT distinct teamname, a.gtdate, b.technumber, c.lastname, flaghours
  FROM #glRO a
  LEFT JOIN #roTech b on a.ro = b.ro
    AND a.paymenttypekey = b.paymenttypekey
  INNER JOIN tpTechs c on b.technumber = c.technumber 
  INNER JOIN tpTeamTechs d on c.techkey = d.techkey
  INNER JOIN tpTeams e on d.teamkey = e.teamkey
  WHERE b.technumber <> 'N/A'  
    AND a.gtdate BETWEEN d.fromdate AND d.thrudate) x
GROUP BY teamname) a
LEFT JOIN (
select teamname, round(SUM(clockhours), 2) AS clockhours
FROM (
  SELECT c.thedate, b.employeenumber, a.clockhours
  FROM dds.edwClockHoursFact a 
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.day c on a.datekey  = c.datekey
  WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
INNER JOIN tpTechs b on a.employeenumber = b.employeenumber
INNER JOIN tpTeamTechs c on b.techkey = c.techkey
INNER JOIN tpTeams d on c.teamkey = d.teamkey
GROUP BY teamname) b on a.teamname = b.teamname

-- need team proficiency payperiod to date for each day

-- team flag hours BY day
SELECT teamname, gtdate, round(SUM(flaghours), 2) AS flaghours
FROM (
  SELECT distinct teamname, a.gtdate, b.technumber, c.lastname, flaghours
  FROM #glRO a
  LEFT JOIN #roTech b on a.ro = b.ro
    AND a.paymenttypekey = b.paymenttypekey
  INNER JOIN tpTechs c on b.technumber = c.technumber 
  INNER JOIN tpTeamTechs d on c.techkey = d.techkey
  INNER JOIN tpTeams e on d.teamkey = e.teamkey
  WHERE b.technumber <> 'N/A'  
    AND a.gtdate BETWEEN d.fromdate AND d.thrudate) x
GROUP BY teamname, gtdate

-- team clockhours BY day
select teamname, thedate, round(SUM(clockhours), 2) AS clockhours
FROM (
  SELECT c.thedate, b.employeenumber, a.clockhours
  FROM dds.edwClockHoursFact a 
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.day c on a.datekey  = c.datekey
  WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
INNER JOIN tpTechs b on a.employeenumber = b.employeenumber
INNER JOIN tpTeamTechs c on b.techkey = c.techkey
INNER JOIN tpTeams d on c.teamkey = d.teamkey
GROUP BY teamname, thedate

SELECT a.*, b.clockhours,
  CASE
    WHEN flaghours = 0 OR clockhours = 0 THEN 0
    ELSE round(flaghours/clockhours, 2)
  END AS Prof
FROM (
  -- team flag hours BY day
  SELECT teamname, gtdate, round(SUM(flaghours), 2) AS flaghours
  FROM (
    SELECT distinct teamname, a.gtdate, b.technumber, c.lastname, flaghours
    FROM #glRO a
    LEFT JOIN #roTech b on a.ro = b.ro
      AND a.paymenttypekey = b.paymenttypekey
    INNER JOIN tpTechs c on b.technumber = c.technumber 
    INNER JOIN tpTeamTechs d on c.techkey = d.techkey
    INNER JOIN tpTeams e on d.teamkey = e.teamkey
    WHERE b.technumber <> 'N/A'  
      AND a.gtdate BETWEEN d.fromdate AND d.thrudate) x
  GROUP BY teamname, gtdate) a
LEFT JOIN (
select teamname, thedate, round(SUM(clockhours), 2) AS clockhours
FROM (
  SELECT c.thedate, b.employeenumber, a.clockhours
  FROM dds.edwClockHoursFact a 
  INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dds.day c on a.datekey  = c.datekey
  WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
INNER JOIN tpTechs b on a.employeenumber = b.employeenumber
INNER JOIN tpTeamTechs c on b.techkey = c.techkey
INNER JOIN tpTeams d on c.teamkey = d.teamkey
GROUP BY teamname, thedate) b on a.teamname = b.teamname AND a.gtdate = b.thedate

-- need to DO a running total for the pay period
-- what i know for sure IS that each payperiod IS 14 days long
-- added dayinbiweeklypayperiod to dds.day

--Team proficiency (running total) for each day IN the payperiod
    
 select *
 INTO #olson
 FROM (   
    SELECT thedate, dayinbiweeklypayperiod
    FROM dds.day
    WHERE curdate() BETWEEN biweeklypayperiodstartdate AND biweeklypayperiodenddate
      AND thedate <= curdate()) a
  LEFT JOIN (
  select teamname, thedate, coalesce(round(SUM(clockhours), 2), 0) AS clockhours
    FROM (
      SELECT c.thedate, b.employeenumber, a.clockhours
      FROM dds.edwClockHoursFact a 
      INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
      INNER JOIN dds.day c on a.datekey  = c.datekey
      WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
    INNER JOIN tpTechs b on a.employeenumber = b.employeenumber
    INNER JOIN tpTeamTechs c on b.techkey = c.techkey
    INNER JOIN tpTeams d on c.teamkey = d.teamkey
      AND d.teamname = 'olson'
    GROUP BY teamname, thedate) b on a.thedate = b.thedate    
    
SELECT a.*, 
  clockhours + coalesce((
    SELECT SUM(clockhours) 
    FROM #olson b
    WHERE b.thedate < a.thedate),0) AS running
FROM #olson a
ORDER BY thedate


SELECT a.thedate, a.clockhours, SUM(b.clockhours)
FROM #olson a, #olson b
WHERE b.thedate < a.thedate 
GROUP BY a.thedate, a.clockhours


 
-- this works    
SELECT a.thedate, a.teamname, a.clockhours, SUM(b.clockhours) AS RunningTotal
FROM (   
  select teamname, thedate, coalesce(round(SUM(clockhours), 2), 0) AS clockhours
    FROM (
      SELECT c.thedate, b.employeenumber, a.clockhours
      FROM dds.edwClockHoursFact a 
      INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
      INNER JOIN dds.day c on a.datekey  = c.datekey
      WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
    INNER JOIN tpTechs b on a.employeenumber = b.employeenumber
    INNER JOIN tpTeamTechs c on b.techkey = c.techkey
    INNER JOIN tpTeams d on c.teamkey = d.teamkey
--      AND d.teamname = 'olson'
    GROUP BY teamname, thedate) a, 
  (
  select teamname, thedate, coalesce(round(SUM(clockhours), 2), 0) AS clockhours
    FROM (
      SELECT c.thedate, b.employeenumber, a.clockhours
      FROM dds.edwClockHoursFact a 
      INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
      INNER JOIN dds.day c on a.datekey  = c.datekey
      WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
    INNER JOIN tpTechs b on a.employeenumber = b.employeenumber
    INNER JOIN tpTeamTechs c on b.techkey = c.techkey
    INNER JOIN tpTeams d on c.teamkey = d.teamkey
--      AND d.teamname = 'olson'
    GROUP BY teamname, thedate) b
WHERE b.thedate <= a.thedate     
  AND a.teamname = b.teamname       
GROUP BY a.thedate, a.teamname, a.clockhours     
ORDER BY a.teamname, a.thedate
    
SELECT a.gtdate, a.teamname , a.flaghours, SUM(b.flaghours) AS RunningTotal
FROM (  
  SELECT teamname, gtdate, round(SUM(flaghours), 2) AS flaghours
  FROM (
    SELECT distinct teamname, a.gtdate, b.technumber, c.lastname, flaghours
    FROM #glRO a
    LEFT JOIN #roTech b on a.ro = b.ro
      AND a.paymenttypekey = b.paymenttypekey
    INNER JOIN tpTechs c on b.technumber = c.technumber 
    INNER JOIN tpTeamTechs d on c.techkey = d.techkey
    INNER JOIN tpTeams e on d.teamkey = e.teamkey
    WHERE b.technumber <> 'N/A'  
      AND a.gtdate BETWEEN d.fromdate AND d.thrudate) x
  GROUP BY teamname, gtdate) a,    
  (
  SELECT teamname, gtdate, round(SUM(flaghours), 2) AS flaghours
  FROM (
    SELECT distinct teamname, a.gtdate, b.technumber, c.lastname, flaghours
    FROM #glRO a
    LEFT JOIN #roTech b on a.ro = b.ro
      AND a.paymenttypekey = b.paymenttypekey
    INNER JOIN tpTechs c on b.technumber = c.technumber 
    INNER JOIN tpTeamTechs d on c.techkey = d.techkey
    INNER JOIN tpTeams e on d.teamkey = e.teamkey
    WHERE b.technumber <> 'N/A'  
      AND a.gtdate BETWEEN d.fromdate AND d.thrudate) x
  GROUP BY teamname, gtdate) b
WHERE b.gtdate <= a.gtdate
  AND a.teamname = b.teamname
GROUP BY a.gtdate, a.teamname, a.flaghours
ORDER BY a.teamname, a.gtdate     


SELECT a.thedate, a.teamname, a.clockhours, a.RunningTotal, b.flaghours, b.RunningTotal,
  CASE 
    WHEN a.clockhours = 0 OR b.flaghours = 0 THEN 0
    ELSE round(100 * b.flaghours/a.clockhours, 2)
  END AS DailyTeamProf,
  CASE 
    WHEN a.RunningTotal = 0 OR b.RunningTotal = 0 THEN 0
    ELSE round(100 * b.RunningTotal/a.RunningTotal, 2)
  END AS CumTeamProf  
FROM (
  SELECT a.thedate, a.teamname, a.clockhours, SUM(b.clockhours) AS RunningTotal
  FROM (   
    select teamname, thedate, coalesce(round(SUM(clockhours), 2), 0) AS clockhours
      FROM (
        SELECT c.thedate, b.employeenumber, a.clockhours
        FROM dds.edwClockHoursFact a 
        INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
        INNER JOIN dds.day c on a.datekey  = c.datekey
        WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
      INNER JOIN tpTechs b on a.employeenumber = b.employeenumber
      INNER JOIN tpTeamTechs c on b.techkey = c.techkey
      INNER JOIN tpTeams d on c.teamkey = d.teamkey
  --      AND d.teamname = 'olson'
      GROUP BY teamname, thedate) a, 
    (
    select teamname, thedate, coalesce(round(SUM(clockhours), 2), 0) AS clockhours
      FROM (
        SELECT c.thedate, b.employeenumber, a.clockhours
        FROM dds.edwClockHoursFact a 
        INNER JOIN dds.edwEmployeeDim b on a.employeekey = b.employeekey
        INNER JOIN dds.day c on a.datekey  = c.datekey
        WHERE c.thedate BETWEEN '11/17/2013' AND curdate()) a
      INNER JOIN tpTechs b on a.employeenumber = b.employeenumber
      INNER JOIN tpTeamTechs c on b.techkey = c.techkey
      INNER JOIN tpTeams d on c.teamkey = d.teamkey
  --      AND d.teamname = 'olson'
      GROUP BY teamname, thedate) b
  WHERE b.thedate <= a.thedate     
    AND a.teamname = b.teamname       
  GROUP BY a.thedate, a.teamname, a.clockhours) a
LEFT JOIN (  
  SELECT a.gtdate, a.teamname , a.flaghours, SUM(b.flaghours) AS RunningTotal
  FROM (  
    SELECT teamname, gtdate, round(SUM(flaghours), 2) AS flaghours
    FROM (
      SELECT distinct teamname, a.gtdate, b.technumber, c.lastname, flaghours
      FROM #glRO a
      LEFT JOIN #roTech b on a.ro = b.ro
        AND a.paymenttypekey = b.paymenttypekey
      INNER JOIN tpTechs c on b.technumber = c.technumber 
      INNER JOIN tpTeamTechs d on c.techkey = d.techkey
      INNER JOIN tpTeams e on d.teamkey = e.teamkey
      WHERE b.technumber <> 'N/A'  
        AND a.gtdate BETWEEN d.fromdate AND d.thrudate) x
    GROUP BY teamname, gtdate) a,    
    (
    SELECT teamname, gtdate, round(SUM(flaghours), 2) AS flaghours
    FROM (
      SELECT distinct teamname, a.gtdate, b.technumber, c.lastname, flaghours
      FROM #glRO a
      LEFT JOIN #roTech b on a.ro = b.ro
        AND a.paymenttypekey = b.paymenttypekey
      INNER JOIN tpTechs c on b.technumber = c.technumber 
      INNER JOIN tpTeamTechs d on c.techkey = d.techkey
      INNER JOIN tpTeams e on d.teamkey = e.teamkey
      WHERE b.technumber <> 'N/A'  
        AND a.gtdate BETWEEN d.fromdate AND d.thrudate) x
    GROUP BY teamname, gtdate) b
  WHERE b.gtdate <= a.gtdate
    AND a.teamname = b.teamname
  GROUP BY a.gtdate, a.teamname, a.flaghours) b on a.thedate = b.gtdate 
    AND a.teamname = b.teamname
WHERE a.thedate < curdate()    
ORDER BY a.teamname, a.thedate      