-- current reality  
SELECT a.teamname, a.teamkey, 
  b.census, b.budget, b.poolpercentage,
  d.lastname, d.firstname, d.techkey, 
  e.techteampercentage,
  e.techteampercentage * b.budget AS tech_rate
FROM tpTeams a
INNER JOIN tpteamvalues b on a.teamkey = b.teamkey
  AND b.thrudate > curdate()
INNER JOIN tpteamtechs c on a.teamkey = c.teamkey
  AND c.thrudate > curdate()  
INNER JOIN tptechs d on c.techkey = d.techkey  
INNER JOIN tptechvalues e on d.techkey = e.techkey
  AND e.thrudate > curdate()
WHERE a.departmentkey = 13 
  AND a.thrudate > curdate()
  
  
have been putting off forever
Aaron Lindom termed on 8/31/18 (today IS 1/22/2019, yikes)
  
Aaron Lindom, Team 11 - team of one, term 8/31/18
techkey: 52
teamkey: 40
Remove efffective 8/31/18/18

-- 06/18/22  brandon lamont termed on 6/3

DECLARE @team_key integer;
DECLARE @thru_date date;
DECLARE @tech_key integer;
BEGIN TRANSACTION;
TRY  
  @thru_date = '06/04/2022'; -- last day of pay period
  @team_key = ( --50
    SELECT teamkey
    FROM tpteams
    WHERE teamname = 'Team 20'
  	  AND thrudate > curdate());
  @tech_key = ( --67
    SELECT techkey
    FROM tpTechs
    WHERE lastname = 'lamont'
  	  AND thrudate > curdate());
  -- tpTeams
  UPDATE tpTeamsS
  SET thrudate = @thru_date
  where teamkey = @team_key;
  -- tpTechs
  -- DO nothing
  -- tpTeamTechs
  update tpTeamTechs
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND teamkey = @team_key;
  -- tpTeamValues  
  UPDATE tpTeamValues
  SET thrudate = @thru_date
  WHERE teamkey = @team_key
    AND thrudate > curdate();
  -- tpTechValues
  UPDATE tpTechValues
  SET thrudate = @thru_date
  WHERE techkey = @tech_key
    AND thrudate > curdate();
  
  DELETE FROM tpdata
  WHERE teamkey = @team_key
  AND thedate > @thru_date;

COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    
	
SELECT *
FROM tpdata WHERE teamkey = 50
  AND thedate > '06/04/2022'