-- ALL the techs
SELECT a.teamname, c.firstname, c.lastname, c.technumber, c.employeenumber,
  d.techFlagHoursPPTD, d.techClockHoursPPTD, d.payperiodSTart, curdate() - 1
FROM tpteams a
INNER JOIN tpteamtechs b on a.teamkey = b.teamkey
INNER JOIN tptechs c on b.techkey = c.techkey
LEFT JOIN tpdata d on c.techkey = d.techkey
WHERE c.thrudate > curdate()
  AND b.thrudate > curdate()
  AND d.thedate = curdate() - 1
ORDER BY c.lastname  

-- clock hours
SELECT c.firstname, c.lastname, d.techClockHoursPPTD
FROM tpteams a
INNER JOIN tpteamtechs b on a.teamkey = b.teamkey
INNER JOIN tptechs c on b.techkey = c.techkey
LEFT JOIN tpdata d on c.techkey = d.techkey
WHERE c.thrudate > curdate()
  AND b.thrudate > curdate()
  AND d.thedate = curdate() - 1
ORDER BY c.lastname  

-- flag hours
SELECT c.firstname, c.lastname, c.technumber, d.techFlagHoursPPTD, d.techFlagHourAdjustmentsPPTD
FROM tpteams a
INNER JOIN tpteamtechs b on a.teamkey = b.teamkey
INNER JOIN tptechs c on b.techkey = c.techkey
LEFT JOIN tpdata d on c.techkey = d.techkey
WHERE c.thrudate > curdate()
  AND b.thrudate > curdate()
  AND d.thedate = curdate() - 1
ORDER BY c.technumber 

SELECT thedate, techflaghoursday, techflaghourspptd
FROM tpdata
WHERE technumber = '627'
AND thedate BETWEEN '04/06/2014' AND '04/17/2014'
ORDER BY thedate

SELECT ro, line, flaghours
FROM dds.factRepairOrder
WHERE techkey in (
  SELECT techkey
  FROM dds.dimTech
  WHERE technumber = '627')
AND flagdatekey = (
  SELECT datekey
  FROM dds.day 
  WHERE thedate = '04/07/2014')  

SELECT * 
FROM dds.factrepairorder
WHERE ro = '16150242'
  AND line = 1  