SELECT *
FROM stgArkonaPYHSHDTA
WHERE yhdemp = '1106399' and ypbnum = 1219140

-- FROM db2 query: Payroll - PYHSHDTA - PYHSCDTA - all amounts.sql
select yhdemp, yhsaly,
  sum(yhcrtm) as total, -- 17 Emplr Curr Ret
  sum(yhdhrs) as Hours,  -- 7 Reg Hours
  sum(yhdtgp) as "1 Total Gross",
  sum(yhdtga) as "2 Total Adj Gross",
  sum(yhcfed) as "3 Curr Federal Tax",
  sum(yhcsse) as "4 Curr FICA",
  sum(yhcssm) as "5 Curr Emplr FICA",
  sum(yhcmde) as "6 Curr Employee Medicare",
  sum(yhcmdm) as "7 Curr Employer Medicare",
  sum(yhceic) as "8 YTD EIC Payments",
  sum(yhcst) as "9 Curr State Tax",
  sum(yhcsde) as "10 Curr Employee SDI",
  sum(yhcsdm) as "11 Curr Employer SDI",
  sum(yhccnt) as "12 Curr County Tax",
  sum(yhccty) as "13 Curr City Tax",
  sum(yhcfuc) as "14 Curr FUTA",
  sum(yhcsuc) as "15 Curr SUTA",
  sum(yhccmp) as "16 Workmans Comp",
  sum(yhcrte) as "17 Emply Curr Ret",
  sum(yhcrtm) as "18 Emplr Curr Ret",
  sum(yhdota) as "19 Overtime Amount",
  sum(yhdded) as "20 Total Deduction",
  sum(yhcopy) as "21 Total Other Pay",
  sum(yhcnet) as "22 Current Net",
  sum(yhctax) as "23 Current Tax Tot",
  sum(yhafed) as "1 Curr Adj Fed",
  sum(yhass) as "2 Curr Adj SS",
  sum(yhafuc) as "3 Curr Adj FUTA",
  sum(yhast) as "4 Curr Adj State",
  sum(yhacn) as "5 Curr Adj Cnty",
  sum(yhamu) as "6 Curr Adj City",
  sum(yhasuc) as "7 Curr Adj SUTA",
  sum(yhacm) as "8 Curr Adj Comp",
  sum(yhdvac) as "1 Vacation Taken",
  sum(yhdhol) as "2 Holday Taken",
  sum(yhdsck) as "3 Sick Leave Taken",
  sum(yhavac) as "4 Vacation Acc",
  sum(yhahol) as "5 Holiday Acc",
  sum(yhasck) as "6 Sick Leave Acc",
  sum(yhdhrs) as "7 Reg Hours",
  sum(yhdoth) as "8 Overtime Hours",
  sum(yhdahr) as "9 Alt Pay Hours",
  sum(yhvacd) as "10 Def Vac Hrs",
  sum(yhhold) as "11 Def Hol Hrs"
FROM stgArkonaPYHSHDTA
WHERE yhdemp = '1106399' and ypbnum = 1219140
group by yhdemp

SELECT yhsaly,
   yhcrtm  as total, -- 17 Emplr Curr Ret
   yhdhrs  as Hours,  -- 7 Reg Hours
   yhdtgp  as "1 Total Gross",
   yhdtga  as "2 Total Adj Gross",
   yhcfed  as "3 Curr Federal Tax",
   yhcsse  as "4 Curr FICA",
   yhcssm  as "5 Curr Emplr FICA",
   yhcmde  as "6 Curr Employee Medicare",
   yhcmdm  as "7 Curr Employer Medicare",
   yhceic  as "8 YTD EIC Payments",
   yhcst  as "9 Curr State Tax",
   yhcsde  as "10 Curr Employee SDI",
   yhcsdm  as "11 Curr Employer SDI",
   yhccnt  as "12 Curr County Tax",
   yhccty  as "13 Curr City Tax",
   yhcfuc  as "14 Curr FUTA",
   yhcsuc  as "15 Curr SUTA",
   yhccmp  as "16 Workmans Comp",
   yhcrte  as "17 Emply Curr Ret",
   yhcrtm  as "18 Emplr Curr Ret",
   yhdota  as "19 Overtime Amount",
   yhdded  as "20 Total Deduction",
   yhcopy  as "21 Total Other Pay",
   yhcnet  as "22 Current Net",
   yhctax  as "23 Current Tax Tot",
   yhafed  as "1 Curr Adj Fed",
   yhass  as "2 Curr Adj SS",
   yhafuc  as "3 Curr Adj FUTA",
   yhast  as "4 Curr Adj State",
   yhacn  as "5 Curr Adj Cnty",
   yhamu  as "6 Curr Adj City",
   yhasuc  as "7 Curr Adj SUTA",
   yhacm  as "8 Curr Adj Comp",
   yhdvac  as "1 Vacation Taken",
   yhdhol  as "2 Holday Taken",
   yhdsck  as "3 Sick Leave Taken",
   yhavac  as "4 Vacation Acc",
   yhahol  as "5 Holiday Acc",
   yhasck  as "6 Sick Leave Acc",
   yhdhrs  as "7 Reg Hours",
   yhdoth  as "8 Overtime Hours",
   yhdahr  as "9 Alt Pay Hours",
   yhvacd  as "10 Def Vac Hrs",
   yhhold  as "11 Def Hol Hrs"
FROM stgArkonaPYHSHDTA   
WHERE ypbcyy = 114
  AND yhdemp = '1106399'
ORDER BY yhdemm, yhdedd



SELECT yhdemp, SUM(gross) AS grossPay, SUM(totalPtoPay) AS ptoPay
FROM (
SELECT yhdemp, yhsaly,
   yhdtgp as gross,
   yhdvac as vacHours,
   yhdhol as holHours,
   yhdsck  as ptoHours,
   yhsaly*(yhdvac+yhdhol+yhdsck) AS totalPtoPay,
cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
FROM stgArkonaPYHSHDTA   
WHERE ypbcyy = 114
  AND yhdemp = '1106399'
  AND yhdtgp <> 0) x
GROUP BY yhdemp  


  
SELECT SUM(clockhours) AS clockhours, SUM(vacationhours+ptohours+holidayHours) AS ptoHours
FROM edwClockHoursFact a
INNER JOIN day c on a.datekey = c.datekey
  AND year(c.thedate) = 2014  
WHERE employeekey IN (
  SELECT employeekey
  FROM edwEmployeeDim 
  WHERE employeenumber = '1106399')  