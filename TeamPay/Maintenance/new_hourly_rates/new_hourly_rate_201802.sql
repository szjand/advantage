﻿
!!!!!!!!!!!!! run this against zztest first !!!!!!!!!!!!!!!!!!!!!!!!!!

-- values before updating
-- DROP TABLE #wtf
SELECT a.*, left(trim(trim(b.firstname) + ' ' + trim(lastname)),25) AS name, 
  c.previoushourlyrate -- , round(new_pto_rate - previoushourlyrate, 2)
INTO #wtf  
FROM pto_adj_2021_bs A
left JOIN tptechs b on a.tech_key = b.techkey
LEFT JOIN tptechvalues c on a.tech_key = c.techkey
  AND c.thrudate > curdate()
ORDER BY name  

-------------------------------------------------------------------------
--< 02/14/2021
-------------------------------------------------------------------------
-- 02/28/21 recreate this TABLE for just body shop
-- EACH YEAR A NEW TABLE
DROP TABLE pto_adj_2021_bs;
CREATE TABLE pto_adj_2021_bs(
  tech_key integer,
  new_pto_rate numeric(8,2),
  tech_team_perc double);
-- these INSERT statements generated IN Intranet SQL Scripts\TeamPay\Maintenance\new_hourly_rate_pg.sql  
-- they are ONLY positive changes, no one gets a decrease IN PTO
-- this IS for MAIN SHOP Only at this time AND THESES VALUES INSERTED 02/10/21
/*
insert into pto_adj_2021 values (58,49.65,0.2324);
insert into pto_adj_2021 values (54,33.70,0.2194);
insert into pto_adj_2021 values (73,36.98,0.2323);
insert into pto_adj_2021 values (50,19.36,0.4722);
insert into pto_adj_2021 values (87,18.55,0.4195);
insert into pto_adj_2021 values (9,41.98,0.2642);
insert into pto_adj_2021 values (24,29.24,0.4550);
insert into pto_adj_2021 values (16,25.94,0.1988);
insert into pto_adj_2021 values (11,32.07,0.1952);
insert into pto_adj_2021 values (1,43.57,0.2404);
insert into pto_adj_2021 values (19,21.15,0.4970);
insert into pto_adj_2021 values (13,32.05,0.2193);
insert into pto_adj_2021 values (59,55.91,0.3001);
insert into pto_adj_2021 values (15,37.39,0.4556);
insert into pto_adj_2021 values (88,19.68,0.1549);
insert into pto_adj_2021 values (7,35.41,0.2309);
insert into pto_adj_2021 values (6,30.92,0.2199);
insert into pto_adj_2021 values (79,27.71,0.1822);
insert into pto_adj_2021 values (18,31.66,0.2121);
insert into pto_adj_2021 values (83,43.65,0.5315);
insert into pto_adj_2021 values (89,19.06,0.1458);
insert into pto_adj_2021 values (90,18.34,0.9781);
*/

-- these INSERT statements generated IN Intranet SQL Scripts\TeamPay\Maintenance\new_hourly_rate_pg.sql  
-- they are ONLY positive changes, no one gets a decrease IN PTO
-- this IS for body shop Only at this time AND THESES VALUES INSERTED 02/28/21

insert into pto_adj_2021_bs values (32,38.67,1.0000);
insert into pto_adj_2021_bs values (41,31.10,1.0000);
insert into pto_adj_2021_bs values (34,32.82,0.5286);
insert into pto_adj_2021_bs values (78,23.17,1.0000);
insert into pto_adj_2021_bs values (42,35.73,1.0000);
insert into pto_adj_2021_bs values (43,41.82,0.2400);
insert into pto_adj_2021_bs values (45,30.10,0.1753);
insert into pto_adj_2021_bs values (72,28.81,0.1589);
insert into pto_adj_2021_bs values (35,23.69,1.0000);
insert into pto_adj_2021_bs values (30,35.99,0.5357);
insert into pto_adj_2021_bs values (67,33.71,1.0000);
insert into pto_adj_2021_bs values (85,34.91,1.0000);
insert into pto_adj_2021_bs values (86,19.43,0.1198);
insert into pto_adj_2021_bs values (76,30.70,1.0000);
insert into pto_adj_2021_bs values (77,32.48,1.0000);


FINISH UP ON SUNDAY 02/14/21

-- modify the UPDATE script
-- SELECT * FROM pto_adj_2019
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @tech_percentage double;
DECLARE @tech_key_cur cursor AS 
  SELECT * 
  FROM pto_adj_2021_bs; ----------------------------------------
@from_date = '02/28/2021'; ----------------------------------
@thru_date = @from_date - 1; 
BEGIN TRANSACTION;
TRY 
  OPEN @tech_key_cur;
  WHILE FETCH @tech_key_cur DO 
    @tech_key = @tech_key_cur.tech_key;
    @new_pto_rate = @tech_key_cur.new_pto_rate;
    @tech_percentage = @tech_key_cur.tech_team_perc;
    UPDATE tpTechValues
    SET thrudate = @thru_date 
    WHERE techkey = @tech_key
      AND thrudate > curdate();
    INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
    values (@tech_key, @from_date, @tech_percentage, @new_pto_rate);
    UPDATE tpdata 
    SET techHourlyRate = @new_pto_rate
    WHERE techkey = @tech_key
      AND thedate = @from_date;
  END WHILE;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;

-- need to DO brian peterson separately
SELECT * 
FROM bsflatratetechs

UPDATE bsflatratetechs
SET thrudate = '02/27/2021'
WHERE fromdate = '03/01/2020'
  AND thrudate = '12/31/9999';
  
INSERT INTO bsflatratetechs values ('RY1',13,'213','1110425',
  'bpeterson@rydellcars.com','Brian','Peterson','Brian Peterson',
  14.4,20.4,40.31,'02/28/2021','12/31/9999');

  

-- AND the results:  
-- looks good
-- 02/14/21 looks good (Main Shop ONLY)
-- 02/28/21 body shop ok
select a.lastname, a.firstname, a.techteampercentage, b.techteampercentage, 
  a.previoushourlyrate, b.previoushourlyrate, round(b.previoushourlyrate - a.previoushourlyrate, 2) AS hourly_change
FROM (
  select * 
  FROM tptechvalues a
  INNER JOIN tptechs b on a.techkey = b.techkey
  WHERE a.thrudate = '02/27/2021') a  
LEFT JOIN (
  SELECT *
  FROM tptechvalues
  WHERE fromdate = '02/28/2021') b on a.techkey = b.techkey
ORDER BY a.lastname

-------------------------------------------------------------------------
--/> 02/14/2021
-------------------------------------------------------------------------



-------------------------------------------------------------------------
--< 03/01/2020
-------------------------------------------------------------------------
DROP TABLE pto_adj_2020;
CREATE TABLE pto_adj_2020(
  tech_key integer,
  new_pto_rate numeric(8,2),
  tech_team_perc double);
-- these INSERT statements generated IN Intranet SQL Scripts\TeamPay\Maintenance\new_hourly_rate_pg.sql  
-- they are ONLY positive changes, no one gets a decrease IN PTO
insert into pto_adj_2020 values (50,19.23,0.4722);
insert into pto_adj_2020 values (19,20.91,0.4970);
insert into pto_adj_2020 values (80,26.82,0.1731);
insert into pto_adj_2020 values (24,26.58,0.4798);
insert into pto_adj_2020 values (84,23.45,0.2508);
insert into pto_adj_2020 values (76,29.47,1.0000);
insert into pto_adj_2020 values (59,52.32,0.3000);
insert into pto_adj_2020 values (13,29.50,0.2650);
insert into pto_adj_2020 values (73,36.09,0.2323);
insert into pto_adj_2020 values (58,44.73,0.2323);
insert into pto_adj_2020 values (15,30.29,0.4556);
insert into pto_adj_2020 values (72,27.12,0.1986);
insert into pto_adj_2020 values (77,27.62,1.0000);
insert into pto_adj_2020 values (85,33.25,1.0000);
insert into pto_adj_2020 values (30,35.68,0.5238);
insert into pto_adj_2020 values (6,29.80,0.2748);
insert into pto_adj_2020 values (51,25.84,0.2293);
insert into pto_adj_2020 values (67,31.17,1.0000);
insert into pto_adj_2020 values (55,26.13,0.2995);
insert into pto_adj_2020 values (16,24.35,0.2323);
insert into pto_adj_2020 values (82,24.67,1.0000);
insert into pto_adj_2020 values (42,33.01,1.0000);
insert into pto_adj_2020 values (18,29.75,0.2121);
insert into pto_adj_2020 values (1,38.69,0.2404);
insert into pto_adj_2020 values (78,20.66,1.0000);
insert into pto_adj_2020 values (62,28.00,0.5201);

-- modify the UPDATE script
-- SELECT * FROM pto_adj_2019
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @tech_percentage double;
DECLARE @tech_key_cur cursor AS 
  SELECT * 
  FROM pto_adj_2020; ----------------------------------------
@from_date = '03/01/2020'; ----------------------------------
@thru_date = @from_date - 1; 
BEGIN TRANSACTION;
TRY 
  OPEN @tech_key_cur;
  WHILE FETCH @tech_key_cur DO 
    @tech_key = @tech_key_cur.tech_key;
    @new_pto_rate = @tech_key_cur.new_pto_rate;
    @tech_percentage = @tech_key_cur.tech_team_perc;
    UPDATE tpTechValues
    SET thrudate = @thru_date 
    WHERE techkey = @tech_key
      AND thrudate > curdate();
    INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
    values (@tech_key, @from_date, @tech_percentage, @new_pto_rate);
    UPDATE tpdata 
    SET techHourlyRate = @new_pto_rate
    WHERE techkey = @tech_key
      AND thedate = @from_date;
  END WHILE;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;

-- need to DO brian peterson separately
SELECT * 
FROM bsflatratetechs

UPDATE bsflatratetechs
SET thrudate = '02/29/2020'
WHERE fromdate = '02/08/2015'
  AND thrudate = '12/31/9999';
  
INSERT INTO bsflatratetechs values ('RY1',13,'213','1110425',
  'bpeterson@rydellcars.com','Brian','Peterson','Brian Peterson',
  14.4,20.4,39.99,'03/01/2020','12/31/9999');

  

-- AND the results:  
-- looks good
select a.lastname, a.firstname, a.techteampercentage, b.techteampercentage, 
  a.previoushourlyrate, b.previoushourlyrate, round(b.previoushourlyrate - a.previoushourlyrate, 2) AS hourly_change
FROM (
  select * 
  FROM tptechvalues a
  INNER JOIN tptechs b on a.techkey = b.techkey
  WHERE a.thrudate = '02/29/2020') a  
LEFT JOIN (
  SELECT *
  FROM tptechvalues
  WHERE fromdate = '03/01/2020') b on a.techkey = b.techkey
ORDER BY a.lastname

-------------------------------------------------------------------------
--/> 03/01/2020
-------------------------------------------------------------------------












/*
2-13-18
based on new_hourly_Rate_pg.sql which i used to generate spreadsheets
to send to Andrew AND Randy
CONVERT this back to ads to facilityat automating the UPDATE
*/

-- DROP TABLE #techs
SELECT a.departmentkey, a.employeenumber as employee_number, 
  a.firstname as first_name, a.lastname as last_name, 
  round(c.previousHourlyRate, 2) AS prev_rate
INTO #techs  
FROM tptechs a
INNER JOIN tpteamtechs b on a.techkey = b.techkey
INNER JOIN tptechvalues c on a.techkey = c.techkey
  AND c.thrudate > curdate()
WHERE b.thrudate > curdate(); 
    
-- 1/2/19 Column not found: check_month
-- notes are NOT detailed enuf, NOT sure what IS happening
select departmentkey, distcode, aa.last_name, aa.first_name, aa.employee_number, total_gross, pto, net, bb.clock_hours, 
  round(net/bb.clock_hours, 2) as new_pto_rate,
  pto_rate as current_pto_rate, 
  round(net/bb.clock_hours, 2) - pto_rate as change
from (
  select last_name,first_name, employee_number, sum(total_gross_pay) as total_gross, 
    sum(pto) as pto, sum(total_gross_pay - pto) as net, max(pto_rate) as pto_rate
  from (
    select a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.total_gross_pay, 
      sum(case when c.code_id in ('PTO','400','85','86','VAC') then c.amount else 0 end) as pto, 
      max(b.prev_rate) as pto_rate
    from dds.stgarkonapyhshdta a
    inner join #techs b on a.yhdemp = b.employee_number
--    left join dds.stgArkonapyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
--      and a.company_number = c.company_number
--      and a.employee_ = c.employee_number
--      and c.code_type in ('0','1') -- 0,1: income, 2: deduction -------------********************
    where a.payroll_ending_year = 18 -- leaves out second january check
    group by a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.base_pay, a.total_gross_pay) x
  group by last_name,first_name, employee_number) aa
left join (
  select d.departmentkey, distcode, d.employee_number, d.last_name, d.first_name, sum(clockhours) as clock_hours
  from edwClockHoursFact a
  inner join day b on a.datekey = b.date_key
    and b.the_year = 2018 -----------------------------------------------------********************
  inner join edwEmployeeDim  c on a.employeekey = c.employeekey
  inner join #techs d on c.employeenumber = d.employee_number  
  group by d.departmentkey, distcode, d.employee_number, d.last_name, d.first_name) bb on aa.employee_number = bb.employee_number 
order by last_name


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
-- FROM new_hourly_rate_201702
-- ALL done IN ads, compare results to pg
SELECT COUNT(*) FROM #techs;
DROP TABLE #techs;
SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, a.employeenumber, 
  round(c.previousHourlyRate, 2) AS prev_rate, c.techTeamPercentage
INTO #techs
FROM tpTechs a
INNER JOIN tpTeamTechs b on a.techkey = b.techkey
INNER JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()
WHERE b.thrudate > curdate()

-- DROP TABLE #pay_2017;
SELECT c.firstname, c.lastname, c.employeenumber, sum(b.amount) as paid_2017
INTO #pay_2017
FROM dds.ext_pyptbdta a
INNER JOIN dds.ext_pyhscdta b on a.company_number = b.company_number
  AND a.payroll_run_number = b.payroll_run_number
  AND b.code_type = '1'
  AND b.code_id IN ('87','79','75') 
INNER JOIN #techs c on TRIM(b.employee_number) = c.employeenumber
WHERE a.payroll_cen_year = 117
GROUP BY c.firstname, c.lastname, c.employeenumber;

-- now i can't fucking find WHERE pyptbdta OR pyhscdta are extracted to ads
-- so, go back to PG, generate a spreadsheet, FROM that spreadsheet generate 
-- INSERT statements
-- DROP TABLE pto_adj_2017;
CREATE TABLE pto_adj_2017(
  tech_key integer,
  new_pto_rate numeric(8,2),
  tech_team_perc double);
insert into pto_adj_2017 values(72,24.32,0.1484);
insert into pto_adj_2017 values(76,31.57,1.000);
insert into pto_adj_2017 values(30,31.28,0.4938);
insert into pto_adj_2017 values(52,22.46,1);
insert into pto_adj_2017 values(77,18.42,1);
insert into pto_adj_2017 values(39,30.12,0.1854);
insert into pto_adj_2017 values(41,28.1,1);
insert into pto_adj_2017 values(31,34.01,0.2014);
insert into pto_adj_2017 values(59,44.47,0.4138);
insert into pto_adj_2017 values(55,25.59,0.2995);
insert into pto_adj_2017 values(73,30.25,0.3142);
insert into pto_adj_2017 values(58,30.48,0.289);
insert into pto_adj_2017 values(4,42.5,0.3417);
insert into pto_adj_2017 values(70,17.42,0.2104);
insert into pto_adj_2017 values(6,26.02,0.2689);
insert into pto_adj_2017 values(24,19.67,0.2243);
insert into pto_adj_2017 values(71,21.39,0.18);
insert into pto_adj_2017 values(11,25.66,0.2135);
insert into pto_adj_2017 values(54,29.54,0.24);
insert into pto_adj_2017 values(1,32.97,0.2569);
insert into pto_adj_2017 values(26,17.76,0.2243);
insert into pto_adj_2017 values(18,19.92,0.189);
insert into pto_adj_2017 values(13,23.54,0.2558);
insert into pto_adj_2017 values(19,18.44,0.5168);
insert into pto_adj_2017 values(51,20.72,0.2214);
insert into pto_adj_2017 values(62,24.76,0.986);



-- old AND new values
SELECT lastname, firstname, c.techteampercentage, d.tech_team_perc, 
  c.techteampercentage - d.tech_team_perc, 
  c.previoushourlyrate, d.new_pto_rate, 
  round(d.new_pto_rate - c.previoushourlyrate, 2) 
FROM tpdata a
LEFT JOIN tptechvalues c on a.techkey = c.techkey
  AND c.thrudate > curdate()  
LEFT JOIN pto_adj_2017 d on c.techkey = d.tech_key  
WHERE a.thedate = curdate()
  and d.tech_key IS NOT null

-- modify the UPDATE script
-- SELECT * FROM pto_adj_2017
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @tech_percentage double;
DECLARE @tech_key_cur cursor AS 
  SELECT * 
  FROM pto_adj_2017; 
@from_date = '02/04/2018';
@thru_date = @from_date - 1; 
BEGIN TRANSACTION;
TRY 
  OPEN @tech_key_cur;
  WHILE FETCH @tech_key_cur DO 
    @tech_key = @tech_key_cur.tech_key;
    @new_pto_rate = @tech_key_cur.new_pto_rate;
    @tech_percentage = @tech_key_cur.tech_team_perc;
    UPDATE tpTechValues
    SET thrudate = @thru_date 
    WHERE techkey = @tech_key
      AND thrudate > curdate();
    INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
    values (@tech_key, @from_date, @tech_percentage, @new_pto_rate);
    UPDATE tpdata 
    SET techHourlyRate = @new_pto_rate
    WHERE techkey = @tech_key
      AND thedate = @from_date;
  END WHILE;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;

-- AND the results:  
select a.lastname, a.firstname, a.techteampercentage, b.techteampercentage, 
  a.previoushourlyrate, b.previoushourlyrate
FROM (
  select * 
  FROM tptechvalues a
  INNER JOIN tptechs b on a.techkey = b.techkey
  WHERE a.thrudate = '02/03/2018') a  
LEFT JOIN (
  SELECT *
  FROM tptechvalues
  WHERE fromdate = '02/04/2018') b on a.techkey = b.techkey
ORDER BY a.lastname

-------------------------------------------------------------------------------
-- 2/3/19
-------------------------------------------------------------------------------
DROP TABLE pto_adj_2019;
CREATE TABLE pto_adj_2019(
  tech_key integer,
  new_pto_rate numeric(8,2),
  tech_team_perc double);
-- these INSERT statements generated IN Intranet SQL Scripts\TeamPay\Maintenance\new_hourly_rate_pg.sql  
insert into pto_adj_2019 values (72,26.43,0.1589);
insert into pto_adj_2019 values (76,29.36,1.0000);
insert into pto_adj_2019 values (81,17.75,1.0000);
insert into pto_adj_2019 values (42,32.85,1.0000);
insert into pto_adj_2019 values (78,20.03,1.0000);
insert into pto_adj_2019 values (30,34.68,0.5179);
insert into pto_adj_2019 values (82,21.57,1.0000);
insert into pto_adj_2019 values (43,41.56,0.2400);
insert into pto_adj_2019 values (67,27.60,1.0000);
insert into pto_adj_2019 values (56,39.12,1.0000);
insert into pto_adj_2019 values (45,29.80,0.1753);
insert into pto_adj_2019 values (77,22.31,1.0000);
insert into pto_adj_2019 values (35,22.02,1.0000);
insert into pto_adj_2019 values (39,31.84,0.1854);
insert into pto_adj_2019 values (48,38.98,0.0610);
insert into pto_adj_2019 values (32,38.16,1.0000);
insert into pto_adj_2019 values (41,30.62,1.0000);
insert into pto_adj_2019 values (31,34.86,0.2014);
insert into pto_adj_2019 values (34,31.72,0.4900);
insert into pto_adj_2019 values (59,49.97,0.3000);
insert into pto_adj_2019 values (55,25.31,0.2995);
insert into pto_adj_2019 values (16,23.01,0.2323);
insert into pto_adj_2019 values (80,26.40,0.1731);
insert into pto_adj_2019 values (73,35.20,0.2323);
insert into pto_adj_2019 values (58,36.84,0.2323);
insert into pto_adj_2019 values (4,47.51,0.3124);
insert into pto_adj_2019 values (70,18.65,0.2104);
insert into pto_adj_2019 values (6,29.52,0.2748);
insert into pto_adj_2019 values (7,32.71,0.2309);
insert into pto_adj_2019 values (24,20.55,0.2243);
insert into pto_adj_2019 values (50,17.89,0.4722);
insert into pto_adj_2019 values (11,29.08,0.1952);
insert into pto_adj_2019 values (9,38.68,0.2733);
insert into pto_adj_2019 values (54,32.39,0.2195);
insert into pto_adj_2019 values (79,24.80,0.1952);
insert into pto_adj_2019 values (1,38.04,0.2404);
insert into pto_adj_2019 values (26,19.93,0.2243);
insert into pto_adj_2019 values (18,26.23,0.2121);
insert into pto_adj_2019 values (13,26.95,0.2650);
insert into pto_adj_2019 values (19,19.36,0.4970);
insert into pto_adj_2019 values (51,23.53,0.2293);
insert into pto_adj_2019 values (62,26.40,0.9860);

-- modify the UPDATE script
-- SELECT * FROM pto_adj_2019
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @tech_percentage double;
DECLARE @tech_key_cur cursor AS 
  SELECT * 
  FROM pto_adj_2019; ----------------------------------------
@from_date = '02/03/2019'; ----------------------------------
@thru_date = @from_date - 1; 
BEGIN TRANSACTION;
TRY 
  OPEN @tech_key_cur;
  WHILE FETCH @tech_key_cur DO 
    @tech_key = @tech_key_cur.tech_key;
    @new_pto_rate = @tech_key_cur.new_pto_rate;
    @tech_percentage = @tech_key_cur.tech_team_perc;
    UPDATE tpTechValues
    SET thrudate = @thru_date 
    WHERE techkey = @tech_key
      AND thrudate > curdate();
    INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
    values (@tech_key, @from_date, @tech_percentage, @new_pto_rate);
    UPDATE tpdata 
    SET techHourlyRate = @new_pto_rate
    WHERE techkey = @tech_key
      AND thedate = @from_date;
  END WHILE;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;

-- AND the results:  
select a.lastname, a.firstname, a.techteampercentage, b.techteampercentage, 
  a.previoushourlyrate, b.previoushourlyrate
FROM (
  select * 
  FROM tptechvalues a
  INNER JOIN tptechs b on a.techkey = b.techkey
  WHERE a.thrudate = '02/02/2019') a  
LEFT JOIN (
  SELECT *
  FROM tptechvalues
  WHERE fromdate = '02/03/2019') b on a.techkey = b.techkey
ORDER BY a.lastname


------------------------------------------------------------------------
-- fucked up with wrong dates, did NOT change year IN script @from_date
-- saved once again BY the daily cut
------------------------------------------------------------------------
DELETE
-- SELECT DISTINCT techkey
FROM tptechvalues
WHERE techkey IN (
SELECT tech_key
FROM pto_adj_2019) 

INSERT INTO tptechvalues
SELECT *
FROM zztest.tptechvalues
WHERE techkey IN (
SELECT tech_key
FROM pto_adj_2019)
