﻿----------------------------------------------------------------
--< 02/08/2021
----------------------------------------------------------------
/*
02/08/21
ran it just for ry1 main shop for ken, not  implemented yet
main shop looked good though, compared roster to last payroll file to kim

2/10/21
ken and andrew have asked to have the new (positive only) rates implemented 2/14/21

*/


-- imports:
--   tptechs
--   tpteamtechs
--   tptechvalues
-- bsflatratetechs
drop table if exists ads.ext_bs_flat_rate_techs;
create table ads.ext_bs_flat_rate_techs (
  storecode citext,
  departmentkey integer,
  technumber citext, 
  employeenumber citext,
  username citext,
  firstname citext,
  lastname citext,
  fullname citext,
  pdrrate numeric,
  metalrate numeric,
  otherrate numeric,
  fromdate date, 
  thrudate date);

drop table if exists techs;
create temp table techs as
    SELECT a.departmentkey, a.techkey, a.employeenumber as employee_number, 
      a.firstname as first_name, a.lastname as last_name, 
      round(c.previousHourlyRate, 2) AS prev_rate, c.techteampercentage
    FROM ads.ext_tp_techs a
    JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
    JOIN ads.ext_tp_tech_values c on a.techkey = c.techkey
      AND c.thrudate > current_date
    WHERE b.thrudate > current_date
union
    SELECT departmentkey, 999, employeenumber as employee_number, 
      firstname as first_name, lastname as last_name, 
      round(otherrate, 2) AS prev_rate, 100
    FROM ads.ext_bs_flat_rate_techs 
    where lastname = 'peterson' 
      and firstname = 'brian'
      AND thrudate > current_date;

drop table if exists clock_hours;
create temp table clock_hours as
select e.distrib_code, a.employee_number, sum(clock_hours) as clock_hours
from arkona.xfm_pypclockin a
join dds.dim_date b on a.the_date = b.the_date
  and b.the_year = 2020  -------------------------------------------------------------------------------------------------------------
join techs d on a.employee_number = d.employee_number  
join arkona.xfm_pymast e on  a.employee_number = e.pymast_employee_number
  and e.current_row
group by e.distrib_code, a.employee_number;

-- verified rosters and current pto against the last payroll spreadsheet
drop table if exists new_pto_rates;
create temp table new_pto_rates as
select techkey, distrib_code, aa.last_name, aa.first_name, aa.employee_number, total_gross, pto, net, bb.clock_hours, 
   round(net/bb.clock_hours, 2) as new_pto_rate,
  pto_rate as current_pto_rate,
  round(net/bb.clock_hours, 2) - pto_rate as change
from (
  select techkey, last_name,first_name, employee_number, sum(total_gross_pay) as total_gross, 
    sum(pto) as pto, sum(total_gross_pay - pto) as net, max(pto_rate) as pto_rate
  from (
    select b.techkey, a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.total_gross_pay, 
      sum(case when c.code_id in ('PTO','400','85','86','VAC') then c.amount else 0 end) as pto, 
      max(b.prev_rate) as pto_rate
    from arkona.ext_pyhshdta a
    inner join techs b on a.employee_ = b.employee_number
    left join arkona.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
      and a.company_number = c.company_number
      and a.employee_ = c.employee_number
      and c.code_type in ('0','1') -- 0,1: income, 2: deduction
    where a.payroll_ending_year = 20 -- -------------------------------------------------------------------------------------------
    group by b.techkey, a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.base_pay, a.total_gross_pay) x
  group by techkey, last_name,first_name, employee_number) aa
left join clock_hours bb on aa.employee_number = bb.employee_number 
order by distrib_code, last_name;      

select * from new_pto_rates;

-- need to generate data to insert into ads to update the pto rates
-- a series of insert statements into pto_adj_2019: tech_key, new_pto_rate, tech_team_perc(carry over current value)
-- this includes everyone except brian peterson
-- exclude negative adjusments

select 'insert into pto_adj_2021 values (' || a.techkey || ',' || new_pto_rate || ',' ||b.techteampercentage || ');'-- , a.last_name, a.first_name
from techs a
join ads.ext_tp_tech_values b on a.techkey = b.techkey
  and b.thrudate > current_Date
join new_pto_rates bb on a.techkey = bb.techkey
where change > 0
  and distrib_code <> 'STEC'


-- generate a spreadsheet for kim, main shop only
-- -2/22/21 sent this spreadsheet to john gardner for approval

select last_name, first_name, employee_number, current_pto_rate as old_pto_rate, new_pto_rate
from new_pto_rates
where change > 0
  and distrib_code <> 'STEC'

*** NOTE ON 2/8/21 GENERATED THE NEW RATES FOR MAIN SHOP ONLY, SENT SPREADSHEET TO KIM  ***
*** 02/28/21 did body shop ***
----------------------------------------------------------------
--/> 02/08/2021
----------------------------------------------------------------


----------------------------------------------------------------
--< 02/21/2020
----------------------------------------------------------------
--moved all the relevant queries up here to the top of this file


-- imports:
--   tptechs
--   tpteamtechs
--   tptechvalues
-- bsflatratetechs
drop table if exists ads.ext_bs_flat_rate_techs;
create table ads.ext_bs_flat_rate_techs (
  storecode citext,
  departmentkey integer,
  technumber citext, 
  employeenumber citext,
  username citext,
  firstname citext,
  lastname citext,
  fullname citext,
  pdrrate numeric,
  metalrate numeric,
  otherrate numeric,
  fromdate date, 
  thrudate date);

drop table if exists techs;
-- uh oh, fucked up on brian peterson, his data is actually in the table bsFlatRateTechs (because of the multiiple rates)
create temp table techs as
    SELECT a.departmentkey, a.techkey, a.employeenumber as employee_number, 
      a.firstname as first_name, a.lastname as last_name, 
      round(c.previousHourlyRate, 2) AS prev_rate, c.techteampercentage
    FROM ads.ext_tpTechs a
    JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
    JOIN ads.ext_tp_tech_values c on a.techkey = c.techkey
      AND c.thrudate > current_date
    WHERE b.thrudate > current_date
union
    SELECT departmentkey, 999, employeenumber as employee_number, 
      firstname as first_name, lastname as last_name, 
      round(otherrate, 2) AS prev_rate, 100
    FROM ads.ext_bs_flat_rate_techs 
    where lastname = 'peterson' 
      and firstname = 'brian'
      AND thrudate > current_date;

drop table if exists clock_hours;
create temp table clock_hours as
select e.distrib_code, a.employee_number, sum(clock_hours) as clock_hours
from arkona.xfm_pypclockin a
join dds.dim_date b on a.the_date = b.the_date
  and b.the_year = 2019  -------------------------------------------------------------------------------------------------------------
join techs d on a.employee_number = d.employee_number  
join arkona.xfm_pymast e on  a.employee_number = e.pymast_employee_number
  and e.current_row
group by e.distrib_code, a.employee_number;

-- verified rosters and current pto against the last payroll spreadsheet
drop table if exists new_pto_rates;
create temp table new_pto_rates as
select techkey, distrib_code, aa.last_name, aa.first_name, aa.employee_number, total_gross, pto, net, bb.clock_hours, 
   round(net/bb.clock_hours, 2) as new_pto_rate,
  pto_rate as current_pto_rate,
  round(net/bb.clock_hours, 2) - pto_rate as change
from (
  select techkey, last_name,first_name, employee_number, sum(total_gross_pay) as total_gross, 
    sum(pto) as pto, sum(total_gross_pay - pto) as net, max(pto_rate) as pto_rate
  from (
    select b.techkey, a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.total_gross_pay, 
      sum(case when c.code_id in ('PTO','400','85','86','VAC') then c.amount else 0 end) as pto, 
      max(b.prev_rate) as pto_rate
    from arkona.ext_pyhshdta a
    inner join techs b on a.employee_ = b.employee_number
    left join arkona.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
      and a.company_number = c.company_number
      and a.employee_ = c.employee_number
      and c.code_type in ('0','1') -- 0,1: income, 2: deduction
    where a.payroll_ending_year = 19 -- -------------------------------------------------------------------------------------------
    group by b.techkey, a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.base_pay, a.total_gross_pay) x
  group by techkey, last_name,first_name, employee_number) aa
left join clock_hours bb on aa.employee_number = bb.employee_number 
order by distrib_code, last_name;      

select * from new_pto_rates;

-- need to generate data to insert into ads to update the pto rates
-- a series of insert statements into pto_adj_2019: tech_key, new_pto_rate, tech_team_perc(carry over current value)
-- this includes everyone except brian peterson
-- exclude negative adjusments

select 'insert into pto_adj_2020 values (' || a.techkey || ',' || new_pto_rate || ',' ||b.techteampercentage || ');'-- , a.last_name, a.first_name
from techs a
join ads.ext_tp_tech_values b on a.techkey = b.techkey
  and b.thrudate > current_Date
join new_pto_rates bb on a.techkey = bb.techkey
where change > 0


-- generate a spreadsheet for kim
select last_name, first_name, employee_number, current_pto_rate as old_pto_rate, new_pto_rate
from new_pto_rates
where change > 0



----------------------------------------------------------------
--/> 02/21/2020
----------------------------------------------------------------



-- imports:
--   tptechs
--   tpteamtechs
--   tptechvalues
--   pyptbdta
--   pyhscdta
--   pyhshdta
--   edwClockHoursFact
--   edwEmployeeDim
-- 
-- -- current techs
-- SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
--   round(c.previousHourlyRate, 2) AS prev_rate
-- --INTO #techs
-- FROM ads.ext_tpTechs a
-- INNER JOIN ads.ext_tpTeamTechs b on a.techkey = b.techkey
-- INNER JOIN ads.ext_tpTechValues c on a.techkey = c.techkey
--   AND c.thrudate > current_date
-- WHERE b.thrudate > current_date 
-- 
-- -- 2016 pay
-- SELECT c.firstname, c.lastname, c.employeenumber, sum(b.amount) as paid_2016
-- --INTO #pay_2015
-- FROM dds.ext_pyptbdta a
-- INNER JOIN dds.ext_pyhscdta b on a.company_number = b.company_number
--   AND a.payroll_run_number = b.payroll_run_number
--   AND b.code_type = '1'
--   AND b.code_id IN ('87','79','75') 
-- INNER JOIN (
--   SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
--     round(c.previousHourlyRate, 2) AS prev_rate
--   --INTO #techs
--   FROM ads.ext_tpTechs a
--   INNER JOIN ads.ext_tpTeamTechs b on a.techkey = b.techkey
--   INNER JOIN ads.ext_tpTechValues c on a.techkey = c.techkey
--     AND c.thrudate > current_date
--   WHERE b.thrudate > current_date ) c on TRIM(b.employee_number) = c.employeenumber
-- WHERE a.payroll_cen_year = 116
-- GROUP BY c.firstname, c.lastname, c.employeenumber
-- 
-- 
-- -- clock hours
-- SELECT e.firstname, e.lastname, e.employeenumber, SUM(c.clockhours) AS clock_hours
-- -- INTO #clock_hours_2015
-- FROM dds.day a
-- INNER JOIN dds.day b on a.datekey = b.datekey
--   and b.theyear = 2016
-- INNER JOIN ads.ext_dds_edwClockHoursFact c on a.datekey = c.datekey  
--   AND c.clockhours <> 0
-- INNER JOIN ads.ext_dds_edwEmployeeDim d on c.employeekey = d.employeekey  
-- INNER JOIN (
--   SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
--     round(c.previousHourlyRate, 2) AS prev_rate
--   --INTO #techs
--   FROM ads.ext_tpTechs a
--   INNER JOIN ads.ext_tpTeamTechs b on a.techkey = b.techkey
--   INNER JOIN ads.ext_tpTechValues c on a.techkey = c.techkey
--     AND c.thrudate > current_date
--   WHERE b.thrudate > current_date ) e on d.employeenumber = e.employeenumber
-- GROUP BY e.firstname, e.lastname, e.employeenumber;

-- 1/30/18, trying to do it in PG for 2017

select x.departmentkey, x.employeenumber, x.firstname, x.lastname, 
  y.paid_2017, round(z.clock_hours, 2) as clock_hours_2017, 
  x.prev_rate as current_pto_rate, round(y.paid_2017/z.clock_hours, 2) as new_pto_rate,
  round(y.paid_2017/z.clock_hours, 2) - x.prev_rate
   
from (
  -- current techs
  SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
    round(c.previousHourlyRate, 2) AS prev_rate
  --INTO #techs
  FROM ads.ext_tpTechs a
  INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
  INNER JOIN ads.ext_tp_tech_values c on a.techkey = c.techkey
    AND c.thrudate > current_date
  WHERE b.thrudate > current_date ) x
left join (
  SELECT c.firstname, c.lastname, c.employeenumber, sum(b.amount) as paid_2017
  --INTO #pay_2015
  FROM arkona.ext_pyptbdta a
  INNER JOIN arkona.ext_pyhscdta b on a.company_number = b.company_number
    AND a.payroll_run_number = b.payroll_run_number
    AND b.code_type = '1'
    AND b.code_id IN ('87','79','75') 
  INNER JOIN (
    SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
      round(c.previousHourlyRate, 2) AS prev_rate
    --INTO #techs
    FROM ads.ext_tpTechs a
    INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
    INNER JOIN ads.ext_tp_tech_values c on a.techkey = c.techkey
      AND c.thrudate > current_date
    WHERE b.thrudate > current_date ) c on TRIM(b.employee_number) = c.employeenumber
  WHERE a.payroll_cen_year = 117 ---------------------------------------------------------------------
  GROUP BY c.firstname, c.lastname, c.employeenumber) y on x.employeenumber = y.employeenumber  
left join (
  SELECT e.firstname, e.lastname, e.employeenumber, SUM(c.clockhours) AS clock_hours
  -- INTO #clock_hours_2015
  FROM dds.day a
  INNER JOIN dds.day b on a.datekey = b.datekey
    and b.theyear = 2017 ------------------------------------------------------------------------------
  INNER JOIN ads.ext_edw_clock_hours_fact c on a.datekey = c.datekey  
    AND c.clockhours <> 0
  INNER JOIN ads.ext_edw_employee_dim d on c.employeekey = d.employeekey  
  INNER JOIN (
    SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, 
      round(c.previousHourlyRate, 2) AS prev_rate
    --INTO #techs
    FROM ads.ext_tpTechs a
    INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
    INNER JOIN ads.ext_tp_tech_values c on a.techkey = c.techkey
      AND c.thrudate > current_date
    WHERE b.thrudate > current_date ) e on d.employeenumber = e.employeenumber
  GROUP BY e.firstname, e.lastname, e.employeenumber) z on x.employeenumber = z.employeenumber
order by departmentkey, lastname;


---------------------------------------------------------------------------------------------------------
-- body shop estimators, parts, foreman
-- from pyton projects\body_shop_pay\sql\pto_rate.sql
---------------------------------------------------------------------------------------------------------
-- select * from bspp.personnel
select aa.last_name, aa.first_name, aa.employee_number, total_gross, pto, net, bb.clock_hours, 
  round(net/bb.clock_hours, 2) as new_pto_rate,
  pto_rate as current_pto_rate, 
  round(net/bb.clock_hours, 2) - pto_rate as change
from (
  select last_name,first_name, employee_number, sum(total_gross_pay) as total_gross, 
    sum(pto) as pto, sum(total_gross_pay - pto) as net, max(pto_rate) as pto_rate
  from (
    select a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.total_gross_pay, 
      sum(case when c.code_id in ('PTO','400','85','86','VAC') then c.amount else 0 end) as pto, 
      max(b.pto_rate) as pto_rate
    from arkona.ext_pyhshdta a
    inner join bspp.personnel b on a.employee_ = b.employee_number
    left join arkona.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
      and a.company_number = c.company_number
      and a.employee_ = c.employee_number
      and c.code_type in ('0','1') -- 0,1: income, 2: deduction
    where a.payroll_ending_year = 17 -- leaves out second january check
    group by a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.base_pay, a.total_gross_pay) x
  group by last_name,first_name, employee_number) aa
left join (
  select d.employee_number, d.last_name, d.first_name, sum(clockhours) as clock_hours
  from ads.ext_edw_clock_hours_fact a
  inner join dds.dim_date b on a.datekey = b.date_key
    and b.the_year = 2017
  inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
  inner join bspp.personnel d on c.employeenumber = d.employee_number  
  group by d.employee_number, d.last_name, d.first_name) bb on aa.employee_number = bb.employee_number 
order by last_name


-- 1/31 -----------------------------------------------------------------------
-- think i like this, total breakout

-- and these are the actual codes used in 2017
select distinct code_id, description
from arkona.ext_pyhscdta
where payroll_cen_year = 117
  and code_type in ('0','1')
  and company_number = 'RY1'
  and employee_number in ( 
    SELECT a.employeenumber
    FROM ads.ext_tpTechs a
    INNER JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
    INNER JOIN ads.ext_tp_tech_values c on a.techkey = c.techkey
      AND c.thrudate > current_date
    WHERE b.thrudate > current_date) 
and amount <> 0    

-- 1/22/19, brian peterson was being left out because, he is no longer on a team
-- 2/2/19 added techkey for generating update statements
drop table if exists techs;
create temp table techs as
    SELECT a.departmentkey, a.techkey, a.employeenumber as employee_number, 
      a.firstname as first_name, a.lastname as last_name, 
      round(c.previousHourlyRate, 2) AS prev_rate, c.techteampercentage
    FROM ads.ext_tpTechs a
    JOIN ads.ext_tp_team_techs b on a.techkey = b.techkey
    JOIN ads.ext_tp_tech_values c on a.techkey = c.techkey
      AND c.thrudate > current_date
    WHERE b.thrudate > current_date
union
    SELECT a.departmentkey, a.techkey, a.employeenumber as employee_number, 
      a.firstname as first_name, a.lastname as last_name, 
      round(c.previousHourlyRate, 2) AS prev_rate, c.techteampercentage
    FROM ads.ext_tpTechs a
    JOIN ads.ext_tp_tech_values c on a.techkey = c.techkey
      AND c.thrudate > current_date
    where a.lastname = 'peterson' 
      and a.firstname = 'brian';


select aa.*, bb.clock_hours as "2017 CLOCK HOURS", 
  "2016 PTO RATE" as "OLD PTO RATE",
  round(aa."2017 NET GROSS"/bb.clock_hours, 2) as "NEW PTO RATE",
  round(aa."2017 NET GROSS"/bb.clock_hours, 2) - aa."2016 PTO RATE" as "PTO RATE CHANGE"
from (
  select b.departmentkey, b.employee_number, b.last_name, b.first_name, 
    coalesce(sum(a.amount) filter (where a.code_id = '66'), 0) as TRAINING,
    coalesce(sum(a.amount) filter (where a.code_id = '70'), 0) as "TECH XTRA BONUS",
    coalesce(sum(a.amount) filter (where a.code_id = '75'), 0) as "RATE ADJUSTMENT",
    coalesce(sum(a.amount) filter (where a.code_id = '79'), 0) as "COMMISSIONS",
    coalesce(sum(a.amount) filter (where a.code_id = '85'), 0) as "VAC/PTO PAY",
    coalesce(sum(a.amount) filter (where a.code_id = '86'), 0) as "HOLIDAY PAY",
    coalesce(sum(a.amount) filter (where a.code_id = '87'), 0) as "TECH HOURS PAY",
    coalesce(sum(a.amount) filter (where a.code_id = 'HOL'), 0) as "HOLIDAY PAY",
    coalesce(sum(a.amount) filter (where a.code_id = 'OVT'), 0) as "OVERTIME PAY",
    coalesce(sum(a.amount) filter (where a.code_id = 'PTO'), 0) as "PTO PAY",
    coalesce(sum(a.amount) filter (where a.code_id = 'VAC'), 0) as "VACATION PAY",
    coalesce(sum(a.amount) filter (where a.code_id = '400'), 0) as "PAY OUT PTO",
    coalesce(sum(a.amount) filter (where a.code_id = '500'), 0) as "LEASE PROGRAM",
    coalesce(sum(a.amount) filter (where a.code_id = '510'), 0) as "VOLUNTEER PTO",
    sum(a.amount) as "2017 GROSS",
    sum(a.amount) filter (where a.code_id in ('85','86','PTO','VAC','400','510')) as "2017 TOTAL PTO",
    sum(a.amount) - sum(a.amount) filter (where a.code_id in ('85','86','PTO','VAC','400','510'))  as "2017 NET GROSS",
    b.prev_rate as "2016 PTO RATE"
  from arkona.ext_pyhscdta a
  inner join techs b on a.employee_number = b.employee_number
  where payroll_cen_year = 117
    and code_type in ('0','1')
  group by b.departmentkey, b.employee_number, b.last_name, b.first_name, b.prev_rate) aa
left join (
  select d.employee_number, sum(clockhours) as clock_hours
  from ads.ext_edw_clock_hours_fact a
  inner join dds.dim_date b on a.datekey = b.date_key
    and b.the_year = 2017
  inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
  inner join techs d on c.employeenumber = d.employee_number  
  group by d.employee_number) bb on aa.employee_number = bb.employee_number  
order by aa.departmentkey, aa.last_name, aa.first_name

-- even though the above break out is cool, i don't believe it is as accurate
-- as using gross from pyhshdta as below, going with that, besides, theses fucking 
-- managers don't want that kind of detail

-- 1/20/19 
-- checked this query againt thee spreadsheets i sent out on 2/5/18
-- this is it
-- need a fresh scrape of ads.ext_edw_clock_hours_fact, tptechs, tpteamtechs, tptechvalues
-- set spreadsheets to andrew and randy 1/22/19

select departmentkey, distcode, aa.last_name, aa.first_name, aa.employee_number, total_gross, pto, net, bb.clock_hours, 
  round(net/bb.clock_hours, 2) as new_pto_rate,
  pto_rate as current_pto_rate, 
  round(net/bb.clock_hours, 2) - pto_rate as change
from (
  select last_name,first_name, employee_number, sum(total_gross_pay) as total_gross, 
    sum(pto) as pto, sum(total_gross_pay - pto) as net, max(pto_rate) as pto_rate
  from (
    select a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.total_gross_pay, 
      sum(case when c.code_id in ('PTO','400','85','86','VAC') then c.amount else 0 end) as pto, 
      max(b.prev_rate) as pto_rate
    from arkona.ext_pyhshdta a
    inner join techs b on a.employee_ = b.employee_number
    left join arkona.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
      and a.company_number = c.company_number
      and a.employee_ = c.employee_number
      and c.code_type in ('0','1') -- 0,1: income, 2: deduction
    where a.payroll_ending_year = 18 -- -------------------------------------------------------------------------------------------
    group by a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
      b.employee_number, a.base_pay, a.total_gross_pay) x
  group by last_name,first_name, employee_number) aa
left join (
  select d.departmentkey, distcode, d.employee_number, d.last_name, d.first_name, sum(clockhours) as clock_hours
  from ads.ext_edw_clock_hours_fact a
  inner join dds.dim_date b on a.datekey = b.date_key
    and b.the_year = 2018 -------------------------------------------------------------------------------------------------------------
  inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
  inner join techs d on c.employeenumber = d.employee_number  
  group by d.departmentkey, distcode, d.employee_number, d.last_name, d.first_name) bb on aa.employee_number = bb.employee_number 
where distcode in ('stec','btec') -- eliminates carter ferry's sales data
order by departmentkey, last_name


------------------------------------------------------------------
-- 2/3/19

-- need to generate data to insert into ads to update the pto rates
-- a series of insert statements into pto_adj_2019: tech_key, new_pto_rate, tech_team_perc(carry over current value)



-- select bb.departmentkey, bb.last_name, a.techkey, bb.new_pto_rate, b.techteampercentage
select 'insert into pto_adj_2019 values (' || a.techkey || ',' || new_pto_rate || ',' ||b.techteampercentage || ');'
from techs a
join ads.ext_tp_tech_values b on a.techkey = b.techkey
  and b.thrudate > current_Date
left join (
  select techkey, departmentkey, aa.last_name, aa.first_name, aa.employee_number,
    round(net/bb.clock_hours, 2) as new_pto_rate
  --   pto_rate as current_pto_rate, 
  --   round(net/bb.clock_hours, 2) - pto_rate as change
  from (
    select last_name,first_name, employee_number, techkey, sum(total_gross_pay) as total_gross, 
      sum(pto) as pto, sum(total_gross_pay - pto) as net, max(pto_rate) as pto_rate
    from (
      select b.techkey, a.check_month, b.last_name, b.first_name, 
        b.employee_number, a.total_gross_pay, 
        sum(case when c.code_id in ('PTO','400','85','86','VAC') then c.amount else 0 end) as pto, 
        max(b.prev_rate) as pto_rate
      from arkona.ext_pyhshdta a
      inner join techs b on a.employee_ = b.employee_number
      left join arkona.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
        and a.company_number = c.company_number
        and a.employee_ = c.employee_number
        and c.code_type in ('0','1') -- 0,1: income, 2: deduction
      where a.payroll_ending_year = 18 -- -------------------------------------------------------------------------------------------
      group by a.payroll_run_number, a.check_month, b.last_name, b.first_name, 
        b.employee_number, a.base_pay, a.total_gross_pay, b.techkey) x
    group by last_name,first_name, employee_number, techkey) aa
  left join (
    select d.departmentkey, distcode, d.employee_number, d.last_name, d.first_name, sum(clockhours) as clock_hours
    from ads.ext_edw_clock_hours_fact a
    inner join dds.dim_date b on a.datekey = b.date_key
      and b.the_year = 2018 -------------------------------------------------------------------------------------------------------------
    inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
    inner join techs d on c.employeenumber = d.employee_number  
    group by d.departmentkey, distcode, d.employee_number, d.last_name, d.first_name) bb on aa.employee_number = bb.employee_number 
where distcode in ('stec','btec')) bb on a.techkey = bb.techkey
order by bb.departmentkey, bb.last_name



-- 5/26/19
-- need to do honda, at joel's request, pto refresh to occur in June
-- set spreadsheets to andrew and randy 1/22/19
update hs.main_shop_flat_rate_techs
set thru_date = '03/15/2019'
where employee_number = '210073'

select * from hs.main_shop_flat_rate_techs where thru_Date > current_date

-- year_months
select payroll_ending_year, payroll_ending_month, ((payroll_ending_year + 2000) * 100) + payroll_ending_month
from arkona.ext_pyhshdta
where employee_ = '273315'
  and ((payroll_ending_year + 2000) * 100) + payroll_ending_month between 201806 and 201905

  
drop table if exists techs;
create temp table techs as
select b.employee_name, a.employee_number, a.pto_rate
-- select a.*
from hs.main_shop_flat_rate_techs a
join arkona.xfm_pymast b on a.employee_number = b.pymast_employee_number
  and b.current_row
  and b.active_code <> 'T'
where a.thru_Date > current_date;

-- 5/30/19 excluded lease from gross
drop table if exists earnings;
create temp table earnings as
select employee_name, employee_number, sum(total_gross_pay - lease) as total_gross, 
  sum(pto) as pto, sum(total_gross_pay - pto - lease) as net, max(pto_rate) as pto_rate 
from (
  select a.check_month, b.employee_name,
    b.employee_number, a.total_gross_pay, 
    sum(case when c.code_id in ('PTO','400','85','86','VAC') then c.amount else 0 end) as pto, 
    sum(case when c.code_id in ('500') then c.amount else 0 end)  as lease,
    max(b.pto_rate) as pto_rate
  from arkona.ext_pyhshdta a
  inner join techs b on a.employee_ = b.employee_number
  left join arkona.ext_pyhscdta c on a.payroll_run_number = c.payroll_run_number -- has to be left, sometimes no row for a check
    and a.company_number = c.company_number
    and a.employee_ = c.employee_number
    and c.code_type in ('0','1') -- 0,1: income, 2: deduction
  where ((a.payroll_ending_year + 2000) * 100) + a.payroll_ending_month between 201806 and 201905
  group by a.payroll_run_number, a.check_month, b.employee_name, 
    b.employee_number, a.base_pay, a.total_gross_pay) x
group by employee_name, employee_number;

5/30/19 replaced ads.ext_edw_clock_hours_fact with arkona.xfm_pypclockin
drop table if exists clock_hours;
create temp table clock_hours as
select d.employee_number, d.employee_name, sum(clockhours) as clock_hours
from ads.ext_edw_clock_hours_fact a
inner join dds.dim_date b on a.datekey = b.date_key
  and b.year_month  between 201806 and 201905  -------------------------------------------------------------------------------------------------------------
inner join ads.ext_edw_employee_dim c on a.employeekey = c.employeekey
inner join techs d on c.employeenumber = d.employee_number  
group by d.employee_number, d.employee_name;

drop table if exists clock_hours;
create temp table clock_hours as
select a.employee_number, sum(clock_hours) as clock_hours
from arkona.xfm_pypclockin a
inner join dds.dim_date b on a.the_date = b.the_date
  and b.year_month  between 201806 and 201905  -------------------------------------------------------------------------------------------------------------
inner join techs d on a.employee_number = d.employee_number  
group by a.employee_number;

-- spread sheet for joel/kim
select aa.employee_name, aa.employee_number, total_gross, pto, net, bb.clock_hours, 
  round(net/bb.clock_hours, 2) as new_pto_rate,
  pto_rate as current_pto_rate, 
  round(net/bb.clock_hours, 2) - pto_rate as change
from earnings aa
left join clock_hours bb on aa.employee_number = bb.employee_number 



-- update hs.main_shop_flat_rate_techs
select y.*, x.new_pto_rate
from (
  select aa.employee_name, aa.employee_number, total_gross, pto, net, bb.clock_hours, 
    round(net/bb.clock_hours, 2) as new_pto_rate,
    pto_rate as current_pto_rate, 
    round(net/bb.clock_hours, 2) - pto_rate as change
  from earnings aa
  left join clock_hours bb on aa.employee_number = bb.employee_number ) x
left join hs.main_shop_flat_rate_techs y on x.employee_number = y.employee_number
  and y.thru_date > current_date;


do
$$
declare 
  _emp citext := '2130150';
  _flat_rate numeric(6,2) := 26.5;
  _pto_rate numeric(6,2) := 37.76;
  _tech_number citext := '15';
  _thru_date Date := '06/08/2019';
  _from_date date := '06/09/2019';
begin
  update hs.main_shop_flat_rate_techs
  set thru_date = _thru_date
  where employee_number = _emp
    and thru_date > current_date;
  insert into hs.main_shop_flat_rate_techs (employee_number,tech_number,flat_rate,pto_rate,from_date)
  values(_emp, _tech_number, _flat_rate, _pto_rate, _from_date);
end
$$;


update hs.main_shop_flat_rate_techs
set pto_rate = 21.57
where employee_number = '256984'
  and thru_date > current_date
  