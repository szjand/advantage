/*
  select thedate, lastname, firstname, employeenumber,  
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, 
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = a.payperiodend
    AND departmentKey = 18
	AND year(thedate) = 2014
ORDER BY lastname, thedate	

-- main shop, effective 7/13/2014
SELECT lastname, firstname, HourlyRate, SUM(clockHours) AS clockHours, SUM(commPay) AS commPay,
  round(SUM(commPay)/SUM(clockHours) , 2)
FROM (  
  select thedate, lastname, firstname, employeenumber,  
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, 
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = a.payperiodend
    AND departmentKey = 18
	AND year(thedate) = 2014) x
GROUP BY lastname, firstname, HourlyRate	
HAVING SUM(clockhours) > 700


-- main shop, effective 7/13/2014
SELECT lastname, firstname, HourlyRate, SUM(clockHours) AS clockHours, SUM(commPay) AS commPay,
  round(SUM(commPay)/SUM(clockHours) , 2)
FROM (  
  select thedate, lastname, firstname, employeenumber,  
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, 
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = a.payperiodend
    AND departmentKey = 18
	AND year(thedate) = 2014) x
GROUP BY lastname, firstname, HourlyRate	
HAVING SUM(clockhours) > 700


-- body shop
-- team techs
SELECT lastname, firstname, HourlyRate AS ptoRate, SUM(clockHours) AS clockHours, SUM(commPay) AS commPay,
  round(SUM(commPay)/SUM(clockHours) , 2) AS newPtoRate
SELECT TRIM(firstname) + '' + lastname AS Name, employeenumber AS [Emp#],
  SUM(commPay) AS [Comm Pay],  SUM(clockHours) AS clockHours AS [Clock Hours],
  round(SUM(commPay)/SUM(clockHours) , 2) AS [New PTO Rate],
  HourlyRate AS [Current PTO Rate], 
  round(SUM(commPay)/SUM(clockHours) , 2) - hourlyrate 
FROM (  
  select thedate, lastname, firstname, employeenumber,  
    techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
    techclockhourspptd AS ClockHours, 
    round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
  FROM tpdata a
  INNER JOIN tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = a.payperiodend
    AND departmentKey = 13
	AND thedate BETWEEN '03/01/2014' AND '12/31/2014'
  AND EXISTS (
    SELECT 1
    FROM tpTeamTechs
    WHERE techKey = a.techKey
      AND thruDate > curdate())) x
GROUP BY lastname, firstname, HourlyRate	
UNION
-- pdr
SELECT j.lastname, j.firstname, j.otherRate AS hourlyRate, 
  SUM(techClockHoursPPTD) AS clockHours, SUM(metalPay + pdrPay) AS commPay,
  round(SUM(metalPay + pdrPay)/SUM(techClockHoursPPTD), 2) AS newHourlyRate
-- SELECT *
FROM (-- clockhours
  SELECT theDate, techNumber, techClockHoursPPTD
  FROM bsFlatRateData
  WHERE thedate BETWEEN '03/01/2014' AND '12/31/2014'
    AND thedate = payperiodend) g
LEFT JOIN (-- metal pay
  SELECT techNumber, payPeriodEnd, round(SUM(techMetalFlagHoursDay * metalRate), 2) AS metalPay
  FROM (
    SELECT a.theDate, a.payPeriodEnd, a.techNumber, a.techMetalFlagHoursDay, b.metalRate
    FROM bsFlatRateData a
    LEFT JOIN bsFlatRateTechs b on a.technumber = b.technumber
      AND a.theDate BETWEEN b.fromDate AND b.thruDate
    WHERE thedate BETWEEN '02/09/2014' AND '08/23/2014') c 
  GROUP BY techNumber, payPeriodEnd) h on g.theDate = h.payPeriodEnd
LEFT JOIN (-- pdr pay
  SELECT techNumber, payPeriodEnd, round(SUM(techPdrFlagHoursDay * pdrRate), 2) AS pdrPay
  FROM (
    SELECT a.theDate, a.payPeriodEnd, a.techNumber, a.techPdrFlagHoursDay, b.pdrRate
    FROM bsFlatRateData a
    LEFT JOIN bsFlatRateTechs b on a.technumber = b.technumber
      AND a.theDate BETWEEN b.fromDate AND b.thruDate
    WHERE thedate BETWEEN '02/09/2014' AND '08/23/2014') c 
  GROUP BY techNumber, payPeriodEnd) i on g.theDate = i.payPeriodEnd
LEFT JOIN (
  SELECT DISTINCT lastname, firstname, otherRate
  FROM bsFlatRateTechs) j on 1 = 1  
GROUP BY j.lastname, j.firstname, j.otherRate  
ORDER BY lastname, firstname
    

-- 1/25/15
-- does NOT take bonus INTO account
SELECT d.*, g.previousHourlyRate, round(newhourlyrate - previoushourlyrate, 2)
FROM (-- 2014 total flaghours/total clockhours = effective hourly rate
  SELECT lastname, firstname, SUM(clockHours) AS clockHours, SUM(commPay) AS commPay,
    round(SUM(commPay)/SUM(clockHours) , 2) AS newHourlyRate
  FROM (  
    select thedate, lastname, firstname, employeenumber,  
      techhourlyrate AS HourlyRate, techtfrrate AS CommRate, 
      techclockhourspptd AS ClockHours, 
      round(techclockhourspptd * techtfrrate * teamprofpptd/100, 2) AS commPay	
    FROM tpdata a
    INNER JOIN tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey
    WHERE thedate = a.payperiodend
      AND departmentKey = 18
  	AND year(thedate) = 2014) x
  GROUP BY lastname, firstname
  HAVING SUM(clockhours) > 700) d
LEFT JOIN (
  SELECT e.firstname, e.lastname, f.previousHourlyRate
  FROM tpTechs e
  LEFT JOIN tpTechValues f on e.techkey = f.techkey
  WHERE f.thrudate > curdate()) g on d.firstname = g.firstname AND d.lastname = g.lastname

-- 1/26 might be crossing streams here, but leaning towards gross pay - pto pay
--    FROM pyhshdta, AND clockhours FROM dds
--    make this effective 1/25/15

-- 2/1/16 this will no longer WORK, payroll now categorizes team pay folks AS
--        commissioned, therefore there IS no yhsaly
*/
/******************************************************************************

2/1/16 

******************************************************************************/
-- josh syverson: has tp, vac & pto
SELECT *
FROM tpdata
WHERE departmentkey = 18
  AND payperiodseq = 183
  AND techptohourspptd <> 0

-- for tp, pyhshdta no longer breaks out pto/hol hours OR pay  
SELECT yhdemp, yhsaly,
  yhdtgp as gross,
  yhdvac as vacHours,
  yhdhol as holHours,
  yhdsck  as ptoHours,
  yhsaly*(yhdvac+yhdhol+yhdsck) AS totalPtoPay,
  cast(CREATETIMESTAMP( 2000+yhdeyy, yhdemm, yhdedd, 0, 0, 0, 0 ) AS sql_date) AS payrollenddate,
  cast(CREATETIMESTAMP( 2000+yhdcyy, yhdcmm, yhdcdd, 0, 0, 0, 0 ) AS sql_date) AS checkdate
-- SELECT *  
FROM dds.stgArkonaPYHSHDTA   
WHERE ypbcyy = 115
  AND yhdemp = '1135410'
  AND yhdtgp <> 0  
  AND ypbnum = 1231150
 
-- but IS promising, shows actual pto/vac pay, hol pay, tech hours pay
-- but i don't scrape (aqt) it, so DO a one time scrape, let's see what it looks like
SELECT *
FROM dds.ext_PYHSCDTA  
WHERE employee_number = '1135410'
  AND payroll_run_number = 1231150
  
-- ok, this verifies that ALL these paychecks coincided with biweekly start AND END dates  
DROP TABLE #wtf;
SELECT a.payroll_run_number, 
  cast(trim(substring(CAST(a.payroll_start_date AS sql_char),5,2)) + '/' + 
      trim(substring(CAST(a.payroll_start_date AS sql_char),7,2)) + '/' +
      LEFT(trim(CAST(a.payroll_start_date AS sql_char)),4) AS sql_Date) AS payroll_start_date,
  cast(trim(substring(CAST(a.payroll_end_date AS sql_char),5,2)) + '/' + 
      trim(substring(CAST(a.payroll_end_date AS sql_char),7,2)) + '/' +
      LEFT(trim(CAST(a.payroll_end_date AS sql_char)),4) AS sql_Date) AS payroll_end_date, 
--  a.payroll_start_date, a.payroll_end_date, 
  a.check_date, a.description, b.employee_number, b.code_id, b.amount, 
  b.description, c.*
INTO #wtf  
FROM dds.ext_pyptbdta a
INNER JOIN dds.ext_pyhscdta b on a.company_number = b.company_number
  AND a.payroll_run_number = b.payroll_run_number
INNER JOIN (
  SELECT a.firstname, a.lastname, a.employeenumber, c.previousHourlyRate
  FROM tpTechs a
  INNER JOIN tpTeamTechs b on a.techkey = b.techkey
  INNER JOIN tpTechValues c on a.techkey = c.techkey
    AND c.thrudate > curdate()
  WHERE b.thrudate > curdate() 
    /*AND a.departmentkey = 18*/) c on trim(b.employee_number) = c.employeenumber
--LEFT JOIN dds.day d on 
--  cast(trim(substring(CAST(payroll_start_date AS sql_char),5,2)) + '/' + 
--    trim(substring(CAST(payroll_start_date AS sql_char),7,2)) + '/' +
--    LEFT(trim(CAST(payroll_start_date AS sql_char)),4) AS sql_Date) = d.biweeklypayperiodstartdate    
WHERE a.payroll_cen_year = 115
  AND a.company_number = 'RY1'
ORDER BY lastname, payroll_start_date  


SELECT lastname, firstname, COUNT(*)
FROM #wtf
GROUP BY lastname, firstname

SELECT * 
FROM #wtf 
WHERE lastname = 'shereck' AND code_id = '79'
ORDER BY payroll_start_date

SELECT lastname, firstname, code_id, description_1, SUM(amount)
FROM #wtf
WHERE lastname IN ('olson','shereck')
GROUP BY lastname, firstname, code_id, description_1


SELECT firstname, lastname, avg(amount), COUNT(*) 
FROM #wtf 
WHERE  code_id = '79'
GROUP BY firstname, lastname

SELECT company_number, code_id, code_type, description 
FROM dds.ext_pyhscdta
WHERE payroll_cen_year = 115
  AND company_number = 'ry1'
  AND code_type = '1'
  AND code_id in ('87','79', '75') 
GROUP BY company_number, code_id, code_type, description 
ORDER BY code_id

include codes (code_type 1 only)
  87 Tech Hours Paid
  79 Commissions
  75 Rate Adjustment
 
SELECT *
FROM ( 
  SELECT firstname, lastname, employeenumber, 
    round(previoushourlyrate,2) AS cur_hourly, 
    SUM(amount) AS wages_2015
  FROM #wtf
  WHERE code_id IN ('87','79','75')  
  GROUP BY firstname, lastname, employeenumber, round(previoushourlyrate,2)) a

select * FROM dds.edwClockHoursFact
  
SELECT d.employeenumber, d.firstname, d.lastname, SUM(c.clockhours)
FROM (  
  SELECT MIN(payroll_start_date) AS from_Date, MAX(payroll_end_date) AS thru_date
  FROM #wtf) a
INNER JOIN dds.day b on b.thedate BETWEEN a.from_date AND a.thru_date
INNER JOIN dds.edwClockHoursFact c on b.datekey = c.datekey
  AND c.clockhours <> 0
  AND c.employeekey IN (
    SELECT employeekey
    FROM dds.edwEmployeeDim 
    WHERE employeenumber IN (
      SELECT a.employeenumber
      FROM tpTechs a
      INNER JOIN tpTeamTechs b on a.techkey = b.techkey
      INNER JOIN tpTechValues c on a.techkey = c.techkey
        AND c.thrudate > curdate()
      WHERE b.thrudate > curdate())) 
LEFT JOIN dds.edwEmployeeDim d on c.employeekey = d.employeekey      
GROUP BY d.employeenumber, d.firstname, d.lastname  

-- divide queries up INTO techs, pay, hours, date range, 

-- current techs
-- 2/7/16 Brian Peterson (PDR) IS NOT getting updated, leave him out
SELECT COUNT(*) FROM #techs;
DROP TABLE #techs;
SELECT a.departmentkey, a.employeenumber, a.firstname, a.lastname, a.employeenumber, 
  round(c.previousHourlyRate, 2) AS prev_rate, c.techTeamPercentage
INTO #techs
FROM tpTechs a
INNER JOIN tpTeamTechs b on a.techkey = b.techkey
INNER JOIN tpTechValues c on a.techkey = c.techkey
  AND c.thrudate > curdate()
WHERE b.thrudate > curdate()
--UNION 
--SELECT departmentkey, employeenumber, firstname, lastname, employeenumber, round(otherrate, 2) AS prev_rate
--FROM bsflatratetechs  
--WHERE thrudate > curdate();

-- 2015 pay
-- DROP TABLE #pay_2015;
SELECT c.firstname, c.lastname, c.employeenumber, sum(b.amount) as paid_2015
INTO #pay_2015
FROM dds.ext_pyptbdta a
INNER JOIN dds.ext_pyhscdta b on a.company_number = b.company_number
  AND a.payroll_run_number = b.payroll_run_number
  AND b.code_type = '1'
  AND b.code_id IN ('87','79','75') 
INNER JOIN #techs c on TRIM(b.employee_number) = c.employeenumber
WHERE a.payroll_cen_year = 115
GROUP BY c.firstname, c.lastname, c.employeenumber;

-- date_range
SELECT * FROM #date_range
SELECT
  cast(trim(substring(CAST(from_date AS sql_char),5,2)) + '/' + 
      trim(substring(CAST(from_date AS sql_char),7,2)) + '/' +
      LEFT(trim(CAST(from_date AS sql_char)),4) AS sql_Date) AS from_date,
  cast(trim(substring(CAST(thru_date AS sql_char),5,2)) + '/' + 
      trim(substring(CAST(thru_date AS sql_char),7,2)) + '/' +
      LEFT(trim(CAST(thru_date AS sql_char)),4) AS sql_Date) AS thru_date  
INTO #date_range          
FROM (
  select MIN(payroll_start_date) AS from_date, MAX(payroll_end_date) AS thru_date
  FROM dds.ext_pyptbdta
  WHERE company_number = 'RY1'
    AND payroll_cen_year = 115) x; 

-- clock hours
SELECT e.firstname, e.lastname, e.employeenumber, SUM(c.clockhours) AS clock_hours
INTO #clock_hours_2015
FROM dds.day a
INNER JOIN #date_range b on a.thedate BETWEEN b.from_date AND b.thru_date  
INNER JOIN dds.edwClockHoursFact c on a.datekey = c.datekey  
  AND c.clockhours <> 0
INNER JOIN dds.edwEmployeeDim d on c.employeekey = d.employeekey  
INNER JOIN #techs e on d.employeenumber = e.employeenumber
GROUP BY e.firstname, e.lastname, e.employeenumber;
  
SELECT a.*, b.paid_2015, c.clock_hours, round(b.paid_2015/c.clock_hours, 2)
FROM #techs a
LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber 
ORDER BY a.departmentkey, a.lastname

SELECT a.*, iif(current_pto_rate > new_pto_rate, 'current', 'new')
FROM (
  SELECT TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
    round(b.paid_2015/c.clock_hours, 2) AS new_pto_rate
  FROM #techs a
  LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber 
  WHERE departmentkey = 13) a
--WHERE iif(current_pto_rate > new_pto_rate, 'current', 'new') = 'current'
ORDER BY a.tech


SELECT a.*, iif(current_pto_rate > new_pto_rate, 'current', 'new')
FROM (
  SELECT TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
    round(b.paid_2015/c.clock_hours, 2) AS new_pto_rate
  FROM #techs a
  LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber 
  WHERE departmentkey = 18) a
ORDER BY a.tech


-- 2/7/116: new pto rate goes INTO effect this pay period
-- IN accordance with ben's new pussy approach to tech pay, IF the new
-- pto rate IS less than current pto rate, don't change it
--
-- also, josh walton (techkey 34) already updated for this pay period AS 
-- part of his techteampercentage UPDATE

SELECT departmentkey, a.employeenumber, TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
  round(b.paid_2015/c.clock_hours, 2) AS new_pto_rate
FROM #techs a
LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber 
ORDER by a.departmentkey, a.lastname

-- those techs tag get an increase IN pto rate (TABLE y)
-- this works to accommodate the pussy mode of who gets changed AS well AS
-- josh walton who has already been changed
SELECT *
FROM (
  SELECT departmentkey, a.employeenumber, TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
    round(b.paid_2015/c.clock_hours, 2) AS new_pto_rate
  FROM #techs a
  LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber) x
LEFT JOIN (
  SELECT departmentkey, a.employeenumber, TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
    round(b.paid_2015/c.clock_hours, 2) AS new_pto_rate
  FROM #techs a
  LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber
  WHERE round(b.paid_2015/c.clock_hours, 2) > a.prev_rate) y on x.employeenumber = y.employeenumber
ORDER by x.departmentkey, x.tech


SELECT a.departmentkey, a.employeenumber, techkey, TRIM(a.lastname) + ' ' + a.firstname AS tech, a.prev_rate AS current_pto_rate, 
  round(b.paid_2015/c.clock_hours, 2) AS new_pto_rate
FROM #techs a
INNER JOIN tptechs aa on a.employeenumber = aa.employeenumber
  AND aa.thrudate > curdate()
LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber
WHERE round(b.paid_2015/c.clock_hours, 2) > a.prev_rate
ORDER BY a.departmentkey, a.lastname

-- TRY this on 2 techs (42, 51), make sure it ALL works ok 
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @tech_key integer;
DECLARE @new_pto_rate double;
DECLARE @tech_percentage double;
DECLARE @tech_key_cur cursor AS 
  SELECT techkey, round(b.paid_2015/c.clock_hours, 2) AS new_pto_rate, 
    techTeamPercentage
  FROM #techs a
  INNER JOIN tptechs aa on a.employeenumber = aa.employeenumber
    AND aa.thrudate > curdate()
  LEFT JOIN #pay_2015 b on a.employeenumber = b.employeenumber
  LEFT JOIN #clock_hours_2015 c on a.employeenumber = c.employeenumber
  WHERE round(b.paid_2015/c.clock_hours, 2) > a.prev_rate; 
@from_date = '02/07/2016';
@thru_date = @from_date - 1; 
BEGIN TRANSACTION;
TRY 
  OPEN @tech_key_cur;
  WHILE FETCH @tech_key_cur DO 
    @tech_key = @tech_key_cur.techkey;
    @new_pto_rate = @tech_key_cur.new_pto_rate;
    @tech_percentage = @tech_key_cur.techTeamPercentage;
    UPDATE tpTechValues
    SET thrudate = @thru_date 
    WHERE techkey = @tech_key
      AND thrudate > curdate();
    INSERT INTO tpTechValues(techkey,fromdate,techteampercentage,previoushourlyrate)
    values (@tech_key, @from_date, @tech_percentage, @new_pto_rate);
    UPDATE tpdata 
    SET techHourlyRate = @new_pto_rate
    WHERE techkey = @tech_key
      AND thedate = @from_date;
  END WHILE;
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  


---------------------------------------------------------------------------------------------------
-- 1/13/17 
---------------------------------------------------------------------------------------------------
