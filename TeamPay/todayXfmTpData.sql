CREATE PROCEDURE todayXfmTpData()
BEGIN
/*
EXECUTE PROCEDURE todayXfmTpData();
*/
BEGIN TRANSACTION; -- clock hours
TRY
  TRY 
    UPDATE tpdata
    SET TechClockHoursDay = x.ClockHours
    FROM (
      SELECT b.thedate, c.employeenumber, round(SUM(clockhours), 4) AS clockhours
      FROM dds.factClockHoursToday a
      INNER JOIN dds.day b on a.datekey = b.datekey
        AND b.thedate = curdate()
      LEFT JOIN dds.edwEmployeeDim c on a.employeekey = c.employeekey
      WHERE c.employeenumber IN (
        SELECT DISTINCT employeenumber
        FROM tpData)
      GROUP BY b.thedate, c.employeenumber) x  
    WHERE tpdata.thedate = x.thedate
      AND tpdata.employeenumber = x.employeenumber;  
    -- ClockHoursPPTD
    UPDATE tpData
    SET TechClockHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday, SUM(b.techclockhoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techclockhoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;   
      
    -- teamclockhoursDay
    UPDATE tpData 
    SET teamclockhoursday = x.teamclockhoursday
    FROM (
      SELECT thedate, teamkey, SUM(techclockhoursday) AS teamclockhoursday
      FROM tpdata
      GROUP BY thedate, teamkey) x 
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;  
    -- teamclockhours pptd
    UPDATE tpData
    SET teamclockhourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamclockhoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamclockhoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;           
  CATCH ALL 
    RAISE todayTpData(1, 'Clock Hours ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- flag hours
TRY
  TRY   
    -- TechFlagHoursDay 
    UPDATE tpData  
    SET TechFlagHoursDay = x.flaghours
    FROM (
      SELECT a.technumber, a.thedate, round(sum(coalesce(b.flaghours, 0)), 2) AS flaghours
      FROM tpData a
      LEFT JOIN (    
        SELECT d.thedate, ro, line, c.technumber, a.flaghours
        FROM dds.todayFactRepairOrder a
        INNER JOIN dds.dimTech c on a.techkey = c.techkey
        INNER JOIN dds.day d on a.flagdatekey = d.datekey
        WHERE a.flaghours <> 0
          AND d.thedate = curdate()
          AND c.technumber IN (
            SELECT technumber
            FROM tptechs)) b on a.technumber = b.technumber AND a.thedate = b.thedate   
      WHERE a.thedate = curdate()      
      GROUP BY a.technumber, a.thedate) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.technumber = x.technumber;      
    -- TechFlagHoursPPTD  
    UPDATE tpData
    SET TechFlagHoursPPTD = x.PPTD
    FROM (
      SELECT a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday, SUM(b.techflaghoursday) AS pptd
      FROM tpData a, tpdata b
      WHERE a.payperiodseq = b.payperiodseq
        AND a.techkey = b.techkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.techkey, a.payperiodseq, a.techflaghoursday) x
    WHERE tpData.thedate = x.thedate
      AND tpData.techkey = x.techkey;      
    -- teamflaghoursday
    UPDATE tpData
    SET TeamFlagHoursDay = x.hours
    FROM (
      SELECT thedate, teamkey, SUM(techflaghoursday) AS hours
      FROM tpdata 
      WHERE thedate = curdate()
      GROUP BY thedate, teamkey) x
    WHERE tpData.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;  
    -- teamflaghour pptd  
    UPDATE tpData
    SET teamflaghourspptd = x.pptd
    FROM (
      SELECT a.thedate, a.teamkey, a.payperiodseq, a.dayhours, round(SUM(b.dayhours), 2) AS pptd
      FROM (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) a,
        (
        SELECT thedate, teamkey, teamname, payperiodseq, teamflaghoursday AS dayhours
        FROM tpdata
        GROUP BY thedate, teamkey, teamname, payperiodseq, teamflaghoursday) b  
      WHERE a.payperiodseq = b.payperiodseq
        AND a.teamkey = b.teamkey
        AND b.thedate <= a.thedate
      GROUP BY a.thedate, a.teamkey, a.payperiodseq, a.dayhours) x
    WHERE tpData.thedate = x.thedate
      AND tpData.teamkey = x.teamkey;    
  CATCH ALL 
    RAISE tpdata(2, 'Flag Hours ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

BEGIN TRANSACTION; -- team proficiency
TRY
  TRY  
    UPDATE tpdata
    SET teamprofday = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhoursday = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhoursday, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay) AS TotalFlag, 
          teamclockhoursday
        FROM tpdata
        WHERE thedate = curdate()
        GROUP BY thedate, teamkey, 
          (teamflaghoursday + teamOtherHoursDay + teamTrainingHoursDay + TeamFlagHourAdjustmentsDay), 
          teamclockhoursday) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;     
    -- team prof pptd
    UPDATE tpdata
    SET teamprofpptd = x.prof
    FROM (
      SELECT a.*,
        CASE 
          WHEN TotalFlag = 0 OR teamclockhourspptd = 0 THEN 0
          ELSE round(100 * TotalFlag/teamclockhourspptd, 2)
        END AS prof
      FROM (
        SELECT thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD) AS TotalFlag, 
          teamclockhourspptd
        FROM tpdata
        GROUP BY thedate, teamkey, 
          (teamflaghourspptd + teamOtherHoursPPTD + teamTrainingHoursPPTD + TeamFlagHourAdjustmentsPPTD), 
          teamclockhourspptd) a) x
    WHERE tpdata.thedate = x.thedate
      AND tpdata.teamkey = x.teamkey;           
  CATCH ALL 
    RAISE tpdata(3, 'Team Proficiency ' + __errtext);
  END TRY;        
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

         
END;
