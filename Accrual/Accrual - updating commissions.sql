SELECT storecode, employeenumber, employeekey, name,
  '03/25/2012' AS FromDate, '03/31/2012' AS ThruDate, 0 AS Amount
FROM edwEmployeeDim ex
WHERE EmployeeKeyFromDate = (
    SELECT MAX(EmployeekeyFromDate)
    FROM edwEmployeeDim
    WHERE EmployeeNumber = ex.Employeenumber
      AND EmployeeKeyFromDate <= '03/31/2012'
    GROUP BY employeenumber)
  AND PayPeriodCode = 'B'
  AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')
  

INSERT INTO AccrualCommissions
SELECT a.storecode, a.employeenumber, a.employeekey, a.name,
  '04/22/2012' AS FromDate, '04/30/2012' AS ThruDate, 0 AS Amount
FROM edwEmployeeDim a
LEFT JOIN ( -- clock hours for the interval
    SELECT storecode, employeenumber, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
      sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
      sum(holidayhours) AS holidayhours
    FROM day d
    INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
    LEFT JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
    WHERE thedate BETWEEN '04/22/2012' AND '04/30/2012'
    GROUP BY storecode, employeenumber) b ON a.storecode = b.storecode 
  AND a.employeenumber = b.employeenumber 
WHERE EmployeeKeyFromDate = (
    SELECT MAX(EmployeekeyFromDate)
    FROM edwEmployeeDim
    WHERE EmployeeNumber = a.Employeenumber
      AND EmployeeKeyFromDate <= '04/30/2012'
    GROUP BY employeenumber)
  AND PayPeriodCode = 'B'
  AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')  
  AND b.storecode IS NOT NULL 
  
  
SELECT *
FROM AccrualCommissions  
WHERE thrudate = '02/29/2012'
AND amount <> 0
ORDER BY storecode, name


SELECT *
FROM accrualCommissions
WHERE fromdate = '04/22/2012'
AND name LIKE 'R%'
ORDER BY storecode, name

/* 6/1 */
-- this needs to be a script
DECLARE @fromdate string;
DECLARE @thrudate string;

@fromdate = '06/17/2012'; -- last payroll enddate + 1
@thrudate = '06/30/2012'; -- EOM
INSERT INTO AccrualCommissions
SELECT a.storecode, a.employeenumber, a.employeekey, a.name,
  CAST(@fromdate AS sql_Date) AS FromDate, CAST(@thrudate AS sql_date) AS ThruDate, 0 AS Amount
FROM edwEmployeeDim a
LEFT JOIN ( -- clock hours for the interval
    SELECT storecode, employeenumber, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
      sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
      sum(holidayhours) AS holidayhours
    FROM day d
    INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
    LEFT JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
    WHERE thedate BETWEEN CAST(@fromdate AS sql_date) AND CAST(@thrudate AS sql_date)
    GROUP BY storecode, employeenumber) b ON a.storecode = b.storecode 
  AND a.employeenumber = b.employeenumber 
WHERE EmployeeKeyFromDate = (
    SELECT MAX(EmployeekeyFromDate)
    FROM edwEmployeeDim
    WHERE EmployeeNumber = a.Employeenumber
      AND EmployeeKeyFromDate <= CAST(@thrudate AS sql_date)
    GROUP BY employeenumber)
  AND PayPeriodCode = 'B'
  AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')  
  AND b.storecode IS NOT NULL; 
  
