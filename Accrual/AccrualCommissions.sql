/*
create TABLE AccrualCommissions (
  StoreCode cichar(3),
  EmployeeNumber cichar(7),
  EmployeeKey integer,
  Name cichar(25),
  FromDate date,
  ThruDate date,
  Amount money) IN database;
*/   
INSERT INTO AccrualCommissions  
SELECT storecode, employeenumber, employeekey, name, '02/12/2012','02/29/2012', 0
FROM (  
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
    PayPeriodCode, DistCode, salary, hourlyrate
  FROM edwEmployeeDim ex
  WHERE EmployeeKeyFromDate = (
      SELECT MAX(EmployeekeyFromDate)
      FROM edwEmployeeDim
      WHERE EmployeeNumber = ex.Employeenumber
        AND EmployeeKeyFromDate <= '02/11/2012'
      GROUP BY employeenumber)
    AND PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')) x
--/*WHERE name IN ('robles,rudolph', 'berg,stephen r.','haney,amanda l','dalen,benjamin a','vacura,scott','molstad,brandon l','paige,timothy a', 'hunter,robert l')  -- pdq ok  
--/*WHERE name IN ('allrich,todd m','novak,steve ray','neumann,andrew d') -- ry3 ok
/*
WHERE name IN ('mckay, david b','dangerfield,joel','telken,tony','olson,chad',
  'brouillet,christopher','carl,justin j','sobolik,paul a.','mcgee,justin d',
  'grohs,dustin j','johnson,brandon j','rud,joseph','steffens,darrin j',
  'kliner,patrick h','adams,jeff m','tupa,brandon s')
  AND storecode = 'RY2' -- ry2 ok
*/  
/*
WHERE name IN (
'BURGER,JERAMY R',          
'DRISCOLL,TERRANCE',        
            
'EGSTAD,KEITH R',           
'FRANKLIN,WILLIAM F.',      
'GARDNER,CHAD A',           
       
'IVERSON,ARNOLD W.',        
'JACOBSON,PETER A',         
'JOHNSON,CODY',             
'LENE,RYAN',                
'MATEJCEK,WILLIAM J',       
'MAVITY,ROBERT',            
'OBREGON,RICHARD A',        
'PETERSON,BRIAN D.',        
'PETERSON,MAVRIK',          
         
'ROSE,CORY',                
'ROSENAU,JEREMY',           
'SEVIGNY,SCOTT',            
           
'TAYLOR,CHAD R',            
'WALDEN,CHRIS W.',          
'WALTON,JOSHUA',
'shereck,loren') AND storecode = 'RY1'
*/
/*
 -- ry1 tech ok
WHERE name IN ('girodat,jeff s.','mcveigh,dennis','hunter, jason w','swift,kenneth e',
  'van heste II,fred e','waldbauer,alex','morrow,kyle g','evenson,garrett',
  'koller,mark d.','heffernan, josh')
AND storecode = 'RY1'
*/
-- WHERE name IN ('ostlund,anne b','hill,brian','hankin,katherine m.','steinke,mark','lueker,david c') -- bs cons ok
/*
WHERE name IN ('espelund,kenneth mike','evavold,daniel j','troftgruben,rodney',
'bursinger,travis c','ramberg,gary','kilmer,justin','moulds,crystal d',
'kacher,richard alex','mcmillan,shauna','longoria,beverley anne')
*/
--WHERE name = 'paschke,matthew g'
--WHERE name IN ('mccoy,james f','diemert,jay','lee,raymond s.','seeba,bradley f.','laughlin,larry j','harmon,terry j.','kolstad,kirk r.','jerstad,kenneth','henry,dirk l','feist,ludwig h')  -- ry1 parts
WHERE name = 'jordan,joseph p.'

--ORDER BY name

--SELECT * FROM AccrualCommissions ORDER BY storecode, name













