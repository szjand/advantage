/*
-- 9/28, got to get this shit done
-- implement physical tables for interim steps
Salaried: any employee active during the interval who was salaried at anytime during the inteval
/* issues
the distcode GIL has Vacation/Holiday/PTO as as 130224 for both 12301 & 12304
  should be 12301/130201 and for 12304/130200
  Resulted IN constraint violation IN AccrualHourly2
  temporary fix was to exclude Vac/Hol/PTO with 0 amounts
but of fucking course, september accrual gil has vacation so this bombs  
*/ 
/*
-- 10/1, think i need to ADD employeekey to the interim tables for ease IN
--       processing other shit FROM this data, stuff LIKE clockhours        

-- 11/2
       1. shit, had to modify the final output, AccrualFileForJeri, with the detail
       flat rate numbers
       2. offset numbers are fucked up
             a. gregs numbers are NOT included, his amounts are generated based AND input
             separate FROM everything else.
             The offset is generated from accrualsalaried2, accrualhourly2 & accrualcommissions2
             which DO include gregs amounts
             b. nor DO they include the changes done for detail
NOT fixed yet  
          
-- 12/3/13
1. detail, get the spreadsheet FROM ben OR kim, enter the flat rate bonus value
     AS an accrual commission, just LIKE for body shop, writers, ry2 techs
     
2. AccrualHourly1 failed with pk violation, pk IS employeenumber/grossaccount(GROSS_EXPENSE_ACT_)
   emp# 168815 & 118010 both return multiple rows with differed FIXED_DED_AMT values   
   after looking at pydeduct AND talking to kim, both employees have multiple (91, 99) retirement
   deductions, each with differed fixed deduction amounts  
   
  *666 the stupid fix, for both those employees the fixed ded for 91 IS 3, for 99 IS 1
       only other emp with 99 is 11650 
       so take the one for the 2 multiples with the largest deduction
  fuck, it IS only accrual AND the difference will be smallish enuf
  AND 
    case 
      when h.employee_number = '11650' then h.ded_pay_code = '99'
      else h.DED_PAY_CODE = '91'
    END;   
    
3. hard coded an offset for greg's numbers    

-- 1/3/14
1. sales consultant accrual: added sales consultants to AccrualCommissions
     that did NOT WORK out, DO a separate SET of files for sales
2. team flag rate has begun IN the shop, need to ADD IN their "bonuses" AS well IN AccrualCommissions


1/3/14: 
1/2/15 same thing
  per jeri, we always DO it, i DO NOT remember ever doing it, but
  the year END accrual includes sales people, 
  see script: Accrual - Sales END of Year Only
    populates TABLE AccrualSales2
    which IS unioned with ALL the others IN the INSERT INTO AcrrualFileForJeri
    AND reversing entries
    
2/4/14: 
  1. non null constraint violation on AccrualSalaried1.FicaPercentage
     the problem IS that stgArkonaPYNCTRL has NOT been updated with values for 2014
     did a manual UPDATE to get the 2014 values
     
6/2/14 *c*
  INSERT INTO AccrualClockHours fails because amanda changed her name IN the
  accrual period, DO NOT know why i am grouping on name IN that query, for this
  time remove the name FROM the grouping     
  
12/1/14 
  exclude dist code AFTE FROM accrual  
*/

/*
DROP table AccrualSalaried1;
CREATE TABLE AccrualSalaried1 (
  StoreCode cichar(3) CONSTRAINT NOT NULL,
  EmployeeNumber cichar(9) CONSTRAINT NOT NULL ,
  TotalPay money CONSTRAINT NOT NULL,
  GrossDistributionPercentage double(2) CONSTRAINT NOT NULL,
  GrossAccount cichar(10) CONSTRAINT NOT NULL,
  FicaAccount cichar(10) CONSTRAINT NOT NULL,
  MedicareAccount cichar(10) CONSTRAINT NOT NULL,
  RetirementAccount cichar(10) CONSTRAINT NOT NULL,
  FicaPercentage double(2) CONSTRAINT NOT NULL,
  MedicarePercentage double(2) CONSTRAINT NOT NULL,
  FicaExempt money CONSTRAINT NOT NULL,
  MedicareExempt money CONSTRAINT NOT NULL,
  FixedDeductionAmount double(2) CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (Employeenumber, GrossAccount)) IN database;
  
DROP TABLE AccrualSalaried2; 
CREATE TABLE AccrualSalaried2 (
  Category cichar(12) CONSTRAINT NOT NULL,
  EmployeeNumber cichar(9) CONSTRAINT NOT NULL,
  Account cichar(10) CONSTRAINT NOT NULL,
  Amount money CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (employeenumber, category, account)) IN database;

DROP TABLE AccrualDates;  
CREATE TABLE AccrualDates (
  Journal cichar(3) CONSTRAINT NOT NULL,
  theDate cichar(10) CONSTRAINT NOT NULL,
  Document cichar(9) CONSTRAINT NOT NULL,
  Reference cichar(9) CONSTRAINT NOT NULL,
  Description cichar(16) CONSTRAINT NOT NULL,
  FromDate date CONSTRAINT NOT NULL ,
  ThruDate date CONSTRAINT NOT NULL ,
  NumDays integer CONSTRAINT NOT NULL ,
  NumDays14 double(4) CONSTRAINT NOT NULL ) IN database; 
  
DROP TABLE AccrualClockHours;
CREATE TABLE AccrualClockHours (
  EmployeeNumber cichar(9) CONSTRAINT NOT NULL,
  RegularHours double(2) CONSTRAINT NOT NULL,  
  OvertimeHours double(2) CONSTRAINT NOT NULL,
  VacationHours double(2) CONSTRAINT NOT NULL,
  PTOHours double(2) CONSTRAINT NOT NULL,
  HolidayHours double(2) CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (employeenumber)) IN database;

DROP TABLE AccrualHourly1;  
CREATE TABLE AccrualHourly1 (
  StoreCode cichar(3) CONSTRAINT NOT NULL,
  EmployeeNumber cichar(9) CONSTRAINT NOT NULL,
  RegularPay money CONSTRAINT NOT NULL,
  OvertimePay money CONSTRAINT NOT NULL,
  VacationPay money CONSTRAINT NOT NULL, 
  PTOPay money CONSTRAINT NOT NULL, 
  HolidayPay money CONSTRAINT NOT NULL,
  TotalNonOTPay money CONSTRAINT NOT NULL,
  GrossDistributionPercentage double(2) CONSTRAINT NOT NULL,
  GrossAccount cichar(10) CONSTRAINT NOT NULL,
  OvertimeAccount cichar(10) CONSTRAINT NOT NULL,
  VacationAccount cichar(10) CONSTRAINT NOT NULL,
  HolidayAccount cichar(10) CONSTRAINT NOT NULL,
  PTOAccount cichar(10) CONSTRAINT NOT NULL,
  FicaAccount cichar(10) CONSTRAINT NOT NULL,
  MedicareAccount cichar(10) CONSTRAINT NOT NULL,
  RetirementAccount cichar(10) CONSTRAINT NOT NULL,
  FicaPercentage double(2) CONSTRAINT NOT NULL,
  MedicarePercentage double(2) CONSTRAINT NOT NULL,
  FicaExempt money CONSTRAINT NOT NULL,
  MedicareExempt money CONSTRAINT NOT NULL,
  FixedDeductionAmount double(2) CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (Employeenumber, GrossAccount)) IN database;
  
DROP TABLE AccrualHourly2; 
CREATE TABLE AccrualHourly2 (
  Category cichar(12) CONSTRAINT NOT NULL,
  EmployeeNumber cichar(9) CONSTRAINT NOT NULL,
  Account cichar(10) CONSTRAINT NOT NULL,
  Amount money CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (employeenumber, category, account)) IN database;
  
DROP TABLE AccrualCommissions2; 
CREATE TABLE AccrualCommissions2 (
  Category cichar(12) CONSTRAINT NOT NULL,
  EmployeeNumber cichar(9) CONSTRAINT NOT NULL,
  Account cichar(10) CONSTRAINT NOT NULL,
  Amount money CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (employeenumber, category, account)) IN database;

DROP TABLE AccrualFileForJeri;
CREATE TABLE AccrualFileForJeri (
  journal cichar(3) CONSTRAINT NOT NULL,
  date cichar(10) CONSTRAINT NOT NULL,
  account cichar(10) CONSTRAINT NOT NULL,
  control cichar(9) CONSTRAINT NOT NULL,
  document cichar(9) CONSTRAINT NOT NULL,
  reference cichar(9) CONSTRAINT NOT NULL,
  amount double(2) CONSTRAINT NOT NULL,
  description cichar(16) CONSTRAINT NOT NULL,
  CONSTRAINT PK PRIMARY KEY (control, account)) IN database;
  
  
AccrualDates:  1 row with the date intervals AND constants necessary to generate Jeri's file
AccrualSalaried1: any employee that was salaried at anytime during the accrual interval
                  percentage of salary based on interval/14 (TotalPay)
                  1 row per employeenumber/GrossAccount
                  
                  
 */ 
 
/*
11/1/13
what are the fucking dates
SELECT DISTINCT biweeklypayperiodstartdate, biweeklypayperiodenddate FROM day WHERE yearmonth = 201404
*/

DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @theDate string;
--@fromDate = '08/25/2013';
--@thrudate = '08/31/2013';
--@theDate = '08/31/2013';
--@fromDate = '09/22/2013'; 
--@thrudate = '09/30/2013';
--@theDate = '09/30/2013';
--@fromDate = '10/20/2013'; -- last payroll enddate + 1
--@thrudate = '10/31/2013';
--@fromDate = '09/21/2014'; 
--@thrudate = '09/30/2014';
--@theDate = '09/30/2014';
--@fromDate = '10/19/2014'; 
--@thrudate = '10/31/2014';
--@theDate = '10/31/2014';
@fromDate = '12/14/2014'; 
@thrudate = '12/31/2014';
@theDate = '12/31/2014';
DELETE FROM AccrualDates;
INSERT INTO AccrualDates 
SELECT 'GLI', @theDate,
  'GLI' + 
    CASE 
      WHEN month(@thrudate) < 10 THEN '0' +  trim(CAST(month(@thrudate) AS sql_char)) 
      ELSE trim(CAST(month(@thrudate) AS sql_char))
    END  +
  trim(CAST(dayofmonth(@thrudate) AS sql_char)) +
  right(trim(CAST(year(@thrudate) AS sql_char)), 2),
  'GLI' +
    CASE 
      WHEN month(@thrudate) < 10 THEN '0' +  trim(CAST(month(@thrudate) AS sql_char)) 
      ELSE trim(CAST(month(@thrudate) AS sql_char))
    END  +
  trim(CAST(dayofmonth(@thrudate) AS sql_char)) +
  right(trim(CAST(year(@thrudate) AS sql_char)), 2),  
  'Payroll Accrual', @fromdate,@thrudate, 
  timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1,
  (timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1)/14.0
FROM system.iota;  

DELETE FROM AccrualSalaried1;
INSERT INTO AccrualSalaried1
SELECT a.storecode, a.employeenumber, 
  a.salary * aa.NumDays14 AS TotalPay,
  d.GROSS_DIST, d.GROSS_EXPENSE_ACT_, 
  d.EMPLR_FICA_EXPENSE, d.EMPLR_MED_EXPENSE, d.EMPLR_CONTRIBUTIONS,
  e.yficmp, e.ymedmp, coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
  coalesce(h.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
FROM edwEmployeeDim a
INNER JOIN AccrualDates aa on 1 = 1
INNER JOIN ( -- the possibly dodgy assumption here IS that employeekeys are sequential, but they are autoinc 
  SELECT a.Employeenumber, MAX(a.employeekey) AS employeekey
  FROM edwEmployeeDim a
  INNER JOIN AccrualDates aa on 1 = 1
  INNER JOIN edwEmployeeDim b on a.employeekey =  b.employeekey -- ALL employeekeys valid for the interval
    AND b.employeekeyFromDate < aa.thrudate
    AND b.employeekeyThruDate > aa.fromdate
  WHERE a.hiredate <= aa.thrudate -- only folks active during the interval
    AND a.termdate >= aa.fromdate
    AND a.storecode <> 'ry3'  
    AND a.employeenumber NOT IN ('190915','1160100')
    AND a.PayPeriodCode = 'B'
    AND a.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 
      'BSM','SALE','TEAM','USCM','USCD', 'STEV', 'AFTE') 
    AND a.PayRollClassCode = 'S' -- salaried only
  GROUP BY a.employeenumber) x on a.employeekey = x.employeekey
LEFT JOIN stgArkonaPYACTGR d ON a.StoreCode = d.COMPANY_NUMBER -- distribution
  AND a.DistCode = d.DIST_CODE
  AND d.CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL e ON a.storecode = e.yco#
  AND e.ycyy = 100 + (year(aa.ThruDate) - 2000) -- 112 -  
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) f ON a.employeenumber = f.EMPLOYEE_NUMBER    
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) g ON a.employeenumber = g.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT h ON a.storecode = h.COMPANY_NUMBER
  AND a.employeenumber = h.EMPLOYEE_NUMBER 
  AND h.DED_PAY_CODE IN ('91', '99');  
 
  
DELETE FROM AccrualSalaried2;  
INSERT INTO AccrualSalaried2
SELECT 'Gross', EmployeeNumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * TotalPay AS Amount
FROM AccrualSalaried1
UNION ALL 
SELECT 'FICA', EmployeeNumber, FicaAccount AS Account,
  round((GrossDistributionPercentage/100.0) * (TotalPay - (coalesce(FicaExempt, 0)) * b.NumDays14) * FicaPercentage/100.0, 2) AS Amount
FROM AccrualSalaried1 a
INNER JOIN AccrualDates b on 1 = 1
UNION ALL 
SELECT 'MEDIC', EmployeeNumber, MedicareAccount AS Account,
--  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * (NumDays/14)) * MedicarePercentage/100.0, 2) AS Amount
  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * b.NumDays14) * MedicarePercentage/100.0, 2) AS Amount
FROM AccrualSalaried1 a
INNER JOIN AccrualDates b on 1 = 1
UNION ALL 
SELECT 'RETIRE', EmployeeNumber, RetirementAccount AS Account,
CASE 
  WHEN FixedDeductionAmount IS NULL THEN 0
  ELSE 
  CASE 
    WHEN (TotalPay * FixedDeductionAmount/200.0) < .02 * TotalPay 
      THEN round((GrossDistributionPercentage/100.0 * TotalPay) * (FixedDeductionAmount/200.0), 2)
    ELSE round((GrossDistributionPercentage/100.0) * TotalPay * .02, 2) 
  END               
END AS Amount  
FROM AccrualSalaried1;

-- this IS WHERE any employee that was salaried for the interval IS exempted
DELETE FROM AccrualClockHours;
INSERT INTO AccrualClockHours
SELECT c.employeenumber, SUM(regularHours) AS regularHours, 
  SUM(OvertimeHours) AS Overtimehours, SUM(VacationHours) AS VacationHours,
  SUM(PTOHours) AS PTOHours, SUM(HolidayHours) AS HolidayHours
FROM edwClockHoursFact a
INNER JOIN AccrualDates aa on 1 = 1
INNER JOIN day b on a.datekey = b.datekey
  AND b.thedate BETWEEN aa.fromdate AND aa.thrudate //(SELECT FromDate FROM AccrualDates) AND (SELECT ThruDate FROM AccrualDates)
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey  
WHERE regularhours + overtimehours + vacationhours + ptohours + holidayhours > 0
  AND NOT EXISTS ( -- emp# NOT IN #salaried
    SELECT 1
    FROM AccrualSalaried1
    WHERE employeenumber = c.employeenumber)
-- *c*   
-- GROUP BY c.employeenumber;--, c.name;
GROUP BY c.employeenumber, c.name;

DELETE FROM AccrualHourly1;
INSERT INTO AccrualHourly1  
SELECT b.storecode, b.employeenumber, 
  RegularHours*HourlyRate as RegularPay, 
  OvertimeHours*HourlyRate*1.5 AS OvertimePay, VacationHours*HourlyRate AS VacationPay,
  PTOHours*HourlyRate AS PTOPay, HolidayHours*HourlyRate AS HolidayPay, 
  RegularHours*HourlyRate + VacationHours*HourlyRate + PTOHours*HourlyRate + HolidayHours*HourlyRate AS TotalNonOTPay,  
  d.GROSS_DIST, d.GROSS_EXPENSE_ACT_, d.OVERTIME_ACT_, 
  d.VACATION_EXPENSE_ACT_, d.HOLIDAY_EXPENSE_ACT_, d.SICK_LEAVE_EXPENSE_ACT_,
  d.EMPLR_FICA_EXPENSE, d.EMPLR_MED_EXPENSE, d.EMPLR_CONTRIBUTIONS,
  e.yficmp, e.ymedmp, coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
  coalesce(h.FIXED_DED_AMT, 0) AS FIXED_DED_AMT      
FROM AccrualClockHours a
INNER JOIN AccrualDates aa on 1 = 1
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
INNER JOIN (
  SELECT a.Employeenumber, MAX(a.employeekey) AS employeekey
  FROM edwEmployeeDim a
  INNER JOIN AccrualDates aa on 1 = 1
  INNER JOIN edwEmployeeDim b on a.employeekey =  b.employeekey -- ALL employeekeys valid for the interval
    AND b.employeekeyFromDate < aa.ThruDate //(SELECT ThruDate FROM AccrualDates)
    AND b.employeekeyThruDate > aa.FromDate //(SELECT FromDate FROM AccrualDates)
  WHERE a.hiredate <= aa.ThruDate //(SELECT ThruDate FROM AccrualDates) -- only folks active during the interval
    AND a.termdate >= aa.FromDate //(SELECT FromDate FROM AccrualDates)
    AND a.storecode <> 'ry3'  
    AND a.employeenumber NOT IN ('190915','1160100')
    AND a.PayPeriodCode = 'B'
    AND a.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 
      'BSM','SALE','TEAM','USCM','USCD', 'STEV', 'AFTE') 
    AND a.PayRollClassCode = 'H' -- salaried only
  GROUP BY a.employeenumber) c on b.employeekey = c.employeekey  
LEFT JOIN stgArkonaPYACTGR d ON b.StoreCode = d.COMPANY_NUMBER -- distribution
  AND b.DistCode = d.DIST_CODE
  AND d.CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL e ON b.storecode = e.yco#
--  AND e.ycyy = 100 + (year(@ThruDate) - 2000) -- 112 -  
  AND e.ycyy =  100 + (year(aa.ThruDate) - 2000) //(SELECT 100 + year(ThruDate) - 2000 FROM AccrualDates)
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) f ON b.employeenumber = f.EMPLOYEE_NUMBER    
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) g ON b.employeenumber = g.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT h ON b.storecode = h.COMPANY_NUMBER
  AND b.employeenumber = h.EMPLOYEE_NUMBER 
--  AND h.DED_PAY_CODE IN ('91', '99');
-- *666
  AND 
    case 
      when h.employee_number = '11650' then h.ded_pay_code = '99'
      else h.DED_PAY_CODE = '91'
    END; 
       
--< team pay / flat rate adjustment -------------------------------------------<       

/*  
team pay adjustment 

*/
UPDATE accrualHourly1
SET regularPay = x.commissionPay,
    overtimePay = 0
FROM accrualTpAdjustment x   
WHERE accrualHourly1.employeenumber = x.employeenumber
  AND monthEndDate = @thruDate;

UPDATE accrualHourly1
SET totalNonOtPay =  regularPay + vacationpay + ptopay + holidaypay
WHERE employeenumber IN (
  SELECT employeenumber 
  FROM accrualTpAdjustment
  WHERE monthEndDate = @thruDate);

--/> team pay / flat rate adjustment ------------------------------------------/> 

DELETE FROM AccrualHourly2;  
INSERT INTO AccrualHourly2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * RegularPay AS Amount
FROM AccrualHourly1
UNION ALL
SELECT 'OT', employeenumber, OvertimeAccount AS Account,
  GrossDistributionPercentage/100.0 * OvertimePay AS Amount
FROM AccrualHourly1
WHERE OvertimePay <> 0
UNION ALL 
SELECT 'VAC', employeenumber, VacationAccount AS Account,
  GrossDistributionPercentage/100.0 * VacationPay AS Amount
FROM AccrualHourly1
WHERE VacationPay <> 0  and employeenumber <> '131500'
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTOPay AS Amount
FROM AccrualHourly1
WHERE PTOPay <> 0
UNION ALL 
SELECT 'HOL', employeenumber, HolidayAccount AS Account,
  GrossDistributionPercentage/100.0 * HolidayPay AS Amount
FROM AccrualHourly1
WHERE HolidayPay <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  round(((GrossDistributionPercentage/100.0 * (TotalNonOTPay + OvertimePay)) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2) AS Amount
FROM AccrualHourly1
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  round(((GrossDistributionPercentage/100.0 * (TotalNonOTPay + OvertimePay)) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2) AS Amount
FROM AccrualHourly1
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  CASE 
    WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
    
    CASE 
      WHEN (TotalNonOTPay + OvertimePay) * FixedDeductionAmount/200.0  < .02 * (TotalNonOTPay + OvertimePay) 
        THEN round(GrossDistributionPercentage/100.0 * (TotalNonOTPay + OvertimePay) * FixedDeductionAmount/200.0, 2)
      ELSE round(GrossDistributionPercentage/100.0 * .02 * (TotalNonOTPay + OvertimePay), 2) 
    END 
  END AS Amount     
FROM AccrualHourly1;
-- 9/28 -- got this far, ok'ish, need commissions
/* commissions
this gets dicy to test,
hmm, was there NOT an accrual that was done for an entire pay period?
yep, may want to test against this one later
SELECT DISTINCT a.fromdate, a.thrudate
FROM accrualcommissions a
INNER JOIN day b on a.fromdate = b.biweeklypayperiodstartdate
  AND a.thrudate = b.biweeklypayperiodenddate

initially, just use the most recent accrual
SELECT top 1 fromdate, thrudate FROM accrualcommissions ORDER BY fromdate desc 
8/25 - 8/31 
*/
/* this IS the equivalent of the Accrualxxxx1 tables
SELECT a.storecode, a.amount AS TotalPay, c.*
FROM AccrualCommissions a
INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
LEFT JOIN (
  SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
    RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
    MedicareExempt, FixedDeductionAmount 
  FROM accrualhourly1
  UNION
  SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
    RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
    MedicareExempt, FixedDeductionAmount 
  FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
WHERE a.amount <> 0  
*/  
DELETE FROM AccrualCommissions2;
INSERT INTO AccrualCommissions2
SELECT 'Gross', EmployeeNumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * TotalPay AS Amount
FROM (
  SELECT a.storecode, a.amount AS TotalPay, c.*
  FROM AccrualCommissions a
  INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
  LEFT JOIN (
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualhourly1
    UNION
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
  WHERE a.amount <> 0) a
UNION ALL 
SELECT 'FICA', EmployeeNumber, FicaAccount AS Account,
  round((GrossDistributionPercentage/100.0) * (TotalPay - (coalesce(FicaExempt, 0)) * (select NumDays/14 FROM AccrualDates)) * FicaPercentage/100.0, 2) AS Amount
FROM (
  SELECT a.storecode, a.amount AS TotalPay, c.*
  FROM AccrualCommissions a
  INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
  LEFT JOIN (
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualhourly1
    UNION
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
  WHERE a.amount <> 0) b
UNION ALL 
SELECT 'MEDIC', EmployeeNumber, MedicareAccount AS Account,
--  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * (NumDays/14)) * MedicarePercentage/100.0, 2) AS Amount
  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * (select NumDays/14 FROM AccrualDates)) * MedicarePercentage/100.0, 2) AS Amount
FROM (
  SELECT a.storecode, a.amount AS TotalPay, c.*
  FROM AccrualCommissions a
  INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
  LEFT JOIN (
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualhourly1
    UNION
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
  WHERE a.amount <> 0) c
UNION ALL 
SELECT 'RETIRE', EmployeeNumber, RetirementAccount AS Account,
CASE 
  WHEN FixedDeductionAmount IS NULL THEN 0
  ELSE 
  CASE 
    WHEN (TotalPay * FixedDeductionAmount/200.0) < .02 * TotalPay 
      THEN round((GrossDistributionPercentage/100.0 * TotalPay) * (FixedDeductionAmount/200.0), 2)
    ELSE round((GrossDistributionPercentage/100.0) * TotalPay * .02, 2) 
  END               
END AS Amount  
FROM (
SELECT a.storecode, a.amount AS TotalPay, c.*
  FROM AccrualCommissions a
  INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
  LEFT JOIN (
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualhourly1
    UNION
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
  WHERE a.amount <> 0) d; 

--ok,   
--SELECT * FROM #entries
DELETE FROM AccrualFileForJeri;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, account, employeenumber AS control, document, reference, 
  round(cast(a.amount AS sql_double), 2) AS amount, description 
FROM ( 
  SELECT employeenumber, account, SUM(amount) AS amount
  FROM ( 
    SELECT * FROM accrualsalaried2 WHERE amount <> 0
    UNION all
    SELECT * FROM accrualhourly2 WHERE amount <> 0  
    UNION all
    SELECT * FROM accrualcommissions2 WHERE amount <> 0) a
-- year END only WHEN sales IS included    
--    UNION ALL -- year END only WHEN sales IS included
--    SELECT * FROM accrualsales2 WHERE amount <> 0) a
  GROUP BY employeenumber, account) a, AccrualDates b;
  
-- Greg 9/4/12 changed accounts per Jeri
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12001', '1130426', document, reference, 
  round(1415.40 * NumDays14, 2), description 
FROM AccrualDates;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12002', '1130426', document, reference, 
  round(1569.23 * NumDays14, 2), description 
FROM AccrualDates;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12004', '1130426', document, reference, 
  round(1569.23 * NumDays14, 2), description 
FROM AccrualDates;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12005', '1130426', document, reference, 
  round(1569.23 * NumDays14, 2), description 
FROM AccrualDates;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12006', '1130426', document, reference, 
  round(1569.23 * NumDays14, 2), description 
FROM AccrualDates;
  
-- reversing entries
INSERT INTO AccrualFileForJeri
SELECT e.journal, e.thedate, d.Account, e.document, e.document, e.reference, 
  round(d.Amount, 2), e.description
FROM (  
  SELECT 
    CASE LEFT(Account, 1)
      WHEN '1' THEN '132101'
      WHEN '2' THEN '232103'
    END AS Account, 
  SUM(amount) * -1 AS Amount
  FROM (
    SELECT employeenumber, account, SUM(amount) AS amount
    FROM ( 
      SELECT * FROM accrualsalaried2 WHERE amount <> 0
      UNION all
      SELECT * FROM accrualhourly2 WHERE amount <> 0  
      UNION all
      SELECT * FROM accrualcommissions2 WHERE amount <> 0) a
-- year END only WHEN sales IS included      
--      UNION ALL -- year END only WHEN sales IS included
--      SELECT * FROM accrualsales2 WHERE amount <> 0) a      
    GROUP BY employeenumber, account) c
  GROUP BY LEFT(Account, 1)) d, AccrualDates e;
  
-- AND greg reversing entries  
UPDATE AccrualFileForJeri
SET amount = amount - (1415.40 + 4*1569.23)
WHERE account = '132101';
/* OR
SELECT SUM(amount)
FROM AccrualFileForJeri
WHERE account IN ('12001','12002','12004','12005','12006')
  AND control = '1130426'
*/  
SELECT * FROM AccrualFileForJeri;  

/*
--< shit FROM sept 2013 --------------------------------------------------------

-- compare the new accrual to the old 
SELECT account, SUM(amount) into #old FROM #entries GROUP BY account;
SELECT account, SUM(amount) into #new FROM AccrualFileForJeri GROUP BY account;

SELECT * 
FROM #old a
full OUTER JOIN #new b on a.account = b.account
WHERE abs(round(a.expr, 2) - round(b.expr, 2)) > 10
-------------------------------------------------

WHERE i am trying to go with the snapshot IS grouping the accrual 
amounts BY depts based on the account based on account categorizing FROM zcb
either cb2 OR gmfs expenses
the problem IS that these accounts are NOT categorized IN zcb
SELECT *
FROM (
  SELECT DISTINCT account 
  FROM AccrualFileForJeri) a
LEFT JOIN (
  SELECT a.gmacct, a.gmtype, a.gmdesc, a.gmdept, b.fxgact, b.fxfact
  FROM stgArkonaGLPMAST a
  LEFT JOIN stgArkonaffpxrefdta b ON a.gmacct = b.fxgact
    AND b.fxconsol = ''
    AND b.fxcyy = 2013
  WHERE a.gmyear = 2013) b on a.account = b.gmacct  
WHERE NOT EXISTS (
  SELECT 1
  FROM zcb
  WHERE glaccount = a.account)  

-- this includes ALL accounts, hacked up  
-- BY account
SELECT dl2, dl5, al3, account, amount, round(amount/aa.NumDays, 0) AS PerDay
FROM (
  SELECT account, round(SUM(amount), 0) AS amount
  FROM AccrualFileForJeri
  WHERE account NOT IN ('232103','132101')
  GROUP BY account) a
INNER JOIN AccrualDates aa on 1 = 1  
LEFT JOIN ( 
  SELECT glaccount, dl2, dl5, al3, gmcoa
  FROM zcb 
  WHERE name = 'gmfs expenses'
  UNION 
  SELECT a.account, 
    CASE gmstyp
      WHEN 'A' THEN 'RY1'
      WHEN 'B' THEN 'RY2'
    END AS dl2,
    CASE 
      WHEN a.account = '124703' THEN 'Body Shop'
      WHEN a.account IN ('124704','224704', '130200','232101') THEN 'Main Shop'
      WHEN a.account IN ('124723','130203','224723', '130203') THEN 'PDQ'
      WHEN a.account IN ('124724','130224','224724', '130224') THEN 'Detail'
      WHEN a.account = '12914' THEN 'New'
     END AS dl5,
     gmdesc, fxfact  
  FROM (
    SELECT account, round(SUM(amount), 0) AS amount
    FROM AccrualFileForJeri
    WHERE account NOT IN ('232103','132101')
    GROUP BY account) a
  LEFT JOIN (
    SELECT a.gmstyp, a.gmacct, a.gmtype, a.gmdesc, a.gmdept, b.fxgact, b.fxfact
    FROM stgArkonaGLPMAST a
    LEFT JOIN stgArkonaffpxrefdta b ON a.gmacct = b.fxgact
      AND b.fxconsol = ''
      AND b.fxcyy = 2013
    WHERE a.gmyear = 2013) b on a.account = b.gmacct  
  WHERE NOT EXISTS (
    SELECT 1
    FROM zcb
    WHERE glaccount = a.account)) c on a.account = c.glaccount   
ORDER BY dl2, dl5, aa.NumDays  

-- BY dept  
SELECT dl2, dl5, sum(amount) AS amount, round(SUM(amount)/aa.numdays, 0) AS PerDay
FROM (
  SELECT account, round(SUM(amount), 0) AS amount
  FROM AccrualFileForJeri
  WHERE account NOT IN ('232103','132101')
  GROUP BY account) a
INNER JOIN AccrualDates aa on 1 = 1  
LEFT JOIN ( 
  SELECT glaccount, dl2, dl5, al3, gmcoa
  FROM zcb 
  WHERE name = 'gmfs expenses'
  UNION 
  SELECT a.account, 
    CASE gmstyp
      WHEN 'A' THEN 'RY1'
      WHEN 'B' THEN 'RY2'
    END AS dl2,
    CASE 
      WHEN a.account = '124703' THEN 'Body Shop'
      WHEN a.account IN ('124704','224704', '130200','232101') THEN 'Main Shop'
      WHEN a.account IN ('124723','130203','224723', '130203') THEN 'PDQ'
      WHEN a.account IN ('124724','130224','224724', '130224') THEN 'Detail'
      WHEN a.account = '12914' THEN 'New'
     END AS dl5,
     gmdesc, fxfact  
  FROM (
    SELECT account, round(SUM(amount), 0) AS amount
    FROM AccrualFileForJeri
    WHERE account NOT IN ('232103','132101')
    GROUP BY account) a
  LEFT JOIN (
    SELECT a.gmstyp, a.gmacct, a.gmtype, a.gmdesc, a.gmdept, b.fxgact, b.fxfact
    FROM stgArkonaGLPMAST a
    LEFT JOIN stgArkonaffpxrefdta b ON a.gmacct = b.fxgact
      AND b.fxconsol = ''
      AND b.fxcyy = 2013
    WHERE a.gmyear = 2013) b on a.account = b.gmacct  
  WHERE NOT EXISTS (
    SELECT 1
    FROM zcb
    WHERE glaccount = a.account)) c on a.account = c.glaccount   
GROUP BY dl2, dl5, aa.numdays    
ORDER BY dl2, dl5  
  

SELECT a.account, a.control, round(SUM(a.amount), 0) AS amount, 
  sum(b.regularhours + b.overtimehours) AS clockhours, 
  c.name, c.payrollclass, c.salary, c.hourlyrate
-- SELECT *
FROM AccrualFileForJeri a
INNER JOIN AccrualDates aa on 1 = 1
LEFT JOIN accrualclockhours b on a.control = b.employeenumber
LEFT JOIN edwEmployeeDim c on a.control = c.employeenumber
  AND aa.thrudate BETWEEN c.employeekeyfromdate AND c.employeekeythrudate
WHERE account NOT IN ('232103','132101')
GROUP BY account, control, c.name, c.payrollclass, c.salary, c.hourlyrate


SELECT * 
FROM AccrualFileForJeri a
full OUTER JOIN #entries b on a.account = b.account AND a.control = b.control
WHERE a.journal IS NULL OR b.journal IS NULL 

 
9/29
DROP TABLE #wtf;
SELECT employeenumber, account, SUM(amount) AS amount
INTO #wtf
FROM ( 
  SELECT * FROM accrualsalaried2 WHERE amount <> 0
  UNION all
  SELECT * FROM accrualhourly2 WHERE amount <> 0  
  UNION all
  SELECT * FROM accrualcommissions2 WHERE amount <> 0) a
GROUP BY employeenumber, account  

-- these are the big diffs BETWEEN the new accrual vs the old script
-- they are ALL hourly folks that received a RAISE IN the accrual interval!
-- the big hit IS on the clockhours, prev version did NOT compute them corretctly
-- basing clockhours on a JOIN to edwEmployeeDim
SELECT name, employeenumber, employeekeyfromdate, employeekeythrudate, managername, left(rowchangereason, 20), payrollclass
FROM edwEmployeeDim
WHERE employeenumber IN (
  SELECT a.control 
  FROM #entries a
  full OUTER JOIN #wtf b on a.control = b.employeenumber AND a.account = b.account
  WHERE abs(round(a.amount, 2) - round(b.amount,2)) > 20)
ORDER BY name, employeekeyfromdate




-- 9/29
-- !!!!!!!! DO some fucking tests on salried fica, etc full amount OR percentage (ratio)
-- still haven't figured out what to DO about that shit IN hourly

-- 9/28
-- ok, this IS a reasonable comparison of accrual vs a paycheck
-- need to include commissions AND categorize discrepancies, of which there are many
SELECT c.name, a.*, b.gross, b.fica, b.medic, d.regularhours, d.overtimehours
FROM (  
  SELECT employeenumber, 
    SUM(CASE Category WHEN 'Gross' THEN Amount END) AS Gross,
    SUM(CASE Category WHEN 'FICA' THEN Amount END) AS FICA,
    SUM(CASE Category WHEN 'MEDIC' THEN Amount END) AS MEDIC
  FROM accrualhourly2  
  GROUP BY employeenumber) a
LEFT JOIN (
  SELECT yhdemp AS EmployeeNumber, yhdbsp AS Gross, yhcsse AS FICA, yhcmde AS MEDIC
  FROM stgArkonaPYHSHDTA
  WHERE ypbnum = 913130) b on a.employeenumber = b.employeenumber
LEFT JOIN edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
LEFT JOIN AccrualClockHours d on a.employeenumber = d.employeenumber  
WHERE (abs(a.gross - b.gross) > .1 OR abs(a.fica - b.fica) > .1 OR abs(a.medic - b.medic) > .1)     
ORDER BY name  

SELECT c.name, a.*, b.gross, b.fica, b.medic, d.regularhours, d.overtimehours
FROM (  
  SELECT employeenumber, 
    SUM(CASE Category WHEN 'Gross' THEN Amount END) AS Gross,
    SUM(CASE Category WHEN 'FICA' THEN Amount END) AS FICA,
    SUM(CASE Category WHEN 'MEDIC' THEN Amount END) AS MEDIC
  FROM accrualhourly2  
  GROUP BY employeenumber) a
LEFT JOIN (
  SELECT yhdemp AS EmployeeNumber, yhdbsp AS Gross, yhcsse AS FICA, yhcmde AS MEDIC
  FROM stgArkonaPYHSHDTA
  WHERE ypbnum = 913130) b on a.employeenumber = b.employeenumber
LEFT JOIN edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
LEFT JOIN AccrualClockHours d on a.employeenumber = d.employeenumber  
WHERE (abs(a.gross - b.gross) > .1 OR abs(a.fica - b.fica) > .1 OR abs(a.medic - b.medic) > .1)     
ORDER BY name 

--/> shit FROM sept 2013 --------------------------------------------------------

*/

