/*
4/2 here we go again
jeri: I changed Derrick to salary.  Will you rerun the accrual?  Main shop and detail seem off.
need to rerun accrual first thing IN the morning
1. darrick richardson NOT IN accrual, jeri changed him FROM commission to salary @ 2000
2. no one was working IN office on saturday, many ros closed today may account for shop & detail

*/

SELECT *
FROM edwEmployeeDim 
WHERE payrollclasscode = 'C'
  AND active = 'active'
  
SELECT name, employeenumber, payrollclass, payperiod
FROM edwEmployeeDim 
WHERE distcode = 'wtec'
  AND active = 'active'  
  AND currentrow = true
  
 SELECT * FROM accrualfileforjeri WHERE control = '1112411'
 
 
 SELECT distinct a.name, a.employeenumber, a.pydept, a.distcode, a.payrollclass, a.payperiod
 FROM edwEmployeeDim a
 INNER JOIN accrualfileforjeri b on a.employeenumber = b.control
 WHERE a.currentrow = true

 
 select a.name, a.employeenumber, a.pydept, a.distcode, a.payrollclass, a.payperiod
 FROM edwEmployeeDim a
 WHERE currentrow = true
   AND active = 'active'
   AND NOT EXISTS (
     SELECT 1
	 FROM accrualfileforjeri
	 WHERE control = a.employeenumber)
ORDER BY a.name	 
	 
SELECT * FROM edwEmployeeDim WHERE lastname = 'richardson' AND employeekey IN (3389,4291)

UPDATE edwEmployeeDim
SET employeekeythrudate = '03/15/2018',
    employeekeythrudatekey = (SELECT datekey FROM day WHERE thedate = '03/15/2018')
WHERE employeekey = 3389;
UPDATE edwEmployeeDim
SET employeekeyfromdate = '03/16/2018',
    employeekeyfromdatekey = (SELECT datekey FROM day WHERE thedate = '03/16/2018')
WHERE employeekey = 4291;
  
 SELECT a.name, a.employeenumber, a.pydept, a.distcode, a.payrollclass, a.payperiod, sum(b.amount) AS amount
 FROM edwEmployeeDim a
 INNER JOIN accrualfileforjeri b on a.employeenumber = b.control
 WHERE a.currentrow = true
   AND distcode IN ('STEC','WTEC')
GROUP BY a.name, a.employeenumber, a.pydept, a.distcode, a.payrollclass, a.payperiod   
ORDER BY distcode, name


these 2 queries are 
IN E:\DDS2\Scripts\Accrual\201803\main_shop_detail.xlxs

SELECT distcode, SUM(amount) -- s: 78218.79, d: 13991.76
FROM edwEmployeeDim a
INNER JOIN accrualfileforjeri b on a.employeenumber = b.control
  WHERE a.currentrow = true
    AND pydept LIKE 'service%'
GROUP BY distcode


SELECT a.name, a.employeenumber, a.pydept, a.distcode, a.payrollclass, a.payperiod, sum(b.amount) AS amount
FROM edwEmployeeDim a
INNER JOIN accrualfileforjeri b on a.employeenumber = b.control
  WHERE a.currentrow = true
    AND pydept LIKE 'service%'
GROUP BY a.name, a.employeenumber, a.pydept, a.distcode, a.payrollclass, a.payperiod   
ORDER BY distcode, name