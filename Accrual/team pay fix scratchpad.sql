DROP TABLE tmpAccrualHourly1;
CREATE TABLE tmpAccrualHourly1 ( 
      StoreCode CIChar( 3 ),
      EmployeeNumber CIChar( 9 ),
      RegularPay Money,
      OvertimePay Money,
      VacationPay Money,
      PTOPay Money,
      HolidayPay Money,
      TotalNonOTPay Money,
      GrossDistributionPercentage Double( 2 ),
      GrossAccount CIChar( 10 ),
      OvertimeAccount CIChar( 10 ),
      VacationAccount CIChar( 10 ),
      HolidayAccount CIChar( 10 ),
      PTOAccount CIChar( 10 ),
      FicaAccount CIChar( 10 ),
      MedicareAccount CIChar( 10 ),
      RetirementAccount CIChar( 10 ),
      FicaPercentage Double( 2 ),
      MedicarePercentage Double( 2 ),
      FicaExempt Money,
      MedicareExempt Money,
      FixedDeductionAmount Double( 2 )) IN DATABASE;
      
INSERT INTO tmpAccrualHourly1
SELECT *
FROM accrualHourly1;     

-- UPDATE tmpAccrualHourly1 with actual last payperiod + 1 day
-- select * FROM #actual
UPDATE tmpAccrualHourly1
SET regularPay = x.commissionPayPerDay,
    overtimePay = 0
FROM #actual x   
WHERE tmpAccrualHourly1.employeenumber = x.employeenumber;
UPDATE tmpAccrualHourly1
SET totalNonOtPay =  regularPay + vacationpay + ptopay + holidaypay
WHERE employeenumber IN (SELECT employeenumber FROM #actual)

select * 
FROM accrualhourly1 a
LEFT JOIN tmpaccrualhourly1 b on a.employeenumber = b.employeenumber
WHERE a.employeenumber IN (SELECT employeenumber FROM #actual)

UPDATE tmpAccrualHourly1
SET regularPay = x.commissionPay,
    overtimePay = 0
FROM (    
  select lastname, firstname, employeenumber,  
    round(11*max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPay
  FROM scotest.tpdata a
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 31 AND curdate()
  GROUP BY lastname, firstname, employeenumber  
  UNION -- brian peterson
  select c.lastname, c.firstname, a.employeenumber, 
    round(11*max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPay
  FROM scotest.bsFlatRateData a
  LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.fromdate AND b.thrudate
  LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 31 AND curdate()
  GROUP BY c.lastname, c.firstname, a.employeenumber) x   
WHERE tmpAccrualHourly1.employeenumber = x.employeenumber;

UPDATE tmpAccrualHourly1
SET totalNonOtPay =  regularPay + vacationpay + ptopay + holidaypay
WHERE employeenumber IN ( 
SELECT employeenumber FROM (
  select lastname, firstname, employeenumber,  
    round(11*max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPay
  FROM scotest.tpdata a
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 31 AND curdate()
  GROUP BY lastname, firstname, employeenumber  
  UNION -- brian peterson
  select c.lastname, c.firstname, a.employeenumber, 
    round(11*max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPay
  FROM scotest.bsFlatRateData a
  LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.fromdate AND b.thrudate
  LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 31 AND curdate()
  GROUP BY c.lastname, c.firstname, a.employeenumber) x)
    
    
SELECT * INTO #tp FROM (
  select lastname, firstname, employeenumber,  
    round(11*max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPay
  FROM scotest.tpdata a
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 31 AND curdate()
  GROUP BY lastname, firstname, employeenumber  
  UNION -- brian peterson
  select c.lastname, c.firstname, a.employeenumber, 
    round(11*max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPay
  FROM scotest.bsFlatRateData a
  LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.fromdate AND b.thrudate
  LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 31 AND curdate()
  GROUP BY c.lastname, c.firstname, a.employeenumber) x    
  
SELECT c.lastname, c.firstname, a.employeenumber, a.regularpay, a.overtimepay, a.totalnonotpay, 
  b.regularpay, b.totalnonotpay
FROM (  
SELECT employeenumber, regularpay, overtimepay, vacationpay, ptopay, holidaypay,
  totalnonotpay
FROM accrualhourly1
WHERE employeenumber IN (SELECT employeenumber FROM #tp)) a
LEFT JOIN (  
SELECT employeenumber, regularpay, overtimepay, vacationpay, ptopay, holidaypay,
  totalnonotpay
FROM tmpaccrualhourly1
WHERE employeenumber IN (SELECT employeenumber FROM #tp)) b on a.employeenumber = b.employeenumber
LEFT JOIN scotest.tpemployees c on a.employeenumber = c.employeenumber
ORDER BY lastname, firstname
  
select * FROM #tp

-- 7/2 comparing MAX of last 3 vs last actual,
-- MAX of last 3
SELECT * INTO #maxi FROM (
select lastname, firstname, employeenumber,  
  round(11*max(techtfrrate*teamprofpptd*techclockhourspptd/100)/10, 2) AS commissionPayPerDay
FROM scotest.tpdata a
INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
  AND a.teamKey = b.teamkey
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 31 AND curdate()
GROUP BY lastname, firstname, employeenumber  
UNION -- brian peterson
select c.lastname, c.firstname, a.employeenumber, 
  round(11*max(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPayPerDay
FROM scotest.bsFlatRateData a
LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
  AND a.thedate BETWEEN b.fromdate AND b.thrudate
LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 31 AND curdate()
GROUP BY c.lastname, c.firstname, a.employeenumber) x 

-- USING actual pay period + 1 day
SELECT * INTO #actual FROM (
select lastname, firstname, employeenumber,  
  round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2) +
  round(techtfrrate*teamprofpptd*techclockhourspptd/100/10, 2) AS commissionPayPerDay
FROM scotest.tpdata a
INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
  AND a.teamKey = b.teamkey
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 7 AND curdate()
UNION -- brian peterson
select c.lastname, c.firstname, a.employeenumber, 
  round(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD, 2) +
  round((b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPayPerDay
FROM scotest.bsFlatRateData a
LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
  AND a.thedate BETWEEN b.fromdate AND b.thrudate
LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
WHERE thedate = payperiodend
  AND thedate BETWEEN curdate() - 7 AND curdate())x  
  
  
SELECT *
FROM #maxi a
LEFT JOIN #actual b on a.employeenumber = b.employeenumber  

SELECT SUM(commissionPayPerDay) FROM #maxi -- 79520


SELECT SUM(commissionPayPerDay) FROM #actual -- 66328

select * FROM #actual