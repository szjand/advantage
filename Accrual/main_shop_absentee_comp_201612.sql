/*
Andrew IS upset, his absentee comp IS way higher than he anticipated

the problem IS NOT, AS i initially thought, that i have NOT been accruing vacation,
it IS that, for flat rate folks, vac/pto/hol has been lumped INTO gross, 124704,
instead of being broken out to 12404
*/


DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromdate = '11/13/2016';
@thrudate = '11/30/2016';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);
--DELETE FROM AccrualFlatRateGross;    
--INSERT INTO AccrualFlatRateGross   
SELECT storecode, employeenumber, lastname, firstname, @fromDate, @thruDate, 
  @days * max(round((commissionpay + ptopay)/10, 2)) AS gross, distcode 
INTO #wtf1  
FROM (    
  select aa.storecode, a.employeenumber, a.lastname, a.firstname, 
    round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2) AS commissionPay,
    round(techHourlyRate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd), 2) AS ptoPay,
    aa.distcode
  FROM scotest.tpdata a
  LEFT JOIN edwEmployeeDim aa on a.employeenumber = aa.employeenumber
    AND aa.currentrow = true
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey  
  WHERE thedate = payperiodend--) x
-- *b*  
/**/  
    AND payperiodseq IN ( -- last 3 full pay periods
      SELECT top 3 DISTINCT payperiodseq
      FROM scotest.tpdata
      WHERE payperiodend <= @thrudate
      ORDER BY payperiodseq DESC)) x  
/**/      
WHERE EXISTS ( -- currently a team pay tech
  SELECT 1
  FROM scotest.tpdata
  WHERE employeenumber = x.employeenumber
    AND thedate = @thrudate)    
OR (x.lastname = 'peterson' AND x.firstname = 'brian')      
GROUP BY storecode, employeenumber, lastname, firstname, distcode;

select * FROM #wtf1


DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromdate = '11/13/2016';
@thrudate = '11/30/2016';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);


-- DELETE FROM AccrualFlatRate1;
-- INSERT INTO AccrualFlatRate1
SELECT d.storecode, d.employeenumber, d.gross, 0 AS pto,
  d.gross AS TotalNonOTPay,
  f.GROSS_DIST AS grossdistributionpercentage, f.GROSS_EXPENSE_ACT_ AS grossaccount, f.SICK_LEAVE_EXPENSE_ACT_ AS ptoaccount,  
  f.EMPLR_FICA_EXPENSE AS ficaaccount, f.EMPLR_MED_EXPENSE AS medicareaccount, f.EMPLR_CONTRIBUTIONS AS retirementaccount, 
  g.yficmp AS ficapercentage, g.ymedmp as medicarepercentage,
  coalesce(h.FicaEx, 0) AS FicaExempt,
  coalesce(i.MedEx, 0) AS MedicareExempt,
  coalesce(j.FIXED_DED_AMT, 0) AS FixedDeductionAmount
INTO #wtf2  
FROM #wtf1 d-- AccrualFlatRateGross d      
LEFT JOIN zFixPyactgr f on d.storecode = f.company_number
  AND d.distcode = f.dist_code    
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL g ON d.storecode = g.yco#
  AND g.ycyy = 100 + (year(@thruDate) - 2000) -- 112 -    
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) h ON d.employeenumber = h.EMPLOYEE_NUMBER 
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) i ON d.employeenumber = i.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT j ON d.storecode = j.COMPANY_NUMBER
  AND d.employeenumber = j.EMPLOYEE_NUMBER 
--  AND h.DED_PAY_CODE IN ('91', '99');
-- *666
  AND 
    case 
      when j.employee_number = '11650' then j.ded_pay_code = '99'
      else j.DED_PAY_CODE = '91'
    END;

select * FROM #wtf2	WHERE employeenumber = '152235'



	
DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromdate = '11/13/2016';
@thrudate = '11/30/2016';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);	
	

--DELETE FROM AccrualFlatRate2;   
--INSERT INTO AccrualFlatRate2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * Gross AS Amount
INTO #wtf3  
FROM #wtf2 --AccrualFlatRate1
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTO AS Amount
FROM AccrualFlatRate1
WHERE PTO <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2)) AS Amount
FROM #wtf2 --AccrualFlatRate1
GROUP BY employeenumber, FicaAccount
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2)) AS Amount
FROM #wtf2 --AccrualFlatRate1
GROUP BY employeenumber, MedicareAccount
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  sum(
    CASE 
      WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
      CASE 
        WHEN TotalNonOTPay * FixedDeductionAmount/200.0  < .02 * TotalNonOTPay
          THEN round(GrossDistributionPercentage/100.0 * TotalNonOTPay * FixedDeductionAmount/200.0, 2)
        ELSE round(GrossDistributionPercentage/100.0 * .02 * TotalNonOTPay, 2) 
      END 
    END) AS Amount     
FROM AccrualFlatRate1
GROUP BY employeenumber, RetirementAccount;
    	
SELECT * FROM #wtf1		

SELECT expr AS gross FROM #wtf1 WHERE employeenumber = '152235'

select * FROM #wtf2 WHERE employeenumber = '152235'

select * FROM #wtf3 WHERE employeenumber = '152235'

DROP TABLE #wtf1;
DROP TABLE #wtf2;
DROP TABLE #wtf3;