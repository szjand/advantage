/*
3/30/12
       added variables
       added DistCode STEV to exclusion
       
DROP TABLE #wtf1;
DROP TABLE #jon1;       
*/
DECLARE @Date string; // last day of the month for which payroll IS being accrued;
DECLARE @FromDate date; // beginning date for the interval to be accrued
DECLARE @ThruDate date; // ending date for the interval to be accrued
DECLARE @NumDays integer;

DECLARE @Journal string;
DECLARE @Doc string;
DECLARE @Ref string;
@Journal = 'GLI';
@Doc = 'GLI033112';
@Ref = 'GLI033112';
@Date = '03/31/2012';

-- Feb 2012
--@FromDate = '02/12/2012';
--@ThruDate = '02/29/2012';
--Mar 2012
@FromDate = '03/25/2012';
@ThruDate = '03/31/2012';

--@FromDate = '01/29/2012';
--@ThruDate = '01/31/2012';
--@FromDate = '02/01/2012';
--@ThruDate = '02/29/2012';
--@FromDate = '01/29/2012';
--@ThruDate = '02/12/2012';
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1;
--SELECT @NumDays FROM system.iota;
--SELECT * FROM #wtf1
SELECT a.*, b.regularhours, b.overtimehours, b.vacationhours, b.ptohours, b.holidayhours,
  p.ytadic, p.ytaseq, p.ytagpp, p.ytagpa, p.ytaopa, p.ytavac, p.ytahol, p.ytasck,
  p.ytaeta, p.ytamed, p.ytacon,
  pc.yficmp, pc.ymedmp, fe.FicaEx, me.MedEx, d.yddamt 
-- DROP TABLE #wtf1
INTO #wtf1
FROM ( -- a: 1 row per store/emp#  emp IS payed bi-weekly, dist code NOT IN list, 
       -- the empkey that was valid ON last payroll ending date
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
    PayPeriodCode, DistCode, salary, hourlyrate
  FROM edwEmployeeDim ex
  WHERE EmployeeKeyFromDate = (
      SELECT MAX(EmployeekeyFromDate)
      FROM edwEmployeeDim
      WHERE EmployeeNumber = ex.Employeenumber
        AND EmployeeKeyFromDate <= @ThruDate
      GROUP BY employeenumber)
    AND PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')) a  
LEFT JOIN ( -- clock hours for the interval
    SELECT storecode, employeenumber, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
      sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
      sum(holidayhours) AS holidayhours
    FROM day d
    INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
    LEFT JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
    WHERE thedate BETWEEN @FromDate AND @ThruDate
    GROUP BY storecode, employeenumber) b ON a.storecode = b.storecode 
  AND a.employeenumber = b.employeenumber 
LEFT JOIN stgArkonaPYACTGR p ON a.StoreCode = p.ytaco# 
  AND a.DistCode = p.ytadic
  AND CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL pc ON a.storecode = pc.yco#
  AND pc.ycyy = 112 -- 100 + (year(@ThruDate) - 2000) 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS FicaEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex2 = 'Y')
  GROUP BY ydempn) fe ON a.employeenumber = fe.ydempn  
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS MedEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex6 = 'Y')
  GROUP BY ydempn) me ON a.employeenumber = me.ydempn    
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT d ON a.storecode = d.ydco#
  AND a.employeenumber = d.ydempn 
  AND d.yddcde IN ('91', '99') 
WHERE b.storecode IS NOT NULL; 
--  AND regularhours + overtimehours <> 0;
  
select @Journal as Journal, @Date as [Date], Account, EmployeeNumber as Control, 
  @Doc as Document, @Ref as Reference, round(sum(Amount),2) as Amount, 
  'Payroll Accrual' as Description
-- DROP TABLE #jon1
INTO #jon1
FROM (
  SELECT 'Gross', employeenumber, ytagpa AS Account,
    ytagpp/100.0 *
      CASE PayrollClassCode
        WHEN 'H' THEN (regularhours * HourlyRate)
--        WHEN 'S' THEN salary * (@NumDays*1.0/DayOfMonth(@thrudate))
        WHEN 'S' THEN salary/14 * @NumDays
      END AS Amount
  FROM #wtf1
  UNION ALL
  SELECT 'OT', employeenumber, ytaopa AS Account,
    ytagpp/100.0 * overtimehours * HourlyRate * 1.5 AS Amount
  FROM #wtf1
  WHERE PayrollClassCode = 'H'
  UNION ALL 
  SELECT 'VAC', employeenumber, ytaopa AS Account,
    ytagpp/100.0 * vacationhours * HourlyRate AS Amount
  FROM #wtf1
  WHERE PayrollClassCode = 'H'
  UNION ALL 
  SELECT 'PTO', employeenumber, ytasck AS Account,
    ytagpp/100.0 * ptohours * HourlyRate AS Amount
  FROM #wtf1
  WHERE PayrollClassCode = 'H'
  UNION ALL 
  SELECT 'HOL', employeenumber, ytahol AS Account,
    ytagpp/100.0 * holidayhours * HourlyRate AS Amount
  FROM #wtf1
  WHERE PayrollClassCode = 'H'
  UNION ALL
  SELECT 'FICA', employeenumber, ytaeta AS Account,
    ytagpp/100.0 *
      CASE PayrollClassCode
        WHEN 'H' THEN round(((((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) 
            + (overtimehours * HourlyRate * 1.5)) - coalesce(FicaEx, 0)) * yficmp/100.0, 2)
    --    WHEN 'S' THEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate))) * p.yficmp/100.0
--        WHEN 'S' THEN round((salary - coalesce(FicaEx, 0)) * (@NumDays*1.0/DayOfMonth(@thrudate)) * yficmp/100.0, 2)
        WHEN 'S' THEN round(((salary/14 * @NumDays) - (coalesce(FicaEx, 0)) * @NumDays/14) * yficmp/100.0, 2)
      END AS Amount
  FROM #wtf1
  UNION ALL 
  SELECT 'MEDIC', employeenumber, ytamed AS Account,
    ytagpp/100.0 *
      CASE PayrollClassCode
        WHEN 'H' THEN round(((((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) - coalesce(MedEx, 0)) * ymedmp/100.0, 2)
    --    WHEN 'S' THEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate))) * p.ymedmp/100.0
--        WHEN 'S' THEN round((salary - coalesce(MedEx, 0)) * (@NumDays*1.0/DayOfMonth(@thrudate)) * ymedmp/100.0, 2)
        WHEN 'S' THEN round(((salary/14 * @NumDays) - (coalesce(MedEx, 0)) * @NumDays/14) * ymedmp/100.0, 2)
      END AS Amount
  FROM #wtf1   
  UNION ALL 
  SELECT 'RETIRE', employeenumber, ytacon AS Account,
      CASE 
        WHEN yddamt IS NULL THEN 0
        ELSE 
        ytagpp/100.0 *
          CASE PayrollClassCode
            WHEN 'H' THEN 
              CASE 
                WHEN (((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) * yddamt/200.0 -- 1/2 of employee contr
                    < .02 * (((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) 
                  THEN round((((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)) * yddamt/200.0, 2)
                ELSE round(.02 * (((regularhours+ vacationhours+ptohours+holidayhours) * HourlyRate) + (overtimehours * HourlyRate * 1.5)), 2) 
              END  
        --    WHEN 'S' THEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate))) * d.yddamt/100.0
            WHEN 'S' THEN 
              CASE  
--                WHEN (salary * (@NumDays*1.0/DayOfMonth(@thrudate)) * yddamt/200.0) < .02 * salary THEN round((salary * (@NumDays*1.0/DayOfMonth(@thrudate)) * yddamt/200.0), 2)
--                ELSE round(.02 * salary * (@NumDays*1.0/DayOfMonth(@thrudate)), 2) 
                WHEN ((salary/14) * @NumDays) * (yddamt/200.0) < .02 * ((salary/14) * @NumDays) THEN round((salary/14 * @NumDays) * (yddamt/200.0), 2)
                ELSE round((.02 * (salary/14) * @NumDays), 2)                  
              END 
          END 
      END AS Amount     
  FROM #wtf1 ) x
WHERE Amount > 0
GROUP BY employeenumber, Account   
  
  
/*
-- parts: acct 12306
SELECT SUM(amount) FROM (
SELECT journal, date, account, control, document, reference, SUM(amount) AS Amount, description
FROM (
SELECT * 
FROM #jon1
WHERE amount <> 0
AND account = '12306') x
GROUP BY journal, date, account, control, document, reference, description) xx

SELECT * FROM #jon1 WHERE control = '175360'

SELECT SUM(amount)
FROM #jon1
 
SELECT * FROM #jon2  
*/  

