-- totals
SELECT name, x.employeenumber, y.technumber,
  trim(cast(round(ClockHours, 2) as sql_char)) AS ClockHours, 
  trim(cast(coalesce(round(flaghours, 2), 0) as sql_char)) AS FlagHours,
  trim(cast(CASE 
    WHEN round(Clockhours, 0) = 0 OR coalesce(flaghours,0) = 0 THEN 0
    ELSE round((Flaghours/Clockhours)*100, 2) 
  END AS sql_char)) AS Proficiency
FROM (-- clockhours
  SELECT trim(b.firstname) + ' ' +  b.lastname AS name, b.employeenumber, sum(c.clockhours) AS clockhours
  FROM day a
  LEFT JOIN edwEmployeeDim b on b.pydeptcode = '24' 
    AND b.PayrollClassCode = 'H'
    AND b.termdate >= a.thedate
    AND a.thedate BETWEEN b.employeekeyfromdate AND b.employeekeythrudate
  LEFT JOIN edwClockHoursFact c on a.datekey = c.datekey AND b.employeekey = c.employeekey  
  WHERE a.thedate BETWEEN  '10/20/2013' AND '10/31/2013'
  GROUP BY trim(b.firstname) + ' ' +  b.lastname, b.employeenumber) x  
INNER JOIN (-- flaghours
  SELECT d.employeenumber, technumber, SUM(round(c.weightfactor*flaghours, 2)) AS flaghours
  FROM factRepairOrder a
  INNER JOIN day b on a.flagdatekey = b.datekey
  INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
  INNER JOIN dimTech d on c.TechKey = d.TechKey
    AND d.FlagDeptCode = 'RE'
    AND d.technumber IN ('d02','d07','d08','d11','d12','d18','d19','d21','d23','d25','d26','d27','d30','d32','d35')
  WHERE b.thedate BETWEEN '10/20/2013' AND '10/31/2013'
    AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
  GROUP BY d.employeenumber, technumber) y on x.employeenumber = y.employeenumber
ORDER BY name

-- flag hours BY tech BY ro
SELECT d.technumber, TRIM(e.firstname) + e.lastname AS name, f.thedate, ro, round(c.weightfactor*flaghours, 2) AS flaghours
FROM factRepairOrder a
INNER JOIN day b on a.flagdatekey = b.datekey
INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
INNER JOIN dimTech d on c.TechKey = d.TechKey
  AND d.FlagDeptCode = 'RE'
  AND d.currentrow = true
INNER JOIN edwEmployeeDim e on d.employeenumber = e.employeenumber
  AND e.active = 'active'
  AND e.currentrow = true  
LEFT JOIN day f on a.flagdatekey = f.datekey  
WHERE b.thedate BETWEEN '10/20/2013' AND '10/31/2013'
  AND a.flaghours > 0 // ?? don't know IF i want to DO this OR NOT
  AND d.technumber IN ('d02','d07','d08','d11','d12','d18','d19','d21','d23','d25','d26','d27','d30','d32','d35')
--GROUP BY d.technumber, d.employeenumber, d.name, d.technumber, TRIM(e.firstname) + e.lastname
ORDER BY name

-- ok, ben IS USING the arkona labor profit analysis report
-- WHICH INCLUDES CLOSED ROS ONLY
-- BY tech BY ro
-- brandon halver has the biggest discrepancy IN flag hours (low IN ben's)
-- lets look at ALL 3 dates, query based on flagdate
SELECT d.technumber, TRIM(e.firstname) + e.lastname AS name, b.thedate AS flagdate, 
  f.thedate AS closedate, g.thedate AS finalclosedate,
  ro, round(c.weightfactor*flaghours, 2) AS flaghours
FROM factRepairOrder a
INNER JOIN day b on a.flagdatekey = b.datekey
INNER JOIN brTechGroup c on a.TechGroupKey = c.TechGroupKey
INNER JOIN dimTech d on c.TechKey = d.TechKey
  AND d.FlagDeptCode = 'RE'
  AND d.currentrow = true
INNER JOIN edwEmployeeDim e on d.employeenumber = e.employeenumber
  AND e.active = 'active'
  AND e.currentrow = true  
LEFT JOIN day f on a.closedatekey = f.datekey  
LEFT JOIN day g on a.finalclosedatekey = g.datekey
WHERE b.thedate BETWEEN '10/20/2013' AND '10/31/2013'
  AND a.flaghours > 0 
  AND d.technumber = 'd25' -- kevin stafford
--  AND d.technumber IN ('d07','d27','d32','d19','d02','d26','d08','d23','d11','d18','d21','d35','d30','d25','d12')  
ORDER BY ro