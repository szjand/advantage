﻿-- from jeri
Sales accrual exceptions for the following items.  All other pay will follow what is set up in the 
employee master file that you are already pulling.
GM:
PTO (74):  12401 (40%), 12402 (60%)  -- same as dist SALE
F&I Comm (79A):  185501 (40%), 185600 (60%)  -- same as dist TEAM
Pulse (79B):  164502

HN:
PTO (74): 22401 (30%), 22410 (20%), 22402 (50%)
Pulse (79B): 264502


select pymast_employee_number, distrib_code from arkona.ext_pymast where employee_last_name = 'close'

select * from arkona.ext_pyhshdta where employee_ = '162983' and check_year = 21 and check_month = 7

select * from arkona.ext_pyhscdta where employee_number = '162983' and payroll_run_number = 702210 

select * from arkona.ext_pyactgr where dist_code = 'SALE' and company_number in ('RY1','RY2')

select * from sls.accrual_sales where year_month = 202106

select * from sls.accrual_sales_1

select * from sls.accrual_sales_2

select * from sls.accrual_file_for_jeri where the_date = '06/30/2021' and control = '162983'
select * from sls.accrual_file_for_jeri where the_date = '06/30/2021' and control = '17534'
do pto/pulse/fi comm get treated the same way re taxes, retirement

my guess is i need to break out gross for the gross/pto/fi/pulse accounts

but the total is used for taxes & retirement

select * from sls.accrual_sales where year_month = 202106
-- 07/16/21 starting with the query from sls.json_get_payroll_for_kim('RY1')
-- skip the json
-- hard code ry1
-- so this generates amounts for pay codes 74 (pto), 79 (adj, addtl comp, unit commission) 79A (fi comm) 79b (pulse)
-- if i understand jeris instructions correctly, only code 79 is to be handled by pyactgr
-- all the 79s can be grouped togeher




drop table if exists tmp_accrual_sales cascade;
create temp table tmp_accrual_sales as
with
	open_month as (select 202106 as year_month)
--   select json_agg(row_to_json(x) order by consultant) as sales_consultant_payroll
--   from (
    select 202106 as year_month, case left(a.employee_number, 1) when '1' then 'RY1' when'2' then 'RY2' end as store,
			a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
      a.pto_pay as pto_74,
				-- combine all the 79s
        coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0) + round (
					case -- paid spiffs deducted only if base guarantee
						when a.total_earned >= a.guarantee then
							a.unit_pay - coalesce(a.draw, 0)
						else
							(a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
					end, 2) as total_79,       
        a.fi_pay as fi_comm_79A,   
        a.pulse as pulse_79B,
        round( 
          case -- paid spiffs deducted only if base guarantee
            when a.total_earned >= a.guarantee then
              a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
            else
              (a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
                  - coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
            end, 2) as total_gross -- "Total Month End Payout"      
    from sls.consultant_payroll a
    left join sls.pto_intervals b on a.employee_number = b.employee_number
      and a.year_month = b.year_month
    left join sls.paid_by_month c on a.employee_number = c.employee_number
      and a.year_month = c.year_month
    left join (
      select employee_number, sum(amount) as adjusted_amount
      from sls.payroll_adjustments
      where year_month =  (select year_month from open_month)
      group by employee_number) d on a.employee_number = d.employee_number
    left join (
      select employee_number, sum(amount) as additional_comp
      from sls.additional_comp
      where thru_date > (
        select first_of_month
        from sls.months
        where open_closed = 'open')
      group by employee_number) e on a.employee_number = e.employee_number  
    left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
    select a.employee_number, 
      round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
    from sls.personnel a
    inner join dds.dim_date b on a.start_date = b.the_date
      and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number      
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number         
    where a.year_month = (select year_month from open_month);
--     and f.store_code = 'RY1' -- ) x) y;

select * from tmp_accrual_sales_1;

drop table if exists tmp_accrual_sales_1 cascade;
create temp table tmp_accrual_sales_1 as
	select a.store, a.employee_number, 0 as TotalPay,
		c.GROSS_DIST, c.GROSS_EXPENSE_ACT_, 
		c.EMPLR_FICA_EXPENSE, c.EMPLR_MED_EXPENSE, c.EMPLR_CONTRIBUTIONS,
		e.fica_employer_percent, e.employer_medicare_, 
		coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
		coalesce(h.fixed_ded_amt, 0) AS fixed_ded_amt 
	from tmp_accrual_sales a -- ****************************************************************************
	join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
	left join arkona.ext_pyactgr c on b.pymast_company_number = c.company_number
		and b.distrib_code = c.dist_code
	left join arkona.ext_pycntrl e on b.pymast_company_number = e.company_number  -- fica & medicare percentages for store/year 
		and e.payroll_year = 121 -- _payroll_year -- ****************************************************************************
	left join ( -- FICA Exempt deductions, may be NULL, coalesce IN select
		select employee_number, sum(fixed_ded_amt) as FicaEx
		from arkona.ext_pydeduct x
		where exists (
			select 1
			from arkona.ext_pypcodes
			where ded_pay_code = x.ded_pay_code
				and exempt_2_fica_ = 'Y')
		 group by employee_number) f on a.employee_number = f.employee_number
	left join ( -- -- Medicare Exempt deductions, may be NULL, coalesce IN select
		select employee_number, sum(fixed_ded_amt) as MedEx
		from arkona.ext_pydeduct x
		where exists (
			select 1
			from arkona.ext_pypcodes
			where ded_pay_code = x.ded_pay_code
				and exempt_6_medc_tax_ = 'Y')
		 group by employee_number) g on a.employee_number = g.employee_number	 
	left join arkona.ext_pydeduct h on  b.pymast_company_number = h.company_number -- retirement deductions
		and a.employee_number = h.employee_number
		and h.ded_pay_code in ('91','91b','91c','99','99a','99b','99c')
	where a.year_month = 202106; -- _year_month; -- ****************************************************************************

select * from tmp_accrual_sales_1  order by employee_number

/*
1135518 fica & medicare exempt real:: 205.310  this:: 365.31  
is this because of kims corrections - yes the difference is 160 which is the 18 HSA PRETAX, which is the one that got fixed
table f 
		select employee_number, sum(fixed_ded_amt) as FicaEx
		from arkona.ext_pydeduct x
		where exists (
			select 1
			from arkona.ext_pypcodes
			where ded_pay_code = x.ded_pay_code
				and exempt_2_fica_ = 'Y')
		 group by employee_number
		 order by employee_number

select a.ded_pay_code, a.fixed_Ded_amt, b.description, b.exempt_1_fed_Tax_, b.exempt_2_fica_, b.exempt_6_medc_tax_ 
from arkona.ext_pydeduct a
join arkona.ext_pypcodes b on a.ded_pay_code = b.ded_pay_code
  and a.company_number = b.company_number
where employee_number = '1135518'
*/

select * from sls.accrual_file_for_jeri  where account = '12502w'
select * from fin.dim_account where account in ('12502','12502w')

GM:
PTO (74):  12401 (40%), 12402 (60%)  -- same as dist SALE
F&I Comm (79A):  185501 (40%), 185600 (60%)  -- same as dist TEAM
Pulse (79B):  164502

HN:
PTO (74): 22401 (30%), 22410 (20%), 22402 (50%)
Pulse (79B): 264502

drop table if exists non_gross_distribution;
create temp table non_gross_distribution (
	store citext not null,
  category citext not null,
  account citext not null,
  distribution_percentage numeric(3,2) not null,
  primary key (category,account));

insert into non_gross_distribution values
('RY1','pto_74','12401',.4),
('RY1','pto_74','12402',.6),
('RY1','comm_79A','185501',.4),
('RY1','comm_79A','185600',.6),
('RY1','pulse_79B','164502',1),
('RY2','pto_74','22401',.3),
('RY2','pto_74','22410',.2),
('RY2','pto_74','22402',.5),
('RY2','pulse_79B','264502',1);

/*
	select 'gross', employee_number, gross_expense_act_ as account,
		round(gross_dist/100.0 * total_79, 2) as amount	
	from (
		select a.store, a.consultant, a.total_79, c.*
		from tmp_accrual_sales a
		left join tmp_accrual_sales_1  c on a.employee_number = c.employee_number
		where a.year_month = 202106) aa
 proof that the single level query works
select * 
from (
	select 'gross', employee_number, gross_expense_act_ as account,
		round(gross_dist/100.0 * total_79, 2) as amount	
	from (
		select a.store, a.consultant, a.total_79, c.*
		from tmp_accrual_sales a
		left join tmp_accrual_sales_1  c on a.employee_number = c.employee_number
		where a.year_month = 202106) aa) x
full outer join (		
		select 'gross', a.store, a.consultant, a.employee_number, c.gross_expense_act_ as account, 
		  round(c.gross_dist/100.0 * total_79, 2) as amount
		from tmp_accrual_sales a
		left join tmp_accrual_sales_1  c on a.employee_number = c.employee_number
		where a.year_month = 202106) y on x.employee_number = y.employee_number and x.account = y.account
where x.amount <> y.amount		
*/


select * 
from tmp_accrual_sales_2 a
left join sls.tmp_accrual_sales_2 b on a.employee_number = b.employee_number
  and a.account = b.account
order by a.consultant, a.account  



drop table if exists tmp_accrual_sales_2 cascade;
create temp table tmp_accrual_sales_2 as
-- total_79 based on the existing accounts from pyactgr
		select 'gross' as category, a.store, a.consultant, a.employee_number, b.gross_expense_act_ as account, --a.total_79,
		  round(b.gross_dist/100.0 * total_79, 2) as amount
		from tmp_accrual_sales a
		left join tmp_accrual_sales_1  b on a.employee_number = b.employee_number
		where a.year_month = 202106
union
-- pto
		select 'pto', a.store, a.consultant, a.employee_number, b.account, --a.pto_74,
		  round(b.distribution_percentage * a.pto_74, 2) as amount
		from tmp_accrual_sales a
		left join non_gross_distribution b on a.store = b.store
		  and b.category = 'pto_74'
		where a.year_month = 202106
		and a.pto_74 <> 0
union
-- pulse
		select 'pulse', a.store, a.consultant, a.employee_number, b.account, --a.pulse_79B,
		  a.pulse_79B as amount
		from tmp_accrual_sales a
		left join non_gross_distribution b on a.store = b.store
		  and b.category = 'pulse_79B'
		where a.year_month = 202106
		and a.pulse_79B <> 0		
union
-- fi_comm
		select 'fi_comm', a.store, a.consultant, a.employee_number, b.account, --a.fi_comm_79A,
		  round(b.distribution_percentage * a.fi_comm_79A, 2) as amount
		from tmp_accrual_sales a
		left join non_gross_distribution b on a.store = b.store
		  and b.category = 'comm_79A'
		where a.year_month = 202106
		and a.fi_comm_79A <> 0
union
-- fica
		select 'fica', a.store, a.consultant, a.employee_number, b.emplr_fica_expense, 
      sum(round((b.gross_dist/100.0) * (total_gross - (coalesce(ficaex, 0))) * fica_employer_percent/100.0, 2)) AS Amount 
		from tmp_accrual_sales a
		left join tmp_accrual_sales_1 b on a.store = b.store
		  and a.employee_number = b.employee_number
		where a.year_month = 202106
		group by a.store, a.consultant, a.employee_number, b.emplr_fica_expense
union		
-- medic
		select 'medic', a.store, a.consultant, a.employee_number, b.emplr_med_expense, 
      sum(round((b.gross_dist/100.0) * (total_gross - (coalesce(medex, 0))) * employer_medicare_/100.0, 2)) AS Amount 
		from tmp_accrual_sales a
		left join tmp_accrual_sales_1 b on a.store = b.store
		  and a.employee_number = b.employee_number
		where a.year_month = 202106
		group by a.store, a.consultant, a.employee_number, b.emplr_med_expense
union
-- retire
		select 'retire', a.store, a.consultant, a.employee_number, b.emplr_contributions,
		sum(
			case
				when fixed_ded_amt is null then 0
				else
					case
						when (a.total_gross * b.fixed_ded_amt/200.0) < .02 * a.total_gross
							then round((b.gross_dist/100.0 * total_gross) * (fixed_ded_amt/200.0), 2)
						else
							round((b.gross_dist/100.0) * total_gross * .02, 2)
					end
			end) as the_amount
		from tmp_accrual_sales a
		left join tmp_accrual_sales_1 b on a.store = b.store
		  and a.employee_number = b.employee_number
		where a.year_month = 202106
   group by  a.store, a.consultant, a.employee_number, b.emplr_contributions
order by consultant, category


-- decent comparison to pay check
select *, a.amount - f.amount 
from (
	select store, consultant, employee_number, account, sum(amount) as amount
	from tmp_accrual_sales_2
	group by store, consultant, employee_number, account) a
left join (
  select b.control, e.account, sum(b.amount) as amount
  from fin.fact_gl b
  join dds.dim_date d on b.date_key = d.date_key
    and d.the_date = '07/02/2021'
  join fin.dim_account e on b.account_key = e.account_key
  where b.post_status = 'Y'
-- 		and control = '196575'
-- 		and e.account = '12502' 
		and exists (
			select 1
			from tmp_accrual_sales_2
			where control = b.control)
   group by b.control, e.account) f on a.employee_number = f.control
    and a.account = f.account
order by a.consultant

  select b.control, e.account, b.amount
  from fin.fact_gl b
  join tmp_accrual_sales_2 c on b.control = c.employee_number
  join dds.dim_date d on b.date_key = d.date_key
    and d.the_date = '07/02/2021'
  join fin.dim_account e on b.account_key = e.account_key
    and c.account = e.account
  where b.post_status = 'Y'
  and control = '196575'
  and c.account = '12502'


  select b.control, e.account, sum(b.amount) as amount
  from fin.fact_gl b
  join dds.dim_date d on b.date_key = d.date_key
    and d.the_date = '07/02/2021'
  join fin.dim_account e on b.account_key = e.account_key
  where b.post_status = 'Y'
		and control = '196575'
		and e.account = '12502' 
		and exists (
			select 1
			from tmp_accrual_sales_2
			where control = b.control)
   group by b.control, e.account
-- -- -- fica and medic work area
-- -- retire		
-- -- 07/23/21
-- -- i think for fica, medicare & retirement i need the total of 79 74 79a & 79b
-- -- which will be the total_gross attribute in tmp_accrual_sales
-- select * from tmp_accrual_sales a
-- 
-- select * from tmp_accrual_sales_1  order by employee_number
-- 
-- -- fica
-- 	select 'fica', employee_number, fica_account as account,
-- 		 sum(round((Gross_Distribution_Percentage/100.0) * (/*Total_Pay*/ amount - (coalesce(Fica_Exempt, 0))) * Fica_Percentage/100.0, 2)) AS Amount 
-- 	from (
-- 		select a.store, a.last_name, a.amount, c.*
-- 		from sls.accrual_sales a
-- 		left join sls.accrual_sales_1 c on a.employee_number = c.employee_number
-- 		where a.year_month = _year_month) bb	
-- 	group by last_name, employee_number, fica_account
-- -- some differences are because of the added spiffs from kim
-- -- some i don't understand: waldeck 165989, tarr 1135518
-- -- !!!! the difference is the HSA pretax mistake
-- select aa.*, bb.fica, bb.fica_1 
-- from (
-- select consultant, employee_number, sum(amount), total_gross from (
-- 		select 'fica', a.store, a.consultant, a.employee_number, b.emplr_fica_expense, a.total_gross,
--       sum(round((b.gross_dist/100.0) * (total_gross - (coalesce(ficaex, 0))) * fica_employer_percent/100.0, 2)) AS Amount 
-- 		from tmp_accrual_sales a
-- 		left join tmp_accrual_sales_1 b on a.store = b.store
-- 		  and a.employee_number = b.employee_number
-- 		where a.year_month = 202106
-- 		group by a.store, a.consultant, a.employee_number, b.emplr_fica_expense, a.total_gross
-- ) x group by consultant, employee_number, total_gross) aa
-- left join (
--   select employee_, sum(curr_fica) as fica, sum(curr_emplr_fica) as fica_1
--   from arkona.ext_pyhshdta
--   where check_year = 21
-- 		and check_month = 7
-- 		and check_day = 2
--   group by employee_) bb on aa.employee_number = bb.employee_
-- order by consultant  
-- 
-- 
-- -- medic
-- 		select 'medic', a.store, a.consultant, a.employee_number, b.emplr_fica_expense, a.total_gross,
--       sum(round((b.gross_dist/100.0) * (total_gross - (coalesce(medex, 0))) * employer_medicare_/100.0, 2)) AS Amount 
-- 		from tmp_accrual_sales a
-- 		left join tmp_accrual_sales_1 b on a.store = b.store
-- 		  and a.employee_number = b.employee_number
-- 		where a.year_month = 202106
-- 		group by a.store, a.consultant, a.employee_number, b.emplr_fica_expense, a.total_gross
-- 
-- 
-- select aa.*, bb.medic
-- from (
-- select consultant, employee_number, sum(amount), total_gross from (
-- 		select 'fica', a.store, a.consultant, a.employee_number, b.emplr_fica_expense, a.total_gross,
--       sum(round((b.gross_dist/100.0) * (total_gross - (coalesce(medex, 0))) * employer_medicare_/100.0, 2)) AS Amount 
-- 		from tmp_accrual_sales a
-- 		left join tmp_accrual_sales_1 b on a.store = b.store
-- 		  and a.employee_number = b.employee_number
-- 		where a.year_month = 202106
-- 		group by a.store, a.consultant, a.employee_number, b.emplr_fica_expense, a.total_gross
-- ) x group by consultant, employee_number, total_gross) aa
-- left join (
--   select employee_, sum(curr_employer_medicare) as medic
--   from arkona.ext_pyhshdta
--   where check_year = 21
-- 		and check_month = 7
-- 		and check_day = 2
--   group by employee_) bb on aa.employee_number = bb.employee_
-- order by consultant  





































select * from arkona.ext_pyactgr limit 10
-- aubol & stout
select * from arkona.ext_pyactgr where employee_number in ('17534','1132700')

--07/19/21
-- dist codes for mgrs supplied by jeri
-- jeri does not provide pto amounts, i only ask because aubol does log pto, i am guessing kim splits it
select employee_name, pymast_employee_number, distrib_code,
  c.GROSS_DIST, c.GROSS_EXPENSE_ACT_, 
		c.EMPLR_FICA_EXPENSE, c.EMPLR_MED_EXPENSE, c.EMPLR_CONTRIBUTIONS, c.vacation_expense_act_, sick_leave_expense_act_
from arkona.ext_pymast b
left join arkona.ext_pyactgr c on b.pymast_company_number = c.company_number
	and b.distrib_code = c.dist_code
where employee_last_name in ('shirek','erickson','rydell',
	'wilkie','holland','dockendorf','michael','stout',
	'schumacher','longoria','knudson', 'aubol') 
	and active_code <> 'T' 
  and employee_name not in ('DOCKENDORF, BEN','ERICKSON, ANTHONY','ERICKSON, ANTHONY','ERICKSON, JUSTIN',
		'ERICKSON, DENNIS','KNUDSON, KENNETH','LONGORIA, BEVERLEY A')


















-- attemtping to match accounting from the relevant check

select b.the_date, a.* 
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.the_date = '07/02/2021'
join fin.dim_Account c on a.account_key = c.account_key
  and c.account = '164502'
where a.post_status = 'Y'
  and a.control = '1130690'

select *
from (
		select 'pulse', a.store, a.consultant, a.employee_number, b.account, a.pulse_79B,
		  a.pulse_79B as amount
		from tmp_accrual_sales a
		left join non_gross_distribution b on a.store = b.store
		  and b.category = 'pulse_79B'
		where a.year_month = 202106
		and a.pulse_79B <> 0) x
full outer join (
		select b.the_date, c.account, a.control, sum(a.amount)
		from fin.fact_gl a
		join dds.dim_date b on a.date_key = b.date_key
			and b.the_date = '07/02/2021'
		join fin.dim_Account c on a.account_key = c.account_key
		join non_gross_distribution d on c.account = d.account
		  and d.category = 'pulse_79B'
		where a.post_status = 'Y'
		group by b.the_date, c.account, a.control) y on x.employee_number = y.control
		  and x.account = y.account		














-- -- the guarantee_multiplier issue
-- 07/15/21 i am using the wrong calculation, this is using calendar dates
-- and even worse, days not worked rather than days worked
-- both sls.json_get_payroll_for_submittal(citext) &  sls.json_get_payroll_for_kim(citext) use the same formulation
-- and include, which i did not here, a multiplier for mid month terms, in june, tiffany
select a.last_name, a.first_name, a.employee_number, g.start_date, a.draw, g.guarantee_multiplier, h.term_guarantee_multiplier,
-- 	extract(day from g.start_date)::numeric, (a.guarantee - a.draw) * g.guarantee_multiplier
	a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1) - coalesce(a.draw, 0)
	from sls.consultant_payroll a
	left join sls.pto_intervals b on a.employee_number = b.employee_number
		and a.year_month = b.year_month
	left join sls.paid_by_month c on a.employee_number = c.employee_number
		and a.year_month = c.year_month
	left join (
		select employee_number, sum(amount) as adjusted_amount
		from sls.payroll_adjustments
		where year_month =  202106
		group by employee_number) d on a.employee_number = d.employee_number
	left join (
		select employee_number, sum(amount) as additional_comp
		from sls.additional_comp
		where thru_date > (
			select first_of_month
			from sls.months
			where year_month = 202106)
		group by employee_number) e on a.employee_number = e.employee_number  
	left join sls.personnel f on a.employee_number = f.employee_number
-- 	left join ( -- if this is the first month of employment, multiplier = days worked/days in month
-- 		select a.employee_number, a.start_date,
-- 			round(extract(day from a.start_date)::numeric 
-- 				/ (select max(day_of_month)::numeric from dds.dim_date where year_month = b.year_month), 2) as guarantee_multiplier
-- 		from sls.personnel a
-- 		inner join dds.dim_date b on a.start_date = b.the_date
-- 			and b.year_month = 202106) g on f.employee_number = g.employee_number    
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
    select a.employee_number, a.start_date,
      round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
    from sls.personnel a
    inner join dds.dim_date b on a.start_date = b.the_date
--       and b.year_month = (select year_month from open_month)) g on f.employee_number = g.employee_number     
      and b.year_month = 202106) g on f.employee_number = g.employee_number  
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, a.end_date,
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
--       and b.year_month = (select year_month from open_month) ) h on f.employee_number = h.employee_number     
      and b.year_month = 202106) h on f.employee_number = h.employee_number      
	where a.year_month = 202106
    and a.employee_number in ('126948','159857','264845','187651',/* & some regular emps*/'1135518','189100')

