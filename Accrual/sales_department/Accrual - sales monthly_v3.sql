﻿this is the distillation of sales_monthly_v2 to include the manual edits required
xfm all the tmp tables to sls.tmp_xxxx
a re-creation of the june 21 accrual

-- drop table if exists sls.tmp_accrual_sales cascade;
-- create table sls.tmp_accrual_sales (
-- 	year_month integer not null,
--   store citext not null,
--   employee citext not null,
--   employee_number citext not null,
--   pto_74 numeric default '0',
--   total_79 numeric default '0',
--   fi_comm_79a numeric default '0',
--   pulse_79b numeric default '0',
--   total_gross numeric default '0',
--   primary key(year_month, employee_number));
--   
-- 
-- sales_accrual_1:  first step of deductions
-- drop table if exists sls.tmp_accrual_sales_1 cascade;
-- CREATE unlogged TABLE sls.tmp_accrual_sales_1 ( 
--       store citext not null,
--       employee_number citext not null,
-- --       total_pay numeric not null,
--       gross_distribution_percentage numeric not null,
--       gross_account citext not null,
--       fica_account citext not null,
--       medicare_account citext not null,
--       retirement_account citext not null,
--       fica_percentage numeric not null,
--       medicare_percentage numeric not null,
--       fica_exempt numeric not null,
--       medicare_exempt numeric not null,
--       fixed_deduction_amount numeric not null,
--   		has_91c boolean not null default FALSE, 
--       primary key(employee_number,gross_account));
-- 
-- -- sales_accrual_2:  retirement, gross
-- drop table if exists sls.tmp_accrual_sales_2 cascade;
-- create unlogged table sls.tmp_accrual_sales_2 (
-- 	category citext not null,
-- 	employee citext not null,
-- 	employee_number citext not null,
-- 	account citext not null,
-- 	amount numeric,
-- 	primary key(employee_number, category, account));
-- --
-- drop table if exists sls.tmp_accrual_dates cascade;
-- create unlogged table sls.tmp_accrual_dates (
-- 	journal citext not null,
-- 	the_date date primary key,
-- 	document citext not null,
-- 	reference citext not null,
-- 	description citext not null default 'Payroll Accrual');
-- 
-- drop table if exists sls.tmp_accrual_file_for_jeri cascade;
-- create unlogged table sls.tmp_accrual_file_for_jeri (
--   journal citext not null,
--   the_date date not null,
--   account citext not null,
--   control citext not null,
--   document citext not null,
--   reference citext not null,
--   amount numeric(8,2) not null,
--   description citext not null,
--   primary key(control,account));
-- 
-- drop table if exists sls.tmp_accrual_history cascade;
-- create table sls.tmp_accrual_history (
--   year_month integer not null,
--   account citext not null,
--   control citext not null,
--   amount numeric(8,2) not null,
--   primary key(year_month,account,control));


-- TODO create a table of accounts including category


for the july accrual, until jeri oks it do it all in the sls.tmp tables

-- 1. 07/01/21 sales mgmt from jeri
-- sales mgmt accrual_sales, generate the insert statements with 0 as total_gross
-- TODO make this a table
select 'insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values ('|| 202107 || ',''' || pymast_company_number ||''','''||
  employee_last_name||','||employee_first_name|| ''',''' ||pymast_employee_number || ''','||0||','||0||');'
from arkona.ext_pymast
where employee_last_name in ('shirek','erickson','rydell',
	'wilkie','holland','dockendorf','michael','stout',
	'schumacher','longoria','haley','knudson','foster','monson')
and active_code <> 'T'	
order by employee_last_name, employee_first_name	

truncate sls.tmp_accrual_sales;



-- 2. sales consultants accrual_sales & managers from jeri
do $$
declare
	_journal citext := 'GLI'; -- for sls.accrual_dates
	_the_date date := '07/31/2021'; -- for sls.accrual_dates & filter on sls.personnel
	_year_month integer := 202107;
	_payroll_year integer := 121;

begin	
	truncate sls.tmp_accrual_dates;
	insert into sls.tmp_accrual_dates
	select _journal, _the_date,
		_journal || lpad(extract(month from _the_date)::text, 2, '0') || lpad(extract(day from _the_date)::text, 2, '0') || right(extract(year from _the_date)::text, 2),
		_journal || lpad(extract(month from _the_date)::text, 2, '0') || lpad(extract(day from _the_date)::text, 2, '0') || right(extract(year from _the_date)::text, 2);

-- sales consultants	
	truncate sls.tmp_accrual_sales;	

	
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','SHIREK,NICHOLAS','1126300',10000,10000);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','RYDELL,WESLEY','1120800',12000,12000);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','WILKIE,DAVID','1149180',6300,6300);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','MONSON,TAYLOR','118030',1000,1000);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','HOLLAND,NIKOLAI','111232',7000,7000);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','DOCKENDORF,NATE','133017',8500,8500);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','MICHAEL,ANTHONY','195460',9500,9500);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','STOUT,RICK','1132700',15000,15000);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','SCHUMACHER,BRADLEY','165470',7000,7000);
  insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','ERICKSON,RONALD','140500',6725,6725);
  
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY2','LONGORIA,MICHAEL','265328',7250,7250);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY1','FOSTER,SAMUEL','248080',5750,5750);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202107,'RY2','KNUDSON,BENJAMIN','279380',7000,7000);	

	insert into sls.tmp_accrual_sales		
	select _year_month, case left(a.employee_number, 1) when '1' then 'RY1' when'2' then 'RY2' end as store,
		a.last_name || ', ' || a.first_name as consultant, a.employee_number, 
		a.pto_pay as pto_74,
			-- combine all the 79s
			coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0) + round (
				case -- paid spiffs deducted only if base guarantee
					when a.total_earned >= a.guarantee then
						a.unit_pay - coalesce(a.draw, 0)
					else
						(a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) - coalesce(c.spiffs, 0)
				end, 2) as total_79,       
			a.fi_pay as fi_comm_79A,   
			a.pulse as pulse_79B,
			round( 
				case -- paid spiffs deducted only if base guarantee
					when a.total_earned >= a.guarantee then
						a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
					else
						(a.guarantee * coalesce(g.guarantee_multiplier, 1) * coalesce(h.term_guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
								- coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
					end, 2) as total_gross -- "Total Month End Payout"      
	from sls.consultant_payroll a
	left join sls.pto_intervals b on a.employee_number = b.employee_number
		and a.year_month = b.year_month
	left join sls.paid_by_month c on a.employee_number = c.employee_number
		and a.year_month = c.year_month
	left join (
		select employee_number, sum(amount) as adjusted_amount
		from sls.payroll_adjustments
		where year_month =  _year_month
		group by employee_number) d on a.employee_number = d.employee_number
	left join (
		select employee_number, sum(amount) as additional_comp
		from sls.additional_comp
		where thru_date > (
			select first_of_month
			from sls.months
			where open_closed = 'open')
		group by employee_number) e on a.employee_number = e.employee_number  
	left join sls.personnel f on a.employee_number = f.employee_number
  left join ( -- if this is the first month of employment, multiplier = days worked/days in month
    select a.employee_number, 
      round(wd_of_month_remaining::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as guarantee_multiplier 
    from sls.personnel a
    inner join dds.dim_date b on a.start_date = b.the_date
      and b.year_month = _year_month) g on f.employee_number = g.employee_number      
  left join ( -- if employee termed mid month, multiplier elapsed wd in month at term date/work days in month
    select a.employee_number, 
      round(wd_of_month_elapsed::numeric/(wd_of_month_remaining + wd_of_month_elapsed), 4) as term_guarantee_multiplier
    from sls.personnel a
    inner join dds.dim_date b on a.end_date = b.the_date
      and b.year_month = _year_month) h on f.employee_number = h.employee_number         
    where a.year_month = _year_month;
    
-- tom aubol
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,pto_74,fi_comm_79a,total_gross)
	select  _year_month, 'RY1', 'Aubol, Thomas', '17534', pto_pay,
		fi_gross + chargebacks - draw, fi_gross + chargebacks - draw
  from sls.fi_manager_comp
  where year_month = _year_month;
  
end	$$;    

select * from sls.tmp_accrual_sales where year_month = '202107' order by employee

-- 3. add kims adjustments (spiffs, corrections, etc) to accrual_sales;
-- pto
update sls.tmp_accrual_sales
set pto_74 = pto_74 + 360.96, total_gross = total_gross + 360.96
where employee = 'Johnson, Christie'
  and year_month = '202107';

update sls.tmp_accrual_sales
set pto_74 = pto_74 + 906.32, total_gross = total_gross + 906.32
where employee = 'Bjarnason, Justin'
  and year_month = '202107';
-- gross
update sls.tmp_accrual_sales
set total_79 = total_79 + 300, total_gross = total_gross + 300
where employee = 'Spencer, Fred'
  and year_month = '202107';

update sls.tmp_accrual_sales
set total_79 = total_79 + 400, total_gross = total_gross + 400
where employee = 'Wilde, Corey'
  and year_month = '202107';

update sls.tmp_accrual_sales
set total_79 = total_79 + 50, total_gross = total_gross + 50
where employee = 'Rumen, Robert'
  and year_month = '202107';

update sls.tmp_accrual_sales
set total_79 = total_79 + 50, total_gross = total_gross + 50
where employee = 'Warmack, James'
  and year_month = '202107';

update sls.tmp_accrual_sales
set total_79 = total_79 + 100, total_gross = total_gross + 100
where employee = 'Weber, James'
  and year_month = '202107';

update sls.tmp_accrual_sales
set total_79 = total_79 + 100, total_gross = total_gross + 100
where employee = 'SCHUMACHER,BRADLEY'
  and year_month = '202107';


  
-- 4. accrual_sales_1 
do $$
declare
	_journal citext := 'GLI'; -- for sls.accrual_dates
	_the_date date := '07/31/2021'; -- for sls.accrual_dates & filter on sls.personnel
	_year_month integer := 202107;
	_payroll_year integer := 121;

begin
truncate sls.tmp_accrual_sales_1;
insert into sls.tmp_accrual_sales_1
	select a.store, a.employee_number, 
		c.GROSS_DIST, c.GROSS_EXPENSE_ACT_, 
		c.EMPLR_FICA_EXPENSE, c.EMPLR_MED_EXPENSE, c.EMPLR_CONTRIBUTIONS,
		e.fica_employer_percent, e.employer_medicare_, 
		coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
		coalesce(h.fixed_ded_amt, 0) AS fixed_ded_amt,
		case when h.ded_pay_code = '91c' then true else false end
	from sls.tmp_accrual_sales a
	join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
	left join arkona.ext_pyactgr c on b.pymast_company_number = c.company_number
		and b.distrib_code = c.dist_code
	left join arkona.ext_pycntrl e on b.pymast_company_number = e.company_number  -- fica & medicare percentages for store/year 
		and e.payroll_year = _payroll_year 
	left join ( -- FICA Exempt deductions, may be NULL, coalesce IN select
		select employee_number, sum(fixed_ded_amt) as FicaEx
		from arkona.ext_pydeduct x
		where exists (
			select 1
			from arkona.ext_pypcodes
			where ded_pay_code = x.ded_pay_code
				and exempt_2_fica_ = 'Y')
		 group by employee_number) f on a.employee_number = f.employee_number
	left join ( -- -- Medicare Exempt deductions, may be NULL, coalesce IN select
		select employee_number, sum(fixed_ded_amt) as MedEx
		from arkona.ext_pydeduct x
		where exists (
			select 1
			from arkona.ext_pypcodes
			where ded_pay_code = x.ded_pay_code
				and exempt_6_medc_tax_ = 'Y')
		 group by employee_number) g on a.employee_number = g.employee_number	 
	left join arkona.ext_pydeduct h on  b.pymast_company_number = h.company_number -- retirement deductions
		and a.employee_number = h.employee_number
		and h.ded_pay_code in ('91','91b','91c','99','99a','99b','99c')
	where a.year_month = _year_month; 

end	$$;	
-- 
-- select * from sls.tmp_accrual_sales_1 order by employee_number

-- 5. alt_distribution
drop table if exists sls.tmp_alt_distribution;
create table sls.tmp_alt_distribution (
	store citext not null,
  category citext not null,
  account citext not null,
  distribution_percentage numeric(3,2) not null,
  primary key (category,account));

insert into sls.tmp_alt_distribution values
('RY1','pto_74','12401',.4),
('RY1','pto_74','12402',.6),
('RY1','comm_79A','185501',.4),
('RY1','comm_79A','185600',.6),
('RY1','pulse_79B','164502',1),
('RY2','pto_74','22401',.3),
('RY2','pto_74','22410',.2),
('RY2','pto_74','22402',.5),
('RY2','pulse_79B','264502',1);




-- 6. accrual_sales_2 
-- the issue with double retirement employer contribution is if 91c, then it is 2 times per Jeri 7/27/21, needed for dealertrack
-- leave extra fields in sls.tmp_accrual_sales_2 for troubleshooting purposes
do $$
declare
-- 	_journal citext := 'GLI'; -- for sls.accrual_dates
-- 	_the_date date := '06/30/2021'; -- for sls.accrual_dates & filter on sls.personnel
	_year_month integer := 202107;
-- 	_payroll_year integer := 121;
begin
truncate sls.tmp_accrual_sales_2;
insert into sls.tmp_accrual_sales_2
		select 'gross' as category, a.employee, a.employee_number, b.gross_account, --a.total_79,
		  round(b.gross_distribution_percentage/100.0 * total_79, 2) as amount
		from sls.tmp_accrual_sales a
		left join sls.tmp_accrual_sales_1  b on a.employee_number = b.employee_number
		where a.year_month = _year_month
union
-- pto
		select 'pto',  a.employee,a.employee_number, b.account, --a.pto_74,
		  round(b.distribution_percentage * a.pto_74, 2) as amount
		from sls.tmp_accrual_sales a
		left join sls.tmp_alt_distribution b on a.store = b.store
		  and b.category = 'pto_74'
		where a.year_month = _year_month
		and a.pto_74 <> 0
union
-- pulse
		select 'pulse',  a.employee,a.employee_number, b.account, --a.pulse_79B,
		  a.pulse_79B as amount
		from sls.tmp_accrual_sales a
		left join sls.tmp_alt_distribution b on a.store = b.store
		  and b.category = 'pulse_79B'
		where a.year_month = _year_month
		and a.pulse_79B <> 0		
union
-- fi_comm
		select 'fi_comm',  a.employee,a.employee_number, b.account, --a.fi_comm_79A,
		  round(b.distribution_percentage * a.fi_comm_79A, 2) as amount
		from sls.tmp_accrual_sales a
		left join sls.tmp_alt_distribution b on a.store = b.store
		  and b.category = 'comm_79A'
		where a.year_month = _year_month
		and a.fi_comm_79A <> 0
union
-- fica
		select 'fica',  a.employee,a.employee_number, b.fica_account, 
      sum(round((b.gross_distribution_percentage/100.0) * (a.total_gross - (coalesce(fica_exempt, 0))) * fica_percentage/100.0, 2)) AS Amount 
		from sls.tmp_accrual_sales a
		left join sls.tmp_accrual_sales_1 b on a.store = b.store
		  and a.employee_number = b.employee_number
		where a.year_month = _year_month
		group by  a.employee,a.employee_number, b.fica_account
union		
-- medic
		select 'medic',  a.employee,a.employee_number, b.medicare_account, 
      sum(round((b.gross_distribution_percentage/100.0) * (a.total_gross - (coalesce(b.medicare_exempt, 0))) * b.medicare_percentage/100.0, 2)) AS Amount 
		from sls.tmp_accrual_sales a
		left join sls.tmp_accrual_sales_1 b on a.store = b.store
		  and a.employee_number = b.employee_number
		where a.year_month = _year_month
		group by  a.employee,a.employee_number, b.medicare_account
union
-- retire with the 91c fix
select 'retire', employee, employee_number, retirement_account, 
  case
    when has_91c then 2 * the_amount
    else the_amount
  end
from (
	select 'retire',  a.employee,a.employee_number, b.retirement_account,
	sum(
		case
			when fixed_deduction_amount is null then 0
			else
				case
					when (a.total_gross * b.fixed_deduction_amount/200.0) < .02 * a.total_gross
						then round((b.gross_distribution_percentage/100.0 * a.total_gross) * (b.fixed_deduction_amount/200.0), 2)
					else
						round((b.gross_distribution_percentage/100.0) * a.total_gross * .02, 2)
				end
		end) as the_amount, has_91c
	from sls.tmp_accrual_sales a
	left join sls.tmp_accrual_sales_1 b on a.store = b.store
		and a.employee_number = b.employee_number
	where a.year_month = _year_month
 group by  a.employee,a.employee_number, b.retirement_account, has_91c) c;
END $$;

select * from sls.tmp_accrual_sales_2

-- generate table sls.accrual_file_for_jeri including reversing entries
	truncate sls.accrual_file_for_jeri cascade;
	insert into sls.accrual_file_for_jeri
	-- accrual file for jeri, base entries
	select b.journal, b.the_date, a.account, a.employee_number as control, 
		b.document, b.reference, sum(a.amount) as amount, b.description
	from sls.tmp_accrual_sales_2 a, sls.tmp_accrual_dates b
	where amount <> 0
	group by b.journal, b.the_date, a.account, a.employee_number, 
		b.document, b.reference, b.description
	union 
	-- accrual file for jeri, reversing entries
	SELECT e.journal, e.the_date, d.Account, e.document, e.document, e.reference, 
		round(d.Amount, 2), e.description
	from (  
		select 
			case left(account, 1)
				when '1' then '132101'
				when '2' then '232103'
			end as account,
			sum(amount) * -1 as amount
		from (
			select employee_number, account, sum(amount) as amount
			from sls.tmp_accrual_sales_2
			group by employee_number, account) c
		group by 
			case left(account, 1)
				when '1' then '132101'
				when '2' then '232103'
			end) d, sls.tmp_accrual_dates e;


select * from sls.accrual_file_for_jeri

select * from sls.accrual_history

	insert into sls.accrual_history
	select 202107, account, control, amount
	from sls.accrual_file_for_jeri;


-- individual values & total by employee
select employee, employee_number,
  sum(amount) filter (where category in ('fica','medic')) as fica_med,
  sum(amount) filter (where category = 'retire') as retire,
  sum(amount) filter (where category = 'pto') as pto,
  sum(amount) filter (where category = 'pulse') as pulse,
  sum(amount) filter (where category = 'fi_comm') as fi_comm,
  sum(amount) filter (where category not in ('fica','medic','retire')) as total_gross
from sls.tmp_accrual_sales_2
where amount <> 0
group by employee, employee_number
order by left(employee_number, 1), employee


--08/04/21 compare to accounting from check, pretty good, not perfect
select *, a.amount - f.amount 
from (
	select employee, employee_number, account, sum(amount) as amount
	from sls.tmp_accrual_sales_2
	where amount <> 0
	group by employee, employee_number, account) a
left join (
  select b.control, e.account, sum(b.amount) as amount
  from fin.fact_gl b
  join dds.dim_date d on b.date_key = d.date_key
    and d.the_date = '08/02/2021'
  join fin.dim_account e on b.account_key = e.account_key
  where b.post_status = 'Y'
-- 		and control = '196575'
-- 		and e.account = '12502' 
		and exists (
			select 1
			from sls.tmp_accrual_sales_2
			where control = b.control)
   group by b.control, e.account) f on a.employee_number = f.control
    and a.account = f.account
order by a.employee




















select b.category, b.employee, a.employee_number, a.account, a.amount, b.amount, b.account, sum(a.amount) over (partition by a.employee_number), sum(b.amount) over (partition by b.employee_number) 
from (
select * from sls.accrual_sales_2) a
full outer join (
select * from sls.tmp_accrual_sales_2) b on a.employee_number = b.employee_number and a.account = b.account
order by b.employee, b.category


select * from sls.tmp_accrual_sales_2 where employee_number = '196575' order by account

select * from sls.tmp_accrual_sales_1 where employee_number = '133017'

select * from sls.tmp_accrual_sales order by employee

select distinct case when category in ('fica','medic') then 'fica/medic' else category end as category, account from sls.tmp_accrual_sales_2 order by account

select * from sls.tmp_accrual_sales_2 where account = '185501'
select * from sls.tmp_accrual_sales_2 where employee_number = '17534'
-- decent comparison to pay check
-- fica and medic have the same account, so can't include category in grouping
-- emplr contrib to retirement looks f/u, olderbak 1160225 
-- saved as 202106_diff.xlsx to discuss with jeri
select --(select employee from sls.tmp_accrual_sales where employee_number = a.employee_number), 
	a.*, f.amount as check_amount, a.amount - f.amount as diff,  g.has_91c
from (
	select employee, employee_number, 
		case when category in ('fica','medic') then 'fica/medic' else category end as category,
		account, sum(amount) as amount
	from sls.tmp_accrual_sales_2
	where amount <> 0
	group by employee, employee_number, case when category in ('fica','medic') then 'fica/medic' else category end, account) a
left join (
  select b.control, e.account, sum(b.amount) as amount
  from fin.fact_gl b
  join dds.dim_date d on b.date_key = d.date_key
    and d.the_date = '07/02/2021'
  join fin.dim_account e on b.account_key = e.account_key
  where b.post_status = 'Y'
-- 		and control = '196575'
-- 		and e.account = '12502' 
		and exists (
			select 1
			from sls.tmp_accrual_sales_2
			where control = b.control)
   group by b.control, e.account) f on a.employee_number = f.control
    and a.account = f.account
left join (
  select distinct employee_number, has_91c
  from sls.tmp_accrual_sales_1) g on a.employee_number = g.employee_number   
-- where category = 'retire'  
-- order by abs(a.amount - f.amount) desc    
order by a.employee, a.category
order by category, employee



