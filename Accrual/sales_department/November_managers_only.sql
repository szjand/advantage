﻿do $$
declare
	_journal citext := 'GLI'; -- for sls.accrual_dates
	_the_date date := '11/30/2021'; ----------- last day of accrual month for sls.accrual_dates & filter on sls.personnel
	_year_month integer := 202111;  -------------------------------------------------------
	_payroll_year integer := 121;   ---------------------------

begin	
	truncate sls.tmp_accrual_dates;
	insert into sls.tmp_accrual_dates
	select _journal, _the_date,
		_journal || lpad(extract(month from _the_date)::text, 2, '0') || lpad(extract(day from _the_date)::text, 2, '0') || right(extract(year from _the_date)::text, 2),
		_journal || lpad(extract(month from _the_date)::text, 2, '0') || lpad(extract(day from _the_date)::text, 2, '0') || right(extract(year from _the_date)::text, 2);

-- 11/01/2021 fuck it, just update this statement as needed
	truncate sls.tmp_accrual_sales;	
---------------------------------------------------fucking forgot to change the year_month -------------------------------------------------------
-- this is the insert statement generated above	
-- AND AGAIN ON 12/1 I DID NOT EDIT THE DATE
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY1','SHIREK,NICHOLAS','1126300',10000,10000);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY1','RYDELL,WESLEY','1120800',12000,12000);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY1','WILKIE,DAVID','1149180',9300,9300);
-- 	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202110,'RY1','MONSON,TAYLOR','118030',1000,1000);
-- 	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202110,'RY1','HOLLAND,NIKOLAI','111232',4932,4932);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY1','DOCKENDORF,NATE','133017',8500,8500);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY1','MICHAEL,ANTHONY','195460',10500,10500);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY1','STOUT,RICK','1132700',12000,12000);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY1','SCHUMACHER,BRADLEY','165470',7500,7500);
  insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY1','ERICKSON,RONALD','140500',6530,6530);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY2','LONGORIA,MICHAEL','265328',7250,7250);
	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202111,'RY1','FOSTER,SAMUEL','248080',5250,5250);
-- 	insert into sls.tmp_accrual_sales(year_month,store,employee,employee_number,total_79,total_gross) values (202110,'RY2','KNUDSON,BENJAMIN','279380',7000,7000);	

end	$$;    


do $$
declare
	_journal citext := 'GLI'; -- for sls.accrual_dates
	_the_date date := '11/30/2021'; ----------- last day of accrual month for sls.accrual_dates & filter on sls.personnel
	_year_month integer := 202111;  ------------------------------------------
	_payroll_year integer := 121;

begin
truncate sls.tmp_accrual_sales_1;
insert into sls.tmp_accrual_sales_1
	select a.store, a.employee_number, 
		c.GROSS_DIST, c.GROSS_EXPENSE_ACT_, 
		c.EMPLR_FICA_EXPENSE, c.EMPLR_MED_EXPENSE, c.EMPLR_CONTRIBUTIONS,
		e.fica_employer_percent, e.employer_medicare_, 
		coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
		coalesce(h.fixed_ded_amt, 0) AS fixed_ded_amt,
		case when h.ded_pay_code = '91c' then true else false end
	from sls.tmp_accrual_sales a
	join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
	left join arkona.ext_pyactgr c on b.pymast_company_number = c.company_number
		and b.distrib_code = c.dist_code
	left join arkona.ext_pycntrl e on b.pymast_company_number = e.company_number  -- fica & medicare percentages for store/year 
		and e.payroll_year = _payroll_year 
	left join ( -- FICA Exempt deductions, may be NULL, coalesce IN select
		select employee_number, sum(fixed_ded_amt) as FicaEx
		from arkona.ext_pydeduct x
		where exists (
			select 1
			from arkona.ext_pypcodes
			where ded_pay_code = x.ded_pay_code
				and exempt_2_fica_ = 'Y')
		 group by employee_number) f on a.employee_number = f.employee_number
	left join ( -- -- Medicare Exempt deductions, may be NULL, coalesce IN select
		select employee_number, sum(fixed_ded_amt) as MedEx
		from arkona.ext_pydeduct x
		where exists (
			select 1
			from arkona.ext_pypcodes
			where ded_pay_code = x.ded_pay_code
				and exempt_6_medc_tax_ = 'Y')
		 group by employee_number) g on a.employee_number = g.employee_number	 
	left join arkona.ext_pydeduct h on  b.pymast_company_number = h.company_number -- retirement deductions
		and a.employee_number = h.employee_number
		and h.ded_pay_code in ('91','91b','91c','99','99a','99b','99c')
	where a.year_month = _year_month; 

end	$$;	    

select count(distinct employee_number) from sls.tmp_accrual_sales_1;

do $$
declare
	_year_month integer := 202111; ------------------------------------------------------------------
begin
truncate sls.tmp_accrual_sales_2;
insert into sls.tmp_accrual_sales_2
-- drop table if exists wtf;
-- create temp table wtf as
		select 'gross' as category, a.employee, a.employee_number, b.gross_account, --a.total_79,
		  round(b.gross_distribution_percentage/100.0 * total_79, 2) as amount
		from sls.tmp_accrual_sales a
		left join sls.tmp_accrual_sales_1  b on a.employee_number = b.employee_number
		where a.year_month = _year_month
union
-- pto
		select 'pto',  a.employee,a.employee_number, b.account, --a.pto_74,
		  round(b.distribution_percentage * a.pto_74, 2) as amount
		from sls.tmp_accrual_sales a
		left join sls.tmp_alt_distribution b on a.store = b.store
		  and b.category = 'pto_74'
		where a.year_month = _year_month
		and a.pto_74 <> 0
union
-- pulse
		select 'pulse',  a.employee,a.employee_number, b.account, --a.pulse_79B,
		  a.pulse_79B as amount
		from sls.tmp_accrual_sales a
		left join sls.tmp_alt_distribution b on a.store = b.store
		  and b.category = 'pulse_79B'
		where a.year_month = _year_month
		and a.pulse_79B <> 0		
union
-- fi_comm
		select 'fi_comm',  a.employee,a.employee_number, b.account, --a.fi_comm_79A,
		  round(b.distribution_percentage * a.fi_comm_79A, 2) as amount
		from sls.tmp_accrual_sales a
		left join sls.tmp_alt_distribution b on a.store = b.store
		  and b.category = 'comm_79A'
		where a.year_month = _year_month
		and a.fi_comm_79A <> 0
union
-- fica
		select 'fica',  a.employee,a.employee_number, b.fica_account, 
      sum(round((b.gross_distribution_percentage/100.0) * (a.total_gross - (coalesce(fica_exempt, 0))) * fica_percentage/100.0, 2)) AS Amount 
		from sls.tmp_accrual_sales a
		left join sls.tmp_accrual_sales_1 b on a.store = b.store
		  and a.employee_number = b.employee_number
		where a.year_month = _year_month
		group by  a.employee,a.employee_number, b.fica_account
union		
-- medic
		select 'medic',  a.employee,a.employee_number, b.medicare_account, 
      sum(round((b.gross_distribution_percentage/100.0) * (a.total_gross - (coalesce(b.medicare_exempt, 0))) * b.medicare_percentage/100.0, 2)) AS Amount 
		from sls.tmp_accrual_sales a
		left join sls.tmp_accrual_sales_1 b on a.store = b.store
		  and a.employee_number = b.employee_number
		where a.year_month = _year_month
		group by  a.employee,a.employee_number, b.medicare_account
union
-- retire with the 91c fix
select 'retire', employee, employee_number, retirement_account, 
  case
    when has_91c then 2 * the_amount
    else the_amount
  end
from (
	select 'retire',  a.employee,a.employee_number, b.retirement_account,
	sum(
		case
			when fixed_deduction_amount is null then 0
			else
				case
					when (a.total_gross * b.fixed_deduction_amount/200.0) < .02 * a.total_gross
						then round((b.gross_distribution_percentage/100.0 * a.total_gross) * (b.fixed_deduction_amount/200.0), 2)
					else
						round((b.gross_distribution_percentage/100.0) * a.total_gross * .02, 2)
				end
		end) as the_amount, has_91c
	from sls.tmp_accrual_sales a
	left join sls.tmp_accrual_sales_1 b on a.store = b.store
		and a.employee_number = b.employee_number
	where a.year_month = _year_month
 group by  a.employee,a.employee_number, b.retirement_account, has_91c) c;
END $$;

select count(distinct employee_number) from sls.tmp_accrual_sales_2;
select * from sls.tmp_accrual_sales_2 order by employee, category


	select b.journal, b.the_date, a.account, a.employee_number as control, 
		b.document, b.reference, sum(a.amount) as amount, b.description
	from sls.tmp_accrual_sales_2 a, sls.tmp_accrual_dates b
	where amount <> 0
	group by b.journal, b.the_date, a.account, a.employee_number, 
		b.document, b.reference, b.description
	union 
	-- accrual file for jeri, reversing entries
	SELECT e.journal, e.the_date, d.Account, e.document, e.document, e.reference, 
		round(d.Amount, 2), e.description
	from (  
		select 
			case left(account, 1)
				when '1' then '132101'
				when '2' then '232103'
			end as account,
			sum(amount) * -1 as amount
		from (
			select employee_number, account, sum(amount) as amount
			from sls.tmp_accrual_sales_2
			group by employee_number, account) c
		group by 
			case left(account, 1)
				when '1' then '132101'
				when '2' then '232103'
			end) d, sls.tmp_accrual_dates e;