﻿1.
use the report that goes to kim to generate the accrual
supplement with aubol pay
the advantage of this approach is the actual payroll would be accrued

all i need to do is copy the payrolls (consultants and aubol) to advantage

from the sales managers perspective, year end now becomes the same as any other month

.../sales_pay_plan_201803/kims_page.sql looks good for the consultants, solid final month total numbers

if i understand correctly, the accrual amount will be for the total amount to be paid (not including draw)
	regardless of the number of days in the accrual

	
talk it over with jeri, but i think this works

except, what about rj, nick, team leaders etc, looks like i use to include them , but not in 2020
lets see if i can do it all in postgres, no need for dimemployee if it is all based on the 
consultant payroll

05/17/21
talked to jeri, she seem ok
she will look into the managers
she is thinking on not starting in may
though she would like me to run it separately for may so she can check it out
-----------------------------------------------------------------------------
do a test on 202104
1st the consultants and their gross
--< 06/26/21 ---------------------------------------------------------------------------------------------------
	the spiffs are the missing piece

06/26/21
	reqd arkona tables:  
		arkona.ext_pymast
		arkona.ext_pyactgr
		arkona.ext_pycntrl
		arkona.ext_pydeduct
all are updated nightly		

from jeri:
	Differences are attributed to the following:

	Spiffs are not being included in the accrual.  They are on the vision page, so perhaps they can be?
	The other difference is due to Onstar bonuses (not in Vision as sales does not keep track of this; 
		we pay as we receive from GM)—we get reimbursed this from GM, so this is ok to exclude.  
	Appears to be a slight rounding difference on the Unit Pay when there is a split deal.  
	It shows correctly in Vision, but when it goes to Kim, she says it is only whole dollar amounts.

	This will be the first month-end where we accrue for sales.  
	I will need to turn in the amounts for sales managers, correct (with exception of Tom)?

the "rounding" error
RY2 april		accrual		paycheck
		brunk		3012.5		3013
		wild		8011.5		8012
the copy of the spreadsheet that i have shows the correct amounts
is kims spreadsheet somehow different?		

--/> 06/26/21 ---------------------------------------------------------------------------------------------------


-- drop table if exists sls.accrual_sales cascade;
-- create table sls.accrual_sales (
-- 	year_month integer not null,
--   store citext not null,
--   first_name citext not null,
--   last_name citext not null,
--   employee_number citext not null,
--   amount numeric not null,
--   primary key(year_month, employee_number));
--   
-- 
-- -- sales_accrual_1:  first step of deductions
-- drop table if exists sls.accrual_sales_1 cascade;
-- CREATE unlogged TABLE sls.accrual_sales_1 ( 
--       store citext not null,
--       employee_number citext not null,
--       total_pay numeric not null,
--       gross_distribution_percentage numeric not null,
--       gross_account citext not null,
--       fica_account citext not null,
--       medicare_account citext not null,
--       retirement_account citext not null,
--       fica_percentage numeric not null,
--       medicare_percentage numeric not null,
--       fica_exempt numeric not null,
--       medicare_exempt numeric not null,
--       fixed_deduction_amount numeric not null,
--       primary key(employee_number,gross_account));
-- 
-- -- sales_accrual_2:  retirement, gross
-- drop table if exists sls.accrual_sales_2 cascade;
-- create unlogged table sls.accrual_sales_2 (
-- 	category citext not null,
-- 	employee_number citext not null,
-- 	account citext not null,
-- 	amount numeric,
-- 	primary key(employee_number, category, account));
--
-- drop table if exists sls.accrual_dates cascade;
-- create unlogged table sls.accrual_dates (
-- 	journal citext not null,
-- 	the_date date primary key,
-- 	document citext not null,
-- 	reference citext not null,
-- 	description citext not null default 'Payroll Accrual');

-- drop table if exists sls.accrual_file_for_jeri cascade;
-- create unlogged table sls.accrual_file_for_jeri (
--   journal citext not null,
--   the_date date not null,
--   account citext not null,
--   control citext not null,
--   document citext not null,
--   reference citext not null,
--   amount numeric(8,2) not null,
--   description citext not null,
--   primary key(control,account));
-- 
-- drop table if exists sls.accrual_history cascade;
-- create table sls.accrual_history (
--   year_month integer not null,
--   account citext not null,
--   control citext not null,
--   amount numeric(8,2) not null,
--   primary key(year_month,account,control));
--
-- 07/01/21 suprise suprise, jeri sent me the sales management pay as well

  
select * from sls.accrual_sales where year_month = 202105;
  
delete from sls.accrual_sales where year_month = 202105;
/*
06/29/21
kim will be sending me spiffs on the 1st, don't yet know what that will look like.
will definitely need to put them in a table and add them to this query

-- need to add aubol

-- need to add accrual dates table

-- need to format like AccrualFileForJeri

-- need to include reversing entires

-- need to add to accrual_history: probably a separate pg table for just this stuff

-- need to add spiffs to accrual sales
*/   
!!! requirements:
			must run the aubol payrol 
			enter spiffs from kim
			both store consultant payrolls have been submitted
!!!

select * from sls.accrual_sales where year_month = 202105;
select * from sls.accrual_history where year_month = 202105;

delete from sls.accrual_history where year_month = 202106;
delete from sls.accrual_sales where year_month = 202106;


!!!!!!!! TODO !!!!!!!!!!!!!!!
the guarantee multiplier is wrong, does not calculate correctly
07/15/21 fixed in accraul - sales monthly_v2.sql
gonzalez and thompson

why does diff generate 2 rows for ry2

-- 07/01/21 sales mgmt
1. sales mgmt accrual_sales
select 'insert into sls.accrual_sales values ('|| 202106 || ',''' || pymast_company_number ||''','''||
  employee_first_name||''',''' ||employee_last_name|| ''',''' ||pymast_employee_number || ''','||0||');'
from arkona.ext_pymast
where employee_last_name in ('shirek','erickson','rydell',
	'wilkie','holland','dockendorf','michael','stout',
	'schumacher','longoria','haley','knudson')
and active_code <> 'T'	
order by employee_last_name, employee_first_name	

insert into sls.accrual_sales values (202106,'RY1','NATE','DOCKENDORF','133017',8500);
insert into sls.accrual_sales values (202106,'RY1','RONALD','ERICKSON','140500',6980);
insert into sls.accrual_sales values (202106,'RY2','DYLANGER','HALEY','263700',4587);
insert into sls.accrual_sales values (202106,'RY1','NIKOLAI','HOLLAND','111232',8500);
insert into sls.accrual_sales values (202106,'RY2','BENJAMIN','KNUDSON','279380',7000);
insert into sls.accrual_sales values (202106,'RY2','MICHAEL','LONGORIA','265328',7250);
insert into sls.accrual_sales values (202106,'RY1','ANTHONY','MICHAEL','195460',15500);
insert into sls.accrual_sales values (202106,'RY1','WESLEY','RYDELL','1120800',12000);
insert into sls.accrual_sales values (202106,'RY1','NICHOLAS','SHIREK','1126300',10000);
insert into sls.accrual_sales values (202106,'RY1','RICK','STOUT','1132700',11000);
insert into sls.accrual_sales values (202106,'RY1','DAVID','WILKIE','1149180',6300);
insert into sls.accrual_sales values (202106,'RY1','BRADLEY','SCHUMACHER','165470',8500);

2. sales consultants accrual_sales
do $$
declare
	_journal citext := 'GLI'; -- for sls.accrual_dates
	_the_date date := '06/30/2021'; -- for sls.accrual_dates & filter on sls.personnel
	_year_month integer := 202106;
	_payroll_year integer := 121;

begin	
	truncate sls.accrual_dates;
	insert into sls.accrual_dates
	select _journal, _the_date,
		_journal || lpad(extract(month from _the_date)::text, 2, '0') || lpad(extract(day from _the_date)::text, 2, '0') || right(extract(year from _the_date)::text, 2),
		_journal || lpad(extract(month from _the_date)::text, 2, '0') || lpad(extract(day from _the_date)::text, 2, '0') || right(extract(year from _the_date)::text, 2);

-- sales consultants			
	insert into sls.accrual_sales
	select _year_month, f.store_code as store, a.first_name, a.last_name as consultant, a.employee_number,   
			round( 
				case -- paid spiffs deducted only if base guarantee
					when a.total_earned >= a.guarantee then
						a.total_earned - coalesce(a.draw, 0) + coalesce(d.adjusted_amount, 0) + coalesce(e.additional_comp, 0)
					else
						(a.guarantee * coalesce(g.guarantee_multiplier, 1)) - coalesce(a.draw, 0) 
								- coalesce(c.spiffs, 0) + coalesce(e.additional_comp, 0) + coalesce(d.adjusted_amount, 0)
					end, 2) as "Total Month End Payout"      
	from sls.consultant_payroll a
	left join sls.pto_intervals b on a.employee_number = b.employee_number
		and a.year_month = b.year_month
	left join sls.paid_by_month c on a.employee_number = c.employee_number
		and a.year_month = c.year_month
	left join (
		select employee_number, sum(amount) as adjusted_amount
		from sls.payroll_adjustments
		where year_month =  _year_month
		group by employee_number) d on a.employee_number = d.employee_number
	left join (
		select employee_number, sum(amount) as additional_comp
		from sls.additional_comp
		where thru_date > (
			select first_of_month
			from sls.months
-- 			where open_closed = 'open') -- unlike the file for kim, this is being run AFTER payroll has been submitted, so, when run for June, July is the open month
			where year_month = _year_month)
		group by employee_number) e on a.employee_number = e.employee_number  
	left join sls.personnel f on a.employee_number = f.employee_number
	left join ( -- if this is the first month of employment, multiplier = days worked/days in month
		select a.employee_number, 
			round(extract(day from a.start_date)::numeric 
				/ (select max(day_of_month)::numeric from dds.dim_date where year_month = b.year_month), 2) as guarantee_multiplier
		from sls.personnel a
		inner join dds.dim_date b on a.start_date = b.the_date
			and b.year_month = _year_month) g on f.employee_number = g.employee_number        
	where a.year_month = _year_month;

-- tom aubol
	insert into sls.accrual_sales 
	select _year_month, 'RY1', 'Thomas', 'Aubol', '17534', 
		fi_gross + chargebacks - draw + pto_pay
	from sls.fi_manager_comp
	where year_month = _year_month;

end	$$;

3. add kims adjustments (spiffs, corrections, etc) to accrual_sales;

update sls.accrual_sales
set amount = amount + 600
-- select * from sls.accrual_sales
where last_name = 'olderbak'
  and year_month = '202106';

update sls.accrual_sales
set amount = amount + 250
-- select * from sls.accrual_sales
where last_name = 'longoria'
  and year_month = '202106';

update sls.accrual_sales
set amount = amount + 300
-- select * from sls.accrual_sales
where last_name = 'greer'
  and year_month = '202106';

update sls.accrual_sales
set amount = amount + 200
-- select * from sls.accrual_sales
where last_name = 'rumen'
  and year_month = '202106';  

update sls.accrual_sales  -- 5 days pto
set amount = 3156.40
-- select * from sls.accrual_sales
where last_name = 'loven'
  and year_month = '202106';  

update sls.accrual_sales  ----------- only shows 190 ???
set amount = 1923.10
-- select * from sls.accrual_sales
where last_name = 'halldorson'
  and year_month = '202106';    

update sls.accrual_sales  ----------- partial month, showed 2100 vision had 1038..60
set amount = 1038.6
-- select * from sls.accrual_sales
where last_name = 'gonzalez'
  and year_month = '202106';   

update sls.accrual_sales  ----------- partial month showed 1410 ?!? vision has 1730.7, nick should have adjusted that
set amount = 1038.60
-- select * from sls.accrual_sales
where last_name = 'thompson'
  and year_month = '202106';   
  

4. accrual_sales_1 & 2, accrual_file_for_jeri & accrual history
do $$
declare
	_journal citext := 'GLI'; -- for sls.accrual_dates
	_the_date date := '06/30/2021'; -- for sls.accrual_dates & filter on sls.personnel
	_year_month integer := 202106;
	_payroll_year integer := 121;

begin	

	truncate sls.accrual_sales_1;      
	insert into sls.accrual_sales_1
	select a.store, a.employee_number, 0 as TotalPay,
		c.GROSS_DIST, c.GROSS_EXPENSE_ACT_, 
		c.EMPLR_FICA_EXPENSE, c.EMPLR_MED_EXPENSE, c.EMPLR_CONTRIBUTIONS,
		e.fica_employer_percent, e.employer_medicare_, 
		coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
		coalesce(h.fixed_ded_amt, 0) AS fixed_ded_amt 
	from sls.accrual_sales a
	join arkona.ext_pymast b on a.employee_number = b.pymast_employee_number
	left join arkona.ext_pyactgr c on b.pymast_company_number = c.company_number
		and b.distrib_code = c.dist_code
	left join arkona.ext_pycntrl e on b.pymast_company_number = e.company_number  -- fica & medicare percentages for store/year 
		and e.payroll_year = _payroll_year
	left join ( -- FICA Exempt deductions, may be NULL, coalesce IN select
		select employee_number, sum(fixed_ded_amt) as FicaEx
		from arkona.ext_pydeduct x
		where exists (
			select 1
			from arkona.ext_pypcodes
			where ded_pay_code = x.ded_pay_code
				and exempt_2_fica_ = 'Y')
		 group by employee_number) f on a.employee_number = f.employee_number
	left join ( -- -- Medicare Exempt deductions, may be NULL, coalesce IN select
		select employee_number, sum(fixed_ded_amt) as MedEx
		from arkona.ext_pydeduct x
		where exists (
			select 1
			from arkona.ext_pypcodes
			where ded_pay_code = x.ded_pay_code
				and exempt_6_medc_tax_ = 'Y')
		 group by employee_number) g on a.employee_number = g.employee_number	 
	left join arkona.ext_pydeduct h on  b.pymast_company_number = h.company_number -- retirement deductions
		and a.employee_number = h.employee_number
		and h.ded_pay_code in ('91','91b','91c','99','99a','99b','99c')
	where a.year_month = _year_month;

select * from sls.accrual_sales_1 order by employee_number


	TRUNCATE sls.accrual_sales_2;
	insert into sls.accrual_sales_2
	select 'gross', employee_number, gross_account as account,
		round(gross_distribution_percentage/100.0 * amount, 2) as amount	
	from (
		select a.store, a.last_name, a.amount, c.*
		from sls.accrual_sales a
		left join sls.accrual_sales_1 c on a.employee_number = c.employee_number
		where a.year_month = _year_month) aa
		
	union all -- the fica fix was to replac Total_Pay with amount
	select 'fica', employee_number, fica_account as account,
		 sum(round((Gross_Distribution_Percentage/100.0) * (/*Total_Pay*/ amount - (coalesce(Fica_Exempt, 0))) * Fica_Percentage/100.0, 2)) AS Amount 
	from (
		select a.store, a.last_name, a.amount, c.*
		from sls.accrual_sales a
		left join sls.accrual_sales_1 c on a.employee_number = c.employee_number
		where a.year_month = _year_month) bb	
	group by last_name, employee_number, fica_account
	
	union all 
	select 'medic', employee_number, medicare_account as account,
		 sum(round((Gross_Distribution_Percentage/100.0) * (/*Total_Pay*/ amount - (coalesce(medicare_Exempt, 0))) * medicare_Percentage/100.0, 2)) AS Amount 
	from (
		select a.store, a.last_name, a.amount, c.*
		from sls.accrual_sales a
		left join sls.accrual_sales_1 c on a.employee_number = c.employee_number
		where a.year_month = _year_month) bb	
	group by employee_number, medicare_account

	union all
	SELECT 'retire', Employee_Number, Retirement_Account AS Account,
	sum(CASE 
		WHEN Fixed_Deduction_Amount IS NULL THEN 0
		ELSE 
		CASE 
			WHEN (amount * Fixed_Deduction_Amount/200.0) < .02 * amount 
				THEN round((Gross_Distribution_Percentage/100.0 * amount) * (Fixed_Deduction_Amount/200.0), 2)
			ELSE round((Gross_Distribution_Percentage/100.0) * amount * .02, 2) 
		END               
	END) AS Amount  
		from (
			select a.store, a.last_name, a.amount, c.*
			from sls.accrual_sales a
			left join sls.accrual_sales_1 c on a.employee_number = c.employee_number
			where a.year_month = _year_month) bb	
		group by employee_number, Retirement_Account;

	truncate sls.accrual_file_for_jeri cascade;
	insert into sls.accrual_file_for_jeri
	-- accrual file for jeri, base entries
	select b.journal, b.the_date, a.account, a.employee_number as control, 
		b.document, b.reference, sum(a.amount) as amount, b.description
	from sls.accrual_sales_2 a, sls.accrual_dates b
	where amount <> 0
	group by b.journal, b.the_date, a.account, a.employee_number, 
		b.document, b.reference, b.description

		
	union 
	-- accrual file for jeri, reversing entries
	SELECT e.journal, e.the_date, d.Account, e.document, e.document, e.reference, 
		round(d.Amount, 2), e.description
	from (  
		select 
			case left(account, 1)
				when '1' then '132101'
				when '2' then '232103'
			end as account,
			sum(amount) * -1 as amount
		from (
			select employee_number, account, sum(amount) as amount
			from sls.accrual_sales_2
			group by employee_number, account) c
		group by 
			case left(account, 1)
				when '1' then '132101'
				when '2' then '232103'
			end) d, sls.accrual_dates e;
-- 
-- 	-- delete from sls.accrual_history where year_month = 202106;

	insert into sls.accrual_history
	select _year_month, account, control, amount
	from sls.accrual_file_for_jeri;
	
end	$$;

select * from sls.accrual_sales_2
select * from sls.accrual_file_for_jeri 
select * from sls.accrual_history where year_month = 202105

-- build out all the amounts and compare to actual paycheck
do $$
declare
	_year_month integer := 202106;
	_payroll_year integer := 121;
	_check_year integer := 21;
	_check_month integer := 7;
	_check_day integer :=  2;
begin	
drop table if exists diffs;
create temp table diffs as
select aaa.*, bbb.total_gross_pay, bbb.curr_emplr_fica, bbb.curr_employer_medicare, bbb.emplr_curr_ret ,
  aaa.gross - bbb.total_gross_pay as gross_dif,
  aaa.fica - bbb.curr_emplr_fica as fica_diff,
  aaa.medic - bbb.curr_employer_medicare as medic_diff,
  aaa.retire - bbb.emplr_curr_ret as retire_diff
from (
	select aa.employee_name, aa.pymast_employee_number, bb.amount as gross, cc.amount as fica, dd.amount as medic, ee.amount as retire
	from arkona.ext_pymast aa
	join (
		select 'gross' as category, a.store, a.last_name, a.employee_number, a.amount
		from sls.accrual_sales a
		where a.year_month = 202106) bb on aa.pymast_employee_number = bb.employee_number 
	left join (
		select 'fica' as category, a.employee_number, 
			sum(round((Gross_Distribution_Percentage/100.0) * (a.amount - (coalesce(Fica_Exempt, 0))) * Fica_Percentage/100.0, 2)) AS Amount 
		from sls.accrual_sales a
		left join sls.accrual_sales_1 c on a.employee_number = c.employee_number
		where a.year_month = _year_month
		group by a.employee_number) cc on aa.pymast_employee_number = cc.employee_number 
	left join (
		select 'medic' as category, a.employee_number, 
			sum(round((Gross_Distribution_Percentage/100.0) * (a.amount - (coalesce(medicare_Exempt, 0))) * medicare_Percentage/100.0, 2)) AS Amount 
		from sls.accrual_sales a
		left join sls.accrual_sales_1 c on a.employee_number = c.employee_number
		where a.year_month = _year_month
		group by a.employee_number) dd on aa.pymast_employee_number = dd.employee_number	
	left join (
		select 'retire' as category, a.employee_number, 
			sum(round(case
				when c.fixed_deduction_amount = 0 then 0
				else
					case
						when (a.amount * c.fixed_deduction_amount/200.0) < .02 * a.amount then
							(gross_distribution_percentage/100.0) * a.amount * (fixed_deduction_amount/200.0)
						else (gross_distribution_percentage/100.0) * a.amount * .02
					end
			end, 2)) as amount
		from sls.accrual_sales a
		left join sls.accrual_sales_1 c on a.employee_number = c.employee_number
		where a.year_month = _year_month
		group by a.last_name, a.employee_number) ee on aa.pymast_employee_number = ee.employee_number) aaa	
left join arkona.ext_pyhshdta bbb on aaa.pymast_employee_number = bbb.employee_
  and  bbb.check_year = _check_year and bbb.check_month = _check_month and bbb.check_day = _check_day
  and bbb.seq_void <> '0J'
order by left(aaa.pymast_employee_number, 1), aaa.employee_name;
end $$;
select * from diffs;








  