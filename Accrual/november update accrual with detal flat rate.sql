SELECT *
FROM AccrualFileForJeri
WHERE control in (
  SELECT distinct employeenumber
  FROM dimTech 
  WHERE technumber IN ('d07','d27','d32','d19',
    'd02','d26','d08','d23','d11','d18','d21',
    'd35','d30','d25','d12'))

-- DROP TABLE #wtf;
SELECT control, gross, deducts12524, deducts12924, 
  deducts12524/gross AS deduct12524perc,
  iif(deducts12924 <> 0, deducts12924/gross, 0) AS deduct12924perc,
  0000.00 AS newgross, 0000.00 AS new12524, 0000.00 AS new12924
INTO #wtf 
FROM (  
  SELECT control,
    sum(CASE WHEN account = '124724' THEN amount END) AS gross,
    SUM(CASE WHEN account = '12524' THEN amount END) AS deducts12524,
    coalesce(SUM(CASE WHEN account = '12924' THEN amount END), 0) AS deducts12924
  FROM AccrualFileForJeri
  WHERE control in (
    SELECT distinct employeenumber
    FROM dimTech 
    WHERE technumber IN ('d07','d27','d32','d19',
      'd02','d26','d08','d23','d11','d18','d21',
      'd35','d30','d25','d12'))
  GROUP BY control) x;    
  
SELECT TRIM(firstname) + ' ' + lastname, a.* 
FROM #wtf a
left join edwEmployeeDim b on a.control = b.employeenumber
  AND b.active = 'active'
  AND b.currentrow = true
ORDER BY TRIM(firstname) + ' ' + lastname;
 
UPDATE #wtf SET newgross = 1857.92 WHERE control = '1105221'; 
UPDATE #wtf SET newgross = 477.60 WHERE control = '1109820';  
UPDATE #wtf SET newgross = 708.00 WHERE control = '1114820';  
UPDATE #wtf SET newgross = 69.70 WHERE control = '1116800';  
UPDATE #wtf SET newgross = 1237.28 WHERE control = '1122352';  
UPDATE #wtf SET newgross = 591.60 WHERE control = '1130500';  
UPDATE #wtf SET newgross = 842.40 WHERE control = '123150';  
UPDATE #wtf SET newgross = 372.60 WHERE control = '127350';  
UPDATE #wtf SET newgross = 115.80 WHERE control = '150120';  
UPDATE #wtf SET newgross = 540.00 WHERE control = '161092';  
UPDATE #wtf SET newgross = 1210.95 WHERE control = '164335';  
UPDATE #wtf SET newgross = 980.00 WHERE control = '164920';  
UPDATE #wtf SET newgross = 732.00 WHERE control = '184100';  
UPDATE #wtf SET newgross = 1611.20 WHERE control = '190600';  
UPDATE #wtf SET newgross = 942.50 WHERE control = '193180';   

UPDATE #wtf
SET new12524 = round(newgross*deduct12524perc,2),
    new12924 = round(newgross*deduct12924perc,2);
    
SELECT * FROM #wtf;    
    
UPDATE AccrualFileForJeri
SET amount = x.newgross
FROM #wtf x
WHERE AccrualFileForJeri.control = x.control
  AND AccrualFileForJeri.account = '124724';
  
UPDATE AccrualFileForJeri
SET amount = x.new12524
FROM #wtf x
WHERE AccrualFileForJeri.control = x.control
  AND AccrualFileForJeri.account = '12524';  
  
UPDATE AccrualFileForJeri
SET amount = x.new12924
FROM #wtf x
WHERE AccrualFileForJeri.control = x.control
  AND AccrualFileForJeri.account = '12924';    
  
SELECT a.*, b.control, b.account, b.amount
FROM #wtf a
LEFT JOIN AccrualFileForJeri b on a.control = b.control
ORDER BY b.control, b.account;  