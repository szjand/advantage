DECLARE @date date; // last day of the month for which payroll IS being accrued;
DECLARE @FromDate date; // beginning date for the interval to be accrued
DECLARE @ThruDate date; // ending date for the interval to be accrued
DECLARE @NumDays integer;
-- '01/29/2012' AND '02/11/2012'
@FromDate = '02/12/2012';
@ThruDate = '02/29/2012';
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1;
--SELECT @NumDays FROM system.iota;
--SELECT * FROM #wtf1
SELECT a.*, b.regularhours, b.overtimehours, b.vacationhours, b.ptohours, b.holidayhours,
  p.ytadic, p.ytaseq, p.ytagpp, p.ytagpa, p.ytaopa, p.ytavac, p.ytahol, p.ytasck,
  p.ytaeta, p.ytamed, p.ytacon,
  pc.yficmp, pc.ymedmp, fe.FicaEx, me.MedEx, d.yddamt 
-- DROP TABLE #wtf1
--INTO #wtf1
FROM ( -- a: 1 row per store/emp#  emp IS payed bi-weekly, dist code NOT IN list, 
       -- the empkey that was valid ON last payroll ending date
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, pydeptcodedesc, 
    PayPeriodCode, DistCode, salary, hourlyrate
  FROM edwEmployeeDim ex
  WHERE EmployeeKeyFromDate = (
      SELECT MAX(EmployeekeyFromDate)
      FROM edwEmployeeDim
      WHERE EmployeeNumber = ex.Employeenumber
        AND EmployeeKeyFromDate <= @ThruDate
      GROUP BY employeenumber)
    AND PayPeriodCode = 'B'
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')) a  
LEFT JOIN ( -- clock hours for the interval
    SELECT storecode, employeenumber, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
      sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
      sum(holidayhours) AS holidayhours
    FROM day d
    INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
    LEFT JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
    WHERE thedate BETWEEN @FromDate AND @ThruDate
    GROUP BY storecode, employeenumber) b ON a.storecode = b.storecode 
  AND a.employeenumber = b.employeenumber 
LEFT JOIN stgArkonaPYACTGR p ON a.StoreCode = p.ytaco# 
  AND a.DistCode = p.ytadic
  AND CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL pc ON a.storecode = pc.yco#
  AND pc.ycyy = 112 -- 100 + (year(@ThruDate) - 2000) 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS FicaEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex2 = 'Y')
  GROUP BY ydempn) fe ON a.employeenumber = fe.ydempn  
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS MedEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex6 = 'Y')
  GROUP BY ydempn) me ON a.employeenumber = me.ydempn    
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT d ON a.storecode = d.ydco#
  AND a.employeenumber = d.ydempn 
  AND d.yddcde IN ('91', '99') 
WHERE b.storecode IS NOT NULL
--  AND regularhours + overtimehours <> 0;
  AND pydeptcodedesc LIKE 'part%'
  AND a.storecode = 'ry1'
ORDER BY name 
  
  
  
/*  
SELECT storecode AS store, employeenumber, name, pydeptcodedesc AS dept, distcode
FROM edwEmployeeDim 
WHERE payrollclasscode = 'C'
 AND activecode = 'A'
ORDER BY storecode, name 
*/
  