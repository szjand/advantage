/*
Trevor Hahn's 168715, salary was wrong IN dealertrack, it was 60,000
Jeri wanted me to just send his info
AND that's what this script does, generate an accrual file for just him

*/

SELECT * FROM edwEmployeeDim WHERE employeenumber = '16425'
UPDATE edwEmployeeDim 
SET salary = 2307.7, hourlyrate = 28.85
WHERE employeenumber = '168715'

-- accrualsalaried1
INSERT INTO z_trevor_1
SELECT a.storecode, a.employeenumber, 
  a.salary * aa.NumDays14 AS TotalPay,
  d.GROSS_DIST, d.GROSS_EXPENSE_ACT_, 
  d.EMPLR_FICA_EXPENSE, d.EMPLR_MED_EXPENSE, d.EMPLR_CONTRIBUTIONS,
  e.yficmp, e.ymedmp, coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
  coalesce(h.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
 
FROM edwEmployeeDim a
INNER JOIN AccrualDates aa on 1 = 1
INNER JOIN ( -- the possibly dodgy assumption here IS that employeekeys are sequential, but they are autoinc 
  SELECT a.Employeenumber, MAX(a.employeekey) AS employeekey
  FROM edwEmployeeDim a
  INNER JOIN AccrualDates aa on 1 = 1
  INNER JOIN edwEmployeeDim b on a.employeekey =  b.employeekey -- ALL employeekeys valid for the interval
    AND b.employeekeyFromDate < aa.thrudate
    AND b.employeekeyThruDate > aa.fromdate
  WHERE a.hiredate <= aa.thrudate -- only folks active during the interval
    AND a.termdate >= aa.fromdate
    AND a.storecode <> 'ry3'  
-- *72*    
    AND a.employeenumber NOT IN ('190915','1160100','118026')
    AND a.PayPeriodCode = 'B'
-- *jg*    
--    AND a.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 
--     /*'BSM',*/'SALE','TEAM','USCM','USCD', 'STEV', 'AFTE','BEN') 
-- *dh*	  
   AND ((
   	   a.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 
      'SALE','TEAM','USCM','USCD', 'STEV', 'AFTE','BEN'))
	  OR (
	      a.DistCode = 'SALE' AND a.employeenumber = '148080'))   	   
    AND a.PayRollClassCode = 'S' -- salaried only
  GROUP BY a.employeenumber) x on a.employeekey = x.employeekey
LEFT JOIN zFixPyactgr d on a.storecode = d.company_number
  AND a.distcode = d.dist_code  
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL e ON a.storecode = e.yco#
-- *x*  
--  AND e.ycyy = 100 + (year(aa.ThruDate) - 2000) -- 112 -
  AND e.ycyy = 117 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) f ON a.employeenumber = f.EMPLOYEE_NUMBER    
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) g ON a.employeenumber = g.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT h ON a.storecode = h.COMPANY_NUMBER
  AND a.employeenumber = h.EMPLOYEE_NUMBER 
  AND h.DED_PAY_CODE IN ('91', '99')
WHERE a.employeenumber = '168715'   
 
-- accrualsalaried2
INSERT INTO z_trevor_2
SELECT 'Gross', EmployeeNumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * TotalPay AS Amount
FROM z_trevor_1
UNION ALL 
SELECT 'FICA', EmployeeNumber, FicaAccount AS Account,
  round((GrossDistributionPercentage/100.0) * (TotalPay - (coalesce(FicaExempt, 0)) * b.NumDays14) * FicaPercentage/100.0, 2) AS Amount
FROM z_trevor_1 a
INNER JOIN AccrualDates b on 1 = 1
UNION ALL 
SELECT 'MEDIC', EmployeeNumber, MedicareAccount AS Account,
--  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * (NumDays/14)) * MedicarePercentage/100.0, 2) AS Amount
  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * b.NumDays14) * MedicarePercentage/100.0, 2) AS Amount
FROM z_trevor_1 a
INNER JOIN AccrualDates b on 1 = 1
UNION ALL 
SELECT 'RETIRE', EmployeeNumber, RetirementAccount AS Account,
CASE 
  WHEN FixedDeductionAmount IS NULL THEN 0
  ELSE 
  CASE 
    WHEN (TotalPay * FixedDeductionAmount/200.0) < .02 * TotalPay 
      THEN round((GrossDistributionPercentage/100.0 * TotalPay) * (FixedDeductionAmount/200.0), 2)
    ELSE round((GrossDistributionPercentage/100.0) * TotalPay * .02, 2) 
  END               
END AS Amount  
FROM z_trevor_1;



INSERT INTO ztrevor_AccrualFileForJeri
SELECT journal, thedate, account, employeenumber AS control, document, reference, 
  round(cast(a.amount AS sql_double), 2) AS amount, description 
FROM ( 
  SELECT employeenumber, account, SUM(amount) AS amount
  FROM ( 
    SELECT * FROM z_trevor_2 WHERE amount <> 0)  a

  GROUP BY employeenumber, account) a, AccrualDates b;
  
  
-- reversing entries
INSERT INTO ztrevor_AccrualFileForJeri
SELECT e.journal, e.thedate, d.Account, e.document, e.document, e.reference, 
  round(d.Amount, 2), e.description
FROM (  
  SELECT 
    CASE LEFT(Account, 1)
      WHEN '1' THEN '132101'
      WHEN '2' THEN '232103'
    END AS Account, 
  SUM(amount) * -1 AS Amount
  FROM (
    SELECT employeenumber, account, SUM(amount) AS amount
    FROM ( 
      SELECT * FROM z_trevor_2 WHERE amount <> 0) a         
    
    GROUP BY employeenumber, account) c	
  GROUP BY LEFT(Account, 1)) d, AccrualDates e;
  
  
SELECT * FROM ztrevor_AccrualFileForJeri  