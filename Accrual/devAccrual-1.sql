-- 1. 30459 shows IN result SET AND should NOT
-- 2. 0 amounts empl# 1106360
-- 2/2 from jeri: The following dist codes are for semi-monthly:  BSM, SALE, TEAM, USCM, USCD 
-- issue why does Tyler Dahl emp# 30459 empkey 1289 hired 1/2/08 term 6/30/09 show up
-- AND 0 amounts: removed be exempting jeri's list of dist code, 
-- 2/3/12
--hmm, which are semi-monthly, which IS something i know abt an employee, but WHERE
-- what get's accrued AND what doesn't
-- 2/6/12
-- per jeri Exclude ALL who are payed semi-monthly, which IS an attribute of an employee, need to ADD ympper to empDim
-- PayPeriodCode: B OR S
-- PayPeriodCodeDesc: Bi-Weekly, Semi-Monthly

/*  30459 problem - employed for the interval ***********/ 
select EmployeeNumber, Name, DistCode, 
  Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) as GrossPay,
  Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.062 as FICA,
  Salary * (timestampdiff(sql_tsi_day, '1/15/2012', '1/31/2012') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.0145 as Medicare
-- SELECT COUNT(*)  
from edwEmployeeDim 
where StoreCodeDesc = 'Rydell' 
  and PayrollClassCodeDesc = 'Salary' 
  AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
  AND employeekeyfromdate <= '01/31/2012' 
  AND employeekeythrudate >= '01/15/2012' 
  AND ActiveCodeDesc = 'Active'
  AND employeenumber = '1106360'
--  and employeenumber = '30459'
/*  30459 problem - employed for the interval ***********/ 

-- WHERE are the rules, what's the connection BETWEEN dist codes AND pay periods?

SELECT DISTINCT ymco#, ym
FROM stgArkonaPYMAST

SELECT ymempn, ymname, ymclas, ympper

FROM stgArkonaPYMAST

SELECT MAX(ymco#), MAX(ymempn), MAX(ymname), ymclas, ympper
FROM stgArkonaPYMAST
WHERE ymactv <> 'T'
GROUP BY ymclas, ympper

SELECT COUNT(*), ymclas, ympper
FROM stgArkonaPYMAST
WHERE ymactv <> 'T'
GROUP BY ymclas, ympper

-- nah what i want IS dist code AND pay period, that was payroll class (H, S, C)

SELECT COUNT(*), ymco#, ymdist, ympper
FROM stgArkonaPYMAST
WHERE ymactv <> 'T'
GROUP BY ymco#, ymdist, ympper

SELECT ymco#, ymdist
FROM (
SELECT COUNT(*), ymco#, ymdist, ympper
FROM stgArkonaPYMAST
WHERE ymactv <> 'T'
GROUP BY ymco#, ymdist, ympper)x
GROUP BY ymco#, ymdist 
  HAVING COUNT(*) > 1
  
SELECT DISTINCT ympper FROM stgArkonaPYMAST -- shit, only 2: B & S

so with only 2 pay periods, IS it AS simple AS only those  
-- 2 dups  bsm, team
SELECT ymname, ymactv, ymdist, ympper
FROM stgarkonapymast
WHERE ymco# = 'RY1'
  AND ymdist IN ('BSM','TEAM')  

period of time: ?  
day past last paycheck through EOM
ooh, does pyhshdta tell me the pay period? 
first things first, get some normal christian dates IN stgArkonaPYHSHDTA


-- 2/5
look at pay period ending dates (B & S) for 
SELECT payrollendingdate, yhpper
FROM stgArkonaPYHSHDTA
GROUP BY payrollendingdate, yhpper

SELECT distinct payrollendingdate
FROM stgArkonaPYHSHDTA
WHERE yhpper = 'B' -- bi-weekly
  AND yhclas = 'H' -- hourly
  AND ymdist NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')

they are NOT every 2 weeks: eg; 12/3/11 12/9/11/ 12/17/11 12/23/11
payroll ending year/month/day:  yhdeyy/yhdemm/yhdedd
-- 2/6 
-- FROM the accrual exercise for jan 2023, need the last bi-weekly payroll ending date
-- why isn't it the 23rd instead of the 14th
-- the 23rd was for one employee, i'm guessing a term's last check
-- what's different about ypbnum 123000 compared to ypbnum 120000 that tells me
-- an obvious hack would be COUNT(*) grouped ON pybnum
-- looks LIKE COUNT IS the way to go
SELECT *
FROM stgArkonaPYHSHDTA
WHERE payrollendingdate IN ( '01/23/2012', '01/14/2012')
ORDER BY payrollendingdate DESC

SELECT *
FROM vPYHSHDTA
WHERE payrollendingdate IN ( '01/23/2012', '01/14/2012')
ORDER BY payrollendingdate DESC

-- hmm IS it always just 1
-- fuck, ususally
SELECT ypbnum, payrollendingdate, COUNT(*)
FROM vpyhshdta
WHERE payperiod = 'B'
  AND payrollclass = 'H' 
  AND ymdist NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD') 
GROUP BY ypbnum, payrollendingdate
  HAVING COUNT(*) > 3
ORDER BY payrollendingdate desc  

-- this IS it: the accrual intervals
SELECT y.thedate AS FromDate, d.thedate AS ThruDate 
FROM (
  SELECT timestampadd(sql_tsi_day, 1, MAX(payrollendingdate)) AS thedate
  FROM (
    SELECT ypbnum, payrollendingdate, COUNT(*)
    FROM vpyhshdta
    WHERE payperiod = 'B'
      AND payrollclass = 'H' 
      AND ymdist NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD') 
    GROUP BY ypbnum, payrollendingdate
      HAVING COUNT(*) > 3) x -- eliminates term last paychecks
  GROUP BY year(payrollendingdate), month(payrollendingdate)) y 
LEFT JOIN day d ON year(y.thedate) = year(d.thedate)
  AND month(y.thedate) = month(d.thedate)
  AND d.LastDayofMonth = true

-- what about salaried

SELECT payrollendingdate, payperiod, COUNT(*)
FROM vpyhshdta
  WHERE payperiod = 's'
group by payrollendingdate, payperiod
ORDER BY payrollendingdate DESC

    SELECT ypbnum, payrollendingdate, COUNT(*)
    FROM vpyhshdta
    WHERE payperiod = 'S'
      AND payrollclass = 'S' 
      AND ymdist NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD') 
    GROUP BY ypbnum, payrollendingdate
ORDER BY payrollendingdate desc    

SELECT PayPeriod, PayrollClass, ymdist, COUNT(*)
FROM vPYHSHDTA
WHERE yhdco# = 'RY1'
  AND PayPeriod = 'B'
  AND PayrollClass = 'H'
GROUP BY PayPeriod, PayrollClass, ymdist
ORDER BY ymdist

SELECT *
FROM stgArkonaPYHSHDTA
WHERE TRIM(yhpper) = ''

-- these are the folks being excluded BY dist code
SELECT DISTINCT ymname, ymdist, PayPeriod, PayrollClass
FROM vPYHSHDTA
WHERE payperiod = 'B'
--  AND payrollclass = 'H'
  AND ymdist in ( -- these are hourly employees payed bi-weekly that should NOT be accrued?
    '22', -- hobby shop
    '29', -- cartiva
    'BSM', -- randy & chris
    'SALE') -- sales consultants
ORDER BY ymdist, ymname


/******* 2/7/12 reworking accrual query ****************************************/

SELECT e.employeekey, e.storecode, e.employeenumber, e.name, e.distcode, e.payrollclasscode,
  hourlyrate, salary, e.EmployeeKeyFromDate, e.EmployeeKeyThruDate, h.* 
FROM edwEmployeeDim e
INNER JOIN (
  SELECT y.thedate AS FromDate, d.thedate AS ThruDate, timestampdiff(sql_tsi_day, y.thedate, d.thedate) + 1 AS NumDays
  FROM (  
    SELECT cast(timestampadd(sql_tsi_day, 1, MAX(payrollendingdate)) AS sql_date) AS thedate
    FROM (
      SELECT payrollendingdate, COUNT(*)
      FROM vpyhshdta
      WHERE payperiod = 'B'
      GROUP BY payrollendingdate
        HAVING COUNT(*) > 3) x
    GROUP BY year(payrollendingdate), month(payrollendingdate)) y  
  LEFT JOIN day d ON year(y.thedate) = year(d.thedate)
    AND month(y.thedate) = month(d.thedate)
    AND d.LastDayofMonth = true
  WHERE d.thedate = '12/31/2011') h ON e.EmployeeKeyFromDate <= h.ThruDate AND employeekeythrudate >= h.FromDate
WHERE e.payperiodcode = 'B'  
  AND e.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD') 


/*/**** 2/7/12 reworking accrual query ****************************************/

/*  construct a manageable view FROM physhdta */
drop VIEW 
   vPYHSHDTA
AS 
   select yhdco#, ypbnum, ymname, yhdemp, ymdept, ymdist, yhpper as PayPeriod, 
  yhclas as PayrollClass, PayrollEndingDate, 
  yhsaly as Salary, yhrate as HourlyRate, yhdbsp AS BasePay, yhdtgp AS TotalGrossPay, 
  yhcfed AS FedTax, yhcsse AS FICA, yhcssm AS EmployerFICA, yhcmde AS Medicare,
  yhcmdm AS EmployerMedicare, yhcst AS StateTax, yhcfuc AS FUTA, yhcsuc AS SUTA,
  yhdhrs AS RegHours, yhdoth AS OTHours
FROM stgArkonaPYHSHDTA;
/*/* construct a manageable view FROM physhdta */ 
  
/*** voids *******/
SELECT COUNT(*), yhvoid FROM stgArkonaPYHSHDTA GROUP BY yhvoid
-- shit didn't take INTO account voids
SELECT yhdco#, payrollendingdate, COUNT(*) FROM stgArkonaPYHSHDTA WHERE yhvoid = 'V' GROUP BY  yhdco#, payrollendingdate
DELETE FROM stgArkonaPYHSHDTA WHERE yhvoid = 'V'
/*/***  voids *******/

-- emp# fix & excl jeri's dist codes  
-- the original version FROM greg 
-- 2/23, it certainly IS fast, NOT confident of the amounts, need to accumulate at a paycheck level
-- hmmmmmmmm
-- want to be able to compare the results to pyshsdta
select 'RST' as Journal, '1/31/2012' as [Date], Account, EmployeeNumber as Control, 
  'RST-1/31/12' as Document, 'RST-01/31/12' as Reference, sum(Amount) as Amount, 
  'Payroll Accrual' as Description
-- DROP TABLE #wtf  
INTO #wtff  
FROM ( -- x
  select EmployeeNumber, GrossAccount as Account, Gross as Amount
  FROM ( -- pay LEFT JOIN PYACTGR AS a
    SELECT EmployeeNumber, Name, DistCode, ytaco# as Company, ytagpa as GrossAccount, 
      ytaeta as FICAAccount, ytamed as MedicareAccount, 
      round(GrossPay * ytagpp / 100, 2) as Gross, round(FICA * ytagpp / 100, 2) as FICA, 
      round(Medicare * ytagpp / 100, 2) as Medicare
    FROM (
      SELECT EmployeeNumber, Name, DistCode, 
        SUM(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) as GrossPay,
        SUM(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.062 as FICA,
        SUM(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.0145 as Medicare
      FROM edwClockHoursFact chf
      INNER JOIN edwEmployeeDim ed on ed.EmployeeKey = chf.EmployeeKey  
        AND StoreCodeDesc = 'Rydell' 
        AND PayrollClassCodeDesc = 'Hourly' 
        AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
      INNER JOIN Day on Day.DateKey = chf.DateKey 
        AND TheDate BETWEEN '12/18/2011' AND '12/31/2011'
      GROUP BY EmployeeNumber, Name, DistCode
        HAVING SUM(ClockHours) > 0
      UNION ALL 
      select EmployeeNumber, Name, DistCode, 
        Salary * (timestampdiff(sql_tsi_day, '12/18/2011', '12/31/2011') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) as GrossPay,
        Salary * (timestampdiff(sql_tsi_day, '12/18/2011', '12/31/2011') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.062 as FICA,
        Salary * (timestampdiff(sql_tsi_day, '12/18/2011', '12/31/2011') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.0145 as Medicare
      from edwEmployeeDim 
      where StoreCodeDesc = 'Rydell' 
        and PayrollClassCodeDesc = 'Salary' 
        AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
        AND employeekeyfromdate <= '12/31/2011'
        AND employeekeythrudate >= '12/18/2011'
        AND ActiveCodeDesc = 'Active') pay
  LEFT JOIN stgArkonaPYACTGR acct on acct.ytadic = pay.DistCode 
    and acct.ytaco# = 'RY1' 
    and ytadic not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')) a
  union all
  select EmployeeNumber, FICAAccount, FICA
  FROM ( -- pay LEFT JOIN  PYACTGR AS b
    select EmployeeNumber, Name, DistCode, ytaco# as Company, ytagpa as GrossAccount, 
      ytaeta as FICAAccount, ytamed as MedicareAccount, 
      round(GrossPay * ytagpp / 100, 2) as Gross, round(FICA * ytagpp / 100, 2) as FICA, 
      round(Medicare * ytagpp / 100, 2) as Medicare
    FROM ( -- pay
      select EmployeeNumber, Name, DistCode, 
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) as GrossPay,
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.062 as FICA,
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.0145 as Medicare
      from edwClockHoursFact chf
      inner join edwEmployeeDim ed on ed.EmployeeKey = chf.EmployeeKey 
        and StoreCodeDesc = 'Rydell' 
        and PayrollClassCodeDesc = 'Hourly' 
        and DistCode not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
      inner join Day on Day.DateKey = chf.DateKey 
        AND TheDate BETWEEN '12/18/2011' AND '12/31/2011'
      group by EmployeeNumber, Name, DistCode
        having sum(ClockHours) > 0
      union all
      select EmployeeNumber, Name, DistCode, 
        Salary * (timestampdiff(sql_tsi_day, '12/18/2011', '12/31/2011') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) as GrossPay,
        Salary * (timestampdiff(sql_tsi_day, '12/18/2011', '12/31/2011') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.062 as FICA,
        Salary * (timestampdiff(sql_tsi_day, '12/18/2011', '12/31/2011') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.0145 as Medicare
      from edwEmployeeDim 
      where StoreCodeDesc = 'Rydell' 
        and PayrollClassCodeDesc = 'Salary' 
        and DistCode not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
        AND employeekeyfromdate <= '12/31/2011'
        AND employeekeythrudate >= '12/18/2011'
        AND ActiveCodeDesc = 'Active') pay
  left join stgArkonaPYACTGR acct on acct.ytadic = pay.DistCode 
    and acct.ytaco# = 'RY1' 
    and ytadic not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')) b
  union all
  select EmployeeNumber, MedicareAccount, Medicare 
  FROM ( -- pay LEFT JOIN PYACTGR AS c
    select EmployeeNumber, Name, DistCode, ytaco# as Company, ytagpa as GrossAccount, 
      ytaeta as FICAAccount, ytamed as MedicareAccount, 
      round(GrossPay * ytagpp / 100, 2) as Gross, round(FICA * ytagpp / 100, 2) as FICA, 
      round(Medicare * ytagpp / 100, 2) as Medicare
    FROM ( -- pay
      select EmployeeNumber, Name, DistCode, 
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) as GrossPay,
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.062 as FICA,
        sum(((RegularHours + VacationHours + PTOHours + HolidayHours) * HourlyRate) + (OvertimeHours * HourlyRate * 1.5)) * 0.0145 as Medicare
      from edwClockHoursFact chf
      inner join edwEmployeeDim ed on ed.EmployeeKey = chf.EmployeeKey 
        and StoreCodeDesc = 'Rydell' 
        and PayrollClassCodeDesc = 'Hourly' 
        and DistCode not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
      inner join Day on Day.DateKey = chf.DateKey 
        AND TheDate BETWEEN '12/18/2011' AND '12/31/2011'
      group by EmployeeNumber, Name, DistCode
        having sum(ClockHours) > 0
      union all
      select EmployeeNumber, Name, DistCode, 
        Salary * (timestampdiff(sql_tsi_day, '12/18/2011', '12/31/2011') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) as GrossPay,
        Salary * (timestampdiff(sql_tsi_day, '12/18/2011', '12/31/2011') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.062 as FICA,
        Salary * (timestampdiff(sql_tsi_day, '12/18/2011', '12/31/2011') + 1) * 1.0 / (timestampdiff(sql_tsi_day, '1/1/2012', '1/31/2012') + 1) * 0.0145 as Medicare
      from edwEmployeeDim 
      where StoreCodeDesc = 'Rydell' 
        and PayrollClassCodeDesc = 'Salary' 
        and DistCode not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')
        AND employeekeyfromdate <= '12/31/2011'
        AND employeekeythrudate >= '12/18/2011'
        AND ActiveCodeDesc = 'Active') pay
    left join stgArkonaPYACTGR acct on acct.ytadic = pay.DistCode 
      and acct.ytaco# = 'RY1' 
      and ytadic not in ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD')) c) x
group by EmployeeNumber, Account
order by Account, EmployeeNumber
--ORDER BY Amount  
