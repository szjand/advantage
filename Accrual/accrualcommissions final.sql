/*
3/30/12
       added variables
       added DistCode STEV to exclusion
       reduced base Greg amount to acct 12101

DROP TABLE #wtf2;
DROP TABLE #jon2;       
DROP TABLE #entries;              
*/
DECLARE @Date string; // last day of the month for which payroll IS being accrued;
DECLARE @FromDate date; // beginning date for the interval to be accrued
DECLARE @ThruDate date; // ending date for the interval to be accrued
DECLARE @NumDays integer;

DECLARE @Journal string;
DECLARE @Doc string;
DECLARE @Ref string;
@Journal = 'GLI';
@Doc = 'GLI033112';
@Ref = 'GLI033112';
@Date = '03/31/2012';
-- Feb 2012
--@FromDate = '02/12/2012';
  @ThruDate = '02/29/2012';
--Mar 2012
@FromDate = '03/25/2012';
@ThruDate = '03/31/2012';
--@FromDate = '01/29/2012';
--@ThruDate = '01/31/2012';
--@FromDate = '02/01/2012';
--@ThruDate = '02/29/2012';
@NumDays = timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1;
--SELECT @NumDays FROM system.iota;
--SELECT * FROM #wtf2
SELECT a.*, ac.amount AS comm,
  p.ytadic, p.ytaseq, p.ytagpp, p.ytagpa, p.ytaopa, p.ytavac, p.ytahol, p.ytasck,
  p.ytaeta, p.ytamed, p.ytacon,
  pc.yficmp, pc.ymedmp, fe.FicaEx, me.MedEx, d.yddamt 
-- DROP TABLE #wtf2
INTO #wtf2
FROM ( -- a: 1 row per store/emp#  emp IS payed bi-weekly, dist code NOT IN list, 
       -- the empkey that was valid ON last payroll ending date
  SELECT storecode, employeenumber, name, employeekey, payrollclasscode, 
    PayPeriodCode, DistCode, salary, hourlyrate
  FROM edwEmployeeDim ex
  WHERE EmployeeKeyFromDate = (
      SELECT MAX(EmployeekeyFromDate)
      FROM edwEmployeeDim
      WHERE EmployeeNumber = ex.Employeenumber
        AND EmployeeKeyFromDate <= @ThruDate
      GROUP BY employeenumber)
    AND PayPeriodCode = 'B'
-- 3/30/12 added DistCode STEV per Jeri NOT dealership employees (Benjamin Marotte & Steve Zoellick') 
    AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 'BSM','SALE','TEAM','USCM','USCD', 'STEV')) a  
INNER JOIN AccrualCommissions ac ON a.storecode = ac.storecode AND a.employeekey = ac.employeekey   
LEFT JOIN ( -- clock hours for the interval
    SELECT storecode, employeenumber, sum(regularhours) AS regularhours, sum(overtimehours) AS overtimehours, 
      sum(vacationhours) AS vacationhours, sum(ptohours) AS ptohours, 
      sum(holidayhours) AS holidayhours
    FROM day d
    INNER JOIN edwClockHoursFact f ON d.datekey = f.datekey
    LEFT JOIN edwEmployeeDim e ON f.employeekey = e.employeekey
    WHERE thedate BETWEEN @FromDate AND @ThruDate
    GROUP BY storecode, employeenumber) b ON a.storecode = b.storecode 
  AND a.employeenumber = b.employeenumber 
LEFT JOIN stgArkonaPYACTGR p ON a.StoreCode = p.ytaco# 
  AND a.DistCode = p.ytadic
  AND CurrentRow = true
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL pc ON a.storecode = pc.yco#
  AND pc.ycyy = 112 -- 100 + (year(@ThruDate) - 2000) 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS FicaEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex2 = 'Y')
  GROUP BY ydempn) fe ON a.employeenumber = fe.ydempn  
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT ydempn, SUM(yddamt) AS MedEx
  FROM stgArkonaPYDEDUCT d
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = d.yddcde
    AND ytdex6 = 'Y')
  GROUP BY ydempn) me ON a.employeenumber = me.ydempn    
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT d ON a.storecode = d.ydco#
  AND a.employeenumber = d.ydempn 
  AND d.yddcde IN ('91', '99') 
WHERE b.storecode IS NOT NULL; 
--  AND regularhours + overtimehours <> 0;

select @Journal as Journal, @Date as [Date], Account, EmployeeNumber as Control, 
  @Doc as Document, @Ref as Reference, round(sum(Amount),2) as Amount, 
  'Payroll Accrual' as Description
-- DROP TABLE #jon2
INTO #jon2
FROM (
  SELECT 'Comm', employeenumber, ytagpa AS Account,
    ytagpp/100.0 * Comm AS Amount
  FROM #wtf2
  UNION ALL
  SELECT 'FICA', employeenumber, ytaeta AS Account,
    round(ytagpp/100.0 * Comm * yficmp/100.0, 2) AS Amount
  FROM #wtf2
  UNION ALL 
  SELECT 'MEDIC', employeenumber, ytamed AS Account,
    round(ytagpp/100.0 * Comm * ymedmp/100.0, 2) AS Amount
  FROM #wtf2  
  UNION ALL 
  SELECT 'RETIRE', employeenumber, ytacon AS Account,
  CASE 
    WHEN yddamt IS NULL THEN 0
    ELSE 
      ytagpp/100.0 *
        CASE 
          WHEN Comm * yddamt/200.0 < .02 * Comm
            THEN round(Comm * yddamt/200.0, 2)
          ELSE round(.02 * Comm, 2) 
        END 
    END AS Amount     
  FROM #wtf2 ) x
WHERE Amount <> 0
GROUP BY employeenumber, Account;  
-- greg
/**/

--INSERT INTO #jon2 values(@Journal, @Date, '12101', '1130426', @Doc, @Ref, 3415.40 * @NumDays/14, 'Payroll Accrual');
-- changed, per Jeri 3/30/12
INSERT INTO #jon2 values(@Journal, @Date, '12101', '1130426', @Doc, @Ref, 1415.40 * @NumDays/14, 'Payroll Accrual');
INSERT INTO #jon2 values(@Journal, @Date, '12102', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
INSERT INTO #jon2 values(@Journal, @Date, '12104', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
INSERT INTO #jon2 values(@Journal, @Date, '12105', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');
INSERT INTO #jon2 values(@Journal, @Date, '12106', '1130426', @Doc, @Ref, 1569.23 * @NumDays/14, 'Payroll Accrual');

/**/
-- output
SELECT journal, date, account, control, document, reference, SUM(amount) AS Amount, description
INTO #entries
FROM (
SELECT * FROM #jon1
UNION ALL 
SELECT * FROM #jon2) x
WHERE amount <> 0
GROUP BY journal, date, account, control, document, reference, description;

-- reversing entries
ALTER TABLE #entries
ALTER COLUMN control control CIChar(9);

INSERT INTO #entries
select @Journal as Journal, @Date as [Date], 
  case
    when LEFT(account, 1) = '1' then '132101'
    when left(Account, 1) = '2' then '232103'
    when left(Account, 1) = '3' then '332100'
  end  as Account, @Doc AS Control, 
  @Doc as Document, @Ref as Reference, round(sum(Amount),2)* -1 as Amount, 
  'Payroll Accrual' as Description
FROM #entries
GROUP BY LEFT(account, 1);

SELECT * FROM #entries;
 
/*
SELECT SUM(amount) FROM (
SELECT journal, date, account, control, document, reference, SUM(amount) AS Amount, description
FROM (
SELECT * FROM #jon1
UNION ALL 
SELECT * FROM #jon2) x
WHERE amount <> 0
 AND account = '12306' -- Parts Dept
GROUP BY journal, date, account, control, document, reference, description) x

SELECT COUNT(*)
FROM #jon2

SELECT * FROM #jon1 WHERE control = '164202'

SELECT COUNT(*) FROM #wtf2
SELECT * FROM #wtf2 WHERE employeenumber = '164202'

-- output
-- DROP TABLE #entries
SELECT journal, date, account, control, document, reference, SUM(amount) AS Amount, description
INTO #entries
FROM (
SELECT * FROM #jon1
UNION ALL 
SELECT * FROM #jon2) x
WHERE amount <> 0
GROUP BY journal, date, account, control, document, reference, description

-- reversing entries

SELECT * FROM #entries ORDER BY account DESC

SELECT 'GLI', LEFT(account, 1), SUM(Amount)
FROM #entries
GROUP BY LEFT(account, 1)

select 
  case
    when LEFT(account, 1) = '1' then '132101'
    when left(Account, 1) = '2' then '232103'
    when left(Account, 1) = '3' then '332100'
  end 
FROM #entries
GROUP BY LEFT(account, 1)

SELECT * FROM #entries

SELECT 
  CASE
    WHEN Account = 1 THEN '132101'
    WHEN Account = 
select LEFT(account, 1) as Account, round(sum(Amount),2) as Amount
FROM #entries
GROUP BY LEFT(account, 1);
  
*/
