
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @pdr_rate double;
DECLARE @metal_rate double;
DECLARE @hourly_rate double;
@pdr_rate = (SELECT pdrrate FROM scotest.bsFlatRateTechs WHERE thrudate > curdate());
@metal_rate = (SELECT metalrate FROM scotest.bsFlatRateTechs WHERE thrudate > curdate());
@hourly_rate = (SELECT otherrate FROM scotest.bsFlatRateTechs WHERE thrudate > curdate());
@from_date = '09/12/2021'; --------------------- change dates
@thru_date = '09/30/2021';

-- generate an INSERT stateement to put INTO AccrualFlatRateGross.sql 
DELETE FROM z_tmp_AccrualFlatRateGross;
INSERT INTO z_tmp_AccrualFlatRateGross
SELECT 'RY1' AS storecode,'1110425' AS employeenumber, 'Peterson' AS lastname, 'Brian'as firstname,
			  @from_date, @thru_date, pdrPay + metalPay + hourlyPay AS gross,'BTEC', hourlypay 
FROM ( 
  SELECT pdrRate, round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay,
    metalRate, 
    round(SUM(metalFlagHours), 2) AS metalFlagHours,
    round(metalRate * round(SUM(metalFlagHours), 2), 2) AS metalPay
  FROM (  
    SELECT theDate, @pdr_rate AS pdrRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      @metal_rate AS metalRate,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM factRepairOrder a
    INNER JOIN day b on a.closedatekey = b.datekey
    INNER JOIN dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.theDate BETWEEN @from_date AND @thru_date) m    
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT @hourly_rate AS hourlyRate, SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
    sum(holidayHours) AS holidayHours,
    @hourly_rate * (SUM(vacationHours) + SUM(ptoHours) + sum(holidayHours)) AS hourlyPay
  FROM edwClockHoursFact a
  INNER JOIN edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.theDate BETWEEN @from_date AND @thru_date) o  on 1 = 1; 	
		
		
SELECT 'insert into AccrualFlatRateGross values (' + '''' + storecode + '''' + ',' 
  + '''' + trim(employeenumber) + '''' + ',' + '''' + trim(lastname) + '''' + ',' + '''' + trim(firstname) + '''' + ',' 
	+ '''' + cast(fromdate AS sql_char) + '''' + ',' + '''' + cast(thrudate AS sql_char) + '''' + ','
	+ trim(cast(gross AS sql_char)) + ',' + '''' + trim(distcode) + '''' + ',' + trim(cast(ptopay AS sql_char)) + ');'
FROM z_tmp_AccrualFlatRateGross;


/*
-- ALL this below IS what i cobbled together on 9/1 to figure out what was happening

SELECT * FROM accrualflatrategross WHERE employeenumber = '145789'

  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN '08/15/2021' AND '08/31/2021'
    AND holiday = false
		
		
SELECT * FROM scotest.tpemployees WHERE lastname = 'peterson'  -- 1110425

SELECT * FROM accrual_history WHERE control = '1110425'
		
  SELECT storecode, employeenumber, lastname, firstname,
  -- *f*
  --  @days * max(round((commissionpay + ptopay)/10, 2)), distcode 
    12 * max(round(commissionpay/10, 2)) AS gross, distcode 
  FROM (    
    select aa.storecode, a.employeenumber, a.lastname, a.firstname, 
      round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2) AS commissionPay,
      round(techHourlyRate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd), 2) AS ptoPay,
      aa.distcode
    FROM scotest.tpdata a
    LEFT JOIN edwEmployeeDim aa on a.employeenumber = aa.employeenumber
      AND aa.currentrow = true
    INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey  
    WHERE thedate = payperiodend
		  -- AND a.lastname = 'peterson'
      AND payperiodseq IN ( -- last 3 full pay periods
        SELECT top 3 DISTINCT payperiodseq
        FROM scotest.tpdata
        WHERE payperiodend <= '08/31/2021'
        ORDER BY payperiodseq DESC)) x    
  WHERE EXISTS ( -- currently a team pay tech
    SELECT 1
    FROM scotest.tpdata
    WHERE employeenumber = x.employeenumber
      AND thedate = '08/31/2021')    
  OR (x.lastname = 'peterson' AND x.firstname = 'brian')      
  GROUP BY storecode, employeenumber, lastname, firstname, distcode
	ORDER BY lastname
	
this gives me AccrualFlatRateGross	
-- DROP TABLE #brian
DECLARE @from_date date;
DECLARE @thru_date date;
DECLARE @pdr_rate double;
DECLARE @metal_rate double;
DECLARE @hourly_rate double;
@pdr_rate = (SELECT pdrrate FROM scotest.bsFlatRateTechs WHERE thrudate > curdate());
@metal_rate = (SELECT metalrate FROM scotest.bsFlatRateTechs WHERE thrudate > curdate());
@hourly_rate = (SELECT otherrate FROM scotest.bsFlatRateTechs WHERE thrudate > curdate());
@from_date = '08/15/2021'; --------------------- change dates
@thru_date = '08/31/2021';
-- INSERT INTO z_tmp_AccrualFlatRateGross
SELECT 'RY1' AS storecode,'1110425' AS employeenumber, 'Peterson' AS lastname, 'Brian'as firstname,
			  '08/15/2021', '08/31/2021', pdrPay + metalPay + hourlyPay AS gross,'BTEC', 0
FROM ( 
  SELECT pdrRate, round(SUM(pdrFlagHours),2) AS pdrFlagHours, 
    round(pdrRate * round(SUM(pdrFlagHours),2), 2) AS pdrPay,
    metalRate, 
    round(SUM(metalFlagHours), 2) AS metalFlagHours,
    round(metalRate * round(SUM(metalFlagHours), 2), 2) AS metalPay
  FROM (  
    SELECT theDate, @pdr_rate AS pdrRate,
      coalesce(CASE WHEN c.opcode = 'PDR' THEN flaghours END, 0) AS pdrFlagHours,
      @metal_rate AS metalRate,
      coalesce(CASE WHEN c.opcode <>'PDR' THEN flaghours END, 0) AS metalFlagHours
    FROM factRepairOrder a
    INNER JOIN day b on a.closedatekey = b.datekey
    INNER JOIN dimOpcode c on a.opcodeKey = c.opcodeKey
    INNER JOIN dimPaymentType f on a.paymentTypeKey = f.paymentTypeKey
    INNER JOIN dimTech g on a.techKey = g.techKey
    WHERE g.technumber = '213'
      AND b.theDate BETWEEN @from_date AND @thru_date) m    
    GROUP BY pdrRate, metalRate) n  
LEFT JOIN (
  SELECT @hourly_rate AS hourlyRate, SUM(vacationHours) AS vacationHours, sum(ptoHours) AS ptoHours, 
    sum(holidayHours) AS holidayHours,
    @hourly_rate * (SUM(vacationHours) + SUM(ptoHours) + sum(holidayHours)) AS hourlyPay
  FROM edwClockHoursFact a
  INNER JOIN edwEmployeeDim b on a.employeekey = b.employeekey
  INNER JOIN dimTech c on b.storecode = c.storecode
    AND b.employeenumber = c.employeenumber
    AND c.currentrow = true
  INNER JOIN day d on a.datekey = d.datekey
  WHERE c.storecode = 'ry1'
    AND c.technumber = '213'
    AND d.theDate BETWEEN @from_date AND @thru_date) o  on 1 = 1 	
		
INSERT INTO z_tmp_AccrualFlatRate1
SELECT d.storecode, d.employeenumber, d.gross, ptopay AS pto,
  d.gross AS TotalNonOTPay,
  f.GROSS_DIST, f.GROSS_EXPENSE_ACT_, f.SICK_LEAVE_EXPENSE_ACT_,  
  f.EMPLR_FICA_EXPENSE, f.EMPLR_MED_EXPENSE, f.EMPLR_CONTRIBUTIONS, 
  g.yficmp, g.ymedmp,
  coalesce(h.FicaEx, 0) AS FicaEx,
  coalesce(i.MedEx, 0) AS MedEx,
  coalesce(j.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
FROM z_tmp_AccrualFlatRateGross d      
LEFT JOIN zFixPyactgr f on d.storecode = f.company_number
  AND d.distcode = f.dist_code    
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL g ON d.storecode = g.yco#
-- *x*  
--  AND g.ycyy = 100 + (year(@thruDate) - 2000) -- 112 -   
  AND g.ycyy = 117 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) h ON d.employeenumber = h.EMPLOYEE_NUMBER 
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) i ON d.employeenumber = i.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT j ON d.storecode = j.COMPANY_NUMBER
  AND d.employeenumber = j.EMPLOYEE_NUMBER 
--  AND h.DED_PAY_CODE IN ('91', '99');
-- *666
  AND 
    case 
      when j.employee_number = '11650' then j.ded_pay_code = '99'
      else j.DED_PAY_CODE = '91'
    END;		
		
INSERT INTO z_tmp_AccrualFlatRate2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * Gross AS Amount
FROM z_tmp_AccrualFlatRate1
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTO AS Amount
FROM z_tmp_AccrualFlatRate1
WHERE PTO <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2)) AS Amount
FROM z_tmp_AccrualFlatRate1
GROUP BY employeenumber, FicaAccount
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2)) AS Amount
FROM z_tmp_AccrualFlatRate1
GROUP BY employeenumber, MedicareAccount
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  sum(
    CASE 
      WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
      CASE 
        WHEN TotalNonOTPay * FixedDeductionAmount/200.0  < .02 * TotalNonOTPay
          THEN round(GrossDistributionPercentage/100.0 * TotalNonOTPay * FixedDeductionAmount/200.0, 2)
        ELSE round(GrossDistributionPercentage/100.0 * .02 * TotalNonOTPay, 2) 
      END 
    END) AS Amount     
FROM z_tmp_AccrualFlatRate1
GROUP BY employeenumber, RetirementAccount;
 	
	
SELECT * FROM z_tmp_AccrualFlatRate2	


DELETE FROM AccrualFileForJeri;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, account, employeenumber AS control, document, reference, 
  round(cast(a.amount AS sql_double), 2) AS amount, description 
FROM ( 
  SELECT employeenumber, account, SUM(amount) AS amount
  FROM ( 
    SELECT * FROM z_tmp_AccrualFlatRate2 WHERE amount <> 0)  a
		
 
-- reversing entries
-- reversing entries

SELECT e.journal, e.thedate, d.Account, e.document, e.document, e.reference, 
  round(d.Amount, 2), e.description
FROM (  
  SELECT employeenumber, account, SUM(amount) AS amount
  FROM z_tmp_AccrualFlatRate2 WHERE amount <> 0)  a        
	    
    GROUP BY employeenumber, account) c	
  GROUP BY LEFT(Account, 1)) d, AccrualDates e;