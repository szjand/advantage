DECLARE @year_month integer;
@year_month = (
  SELECT DISTINCT yearmonth
  FROM day
  WHERE thedate IN ('05/01/2015'));--,'06/01/2015','07/01/2015','08/01/2015','09/01/2015')); --= curdate() - 90);
  
SELECT @year_month FROM system.iota;
  
SELECT d.*, 
  CASE
    WHEN year(check_date) * 100 + month(check_date) = d.yearmonth THEN 
      timestampdiff(sql_tsi_day, last_payday, last)
    ELSE timestampdiff(sql_tsi_day, last_payday, last) + 14
  END AS accrual_days,
  CASE
    WHEN year(check_date) * 100 + month(check_date) = d.yearmonth THEN   
      (timestampdiff(sql_tsi_day, last_payday, last))/14.0
    ELSE
      (timestampdiff(sql_tsi_day, last_payday, last) + 14)/14.0
  END AS  accrual_days_14
FROM (  
  SELECT c.*, 
    (
      SELECT MIN(thedate) 
      FROM day 
      WHERE dayofweek = 6 AND thedate > c.last_payday) AS check_date  
  FROM (  
    SELECT yearmonth, MIN(thedate) AS first, MAX(thedate) AS last,
      MAX(biweeklyPayPeriodEndDate) AS last_payday 
    FROM day
    WHERE yearmonth = 201505    
    GROUP BY yearmonth) c) d        
      
  
  
  
    
    SELECT a.yearmonth, 
    max(b.last) eom ,
    max(a.biweeklyPayPeriodEndDate) AS last_payday 
    FROM day a
    inner JOIN (
      SELECT yearmonth, MAX(thedate) AS last, MIN(thedate) AS first
      FROM day
      WHERE yearmonth = 201505
      GROUP BY yearmonth) b on a.biweeklyPayPeriodEndDate BETWEEN b.first AND b.last
        AND a.yearmonth = b.yearmonth
    GROUP BY a.yearmonth) c) d;
    
SELECT yearmonth, MIN(thedate) AS first, MAX(thedate) AS last,
  MAX(biweeklyPayPeriodEndDate) AS last_payday 
FROM day
WHERE yearmonth = 201505    
GROUP BY yearmonth