

SELECT c.account, d.pydept, SUM(c.amount)
FROM accrualfileforjeri c
LEFT JOIN (
SELECT a.account, b.pydept
FROM (
  SELECT account, MAX(control) AS control
  FROM accrualfileforjeri 
  GROUP BY account) a
LEFT JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true) d on c.account = d.account
GROUP BY c.account, d.pydept
ORDER BY d.pydept

SELECT d.pydept, SUM(c.amount)
FROM accrualfileforjeri c
LEFT JOIN (
SELECT a.account, b.pydept
FROM (
  SELECT account, MAX(control) AS control
  FROM accrualfileforjeri 
  GROUP BY account) a
LEFT JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true) d on c.account = d.account
GROUP BY d.pydept
ORDER BY d.pydept

SELECT d.pydept, SUM(c.amount)
FROM Accrual201504 c
LEFT JOIN (
SELECT a.account, b.pydept
FROM (
  SELECT account, MAX(control) AS control
  FROM Accrual201504 
  GROUP BY account) a
LEFT JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true) d on c.account = d.account
GROUP BY d.pydept
ORDER BY d.pydept



SELECT x.pydept, round(x.amt, 0) AS May_2015, round(y.amt, 0) AS Apr_2015
FROM (
SELECT d.pydept, SUM(c.amount) AS amt
FROM accrualfileforjeri c
LEFT JOIN (
SELECT a.account, b.pydept
FROM (
  SELECT account, MAX(control) AS control
  FROM accrualfileforjeri 
  GROUP BY account) a
LEFT JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true) d on c.account = d.account
GROUP BY d.pydept) x
LEFT JOIN (
SELECT d.pydept, SUM(c.amount) AS amt
FROM Accrual201504 c
LEFT JOIN (
SELECT a.account, b.pydept
FROM (
  SELECT account, MAX(control) AS control
  FROM Accrual201504 
  GROUP BY account) a
LEFT JOIN edwEmployeeDim b on a.control = b.employeenumber
  AND b.currentrow = true) d on c.account = d.account
GROUP BY d.pydept) y on x.pydept = y.pydept


DROP TABLE Accrual201504