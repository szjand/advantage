

SELECT w.gmdept, 
		 --_202001, _202002,
--	   _202003,_202004, _202005, _202006, _202007, _202008,_202009, _202010, _202011,
--	   _202012, 
		 _202101, _202102, _202103, _202104, _202105, _202106, _202107, _202108,_202109,
		 _202110, _202111, _202112
FROM (
/*

*/ 
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202001
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202001 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) w -- zb on w.gmdept = zb.gmdept 
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202002
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202002 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zc on w.gmdept = zc.gmdept     
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202003
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202003 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zd on w.gmdept = zd.gmdept       
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202004
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202004 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) ze on w.gmdept = ze.gmdept       
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202005
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202005 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zf on w.gmdept = zf.gmdept   
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202006
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202006 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zg on w.gmdept = zg.gmdept     
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202007
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202007 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zh on w.gmdept = zh.gmdept    
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202008
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202008 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zi on w.gmdept = zi.gmdept      
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202009
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202009 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zj on w.gmdept = zj.gmdept     
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202010
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202010 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zk on w.gmdept = zk.gmdept
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202011
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202011 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zl on w.gmdept = zl.gmdept              
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202012
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2020
  WHERE a.year_month = 202012 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zm on w.gmdept = zm.gmdept      
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202101
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202101 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zn on w.gmdept = zn.gmdept    
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202102
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202102 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zo on w.gmdept = zo.gmdept     
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202103
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202103 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zp on w.gmdept = zp.gmdept       
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202104
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202104 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zq on w.gmdept = zq.gmdept   	
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202105
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202105 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zr on w.gmdept = zr.gmdept  	
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202106
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202106 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zs on w.gmdept = zs.gmdept  		
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202107
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202107 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zt on w.gmdept = zt.gmdept  			
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202108
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202108 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zu on w.gmdept = zu.gmdept  			
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202109
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202109 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zv on w.gmdept = zv.gmdept  			
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202110
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202110 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zw on w.gmdept = zw.gmdept  		
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202111
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202111 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zx on w.gmdept = zx.gmdept 	
LEFT JOIN (  
  SELECT gmdept, round(SUM(amount)/MAX(days), 0) AS _202112
  FROM (
  select b.gmdept, (thru_date - from_date) + 1 AS days, amount
  -- select *
  FROM accrual_history a
  LEFT JOIN stgarkonaglpmast b on a.account = b.gmacct
    AND b.gmyear = 2021
  WHERE a.year_month = 202112 AND LEFT(a.account, 1) = '1'
  ) x GROUP BY gmdept) zy on w.gmdept = zy.gmdept 		