SELECT employeenumber
FROM edwEmployeeDim
WHERE distcode = 'wtec'
  AND payrollclasscode = 'c'
  AND currentrow = true
  
  
  
SELECT d.thedate, a.techkey, a.technumber, b.lastname, b.firstname, b.employeekey, 
  b.employeenumber,
  c.* 
FROM dimtech a
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.distcode = 'wtec' 
  AND b.payrollclasscode = 'c'
  AND b.currentrow = true
INNER JOIN edwClockHoursFact c on b.employeekey = c.employeekey
INNER JOIN day d on c.datekey = d.datekey
  AND d.datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '05/17/2015' AND '05/30/2015')  
ORDER BY lastname, thedate    
  
  
SELECT lastname, firstname, SUM(clockhours), SUM(regularhours), 
  SUM(overtimehours),SUM(vacationhours),SUM(ptohours),SUM(holidayhours)
FROM (
  SELECT a.techkey, a.technumber, b.lastname, b.firstname, b.employeekey, 
    b.employeenumber,
    c.* 
  FROM dimtech a
  INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.distcode = 'wtec' 
    AND b.payrollclasscode = 'c'
    AND b.currentrow = true
  INNER JOIN edwClockHoursFact c on b.employeekey = c.employeekey
  WHERE c.datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '05/17/2015' AND '05/30/2015')) x
GROUP BY lastname, firstname      



SELECT DISTINCT b.employeenumber 
FROM dimtech a
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.distcode = 'wtec' 
  AND b.payrollclasscode = 'c'
  AND b.currentrow = true

  
SELECT *
FROM accrualcommissions
WHERE amount <> 0
  AND employeenumber IN (
  SELECT DISTINCT b.employeenumber 
  FROM dimtech a
  INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
    AND b.distcode = 'wtec' 
    AND b.payrollclasscode = 'c'
    AND b.currentrow = true)
    
-- 6/2  shit, ALL the commissioned/flat rate are fucked up
    
-- main shop
SELECT *
FROM edwEmployeeDim 
WHERE distcode = 'stec'
  AND payrollclasscode = 'c'
  AND currentrow = true
ORDER BY name  
-- body shop
SELECT *
FROM edwEmployeeDim 
WHERE distcode = 'btec'
  AND payrollclasscode = 'c'
  AND currentrow = true
ORDER BY name  
-- detail
SELECT *
FROM edwEmployeeDim 
WHERE distcode = 'wtec'
  AND payrollclasscode = 'c'
  AND currentrow = true
ORDER BY name  


/*
for ALL 3 dist codes:
  1. there IS no overtime AND
  2. vac, hol & pto ALL go to the same acct 

SELECT *
FROM zFixPyactgr
WHERE dist_code IN ('btec','stec','wtec')
*/

based on AccrualHourly1

DECLARE @thruDate date;
@thruDate = '05/30/2015';
DELETE FROM AccrualTeamPay1;
INSERT INTO AccrualTeamPay1
/*
SELECT d.*, e.storecode, e.distcode, 
  f.GROSS_DIST, f.GROSS_EXPENSE_ACT_, 
  f.EMPLR_FICA_EXPENSE, f.EMPLR_MED_EXPENSE, f.EMPLR_CONTRIBUTIONS, 
  f.SICK_LEAVE_EXPENSE_ACT_, g.yficmp, g.ymedmp,
  coalesce(h.FicaEx, 0) AS FicaEx,
  coalesce(i.MedEx, 0) AS MedEx,
  coalesce(j.FIXED_DED_AMT, 0) AS FIXED_DED_AMT 
*/ 

SELECT e.storecode, d.employeenumber, d.gross, d.pto,
  d.gross + d.pto AS TotalNonOTPay,
  f.GROSS_DIST, f.GROSS_EXPENSE_ACT_, f.SICK_LEAVE_EXPENSE_ACT_,  
  f.EMPLR_FICA_EXPENSE, f.EMPLR_MED_EXPENSE, f.EMPLR_CONTRIBUTIONS, 
  g.yficmp, g.ymedmp,
  coalesce(h.FicaEx, 0) AS FicaEx,
  coalesce(i.MedEx, 0) AS MedEx,
  coalesce(j.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
FROM ( 
  select lastname, employeenumber, 
    round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2) AS gross,
    round(techhourlyrate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd), 2) AS pto
  --  round(techtfrrate*teamprofpptd*techclockhourspptd/100/10, 2) AS commissionPay
  FROM scotest.tpdata a
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 7 AND curdate()  
  UNION -- brian peterson
  select b.lastname, a.employeenumber, 
    round(b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD, 2) AS gross, --+
    round(b.otherRate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd), 2) AS pto
  --  round((b.pdrrate * a.techPdrFlagHoursPPTD + b.metalrate * a.techMetalFlagHoursPPTD)/10, 2) AS commissionPay
  FROM scotest.bsFlatRateData a
  LEFT JOIN scotest.bsFlatRateTechs b on a.employeenumber = b.employeenumber
    AND a.thedate BETWEEN b.fromdate AND b.thrudate
  LEFT JOIN scotest.tpEmployees c on b.employeenumber = c.employeenumber  
  WHERE thedate = payperiodend
    AND thedate BETWEEN curdate() - 7 AND curdate()) d  
LEFT JOIN edwEmployeeDim e on d.employeenumber = e.employeenumber
  AND e.currentrow = true    
LEFT JOIN zFixPyactgr f on e.storecode = f.company_number
  AND e.distcode = f.dist_code    
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL g ON e.storecode = g.yco#
  AND g.ycyy = 100 + (year(@thruDate) - 2000) -- 112 -    
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) h ON d.employeenumber = h.EMPLOYEE_NUMBER 
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) i ON d.employeenumber = i.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT j ON e.storecode = j.COMPANY_NUMBER
  AND d.employeenumber = j.EMPLOYEE_NUMBER 
--  AND h.DED_PAY_CODE IN ('91', '99');
-- *666
  AND 
    case 
      when j.employee_number = '11650' then j.ded_pay_code = '99'
      else j.DED_PAY_CODE = '91'
    END;

DELETE FROM AccrualTeamPay2;   
INSERT INTO AccrualTeamPay2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * Gross AS Amount
FROM AccrualTeamPay1
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTO AS Amount
FROM AccrualTeamPay1
WHERE PTO <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2)) AS Amount
FROM AccrualTeamPay1
GROUP BY employeenumber, FicaAccount
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2)) AS Amount
FROM AccrualTeamPay1
GROUP BY employeenumber, MedicareAccount
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  sum(CASE 
    WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
    
    CASE 
      WHEN TotalNonOTPay * FixedDeductionAmount/200.0  < .02 * TotalNonOTPay
        THEN round(GrossDistributionPercentage/100.0 * TotalNonOTPay * FixedDeductionAmount/200.0, 2)
      ELSE round(GrossDistributionPercentage/100.0 * .02 * TotalNonOTPay, 2) 
    END 
  END) AS Amount     
FROM AccrualTeamPay1
GROUP BY employeenumber, RetirementAccount;


SELECT * FROM AccrualTeamPay2


-- clock & flag for detail
SELECT m.lastname, n.clock_hours, m.flag_hours, round(m.flag_hours/n.clock_hours, 2)
FROM (
  SELECT d.lastname, c.technumber, SUM(flaghours) AS flag_hours,
    d.employeekey
  FROM factRepairOrder a
  INNER JOIN day b on a.flagdatekey = b.datekey
    AND b.thedate BETWEEN '05/17/2015' AND '05/30/2015'
  INNER JOIN dimtech c on a.techkey = c.techkey
  INNER JOIN (
    SELECT employeenumber, lastname, employeekey
    FROM edwEmployeeDim
    WHERE distcode = 'wtec'
      AND payrollclasscode = 'c'
      AND currentrow = true) d on c.employeenumber = d.employeenumber
  GROUP BY d.lastname, c.technumber, d.employeekey) m
LEFT JOIN (
  select c.lastname, SUM(clockhours) AS clock_hours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
    AND b.thedate BETWEEN '05/17/2015' AND '05/30/2015'
  INNER JOIN (
    SELECT employeenumber, lastname, employeekey
    FROM edwEmployeeDim
    WHERE distcode = 'wtec'
      AND payrollclasscode = 'c'
      AND currentrow = true) c on a.employeekey = c.employeekey  
  GROUP BY c.lastname) n on m.lastname = n.lastname    

