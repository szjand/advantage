12/02/19
  jeri noticed accrual for detail was short
  for some unknown reason, IN accrualFlatRateGross.sql, i commented out the INSERT
  statement for detail, flat rate detail techs have NOT been accrued since
  August
  Good time to UPDATE that process, currently accrual_detail_techs only has 4 of 12 techs
  1. get updated list of techs AND rates to UPDATE accrual_detail_techs
  2. ADD a check IN accrual - ALL IN one.sql to keep accrual_detail_techs current

SELECT *
FROM accrualfileforjeri
WHERE control = '184806'


SELECT aa.store_code, aa.employee_number, aa.last_name, aa.first_name,
  '11/24/2019','11/30/2019', bb.flag_hours, cc.reg_hours, cc.pto_hours,
--  round(
--    CASE
--      WHEN aa.last_name <> 'richardson' THEN flat_rate * flag_hours
--      WHEN aa.last_name = 'richardson' THEN 
--        1.5 * ((reg_hours * flat_rate) + (1.5 * (ot_hours * pto_rate)))
--    END, 2) AS gross,
  round(flat_rate * coalescE(flag_hours, 0), 2) AS gross,
  aa.dist_code,
  pto_hours * pto_rate AS pto     
FROM accrual_detail_techs aa
LEFT JOIN ( -- flag hours
  SELECT d.employee_number, SUM(a.flaghours) AS flag_hours
  FROM factrepairorder a
  INNER JOIN day b on a.flagdatekey = b.datekey
  INNER JOIN dimtech c on a.techkey = c.techkey
  INNER JOIN accrual_detail_techs d on c.employeenumber = d.employee_number
  WHERE b.thedate BETWEEN '11/24/2019' AND '11/30/2019'
  GROUP BY d.employee_number) bb on aa.employee_number = bb.employee_number
left join (-- clock hours
  SELECT e.employee_number, SUM(a.regularhours) AS reg_hours, 
    SUM(a.overtimehours) AS ot_hours,
    SUM(vacationhours + ptohours + holidayhours) AS pto_hours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
    AND b.thedate BETWEEN '11/24/2019' AND '11/30/2019'
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  INNER JOIN accrual_detail_techs e on c.employeenumber = e.employee_number
  GROUP BY e.employee_number) cc on aa.employee_number = cc.employee_number;
  

  
  
select year_month, control, SUM(amount) 
from accrual_history 
WHERE control in ('134956') 
group by year_month, control 
ORDER BY year_month DESC, control

-- this shows that no flat rate detail techs are being accrued
select a.name, a.employeenumber, a.payrollclass, b.accrued
-- SELECT *
FROM edwEmployeeDim a
LEFT JOIN (
  select year_month, control, SUM(amount) AS accrued
  from accrual_history 
  WHERE year_month = 201911
  group by year_month, control) b on a.employeenumber = b.control
WHERE a.distcode = 'WTEC'
  AND a.activecode = 'A'
  AND a.currentrow = true
ORDER BY cast(a.employeenumber AS sql_integer)


-- SELECT *
FROM edwEmployeeDim a
WHERE a.distcode = 'WTEC'
  AND a.activecode = 'A'
  AND a.currentrow = true
 
-- this list agrees with payroll sheet FROM kim (created BY dayton)  
select a.storecode, a.employeenumber, a.technumber, b.lastname, b.firstname, b.distcode
FROM dimtech a
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.distcode = 'WTEC'
  AND b.currentrow = true
  AND b.payrollclass = 'commission'
  AND b.active = 'active'
  AND b.lastname NOT IN ( 'telken')
WHERE a.active = true
  AND a.techkeythrudate > curdate()
  AND a.flagdept = 'detail'
  AND a.storecode = 'RY1'
ORDER BY b.lastname  

-- ADD the missing techs  
INSERT INTO accrual_detail_techs (store_code,employee_number,tech_number,last_name,first_name,dist_code)
select a.storecode, a.employeenumber, a.technumber, b.lastname, b.firstname, b.distcode
FROM dimtech a
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.distcode = 'WTEC'
  AND b.currentrow = true
  AND b.payrollclass = 'commission'
  AND b.active = 'active'
  AND b.lastname NOT IN ( 'telken','leavy','davis','hepola','johnson')
WHERE a.active = true
  AND a.techkeythrudate > curdate()
  AND a.flagdept = 'detail'
  AND a.storecode = 'RY1'
  
-- now need to DO the rates
SELECT 'UPDATE accrual_detail_techs SET flat_rate = , pto_rate =  WHERE last_name =' + '''' + trim(last_name) + '''' + ';' 
FROM accrual_Detail_techs
ORDER BY last_name;



UPDATE accrual_detail_techs SET flat_rate = 14, pto_rate = 14 WHERE last_name ='Davis' ;
UPDATE accrual_detail_techs SET flat_rate = 13, pto_rate = 13 WHERE last_name ='Hepola';
UPDATE accrual_detail_techs SET flat_rate = 14.5, pto_rate = 14.5 WHERE last_name ='Johnson';
UPDATE accrual_detail_techs SET flat_rate = 20, pto_rate = 20 WHERE last_name ='Leavy';
UPDATE accrual_detail_techs SET flat_rate = 13, pto_rate = 13 WHERE last_name ='Mccoy';
UPDATE accrual_detail_techs SET flat_rate = 14, pto_rate = 14 WHERE last_name ='Preston';
UPDATE accrual_detail_techs SET flat_rate = 14, pto_rate = 14 WHERE last_name ='Richardson';
UPDATE accrual_detail_techs SET flat_rate = 13, pto_rate = 13 WHERE last_name ='Rolighed';
UPDATE accrual_detail_techs SET flat_rate = 15, pto_rate = 15 WHERE last_name ='Sauls';
UPDATE accrual_detail_techs SET flat_rate = 13, pto_rate = 13 WHERE last_name ='Schadt';
UPDATE accrual_detail_techs SET flat_rate = 14.5, pto_rate = 14.5 WHERE last_name ='Schneider';
UPDATE accrual_detail_techs SET flat_rate = 13, pto_rate = 13 WHERE last_name ='Straus';



-- CREATE a check to put INTO accrual - ALL IN one.sql
-- DROP TABLE #techs_1;
SELECT employee_number, last_name, first_name 
INTO #techs_1
FROM accrual_detail_techs

-- DROP TABLE #techs_2;
-- select a.storecode, a.employeenumber, a.technumber, b.lastname, b.firstname, b.distcode
SELECT a.employeenumber, b.lastname, b.firstname
INTO #techs_2
FROM dimtech a
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.distcode = 'WTEC'
  AND b.currentrow = true
  AND b.payrollclass = 'commission'
  AND b.active = 'active'
  AND b.lastname NOT IN ( 'telken')
WHERE a.active = true
  AND a.techkeythrudate > curdate()
  AND a.flagdept = 'detail'
  AND a.storecode = 'RY1'
  
SELECT *
FROM #techs_1 a
full OUTER JOIN #techs_2 b on a.employee_number = b.employeenumber  