/*
some how, for detail i managed to double the flatrategross
AND submitted that for the accrual, which totally fucked up details
adjusted cost of labor becase it overstated the amount paid
this IS the attempt to fix it

jeri wants a "correcting file", for those accrual entries that were incorrect
generate a file of correcting (negative) entries
*/   

/* 
1. CREATE the tables
*/

CREATE TABLE zjon_AccrualFlatRateGross ( 
      storecode CIChar( 3 ),
      employeenumber CIChar( 9 ),
      lastname CIChar( 25 ),
      firstname CIChar( 25 ),
      fromdate Date,
      thrudate Date,
      gross Double( 15 ),
      distcode CIChar( 4 )) IN DATABASE;
      
CREATE TABLE zjon_AccrualFlatRate1 ( 
      StoreCode CIChar( 3 ),
      EmployeeNumber CIChar( 9 ),
      Gross Money,
      PTO Money,
      TotalNonOTPay Money,
      GrossDistributionPercentage Double( 2 ),
      GrossAccount CIChar( 10 ),
      PTOAccount CIChar( 10 ),
      FicaAccount CIChar( 10 ),
      MedicareAccount CIChar( 10 ),
      RetirementAccount CIChar( 10 ),
      FicaPercentage Double( 2 ),
      MedicarePercentage Double( 2 ),
      FicaExempt Money,
      MedicareExempt Money,
      FixedDeductionAmount Double( 2 )) IN DATABASE;      
      
CREATE TABLE zjon_AccrualFlatRate2 ( 
      Category CIChar( 12 ),
      EmployeeNumber CIChar( 9 ),
      Account CIChar( 10 ),
      Amount Money) IN DATABASE;      
      

/*
2. populate the tables with the correct numbers
*/    


DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromdate = '12/27/2015';
@thrudate = '12/31/2015';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);

INSERT INTO zjon_AccrualFlatRateGross   
SELECT 'RY1', employeenumber, lastname, firstname, @fromdate, @thrudate,  
  coalesce(round((12 * flaghours) + ((rate - 12) * flaghours) + (12 * ptohours), 2), 0), 'WTEC'
FROM (
  SELECT m.*, n.flaghours, round(flaghours/clockhours, 2) AS prof,
  -- hard code the prof matrix for now
    CASE
-- *a*  
      WHEN lastname IN ('Telken','Kingbird','Roehrich','Gonzalez') THEN 12.0
      WHEN lastname = 'Nelson' AND firstname = 'Hunter' THEN 12.0   
      WHEN round(flaghours/clockhours, 2) <= 1.3 THEN 12.0   
      WHEN round(flaghours/clockhours, 2) >= 1.3 AND round(flaghours/clockhours, 2) < 1.4 THEN 12.5
      WHEN round(flaghours/clockhours, 2) >= 1.4 AND round(flaghours/clockhours, 2) < 1.5 THEN 13.5
      WHEN round(flaghours/clockhours, 2) >= 1.5 AND round(flaghours/clockhours, 2) < 1.6 THEN 15
      WHEN round(flaghours/clockhours, 2) >= 1.6 AND round(flaghours/clockhours, 2) < 1.7 THEN 16
      WHEN round(flaghours/clockhours, 2) >= 1.7 AND round(flaghours/clockhours, 2) < 1.8 THEN 17
      WHEN round(flaghours/clockhours, 2) >= 1.8 AND round(flaghours/clockhours, 2) < 1.9 THEN 18
      WHEN round(flaghours/clockhours, 2) >= 1.9 AND round(flaghours/clockhours, 2) < 2 THEN 19
      WHEN round(flaghours/clockhours, 2) > 2 THEN 20.0  
    END AS rate
  FROM (
    SELECT a.firstname, a.lastname, a.employeenumber, a.employeekey, 
      SUM(b.clockhours) AS clockhours, SUM(holidayhours+ptohours+vacationhours) AS ptohours,
      c.technumber, c.techkey
    FROM edwEmployeeDim a 
    LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
      AND b.datekey IN (
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN @fromdate AND @thrudate)
    LEFT JOIN dimtech c on a.employeenumber = c.employeenumber
      AND c.currentrow = true    
    WHERE a.distcode = 'wtec'
  --    AND a.currentrow = true
      AND a.active = 'active'
      AND a.employeekeyfromdate <= @thrudate
      AND a.employeekeythrudate > @fromdate
      AND a.payrollclass = 'commission'
    GROUP BY a.firstname, a.lastname, a.employeenumber, a.employeekey, c.technumber, c.techkey) m   
  LEFT JOIN (
    SELECT d.technumber, d.description, d.employeenumber, SUM(flaghours) AS flaghours
    FROM factrepairorder a
    INNER JOIN day b on a.closedatekey = b.datekey
    INNER JOIN dimtech d on a.techkey = d.techkey
    WHERE b.thedate between @fromdate AND @thrudate
      AND d.flagdeptcode = 're'
      AND d.employeenumber <> 'NA'
    GROUP BY d.technumber, d.description, d.employeenumber) n on m.employeenumber = n.employeenumber  ) x;   


INSERT INTO zjon_AccrualFlatRate1
SELECT d.storecode, d.employeenumber, d.gross, 0 AS pto,
  d.gross AS TotalNonOTPay,
  f.GROSS_DIST, f.GROSS_EXPENSE_ACT_, f.SICK_LEAVE_EXPENSE_ACT_,  
  f.EMPLR_FICA_EXPENSE, f.EMPLR_MED_EXPENSE, f.EMPLR_CONTRIBUTIONS, 
  g.yficmp, g.ymedmp,
  coalesce(h.FicaEx, 0) AS FicaEx,
  coalesce(i.MedEx, 0) AS MedEx,
  coalesce(j.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
FROM zjon_AccrualFlatRateGross d      
LEFT JOIN zFixPyactgr f on d.storecode = f.company_number
  AND d.distcode = f.dist_code    
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL g ON d.storecode = g.yco#
  AND g.ycyy = 100 + (year(@thruDate) - 2000) -- 112 -    
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) h ON d.employeenumber = h.EMPLOYEE_NUMBER 
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) i ON d.employeenumber = i.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT j ON d.storecode = j.COMPANY_NUMBER
  AND d.employeenumber = j.EMPLOYEE_NUMBER 
--  AND h.DED_PAY_CODE IN ('91', '99');
-- *666
  AND 
    case 
      when j.employee_number = '11650' then j.ded_pay_code = '99'
      else j.DED_PAY_CODE = '91'
    END;

   
INSERT INTO zjon_AccrualFlatRate2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * Gross AS Amount
FROM zjon_AccrualFlatRate1
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTO AS Amount
FROM zjon_AccrualFlatRate1
WHERE PTO <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2)) AS Amount
FROM zjon_AccrualFlatRate1
GROUP BY employeenumber, FicaAccount
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2)) AS Amount
FROM zjon_AccrualFlatRate1
GROUP BY employeenumber, MedicareAccount
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  sum(
    CASE 
      WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
      CASE 
        WHEN TotalNonOTPay * FixedDeductionAmount/200.0  < .02 * TotalNonOTPay
          THEN round(GrossDistributionPercentage/100.0 * TotalNonOTPay * FixedDeductionAmount/200.0, 2)
        ELSE round(GrossDistributionPercentage/100.0 * .02 * TotalNonOTPay, 2) 
      END 
    END) AS Amount     
FROM AccrualFlatRate1
GROUP BY employeenumber, RetirementAccount;
    

/*
3. generate the correction file

SELECT * FROM accrualfileforjeri
*/

SELECT b.journal, b.date, b.account, b.control, b.document, b.reference, 
  cast(a.amount - b.amount AS sql_double) AS amount, b.description
--  a.amount, b.amount
FROM (
  SELECT employeenumber, account, SUM(amount) AS amount
  FROM zjon_accrualflatrate2 
  WHERE amount <> 0
  GROUP BY employeenumber, account) a
LEFT JOIN accrualfileforjeri b on a.employeenumber = b.control
  AND a.account = b.account
WHERE a.amount <> b.amount
ORDER BY a.employeenumber


SELECT SUM(a.amount - b.amount)
FROM (
  SELECT employeenumber, account, SUM(amount) AS amount
  FROM zjon_accrualflatrate2 
  WHERE amount <> 0
  GROUP BY employeenumber, account) a
LEFT JOIN accrualfileforjeri b on a.employeenumber = b.control
  AND a.account = b.account
WHERE a.amount <> b.amount

       