
error on 
INSERT INTO AccrualFlatRateGross 
IN the detail section
for now, just exclude the employees with NULL gross
no, maybe 0 gross due to pto, so coalece the gross
poQuery: Error 7200:  AQE Error:  State = HY000;   NativeError = 5147;  [iAnywhere Solutions][Advantage SQL][ASA] Error 5147:  Constraint violation while updating column "gross".  The column cannot be updated to a NULL 
value due to a 'not NULL' constraint. ** Script error information:  -- Location of error in the SQL statement is: 7405 (line: 191 column: 1)

SELECT aa.store_code, aa.employee_number, aa.last_name, aa.first_name,
--  @fromdate, @thrudate, 
  coalesce(
    round(
      CASE
        WHEN aa.last_name <> 'richardson' THEN flat_rate * flag_hours
--      WHEN aa.last_name = 'richardson' THEN 
--        1.5 * ((reg_hours * flat_rate) + (1.5 * (ot_hours * pto_rate)))
      END, 2), 0) AS gross,
  aa.dist_code,
  pto_hours * pto_rate AS pto     
FROM accrual_detail_techs aa
left JOIN ( -- flag hours
  SELECT d.employee_number, SUM(a.flaghours) AS flag_hours
  FROM factrepairorder a
  INNER JOIN day b on a.flagdatekey = b.datekey
  INNER JOIN dimtech c on a.techkey = c.techkey
  INNER JOIN accrual_detail_techs d on c.employeenumber = d.employee_number
  WHERE b.thedate BETWEEN '12/22/2019' AND '12/31/2019'
  GROUP BY d.employee_number) bb on aa.employee_number = bb.employee_number
left join (-- clock hours
  SELECT e.employee_number, SUM(a.regularhours) AS reg_hours, 
    SUM(a.overtimehours) AS ot_hours,
    SUM(vacationhours + ptohours + holidayhours) AS pto_hours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
    AND b.thedate BETWEEN '12/22/2019' AND '12/31/2019'
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  INNER JOIN accrual_detail_techs e on c.employeenumber = e.employee_number
  GROUP BY e.employee_number) cc on aa.employee_number = cc.employee_number;