SELECT a.firstname, a.lastname, a.employeenumber, a.employeekey, 
  SUM(b.clockhours) AS clockhours,
  c.technumber, c.techkey
FROM edwEmployeeDim a 
LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
  AND b.datekey IN (
    SELECT datekey
    FROM day
    WHERE thedate BETWEEN '08/21/2015' AND '08/31/2015')
LEFT JOIN dimtech c on a.employeenumber = c.employeenumber
  AND c.currentrow = true    
WHERE a.distcode = 'wtec'
  AND a.currentrow = true
  AND a.active = 'active'
  AND a.employeekeyfromdate <= '08/31/2015'
  AND a.employeekeythrudate > '08/22/2015'
  AND a.payrollclass = 'commission'
GROUP BY a.firstname, a.lastname, a.employeenumber, a.employeekey, c.technumber, c.techkey 

  
  
  
SELECT d.technumber, d.description, d.employeenumber, SUM(flaghours) AS flaghours
FROM factrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
INNER JOIN dimtech d on a.techkey = d.techkey
WHERE b.thedate between '08/23/2015' AND '08/31/2015'
  AND d.flagdeptcode = 're'
  AND d.employeenumber <> 'NA'
GROUP BY d.technumber, d.description, d.employeenumber  
  
    
SELECT *
FROM dimtech
WHERE technumber = 'd41'  

SELECT a.ro, a.techkey, d.technumber, d.description, d.employeenumber, d.laborcost
FROM factrepairorder a
INNER JOIN day b on a.closedatekey = b.datekey
INNER JOIN dimtech d on a.techkey = d.techkey
WHERE b.thedate between '07/09/2015' AND '7/10/2015'
  AND d.flagdeptcode = 're'
  AND d.employeenumber <> 'NA'
  
  
Detail Bonus Plan

Proficiency
Hourly Rate Increase	Flat Rate Hourly Rate With Bonus
120% and Under	$0.00	$12.00
130%	$0.50	$12.50
140%	$1.50	$13.50
150%	$3.00	$15.00
160%	$4.00	$16.00
170%	$5.00	$17.00
180%	$6.00	$18.00
190%	$7.00	$19.00
200%	$8.00	$20.00

-- *a*
FROM Aubrey 11/2/15:Wyatt Telken, Ariella Kingbird and Nick Roehrich 
will never have enough hours in a pay period to get bonus pay 
so I would put them at 0

-- *b*
FROM aubrey 12/1 
No Bonus pay for Bryan Gonzalez or Hunter Nelson, they don�t work the minimum 
required hours. Other than that is looks good.

FROM aubrey 5/1/16
Eric Horton and Kordell Nelson I would remove bonus for as they generally 
don�t work enough hours in a pay period. I would add the bonus for 
Bryan Gonzalez as he did get enough hours. 

DECLARE @fromdate date;
DECLARE @thrudate date;
--@fromdate = '08/23/2015';
--@thrudate = '08/31/2015';
--@fromdate = '09/20/2015';
--@thrudate = '09/30/2015';
--@fromdate = '11/15/2015';
--@thrudate = '11/30/2015';
--@fromdate = '05/15/2016';
--@thrudate = '05/31/2016';
@fromdate = '06/25/2017';
@thrudate = '06/30/2017';
-- this gives the aubrey spreadsheet view
/**/
SELECT TRIM(firstname) + ' ' + lastname AS Technician, clockhours AS "Hours Worked", 
  flaghours AS "Hours Flagged", prof AS Proficiency, 12.00 AS "Flag Rate Hourly Rate", 
  12 * flaghours AS "Base Pay", 
-- *a*  
   rate - 12 AS "Flat Rate Prof Hourly Increase", 
  (rate - 12) * flaghours AS "Flat Rate Bonus", ptohours AS "PTO Hours", 
  12 as "Hourly Rate", 12 * ptohours AS Total, 
  (12 * flaghours) + ((rate - 12) * flaghours) + (12 * ptohours) AS "Total Pay"
/* 
-- for inserting INTO AccrualFlatRateGross 
-- this IS IN AccrualFlatRateGross

SELECT 'RY1', employeenumber, lastname, firstname, @fromdate, @thrudate,  
  coalesce(round((12 * flaghours) + ((rate - 12) * flaghours) + (12 * ptohours), 2), 0), 'WTEC'
/**/  
FROM (
  SELECT m.*, n.flaghours, round(flaghours/clockhours, 2) AS prof,
  -- hard code the prof matrix for now
  CASE
  -- *a*, *b* 
    WHEN lastname IN ('Telken','Kingbird','Roehrich'/*,'Gonzalez'*/) THEN 12.0
    WHEN lastname = 'Nelson' AND firstname = 'Hunter' THEN 12.0
    WHEN lastname = 'Horton' AND firstname = 'Eric' THEN 12.0
    WHEN lastname = 'Nelson' AND firstname = 'Kordell' THEN 12.0
    WHEN round(flaghours/clockhours, 2) <= 1.3 THEN 12.0   
    WHEN round(flaghours/clockhours, 2) >= 1.3 AND round(flaghours/clockhours, 2) < 1.4 THEN 12.5
    WHEN round(flaghours/clockhours, 2) >= 1.4 AND round(flaghours/clockhours, 2) < 1.5 THEN 13.5
    WHEN round(flaghours/clockhours, 2) >= 1.5 AND round(flaghours/clockhours, 2) < 1.6 THEN 15
    WHEN round(flaghours/clockhours, 2) >= 1.6 AND round(flaghours/clockhours, 2) < 1.7 THEN 16
    WHEN round(flaghours/clockhours, 2) >= 1.7 AND round(flaghours/clockhours, 2) < 1.8 THEN 17
    WHEN round(flaghours/clockhours, 2) >= 1.8 AND round(flaghours/clockhours, 2) < 1.9 THEN 18
    WHEN round(flaghours/clockhours, 2) >= 1.9 AND round(flaghours/clockhours, 2) < 2 THEN 19
    WHEN round(flaghours/clockhours, 2) > 2 THEN 20.0  
  END AS rate
  FROM (
    SELECT a.firstname, a.lastname, a.employeenumber, a.employeekey, 
      SUM(b.clockhours) AS clockhours, SUM(holidayhours+ptohours+vacationhours) AS ptohours,
      c.technumber, c.techkey
    FROM edwEmployeeDim a 
    LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
      AND b.datekey IN (
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN @fromdate AND @thrudate)
    LEFT JOIN dimtech c on a.employeenumber = c.employeenumber
      AND c.currentrow = true    
    WHERE a.distcode = 'wtec'
    --    AND a.currentrow = true
      AND a.active = 'active'
      AND a.employeekeyfromdate <= @thrudate
      AND a.employeekeythrudate > @fromdate
      AND a.payrollclass = 'commission'
    GROUP BY a.firstname, a.lastname, a.employeenumber, a.employeekey, c.technumber, c.techkey) m   
  LEFT JOIN (
  SELECT d.technumber, d.description, d.employeenumber, SUM(flaghours) AS flaghours
  FROM factrepairorder a
  INNER JOIN day b on a.closedatekey = b.datekey
  INNER JOIN dimtech d on a.techkey = d.techkey
  WHERE b.thedate between @fromdate AND @thrudate
    AND d.flagdeptcode = 're'
    AND d.employeenumber <> 'NA'
  GROUP BY d.technumber, d.description, d.employeenumber) n on m.employeenumber = n.employeenumber) x  
ORDER BY lastname, firstname  


-- 8/1/17
-- new pay plan
-- don't know what it IS yet
-- here are base stats on relevant personnel (i believe)
/*
1. DELETE existing rows FROM accrualflatrategross
DELETE FROM AccrualFlatRateGross; 

2. INSERT base rows INTO AccrualFlatRateGross

DECLARE @fromdate date;
DECLARE @thrudate date;
@fromdate = '12/24/2017';
@thrudate = '12/31/2017';
INSERT INTO AccrualFlatRateGross (storecode,employeenumber,lastname,firstname,
  fromdate,thrudate,gross,distcode,ptopay)
SELECT 'RY1', employeenumber, lastname, firstname, @fromdate, @thrudate, 0,
  'WTEC', 0
FROM (
  SELECT m.*, n.flaghours, round(flaghours/clockhours, 2) AS prof
  FROM (
    SELECT a.firstname, a.lastname, a.employeenumber, a.employeekey, 
      SUM(b.clockhours) AS clockhours, SUM(holidayhours+ptohours+vacationhours) AS ptohours,
      c.technumber, c.techkey
    FROM edwEmployeeDim a 
    LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
      AND b.datekey IN (
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN @fromdate AND @thrudate)
    LEFT JOIN dimtech c on a.employeenumber = c.employeenumber
      AND c.currentrow = true    
    WHERE a.distcode = 'wtec'
    --    AND a.currentrow = true
      AND a.active = 'active'
      AND a.employeekeyfromdate <= @thrudate
      AND a.employeekeythrudate > @fromdate
      AND a.payrollclass = 'commission'
    GROUP BY a.firstname, a.lastname, a.employeenumber, a.employeekey, c.technumber, c.techkey) m   
  LEFT JOIN (
  SELECT d.technumber, d.description, d.employeenumber, SUM(flaghours) AS flaghours
  FROM factrepairorder a
  INNER JOIN day b on a.closedatekey = b.datekey
  INNER JOIN dimtech d on a.techkey = d.techkey
  WHERE b.thedate between @fromdate AND @thrudate
    AND d.flagdeptcode = 're'
    AND d.employeenumber <> 'NA'
  GROUP BY d.technumber, d.description, d.employeenumber) n on m.employeenumber = n.employeenumber) x;
  
3.manually enter values from John G INTO AccrualFlatRateGross  
SELECT * FROM AccrualFlatRateGross ORDER BY lastname

4.run AccrualFlatRateGross.sql


SELECT *
FROM accrual_history
WHERE control = '164920'

*/
-------------------------------------------------------------------------------
-- 01/29/2018
-------------------------------------------------------------------------------
ok, changing again, there are a GROUP of detail techs that are paid strictly 
commission, based on a fixed flat rate hourly rate.
They need to be part of the accrual process: 
  flag hours (flag date) * flat rate hourly rate
This IS WHERE i am working it out, but the actual generation will go IN 
AccrualFlatRateGross.sql
  
  
DROP table accrual_detail_techs;
CREATE TABLE accrual_detail_techs (
  store_code cichar(3),
  employee_number cichar(9),
  tech_number cichar(3),
  last_name cichar(25),
  first_name cichar(25), 
  dist_code cichar(4),
  flat_rate numeric(6,2),
  pto_rate numeric(6,2));
  
INSERT INTO accrual_detail_techs    
-- these are the detail techs for accrual
SELECT 'RY1', a.employeenumber, b.technumber, a.lastname, a.firstname, 
  a.distcode,
  CASE lastname
    WHEN 'davis' THEN 14
    WHEN 'gonzalez' THEN 14
    WHEN 'hepola' THEN 13
    WHEN 'johnson' THEN 13.25
    WHEN 'leavy' THEN 20
    WHEN 'richardson' THEN 15
    WHEN 'telken' THEN 13.5
    WHEN 'vanhorne' THEN 12
  END AS flat_rate,
  CASE lastname
    WHEN 'davis' THEN 14
    WHEN 'gonzalez' THEN 14
    WHEN 'hepola' THEN 13
    WHEN 'johnson' THEN 13.25
    WHEN 'leavy' THEN 12
    WHEN 'richardson' THEN 15
    WHEN 'telken' THEN 13.5
    WHEN 'vanhorne' THEN 12
  END AS pto_rate  
FROM edwEmployeeDim a
LEFT JOIN dimtech b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.distcode = 'wtec'
  AND a.currentrow = true 
  AND a.active = 'active' 
  AND a.payrollclass = 'commission'
  AND a.payperiod = 'bi-weekly';  
  
SELECT *
FROM accrual_detail_techs  

-- SELECT * FROM AccrualFlatRateGross
SELECT * FROM accrual_detail_techs
-- why no richardson, he IS paid on clock hours, IS a shop foreman AND does
-- NOT flag hours 

DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromdate = '01/21/2018';
@thrudate = '01/31/2018';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);
SELECT aa.store_code, aa.employee_number, aa.last_name, aa.first_name,
  @fromdate, @thrudate, 
--  coalesce(bb.flag_hours, 0) AS flag_hours, 
--  cc.reg_hours, cc.ot_hours, cc.pto_hours, 
  round(
    CASE
      WHEN aa.last_name <> 'richardson' THEN flat_rate * flag_hours
      WHEN aa.last_name = 'richardson' THEN 
        1.5 * ((reg_hours * flat_rate) + (1.5 * (ot_hours * pto_rate)))
    END, 2) AS gross,
  aa.dist_code,
  pto_hours * pto_rate AS pto     
FROM accrual_detail_techs aa
LEFT JOIN ( -- flag hours
  SELECT d.employee_number, SUM(a.flaghours) AS flag_hours
  FROM factrepairorder a
  INNER JOIN day b on a.flagdatekey = b.datekey
  INNER JOIN dimtech c on a.techkey = c.techkey
  INNER JOIN accrual_detail_techs d on c.employeenumber = d.employee_number
  WHERE b.thedate BETWEEN @fromdate AND @thrudate
  GROUP BY d.employee_number) bb on aa.employee_number = bb.employee_number
left join (-- clock hours
  SELECT e.employee_number, SUM(a.regularhours) AS reg_hours, 
    SUM(a.overtimehours) AS ot_hours,
    SUM(vacationhours + ptohours + holidayhours) AS pto_hours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
    AND b.thedate BETWEEN @fromdate and @thrudate
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  INNER JOIN accrual_detail_techs e on c.employeenumber = e.employee_number
  GROUP BY e.employee_number) cc on aa.employee_number = cc.employee_number