/*
7/1/15 added AccrualBSGross, AccrualBS1, AccrualBS2
*/
DROP TABLE AccrualBSGross;
CREATE TABLE AccrualBSGross (
  storecode cichar(3) constraint NOT NULL,
  employeenumber cichar(9) constraint NOT NULL,
  firstname cichar(25) constraint NOT NULL,
  lastname cichar(25) constraint NOT NULL,  
  gross money constraint NOT NULL,
  pto money constraint NOT NULL,
  yearmonth integer constraint NOT null) IN database;

CREATE TABLE AccrualBS1 ( 
      StoreCode CIChar( 3 ) constraint NOT NULL,
      EmployeeNumber CIChar( 9 ) constraint NOT NULL,
      Gross Money constraint NOT NULL,
      PTO Money constraint NOT NULL,
      TotalNonOTPay Money constraint NOT NULL,
      GrossDistributionPercentage Double( 2 ) constraint NOT NULL,
      GrossAccount CIChar( 10 ) constraint NOT NULL,
      PTOAccount CIChar( 10 ) constraint NOT NULL,
      FicaAccount CIChar( 10 ) constraint NOT NULL,
      MedicareAccount CIChar( 10 ) constraint NOT NULL,
      RetirementAccount CIChar( 10 ) constraint NOT NULL,
      FicaPercentage Double( 2 ) constraint NOT NULL,
      MedicarePercentage Double( 2 ) constraint NOT NULL,
      FicaExempt Money constraint NOT NULL,
      MedicareExempt Money constraint NOT NULL,
      FixedDeductionAmount Double( 2 ) constraint NOT NULL) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualBS1',
   'AccrualBS1.adi',
   'PK',
   'EmployeeNumber;GrossAccount',
   '',
   2051,
   512,
   '' );
   
CREATE TABLE AccrualBS2 ( 
      Category CIChar( 12 ),
      EmployeeNumber CIChar( 9 ),
      Account CIChar( 10 ),
      Amount Money) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualBS2',
   'AccrualBS2.adi',
   'PK',
   'EmployeeNumber;Category;Account',
   '',
   2051,
   512,
   '' );    
    
CREATE TABLE AccrualTeamPay1 ( 
      StoreCode CIChar( 3 ) constraint NOT NULL,
      EmployeeNumber CIChar( 9 ) constraint NOT NULL,
      Gross Money constraint NOT NULL,
      PTO Money constraint NOT NULL,
      TotalNonOTPay Money constraint NOT NULL,
      GrossDistributionPercentage Double( 2 ) constraint NOT NULL,
      GrossAccount CIChar( 10 ) constraint NOT NULL,
      PTOAccount CIChar( 10 ) constraint NOT NULL,
      FicaAccount CIChar( 10 ) constraint NOT NULL,
      MedicareAccount CIChar( 10 ) constraint NOT NULL,
      RetirementAccount CIChar( 10 ) constraint NOT NULL,
      FicaPercentage Double( 2 ) constraint NOT NULL,
      MedicarePercentage Double( 2 ) constraint NOT NULL,
      FicaExempt Money constraint NOT NULL,
      MedicareExempt Money constraint NOT NULL,
      FixedDeductionAmount Double( 2 ) constraint NOT NULL) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualTeamPay1',
   'AccrualTeamPay1.adi',
   'PK',
   'EmployeeNumber;GrossAccount',
   '',
   2051,
   512,
   '' ); 
   
   
CREATE TABLE AccrualTeamPay2 ( 
      Category CIChar( 12 ),
      EmployeeNumber CIChar( 9 ),
      Account CIChar( 10 ),
      Amount Money) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualTeamPay2',
   'AccrualTeamPay2.adi',
   'PK',
   'EmployeeNumber;Category;Account',
   '',
   2051,
   512,
   '' );    
   
DROP TABLE AccrualDetail1;  
CREATE TABLE AccrualDetail1 ( 
      StoreCode CIChar( 3 ) constraint NOT NULL,
      lastName cichar(40) constraint NOT NULL,
      EmployeeNumber CIChar( 9 ) constraint NOT NULL,
      Gross Money constraint NOT NULL,
      PTO Money constraint NOT NULL,
      TotalNonOTPay Money constraint NOT NULL,
      GrossDistributionPercentage Double( 2 ) constraint NOT NULL,
      GrossAccount CIChar( 10 ) constraint NOT NULL,
      PTOAccount CIChar( 10 ) constraint NOT NULL,
      FicaAccount CIChar( 10 ) constraint NOT NULL,
      MedicareAccount CIChar( 10 ) constraint NOT NULL,
      RetirementAccount CIChar( 10 ) constraint NOT NULL,
      FicaPercentage Double( 2 ) constraint NOT NULL,
      MedicarePercentage Double( 2 ) constraint NOT NULL,
      FicaExempt Money constraint NOT NULL,
      MedicareExempt Money constraint NOT NULL,
      FixedDeductionAmount Double( 2 ) constraint NOT NULL) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualDetail1',
   'AccrualDetail1.adi',
   'PK',
   'EmployeeNumber;GrossAccount',
   '',
   2051,
   512,
   '' ); 
   
   
CREATE TABLE AccrualDetail2 ( 
      Category CIChar( 12 ),
      EmployeeNumber CIChar( 9 ),
      Account CIChar( 10 ),
      Amount Money) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualDetail2',
   'AccrualDetail2.adi',
   'PK',
   'EmployeeNumber;Category;Account',
   '',
   2051,
   512,
   '' );       