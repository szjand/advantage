/*
    a combination of "Accrual - One Query to Rule Them All.sql" AND
    "accrual = january 2015 - one time fix.sql"
    "AccrualFlatRateGross.sql"
*/


1. 
zFixPYACTGR
   extract FROM db2 AND repopulate zFixPYACTGR
2.   
determine accrual period
per jeri: 11/13/2016 -> 11/30/2016

/*
****
  SELECT max(biweeklypayperiodenddate) from day where biweeklypayperiodenddate < curdate() 
  
  SELECT top 2 biweeklypayperiodenddate from (
    select biweeklypayperiodenddate
	from day
	group by biweeklypayperiodenddate) a where biweeklypayperiodenddate < curdate() ORDER BY biweeklypayperiodenddate DESC
  
3.
generate base rows for Accrual Commissions

    
-- 6/1/16 this IS better but no done yet
-- 8/1/16 adding active filtered out the superfluous adam lindquist ry1 row
-- 04/01/2020, removed dist code BSM FROM the exclusion
jon: John Gardner is included in the accrual information you sent, for the first time
His distribution code is BSM, as is Randy�s and Gayla�s
BSM is excluded from accrual
jeri: Oh shoot.  That is because Randy was on monthly.  It should be included now.

!!! DO NOT just run just the first query IN this script !!!
--< BEGIN ----------------------------------------------------------------
DECLARE @fromdate date;
DECLARE @thrudate date;  
@fromdate = '12/19/2021'; -- last payroll enddate + 1
@thrudate = '12/31/2021'; -- EOM
INSERT INTO AccrualCommissions
SELECT storecode, employeenumber, employeekey, name,
  @fromdate AS FromDate, 
  @thrudate AS ThruDate, 0 AS Amount
FROM edwEmployeeDim 
WHERE currentrow = true
  AND payperiodcode = 'B'
  AND DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', 
    '94', 'SALE','TEAM','USCM','USCD', 'STEV') 
  AND termdate > @thrudate
  AND active = 'active';
  
 -- this gets the hourly folks IN digital that have distcode = SALE
INSERT INTO accrualcommissions     
SELECT storecode, employeenumber, employeekey, name,
  @fromdate AS FromDate, 
  @thrudate AS ThruDate, 0 AS Amount
FROM edwEmployeeDim 
WHERE currentrow = true
  AND payperiodcode = 'B'
  AND DistCode ='SALE'
  AND payrollclass = 'Hourly'
  AND termdate > @thrudate
  AND active = 'active';

/*     
-- this picks up Brooke Sutherland who IS distcode SALE AND Salaried
INSERT INTO accrualcommissions     
SELECT storecode, employeenumber, employeekey, name,
  @fromdate AS FromDate, 
  @thrudate AS ThruDate, 0 AS Amount
FROM edwEmployeeDim 
WHERE currentrow = true
  AND employeenumber = '256845'
  AND payperiodcode = 'B'
  AND DistCode ='SALE'
  AND termdate > @thrudate
  AND active = 'active';   
*/
 
-- 11/01/20 need to ADD sam foster  
-- SELECT * FROM edwEmployeeDim WHERE lastname = 'foster'
-- 08/2/21 ADD preston CLOSE, now IN bdc, salaried but gets bonus
INSERT INTO accrualcommissions     
SELECT storecode, employeenumber, employeekey, name,
  @fromdate AS FromDate, 
  @thrudate AS ThruDate,
  0 AS Amount
FROM edwEmployeeDim 
WHERE currentrow = true
  AND employeenumber = '162983'
  AND payperiodcode = 'B'
  AND DistCode = 'SALE'
  --AND termdate > @thrudate
  AND active = 'active';   
  
--/> END ----------------------------------------------------------------  
  
*/  

--06/01/20 dylanger haley NOT showing up IN AccrualCommisions
SELECT * FROM edwEmployeeDim WHERE firstname = 'dylanger'  -- 163700
SELECT * FROM accrualcommissions WHERE employeenumber = '163700'
-- ah, they changed his dist code


-- 11/01/20 termed employee elisa hale, enter her one time
INSERT INTO accrualcommissions     
SELECT storecode, employeenumber, employeekey, name,
  '10/25/2020' AS FromDate, 
  '10/31/2020' AS ThruDate, 
  0 AS Amount
FROM edwEmployeeDim 
WHERE currentrow = true
  AND employeenumber = '147852'
  AND payperiodcode = 'B'
  AND DistCode = 'SALE'
  --AND termdate > @thrudate
  AND active = 'active';   
  

4.
manually UPDATE AccrualCommisions with info FROM jeri
include the body shop estimator info
sql\postgresq\body_shop_pay_plan\payroll_query.sql 
12/01/2020
depending on what i get FROM jeri/nick
also need to manually enter main shop writer data INTO accrualCommissions

-- a crude quick check of accrual commisions

SELECT b.distcode, a.*, CAST(round(a.amount, 0) as sql_integer)
FROM accrualcommissions a
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.thrudate = '11/30/2021'
AND a.amount <> 0
ORDER BY b.storecode, b.distcode, b.name

-- total BY store/dept
SELECT b.storecode, b.pydept, SUM(a.amount)
FROM accrualcommissions a
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.currentrow = true
WHERE a.thrudate = '03/31/2021'
AND a.amount <> 0
GROUP BY b.storecode, b.pydept
ORDER BY b.storecode, b.pydept


5.
1/30/18
AccrualFlatRateGross.sql
insert honda flat rate techs generated FROM postgres
-- sql/postgresql/honda_main_shop_flat_rate/accrual.sql

5a. check detail techs
-- DROP TABLE #techs_1;
SELECT employee_number, last_name, first_name 
INTO #techs_1
FROM accrual_detail_techs;

-- DROP TABLE #techs_2;
-- select a.storecode, a.employeenumber, a.technumber, b.lastname, b.firstname, b.distcode
SELECT a.employeenumber, b.lastname, b.firstname, a.technumber, b.distcode
INTO #techs_2
FROM dimtech a
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
  AND b.distcode = 'WTEC'
  AND b.currentrow = true
  AND b.payrollclass = 'commission'
  AND b.active = 'active'
  AND b.lastname NOT IN ( 'telken')
WHERE a.active = true
  AND a.techkeythrudate > curdate()
  AND a.flagdept = 'detail'
  AND a.storecode = 'RY1';
  
SELECT *
FROM #techs_1 a
full OUTER JOIN #techs_2 b on a.employee_number = b.employeenumber; 

techs IN accrual_detail_techs:
	  dist code: WTEC
	  payroll class: Commission
	  department: 24
	  job: Detail Tech
	  

-- 06/01/20 ADD those techs missing FROM techs_1 - guessing at rates
INSERT INTO accrual_detail_techs(store_code,employee_number,tech_number,
  last_name,first_name,dist_code,flat_rate,pto_rate)
SELECT storecode,employeenumber,technumber,  
  'Brickson','Nolan','WTEC',14,14
FROM dimtech WHERE technumber = 'r16';

-- AND remove those no longer IN the game
DELETE 
-- SELECT *
FROM accrual_Detail_techs WHERE employee_number IN ('124816')
  


6.
accrual - january 2015 - one time fix.sql


SELECT * FROM edwEmployeeDim WHERE name LIKE '%bethany%'

7. check pycntrl for 2019 data, needed for accrual - january 2015 - one time fix.sql
   AND accrualFlatRateGross.sql
   03/1/21 checked, nothing has changed thru 2021
   
select DISTINCT ycyy FROM stgArkonaPYCNTRL    