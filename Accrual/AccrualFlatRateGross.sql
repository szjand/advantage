/*
DROP TABLE AccrualFlatRateGrossPerDay;
CREATE TABLE AccrualFlatRateGrossPerDay (
  employeenumber cichar(9) constraint NOT NULL,
  lastname cichar(25) constraint NOT NULL,
  firstname cichar(25) constraint NOT NULL,
  fromdate date constraint NOT NULL,
  thrudate date constraint NOT NULL,
  gross_per_day double  constraint NOT NULL default '0',
  constraint pk primary key(employeenumber,fromdate)) IN database;

DROP TABLE AccrualFlatRateGross; 
CREATE TABLE AccrualFlatRateGross (
  storecode cichar(3) constraint NOT NULL,
  employeenumber cichar(9) constraint NOT NULL,
  lastname cichar(25) constraint NOT NULL,
  firstname cichar(25) constraint NOT NULL,
  fromdate date constraint NOT NULL,
  thrudate date constraint NOT NULL,
  gross double  constraint NOT NULL default '0',
  distcode cichar(4) constraint NOT NULL,
  constraint pk primary key(employeenumber,fromdate)) IN database;  
  
CREATE TABLE AccrualFlatRate1 ( 
      StoreCode CIChar( 3 ) constraint NOT NULL,
      EmployeeNumber CIChar( 9 ) constraint NOT NULL,
      Gross Money constraint NOT NULL,
      PTO Money constraint NOT NULL,
      TotalNonOTPay Money constraint NOT NULL,
      GrossDistributionPercentage Double( 2 ) constraint NOT NULL,
      GrossAccount CIChar( 10 ) constraint NOT NULL,
      PTOAccount CIChar( 10 ) constraint NOT NULL,
      FicaAccount CIChar( 10 ) constraint NOT NULL,
      MedicareAccount CIChar( 10 ) constraint NOT NULL,
      RetirementAccount CIChar( 10 ) constraint NOT NULL,
      FicaPercentage Double( 2 ) constraint NOT NULL,
      MedicarePercentage Double( 2 ) constraint NOT NULL,
      FicaExempt Money constraint NOT NULL,
      MedicareExempt Money constraint NOT NULL,
      FixedDeductionAmount Double( 2 ) constraint NOT NULL) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualFlatRate1',
   'AccrualFlatRate1.adi',
   'PK',
   'EmployeeNumber;GrossAccount',
   '',
   2051,
   512,
   '' ); 
      
CREATE TABLE AccrualFlatRate2 ( 
      Category CIChar( 12 ),
      EmployeeNumber CIChar( 9 ),
      Account CIChar( 10 ),
      Amount Money) IN DATABASE;
EXECUTE PROCEDURE sp_CreateIndex90( 
   'AccrualFlatRate2',
   'AccrualFlatRate2.adi',
   'PK',
   'EmployeeNumber;Category;Account',
   '',
   2051,
   512,
   '' );    
*/
/* 
team pay can be the MAX of the previous 3 pay periods FROM tpData
detail will have to be based on previous 3 paychecks 
*/
/*
-- *b* Oct accrual full pay period, no need to average
-- *c* need to include brian peterson
-- *d* Nov full pay period + 2 days
-- *e* 8/1/16 fucked up with a rehire tech IN detail, Jason Olson
-- *f*
  need to separate out gross AND pto
  gross IS estimated, pto IS NOT
  for AccrualFlatRateGross, will need to ADD a pto COLUMN
  
  shit, looks LIKE a separate calculation for gross USING 3 pay periods, vs pto which IS just
  current pay period
  
  pto will just be actual pto pay (hol+vac+pto) for the days IN the accrual range 
  
  ALTER TABLE AccrualFlatRateGross
  ADD COLUMN ptopay double;  
  
3/1/17  
 *x*
Constraint violation while updating column "FicaPercentage".  The column cannot be updated to a NULL value
    AccrualFlatRate1
	    forgot to DO zfixpyactgr, which IS true 
		but fuck me, no 2017 values IN stgArkonaPYNCTRL???
		fixed: inserted 2017 values INTO stgArkonaPYCNTRL		
    
01-30-18
  detail IS now generated IN this script    
02/01/18
  *x* again, no 2018 values IN stgArkonaPYCNTRL
7/2/18
  honda techs, manually generate AND INSERT the INSERT statements INTO this script  
  
12/2/19
  *k*
  November was a 3 payroll month, which highlited to jeri that detail accrual IS fucked up
  no accrual for detail flat rate techs
  discoverd that IN the detail section for some unknown reason, i commented out the
  INSERT statement, fixed that
  took the opportunity to UPDATE the accrual_detail_techs TABLE, currently only have 4
       techs, there are way more than that
       see ..scripts/accrual/detail/detail_201911.sql
    
*/
DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromdate = '12/19/2021';
@thrudate = '12/31/2021';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);
DELETE FROM AccrualFlatRateGross;    

-- honda flat rate techs generated FROM postgres
-- E:\sql\postgresql\honda_main_shop_flat_rate\accrual.sql

insert into AccrualFlatRateGross values ('RY2','276431','Engebretson','Erik','2021-12-19','2021-12-31',1758.04,'TECH',308.16);
insert into AccrualFlatRateGross values ('RY2','273315','Johnson','Brandon','2021-12-19','2021-12-31',2030.80,'TECH',1117.92);
insert into AccrualFlatRateGross values ('RY2','279630','Knudson','Kenneth','2021-12-19','2021-12-31',2321.78,'TECH',516.16);
insert into AccrualFlatRateGross values ('RY2','2130150','Sobolik','Paul','2021-12-19','2021-12-31',2826.44,'TECH',761.80);

-- 09/04/21 Brian Peterson has NOT been IN an accrual since 2015
-- handle it the same way i am doing honda techs
-- this statement generated IN sql/advantage/accrual/brian_peterson.sql
-- insert into AccrualFlatRateGross values ('RY1','1110425','Peterson','Brian','2021-08-15','2021-08-31',3014.84,'BTEC',644.96);
-- insert into AccrualFlatRateGross values ('RY1','1110425','Peterson','Brian','2021-09-12','2021-09-30',5584.1,'BTEC',0);
-- 11/01/21 Brian Peterson IS no a team of one AND IS picked up with the rest of the techs


INSERT INTO AccrualFlatRateGross   
-- *f*
SELECT e.*, coalesce(f.ptopay, 0) AS ptopay
FROM (	
  SELECT storecode, employeenumber, lastname, firstname, @fromDate, @thruDate, 
  -- *f*
  --  @days * max(round((commissionpay + ptopay)/10, 2)), distcode 
    @days * max(round(commissionpay/10, 2)) AS gross, distcode 
  FROM (    
    select aa.storecode, a.employeenumber, a.lastname, a.firstname, 
      round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2) AS commissionPay,
      round(techHourlyRate * (techvacationhourspptd + techptohourspptd + techholidayhourspptd), 2) AS ptoPay,
      aa.distcode
    FROM scotest.tpdata a
    LEFT JOIN edwEmployeeDim aa on a.employeenumber = aa.employeenumber
      AND aa.currentrow = true
    INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey  
    WHERE thedate = payperiodend
      AND payperiodseq IN ( -- last 3 full pay periods
        SELECT top 3 DISTINCT payperiodseq
        FROM scotest.tpdata
        WHERE payperiodend <= @thrudate
        ORDER BY payperiodseq DESC)) x    
  WHERE EXISTS ( -- currently a team pay tech
    SELECT 1
    FROM scotest.tpdata
    WHERE employeenumber = x.employeenumber
      AND thedate = @thrudate)    
  OR (x.lastname = 'peterson' AND x.firstname = 'brian')      
  GROUP BY storecode, employeenumber, lastname, firstname, distcode) e
-- *f*  
LEFT JOIN ( -- pto
  select a.employeenumber, 
    sum(round(techHourlyRate * (techvacationhoursday + techptohoursday + techholidayhoursday), 2)) AS ptoPay
  FROM scotest.tpdata a
  LEFT JOIN edwEmployeeDim aa on a.employeenumber = aa.employeenumber
    AND aa.currentrow = true
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey  
  WHERE thedate BETWEEN @fromdate AND @thrudate
  GROUP BY a.employeenumber) f on e.employeenumber = f.employeenumber;  

/*
detail
1-30-18
flag date per john gardner
7/2 Darrick Richardson IS salaried
9/3/19
delete FROM accrual_detail_techs WHERE employee_number = '1135710';
*k*

1/2/20: gross could be NULL, but tech has pto: coalesce the gross
*/
INSERT INTO AccrualFlatRateGross 
SELECT aa.store_code, aa.employee_number, aa.last_name, aa.first_name,
  @fromdate, @thrudate, 
  coalesce(
    round(
      CASE
        WHEN aa.last_name <> 'richardson' THEN flat_rate * flag_hours
--      WHEN aa.last_name = 'richardson' THEN 
--        1.5 * ((reg_hours * flat_rate) + (1.5 * (ot_hours * pto_rate)))
      END, 2), 0) AS gross,
  aa.dist_code,
  pto_hours * pto_rate AS pto       
FROM accrual_detail_techs aa
LEFT JOIN ( -- flag hours
  SELECT d.employee_number, SUM(a.flaghours) AS flag_hours
  FROM factrepairorder a
  INNER JOIN day b on a.flagdatekey = b.datekey
  INNER JOIN dimtech c on a.techkey = c.techkey
  INNER JOIN accrual_detail_techs d on c.employeenumber = d.employee_number
  WHERE b.thedate BETWEEN @fromdate AND @thrudate
  GROUP BY d.employee_number) bb on aa.employee_number = bb.employee_number
left join (-- clock hours
  SELECT e.employee_number, SUM(a.regularhours) AS reg_hours, 
    SUM(a.overtimehours) AS ot_hours,
    SUM(vacationhours + ptohours + holidayhours) AS pto_hours
  FROM edwClockHoursFact a
  INNER JOIN day b on a.datekey = b.datekey
    AND b.thedate BETWEEN @fromdate and @thrudate
  INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey
  INNER JOIN accrual_detail_techs e on c.employeenumber = e.employee_number
  GROUP BY e.employee_number) cc on aa.employee_number = cc.employee_number;
   

   
DELETE FROM AccrualFlatRate1;
INSERT INTO AccrualFlatRate1
-- *f*
-- SELECT d.storecode, d.employeenumber, d.gross, 0 AS pto,
SELECT d.storecode, d.employeenumber, d.gross, ptopay AS pto,
  d.gross AS TotalNonOTPay,
  f.GROSS_DIST, f.GROSS_EXPENSE_ACT_, f.SICK_LEAVE_EXPENSE_ACT_,  
  f.EMPLR_FICA_EXPENSE, f.EMPLR_MED_EXPENSE, f.EMPLR_CONTRIBUTIONS, 
  g.yficmp, g.ymedmp,
  coalesce(h.FicaEx, 0) AS FicaEx,
  coalesce(i.MedEx, 0) AS MedEx,
  coalesce(j.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
FROM AccrualFlatRateGross d      
LEFT JOIN zFixPyactgr f on d.storecode = f.company_number
  AND d.distcode = f.dist_code    
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL g ON d.storecode = g.yco#
-- *x*  
--  AND g.ycyy = 100 + (year(@thruDate) - 2000) -- 112 -   
  AND g.ycyy = 117 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) h ON d.employeenumber = h.EMPLOYEE_NUMBER 
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) i ON d.employeenumber = i.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT j ON d.storecode = j.COMPANY_NUMBER
  AND d.employeenumber = j.EMPLOYEE_NUMBER 
--  AND h.DED_PAY_CODE IN ('91', '99');
-- *666
  AND 
    case 
      when j.employee_number = '11650' then j.ded_pay_code = '99'
      else j.DED_PAY_CODE = '91'
    END;

DELETE FROM AccrualFlatRate2;   
INSERT INTO AccrualFlatRate2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * Gross AS Amount
FROM AccrualFlatRate1
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTO AS Amount
FROM AccrualFlatRate1
WHERE PTO <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2)) AS Amount
FROM AccrualFlatRate1
GROUP BY employeenumber, FicaAccount
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2)) AS Amount
FROM AccrualFlatRate1
GROUP BY employeenumber, MedicareAccount
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  sum(
    CASE 
      WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
      CASE 
        WHEN TotalNonOTPay * FixedDeductionAmount/200.0  < .02 * TotalNonOTPay
          THEN round(GrossDistributionPercentage/100.0 * TotalNonOTPay * FixedDeductionAmount/200.0, 2)
        ELSE round(GrossDistributionPercentage/100.0 * .02 * TotalNonOTPay, 2) 
      END 
    END) AS Amount     
FROM AccrualFlatRate1
GROUP BY employeenumber, RetirementAccount;
 

