
DECLARE @fromDate date;
DECLARE @thruDate date;
DECLARE @theDate string;
@fromDate = '03/22/2015'; 
@thrudate = '03/31/2015';
@theDate = '03/31/2015';
DELETE FROM AccrualDates;
INSERT INTO AccrualDates 
SELECT 'GLI', @theDate,
  'GLI' + 
    CASE 
      WHEN month(@thrudate) < 10 THEN '0' +  trim(CAST(month(@thrudate) AS sql_char)) 
      ELSE trim(CAST(month(@thrudate) AS sql_char))
    END  +
  trim(CAST(dayofmonth(@thrudate) AS sql_char)) +
  right(trim(CAST(year(@thrudate) AS sql_char)), 2),
  'GLI' +
    CASE 
      WHEN month(@thrudate) < 10 THEN '0' +  trim(CAST(month(@thrudate) AS sql_char)) 
      ELSE trim(CAST(month(@thrudate) AS sql_char))
    END  +
  trim(CAST(dayofmonth(@thrudate) AS sql_char)) +
  right(trim(CAST(year(@thrudate) AS sql_char)), 2),  
  'Payroll Accrual', @fromdate,@thrudate, 
  timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1,
  (timestampdiff(sql_tsi_day, @FromDate, @ThruDate) + 1)/14.0
FROM system.iota;  

DELETE FROM AccrualSalaried1;
INSERT INTO AccrualSalaried1
SELECT a.storecode, a.employeenumber, 
  a.salary * aa.NumDays14 AS TotalPay,
  d.GROSS_DIST, d.GROSS_EXPENSE_ACT_, 
  d.EMPLR_FICA_EXPENSE, d.EMPLR_MED_EXPENSE, d.EMPLR_CONTRIBUTIONS,
  e.yficmp, e.ymedmp, coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
  coalesce(h.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
FROM edwEmployeeDim a
INNER JOIN AccrualDates aa on 1 = 1
INNER JOIN ( -- the possibly dodgy assumption here IS that employeekeys are sequential, but they are autoinc 
  SELECT a.Employeenumber, MAX(a.employeekey) AS employeekey
  FROM edwEmployeeDim a
  INNER JOIN AccrualDates aa on 1 = 1
  INNER JOIN edwEmployeeDim b on a.employeekey =  b.employeekey -- ALL employeekeys valid for the interval
    AND b.employeekeyFromDate < aa.thrudate
    AND b.employeekeyThruDate > aa.fromdate
  WHERE a.hiredate <= aa.thrudate -- only folks active during the interval
    AND a.termdate >= aa.fromdate
    AND a.storecode <> 'ry3'  
    AND a.employeenumber NOT IN ('190915','1160100')
    AND a.PayPeriodCode = 'B'
    AND a.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 
      'BSM','SALE','TEAM','USCM','USCD', 'STEV', 'AFTE') 
    AND a.PayRollClassCode = 'S' -- salaried only
  GROUP BY a.employeenumber) x on a.employeekey = x.employeekey
LEFT JOIN zFixPyactgr d on a.storecode = d.company_number
  AND a.distcode = d.dist_code  
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL e ON a.storecode = e.yco#
  AND e.ycyy = 100 + (year(aa.ThruDate) - 2000) -- 112 -  
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) f ON a.employeenumber = f.EMPLOYEE_NUMBER    
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) g ON a.employeenumber = g.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT h ON a.storecode = h.COMPANY_NUMBER
  AND a.employeenumber = h.EMPLOYEE_NUMBER 
  AND h.DED_PAY_CODE IN ('91', '99');  
 
  
DELETE FROM AccrualSalaried2;  
INSERT INTO AccrualSalaried2
SELECT 'Gross', EmployeeNumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * TotalPay AS Amount
FROM AccrualSalaried1
UNION ALL 
SELECT 'FICA', EmployeeNumber, FicaAccount AS Account,
  round((GrossDistributionPercentage/100.0) * (TotalPay - (coalesce(FicaExempt, 0)) * b.NumDays14) * FicaPercentage/100.0, 2) AS Amount
FROM AccrualSalaried1 a
INNER JOIN AccrualDates b on 1 = 1
UNION ALL 
SELECT 'MEDIC', EmployeeNumber, MedicareAccount AS Account,
  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * b.NumDays14) * MedicarePercentage/100.0, 2) AS Amount
FROM AccrualSalaried1 a
INNER JOIN AccrualDates b on 1 = 1
UNION ALL 
SELECT 'RETIRE', EmployeeNumber, RetirementAccount AS Account,
CASE 
  WHEN FixedDeductionAmount IS NULL THEN 0
  ELSE 
  CASE 
    WHEN (TotalPay * FixedDeductionAmount/200.0) < .02 * TotalPay 
      THEN round((GrossDistributionPercentage/100.0 * TotalPay) * (FixedDeductionAmount/200.0), 2)
    ELSE round((GrossDistributionPercentage/100.0) * TotalPay * .02, 2) 
  END               
END AS Amount  
FROM AccrualSalaried1;

-- this IS WHERE any employee that was salaried for the interval IS exempted
DELETE FROM AccrualClockHours;
INSERT INTO AccrualClockHours
SELECT c.employeenumber, SUM(regularHours) AS regularHours, 
  SUM(OvertimeHours) AS Overtimehours, SUM(VacationHours) AS VacationHours,
  SUM(PTOHours) AS PTOHours, SUM(HolidayHours) AS HolidayHours
FROM edwClockHoursFact a
INNER JOIN AccrualDates aa on 1 = 1
INNER JOIN day b on a.datekey = b.datekey
  AND b.thedate BETWEEN aa.fromdate AND aa.thrudate //(SELECT FromDate FROM AccrualDates) AND (SELECT ThruDate FROM AccrualDates)
INNER JOIN edwEmployeeDim c on a.employeekey = c.employeekey  
WHERE regularhours + overtimehours + vacationhours + ptohours + holidayhours > 0
  AND NOT EXISTS ( -- emp# NOT IN #salaried
    SELECT 1
    FROM AccrualSalaried1
    WHERE employeenumber = c.employeenumber)
GROUP BY c.employeenumber, c.name;

DELETE FROM AccrualHourly1;
INSERT INTO AccrualHourly1  
SELECT b.storecode, b.employeenumber, 
  RegularHours*HourlyRate as RegularPay, 
  OvertimeHours*HourlyRate*1.5 AS OvertimePay, VacationHours*HourlyRate AS VacationPay,
  PTOHours*HourlyRate AS PTOPay, HolidayHours*HourlyRate AS HolidayPay, 
  RegularHours*HourlyRate + VacationHours*HourlyRate + PTOHours*HourlyRate + HolidayHours*HourlyRate AS TotalNonOTPay,  
  d.GROSS_DIST, d.GROSS_EXPENSE_ACT_, d.OVERTIME_ACT_, 
  d.VACATION_EXPENSE_ACT_, d.HOLIDAY_EXPENSE_ACT_, d.SICK_LEAVE_EXPENSE_ACT_,
  d.EMPLR_FICA_EXPENSE, d.EMPLR_MED_EXPENSE, d.EMPLR_CONTRIBUTIONS,
  e.yficmp, e.ymedmp, coalesce(f.FicaEx, 0) AS FicaEx, coalesce(g.MedEx, 0) AS MedEx,
  coalesce(h.FIXED_DED_AMT, 0) AS FIXED_DED_AMT      
FROM AccrualClockHours a
INNER JOIN AccrualDates aa on 1 = 1
INNER JOIN edwEmployeeDim b on a.employeenumber = b.employeenumber
INNER JOIN (
  SELECT a.Employeenumber, MAX(a.employeekey) AS employeekey
  FROM edwEmployeeDim a
  INNER JOIN AccrualDates aa on 1 = 1
  INNER JOIN edwEmployeeDim b on a.employeekey =  b.employeekey -- ALL employeekeys valid for the interval
    AND b.employeekeyFromDate < aa.ThruDate //(SELECT ThruDate FROM AccrualDates)
    AND b.employeekeyThruDate > aa.FromDate //(SELECT FromDate FROM AccrualDates)
  WHERE a.hiredate <= aa.ThruDate //(SELECT ThruDate FROM AccrualDates) -- only folks active during the interval
    AND a.termdate >= aa.FromDate //(SELECT FromDate FROM AccrualDates)
    AND a.storecode <> 'ry3'  
    AND a.employeenumber NOT IN ('190915','1160100')
    AND a.PayPeriodCode = 'B'
    AND a.DistCode NOT IN ('22', '25', '26', '29', '29G', '44', '84', '905', '94', 
      'BSM','SALE','TEAM','USCM','USCD', 'STEV', 'AFTE') 
    AND a.PayRollClassCode = 'H' -- salaried only
  GROUP BY a.employeenumber) c on b.employeekey = c.employeekey  
LEFT JOIN zFixPyactgr d on b.storecode = d.company_number
  AND b.distcode = d.dist_code    
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL e ON b.storecode = e.yco#
  AND e.ycyy =  100 + (year(aa.ThruDate) - 2000) 
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) f ON b.employeenumber = f.EMPLOYEE_NUMBER    
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) g ON b.employeenumber = g.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT h ON b.storecode = h.COMPANY_NUMBER
  AND b.employeenumber = h.EMPLOYEE_NUMBER 
  AND 
    case 
      when h.employee_number = '11650' then h.ded_pay_code = '99'
      else h.DED_PAY_CODE = '91'
    END; 
       
--< team pay / flat rate adjustment -------------------------------------------<       

/*  
team pay adjustment 

*/
UPDATE accrualHourly1
SET regularPay = x.commissionPay,
    overtimePay = 0
FROM accrualTpAdjustment x   
WHERE accrualHourly1.employeenumber = x.employeenumber
  AND monthEndDate = @thruDate;

UPDATE accrualHourly1
SET totalNonOtPay =  regularPay + vacationpay + ptopay + holidaypay
WHERE employeenumber IN (
  SELECT employeenumber 
  FROM accrualTpAdjustment
  WHERE monthEndDate = @thruDate);

--/> team pay / flat rate adjustment ------------------------------------------/> 

DELETE FROM AccrualHourly2;  
INSERT INTO AccrualHourly2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * RegularPay AS Amount
FROM AccrualHourly1
UNION ALL
SELECT 'OT', employeenumber, OvertimeAccount AS Account,
  GrossDistributionPercentage/100.0 * OvertimePay AS Amount
FROM AccrualHourly1
WHERE OvertimePay <> 0
UNION ALL 
SELECT 'VAC', employeenumber, VacationAccount AS Account,
  GrossDistributionPercentage/100.0 * VacationPay AS Amount
FROM AccrualHourly1
WHERE VacationPay <> 0  and employeenumber <> '131500'
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTOPay AS Amount
FROM AccrualHourly1
WHERE PTOPay <> 0
UNION ALL 
SELECT 'HOL', employeenumber, HolidayAccount AS Account,
  GrossDistributionPercentage/100.0 * HolidayPay AS Amount
FROM AccrualHourly1
WHERE HolidayPay <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * (TotalNonOTPay + OvertimePay)) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2)) AS Amount
FROM AccrualHourly1
GROUP BY employeenumber, FicaAccount
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * (TotalNonOTPay + OvertimePay)) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2)) AS Amount
FROM AccrualHourly1
GROUP BY employeenumber, MedicareAccount
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  sum(CASE 
    WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
    
    CASE 
      WHEN (TotalNonOTPay + OvertimePay) * FixedDeductionAmount/200.0  < .02 * (TotalNonOTPay + OvertimePay) 
        THEN round(GrossDistributionPercentage/100.0 * (TotalNonOTPay + OvertimePay) * FixedDeductionAmount/200.0, 2)
      ELSE round(GrossDistributionPercentage/100.0 * .02 * (TotalNonOTPay + OvertimePay), 2) 
    END 
  END) AS Amount     
FROM AccrualHourly1
GROUP BY employeenumber, RetirementAccount;

DELETE FROM AccrualCommissions2;
INSERT INTO AccrualCommissions2
SELECT 'Gross', EmployeeNumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * TotalPay AS Amount
FROM (
  SELECT a.storecode, a.amount AS TotalPay, c.*
  FROM AccrualCommissions a
  INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
  LEFT JOIN (
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualhourly1
    UNION
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
  WHERE a.amount <> 0) a
UNION ALL 
SELECT 'FICA', EmployeeNumber, FicaAccount AS Account,
  round((GrossDistributionPercentage/100.0) * (TotalPay - (coalesce(FicaExempt, 0)) * (select NumDays/14 FROM AccrualDates)) * FicaPercentage/100.0, 2) AS Amount
FROM (
  SELECT a.storecode, a.amount AS TotalPay, c.*
  FROM AccrualCommissions a
  INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
  LEFT JOIN (
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualhourly1
    UNION
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
  WHERE a.amount <> 0) b
UNION ALL 
SELECT 'MEDIC', EmployeeNumber, MedicareAccount AS Account,
--  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * (NumDays/14)) * MedicarePercentage/100.0, 2) AS Amount
  round((GrossDistributionPercentage/100.0) *  (TotalPay - (coalesce(MedicareExempt, 0)) * (select NumDays/14 FROM AccrualDates)) * MedicarePercentage/100.0, 2) AS Amount
FROM (
  SELECT a.storecode, a.amount AS TotalPay, c.*
  FROM AccrualCommissions a
  INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
  LEFT JOIN (
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualhourly1
    UNION
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
  WHERE a.amount <> 0) c
UNION ALL 
SELECT 'RETIRE', EmployeeNumber, RetirementAccount AS Account,
CASE 
  WHEN FixedDeductionAmount IS NULL THEN 0
  ELSE 
  CASE 
    WHEN (TotalPay * FixedDeductionAmount/200.0) < .02 * TotalPay 
      THEN round((GrossDistributionPercentage/100.0 * TotalPay) * (FixedDeductionAmount/200.0), 2)
    ELSE round((GrossDistributionPercentage/100.0) * TotalPay * .02, 2) 
  END               
END AS Amount  
FROM (
SELECT a.storecode, a.amount AS TotalPay, c.*
  FROM AccrualCommissions a
  INNER JOIN accrualdates b on a.fromdate = b.fromdate AND a.thrudate = b.thrudate
  LEFT JOIN (
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualhourly1
    UNION
    SELECT Employeenumber, GrossDistributionPercentage, GrossAccount, FicaAccount, MedicareAccount, 
      RetirementAccount, FicaPercentage, MedicarePercentage, FicaExempt, 
      MedicareExempt, FixedDeductionAmount 
    FROM accrualsalaried1) c on a.employeenumber = c.employeenumber
  WHERE a.amount <> 0) d; 

DELETE FROM AccrualFileForJeri;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, account, employeenumber AS control, document, reference, 
  round(cast(a.amount AS sql_double), 2) AS amount, description 
FROM ( 
  SELECT employeenumber, account, SUM(amount) AS amount
  FROM ( 
    SELECT * FROM accrualsalaried2 WHERE amount <> 0
    UNION all
    SELECT * FROM accrualhourly2 WHERE amount <> 0  
    UNION all
    SELECT * FROM accrualcommissions2 WHERE amount <> 0) a
-- year END only WHEN sales IS included    
--    UNION ALL -- year END only WHEN sales IS included
--    SELECT * FROM accrualsales2 WHERE amount <> 0) a
  GROUP BY employeenumber, account) a, AccrualDates b;
  
-- Greg 9/4/12 changed accounts per Jeri
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12001', '1130426', document, reference, 
  round(1415.40 * NumDays14, 2), description 
FROM AccrualDates;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12002', '1130426', document, reference, 
  round(1569.23 * NumDays14, 2), description 
FROM AccrualDates;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12004', '1130426', document, reference, 
  round(1569.23 * NumDays14, 2), description 
FROM AccrualDates;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12005', '1130426', document, reference, 
  round(1569.23 * NumDays14, 2), description 
FROM AccrualDates;
INSERT INTO AccrualFileForJeri
SELECT journal, thedate, '12006', '1130426', document, reference, 
  round(1569.23 * NumDays14, 2), description 
FROM AccrualDates;
  
-- reversing entries
INSERT INTO AccrualFileForJeri
SELECT e.journal, e.thedate, d.Account, e.document, e.document, e.reference, 
  round(d.Amount, 2), e.description
FROM (  
  SELECT 
    CASE LEFT(Account, 1)
      WHEN '1' THEN '132101'
      WHEN '2' THEN '232103'
    END AS Account, 
  SUM(amount) * -1 AS Amount
  FROM (
    SELECT employeenumber, account, SUM(amount) AS amount
    FROM ( 
      SELECT * FROM accrualsalaried2 WHERE amount <> 0
      UNION all
      SELECT * FROM accrualhourly2 WHERE amount <> 0  
      UNION all
      SELECT * FROM accrualcommissions2 WHERE amount <> 0) a  
    GROUP BY employeenumber, account) c
  GROUP BY LEFT(Account, 1)) d, AccrualDates e;
  
-- AND greg reversing entries  
UPDATE AccrualFileForJeri
SET amount = amount - (1415.40 + 4*1569.23)
WHERE account = '132101';
 
SELECT * FROM AccrualFileForJeri;
