/*
ALTER TABLE zfix_AccrualFlatRateGross
ADD COLUMN ptopay double;
*/
DECLARE @days integer;
DECLARE @fromDate date;
DECLARE @thruDate date;
@fromdate = '12/25/2016';
@thrudate = '12/31/2016';
@days = (
  SELECT COUNT(*) 
  FROM day 
  WHERE dayofweek BETWEEN 2 AND 6
    AND thedate BETWEEN @fromDate AND @thruDate
    AND holiday = false);
-- DELETE FROM AccrualFlatRateGross;    
-- INSERT INTO AccrualFlatRateGross   
DELETE FROM zfix_AccrualFlatRateGross;
INSERT INTO zfix_AccrualFlatRateGross
/*
need to separate out gross AND pto
gross IS estimated, pto IS NOT
shit WHILE i am at it, looks LIKE i should be including bonus pay (main shop techs) AS well AS part of gross
nah, forget the bonus
for AccrualFlatRateGross, will need to ADD a pto COLUMN

shit, looks LIKE a separate calculation for gross USING 3 pay periods, vs pto which IS just
current pay period

pto will just be pto pay (hol+vac+pto) for the days IN the accrual range
*/
-- team pay techs (plus brian peterson: body shop pdr)
SELECT e.*, coalesce(f.ptopay, 0) AS ptopay
FROM (	
  SELECT storecode, employeenumber, lastname, firstname, @fromDate, @thruDate, 
    @days * max(round(commissionpay/10, 2)) AS gross, distcode 
  FROM (    
    select aa.storecode, a.employeenumber, a.lastname, a.firstname, 
      round(techtfrrate*teamprofpptd*techclockhourspptd/100, 2) AS commissionPay,
      aa.distcode
    FROM scotest.tpdata a
    LEFT JOIN edwEmployeeDim aa on a.employeenumber = aa.employeenumber
      AND aa.currentrow = true
    INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
      AND a.teamKey = b.teamkey  
    WHERE thedate = payperiodend--) x
      AND payperiodseq IN ( -- last 3 full pay periods
        SELECT top 3 DISTINCT payperiodseq
        FROM scotest.tpdata
        WHERE payperiodend <= @thrudate
        ORDER BY payperiodseq DESC)) x    
  WHERE EXISTS ( -- currently a team pay tech
    SELECT 1
    FROM scotest.tpdata
    WHERE employeenumber = x.employeenumber
      AND thedate = @thrudate)    
  OR (x.lastname = 'peterson' AND x.firstname = 'brian')      
  GROUP BY storecode, employeenumber, lastname, firstname, distcode) e
LEFT JOIN ( -- pto
  select a.employeenumber, 
    sum(round(techHourlyRate * (techvacationhoursday + techptohoursday + techholidayhoursday), 2)) AS ptoPay
  FROM scotest.tpdata a
  LEFT JOIN edwEmployeeDim aa on a.employeenumber = aa.employeenumber
    AND aa.currentrow = true
  INNER JOIN scotest.tpteamtechs b on a.techkey = b.techkey
    AND a.teamKey = b.teamkey  
  WHERE thedate BETWEEN @fromdate AND @thrudate
  GROUP BY a.employeenumber) f on e.employeenumber = f.employeenumber;

-- detail
-- INSERT INTO AccrualFlatRateGross 
INSERT INTO zfix_AccrualFlatRateGross 
SELECT 'RY1', employeenumber, lastname, firstname, @fromdate, @thrudate,  
  coalesce(round((12 * flaghours) + ((rate - 12) * flaghours), 2), 0), 'WTEC',
  coalesce(round(12 * ptohours, 2), 0)  AS ptopay
FROM (
  SELECT m.*, n.flaghours, round(flaghours/clockhours, 2) AS prof,
  -- hard code the prof matrix for now
    CASE
-- *a*  
      WHEN lastname IN ('Telken','Kingbird','Roehrich'/*,'Gonzalez'*/) THEN 12.0
      WHEN lastname = 'Nelson' AND firstname = 'Hunter' THEN 12.0   
      WHEN lastname = 'Horton' AND firstname = 'Eric' THEN 12.0
      WHEN lastname = 'Nelson' AND firstname = 'Kordell' THEN 12.0
      WHEN round(flaghours/clockhours, 2) <= 1.3 THEN 12.0   
      WHEN round(flaghours/clockhours, 2) >= 1.3 AND round(flaghours/clockhours, 2) < 1.4 THEN 12.5
      WHEN round(flaghours/clockhours, 2) >= 1.4 AND round(flaghours/clockhours, 2) < 1.5 THEN 13.5
      WHEN round(flaghours/clockhours, 2) >= 1.5 AND round(flaghours/clockhours, 2) < 1.6 THEN 15
      WHEN round(flaghours/clockhours, 2) >= 1.6 AND round(flaghours/clockhours, 2) < 1.7 THEN 16
      WHEN round(flaghours/clockhours, 2) >= 1.7 AND round(flaghours/clockhours, 2) < 1.8 THEN 17
      WHEN round(flaghours/clockhours, 2) >= 1.8 AND round(flaghours/clockhours, 2) < 1.9 THEN 18
      WHEN round(flaghours/clockhours, 2) >= 1.9 AND round(flaghours/clockhours, 2) < 2 THEN 19
      WHEN round(flaghours/clockhours, 2) > 2 THEN 20.0  
    END AS rate
  FROM (
    SELECT a.firstname, a.lastname, a.employeenumber, a.employeekey, 
      SUM(b.clockhours) AS clockhours, SUM(holidayhours+ptohours+vacationhours) AS ptohours,
      c.technumber, c.techkey
    FROM edwEmployeeDim a 
    LEFT JOIN edwClockHoursFact b on a.employeekey = b.employeekey
      AND b.datekey IN (
        SELECT datekey
        FROM day
        WHERE thedate BETWEEN @fromdate AND @thrudate)
    LEFT JOIN dimtech c on a.employeenumber = c.employeenumber
-- *e*
      AND c.active = true    
      AND c.currentrow = true    
    WHERE a.distcode = 'wtec'
  --    AND a.currentrow = true
      AND a.active = 'active'
      AND a.employeekeyfromdate <= @thrudate
      AND a.employeekeythrudate > @fromdate
      AND a.payrollclass = 'commission'
    GROUP BY a.firstname, a.lastname, a.employeenumber, a.employeekey, c.technumber, c.techkey) m   
  LEFT JOIN (
    SELECT d.technumber, d.description, d.employeenumber, SUM(flaghours) AS flaghours
    FROM factrepairorder a
    INNER JOIN day b on a.closedatekey = b.datekey
    INNER JOIN dimtech d on a.techkey = d.techkey
    WHERE b.thedate between @fromdate AND @thrudate
      AND d.flagdeptcode = 're'
      AND d.employeenumber <> 'NA'
    GROUP BY d.technumber, d.description, d.employeenumber) n on m.employeenumber = n.employeenumber  ) x;  

--DELETE FROM AccrualFlatRate1;
--INSERT INTO AccrualFlatRate1
DELETE FROM zfix_AccrualFlatRate1;
INSERT INTO zfix_AccrualFlatRate1
--SELECT d.storecode, d.employeenumber, d.gross, 0 AS pto,
SELECT d.storecode, d.employeenumber, d.gross, ptopay AS pto,
  d.gross AS TotalNonOTPay,
  f.GROSS_DIST, f.GROSS_EXPENSE_ACT_, f.SICK_LEAVE_EXPENSE_ACT_,  
  f.EMPLR_FICA_EXPENSE, f.EMPLR_MED_EXPENSE, f.EMPLR_CONTRIBUTIONS, 
  g.yficmp, g.ymedmp,
  coalesce(h.FicaEx, 0) AS FicaEx,
  coalesce(i.MedEx, 0) AS MedEx,
  coalesce(j.FIXED_DED_AMT, 0) AS FIXED_DED_AMT   
-- FROM AccrualFlatRateGross d      
FROM zfix_AccrualFlatRateGross d  
LEFT JOIN zFixPyactgr f on d.storecode = f.company_number
  AND d.distcode = f.dist_code    
LEFT JOIN -- fica & medicare percentages for store/year
  stgArkonaPYCNTRL g ON d.storecode = g.yco#
  AND g.ycyy = 100 + (year(@thruDate) - 2000) -- 112 -    
LEFT JOIN (-- FICA Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS FicaEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex2 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) h ON d.employeenumber = h.EMPLOYEE_NUMBER 
LEFT JOIN (-- Medicare Exempt deductions, may be NULL, coalesce IN select
  SELECT EMPLOYEE_NUMBER, SUM(FIXED_DED_AMT) AS MedEx
  FROM stgArkonaPYDEDUCT x
  WHERE EXISTS (
    SELECT 1
    FROM stgArkonaPYPCODES
    WHERE ytddcd = x.DED_PAY_CODE
    AND ytdex6 = 'Y')
  GROUP BY EMPLOYEE_NUMBER) i ON d.employeenumber = i.EMPLOYEE_NUMBER  
LEFT JOIN -- retirement deductions
  stgArkonaPYDEDUCT j ON d.storecode = j.COMPANY_NUMBER
  AND d.employeenumber = j.EMPLOYEE_NUMBER 
--  AND h.DED_PAY_CODE IN ('91', '99');
-- *666
  AND 
    case 
      when j.employee_number = '11650' then j.ded_pay_code = '99'
      else j.DED_PAY_CODE = '91'
    END;	
	
	
--DELETE FROM AccrualFlatRate2;   
--INSERT INTO AccrualFlatRate2
DELETE FROM zfix_AccrualFlatRate2;   
INSERT INTO zfix_AccrualFlatRate2
SELECT 'Gross', employeenumber, GrossAccount AS Account,
  GrossDistributionPercentage/100.0 * Gross AS Amount
FROM zfix_AccrualFlatRate1
--FROM AccrualFlatRate1
UNION ALL 
SELECT 'PTO', employeenumber, PTOAccount AS Account,
  GrossDistributionPercentage/100.0 * PTO AS Amount
FROM zfix_AccrualFlatRate1
--FROM AccrualFlatRate1
WHERE PTO <> 0
UNION ALL
SELECT 'FICA', employeenumber, FicaAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(FicaExempt, 0)) * FicaPercentage/100.0, 2)) AS Amount
FROM zfix_AccrualFlatRate1
--FROM AccrualFlatRate1
GROUP BY employeenumber, FicaAccount
UNION ALL 
SELECT 'MEDIC', employeenumber, MedicareAccount AS Account,
  sum(round(((GrossDistributionPercentage/100.0 * TotalNonOTPay) - coalesce(MedicareExempt, 0)) * MedicarePercentage/100.0, 2)) AS Amount
FROM zfix_AccrualFlatRate1
--FROM AccrualFlatRate1
GROUP BY employeenumber, MedicareAccount
UNION ALL 
SELECT  'RETIRE', employeenumber, RetirementAccount AS Account,
  sum(
    CASE 
      WHEN FixedDeductionAmount IS NULL THEN 0
    ELSE 
      CASE 
        WHEN TotalNonOTPay * FixedDeductionAmount/200.0  < .02 * TotalNonOTPay
          THEN round(GrossDistributionPercentage/100.0 * TotalNonOTPay * FixedDeductionAmount/200.0, 2)
        ELSE round(GrossDistributionPercentage/100.0 * .02 * TotalNonOTPay, 2) 
      END 
    END) AS Amount     
FROM zfix_AccrualFlatRate1
--FROM AccrualFlatRate1
GROUP BY employeenumber, RetirementAccount;	

/*
select COUNT(*) FROM zfix_AccrualFlatRate2

select COUNT(*) FROM AccrualFlatRate2

select * FROM zfix_AccrualFlatRate2 ORDER BY category

SELECT * FROM zfix_accrualflatrategross

select * FROM zfix_AccrualFlatRate1

-- accross the board, only one account per store, dist_code, seq for vacation, holiday, sick_leave
SELECT company_number, dist_code, seq_number
FROM (
select company_number, dist_code, seq_number, vacation_expense_act_, holiday_expense_act_, sick_leave_expense_act_
FROM zfixpyactgr
GROUP BY company_number, dist_code, seq_number, vacation_expense_act_, holiday_expense_act_, sick_leave_expense_act_
) x
GROUP BY company_number, dist_code, seq_number
HAVING COUNT(*) > 1

*/

select a.*, b.*, c.name
FROM zfix_accrualflatrate2 a
LEFT JOIN accrualflatrate2 b on a.employeenumber  = b.employeenumber 
  AND a.account = b.account
  AND a.category = b.category
LEFT JOIN edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
ORDER BY c.name, a.account  


select c.name, a.employeenumber, SUM(a.amount), SUM(b.amount)
FROM zfix_accrualflatrate2 a
LEFT JOIN accrualflatrate2 b on a.employeenumber  = b.employeenumber 
  AND a.account = b.account
  AND a.category = b.category
LEFT JOIN edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
GROUP BY c.name, a.employeenumber
ORDER BY c.name 

select a.account, SUM(a.amount), SUM(b.amount)
FROM zfix_accrualflatrate2 a
LEFT JOIN accrualflatrate2 b on a.employeenumber  = b.employeenumber 
  AND a.account = b.account
  AND a.category = b.category
LEFT JOIN edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
GROUP BY a.account


select SUM(a.amount), SUM(b.amount)
FROM zfix_accrualflatrate2 a
LEFT JOIN accrualflatrate2 b on a.employeenumber  = b.employeenumber 
  AND a.account = b.account
  AND a.category = b.category
LEFT JOIN edwEmployeeDim c on a.employeenumber = c.employeenumber
  AND c.currentrow = true 
  
SELECT SUM(amount) FROM accrualflatrate2
UNION all
SELECT SUM(amount) FROM zfix_accrualflatrate2
  
  
select a.*, b.gross, b.ptopay
FROM accrualflatrategross a
LEFT JOIN zfix_accrualflatrategross b on a.employeenumber = b.employeenumber
WHERE a.gross <> b.gross

SELECT SUM(gross), COUNT(*) FROM accrualflatrategross -- 142007
SELECT SUM(gross) FROM zfix_accrualflatrategross  --136920

select SUM(totalnonotpay) FROM accrualflatrate1

select category, SUM(amount) FROM accrualflatrate2 GROUP BY category

select category, SUM(amount) FROM zfix_accrualflatrate2 GROUP BY category
