﻿select --b.year_month, a.amount, 
  a.control, d.employee_name, 
  sum(case when b.year_month = 201902 then a.amount end) as feb,
  sum(case when b.year_month = 201903 then a.amount end) as mar
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month in (201902, 201903)
join fin.dim_account c on a.account_key = c.account_key
  and c.account = '12304B'  
left join arkona.xfm_pymast d on a.control = d.pymast_employee_number
  and d.current_row  
where a.post_status = 'Y'
group by a.control, d.employee_name


select b.the_date,
  a.*
-- select sum(amount)  
from fin.fact_gl a
join dds.dim_date b on a.date_key = b.date_key
  and b.year_month in (201903)
join fin.dim_account c on a.account_key = c.account_key
  and c.account = '12304B'  
left join arkona.xfm_pymast d on a.control = d.pymast_employee_number
  and d.current_row  
where a.post_status = 'Y'
  and a.control = '191060'
order by trans, seq